@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')
<?php //dd($user_list); ?>
<div class="right_col" >
<div class="row" style="text-align: left; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.admin.addgroup')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Group Name</label>
                                <div class="clearfix"></div>
                                {!! Form::text('name', $searchFields['name'] ?? '', [
                                    'class'       => 'form-control',
                                    'placeholder' => 'Group Name',
                                ]) !!}
                            </div></div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                {!! Form::select('status', array('-1'=>'Select Status','1'=>'Active','0'=>'Inactive'), $searchFields['status'] ?? '', [
                                    'class'       => 'form-control',
                                ]) !!}
                            </div></div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning">Clear</a>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Group Name </th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($group_list) > 0)
                                    @foreach ($group_list as $group)
                                    <tr style="cursor: pointer;" onclick="groupEditLoadData(this,'{{$group->id}}','{{$group->name}}','{{$group->status}}')" >
                                        <td class="common_td_rules name">{{ $group->name }}</td>
                                        <td class="common_td_rules status">{{ $group->status_name }}</td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="user_name">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.admin.savegroup')}}" method="POST" id="groupForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="group_id" id="group_id" value="0">

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Group Name</label><span class="error_red">*</span>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" required  name="name" value="{{request()->old('name')}}" id="name">
                            <span class="error_red">{{ $errors->first('name') }}</span>
                        </div></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Status</label><span class="error_red">*</span>
                            <div class="clearfix"></div>
                            <select class="form-control" name="status" id="status">
                                <option value="1">{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                            <span class="error_red">{{ $errors->first('status') }}</span>
                        </div></div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function () {
        // $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

    });

    function groupEditLoadData(obj,id,name,status){
        var edit_icon_obj = $(obj);
        $('#group_id').val(id);
        $('#name').val(name);
        $('#status').val(status);
        $('#groupForm').attr('action', "{{$updateUrl ?? ''}}");
    }
    function resetFunction(){
         $('#groupForm')[0].reset();
    }
@include('Purchase::messagetemplate')
</script>

@endsection
