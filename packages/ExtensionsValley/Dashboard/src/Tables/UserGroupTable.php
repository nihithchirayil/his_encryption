<?php
namespace ExtensionsValley\Dashboard\Tables;

use ExtensionsValley\Dashboard\Tables\BaseTable;
use Illuminate\Http\Request;

class UserGroupTable extends BaseTable
{

    /**
     * The database table used by the model.
     *
     * @var string
     */

    public $page_title = "Assign User Groups";

    public $table_name = "user_group";

    public $acl_key = "extensionsvalley.dashboard.usergroup";

    public $namespace = 'ExtensionsValley\Dashboard\Tables\UserGroupTable';

    public $overrideview = "Dashboard::user.assignusergroupform";

    public $model_name = 'ExtensionsValley\Dashboard\Models\UserGroup';

    public $listable = [];

    public $show_toolbar = [];

    public $routes = [];

    public $advanced_filter = [];

}
