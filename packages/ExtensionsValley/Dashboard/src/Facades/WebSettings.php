<?php
namespace ExtensionsValley\Dashboard\Facades;

use Illuminate\Support\Facades\Facade;

class WebConf extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    public static $version = "2.0";

    protected static function getFacadeAccessor()
    {
        return 'WebConf';
    }

    public static function get($key)
    {
        if (!\Cache::has($key)) {
            $vals =  \DB::table('gen_settings')
                    ->where('settings_key', $key)
                    ->value('settings_value');

            \Cache::forever($key, $vals);
        }else{
            $vals =  \Cache::get($key);
        }
        return $vals;
    }

    public static function checkKey($key)
    {
        return \DB::table('gen_settings')
            ->where('settings_key', $key)
            ->count();
    }

    public static function getConfig($config_name){

		$app_company = strtolower ( env ( 'APP_COMPANY' ) );
		
		if (\Config::has($app_company . '.' . $config_name)){
			return config ($app_company . '.' . $config_name);
		}else{
			return config ('settings.' . $config_name);
		}
	
	}

}
