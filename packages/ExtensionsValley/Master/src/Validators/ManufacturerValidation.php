<?php
namespace ExtensionsValley\Master\Validators;

class ManufacturerValidation
{
    public function getManufacturerUpdateRules($manufacturer)
    {
        return [
            'manufacturer_name' => 'required|max:255',
            'manufacturer_code' => 'required|max:50|unique:manufacturer,manufacturer_code,' .$manufacturer->id,
            'status' => 'required',
        ];
    }
    public function getManufacturerRules()
    {
        return [
            'manufacturer_name' => 'required|max:255',
            'manufacturer_code' => 'required|max:50|unique:manufacturer,manufacturer_code',
            'status' => 'required',
        ];
    }
}