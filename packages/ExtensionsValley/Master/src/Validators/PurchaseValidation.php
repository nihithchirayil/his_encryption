<?php
namespace ExtensionsValley\Master\Validators;

class PurchaseValidation
{
    public function getPurchaseUpdateRules()
    {
        return [
            'name' => 'required|max:255',
            // 'manufacturer_code' => 'required|max:50|unique:manufacturer,manufacturer_code,' .$manufacturer->id,
            'status' => 'required',
        ];
    }
    public function getPurchaseRules()
    {
        return [
            'name' => 'required|max:255',
            'status' => 'required',
        ];
    }
}