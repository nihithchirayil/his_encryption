<?php
namespace ExtensionsValley\Master\Validators;

class CommonMasterValidation
{

    public function getNursingMasterRules()
    {
        return [
            'location_name' => 'required|max:255',
            'status' => 'required',
        ];
    }
    public function getSubcategoryUpdateRules($subcategory)
    {
        return [
            'subcategory_name' => 'required|max:255',
            'subcategory_code' => 'required|max:50|unique:sub_category,subcategory_code,' . $subcategory->id,
          //  'item_type' => 'required',
            'status' => 'required',
        ];
    }

    public function getLedgerRules()
        {
            return [
                'payment_receipt' => 'required',
                'bank_name' => 'required',
            ];
        }
         public function getLedgerGroupRules()
        {
            return [
                'group_name' => 'required',
            ];
        }
        public function getLedgerMasterRules()
        {
            return [
                'ledger_name' => 'required',
            ];
        }
        public function getLedgerMappingRules()
        {
            return [
                'ledger_name' => 'required',
                'mapping_type' => 'required|not_in:0',
                'mapping_value' => 'required',
            ];
        }
        public function getProductSellingRules()
        {
            return [
                'product_id' => 'required',
                'batch_no' => 'required',
            ];
        }
        public function getTaxRules()
        {
            return [
                'tax_code' => 'required',
                'tax_name' => 'required',
            ];
        }
        public function getVoucherTypeRules()
        {
            return [
                'type' => 'required',
            ];
        }
        public function getGenericRules(){
            return [
                'generic_name' => 'required',
            ];
        }
        public function getAccountsMappingRules()
        {
            return [
                'ledger_name' => 'required',
                'spec_name' => 'required',
                'dept_name' => 'required',
            ];
        }
}
