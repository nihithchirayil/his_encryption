<?php
namespace ExtensionsValley\Master\Validators;

class LocationValidation
{
    public function getLocationUpdateRules()
    {
        return [
            'name' => 'required|max:255',
            // 'manufacturer_code' => 'required|max:50|unique:manufacturer,manufacturer_code,' .$manufacturer->id,
            'status' => 'required',
        ];
    }
    public function getLocationRules()
    {
        return [
            'name' => 'required|max:255',
            'status' => 'required',
        ];
    }
}