<?php
namespace ExtensionsValley\Master\Validators;

class ProductValidation
{

    public function getRules()
    {
        return [
            'item_desc' => 'required|max:255',
        ];
    }

    public function getUpdateRules($item)
    {
        return [
            // 'item_desc' => 'required|max:255|unique:product,item_desc,' . $item->id,
            'item_desc' => 'required|max:255',
        ];
    }

    public function getLocationRules()
    {
        return [
            'location_name' => 'required|max:255',
            // 'default_product_type' => 'required|not_in:-1',
            'location_code' => 'required|max:50|unique:location',
           'location_type' => 'required|not_in:-1',
            'status' => 'required',
            // 'email_id' => 'email',
        ];
    }

    public function getLocationUpdateRules($location)
    {
        return [
            'location_name' => 'required|max:255',
            // 'default_product_type' => 'not_in:-1',
            'location_code' => 'required|max:50|unique:location,location_code,' . $location->id,
           'location_type' => 'required|not_in:-1',
            'status' => 'required',
            // 'email_id' => 'email',
        ];
    }

    public function getMenuAccessRules()
    {
        return [
            'acl_key' => 'required|not_in:-1',
            'group_id' => 'required|not_in:-1',
            'status' => 'required'
        ];
    }

    public function getMenuAccessUpdateRules($menu)
    {
        return [
            'acl_key' => 'required|not_in:-1',
            'group_id' => 'required|not_in:-1',
            'status' => 'required'
        ];
    }
        public function getPacsAccessRules()
    {
        return [
            'pacs_id' => 'required|not_in:-1',
            'group_id' => 'required|not_in:-1',
            'status' => 'required'
        ];
    }

    public function getPacsAccessUpdateRules($menu)
    {
        return [
            'pacs_id' => 'required|not_in:-1',
            'group_id' => 'required|not_in:-1',
            'status' => 'required'
        ];
    }

}
