<?php
namespace ExtensionsValley\Master\Validators;

class RackValidation
{

    public function getRules()
    {
        return [
            'name' => 'required|max:255',
            'code' => 'required|max:50|unique:rack_master',
            'location' => 'required',
            'status' => 'required',
        ];
    }

    public function getUpdateRules($rack)
    {
        return [
            'name' => 'required|max:255',
            'code' => 'required|max:50|unique:rack_master,code,' .$rack->id,
            'location' => 'required',
            'status' => 'required',
            
        ];
    }

    public function getPercentageRules()
    {
        return [
            'doctor_id' => 'required|not_in:-1',
            'service_id' => 'required|not_in:-1',
            'item_type' => 'required',
            'effective_date' => 'required',
            'percentage' => 'required',
            'status' => 'required',
        ];
    }
    public function getOTBillHeadRules()
    {
        return [
            'group_name' => 'required',
            'group_code' => 'required',
            // 'color_code' => 'required',
            'bill_tag_id' => 'required|not_in:-1',
            // 'group_no' => 'required',
        ];
    }
    public function getOTBillDetailRules()
    {
        return [
            'group_name' => 'required|not_in:-1',
            'service_name' => 'required|not_in:-1',
            'price' => 'required',
        ];
    }

}
