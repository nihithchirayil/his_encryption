<?php
namespace ExtensionsValley\Master\Validators;

class CategoryValidation
{

    public function getRules()
    {
        return [
            'name' => 'required|max:255',
            'category_code' => 'required|max:50|unique:category',
           // 'item_type' => 'required',
            'status' => 'required',
        ];
    }

    public function getUpdateRules($category)
    {
        return [
            'name' => 'required|max:255',
            'category_code' => 'required|max:50|unique:category,category_code,' .$category->id,
          //  'item_type' => 'required',
            'status' => 'required',
            
        ];
    }

    public function getSubcategoryRules()
    {
        return [
            'subcategory_name' => 'required|max:255',
            'category_id' => 'required',
            'subcategory_code' => 'required|max:50|unique:sub_category',
           // 'item_type' => 'required',
            'status' => 'required',
        ];
    }
    public function getSubcategoryNewRules()
    {
        return [
            'subcategory_name' => 'required|max:255',
            'category_id' => 'required',
            // 'subcategory_code' => 'required|max:50|unique:sub_category',
           // 'item_type' => 'required',
            'status' => 'required',
        ];
    }
    public function getSubcategoryUpdateRules($subcategory)
    {
        return [
            'subcategory_name' => 'required|max:255',
            'subcategory_code' => 'required|max:50|unique:sub_category,subcategory_code,' .$subcategory->id,
          //  'item_type' => 'required',
            'status' => 'required',
        ];
    }

    public function getFavouriteRules()
    {
        return [
            'group_name' => 'required|max:255',
            'doctor' => 'required|not_in:0'
        ];
    }

}
