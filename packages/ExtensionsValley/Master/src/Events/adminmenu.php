<?php

namespace ExtensionsValley\Master\Events;

\Event::listen('admin.menu.groups', function ($collection) {

    $collection->put('extensionsvalley.master', [
        'menu_text' => 'Master'
        , 'menu_icon' => '<i class="fa fa-cogs"></i>'
        , 'acl_key' => 'extensionsvalley.master.masters'
        , 'sub_menu' => [
            '0' => [
                'link' => url('/') . '/master/listCategory'
                , 'menu_text' => 'Manage Category'
                , 'acl_key' => 'extensionsvalley.master.listCategory'
            ],
            '1' => [
                'link' => url('/') . '/master/listSubCategory'
                , 'menu_text' => 'Manage Sub-Category'
                , 'acl_key' => 'extensionsvalley.master.listSubCategory'
            ],
            '2' => [
                'link' => url('/') . '/item/listItem'
                , 'menu_text' => 'Manage Item'
                , 'acl_key' => 'extensionsvalley.item.listItem'
            ],
            '3' => [
                'link' => url('/') . '/item/listLocation'
                , 'menu_text' => 'Manage Location'
                , 'acl_key' => 'extensionsvalley.item.listLocation'
            ],
            '4' => [
                'link' => url('/') . '/master/insuranceSettlementList'
                , 'menu_text' => 'Insurance Settlement List'
                , 'acl_key' => 'extensionsvalley.master.insuranceSettlementList'
            ],
            '5' => [
                'link' => url('/') . '/master/daya_loyality'
                , 'menu_text' => 'Daya Loyality List'
                , 'acl_key' => 'extensionsvalley.master.daya_loyality'
            ],
            '6' => [
                'link' => url('/') . '/master/share_holder_list'
                , 'menu_text' => 'Daya Share Holder List'
                , 'acl_key' => 'extensionsvalley.master.share_holder_list'
            ],
            '7' => [
                'link' => url('/').'/admin/bill_tag_group'
                , 'menu_text' => 'Assign BillTag Group'
                , 'acl_key' => 'extensionsvalley.admin.bill_tag_group'
            ],

        ],
        ]);
    $collection->put('extensionsvalley.account', [
        'menu_text' => 'Accounts'
        , 'menu_icon' => '<i class="fa fa-book"></i>'
        , 'acl_key' => 'extensionsvalley.master.accounts'
        , 'sub_menu' => [
            '7' => [
                'link' => url('/') . '/master/list_ledger_master'
                , 'menu_text' => ' Ledger Master'
                , 'acl_key' => 'extensionsvalley.master.list_ledger_master'
            ],
            '8' => [
                'link' => url('/') . '/master/list_ledger_group'
                , 'menu_text' => ' Ledger Group'
                , 'acl_key' => 'extensionsvalley.master.list_ledger_group'
            ],
            '9' => [
                'link' => url('/') . '/master/ledger_booking_entry_list'
                , 'menu_text' => 'Booking Entry'
                , 'acl_key' => 'extensionsvalley.master.ledger_booking_entry_list'
            ],
            '10' => [
                'link' => url('/') . '/master/pay_receipt_ledger_list'
                , 'menu_text' => 'Payment Receipt Entry'
                , 'acl_key' => 'extensionsvalley.master.pay_receipt_ledger_list'
            ],
        ],
    ]);
});
