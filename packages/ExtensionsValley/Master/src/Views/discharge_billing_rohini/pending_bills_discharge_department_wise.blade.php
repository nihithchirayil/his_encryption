@php 
$i =0;
@endphp
@if(!empty($pending_bills))
@foreach($pending_bills['discharge_items'] as $key => $val)
@php 
$i++;
@endphp
    <tr>
        <td title="{{$i+1}}">{{$i}}</td>
        <td class="department_row" title="{{$val['department_name']}}"><input type="checkbox" class="department_check" checked="checked" style="width: 15px; margin: 0;" name="department_check" value="{{$val['department_id']}}" /></td>
        <td class="common_td_rules" style="text-align:left;" title="{{$val['department_name']}}">{{$val['department_name']}}</td>
        <td title="{{$val['gross_amount']}}">{{$val['gross_amount']}}</td>
        {{-- <td title="{{$val['discount_amount']}}">{{$val['discount_amount']}}</td> --}}
        <td title="{{$val['net_amount']}}">{{$val['net_amount']}}</td>
        <td title="{{$val['patient_payable']}}">{{$val['patient_payable']}}</td>
        <td title="{{$val['company_payable']}}">{{$val['company_payable']}}</td>
    </tr>
@endforeach
@else
<td colspan="7">No Data Found..!</td>
@endif