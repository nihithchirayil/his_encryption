@php 
$i =0;
@endphp
@if(!empty($pending_bills))
@foreach($pending_bills['discharge_items'] as $key1 => $val1)
@foreach($val1['item_array'] as $key => $val)
@php
$i++;
$bill_date = (!empty($val['bill_date'])) ? ExtensionsValley\Emr\CommonController::getDateFormat($val['bill_date'],'M-d-Y') : '';
@endphp
    <tr class="bill_row_{{$val['department_id']}} bills_row" data-bill-head-id="{{$val['bill_head_id']}}" data-credit-bill-detail-id="{{$val['credit_bill_detail_id']}}">
        <td class="serial_no" title="{{$i+1}}">{{$i}}</td>
        <td title="{{$val['bill_no']}}">{{$val['bill_no']}}</td>
        <td title="{{$bill_date}}">{{$bill_date}}</td>
        <td class="common_td_rules" title="{{$val['item_desc']}}" style="text-align:left;">{{$val['item_desc']}}</td>
        <td title="{{$val['qty']}}">{{$val['qty']}}</td>
        <td title="{{$val['selling_price_wo_tax']}}">{{$val['selling_price_wo_tax']}}</td>
        <td title="{{$val['tax_percent']}}">{{$val['tax_percent']}}</td>
        <td title="{{$val['discount']}}">{{$val['discount']}}</td>
        {{-- <td title="{{$val['discount_amount']}}">{{$val['discount_amount']}}</td> --}}
        <td class="net_amount_discharge" title="{{$val['net_amount']}}">{{$val['net_amount']}}</td>
        <td class="patient_payable_amount" title="{{$val['patient_to_pay']}}">{{$val['patient_to_pay']}}</td>
        <td class="company_payable_amount" title="{{$val['company_payable']}}">{{$val['company_payable']}}</td>
        <td>
            @if ($val['bill_tag']!='PH' && $val['discharge_bill_detail_id']!=0)
            <button title="Edit Bill" type="button" class="btn btn-sm btn-primary editServiceBillModal" data-dis_amt="{{$val['discount_amount']}}"  data-selling_price_wo_tax="{{$val['selling_price_wo_tax']}}" data-credit_bill_detail_id="{{ $val['credit_bill_detail_id'] }}" data-net_amt="{{$val['net_amount']}}" data-qty="{{$val['qty']}}" data-item="{{$val['item_desc']}}" data-bill_no="{{$val['bill_no']}}" data-bill_tag="{{ $val['bill_tag'] }}" ><i class="fa fa-edit"></i></button>
                
            @endif
            <!-- <button title="View Bill" type="button" class="btn btn-sm btn-primary" ><i class="fa fa-eye"></i></button> -->
        </td>
    </tr>
@endforeach
@endforeach
@else
<td colspan="12">No Data Found..!</td>
@endif