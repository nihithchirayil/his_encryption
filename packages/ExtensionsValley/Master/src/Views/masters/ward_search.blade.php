<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="10%">Ward Number</th>
                <th width="11%">Short Name</th>
                <th width="20%">Ward Name</th>
                <th width="10%">Block</th>
                <th width="13%">Floor</th>
                <th width="13%">Nursing Location</th>
                <th width="7%">Status</th>
                <th style = "width:10%">Action</th>
            </tr>
        </thead>
        <tbody>
            @if(count($wards) > 0)
                @foreach ($wards as $ward)
                    <tr style="cursor: pointer;"  data-id="{{ $ward->id }}" data-wrd-name="{{ $ward->ward_name }}" data-ward_no="{{ $ward->ward_no }}" 
                        data-short="{{ $ward->ward_short }}" data-floor="{{ $ward->floor_id }}" data-blk="{{ $ward->block_name }}"
                        data-status="{{ $ward->status_name }}" data-nurs="{{ $ward->nursing_location_id}}" >
                        <td class = "td_common_numeric_rules">{{($wards->currentPage() - 1) * $wards->perPage() + $loop->iteration}}</td>
                        <td class ="td_common_numeric_rules">{{ $ward->ward_no}}</td>
                        <td class ="common_td_rules">{{ $ward->ward_short}}</td>
                        <td class ="common_td_rules">{{ $ward->ward_name }}</td>
                        <td class ="common_td_rules">{{ $ward->block_name}}</td>
                        <td class ="common_td_rules">{{ $ward->floor_name}}</td>
                        <td class ="common_td_rules">{{ $ward->nursing_location}}</td>
                        <td class ="common_td_rules">{{ $ward->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            {{-- <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button> --}}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>