<script type="text/javascript">
    $(document).ready(function () {
        $(".page-link").click(function () {
            var url = $(this).attr("href");
            var brand_name=$('#brand_name_hidden').val();
            var chemical_name=$('#chemical_name_hidden').val();
            var generic_name=$('#generic_name_hidden').val();
            var therapeutic_category=$('#therapeutic_category_hidden').val();
            var therapeutic_subcategory=$('#therapeutic_subcategory_hidden').val();
            var manufacturer_name=$('#manufacturer_name_hidden').val();
            var param={brand_name:brand_name,chemical_name:chemical_name,generic_name:generic_name,therapeutic_category:therapeutic_category,therapeutic_subcategory:therapeutic_subcategory,manufacturer_name:manufacturer_name,from_type:1};

    $.ajax({
        type: "GET",
        url: url,
        data: param,
        beforeSend: function() {
            $('#searchpharmacydatabtn').attr('disabled', true);
            $('#searchpharmacydataspin').removeClass('fa fa-search');
            $('#searchpharmacydataspin').addClass('fa fa-spinner');
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function(html) {
            $('#ResultsViewArea').html(html);
            $('#print_results').removeClass('disabled');
            $('#csv_results').removeClass('disabled');
        },
        complete: function() {
            $('#ResultsViewArea').LoadingOverlay("hide");
            $('#searchpharmacydatabtn').attr('disabled', false);
            $('#searchpharmacydataspin').removeClass('fa fa-spinner');
            $('#searchpharmacydataspin').addClass('fa fa-search');
            $('#ResultDataContainer').css('display', 'block');
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
        },
        error: function() {
            Command: toastr["error"]("Network Error!");
            $('#ResultDataContainer').css('display', 'hide');
            return;
        }

         });
            return false;
        });

    });

</script>
<style>
    .ps-scrollbar-x,.ps-scrollbar-x-rail{
        width: 0px !important;
    }
    table tr:hover td{
        text-overflow: initial;
        white-space: normal;
    }
</style>
<div class="box-body clearfix" id="result_container_div">
<div class="theadscroll" style="position: relative; max-height: 450px;cursor:pointer;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="5%">SL.No.</th>
                <th width="15%">Brand Name</th>
                <th width="8%">Dosage Form</th>
                {{-- <th width="10%">Chemical Name</th> --}}
                <th width="25%">Generic Name</th>
                <th width="25%">Thearapeutic Category</th>
                <th width="15%">Thearapeutic Subcategory</th>
                <th width="12%">Manufacturer</th>
                
            </tr>
        </thead>
        <tbody>
            @if(count($list) > 0)
                @foreach ($list as $data)
                    <tr>
                        <td class="td_common_numeric_rules">
                            {{ ($list->currentPage() - 1) * $list->perPage() + $loop->iteration }}</td>
                        <td class="common_td_rules">{{$data->brand_name ? $data->brand_name : '-'}}</td>
                        <td class="common_td_rules">{{$data->dose ? $data->dose : '-'}}</td>
                        {{-- <td class="common_td_rules">{{$data->generic_name ? $data->generic_name : '-'}}</td> --}}
                        <td class="common_td_rules">{{$data->generic_name ? $data->generic_name : '-'}}</td>
                        <td class="common_td_rules">{{$data->therapeutic_category ? $data->therapeutic_category : '-'}}</td>
                        <td class="common_td_rules">{{$data->therapeutic_subcategory ? $data->therapeutic_subcategory : '-'}}</td>
                        <td class="common_td_rules">{{$data->manufacturer_name ? $data->manufacturer_name : '-'}}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="7" class="category_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-right">
        <ul class="pagination purple_pagination" style="text-align:right !important;">
            {!! $page_links !!}
        </ul>
    </div>
</div>
