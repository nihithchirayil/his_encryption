@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.css')}}" rel="stylesheet">
<style>
.ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 200px;
        margin: 0px 0px 0px 0px;
        overflow-y: auto;
        width: 34%;
        z-index: 599;
        position:absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
</style>
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.listRacks')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Rack Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="code" id="code_search"
                                    value="{{ $searchFields['code'] ?? '' }}">
                            </div></div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Rack Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="name" id="name_search"
                                    value="{{ $searchFields['name'] ?? '' }}">
                            </div></div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Location</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="location" id="location_search"
                                    value="{{ $searchFields['location'] ?? '' }}">
                            </div></div>
                            
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                             <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>  Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; max-height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Rack Code</th>
                                    <th>Rack Name</th>
                                    <th>Location</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($list) > 0)
                                    @foreach ($list as $data)
                                        <tr style="cursor: pointer;" onclick="rackEditLoadData(this,
                                            '{{$data->rack_id}}',
                                            '{{$data->code}}',
                                            '{{$data->name}}',
                                            '{{$data->status}}',
                                            '{{$data->location_code}}',
                                            '{{$data->location}}')">
                                            <td class="code common_td_rules">{{ $data->code }}</td>
                                            <td class="category_name common_td_rules">{{ $data->name }}</td>
                                            <td class="item common_td_rules">{{ $data->location  }}</td>
                                            <td class="status common_td_rules">{{ $data->status_name  }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="category_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.saveRacks')}}" method="POST" id="rackForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="rack_id" id="rack_id" value="0">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Rack Code</label>
                            <div class="clearfix"></div>
                            <input type="text" required class="form-control" name="code" id="code">
                            <div class="" id="rack_code_status">
                            </div>
                        </div></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Rack Name</label>
                            <div class="clearfix"></div>
                            <input type="text" required class="form-control" name="name" id="name">
                        </div></div>
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Location</label>
                                <div class="clearfix"></div>
                               <input type="hidden" class="form-control" name="location" id="location">
                               <input type="text" required autocomplete="off" onkeyup="searchLocation(this,event)" name="location_name" id="location_name" class="form-control">
                                <div class="ajaxSearchBox" id="locationdiv" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px; margin: -2px 0px 0px 0px; overflow-y: auto; width: 100%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;height: 270px; border: 1px solid rgba(0, 0, 0, 0.3);"> 
                            </div></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Status</label>
                            <div class="clearfix"></div>
                            <select class="form-control" name="status" id="status">
                                <option value="1">{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                        </div></div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
         // $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        $('#code').keyup(function () {

            var category_code_str = $(this).val();
            category_code_str = category_code_str.replace(/[^a-zA-Z0-9]/g, '-');

            category_code_str = category_code_str.replace(/\-+/g, '');

            category_code_str = category_code_str.toUpperCase();

            var category_code = category_code_str.trim();

            $('#code').val(category_code);

            if (category_code != '') {
                var url = '{{route('extensionsvalley.master.checkRackCode')}}';
                $.ajax({
                    method: "GET",
                    url: url,
                    data: 'code=' + category_code,
                    success: function (data) {
                        console.log(data);
                        if (data == 0) {
                            $('#code').css('color', 'green');
                            $('#rack_code_status').html(
                                '<button tabindex="-1" type="button" title="Available" class="btn btn-success btn-sm"><i class="fa fa-check fa-xs" aria-hidden="true"></i></button>'
                            );

                        } else {
                            $('#code').css('color', 'red');
                            $('#rack_code_status').html(
                                '<button tabindex="-1" type="button" title="Not available" class="btn btn-danger btn-sm"><i class="fa fa-ban fa-xs" aria-hidden="true"></i></button>'
                            );
                        }
                    }

                });

            }


        });

    });

    function rackEditLoadData(obj,rack_id,code,name,status,location_code,location){
        var edit_icon_obj = $(obj);
        $('#rack_id').val(rack_id);
        $('#code').prop('readonly',true).val(code);
        $('#name').val(name);
        $('#status').val(status);
        $('#location').val(location_code);
        $('#location_name').val(location);
        $('#rackForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
         $('#rackForm')[0].reset();
         $('#code').prop('readonly',false).val("");
    }
    function searchLocation(obj,e) { 
        var keycheck = /[a-zA-Z0-9 ]/; 
        var value = String.fromCharCode(e.keyCode);
        if (value.match(keycheck) || e.keyCode == 8 || e.keyCode == 46) {
            var location = $(obj).val();
            if (location == "") {
                $("#locationdiv").html("");
            } else {
            var url = "";
                $.ajax({
                type: "GET",
                    url: url,
                    data:  'location_name=' + location + '&location_name_search=1',
                    beforeSend: function () {
                    $("#locationdiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#locationdiv").html(html).show();
                        $("#locationdiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            }

            }else {
            ajaxProgressiveKeyUpDown('locationdiv', e);
            }
}
function fillLocationData(name,code){
    $('#location_name').val(name);
    $('#location').val(code);
}
@include('Purchase::messagetemplate')
</script>

@endsection
