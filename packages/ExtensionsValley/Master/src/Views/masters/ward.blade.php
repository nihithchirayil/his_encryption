@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
            <h4 class="blue text-left"> <strong>Ward Master</strong></h4>
            <div class="col-md-8 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Ward Name</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="ward" value="">
                            <input type="hidden" value="" id="ward_hidden">
                            <div class="ajaxSearchBox" id="wardAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                <select class="form-control status" name="status" id="status" >
                                    <option value="">{{__('All')}}</option>
                                    <option value="1">{{__('Active')}}</option>
                                    <option value="0">{{__('Inactive')}}</option>
                                </select>
                            </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Floor</label>
                                <div class="clearfix"></div>
                                <select class="form-control floor" name="floor" id="floor" >
                                    <option value="">--Select--</option>
                                    <option value="1">{{__('FIRST FLOOR')}}</option>
                                    <option value="2">{{__('SECOND FLOOR')}}</option>
                                    <option value="3">{{__('THIRD FLOOR')}}</option>
                                    <option value="4">{{__('FOURTH FLOOR')}}</option>
                                </select>
                            </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Nursing Location</label>
                                <div class="clearfix"></div>
                                <select class="form-control nurse" name="nurse" id="nurse" >
                                    <option value="">--Select--</option>
                                    <option value="511">{{__('JR ICU ')}}</option>
                                    <option value="508">{{__('LABOUR ROOM(N)')}}</option>
                                    <option value="605">{{__('TESTLOCATION')}}</option>
                                </select>
                            </div>
                            </div>
                            <div class="col-md-2 padding_sm ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" ><i class="fa fa-search"></i>
                                    Search</button>
                            </div>



                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}" >

                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="box no-border" >
                    <div class="box-body clearfix" style=" height : 517px">
                        <h4 class="blue text-center"><span id="add"> Add Ward </span></h4>
                        <hr >
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Ward Number</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control ward_no_add" name="ward_no_add" value=" {{  $wards->ward_no ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Short Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control short_add" name="short_add" value=" {{  $wards->ward_short ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Ward name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control ward_add" name="ward_add" value=" {{  $wards->ward_name ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Block</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control block_add" name="block_add" value=" {{  $wards->block_name ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Floor</label>
                            <div class="clearfix"></div>
                            <select class="form-control floor_add" name="floor_add" id="floor_add" >
                                <option value="">--Select--</option>
                                <option value="1">{{__('FIRST FLOOR')}}</option>
                                <option value="2">{{__('SECOND FLOOR')}}</option>
                                <option value="3">{{__('THIRD FLOOR')}}</option>
                                <option value="4">{{__('FOURTH FLOOR')}}</option>
                            </select>
                        </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Nursing Location</label>
                            <div class="clearfix"></div>
                            <select class="form-control nurse_add" name="nurse_add" id="nurse_add" >
                                <option value="">--Select--</option>
                                <option value="511">{{__('JR ICU ')}}</option>
                                <option value="508">{{__('LABOUR ROOM(N)')}}</option>
                                <option value="605">{{__('TESTLOCATION')}}</option>
                            </select>
                        </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-1 padding_sm">
                                {{-- <div class="mate-input-box"> --}}
                                <label for="">Active</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="form-control status_add" name="status_add" id="status_add" value="1"> 
                                <div id='status1'>
                            </div></div>
                            {{-- </div> --}}
                        </div>
                       
                        <input type="hidden" name="reference_id" class="reference_id" >
                        <div class="col-md-4 padding_sm pull-right"  style="margin-bottom:10px;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-success saveButton" onclick="saveForm()"><i class="fa fa-save"></i> Save </button>
                        </div>
                        <div class="col-md-4 padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-info clearFormButton" onclick="resetForm()"><i class="fa fa-times"></i> Clear </button>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/ward.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
{{-- <script src="js/loadingoverlay.js"></script> --}}
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
