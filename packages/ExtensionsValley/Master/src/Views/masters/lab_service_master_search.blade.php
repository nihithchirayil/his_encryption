<style>
    table tr:hover td{
        text-overflow: initial;
        white-space: normal;
    }
</style>
<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="7%">Service Code</th>
                <th width="13%">Service Name</th>
                <th width="10%">Department</th>
                <th width="17%">Sub Department</th>
                <th width="9%">NABH Price</th>
                <th width="13%">Room Type</th>
                <th width="13%">Effective Date</th>
                <th width="7%">Status</th>
                <th style = "width:15%">Action</th>
            </tr>
        </thead>
        <tbody>
            @if(count($lab_service_master) > 0)
                @foreach ($lab_service_master as $service)
                    <tr style="cursor: pointer;"  data-id="{{ $service->id }}" 
                        data-service_code="{{ $service->service_code }}" data-service_desc="{{ $service->service_desc }}" data-lab_item="{{ $service->lab_item }}"
                        data-department_id="{{ $service->department_id }}" data-subdepartment_id="{{ $service->subdepartment_id }}" 
                        data-billable="{{ $service->billable }}" data-refundable="{{ $service->refundable }}" data-discountable="{{ $service->discountable }}"
                        data-duration="{{ $service->duration }}" data-duration_mode="{{ $service->daily }}" data-duration_type="{{ $service->warning }}" 
                        data-restrict_duration_count="{{ $service->restrict_duration_count }}" data-procedure="{{ $service->procedure }}" data-discharge_item="{{ $service->discharge_item }}"
                        data-hosp_service="{{ $service->hosp_service }}" data-doc_required="{{ $service->doc_required }}" data-nursing_service="{{ $service->nursing_service }}"
                        data-esi_code="{{ $service->esi_code }}" data-esi_name="{{ $service->esi_name }}" data-nabh_price="{{ $service->nabh_price}}"
                        data-status="{{ $service->status_name }}" data-gender="{{ $service->gender}}" data-price_editable="{{ $service->price_editable }}"
                        data-effective_date="{{ $service->date }}" 
                        data-report_type="{{ $service->report_type}}" data-vacutainer="{{ $service->vacutainer_id}}" data-shortname="{{ $service->short_name}}"
                        data-printorder="{{ $service->lab_service_order }}"  data-opprice="{{ $service->op }}"  data-ipprice="{{ $service->ip }}"
                        data-samplename="{{ $service->sample_id}}" data-status_service_charge="{{ $service->status_service_charge }}">
                        <td class = "td_common_numeric_rules">{{($lab_service_master->currentPage() - 1) * $lab_service_master->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $service->service_code}}</td>
                        <td class ="common_td_rules">{{ $service->service_desc}}</td>
                        <td class ="common_td_rules">{{ $service->dept_name }}</td>
                        <td class ="common_td_rules">{{ $service->sub_dept_name}}</td>
                        <td class ="td_common_numeric_rules">{{ $service->nabh_price}}</td>
                        <td class ="common_td_rules">{{ $service->room_type}}</td>
                        <td class ="td_common_numeric_rules">{{date('Y-M-d',strtotime($service->date))}}</td>
                        <td class ="common_td_rules">{{ $service->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            {{-- <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button> --}}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>