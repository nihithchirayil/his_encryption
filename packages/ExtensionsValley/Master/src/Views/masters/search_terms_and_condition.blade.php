<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th>Terms</th>
                <th>Status</th>
                <th style = "width:20%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($terms_and_conditions) > 0)
                @foreach ($terms_and_conditions as $terms_and_condition)
                    <tr style="cursor: pointer;"  data-id="{{ $terms_and_condition->id }}" data-tac-name="{{ $terms_and_condition->terms }}" data-status="{{ $terms_and_condition->status_name }}">
                        <td class = "td_common_numeric_rules">{{($terms_and_conditions->currentPage() - 1) * $terms_and_conditions->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $terms_and_condition->terms }}</td>
                        <td class ="common_td_rules">{{ $terms_and_condition->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>