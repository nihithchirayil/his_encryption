<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="1%">SL.NO</th>
                <th width="35%">From Location </th>
                <th width="35%">To Location </th>
                <th width="20%">Created At</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        <tbody>
            @if(count($bulktrs) > 0)
                @foreach ($bulktrs as $bulktr)
                    <tr style="cursor: pointer;"  data-id="{{ $bulktr->id }}" data-vul-name="{{ $bulktr->from_location }}" data-bulk-name="{{ $bulktr->to_location }}" 
                        data-item="{{ $bulktr->group_code }}">
                        <td class = "td_common_numeric_rules">{{($bulktrs->currentPage() - 1) * $bulktrs->perPage() + $loop->iteration}}</td>
                        <td class="location_type common_td_rules">{{ $bulktr->from_location_name }}</td>
                        <td class="location_code common_td_rules">{{ $bulktr->to_location_name }}</td>
                        <td class="location_name td_common_numeric_rules">{{ $bulktr->created_at }}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td class=" td_common_numeric_rules">
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            {{-- <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button> --}}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
    
    <div class="clearfix"></div>
    <div class="col-md-12 text-right">
        <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
            {!! $page_links !!}
        </ul>
    </div>