@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }
    .disabled { color: #999; }
    .enabled { color: #000000; }
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" value='<?= date('M-d-Y') ?>' id="current_date">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
            <h4 class="blue text-left"> <strong>Bill Cancel Request List</strong></h4>
            <div class="col-md-12 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                            <div class="col-md-2 padding_sm" style="margin-top: 5px;">
                                <div class="mate-input-box">
                                    <label for="">Bill Date From</label>
                                    <input type="text" data-attr="date" autocomplete="off" id="bill_date_from"
                                        autofocus="" value="{{date('M-d-Y')}}" class="form-control date-picker"
                                        placeholder="YYYY-MM-DD">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 5px;">
                                <div class="mate-input-box">
                                    <label for="">Bill Date To</label>
                                    <input type="text" data-attr="date" autocomplete="off" id="bill_date_to"
                                        autofocus="" value="{{date('M-d-Y')}}" class="form-control date-picker"
                                        placeholder="YYYY-MM-DD">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 5px;">
                                <div class="mate-input-box">
                                    <label for="">Bill No</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off"
                                        class="form-control hidden_search" id="bill_no" value="">
                                    <input type="hidden" value="0" id="bill_no_hidden">
                                    <div class="ajaxSearchBox" id="bill_noAjaxDiv"
                                        style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999;
                            position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 5px;">
                                <div class="mate-input-box">
                                    <label for="">Bill Tag</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off"
                                        class="form-control hidden_search" id="bill_tag" value="">
                                    <input type="hidden" value="0" id="bill_tag_hidden">
                                    <div class="ajaxSearchBox" id="bill_tagAjaxDiv"
                                        style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999;
                            position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 5px;">
                                <div class="mate-input-box">
                                    <label for="">Patient Name/UHID</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off"
                                        class="form-control hidden_search" id="patient_name" value="">
                                    <input type="hidden" value="0" id="patient_name_hidden">
                                    <div class="ajaxSearchBox" id="patient_nameAjaxDiv"
                                        style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999;
                            position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-md-1 padding_sm" style="">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a onclick="billcancelledData()" class="btn btn-block btn-primary"><i class="fa fa-search"></i> Search</a>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a onclick="resetFilter()" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Reset</a>
                            </div>
                           
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/bill_cancelled_list.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
{{-- <script src="js/loadingoverlay.js"></script> --}}
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
