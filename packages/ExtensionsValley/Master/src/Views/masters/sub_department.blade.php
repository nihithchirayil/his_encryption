@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
            <h4 class="blue text-left"> <strong>Sub Department Master</strong></h4>
            <div class="col-md-8 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                            <div class="col-md-6 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Department Name</label>
                                <div class="clearfix"></div>
                                {{-- <input type="text" class="form-control branch_add" name="branch_add" id="branch_add" value="  "> --}}
                                <select class="form-control select2" id="dept_name" name="dept_name">
                                    <option  value="">Select Department Name</option>
                                    @foreach ($dept_name as $dept)
                                    <option value="{{ $dept->id }}">{{ $dept->dept_name }}</option>
    
                                @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Sub Department Name</label>
                                <div class="clearfix"></div>
                                {{-- <input type="text" class="form-control branch_add" name="branch_add" id="branch_add" value="  "> --}}
                                <select class="form-control select2" id="sub_dept_name" name="sub_dept_name">
                                    <option  value="">Select Sub Department Name</option>
                                    @foreach ($sub_dept_name as $sub_dept)
                                    <option value="{{ $sub_dept->id }}">{{ $sub_dept->sub_dept_name }}</option>
    
                                @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Sub Department Code</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="sub_dept_code" value="">
                            <input type="hidden" value="" id="sub_dept_code_hidden">
                            <div class="ajaxSearchBox" id="sub_dept_codeAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-1 padding_sm" style="width: 105px;margin-block-start: 17px;">
                                <div class="clearfix"></div>
                                <input type="checkbox" class="status" name="status" id="status" value="1"> 
                                <label for="">Active</label>
                                <div id='status1'>
                            </div>
                        </div>
                            <div class="col-md-2 padding_sm ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" ><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}">

                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="box no-border" >
                    <div class="box-body clearfix" style=" height : 255px">
                        <h4 class="blue text-center"><span id="add"> Add Sub Department  </span></h4>
                        <hr >
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Department Name</label>
                            <div class="clearfix"></div>
                            {{-- <input type="text" class="form-control branch_add" name="branch_add" id="branch_add" value="  "> --}}
                            <select class="form-control select2" id="dept_name_add" name="dept_name_add">
                                <option  value="">Select Department Name</option>
                                @foreach ($dept_name as $dept)
                                <option value="{{ $dept->id }}">{{ $dept->dept_name }}</option>

                            @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Sub Department Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control sub_dept_name_add" name="sub_dept_name_add" id="sub_dept_name_add" value=" {{$sub_department->sub_dept_name ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Sub Department Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control sub_dept_code_add" name="sub_dept_code_add" id="sub_dept_code_add" value=" {{$sub_department->sub_dept_code ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="status_add" name="status_add" id="status_add" value="1"> 
                            <label for="">Active</label>
                            <div id='status1'></div>
                        </div>
                       
                      
                        <input type="hidden" name="reference_id" class="reference_id" >
                        <div class="col-md-4 padding_sm pull-right"  style="margin-bottom:10px;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-success saveButton" onclick="saveForm()"><i class="fa fa-save"></i> Save </button>
                        </div>
                        <div class="col-md-4 padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-info clearFormButton" onclick="resetForm()"><i class="fa fa-times"></i> Clear </button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/sub_department.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
{{-- <script src="js/loadingoverlay.js"></script> --}}
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
