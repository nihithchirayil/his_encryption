@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.doctor_percentages')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Doctor Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="doctor_name" id="doctor_name"
                                    value="{{ $searchFields['doctor_name'] ?? '' }}">
                            </div></div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Service Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="service_desc" id="service_desc"
                                    value="{{ $searchFields['service_desc'] ?? '' }}">
                            </div></div>
                            
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                             <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>  Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; max-height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Doctor Name</th>
                                    <th>Service Name</th>
                                    <th>Item Type</th>
                                    <th>Effective Date</th>
                                    <th>Percentage</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($list) > 0)
                                    @foreach ($list as $data)
                                        <tr style="cursor: pointer;" onclick="editLoadData(this,
                                            '{{$data->dp_id}}',
                                            '{{$data->doctor_id}}',
                                            '{{$data->service_id}}',
                                            '{{$data->item_type}}',
                                            '{{$data->date}}',
                                            '{{$data->percentage}}',
                                            '{{$data->status}}'
                                            )">
                                            <td class="code common_td_rules">{{ $data->doctor_name }}</td>
                                            <td class="category_name common_td_rules">{{ $data->service_desc }}</td>
                                            <td class="item common_td_rules">{{ $data->type_name  }}</td>
                                            <td class="item common_td_rules">{{ $data->date  }}</td>
                                            <td class="item common_td_rules">{{ $data->percentage  }}</td>
                                            <td class="status common_td_rules">{{ $data->status_name  }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="category_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.save_doctor_percentages')}}" method="POST" id="percentageForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="dp_id" id="dp_id" value="0">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Doctor Name</label>
                            <div class="clearfix"></div>
                             {!! Form::select('doctor_id', array("-1"=> " Select") + $doctors->toArray(),$searchFields['item_type_search'] ?? '',
                                ['class'=>"form-control", 'id'=>"doctor_id"]) !!}
                        </div></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Service Name</label>
                            <div class="clearfix"></div>
                             {!! Form::select('service_id', array("-1"=> " Select") + $services->toArray(),$searchFields['item_type_search'] ?? '',
                                ['class'=>"form-control", 'id'=>"service_id"]) !!}
                        </div></div>
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Item Type</label>
                                <div class="clearfix"></div>
                               <select class="form-control" name="item_type" id="item_type">
                                <option value="1">{{__('Department')}}</option>
                                <option value="2">{{__('Sub Department')}}</option>
                                <option value="3">{{__('Item')}}</option>
                            </select>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Effective Date</label>
                                <div class="clearfix"></div>
                               <input type="text" required class="form-control datepicker" name="effective_date" id="effective_date">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Percentage</label>
                                <div class="clearfix"></div>
                               <input type="text" required class="form-control" name="percentage" id="percentage">
                            </div>
                        </div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Status</label>
                            <div class="clearfix"></div>
                            <select class="form-control" name="status" id="status">
                                <option value="1">{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                        </div></div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        
                        <div class="col-md-6 padding_sm">
                           <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function () {
         // $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });
        $('.datepicker').datetimepicker({
                            format: 'DD-MMM-YYYY'
                        });

    });

    function editLoadData(obj,dp_id,doctor_id,service_id,item_type,effective_date,percentage,status){
        var edit_icon_obj = $(obj);
        $('#dp_id').val(dp_id);
        $('#doctor_id').val(doctor_id);
        $('#service_id').val(service_id);
        $('#item_type').val(item_type);
        $('#effective_date').val(effective_date);
        $('#percentage').val(percentage);
        $('#status').val(status);
        $('#percentageForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
         $('#percentageForm')[0].reset();
    }
@include('Purchase::messagetemplate')
</script>

@endsection
