@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
        <h4 class="blue text-left"> <strong>Pricing Master</strong></h4>
            <div class="col-md-8 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Pricing Name</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="pricing_name" value="">
                            <input type="hidden" value="" id="pricing_name_hidden">
                            <div class="ajaxSearchBox" id="pricing_nameAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Company Name</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="company" value="">
                            <input type="hidden" value="" id="company_hidden">
                            <div class="ajaxSearchBox" id="companyAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-4 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Pricing Type</label>
                                <div class="clearfix"></div>
                                <select class="form-control pricing_type" name="pricing_type" id="pricing_type" >
                                    <option value="">---Select---</option>
                                    <option value="1">PRICING</option>
                                    <option value="2">DEPARTMENT</option>
                                    <option value="3">SUB DEPARTMENT</option>
                                    <option value="4">ITEM</option>
                                </select>
                            </div>
                            </div>
                            {{-- <div class="col-md-12 padding_sm"> --}}
                                <div class="col-md-2 padding_sm">
                                    {{-- <div class="mate-input-box"> --}}
                                    <div class="clearfix"></div>
                                    <input type="checkbox" class="effective_date" name="effective_date" id="effective_date" value="1"> 
                                    <label for="">Effective Date</label>
                                    <input type="date" name="date1" id="date1" class="form-control date1" value="<?= date("Y-m-d"); ?>">

                                    <div id='status1'>
                                </div></div>
                                {{-- </div> --}}
                            {{-- </div> --}}
                            {{-- <div class="col-md-12 padding_sm"> --}}
                                <div class="col-md-2 padding_sm">
                                    {{-- <div class="mate-input-box"> --}}
                                    <div class="clearfix"></div>
                                    <input type="checkbox" class=" expiry_date" name="expiry_date" id="expiry_date" value="1"> 
                                    <label for="">Expiry Date</label>
                                    <input type="date" name="date2" id="date2" class="form-control date2" value="<?= date("Y-m-d"); ?>">

                                    <div id='status1'>
                                </div></div>
                                {{-- </div> --}}
                            {{-- </div> --}}
                            <div class="col-md-1 padding_sm">
                                {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class="status" name="status" id="status" value="1"> 
                                <label for="">Active</label>
                                <div id='status1'>
                            {{-- </div></div> --}}
                            </div>
                        </div>
                            <div>
                                <div class="col-md-2 padding_sm">
                                    {{-- <div class="mate-input-box"> --}}
                                    <div class="clearfix"></div>
                                    <input type="checkbox" class=" show_price_discount" name="show_price_discount" id="show_price_discount" value="1"> 
                                    <label for="">Show Discount</label>
                                    <div id='status1'>
                                </div></div>
                                {{-- </div> --}}
                            </div>
                            <div class="col-md-2 padding_sm ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" ><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}">

                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="box no-border" >
                    <div class="box-body clearfix" style=" height : 455px">
                        <h4 class="blue text-center"><span id="add"> Add Pricing  </span></h4>
                        <hr >
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Pricing Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control pricing_name_add" name="pricing_name_add" id="pricing_name_add" value=" {{$pricing->pricing_name ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Company Name</label>
                            <div class="clearfix"></div>
                            {{-- <input type="text" class="form-control branch_add" name="branch_add" id="branch_add" value="  "> --}}
                            <select class="form-control select2" id="company_add" name="company_add">
                                <option  value="">Select Company Name</option>
                                @foreach ($prices as $price)
                                <option value="{{ $price->id }}">{{ $price->company_name }}</option>

                            @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Pricing Type</label>
                            <div class="clearfix"></div>
                            <select class="form-control pricing_type_add" name="pricing_type_add" id="pricing_type_add" >
                                <option value="">---Select---</option>
                                <option value="1">PRICING</option>
                                <option value="2">DEPARTMENT</option>
                                <option value="3">SUB DEPARTMENT</option>
                                <option value="4">ITEM</option>
                            </select>
                        </div>
                        </div>
                        <div class="col-md-6 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Effective Date</label>
                                <div class="clearfix"></div>
                        <input type="date" name="effective_date_add" id="effective_date_add" class="form-control effective_date_add" value="<?= date("Y-m-d"); ?>">
                    </div></div>
                    <div class="col-md-6 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Expiry Date</label>
                        <div class="clearfix"></div>
                <input type="date" name="expiry_date_add" id="expiry_date_add" class="form-control expiry_date_add" value="<?= date("Y-m-d"); ?>">
            </div></div>
            <br>
                        <div class="col-md-4 padding_sm">
                            {{-- <div> --}}
                                {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class=" allow_pay_from_cash_add" name="allow_pay_from_cash_add" id="allow_pay_from_cash_add" value="1"> 
                                <label for="">Allow Pay From Cash</label>

                                <div id='status1'>
                            </div></div>
                            {{-- </div> --}}
                        {{-- </div> --}}
                        <div class="col-md-4 padding_sm">
                            {{-- <div> --}}
                                {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class=" billing_use_only_add" name="billing_use_only_add" id="billing_use_only_add" value="1"> 
                                <label for="">Billing Use Only</label>
                                <div id='status1'>
                            </div>
                        </div>
                            {{-- </div> --}}
                        {{-- </div> --}}
                        {{-- {{-- <div class="col-md-6 padding_sm"> --}}
                            <div class="col-md-4 padding_sm">
                                {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class="status_add" name="status_add" id="status_add" value="1"> 
                                <label for="">Active</label>
                                <div id='status1'>
                            {{-- </div></div> --}}
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            {{-- <div class="col-md-10 padding_sm"> --}}
                                {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class="show_price_discount_add" name="show_price_discount_add" id="show_price_discount_add" value="1"> 
                                <label for="">Show Discount</label>
                                <div id='status1'>
                            {{-- </div></div> --}}
                            </div>
                        </div>
                        <br>
                        <input type="hidden" name="reference_id" class="reference_id" >
                        <div class="col-md-4 padding_sm pull-right"  style="margin-bottom:10px;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-success saveButton" onclick="saveForm()"><i class="fa fa-save"></i> Save </button>
                        </div>
                        <div class="col-md-4 padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-info clearFormButton" onclick="resetForm()"><i class="fa fa-times"></i> Clear </button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/pricing.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
{{-- <script src="js/loadingoverlay.js"></script> --}}
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
