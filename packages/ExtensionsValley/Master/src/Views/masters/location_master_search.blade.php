<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="5%">Location Code</th>
                <th width="15%">Location Name</th>
                <th width="15%">Location Type</th>
                <th width="11%">Floor</th>
                <th width="10%">Phone No</th>
                <th width="10%">Contact Person</th>
                <th width="5%">Status</th>
                <th width="9%">Is Nursing Station</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($location_master) > 0)
                @foreach ($location_master as $location)
                    <tr style="cursor: pointer;"  data-id="{{ $location->id }}" data-location_name="{{ $location->location_name }}" data-code="{{ $location->location_code }}" 
                        data-type="{{ $location->type }}" data-status="{{ $location->status_name }}" data-floor="{{ $location->floor }}" data-contact_no="{{ $location->contact_no }}"
                        data-contact_person="{{ $location->contact_person }}" data-nursing_station="{{ $location->nursing_station }}" data-pharmacy_item="{{ $location->pharmacy_item }}" 
                        data-materials_item="{{ $location->materials_item }}">
                        <td class = "td_common_numeric_rules">{{($location_master->currentPage() - 1) * $location_master->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $location->location_code }}</td>
                        <td class ="common_td_rules">{{ $location->location_name }}</td>
                        <td class ="common_td_rules">{{ $location->type_name }}</td>
                        <td class ="common_td_rules">{{ $location->floor_name}}</td>
                        <td class ="common_td_rules">{{ $location->contact_no}}</td>
                        <td class ="common_td_rules">{{ $location->contact_person}}</td>
                        <td class ="common_td_rules">{{ $location->status}}</td>
                        <td class ="common_td_rules">{{ $location->is_nursing_station}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        @if($location->deleted_at=='')
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button>
                        </td>
                        @else
                        <td>
                            <button type="button" id="restore" class="btn btn-sm btn-primary " onclick="revert(this);" ><i class="fa fa-undo"></i> Revert</button>
                        </td>
                        @endif
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
