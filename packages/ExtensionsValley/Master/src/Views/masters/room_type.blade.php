@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }
    fieldset {
  background: white;
  border: 5px ;
  border-color: black;
}

legend {
  padding: 10px;
  background: white;
  color: black;
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
        <h4 class="blue text-left"> <strong>Room type Master</strong></h4>
            <div class="col-md-8 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Room Type</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="room_type" value="">
                            <input type="hidden" value="" id="room_type_hidden">
                            <div class="ajaxSearchBox" id="room_typeAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Service Name</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="service_desc" value="">
                            <input type="hidden" value="" id="service_desc_hidden">
                            <div class="ajaxSearchBox" id="service_descAjaxDiv"
                            style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999px;
                            position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:99px;">
                                <label for=""><strong>Room Type Based On</strong></label>

                            <div class="col-md-10 padding_sm">
                                <div class="clearfix"></div>
                                <input type="checkbox" class="hourly_rent" name="hourly_rent" id="hourly_rent" value="1"> 
                                <label for="">Hourly Rent</label>
                               
                            </div>
                       
                            <div class="col-md-10 padding_sm">
                                <div class="clearfix"></div>
                                <input type="checkbox" class="daily_rent" name="daily_rent" id="daily_rent" value="2"> 
                                <label for="">Daily Rent</label>
                            </div>
                            </div>
                        </div>
                   </div>
                            <div class="col-md-1 padding_sm">
                                <div class="clearfix"></div>
                                <input type="checkbox" class="icu" name="icu" id="icu" value="1"> 
                                <label for="">Icu</label>
                               
                            </div>
                       
                            <div class="col-md-3 padding_sm">
                                <div class="clearfix"></div>
                                <input type="checkbox" class=" day_care_bed" name="day_care_bed" id="day_care_bed" value="1"> 
                                <label for="">Day Care Bed</label>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="clearfix"></div>
                                <input type="checkbox" class=" baby_bed" name="baby_bed" id="baby_bed" value="1"> 
                                <label for="">BabyBed</label>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="clearfix"></div>
                                <input type="checkbox" class=" required_last_day_rent" name="required_last_day_rent" id="required_last_day_rent" value="1"> 
                                <label for="">Required Last Day Rent</label>
                            </div>
                            <div class="col-md-2 padding_sm ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" style="margin-left:153px;"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}">

                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="box no-border" >
                    <div class="box-body clearfix" style=" height : 455px">
                        <h4 class="blue text-center"><span id="add"> Add Room Type</span></h4>
                        <hr >
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Romm Type Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control room_type_name" name="room_type_name" id="room_type_name" value=" {{$room_type_master->room_type_name ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Service Name</label>
                            <div class="clearfix"></div>
                            {{-- <input type="text" class="form-control branch_add" name="branch_add" id="branch_add" value="  "> --}}
                            <select class="form-control select2" id="service_add" name="service_add">
                                <option  value="">Select Service Name</option>
                                @foreach ($service_name as $service)
                                <option value="{{ $service->service_code }}">{{ $service->service_desc }}</option>

                            @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Order Level</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control order_level" name="order_level" id="order_level" value=" {{$room_type_master->order_level ?? '' }} ">
                            </div>
                        </div>
                       
                       <div class="col-md-4 padding_sm">
                           <div class="box no-border no-margin">
                               <div class="box-footer"
                                      style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:99px;">
                                   <label for=""><strong>Room Type Based On</strong></label>
                                   <div class="col-md-10 padding_sm">
                                       <div class="clearfix"></div>
                                       <input  type="checkbox" class="rent_add" name="hourly" id="hourly_rent_add" value="1"> 
                                       <label for="hourly_rent_add">Hourly Rent</label>
                                      
                                   </div>
                              
                                   <div class="col-md-10 padding_sm">
                                       <div class="clearfix"></div>
                                       <input type="checkbox" class="rent_add" name="hourly" id="daily_rent_add" value="2"> 
                                       <label for="daily_rent_add">Daily Rent</label>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="col-md-3 padding_sm">
                        <div class="clearfix"></div>
                        <input type="checkbox" class="status_add" name="status_add" id="status_add" value="1"> 
                        <label for="">Active</label>
                       </div>
                        <div class="col-md-4 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="icu_add" name="icu_add" id="icu_add" value="1"> 
                            <label for="">Icu</label>
                           
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class=" baby_bed_add" name="baby_bed_add" id="baby_bed_add" value="1"> 
                            <label for="">BabyBed</label>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class=" day_care_bed_add" name="day_care_bed_add" id="day_care_bed_add" value="1"> 
                            <label for="">Day Care Bed</label>
                        </div>
                       
                        <div class="col-md-6 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class=" required_last_day_rent_add" name="required_last_day_rent_add" id="required_last_day_rent_add" value="1"> 
                            <label for="">Required Last Day Rent</label>
                        </div>
                        <input type="hidden" name="reference_id" class="reference_id" >
                        <div class="col-md-4 padding_sm pull-right"  style="margin-bottom:10px;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-success saveButton" onclick="saveForm()"><i class="fa fa-save"></i> Save </button>
                        </div>
                        <div class="col-md-4 padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-info clearFormButton" onclick="resetForm()"><i class="fa fa-times"></i> Clear </button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/room_type.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
{{-- <script src="js/loadingoverlay.js"></script> --}}
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
