<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if(url && url!='undefined') {
            var ledger_id = $('#ledger_name_hidden').val();
            var dept_id = $('#dept_hidden').val();
            var speciality_id = $('#specility_hidden').val();

            $.ajax({
                type: "POST",
                url: url,
                data: { _token: token, ledger_id: ledger_id,dept_id: dept_id,speciality_id: speciality_id },
                beforeSend: function () {
                    $('#ledgerseracch').attr('disabled', true);
                    $('#ledgersearchspin').removeClass('fa fa-search');
                    $('#ledgersearchspin').addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    $('#searchDataDiv').html(data);
                },
                complete: function () {
                    $('#ledgerseracch').attr('disabled', false);
                    $('#ledgersearchspin').removeClass('fa fa-spinner fa-spin');
                    $('#ledgersearchspin').addClass('fa fa-search');
                },
                error: function () {
                    toastr.error('Please check your internet connection and try again');
                }
            });
        }
            return false;
        });

    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th class="common_td_rules" width="25%">Ledger Name.</th>
                    <th class="common_td_rules" width="25%">Group Name</th>
                    <th class="common_td_rules" width="25%">Department</th>
                    <th class="common_td_rules" width="25%">Speciality</th>
                </tr>
            </thead>
            <tbody>
                <?php
if (count($ledger_list) != 0) {
    foreach ($ledger_list as $list) {
        ?>
                <tr style="cursor: pointer;" onclick="getmapingid('{{base64_encode($list->map_id)}}')">
                <td class="common_td_rules">{{$list->ledger_name}}</td>
                <td class="common_td_rules">{{$list->group_name}}</td>
                <td class="common_td_rules">{{$list->department}}</td>
                <td class="common_td_rules">{{$list->speciality}}</td>
                </tr>

                <?php
}
} else {
    ?>
                <tr>
                    <td colspan="4" class="re-records-found">No Records Found</td>
                </tr>
                <?php
}
?>

            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>