@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')

<div class="right_col" >
<input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
    <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;padding-right: 80px"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix" style="border: 2px solid #CED9FF !important;">
                        <form action="{{route('extensionsvalley.master.list_ledger_master')}}" id="listingSearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Account Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="ledger_name" id="ledger_name" onkeyup="select_ledger_names(this.id, event,1)"
                                       value="{{ $searchFields['ledger_name'] ?? '' }}" autocomplete="off">
                                <input type="hidden" class="form-control" name="hidden_ledger_name_id" id="hidden_ledger_name_id"
                                           value="{{ $searchFields['hidden_ledger_name_id'] ?? '' }}">
                                    <div class="ajaxSearchBox" id="search_ledger_name"
                                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                             margin: 15px -4px 0px;overflow-y: auto; width: 500px !important; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                </div>
                            </div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Group Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="group_names" id="group_name"
                                           value="{{ $searchFields['group_names'] ?? '' }}" autocomplete="off" onkeyup="select_group_names(this.id, event,1)" >
                                    <input type="hidden" class="form-control" name="hidden_group_names_id" id="hidden_group_names_id"
                                           value="{{ $searchFields['hidden_group_names_id'] ?? '' }}">
                                    <div class="ajaxSearchBox" id="search_group"
                                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                              margin: 15px -4px 0px;overflow-y: auto; width: 500px !important;  z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                </div>

                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <div class="clearfix"></div>
                                    <?php
                                if(isset($searchFields['is_bank_search']) && $searchFields['is_bank_search'] ==1 ){
                                    $checked_chk = 'checked';
                                }else{
                                    $checked_chk = '';
                                }
                                ?>
                                    <input type="checkbox" class="" name="is_bank_search" id="is_bank_search" value="1" <?= $checked_chk ?>>
                                    &nbsp; &nbsp;<span style="font-size: 12px;font-weight: 700;">Is Bank</span>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm" style="width: 10%;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <span class="btn btn-block light_purple_bg" id="searchMasterBtn" onclick="searchLedgerMaster();"><i class="fa fa-search" id="searchMasterBtnSpin"></i>
                                    Search</span>
                            </div>
                              <div class="col-md-1 padding_sm"  style="width: 10%;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                        <div class="clearfix"></div>
                        <div class="box no-border no-margin">
                            <div class="box-footer" id="searchLedgerMasterDataDiv"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 525px;">

                            </div>
                        </div>
                    </div>

        </div>
        <div class="col-md-4 padding_sm" id="ledger_master_form_whole">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.save_ledger_master')}}" method="POST" id="ledger_master_form">
                        {!! Form::token() !!}
                        <input type="hidden" name="ledger_master_id" id="ledger_master_id" value="">
                        <input type="hidden" name="from_booking_entry" id="from_booking_entry" value="0">
                        <input type="hidden" name="is_asset_etry" id="is_asset_etry" value="0">
                        <input type="hidden" name="row_ids" id="row_ids" value="">
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Account Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="ledger_name" id="ledger_name_update">
                                 <span style="color: #d14;"> {{ $errors->first('ledger_name') }}</span>
                            </div>
                            </div>
                            <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                           <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Account No</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="ledger_code" id="ledger_code_update">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Group</label>
                            <div class="clearfix"></div>
                            <input type="hidden" value="" name="group_asset_id"  autocomplete="off" class="form-control " id="group_asset_id">
                            <input type="text" value=""  name="group_asset_name"  autocomplete="off" class="form-control group_asset_name" onkeyup="select_group_asset(this.id, event)" id="group_asset_name">
                            </div>
                            <div class="ajaxSearchBox group_asset_name_box" id="group_asset_name_box-1"
                                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                             margin: 14px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Is Bank</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="is_bank" id="is_bank" value="1" onclick="showbank_details()">
                            </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Default payment Bank</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="payment_bank_default" id="payment_bank_default" value="1">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Default receipt Bank</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="receipt_bank_default" id="receipt_bank_default" value="1">
                            </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Opening Balance</label>
                                <div class="clearfix"></div>
                                <input type="text" name="opening_balance" id="opening_balance" value="0" class="form-control">
                                </div>
                            </div>
                        </div>
                            <div class="clearfix"></div>
                        <div class="ht10"></div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Address</label>
                                <div class="clearfix"></div>
                                <textarea name="ledger_address" id="ledger_address" class="form-control "></textarea>
                                </div>
                            </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">GST No</label>
                                <div class="clearfix"></div>
                                <input type="text" name="gst_no" id="gst_no" value="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Pin No</label>
                                <div class="clearfix"></div>
                                <input type="text" name="pin_no" id="pin_no" value="" class="form-control">
                            </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                         <div class="col-md-12 padding_sm">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Ledger Type</label>
                                <div class="clearfix"></div>
                             <?php $ledger_types = array(0=> " select") + $ledger_type->toArray() ?>
                                {!! Form::select('ledger_types', $ledger_types,'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'ledger_types','onchange'=>'showCreditData(this)']) !!}
                            </div>
                            </div>
                         </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm credit_div" style=" display: none">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Max.Credit amount</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="credit_amount" id="credit_amount" onkeyup="number_validation(this)">
                            </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Creidit Days</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="credit_days" id="credit_days" onkeyup="number_validation(this)">
                            </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm credit_div" style=" display: none">
                            <div class="col-md-6 padding_sm">
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="" value="2" name="pl_type" >
                                <label for="type"> Income </label>
                            </div>
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="" value="1" name="pl_type">
                                <label for="type"> Expense </label>
                            </div>
                        </div>

                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Allow multiple entry</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="allow_multiple_entry" id="allow_multiple_entry" value="1">
                            </div>
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" id="led_cancel_btn">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <span class="btn btn-block light_purple_bg" onclick="saveLedgerMaster()" id="saveLedgerMasterBtn"><i class="fa fa-save" id="add_new_ledger_spin"></i> Save</span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="modal fade" id="getAuditLog" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 80%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <span class="modal-title" id="audit_log_header"></span>
            </div>
            <div class="modal-body" style="min-height:400px;height:500px;overflow: auto" id="audit_log_body">

            </div>
        </div>
    </div>
</div>
<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
@stop

@section('javascript_extra')
 <script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
 <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
 <script src="{{asset("packages/extensionsvalley/accounts/default/javascript/ledger_master.js")}}"></script>
 <script src="{{asset("packages/extensionsvalley/accounts/default/javascript/audit_log.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function ($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
    position: 'absolute',
            scrollContainer: true
    });
    searchLedgerMaster();
    });
    function ledgerEditLoadData(id, name, ledger_group_id, is_bank, payment_bank_default, receipt_bank_default,
    ledger_code,ledger_group_name,ledger_type,max_credit_amount,credit_days,is_asset,pl_type,ledger_address,gst_no,pin_no,opening_balance,allow_multiple_entry){
    $('#ledger_master_id').val(id);
    $('#ledger_name_update').val(name);
    $('#ledger_code_update').val(ledger_code);
    $('#group_asset_id').val(ledger_group_id);
    $('#group_asset_name').val(ledger_group_name);
    $('#payment_bank_default').val(payment_bank_default);
    $('#receipt_bank_default').val(receipt_bank_default);
    $('#ledger_types').val(ledger_type);
    $('#credit_amount').val(max_credit_amount);
    $('#credit_days').val(credit_days);
    $('#ledger_address').val(ledger_address);
    $('#gst_no').val(gst_no);
    $('#pin_no').val(pin_no);
    $("#opening_balance").val(opening_balance);
    $("#is_asset_etry").val(is_asset);
        if (is_bank == 1){
    $("#is_bank").prop("checked", true);
    $("#bank_details").show();
    } else{
    $("#is_bank").prop("checked", false);
    }

    if (allow_multiple_entry == 1){
    $("#allow_multiple_entry").prop("checked", true);
    $("#allow_multiple_entry").show();
    } else{
    $("#allow_multiple_entry").prop("checked", false);
    }

    if (payment_bank_default == 1){
    $("#payment_bank_default").prop("checked", true);
    } else{
    $("#payment_bank_default").prop("checked", false);
    }
    if (receipt_bank_default == 1){
    $("#receipt_bank_default").prop("checked", true);
    } else{
    $("#receipt_bank_default").prop("checked", false);
    }
    if(ledger_type >0){
        $(".credit_div").show();
    }
    if(pl_type == 1 || pl_type ==2 ){
    $("input[name=pl_type][value=" + pl_type + "]").attr('checked', 'checked');
    }else{
    $("input[type=radio][name=pl_type]").prop('checked', false);
    }
    $('#ledger_master_form').attr('action', '{{$updateUrl}}');
    }
function showbank_details(){
    var lfckv = document.getElementById("is_bank").checked;
  if(lfckv==false){
  }
    $("#bank_details").show();
}
 function select_group_names(id, event,typ) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
        if(typ == 1){
        var ajax_div = 'search_group';
        var ledger_desc = $("#group_name").val();
        }else{
            var ajax_div = 'search_group_update';
            var ledger_desc = $("#ledger_group_parent_name").val();
        }
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {

        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: "{!! route('extensionsvalley.master.list_ledger_group') !!}",
                data: 'group_name=' + ledger_desc + '&search_group_name=1&types=3',
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillAssetGroupValuesMaster(e, ledger_name, id,types,is_asset) {
$("#group_name").val(ledger_name.replace(/&amp;/g, '&'));
$("#hidden_group_names_id").val(id);
}
 function select_ledger_names(id, event,typ) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
        if(typ == 1){
        var ajax_div = 'search_ledger_name';
        var ledger_desc = $("#ledger_name").val();
        }
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {

        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: "{!! route('extensionsvalley.accounts.searchLedgerMaster') !!}",
                data: 'ledger_desc=' + ledger_desc+'&from_ledger_master=1',
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillLedgerValues(e, ledger_name, id,ledger_group_id,ledger_code) {
$("#ledger_name").val(ledger_name);
$("#hidden_ledger_name_id").val(id);
}
function ledgerDelete(e,id){
    if (confirm("Are you sure you want to Delete ?")) {
    $.ajax({
                type: "GET",
                url: "{!! route('extensionsvalley.master.list_ledger_master') !!}",
                data: 'ledger_delete=1&ledger_id='+id,
                beforeSend: function () {
                    $('#delete_spin_'+id).removeClass('fa fa-trash');
                    $('#delete_spin_'+id).addClass('fa fa-spinner fa-spin');
                },
                success: function (data) {
                    $('#delete_spin_'+id).removeClass('fa fa-spinner fa-spin');
                    $('#delete_spin_'+id).addClass('fa fa-trash');
                    if(data == 1){
                     $(e).closest('tr').remove();
                      toastr.success('Deleted Successfully...');
                    }else{
                         toastr.warning('This data may exist in transaction.. delete failed.!');
                    }
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }
}
@include('Purchase::messagetemplate')
</script>

@endsection
