<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <style>
            table tr:hover td {
                text-overflow: initial;
                white-space: normal;
            }
        
        </style>
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">Sl.No.</th>
                <th width="25%">Sub Category Name</th>
                <th width="20%">Sub Category Code</th>
                <th width="30%">Category Name</th>
                <th width="10%">Status</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($gen_sub_cat) > 0)
                @foreach ($gen_sub_cat as $gen_sub)
                    <tr style="cursor: pointer;"  data-id="{{ $gen_sub->id }}" data-sub_cat_name="{{ $gen_sub->subcategory_name }}" 
                        data-cat_code="{{ $gen_sub->cat}}" data-sub_cat_code="{{ $gen_sub->subcategory_code }}"
                        data-status="{{ $gen_sub->status_name }}" >
                        <td class = "td_common_numeric_rules">{{($gen_sub_cat->currentPage() - 1) * $gen_sub_cat->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $gen_sub->subcategory_name }}</td>
                        <td class ="common_td_rules">{{ $gen_sub->subcategory_code }}</td>
                        <td class ="common_td_rules">{{ $gen_sub->category_name }}</td>
                        <td class ="common_td_rules">{{ $gen_sub->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
