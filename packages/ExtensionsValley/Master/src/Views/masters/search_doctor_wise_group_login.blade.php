<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th>Doctor</th>
                <th>Parent Doctor</th>
                <th style = "width:20%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($doctor_wise_group_login) > 0)
                @foreach ($doctor_wise_group_login as $doctor_login)
                    <tr style="cursor: pointer;"  data-id="{{ $doctor_login->id }}" data-doctor="{{ $doctor_login->doctor_id}}" data-parent_doc="{{ $doctor_login->parent_doctor_id}}">
                        <td class = "td_common_numeric_rules">{{($doctor_wise_group_login->currentPage() - 1) * $doctor_wise_group_login->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $doctor_login->doctor}}</td>
                        <td class ="common_td_rules">{{ $doctor_login->parent_doctor}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>