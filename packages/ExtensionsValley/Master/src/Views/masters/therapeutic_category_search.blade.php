<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <style>
            table tr:hover td {
                text-overflow: initial;
                white-space: normal;
            }
        
        </style>
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">Sl.No.</th>
                <th width="30%">Category Name</th>
                <th width="30%">Category Code</th>
                <th width="30%">Is Surgical</th>
                <th width="10%">Status</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($gen_therapic_cat) > 0)
                @foreach ($gen_therapic_cat as $therapic_cat)
                    <tr style="cursor: pointer;"  data-id="{{ $therapic_cat->id }}" data-cat_name="{{ $therapic_cat->category_name }}" 
                        data-cat_code="{{ $therapic_cat->category_code }}" data-surgical="{{ $therapic_cat->surgical }}"
                        data-status="{{ $therapic_cat->status_name }}" >
                        <td class = "td_common_numeric_rules">{{($gen_therapic_cat->currentPage() - 1) * $gen_therapic_cat->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $therapic_cat->category_name }}</td>
                        <td class ="common_td_rules">{{ $therapic_cat->category_code }}</td>
                        <td class ="common_td_rules">{{ $therapic_cat->is_surgical }}</td>
                        <td class ="common_td_rules">{{ $therapic_cat->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
