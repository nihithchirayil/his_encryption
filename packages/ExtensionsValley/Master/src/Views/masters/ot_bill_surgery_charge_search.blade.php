<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="15%">Surgery Charge</th>
                <th width="7%">CSSD Charge</th>
                <th width="11%">ASST Surgeon Charge</th>
                <th width="7%">Status</th>
                <th width="13%">Local Anesthesia Charge</th>
                <th width="9%">Surgery Type</th>
                <th style = "width:20%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($ot_bills) > 0)
                @foreach ($ot_bills as $ot_bill)
                    <tr style="cursor: pointer;"  data-id="{{ $ot_bill->id }}" data-surgery_charge="{{ $ot_bill->surgery_charge }}" data-cssd_charge="{{ $ot_bill->cssd_charge }}" 
                        data-asst_surgeon_charge="{{ $ot_bill->asst_surgeon_charge }}" data-status="{{ $ot_bill->status_name }}"  data-local_anesthesia_charge="{{ $ot_bill->local_anesthesia_charge }}"
                        data-surgery_type="{{ $ot_bill->surgery }}">
                        <td class = "td_common_numeric_rules">{{($ot_bills->currentPage() - 1) * $ot_bills->perPage() + $loop->iteration}}</td>
                        <td class ="td_common_numeric_rules">{{ $ot_bill->surgery_charge }}</td>
                        <td class ="td_common_numeric_rules">{{ $ot_bill->cssd_charge}}</td>
                        <td class ="td_common_numeric_rules">{{ $ot_bill->asst_surgeon_charge}}</td>
                        <td class ="common_td_rules">{{ $ot_bill->status}}</td>
                        <td class ="td_common_numeric_rules">{{ $ot_bill->local_anesthesia_charge}}</td>
                        <td class ="common_td_rules">{{ $ot_bill->surgery_type}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
