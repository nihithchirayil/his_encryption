@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')

<div class="right_col" >
    <div class="row" style="text-align: right;padding-right: 80px;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <input type="hidden" name="base_url" id="base_url" value="{{URL::To('/')}}">
    <input type="hidden" id="page_url" value=0>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.list_generic_name')}}" id="listingSearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Generic Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="generic_name_search" id="generic_name_search"
                                           value="{{ $searchFields['generic_name_search'] ?? '' }}">
                                </div>

                            </div>                            
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg" onclick="searchBill()"><i id="searchdataspin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" id="generilistdiv">
                
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix" id="generic_form_whole">
                    <form action="{{route('extensionsvalley.master.save_generic_name')}}" method="POST" id="generic_form">
                        {!! Form::token() !!}
                        <input type="hidden" name="generic_id" id="generic_id" value="">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Generic Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="generic_name" id="generic_name_update">
                                <span style="color: #d14;"> {{ $errors->first('generic_name') }}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>                        
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" style='color:#555555;' name="status" id="status_update">
                                    <option value="1">{{__('Active')}}</option>
                                    <option value="0">{{__('Inactive')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>    
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Therapeutic Category</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" style='color:#555555;' name="therapeutic_category" id="therapeutic_category">
                                <option value="">Select Therapeutic Category</option>
                                   {{-- <//?php foreach($therapeutic_category as $key=>$val){
                                       ?>
                                        <option value="<//?=$key?>"><//?=$val?></option>
                                       <//?php
                                   }
                                   ?> --}}
                                   @foreach ($therapeutic_category as $val)
                                   <option value="{{ $val->category_code }}">{{ $val->category_name }}</option>
   
                               @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm" id="subcategory_select">
                            <div class="mate-input-box">
                                <label for="">Therapeutic Subcategory</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" style='color:#555555;' name="therapeutic_subcategory" id="therapeutic_subcategory">
                                <option value="">Select Therapeutic Subcategory</option>
                                   {{-- <//?php foreach($generic_subcategory as $key=>$val){
                                       ?>
                                        <option value="<//?=$key?>"><//?=$val?></option>
                                       <//?php
                                   }
                                   ?> --}}
                                   @foreach ($generic_subcategory as $val)
                                   <option value="{{ $val->subcategory_code }}">{{ $val->subcategory_name }}</option>
   
                               @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm" id="generic_cancel">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <span class="btn btn-block light_purple_bg" onclick="saveGenericName()"><i class="fa fa-save"></i> Save</span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function ($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
    position: 'absolute',
            scrollContainer: true
    });
    searchBill();
    });
    function genericEditLoadData(id,name,status,category_code,subcategory_code){
        $('#generic_id').val(id);
        $('#generic_name_update').val(name);
        $('#status_update').val(status).select2();
        $('#therapeutic_category').val(category_code).select2();
        $('#therapeutic_subcategory').val(subcategory_code).select2();
        setTimeout(relativeSubCategiry(subcategory_code),1000);

        // relativeSubCategiry();
        $('#generic_form').attr('action', '{{$updateUrl}}');
    }
    
    function searchBill(){
        var url='<?=route('extensionsvalley.master.getGenericName')?>';
        var generic_name = $('#generic_name_search').val();
        $.ajax({
        type: "GET",
            url: url,
            data: "generic_name="+generic_name,
            beforeSend: function () {
                $('#searchdatabtn').attr('disabled',true);
                $('#searchdataspin').removeClass('fa fa-search');
                $('#searchdataspin').addClass('fa fa-spinner');
                $('#generilistdiv').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (msg) { 
                $('#generilistdiv').html(msg);
            },
            complete: function () {
                $('#searchdatabtn').attr('disabled',false);
                $('#searchdataspin').removeClass('fa fa-spinner');
                $('#searchdataspin').addClass('fa fa-search');
                $('#generilistdiv').LoadingOverlay("hide");
            },error: function(){
                toastr.error("Please Check Internet Connection");
            }
        });
   }
    
function saveGenericName(){
   
   var url = $('#generic_form').attr('action');
   if($("#generic_name_update").val()==''){
    toastr.warning("Please enter generic name");
       return false;
   }
  $.ajax({
        type: "GET",
        url: url ,
        data: $("#generic_form").serialize(),
        beforeSend: function () {
            $('#generic_form_whole').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        },
        success: function (data) {
           if(data){
               toastr.success("Successfully Updated");
               //searchBill();
               //document.getElementById('generic_form').reset();
               var generic_id= $('#generic_id').val();
               var gen=$('#generic_name_update').val();  
               if(generic_id!=''){
               var gen=$('#generic_name_update').val();
               var sta=$('#status_update').select2('data')[0].text;
               var sta_code=$('#status_update').val();
               var cat=$('#therapeutic_category').select2('data')[0].text;  
               var cat_code=$('#therapeutic_category').val(); 
               var sub=$('#therapeutic_subcategory').select2('data')[0].text;  
               var sub_code=$('#therapeutic_subcategory').val();   
            //    $('.generic'+generic_id).html('');
            //    $('.generic'+generic_id).html(' <td  class="category_name common_td_rules" >'+gen+'</td><td  class="status common_td_rules">'+cat+'</td><td  class="status common_td_rules">'+sub+'</td><td  class="status common_td_rules">'+sta+'</td><td  class="text-center"><button class="btn btn-block light_purple_bg edit'+generic_id+'" type="button"><i class="fa fa-edit"></i></button></td>');
               $('.edit' +generic_id).attr('onClick', 'genericEditLoadData('+generic_id+',"'+gen+'",'+sta_code+','+cat_code+','+sub_code+')');

               $('#gen'+generic_id).html(gen);  
               $('#cat'+generic_id).html(cat);  
               $('#sub'+generic_id).html(sub);  
               $('#sta'+generic_id).html(sta);  
               }else{
                   $('#generic_name_search').val(gen);
                   searchBill();

               }
              
               $('#generic_name_update').val('');  
               $('#therapeutic_category').val('');  
                $('#therapeutic_category').select2();
                $('#therapeutic_subcategory').val('');
                $('#status_update').val('');
                $('#generic_id').val('');
                $('#therapeutic_subcategory').select2();
                save_url='{{route('extensionsvalley.master.save_generic_name')}}';
                $('#generic_form').attr('action', save_url);

           }
        },
        complete: function () {
            $('#generic_form_whole').LoadingOverlay("hide");
        }
    });
}

$('#therapeutic_category').on('change',function(){
    relativeSubCategiry();
});
function relativeSubCategiry(subcategory_code=0){
    var therapeutic_category = $('#therapeutic_category').val();
    if (therapeutic_category == '') {
      var therapeutic_category = 'a';
    }

 if(therapeutic_category) {
    var base_url=$('#base_url').val();
    var url = base_url + "/master/relativetherapeuticsubcategory";
   // var url= route_json.save_sub_department;

        $.ajax({
                    type: "GET",
                    url: url,
                    data: 'therapeutic_category=' + therapeutic_category,
                    beforeSend: function () {
                  $("#subcategory_select").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                  $( "#therapeutic_subcategory" ).prop( "disabled", true ); //Disable
                    },
                    success: function (html) {

                                if(html){
                    //      var obj=JSON.parse(html);
                    //    console.log(obj);

                                    //    $("#therapeutic_subcategory").empty();

                                        $("#therapeutic_subcategory").html('<option value="">Select Sub Therapeutic Category</option>');
                                        $.each(html,function(key,value){
                                          $("#therapeutic_subcategory").append('<option value="'+key+'">'+value+'</option>');
                                        });

                                }else{
                                        $("#therapeutic_subcategory").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["error"]("Please check your internet connection and try again!");
                     // alert('Please check your internet connection and try again');
                     $( "#therapeutic_subcategory" ).prop( "disabled", false );

                    },
                    complete: function () {
                        if(subcategory_code!=0){
                            $("#therapeutic_subcategory").val(subcategory_code).select2();
                        }
            $("#subcategory_select").LoadingOverlay("hide");
$( "#therapeutic_subcategory" ).prop( "disabled", false );
                    }

                });

        }
    else {
                $("#therapeutic_category").focus();
                // $("#therapeutic_subcategory").empty();
            }

    
}

//     var params = { ward: generic};
//     var base_url=$('#base_url').val();
//     var url = base_url + "/master/relativetherapeuticsubcategory";
//     $.ajax({
//         type: "GET",
//         url: url,
//         beforeSend: function () {
//             $("#subcategory_select").LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
//             $("#therapeutic_subcategory").empty();
//         },
//         success: function (data) {
//             if (data) {
//                 $("#therapeutic_subcategory").html('<option value="">Select Therapeutic Subcategory</option>');
//                 $.each(data, function (key,value) {
//                     ($("#therapeutic_subcategory").append('<option value="' + value.subcategory_code + '">' + value.subcategory_name + '</option>'));
//                 })
//                 if(subcategory_code!=0){
//                     $('#therapeutic_subcategory').val(subcategory_code).select2();

//                 }

//             }

//         },
//         complete:function (){
//             $("#subcategory_select").LoadingOverlay("hide");
//         },


//     })
// }

 
</script>

@endsection
