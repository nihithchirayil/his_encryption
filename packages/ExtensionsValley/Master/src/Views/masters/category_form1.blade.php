@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('custom_css')
<link href="{{asset("packages/extensionsvalley/stockmanagement/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/stockmanagement/plugins/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/stockmanagement/plugins/custom-input-icons/custom-input-icons.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/stockmanagement/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/stockmanagement/plugins/perfect-scrollbar/css/perfect-scrollbar.css")}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css">
@endsection
@section('content-area')

 <!-- page content -->
        <div class="right_col"  role="main">
        <div class="row codfox_container">

       <div class="col-md-12 padding_sm">
           <div class="col-md-12 no-padding">
               <div class="box no-border">
               <div class="box-body clearfix">
                   
                   <div class="col-md-2 padding_sm">
                       <label for="">Requisition Reference No</label>
                       <div class="clearfix"></div>
                       <input type="text" class="form-control">
                   </div>
                 
                   <div class="col-md-2 padding_sm">
                       <label for="">From Date</label>
                       <div class="clearfix"></div>
                       <input type="text" class="form-control datepicker">
                   </div>

                   <div class="col-md-2 padding_sm">
                       <label for="">To Date</label>
                       <div class="clearfix"></div>
                       <input type="text" class="form-control datepicker">
                   </div>

                   <div class="col-md-2 padding_sm">
                       <label for="">Department</label>
                       <div class="clearfix"></div>
                       <select class="form-control">
                        <option value="">Department</option>
                        <option value="1">1</option>
                       </select>
                   </div>

                   <div class="col-md-2 padding_sm">
                       <label for="">Status</label>
                       <div class="clearfix"></div>
                       <select class="form-control">
                        <option value="">Status</option>
                        <option value="1">1</option>
                       </select>
                   </div>

                   <div class="col-md-1 padding_sm">
                       <label for="">&nbsp;</label>
                       <div class="clearfix"></div>
                       <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i> Search</button>
                   </div>

                   <div class="col-md-1 padding_sm">
                       <label for="">&nbsp;</label>
                       <div class="clearfix"></div>
                       <a href="{{url('admin/add-requisition')}}" class="btn btn-block light_purple_bg"><i class="fa fa-plus"></i> Add</a>
                   </div>
               </div>
           </div>
           </div>
           
           <div class="clearfix"></div>
               <div class="box no-border no-margin" style="height: 450px;">
                   <div class="box-body clearfix">
                       <div class="theadscroll" style="position: relative; height: 350px;">
                           <table class="table no-margin theadfix_wrapper table-striped table-col-bordered table_sm table-condensed" style="border: 1px solid #CCC;">
                               <thead>
                               <tr class="table_header_bg">
                                   <th>Request No</th>
                                   <th>Department</th>
                                   <th>Store</th>
                                   <th>Requested Date</th>
<th>Approved Date</th>
<th>Approved By</th>
<th>Issued By</th>
<th>Rejected By</th>
<th>Received By</th>
<th>Received Date</th>
<th>Status</th>
<th>Issued Status</th>
                                   <th class="text-center">Edit</th>
                               </tr>
                           </thead>
                           <tbody>
                            @for($i=0;$i<20;$i++)
                               <tr>
                                   <td>C</td>
                                   <td>C</td>
                                   <td>C</td>
                                   <td>C</td>
                                   <td>C</td>
                                   <td>C</td>
                                   <td>C</td>
                                   <td>C</td>
                                   <td>C</td>
                                   <td>C</td>
                                   <td>C</td>
                                   <td>C</td>
                                   <td class="text-center"><i class="fa fa-edit"></i></td>
                               </tr>
                               @endfor
                           </tbody>
                       </table>
                    </div>
                   <div class="clearfix"></div>
                   <div class="col-md-12 text-center">
                       <ul class="pagination purple_pagination">
                           <li class="disabled"><span>«</span></li>
                           <li class="active"><span>1</span></li>
                           <li><a href="?page=2">2</a></li>
                           <li><a href="?page=3">3</a></li>
                           <li><a href="?page=4">4</a></li>
                           <li><a href="?page=5">5</a></li>
                           <li><a href="?page=6">6</a></li>
                           <li><a href="?page=7">7</a></li>
                           <li><a href="?page=8">8</a></li>
                           <li class="disabled"><span>...</span></li>
                           <li><a href="?page=91">91</a></li>
                           <li><a href="?page=92">92</a></li>
                           <li><a href="?page=2" rel="next">»</a></li>
                       </ul>
                   </div>
               </div>
           </div>
       </div>
       
        </div>
    </div>

    <!-- /page content -->
@stop
@section('custom_js')
{{-- <script src="{{asset("packages/extensionsvalley/stockmanagement/js/jquery-1.11.3.js")}}"></script> --}}
<script src="{{asset("packages/extensionsvalley/stockmanagement/plugins/momentjs/moment.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/stockmanagement/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/stockmanagement/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/stockmanagement/plugins/floathead/jquery.floatThead.js")}}"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
     <script type="text/javascript">
       $(document).ready(function() {

        //    $(document).on('click', '.grn_drop_btn', function(e) {
        //     e.stopPropagation();
        //     $(".grn_btn_dropdown").hide();
        //     $(this).next().slideDown('');
        // });
        //    $(document).on('click', '.btn_group_box', function(e) {
        //     e.stopPropagation();
        //  });

// $(document).on('click', function() {
//            $(".grn_btn_dropdown").hide();
// });

//         $(".select_button li").click(function() {
//             $(this).toggleClass('active');
//         });

       
        // $(document).on('click', '.notes_sec_list ul li', function() {
        //     var disset = $(this).attr("id");
        //     $('.notes_sec_list ul li').removeClass("active");
        //     $(this).addClass("active");
        //     $(this).closest('.notes_box').find(".note_content").css("display", "none");
        //     $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
        // });


        var $table = $('table.theadfix_wrapper');

        // $table.floatThead({
        //     scrollContainer: function($table) {
        //         return $table.closest('.theadscroll');
        //     }

        // });

        // $('.theadscroll').perfectScrollbar({
        //     wheelPropagation: true,
        //     minScrollbarLength: 30
        // });




        // $('#existing_patient_Modal').on('shown.bs.modal', function(e) {
        //     var $table = $('table.theadfix_wrapper');
        //     $table.floatThead({
        //         scrollContainer: function($table) {
        //             return $table.closest('.theadscroll');
        //         }
        //     });
        // })



    });
    </script>
@endsection