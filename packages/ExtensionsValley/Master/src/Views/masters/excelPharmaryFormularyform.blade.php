<div class="row">
    <div class="col-md-12" id="result_container_print">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <?php
            $collect = collect($list);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Item Formulary</b></h4>
            <table id="result_data_table"
                class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                style="border: 1px solid #CCC;table-layout:fixed !important;width:100%;">

                <thead>
                    <tr  class="headerclass" 
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif;font-size:12px">
                        <th  width="5%">SL.No.</th>
                        <th width="15%">Brand Name</th>
                        <th width="10%">Dosage Form</th>
                        <th width="15%">Generic Name</th>
                        <th width="15%">Thearapeutic Category</th>
                        <th width="15%">Thearapeutic Subcategory</th>
                        <th width="15%">Manufacturer</th>

                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=1;
                    @endphp
                    @if (count($list) > 0)
                        @foreach ($list as $data)
                            <tr  >
                                <td  class="td_common_numeric_rules">{{ $i }}</td>
                                <td class="common_td_rules">{{ $data->brand_name ? $data->brand_name : '-' }}</td>
                                <td class="common_td_rules">{{ $data->dose ? $data->dose : '-' }}</td>
                                <td class="common_td_rules">{{ $data->generic_name ? $data->generic_name : '-' }}</td>
                                <td class="common_td_rules">{{ $data->therapeutic_category ? $data->therapeutic_category : '-' }}</td>
                                <td class="common_td_rules">{{ $data->therapeutic_subcategory ? $data->therapeutic_subcategory : '-' }}</td>
                                <td class="common_td_rules">{{ $data->manufacturer_name ? $data->manufacturer_name : '-' }}</td>
                            </tr>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7" class="category_code">No Records found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
