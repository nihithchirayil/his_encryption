@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.listCategory')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Category Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="category_code" id="category_code_search"
                                    value="{{ $searchFields['category_code'] ?? '' }}">
                            </div></div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Category Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="name" id="category_name_search"
                                    value="{{ $searchFields['name'] ?? '' }}">
                            </div></div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Item Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('item_type_search', array(""=> " Select") + $item_type->toArray(),$searchFields['item_type_search'] ?? '',
                                ['class'=>"form-control", 'id'=>"item_type_search"]) !!}
                            </div></div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                             <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>  Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; max-height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Category Name</th>
                                    <th>Category Code</th>
                                    <th>Item Type</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($category_list) > 0)
                                    @foreach ($category_list as $category)
                                        <tr style="cursor: pointer;" onclick="categoryEditLoadData(this,'{{$category->category_id}}','{{$category->category_code}}',
                                                '{{$category->name}}','{{$category->status}}','{{$category->item_type_id}}')">
                                            <td class="category_name common_td_rules">{{ $category->name }}</td>
                                            <td class="category_code common_td_rules">{{ $category->category_code }}</td>
                                            <td class="item common_td_rules">{{ $category->item_type_name  }}</td>
                                            <td class="status common_td_rules">{{ $category->status_name  }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="category_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.saveCategory')}}" method="POST" id="categoryForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="category_id" id="category_id" value="0">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Category Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="category_code" id="category_code">
                            <div class="" id="category_code_status">
                            </div>
                        </div></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Category Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="name" id="category_name">
                        </div></div>
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Item Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('item_type_id', array(""=> " Select") + $item_type->toArray(), '',
                                ['class'=>"form-control", 'id'=>"item_type_id"]) !!}
                            </div></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Status</label>
                            <div class="clearfix"></div>
                            <select class="form-control" name="status" id="status">
                                <option value="1">{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                        </div></div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="addcategory_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Category</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="theadscroll">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Category Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control">
                            </div></div>

                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Category Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control">
                            </div></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 5px 0 0;" data-dismiss="modal" class="btn light_purple_bg"><i
                        class="fa fa-times"></i> Cancel</button>
                <button class="btn light_purple_bg"><i class="fa fa-save"></i> Save</button>
            </div>
        </div>

    </div>
</div>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function () {
        //  $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        $('#category_code').keyup(function () {

            var category_code_str = $(this).val();
            category_code_str = category_code_str.replace(/[^a-zA-Z0-9]/g, '-');

            category_code_str = category_code_str.replace(/\-+/g, '');

            category_code_str = category_code_str.toUpperCase();

            var category_code = category_code_str.trim();

            $('#category_code').val(category_code);

            if (category_code != '') {
                var url = '{{route('extensionsvalley.master.checkCategoryCode')}}';
                $.ajax({
                    method: "GET",
                    url: url,
                    data: 'category_code=' + category_code,
                    success: function (data) {
                        console.log(data);
                        if (data == 0) {
                            $('#category_code').css('color', 'green');
                            $('#category_code_status').html(
                                '<button tabindex="-1" type="button" title="Available" class="btn btn-success btn-sm"><i class="fa fa-check fa-xs" aria-hidden="true"></i></button>'
                            );

                        } else {
                            $('#category_code').css('color', 'red');
                            $('#category_code_status').html(
                                '<button tabindex="-1" type="button" title="Not available" class="btn btn-danger btn-sm"><i class="fa fa-ban fa-xs" aria-hidden="true"></i></button>'
                            );
                        }
                    }

                });

            }


        });

    });

    function categoryEditLoadData(obj,category_id,category_code,category_name,status,item_type_id){
        var edit_icon_obj = $(obj);
        $('#category_id').val(category_id);
        $('#category_code').prop('readonly',true).val(category_code);
        $('#category_name').val(category_name);
        $('#status').val(status);
        $('#item_type_id').val(item_type_id);
        $('#categoryForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
         $('#categoryForm')[0].reset();
    }
@include('Purchase::messagetemplate')
</script>

@endsection
