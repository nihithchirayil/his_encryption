  
               <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                   <div class="mate-input-box">
                   <label for="">Bill No</label>
                   <div class="clearfix"></div>
                   <input type="text" class="form-control bill_no_add" name="bill_no_add" id="bill_no_add" value=" {{$bill_details->bill_no?? '' }} ">
                   </div>
               </div>
               <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                   <div class="mate-input-box">
                   <label for="">Bill Tag</label>
                   <div class="clearfix"></div>
                   <input type="text" class="form-control bill_tag_add" name="bill_tag_add" id="bill_tag_add" value=" {{$bill_details->bill_tag?? '' }} ">
                   </div>
               </div>
             
               <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                   <div class="mate-input-box">
                   <label for="">Admitting Doctor Name</label>
                   <div class="clearfix"></div>
                   <select class="form-control select2" id="adm_doctor_id" name="adm_doctor_name">
                       <option  value="">Select Admitting Doctor Name</option>
                       @foreach ($adm_doctor1 as $adm1)
                       <option  value="{{ $adm1->id }}">{{ $adm1->doctor_name }}</option>
                   @endforeach
                   </select>
                   </div>
               </div>
    
               <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                   <div class="mate-input-box">
                   <label for="">Consulting Doctor Name</label>
                   <div class="clearfix"></div>
                   <select class="form-control select2" id="cons_doctor_id" name="cons_doctor_name">
                       <option  value="">Select Consulting Doctor Name</option>
                       @foreach ($cons_doctor1 as $cons1)
                       <option  value="{{ $cons1->id }}">{{ $cons1->doctor_name }}</option>
                   @endforeach
                   </select>
                   </div>
               </div>
             
               <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                   <div class="mate-input-box">
                   <label for="">Bill Amount</label>
                   <div class="clearfix"></div>
                   <input type="text" class="form-control bill_amount" name="bill_amount" id="bill_amount" value=" {{$bill_details->bill_amount ?? '' }} ">
                   </div>
               </div>
               <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                   <div class="mate-input-box">
                   <label for="">Paid Status</label>
                   <div class="clearfix"></div>
                   <select class="form-control status" name="status" id="status" >
                       <option value="">Select</option>
                       <option value="1">Paid</option>
                       <option value="0">Unpaid</option>
                   </select>
               </div>
               </div>
               <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                   <div class="mate-input-box">
                   <label for="">Net Amount</label>
                   <div class="clearfix"></div>
                   <input type="text" class="form-control net_amount" name="net_amount" id="net_amount" value=" {{$bill_details->net_amount ?? '' }} ">
                   </div>
               </div>
               <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                   <div class="mate-input-box">
                   <label for="">Location</label>
                   <div class="clearfix"></div>
                   <input type="text" class="form-control location" name="location" id="location" value=" {{$bill_details->location ?? '' }} ">
                   </div>
               </div>
             <div class="row">
                        <div class="theadscroll always-visible" id="result_container_div" style="position: relative;height:90%;border-radius:10px;width:100%;" >
                           
                             <div class="panel-body" style="min-height: 300px;">
                                  @csrf
                <table id="result_table" class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                       style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th style ="width:1%">SL.No.</th>
                        <th width="20%">Item Name</th>
                        <th width="20%">Discount Amount</th>
                        <th width="20%">Net Amount</th>
                        <th width="20%">Quantity</th>
                        <th width="19%">Unit MRP</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                      $i=1;  
                    @endphp
                    @if($count>0)
                    @foreach($bills as $bill)
                        
                    <tr>      
                         <td class = "td_common_numeric_rules">{{$i++}}</td>
                         <td class ="common_td_rules">{{ $bill->item_desc }}</td>
                         <td class ="td_common_numeric_rules">{{ $bill->discount_amount}}</td>
                         <td class ="td_common_numeric_rules">{{ $bill->net_amount}}</td>
                         <td class ="td_common_numeric_rules">{{ $bill->qty}}</td>
                         <td class ="td_common_numeric_rules">{{ $bill->unit_mrp}}</td>
                           
                   </tr>
                    @endforeach
                    @endif
                </tbody>
                </table>
      </div>
     </div>
 </div>