@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.ot_bill_group_detail')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Group Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="group_name"
                                    value="{{ $searchFields['group_name'] ?? '' }}">
                            </div></div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Service Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="service_name"
                                    value="{{ $searchFields['service_name'] ?? '' }}">
                            </div></div>
                            
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                             <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>  Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; max-height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Group Name</th>
                                    <th>Group Code</th>
                                    <th>Service Name</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($list) > 0)
                                    @foreach ($list as $data)
                                        <tr style="cursor: pointer;" onclick="editLoadData(this,
                                            '{{$data->id}}',
                                            '{{$data->head_id}}',
                                            '{{$data->service_item_id}}',
                                            '{{$data->price}}'
                                            )">
                                            <td class="code common_td_rules">{{ $data->group_name }}</td>
                                            <td class="category_name common_td_rules">{{ $data->group_code }}</td>
                                            <td class="item common_td_rules">{{ $data->name  }}</td>
                                            <td class="item common_td_rules">{{ $data->price  }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="category_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.save_ot_bill_group_detail')}}" method="POST" id="headForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="head_id" id="head_id" value="0">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Group Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                              {!! Form::select('group_name', array("-1"=> " Select") + $bill_head->toArray(), null,
                                ['class'=>"form-control select2", 'id'=>"group_name", 
                                'required' => 'required']) !!}
                                 <span class="error_red">{{ $errors->first('group_name') }}</span>
                        </div></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Service Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            {!! Form::select('service_name', array("-1"=> " Select") + $services->toArray(), null,
                                ['class'=>"form-control select2", 'id'=>"service_name", 'required' => 'required']) !!}
                                <span class="error_red">{{ $errors->first('service_name') }}</span>
                        </div></div>
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Price<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                               <input type="text" value="0" required class="form-control" name="price" id="price">
                               <span class="error_red">{{ $errors->first('price') }}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function () {
         // $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });
        $('.datepicker').datetimepicker({
                            format: 'DD-MMM-YYYY'
                        });

    });

    function editLoadData(obj,id,head_id,service_item_id,price){
        var edit_icon_obj = $(obj);
        $('#head_id').val(id);
        $('#group_name').val(head_id);
        $('#group_name').select2().trigger('change');
        $('#service_name').val(service_item_id);
        $('#service_name').select2().trigger('change');
        $('#price').val(price);
        $('#headForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
        $('#headForm')[0].reset();
        $('#group_name').val('-1');
        $('#group_name').select2().trigger('change');
        $('#service_name').val('-1');
        $('#service_name').select2().trigger('change');
    }
@include('Purchase::messagetemplate')
</script>

@endsection
