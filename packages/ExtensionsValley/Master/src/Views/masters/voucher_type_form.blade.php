@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.listVoucherTypes')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('types', array(""=> " Select") + $type->toArray(),$searchFields['types'] ?? '',
                                ['class'=>"form-control", 'id'=>"types"]) !!}
                            </div></div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                             <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>  Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; max-height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Type</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($voucher_list) > 0)
                                    @foreach ($voucher_list as $vc)
                                        <tr style="cursor: pointer;" onclick="voucherEditLoadData(this,'{{$vc->v_id}}','{{$vc->type}}',
                                                '{{$vc->status}}')">
                                            <td class="category_code common_td_rules">{{ $vc->type }}</td>
                                            <td class="status common_td_rules">{{ $vc->status_name  }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="category_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.saveVoucherType')}}" method="POST" id="voucherForm">
                        {!! Form::token() !!}
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Type</label>
                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="type_id" id="type_id" required="">
                            <input type="text" class="form-control" name="type" id="type">
                            <span style="color: #d14;"> {{ $errors->first('type') }}</span>
                        </div></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Status</label>
                            <div class="clearfix"></div>
                            <select class="form-control" name="status" id="status">
                                <option value="1">{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                        </div></div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        
                        <div class="col-md-6 padding_sm">
                           <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function () {
        //  $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

       

    });

    function voucherEditLoadData(obj,id,type,status){
        var edit_icon_obj = $(obj);
        $('#type_id').val(id);
        $('#type').val(type);
        $('#status').val(status);
        $('#voucherForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
         $('#voucherForm')[0].reset();
    }
@include('Purchase::messagetemplate')
</script>

@endsection
