<script type="text/javascript">
    $(document).ready(function () {
        $(".page-link").click(function () {
            var url = $(this).attr("href");
            var generic_name = $('#generic_name_search').val();
            $.ajax({
            type: "GET",
                url: url,
                data: "generic_name="+generic_name,
                beforeSend: function () {
                    $('#searchdatabtn').attr('disabled',true);
                    $('#searchdataspin').removeClass('fa fa-search');
                    $('#searchdataspin').addClass('fa fa-spinner');
                    $('#generilistdiv').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                },
                success: function (msg) { 
                    $('#generilistdiv').html(msg);
                },
                complete: function () {
                    $('#searchdatabtn').attr('disabled',false);
                    $('#searchdataspin').removeClass('fa fa-spinner');
                    $('#searchdataspin').addClass('fa fa-search');
                    $('#generilistdiv').LoadingOverlay("hide");
                },error: function(){
                    toastr.error("Please Check Internet Connection");
                }
            });
            return false;
        });

    });

</script>
<div class="box-body clearfix">
<div class="theadscroll" style="position: relative; height: 417px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
            style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="30%">Generic Name</th>
                <th width="30%">Therapeutic Category</th>
                <th width="30%">Therapeutic Subcategory</th>
                <th width="5%">Status</th>
                <th width="5%">Edit</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($generic_name_list) != 0) {
                foreach ($generic_name_list as $list) {
                    $status="Inactive";
                    if($list->status=='1'){
                        $status="Active";
                    }
                    ?>
            <tr class='generic{{$list->generic_id}}'>
                <td id="gen{{$list->generic_id}}" class="category_name common_td_rules" >{{ $list->generic_name }}</td>
                <td id="cat{{$list->generic_id}}" class="status common_td_rules">{{ $list->category_name }}</td>
                <td id="sub{{$list->generic_id}}" class="status common_td_rules">{{ $list->subcategory_name }}</td>
                <td id="sta{{$list->generic_id}}" class="status common_td_rules">{{ $status }}</td>
                <td id="{{$list->generic_name}}" class="text-center">
                    <button class='btn btn-block light_purple_bg edit{{$list->generic_id}}' type="button" onclick="genericEditLoadData('{{$list->generic_id}}','{{$list->generic_name}}','{{$list->status}}','{{$list->category_code}}','{{$list->subcategory_code}}')"><i class="fa fa-edit"></i></button>
                </td>
            </tr>
            <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="5" class="re-records-found">No Records found</td>
                </tr>
                <?php
            }
            ?>
            
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-center">
    <ul class="pagination purple_pagination pull-right" style="margin-top: -6px;">
        {!! $page_links !!}
    </ul>
</div>
</div>