@if ($offset == 0)
@if($shift1_enable == 1)
<div class="col-md-12">
    @else
    <div class="col-md-6">
        @endif
        <div class="theadscroll" id="load_data2" style="position: relative; height: 350px;">
            <table
                class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                style="border: 1px solid #CCC;">
                <thead>
                    @if($shift1_enable != 1)
                    <tr class="table_header_bg">
                        <th colspan="9" style="text-align: center">Shift 1</th>
                    </tr>
                    @endif
                    <tr class="table_header_bg">
                        <th style="text-align: center">#</th>
                        <th scope="col" onclick="sorting(tbody01,1)" width="15%" style="cursor: pointer;">UHID<i
                                class=""></i></th>

                        <th width="12%" style="white-space: nowrap">Patient Name</th>
                        <th width="11%">Doctor Name</th>
                        <th width="10%">Visit Date</th>
                        <th width="8%">Type</th>
                        <th width="8%">Token</th>
                        <th width="9%">Old UHID</th>
                        <th width="9%">Created By</th>
                        <th width="9%">Remarks</th>
                        <th title="Doctor List" width="2%"><i class="fa fa-user-md"></i></th>
                        <th width="5%">Action</th>
                    </tr>
                </thead>
                <tbody id="tbody01">
                    <?php
                    if (count($mrd_list) != 0) {
                        foreach ($mrd_list as $key => $lists) {
                            if($key=='morning'){
                            foreach ($lists as $list){
                                $style_class = '';
                                $more_vists = '';
                                if ($list->is_printed == 1 && $list->renewal_type == 1) {
                                    $style_class = 'printed_new_reg_mrd';
                                } else {
                                    if ($list->is_printed == 1) {
                                        $style_class = 'printed_mrd';
                                    }
                                    if ($list->renewal_type == 1) {
                                        $style_class = 'new_reg_mrd';
                                    }
                                }

                                  if(intval($list->visit_count) > 1){
                                    $more_vists = 'more_vists';
                                }
                                if ($list->renewal_type == 1 && $list->is_newborn == 1) {
                                    $style_class = 'new_born_mrd';
                                }
                            ?>
                    <tr class='generic{{ $list->mrd_id }} {{$style_class}}'>
                        <td style="text-align: center">
                        <div class="checkbox checkbox-success inline">
                            @php
                            $receiveAllDataCheck="";
                            @endphp
                            @if (isset($receivemrd[$list->mrd_id]))
                            @php
                            $receiveAllDataCheck="checked=''";
                            @endphp
                            @endif
                            <input {{ $receiveAllDataCheck }}
                                onclick="checkReceiveDispathMrd({{ $list->mrd_id }},{{ $list->patient_id }})"
                                title="Receive/Dispatch" type="checkbox" id="receiveAllData<?=$list->mrd_id ?>"
                                value="1">
                            <label for="receiveAllData<?=$list->mrd_id ?>"></label>
                        </div>
                        </td>
                        <td class="common_td_rules">{{ $list->uhid }}</td>
                        <td id="patient_name_list{{  $list->mrd_id }}" class="common_td_rules"
                            title="{{ $list->patient_name }}">{{ $list->patient_name }}</td>
                        <td class="common_td_rules">{{ $list->doctor_name }}</td>
                        <td class="common_td_rules">{{ $list->visit_time }}</td>
                        <td class="common_td_rules">{{ $list->visit_type }}</td>
                        <td style="text-align: center">                        
                        @if(intval($list->visit_count) > 1)
                            <span class="badge badge-pill font-weight-ormal more_vists_blink">{{ $list->token_no }}</span>
                        @else 
                            {{ $list->token_no }} 
                        @endif
                    
                        </td>
                        
                        <td class="common_td_rules">{{ $list->ext_uhid }}</td>
                        <td class="common_td_rules">{{ $list->created_name }}</td>
                        <td class="common_td_rules">
                            <input type="text" class="form-control" id="remarks_{{ $list->mrd_id }}"
                                value="{{ $list->remarks }}">
                        </td>
                        <td class="text-center">
                            @if(intval($list->visit_count)>1)
                            <button class='btn btn-primary' id="getMrdDoctorListBtn{{  $list->mrd_id }}" type="button"
                                onclick="getMrdDoctorList('{{ $list->mrd_id }}','{{ $list->patient_id }}')"><i
                                    id="getMrdDoctorListSpin{{  $list->mrd_id }}" class="fa fa-user-md"></i></button>
                            @else
                            -
                            @endif
                        </td>
                        <td class="text-center">
                            @if (intval($list->status) == 1)
                            <button class='btn btn-success' type="button"
                                onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                                    class="fa fa-download"></i>&nbsp;Receive</button>
                            @else
                            <button class='btn btn-warning' type="button"
                                onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                                    class="fa fa-upload"></i>&nbsp;Dispatch</button>
                            @endif
                        </td>
                    </tr>
                    <?php
                        }
                    }
                }
            }

                    ?>

                </tbody>
            </table>
        </div>
    </div>
    @if($shift1_enable != 1)
    <div class="col-md-6">
        <div class="theadscroll" id="load_data1" style="position: relative; height: 350px;">

            <table
                class="table no-margin table_sm theadfix_wrapper2 table-striped no-border table-condensed styled-table"
                style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th colspan="9" style="text-align: center">Shift 2</th>
                    </tr>
                    <tr class="table_header_bg">
                        <th style="text-align: center">#</th>
                        <th scope="col" onclick="sorting(tbody02,1)" width="10%" style="cursor: pointer;">UHID<i
                                class=""></i></th>
                        <th width="12%" style="white-space: nowrap">Patient Name</th>
                        <th width="11%">Doctor Name</th>
                        <th width="8%">Visit Date</th>
                        <th width="8%">Type</th>
                        <th width="8%">Token</th>
                        <th width="9%">Old UHID</th>
                        <th width="9%">Created By</th>
                        <th width="9%">Remarks</th>
                        <th width="5%">Action</th>
                    </tr>
                </thead>
                <tbody id="tbody02">
                    <?php
                 if (count($mrd_list) != 0) {

                    foreach ($mrd_list as $key => $lists) {
                    if($key=='evening'){
                     foreach($lists as $list)  {
                        $style_class = '';
                        $more_vists = '';
                        if ($list->is_printed == 1 && $list->renewal_type == 1) {
                            $style_class = 'printed_new_reg_mrd';
                        } else {
                            if ($list->is_printed == 1) {
                                $style_class = 'printed_mrd';
                            }
                            if ($list->renewal_type == 1) {
                                $style_class = 'new_reg_mrd';
                            }
                        }
                          if(intval($list->visit_count)>1){
                            $more_vists = 'more_vists';
                        }
                        if ($list->renewal_type == 1 && $list->is_newborn == 1) {
                            $style_class = 'new_born_mrd';
                        }
                ?>
                    <tr class='generic{{ $list->mrd_id }} {{$style_class}}'>
                        <td style="text-align: center">
                        <div class="checkbox checkbox-success inline">
                            @php
                            $receiveAllDataCheck="";
                            @endphp
                            @if (isset($receivemrd[$list->mrd_id]))
                            @php
                            $receiveAllDataCheck="checked=''";
                            @endphp
                            @endif
                            <input {{ $receiveAllDataCheck }}
                                onclick="checkReceiveDispathMrd({{ $list->mrd_id }},{{ $list->patient_id }})"
                                title="Receive/Dispatch" type="checkbox" id="receiveAllData<?=$list->mrd_id ?>"
                                value="1">
                            <label for="receiveAllData<?=$list->mrd_id ?>"></label>
                        </div>
                        </td>
                        <td class="common_td_rules">{{ $list->uhid }}</td>
                        <td class="common_td_rules" title="{{ $list->patient_name }}">{{ $list->patient_name }}</td>
                        <td class="common_td_rules">{{ $list->doctor_name }}</td>
                        <td class="common_td_rules">{{ $list->visit_time }}</td>
                        <td class="common_td_rules">{{ $list->visit_type }}</td>
                        <td style="text-align: center">
                        @if(intval($list->visit_count) > 1)
                            <span class="badge badge-pill font-weight-ormal more_vists_blink">{{ $list->token_no }}</span>
                        @else 
                            {{ $list->token_no }} 
                        @endif
                        </td>
                        <td class="common_td_rules">{{ $list->ext_uhid }}</td>
                        <td class="common_td_rules">{{ $list->created_name }}</td>
                        <td class="common_td_rules">
                            <input type="text" class="form-control" id="remarks_{{ $list->mrd_id }}"
                                value="{{ $list->remarks }}">
                        </td>
                        <td class="text-center">
                            @if (intval($list->status) == 1)
                            <button class='btn btn-success' type="button"
                                onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                                    class="fa fa-download"></i>&nbsp;Receive</button>
                            @else
                            <button class='btn btn-warning' type="button"
                                onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                                    class="fa fa-upload"></i>&nbsp;Dispatch</button>
                            @endif
                        </td>
                    </tr>
                    <?php
                    }
                }
                    }
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>
    @endif
    @else
    <table>
        <tbody id="morningLoad">
            <?php
    if (count($mrd_list) != 0) {
        foreach ($mrd_list as $key => $lists) {
                    if($key=='morning'){
                     foreach($lists as $list)  {
                        $style_class = '';
                        $more_vists = '';
                        if ($list->is_printed == 1 && $list->renewal_type == 1) {
                            $style_class = 'printed_new_reg_mrd';
                        } else {
                            if ($list->is_printed == 1) {
                                $style_class = 'printed_mrd';
                            }
                            if ($list->renewal_type == 1) {
                                $style_class = 'new_reg_mrd';
                            }
                        }

                          if(intval($list->visit_count)>1){
                                    $more_vists = 'more_vists';
                            }
                        if ($list->renewal_type == 1 && $list->is_newborn == 1) {
                            $style_class = 'new_born_mrd';
                        }

            ?>

            <tr class='generic{{ $list->mrd_id }} {{$style_class}}'>
                <td style="text-align: center">
                    <div class="checkbox checkbox-success inline">
                        @php
                        $receiveAllDataCheck="";
                        @endphp
                        @if (isset($receivemrd[$list->mrd_id]))
                        @php
                        $receiveAllDataCheck="checked=''";
                        @endphp
                        @endif
                        <input {{ $receiveAllDataCheck }}
                            onclick="checkReceiveDispathMrd({{ $list->mrd_id }},{{ $list->patient_id }})"
                            title="Receive/Dispatch" type="checkbox" id="receiveAllData<?=$list->mrd_id ?>"
                            value="1">
                        <label for="receiveAllData<?=$list->mrd_id ?>"></label>
                    </div>
                </td>
                <td class="common_td_rules">{{ $list->uhid }}</td>
                <td id="patient_name_list{{  $list->mrd_id }}" class="common_td_rules"
                    title="{{ $list->patient_name }}">{{ $list->patient_name }}</td>
                <td class="common_td_rules">{{ $list->doctor_name }}</td>
                <td class="common_td_rules">{{ $list->visit_time }}</td>
                <td class="common_td_rules">{{ $list->visit_type }}</td>
                <td style="text-align: center">
                @if(intval($list->visit_count) > 1)
                    <span class="badge badge-pill font-weight-ormal more_vists_blink">{{ $list->token_no }}</span>
                @else 
                    {{ $list->token_no }} 
                @endif
                </td>
                <td class="common_td_rules">{{ $list->ext_uhid }}</td>
                <td class="common_td_rules">{{ $list->created_name }}</td>
                <td class="common_td_rules">
                    <input type="text" class="form-control" id="remarks_{{ $list->mrd_id }}"
                        value="{{ $list->remarks }}">
                </td>
                <td class="text-center">
                    @if(intval($list->visit_count)>1)
                    <button class='btn btn-primary' id="getMrdDoctorListBtn{{  $list->mrd_id }}" type="button"
                        onclick="getMrdDoctorList('{{ $list->mrd_id }}','{{ $list->patient_id }}')"><i
                            id="getMrdDoctorListSpin{{  $list->mrd_id }}" class="fa fa-user-md"></i></button>
                    @else
                    -
                    @endif
                </td>
                <td class="text-center">
                    @if ($list->status == '0')
                    <button class='btn btn-warning' type="button"
                        onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                            class="fa fa-upload"></i>&nbsp;Dispatch</button>
                    @endif
                </td>
            </tr>
            <?php
        }
             }
        }
    }
    ?>
        </tbody>
    </table>
    <table>
        <tbody id="eveningLoad">


            <?php
    if (count($mrd_list) != 0) {
        foreach ($mrd_list as $key => $lists) {
                    if($key=='evening'){
                     foreach($lists as $list)  {
                        $style_class = '';
                        if ($list->is_printed == 1 && $list->renewal_type == 1) {
                            $style_class = 'printed_new_reg_mrd';
                        } else {
                            if ($list->is_printed == 1) {
                                $style_class = 'printed_mrd';
                            }
                            if ($list->renewal_type == 1) {
                                $style_class = 'new_reg_mrd';
                            }
                        }
                        if ($list->renewal_type == 1 && $list->is_newborn == 1) {
                            $style_class = 'new_born_mrd';
                        }

            ?>
            <tr class='generic{{ $list->mrd_id }} {{$style_class}}'>
                <td style="text-align: center">
                    <div class="checkbox checkbox-success inline">
                        @php
                        $receiveAllDataCheck="";
                        @endphp
                        @if (isset($receivemrd[$list->mrd_id]))
                        @php
                        $receiveAllDataCheck="checked=''";
                        @endphp
                        @endif
                        <input {{ $receiveAllDataCheck }}
                            onclick="checkReceiveDispathMrd({{ $list->mrd_id }},{{ $list->patient_id }})"
                            title="Receive/Dispatch" type="checkbox" id="receiveAllData<?=$list->mrd_id ?>"
                            value="1">
                        <label for="receiveAllData<?=$list->mrd_id ?>"></label>
                    </div>
                </td>
                <td class="common_td_rules">{{ $list->uhid }}</td>
                <td class="common_td_rules" title="{{ $list->patient_name }}">{{ $list->patient_name }}</td>
                <td class="common_td_rules">{{ $list->doctor_name }}</td>
                <td class="common_td_rules">{{ $list->visit_time }}</td>
                <td class="common_td_rules">{{ $list->visit_type }}</td>
                <td class="common_td_rules">{{ $list->token_no }}</td>
                <td class="common_td_rules">{{ $list->ext_uhid }}</td>
                <td class="common_td_rules">{{ $list->created_name }}</td>
                <td class="common_td_rules">
                    <input type="text" class="form-control" id="remarks_{{ $list->mrd_id }}"
                        value="{{ $list->remarks }}">
                </td>
                <td class="text-center">
                    @if ($list->status == '0')
                    <button class='btn btn-warning' type="button"
                        onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                            class="fa fa-upload"></i>&nbsp;Dispatch</button>
                    @endif
                </td>
            </tr>
            <?php
        }
    }
}
    }
    ?>
        </tbody>
    </table>
    @endif



    <script>
        $('#load_data2').on('scroll', function() {
        var scrollHeight = $('#load_data2').height();
        //var scrollPosition = $('#load_data').height() + $('#load_data').scrollTop();
        var scrollPosition = $('#load_data2').scrollTop() + $('#load_data2').innerHeight();
        if (scrollPosition + 3 >= $('#load_data2')[0].scrollHeight) {
            //if (scrollPosition > scroll_length_initial) {
            //alert('ddd');
            offset = parseInt(offset) + limit;
            //limit = limit+15;
            //console.log(limit+'##'+offset+'##'+total_rec);
            if (offset < total_rec) {
                setTimeout(function() {
                    searchBill(limit, offset);
                }, 500);
                //searchList(limit,offset);
            }
        }
    })


    $('#load_data1').on('scroll', function() {
        var scrollHeight = $('#load_data1').height();
        //var scrollPosition = $('#load_data').height() + $('#load_data').scrollTop();
        var scrollPosition = $('#load_data1').scrollTop() + $('#load_data1').innerHeight();
        if (scrollPosition + 3 >= $('#load_data1')[0].scrollHeight) {
            //if (scrollPosition > scroll_length_initial) {
            //alert('ddd');
            offset = parseInt(offset) + limit;
            //limit = limit+15;
            //console.log(limit+'##'+offset+'##'+total_rec);
            if (offset < total_rec) {
                setTimeout(function() {
                    searchBill(limit, offset);
                }, 500);
                //searchList(limit,offset);
            }
        }
    })
    </script>
