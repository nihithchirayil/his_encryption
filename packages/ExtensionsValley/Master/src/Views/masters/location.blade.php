@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style> 
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        
                        <form action="{{route('extensionsvalley.master.locationsave')}}" id="locationform" method="POST">
                            {!! Form::token() !!}
                          
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <input type="hidden" name="bt_id" id="bt_id" > 
                                    <label for="">From Location</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" style='color:#555555;'  name="from_location" id="from_location">
                                    <option value="">Select Location</option>
                                       <?php foreach($location_name as $key=>$val){
                                           ?>
                                            <option value="<?=$key?>"><?=$val?></option>
                                           <?php
                                       }
                                       ?>
    
                                    </select>
                                </div></div>
                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">To Location</label>
                                        <div class="clearfix"></div>
                                        <select class="form-control select2" style='color:#555555;'  name="to_location" id="to_location">
                                        <option value="">Select Location</option>
                                           <?php foreach($location_name as $key=>$val){
                                               ?>
                                                <option value="<?=$key?>"><?=$val?></option>
                                               <?php
                                           }
                                           ?>
        
                                        </select>
                                    </div></div>
                                    <div class="col-md-2 padding_sm">
                                        <button class="btn btn-block light_purple_bg" ><i class="fa fa-save"></i> Transfer</button>
                                    </div>
                           
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 550px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>From Location </th>
                                    <th>To Location </th>
                                    <th>Created At</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($location_list) > 0)
                                    @foreach ($location_list as $location)
                                <tr style="cursor: pointer;" onclick="transfer(this,
                                    '{{$location->id}}',
                                    '{{$location->from_location}}',
                                    '{{$location->to_location}}')" >
                                            <td class="location_type common_td_rules">{{ $location->from_location_name }}</td>
                                            <td class="location_code common_td_rules">{{ $location->to_location_name }}</td>
                                            <td class="location_name common_td_rules">{{ $location->created_at }}</td>
                                           
                                            
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="location_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right" style="margin-top:-31px;">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {{-- {!! $page_links !!} --}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
            </div>
        </div>
  

        @stop

        @section('javascript_extra')
        <script type="text/javascript">
        
        
        
        
            $(document).ready(function () {
        
              
                // $('#menu_toggle').trigger('click');
                var $table = $('table.theadfix_wrapper');
        
                $table.floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('.theadscroll');
                    }
        
                });
        
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
        
        
                $('.fixed_header').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
        
        
            });
        
            function transfer(obj,id,from_location,to_location){
                // alert(ledger_name);
                
                var edit_icon_obj = $(obj);
                 $('#bt_id').val(id);
        
              
                // $('#manufacturer_code').prop('readonly',true).val(manufacturer_code);
                $('#from_location').val(from_location);
                $('#from_location').select2();
                $('#to_location').val(to_location);
                $('#to_location').select2();
                $('#locationform').attr('action', '{{$updateUrl}}');


            }
           
        
            function resetFunction(){
                 $('#locationForm')[0].reset();
                 $("#location_code").prop("readonly", false);
            }
        @include('Purchase::messagetemplate')
        </script>
            
        @endsection
        