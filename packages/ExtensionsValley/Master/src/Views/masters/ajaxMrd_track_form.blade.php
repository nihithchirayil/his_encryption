@if ($offset == 0)
<div class="row">
    @if ($mrd_type=='outstore')
    @php
    $comp_id='load_data3';
    @endphp
    @elseif($mrd_type=='todays_received')
    @php
    $comp_id='load_data4';
    @endphp
    @endif
    <div class="col-md-12 theadscroll" id="{{ $comp_id }}" style="position: relative; height: 350px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th style="text-align: center">#</th>
                    <th scope="col" onclick="sorting(tbody03,1)" width="10%" style="cursor: pointer;">UHID<i
                            class=""></i></th>
                    <th width="12%">Patient Name</th>
                    <th width="12%">Doctor Name</th>
                    <th width="8%">Visit Date</th>
                    <th width="8%">Visit Status</th>
                    <th width="8%">Token</th>
                    <th width="10%">shift</th>
                    <th width="10%">Old UHID</th>
                    <th width="9%">Created By</th>
                    <th width="10%">Remarks</th>
                    <th width="5%">Action</th>
                </tr>
            </thead>
            <tbody id="tbody03">
                <?php
                if (count($mrd_list) != 0) {
                    foreach ($mrd_list as $list) {
                        $status="In Store";
                        if($list->status=='1'){
                            $status="Not in Store";
                        }
                        ?>
                <tr class='generic{{ $list->mrd_id }}'>
                    <td style="text-align: center">
                        <div class="checkbox checkbox-success inline">
                            @php
                            $receiveAllDataCheck="";
                            @endphp
                            @if (isset($receivemrd[$list->mrd_id]))
                            @php
                            $receiveAllDataCheck="checked=''";
                            @endphp
                            @endif
                            <input {{ $receiveAllDataCheck }}
                                onclick="checkReceiveDispathMrd({{ $list->mrd_id }},{{ $list->patient_id }})"
                                title="Receive/Dispatch" type="checkbox" id="receiveAllData<?=$list->mrd_id ?>"
                                value="1">
                            <label for="receiveAllData<?=$list->mrd_id ?>"></label>
                        </div>
                    </td>
                    <td class="common_td_rules">{{ $list->uhid }}</td>
                    <td class="common_td_rules">{{ $list->patient_name }}</td>
                    <td class="common_td_rules">{{ $list->doctor_name }}</td>
                    <td class="common_td_rules">{{ $list->visit_time }}</td>
                    <td class="common_td_rules">{{ $list->visit_type }}</td>
                    <td class="common_td_rules">{{ $list->token_no }}</td>
                    <td class="common_td_rules">
                        @if ($list->slot_hour != '')
                        @if ($list->session_id ==1)
                        Shift 1
                        @else
                        Shift 2
                        @endif
                        @endif
                    </td>
                    <td class="common_td_rules">{{ $list->ext_uhid }}</td>
                    <td class="common_td_rules">{{ $list->created_name }}</td>
                    <td class="common_td_rules">
                        <input type="text" class="form-control" id="remarks_{{ $list->mrd_id }}"
                            value="{{ $list->remarks }}">
                    </td>
                    <td class="text-center">
                        @if (intval($list->status)==1)
                        <button class='btn btn-success' type="button"
                            onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                                class="fa fa-download"></i>&nbsp;Receive</button>
                        @else
                        <button class='btn btn-warning' type="button"
                            onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                                class="fa fa-upload"></i>&nbsp;Dispatch</button>
                        @endif
                    </td>
                </tr>
                <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    $('#load_data3').on('scroll', function() {
            var scrollHeight = $('#load_data3').height();
            var scrollPosition = $('#load_data3').scrollTop() + $('#load_data3').innerHeight();

            if (scrollPosition + 3 >= $('#load_data3')[0].scrollHeight) {

                offset = offset + limit;
                console.log(offset,total_rec);
                if (offset < total_rec) {
                    setTimeout(function() {
                        searchBill(limit, offset);
                    }, 500);
                }
            }
        })
        $('#load_data4').on('scroll', function() {
            var scrollHeight = $('#load_data4').height();
            var scrollPosition = $('#load_data4').scrollTop() + $('#load_data4').innerHeight();
            console.log(offset,total_rec);

            if (scrollPosition + 3 >= $('#load_data4')[0].scrollHeight) {

                offset = offset + limit;

                if (offset < total_rec) {
                    setTimeout(function() {
                        searchBill(limit, offset);
                    }, 500);
                }
            }
        })
</script>
@else
<?php
    if (count($mrd_list) != 0) {
        foreach ($mrd_list as $list) {
            $status="In Store";
            if($list->status=='1'){
                $status="Not in Store";
            }
            ?>
<tr class='generic{{ $list->mrd_id }}'>
    <td style="text-align: center">
        <div class="checkbox checkbox-success inline">
            @php
            $receiveAllDataCheck="";
            @endphp
            @if (isset($receivemrd[$list->mrd_id]))
            @php
            $receiveAllDataCheck="checked=''";
            @endphp
            @endif
            <input {{ $receiveAllDataCheck }}
                onclick="checkReceiveDispathMrd({{ $list->mrd_id }},{{ $list->patient_id }})"
                title="Receive/Dispatch" type="checkbox" id="receiveAllData<?=$list->mrd_id ?>" value="1">
            <label for="receiveAllData<?=$list->mrd_id ?>"></label>
        </div>
    </td>
    <td class="common_td_rules">{{ $list->uhid }}</td>
    <td class="common_td_rules">{{ $list->patient_name }}</td>
    <td class="common_td_rules">{{ $list->doctor_name }}</td>
    <td class="common_td_rules">{{ $list->visit_time }}</td>
    <td class="common_td_rules">{{ $list->visit_type }}</td>
    <td class="common_td_rules">{{ $list->token_no }}</td>
    <td class="common_td_rules">
        @if ($list->slot_hour != '')
        @if ($list->slot_hour < 13) Morning @else Evening @endif @endif </td>
    <td class="common_td_rules">{{ $list->ext_uhid }}</td>
    <td class="common_td_rules">{{ $list->created_name }}</td>
    <td class="common_td_rules">
        <input type="text" class="form-control" id="remarks_{{ $list->mrd_id }}" value="{{ $list->remarks }}">
    </td>
    <td class="text-center">
        @if ($list->status == '1')
        <button class='btn btn-success' type="button"
            onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                class="fa fa-download"></i>&nbsp;Receive</button>
        @else
        <button class='btn btn-warning' type="button"
            onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                class="fa fa-upload"></i>&nbsp;Dispatch</button>
        @endif
    </td>
</tr>
<?php
        }
    }
    ?>
@endif
