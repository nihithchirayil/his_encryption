@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>
    .nav-tabs>li.active>a,
    .nav-tabs>li.active>a:focus,
    .nav-tabs>li.active>a:hover {
        background-color: #238d74 !important;
        color: white !important;
    }

    .ajaxSearchBox {
        margin-top: 12px !important;
    }

    .printed_mrd {
        background: lightgreen !important;
    }

    .new_reg_mrd {
        background: #a2dddf !important;
    }


    .more_vists {
        /* animation: blinker 1.2s linear infinite; */
        background: #ec2a0d !important;
        color: white
    }

    .more_vists_blink {
        background: #21b146;
        /* animation: blinker 2.5s linear infinite; */
        animation: bounce .3s infinite alternate;
        width: 40px;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

    @keyframes bounce {
        100% {
            transform: scale(1.1);
        }
    }

    .printed_new_reg_mrd {
        background: lightgreen !important;
        color: #066b6e !important;
    }

    .new_born_mrd {
        background: #ff0 !important;
    }

    thead th {
        position: sticky;
        top: 0;
        background-color: #01987a;
    }
</style>
<link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
@endsection
@section('content-area')


<div class="modal fade" id="error_dispatchdatamodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%;">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">File is not received from the following doctor</h4>
            </div>
            <div class="modal-body">
                <div class="box no-border no-margin">
                    <div class="box-footer revenue_main_shadow" style="min-height: 500px;">
                        <div class="col-md-12 padding_sm" id="error_dispatchdatamodelDiv">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="morevist_doctorlist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 500px;width: 100%;">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="morevist_doctorlistheader">NA</h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
                <div class="box no-border no-margin">
                    <div class="box-footer revenue_main_shadow" style="min-height: 300px;">
                        <div class="col-md-12 padding_sm" id="morevist_doctorlistdiv">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>


<div class="right_col">
    <input type="hidden" name="base_url" id="base_url" value="{{ URL::To('/') }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
    <input type="hidden" id="shift1_enable" value="{{ $shift1_enable }}">

    <div class="row">
        <input type="hidden" id="head_id_hidden" value="">
        <span class="pull-right" style="text-align: right;padding-right: 85px;font-size: 12px; font-weight: bold;">
            {{ $title }}
        </span>
        <span class="pull-left">Screen will refresh in <span class="js-timeout" style="color:red">2:00</span></span>
    </div>

    <div class="col-md-12">
        <div class="box no-border">
            <div class="box-body clearfix">
                <form action="{{ route('extensionsvalley.master.list_generic_name') }}" id="listingSearchForm"
                    method="POST">
                    {!! Form::token() !!}
                    @if($shift1_enable == 1)
                    <div class="col-md-2 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Barcode</label>
                            <input type="text" name="txt_seach_scan" autocomplete="off" value="" class="form-control"
                                id="txt_seach_scan" placeholder="Search Barcode..">
                        </div>
                    </div>
                    @endif

                    <div class="col-md-2 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">UHID</label>
                            <input type="text" name="op_no_search" autocomplete="off" value="" class="form-control"
                                id="op_no_search" placeholder="Patient name/UHID">
                            <input type="hidden" id="op_no_search_hidden" value="" name="op_no_search_hidden">
                            <div id="op_no_searchCodeAjaxDiv" class="ajaxSearchBox"
                                style="min-height:300px; width: 450px !important;"></div>
                        </div>
                    </div>
                    @if($shift1_enable == 0)
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Shift</label>
                            <div class="clearfix"></div>
                            <select class="form-control select2" style='color:#555555;' name="shift" id="shift">
                                <option value="0">{{ __('All') }}</option>
                                <option value="1">{{ __('Morning') }}</option>
                                <option value="2">{{ __('Evening') }}</option>
                            </select>
                        </div>
                    </div>
                    @endif
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Doctor</label>
                            <div class="clearfix"></div>
                            <select class="form-control select2" style='color:#555555;' name="doctor" id="doctor">
                                <option value="-1">{{ 'Select Doctor' }}</option>
                                <?php foreach($doctor_master as $key=>$val){
                               ?>
                                <option value="<?= $key ?>">
                                    <?= $val ?>
                                </option>
                                <?php
                           }
                           ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm">
                        <div class="mate-input-box">
                            <label for="">From Date</label>
                            <div class="clearfix"></div>
                            <input type="text" name="from_date" autocomplete="off" value="<?= date('Mon-d-Y') ?>"
                                class="form-control datepicker" id="from_date" placeholder="From Date">
                        </div>
                    </div>

                    <div class="col-md-1 padding_sm">
                        <div class="mate-input-box">
                            <label for="">To Date</label>
                            <div class="clearfix"></div>
                            <input type="text" name="to_date" autocomplete="off" value="<?= date('Mon-d-Y') ?>"
                                class="form-control datepicker" id="to_date" placeholder="To Date">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Old UHID</label>
                            <input type="text" name="old_uhid_search" autocomplete="off" value="" class="form-control"
                                id="old_uhid_search" placeholder="Old UHID">
                            <input type="hidden" id="old_uhid_search_hidden" value="" name="old_uhid_search_hidden">
                            <div id="old_uhid_searchCodeAjaxDiv" class="ajaxSearchBox"
                                style="min-height:300px; width: 450px !important;"></div>
                        </div>
                    </div>
                    <div class="col-md-1">

                        <div class="col-md-12 padding_sm ">

                            <div class="clearfix"></div>
                            <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg"
                                onclick="searchBill()"><i id="searchdataspin" class="fa fa-search"></i>
                                Search
                            </button>
                        </div>
                        <div class="col-md-12 padding_sm ">

                            <div class="clearfix"></div>
                            <a onclick="refresh();" class="btn btn-block btn-warning"><i class="fa fa-refresh"></i>
                                Refresh</a>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="col-md-12 padding_sm pull-right" style="margin-top: 2px;">
                            <button type="button" class="btn light_purple_bg btn-block disabled"
                                onclick="fetchPrintDetails();" name="print_results" id="print_results">
                                <i class="fa fa-print" aria-hidden="true" id="print_spin"></i>
                                Print
                            </button>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="checkbox checkbox-success inline">
                                <input id="is_checked" type="checkbox" name="is_checked">
                                <label class="text-blue" for="is_checked">
                                    All Patients
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div class="box-body" style="padding:10px">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs sm_nav">
                <li class="active"><a data-toggle="tab" aria-expanded="true" data-attr-id="instore" href="#instore">In
                        store <span id="instorecount"></span></a></li>
                <li><a data-toggle="tab" aria-expanded="false" data-attr-id="outstore" href="#outstore">Receive
                        Dispached <span id="receivedispatchcount"></span></a></li>
                <li><a data-toggle="tab" aria-expanded="false" data-attr-id='todays_received' href="#todays_received">
                        Received <span id="receivedcount"></span></a>
                </li>
                <li class="pull-right" style="margin-top: 10px; margin-right: 18px !important;">
                    <button style="display: none" onclick="receiveDispathAllRecords()" id="receiveDispathAllRecordsBtn"
                        class="btn btn-success" type="button">
                        <i id="receiveDispathAllRecordsSpin" class="fa fa-check"></i><span id="recevie_dispatch_text"></span></button>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="instore">
                    <div class="col-md-12">
                        <div class="col-md-1" style="padding:0px;">
                            <i class="fa fa-square" style="color:lightgreen;"></i> Printed
                        </div>
                        <div class="col-md-1" style="padding:0px;">
                            <i class="fa fa-square" style="color:#a2dddf;"></i> New Reg
                        </div>
                        <div class="col-md-1" style="padding:0px;">
                            <i class="fa fa-square" style="color:#ff0;"></i> New Born
                        </div>
                        <div class="col-md-2" style="padding:0px;">
                            <i class="fa fa-square" style="color:#21b146;"></i> More than 1 visit
                        </div>
                    </div>
                    @include('Master::masters.mrd_track_instore')
                </div>
                <div class="tab-pane fade" id="outstore">
                    @include('Master::masters.mrd_track_outstore')
                </div>
                <div class="tab-pane fade" id="todays_received">
                    @include('Master::masters.mrd_track_today_received')
                </div>
            </div>
        </div>
    </div>



</div>
<div id="print_hidden" style="display:none">

</div>

<div id="offsetDetails" style="display: none"></div>
<div class="modal fade" id="dispatchModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 50%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
            </div>
            <div class="modal-body" style="min-height:240px;height:240px">
                <div class="row padding_sm">
                    <div class='theadscroll' style="height: 32vh;" id="dispatch_modal">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="receive_all_document_btn" type="button"><i
                        class="fa fa-download"></i> Receive All</button>
            </div>
        </div>
    </div>
</div>
{{-- -------------------today visit history --}}
<div class="modal fade" id="todayVisitHistory" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 70%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <span style="font-size: 14px;">Visit History</span>
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
            </div>
            <div class="modal-body" style="min-height:240px;height:240px">
                <div class="row padding_sm">
                    <div class='theadscroll' style="height: 350px;" id="todayVisitHistory_data">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/mrd_track.js') }}"></script>

<script type="text/javascript"></script>

@endsection
