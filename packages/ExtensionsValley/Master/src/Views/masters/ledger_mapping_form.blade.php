@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.item.list_gl_mapping')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Ledger Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="ledger"
                                    value="{{ $searchFields['ledger'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Mapping Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('type', array(
                                "0" => " Select Type",
                                "department" => " Department",
                                "gst" => " GST",
                                "insurance" => " Insurance",
                                "item" => " Item",
                                "sub_department" => " Sub Department",
                                "supplier" => " Supplier",
                                "surgery" => " Surgery"), $searchFields['type'] ?? '',
                                ['class'=>"form-control", 'id'=>"type"]) !!}
                            </div>
                        </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Mapping Value</label>
                                <div class="clearfix"></div>
                                <input type="text" value="{{ $searchFields['value'] ?? '' }}"  name="value" autocomplete="off" id="value"
                                       class="form-control value"
                                       placeholder="Mapping Value" onkeyup="searchMapValue(this.id, event)">
                            <div class="ajaxSearchBox" id="value_div" style="z-index: 9999 !important;margin-top:45px !important"></div>
                            <input type="hidden" value="{{ $searchFields['value_id'] ?? '' }}" name="value_id" id="value_id">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Ledger Name</th>
                                    <th>Mapping Type</th>
                                    <th>Mapping Value</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($list) > 0)
                                    @foreach ($list as $list)
                                        <tr style="cursor: pointer;" onclick="editLoadData(this,
                                            '{{$list->id}}',
                                            '{{$list->ledger_id}}',
                                            '{{$list->ledger_name}}',
                                            '{{$list->mapping_type}}',
                                            '{{$list->mapping_value}}',
                                            '{{$list->map_name}}',
                                            '{{$list->status}}');" >
                                            <td class="ledger_id common_td_rules">{{ $list->ledger_name }}</td>
                                            <td class="mapping_type common_td_rules">{{ $list->map_type }}</td>
                                            <td class="mapping_value common_td_rules">{{ $list->map_name }}</td>
                                            <td class="status common_td_rules">{{ $list->status_name }}</td>

                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="tax_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.item.save_gl_mapping')}}" method="POST" id="mapForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="gl_mapping_id" id="gl_mapping_id" value="0">
                        <div class="col-md-12 padding_sm">
                           <div class="mate-input-box">
                            <label for="">Ledger Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" name="ledger_name" autocomplete="off" id="ledger_name"
                                       class="form-control fav_grp"
                                       placeholder="Ledger Name" required onkeyup="searchLedger(this.id, event)">
                                       <div class="ajaxSearchBox" id="ledger_div" style="z-index: 9999 !important"></div>
                                       <input type="hidden" name="ledger_id" id="ledger_id">
                            <span class="error_red">{{ $errors->first('ledger_name') }}</span>
                          </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="form-group {{ $errors->has('mapping_type') ? 'has-error' : '' }} control-required">
                                <div class="mate-input-box">
                            <label for="">Mapping Type<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                {!! Form::select('mapping_type', array(
                                "0" => " Select Type",
                                "department" => " Department",
                                "gst" => " GST",
                                "insurance" => " Insurance",
                                "item" => " Item",
                                "sub_department" => " Sub Department",
                                "supplier" => " Supplier",
                                "surgery" => " Surgery"), '',
                                ['class'=>"form-control", 'id'=>"mapping_type",
                                'required'=>"required"]) !!}
                            <span class="error_red">{{ $errors->first('mapping_type') }}</span>
                        </div>
                        </div>
                        </div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Mapping Value<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" required name="mapping_value" autocomplete="off" id="mapping_value"
                                       class="form-control mapping_value"
                                       placeholder="Mapping Value" onkeyup="searchMappingValue(this.id, event)">
                            <div class="ajaxSearchBox" id="mapping_value_div" style="z-index: 9999 !important"></div>
                            <input type="hidden" name="mapping_value_id" id="mapping_value_id">
                            <span class="error_red">{{ $errors->first('mapping_value') }}</span>
                          </div>
                        </div>


                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">{{__('Active')}}</option>
                                    <option value="0">{{__('Inactive')}}</option>
                                </select>
                                <span class="error_red">{{ $errors->first('status') }}</span>
                                </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        $(document).on('change', '#mapping_type', function(e) {
                var mapping_type = $("#mapping_type").val();
                $('#mapping_value').val('');
                $('#mapping_value_id').val('');
        });
        $(document).on('change', '#type', function(e) {
                var mapping_type = $("#type").val();
                $('#value').val('');
                $('#value_id').val('');
        });
$('#mapForm').on('submit', function(e){
        var len = $('#ledger_id').val().length;
        var len1 = $('#mapping_value_id').val().length;
        if (len < 1) {
            alert("Please select Ledger from List");return false;
        }
        if (len1 < 1) {
            alert("Please select Mapping value from List");return false;
        }
    });
    });

    function fillLedgerDetails(e, id, name) {
                        $('#ledger_name').val(name);
                        $('#ledger_id').val(id);
                        $('#ledger_div').hide();
    }
    function searchLedger(id, event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var ajax_div = 'ledger_div';

        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            var group = $('#' + id).val();
            if (group == "") {
                $("#" + ajax_div).html("");
            } else {
                $.ajax({
                    type: "GET",
                    url: "",
                    data: 'ledger_name=' + group,
                    beforeSend: function () {
                        $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#" + ajax_div).html(html).show();
                        $("#" + ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
        }
    }

    function fillMappingValues(e, id, name) {
                        $('#mapping_value').val(name);
                        $('#mapping_value_id').val(id);
                        $('#mapping_value_div').hide();
    }
    function searchMappingValue(id, event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var ajax_div = 'mapping_value_div';

        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            var mapping_value = $('#' + id).val();
            var mapping_type = $('#mapping_type').val();
            if(mapping_type == '0'){
                alert("Please Select Valid Mapping Type! ");
                return false;
            }
            if (mapping_value == "") {
                $("#" + ajax_div).html("");
            } else {
                $.ajax({
                    type: "GET",
                    url: "",
                    data: 'mapping_value=' + mapping_value + '&mapping_type=' + mapping_type,
                    beforeSend: function () {
                        $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#" + ajax_div).html(html).show();
                        $("#" + ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
        }
    }

    function fillMapValues(e, id, name) {
                        $('#value').val(name);
                        $('#value_id').val(id);
                        $('#value_div').hide();
    }
    function searchMapValue(id, event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var ajax_div = 'value_div';

        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            var mapping_value = $('#' + id).val();
            var mapping_type = $('#type').val();
            if(mapping_type == '0'){
                alert("Please Select Valid Mapping Type! ");
                return false;
            }
            if (mapping_value == "") {
                $("#" + ajax_div).html("");
            } else {
                $.ajax({
                    type: "GET",
                    url: "",
                    data: 'value=' + mapping_value + '&type=' + mapping_type,
                    beforeSend: function () {
                        $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#" + ajax_div).html(html).show();
                        $("#" + ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
        }
    }

    function editLoadData(obj,id,ledger_id,ledger_name,mapping_type,mapping_value,map_name,status){
        $('#gl_mapping_id').val(id);
        $('#ledger_id').val(ledger_id);
        $('#ledger_name').val(ledger_name);
        $('#mapping_type').val(mapping_type);
        $('#mapping_value_id').val(mapping_value);
        $('#mapping_value').val(map_name);
        $('#status').val(status);
        $('#mapForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
         $('#mapForm')[0].reset();
    }
@include('Purchase::messagetemplate')
</script>

@endsection
