<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table theadfix_wrapper table_sm table-condensed table-col-bordered center_align floatThead-table" style="border-collapse: collapse; border: 0px none rgb(128, 128, 128); display: table; margin: 0px; table-layout: fixed; width: 100%;">
        <style>
            table tr:hover td {
                text-overflow: initial;
                white-space: normal;
            }
        
        </style>
        <thead>
     
            <tr class="table_header_bg">
                {{-- <th>Sl.No.</th> --}}
                <th>Patient Name</th>
                <th>UHID</th>
                <th>Patient Phone</th>
                <th>Bill No</th>
                <th>Bill Tag</th>
                <th>Bill Date</th>
                <th>Visit Status</th>
                <th>Payment Type</th>
                <th>Bill Amount</th>
                <th>Tax Amount</th>
                <th>Net Amount</th>
                <th>Cancel Date</th>
                <th>Requested By</th>
               
                <th width = "11%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($res) > 0)
                @foreach ($res as $value)
                    <tr style="cursor: pointer;">
                        {{-- <td class = "td_common_numeric_rules">{{($res->currentPage() - 1) * $res->perPage() + $loop->iteration}}</td> --}}
                        <td class ="common_td_rules">{{ $value->patient_name }}</td>
                        <td class ="common_td_rules">{{ $value->uhid }}</td>
                        <td class ="common_td_rules">{{ $value->patient_phone }}</td>
                        <td class ="common_td_rules">{{ $value->bill_no}}</td>
                        <td class ="common_td_rules">{{ $value->bill_tag}}</td>
                        <td class ="td_common_numeric_rules">{{ date('M-d-Y',strtotime($value->bill_date))}}</td>
                        <td class ="common_td_rules">{{ $value->visit_status}}</td>
                        {{-- <td class ="common_td_rules">{{ $value->consulting_doctor_name}}</td> --}}
                        <td class ="common_td_rules">{{ $value->payment_type}}</td>
                        <td class ="td_common_numeric_rules">{{ $value->bill_amount}}</td>
                        <td class ="td_common_numeric_rules">{{ $value->tax_amount}}</td>
                        <td class ="td_common_numeric_rules">{{ $value->net_amount_wo_roundoff}}</td>
                        <td class ="td_common_numeric_rules">{{ date('M-d-Y',strtotime($value->cancelled_date))}}</td>
                        <td class ="td_common_numeric_rules">{{ $value->user_name}}</td>
                      
                        <td>
                            <button type="button" id="approved" class="btn btn-sm btn-primary approved" onclick="cancel_approved(this,'{{ $value->id}}');" ><i class="fa fa-check"></i> Approve</button>
                            <button type="button" id="rejected" class="btn btn-sm btn-warning rejected" onclick="rejected(this,'{{ $value->id}}');" ><i class="fa fa-times"></i> Reject</button>
                        </td>
                       
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="14">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {{-- {!! $page_links !!} --}}
    </ul>
</div>
