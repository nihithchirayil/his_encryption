<style>
    table tr:hover td{
        text-overflow: initial;
        white-space: normal;
    }
</style>
<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="7%">Room Type Name</th>
                <th width="17%">Service</th>
                <th width="11%">ICU</th>
                <th width="11%">Baby Bed</th>
                <th width="11%">Day Care Bed</th>
                <th width="9%">Required Last Day</th>
                <th width="11%">Room Type Based On</th>
                <th width="7%">Order Level</th>
                <th width="7%">Status</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($room_type_master) > 0)
                @foreach ($room_type_master as $room_type)
                    <tr style="cursor: pointer;"  data-id="{{ $room_type->id }}" data-room_type_name="{{ $room_type->room_type_name }}" data-icu="{{ $room_type->icu}}" 
                        data-baby_bed="{{ $room_type->baby_bed }}"   data-day_care_bed="{{$room_type->day_care_bed }}" data-required_last_day_rent="{{ $room_type->required_last_day_rent }}"
                         data-service="{{ $room_type->code }}"  data-order_level="{{ $room_type->order_level }}" 
                         data-rent="{{ $room_type->hourly_rent }}"  data-status="{{ $room_type->status_name }}">
                        <td class = "td_common_numeric_rules">{{($room_type_master->currentPage() - 1) * $room_type_master->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $room_type->room_type_name }}</td>
                        <td class ="common_td_rules">{{ $room_type->service_desc}}</td>
                        <td class ="common_td_rules">{{ $room_type->is_icu}}</td>
                        <td class ="common_td_rules">{{ $room_type->is_dummy_baby_bed}}</td>
                        <td class ="common_td_rules">{{ $room_type->is_day_care_bed}}</td>
                        <td class ="common_td_rules">{{ $room_type->is_required_last_day_rent}}</td>
                        <td class ="common_td_rules">{{ $room_type->room_type_based_on}}</td>
                        <td class ="td_common_numeric_rules">{{ $room_type->order_level}}</td>
                        <td class ="common_td_rules">{{ $room_type->status}}</td>
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
