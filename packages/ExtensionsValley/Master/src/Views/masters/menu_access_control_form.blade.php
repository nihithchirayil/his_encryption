@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style> 
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.list_menu_access')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Menu</label>
                                <div class="clearfix"></div>
                                {!! Form::select('menu_name', array("-1"=> " Select Menu") + $menu_items->toArray(), $searchFields['menu_name'] ?? '', [
                                            'class'       => 'form-control'
                                        ]) !!}
                                    </div>
                            </div>
    
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Group</label>
                                <div class="clearfix"></div>
                                {!! Form::select('group', array("-1"=> " Select Group") + $group->toArray(), $searchFields['group'] ?? '', [
                                            'class'       => 'form-control',
                                        ]) !!}
                                    </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>  Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Menu Name</th>
                                    <th>Group Name</th>
                                    <th>Worksheet</th>
                                    <th>Submit</th>
                                    <th>Verify</th>
                                    <th>Approve</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($menu_list) > 0)
                                    @foreach ($menu_list as $menu)
                                        <tr style="cursor: pointer;" onclick="editLoadData(this,
                                            '{{$menu->id}}',
                                            '{{$menu->acl_key}}',
                                            '{{$menu->group_id}}', 
                                            '{{$menu->worksheet}}', 
                                            '{{$menu->submit}}',
                                            '{{$menu->verify}}',
                                            '{{$menu->approve}}',
                                            '{{$menu->status}}');" >
                                            <td class="acl_key common_td_rules">{{ $menu->menu_name }}</td>
                                            <td class="group_id common_td_rules">{{ $menu->group_name }}</td>
                                            <td class="worksheet common_td_rules">{{ $menu->worksheet_status }}</td>
                                            <td class="submit common_td_rules">{{ $menu->submit_status }}</td>
                                            <td class="verify common_td_rules">{{ $menu->verify_status }}</td>
                                            <td class="approve common_td_rules">{{ $menu->approve_status }}</td>
                                            <td class="status common_td_rules">{{ $menu->status_name }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="vendor_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.save_menu_access')}}" method="POST" id="menuaccessForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="menu_access_id" id="menu_access_id" value="0">
                        <div class="col-md-12 padding_sm">
                           <div class="mate-input-box">
                            <label for="">Menu Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            {!! Form::select('acl_key', array("-1"=> " Select Menu") + $menu_items->toArray(),null, [
                            'class'       => 'form-control acl_key',
                            'id'    => 'acl_key'
                        ]) !!}
                            <span class="error_red">{{ $errors->first('acl_key') }}</span>
                          </div> 
                        </div>
                        <div class="col-md-12 padding_sm">
                           <div class="mate-input-box">
                            <label for="">Group Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            {!! Form::select('group_id', array("-1"=> " Select Group") + $group->toArray(), '',
                                ['class'=>"form-control", 'id'=>"group_id"]) !!}
                            <span class="error_red">{{ $errors->first('group_id') }}</span>
                          </div> 
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Worksheet Status</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="worksheet" id="worksheet" > 
                            </div></div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Submit Status</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="submit" id="submit" > 
                            </div></div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Verify Status</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="verify" id="verify" > 
                            </div></div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Approve Status</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="approve" id="approve" > 
                            </div></div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Status<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <select class="form-control" name="status" id="status">
                                <option value="1">{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                            <span class="error_red">{{ $errors->first('status') }}</span>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

    });

    function editLoadData(obj,id,acl_key,group_id,worksheet,submit,verify,approve,status){
        $('#menu_access_id').val(id);
        $('#acl_key').prop('readonly',true).val(acl_key);
        $('#group_id').val(group_id);
        $('#status').val(status);
        $('#menuaccessForm').attr('action', '{{$updateUrl}}');
        if (worksheet == 1){
            $("#worksheet").prop("checked", true);
        } else{
            $("#worksheet").prop("checked", false);
        }
        if (submit == 1){
            $("#submit").prop("checked", true);
        } else{
            $("#submit").prop("checked", false);
        }
        if (verify == 1){
            $("#verify").prop("checked", true);
        } else{
            $("#verify").prop("checked", false);
        }
        if (approve == 1){
            $("#approve").prop("checked", true);
        } else{
            $("#approve").prop("checked", false);
        }
    }
    function resetFunction(){
         $('#menuaccessForm')[0].reset();
    }

</script>
    
@endsection
