

@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

<style>
.fixed_row{
    background: #a6c3ad; 
    height: 25px; 
    color: white;
}
.scroll_row {
    float: left;
    height: 285px;
    overflow-x: hidden;
    overflow-y: auto;
    width: 100%;
}
ul.menu_container {
    font-size: 13px;
    margin: 0;
    padding: 0;
}

ul li.mainmenu{
    list-style-type: none;
    padding: 5px 0;
    float: left;
    width: 100%;
    margin: 1px 0;
    background: #f5f5f5;
}
ul.subheading {
    float: left;
    margin: 10px 0 0 0;
    padding: 0 0 0 30px;
    width: 100%;
    list-
}
ul.subheading li {
    /* background: #e0e0e0; */
    height: 30px;
    border: 1px solid #fffcfc;
    list-style-type: none;
}
.minusbtn, .plusbtn {
    cursor: pointer;
    font-size: 16px;
    margin: 2px 0 0;
}
.headerbtn{
    border: 1px solid green;
    min-width: 90px;
    color: green;
}
.headerbtn:hover{
    background-color: green;
    color: white;
    border: 1px solid white;
}
</style>
@endsection

@section('content-area')
<div class="right_col">
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="c_token" value="{{ csrf_token() }}">
<div class="col-md-12" style="margin:0px">
<div class="container-fluid" style="padding: 15px;">
<section class="content" style="clear: both;">

	<div class="row" style="margin-top:15px;">
		<div class="col-md-12">
			<div class="panel panel-primary" style="border:1px solid #eeeedd">
				<div class="panel-heading" style="background: #36A693; color:#FFF; border:none; border-bottom:1px solid #EFF1F0;">
					<h3 class="panel-title">{{$title}}</h3>
					<div class="pull-right" style="margin-top:-25px;">
						<span id="ajaxMsg" class="alert-success btn-info btn-sm" style="font-size: 14px; display: none; font-weight:900;">
						</span>

					</div>
				</div>

                <div class="row-fluid">
                    <div class="col-xs-12 no-padding">
                        <div class="table-responsive">
                            <div class="mainmenu_container">
                                <div class="fixed_row ">
                                    <div class="col-xs-1 w_4">#</div>
                                    <div class="col-xs-4" style="padding: 3px 0px;">Menu</div>
                                    <div class="col-xs-1 col-xs-offset-4" style="padding-left: 10px;"><button type="button" class="btn btn-sm btn-block bg-default headerbtn moveMenuBtn"><i class="fa fa-cut"></i> Move</button></div>
                                    <div class="col-xs-1" style="padding-left: 10px;"><button type="button" class="btn btn-sm btn-block bg-default headerbtn copyMenuBtn"><i class="fa fa-copy"></i> Copy</button></div>
                                </div>
                                <div class="scroll_row" id="aclListBody" style='height:370px;'>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</section>
    </div>
    </div>
    </div>

    <div id="change_menu_parent_modal" class="modal fade " role="dialog" style="z-index:9999">
        <div class="modal-dialog" style="width:40%">

            <div class="modal-content">
                <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close close_white">&times;</button>
                    <h4 class="modal-title">Move/Copy Menu</h4>
                </div>
                <div class="modal-body" id="change_menu_parent_list_data" style="min-height:240px;">
                    <div class="row-fluid">
                        <div class="col-md-12 no-padding" style="margin-top:15px; ">
                            <div class="col-md-12 padding_sm" >
                                <div class="mate-input-box">
                                    <label for="">Parent Menu</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('parent_id', array("0"=> "Select Parent Menu") + $menu_array, '', ['class' => 'form-control parent_id_select select2']) !!}
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm copy_menu_permission" style="margin-top:20px; display:none;">
                                <div class="col-md-6 padding_sm">
                                    <label for=""> Copy Menu Permissions </label>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <input type="checkbox" class="access_control_status" checked="" data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                                </div> 
                            </div>
                            <div class="col-md-12 padding_sm" style="margin-top:20px; ">
                                <div class="col-md-6 padding_sm">
                                    <label for=""> Make as Parent Menu </label>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <input type="checkbox" class="parent_menu_status" checked="" data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                                </div> 
                            </div> 
                        </div>
                        
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success saveMenuChangesBtn" ><i class="fa fa-save"></i> Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>

        </div>
    </div>

	@endsection 
    

    @section('javascript_extra')
    {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
    <script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/menu_detail_view.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>


@endsection

