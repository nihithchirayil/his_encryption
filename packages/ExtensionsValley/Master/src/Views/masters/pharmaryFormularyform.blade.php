@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
{{-- 
<style>
  .liHover{
     background: #48705f;
  }
  .box_border {
      padding-top: 25px;
      padding-bottom: 10px;
      border: 1px solid #ccc;
      background: #f3f3f3;
  }
  .tinymce-content p {
      padding: 0;
      margin: 2px 0;
  }
  .content_add:hover{
   color: red;
   background-color: yellow;
  }
  .content_remove:hover{
   color: red;
   background-color: yellow;
  }
  .modal-header{
      background-color:orange;
  }

    #MTable > tbody > tr > td{
        word-wrap:break-word
      }
      table#posts {
        word-wrap: break-word;
    }
    .page{
        padding: 65px;
        min-height: 1134px;
        width: 812px;
    }



    .avlailable_check{
        margin-top: 2px !important;
        margin-right: 5px !important;
    }
    .order_by_select{
        float:right !important;
    }
    .filters_title {
        margin-top:0px !important;
        margin-bottom:0px !important;
        margin-left:2px !important;
    }

    .filter_label{
        font-weight: 50 !important;
        font-size: 11px !important;
        color: #266c94 !important;
    }

    .col-xs-2{
        padding-right:7px !important;
        padding-left:7px !important;
    }

    .btn_add_to_order_by{
        float: right;
        width: 18px;
        height: 16px;
        padding: 0px;
    }
    .ajaxSearchBox {
        display:none;
        
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 350px;
        margin: 3px 0px 0px 0px;
        overflow-y: auto;
        width: 250px;
        z-index: 599;
        position:relative;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);
        width: 100%;

    }
    .ajaxSearchBox>li{
        padding:2px 2px 5px 5px;
        font-size: 10px !important;
        font-weight: 400 !important;
        font-family: "sans-serif";
        border-bottom: 1px solid grey;
    }

    .liHover{
        background: #4c6456 !important;
        color:white;
        padding:2px 2px 5px 5px;
        font-size: 10px !important;
        font-weight: 400;
        font-family: "sans-serif";
        border-bottom: 1px solid white;
    }
    .bg-blue-active {
        background-color: #E1D5F4  !important;
    }
    .filter_label {
        color: #1c3525 !important;
    }
    .bg-primary {
        color: black;
        background-color: #768a7e;
    }
    .box.box-primary {
        border-top:1px solid #E1D5F4 ;
    }
    .btn-info{
        color: black !important;
        background-color: #E1D5F4  !important;
        border-color: #768a7e !important;
        background-image: linear-gradient(to bottom,#bedecb 0,#2aabd2 100%);
    }
    .liHover {
        background: #8e948f;
        color: white;
    }
    .filter_label {
       margin-bottom: 0px;
    }
    .btn-group {
        width:100% !important;
    }
    .col-xs-12{
        margin-bottom:8px;
    }
    .col-xs-6{
        margin-bottom:8px;
    }
    .filter_label {
        color: #294236 !important;
        font-weight: 600 !important;
    }
    tr:hover{
        font-size: 12.5px;
        color:blue !important;
        background-color: #f2f3cc !important;
    }
    @media print{
        .headerclass{
            background:#999;
        }
        hr {
            display: block;
            height: 1px;
            background: transparent;
            width: 100%;
            border: none;
            border-top: solid 1px #aaa;
        }
    }

</style> --}}
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
<input type="hidden" value='<?= $route_data ?>' id="route_value">

<div class="right_col" >
<div class="row" style="text-align: right; padding-right: 66px; font-size: 12px; font-weight: bold;"> {{$title}}
<div class="clearfix"></div></div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                    <div class="row padding_sm">
                        <div class="col-md-12 padding_sm">
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="filter_label">Brand Name</label>
                                        <input class="form-control hidden_search" value="" placeholder="Brand Name" autocomplete="off" type="text" id="brand_name" name="brand_name">
                                        <div id="brand_nameAjaxDiv" class="ajaxSearchBox"></div>
                                        <div class="clearfix"></div>
                                        <input class="filters" value="0" type="hidden" name="brand_name_hidden" id="brand_name_hidden">
                                    </div>
                                </div>
                                {{-- <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="filter_label">Chemical Name</label>
                                        <input class="form-control hidden_search" value="" placeholder="Chemical Name" autocomplete="off" type="text" id="chemical_name" name="chemical_name">
                                        <div id="chemical_nameAjaxDiv" class="ajaxSearchBox"></div>
                                        <div class="clearfix"></div>
                                        <input class="filters" value="0" type="hidden" name="chemical_name_hidden" id="chemical_name_hidden">
                                    </div>
                                </div> --}}
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="filter_label">Generic Name</label>
                                        <input class="form-control hidden_search" value="" placeholder="Generic Name" autocomplete="off" type="text" id="generic_name" name="generic_name">
                                        <div id="generic_nameAjaxDiv" class="ajaxSearchBox"></div>
                                        <div class="clearfix"></div>
                                        <input class="filters" value="0" type="hidden" name="generic_name_hidden" id="generic_name_hidden">
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="filter_label">Therapeutic Category</label>
                                        <input class="form-control hidden_search" value="" placeholder="Therapeutic Category" autocomplete="off" type="text" id="therapeutic_category" name="therapeutic_category">
                                        <div id="therapeutic_categoryAjaxDiv" class="ajaxSearchBox"></div>
                                        <div class="clearfix"></div>
                                        <input class="filters" value="0" type="hidden" name="therapeutic_category_hidden" id="therapeutic_category_hidden">
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="filter_label">Therapeutic Subcategory</label>
                                        <input class="form-control hidden_search" value="" placeholder="Therapeutic Subcategory" autocomplete="off" type="text" id="therapeutic_subcategory" name="therapeutic_subcategory">
                                        <div id="therapeutic_subcategoryAjaxDiv" class="ajaxSearchBox"></div>
                                        <div class="clearfix"></div>
                                        <input class="filters" value="0" type="hidden" name="therapeutic_subcategory_hidden" id="therapeutic_subcategory_hidden">
                                    </div>
                                </div>

                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="filter_label">Manufacturer Name</label>
                                        <input class="form-control hidden_search" value="" placeholder="Manufacturer Name" autocomplete="off" type="text" id="manufacturer_name" name="manufacturer_name">
                                        <div id="manufacturer_nameAjaxDiv" class="ajaxSearchBox"></div>
                                        <div class="clearfix"></div>
                                        <input class="filters" value="0" type="hidden" name="manufacturer_name_hidden" id="manufacturer_name_hidden">
                                    </div>
                                </div>
                                

                                
                            </div>
                            <div class="col-md-12" style="text-align:right; margin-top:10px; padding: 0px;">
                                <div class="clearfix"></div>

                                <button type="reset" class="btn light_purple_bg" onclick="search_clear();"  name="clear_results">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                    Reset
                                </button>
                                
                                <a class="btn light_purple_bg" onclick="printReportData();"  name="print_results" id='printpharmacydatabtn'>
                                    <i  class="fa fa-print" aria-hidden="true"></i>
                                    Print
                                </a>
                                <a class="btn light_purple_bg" onclick="generate_csv();"  name="csv_results" id='excelpharmacydatabtn'>
                                    <i id='excelpharmacydataspin' class="fa fa-file-excel-o" aria-hidden="true"></i>
                                    Excel
                                </a>
                                
                                <a class="btn light_purple_bg" onclick="getReportData();"  name="search_results" id="searchpharmacydatabtn">
                                    <i id='searchpharmacydataspin' class="fa fa-search" aria-hidden="true"></i>
                                    Search
                                </a>

                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" id="ResultsViewArea">
                
            </div>
            <div class="box no-border no-margin" style="display: none;" id="ResultsViewExcel">
                
            </div>
           
        </div>
    </div>
</div>



















    

<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:rgb(91 110 91);">
          <h4 class="modal-title" style="color: white;">Print Configuration</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="height:120px;">
            <div class="col-md-12">
                <span style="margin-right: 5%;">Print Mode :</span>
                <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
            </div>
            <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                Include Hospital Header
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <button onclick="generate_csv(3)" class="btn bg-primary pull-right" style="color:white" id="printItemFormulary">
                    <i id="printItemFormularySpin" class="fa fa-print" aria-hidden="true"></i> Print
                </button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>

<script src="{{asset("packages/extensionsvalley/emr/js/pharmacyformularyform.js")}}"></script>

<script src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js') }}"></script>

<script type="text/javascript">

</scri>
@endsection