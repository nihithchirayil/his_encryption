@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    @stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
    <link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
@endsection
@section('content-area')
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.listSubCategory')}}" id="listingSearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Sub-Category Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="subcategory_code" id="subcategory_code_search"
                                    value="{{ $searchFields['subcategory_code'] ?? '' }}">
                                </div>
                            </div>

                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Sub-Category Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="subcategory_name" id="subcategory_name_search"
                                    value="{{ $searchFields['subcategory_name'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg subcategory_search"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Category Name</th>
                                    <th>Sub-Category Code</th>
                                    <th>Sub-Category Name</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($subcategory_list) > 0)
                                    @foreach ($subcategory_list as $subcategory)
                                        <tr style="cursor: pointer;" onclick="subcategoryEditLoadData(this,'{{$subcategory->subcategory_id}}','{{$subcategory->subcategory_id}}',
                                                '{{$subcategory->category_id}}', '{{$subcategory->subcategory_code}}',
                                                '{{$subcategory->subcategory_name}}',{{$subcategory->status}})">
                                            <td class="category_name common_td_rules">{{ $subcategory->category_name }}</td>
                                            <td class="subcategory_code common_td_rules">{{ $subcategory->subcategory_code }}</td>
                                            <td class="subcategory_name common_td_rules">{{ $subcategory->subcategory_name }}</td>
                                            <td class="status common_td_rules">{{ $subcategory->status_name  }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="re-records-found">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    {{-- <form  id="subcategoryForm"> --}}
                        {{-- {!! Form::token() !!} --}}
                        <input type="hidden" name="subcategory_id" id="subcategory_id" value="0">
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Select Category</label>
                                <div class="clearfix"></div>
                                {!! Form::select('category_id', $category, '',
                                ['class'=>"form-control", 'id'=>"category_id"]) !!}
                            </div></div>
                         <div class="col-md-12 padding_sm" id="subcat_cod_div">
                            <div class="mate-input-box">
                            <label for="">Sub-Category Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="subcategory_code" id="subcategory_code">
                            <div class="" id="subcategory_code_status">
                            </div>
                        </div></div>


                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Sub-Category Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="subcategory_name" id="subcategory_name">
                        </div></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Status</label>
                            <div class="clearfix"></div>
                            <select class="form-control" name="status" id="status">
                                <option value="1">{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                        </div></div>
                        <input type="hidden" value="" id="subcat_id">
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                           <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg savesubcat"  onclick="savesubcat()"><i class="fa fa-save"></i> Save</button>
                        </div>
                       
                    {{-- </form> --}}
                    
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function () {
        $('#subcat_cod_div').hide();

        $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });
       
        $('#subcategory_code').keyup(function () {

            var subcategory_code_str = $(this).val();
            subcategory_code_str = subcategory_code_str.replace(/[^a-zA-Z0-9]/g, '-');

            subcategory_code_str = subcategory_code_str.replace(/\-+/g, '');

            subcategory_code_str = subcategory_code_str.toUpperCase();

            var subcategory_code = subcategory_code_str.trim();

            $('#subcategory_code').val(subcategory_code);

            if (subcategory_code != '') {
                var url = '{{route('extensionsvalley.master.checkCategoryCode')}}';
                $.ajax({
                    method: "GET",
                    url: url,
                    data: 'subcategory_code=' + subcategory_code,
                    success: function (data) {
                        console.log(data);
                        if (data == 0) {
                            $('#subcategory_code').css('color', 'green');
                            $('#subcategory_code_status').html(
                                '<button tabindex="-1" type="button" title="Available" class="btn btn-success btn-sm"><i class="fa fa-check fa-xs" aria-hidden="true"></i></button>'
                            );

                        } else {
                            $('#subcategory_code').css('color', 'red');
                            $('#subcategory_code_status').html(
                                '<button tabindex="-1" type="button" title="Not available" class="btn btn-danger btn-sm"><i class="fa fa-ban fa-xs" aria-hidden="true"></i></button>'
                            );
                        }
                    }

                });

            }


        });

    });
    function savesubcat(){
        base_url = $('#base_url').val();
        if($("#subcategory_name").val() == ''){
            Command: toastr["error"]("Please enter Subcategory Name");
            return;
        }
        var url = base_url + "/master/saveSubCategory";
        var data = {};
        
        data.subcat_id = $("#subcat_id").val();
        data.sub_cat_name = $("#subcategory_name").val();
        data.cat_code_add= $("#category_id").val();
        data.status = $("#status").val();
       
       
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $(".savesubcat").find('i').removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');
                $('.savesubcat').attr('disabled', true);

            },
            success: function (data) {  
                
                    $(".savesubcat").find('i').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-save');
                    $('.savesubcat').attr('disabled', false);
                    Command: toastr["success"]("success");
                    $(".subcategory_search").trigger("click");          
                    resetFunction();
                
            },
            complete: function () {

            }
        });
    }
    function subcategoryEditLoadData(obj,id,subcategory_id,category_id,subcategory_code,subcategory_name,status){
        $('#subcat_cod_div').show();
        var edit_icon_obj = $(obj);
        $('#subcat_id').val(id);
        $('#subcategory_id').val(subcategory_id);
        $('#category_id').val(category_id);
        $('#subcategory_code').prop('readonly',true).val(subcategory_code);
        $('#subcategory_name').val(subcategory_name);
        $('#status').val(status);
        // $('#subcategoryForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
        $('#subcat_cod_div').hide();
        $('#subcat_id').val('');
        $('#subcategory_id').val('');
        $("#category_id").val($("#category_id option:first").val());
        $("#status").val($("#status option:first").val());
        $('#subcategory_name').val('');
         $('#subcategoryForm')[0].reset();
    }
@include('Purchase::messagetemplate')
</script>

@endsection
