<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="20%">Name</th>
                <th width="7%">Code</th>
                <th width="7%">Age From</th>
                <th width="7%">Age To</th>
                <th width="9%">Status</th>
                <th width="9%">Gender</th>
                <th style = "width:20%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($vulnerables) > 0)
                @foreach ($vulnerables as $vulnerable)
                    <tr style="cursor: pointer;"  data-id="{{ $vulnerable->id }}" data-vul-name="{{ $vulnerable->name }}" data-code="{{ $vulnerable->code }}" 
                        data-status="{{ $vulnerable->status_name }}" data-age_from="{{ $vulnerable->age_from }}" data-age_to="{{ $vulnerable->age_to }}"
                        data-gender="{{ $vulnerable->gender_name }}">
                        <td class = "td_common_numeric_rules">{{($vulnerables->currentPage() - 1) * $vulnerables->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $vulnerable->name }}</td>
                        <td class ="common_td_rules">{{ $vulnerable->code}}</td>
                        <td class ="td_common_numeric_rules">{{ $vulnerable->age_from}}</td>
                        <td class ="td_common_numeric_rules">{{ $vulnerable->age_to}}</td>
                        <td class ="common_td_rules">{{ $vulnerable->status}}</td>
                        <td class ="common_td_rules">{{ $vulnerable->gender}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>