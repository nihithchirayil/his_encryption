@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }
    .box {
  border: 1px solid green ;
}
.modal:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }
          
        .modal-dialog {
            display: inline-block;
            vertical-align: middle;
            left:231px;
        }
          
        .modal .modal-content {
            /* padding: 20px 20px 20px 20px;
            -webkit-animation-name: modal-animation;
            -webkit-animation-duration: 0.5s;
            animation-name: modal-animation;
            animation-duration: 0.5s; */
        }
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
            <h4 class="blue text-left"> <strong>Hospital Details Master</strong></h4>
            <div class="col-md-12 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                           
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Hospital Name</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="name" value="">
                            <input type="hidden" value="" id="name_hidden">
                            <div class="ajaxSearchBox" id="nameAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Code</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="code" value="">
                            <input type="hidden" value="" id="code_hidden">
                            <div class="ajaxSearchBox" id="codeAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-1 padding_sm ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" ><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <div onclick="add();" class="btn btn-block light_purple_bg" id="add"><i class="fa fa-plus" ></i> Add</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}"
                    {{-- data-update_url="{{ $update_url }}" --}}
                    >

                    </div>
                </div>
            </div>
            <div id="datapopup" class="modal fade" role="dialog" >
                <div class="modal-dialog">
        
                    <!-- Modal content-->
                    <div  class="modal-content" style="width:1050px; display: inline-block;
                    vertical-align: middle;">
                        <div style=background-color:darkgreen; id="popup2"
                            class="modal-header modal_header_dark_purple_bg modal-header-sm">
                            <button type="button" class="close" onclick="reset1();" data-dismiss="modal">&times;</button>
                            <h4 align="center" class="modal-title titleName">Hospital Details</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div id="popup" class="theadscroll always-visible" style="position: relative;height: 455px; width:1000px">
                                        {{-- <h4 class="blue text-center"><span id="add"> Add Location  </span></h4> --}}
                                        <hr >
                                      
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Hospital Name</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control name_add" name="name_add" id="name_add" value=" {{$hospital_details->name?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Code</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control code_add" name="code_add" id="code_add" value=" {{$hospital_details->code?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Address</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control address_add" name="address_add" id="address_add" value=" {{$hospital_details->address ?? '' }} ">
                                            </div>
                                        </div>
                                        
                                       
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Country</label>
                                            <div class="clearfix"></div>
                                            <select class="form-control country_add" name="country_add" id="country_add" >
                                                <option value="">--Select--</option>
                                                <option value="1">{{__('India ')}}</option>
                                                <option value="2">{{__('America')}}</option>
                                                <option value="3">{{__('China')}}</option>
                                                <option value="4">{{__('OMAN')}}</option>
                                                <option value="5">{{__('QATAR')}}</option>
                                                <option value="6">{{__('ROMANIA')}}</option>
                                            </select>
                                        </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">State</label>
                                            <div class="clearfix"></div>
                                            {{-- <input type="text" class="form-control branch_add" name="branch_add" id="branch_add" value="  "> --}}
                                            <select class="form-control select2" id="state_add" name="state_add">
                                                <option  value="">Select State</option>
                                                @foreach ($state as $st)
                                                <option value="{{ $st->id }}">{{ $st->name }}</option>
                
                                            @endforeach
                                            </select>
                                            </div>
                                        </div>
                                      
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">City</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control city_add" name="city_add" id="city_add" value=" {{$hospital_details->city?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Pincode</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control pincode_add" name="pincode_add" id="pincode_add" value=" {{$hospital_details->pincode ?? '' }} ">
                                            </div>
                                        </div>
                                       
                                       
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Default Renewal Validity</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control default_renewal_validity_add" name="default_renewal_validity_add" id="default_renewal_validity_add" value=" {{$hospital_details->default_renewal_validity ?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Default Free Visits</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control default_free_visits_add" name="default_free_visits_add" id="default_free_visits_add" value=" {{$hospital_details->default_free_visits ?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Default Free Visits After Discharge</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control default_free_visits_after_discharge_add" name="default_free_visits_after_discharge_add" id="default_free_visits_after_discharge_add" value=" {{$hospital_details->default_free_visits_after_discharge ?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Phone Number</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control phone_add" name="phone_add" id="phone_add" value=" {{$hospital_details->phone ?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Booking Number</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control booking_no_add" name="booking_no_add" id="booking_no_add" value=" {{$hospital_details->booking_no ?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">GST Number</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control gst_no_add" name="gst_no_add" id="gst_no_add" value=" {{$hospital_details->gst_no ?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">TIN Number</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control tin_no_add" name="tin_no_add" id="tin_no_add" value=" {{$hospital_details->tin_no ?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Drug Licence Number</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control drug_licence_no_add" name="drug_licence_no_add" id="drug_licence_no_add" value=" {{$hospital_details->drug_licence_no ?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">PAN Card Number</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control pan_no_add" name="pan_no_add" id="pan_no_add" value=" {{$hospital_details->pan_no ?? '' }} ">
                                            </div>
                                        </div>
                                      
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Manage Director</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control manage_director_add" name="manage_director_add" id="manage_director_add" value=" {{$hospital_details->manage_director ?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Vice President</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control vice_president_add" name="vice_president_add" id="vice_president_add" value=" {{$hospital_details->vice_president ?? '' }} ">
                                            </div>
                                        </div>
                                      
                                        <div class="col-md-1 padding_sm" style="margin-block-start: 19px;">
                                            <div class="clearfix"></div>
                                            <input type="checkbox" class="status_add" name="status_add" id="status_add" value="1"> 
                                            <label for="">Active</label>
                                            <div id='status1'></div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            {{-- <div class="mate-input-box"> --}}
                                            <label for="">Hospital Header</label>
                                            <div class="clearfix"></div>
                                            <textarea class="form-control hospital_header" name="hospital_header" id="hospital_header" style=" min-width:100px; max-width:100%; max-height:100%; min-height:1px;height:7%;width:100%;" value=" {{$hospital_details->hospital_header?? '' }} ">
                                        </textarea>
                                            {{-- </div> --}}
                                        </div>
                                    </div>
                                  
                                    <div class="row">
                                        <div id='id'class="col-md-4 padding_sm">
                                            {{-- <div class="mate-input-box"> --}}
                                            <label>Hospital Logo</label>
                                             <input type="file" name="files[]" id="hospital_logo" onchange="previewFile(this);">
                                        </div>
                                        {{-- </div> --}}
                                        <div id='id1' class="col-md-4 padding_sm">
                                            {{-- <div class="mate-input-box"> --}}
                                            <label>Nabh Logo</label>
                                             <input type="file" name="files[]" id="nabh_logo" onchange="previewFile1(this);">
                                        </div>
                                        {{-- </div> --}}
                                        <div id='id2' class="col-md-4 padding_sm">
                                            {{-- <div class="mate-input-box"> --}}
                                            <label>Report Header Img</label>
                                             <input type="file" name="files[]" id="report_header_img" onchange="previewFile2(this);">
                                        {{-- </div> --}}
                                        </div>
                                       
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 padding_sm" style="margin-bottom:10px;">
                                       <label align="center" id="img1">Hospital Logo</label>
                                       <img src="" class="box" id="hospital_logo1" style="width=100 ;height=100">
                                        </div>

                                        <div class="col-md-4 padding_sm" style="margin-bottom:10px;">
                                        <label align="center" id="img2">Nabh Logo</label>
                                        <img src="" class="box" id="nabh_logo1" style="width=100 ;height=100">
                                       </div>

                                        <div class="col-md-4 padding_sm" style="margin-bottom:10px;">
                                        <label align="center" id="img3">Report Header Img</label>
                                        <img src="" class="box" id="report_header_img1" style="width=100 ;height=100">
                                       </div>
                                    </div>
                                        <input type="hidden" name="reference_id" class="reference_id">
                                        {{-- @if() --}}
                                      
                                   
                                </div>
                            </div>
                        </div>

                        <div class = "modal-footer">
                            <div class="col-md-2 padding_sm pull-right"  style="margin-bottom:10px;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="submit" class="btn btn-block btn-success saveButton" onclick="saveForm()"><i class="fa fa-save"></i> Save </button>
                            </div>
                            <div class="col-md-2 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="submit" class="btn btn-block btn-info clearFormButton" onclick="resetForm()"><i class="fa fa-times"></i> Clear </button>
                            </div>                           
                       </div>
                            
                        </div>
                       
                        </div>
           
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/hospital_details.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
{{-- <script src="js/loadingoverlay.js"></script> --}}
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
