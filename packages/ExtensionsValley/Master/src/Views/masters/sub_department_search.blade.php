<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="30%">Department Name</th>
                <th width="30%">Sub Department Name</th>
                <th width="10%">Sub Department Code</th>
                <th width="10%">Status</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($sub_department) > 0)
                @foreach ($sub_department as $sub_dept)
                    <tr style="cursor: pointer;"  data-id="{{ $sub_dept->id }}" data-dept_name="{{ $sub_dept->dept_id }}" data-sub_dept_code="{{ $sub_dept->sub_dept_code }}" 
                        data-sub_dept_name="{{ $sub_dept->sub_dept_name }}" data-status="{{ $sub_dept->status_name }}">
                        <td class = "td_common_numeric_rules">{{($sub_department->currentPage() - 1) * $sub_department->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $sub_dept->dept_name }}</td>
                        <td class ="common_td_rules">{{ $sub_dept->sub_dept_name }}</td>
                        <td class ="common_td_rules">{{ $sub_dept->sub_dept_code }}</td>
                        <td class ="common_td_rules">{{ $sub_dept->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            {{-- <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button> --}}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
