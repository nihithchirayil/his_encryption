<div class="theadscroll" id="load_data2" style="position: relative; height: 300px;">
    <table class="table no-margin table_sm theadfix_wrapper1 table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="50%">Doctor Name</th>
                <th width="50%">Visit Date</th>
            </tr>
        </thead>
        <tbody>
            @if(count($doctor_list)!=0)
            @foreach($doctor_list as $each)
            <tr>
                <td class="common_td_rules">{{ $each->doctor_name }}</td>
                <td class="common_td_rules">{{ $each->visit_time }}</td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="2" style="text-align: center">No Result Found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
