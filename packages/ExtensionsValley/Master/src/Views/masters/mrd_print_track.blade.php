<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
        {!!$hospital_header!!}
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From: <?= date('M-d-Y',strtotime($from))  ?> To <?= date('M-d-Y',strtotime($to))  ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Patient MRD Tracker</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                    style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='3%'>SL.No.</th>
                        <th width='10%'>Uhid</th>
                        <th width='15%'>Patient</th>
                        <th width='20%'>Doctor Name</th>
                        <th width='5%'>Token</th>
                        <th width='10%'>Visit Status</th>
                        <th width='10%'>Visit Date</th>
                        <th width='10%'>Session</th>
                        <th width='10%'>Remarks</th>

                   </tr>


                </thead>
                <tbody>
                    @if (count($res) != 0)
                        @foreach ($res as $data)
                            <tr>
                                <td class="td_common_numeric_rules">{{ $loop->iteration }}</td>
                                <td class="common_td_rules">{{ $data->uhid }}</td>
                                <td class="common_td_rules">{{ $data->patient_name }}</td>
                                <td class="common_td_rules">{{ $data->doctor_name }}</td>
                                <td class="td_common_numeric_rules">{{ $data->token_prefix }}</td>
                                <td class="common_td_rules">{{ $data->visit_type }}</td>
                                <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->visit_time)) }}</td>
                                @php
                                $session='--';
                            @endphp
                            @if ($data->session_id==1)
                              @php
                               $session='Morning';
                              @endphp
                            @elseif($data->session_id==2)
                            @php
                            $session='Evening';

                            @endphp 
                            @endif
                            <td class="common_td_rules">{{$session}}</td>

                            <td class="common_td_rules">{{ @$data->remarks  ? $data->remarks  : '--'}}</td>
                              

                            </tr>


                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
