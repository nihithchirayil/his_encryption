@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
            <h4 class="blue text-left"> <strong>Modality Master</strong></h4>
            <div class="col-md-8 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                           
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Modality Name</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="name" value="">
                            <input type="hidden" value="" id="name_hidden">
                            <div class="ajaxSearchBox" id="nameAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Modality Code</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="code" value="">
                            <input type="hidden" value="" id="code_hidden">
                            <div class="ajaxSearchBox" id="codeAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                          
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Status</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control status" name="status" id="status">
                                        <option value="">All</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" ><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}" data-delete-url="{{ $delete_url }}"
                    >

                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="box no-border" >
                    <div class="box-body clearfix" style=" height :522px">
                        <h4 class="blue text-center"><span id="add"> Add Modality  </span></h4>
                        <hr >
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Modality Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control name_add" name="name_add" id="name_add" value=" {{$modality->location_name ?? '' }} ">
                            </div>
                        </div>

                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Modality Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control code_add" name="code_add" id="code_add" value=" {{$modality->code?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Sub Department</label>
                            <div class="clearfix"></div>
                            {{-- <input type="text" class="form-control branch_add" name="branch_add" id="branch_add" value="  "> --}}
                            <select class="form-control select2" id="sub_dept" name="sub_dept">
                                <option  value="">Select Sub Department</option>
                                @foreach ($sub_dept as $sub)
                                <option value="{{ $sub->id }}">{{ $sub->sub_dept_name }}</option>

                            @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="status_add" name="status_add" id="status_add" value="1"> 
                            <label for="">Active</label>
                            <div id='status1'></div>
                        </div>
                       
                        <input type="hidden" name="reference_id" class="reference_id" >
                        <div class="col-md-4 padding_sm pull-right"  style="margin-bottom:10px;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-success saveButton" onclick="saveForm()"><i class="fa fa-save"></i> Save </button>
                        </div>
                        <div class="col-md-4 padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-info clearFormButton" onclick="resetForm()"><i class="fa fa-times"></i> Clear </button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/modality_master.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
{{-- <script src="js/loadingoverlay.js"></script> --}}
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
