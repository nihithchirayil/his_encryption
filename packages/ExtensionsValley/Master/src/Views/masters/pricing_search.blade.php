<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="7%">Pricing Name</th>
                <th width="17%">Company Name</th>
                <th width="11%">Pricing Type</th>
                <th width="11%">Effective Date</th>
                <th width="11%">Expiry Date</th>
                <th width="9%">Show Price Discount</th>
                <th width="11%">Billing Use Only</th>
                <th width="7%">Allow Pay From Cash</th>
                <th width="7%">Status</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($pricing) > 0)
                @foreach ($pricing as $price)
                    <tr style="cursor: pointer;"  data-id="{{ $price->id }}" data-pricing="{{ $price->pricing_name }}" data-company="{{ $price->company_id}}" 

                        data-pricing_type="{{ $price->pricing }}"   data-effective_date="{{$price->date1 }}" data-expiry_date="{{ $price->date2 }}" data-discount="{{ $price->discount }}"
                        
                        data-billing="{{ $price->billing }}" data-pay_from_cash="{{ $price->pay_from_cash }}"  data-status="{{ $price->status_name }}">
                        <td class = "td_common_numeric_rules">{{($pricing->currentPage() - 1) * $pricing->perPage() + $loop->iteration}}</td>
                        <td class ="td_common_numeric_rules">{{ $price->pricing_name }}</td>
                        <td class ="common_td_rules">{{ $price->company}}</td>
                        <td class ="common_td_rules">{{ $price->pricing_type}}</td>
                        <td class ="common_td_rules">{{date('Y-M-d',strtotime($price->date1))}}</td>
                        <td class ="td_common_numeric_rules">{{date('Y-M-d',strtotime($price->date2))}}</td>
                        <td class ="td_common_numeric_rules">{{ $price->show_price_discount}}</td>
                        <td class ="common_td_rules">{{ $price->billing_use_only}}</td>
                        <td class ="common_td_rules">{{ $price->allow_pay_from_cash}}</td>
                        <td class ="common_td_rules">{{ $price->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            {{-- <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button> --}}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
