<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="30%">Bill Tag</th>
                <th width="30%">Department</th>
                <th width="10%">Status</th>
                <th style = "width:13%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($billtag_department_map) > 0)
                @foreach ($billtag_department_map as $billtag_department)
                    <tr style="cursor: pointer;"  data-id="{{ $billtag_department->id }}" data-bill_tag="{{ $billtag_department->bill_tag_id }}" 
                        data-dept_name="{{ $billtag_department->department_id }}" data-status="{{ $billtag_department->status_name }}">
                        <td class = "td_common_numeric_rules">{{($billtag_department_map->currentPage() - 1) * $billtag_department_map->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $billtag_department->bill_tag }}</td>
                        <td class ="common_td_rules">{{ $billtag_department->dept_name }}</td>
                        <td class ="common_td_rules">{{ $billtag_department->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            {{-- <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button> --}}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
