@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
            <div class="col-md-8 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <input type="hidden" name="bt_id" id="bt_id" > 
                                    <label for="">From Location</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" style='color:#555555;'  name="from_location" id="from_location" onchange = "changeLocationCheck(this)">
                                    <option value="">Select Location</option>
                                      
                                           @foreach ($location_name as $branch)
                                           <option value="{{ $branch->location_code }}">{{ $branch->location_name }}</option>
                                   
                                           @endforeach
                                    </select>
                                </div></div>
                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box" id="append_to_loc">
                                        <label for="">To Location</label>
                                        <div class="clearfix"></div>
                                        <select class="form-control select2" style='color:#555555;'  name="to_location" id="to_location" >
                                        <option value="">Select Location</option>
                                        @foreach ($location_name as $loc)
                                        <option value="{{ $loc->location_code }}">{{ $loc->location_name }}</option>
                                
                                    @endforeach
        
                                        </select>
                                    </div></div>
                                    <div class="col-md-4 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">Item Type</label>
                                            <div class="clearfix"></div>
                                            <select class="form-control select2" style='color:#555555;'  name="item" id="item" >
                                            <option value="">Select Item</option>
                                            @foreach ($item_name as $item)
                                            <option value="{{ $item->group_code }}">{{ $item->name }}</option>
                                    
                                        @endforeach
            
                                            </select>
                                        </div></div>
                                    <input type="hidden" name="reference_id" class="reference_id" >

                                    <div class="col-md-2 padding_sm">
                                        <label for="">&nbsp;</label>
                                        <div class="clearfix"></div>
                                        <button class="btn btn-block btn-primary transfer" onclick="saveForm()" ><i class="fa fa-save"></i> Transfer</button>
                                    </div>
                            <div class="col-md-2 padding_sm ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" ><i class="fa fa-search"></i>
                                    Search</button>
                            </div>



                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}" >

                    </div>
                </div>
            </div>


    </div>
</div>
<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/bulk_transfer.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
{{-- <script src="js/loadingoverlay.js"></script> --}}
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
