@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
   
      <!-- Custom Theme Scripts -->
      <link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
       <!-- jQuery -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/jquery.min.js') }}"></script>

    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">

@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="row" style="margin-bottom: 10px"> </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <h4 class="blue"><strong> <?= $title ?> </strong></h4>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-9 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix" style="height:70px">
                        <div class="col-md-12 padding_sm">
                            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">

                            <div class="col-md-4 padding_sm" style="margin-top:10px;">
                                <div class="mate-input-box ">
                                    <label for="">Checklist name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control hidden_search" id="checkList" value="" placeholder="Search name....">
                                    <input type="hidden" value="0" id="checkList_hidden">
                                    <div class="ajaxSearchBox" id="checkListAjaxDiv"
                                        style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 21px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999;
                                                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                        </div>
                                    
                                </div>
                            </div>

                           
                            <div class="col-md-1 pull-right  padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" onclick="searchCheckList();" id="searchBtn"
                                    class="btn btn-block btn-primary"><i id="searchSpin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 padding_sm">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div id="searchCheckList"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 padding_sm">
                <div class="box no-border">
                    <input type="hidden" name="hidden_id" id="hidden_id" value="">
                    <div class="box-body clearfix" style="height: 520px">
                        <h5 class="blue text-center"><strong> Add New Check List </strong></h5>
                        <hr>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Checklist Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="name"
                                        name="name" value="" required>
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm" style="margin-top:10px;">
                                <div class="mate-input-box ">
                                    <label for="">Modality</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select" id="search_modality">

                                     <option value="">Select Modality</option>
                                        <?php
                                        foreach ($modality as $each) {
                                            ?>
                                     <option value="<?= $each->code ?>"><?= $each->name ?></option>
                                        <?php
                                    }
                                    ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Status</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1" selected>{{__('Active')}}</option>
                                        <option value="0">{{__('Inactive')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-top:10px;">
                            <div class="col-md-4 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <button type="button" id="savebtnBtn" class="btn btn-block btn-success"
                                    onclick="saveCheckList();"><i id="savebtnSpin" class="fa fa-save"></i><span id="saveBtn">Save</span> 
                                </button>
                            </div>
                            <div class="col-md-4 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-info clearFormButton"
                                    onclick="clearItem();"><i class="fa fa-times"></i> Clear </button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>



    </div>
    </div>
    </div>
    <!-- /page content -->

@stop
@section('javascript_extra')
    <script src="{{asset("packages/extensionsvalley/emr/js/checkList.js")}}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version='.env('APP_JS_VERSION', '0.0.1')) }}"> </script>
@endsection
