<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="30%">Request Purpose</th>
                <th width="10%">Status</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($mrd_request_purpose) > 0)
                @foreach ($mrd_request_purpose as $mrd_request_purp)
                    <tr style="cursor: pointer;"  data-id="{{ $mrd_request_purp->id }}" data-request_purp="{{ $mrd_request_purp->request_purpose }}" 
                        data-status="{{ $mrd_request_purp->status_name }}" >
                        <td class = "td_common_numeric_rules">{{($mrd_request_purpose->currentPage() - 1) * $mrd_request_purpose->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $mrd_request_purp->request_purpose }}</td>
                        <td class ="common_td_rules">{{ $mrd_request_purp->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
