@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">

<div class="right_col">
    <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 no-padding">
            <div class="box no-border">
                <div class="box-body clearfix">
                    {!! Form::token() !!}
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Mapping Type</label>
                            <div class="clearfix"></div>
                            {!! Form::select('txt_mapping_type', array(
                            "0" => " Select Type",
                            "department" => " Department",
                            "insurance" => " Insurance",
                            "supplier" => " Supplier",
                            "sub_department" => " Sub Department"), '',
                            ['onchange'=> "list_department_data()" , 'class'=>"form-control", 'id'=>"txt_mapping_type"])
                            !!}
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Mapping Value</label>
                            <div class="clearfix"></div>
                            <input type="text" value="" name="value" autocomplete="off" id="value"
                                class="form-control value" placeholder="Mapping Value"
                                onkeyup="searchMapValue(this.id, event)" autocomplete="off">
                            <div class="ajaxSearchBox" id="value_div" style="z-index: 9999 !important"></div>
                            <input type="hidden" value="" name="value_id" id="value_id">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Ledger Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" id="txt_ledger" name="ledger" value=""
                                autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <button class="btn btn-block light_purple_bg" id="btn_list_department_data"
                            onclick="list_department_data();"><i class="fa fa-search"></i>
                            Search</button>
                    </div>
                    <div class="col-md-1 padding_sm">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <button class="btn btn-block btn-warning" id="btn_clear_department_data"
                            onclick="clear_department_data();"><i class="fa fa-times"></i>
                            Clear</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1 padding_sm pull-right">
            <label for="">&nbsp;</label>
            <div class="clearfix"></div>
            <button class="btn btn-block btn-info" id="btn_save_mapping_data" onclick="save_mapping_data();"><i
                    class="fa fa-save"></i>
                Save</button>
        </div>

        <div class="clearfix"></div>
        <div class="box no-border no-margin">
            <div class="box-body clearfix">
                <div class="theadscroll" style="position: relative; height: 65vh;" id="ledger_mapping_listDiv">

                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="getAuditLog" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 80%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <span class="modal-title" id="audit_log_header"></span>
            </div>
            <div class="modal-body" style="min-height:400px;height:500px;overflow: auto" id="audit_log_body">

            </div>
        </div>
    </div>
</div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/audit_log.js")}}"></script>

<script type="text/javascript">
$(document).ready(function() {
    var $table = $('table.theadfix_wrapper');
    token = $('#hidden_filetoken').val();
    base_url = $('#base_url').val();

    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });


    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
});

function list_department_data() {
    var mapping_type = $('#txt_mapping_type').val();
    var ledger = $('#txt_ledger').val();
    var mapping_value_id = $('#value_id').val();

    if (mapping_type == '0') {
        toastr.warning('Please select any mapping type');
        return;
    }
    $.ajax({
        type: "POST",
        url: "",
        data: '&type=' + mapping_type + '&ledger=' + ledger + '&mapping_value_id=' + mapping_value_id,
        beforeSend: function() {
            $('#btn_list_department_data').attr('disabled', true);
            $('#btn_list_department_data').find('i').removeClass('fa fa-search');
            $('#btn_list_department_data').find('i').addClass('fa fa-spinner fa-spin');
        },
        success: function(data) {
            $('#ledger_mapping_listDiv').html(data);
        },
        complete: function() {
            mapping_type_array = {};
            $('#btn_list_department_data').attr('disabled', false);
            $('#btn_list_department_data').find('i').removeClass('fa fa-spinner fa-spin');
            $('#btn_list_department_data').find('i').addClass('fa fa-search');
            var mappingText = $('#txt_mapping_type option:selected').html();
            $('.head_mapping_type').html(mappingText);
            $('.theadscroll').perfectScrollbar("update");
            $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
            });
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        }
    });
}

function searchLedger(id, event, count = 0) {
    var mapping_type = $('#txt_mapping_type').val();
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var ajax_div = 'ledger_div' + count;
    if ((value.match(keycheck) || event.keyCode == '8') && (event.keyCode != '40' && event.keyCode != '38' && event
            .keyCode != '13')) {
        var group = $('#' + id).val();
        if (group == "") {
            $("#" + ajax_div).html("");
            $("#" + ajax_div).hide();
            $("#ledger_id" + count).val(0);
            getMappingTypeArray(0, '', count);
        } else {
            $.ajax({
                type: "GET",
                url: "",
                data: 'ledger_name=' + group + '&count=' + count + '&mapping_type='+mapping_type,

                beforeSend: function() {
                    $("#" + ajax_div).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function(html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function() {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}

function save_mapping_data() {
    var mapping_type = $('#txt_mapping_type').val();
    if (mapping_type == '0') {
        toastr.warning('Please select any mapping type');
        return;
    }
    if (mapping_type_array.length == 0) {
        toastr.warning('Please select any ledger');
        return;
    }
    var url = base_url + "/item/saveLedgerMapping";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            mapping_value_array: JSON.stringify(mapping_type_array),
            mapping_type: mapping_type
        },

        beforeSend: function() {
            $('#btn_save_mapping_data').attr('disabled', true);
            $('#btn_save_mapping_data').find('i').removeClass('fa fa-save');
            $('#btn_save_mapping_data').find('i').addClass('fa fa-spinner fa-spin');
        },
        success: function(data) {
            if (data != 0) {
                toastr.success('Successfully added');
                list_department_data();
            }
        },
        complete: function() {
            $('#btn_save_mapping_data').attr('disabled', false);
            $('#btn_save_mapping_data').find('i').removeClass('fa fa-spinner fa-spin');
            $('#btn_save_mapping_data').find('i').addClass('fa fa-save');
        }
    });
}
var mapping_type_array = {};

function getMappingTypeArray(ledger_id, ledger_name, count) {
    var mapping_type = $('.tr_mapping_cls' + count).attr('attr_mapping_type');
    mapping_type_array[mapping_type] = ledger_id + '::' + ledger_name;
    return mapping_type_array;
}

function fillLedgerDetails(e, id, name, count = 0) {
    $('#ledger_name' + count).val(name);
    $('#ledger_id' + count).val(id);
    $('#ledger_div' + count).hide();
    $('#ass_ug_chk' + count).prop('checked', true);
    getMappingTypeArray(id, name, count);
}

function clear_department_data() {
    $('#txt_mapping_type').val(0);
    $('#ledger_mapping_listDiv').html('');
    // list_department_data();
}

function searchMapValue(id, event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    var ajax_div = 'value_div';

    if ((value.match(keycheck) || event.keyCode == '8') && (event.keyCode != '40' && event.keyCode != '38' && event
            .keyCode != '13')) {
        var mapping_value = $('#' + id).val();
        var mapping_type = $('#txt_mapping_type').val();
        if (mapping_type == '0') {
            toastr.warning('Please Select Valid Mapping Type!');
            $('#' + id).val('');
            return;
        }
        if (mapping_value == "") {
            $("#" + ajax_div).html("");
            $('#value_id').val(0);
        } else {
            $.ajax({
                type: "GET",
                url: "",
                data: 'mapping_value=' + mapping_value + '&mapping_type=' + mapping_type,
                beforeSend: function() {
                    $("#" + ajax_div).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function(html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function() {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}

function fillMapValues(e, id, name) {
    $('#value').val(name);
    $('#value_id').val(id);
    $('#value_div').hide();
}

function delete_dl_mapping(mapping_id, row) {
    var mapping_type = $('#txt_mapping_type').val();
    var mapping_type_value = $('.tr_mapping_cls' + row).attr('attr_mapping_type');

    var url = base_url + "/item/deleteLedgerMapping";
    if (mapping_type == '0') {
        toastr.warning('Please Select Valid Mapping Type!');
        return;
    }
    console.log(mapping_id);
    bootbox.confirm({
        message: "Do you want to delete ?",
        buttons: {
            'confirm': {
                label: "Continue",
                className: 'btn-warning',
                default: 'true'
            },
            'cancel': {
                label: 'Cancel',
                className: 'btn-success'
            }
        },
        callback: function(result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: 'mapping_id=' + mapping_id + '&mapping_type=' + mapping_type + '&mapping_type_value=' + mapping_type_value,
                    beforeSend: function() {
                        $('#span_delete_dl_mapping' + row).removeClass('fa fa-trash');
                        $('#span_delete_dl_mapping' + row).addClass('fa fa-spinner fa-spin');
                    },
                    success: function(data) {
                        if (data != 0) {
                            toastr.success('Successfully deleted');
                            list_department_data();
                        }
                    },
                    complete: function() {
                        $('#span_delete_dl_mapping' + row).removeClass('fa fa-spinner fa-spin');
                        $('#span_delete_dl_mapping' + row).addClass('fa fa-trash');
                    }
                });

            }
        }
    });
}
@include('Purchase::messagetemplate')
</script>

@endsection
