<table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
    style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_bg">
            <th width='1%'>Sl.No</th>
            <th width='10%' class="head_mapping_type">{{ucfirst($mapping_type)}}</th>
            <th width='10%'>Ledger Name</th>
            <th width='1%'><i class="fa fa-history"></th>
            <th width='1%'><i style="padding: 5px 8px; font-size: 15px;" class="fa fa-trash"></i></th>
        </tr>
    </thead>
    <tbody>
        @php $i=1; @endphp
        @if(count($list) > 0)
        @foreach ($list as $list)
        @php
        $ledger_id = @$list->ledger_id ? $list->ledger_id : 0;
        $ledger_name = @$list->ledger_name ? $list->ledger_name : '';
        $status = @$list->status ? $list->status : 0;
        $checked ='';
        if ($status == 1) {
        $checked = 'checked';
        }
        @endphp
        <tr style="cursor: pointer;" class="tr_mapping_cls tr_mapping_cls{{$i}}" attr_mapping_type={{$list->id}}
            attr_count={{$i}}>
            <td class="common_td_rules">{{$i}}</td>
            <td class="mapping_value common_td_rules">{{ $list->name }}</td>
            <td >
                <div class="mate-input-box" style="padding: 0px 0px 2px 3px !important;">
                    <input type="text" name="ledger_name" autocomplete="off" id="ledger_name{{$i}}"
                        class="form-control fav_grp ledger_name_class" placeholder="Ledger Name" required=""
                        onkeyup="searchLedger(this.id, event, {{$i}})" value="{{$ledger_name}}">
                    <input type="hidden" name="ledger_id{{$i}}" id="ledger_id{{$i}}" value="{{$ledger_id}}">
                </div>
                <div style="position:relative">
                <div class="ajaxSearchBox"
                    style="top: 0!important; text-align: left; list-style: none; cursor: pointer; max-height: 400px; margin: 0px; z-index: 99999; position: absolute; background: rgb(255, 255, 255); border-radius: 3px; height: 270px; border: 1px solid rgba(0, 0, 0, 0.3); width: 100% !important; display: none;"
                    id="ledger_div{{$i}}">
                </div>
                </div>
            </td>
            <td>
                @if(isset($list->updated_at) && $list->updated_at != '')
                <i  style="cursor:pointer;color: #f0ad4e"  id="history_gl_mapping_{{$list->mapping_id}}" onclick="glMappingAuditLog({{$list->mapping_id}})"
                    class="fa fa-history"></i>
                @endif
            </td>
            <td>
                @if ($list->mapping_id != 0)
                <i style="padding: 5px 8px; font-size: 15px;" id="span_delete_dl_mapping{{$i}}" onclick="delete_dl_mapping({{$list->mapping_id}}, {{$i}})"
                    class="fa fa-trash text-red deleteRow"></i>
            </td>
            @else
            -
            @endif
            <!-- <td class="status" style="text-align:center">
                <input {{ $checked }} style="" type="checkbox" name="ass_ug_chk[]" class="ass_ug_chk"
                    id="ass_ug_chk{{$i}}">
            </td> -->
        </tr>
        @php $i++; @endphp
        @endforeach
        @else
        <tr>
            <td colspan="5" class="tax_code">No Records found</td>
        </tr>
        @endif
    </tbody>
</table>
