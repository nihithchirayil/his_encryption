@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')

<div class="right_col" >
    <div class="row" style="text-align: right;padding-right: 80px;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.list_ledger_opening_balance')}}" id="listingSearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="" class="header_label">Ledger</label>
                                    <div class="clearfix"></div>
                                    <input type="hidden" value="{{$searchFields['search_ledger_id'] ?? ''}}" name="search_ledger_id"  autocomplete="off" class="form-control " id="search_ledger_id-1">
                                    <input type="text" value="{{$searchFields['search_ledger_name'] ?? ''}}"  name="search_ledger_name"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event, 1)" id="search_ledger_name">
                                </div>
                                <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box"
                                     style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                     margin: -2px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                     border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                            </div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Opening Balance</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="balance" id="balance"
                                           value="{{ $searchFields['balance'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                               style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Ledger Name</th>
                                    <th>Opening Balance</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th class="text-center">Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($opening_balance) > 0)
                                @foreach ($opening_balance as $list)
                                <tr>
                                    <td class="common_td_rules">{{ $list->ledger_name }}</td>
                                    <td class="td_common_numeric_rules">{{ $list->opening_balance }}</td>
                                    <td class="common_td_rules">{{ $list->date }}</td>
                                    <td class="common_td_rules">{{ $list->status_name  }}</td>
                                    <td class="text-center">
                                        <i style="cursor:pointer" onclick="ledgerEditLoadData('{{$list->id}}','{{$list->ledger_name}}','{{$list->ledger_id}}','{{$list->opening_balance}}','{{$list->date}}','{{$list->status}}')"  class="fa fa-edit"></i>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="6" class="re-records-found">No Records found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-center">
                        <ul class="pagination purple_pagination pull-right">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.save_ledger_opening_balance')}}" method="POST" id="ledger_group_form">
                        {!! Form::token() !!}
                        <input type="hidden" name="opening_id" id="opening_id" value="">

                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="" class="header_label">Ledger</label>
                                    <div class="clearfix"></div>
                                    <input type="hidden" value="" name="ledger_id"  autocomplete="off" class="form-control " id="ledger_id-1">
                                    <input type="text" value=""  name="ledger_name_update"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event, 2)" id="ledger_name_update">
                                </div>
                                <div class="ajaxSearchBox search_ledger_item_box" id="ledger_name_box"
                                     style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                     margin: -2px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                     border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                            </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Opening Balance</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="opening_balance" id="opening_balance">
                                <span style="color: #d14;"> {{ $errors->first('opening_balance') }}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" name="opening_date" id="opening_date">
                                <span style="color: #d14;"> {{ $errors->first('opening_date') }}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                <select class="form-control" name="status" id="status_update">
                                    <option value="1">{{__('Active')}}</option>
                                    <option value="0">{{__('Inactive')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg" onclick="return validate()"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function ($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
    position: 'absolute',
            scrollContainer: true
    });
    });
    function ledgerEditLoadData(id, ledger_name, ledger_id, opening_balance, date, status){
    $('#opening_id').val(id);
    $('#ledger_name_update').val(ledger_name);
    $('#ledger_id').val(ledger_id);
    $('#opening_balance').val(opening_balance);
    $('#opening_date').val(date);
    $('#status_update').val(status);
    $('#ledger_group_form').attr('action', '{{$updateUrl}}');
    }

    function validate(){
    if (($('#ledger_id-1').val() == '')){
    alert("Please select Ledger Name");
    return false;
    } else if ($('#opening_balance').val() == ''){
    alert("Please select opening balance");
    return false;
    }
    else{
    return true;
    }
    }

function select_item_desc(id, event, type) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (type == 1) {
        var ajax_div = 'search_ledger_item_box';
        var ledger_desc = $('#search_ledger_name').val();
    } else {
        var ajax_div = 'ledger_name_box';
        var ledger_desc = $('#ledger_name_update').val();
    }

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: "",
                data: 'ledger_desc=' + ledger_desc + '&search_ledger_desc=1&type=' + type,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillLedgerValues(e, ledger_name, id,type) {
    if (type == 1) {
       $("#search_ledger_id-1").val(id);
       $("#search_ledger_name").val(ledger_name.replace(/&amp;/g, '&'));
   } else {
       $("#ledger_id-1").val(id);
       $("#ledger_name_update").val(ledger_name.replace(/&amp;/g, '&'));
    }

}
    @include('Purchase::messagetemplate')
</script>

@endsection
