<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="30%">Doctor</th>
                <th width="20%">Speciality</th>
                <th width="30%">Parent Doctor</th>
                <th width="10%">Status</th>
                <th width="10%">Ip Summary Login Status</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($login_speciality_map) > 0)
                @foreach ($login_speciality_map as $login)
                    <tr style="cursor: pointer;"  data-id="{{ $login->id }}" data-doctor_name="{{ $login->doctor_id }}" data-speciality="{{ $login->speciality_id }}" 
                        data-parent_doctor="{{ $login->parent_doctor_id }}" data-status="{{ $login->status_name }}" data-login_status="{{ $login->login_status }}">
                        <td class = "td_common_numeric_rules">{{($login_speciality_map->currentPage() - 1) * $login_speciality_map->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $login->doctor }}</td>
                        <td class ="common_td_rules">{{ $login->speciality }}</td>
                        <td class ="common_td_rules">{{ $login->parent_doctor }}</td>
                        <td class ="common_td_rules">{{ $login->status}}</td>
                        <td class ="common_td_rules">{{ $login->ip_summary_login_status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            {{-- <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button> --}}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
