@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.ot_bill_group_head')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Group Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="group_name"
                                    value="{{ $searchFields['group_name'] ?? '' }}">
                            </div></div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Group Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="group_code"
                                    value="{{ $searchFields['group_code'] ?? '' }}">
                            </div></div>
                            
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                             <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>  Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; max-height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Group Name</th>
                                    <th>Group Code</th>
                                    <th>Group Number</th>
                                    <th>Color Code</th>
                                    <th>Bill Tag</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($list) > 0)
                                    @foreach ($list as $data)
                                        <tr style="cursor: pointer;" onclick="editLoadData(this,
                                            '{{$data->head_id}}',
                                            '{{$data->group_name}}',
                                            '{{$data->group_code}}',
                                            '{{$data->color_code}}',
                                            '{{$data->bill_tag_id}}',
                                            '{{$data->group_no}}'
                                            )">
                                            <td class="code common_td_rules">{{ $data->group_name }}</td>
                                            <td class="category_name common_td_rules">{{ $data->group_code }}</td>
                                            <td class="category_name common_td_rules">{{ $data->group_no }}</td>
                                            <td class="item common_td_rules">{{ $data->color_code  }}</td>
                                            <td class="item common_td_rules">{{ $data->bill_tag_name  }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="category_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.save_ot_bill_group_head')}}" method="POST" id="headForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="head_id" id="head_id" value="0">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Group Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                             <input type="text" required class="form-control" name="group_name" id="group_name">
                             <span class="error_red">{{ $errors->first('group_name') }}</span>
                        </div></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Color Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="color_code" id="color_code">
                        </div></div>
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Group Code<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                               <select class="form-control" name="group_code" id="group_code">
                                <option value="B1">{{__('Bill 1')}}</option>
                                <option value="B2">{{__('Bill 2')}}</option>
                                <option value="B3">{{__('Bill 3')}}</option>
                                <option value="B4">{{__('OT Bill')}}</option>
                                <option value="B5">{{__('Bill 5')}}</option>
                                <option value="B6">{{__('Bill 6')}}</option>
                            </select>
                            <span class="error_red">{{ $errors->first('group_code') }}</span>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Group Number</label>
                                <div class="clearfix"></div>
                               <input type="number" class="form-control" name="group_no" id="group_no">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Bill Tag<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                               {!! Form::select('bill_tag_id', array("-1"=> " Select") + $bill_tag->toArray(), '', ['class'=>"form-control select2", 'id'=>"bill_tag_id"]) !!}
                               <span class="error_red">{{ $errors->first('bill_tag_id') }}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function () {
         // $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });
        $('.datepicker').datetimepicker({
                            format: 'DD-MMM-YYYY'
                        });

    });

    function editLoadData(obj,head_id,group_name,group_code,color_code,bill_tag_id,group_no){
        var edit_icon_obj = $(obj);
        $('#head_id').val(head_id);
        $('#group_name').val(group_name);
        $('#group_code').val(group_code);
        $('#color_code').val(color_code);
        $('#bill_tag_id').val(bill_tag_id);
        $('#bill_tag_id').select2().trigger('change');
        $('#group_no').val(group_no);
        $('#headForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
         $('#headForm')[0].reset();
         $('#bill_tag_id').val('-1');
         $('#bill_tag_id').select2().trigger('change');
    }
@include('Purchase::messagetemplate')
</script>

@endsection
