<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="45%">Department Name</th>
                <th width="25%">Department Code</th>
                <th width="10%">Status</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($department) > 0)
                @foreach ($department as $dept)
                    <tr style="cursor: pointer;"  data-id="{{ $dept->id }}" data-dept_name="{{ $dept->dept_name }}" data-dept_code="{{ $dept->dept_code }}" data-status="{{ $dept->status_name }}">
                        <td class = "td_common_numeric_rules">{{($department->currentPage() - 1) * $department->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $dept->dept_name }}</td>
                        <td class ="common_td_rules">{{ $dept->dept_code }}</td>
                        <td class ="common_td_rules">{{ $dept->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            {{-- <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button> --}}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
