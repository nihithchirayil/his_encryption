<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <style>
            table tr:hover td {
                text-overflow: initial;
                white-space: normal;
            }
        
        </style>
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">Sl.No.</th>
                <th width="30%">Service Name</th>
                <th width="30%">Hospital Share %</th>
                <th width="30%">Vendor Share %</th>
                <th width="10%">Status</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($lab_vendor_share) > 0)
                @foreach ($lab_vendor_share as $lab_vendor)
                    <tr style="cursor: pointer;"  data-id="{{ $lab_vendor->id }}" data-service_name="{{ $lab_vendor->service_id }}" 
                        data-hospital_share="{{ $lab_vendor->hospital_share }}" data-vendor_share="{{ $lab_vendor->vendor_share }}"
                        data-status="{{ $lab_vendor->status_name }}" data-price="{{ $lab_vendor->price }}" >
                        <td class = "td_common_numeric_rules">{{($lab_vendor_share->currentPage() - 1) * $lab_vendor_share->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $lab_vendor->service_desc }}</td>
                        <td class ="td_common_numeric_rules">{{ $lab_vendor->hospital_share }}</td>
                        <td class ="td_common_numeric_rules">{{ $lab_vendor->vendor_share }}</td>
                        <td class ="common_td_rules">{{ $lab_vendor->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
