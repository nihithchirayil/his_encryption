@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }
    .disabled { color: #999; }
    .enabled { color: #000000; }
    .mate-input-box input.form-control{
        margin-top:8px;
    }
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="sub_dept_html" value="{{$sub_dept_html}}"/>
<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
            <h4 class="blue text-left"> <strong>Service Master</strong></h4>
            <div class="col-md-8 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Service Name</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="service_name" value="">
                                <input type="hidden" value="" id="service_name_hidden">
                                <div class="ajaxSearchBox" id="service_nameAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999px;
                                position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                </div>
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Service Code</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="service_code" value="">
                                <input type="hidden" value="" id="service_code_hidden">
                                <div class="ajaxSearchBox" id="service_codeAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999px;
                                position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                </div>
                                </div>
                            </div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box" >
                                    <label for="">Select Department</label>
                                    {!! Form::select('search_department',$department_list,0, ['class' => 'form-control select2','placeholder'=>'','placeholder'=>'Select Department', 'id' =>'search_department', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box" >
                                    <label for="">Select Sub Department</label>
                                    {!! Form::select('search_sub_department',$sub_department_list,0, ['class' => 'form-control select2','placeholder'=>'','placeholder'=>'Select Sub Department', 'id' =>'search_sub_department', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                </div>
                            </div>



                            {{-- <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Room Type</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="room_type" value="">
                            <input type="hidden" value="" id="room_type_hidden">
                            <div class="ajaxSearchBox" id="room_typeAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div> --}}


                            <div class="col-md-1 padding_sm pull-right ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" ><i class="fa fa-search"></i>
                                    Search</button>
                            </div>



                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}" >

                    </div>
                </div>
            </div>


            <div class="col-md-4 add_service_area">
                <div class="box no-border" >
                    <div class="box-body clearfix" style=" height :618px;">
                        <h4 class="blue text-center"><span id="add"> Add Service </span></h4>
                        <hr >
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Service Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control service_code_add" name="service_code_add" value=" {{  $service_master->service_code ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Service Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control service_name_add" name="service_name_add" value=" {{  $service_master->service_name ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">SAC Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control sac_code_add" name="sac_code_add" value="{{  $service_master->sac_code ?? '' }}">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Department</label>
                            <div class="clearfix"></div>
                            {{-- <input type="text" class="form-control branch_add" name="branch_add" id="branch_add" value="  "> --}}
                            <select onclick="changeSubDepartment();" class="form-control select2" id="department_add" name="department_add">
                                <option  value="">Select Department</option>
                                @foreach ($dept_name as $dept)
                                    <option value="{{ $dept->id }}">{{ $dept->dept_name }}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Sub Department</label>
                            <div class="clearfix"></div>
                            {{-- <input type="text" class="form-control branch_add" name="branch_add" id="branch_add" value="  "> --}}
                            <select class="form-control select2" id="subdepartment_add" name="subdepartment_add">
                                <option  value="">Select Sub Department</option>
                                @foreach ($sub_dept_name as $sub_dept)
                                <option value="{{ $sub_dept->id }}">{{ $sub_dept->sub_dept_name }}</option>

                            @endforeach
                            </select>
                            </div>
                        </div>
                        {{-- <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">ESI Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control esi_name_add" name="esi_name_add" value=" {{  $service_master->esi_name ?? '' }} ">
                            </div>
                        </div> --}}
                        {{-- <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">ESI Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control esi_code_add" name="esi_code_add" value=" {{  $service_master->esi_code ?? '' }} ">
                            </div>
                        </div> --}}
                        {{-- <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">NABH Price</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control nabh_price_add" name="nabh_price_add" value=" {{  $service_master->nabh_price ?? '' }} ">
                            </div>
                        </div> --}}

                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Price</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control price" name="price" value=" {{  $service_master->price ?? '0' }} ">
                            </div>
                        </div>


                        {{-- <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Application Gender</label>
                            <div class="clearfix"></div>
                            <select class="form-control applicable_gender_add" name="applicable_gender_add" id="applicable_gender_add" >
                                <option value="">--Select--</option>
                                <option value="1">{{__('Transgender')}}</option>
                                <option value="2">{{__('Male')}}</option>
                                <option value="3">{{__('Female')}}</option>
                            </select>
                        </div>
                        </div> --}}

                            <div class="col-md-4 padding_sm">
                                <div class="clearfix"></div>
                                <input type="checkbox" checked class="status_add" name="status_add" id="status_add" value="1">
                                <label style="padding-left:2px;" for="">Active</label>
                                <div id='status1'>
                            </div></div>

                        <div class="col-md-4 padding_sm">
                            {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" checked class=" is_billable" name="is_billable" id="is_billable" value="1">
                                <label>Is Billable</label>
                        </div>
                        <div class="col-md-4 padding_sm">
                            {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" checked class="is_refundable" name="is_refundable" id="is_refundable" value="1">
                                <label for="">Is Refundable</label>
                        </div>

                        <div class="col-md-4 padding_sm">
                            {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class="is_discountable" name="is_discountable" id="is_discountable" value="1">
                                <label for="">Is Discountable</label>
                        </div>
                        <div class="col-md-4 padding_sm">
                                {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class="is_price_editable" name="is_price_editable" id="is_price_editable" value="1">
                                <label for="">Is Price Editable</label>
                        </div>
                        <div class="col-md-4 padding_sm">
                            {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class="is_hosp_service" name="is_hosp_service" id="is_hosp_service" value="1">
                                <label for="">Is Hosp Service</label>
                        </div>
                        <div class="col-md-4 padding_sm">
                            {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class="is_doc_required" name="is_doc_required" id="is_doc_required" value="1">
                                <label for="">Is Doc Required</label>
                        </div>

                        <div class="col-md-4 padding_sm">
                            {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class="is_nursing_service" name="is_nursing_service" id="is_nursing_service" value="1">
                                <label for="">Is Nursing Service</label>
                        </div>

                        <div class="col-md-4 padding_sm">
                            {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class="is_lab_item" name="is_lab_item" id="is_lab_item" value="1">
                                <label for="">Is Lab Item</label>
                        </div>
                        <div class="col-md-4 padding_sm">
                            {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class="is_procedure" name="is_procedure" id="is_procedure" value="1">
                                <label for="">Is Procedure</label>
                        </div>
                        <div class="col-md-4 padding_sm">
                            {{-- <div class="mate-input-box"> --}}
                                <div class="clearfix"></div>
                                <input type="checkbox" class=" is_discharge_item" name="is_discharge_item" id="is_discharge_item" value="1">
                                <label for="">Is Discharge Item</label>
                        </div>
                        {{-- <div class="col-md-6 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="restrict_duration" name="restrict_duration" id="restrict_duration" onclick="duration();" value="1">
                            <label for="">Restrict Service By Duration</label>

                        </div> --}}
                    </div>

                        {{-- <div id="id" class="box-body clearfix" style=" height : 217px">

                            <h4 class="black text-center" id='duration'>Duration Details</h4>
                            <hr >
                            <div class="col-md-2 padding_sm">

                                    <div class="clearfix"></div>
                                    <input type="checkbox" class="mode" name="duration_mode" id="hourly" value="1">
                                    <label for="hourly" id="h" class="label_act">Hourly</label>
                            </div>
                            <div class="col-md-2 padding_sm">

                                    <div class="clearfix"></div>
                                    <input type="checkbox" class="mode" name="duration_mode" id="daily" value="2">
                                    <label for="daily" id="d">Daily</label>
                            </div>
                            <div class="col-md-2 padding_sm">

                                    <div class="clearfix"></div>
                                    <input type="checkbox" class="type" name="duration_type" id="warning" value="1">
                                    <label for="warning" id="w">Warning</label>
                            </div>
                            <div class="col-md-2 padding_sm">

                                    <div class="clearfix"></div>
                                    <input type="checkbox" class="type" name="duration_type" id="block" value="2">
                                    <label for="block" id="b">Block</label>
                            </div>
                            <div class="col-md-12 padding_sm" style="margin-bottom:10px; margin-left:259px;">
                                <div class="clearfix"></div>
                                <label for="" id="count">Duration Count</label>
                                <input style="width:71px;" type="text" class="restrict_duration_count" name="restrict_duration_count" id="restrict_duration_count" value=" {{  $service_master->restrict_duration_count ?? '' }} ">
                            </div>
                        </div> --}}


                        <div id="id" class="box-body clearfix">

                                <input type="hidden" name="reference_id" class="reference_id" >
                            <div class="col-md-12 padding_sm pull-right"  style="margin-bottom:10px;margin-top:10px;">
                                <input type="hidden" id="service_charge_id_hidden" name="service_charge_id_hidden" value='0' class="" >
                            <div class="col-md-4 padding_sm pull-right"  style="margin-bottom:10px;margin-top:10px;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-success saveButton" onclick="saveForm()"><i class="fa fa-save"></i> Save </button>
                            </div>
                            <div class="col-md-4 padding_sm pull-right" style="margin-top:10px;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block bg-blue clearFormButton" onclick="resetForm()"><i class="fa fa-times"></i> Clear </button>
                            </div>
                        </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/service_master.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
{{-- <script src="js/loadingoverlay.js"></script> --}}
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
