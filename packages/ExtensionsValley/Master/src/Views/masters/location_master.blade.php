@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
            <h4 class="blue text-left"> <strong>Location Master</strong></h4>
            <div class="col-md-8 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                           
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Location Name</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="location" value="">
                            <input type="hidden" value="" id="location_hidden">
                            <div class="ajaxSearchBox" id="locationAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-4 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Floor</label>
                                <div class="clearfix"></div>
                                <select class="form-control floor" name="floor" id="floor" >
                                    <option value="">--Select--</option>
                                    <option value="1">{{__('FIRST FLOOR')}}</option>
                                    <option value="2">{{__('SECOND FLOOR')}}</option>
                                    <option value="3">{{__('THIRD FLOOR')}}</option>
                                    <option value="4">{{__('FOURTH FLOOR')}}</option>
                                </select>
                            </div>
                            </div>
                            <div class="col-md-4 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Location Type</label>
                                <div class="clearfix"></div>
                                {{-- <input type="text" class="form-control branch_add" name="branch_add" id="branch_add" value="  "> --}}
                                <select class="form-control select2" id="location_type" name="location_type">
                                    <option  value="">Select Location Type</option>
                                    @foreach ($loc_type as $type)
                                    <option value="{{ $type->id }}">{{ $type->type_name }}</option>
    
                                @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Status</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control status" name="status" id="status">
                                        <option value="">All</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" ><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}" data-delete-url="{{ $delete_url }}"
                    data-restore-url="{{ $restore_url }}">

                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="box no-border" >
                    <div class="box-body clearfix" style=" height : 650px">
                        <h4 class="blue text-center"><span id="add"> Add Location  </span></h4>
                        <hr >
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Location Code</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control location_code_add" name="location_code_add" id="location_code_add" value=" {{$location_master->location_code?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Location Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control location_add" name="location_add" id="location_add" value=" {{$location_master->location_name ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Location Type</label>
                            <div class="clearfix"></div>
                            {{-- <input type="text" class="form-control branch_add" name="branch_add" id="branch_add" value="  "> --}}
                            <select class="form-control select2" id="location_type_add" name="location_type_add">
                                <option  value="">Select Location Type</option>
                                @foreach ($loc_type as $type1)
                                <option value="{{ $type1->id }}">{{ $type1->type_name }}</option>

                            @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Floor</label>
                            <div class="clearfix"></div>
                            <select class="form-control floor_add" name="floor_add" id="floor_add" >
                                <option value="">--Select--</option>
                                <option value="1">{{__('FIRST FLOOR')}}</option>
                                <option value="2">{{__('SECOND FLOOR')}}</option>
                                <option value="3">{{__('THIRD FLOOR')}}</option>
                                <option value="4">{{__('FOURTH FLOOR')}}</option>
                            </select>
                        </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Phone No</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control contact_no_add" name="contact_no_add" id="contact_no_add" value=" {{$location_master->contact_no?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Contact Person</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control contact_person_add" name="contact_person_add" id="contact_person_add" value=" {{$location_master->contact_person ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="status_add" name="status_add" id="status_add" value="1"> 
                            <label for="">Active</label>
                            <div id='status1'></div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="deals_pharmacy_item_add" name="deals_pharmacy_item_add" id="deals_pharmacy_item_add" value="1"> 
                            <label for="">Deal Pharmacy Item</label>
                            <div id='status1'></div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="deals_materials_item_add" name="deals_materials_item_add" id="deals_materials_item_add" value="1"> 
                            <label for="">Deal Materials Item</label>
                            <div id='status1'></div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="is_nursing_station_add" name="is_nursing_station_add" id="is_nursing_station_add" value="1"> 
                            <label for="">Is Nursing Station</label>
                            <div id='status1'></div>
                        </div>
                      
                        <input type="hidden" name="reference_id" class="reference_id" >
                        <div class="col-md-4 padding_sm pull-right"  style="margin-bottom:10px;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-success saveButton" onclick="saveForm()"><i class="fa fa-save"></i> Save </button>
                        </div>
                        <div class="col-md-4 padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-info clearFormButton" onclick="resetForm()"><i class="fa fa-times"></i> Clear </button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/location_master.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
{{-- <script src="js/loadingoverlay.js"></script> --}}
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
