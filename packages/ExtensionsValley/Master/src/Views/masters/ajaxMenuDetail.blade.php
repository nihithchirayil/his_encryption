
<?php $i=0; ?>
@if(isset($grid[0]))
<ul class="menu_container" id="parient_0">
    @foreach($grid[0] as $parient_id => $level_data)
   
    <li class="mainmenu" id="check_0">
        <div class="col-xs-1 w_4"><span style="float: right; ">{{++$i}}</span> <input type="hidden" name="menu_id[]" value="{{$parient_id}}"></div>
        <div class="col-xs-4 no-padding"><span>{{$level_data['name']}}</span> <span style="float:right;">@if(isset($grid[1][$parient_id]) && !empty($grid[1][$parient_id]))<i class="fa fa-minus-square red minusbtn" style="display: none;" id="minus_{{$parient_id}}" onclick="menudrop('{{$parient_id}}','minus')"></i> <i class="fa fa-plus-square green plusbtn" id="plus_{{$parient_id}}" onclick="menudrop('{{$parient_id}}','plus')"></i>@endif</span></div>
        <div class="col-xs-1" style="padding-left : 15px">
            <input class="select_check" data-parentid="0" data-menuid="{{$parient_id}}" name="select_check_update[{{$parient_id}}]" data-actiontype="has_select_check" id="select_check_{{$parient_id}}" type="checkbox" > 
        </div>

        @if($max_level != 0)
            <?php sub_category($max_level,$i,0,1,$parient_id,$grid); ?>
        @endif
    </li>
    @endforeach
</ul>
@endif

<?php function sub_category($level,$count_parent,$count_child,$sub_level,$parient,$grid_data) { ?>
    @if(isset($grid_data[$sub_level][$parient]) && !empty($grid_data[$sub_level][$parient]) )
    <ul class="subheading" id="parient_{{$parient}}">
        <?php $j = 0; $next_level = $sub_level + 1; ?>

        <?php 
        foreach ($grid_data[$sub_level][$parient] as $next_parent_id => $next_level_data) {

            ?>
        
        <li id="check_<?php echo $parient ?>" @if(!isset($grid_data[$next_level][$next_parent_id]) && empty($grid_data[$next_level][$next_parent_id])) style="width:100%; position:relative; display:inline-block;" @endif>
            @if($count_child != 0)
            <div class="col-xs-1 w_4"><span style="float: right;">{{$count_parent.".".$count_child.".".++$j}}</span> <input type="hidden" name="menu_id[]" value="{{$next_parent_id}}"></div>
            @else
            <div class="col-xs-1 w_4"><span style="float: right;">{{$count_parent.".".++$j}}</span> <input type="hidden" name="menu_id[]" value="{{$next_parent_id}}"></div>
            @endif
            <div class="col-xs-4 no-padding"><span>{{$next_level_data['name']}}</span> <span style="float:right;">@if(isset($grid_data[$next_level][$next_parent_id]) && !empty($grid_data[$next_level][$next_parent_id]))<i class="fa fa-minus-square red minusbtn" style="display: none;" id="minus_{{$next_parent_id}}" onclick="menudrop('{{$next_parent_id}}','minus')"></i> <i class="fa fa-plus-square green plusbtn" id="plus_{{$next_parent_id}}" onclick="menudrop('{{$next_parent_id}}','plus')"></i>@endif</span></div>
            <div class="col-xs-1" style="padding-left : 15px">
                <input class="select_check" data-parentid="{{$parient}}" data-menuid="{{$next_parent_id}}" name="select_check_update[{{$next_parent_id}}]" data-actiontype="has_select_check" id="select_check_{{$next_parent_id}}" type="checkbox"> 
            </div>

            @if($level != $sub_level)
                <?php sub_category($level,$count_parent,$j,$next_level,$next_parent_id,$grid_data); ?>
            @endif
        </li>
        <?php } ?>
    </ul>
    @endif
<?php }

?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".menu_container").children().find('.subheading').hide();
    });
</script>