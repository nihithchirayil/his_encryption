@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style> 
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.listNursingStationMaster')}}" id="nursingstationSearchForm" method="POST">
                            {!! Form::token() !!}    
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="name" id="nursingstation_name_search"
                                    value="{{ $searchFields['name'] ?? '' }}">
                            </div></div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Location Name</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($station_list) > 0)
                                    @foreach ($station_list as $station)
                                        <tr style="cursor: pointer;" onclick="stationEditLoadData(this,'{{$station->id}}',
                                                '{{$station->location_name}}','{{$station->building}}','{{$station->deals_pharmacy_item}}','{{$station->deals_materials_item}}'
                                                ,'{{$station->contact_person}}','{{$station->contact_no}}','{{$station->status}}')">
                                            <td class="station_name common_td_rules">{{ $station->location_name }}</td>
                                            <td class="status common_td_rules">{{ $station->status_name  }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="category_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.saveNursingStationMaster')}}" method="POST" id="nursingstationForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="station_id" id="station_id" value="0">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="location_name" id="location_name">
                            <div class="" id="category_code_status">
                            </div>
                        </div></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Building</label>
                            <div class="clearfix"></div>
                            {!! Form::select('building',$building, '', 
                                ['class'=>"form-control", 'id'=>"building",'placeholder' => 'Please select ...']) !!}
                        </div></div>
                        <div class="col-md-6 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Deals with pharmacy item</label>
                            <input type="checkbox" class="" name="deals_pharmacy_item" id="deals_pharmacy_item" value="1">
                        </div></div>
                        <div class="col-md-6 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Deals with materials item</label>
                            <input type="checkbox" class="" name="deals_materials_item" id="deals_materials_item" value="1">
                        </div></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Incharge name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="contact_person" id="contact_person">
                        </div></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Incharge Phone</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="contact_no" id="contact_no">
                        </div></div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Status</label>
                            <div class="clearfix"></div>
                            <select class="form-control" name="status" id="status">
                                <option value="1">{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                        </div></div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                           <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function () {
        // $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        

    });
    function stationEditLoadData(obj,id,name,building,deals_pharmacy_item,deals_materials_item,incharge_name,incharge_phone_no,status){
        var edit_icon_obj = $(obj);
        $('#station_id').val(id);
        $('#location_name').val(name);
        $('#building').val(building);
        $('#status').val(status);
        $("#contact_no").val(incharge_phone_no);
        $("#contact_person").val(incharge_name);
        if(deals_pharmacy_item==1){
            $("#deals_pharmacy_item").prop("checked",true);
        }else{
            $("#deals_pharmacy_item").prop("checked",false);
        }
        if(deals_materials_item==1){
            $("#deals_materials_item").prop("checked",true);
        }else{
            $("#deals_materials_item").prop("checked",false);
        }
        $('#nursingstationForm').attr('action', '{{$updateUrl}}');
    }

</script>
    
@endsection
