<div class="row">
    <div class="col-md-12" id="result_container_div">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                    style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='3%'>Sn. No.</th>
                        <th width='14%'>Doctor Name</th>
                        <th width='14%'>Token</th>
                        <th width='14%'>Visit Date</th>
                        <th width='14%'>Status</th>
                    
                    </tr>


                </thead>
                <tbody>
                    @if (count($res) != 0)
                        @foreach ($res as $data)
                            <tr>
                                <td class="td_common_numeric_rules">{{ $loop->iteration }}.</td>
                                <td class="common_td_rules">{{ $data->doctor_name }}</td>
                                <td class="common_td_rules">{{ $data->token_prefix }}</td>
                                <td class="common_td_rules">{{ $data->visit_datetime }}</td>
                                <td class="common_td_rules">{{ $data->file_status }}</td>
                            </tr>


                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
    </div>
</div>
