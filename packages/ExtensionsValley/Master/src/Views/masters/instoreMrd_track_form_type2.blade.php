@if ($offset == 0)

    <div class="col-md-12">
        <div class="theadscroll" id="load_data2" style="position: relative; height: 350px;">
            <table
                class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                style="border: 1px solid #CCC;">
                <thead>
                 
                    <tr class="table_header_bg">

                        <th scope="col" onclick="sorting(tbody01,0)" width="15%" style="cursor: pointer;">UHID<i
                                class=""></i></th>

                        <th width="10%" style="white-space: nowrap">Patient Name</th>
                        <th width="11%">Doctor Name</th>
                        <th width="8%">Visit Date</th>
                        <th width="8%">Visit Status</th>
                        <th width="8%">Token</th>
                        <th width="10%">Old UHID</th>
                        <th width="9%">Created By</th>
                        <th width="10%">Remarks</th>
                        <th width="8%">Action</th>
                    </tr>
                </thead>
                <tbody id="tbody01">
                    <?php
                    if (count($mrd_list) != 0) {
                        foreach ($mrd_list as $key => $lists) {
                            foreach ($lists as $list){
                            ?>
                    <tr class='generic{{ $list->mrd_id }}'
                        @if ($list->is_printed == 1) style="background:lightgreen" @endif>
                        <td class="common_td_rules">{{ $list->uhid }}</td>
                        <td class="common_td_rules">{{ $list->patient_name }}</td>
                        <td class="common_td_rules">{{ $list->doctor_name }}</td>
                        <td class="common_td_rules">{{ $list->visit_time }}</td>
                        <td class="common_td_rules">{{ $list->visit_type }}</td>
                        <td class="common_td_rules">{{ $list->token_prefix }}</td>
                        <td class="common_td_rules">{{ $list->ext_uhid }}</td>
                        <td class="common_td_rules">{{ $list->created_name }}</td>
                        <td class="common_td_rules">
                            <input type="text" class="form-control" id="remarks_{{ $list->mrd_id }}"
                                value="{{ $list->remarks }}">
                        </td>
                        <td class="text-center">
                            @if ($list->status == '1')
                                <button class='btn btn-success' type="button"
                                    onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                                        class="fa fa-download"></i>&nbsp;Receive</button>
                            @else
                                <button class='btn btn-warning' type="button"
                                    onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                                        class="fa fa-upload"></i>&nbsp;Dispatch</button>

                                        @if (intval($list->visit_count) > 1  ) 
                                        <button class='btn btn-info' type="button" id="visit_date_btn{{  $list->mrd_id }}"
                                        onclick="getVisitDetails({{ $list->mrd_id }},'{{ $list->patient_id }}','{{ $list->visit_time }}')"><i  id="visit_date_spin{{  $list->mrd_id }}"
                                            class="fa fa-info"></i></button>
                                        @endif

                            @endif
                        </td>
                    </tr>
                    <?php
                        }
                }
            }

                    ?>

                </tbody>
            </table>
        </div>
    </div>

    
@else
    <table>
        <tbody id="morningLoad">
            <?php
    if (count($mrd_list) != 0) {
        foreach ($mrd_list as $key => $lists) {
                     foreach($lists as $list)  {
            ?>

            <tr class='generic{{ $list->mrd_id }}'
                @if ($list->is_printed == 1) style="background:lightgreen" @endif>
                <td class="common_td_rules">{{ $list->uhid }}</td>
                <td class="common_td_rules">{{ $list->patient_name }}</td>
                <td class="common_td_rules">{{ $list->doctor_name }}</td>
                <td class="common_td_rules">{{ $list->visit_time }}</td>
                <td class="common_td_rules">{{ $list->visit_type }}</td>
                <td class="common_td_rules">{{ $list->token_no }}</td>

                <td class="common_td_rules">{{ $list->ext_uhid }}</td>
                <td class="common_td_rules">{{ $list->created_name }}</td>
                <td class="common_td_rules">
                    <input type="text" class="form-control" id="remarks_{{ $list->mrd_id }}"
                        value="{{ $list->remarks }}">
                </td>
                <td class="text-center">
                    @if ($list->status == '0')
                        <button class='btn btn-warning' type="button"
                            onclick="updateMrdTrack('{{ $list->mrd_id }}','{{ $list->status }}','{{ $list->patient_id }}')"><i
                                class="fa fa-upload"></i>&nbsp;Dispatch</button>
                    @endif
                </td>
            </tr>
            <?php
        }
        }
    }
    ?>
        </tbody>
    </table>
    <table>
      
@endif



<script>
    $('#load_data2').on('scroll', function() {
        var scrollHeight = $('#load_data2').height();
        //var scrollPosition = $('#load_data').height() + $('#load_data').scrollTop();
        var scrollPosition = $('#load_data2').scrollTop() + $('#load_data2').innerHeight();
        if (scrollPosition + 3 >= $('#load_data2')[0].scrollHeight) {
            //if (scrollPosition > scroll_length_initial) {
            //alert('ddd');
            offset = parseInt(offset) + limit;
            //limit = limit+15;
            //console.log(limit+'##'+offset+'##'+total_rec);
            if (offset < total_rec) {
                setTimeout(function() {
                    searchBill(limit, offset);
                }, 500);
                //searchList(limit,offset);
            }
        }
    })


    $('#load_data1').on('scroll', function() {
        var scrollHeight = $('#load_data1').height();
        //var scrollPosition = $('#load_data').height() + $('#load_data').scrollTop();
        var scrollPosition = $('#load_data1').scrollTop() + $('#load_data1').innerHeight();
        if (scrollPosition + 3 >= $('#load_data1')[0].scrollHeight) {
            //if (scrollPosition > scroll_length_initial) {
            //alert('ddd');
            offset = parseInt(offset) + limit;
            //limit = limit+15;
            //console.log(limit+'##'+offset+'##'+total_rec);
            if (offset < total_rec) {
                setTimeout(function() {
                    searchBill(limit, offset);
                }, 500);
                //searchList(limit,offset);
            }
        }
    })
</script>
