@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }
    fieldset {
  background: white;
  border: 5px ;
  border-color: black;
}

legend {
  padding: 10px;
  background: white;
  color: black;
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
        <h4 class="blue text-left"> <strong>Bed Master</strong></h4>
            <div class="col-md-8 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Bed Name</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="bed_name" value="">
                            <input type="hidden" value="" id="bed_name_hidden">
                            <div class="ajaxSearchBox" id="bed_nameAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Room Type</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" id="room_type" name="room_type">
                                    <option  value="">Select Room Type</option>
                                    @foreach ($room_type_name as $room_type)
                                    <option value="{{ $room_type->id }}">{{ $room_type->room_type_name }}</option>
    
                                @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Ward</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" id="ward" name="ward">
                                    <option  value="">Select Ward</option>
                                    @foreach ($ward_name as $ward)
                                    <option value="{{ $ward->id }}">{{ $ward->ward_name }}</option>
    
                                @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Nursing Location</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" id="nurse_loc" name="nurse_loc">
                                    <option  value="">Select Nursing Location</option>
                                    @foreach ($nursing_station as $nursing)
                                    <option value="{{ $nursing->id }}">{{ $nursing->location_name }}</option>
    
                                @endforeach
                                </select>
                                </div>
                            </div>
                         
                            <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Bed Status</label>
                                <div class="clearfix"></div>
                                <select class="form-control bed_status" name="bed_status" id="bed_status" >
                                    <option value="">All</option>
                                    <option value="1">Ready</option>
                                    <option value="2">Reserved</option>
                                    <option value="3">Occupied</option>
                                    <option value="4">Vacant</option>
                                    <option value="5">Blocked</option>
                                    <option value="6">House Keeping</option>
                                    <option value="7">Awaiting Checkout</option>
                                </select>
                            </div>
                            </div>
                            <div class="col-md-1 padding_sm" style="margin-block-start: 23px;">
                                <div class="clearfix"></div>
                                <input type="checkbox" class="status" name="status" id="status" value="1"> 
                                <label for="">Active</label>
                               
                            </div>
                       
                            <div class="col-md-2 padding_sm ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" style="margin-left:459px;"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}" data-delete-url="{{ $delete_url }}">

                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="box no-border" >
                    <div class="box-body clearfix" style=" height : 755px">
                        <h4 class="blue text-center"><span id="add"> Add Bed</span></h4>
                        <hr >
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Room Type</label>
                            <div class="clearfix"></div>
                            <select class="form-control select2" id="room_type_add" name="room_type_add">
                                <option  value="">Select Room Type</option>
                                @foreach ($room_type_name as $rooms)
                                <option value="{{ $rooms->id }}">{{ $rooms->room_type_name }}</option>

                            @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Ward</label>
                            <div class="clearfix"></div>
                            <select class="form-control select2" id="ward_add" name="ward_add">
                                <option  value="">Select Ward</option>
                                @foreach ($ward_name as $wards)
                                <option value="{{ $wards->id }}">{{ $wards->ward_name }}</option>

                            @endforeach
                            </select>
                            </div>
                        </div>
                            <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                <label for="">Nursing Location</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" id="nurse_loc_add" name="nurse_loc_add">
                                    <option  value="">Select Nursing Location</option>
                                    @foreach ($nursing_station as $nurse)
                                    <option value="{{ $nurse->id }}">{{ $nurse->location_name }}</option>

                                @endforeach
                                </select>
                                </div>
                            </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Floor</label>
                            <div class="clearfix"></div>
                            <select class="form-control floor" name="floor" id="floor" >
                                <option value="">--Select--</option>
                                <option value="1">{{__('FIRST FLOOR')}}</option>
                                <option value="2">{{__('SECOND FLOOR')}}</option>
                                <option value="3">{{__('THIRD FLOOR')}}</option>
                                <option value="4">{{__('FOURTH FLOOR')}}</option>
                            </select>
                        </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Bed Status</label>
                            <div class="clearfix"></div>
                            <select class="form-control bed_status_add" name="bed_status_add" id="bed_status_add" >
                                <option value="">---Select---</option>
                                <option value="1">Ready</option>
                                <option value="2">Reserved</option>
                                <option value="3">Occupied</option>
                                <option value="4">Vacant</option>
                                <option value="5">Blocked</option>
                                <option value="6">House Keeping</option>
                                <option value="7">Awaiting Checkout</option>
                            </select>
                        </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Bed Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control bed_name_add" name="bed_name_add" id="bed_name_add" value=" {{$bed_master->bed_name ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">Description</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control description" name="description" id="description" value=" {{$bed_master->description ?? '' }} ">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                            <label for="">No Of Occupancy</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control number_of_occupancy" name="number_of_occupancy" id="number_of_occupancy" value=" {{$bed_master->number_of_occupancy ?? '' }} ">
                            </div>
                        </div>
                      
                       <div class="col-md-3 padding_sm">
                        <div class="clearfix"></div>
                        <input type="checkbox" class="status_add" name="status_add" id="status_add" value="1"> 
                        <label for="">Active</label>
                       </div>
                        <div class="col-md-4 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="is_multiple" name="is_multiple" id="is_multiple" value="1"> 
                            <label for="">Multiple Bed</label>
                           
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class=" allow_retain" name="allow_retain" id="allow_retain" value="1"> 
                            <label for="">Allow Retain</label>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class=" er_bed" name="er_bed" id="er_bed" value="1"> 
                            <label for="">ER Bed</label>
                        </div>
                       
                        <div class="col-md-4 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class=" dummy_bed" name="dummy_bed" id="dummy_bed" value="1"> 
                            <label for="">Dummy Bed</label>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class=" consider_occupancy" name="consider_occupancy" id="consider_occupancy" value="1"> 
                            <label for="">Consider Occupancy</label>
                        </div>
                        <input type="hidden" name="reference_id" class="reference_id" >
                        <div class="col-md-4 padding_sm pull-right"  style="margin-bottom:10px;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-success saveButton" onclick="saveForm()"><i class="fa fa-save"></i> Save </button>
                        </div>
                        <div class="col-md-4 padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-block btn-info clearFormButton" onclick="resetForm()"><i class="fa fa-times"></i> Clear </button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/bed_master.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
{{-- <script src="js/loadingoverlay.js"></script> --}}
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
