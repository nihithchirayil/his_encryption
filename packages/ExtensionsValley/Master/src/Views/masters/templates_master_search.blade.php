<style>
    table tr:hover td{
        text-overflow: initial;
        white-space: normal;
    }
</style>
<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th style = "width:5%">SL.No.</th>
                <th width="40%">Template Label</th>
                <th width="30%">Control Name</th>
                <th width="10%">Status</th>
                <th style = "width:15%">Action</th>
            </tr>
        </thead>
        <tbody>
            @if(count($template_master) > 0)
                @foreach ($template_master as $templates)
                    <tr style="cursor: pointer;"  data-id="{{ $templates->id }}" data-content="{{json_encode($templates)}}">
                        <td class = "td_common_numeric_rules">{{($template_master->currentPage() - 1) * $template_master->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $templates->label}}</td>
                        <td class ="common_td_rules">{{ $templates->control_name }}</td>
                        <td class ="common_td_rules">{{ $templates->status_name}}</td>
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
