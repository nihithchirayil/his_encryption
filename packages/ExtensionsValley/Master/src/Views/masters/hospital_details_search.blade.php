<style>
    table tr:hover td{
        text-overflow: initial;
        white-space: normal;
    }
</style>

<div class="theadscroll" style="position: relative; height: 400px; cursor:pointer;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
      
        <thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="7%">Hospital Name</th>
                <th width="7%">Code</th>
                <th width="11%">Address</th>
                <th width="5%">Status</th>
                <th width="9%">Phone No</th>
                <th width="9%">Booking No</th>
                <th width="7%">Pincode</th>
                <th width="10%">Pancard No</th>
                <th width="5%">GST No</th>
                <th width="9%">Managing Director</th>
                <th width="9%">Vice President</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($hospital_details) > 0)       

                @foreach ($hospital_details as $hospital_detail)
                    <tr style="cursor: pointer;"  data-id="{{ $hospital_detail->id }}" data-name="{{ $hospital_detail->name }}" 
                         data-city="{{ $hospital_detail->city }}"  data-code="{{ $hospital_detail->code }}"  
                         data-hospital_header="{{ $hospital_detail->hospital_header }}" 
                        data-address="{{ $hospital_detail->address }}" data-pincode="{{ $hospital_detail->pincode }}" data-default_renewal_validity="{{ $hospital_detail->default_renewal_validity }}"
                        data-default_free_visits="{{ $hospital_detail->default_free_visits }}"  data-default_free_visits_after_discharge="{{ $hospital_detail->default_free_visits_after_discharge }}"
                        data-phone="{{ $hospital_detail->phone }}" data-status="{{ $hospital_detail->status_name }}" data-booking_no="{{ $hospital_detail->booking_no }}" 
                        data-gst_no="{{ $hospital_detail->gst_no }}"  data-tin_no="{{ $hospital_detail->tin_no }}"  data-pan_no="{{ $hospital_detail->pan_no }}"
                        data-drug_licence_no="{{ $hospital_detail->drug_licence_no }}" data-vice_president="{{ $hospital_detail->vice_president }}" data-state="{{ $hospital_detail->state }}" 
                        data-manage_director="{{ $hospital_detail->manage_director }}" data-country="{{ $hospital_detail->country }}">
                        <td class = "td_common_numeric_rules">{{($hospital_details->currentPage() - 1) * $hospital_details->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $hospital_detail->name }}</td>
                        <td class ="common_td_rules">{{ $hospital_detail->code }}</td>
                        <td class ="common_td_rules">{{ $hospital_detail->address }}</td>
                        <td class ="common_td_rules">{{ $hospital_detail->status}}</td>
                        <td class ="td_common_numeric_rules">{{ $hospital_detail->phone}}</td>
                        <td class ="td_common_numeric_rules">{{ $hospital_detail->booking_no}}</td>
                        <td class ="td_common_numeric_rules">{{ $hospital_detail->pincode}}</td>
                        <td class ="td_common_numeric_rules">{{ $hospital_detail->pan_no}}</td>
                        <td class ="td_common_numeric_rules">{{ $hospital_detail->gst_no}}</td>
                        <td class ="common_td_rules">{{ $hospital_detail->manage_director}}</td>
                        <td class ="common_td_rules">{{ $hospital_detail->vice_president}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-success " onclick="view(this);" ><i class="fa fa-eye"></i> View</button>

                            <button type="button" id="edit" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                        </td>
                       
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
