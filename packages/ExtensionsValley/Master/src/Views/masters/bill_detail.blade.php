@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>

    ul.pagination {
        margin:5px 0px !important;
    }

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">

<div class="right_col">
    <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
        <h4 class="blue text-left"> <strong>Bill Detail Master</strong></h4>
            <div class="col-md-12 no-padding">
                <div class="col-md-10 no-padding">
                    <div class="box no-border" >
                        <div class="box-body clearfix searchItemsDiv">
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Bill No</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="bill_no" value="">
                            <input type="hidden" value="" id="bill_no_hidden">
                            <div class="ajaxSearchBox" id="bill_noAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Bill Tag</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="bill_tag" value="">
                            <input type="hidden" value="" id="bill_tag_hidden">
                            <div class="ajaxSearchBox" id="bill_tagAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>
                    
                            <div class="col-md-1 padding_sm ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn" ><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}" data-action-url="{{ $action_url }}">

                    </div>
                </div>
            </div>
            <div id="datapopup" class="modal fade" role="dialog" >
                <div class="modal-dialog">
        
                    <!-- Modal content-->
                    <div  class="modal-content" style="width:1050px; display: inline-block;
                    vertical-align: middle;  margin-left:-173px;">
                        <div style=background-color:darkgreen; id="popup2"
                            class="modal-header modal_header_dark_purple_bg modal-header-sm">
                            <button type="button" class="close" onclick="reset1();" data-dismiss="modal">&times;</button>
                            <h4 align="center" class="modal-title titleName">Edit Bill Details</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div id="popup" class="theadscroll always-visible" style="position: relative;height: 255px; width:1000px">
                                        {{-- <h4 class="blue text-center"><span id="add"> Add Location  </span></h4> --}}
                                        <hr >
                                      
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Bill No</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control bill_no_add" name="bill_no_add" id="bill_no_add" value=" {{$bill_details->bill_no?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Bill Tag</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control bill_tag_add" name="bill_tag_add" id="bill_tag_add" value=" {{$bill_details->bill_tag?? '' }} ">
                                            </div>
                                        </div>
                                      
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Admitting Doctor Name</label>
                                            <div class="clearfix"></div>
                                            <select class="form-control select2" id="adm_doctor_id" name="adm_doctor_name">
                                                <option  value="">Select Admitting Doctor Name</option>
                                                @foreach ($adm_doctor as $adm)
                                                <option  value="{{ $adm->id }}">{{ $adm->doctor_name }}</option>
                
                                            @endforeach
                                            </select>
                                            </div>
                                        </div>
                               
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Consulting Doctor Name</label>
                                            <div class="clearfix"></div>
                                            <select class="form-control select2" id="cons_doctor_id" name="cons_doctor_name">
                                                <option  value="">Select Consulting Doctor Name</option>
                                                @foreach ($cons_doctor as $cons)
                                                <option  value="{{ $cons->id }}">{{ $cons->doctor_name }}</option>
                
                                            @endforeach
                                            </select>
                                            </div>
                                        </div>
                                      
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Bill Amount</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control bill_amount" name="bill_amount" id="bill_amount" value=" {{$bill_details->bill_amount ?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Paid Status</label>
                                            <div class="clearfix"></div>
                                            <select class="form-control status" name="status" id="status" >
                                                <option value="">Select</option>
                                                <option value="1">Paid</option>
                                                <option value="0">Unpaid</option>
                                            </select>
                                        </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Net Amount</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control net_amount" name="net_amount" id="net_amount" value=" {{$bill_details->net_amount ?? '' }} ">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-bottom:10px;">
                                            <div class="mate-input-box">
                                            <label for="">Location</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control location" name="location" id="location" value=" {{$bill_details->location ?? '' }} ">
                                            </div>
                                        </div>
                                      
                                  
                                        <input type="hidden" name="reference_id" class="reference_id">
                                      
                                </div>
                            </div>
                        </div>

                        <div class = "modal-footer">
                            <div class="col-md-2 padding_sm pull-right"  style="margin-bottom:10px;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="submit" class="btn btn-block btn-success saveButton" onclick="updateForm()"><i class="fa fa-save"></i> Update </button>
                            </div>
                            <div class="col-md-2 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="submit" class="btn btn-block btn-info clearFormButton" onclick="resetForm()"><i class="fa fa-times"></i> Clear </button>
                            </div>                           
                       </div>
                            
                        </div>
                       
                        </div>
           
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/bill_detail.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_csvprint.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
