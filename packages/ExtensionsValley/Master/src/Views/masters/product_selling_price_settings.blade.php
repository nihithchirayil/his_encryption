@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    .mate-input-box > label{
        margin-top:3px !important;
    }
    .ajaxSearchBox{
        display: block;
        z-index: 7000;
        margin-top: 15px;
    }
</style>
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.list_prodcut_selling')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Product</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="search_product"
                                    value="{{ $searchFields['product'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Batch No</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="search_batch_no"
                                    value="{{ $searchFields['batch_no'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Effective Date</label>
                                <div class="clearfix"></div>
                                <input type="text" data-attr="date" autocomplete="off" class="form-control date_picker" name="search_effective_date"
                                    value="{{ $searchFields['effective_date'] ?? '' }}">
                                </div>
                            </div>


                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Item</th>
                                    <th>Batch No</th>
                                    <th>Unit Selling Price</th>
                                    <th>Unit MRP</th>
                                    <th>New Unit Selling Price</th>
                                    <th>Effective Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($list) > 0)
                                    @foreach ($list as $list)
                                        <tr style="cursor: pointer;" onclick="editLoadData(this,
                                            '{{$list->id}}',
                                            '{{$list->product_name}}',
                                            '{{$list->product_code}}',
                                            '{{$list->batch_no}}',
                                            '{{$list->unit_selling_price}}',
                                            '{{$list->unit_mrp}}',
                                            '{{$list->new_unit_selling_price}}',
                                            '{{date('M-d-Y',strtotime($list->effective_date))}}',
                                            '{{$list->status}}',
                                            );" >
                                            <td class="product_id common_td_rules">{{ $list->product_name }}</td>
                                            <td class="mapping_type common_td_rules">{{ $list->batch_no }}</td>
                                            <td class="mapping_value common_td_rules">{{ $list->unit_selling_price }}</td>
                                            <td class="status common_td_rules">{{ $list->unit_mrp }}</td>
                                            <td class="status common_td_rules">{{ $list->new_unit_selling_price }}</td>
                                            <td class="status common_td_rules">{{ date('M-d-Y',strtotime($list->effective_date))}}</td>

                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="tax_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.save_prodcut_selling')}}" method="POST" id="mapForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="prodcut_selling_id" id="prodcut_selling_id" value="0">

                        <div class="col-md-12 padding_sm">
                           <div class="mate-input-box">
                            <label for="">Product <span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" name="prodcut_name" autocomplete="off" id="prodcut_name"
                                       class="form-control fav_grp"
                                       placeholder="Product Name" required onkeyup="searchProduct(this.id, event)">
                                       <div class="ajaxSearchBox" id="product_div" style=""></div>
                                       <input type="hidden" name="product_id" id="product_id">
                            <span class="error_red">{{ $errors->first('prodcut_name') }}</span>
                          </div>
                        </div>

                        <div class="col-md-12 padding_sm" style="margin-top:10px;">
                           <div class="mate-input-box">
                            <label for="">Batch No <span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" name="batch_no" autocomplete="off" id="batch_no"
                                       class="form-control fav_grp"
                                       placeholder="Batch No" required onkeyup="searchBatch(this.id, event)">
                                       <div class="ajaxSearchBox" id="batch_div" style=""></div>
                                       <input type="hidden" name="batch_id" id="batch_id">
                            <span class="error_red">{{ $errors->first('batch_name') }}</span>
                          </div>
                        </div>

                        <div class="col-md-12 padding_sm" style="margin-top:10px;">
                           <div class="mate-input-box">
                            <label for="">Unit Selling Price <span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" name="unit_selling_price" autocomplete="off" id="unit_selling_price"
                                class="form-control fav_grp"
                                placeholder="Unit Selling Price" required readonly>
                          </div>
                        </div>

                        <div class="col-md-12 padding_sm" style="margin-top:10px;">
                           <div class="mate-input-box">
                            <label for="">Unit MRP<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" name="unit_mrp" autocomplete="off" id="unit_mrp"
                                class="form-control fav_grp"
                                placeholder="Unit MRP" required readonly>
                          </div>
                        </div>

                        <div class="col-md-12 padding_sm" style="margin-top:10px;">
                           <div class="mate-input-box">
                            <label for="">New Unit Selling Price<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" name="new_unit_selling_price" autocomplete="off" id="new_unit_selling_price"
                                class="form-control fav_grp"
                                placeholder="New Unit Selling Price" required>
                          </div>
                        </div>

                        <div class="col-md-12 padding_sm" style="margin-top:10px;">
                           <div class="mate-input-box">
                            <label for="">Effective Date<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" data-attr="date" autocomplete="off" name="effective_date" autocomplete="off" id="effective_date"
                                class="form-control fav_grp date_picker"
                                placeholder="Effective Date" required>
                          </div>
                        </div>

                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">{{__('Active')}}</option>
                                    <option value="0">{{__('Inactive')}}</option>
                                </select>
                                <span class="error_red">{{ $errors->first('status') }}</span>
                                </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $('.date_picker').datetimepicker({
            format: 'MMM-DD-YYYY',
        });

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        $(document).on('change', '#mapping_type', function(e) {
                var mapping_type = $("#mapping_type").val();
                $('#mapping_value').val('');
                $('#mapping_value_id').val('');
        });
        $(document).on('change', '#type', function(e) {
                var mapping_type = $("#type").val();
                $('#value').val('');
                $('#value_id').val('');
        });

    });


    function searchProduct(id, event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var ajax_div = 'product_div';

        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            var prodcut = $('#' + id).val();
            if (prodcut == "") {
                $("#" + ajax_div).html("");
            } else {
                $.ajax({
                    type: "GET",
                    url: "",
                    data: 'prodcut_name=' + prodcut,
                    beforeSend: function () {
                        $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#" + ajax_div).html(html).show();
                        $("#" + ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
        }
    }

    function fillProductStockDetails(e, item_code, item_desc) {
                        $('#prodcut_name').val(item_desc);
                        $('#product_id').val(item_code);
                        $('#product_div').hide();
    }



    function searchBatch(id, event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var ajax_div = 'mapping_value_div';

        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            var batch_no = $('#' + id).val();
            var prodcut_id = $('#product_id').val();
            $('#batch_div').show();
            if(product_id == ''){
                alert("Please Select Item! ");
                return false;
            }
            if (batch_no == "") {
                $("#batch_div").html("");
            } else {
                $.ajax({
                    type: "GET",
                    url: "",
                    data: 'batch_no=' + batch_no + '&prodcut_id=' + prodcut_id,
                    beforeSend: function () {
                        $("#batch_div").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#batch_div").html(html).show();
                        $("#batch_div").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
        }
    }

    function fillBathDetails(e, name,unit_selling_price,unit_mrp) {
                        $('#batch_id').val(name);
                        $('#batch_no').val(name);
                        $('#unit_mrp').val(unit_mrp);
                        $('#unit_selling_price').val(unit_selling_price);
                        $('#batch_div').hide();
    }


    function editLoadData(obj,id,prodcut_name,product_code,batch_no,unit_selling_price,unit_mrp,new_unit_selling_price,effective_date,status){
        $('#prodcut_selling_id').val(id);
        $('#product_id').val(product_code);
        $('#prodcut_name').val(prodcut_name);
        $('#batch_no').val(batch_no);
        $('#unit_selling_price').val(unit_selling_price);
        $('#unit_mrp').val(unit_mrp);
        $('#new_unit_selling_price').val(new_unit_selling_price);
        $('#effective_date').val(effective_date);
        $('#status').val(status);
        $('#mapForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
         $('#mapForm')[0].reset();
    }
@include('Purchase::messagetemplate')
</script>

@endsection
