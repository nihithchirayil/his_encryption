@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
.new_one.mate-input-box {
    width: 100%;
    position: relative;
    padding: 15px 4px 4px 4px !important;
    border-bottom: 2px solid #01A881;
    box-shadow: 0 0 3px #CCC;
    border-radius: 6px 6px 0 0;
    margin-bottom: 10px;
    height: 90px !important;
}
</style>
@endsection
@section('content-area')
    <div class="right_col" role="main">

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <h2>{{$title}}</h2>
                </div>
            </div>
        </div>
        <?php
        $action = 'extensionsvalley.master.locationgroup';
        ?>
        <div class="x_panel">
            {!!Form::open(array('route' => $action, 'method' => 'post'))!!}
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                   <div class="row-fluid" style="border-bottom:1px solid #F4F6F5; background:#FBFDFC; padding:10px 15px 10px 15px;">
                    <div class="mate-input-box new_one">
                                        {!! Form::radio('assign', 1, true,['id' => 'assign_location']) !!} Location  {!! Form::radio('assign', 2,'',['id' => 'assign_group']) !!} Group
                                        <span class="clearfix"></span><br>
                                    <div id="locationDiv"> 
                                        {!! Form::select('location_id',array("0"=> " Choose Location") + ExtensionsValley\Master\Models\Location::getLocations()->toArray(), (request()->has('id')) ?  request()->get('id') : null, ['class' => 'select2','id' => 'location_id', 'style' => ' color:#555555; padding:4px 12px;']) !!}
                                    </div>
                                    <div id="groupDiv">
                                        {!! Form::select('group_id',array("0"=> " Choose Group") +  ExtensionsValley\Dashboard\Models\Group::getGroups()->toArray(), (request()->has('id')) ?  request()->get('id') : null, ['class' => 'select2','id' => 'group_id', 'style' => ' color:#555555; padding:4px 12px; display:none;']) !!}
                                    </div>
                                        <span class="clearfix"></span>
                    </div>
                </div>
                </div>
            </div>
          
            <div class="row">
                    <div id="parent" class="table-responsive theadscroll" style="position: relative; height: 550px;">
                    <table id="fixTable" class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                                        <thead>
                                           <tr class="table_header_bg">
                                                    <th style="width:15%" class="common_td_rules">#</th>
                                                    <th style="width:30%" class="common_td_rules">Names</th>
                                                    <th style="width:4%">Active</th>
                                            </tr>
                                        </thead>
                                        <tbody class="assignListData">
    @if(isset($resultData) && count($resultData))
    <?php $i = 1;  ?>
    @foreach($resultData as $data)
    <tr>

        <td class="common_td_rules">{{$i}}</td>
        <td class="common_td_rules">
            <span id="child_id-{{$i}}" style="border: none;">{{$data->name}}</span>
            <input type="hidden" class="ug_id" name="ug_id[]"  value="{{$data->location_group_id}}">
            <input type="hidden" class="ass_val" name="ass_val[]"  value="{{$ass_val}}">
        </td>
        <td title="">
            <input style="" type="checkbox" name="ass_ug_chk[]" class="ass_ug_chk" <?php if($data->id != 0){ 
            echo 'checked';
            }else{
            } ?>  >
            <input type="hidden" name="hidden_ass_ug_id[]" id="field_view-{{$i}}" value="{{$data->id}}" class="hidden_ass_ug_id" >
        </td> 
        
    </tr> 
    <?php $i++; ?>
    @endforeach
    @else
    <tr>
        <td colspan="3">No Items Found.</td>
    </tr>
    @endif
            </tbody>  
            </table>
            </div>
                    </div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });          
        @php 
            $is_ug = (request()->has('is_ug')) ?  request()->get('is_ug') : '';
        @endphp
        @if($is_ug == 'l')
            $('#groupDiv').hide();
            $('#group_id').val('0');
            $('#assign_location').prop('checked',true) ;
            $('#assign_group').prop('checked',false);
        @elseif($is_ug == 'g')
            $('#locationDiv').hide();
            $('#location_id').val('0');
            $('#assign_group').prop('checked',true);
            $('#assign_location').prop('checked',false) ;
        @else
            $('#groupDiv').hide();
            $('#group_id').val('0');
            $('#assign_location').prop('checked',true) ;
            $('#assign_group').prop('checked',false);
        @endif
        $('#assign_group').change(function () {
                if ($(this).prop('checked')) {
                    $('#locationDiv').hide();
                    $('#location_id').val('0');
                    $('#groupDiv').show();
                    document.getElementById('group_id').selectedIndex = 0;
                    $('.assignListData').hide();
                }else {
                    $('#locationDiv').show();
                    $('#groupDiv').hide();
                    $('#group_id').val('0');
                    $('.assignListData').show();
                }
        });
                        
        $('#assign_location').change(function () {
                if ($(this).prop('checked')) {
                    $('#groupDiv').hide();
                    $('#group_id').val('0');
                    $('#locationDiv').show();
                    document.getElementById('location_id').selectedIndex = 0;
                    $('.assignListData').hide();
                }else {
                    $('#groupDiv').show();
                    $('#locationDiv').hide();
                    $('#location_id').val('0');
                    $('.assignListData').show();
                }
        });
    $(document).off('change', '#location_id').on('change', '#location_id', function(){
        var location_id = jQuery('#location_id').val();
        console.log(location_id);
        if(!isNaN(parseInt(location_id)))
        {
            document.location.href = "?id=" + location_id + "&is_ug=l";
        }
    });
    $(document).off('change', '#group_id').on('change', '#group_id', function(){
        console.log('group_id');
        var group_id = jQuery('#group_id').val();
        if(!isNaN(parseInt(group_id)))
        {
            document.location.href = "?id=" + group_id + "&is_ug=g";
        }
    });

    $(document).on('keyup change', '.ass_ug_chk', function() {
            var status = ($(this).prop('checked'))?1:0;
             
            var ug_id = $(this).closest("tr").find('.ug_id').val().trim();
            var ass_val = $(this).closest("tr").find('.ass_val').val().trim();
            var table_ug_id = $(this).closest("tr").find('.hidden_ass_ug_id').val().trim();
            var ur_gp_id = '';
            if(ass_val == 'g')
                 ur_gp_id = $('#location_id').val().trim();
            if(ass_val == 'l')
                 ur_gp_id = $('#group_id').val().trim();
            var iD_input = $(this).closest("tr").find('.hidden_ass_ug_id').attr('id');
            // alert(iD_input);return false;
            var url = "";
            $.ajax({
                type: "GET",
                url: url,
                data: "ur_gp_id="+ur_gp_id+"&ug_id="+ug_id+"&status="+status +"&ass_val="+ ass_val+"&table_ug_id=" + table_ug_id,
                beforeSend: function () { },
                success: function (data) { 
                    toastr.success(data.message);
                    $("#"+iD_input).val(data.table_ug_id);
                },
                complete: function () { }
            });
        });


   });

</script>
@stop

