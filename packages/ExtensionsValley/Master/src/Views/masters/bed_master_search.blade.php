<style>
    table tr:hover td{
        text-overflow: initial;
        white-space: normal;
    }
</style>
<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
     
            <tr class="table_header_bg">
                <th style="width:1%">SL.No.</th>
                <th width="5%">Bed</th>
                <th width="15%">Nursing Location</th>
                <th width="11%">Ward</th>
                <th width="11%">Room Type</th>
                <th width="11%">Allow Retain</th>
                <th width="9%">Is Multiple</th>
                <th width="11%">Dummy Bed</th>
                <th width="5%">Status</th>
                <th style ="width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            
            @if(count($bed_master) > 0)
                @foreach ($bed_master as $bed)
                    <tr style="cursor: pointer;"  data-id="{{ $bed->id }}" data-room_type_name="{{ $bed->room_type_id }}" data-nurse_loc="{{ $bed->nurse_station_id}}" 
                        data-bed_name="{{ $bed->bed_name }}"   data-ward_name="{{$bed->ward_id }}" 
                        data-description="{{ $bed->description }}" data-bed_status="{{ $bed->bed_status }}"  data-floor_name="{{ $bed->building_floor_id }}" 
                        data-retain="{{ $bed->retain }}" data-occupancy="{{ $bed->occupancy }}"  data-er_bed="{{ $bed->er_bed }}" 
                         data-multiple="{{ $bed->multiple }}"  data-dummy_bed="{{ $bed->dummy_bed }}" 
                         data-number_of_occupancy="{{ $bed->number_of_occupancy }}"  data-status="{{ $bed->status_name }}">
                        <td class = "td_common_numeric_rules">{{($bed_master->currentPage() - 1) * $bed_master->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $bed->bed_name }}</td>
                        <td class ="common_td_rules">{{ $bed->location_name}}</td>
                        <td class ="common_td_rules">{{ $bed->ward_name}}</td>
                        <td class ="common_td_rules">{{ $bed->room_type_name}}</td>
                        <td class ="common_td_rules">{{ $bed->allow_retain}}</td>
                        <td class ="common_td_rules">{{ $bed->is_multiple}}</td>
                        <td class ="common_td_rules">{{ $bed->is_dummy_bed}}</td>
                        <td class ="common_td_rules">{{ $bed->status}}</td>
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
