<style>
    table tr:hover td{
        text-overflow: initial;
        white-space: normal;
    }
</style>
<div class="theadscroll" style="position: relative; height: 400px;">
    <div  aria-hidden="true" class="floatThead-container" style="overflow: hidden; padding-left: 0px; padding-right: 0px; position: absolute; margin-top: 0px; top: 0px; z-index: 1001; left: 0px; width: 100px;">
        <div  class="panel-body" style="min-height: 300px;">
<table id="result_data_table" class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
style="border: 1px solid #CCC;">
<thead>
     
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="9%">Bill No</th>
                <th width="7%">Bill Tag</th>
                <th width="23%">Admitting Doctor Name</th>
                <th width="23%">Consulting Doctor Name</th>
                <th width="7%">Bill Amount</th>
                <th width="7%">Paid Status</th>
                <th width="7%">Net Amount</th>
                <th width="5%">Location</th>
                <th style = "width:17%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($bill_details) > 0)
                @foreach ($bill_details as $bill_detail)
                    <tr style="cursor: pointer;"  data-id="{{ $bill_detail->id }}" data-bill_no="{{ $bill_detail->bill_no }}" 
                        data-bill_tag="{{ $bill_detail->bill_tag}}" data-admitting_doctor_name="{{ $bill_detail->admitting_doctor_id }}" 
                        data-consulting_doctor_name="{{$bill_detail->consulting_doctor_id }}" data-net_amount="{{ $bill_detail->net_amount }}" 
                        data-bill_amount="{{ $bill_detail->bill_amount }}" data-location="{{ $bill_detail->location }}" data-paid_status="{{ $bill_detail->status}}">
                        <td class = "td_common_numeric_rules">{{($bill_details->currentPage() - 1) * $bill_details->perPage() + $loop->iteration}}</td>
                        <td class ="common_td_rules">{{ $bill_detail->bill_no }}</td>
                        <td class ="common_td_rules">{{ $bill_detail->bill_tag}}</td>
                        <td class ="common_td_rules">{{ $bill_detail->admitting_doctor_name}}</td>
                        <td class ="common_td_rules">{{ $bill_detail->consulting_doctor_name}}</td>
                        <td class ="td_common_numeric_rules">{{ $bill_detail->bill_amount}}</td>
                        <td class ="common_td_rules">{{ $bill_detail->paid_status}}</td>
                        <td class ="td_common_numeric_rules">{{ $bill_detail->net_amount}}</td>
                        <td class ="common_td_rules">{{ $bill_detail->location}}</td>
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            <button type="button" id="button" class="btn btn-sm btn-success " onclick="view(this);" ><i class="fa fa-eye"></i> View</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
</div>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
