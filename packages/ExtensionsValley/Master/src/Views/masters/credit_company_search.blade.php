<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th style = "width:1%">SL.No.</th>
                <th width="7%">Company Code</th>
                <th width="17%">Company Name</th>
                <th width="11%">Company Type</th>
                <th width="11%">Address</th>
                <th width="11%">Phone No</th>
                <th width="9%">Fax No</th>
                <th width="11%">Email Address</th>
                <th width="7%">ESI</th>
                <th width="7%">Status</th>
                <th style = "width:19%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(count($credit_company) > 0)
                @foreach ($credit_company as $company)
                    <tr style="cursor: pointer;"  data-id="{{ $company->id }}" data-company_code="{{ $company->company_code }}" data-company_name="{{ $company->company_name }}" 
                        data-company="{{ $company->company }}"   data-address="{{ $company->address }}" data-fax_no="{{ $company->fax_no }}" data-email_addr="{{ $company->email_addr }}"
                        
                        data-tel_no="{{ $company->tel_no }}" data-esi="{{ $company->eee }}"  data-status="{{ $company->status_name }}">
                        <td class = "td_common_numeric_rules">{{($credit_company->currentPage() - 1) * $credit_company->perPage() + $loop->iteration}}</td>
                        <td class ="td_common_numeric_rules">{{ $company->company_code }}</td>
                        <td class ="common_td_rules">{{ $company->company_name}}</td>
                        <td class ="common_td_rules">{{ $company->company_type}}</td>
                        <td class ="common_td_rules">{{ $company->address}}</td>
                        <td class ="td_common_numeric_rules">{{ $company->tel_no}}</td>
                        <td class ="td_common_numeric_rules">{{ $company->fax_no}}</td>
                        <td class ="common_td_rules">{{ $company->email_addr}}</td>
                        <td class ="common_td_rules">{{ $company->esi}}</td>
                        <td class ="common_td_rules">{{ $company->status}}</td>
                        {{-- <td>{{ $document->created_at }}</td> --}}
                        <td>
                            <button type="button" id="button" class="btn btn-sm btn-warning " onclick="editItem(this);" ><i class="fa fa-edit"></i> Edit</button>
                            {{-- <button type="button" class="btn btn-sm btn-danger " onclick="deleteItem(this);" ><i class="fa fa-trash"></i> Delete</button> --}}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" >No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
