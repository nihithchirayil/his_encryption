@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

    <style>
        ul.pagination {
            margin: 5px 0px !important;
        }

        .disabled {
            color: #999;
        }

        .enabled {
            color: #000000;
        }

        .mate-input-box input.form-control {
            margin-top: 8px;
        }
    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="token" value="{{ csrf_token() }}">

    <div class="right_col">
        <div class="row" style="margin-bottom: 15px;"> </div>
        <div class="row codfox_container">
            <h4 class="blue text-left"> <strong>Template Master</strong></h4>
            <div class="col-md-8 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix searchItemsDiv">
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Select Control</label>
                                    <select class="form-control select2" style="color:#555555; padding:4px 12px;"
                                        id="search_temp_ctrl"name="search_temp_ctrl">
                                        <option value="">Select</option>
                                        @foreach ($template_control as $data)
                                            <option value="{{ $data->id }}">{{ $data->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Search name</label>
                                    <input type="text" class="form-control" name="search_ctrl_name" id="search_ctrl_name"/>
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm pull-right ">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-primary searchBtn"><i
                                        class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix table_body_contents" data-search-url="{{ $search_url }}"
                        data-action-url="{{ $action_url }}">
                    </div>
                </div>
            </div>


            <div class="col-md-4 add_service_area">
                <div class="box no-border">
                    <div class="box-body clearfix" style="/*height :545px; */">
                        <h4 class="blue text-center"><span id="add"> Add Template </span></h4>
                        <hr>
                        <div class="col-md-12 padding_sm label_content" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                                <label for="">Label</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control template_label_add" name="template_label_add"
                                    value="">
                            </div>
                        </div>

                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                                <label for="">Control</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" style="color:#555555; padding:4px 12px;"
                                    onchange="hideControlContents()" id="template_control_add" name="template_control_add">
                                    <option value="">Select</option>
                                    @foreach ($template_control as $data)
                                        <option attr_id="{{ $data->is_control }}" value="{{ $data->id }}">
                                            {{ $data->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="dataset_datadiv">
                            <div class="col-md-12 padding_sm dataSourceDiv">
                                <div class="mate-input-box">
                                    <label for="">Data Source</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" onchange="hideDataSource();" id="dataSource">
                                        <option value="">Select</option>
                                        <option value="1">Data Set</option>
                                        <option value="2">Query</option>
                                        <option value="3">Array</option>
                                        <option value="4">DataPoint (Ip Emr)</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 padding_sm  data_point_content hidden"
                                style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                    <label for="">Default Data Point</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('data_point', $master_data_point, 0, [
                                        'class' => 'form-control select2',
                                        'placeholder' => '',
                                        'placeholder' => 'Select',
                                        'id' => 'data_point',
                                        'style' => 'color:#555555; padding:4px 12px;',
                                    ]) !!}
                                </div>
                            </div>

                            <div class="col-md-12 padding_sm template_datasetdiv dataset_content hidden" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                    <label for="">Data Set</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" onchange="getDataSetContent(1)"
                                        id="template_data_set" name="template_data_set">
                                        <option value="">Select</option>
                                        @foreach ($template_dataset as $key => $val)
                                            <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm template_datasetDetailDiv dataset_content hidden"
                                style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                    <label id="dataset_name" for="">NA</label>
                                    <input type="hidden" id="datset_data_edit_val" value="">
                                    <div class="clearfix"></div>
                                    <select id="datset_data_div" class="form-control select2">

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm template_defaultdatasetdiv dataset_content hidden"
                                style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                    <label for="">Default Data Set</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('template_default_data_set', $master_dataset, 0, [
                                        'class' => 'form-control select2',
                                        'placeholder' => '',
                                        'placeholder' => 'Select',
                                        'id' => 'template_default_data_set',
                                        'style' => 'color:#555555; padding:4px 12px;',
                                        'onchange' => 'getDataSetContent(2)',
                                    ]) !!}
                                </div>
                            </div>

                            <div class="col-md-12 padding_sm sql_content hidden" style="margin-bottom:10px;">
                                <div class="mate-textarea">
                                    <label for="">Sql</label>
                                    <div class="clearfix"></div>
                                    <textarea class="form-control sql_add" name="sql_add" value=""></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm group_content hidden" style="margin-bottom:10px;">
                                <div class="mate-textarea">
                                    <label for="">Group Value</label>
                                    <div class="clearfix"></div>
                                    <textarea class="form-control group_value_add" name="group_value_add" value=""></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 padding_sm class_content" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                                <label for="">Class</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control template_class_add" name="template_class_add"
                                    value="">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm placeholder_content" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                                <label for="">Placeholder</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control template_placeholder_add"
                                    name="template_placeholder_add" value="">
                            </div>
                        </div>

                        <div class="col-md-12 padding_sm template_save_master_div" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                                <label for="">Master Tables</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" onchange="getTemplateMasterData()"
                                    id="template_save_master" name="template_save_master">
                                    <option value="">Select</option>
                                    @foreach ($template_save_master as $key => $val)
                                        <option value="{{ $val }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="dataset_datadiv hidden">
                            <div class="col-md-12 padding_sm default_datasaving_primarykey" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                    <label for="">Data Set Primary Key</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control template_data_saving_primary_key" name="template_data_saving_primary_key"
                                        value="">
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm template_emrkeydiv" style="margin-bottom:10px;">
                                <div class="mate-input-box">
                                    <label for="">Data Set Emr Key</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" id="template_compare_key_in_emr" name="template_compare_key_in_emr">
                                        <option value="">Select</option>
                                        @foreach ($template_emr_key as $key => $val)
                                        <option value="{{ $val }}">{{ $val }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 padding_sm template_save_master_detail_div hidden"
                            style="margin-bottom:10px;">
                            <div class="mate-input-box">
                                <label id="template_save_table_detail_header" for="">NA</label>
                                <input type="hidden" id="coloumn_name_edit" value="">
                                <div class="clearfix"></div>
                                <select id="template_save_table_detail" class="form-control select2">

                                </select>
                            </div>
                        </div>


                        <div class="col-md-12 padding_sm default_content" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                                <label for="">Default Value</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control default_value_add" name="default_value_add"
                                    value="">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-bottom:10px;">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Size</label>
                                    <div class="clearfix"></div>
                                    <?php
                                    $sizes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
                                    ?>
                                    <select class="form-control size_add" id="size_add" name="size_add">
                                        <option value="0">0</option>
                                        @foreach ($sizes as $size)
                                            <option value="{{ $size }}">{{ $size }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Container Height</label>
                                    <div class="clearfix"></div>
                                    <input type="text"
                                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                                        class="form-control container_min_add" name="container_min_add" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm hide_table_content" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                                <label for="">Table Header</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control table_header_add" name="table_header_add"
                                    value="">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm hide_table_content" style="margin-bottom:10px;">
                            <div class="mate-input-box">
                                <label for="">Table Column Names</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control colum_names_add" name="colum_names_add"
                                    value="">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm hide_table_content" style="margin-bottom:10px;">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Total Rows</label>
                                    <div class="clearfix"></div>
                                    <input type="text"
                                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                                        class="form-control row_count_add" name="row_count_add" value="">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Total Columns</label>
                                    <div class="clearfix"></div>
                                    <input type="text"
                                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                                        class="form-control colum_count_add" name="colum_count_add" value="">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" checked class="status_add" name="status_add" id="status_add"
                                value="1">
                            <label>Active</label>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="is_favourite" name="is_favourite" id="is_favourite"
                                value="1">
                            <label>Is Favourite</label>
                        </div>
                        <div class="col-md-4 no-padding">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="is_primary_component" name="is_primary_component"
                                id="is_primary_component" value="1">
                            <label>Primary Component </label>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="hide_in_template" name="hide_in_template"
                                id="hide_in_template" value="1">
                            <label>Hide In Template</label>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="is_readonly" name="is_readonly" id="is_readonly"
                                value="1">
                            <label>Is Readonly</label>
                        </div>
                        <div class="col-md-4 no-padding">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="is_static_component" name="is_static_component"
                                id="is_static_component" value="1">
                            <label>Is Static Component</label>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="hide_in_print" name="hide_in_print" id="hide_in_print"
                                value="1">
                            <label>Hide In Print</label>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="clearfix"></div>
                            <input type="checkbox" class="is_mandatory" name="is_mandatory" id="is_mandatory"
                                value="1">
                            <label>Is Mandatory</label>
                        </div>
                    </div>

                    <div id="id" class="box-body clearfix">
                        <input type="hidden" name="reference_id" class="reference_id">
                        <div class="col-md-12 padding_sm pull-right" style="margin-bottom:10px;margin-top:10px;">
                            <input type="hidden" id="service_charge_id_hidden" name="service_charge_id_hidden"
                                value='0' class="">
                            <div class="col-md-4 padding_sm pull-right" style="margin-bottom:10px;margin-top:10px;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-success saveButton"
                                    onclick="saveForm()"><i class="fa fa-save"></i> Save </button>
                            </div>
                            <div class="col-md-4 padding_sm pull-right" style="margin-top:10px;">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block bg-blue clearFormButton"
                                    onclick="resetForm()"><i class="fa fa-times"></i> Clear </button>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- /page content -->

@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/js/templates_master.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>

    <script type="text/javascript"></script>
@endsection
