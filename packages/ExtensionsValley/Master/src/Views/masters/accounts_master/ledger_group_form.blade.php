@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')

<div class="right_col">
    <input type="hidden" name="base_url" id="base_url" value="{{ url('/') }}" />
    <input type="hidden" name="base_url" id="ins_base_url" value="{{ url('/') }}" />
    <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
    <input type="hidden" id="save_url" value="save_ledger_group">
    <div class="row" style="text-align: right;padding-right: 80px;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix" style="box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF !important;">
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Group Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="group_names" id="group_name"
                                        value="{{ $searchFields['group_names'] ?? '' }}" autocomplete="off"
                                        onkeyup="select_group_names(this.id, event,1)">
                                    <input type="hidden" class="form-control" name="hidden_group_names_id"
                                        id="hidden_group_names_id"
                                        value="{{ $searchFields['hidden_group_names_id'] ?? '' }}">
                                    <div class="ajaxSearchBox" id="search_group" style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                             margin: 14px -4px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                </div>

                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Status</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="status" id="status">
                                    <option value="">{{__('Select')}}</option>
                                        <option value="1">{{__('Active')}}</option>
                                        <option value="0">{{__('Inactive')}}</option>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="col-md-4 padding_sm"
                                style="padding-right: 2px !important;padding-left: 2px !important;width: 18%;">
                                <?php
                                // if(isset($searchFields['is_asset_search']) && $searchFields['is_asset_search'] ==1 ){
                                //     $checked_chk = 'checked';
                                // }else{
                                //     $checked_chk = '';
                                // }
                                ?>
                                <div class="mate-input-box">
                                    <span for="">Asset/Liability</span>
                                    &nbsp; <input type="checkbox" class="" name="is_asset_search" id="is_asset_search"
                                        value="1" />
                                </div>
                            </div> -->


                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <span class="btn btn-block light_purple_bg" id="searchGroupBtn" onclick="searchLedgerGroup()"><i
                                        class="fa fa-search" id="searchGroupBtnSpin" ></i>
                                    Search</span>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i
                                        class="fa fa-times"></i> Clear</a>
                            </div>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                        <div class="clearfix"></div>
                        <div class="box no-border no-margin">
                            <div class="box-footer" id="searchLedgerGroupDataDiv"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 525px;">

                            </div>
                        </div>
                    </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix" id="saveLedgerBlck">
                    <input type="hidden" name="ledger_group_id" id="ledger_group_id" value="">
                    <div class="col-md-12 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Group Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="group_name" id="group_name_update"
                                autocomplete="off">
                            <span style="color: #d14;"> {{ $errors->first('group_name') }}</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Parent Group</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="ledger_group_parent_name"
                                id="ledger_group_parent_name" value="" autocomplete="off"
                                onkeyup="select_group_names(this.id, event,2)">
                            <input type="hidden" class="form-control" name="ledger_group_parent_id"
                                id="ledger_group_parent_id" value="">
                            <div class="ajaxSearchBox" id="search_group_update" style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                             margin: 13px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-6 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Account No</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="ledger_code" id="ledger_code" value="">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Status</label>
                            <div class="clearfix"></div>
                            <select class="form-control" name="status" id="status_update">
                                <option value="1">{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" >
                        <div class="mate-input-box">
                            <label for="">Payment Group</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" class="" name="custom_payment_flag" id="custom_payment_flag" value="1">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="display:none">
                        <div class="mate-input-box">
                            <label for="">Is Asset/Liability</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" class="" name="is_asset" id="is_asset" value="1">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>

                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-6 padding_sm">
                        <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                            Cancel</a>
                    </div>
                    <div class="col-md-6 padding_sm">
                        <span class="btn btn-block light_purple_bg" onclick="validate()" id="saveLedgerGroupBtn"><i class="fa fa-save"></i>
                            Save</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="getAuditLog" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 80%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <span class="modal-title" id="audit_log_header"></span>
            </div>
            <div class="modal-body" style="min-height:400px;height:500px;overflow: auto" id="audit_log_body">

            </div>
        </div>
    </div>
</div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/audit_log.js")}}"></script>

<script type="text/javascript">
var base_url = $('#base_url').val();
var token = $('#token_hiddendata').val();
$(document).ready(function() {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }

    });
    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
    searchLedgerGroup();
});

function searchLedgerGroup() {
    var url = base_url + "/master/searchLedgerGroup";
    var group_name = $('#group_name').val();
    var hidden_group_names_id = $('#hidden_group_names_id').val();
    var status = $('#status').val();
    var param = {
        _token: token,
        group_name: group_name,
        hidden_group_names_id: hidden_group_names_id,
        status: status
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#searchLedgerGroupDataDiv").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
            $('#searchGroupBtn').attr('disabled', true);
            $('#searchGroupBtnSpin').removeClass('fa fa-search');
            $('#searchGroupBtnSpin').addClass('fa fa-spinner fa-spin');
        },
        success: function(data) {
            $('#searchLedgerGroupDataDiv').html(data.html);
            // $('#total_cntspan').html(data.total_records);
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30

            });
            setTimeout(function() {
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
            }, 400);
        },
        complete: function() {
            $("#searchLedgerGroupDataDiv").LoadingOverlay("hide");
            $('#searchGroupBtn').attr('disabled', false);
            $('#searchGroupBtnSpin').removeClass('fa fa-spinner fa-spin');
            $('#searchGroupBtnSpin').addClass('fa fa-search');
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function ledgerEditLoadData(id, name, status, is_asset, parent_id, ledger_group_parent_name, ledger_code,custom_payment_flag) {
    $('#ledger_group_id').val(id);
    $('#group_name_update').val(name);
    $('#status_update').val(status);
    $("#ledger_group_parent_id").val(parent_id);
    $("#ledger_group_parent_name").val(ledger_group_parent_name);
    $("#ledger_code").val(ledger_code);
    if (custom_payment_flag == 1) {
        $("#custom_payment_flag").prop("checked", true);
    } else {
        $("#custom_payment_flag").prop("checked", false);
    }
    $('#save_url').val('{{$updateUrl}}');
}

function validate() {
    var save_url = $("#save_url").val();
    var url = base_url + "/master/"+save_url;
    var group_name = $('#group_name_update').val();
    var ledger_group_parent_name = $('#ledger_group_parent_name').val();
    var ledger_group_parent_id = $('#ledger_group_parent_id').val();
    var ledger_code = $('#ledger_code').val();
    var ledger_group_id = $("#ledger_group_id").val();
    if( $('#custom_payment_flag').is(":checked") ){
        custom_payment_flag = 1;
    }else{
        custom_payment_flag = 0
    }
    var status_update = $('#status_update').val();
    var param = {
        _token: token,
        group_name: group_name,
        ledger_group_parent_name: ledger_group_parent_name,
        ledger_group_parent_id: ledger_group_parent_id,
        ledger_code: ledger_code,
        custom_payment_flag: custom_payment_flag,
        status_update: status_update,
        ledger_group_id:ledger_group_id
    };
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        beforeSend: function() {
            $("#saveLedgerBlck").LoadingOverlay("show", {
                background: "rgba(255, 255, 255, 0.7)",
                imageColor: '#009869'
            });
            $('#saveLedgerGroupBtn').attr('disabled', true);
        },
        success: function(data) {
            if(data.status == '1'){
                toastr.success(data.message);
                setTimeout( function(){
                window.location.reload();
            }, 300);
            }else if(data.status == '2'){
                toastr.success(data.message);
            }else{
                toastr.error(data.message);
            }


        },
        complete: function() {
            $("#saveLedgerBlck").LoadingOverlay("hide");
            $('#saveLedgerGroupBtn').attr('disabled', false);
        },
        error: function() {
            toastr.error("Error Please Check Your Internet Connection");
        }
    });
}

function select_group_names(id, event, typ) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (typ == 1) {
        var ajax_div = 'search_group';
        var ledger_desc = $("#group_name").val();
    } else {
        var ajax_div = 'search_group_update';
        var ledger_desc = $("#ledger_group_parent_name").val();
    }
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {

        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: "",
                data: 'group_name=' + ledger_desc + '&search_group_name=1&types=' + typ,
                beforeSend: function() {
                    $("#" + ajax_div).html(
                        '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                    ).show();
                },
                success: function(html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function() {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}

function fillAssetGroupValues(e, ledger_name, id, types, is_asset) {
    if (types == 1) {
        $("#group_name").val(ledger_name.replace(/&amp;/g, '&'));
        $("#hidden_group_names_id").val(id);
    } else {
        $("#ledger_group_parent_name").val(ledger_name.replace(/&amp;/g, '&'));
        $("#ledger_group_parent_id").val(id);
        if (is_asset == 1) {
            $("#is_asset").prop("checked", true);
        } else {
            $("#is_asset").prop("checked", false);
        }
    }
}
</script>

@endsection
