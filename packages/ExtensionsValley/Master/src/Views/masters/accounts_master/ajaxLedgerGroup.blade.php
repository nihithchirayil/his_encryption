<script type="text/javascript">
$(document).ready(function() {
    $(".page-link").click(function() {
        var url = $(this).attr("href");
        if (url && url != 'undefined') {
            var group_name = $('#group_name').val();
            var hidden_group_names_id = $('#hidden_group_names_id').val();
            var status = $('#status').val();
            var param = {
                _token: token,
                group_name: group_name,
                hidden_group_names_id: hidden_group_names_id,
                status: status
            };

            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $("#searchLedgerGroupDataDiv").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                    $('#searchGroupBtn').attr('disabled', true);
                    $('#searchGroupBtnSpin').removeClass('fa fa-search');
                    $('#searchGroupBtnSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchLedgerGroupDataDiv').html(data.html);
                    // $('#total_cntspan').html(data.total_records);
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30

                    });
                    setTimeout(function() {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                },
                complete: function() {
                    $("#searchLedgerGroupDataDiv").LoadingOverlay("hide");
                    $('#searchGroupBtn').attr('disabled', false);
                    $('#searchGroupBtnSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchGroupBtnSpin').addClass('fa fa-search');
                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }
        return false;
    });

});
</script>
<div class="theadscroll" style="position: relative; height: 440px;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th>Group Name</th>
                <th>Account No</th>
                <th>Status</th>
                <th class="text-center">Edit</th>
                <th class="text-center"><i class="fa fa-history"></th>
            </tr>
        </thead>
        <tbody>
            @if(count($ledger_group_master_list) > 0)
            @foreach ($ledger_group_master_list as $list)
            @if($list->ledger_group_parent_id !='')
            @php $bg_color ="background:#d2e3e2;"; @endphp
            @else
            @php $bg_color =""; @endphp
            @endif
            <tr style="">
                <td class="category_name common_td_rules">{{ $list->group_name }}</td>
                <td class="category_name common_td_rules">{{ $list->ledger_code }}</td>
                <td class="status common_td_rules">{{ $list->status_name  }}</td>
                <td class="text-center">
                    <i style="cursor:pointer"
                        onclick="ledgerEditLoadData('{{$list->group_id}}','{{$list->group_name}}',
                                           '{{$list->status}}','{{$list->is_asset}}','{{$list->ledger_group_parent_id}}','{{$list->ledger_group_parent_name}}','{{ $list->ledger_code }}','{{ $list->custom_payment_flag }}')"
                        class="fa fa-edit"></i>
                </td>
                <td class="text-center  common_td_rules">
                    @if($list->audit_status ==1)
                    <i style="cursor:pointer;color: #f0ad4e" id="history_spin_{{$list->group_id}}"
                        onclick="viewAuditLogMaster('{{$list->group_id}}')" class="fa fa-history"></i>
                    @endif
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="6" class="re-records-found">No Records found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-4 pull-left">
   <!--  <span style='width:20px;height:20px;display:block;float:left;background-color:#d2e3e2;margin: 35px 0;'></span>
    <label style="margin: 2px 0 0 10px;float: left;;margin: 35px 0;">&nbsp;&nbsp;Sub Groups</label>  -->
</div>
<div class="col-md-8 text-center">
    <ul class="pagination purple_pagination pull-right">
        {!! $page_links !!}
    </ul>
</div>
