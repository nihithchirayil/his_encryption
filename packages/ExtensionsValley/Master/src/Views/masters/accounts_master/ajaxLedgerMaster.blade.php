<script type="text/javascript">
$(document).ready(function() {
    $(".page-link").click(function() {
        var url = $(this).attr("href");
        if (url && url != 'undefined') {
            var token = $('#token_hiddendata').val();
            var group_name = $('#group_name').val();
            var hidden_group_names_id = $('#hidden_group_names_id').val();
            var ledger_name = $('#ledger_name').val();
            var hidden_ledger_name_id = $('#hidden_ledger_name_id').val();
            var status = $('#status').val();
            var param = {
                _token: token,
                ledger_name: ledger_name,
                hidden_ledger_name_id: hidden_ledger_name_id,
                status: status,
                group_name: group_name,
                hidden_group_names_id: hidden_group_names_id
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $("#searchLedgerMasterDataDiv").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                    $('#searchMasterBtn').attr('disabled', true);
                    $('#searchMasterBtnSpin').removeClass('fa fa-search');
                    $('#searchMasterBtnSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchLedgerMasterDataDiv').html(data.html);
                    // $('#total_cntspan').html(data.total_records);
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30

                    });
                    setTimeout(function() {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                },
                complete: function() {
                    $("#searchLedgerMasterDataDiv").LoadingOverlay("hide");
                    $('#searchMasterBtn').attr('disabled', false);
                    $('#searchMasterBtnSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchMasterBtnSpin').addClass('fa fa-search');
                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
        }
        return false;
    });

});
</script>
<div class="box no-border no-margin">
    <div class="theadscroll" style="position: relative; height: 450px;">
        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th style="width:40%"> Name</th>
                    <th class="group_table" style="width:40%">Group</th>
                    <th>Account No</th>
                    <th class="text-center">Edit</th>
                    <th class="text-center"><i class="fa fa-history"></th>
                    <th class="text-center">Delete</th>
                </tr>
            </thead>
            <tbody>

                @if(count($ledger_master_list) > 0)
                @foreach ($ledger_master_list as $list)
                <tr>
                    <td class="category_name common_td_rules">{{ $list->ledger_name }}</td>
                    <td class="subcategory_code common_td_rules group_table">{{ $list->group_name }}</td>
                    <td class="status common_td_rules">{{ $list->ledger_code }}</td>
                    <td class="text-center  common_td_rules">
                        <i style="cursor:pointer"
                            onclick="ledgerEditLoadData('{{$list->master_id}}','{{addslashes($list->ledger_name)}}','{{$list->ledger_group_id}}',
                                           '{{$list->is_bank}}','{{$list->payment_bank_default}}','{{$list->receipt_bank_default}}',
                                           '{{$list->ledger_code}}','{{$list->group_name}}','{{$list->ledger_type}}',
                                           '{{$list->max_credit_amount}}',
                                           '{{$list->credit_days}}','{{$list->is_asset}}',
                                           '{{$list->pl_type}}','{{$list->ledger_address}}','{{$list->gst_no}}','{{$list->pin_no}}','{{$list->opening_balence}}','{{$list->allow_multiple_entry}}')"
                            class="fa fa-edit"></i>
                    </td>
                    <td class="text-center  common_td_rules">
                        @if($list->audit_status == 1)
                        <i style="cursor:pointer;color: #f0ad4e" id="history_spin_{{$list->master_id}}"
                            onclick="viewAuditLogMaster('{{$list->master_id}}')" class="fa fa-history"></i>
                        @endif
                    </td>
                    <td class="text-center  common_td_rules">
                        <i style="cursor:pointer;color: red" id="delete_spin_{{$list->master_id}}"
                            onclick="ledgerDelete(this,'{{$list->master_id}}')" class="fa fa-trash"></i>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="6" class="re-records-found">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center ">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>

</div>
