<?php


if(isset($ledger_name_details)){
    if (!empty($ledger_name_details)) {

        foreach ($ledger_name_details as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillledgerNameDetails("{{htmlentities($data->ledger_name)}}","{{htmlentities($data->id)}}")' class="list_hover">
                {!! $data->ledger_name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}