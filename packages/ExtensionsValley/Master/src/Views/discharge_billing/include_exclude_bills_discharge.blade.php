@php 
$i =0;
@endphp
@if(!empty($bills))
@foreach($bills as $bill)
@php
$i++;
$bill_date = (!empty($bill->bill_date)) ? ExtensionsValley\Emr\CommonController::getDateFormat($bill->bill_date,'M-d-Y') : '';
@endphp
    <tr data-bill-head-id="{{$bill->bill_head_id}}" style="@if($bill->is_excludefromdischarge == 0) background-color: #ffffff; @else background-color: #9fd3ff; border: 1px solid #c9e6ff; @endif">
        <td title="{{$i+1}}">{{$i}}</td>
        <td title="{{$bill->bill_no}}">{{$bill->bill_no}}</td>
        <td title="{{$bill_date}}">{{$bill_date}}</td>
        <td title="{{$bill->bill_tag}}">{{$bill->bill_tag}}</td>
        <td title="{{$bill->bill_amount}}">{{$bill->bill_amount}}</td>
        <td title="{{$bill->discount_amount}}">{{$bill->discount_amount}}</td>
        <td title="{{$bill->net_amount_wo_roundoff}}">{{$bill->net_amount_wo_roundoff}}</td>
        <td title="{{$bill->return_amount}}">{{$bill->return_amount}}</td>
        <td>
            @if($bill->is_includeindischarge == 0 && $bill->is_excludefromdischarge == 0)
            <button title="Exclude Bill" type="button" class="btn btn-sm btn-primary" onclick="includeExcludeBill({{$bill->bill_head_id}}, 1);" ><i class="fa fa-plus"></i></button>
            @elseif($bill->is_includeindischarge == 0 && $bill->is_excludefromdischarge == 1)
            <button title="Include Bill" type="button" class="btn btn-sm btn-danger" onclick="includeExcludeBill({{$bill->bill_head_id}}, 0);"><i class="fa fa-minus"></i></button>
            @endif
        </td>
    </tr>
@endforeach
@else
<td colspan="12">No Data Found..!</td>
@endif