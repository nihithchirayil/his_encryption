@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-slide.css') }}" rel="stylesheet">

    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/common-custom.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet"> -->
    <style>
        .ajaxSearchBox {
            display:none;
            text-align: left;
            list-style: none;
            cursor: pointer;
            max-height: 350px;
            margin: 3px 0px 0px 0px;
            overflow-y: auto;
            width: 150px;
            z-index: 599;
            position:absolute;
            background: #ffffff;
            border-radius: 3px;
            border: 1px solid rgba(0, 0, 0, 0.3);

        }
        .ajaxSearchBox>li{
            padding:2px 2px 5px 5px;
            font-size: 10px !important;
            font-weight: 400 !important;
            font-family: "sans-serif";
            border-bottom: 1px solid #3e75a5;
        }

        .liHover{
            background: #3e75a5 !important;
            color:white;
            padding:2px 2px 5px 5px;
            font-size: 10px !important;
            font-weight: 400;
            font-family: "sans-serif";
            border-bottom: 1px solid white;
        }
        .search_header {
            background: #36A693 !important;
            color: #FFF !important;
        }
        .box_header {
            background: #3b926a !important;
            /* background: #5397b1 !important; */
            color: #FFF !important;
        }
        .mate-input-box{
            width: 100%;
            position: relative;
            padding: 15px 4px 4px 4px !important;
            border-bottom: 1px solid #01A881;
            box-shadow: 0 0 3px #CCC;
        }
        .mate-input-box label{
            position: absolute;
            top: -2px;
            left: 6px;
            font-size: 12px;
            font-weight: 700;
            color: #107a8c;
            padding-top: 2px;
        }

        .patient_details_table>tbody>tr>td{
            border: 1px solid #CCC !important;
        }

        .list-medicine-search-data-row-listing{
            text-align:left;
        }

        .alerts-border {
            border: 1px #ff0000 solid;
            animation: blink 1s;
            animation-iteration-count: 3;
        }

        @keyframes blink { 50% { border-color:#fff ; }  }

        .number_class{
            text-align:right;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            padding: 2px;
            white-space: nowrap;
        }

        .pagination{
            margin: 10px 0 0 0 !important;
        }
        
        .medicine-list-div-row-listing{
            width: 70%;
            top: 35%;
            left: 28%;
            z-index: 9999;
        }
        .active_medicine{
            background: #337ab7 !important;
            color: white;
            cursor: pointer;
        }
        .medicine-list-div-row-listing tbody tr:hover{
            background: #337ab7 !important;
            color: white;
            cursor: pointer;
        }
        .ajaxSearchBox{
            z-index: 1016 !important;
        }

        .required_field:after{
            color: red;
            content: "*";
            margin-left: 2px;
        }
        .select2-chosen, .select2-choice > span:first-child, .select2-container .select2-choices .select2-search-field input{
            padding: 2px 12px;
        }
        .select2-container .select2-choice{
            height: 22px;
            line-height: 1.42857;
        }

        .select2-arrow{
            top: -4px !important;
        }
    </style>
@endsection
@section('content-area')
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="patient_id" value="">
    <input type="hidden" id="visit_id" value="">
    <input type="hidden" id="bill_id" value="{{$bill_id}}">
    <input type="hidden" id="bill_no" value="">
    <input type="hidden" id="pending_bill_show_config" value="{{$pending_bill_show_config}}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    
    <!-- page content -->
    <div class="right_col" role="main" style="background:white">
        <div class=" col-md-12  padding_sm">
            <div class="box no-border" style="margin-bottom: 10px;">
                <div class="box-header table_header_common">
                    <span class="padding_sm">{{$title}} <span class="discharge_bill_no"></span></span>
                </div>
                <div class="box-body clearfix">
                    <div class=" col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="required_field">UHID</label>
                            <div class="clearfix"></div>
                            <input class="form-control hidden_search filters" value="" autocomplete="off" type="text" id="op_no" name="op_no" style="width:80%" />
                            <div id="op_noAjaxDiv" class="ajaxSearchBox" style="width: 300px !important; margin-top: 17px; max-height: 505px !important;"></div>
                            <input class="filters" value="" type="hidden" name="op_no_hidden" id="op_no_hidden">
                            <button type="button" class="btn btn-sm btn-primary advanceSearchBtn" style=" position: absolute; top: 15px; right: 0;"><i class="fa fa-search"></i></button>
                        </div>  
                        <div class="mate-input-box">
                            <label for="" class="required_field">Billing Location</label>
                            <div class="clearfix"></div>
                            {!! Form::select('billing_location',$location_list, null,['class' => 'form-control','placeholder' => 'Billing Location','title' => 'Billing Location','id' => 'billing_location', 'style' => 'color:#555555; padding:2px 12px; ', 'readonly'=>'readonly', 'disabled'=>'true']) !!}
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm">
                        <table class="table table_sm patient_details_table  no-margin no-border">
                            <tbody>
                                <tr>
                                    <td><b>Patient Name</b></td>
                                    <td style="width:40%;"><span id="patient_name_dis"></span></td>
                                    <td><b>Doctor</b></td>
                                    <td>
                                    {!! Form::select('consulting_doctor',$doctor_list, null,['class' => 'form-control','placeholder' => 'Doctor','title' => 'Doctor','id' => 'consulting_doctor','style' => 'color:#555555; padding:2px 12px; ', 'readonly'=>'readonly', 'disabled'=>'true']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b> IP No </b></td>
                                    <td><span id="ip_number" ></span></td>
                                    <td><b> Sponsor </b></td>
                                    <td>
                                        {!! Form::select('company',$company_list, null,['class' => 'form-control','placeholder' => 'Sponser','title' => 'Sponser','id' => 'company', 'style' => 'color:#555555; padding:2px 12px; ', 'readonly'=>'readonly', 'disabled'=>'true']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b> Room </b></td>
                                    <td><span id="current_room" ></span></td>
                                    <td><b> Pricing </b></td>
                                    <td>
                                    {!! Form::select('pricing',$pricing_list, null,['class' => 'form-control','placeholder' => 'Pricing','title' => 'Pricing','id' => 'pricing', 'style' => 'color:#555555; padding:2px 12px; ', 'readonly'=>'readonly', 'disabled'=>'true']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b> Admission Date </b></td>
                                    <td><span id="admission_date" ></span></td>
                                    <td><b> Payment Type </b></td>
                                    <td>
                                        <select class= "form-control" id='payment_type' >
                                            <option value="">Payment Type</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>


                    <div class="col-md-2 padding_sm" style="margin-top: 10px !important;">
                        <table class="table table_sm  no-margin no-border">
                            <tbody>
                                <tr>
                                    <td><button style="text-align: left;" id="new_service_bill_btn" type="button" class="btn btn-sm btn-block btn-primary"> <i class="fa fa-plus" aria-hidden="true"></i> New Service Bill </button></td>
                                </tr>
                                <tr>
                                    <td><button style="text-align: left;" id="pending_bills_btn" type="button" class="btn btn-sm btn-block btn-primary"> <i class="fa fa-clock-o" aria-hidden="true"></i> Pending Bills </button></td>
                                </tr>
                                <tr>
                                    <td><button style="text-align: left;" type="button" class="btn btn-sm btn-block btn-primary include_exclude_btn"> <i class="fa fa-cog" aria-hidden="true"></i> Include/Exclude Bills </button></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    
                </div>

            </div>
        </div>

        <div class="col-md-12 padding_sm" style="margin-top: 5px; padding: 0px !important;">

            <div class="col-md-7 padding_sm">
                <div class="theadscroll" style="position: relative; height: 140px; border: 1px solid #dcdcdc; background:white;">
                    <table class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed " style="position:absolute;">
                        <thead>
                            <tr class="table_header_common">
                                <th width="2%">#</th>
                                <th width="5%"></th>
                                <th width="43%">Department</th>
                                <th width="10%">Gross Amt</th>
                                <th width="10%">Disc Amt</th>
                                <th width="10%">Net Amt</th>
                                <th width="10%">Patient Pay.</th>
                                <th width="10%">Company Pay.</th>
                            </tr>
                        </thead>
                        <tbody style="min-height: 350px;" class="discharge_billing_department_table_body">
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-5 padding_sm" >
                <div class="col-md-6 no-padding" >
                    <div class="box-header table_header_common">
                        <span class="padding_sm">Bill Discount</span>
                    </div>
                    <div class="box-body " style="min-height:121px;">
                        <div class=" col-md-6" style="margin-top:8px; padding-left: 2px !important; padding-right: 2px !important;">
                            <div class="mate-input-box">
                                <label for="discount_type">Discount Type</label>
                                <div class="clearfix"></div>
                                <select id="discount_type" class="form-control">
                                    <option value="">Discount Type</option>
                                    <option value="1">Percentage(%)</option>
                                    <option value="2">Amount</option>
                                </select>
                            </div>
                        </div>
                        <div class=" col-md-6" style="margin-top:8px; padding-left: 2px !important; padding-right: 2px !important;">
                            <div class="mate-input-box">
                                <label for="discount_type">Discount</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control number_class discount_value" value="" />
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:8px; padding-left: 2px !important; padding-right: 2px !important;">
                            <div class="mate-input-box">
                                <label for="discount_narration">Discount Narration</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control discount_narration" id="discount_narration" value="" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 padding_sm" >
                    <div class="box-header table_header_common">
                        <span class="padding_sm">Death Patient</span>
                    </div>
                    <div class="box-body " style="min-height:121px;">
                        <div class=" col-md-12" style="margin-top:8px; padding-left: 2px !important; padding-right: 2px !important;">
                            <div class="mate-input-box">
                                <label for="death_patient">Death Patient</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" id="death_patient" value="" />
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:8px; padding-left: 2px !important; padding-right: 2px !important;">
                            <div class="mate-input-box">
                                <label for="death_time">Death Date & Time</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datetimepicker" id="death_time" value="" readonly disabled />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
        </div>

        <div class="col-md-12 padding_sm" style="margin-top: 10px;">
            <div class="theadscroll" style="position: relative; height: 250px; border: 1px solid #dcdcdc; background:white;">
                <table class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed " style="position:absolute;">
                    <thead>
                        <tr class="table_header_common">
                            <th width="2%">#</th>
                            <th width="12%">Bill Number</th>
                            <th width="10%">Bill Date</th>
                            <th width="18%">Item Description</th>
                            <th width="5%">Qty</th>
                            <th width="6%">Price</th>
                            <th width="5%">Tax %</th>
                            <th width="5%">Disc %</th>
                            <th width="8%">Discount</th>
                            <th width="8%">Net Amount</th>
                            <th width="8%">Patient Payable</th>
                            <th width="8%">Company Payable</th>
                            <th width="6%">Action</th>
                        </tr>
                    </thead>
                    <tbody style="min-height: 350px;" class="discharge_billing_table_body">
                    </tbody>
                </table>
            </div>
        </div>


        <div class="col-md-12 padding_sm" style="margin-top: 10px;" >
            <div class="col-md-12 padding_sm" >
                <div class="box-body">
                    <table class="table table-bordered table-striped table_sm"  style="margin:0px;">
                        <tbody>
                            <tr>
                                <td>Outstanding Amount</td>
                                <td>
                                    <input class="form-control outstanding_amount number_class " readonly disabled type="text" value="">
                                </td>
                                <td>Advance</td>
                                <td>
                                    <input class="form-control patient_advance number_class" readonly disabled type="text" value="">
                                </td>
                                <td>Balance In Adv.</td>
                                <td>
                                    <input class="form-control balance_in_advance number_class" readonly disabled type="text" value="">
                                </td>
                                <td>Reason</td>
                                <td>
                                    <input class="form-control discharge_reason" type="text" value="">
                                </td>
                                <td colspan="2">
                                    <button type="button" class="btn btn-block btn-success" onclick="saveDischargeBill(1);"> <i class="fa fa-save" aria-hidden="true"></i> Generate Discharge </button>
                                </td>
                            </tr>
                            <tr>
                                <td>Bill Amount</td>
                                <td>
                                    <input class="form-control bill_amount number_class" readonly disabled type="text" value="">
                                </td>
                                <td>Patient Payable</td>
                                <td>
                                    <input class="form-control patient_payable number_class" readonly disabled type="text" value="">
                                </td>
                                <td>Payor Payable</td>
                                <td>
                                    <input class="form-control payor_payable number_class" readonly disabled type="text" value="">
                                </td>
                                <td>Net Amount</td>
                                <td>
                                    <input class="form-control net_amount number_class" readonly disabled type="text" value="" style="font-size:20px !important; color: #5397b1;">
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning" onclick="saveDischargeBill(0);"> <i class="fa fa-save" aria-hidden="true"></i> Save </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary" onclick="resetDischargeBill();"> <i class="fa fa-refresh" aria-hidden="true"></i> Reset </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
<!-- 
            <div class="col-md-2 padding_sm">
                <div class="col-md-12 no-padding">
                    <button type="button" class="btn btn-block btn-success" onclick="saveDischargeBill(1);"> <i class="fa fa-save" aria-hidden="true"></i> Generate Discharge </button>
                </div>
                <div class="col-md-6 no-padding">
                    <button type="button" class="btn btn-block btn-warning" onclick="saveDischargeBill(0);"> <i class="fa fa-save" aria-hidden="true"></i> Save As Draft </button>
                </div>
                <div class="col-md-6 padding_sm">
                    <button type="button" class="btn btn-block btn-primary" onclick="resetDischargeBill();"> <i class="fa fa-refresh" aria-hidden="true"></i> Reset </button>
                </div>
            </div> -->
        </div>

    </div>

    <!-------print modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Discharge Bill</h4>
                    <button type="button" class="close print_config_close_btn" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:190px;">
                    <div class="col-md-12">
                        <p> Bill saved successfully. </p>
                        <p> Bill No : <strong class="save_message_bill_no"></strong> </p>
                    </div>

                    <div class="col-md-12">
                        <hr/>
                        
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="include_hospital_header" id="include_hospital_header">
                        Include Hospital Header
                    </div>
                </div>
                <div class="modal-footer">
                    <button onclick="printBillDetails();" class="btn bg-primary pull-right" style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print Bill
                    </button>
                    <!-- <button onclick="payBill();" class="btn bg-primary pull-right pay_now_button" style="color:white; display:none;">
                        <i class="fa fa-print" aria-hidden="true"></i> Pay Now
                    </button> -->
                    <button type="button" class="btn btn-default print_config_close_btn">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-------include exculde bills modal---------------->
    <div class="modal" role="dialog" id="include_exclude_bill_modal">
        <div class="modal-dialog  modal-lg" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header table_header_common">
                    <h4 class="modal-title" style="color: white;">Include/Exclude Bills</h4>
                    <button type="button" class="close include_exclude_modal_close_btn" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height: 500px;">

                    <div class=" col-md-12  padding_sm">
                        <div class="box no-border" style="margin-bottom: 10px;">
                            <div class="box-body clearfix">
                                <div class=" col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="bill_date_from">From Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control datepicker" value="" id="bill_date_from">
                                    </div>
                                </div>
                                <div class=" col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="bill_date_to">To Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control datepicker" value="" id="bill_date_to">
                                    </div>
                                </div>
                                <div class=" col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="bill_no_search">Bill No</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control" id="bill_no_search" name="bill_no_search" value="">
                                    </div>
                                </div>
                                <div class=" col-md-2 padding_sm">
                                    <div class="mate-input-box" id="bill_tag"style="height:45px!important;">
                                        <label for="bill_tag_search">Bill tag</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('bill_tag_search',$bill_tag, null,['class' => 'form-control select2','placeholder' => 'Bill Tag','title' => 'Bill Tag','id' => 'bill_tag_search', 'style' => 'color:#555555; padding:2px 12px; ']) !!}
                                    </div>
                                </div>
                                <div class=" col-md-1 padding_sm">
                                    <label for="bill_tag_search"></label>
                                    <div class="clearfix"></div>
                                    <button type="button" class="btn btn-sm btn-primary btn-block" onclick="getBillsForIncludeExclude();"><i class="fa fa-search"></i> Search</button>
                                </div>
                                <div class=" col-md-1 padding_sm">
                                    <label for="bill_tag_search"></label>
                                    <div class="clearfix"></div>
                                    <button type="button" class="btn btn-sm btn-primary btn-block" onclick="resetIncludeExcludeSearchForm();"><i class="fa fa-refresh"></i> Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class=" col-md-12  padding_sm" style="margin-top: 10px;">
                        <div class="theadscroll" style="position: relative; height: 350px; border: 1px solid #dcdcdc; background:white;">
                            <table class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed " style="position:absolute;">
                                <thead>
                                    <tr class="table_header_common">
                                        <th width="2%">#</th>
                                        <th width="30%">Bill Number</th>
                                        <th width="13%">Bill Date</th>
                                        <th width="10%">Bill Tag</th>
                                        <th width="10%">Bill Amount</th>
                                        <th width="10%">Discount</th>
                                        <th width="10%">Net Amount</th>
                                        <th width="10%">Return Amount</th>
                                        <th width="5%"></th>
                                    </tr>
                                </thead>
                                <tbody style="min-height: 350px;" class="include_exclude_bills_table_body">
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 20px; margin-top: 15px;">
                        <div class="col-md-2" style="text-align: left;">
                            <span class="badge" style="background-color:#9fd3ff; border: 1px solid #ccc; border-radius: 0; width: 10px; height: 17px;">&nbsp;&nbsp;&nbsp;</span> Excluded
                        </div>
                        <div class="col-md-2" style="text-align: left;">
                            <span class="badge" style="background-color:#ffffff; border: 1px solid #ccc; border-radius: 0; width: 10px; height: 17px;">&nbsp;&nbsp;&nbsp;</span> Included
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default include_exclude_modal_close_btn">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Custom Modals -->
    @include('Master::RegistrationRenewal.advancePatientSearch')
    @include('Master::pharmacy_billing.pending_bills_list')
    @include('Emr::pharmacy_bill_list.cash_receive_model')

@stop
@section('javascript_extra')


    <script src="{{ asset('packages/extensionsvalley/jquery-mousewheel-master/jquery.mousewheel.min.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/discharge_billing.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"> </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/cash_receive.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>


@endsection
