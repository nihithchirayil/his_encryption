<!-- .css -->
<style>
    #doctorHeadClass p:first-child{
        margin: 0 !important;
        padding: 0 !important;
    }
    .number_class {
        text-align: right;
    }
    @media print
    {
        table{ page-break-after:auto; }
        tr    { page-break-inside:auto; page-break-after:auto }
        td    { page-break-inside:auto; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
    }
</style>
<style type="text/css" media="print">
    table {
        font-size : 13px;
    }
    @page{
        margin-left:15px;
        margin-right:15px;
        margin-top:40px;
        margin-bottom:35px;
    }
    .patient_head{
        font-size : 12px !important;
    }

</style>
<!-- .css -->
<!-- .box-body -->
<div class="box-body">
        <div class="col-md-12" >
            <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px; " width="100%" border="1" cellspacing="0" cellpadding="0">
                <thead>
                    @if((string)$hospital_header_status == "1")
                    <tr>
                        <th colspan="10" >
                         {!!$hospitalHeader!!}
                        </th>
                    </tr>
                    @endif
                    <tr>
                        <th colspan="10" style="font-size: 12px;" >
                        DISCHARGE BILL {{@$is_duplicate ? $is_duplicate : ' '}}
                        </th>
                    </tr>
                    <tr>
                    <th colspan="10" style="@if((string)$hospital_header_status == '0') margin-top; 3cm; @endif " >
                        <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;  " width="100%" border="1" cellspacing="0" cellpadding="0">
                            <thead class="patient_head" style="display: table-header-group;">
                            @if($printMode == "1")
                                <tr class="head">
                                    <td><strong>PATIENT NAME</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">@isset($discharge_bill_data[0]->patient_name) {{$discharge_bill_data[0]->patient_name}} @endisset</td>
                                    <td><strong>ADDRESS</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td colspan="4" style="border-left: none;">{{$discharge_bill_data[0]->address}}</td>
                                    
                                </tr>
                                <tr>
                                    <td><strong>{{strtoupper(\WebConf::getConfig('op_no_label'))}}</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->uhid}}</td>
                                    <td style="border-left: none;"><strong>BILL DATE</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{date('M-d-Y h:i A', strtotime($discharge_bill_data[0]->bill_date))}}</td>
                                    <td><strong>SPONSOR</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->pricing_name}}</td>
                                </tr>
                                <tr>
                                    <td style="border-left: none;"><strong>IP NUMBER</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->admission_no}}</td>
                                    <td><strong>BILL NUMBER</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->bill_no}}</td>
                                    <td style="border-left: none;"><strong>DOCTOR</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->doctor_name}}</td>
                                    
                                </tr>
                                <tr>
                                    <td style="border-left: none;"><strong>GENDER/AGE</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->gender}} / {{$discharge_bill_data[0]->age}}</td>
                                    <td style="border-left: none;"><strong>PAYMENT TYPE</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->payment_type}}</td>
                                    <td><strong>LOCATION</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->location_name}}</td>
                                </tr>
                            @else
                                <tr>
                                    <td><strong>PATIENT NAME</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">@isset($discharge_bill_data[0]->print_header) {{$discharge_bill_data[0]->patient_name}} @endisset</td>
                                    <td style="border-left: none;"><strong>GENDER/AGE</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->gender}} / {{$discharge_bill_data[0]->age}}</td>
                                    <td><strong>{{strtoupper(\WebConf::getConfig('op_no_label'))}}</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->uhid}}</td>
                                    <td style="border-left: none;"><strong>IP NUMBER</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->admission_no}}</td>
                                    <td><strong>SPONSOR</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->pricing_name}}</td>
                                    
                                </tr>
                                <tr>
                                    <td><strong>BILL NUMBER</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->bill_no}}</td>
                                    <td style="border-left: none;"><strong>BILL DATE</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{date('M-d-Y h:i A', strtotime($discharge_bill_data[0]->bill_date))}}</td>
                                    <td><strong>LOCATION</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->location_name}}</td>
                                    <td style="border-left: none;"><strong>DOCTOR</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$discharge_bill_data[0]->doctor_name}}</td>
                                    <td><strong>ADDRESS</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td colspan="4" style="border-left: none;">{{$discharge_bill_data[0]->address}}</td>
                                </tr>
                            @endif
                            </thead>
                        </table>
                    </th>
                    </tr>
                    <tr>
                    <th colspan="10">
                        <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;font-size: 12px;" width="100%" border="1" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th width="2%"><strong><center>#</center></strong></th>
                                    <th width="78%"><strong><center>Particulars</center></strong></th>
                                    <th width="10%"><strong><center>Qty</center></strong></th>
                                    <th width="10%"><strong><center>Net Amount</center></strong></th>
                                </tr>
                            </thead>
                        </table>
                    </th>
                    </tr>
                </thead>
                <tbody>
                @if(count($discharge_bill_data)>0)
              
                @foreach($discharge_bill_data as $bill)
                <tr style="border-bottom: 1px solid #ccc;">
                    <td width="2%" > <center>{{$loop->iteration}}</center> </td>
                    <td width="10%" style="text-align:left;padding-left:5px;"> {{ $bill->dept_name }} </td>
                    <td width="8%" class="number_class"> {{ $bill->quantity }} </td>
                    <td width="8%" class="number_class"> {{ $bill->net_amount }} </td>
                </tr>
                @endforeach
               
                @endif
                <tr>
                    <td colspan="10">
                        <table style="border-colapse:colapse;margin-top:15px; width:100%;" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width:50%;"></td>
                                <td style="width:25%;">
                                    <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse; width:100%;" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="text-align:left;"><strong>BILL TOTAL</strong></td>
                                            <td style="text-align:center;"><strong>:</strong></td>
                                            <td style="text-align:right;"><strong>{{$discharge_bill_data[0]->bill_amount}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;"><strong>DISCOUNT</strong></td>
                                            <td style="text-align:center;"><strong>:</strong></td>
                                            <td style="text-align:right;"><strong>{{$discharge_bill_data[0]->discount_amount}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;"><strong>NET AMOUNT</strong></td>
                                            <td style="text-align:center;"><strong>:</strong></td>
                                            <td style="text-align:right;"><strong>{{$discharge_bill_data[0]->net_amount_total}}</strong></td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width:10%;"></td>
                                <td style="width:15%">
                                    <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse; width:100%; height: 100px;" border="1" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td><strong><center>AMOUNT TO PAY</center></strong></td>
                                        </tr>
                                        <tr>
                                            <td><strong><center>{{round($discharge_bill_data[0]->net_amount_total)}}<center></strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="10">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="10">
                                    <div style="text-align:right;"> {{  \ExtensionsValley\Master\GridController::number_to_word(round($discharge_bill_data[0]->net_amount_total)) }} </div>
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>

                <tr>
                    <td colspan="10">
                        <table style="border-colapse:colapse;margin-top:15px; width:100%;" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td>
                                        Bill Created By : {{$discharge_bill_data[0]->created_text}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Bill Printed By : {{$discharge_bill_data[0]->printed_text}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        

</div>
<!-- .box-body -->

