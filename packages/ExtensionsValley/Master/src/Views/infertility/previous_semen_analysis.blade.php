<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>PREVIOUS SEMEN ANALYSIS</b></h5>
</div>


<div class="col-md-12">
    <div class="theadscroll ps-container ps-theme-default" style="position: relative;">
        <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC;">
            <thead>
                <tr style="font-weight:700;">
                    <td style="width:185px; ">Tests</td>
                    <td>NSML</td>
                    <td>DIS</td>
                    <td>Normal</td>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td style="text-align: left; width:185px; ">Ejectable Volume(ml)</td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text ejectable_volume_nsml" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text ejectable_volume_dis" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text ejectable_volume_normal" placeholder="" id="" style="">
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left; width:185px; ">PH</td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text ph_nsml" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text ph_dis" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text ph_normal" placeholder="" id="" style="">
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left; width:185px; ">Sperm Consentration</td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text sperm_consentration_nsml" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text sperm_consentration_dis" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text sperm_consentration_normal" placeholder="" id="" style="">
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left; width:185px; ">Total Sperm No(Million/ml)</td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text total_sperm_no_nsml" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text total_sperm_no_dis" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text total_sperm_no_normal" placeholder="" id="" style="">
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left; width:185px; ">Percent Motility(% motile)</td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text percent_motility_nsml" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text percent_motility_dis" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text percent_motility_normal" placeholder="" id="" style="">
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left; width:185px; ">Forward Progression</td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text forward_progression_nsml" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text forward_progression_dis" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text forward_progression_normal" placeholder="" id="" style="">
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left; width:185px; ">Morphology(% normal)</td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text morphology_nsml" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text morphology_dis" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text morphology_normal" placeholder="" id="" style="">
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left; width:185px; ">Sperm Aglutination(Scale 0-3)</td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text sperm_aglutination_nsml" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text sperm_aglutination_dis" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text sperm_aglutination_normal" placeholder="" id="" style="">
                    </td>
                </tr>

                <tr>
                    <td style="text-align: left; width:185px; ">Viscosity(Scale 0-4)</td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text viscosity_nsml" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text viscosity_dis" placeholder="" id="" style="">
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text center_text viscosity_normal" placeholder="" id="" style="">
                    </td>
                </tr>


            </tbody>
        </table>
    </div>
</div>
