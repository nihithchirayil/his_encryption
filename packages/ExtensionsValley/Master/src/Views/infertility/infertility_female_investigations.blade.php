<div class="col-md-12" style="margin-top:15px !important;">
    
    <div class="col-md-8 padding_sm">
        <h5><b>INVESTIGATIONS ( Wife/Female )</b></h5>
    </div>
    <div class="col-md-4 padding_sm investigationHistoryContainerFemale" style="text-align:right;">
        <button onclick="fetchInvestigationHistoryFemale();" type="button" class="btn btn-sm btn-warning" > <i class="fa fa-list fetchInvestigationIcon" > </i> INVESTIGATION HISTORY</button>
        <div class="popover bottom in" role="tooltip" style="top: 25px; left: -390px; right: -450px; display: none;">
            <div class="arrow"></div>
            <div class="popover-content">

            </div>
        </div>
    </div>
</div>

<div class="col-md-12 female_investigations" style="margin-bottom:20px !important;">

    <div class="col-md-12" style="margin-top:10px;">
        <div class="col-md-12">
            <b>General</b>
        </div>
        <div class="col-md-12">
            @foreach($female_general_inv as $inv)
                <div class="col-md-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="{{$inv->service_code}}" >
                        <label class="form-check-label" for="{{$inv->service_code}}">
                        {{$inv->service_desc}}
                        </label>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="col-md-12" style="margin-top:10px;">
            <b>Hormonal</b>
        </div>
        <div class="col-md-12">
            @foreach($female_hormonal_inv as $inv)
                <div class="col-md-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="{{$inv->service_code}}" >
                        <label class="form-check-label" for="{{$inv->service_code}}">
                        {{$inv->service_desc}}
                        </label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>


</div>
