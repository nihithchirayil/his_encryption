<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>CERVICAL FACTOR</b></h5>
</div>

<div class="col-md-12">
    <div class="col-md-10 ">
        <div class="theadscroll" style="position: relative;">
            <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC;">
                <tbody>

                    <tr>
                        <td style="text-align: left;"> Surgery: </td>
                        <td>
                            <div class="btn-group btn-group-toggle female_cervical_surgery" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_cervical_surgery" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_cervical_surgery" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;"> Injury: </td>
                        <td>
                            <div class="btn-group btn-group-toggle female_cervical_injury" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_cervical_injury" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_cervical_injury" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: left;"> Infections: </td>
                        <td>
                            <div class="btn-group btn-group-toggle female_cervical_infection" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_cervical_infection" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_cervical_infection" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </td>
                    </tr>

            </table>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top:15px !important;">
    <div class="col-md-10 no-padding">
        <div class="col-md-4">
            <h5>Details :</h5>
        </div>
        <div class="col-md-8">
            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_cervical_details" placeholder="" id="" style="">
        </div>
    </div>
</div>
