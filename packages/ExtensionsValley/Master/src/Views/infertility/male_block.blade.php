<div class="col-md-6 no-padding box-body" id="male_block" style="height: 2491px;">
    <div class="col-md-12" style="margin-top:15px !important;">
        <div class="col-md-4 no-padding">
            <h5><b>INFERTILITY : </b></h5>
        </div>
        <div class="col-md-8 no-padding" style="margin-top: 5px;">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn bg-radio_grp">
                <input type="radio" name="infertility_male" value="primary" autocomplete="off"> Primary
                </label>
                <label class="btn bg-radio_grp">
                <input type="radio" name="infertility_male" value="secondary" autocomplete="off"> Secondary
                </label>
            </div>
        </div>
    </div>
    @include('Master::infertility.male_medical_surgical_history')
    @include('Master::infertility.habits_and_env_factors')
    @include('Master::infertility.sexual_and_ejaculatory_function')
    @include('Master::infertility.general_and_physical_examination')
    @include('Master::infertility.uro_general_examination')
    @include('Master::infertility.previous_semen_analysis')

</div>

