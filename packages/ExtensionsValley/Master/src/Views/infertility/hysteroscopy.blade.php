<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>HYSTEROSCOPY</b></h5>
</div>
<div class="col-md-12 no-padding">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="col-md-2">
                <h5>Date :</h5>
            </div>
            <div class="col-md-4">
                <input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="" class="form-control datepicker bottom-border-text female_hysteroscopy_date" placeholder="MM-DD-YYYY" id="" style="">
            </div>
            <div class="col-md-2">
                <h5>Place :</h5>
            </div>
            <div class="col-md-4">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_hysteroscopy_place" placeholder="" id="" style="">
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 no-padding">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="col-md-2">
                <h5>Cavity :</h5>
            </div>
            <div class="col-md-4">
                <div class="btn-group btn-group-toggle female_hysteroscopy_cavity" data-toggle="buttons">
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_hysteroscopy_cavity" value="normal" autocomplete="off"> Normal
                    </label>
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_hysteroscopy_cavity" value="abnormal" autocomplete="off"> Abnormal
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <h5>Ostia :</h5>
            </div>
            <div class="col-md-4">
                <div class="btn-group btn-group-toggle female_hysteroscopy_ostia" data-toggle="buttons">
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_hysteroscopy_ostia" value="normal" autocomplete="off"> Normal
                    </label>
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_hysteroscopy_ostia" value="abnormal" autocomplete="off"> Abnormal
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
