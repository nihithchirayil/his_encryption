   <div class="col-md-12" style="margin-top:15px !important;">
    <div class="col-md-4">
            <div class="box no-border">
                <div class="tinybody">
                    <button class="accordion">MEDICAL HISTORY</button>
                    <div class="panel tinypanel">
                        <textarea class="texteditor11 form-control" id="medical_history"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 ">
            <div class="box no-border">
                <div class="tinybody">
                    <button class="accordion">SURGICAL HISTORY</button>
                    <div class="panel tinypanel">
                        <textarea class="texteditor11 form-control" id="surgical_history"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 ">
            <div class="box no-border">
                <div class="tinybody">
                    <button class="accordion">MEDICATIONS</button>
                    <div class="panel tinypanel">
                        <textarea class="texteditor11 form-control" id="medications"></textarea>
                    </div>
                </div>
            </div>
        </div>

    </div>
