<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>PREVIOUS INDUCTION OF OVULATION</b></h5>
</div>
<div class="col-md-12 no-padding">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="col-md-2">
                <h5>No.Cycles:</h5>
            </div>
            <div class="col-md-4">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_previous_induction_no_of_cycles" placeholder="" id="" style="">
            </div>
            <div class="col-md-2">
                <h5>Year :</h5>
            </div>
            <div class="col-md-4">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_previous_induction_year" placeholder="" id="" style="">
            </div>

        </div>
    </div>
</div>

<div class="col-md-12 no-padding">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="col-md-2">
                <h5>Response :</h5>
            </div>
            <div class="col-md-4">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_previous_induction_response" placeholder="" id="" style="">
            </div>
            <div class="col-md-2">
                <h5>HMG/FSH :</h5>
            </div>
            <div class="col-md-4">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_previous_induction_hmg_fsh" placeholder="" id="" style="">
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 no-padding">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="col-md-2">
                <h5>Letrozole :</h5>
            </div>
            <div class="col-md-4">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_previous_induction_letrozole" placeholder="" id="" style="">
            </div>
            <div class="col-md-2">
                <h5>Clomphene:</h5>
            </div>
            <div class="col-md-4">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_previous_induction_clomphene" placeholder="" id="" style="">
            </div>
        </div>
    </div>
</div>
