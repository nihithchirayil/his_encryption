<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>GENERAL EXAMINATION</b></h5>
</div>
<div class="col-md-12">
    <div class="theadscroll ps-container ps-theme-default" style="position: relative;">
        <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC;">
            <tbody>
                <tr>
                    <td style="width: 240px;">
                        <div class="col-md-3" style="padding-left:0;  text-align:left;">Height</div>
                        <div class="col-md-7">
                            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_general_examination_height" placeholder="" id="" style="">
                        </div>
                        <div class="col-md-2">cms</div>
                    </td>
                    <td>
                        <div class="col-md-3">Weight</div>
                        <div class="col-md-7">
                            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_general_examination_weight" placeholder="" id="" style="">
                        </div>
                        <div class="col-md-2">lbs</div>
                    </td>
                    <td>
                        <div class="col-md-3">BMI</div>
                        <div class="col-md-9">
                            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_general_examination_bmi" placeholder="" id="" style="">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-md-3" style="padding-left:0; text-align:left;">BP</div>
                        <div class="col-md-9">
                            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_general_examination_bp" placeholder="" id="" style="">
                        </div>
                    </td>
                    <td>
                        <div class="col-md-3">CVS</div>
                        <div class="col-md-9">
                            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_general_examination_cvs" placeholder="" id="" style="">
                        </div>
                    </td>
                    <td>
                        <div class="col-md-3">RS</div>
                        <div class="col-md-9">
                            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_general_examination_rs" placeholder="" id="" style="">
                        </div>
                    </td>
                </tr>

                <tr>
                    <td>
                        <div class="col-md-3" style="padding-left:0;  text-align:left;">Abdomen</div>
                        <div class="col-md-9">
                            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_general_examination_abdomen" placeholder="" id="" style="">
                        </div>
                    </td>
                    <td>
                        <div class="col-md-3">ACNE</div>
                        <div class="col-md-9">
                            <div class="btn-group btn-group-toggle female_general_examination_acne" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_general_examination_acne" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_general_examination_acne" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="col-md-3">Striae</div>
                        <div class="col-md-9">
                            <div class="btn-group btn-group-toggle female_general_examination_striae" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_general_examination_striae" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_general_examination_striae" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </div>
                    </td>
                    
                </tr>

                <tr>
                    <td style="text-align:left;"> Thyroid </td>
                    <td colspan="2">
                        <div class="btn-group btn-group-toggle female_general_examination_thyroid" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="female_general_examination_thyroid" value="normal" autocomplete="off"> Normal
                            </label>
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="female_general_examination_thyroid" value="abnormal" autocomplete="off"> Abnormal
                            </label>
                        </div>
                    </td>
                    
                </tr>
                <tr>
                    <td style="text-align:left;"> Galactorrhoea</td>
                    <td colspan="2">
                        <div class="btn-group btn-group-toggle female_general_examination_galactorrhoea" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="female_general_examination_galactorrhoea" value="present" autocomplete="off"> Present
                            </label>
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="female_general_examination_galactorrhoea" value="absent" autocomplete="off"> Absent
                            </label>
                        </div>
                    </td>
                    
                </tr>
                <tr>
                    <td style="text-align:left;"> Hirtusism </td>
                    <td colspan="2">
                        <div class="btn-group btn-group-toggle female_general_examination_hirtusism" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="female_general_examination_hirtusism" value="mild" autocomplete="off"> Mild
                            </label>
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="female_general_examination_hirtusism" value="moderate" autocomplete="off"> Moderate
                            </label>
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="female_general_examination_hirtusism" value="severe" autocomplete="off"> Severe
                            </label>
                        </div>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
