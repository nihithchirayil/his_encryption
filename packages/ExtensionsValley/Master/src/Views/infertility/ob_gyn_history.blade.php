
<div class="col-md-12" style="margin-top:15px !important;">
    <div class="col-md-4 no-padding"><h5 style="margin-top: 5px;"><b>INFERTILITY : </b></h5></div>
    <div class="col-md-8 pull-left">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn bg-radio_grp">
              <input type="radio" name="infertility_female"  autocomplete="off" value="primary"> Primary
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="infertility_female" autocomplete="off" value="secondary"> Secondary
            </label>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 15px;">
    <h5><b>OB-GYN HISTORY</b></h5>
</div>
<div class="col-md-12">
    <div class="col-md-4">
        <h5 title="In years">Menarche :</h5>
    </div>
    <div class="col-md-8">
        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text menarche" placeholder="In years" id="" style="">
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-4">
        <h5 title="In years">Staying Together For </h5>
    </div>
    <div class="col-md-8">
        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text staying_together" placeholder="In years" id="" style="">
    </div>
</div>

<div class="col-md-12">
    <div class="col-md-4">
        <h5 title="In years">Married On :</h5>
    </div>
    <div class="col-md-8">
        <input type="text" data-attr="date" autocomplete="off" name="purchase_date_to" autofocus="" value="" class="form-control datepicker center_text bottom-border-text married_on" placeholder="MM-DD-YYYY" id="purchase_date_to" style="">
    </div>
</div>

<div class="col-md-12" style="margin-top:15px !important;">
    <div class="col-md-12">
        <div class="col-md-12">
            <ul class="select_button no-padding ob_gyn_history_multi_combo">
                    <li data-value="Abortion">
                        <i class="fa fa-check-circle"></i>Abortion
                        <input type="hidden" name="Abortion" id="" disabled="disabled" value="">
                    </li>
                    <li data-value="ND">
                        <i class="fa fa-check-circle"></i>ND
                        <input type="hidden" name="ND" id="" disabled="disabled" value="">
                    </li>
                    <li data-value="MTP">
                        <i class="fa fa-check-circle"></i>MTP
                        <input type="hidden" name="MTP" id="" disabled="disabled" value="">
                    </li>
                    <li data-value="LSCS">
                        <i class="fa fa-check-circle"></i>LSCS
                        <input type="hidden" name="LSCS" id="" disabled="disabled" value="">
                    </li>
            </ul>
        </div>

    </div>
</div>



<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>MENSTURAL CYCLE</b></h5>
</div>
<div class="col-md-12">

    <ul class="select_button no-padding menstural_cycle_multi_combo">
        <li data-value="Regular">
            <i class="fa fa-check-circle"></i>Regular
            <input type="hidden" name="Regular" id="" disabled="disabled" value="">
        </li>
        <li data-value="Scanly">
            <i class="fa fa-check-circle"></i>Scanly
            <input type="hidden" name="Scanly" id="" disabled="disabled" value="">
        </li>
        <li data-value="Painful">
            <i class="fa fa-check-circle"></i>Painful
            <input type="hidden" name="Painful" id="" disabled="disabled" value="">
        </li>
        <li data-value="Irregular">
            <i class="fa fa-check-circle"></i>Irregular
            <input type="hidden" name="Irregular" id="" disabled="disabled" value="">
        </li>
        <li data-value="Moderate">
            <i class="fa fa-check-circle"></i>Moderate
            <input type="hidden" name="Moderate" id="" disabled="disabled" value="">
        </li>
        <li data-value="Painless">
            <i class="fa fa-check-circle"></i>Painless
            <input type="hidden" name="Painless" id="" disabled="disabled" value="">
        </li>
        <li data-value="Profuse">
            <i class="fa fa-check-circle"></i>Profuse
            <input type="hidden" name="Profuse" id="" disabled="disabled" value="">
        </li>
    </ul>
</div>

<div class="col-md-12 no-padding" style="margin-top:15px !important;">
    <div class="col-md-8">
        <div class="col-md-3">
            <h5>LMP</h5>
        </div>
        <div class="col-md-9">
            <input type="text" data-attr="date" autocomplete="off" name="lmp_date" autofocus="" value="" class="form-control datepicker center_text bottom-border-text lmp_date" placeholder="MM-DD-YYYY" style="">
        </div>
    </div>
</div>
<div class="col-md-12 no-padding">
    <div class="col-md-8">
        <div class="col-md-3">
            <h5>PMC</h5>
        </div>
        <div class="col-md-9">
            <input type="text" data-attr="date" autocomplete="off" name="pmc_date" autofocus="" value="" class="form-control datepicker center_text bottom-border-text pmc_date" placeholder="MM-DD-YYYY" style="">
        </div>
    </div>
</div>
