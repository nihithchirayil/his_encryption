<div class="col-md-12 no-padding " style="position: sticky; top: 0; z-index: 999;">
    <div class="col-md-6  box-body text-blue text-center wife_block_head">
        <h4 style="margin-bottom:15px !important;"><b class="wife_header">Wife / Female : @isset($wife_info[0]){{$wife_info[0]->patient_name}} ( {{$wife_info[0]->uhid}} ) @endisset </b></h4>
    </div>
    <div class="col-md-6  box-body text-blue text-center husband_block_head">
        <h4 style="margin-bottom:15px !important;"><b class="husband_header">Husband / Male : @if(@isset($husband_info[0])) {{$husband_info[0]->patient_name}} ( {{$husband_info[0]->uhid}} ) @endif </b></h4>
    </div>
</div>

<div class="col-md-12 no-padding">

{{--  wife  --}}
    <div class="col-md-6 box-body text-center text-blue wife">
        
        <div class="col-md-12">
            <div class="col-md-3 text-right">
                <label>UHID</label>
            </div>
            <div class="col-md-1">:</div>
            <div class="col-md-8">
                <input type="text" autocomplete="off" name="" autofocus="" value="@isset($wife_info[0]){{$wife_info[0]->uhid}}@endisset" class="form-control bottom-border-text wife_uhid" @if(isset($wife_info[0]) && $main_patient_id == $wife_info[0]->patient_id ) readonly @endif placeholder="" id="" style="">
                <input type="hidden" id="op_no_search_hidden_wife" value="" name="op_no_search_hidden_wife">
                <div id="op_no_searchCodeAjaxDiv_wife" class="ajaxSearchBox" style="position:absolute;"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3 text-right">
                <label>Name</label>
            </div>
            <div class="col-md-1">:</div>
            <div class="col-md-8">
                <input type="text" autocomplete="off" name="" autofocus="" value="@isset($wife_info[0]){{$wife_info[0]->patient_name}} @endisset" class="form-control bottom-border-text" @if(isset($wife_info[0]) && $main_patient_id == $wife_info[0]->patient_id ) readonly @endif placeholder="" id="wife_name" style="">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3 text-right">
                <label>Age</label>
            </div>
            <div class="col-md-1">:</div>
            <div class="col-md-8">
                <input type="text" autocomplete="off" name="" autofocus="" value="@isset($wife_info[0]){{$wife_info[0]->age}} @endisset" class="form-control bottom-border-text" @if(isset($wife_info[0]) && $main_patient_id == $wife_info[0]->patient_id ) readonly @endif placeholder="" id="wife_age" style="">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3 text-right">
                <label>Blood Group</label>
            </div>
            <div class="col-md-1">:</div>
            <div class="col-md-8">
                <input type="text" autocomplete="off" name="" autofocus="" value="@isset($wife_info[0]){{$wife_info[0]->blood_group}} @endisset" class="form-control bottom-border-text" @if(@isset($wife_info[0]) && $main_patient_id == $wife_info[0]->patient_id ) readonly @endif placeholder="" id="wife_blood_group" style="">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3 text-right">
                <label>Last Visited Date</label>
            </div>
            <div class="col-md-1">:</div>
            <div class="col-md-8">
                <input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="@isset($wife_info[0]){{date('M-d-Y', strtotime($wife_info[0]->last_visit_datetime))}} @endisset" class="form-control datepicker table_text" @if(isset($wife_info[0]) && $main_patient_id == $wife_info[0]->patient_id ) readonly @endif placeholder="YYYY-MM-DD" id="wife_last_visit_date" style="">
            </div>
        </div>

    </div>


    {{--  husband  --}}
    <div class="col-md-6 box-body text-center text-blue husband">
        <div class="col-md-12">
            <div class="col-md-3 text-right">
                <label>UHID</label>
            </div>
            <div class="col-md-1">:</div>
            <div class="col-md-8">
                <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($husband_info[0])){{$husband_info[0]->uhid}}@endif" @if(isset($husband_info[0]) && $main_patient_id == $husband_info[0]->patient_id ) readonly @endif  class="form-control bottom-border-text" placeholder="" id="op_no_search" style="">
                <input type="hidden" id="op_no_search_hidden" value="" name="op_no_search_hidden">
                <div id="op_no_searchCodeAjaxDiv" class="ajaxSearchBox" style="position:absolute;"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3 text-right">
                <label>Name</label>
            </div>
            <div class="col-md-1">:</div>
            <div class="col-md-8">
                <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($husband_info[0])){{$husband_info[0]->patient_name}}@endif" @if(isset($husband_info[0]) && $main_patient_id == $husband_info[0]->patient_id ) readonly @endif  class="form-control bottom-border-text" placeholder="" id="husband_name" style="">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3 text-right">
                <label>Age</label>
            </div>
            <div class="col-md-1">:</div>
            <div class="col-md-8">
                <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($husband_info[0])){{$husband_info[0]->age}}@endif" @if(isset($husband_info[0]) && $main_patient_id == $husband_info[0]->patient_id ) readonly @endif  class="form-control bottom-border-text" placeholder="" id="husband_age" style="">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3 text-right">
                <label>Blood Group</label>
            </div>
            <div class="col-md-1">:</div>
            <div class="col-md-8">
                <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($husband_info[0])){{$husband_info[0]->blood_group}}@endif" @if(isset($husband_info[0]) && $main_patient_id == $husband_info[0]->patient_id ) readonly @endif  class="form-control bottom-border-text" placeholder="" id="husband_blood_group" style="">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3 text-right">
                <label>Last Visited Date</label>
            </div>
            <div class="col-md-1">:</div>
            <div class="col-md-8">
                <input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="@if(@isset($husband_info[0])){{date('M-d-Y', strtotime($husband_info[0]->last_visit_datetime))}}@endif" @if(isset($husband_info[0]) && $main_patient_id == $husband_info[0]->patient_id ) readonly @endif  class="form-control datepicker table_text" placeholder="YYYY-MM-DD" id="husband_last_visit_date" style="">
            </div>
        </div>
    </div>
</div>
