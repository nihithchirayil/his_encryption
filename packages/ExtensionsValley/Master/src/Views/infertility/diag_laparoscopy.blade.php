<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>DIAGNOSIS LAPAROSCOPY</b></h5>
</div>
<div class="col-md-12 no-padding">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="col-md-2">
                <h5>Date :</h5>
            </div>
            <div class="col-md-4">
                <input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="" class="form-control datepicker bottom-border-text female_diagnosis_laparoscopy_date" placeholder="MM-DD-YYYY" id="" style="">
            </div>
            <div class="col-md-2">
                <h5>Ovaries :</h5>
            </div>
            <div class="col-md-4">
                <div class="btn-group btn-group-toggle female_diagnosis_laparoscopy_ovaries" data-toggle="buttons">
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_diagnosis_laparoscopy_ovaries" value="rt" autocomplete="off"> RT
                    </label>
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_diagnosis_laparoscopy_ovaries" value="lt" autocomplete="off"> LT
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 no-padding">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="col-md-2">
                <h5>UT :</h5>
            </div>
            <div class="col-md-4">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_diagnosis_laparoscopy_ut" placeholder="" id="" style="">
            </div>
            <div class="col-md-2">
                <h5>Pelvis :</h5>
            </div>
            <div class="col-md-4">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_diagnosis_laparoscopy_pelvis" placeholder="" id="" style="">
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 no-padding">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="col-md-2">
                <h5>Tube :</h5>
            </div>
            <div class="col-md-4">
                <div class="btn-group btn-group-toggle female_diagnosis_laparoscopy_tube" data-toggle="buttons">
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_diagnosis_laparoscopy_tube" value="rt" autocomplete="off"> RT
                    </label>
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_diagnosis_laparoscopy_tube" value="lt" autocomplete="off"> LT
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <h5>Others :</h5>
            </div>
            <div class="col-md-4">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_diagnosis_laparoscopy_others" placeholder="" id="" style="">
            </div>
        </div>
    </div>
</div>
