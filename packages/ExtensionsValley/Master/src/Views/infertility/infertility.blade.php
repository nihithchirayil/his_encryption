@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/default/css/infertility.css")}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/master/default/css/new_emr.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
        rel="stylesheet">
<style type="text/css">
    .select_button>li{
        padding: 5px 9px;
    }
    .bg-primary{
        color: white;
        background: #337ab7;
    }
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="wife_patient_id" value="{{ $wife_patient_id }}">
<input type="hidden" id="husband_patient_id" value="{{ $husband_patient_id }}">
<input type="hidden" id="wife_encounter_id" value="{{ $wife_encounter_id }}">
<input type="hidden" id="husband_encounter_id" value="{{ $husband_encounter_id }}">
<input type="hidden" id="wife_visit_id" value="{{ $wife_visit_id }}">
<input type="hidden" id="husband_visit_id" value="{{ $husband_visit_id }}">
<!-- <input type="hidden" id="wife_visit_id" value="1461691">
<input type="hidden" id="husband_visit_id" value="899490"> -->
<input type="hidden" id="doctor_id" value="{{ $doctor_id }}">
<input type="hidden" id="doctor_prescription" value="{{ $doctor_id }}">
<input type="hidden" id="prescription_head_id" value="0">
<input type="hidden" id="prescription_consumable_head_id" value="0">
<input type="hidden" id="patient_id" value="{{ $wife_patient_id }}">
<input type="hidden" id="visit_id" value="{{ $wife_visit_id }}">
<!-- <input type="hidden" id="visit_id" value="1461691"> -->
<input type="hidden" id="encounter_id" value="{{ $wife_encounter_id }}">
<input type="hidden" id="main_patient_id" value="{{ $main_patient_id }}">
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="c_token" value="{{ csrf_token() }}">

<div class="right_col">
   <div class="col-md-12">
       <div class="col-md-12 no-padding text-center box-body">
            <h3><b>INFERTILITY</b></h3>
       </div>
        {{--  header section  --}}
        @include('Master::infertility.infertility_head')

        <!-- <div class="col-md-12 no-padding">
            <div class="col-md-6 text-center box-body text-center">
                <button name="gender_select" onclick="blockSelect(2);"  id="female_select" class="btn btn-primary gender_select">Female</button>
            </div>
            <div class="col-md-6 text-center box-body text-center">
                <button name="gender_select" onclick="blockSelect(1);" id="male_select" class="btn btn-primary gender_select">Male</button>
            </div>
        </div> -->
        <div class="col-md-12 no-padding">
            <!-- FEMALE BLOCK -->
            @include('Master::infertility.female_block')
            <!-- MALE BLOCK -->
            @include('Master::infertility.male_block')
            <div class="clearfix"></div>
            @include('Master::infertility.family_history')
        </div>
        
        <div class="col-md-12 box-body no-padding" style="margin-top: 40px;">
            <div class="col-md-6">
                @include('Master::infertility.infertility_female_investigations')
            </div>
            <div class="col-md-6">
                @include('Master::infertility.infertility_male_investigations')
            </div>
        </div>
        <div class="col-md-12 no-padding" style="margin-top: 30px;">
            <div class="col-md-2 no-padding">
                <h5 style="margin-top: 5px;"><b>PRESCRIPTION : </b></h5>
            </div>
            <div class="col-md-10 no-padding">
                <input type="checkbox" onchange="selectPrescriptionType();" class="prescription_type" checked data-toggle="toggle" data-on="Wife" data-off="Husband" data-onstyle="success" data-offstyle="primary">
            </div>
        </div> 
        <div class="col-md-12 no-padding" style="margin-top: 5px;">
            @include('Emr::emr.includes.patient_prescription')
        </div>


        <div class="col-md-12 no-padding" style="margin-top:20px;box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);border-radius: 3px;border: 1px solid #d2d6de;background: #ffffff none repeat scroll 0 0;height: auto !important;overflow: visible !important;padding: 0px !important;padding-top:3px !important;height:3600px; text-align:right;">
            <div class="col-md-1 col-md-offset-9 padding_sm" > 
                <button type="button" class="btn btn-block btn-primary " onclick="clickToTop();" > <i class="fa fa-arrow-up"> </i> Click to Top </button>
            </div>
            <div class="col-md-2 padding_sm saveInfertilityDiv" > 
                <button type="button" class="btn btn-block btn-success " onclick="saveInfertilityDetails();" > <i class="fa fa-save"> </i> Save </button>
            </div>
        </div>

   </div>
</div>
<!-- Custom Modals -->
@include('Emr::emr.includes.custom_modals')

@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/infertility.js")}}"></script>
<!-- Investigation -->
<script src="{{ asset('packages/extensionsvalley/emr/js/investigation.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script type="text/javascript">

</script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
@endsection
