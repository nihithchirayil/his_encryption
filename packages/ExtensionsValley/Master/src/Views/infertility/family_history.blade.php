<div class="col-md-12 box-body no-padding" style="margin-top:15px !important; padding-bottom: 10px !important;">

  <div class="col-md-12" >
      <h5><b>FAMILY HISTORY</b></h5>
  </div>

  <div class="col-md-12">
      <div class="theadscroll ps-container ps-theme-default" style="position: relative;">
          <table class="table no-margin theadfix_wrapper table-bordered table-condensed family_history_container" style="border: 1px solid #CCC;">
              <thead>
                  <tr style="font-weight:700;">
                      <td>Type</td>
                      <td>Husband/Male</td>
                      <td>Wife/Female</td>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td style="text-align:left; ">H.T</td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_ht_male" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_ht_male" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_ht_male" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_ht_female" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_ht_female" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_ht_female" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <td style="text-align:left; ">Diabetes</td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_diabetes_male" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_diabetes_male" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_diabetes_male" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_diabetes_female" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_diabetes_female" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_diabetes_female" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <td style="text-align:left; ">Thyroid</td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_thyroid_male" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_thyroid_male" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_thyroid_male" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_thyroid_female" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_thyroid_female" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_thyroid_female" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <td style="text-align:left; ">Kochs</td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_kochs_male" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_kochs_male" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_kochs_male" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_kochs_female" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_kochs_female" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_kochs_female" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <td style="text-align:left; ">Infertility</td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_infertility_male" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_infertility_male" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_infertility_male" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_infertility_female" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_infertility_female" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_infertility_female" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <td style="text-align:left; ">Anomalies</td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_anomalies_male" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_anomalies_male" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_anomalies_male" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                      <td>
                          <div class="btn-group btn-group-toggle family_history_anomalies_female" data-toggle="buttons">
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_anomalies_female" value="yes" autocomplete="off"> Yes
                              </label>
                              <label class="btn bg-radio_grp">
                                <input type="radio" name="family_history_anomalies_female" value="no" autocomplete="off"> No
                              </label>
                          </div>
                      </td>
                  </tr>
              </tbody>
          </table>
      </div>
  </div>
</div>
