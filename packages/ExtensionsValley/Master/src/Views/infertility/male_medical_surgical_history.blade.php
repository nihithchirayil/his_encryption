<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>MEDICAL AND SURGICAL HISTORY</b></h5>
</div>
<div class="col-md-12">
    <ul class="select_button no-padding medical_and_surgical_history_multi_combo">
        <li data-value="medical_disease_ht">
            <i class="fa fa-check-circle"></i>Medical Disease HT
            <input type="hidden" name="medical_disease_ht" disabled="disabled" value="medical_disease_ht">
        </li>
        <li data-value="diabetes">
            <i class="fa fa-check-circle"></i>Diabetes
            <input type="hidden" name="diabetes" disabled="disabled" value="diabetes">
        </li>
        <li data-value="kochs">
            <i class="fa fa-check-circle"></i>Koch's
            <input type="hidden" name="kochs" disabled="disabled" value="kochs">
        </li>
        <li data-value="ch_resp_tt_disease">
            <i class="fa fa-check-circle"></i>Ch.resp.tt.disease
            <input type="hidden" name="ch_resp_tt_disease" disabled="disabled" value="ch_resp_tt_disease">
        </li>
        <li data-value="cns_disease">
            <i class="fa fa-check-circle"></i>CNS disease
            <input type="hidden" name="cns_disease" disabled="disabled" value="cns_disease">
        </li>
        <li data-value="psych_dis_cvs">
            <i class="fa fa-check-circle"></i>Psych.Dis.CVS
            <input type="hidden" name="psych_dis_cvs" disabled="disabled" value="psych_dis_cvs">
        </li>
    </ul>
</div>

<div class="col-md-12">
    <div class="col-md-4">Fever In The Past :</div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle medical_and_surgical_history_fever_in_past" data-toggle="buttons">
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_fever_in_past" value="yes" autocomplete="off"> Yes
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_fever_in_past" value="no" autocomplete="off"> No
            </label>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">Urinary Infection :</div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle medical_and_surgical_history_urinary_infection" data-toggle="buttons">
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_urinary_infection" value="yes" autocomplete="off"> Yes
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_urinary_infection" value="no" autocomplete="off"> No
            </label>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">STD :</div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle medical_and_surgical_history_std" data-toggle="buttons">
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_std" value="no" autocomplete="off"> No
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_std" value="syph" autocomplete="off"> Syph
            </label>

            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_std" value="chalm" autocomplete="off"> Chalm
            </label>

            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_std" value="others" autocomplete="off"> Others
            </label>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">Epididymitis :</div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle medical_and_surgical_history_epididymitis" data-toggle="buttons">
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_epididymitis" value="yes" autocomplete="off"> Yes
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_epididymitis" value="no" autocomplete="off"> No
            </label>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">Testicular Maldescent :</div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle medical_and_surgical_history_testicular_maldescent" data-toggle="buttons">
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_testicular_maldescent" value="yes" autocomplete="off"> Yes
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_testicular_maldescent" value="no" autocomplete="off"> No
            </label>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">Possible Testicular Damage :</div>
    <div class="col-md-8">
        <ul class="select_button no-padding medical_and_surgical_history_possible_testicular_damage_multi_combo">
            <li data-value="mumps">
                <i class="fa fa-check-circle"></i>Mumps
                <input type="hidden" name="mumps" disabled="disabled" value="mumps">
            </li>
            <li data-value="orchitis">
                <i class="fa fa-check-circle"></i>Orchitis
                <input type="hidden" name="orchitis" disabled="disabled" value="orchitis">
            </li>
            <li data-value="injury_or_torsion">
                <i class="fa fa-check-circle"></i>Injury/Torsion
                <input type="hidden" name="injury_or_torsion" disabled="disabled" value="injury_or_torsion">
            </li>
            <li data-value="others">
                <i class="fa fa-check-circle"></i>Others
                <input type="hidden" name="others" disabled="disabled" value="others">
            </li>
        </ul>
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-4">Details :</div>
    <div class="col-md-8">
        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text medical_and_surgical_history_details" placeholder="" id="" style="">
    </div>
</div>

<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">Varicocele Treatment :</div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle medical_and_surgical_history_varicocele_treatment" data-toggle="buttons">
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_varicocele_treatment" value="yes" autocomplete="off"> Yes
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="medical_and_surgical_history_varicocele_treatment" value="no" autocomplete="off"> No
            </label>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">Surgery :</div>
    <div class="col-md-8">
        <ul class="select_button no-padding medical_and_surgical_history_surgery_multi_combo">
            <li data-value="ureteral_stricture">
                <i class="fa fa-check-circle"></i>Ureteral Stricture
                <input type="hidden" name="ureteral_stricture" disabled="disabled" value="ureteral_stricture">
            </li>
            <li data-value="prostage">
                <i class="fa fa-check-circle"></i>Prostage
                <input type="hidden" name="prostage" disabled="disabled" value="prostage">
            </li>
            <li data-value="vasectomy_injection_hernia">
                <i class="fa fa-check-circle"></i>Vasectomy Injection Hernia
                <input type="hidden" name="vasectomy_injection_hernia" disabled="disabled" value="vasectomy_injection_hernia">
            </li>
        </ul>
    </div>
</div>

