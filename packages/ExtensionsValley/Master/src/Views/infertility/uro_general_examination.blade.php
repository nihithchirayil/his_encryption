<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>URO GENERAL EXAMINATION</b></h5>
</div>

<div class="col-md-12">
    <div class="theadscroll ps-container ps-theme-default" style="position: relative;">
        <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC;">
            <tbody>

                <tr>
                    <td style="text-align:left; width: 150px; ">Penis</td>
                    <td>
                      <div class="btn-group btn-group-toggle uro_general_examination_penis" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_penis" value="normal" autocomplete="off"> Normal
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_penis" value="scars" autocomplete="off"> Scars
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_penis" value="plagves" autocomplete="off"> Plagves
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_penis" value="hypospadias" autocomplete="off"> Hypospadias
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_penis" value="others" autocomplete="off"> Others
                            </label>
                        </div>
                    </td>
                </tr>

                <tr> 
                    <td style="text-align:left; width: 150px; "> Details </td>
                    <td>
                      <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text uro_general_examination_details" placeholder="" id="" style="">
                    </td>
                </tr>

                <tr>
                    <td style="text-align:left; width: 150px; ">Tests </td>
                    <td>
                      <div class="btn-group btn-group-toggle uro_general_examination_tests" data-toggle="buttons">
                          <label class="btn bg-radio_grp">
                            <input type="radio" name="uro_general_examination_tests" value="palpable" autocomplete="off"> Palpable
                          </label>
                          <label class="btn bg-radio_grp">
                            <input type="radio" name="uro_general_examination_tests" value="nonpalpable" autocomplete="off"> Non Palpable
                          </label>
                          <label class="btn bg-radio_grp">
                            <input type="radio" name="uro_general_examination_tests" value="rside" autocomplete="off"> R Side
                          </label>
                          <label class="btn bg-radio_grp">
                            <input type="radio" name="uro_general_examination_tests" value="lside" autocomplete="off"> L Side
                          </label>

                      </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left; width: 150px; ">Site</td>
                    <td>
                        <div class="btn-group btn-group-toggle uro_general_examination_site" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_site" value="bothnormal" autocomplete="off"> Both Normal
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_site" value="abnormal" autocomplete="off"> Abnormal
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_site" value="rside" autocomplete="off"> R Side
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_site" value="lside" autocomplete="off"> L Side
                            </label>

                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align:left; width: 150px; ">
                        <div class="col-md-5 no-padding" style="text-align:left;">Volume</div>
                        <div class="col-md-5 no-padding" style="text-align:left;">
                            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text uro_general_examination_volume_text" placeholder="" id="" style=""> 
                        </div>
                        <div class="col-md-2 no-padding">ml</div>
                    </td>
                    <td>
                      <div class="btn-group btn-group-toggle uro_general_examination_volume" data-toggle="buttons">

                        <label class="btn bg-radio_grp">
                          <input type="radio" name="uro_general_examination_volume" value="rside" autocomplete="off"> R Side
                        </label>
                        <label class="btn bg-radio_grp">
                          <input type="radio" name="uro_general_examination_volume" value="lside" autocomplete="off"> L Side
                        </label>

                        </div>
                    </td>
                  </tr>
                  <tr>
                    <td style="text-align:left; width: 150px; ">Epididymitis
                    </td>
                    <td> 
                        <div class="btn-group btn-group-toggle uro_general_examination_epididymitis" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="uro_general_examination_epididymitis" value="normal" autocomplete="off">Normal
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="uro_general_examination_epididymitis" value="thickened" autocomplete="off"> Thickened
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="uro_general_examination_epididymitis" value="nonpalpable" autocomplete="off"> Non Palpable
                                </label>
                            </div>
                    </td>
                  </tr>

                  <tr>
                    <td style="text-align:left; width: 150px; "> Scrotal Swelling </td>
                    <td>
                      <div class="btn-group btn-group-toggle uro_general_examination_scrotal_swelling" data-toggle="buttons">

                        <label class="btn bg-radio_grp">
                          <input type="radio" name="uro_general_examination_scrotal_swelling" value="none" autocomplete="off"> None
                        </label>
                        <label class="btn bg-radio_grp">
                          <input type="radio" name="uro_general_examination_scrotal_swelling" value="hernia" autocomplete="off"> Hernia
                        </label>
                        <label class="btn bg-radio_grp">
                          <input type="radio" name="uro_general_examination_scrotal_swelling" value="hydrocele" autocomplete="off"> Hydrocele
                        </label>

                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align:left; width: 150px; ">
                    Varicocele
                    </td>
                    <td>
                      <div class="btn-group btn-group-toggle uro_general_examination_varicocele" data-toggle="buttons">
                          <label class="btn bg-radio_grp">
                            <input type="radio" name="uro_general_examination_varicocele" value="none" autocomplete="off">None
                          </label>
                          <label class="btn bg-radio_grp">
                            <input type="radio" name="uro_general_examination_varicocele" value="grede" autocomplete="off"> Grede
                          </label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align:left; width: 150px; "> Injuinal Exam </td>
                    <td>
                        <div class="btn-group btn-group-toggle uro_general_examination_injuinal_exam" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_injuinal_exam" value="normal" autocomplete="off"> Normal
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_injuinal_exam" value="scary" autocomplete="off"> Scary
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_injuinal_exam" value="lymphnode" autocomplete="off"> Lymph node
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="uro_general_examination_injuinal_exam" value="hernia" autocomplete="off"> Hernia
                            </label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align:left; width: 150px; "> Rectual Exam </td>
                    <td>
                        <div class="btn-group btn-group-toggle uro_general_examination_rectual_exam" data-toggle="buttons">

                          <label class="btn bg-radio_grp">
                            <input type="radio" name="uro_general_examination_rectual_exam" value="normal" autocomplete="off"> Normal
                          </label>
                          <label class="btn bg-radio_grp">
                            <input type="radio" name="uro_general_examination_rectual_exam" value="prostatetender" autocomplete="off"> Prostate-tender
                          </label>
                          <label class="btn bg-radio_grp">
                            <input type="radio" name="uro_general_examination_rectual_exam" value="softswelling" autocomplete="off"> Soft Swelling
                          </label>
                          <label class="btn bg-radio_grp">
                            <input type="radio" name="uro_general_examination_rectual_exam" value="hardswelling" autocomplete="off"> Hard Swelling
                          </label>
                          <label class="btn bg-radio_grp">
                            <input type="radio" name="uro_general_examination_rectual_exam" value="others" autocomplete="off"> Others
                          </label>

                          </div>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
<!-- <div class="col-md-12">
    <div class="col-md-3">Details:</div>
    <div class="col-md-9">
        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text" placeholder="" id="" style="">
    </div>
</div> -->

