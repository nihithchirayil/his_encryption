<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>PREVIOUS INFERTILITY INVESTIGATIONS AND THERAPY</b></h5>
</div>
<div class="col-md-12 no-padding">
    <div class="col-md-10">
        <div class="col-md-12">
            <div class="col-md-3">
                <h5>HGS:</h5>
            </div>
            <div class="col-md-9">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_previous_infertility_hgs_detail" placeholder="" id="" style="">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-2">
                <h5>Date:</h5>
            </div>
            <div class="col-md-7" style="margin-top:5px;">
                <input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="" class="form-control datepicker bottom-border-text female_previous_infertility_hgs_date" placeholder="MM-DD-YYYY" id="" style="">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-2">
                <h5>Place:</h5>
            </div>
            <div class="col-md-7">
                <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_previous_infertility_hgs_place" placeholder="" id="" style="">
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 no-padding" style="margin-top:15px !important;">
    <div class="col-md-10">
        <div class="col-md-12">
            <div class="col-md-3">
                <h5 style="margin:0;">Uterus:</h5>
            </div>
            <div class="col-md-9">
                <div class="btn-group btn-group-toggle female_previous_infertility_uterus" data-toggle="buttons">
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_previous_infertility_uterus" value="normal" autocomplete="off"> Normal
                    </label>
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_previous_infertility_uterus" value="abnormal" autocomplete="off"> Abnormal
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 no-padding" style="margin-top:15px !important;">
    <div class="col-md-10">
        <div class="col-md-12">
            <div class="col-md-3">
                <h5 style="margin:0;">Tubes:</h5>
            </div>
            <div class="col-md-9">
                <div class="btn-group btn-group-toggle female_previous_infertility_tubes" data-toggle="buttons">
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_previous_infertility_tubes" value="rt" autocomplete="off"> RT
                    </label>
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_previous_infertility_tubes" value="rt_blocked" autocomplete="off"> Blocked
                    </label>
                    <label class="btn bg-radio_grp">
                      <input type="radio" name="female_previous_infertility_tubes" value="rt_patent" autocomplete="off"> Patent
                    </label>
                    <label class="btn bg-radio_grp">
                        <input type="radio" name="female_previous_infertility_tubes" value="lt" autocomplete="off"> LT
                    </label>
                    <label class="btn bg-radio_grp">
                        <input type="radio" name="female_previous_infertility_tubes" value="lt_blocked" autocomplete="off"> Blocked
                    </label>
                    <label class="btn bg-radio_grp">
                        <input type="radio" name="female_previous_infertility_tubes" value="lt_patent" autocomplete="off"> Patent
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
