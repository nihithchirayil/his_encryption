<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>GENERAL AND PHYSICAL EXAMINATION</b></h5>
</div>

<div class="col-md-10">
    <div class="theadscroll ps-container ps-theme-default" style="position: relative;">
        <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC;">
            <tbody>
                <tr>
                    <td>
                        <div class="col-md-3 no-padding" style="text-align:left;">Height</div>
                        <div class="col-md-7 no-padding" style="text-align:left;">
                            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text general_and_physical_examination_height" placeholder="" id="" style="">
                        </div>
                        <div class="col-md-2 no-padding">cms</div>
                    </td>
                    <td>
                        <div class="col-md-3 no-padding" style="text-align:left;">Weight</div>
                        <div class="col-md-7 no-padding" style="text-align:left;">
                            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text general_and_physical_examination_weight" placeholder="" id="" style="">
                        </div>
                        <div class="col-md-2 no-padding">lbs</div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align:left;">
                        General And Physical Exam
                    </td>
                    <td>
                        <div class="btn-group btn-group-toggle general_and_physical_examination_general_and_physical_exam" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="general_and_physical_examination_general_and_physical_exam" value="normal" autocomplete="off"> Normal
                            </label>
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="general_and_physical_examination_general_and_physical_exam" value="abnormal" autocomplete="off"> Abnormal
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left;">
                        Siens Of Virilization
                    </td>
                    <td>
                        <div class="btn-group btn-group-toggle general_and_physical_examination_siens_of_virilization" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="general_and_physical_examination_siens_of_virilization" value="normal" autocomplete="off"> Normal
                            </label>
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="general_and_physical_examination_siens_of_virilization" value="abnormal" autocomplete="off"> Abnormal
                            </label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align:left;">
                        Gynecomastia
                    </td>
                    <td>
                        <div class="btn-group btn-group-toggle general_and_physical_examination_gynecomastia" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="general_and_physical_examination_gynecomastia" value="absent" autocomplete="off"> Absent
                            </label>
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="general_and_physical_examination_gynecomastia" value="tannerstage" autocomplete="off"> Tanner Stage
                            </label>
                        </div>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
