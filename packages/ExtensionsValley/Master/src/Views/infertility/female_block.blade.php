<div class="col-md-6 no-padding box-body" id="female_block">
    @include('Master::infertility.ob_gyn_history')
    @include('Master::infertility.hormonal_factor')
    @include('Master::infertility.pelvic_tuval_uterine_facturs')
    @include('Master::infertility.cervical_factor')
    @include('Master::infertility.female_history')
    @include('Master::infertility.previous_inf_inv_therapy')
    @include('Master::infertility.diag_laparoscopy')
    @include('Master::infertility.hysteroscopy')
    @include('Master::infertility.previous_induction')
    @include('Master::infertility.previous_ivi')
    @include('Master::infertility.female_general_examination')
    @include('Master::infertility.pelvic_examination')
</div>
