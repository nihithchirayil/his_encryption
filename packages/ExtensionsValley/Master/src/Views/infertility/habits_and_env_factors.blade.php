<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>HABITS AND ENVIRONMENTAL FACTORS</b></h5>
</div>

<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">Smoking :</div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle habits_and_environmental_factors_smoking" data-toggle="buttons">
            <label class="btn bg-radio_grp">
              <input type="radio" name="habits_and_environmental_factors_smoking" value="yes" autocomplete="off"> Yes
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="habits_and_environmental_factors_smoking" value="no" autocomplete="off"> No
            </label>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">Alcohol :</div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle habits_and_environmental_factors_alcohol" data-toggle="buttons">
            <label class="btn bg-radio_grp">
              <input type="radio" name="habits_and_environmental_factors_alcohol" value="yes" autocomplete="off"> Yes
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="habits_and_environmental_factors_alcohol" value="no" autocomplete="off"> No
            </label>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">Environmental And Occupation Factors :</div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle habits_and_environmental_factors_environmental_and_occupation_factor" data-toggle="buttons">
            <label class="btn bg-radio_grp">
              <input type="radio" name="habits_and_environmental_factors_environmental_and_occupation_factor" value="heat" autocomplete="off"> Heat
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="habits_and_environmental_factors_environmental_and_occupation_factor" value="toxic" autocomplete="off"> Toxic
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="habits_and_environmental_factors_environmental_and_occupation_factor" value="others" autocomplete="off"> Others
            </label>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">Details :</div>
    <div class="col-md-8">
        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text habits_and_environmental_factors_details" placeholder="" id="" style="">
    </div>
</div>

<div class="col-md-12" style="margin-top:10px !important;">
    <div class="col-md-4">Drug Abuse :</div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle habits_and_environmental_factors_drug_abuse" data-toggle="buttons">
            <label class="btn bg-radio_grp">
              <input type="radio" name="habits_and_environmental_factors_drug_abuse" value="yes" autocomplete="off"> Yes
            </label>
            <label class="btn bg-radio_grp">
              <input type="radio" name="habits_and_environmental_factors_drug_abuse" value="no" autocomplete="off"> No
            </label>
        </div>
    </div>
</div>
