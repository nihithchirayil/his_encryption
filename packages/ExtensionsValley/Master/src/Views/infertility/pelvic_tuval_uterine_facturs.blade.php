<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>PELVIC/TUVAL/UTERINE FACTORS</b></h5>
</div>

<div class="col-md-12">
    <div class="col-md-10 ">
        <div class="theadscroll" style="position: relative;">
            <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC;">
                <tbody>
                    <tr>
                        <td style="text-align: left;"> Pelvic Pain :</td>
                        <td>
                            <div class="btn-group btn-group-toggle female_pelvic_pain" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_pelvic_pain" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_pelvic_pain" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: left;"> Previous Pelvic Infection :</td>
                        <td>
                            <div class="btn-group btn-group-toggle female_pelvic_infection" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_pelvic_infection" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_pelvic_infection" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </td>

                    </tr>

                    <tr>
                        <td style="text-align: left;"> Previous Pelvic Surgery :</td>
                        <td>
                            <div class="btn-group btn-group-toggle female_pelvic_surgery" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_pelvic_surgery" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_pelvic_surgery" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: left;"> Dyspareunia :</td>
                        <td>
                            <div class="btn-group btn-group-toggle female_pelvic_dyspareunia" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_pelvic_dyspareunia" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_pelvic_dyspareunia" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </td>

                    </tr>

                    <tr>
                        <td style="text-align: left;"> Dysmenorria :</td>
                        <td>
                            <div class="btn-group btn-group-toggle female_pelvic_dysmenorria" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_pelvic_dysmenorria" value="mild" autocomplete="off"> Mild
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_pelvic_dysmenorria" value="moderate" autocomplete="off"> Moderate
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_pelvic_dysmenorria" value="severe" autocomplete="off"> Severe
                                </label>
                            </div>
                        </td>
                    </tr>
            </table>
        </div>
    </div>

</div>
