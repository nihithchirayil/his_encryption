<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>HORMONAL FACTOR</b></h5>
</div>

<div class="col-md-12">
    <div class="col-md-10">
        <div class="theadscroll" style="position: relative;">
            <table class="table no-margin theadfix_wrapper table-bordered table-condensed female_hormonal_factor" style="border: 1px solid #CCC;">
                <tbody>
                    <tr>
                        <td style="text-align: left;"> Weight :</td>
                        <td>
                            <div class="btn-group btn-group-toggle female_hormonal_weight" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_hormonal_weight" value="gain" autocomplete="off"> Gain
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_hormonal_weight" value="loss" autocomplete="off"> Loss
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_hormonal_weight" value="nill" autocomplete="off"> Nill
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;"> Mastalgin : </td>
                        <td>
                            <div class="btn-group btn-group-toggle female_hormonal_mastalgin" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_hormonal_mastalgin" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_hormonal_mastalgin" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;"> Hirtusism: </td>
                        <td>
                            <div class="btn-group btn-group-toggle female_hormonal_hirtusism" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_hormonal_hirtusism" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_hormonal_hirtusism" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: left;"> Galactorrhoea: </td>
                        <td>
                            <div class="btn-group btn-group-toggle female_hormonal_galactorrhoea" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_hormonal_galactorrhoea" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_hormonal_galactorrhoea" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: left;"> Acne: </td>
                        <td>
                            <div class="btn-group btn-group-toggle female_hormonal_acne" data-toggle="buttons">
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_hormonal_acne" value="yes" autocomplete="off"> Yes
                                </label>
                                <label class="btn bg-radio_grp">
                                  <input type="radio" name="female_hormonal_acne" value="no" autocomplete="off"> No
                                </label>
                            </div>
                        </td>
                    </tr>

            </table>
        </div>
    </div>

</div>
