<div class="col-md-12" style="margin-top:15px !important;">
    <h5><b>PELVIC EXAMINATION</b></h5>
</div>

<div class="col-md-10" style="margin-bottom:20px;">
    <div class="theadscroll ps-container ps-theme-default" style="position: relative;">
        <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC;">
            <tbody>
                <tr>
                    <td style="text-align:left;">
                      Introitus:
                    </td>
                    <td>
                        <div class="btn-group btn-group-toggle female_general_examination_introitus" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="female_general_examination_introitus" value="normal" autocomplete="off"> Normal
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="female_general_examination_introitus" value="abnormal" autocomplete="off"> Abnormal
                            </label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align:left;">
                      Cx:
                    </td>
                    <td>
                        <div class="btn-group btn-group-toggle female_general_examination_cx" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="female_general_examination_cx" value="normal" autocomplete="off"> Normal
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="female_general_examination_cx" value="abnormal" autocomplete="off"> Abnormal
                            </label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align:left;">
                      Uterus Size:
                    </td>
                    <td>
                        <div class="btn-group btn-group-toggle female_general_examination_uterus_size" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="female_general_examination_uterus_size" value="small" autocomplete="off"> Small
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="female_general_examination_uterus_size" value="normal" autocomplete="off"> Normal
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="female_general_examination_uterus_size" value="enlarged" autocomplete="off"> Enlarged
                            </label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align:left;">
                      R/V A/V:
                    </td>
                    <td>
                        <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control center_text bottom-border-text female_general_examination_rv_av" placeholder="" id="" style="">
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left;">
                     Mobility:
                    </td>
                    <td>
                        <div class="btn-group btn-group-toggle female_general_examination_mobility" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="female_general_examination_mobility" value="yes" autocomplete="off"> Yes
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="female_general_examination_mobility" value="no" autocomplete="off"> No
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left;">
                        Adnexa:
                    </td>
                    <td>
                        <div class="btn-group btn-group-toggle female_general_examination_adnexa" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="female_general_examination_adnexa" value="normal" autocomplete="off"> Normal
                            </label>
                            <label class="btn bg-radio_grp">
                              <input type="radio" name="female_general_examination_adnexa" value="abnormal" autocomplete="off"> Abnormal
                            </label>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
