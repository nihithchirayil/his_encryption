   <div id="print_data" class="theadscroll" style="position: relative; height: 430px;">
       <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
           <thead>
               <tr class="table_header_bg" style="cursor: pointer;">
                   <th width="20%">DATE</th>
                   <th width="20%">sales INV No.</th>
                   <th width="20%">sales 0 %</th>
                   <th width="20%">sales 3%</th>
                   <th width="20%">CGST 1.5%</th>
                   <th width="20%">SGST 1.5%</th>
                   <th width="20%">IGST 3%</th>
                   <th width="20%">sales 5%</th>
                   <th width="20%">CGST 2.5 %</th>
                   <th width="20%">SGST 2.5%</th>
                   <th width="20%">IGST 5%</th>
                   <th width="20%">sales 12%</th>
                   <th width="20%">CGST 6%</th>
                   <th width="20%">SGST 6%</th>
                   <th width="20%">IGST 12%</th>
                   <th width="20%">sales 18%</th>
                   <th width="20%">CGST 9%</th>
                   <th width="20%">SGST 9%</th>
                   <th width="20%">IGST 18 %</th>
                   <th width="20%">sales 28%</th>
                   <th width="20%">CGST 14%</th>
                   <th width="20%">SGST 14%</th>
                   <th width="20%">IGST 28 %</th>
                   <th width="20%">Cess</th>
                   <th width="20%">Flood Cess</th>
                   <th width="20%">Discount</th>
                   <th width="20%">ROUND OFF</th>
                   <th width="20%">BILL AMT</th>
                   <th width="20%">CUSTOMER</th>
                   <th width="20%">GSTIN</th>
                   <th width="20%">STATE</th>
                   <th width="20%">VCH TYPE</th>
               </tr>
           </thead>
           <tbody>
               <?php
                if (count($post_data) != 0) {
                    $net_amount_tot=0.0;
                    foreach ($post_data as $each) {
                        $tax_amt = ($each['SALES0']+$each['SALES3']+$each['CGST15']+$each['SGST15']+$each['IGST3']+$each['SALES5']+$each['CGST25']+$each['SGST25']+$each['IGST5']+$each['SALES12']+$each['CGST6']+$each['SGST6']+$each['IGST12']+$each['SALES18']+$each['CGST9']+$each['SGST9']+$each['IGST18']+$each['SALES28']+$each['CGST14']+$each['SGST14']+$each['IGST28']+$each['CESS'])-(floatval($each['discount']));
                        $roundoff = (floatval($tax_amt) -$each['net_amount']);

                    ?>
               <tr>
                   <td class="common_td_rules"><?= $each['bill_date'] ?></td>
                   <td class="common_td_rules"><?= $each['bill_no'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SALES0'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SALES3'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CGST15'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SGST15'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['IGST3'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SALES5'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CGST25'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SGST25'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['IGST5'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SALES12'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CGST6'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SGST6'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['IGST12'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SALES18'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CGST9'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SGST9'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['IGST18'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SALES28'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CGST14'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SGST14'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['IGST28'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CESS'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['FLOODCESS'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['discount'] ?></td>
                   <td class="td_common_numeric_rules"><?= number_format($roundoff, 2, '.', '') ?></td>
                   <td class="td_common_numeric_rules"><?= $each['net_amount'] ?></td>
                   <td class="common_td_rules"><?= $each['patient'] ?></td>
                   <td class="common_td_rules"><?= $each['patient_gstin'] ?></td>
                   <td class="common_td_rules"><?= $each['state'] ?></td>
                   <td class="common_td_rules">Sales</td>
               </tr>
               <?php
                }
            } else {
                ?>
               <tr class="common_td_rules">
                   <td colspan="33" style="text-align: center;"> No Result Found</td>
               </tr>
               <?php
            }
            ?>

           </tbody>
       </table>
   </div>
   <div class="col-md-1 padding_sm pull-right">
       <button type="button" onclick="postDataToSoftware()" id="postDataToSwBtn" class="btn btn-success btn-block">Post
           S/W <i id="postDataToSwSpin" class="fa fa-calculator"></i></button>
   </div>
   <div class="col-md-2 padding_sm pull-right">
       <button type="button" onclick="postDataToTally()" id="postDataToTallyBtn" class="btn btn-warning btn-block">Post
           Tally <i id="postDataToTallySpin" class="fa fa-list"></i></button>
   </div>
