   <div id="print_data" class="theadscroll" style="position: relative; height: 430px;">
       <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
           <thead>
               <tr class="table_header_bg" style="cursor: pointer;">
                   <th width="20%">DATE</th>
                   <th width="20%">PUR IN.NO</th>
                   <th width="20%">SUPL INV .NO</th>
                   <th width="20%">SUPL INV DATE</th>
                   <th width="20%">PURCHASE 0%</th>
                   <th width="20%">PURCHASE 3%</th>
                   <th width="20%">CGST 1.5%</th>
                   <th width="20%">SGST 1.5%</th>
                   <th width="20%">IGST 3%</th>
                   <th width="20%">PURCHASE 5%</th>
                   <th width="20%">CGST 2.5 %</th>
                   <th width="20%">SGST 2.5%</th>
                   <th width="20%">IGST 5%</th>
                   <th width="20%">PURCHASE 12%</th>
                   <th width="20%">CGST 6%</th>
                   <th width="20%">SGST 6%</th>
                   <th width="20%">IGST 12%</th>
                   <th width="20%">PURCHASE 18%</th>
                   <th width="20%">CGST 9%</th>
                   <th width="20%">SGST 9%</th>
                   <th width="20%">IGST 18 %</th>
                   <th width="20%">PURCHASE 28%</th>
                   <th width="20%">CGST 14%</th>
                   <th width="20%">SGST 14%</th>
                   <th width="20%">IGST 28%</th>
                   <th width="20%">CESS</th>
                   <th width="20%">DISCOUNT</th>
                   <th width="20%">ROUNDOFF</th>
                   <th width="20%">BILLAMT</th>
                   <th width="20%">SUPPLIER</th>
                   <th width="20%">GSTIN</th>
                   <th width="20%">STATE</th>
                   <th width="20%">Vch Type</th>
                   <th width="20%">Narration</th>
               </tr>
           </thead>
           <tbody>
               <?php
               function validateDate($date, $format = 'd-M-y')
                {
                    $d = DateTime::createFromFormat($format, $date);
                    return $d && $d->format($format) === $date;
                }
                if (count($post_data) != 0) {
                    $net_amount_tot=0.0;
                    foreach ($post_data as $each) {
                        $tax_amt = ($each['PURCHASE0']+$each['PURCHASE3']+$each['CGST15']+$each['SGST15']+$each['IGST3']+$each['PURCHASE5']+$each['CGST25']+$each['SGST25']+$each['IGST5']+$each['PURCHASE12']+$each['CGST6']+$each['SGST6']+$each['IGST12']+$each['PURCHASE18']+$each['CGST9']+$each['SGST9']+$each['IGST18']+$each['PURCHASE28']+$each['CGST14']+$each['SGST14']+$each['IGST28']+$each['CESS'])-(floatval($each['discount']));
                        $roundoff = floatval($tax_amt) -$each['net_amount'];

                    ?>
               <tr>
                   <td class="common_td_rules">
                       <?= $servieDate ? date('M-d-Y', strtotime($servieDate)) : '-' ?></td>
                   <td class="common_td_rules"><?= $each['bill_no'] ?></td>
                   <td class="common_td_rules"><?= $each['invoice_no'] ?></td>
                   <td class="common_td_rules"><?= $each['invoice_date'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['PURCHASE0'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['PURCHASE3'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CGST15'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SGST15'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['IGST3'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['PURCHASE5'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CGST25'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SGST25'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['IGST5'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['PURCHASE12'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CGST6'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SGST6'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['IGST12'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['PURCHASE18'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CGST9'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SGST9'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['IGST18'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['PURCHASE28'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CGST14'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['SGST14'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['IGST28'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['CESS'] ?></td>
                   <td class="td_common_numeric_rules"><?= $each['discount'] ?></td>
                   <td class="td_common_numeric_rules"><?= number_format($roundoff, 2, '.', '') ?></td>
                   <td class="td_common_numeric_rules"><?= $each['net_amount'] ?></td>
                   <td class="common_td_rules"><?= $each['vendor_name'] ?></td>
                   <td class="common_td_rules"><?= $each['gst_vendor_code'] ?></td>
                   <td class="common_td_rules"><?= $each['state'] ?></td>
                   <td class="common_td_rules">PURCHASE</td>
                   <td class="common_td_rules"><?= $each['narration'] ?></td>
               </tr>
               <?php
                }
            } else {
                ?>
               <tr class="common_td_rules">
                   <td colspan="33" style="text-align: center;"> No Result Found</td>
               </tr>
               <?php
            }
            ?>

           </tbody>
       </table>
   </div>
   <div class="col-md-1 padding_sm pull-right">
       <button type="button" onclick="postDataToSoftware()" id="postDataToSwBtn" class="btn btn-success btn-block">Post
           S/W <i id="postDataToSwSpin" class="fa fa-calculator"></i></button>
   </div>
   <div class="col-md-2 padding_sm pull-right">
       <button type="button" onclick="postDataToTally()" id="postDataToTallyBtn" class="btn btn-warning btn-block">Post
           Tally <i id="postDataToTallySpin" class="fa fa-list"></i></button>
   </div>
