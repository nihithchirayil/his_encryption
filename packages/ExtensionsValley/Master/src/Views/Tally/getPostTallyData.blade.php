<?php
    $company_code=\DB::table('company')->where('id','=',1)->value('code');
?>
   <div class="theadscroll" style="position: relative; height: 430px;">
       <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
           <thead>
               <tr class="table_header_bg" style="cursor: pointer;">
                @if($emergency_post == 1 &&($post_type==6||$post_type==7||$post_type==9))
                    <th width="2%"  ></th>
                @endif
                   <th width="35%" style="text-align: left;">Department Name</th>
                   <th width="30%" style="text-align: left;">Ledger Name</th>
                   <th width="15%" style="text-align: left;">Speciality</th>
                   <th width="10%" style="text-align: left;">Amount</th>
                   <?php if(isset($post_type)&& $post_type==6){ ?>
                   <th width="10%">TDS</th>
                   <?php } ?>
               </tr>
           </thead>
           <tbody>
               <?php
                     $mapped_status = '';
if (count($post_data) != 0) {
    $total = 0.0;
    foreach ($post_data as $each) {
        $mapped_class = '';
        if (strpos($each->ledger_name, 'Ledger not mapped') !== false) {
            $mapped_class = 'text-red';
            $mapped_status = 1;
        }

        $total += floatval($each->amount);
        ?>
               <tr class="<?= $mapped_class ?> posting_cls" data-ledger_name='{{ $each->ledger_name }}'>
                @if($emergency_post == 1 &&($post_type==6||$post_type==7||$post_type==9))
                    <td style="border-left: thin solid black !important;">
                         <input type="checkbox" onclick="getVoucherNo(this)">
                    </td>
                    @endif
                   <td class="common_td_rules"><?= $each->department_name ?></td>
                   <td class="common_td_rules" title="<?= $each->ledger_name ?>">


                    <span><?= $each->ledger_name ?></span>

                     @if (strpos($each->ledger_name, 'Ledger not mapped') !== false && $post_type == 1)
                     <button onclick="ledger_add_btn('{{$each->department_name}}');"  style="color: black;" class="ledger_add_btn pull-right"><i class="fa fa-plus"></i></button>
                     @endif
                   </td>
                   <td class="common_td_rules"><?= $each->speciality_name ?></td>
                   <td class="td_common_numeric_rules"><?= number_format($each->amount, 2) ?></td>
                   <?php if(isset($post_type)&& $post_type==6){ ?>
                   <td><input type="text" value="" name="tds[]" id="<?= $each->ledger_name ?>" class="form-control tds">
                   </td>
                   <?php } ?>
               </tr>
               <?php
}
    ?>
               <tr>
                   <th class="common_td_rules" colspan="3">Total</th>
                   <th class="td_common_numeric_rules"><?= number_format($total, 2) ?></th>

               </tr>
               <?php
            } else {
                $mapped_status = 1;
                ?>
               <tr class="common_td_rules">
                   <td colspan="3" style="text-align: center;"> No Result Found</td>
               </tr>
               <?php
            }
            ?>

           </tbody>
       </table>
   </div>
   <?php
   if (!$mapped_status) {
   ?>
   <div class="col-md-1 padding_sm pull-right">
       <button type="button" onclick="insertTallyData(1)" id="nextsequencedatabtn"
           class="btn btn-success btn-block">Next
           <i id="nextsequencedataspin" class="fa fa-forward"></i></button>
   </div>
   <?php
   }
   ?>

   <div class="col-md-1 padding_sm pull-right">
       <button type="button" onclick="processTallyData()" class="btn btn-warning btn-block">Cancel <i
               class="fa fa-times"></i></button>
   </div>

