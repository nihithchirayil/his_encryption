@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')
    <div class="right_col" role="main">
    <div class="row padding_sm">
    <div class="col-md-2 padding_sm pull-right">
        <?=$title?>
    </div>
    <input type="hidden" id="base_url" value="{{URL::to('/')}}">
    </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">

                    <div class="col-md-2 padding_sm">
                        <label for="">Date</label>
                        <input type="text" value="<?=$current_date?>" autocomplete="off" id="servieDate" class="form-control datepicker" placeholder="Service Date">
                    </div>
                    <div class="col-md-1 padding_sm" style="margin-top: 25px;">
                        <button type="button" onclick="processTallyData()" id="processdatabtn" class="btn btn-primary btn-block">Process <i id="processdataspin" class="fa fa-list"></i></button>
                    </div>
                    <div class="col-md-1 padding_sm" style="margin-top: 25px;">
                        <button type="button" disabled onclick="exceller()" id="generate_csvbtn" class="btn btn-warning btn-block">Excel <i id="generate_csvspin" class="fa fa-file-excel-o"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="x_panel">
        <div class="row padding_sm" style="min-height: 450px;">
            <div class="col-md-12 padding_sm" id="postdatadiv">

            </div>
        </div>
        </div>

</div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/exporttoxlsx.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/IncomeDetalis.js")}}"></script>
@endsection
