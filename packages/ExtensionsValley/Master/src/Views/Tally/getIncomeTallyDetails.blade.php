<div class="theadscroll" style="position: relative; height: 430px;">
    <table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">


                     <thead>
                         <tr class="table_header_bg" style="cursor: pointer;">
                             <th width="45%" style="text-align: left;">Department Name</th>
                             <th width="45%" style="text-align: left;">Ledger Name</th>
                             <th width="10%" style="text-align: left;">Amount</th>
                         </tr>
                     </thead>
                     <tbody>
                     <?php
                      $mapped_status = '';
 if (count($post_data) != 0) {
     $total = 0.0;
     foreach ($post_data as $each) {
         $mapped_class = '';
         if ($each->ledger_name == '*******Ledger not mapped********' || $each->ledger_name == '' || $each->ledger_name == NULL) {
             $mapped_class = 'text-red';
             $mapped_status = 1;
         }
         $total += floatval($each->amount);
         ?>
                         <tr class="<?=$mapped_class?>">
                             <td class="common_td_rules"><?=$each->department_name?></td>
                             <td class="common_td_rules"><?=$each->ledger_name?></td>
                             <td class="td_common_numeric_rules"><?=number_format($each->amount, 2)?></td>
                         </tr>
                         <?php
 }
     ?>
                     <tr>
                     <th class="common_td_rules" colspan="2">Total</th>
                     <th class="td_common_numeric_rules"><?=number_format($total, 2)?></th>

                     </tr>
                     <?php
             } else {
                 $mapped_status = 1;
                 ?>
                     <tr class="common_td_rules"><td colspan="3" style="text-align: center;"> No Result Found</td></tr>
                                 <?php
             }
             ?>

                     </tbody>
                 </table>
         </div>
         <?php
         if (!$mapped_status) {
             ?>
                 <div class="col-md-1 padding_sm pull-right">
                     <button type="button" onclick="insertTallyData()" id="nextsequencedatabtn" class="btn btn-success btn-block">Next <i id="nextsequencedataspin" class="fa fa-forward"></i></button>
                 </div>
                     <?php
         }
         ?>

         <div class="col-md-1 padding_sm pull-right">
             <button type="button" onclick="processTallyData()" class="btn btn-warning btn-block">Cancel <i class="fa fa-times"></i></button>
         </div>


