   <div class="theadscroll" style="position: relative; height: 430px;">
       <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
           <thead>
               <tr class="table_header_bg" style="cursor: pointer;">
                   <th width="10%" style="text-align: left;">Voucher Date</th>
                   <th width="15%" style="text-align: left;">Purticulars</th>
                   <th width="15%" style="text-align: left;">Voucher Type</th>
                   <th width="10%" style="text-align: left;">Voucher No</th>
                   <th width="30%" style="text-align: left;">Narration</th>
                   <th width="10%" style="text-align: left;">Credit</th>
                   <th width="10%" style="text-align: left;">Debit</th>
               </tr>
           </thead>
           <tbody>
               <?php
               function validateDate($date, $format = 'd-M-y')
                {
                    $d = DateTime::createFromFormat($format, $date);
                    return $d && $d->format($format) === $date;
                }
                     $mapped_status = '';
                if (count($post_data) != 0) {
                    foreach ($post_data as $each) {
                    $mapped_class = '';
                    if (strpos($each->ledger_name, 'Ledger not mapped') !== false) {
                        $mapped_class = 'text-red';
                        $mapped_status = 1;
                    }
                    $cr='NULL';
                    $dr='NULL';
                    if($each->credit_debit=='dr'){
                        $dr=$each->amount;
                    }else if($each->credit_debit=='cr'){
                        $cr=$each->amount;
                    }
                    $narration=$each->narration;
                    $newstring = substr(trim($narration), -10);
                    $string_check=validateDate(trim($newstring));
                    if($string_check){
                        $newnarrationstring = date('jS F Y', strtotime($newstring));
                        $narration= str_replace($newstring,' '.$newnarrationstring,$narration);
                    }
                    ?>
               <tr class="<?= $mapped_class ?>">
                   <td class="common_td_rules"><?= $each->purchase_bill_date ?></td>
                   <td class="common_td_rules"><?= $each->ledger_name ?></td>
                   <td class="common_td_rules"><?= $each->voucher_type ?></td>
                   <td class="common_td_rules"><?= $each->voucher_no ?></td>
                   <td class="common_td_rules"><?= $narration ?></td>
                   <td class="td_common_numeric_rules"><?= $cr ?></td>
                   <td class="td_common_numeric_rules"><?= $dr ?></td>
               </tr>
               <?php
                }
            } else {
                ?>
               <tr class="common_td_rules">
                   <td colspan="6" style="text-align: center;"> No Result Found</td>
               </tr>
               <?php
            }
            ?>

           </tbody>
       </table>
   </div>
   <?php if($mapped_status == 0){ ?>
   <div class="col-md-1 padding_sm pull-right">
       <button type="button" onclick="postDataToSoftware()" id="postDataToSwBtn" class="btn btn-success btn-block">Post
           S/W <i id="postDataToSwSpin" class="fa fa-calculator"></i></button>
   </div>
   <div class="col-md-2 padding_sm pull-right">
       <button type="button" onclick="postDataToTally()" id="postDataToTallyBtn" class="btn btn-warning btn-block">Post
           Tally <i id="postDataToTallySpin" class="fa fa-list"></i></button>
   </div>
   <?php } ?>
