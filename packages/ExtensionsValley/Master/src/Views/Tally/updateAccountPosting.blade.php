@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')
    <div class="right_col" role="main">
    <div class="row padding_sm">
    <div class="col-md-1 padding_sm pull-right">
        <?=$title?>
    </div>
    <input type="hidden" id="base_url" value="{{URL::to('/')}}">
    </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">
                    <div class="col-md-4 padding_sm">
                        <label> Post Type</label>
                        <select onchange="processTallyData()" class="form-control bill_tag" id="post_type">
                            <option value=""> Select Post Type</option>
                            <option value="1">Department Wise Collection</option>
                            <option value="2">Registration</option>
                            <option value="3">Discharge</option>
                            <option value="4">OT</option>
                            <option value="5">Pharmacy Sales</option>
                            <option value="6">General Store Purchase</option>
                            <option value="7">Pharmacy Store Purchase</option>
                            <option value="8">Sales Return</option>
                            <option value="9">IP Store Purchase</option>
                            <option value="10">Insurance Posting</option>
                            <option value="11">Pharmacy Sales (IP)</option>
                            <option value="12">Pharmacy Sales Return (IP)</option>
                            <option value="100">Debit Note</option>
                        </select>
                    </div>

                    <div class="col-md-2 padding_sm">
                        <label for="">Date</label>
                        <input type="text" value="<?=$current_date?>" autocomplete="off" id="servieDate" class="form-control datepicker" placeholder="Service Date">
                    </div>
                    <div class="col-md-1 padding_sm pull-right" style="margin-top: 25px;">
                        <button type="button" onclick="updateAccountPosting()" id="updateAccountPostingBtn" class="btn btn-primary btn-block">Update <i id="updateAccountPostingspin" class="fa fa-save"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="x_panel">
        <div class="row padding_sm" style="min-height: 450px;">
            <div class="col-md-12 padding_sm" id="postdatadiv">

            </div>
        </div>
        </div>

</div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/UpdateAccountPosting.js")}}"></script>
@endsection
