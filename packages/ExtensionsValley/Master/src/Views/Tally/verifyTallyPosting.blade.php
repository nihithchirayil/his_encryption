   <div class="theadscroll" style="position: relative; height: 430px;">
       <table style="width: 50%" id="result_data_table"
           class='table theadfix_wrapper no-margin table_sm table-striped no-border styled-table'
           style="font-size: 12px;">
           <thead>
               <tr class="headerclass"
                   style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                   <th colspan="2" width='70%'>Heads</th>
                   <th width='30%'>Amount</th>
               </tr>
           </thead>
           <tbody>
               <tr>
                   <th colspan="2" class="common_td_rules">Pharmacy</th>
                   <td class="td_common_numeric_rules"><?= $pharmacy ?></td>
               </tr>
               <tr>
                   <th colspan="2" class="common_td_rules">Consultation</th>
                   <td class="td_common_numeric_rules"><?= $consultation ?></td>
               </tr>
               <tr>
                   <th colspan="2" class="common_td_rules">Dept. Cash Collection</th>
                   <td class="td_common_numeric_rules"><?= $dept_cash_collection ?></td>
               </tr>
               <tr>
                   <th colspan="2" class="common_td_rules">Discharge</th>
                   <td class="td_common_numeric_rules"><?= $discharge ?></td>
               </tr>
               <tr>
                   <th colspan="2" class="common_td_rules">OT Bill</th>
                   <td class="td_common_numeric_rules"><?= $ot_bill ?></td>
               </tr>
               <tr>
                   <th colspan="2" class="common_td_rules">Not Updated OT Bill</th>
                   <td class="td_common_numeric_rules"><?= $not_updated_ot ?></td>
               </tr>
               <tr>
                   <th colspan="2" class="common_td_rules">Rest Of Bills</th>
                   <td class="td_common_numeric_rules"><?= $rest_of_bills ?></td>
               </tr>
               <tr>
                   <th colspan="2" class="common_td_rules">Dialysis Report</th>
                   <td class="td_common_numeric_rules"><?= $dialysis ?></td>
               </tr>
               <tr>
                   <th class="common_td_rules">Total</th>
                   <th class="td_common_numeric_rules"><?= $total_collection ?></th>
                   <th class="td_common_numeric_rules"><?= number_format($net_total, 2, '.', '') ?></th>
               </tr>
               <tr class="bg-blue">
                   <th colspan="2" class="common_td_rules">Difference</th>
                   <td class="td_common_numeric_rules">
                       <?= number_format(floatval($total_collection) - floatval($net_total), 2, '.', '') ?></td>
               </tr>
           </tbody>
       </table>
   </div>
