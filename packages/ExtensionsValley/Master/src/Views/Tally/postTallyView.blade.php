@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <div class="right_col" role="main">
        <div class="row padding_sm">
            <div class="col-md-1 padding_sm pull-right">
                <?= $title ?>
            </div>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="token" value="{{ csrf_token() }}">
            <input type="hidden" id="block_greater30days" value="{{ $block_greater30days }}">
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">
                    <div class="col-md-3 padding_sm" style="margin-top: 10px">
                        <div class="mate-input-box">
                            <label>Post Type</label>
                            <div class="clearfix"></div>
                            {!! Form::select('post_type', array(""=> "Select") + $accounts_posting_types->toArray(),'',['autocomplete'=>'off', 'class'=>'form-control select2 bill_tag','id' => 'post_type','onchange' => 'processTallyData()']) !!}
                            {{-- <select onchange="processTallyData()" class="form-control select2 bill_tag" id="post_type">
                                <option value=""> Select Post Type</option>
                                <option value="1">Department Wise Collection</option>
                                <option value="2">Registration</option>
                                <option value="3">Discharge</option>
                                <option value="4">OT</option>
                                <option value="5">Pharmacy Sales</option>
                                <option value="6">General Store Purchase</option>
                                <option value="7">Pharmacy Store Purchase</option>
                                <option value="8">Sales Return</option>
                                <option value="9">IP Store Purchase</option>
                                <option value="11">Pharmacy Sales (IP)</option>
                                <option value="12">Pharmacy Sales Return (IP)</option>
                                <option value="100">Debit Note</option>
                                <option value="102">Receivables</option>
                                <option value="101">Collection (Receivables)</option> --}}
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2 padding_sm" style="margin-top: 10px">
                        <div class="mate-input-box">
                            <label>Date</label>
                            <div class="clearfix"></div>
                            <input type="text" value="<?= $current_date ?>" autocomplete="off" id="servieDate" onblur="servieDate();"
                                class="form-control datepicker" placeholder="Service Date">
                        </div>
                    </div>

                <div class="hideshowbutton">
                    <div class="col-md-1 padding_sm" style="margin-top: 25px;">
                        <button type="button" onclick="verifyData()" id="verifydatabtn"
                            class="btn bg-purple btn-block">Verify <i id="verifydataspin"
                                class="fa fa-check-square-o"></i></button>
                    </div>
                    <div class="col-md-1 padding_sm" style="margin-top: 25px;">
                        <button type="button" onclick="processTallyData()" id="processdatabtn"
                            class="btn btn-primary btn-block">Process <i id="processdataspin"
                                class="fa fa-list"></i></button>
                    </div>
                    <div id="normaldataDiv" style="display: none;margin-top: 25px" class="col-md-2 padding_sm" style="margin-top: 25px;">
                        <button type="button" onclick="insertTallyData(1)" id="nextsequencedatabtn"
                            class="btn btn-info btn-block">Normal Format <i id="nextsequencedataspin"
                                class="fa fa-list"></i></button>
                    </div>
                    <div id="multimoddataDiv" style="display: none;margin-top: 25px" class="col-md-2 padding_sm" style="margin-top: 25px;">
                        <button type="button" onclick="insertTallyData(2)" id="insertTallyMultiDataBtn"
                            class="btn bg-teal-active btn-block">Multimod Format <i id="insertTallyMultiDataSpin"
                                class="fa fa-list"></i></button>
                    </div>
                    <div class="col-md-1 padding_sm" style="margin-top: 25px;">
                        <button type="button" disabled  onclick="exceller()" id="generate_csvbtn"
                            class="btn btn-warning btn-block">Excel <i id="generate_csvspin"
                                class="fa fa-file-excel-o"></i></button>
                    </div>
                </div>
                <div class="hideshowrequest">
                @if( $block_greater30days == 1)
                <div class="col-md-1 padding_sm" style="margin-top: 25px;">
                    <button class='btn btn-block light_purple_bg' type="button" onclick="requesttoedit()"><i class="fa fa-paper-plane requstbtn"></i>Request</button>
                </div>
                @endif
                </div>

                </div>
            </div>
        </div>
        <div class="x_panel">
            <div class="row padding_sm" style="min-height: 450px;">
                <div class="col-md-12 padding_sm" id="postdatadiv">

                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="ledger_map_add_modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 550px;width: 80%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;height: 56px;">
                    DEPARTMENT NAME:<h5  id="dept_head" style="margin-left: 132px;
                    margin-top: -17px;"></h5>
                <button type="button" class="close close_white" style="margin-top: -27px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;margin-top: -33px;">&times;</span></button>
                </div>
                <div class="modal-body" style="min-height:201px;">
                    <div class="row padding_sm">
                        <div class='col-md-12 padding_sm' id="ledgermapadd">
                            <div class="col-md-12 padding_sm" style="margin-top:-9px;">
                                <div class="checkbox checkbox-success inline no-margin">
                                    <input type="checkbox" class="form-control filters" name="checkbox"  id="checkbox" value="1">
                                    <label class="text-blue " for="checkbox">Contains Search</label>
                                </div>
                            </div>
                                    <div class="col-md-8 padding_sm" style="    margin-top: 10px;
                                    ">
                                <div class="mate-input-box">
                                <label for="">Ledger Name</label>
                                 <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search"
                                id="ledger_name" value="">
                            <input type="hidden" value="" id="ledger_name_hidden">
                            <div class="ajaxSearchBox" id="ledger_nameAjaxDiv"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 97%; z-index: 99999px;
                                       position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                            </div>
                            </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer" id="readyToSave">
                    <button type="button" onclick="ledgersave()" id="save_ledger" style="margin-top: 20px;margin-left: -55px;
                    width: 79px;" class="btn btn-success">Save <i
                            class="fa fa-save"></i></button>                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="getrequstapproveModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1000px;width: 30%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <span class="modal-title">Send Request</span>
                </div>
                <div class="modal-body" style="overflow: auto">
                    <input type="hidden" id="head_id" value="">
                    <input type="hidden" id="voucher_type" value="">
                    <input type="hidden" id="voucher_no" value="">
                    <div class="row padding_sm">
                        <div class='col-md-12 padding_sm'>
                            <table id="getrequstapproveData">
                                <div class="col-md-12 padding_sm"  id="">
                                    <div>
                                        <label for="">Remarks</label>
                                        <div class="clearfix"></div>
                                       <textarea class="form-control" name="remarks" id="remarks" style="resize: none;"></textarea>
                                    </div>
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" id="readyToSave">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" onclick="saverequesttoedit();" data-dismiss="modal">Request</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript_extra')

    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/PostTally.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js') }}"></script>
@endsection
