   <div class="theadscroll" style="position: relative; height: 430px;">
       <table  id="postTallyData" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
           <thead>
               <tr class="table_header_bg" style="cursor: pointer;">
                    <th colspan="8">File Not Generated for the Day</th>
               </tr>
               <tr class="table_header_bg" style="cursor: pointer;">
                   <th width="10%" style="text-align: left;">Voucher Number</th>
                   <th width="10%" style="text-align: left;">Date</th>
                   <th width="10%" style="text-align: left;">Particulars</th>
                   <th width="10%" style="text-align: left;">Group Name</th>
                   <th width="10%" style="text-align: left;">Voucher Type</th>
                   <th width="8%" style="text-align: left;">Amount</th>
                   <th width="7%" style="text-align: left;">Dr/Cr</th>
                   <th width="35%" style="text-align: left;">Narration</th>
               </tr>
           </thead>
           <tbody>
               <?php
                    $mapped_status = '';
                if (count($post_data) != 0) {
                    foreach ($post_data as $each) {
                    $mapped_class = '';
                    if ($each->ledger_name == '*******Ledger not mapped********' || $each->ledger_name == '' || $each->ledger_name == NULL) {
                        $mapped_class = 'text-red';
                        $mapped_status = 1;
                    }
                ?>
               <tr class="<?=$mapped_class?>">
                   <td class="common_td_rules"><?= $each->voucher_no ?></td>
                   <td class="common_td_rules"><?= date('d/m/Y', strtotime($servieDate)) ?></td>
                   <td class="common_td_rules"><?= $each->ledger_name ?></td>
                   <td class="common_td_rules"><?= $each->grp_name ?></td>
                   <td class="common_td_rules"><?= $each->voucher_type ?></td>
                   <td class="td_common_numeric_rules"><?= $each->amount ?></td>
                   <td class="common_td_rules"><?= ucfirst($each->credit_debit) ?></td>
                   <td class="common_td_rules"><?= $each->narration ?></td>

               </tr>
               <?php
                }
            } else { $mapped_status = 1;
                ?>
               <tr class="common_td_rules">
                   <td colspan="6" style="text-align: center;"> No Result Found</td>
               </tr>
               <?php
            }
            ?>

           </tbody>
       </table>
   </div>
       <?php if (!$mapped_status) { ?>
           <div class="col-md-1 padding_sm pull-right">
            <button type="button" onclick="postInsToSoftware()" id="postDataToSwBtn" class="btn btn-success btn-block">PostS/W 
                <i id="postDataToSwSpin" class="fa fa-calculator"></i></button>
            </div>
      <?php } ?>
