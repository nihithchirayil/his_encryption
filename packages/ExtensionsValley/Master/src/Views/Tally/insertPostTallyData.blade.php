   <div class="theadscroll" style="position: relative; height: 430px;">
       <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
           <thead>
               <tr class="table_header_bg" style="cursor: pointer;">
                <th width="2%"  ></th>
                   <th width="10%" style="text-align: left;">Vocher No</th>
                   <th width="5%" style="text-align: left;">Vocher Name</th>
                   <th width="20%" style="text-align: left;">Ledger Name</th>
                   <th width="10%" style="text-align: left;">Speciality</th>
                   <th width="3%" style="text-align: left;">CR/DR</th>
                   <th width="30%" style="text-align: left;">Narration</th>
                   <th width="10%" style="text-align: left;">Amount</th>
                   <?php if(isset($post_type) &&($post_type==7 || $post_type==6|| $post_type==100)){ ?>
                   <th width="7%" style="text-align: left;">Ref.Date</th>
                   <th width="5%" style="text-align: left;">Ref.No</th>
                   <th width="4%" style="text-align: left;">Delete</th>
                   <?php } ?>
               </tr>
           </thead>
           <tbody>
               <?php
               function validateDate($date, $format = 'd-M-y')
                {
                    $d = DateTime::createFromFormat($format, $date);
                    return $d && $d->format($format) === $date;
                }
                     $mapped_status = 0;
                if (count($post_data) != 0) {
                    foreach ($post_data as $each) {
                    $mapped_class = '';
                    if (strpos($each->ledger_name, 'Ledger not mapped') !== false) {
                        $mapped_class = 'text-red';
                        $mapped_status = 1;
                    }
                    $narration=$each->narration;
                    $newstring = substr(trim($narration), -10);
                    $string_check=validateDate(trim($newstring));
                    if($string_check){
                        $newnarrationstring = date('jS F Y', strtotime($newstring));
                        $narration= str_replace($newstring,' '.$newnarrationstring,$narration);
                    }
                        ?>
               <tr class="<?= $mapped_class ?> <?= preg_replace('/[^A-Za-z0-9\-]/', '',$each->voucher_no) ?>">
                    <td style="border-left: thin solid black !important;"> <input type="checkbox" onclick="color_row(this)"> </td>
                   <td class="common_td_rules"><?= $each->voucher_no ?></td>
                   <td class="common_td_rules"><?= $each->voucher_type ?></td>
                   <td class="common_td_rules" title="<?= $each->ledger_name ?>"><?= $each->ledger_name ?></td>
                   <td class="common_td_rules" title="<?= $each->speciality_name ?>"><?= $each->speciality_name ?></td>
                   <td class="common_td_rules"><?= $each->credit_debit ?></td>
                   <td class="common_td_rules"><?= $narration ?></td>
                   <td class="td_common_numeric_rules"><?= number_format($each->amount, 2) ?></td>
                   <?php if(isset($post_type) &&($post_type==7 || $post_type==6 || $post_type==100)){ ?>
                   <td class="common_td_rules"><?= $each->purchase_bill_date ?></td>
                   <td class="common_td_rules"><?= $each->bill_no ?></td>
                   <td class="common_td_rules deleteposting" style="cursor: pointer" onclick="deleteFromAcPosting('<?= $each->voucher_no ?>','<?= preg_replace('/[^A-Za-z0-9\-]/', '', $each->voucher_no) ?>')"><i class="fa fa-trash" style="color:red" ></i></td>
                   <?php } ?>
               </tr>
               <?php
                }
            } else {
                ?>
               <tr class="common_td_rules">
                   <td colspan="6" style="text-align: center;"> No Result Found</td>
               </tr>
               <?php
            }
            ?>

           </tbody>
       </table>
   </div>
   <?php if($mapped_status == 0){ ?>
   <div class="col-md-1 padding_sm pull-right">
       <button type="button" onclick="postDataToSoftware()" id="postDataToSwBtn" class="btn btn-success btn-block">Post
           S/W <i id="postDataToSwSpin" class="fa fa-calculator"></i></button>
   </div>
   <div class="col-md-2 padding_sm pull-right">
       <button type="button" onclick="postDataToTally()" id="postDataToTallyBtn" class="btn btn-warning btn-block">Post
           Tally <i id="postDataToTallySpin" class="fa fa-list"></i></button>
   </div>
   <?php } ?>
