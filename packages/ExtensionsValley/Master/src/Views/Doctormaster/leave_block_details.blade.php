    <div class="col-md-12 theadscroll" id="result_container_div" style="height: 401px">
       <div style="width:170%">
        <table id="result_data_table" class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="font-size: 12px;">
        <thead>
            <tr class="table_header_bg center_text"
                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th width='5%'>SL.No.</th>
                <th width='10%'>Description</th>
                <th width='10%'>Schedule Description</th>
                <th width='15%'>From Date</th>
                <th width='10%'>From Time</th>
                <th width='15%'>To Date</th>
                <th width='10%'>To Time</th>
                <th width='15%'>Shift</th>
                <th width='20%'>Created At</th>
                <th width='25%'>Created User</th>
            </tr>


        </thead>
              
                <tbody id="tbody_2">
                    @if (count($res) != 0)
                        @foreach ($res as $data)
                        
                        @php
                        $color=' ';
                        if($data->exception_type==1){
                            $color='#75d275';
                        }else if($data->exception_type==2){
                        $color='aqua';
                        }
                        @endphp
                            <tr>
                                <td class="td_common_numeric_rules" width="5%" style="background-color:{{$color}}">{{ $loop->iteration }}.</td>
                                <td class="common_td_rules" width="10%" title="{{ $data->exception_title }}">{{ $data->exception_title}}</td>
                                <td class="common_td_rules" width="10%">{{ $data->schedule_description}}</td>
                                <td class="common_td_rules" width="15%">{{  date('M-d-Y', strtotime($data->from_date))}}</td>
                                <td class="common_td_rules" width="10%">{{$data->fromtime}}</td>
                                <td class="common_td_rules" width="15%">{{  date('M-d-Y', strtotime($data->to_date))}}</td>
                                <td class="common_td_rules" width="10%">{{$data->totime}}</td>
                                <td class="common_td_rules" title="{{$data->shift_name}}" width="15%">{{ $data->shift_name}}</td>
                                <td class="common_td_rules" width="20%">{{  date('M-d-Y', strtotime($data->created_at))}}</td>
                                <td class="common_td_rules" width="25%">{{ $data->created_user}}</td>
                            </tr>
                        @endforeach
                        <div class="col-md-12">
                            <div class="col-md-8" style="height: 20px;margin-left:-19px;">
                                <strong style="display: flex;float: left;">Contain Search:</strong>
                                <input class="form-control" id="issue_search_box2" autocomplete="off"
                                type="text" placeholder="Search By Keyword.. "
                                style="color: #000;display:block;width: 76%;box-shadow: 1px 1px 1px green;"> 
                               </div>
                           </div>
                    @else
                        <tr>
                            <td colspan="5" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
    </div>
</div>
<script>

           $(document).ready(function() {
            $("#issue_search_box2").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#tbody_2 tr").filter(function() {
                    $(this).toggle($(this).text()
                    .toLowerCase().indexOf(value) > -1)
                });
            });
        });
       </script>