<script>
    $(document).ready(function() {          
        $(".page-link").click(function(e) {
            var url = $(this).attr("href");
            var doc = $('#doctor_hidden').val();
            e.preventDefault();

        $.ajax({
                type: "GET",
                url: url,
                data: {
                    doc: doc
                },
                beforeSend: function() {
                    $('#pricing_listing_container').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                },
                success: function(data) {
                    if (data) {
                        $('#pricing_listing_data').html(data);
                    }
                },
                complete: function() {
                    $('#pricing_listing_container').LoadingOverlay("hide");
                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                },
            });
            return false;
        });
    });
</script>
<style>
    .pagination {
    display: inline-block;
    padding-left: 0;
    margin: 1px 0 !important;
    border-radius: 4px;
}
</style>
<div class="col-md-12 theadscroll" style="padding:0px;height:240px">
    <table id="result_data_table"
        class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="font-size: 12px">
        <tr class="table_header_bg "
            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th width="3%">Sl.No.</th>
            <th width="38%">Service</th>
            <th width="15%">Pricing</th>
            <th width="15%">Company</th>
            <th width="15%">Amount</th>
            <th width="12%">Service Type</th>
            <th width="2%"><i class="fa fa-trash"></th>
        </tr>
        <tbody>
            @if(count($res)>0)

            @php
                $i = 1;
            @endphp
            @foreach ($res as $data)
                <tr>
                    <td class="td_common_numeric_rules"> {{ ($res->currentPage() - 1) * $res->perPage() + $loop->iteration }}</td>
                    <td class="common_td_rules">{{ $data->service_desc }}</td>
                    <td class="common_td_rules">{{ $data->pricing_name }}</td>
                    <td class="common_td_rules">{{ $data->company_name }}</td>
                    <td class="td_common_numeric_rules">{{ $data->amount }}</td>
                    <td class="common_td_rules">{{ $data->service_type }}</td>
                    <td class="common_td_rules" onclick="deletePricing({{ $data->id }})"><button id="delete_pricing_btn{{ $data->id }}" class="btn btn-danger"><i id="delete_pricing{{ $data->id }}" class="fa fa-trash delete_this"></i></button></td>

                </tr>
                @php
                    $i++;
                @endphp
            @endforeach
            @else
            <tr >
                <td colspan="5" style="text-align: center">No Records found</td>

            </tr>
            @endif
        </tbody>
    </table>
    <div class="col-md-12">
        <div class="clearfix"></div>
        <div class="col-md-12 text-right" style="margin-top: 20px;">
            <ul class="pagination purple_pagination" style="text-align:right !important; margin:-52px 4px !important">
                {!! $page_links !!}
            </ul>
        </div>
    </div>
</div>

