    <div class="col-md-12 theadscroll" id="result_container_div" style="padding: 0px;height:218px;">
       
           <div class="" style="width:171%">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
            style="font-size: 12px;">
            <thead>
               
                <tr class="table_header_bg center_text"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th width='5%'>SL.No.</th>
                    <th width='10%'>Appt.Date</th>
                    <th width='10%'>Slot</th>
                    <th width='10%'>Token</th>
                    <th width='15%'>UHID</th>
                    <th width='20%'>Patient Name</th>
                    <th width='20%'>Created Date</th>
                    <th width='20%'>Created user</th>

                </tr>


            </thead>
          
            <tbody id="table_body">
                @if (count($res) != 0)
                    @foreach ($res as $data)
                        <tr style="background-color: whitesmoke">
                            <td class="td_common_numeric_rules" width="5%">{{ $loop->iteration }}.</td>
                            <td class="common_td_rules" width="10%">{{  date('M-d-Y', strtotime($data->booking_date))}}</td>
                            <td class="td_common_numeric_rules" width="10%">{{ $data->time_slotdesc }}</td>
                            <td class="common_td_rules" width="10%">{{ $data->token_no}}</td>
                            <td class="common_td_rules" title="{{ $data->uhid}}" width="15%">{{ $data->uhid}}</td>
                            <td class="common_td_rules" title="{{ $data->salutationdesc}} {{ $data->patient_name}}" width="20%">{{ $data->salutationdesc}} {{ $data->patient_name}}</td>
                            <td class="common_td_rules" width="20%">{{  date('M-d-Y', strtotime($data->created_at))}}</td>
                            <td class="common_td_rules" width="20%">{{ $data->createduser}}</td>
                        </tr>

                       
                    @endforeach
                    <div class="col-md-12">
                        <div class="col-md-8" style="height: 20px;margin-left:-19px;">
                         <strong style="display: flex;float: left;">Contain Search:</strong>
                           <input class="form-control" id="issue_search_box1" autocomplete="off"
                           type="text" placeholder="Search By UHID/ Patient Name... "
                           style="color: #000;display:block;width: 74%;box-shadow: 1px 1px 1px green;"> 
                           </div>
                       </div>
                   
                @else
                    <tr>
                        <td colspan="6" style="text-align: center">
                            No Result Found
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
       
           </div>

    </div>
  
   <script>
       $(document).ready(function() {
        $("#issue_search_box1").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#table_body tr").filter(function() {
                $(this).toggle($(this).text()
                .toLowerCase().indexOf(value) > -1)
            });
        });
    });
   </script>