@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')

    <!-- Custom Theme Scripts -->
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">


    <!-- jQuery -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/jquery.min.js') }}">
    </script>

    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">



@endsection
@section('content-area')
    <style>
        .search_header {
            background: #36A693 !important;
            color: #FFF !important;
        }

        .box_header {
            background: #5397b1 !important;
            color: #FFF !important;
        }
    

        .mate-input-box {
            width: 100% !important;
            position: relative !important;
            padding: 15px 4px 4px 4px !important;
            border-bottom: 1px solid #01A881 !important;
            box-shadow: 0 0 3px #CCC !important;
        }

        .mate-input-box label {
            position: absolute !important;
            top: -2px !important;
            left: 6px !important;
            font-size: 12px !important;
            font-weight: 700 !important;
            color: #107a8c !important;
            padding-top: 2px !important;
        }

    </style>
    <!-- page content -->
    <div class="right_col">
        <div class="row" style="margin-bottom: 10px"> </div>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
        <input class="filters" value="" type="hidden" name="doctor_hidden" value="" id="doctor_hidden" />
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm" style="margin-top:-16px;">
                <h4 class="blue"><strong> <?= $title ?> </strong></h4>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-md-12" style="padding:0px">
                            <div class="col-md-2" style="height: 545px">
                                <div class="col-md-12 padding_sm">
                                    <div class="">
                                        <input class="form-control hidden_search" value="" autocomplete="off"
                                            style="width: 105%;margin-left: -14px;" type="text" id="doctor"
                                            placeholder="Please select a Doctor" name="doctor"/>
                                        <div id="doctorAjaxDiv" class="ajaxSearchBox"
                                            style="width: 100% !important; position: absolute; margin-top: -8px; display: block;margin-left: -14px;z-index:9999 !important">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-12 theadscroll"
                                    style="height:492px;margin-top: 6px;width: 190px; overflow:hidden;margin-left: -16px;">
                                    <ul style="margin: 0px;width: 188px;padding: 0px;height:492px">
                                        @foreach ($doctors as $doctor)
                                            @php $color=' ';@endphp
                                            @if ($doctor->status == 1)
                                                @php
                                                    $color = '#809f58';
                                                @endphp
                                            @else
                                                @php
                                                    $color = '#036603';
                                                @endphp
                                            @endif
                                            <li value="{{ $doctor->id }}" onclick="getShiftDetails({{ $doctor->id }})"
                                                id="doctor_name{{ $doctor->id }}"
                                                style="cursor: pointer;padding-left:12px;box-shadow: 0px 0px 1px 0px;list-style-type:none;color:{{ $color }}">{{ $doctor->name }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                                <div class="col-md-12" style="padding: 0px;margin-top: -8px;">
                                    <div class="col-md-6" style="background-color:#809f58 ">Active</div>
                                    <div class="col-md-6" style="background-color:#036603 ">Inactive</div>
                                </div>
                            </div>
                            <div class="col-md-10 padding_sm" style="height: 545px">
                                <div class="col-md-12 ">
                                    <div class="col-md-12" id="profile_nav" style="padding:0px">
                                        <ul id="progressbar" class="nav nav-tabs"
                                            style="cursor: pointer;font-size:14px;margin-left: -11px;padding:0px;width: 102.25%;"
                                            role="tablist">
                                            <li id="doctor_detail" style="display: inline !important;padding:-4px;">
                                                <a id="doctor_detail-tab" href="#doctor_detail_tab" role="tab"
                                                    aria-controls="doctor_detail" aria-selected="true"><strong>Doctor
                                                        Details</strong></a>
                                            </li>
                                            <li id="op_pricing" style="display: inline !important;padding:-4px;">
                                                <a id="op_pricing-tab" href="#op_pricing_tab" role="tab"
                                                    aria-controls="op_pricing" aria-selected="true"><strong>OP
                                                        Pricing</strong></a>
                                            </li>
                                            <li id="ip_pricing" style="display: inline !important;padding:-4px;">
                                                <a id="ip_pricing-tab" href="#ip_pricing_tab" role="tab"
                                                    aria-controls="ip_pricing" aria-selected="true"><strong>IP
                                                        Pricing</strong></a>
                                            </li>
                                            <li id="leave_block" style="display: inline !important;padding:-4px;">
                                                <a id="leave_block-tab" href="#leave_block_tab" role="tab"
                                                    aria-controls="leave_block" aria-selected="true"><strong>Doctor
                                                        Leave/Block</strong></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-12"
                                    style="height: 481px;padding:0px;margin-top:3px;box-shadow: 0px 0px 3px 0px greenyellow;"
                                    id="profile_content">
                                    <div class="tab-content">
                                        <div class="tab-pane fade " id="doctor_detail_tab" role="tabpanel" style=""
                                            aria-labelledby="nav-doctor_detail-tab">
                                            @include(
                                                'Master::Doctormaster.Doctormaster'
                                            );
                                        </div>
                                        <div class="tab-pane fade" id="op_pricing_tab" role="tabpanel" style=""
                                            aria-labelledby="nav-op_pricing-tab">
                                            @include(
                                                'Master::Doctormaster.doctor_op_pricing'
                                            );
                                        </div>
                                        <div class="tab-pane fade" id="ip_pricing_tab" role="tabpanel" style=""
                                            aria-labelledby="nav-ip_pricing-tab">
                                        </div>
                                        <div class="tab-pane" id="leave_block_tab" role="tabpanel" style=""
                                            aria-labelledby="nav-leave_block-tab">
                                            @include(
                                                'Master::Doctormaster.doctor_leave_block'
                                            );
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



            </div>



        </div>







    @stop
    @section('javascript_extra')

        <script
                src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
        </script>

        <script src="{{ asset('packages/extensionsvalley/master/default/javascript/doctordetailmaster.js') }}"></script>
        <script src="{{ asset('packages/extensionsvalley/master/default/javascript/doctorMaster.js') }}"></script>



    @endsection
