<style>
    #listing{
    text-align: center;
    padding: 87px;
    color:#6f727559;
    font-size: 36px;

    }
    #blocking{
    text-align: center;
    padding: 107px;
    color:#6f727559;
    font-size: 36px;

    }
</style>
<div class="col-md-12" style="padding: 0px;">
    <div class="col-md-6" style="box-shadow: 0px 0px 1px 0px;padding:0px;" id="shif_filters">
        <div class="col-md-12" style="margin-top: 5px;">
            <div class="col-md-12">
                <div class="col-md-6" style="background-color: #75d275">
                    <div class="radio radio-success inline no-margin ">
                        <input class="checkit" id="leave" type="radio" name="select_type" value=1 checked>
                        <label class="text-blue" for="leave">
                            Leave
                        </label>
                    </div>
                </div>
                <div class="col-md-6" style="background-color:aqua">
                    <div class="radio radio-success inline no-margin ">
                        <input class="checkit" id="block" type="radio" name="select_type" value=2>
                        <label class="text-blue" for="block">
                            Block
                        </label>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 5px;">
            <div class="col-md-6">
                <div class="mate-input-box">
                    <label>From Date</label>
                    <div class="clearfix"></div>
                    <input type="text" data-attr="date" autocomplete="off" name="from_date"
                        value="{{ date('M-d-Y') }}" class="form-control datepicker filters" placeholder="YYYY-MM-DD"
                        id="from_date">

                </div>
            </div>
            <div class="col-md-6">
                <div class="mate-input-box">
                    <label>To Date</label>
                    <div class="clearfix"></div>
                    <input type="text" data-attr="date" autocomplete="off" name="to_date" value="{{ date('M-d-Y') }}"
                        class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="to_date">

                </div>
            </div>

        </div>
        <div class="col-md-12" style="margin-top: 5px;">
            <div class="col-md-6">
                <div class="mate-input-box">
                    <label>Description</label>
                    <div class="clearfix"></div>
                    <input type="text" data-attr="date" autocomplete="off" name="desc" value=""
                        class="form-control filters" id="desc">

                </div>
            </div>
            <div class="col-md-6">
                <div class="mate-input-box">
                    <label>Select Shift</label>
                    <div class="clearfix"></div>
                    <select name="shift" id="shift" class="form-control select2">
                        <option value="">Select Shift</option>
                    </select>

                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 5px;" id="shift_box">
           
            <div class="col-md-8" style="margin-top: 5px;margin-left: -12px;" id="time_selection">
                <div class="col-md-6">
                    <div class="mate-input-box">
                        <label>From Time</label>
                        <div class="clearfix"></div>
                        <input type="text" data-attr="date" autocomplete="off" name="shift_frmtime" value=""
                            class="form-control timepicker" placeholder="" id="shift_frmtime">
        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mate-input-box">
                        <label>To Time</label>
                        <div class="clearfix"></div>
                        <input type="text" data-attr="date" autocomplete="off" name="shift_totime" value=""
                            class="form-control timepicker" placeholder="" id="shift_totime">
        
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="col-md-12" style="margin-top: 8px">
                    <div class="checkbox checkbox-success inline">
                        <input id="all_shift" type="checkbox" name="all_shift" >
                        <label class="text-blue" for="all_shift">
                            All Shift
                        </label>
                    </div>
                </div>
            </div>
          
        </div>
        <div class="col-md-12 " style="margin-top: 5px;">
            <div class="col-md-5" style="margin-left:-3px">
                <button type="button" class="btn btn-primary" id="search_app" onclick="viewSlotAndAppointment()"> <i
                        id="app_spin" class="fa fa-search"></i> View Slots And Appointments</button>
            </div>
            <div class="col-md-5" style="margine-left:-23px" >
                <a title="Apploitment list Screen" class="btn btn-primary" onclick="redirectToAppoinmentlist()"> <i
                        id="" class="fa fa-share"></i>Appointment List</a>
            </div>
            <div class="col-md-2" style="margine-left:26px">
                <button type="button" class="btn btn-success " id="shift_save"
                    onclick="saveDocLeaveOrBlock()"><i id="shift_spin" class="fa fa-save padding_sm"></i>Save</button>
            </div>
        </div>
       
    </div>
    <div class="col-md-6" style='height:480px;box-shadow: 0px 0px 1px 0px;float:right;padding:0px;'>
        <div class="col-md-12" style="margin-top: 10px;padding:1px">
            <div class="col-md-5">
                <div class="mate-input-box">
                    <label>From Date</label>
                    <div class="clearfix"></div>
                    <input type="text" data-attr="date" autocomplete="off" name="app_from_date"
                        value="{{ date('M-d-Y') }}" class="form-control datepicker filters" placeholder="YYYY-MM-DD"
                        id="app_from_date">

                </div>
            </div>
            <div class="col-md-5">
                <div class="mate-input-box">
                    <label>To Date</label>
                    <div class="clearfix"></div>
                    <input type="text" data-attr="date" autocomplete="off" name="app_to_date"
                        value="{{ date('M-d-Y') }}" class="form-control datepicker filters" placeholder="YYYY-MM-DD"
                        id="app_to_date">

                </div>
            </div>
            <div class="col-md-2">
                <button type="button" title="search " id="detail_search" class="btn btn-primary"
                    onclick="viewLeaveAndBlockDetails()"> <i id="detail_spin" class="fa fa-search"></i>
                    Search</button>
            </div>
        </div>
        <div class="col-md-12" style="padding: 0px;height:401px;">
            
            <div id="blocked_lists" style="padding: 0px;height:401px;">
                <h2 id="blocking">Leave and Block List</h2>
            </div>
        </div>
       

    </div>
    <div class="col-md-6" style='height:237px;box-shadow: 0px 0px 1px 0px;clear:left;padding:0px;'>
           
        <div id="doc_leave_container" class="" style='height:237px;'>
            <h2 id="listing">Slots And List</h2>

        </div>
        
          

    </div>
</div>
