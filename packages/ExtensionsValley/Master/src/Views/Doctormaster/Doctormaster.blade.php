  <div class="col-md-12 padding_sm">
      <div class=" no-border">
          <div class=" clearfix" style="height: 59px;margin-top: 7px;">
              <div class="col-md-12 padding_sm">
                  <input type="hidden" id="base_url" value="{{ URL::to('/') }}">

                  {{-- <div class="col-md-3 padding_sm" style="margin-top:10px;">
                      <div class="mate-input-box ">
                          <label for="">Doctor Name</label>
                          <div class="clearfix"></div>
                          <input type="text" class="form-control hidden_search" id="doctorName" autocomplete="off">
                          <input type="hidden" value="0" id="doctorName_hidden">
                          <div class="ajaxSearchBox" id="doctorNameAjaxDiv"
                              style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 90%; z-index: 99999;margin-top:13px !important;
                                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                          </div>

                      </div>
                  </div> --}}

                  {{-- <div class="col-md-4 padding_sm" style="margin-top:10px;">
                                <div class="mate-input-box">
                                    <label for="">Parent Menu</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control select2" id="parent_menu">
                                        <option value="">Select Parent Menu</option>

                                    </select>
                                </div>
                            </div> --}}

                  <div class="col-md-2 pull-right  padding_sm">
                      <label for="">&nbsp;</label>
                      <div class="clearfix"></div>
                      <!-- Button trigger modal -->
                      <button type="button" data-toggle="modal" onclick="resetmodal();" id="addDoctor"
                          style="margin-top: -11px;" class="btn btn-block btn-primary" data-target="#addDoctorDetails">
                          <i class="fa fa-plus"></i> Add Doctor</button>
                  </div>
                  {{-- <div class="col-md-2 pull-right  padding_sm">
                      <label for="">&nbsp;</label>
                      <div class="clearfix"></div>
                      <button type="button" onclick="doctorSearchList()" id="searchBtn"
                          class="btn btn-block btn-primary"><i id="searchSpin" class="fa fa-search"></i>
                          Search</button>
                  </div> --}}
                  <!-- Modal -->
                  <div class="modal fade" tabindex="-1" role="dialog" id="addDoctorDetails"
                      style="display: none; padding-right: 17px;">
                      <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content" style="width: 1242px;margin-left: -137px;margin-top: -28px;">
                              <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                                  <button type="button" class="close close_white" data-dismiss="modal"
                                      aria-label="Close"><span aria-hidden="true"
                                          style="font-size: 30px;">&times;</span></button>
                                  <h3 class="modal-title">ADD NEW DOCTOR</h3>
                              </div>


                              <div class="modal-body">
                                  <div class=" clearfix" style="min-height: 330px" id="doc_modal">
                                      <input type="hidden" id="edit_id">

                                      <div class='col-md-12 padding_sm'>
                                          <div class="col-md-12" style="padding:0px">
                                              <div class="col-md-6" style="padding: 0px">
                                                  <h5>BASIC DETAILS</h5>
                                                  <div class="col-md-12 padding_sm box-body clearfix"
                                                      style="margin-bottom: 20px; padding-bottom: 4px !important;">
                                                      <div style="margin-bottom: 20px">
                                                          <div class="col-md-12">
                                                              <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                  <div class="mate-input-box">
                                                                      <label for="">Doctor Name<span
                                                                              class="required"
                                                                              style="color:red;">*</span></label>
                                                                      <div class="clearfix"></div>
                                                                      <input type="text" autocomplete="off"
                                                                          class="form-control" id="docter_name"
                                                                          name="docter_name" value="" required>
                                                                  </div>

                                                              </div>
                                                              <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                  <div class="mate-input-box">
                                                                      <label for="">Phone Number<span
                                                                              class="required"
                                                                              style="color:red;">*</span></label>
                                                                      <div class="clearfix"></div>
                                                                      <input type="text" autocomplete="off"
                                                                          class="form-control" id="phone_number"
                                                                          name="phone_number" value="" required
                                                                          onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                                  </div>
                                                              </div>

                                                              <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                  <div class="mate-input-box">
                                                                      <label for="">Email</label>
                                                                      <div class="clearfix"></div>
                                                                      <input type="text" autocomplete="off"
                                                                          class="form-control" id="Email" name="Email"
                                                                          value="" required>

                                                                  </div>
                                                              </div>
                                                              <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                <div class="mate-input-box">
                                                                    <label for="">Gender</label>
                                                                    <div class="clearfix"></div>

                                                                    <select name="" class="form-control"
                                                                        id="gender">
                                                                        <option value="">Select Gender</option>
                                                                        <option value="1">Male</option>
                                                                        <option value="2">Female</option>
                                                                        <option value="3">Transgender</option>
                                                                    </select>


                                                                </div>
                                                            </div> 
                                                            
                                                          </div>
                                                          <div class="col-md-12">
                                                            <div class="col-md-6 padding_sm" style="margin-top:10px">
                                                                <div class="mate-input-box">
                                                                    <label for="">Address</label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="text" autocomplete="off"
                                                                        class="form-control" id="address"
                                                                        name="address" value="">
                                                                </div>
                                                            </div>
                                                            
                                                              <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                  <div class="mate-input-box">
                                                                      <label for="">Qualification</label>
                                                                      <input type="text" autocomplete="off"
                                                                          class="form-control" id="qualification"
                                                                          name="qualification" value="">

                                                                  </div>
                                                              </div>

                                                              <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                  <div class="mate-input-box">
                                                                      <label for="">Speciality<span
                                                                              class="required"
                                                                              style="color:red;">*</span></label>
                                                                      <div class="clearfix"></div>
                                                                      <select class="form-control " id="Speciality">

                                                                          <?php
                                                                    foreach ($speciality as $each) {
                                                                        ?>
                                                                          <option value="<?= $each->id ?>">
                                                                              <?= $each->name ?>
                                                                          </option>
                                                                          <?php
                                                                    }
                                                                ?>
                                                                      </select>

                                                                  </div>
                                                              </div>
                                                            
                                                          </div>

                                                          <div class="col-md-12">
                                                            <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                <div class="mate-input-box">
                                                                    <label for="">License No</label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="text" autocomplete="off"
                                                                        class="form-control" id="license_no"
                                                                        name="license_no" value=""
                                                                        onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                                                                </div>
                                                            </div>
                                                              <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                  <div class="mate-input-box padding_sm">
                                                                      <label for="">License Country</label>
                                                                      <div class="clearfix"></div>
                                                                      <select class="form-control "
                                                                          id="license_contry">
                                                                          <option value=""></option>
                                                                          <?php
                                                                    foreach ($contry as $each) {
                                                                        ?>
                                                                          <option value="<?= $each->id ?>">
                                                                              <?= $each->name ?></option>
                                                                          <?php
                                                                    }
                                                                ?>
                                                                      </select>

                                                                  </div>

                                                              </div>
                                                             
                                                              <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                <div class="mate-input-box">
                                                                    <label for="">License State</label>
                                                                    <select class="form-control " id="license_state">
                                                                        <option value=""></option>
                                                                        <?php
                                                                  foreach ($state as $each) {
                                                                      ?>
                                                                        <option value="<?= $each->id ?>">
                                                                            <?= $each->name ?></option>
                                                                        <?php
                                                                  }
                                                              ?>
                                                                    </select>


                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                <div class="mate-input-box">
                                                                    <label for="">Status</label>
                                                                    <div class="clearfix"></div>
                                                                    <select name="" class="form-control "
                                                                        id="status">
                                                                        <option value="1">Active</option>
                                                                        <option value="0">Not Active</option>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                             
                                                          </div>
                                                          <div class="col-md-12">
                                                        
                                                            <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                <div class="mate-input-box">
                                                                    <label for="">Slot Duration</label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="text" autocomplete="off"
                                                                        class="form-control" id="slot_duration"
                                                                        name="slot_duration" value="">

                                                                </div>
                                                            </div>
                                                              <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                  <div class="mate-input-box">
                                                                      <label for="">Token Prefix</label>
                                                                      <div class="clearfix"></div>
                                                                      <input type="text" autocomplete="off"
                                                                          class="form-control" id="token_prefix"
                                                                          name="token_prefix" value="">

                                                                  </div>
                                                              </div>
                                                              <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                                  <div class="mate-input-box">
                                                                      <label for="">Consultation Duration</label>
                                                                      <div class="clearfix"></div>
                                                                      <input type="text" autocomplete="off"
                                                                          class="form-control "
                                                                          id="consultancy_duration"
                                                                          name="consultancy_duration" value="">

                                                                  </div>
                                                              </div>

                                                             
                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-md-6" style="padding: 1px;">
                                                  <h5>ROOM DETAILS</h5>
                                                  <div class="col-md-12 padding_sm box-body clearfix"
                                                      style="margin-bottom: 4px;padding-bottom: 20px !important;">

                                                      <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                          <div class="mate-input-box">
                                                              <label for="">Op Room No</label>
                                                              <div class="clearfix"></div>
                                                              <input type="text" autocomplete="off"
                                                                  class="form-control" id="opRoomNo" name="opRoomNo"
                                                                  value=""
                                                                  onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                                                          </div>
                                                      </div>
                                                      <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                          <div class="mate-input-box">
                                                              <label for="">Screening Room No</label>
                                                              <div class="clearfix"></div>
                                                              <input type="text" autocomplete="off"
                                                                  class="form-control" id="screeningRoomNO"
                                                                  name="screeningRoomNO" value=""
                                                                  onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                                                          </div>
                                                      </div>
                                                      <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                          <div class="">
                                                              <label for=""></label>
                                                              <div class="clearfix"></div>
                                                              <input type="checkbox" name="" id="has_op_right"
                                                                  class=""
                                                                  style="padding:20px"><Label><strong
                                                                      style="padding: 10px">Has Op
                                                                      Rights</strong></Label>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                          <div class="">
                                                              <label for=""></label>
                                                              <div class="clearfix"></div>
                                                              <input type="checkbox" name="" id="has_ip_rights"
                                                                  class=""
                                                                  style="padding:20px"><Label><strong
                                                                      style="padding: 10px">Has Ip
                                                                      Rights</strong></Label>
                                                          </div>
                                                      </div>

                                                  </div>
                                                  <div class="clearfix"></div>
                                                  <h5>FOLLOWUP RULE</h5>


                                                  <div class="col-md-12 padding_sm box-body clearfix"
                                                      style="margin-bottom: 20px;">
                                                      <div class="col-md-12">
                                                          <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                              <div class="mate-input-box">
                                                                  <label for="">Free Followup Days</label>
                                                                  <div class="clearfix"></div>
                                                                  <input type="text" autocomplete="off"
                                                                      class="form-control" id="free_followup_day"
                                                                      name="free_followup_day" value=""
                                                                      onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                                                              </div>
                                                          </div>
                                                          <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                              <div class="mate-input-box">
                                                                  <label for="">Free Followup No</label>
                                                                  <div class="clearfix"></div>
                                                                  <input type="text" autocomplete="off"
                                                                      class="form-control" id="free_followup_no"
                                                                      name="free_followup_no" value=""
                                                                      onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                                                              </div>
                                                          </div>
                                                          <div class="col-md-6 padding_sm" style="margin-top:10px">
                                                            <div class="mate-input-box">
                                                                <label for="">Same Speciality Followup Days</label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" autocomplete="off"
                                                                    class="form-control"
                                                                    id="same_specl_followup_days"
                                                                    name="same_specl_followup_days" value=""
                                                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                                                            </div>
                                                        </div>
                                                      </div>

                                                      <div class="col-md-12">
                                                        
                                                          <div class="col-md-6 padding_sm" style="margin-top:10px">
                                                              <div class="mate-input-box">
                                                                  <label for="">Same Speciality Follow up No</label>
                                                                  <div class="clearfix"></div>
                                                                  <input type="text" autocomplete="off"
                                                                      class="form-control"
                                                                      id="same_specl_followup_no"
                                                                      name="same_specl_followup_no" value=""
                                                                      onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                                                              </div>
                                                          </div>
                                                          <div class="col-md-6 padding_sm" style="margin-top:10px">
                                                            <div class="">
                                                                <label for=""></label>
                                                                <div class="clearfix"></div>
                                                                <input type="checkbox" name="" id="free_followup_for_same"
                                                                    class=""
                                                                    style="padding:20px"><Label><strong
                                                                        style="padding: 10px">Free Follow For
                                                                        Speciality</strong></Label>
                                                            </div>
                                                        </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>

                                          <h5>USER INFORMATIONS</h5>
                                          <div class="col-md-12 padding_sm box-body ">
                                            <div class="col-md-2 padding_sm" style="margin-top:10px">
                                                <div class="mate-input-box">
                                                    <label for="">User Name</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control"
                                                        id="username" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="col-md-2 padding_sm" style="margin-top:10px">
                                                <div class="mate-input-box">
                                                    <label for="">Password</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control"
                                                        id="password" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="col-md-2 padding_sm" style="margin-top:10px">
                                                <div class="mate-input-box">
                                                    <label for="">Default Landing Page</label>
                                                    <div class="clearfix"></div>
                                                    {!! Form::select('default_landing_page', $menu_list, null, [
                                                        'class' => 'form-control', 'id' => 'default_landing_page',
                                                        'placeholder' => 'Select Landing Page',
                                                    ]) !!}
                                                </div>
                                            </div>
                                          </div>

                                          <h5>CONSULTATION SERVICES</h5>
                                          <div class="col-md-12 padding_sm box-body " !important;">
                                              <div class="col-md-2 padding_sm" style="margin-top:10px">

                                                  <div class="mate-input-box">
                                                      <label for="">First Consultation Item</label>
                                                      <div class="clearfix"></div>
                                                      <input type="text" class="form-control hidden_search"
                                                          id="consultationItem" autocomplete="off">
                                                      <input type="hidden" value="0" id="consultationItem_hidden">
                                                      <div class="ajaxSearchBox" id="consultationItemAjaxDiv"
                                                          style="text-align: left; list-style: none;  cursor: pointer; max-height: 81px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 90%; z-index: 99999 !important;
                                                          position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3);">
                                                      </div>

                                                  </div>
                                              </div>
                                              <div class="col-md-2 padding_sm" style="margin-top:10px">
                                                  <div class="mate-input-box">
                                                      <label for="">Subsequent Consultation Item</label>
                                                      <div class="clearfix"></div>
                                                      <input type="text" class="form-control hidden_search"
                                                          id="subconsultationItem" autocomplete="off">
                                                      <input type="hidden" value="0" id="subconsultationItem_hidden">
                                                      <div class="ajaxSearchBox" id="subconsultationItemAjaxDiv"
                                                          style="text-align: left; list-style: none;  cursor: pointer; max-height: 81px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 90%; z-index: 99999 !important;
                                                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3);">
                                                      </div>

                                                  </div>
                                              </div>


                                              <div class="col-md-2 padding_sm" style="margin-top:10px">
                                                  <div class="mate-input-box">
                                                      <label for="">Default Op Pharmacy</label>
                                                      <div class="clearfix"></div>
                                                      <input type="text" autocomplete="off" class="form-control"
                                                          id="defualt_op_phar" name="defualt_op_phar" value=""
                                                          onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                  </div>
                                              </div>

                                              <div class="col-md-2 padding_sm" style="margin-top:10px">
                                                  <div class="mate-input-box">
                                                      <label for="">Prescription Department Head</label>
                                                      <div class="clearfix"></div>
                                                      <input type="text" autocomplete="off" class="form-control"
                                                          id="prescription_dep" name="prescription_dep" value="">

                                                  </div>
                                              </div>
                                              <div class="col-md-2 padding_sm" style="margin-top:10px">
                                                  <div class="mate-input-box">
                                                      <label for="">Medication Note </label>
                                                      <div class="clearfix"></div>
                                                      <input type="text" autocomplete="off" class="form-control"
                                                          id="medication_note" name="medication_note" value="">

                                                  </div>
                                              </div>
                                              <div class="col-md-2 padding_sm" style="margin-top:10px">
                                                  <div class="mate-input-box">
                                                      <label for="">Medication Footer Text</label>
                                                      <div class="clearfix"></div>
                                                      <input type="text" autocomplete="off" class="form-control"
                                                          id="medication_foot" name="medication_foot" value="">

                                                  </div>
                                              </div>

                                              <!-- <div class="col-md-2 padding_sm" style="margin-top:10px">
                                                  <div class="mate-input-box">
                                                      <label for="">Default Landing Page</label>
                                                      <div class="clearfix"></div>
                                                      <input type="text" autocomplete="off" class="form-control"
                                                          id="default_landing" name="default_landing" value="">

                                                  </div>
                                              </div> -->
                                              <div class="col-md-10">
                                                  <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                      <div class="">
                                                          <label for=""></label>
                                                          <div class="clearfix"></div>

                                                          <input type="checkbox" name="" id="appointments_only"
                                                              class=""
                                                              style="padding:20px"><Label><strong
                                                                  style="padding: 10px">Appointments Use
                                                                  Only</strong></Label>


                                                      </div>
                                                  </div>
                                                  <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                      <div class="">
                                                          <label for=""></label>
                                                          <div class="clearfix"></div>
                                                          <input type="checkbox" name="" id="hide_appointments"
                                                              class=""
                                                              style="padding:20px"><Label><strong
                                                                  style="padding: 10px">Hide From
                                                                  Appointments</strong></Label>


                                                      </div>
                                                  </div>
                                                    <div class="col-md-1 padding_sm" style="margin-top:10px">
                                                      <div class="">
                                                          <label for=""></label>
                                                          <div class="clearfix"></div>
                                                          <input type="checkbox" name="" id="is_er"
                                                              class=""
                                                              style="padding:20px"><Label><strong
                                                                  style="padding: 10px">Is ER</strong></Label>

                                                      </div>
                                                  </div>
                                                  <div class="col-md-1 padding_sm" style="margin-top:10px">
                                                    <div class="">
                                                        <label for=""></label>
                                                        <div class="clearfix"></div>
                                                        <input type="checkbox" name="" id="is_mlc"
                                                            class=""
                                                            style="padding:20px"><Label><strong
                                                                style="padding: 10px !important">Is
                                                                Mlc</strong></Label>

                                                    </div>
                                                </div>
                                                <div class="col-md-3 padding_sm" style="margin-top:10px">
                                                      <div class="">
                                                          <label for=""></label>
                                                          <div class="clearfix"></div>
                                                          <input type="checkbox" name="" id="show_queue_management"
                                                              class=""
                                                              style="padding:20px"><Label><strong
                                                                  style="padding: 10px">Enable Queue Management</strong></Label>

                                                      </div>
                                                  </div>
                                                  {{-- <div class="col-md-6 padding_sm" style="margin-top:10px"  >
                                                    <div class="">
                                                        <label for=""></label>
                                                        <div class="clearfix"></div>
                                                        <input type="checkbox" name="" id="emergency_status" class="" style="padding:20px"><Label><strong style="padding: 10px">Emergency Status</strong></Label>
                                                       
                                                    </div>
                                                </div> --}}
                                              </div>

                                          </div>




                                      </div>
                                      <div class="col-md-12 padding_sm" style="margin-top:10px;">
                                          <div class="col-md-2 padding_sm pull-right">
                                              <label for="">&nbsp;</label>
                                              <button type="button" id="savebtnBtn" class="btn btn-block btn-success"
                                                  onclick="saveItem();"><i id="savebtnSpin" class="fa fa-save"></i>
                                                  Save
                                              </button>
                                          </div>
                                          <div class="col-md-2 padding_sm pull-right">
                                              <label for="">&nbsp;</label>
                                              <div class="clearfix"></div>
                                              <button type="button" class="btn btn-block btn-info clearFormButton"
                                                  onclick="resetmodal();"><i class="fa fa-times"></i> Clear
                                              </button>
                                          </div>
                                      </div>
                                  </div>
                              </div>


                          </div>
                      </div>
                  </div>


              </div>
          </div>
      </div>
      <div class="col-md-12 padding_sm">
          <div class="box no-border">
              <div class="box-body clearfix">
                  <div id="searchDataDiv" style="min-height:403px;">
                      <div class="col-md-12" style="margin-top: 145px;margin-left: 283px;">
                          <h1 style="color: lightblue">PLEASE SELECT A DOCTOR</h1>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>







  <script
    src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
  </script>
