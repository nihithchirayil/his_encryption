<style>
    #pr_listing {
        text-align: center;
        padding: 100px;
        color: #6f727559;
        font-size: 36px;

    }

</style>
<div class="col-md-12" style="padding: 0px;">
    <div class="col-md-12">
        <div class="col-md-6">
            <h4 class="blue" style="margin: 3px;"> Add Pricing</h4>
        </div>
    </div>
    <div class="col-md-12" style="box-shadow: 0px 0px 3px 0px;padding: 0px;">
        <div class="col-md-4" style="box-shadow: 0px 0px 1px 0px;height: 140px;">
            <div class="col-md-12" style="margin-top: 2px;">
                <div class="mate-input-box">
                    <label>Company</label>
                    <div class="clearfix"></div>
                    <select name="company" id="company" class="form-control select2">
                        <option value="">Select Company</option>
                        @foreach ($company_detail as $data)
                            <option value="{{ $data->id }}">{{ $data->company_name }}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="col-md-12" style="margin-top: 2px;" id="pricing_box">
                <div class="mate-input-box">
                    <label>Pricing</label>
                    <div class="clearfix"></div>
                    <select name="pricing" id="pricing" class="form-control select2">
                        <option value="">Select Price</option>
                    </select>

                </div>
            </div>
            <div class="col-md-12" style="margin-top:-10px;padding:0px">
                <div class="col-md-6" style="margin-top: 8px">
                    <div class="checkbox checkbox-success inline">
                        <input id="checkbox" type="checkbox" name="checkbox" checked>
                        <label class="text-blue" for="checkbox">
                            Active
                        </label>
                    </div>
                </div>
                <div class="col-md-6 pull-right" style="margin-top: 6px">
                    <button type="button" class="btn btn-success btn-block" id="save_pricing" onclick="savePricing()"><i
                            id="pricing_spin" class="fa fa-save padding_sm"></i> Save Pricing</button>
                </div>
            </div>

        </div>
        <div class="col-md-8 theadscroll" style="box-shadow: 0px 0px 1px 0px;height: 126px;padding:0px;" id="set_pricing_box">
            <table id="result_data_table"
                class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                style="font-size: 12px;">
                <tr class="table_header_bg "
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th width="40%">Service Name</th>
                    <th width="15%">Reg.</th>
                    <th width="15%">Renewal</th>
                    <th width="15%">MLC Reg.</th>
                    <th width="15%">New Born</th>
                </tr>
                <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach ($services as $data)
                        <tr>
                            <td class="">{{ $data->service_desc }}</td>
                            <td class="" style="display: none">{{ $data->service_id }}</td>
                            <td class="" style="display: none">{{ $data->service_code }}</td>
                            <td class="" style="display: none">{{ $data->id }}</td>
                            <td><input type="text" name="" id="{{ $data->service_code }}REG"
                                    data-service_code='{{ $data->service_code }}' data-service_type='REG'
                                    class="form-control inp_reset"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>
                            <td><input type="text" name="" id="{{ $data->service_code }}mlcreg" data-service_code='{{ $data->service_code }}'
                                    data-service_type='mlcreg' class="form-control inp_reset"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>
                            <td><input type="text" name="" id="{{ $data->service_code }}newborn" data-service_code='{{ $data->service_code }}'
                                    data-service_type='newborn' class="form-control inp_reset"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>
                            <td><input type="text" name="" id="{{ $data->service_code }}REN" data-service_code='{{ $data->service_code }}'
                                    data-service_type='REN' class="form-control inp_reset"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>
                        </tr>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
    <div class="col-md-12" style="height:49px;padding: 2px;margin: 7px 0px 7px 0px;padding: -1px;border: 1px solid darkgray;">
        <div class="col-md-2" style="color: white;margin: -2px -26px 0px -21px;width: 132px;border: 1px solid;">
            <div class="col-md-12" style="margin-top: 1px"><button class="btn btn-primary btn-block" title="Search" id="pricing_search"><i id="pricing_search_spin"class="fa fa-search padding_sm" style="font-size: 10px"></i>Search</button>
            </div>
            <div class="col-md-12" style="margin-top: -4px"><button class="btn btn-default btn-block"  title="Refresh" id="pricing_refresh"><i id=""class="fa fa-refresh padding_sm" style="font-size: 10px"></i>Reset</button>
            </div>
        </div>
        <div class="col-md-3">
            <div class="mate-input-box">
                <label>Service</label>
                <div class="clearfix"></div>
                <select name="ser" id="ser" class="form-control select2 ser">
                    <option value="">Select</option>
                    @foreach ($services as $data)
                        <option value="{{ $data->service_code }}">{{ $data->service_desc }}</option>
                    @endforeach
                </select>

            </div>
        </div>
        <div class="col-md-3" style="margin-left: -14px;">
            <div class="mate-input-box">
                <label>Company</label>
                <div class="clearfix"></div>
                <select name="cmp" id="cmp" class="form-control select2 ser">
                    <option value="">Select</option>
                    @foreach ($company_detail as $data)
                        <option value="{{ $data->id }}">{{ $data->company_name }}</option>
                    @endforeach

                </select>

            </div>
        </div>
        <div class="col-md-3" style="margin-left: -12px;">
            <div class="mate-input-box">
                <label>Pricing</label>
                <div class="clearfix"></div>
                <select name="prc" id="prc" class="form-control select2 ser">
                    <option value="">Select</option>
                    @foreach ($pricing as $data)
                        <option value="{{ $data->id }}">{{ $data->pricing_name }}</option>
                    @endforeach
                </select>

            </div>
        </div>
        <div class="col-md-2">
            <div class="mate-input-box">
                <label>Service Type</label>
                <div class="clearfix"></div>
                <select name="typ" id="typ" class="form-control select2 ser">
                    <option value="">Select</option>
                    @foreach ($service_type as $data)
                        <option value="{{ $data->name }}">{{ $data->name }}</option>
                    @endforeach
                </select>

            </div>
        </div>
    </div>
    <div class="col-md-12 " style="height: 226px;padding:0px;"
        id="pricing_listing_container">
        <div class="box no-border">
            <div class="box-body clearfix">
                <div id="pricing_listing_data">
                    <h2 id="pr_listing">Pricing List</h2>

                </div>
            </div>
        </div>
    </div>
</div>
