<div class="theadscroll" style="position: relative; height:303;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>

        </thead>
        <tbody>
            @if (sizeof($searchMenu) > 0)
                @foreach ($searchMenu as $item)
                @php
                     $check = ' ';
                    $show_queue_management = 0;
                    if(isset($item->show_queue_management)) {
                        $show_queue_management = $item->show_queue_management;
                    }
                @endphp
                    @if ($item->status == 1)
                        @php
                            $check = 'checked';
                            
                        @endphp
                    @endif
                    <tr style="cursor:pointer;" data-id="{{ $item->id }}" data-name="{{ $item->doctor_name }}"
                        data-phone="{{ $item->phone }}" data-email="{{ $item->email }}"
                        data-qualification="{{ $item->qualification }}" data-address="{{ $item->address }}"
                        data-speciality="{{ $item->sid }}" data-license_country="{{ $item->license_country }}"
                        data-license_state="{{ $item->license_state }}" data-license_no="{{ $item->license_no }}"
                        data-has_op_rights="{{ $item->has_op_rights }}"
                        data-has_ip_rights="{{ $item->has_ip_rights }}" data-op_room_no="{{ $item->op_room_no }}"
                        data-screening_room_no="{{ $item->screening_room_no }}"
                        data-first_consultation_item="{{ $item->first_consultation_item }}"
                        data-subsequent_consultation_item="{{ $item->subsequent_consultation_item }}"
                        data-free_followup_days="{{ $item->free_followup_days }}"
                        data-free_followup_no="{{ $item->free_followup_no }}"
                        data-free_followup_for_same_speciality="{{ $item->free_followup_for_same_speciality }}"
                        data-same_speciality_followup_days="{{ $item->same_speciality_followup_days }}"
                        data-same_speciality_followup_no="{{ $item->same_speciality_followup_no }}"
                        data-consultation_duration="{{ $item->consultation_duration }}"
                        data-status="{{ $item->status }}" data-gender="{{ $item->gender }}"
                        data-user_id="{{ $item->user_id }}"
                        data-default_op_pharmacy="{{ $item->default_op_pharmacy }}"
                        data-is_mlc="{{ $item->is_mlc }}" data-is_casualty="{{ $item->is_casualty }}"
                        data-prescription_department_head="{{ $item->prescription_department_head }}"
                        data-medication_note="{{ $item->medication_note }}"
                        data-medication_footer_text="{{ $item->medication_footer_text }}"
                        data-slot_duration="{{ $item->slot_duration }}"
                        data-token_prefix="{{ $item->token_prefix }}"
                        data-hide_from_appointments="{{ $item->hide_from_appointments }}"
                        data-is_er="{{ $item->is_er }}"
                        data-show_queue_management="{{ $show_queue_management }}"
                        data-username="{{ $item->username }}"
                        data-default_landing="{{ $item->default_landing }}"
                        data-default_landing_page="{{ $item->default_landing_page }}">

                        <td>
                            <div class="container">
                                <div class="main-body" style="height: 400px">
                                    <div class="col-md-12">
                                        <div class="col-md-2 pull-right">
                                            <div class="checkbox checkbox-success inline">
                                                <input id="active_doc" type="checkbox" onclick="activateDoctor({{$item->id}})" name="checkbox" {{$check}}>
                                                <label class="text-blue" for="active_doc">
                                                    Active
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row gutters-sm">
                                        <div class="col-md-4 mb-3">
                                            <div class="card">
                                                <div class="card-body" style="height: 347px">
                                                    <div class="d-flex flex-column align-items-center text-center">
                                                        <img src="{{ URL::to('/') }}/packages/extensionsvalley/dashboard/images/profile/user.png"
                                                            alt="Admin" class="rounded-circle" width="150">
                                                        <div class="mt-3">
                                                            <h4>{{ $item->doctor_name }}</h4>
                                                            <p class="text-muted font-size-sm">
                                                                {{ $item->speciality_name }}
                                                            </p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-8">
                                            <div class="card mb-3">
                                                <div class="card-body" style="height: 347px">
                                                    <div class="row"
                                                        style="border-bottom: 1px solid floralwhite;">
                                                        <div class="col-sm-3">
                                                            <h6 class="mb-0 head_text">Full Name</h6>
                                                        </div>
                                                        <div class="col-sm-9 text-left">
                                                            {{ $item->doctor_name }}
                                                        </div>
                                                    </div>

                                                    <div class="row"
                                                        style="border-bottom: 1px solid floralwhite;">
                                                        <div class="col-sm-3">
                                                            <h6 class="mb-0 head_text">Email</h6>
                                                        </div>
                                                        <div class="col-sm-9 text-left">
                                                            {{ $item->email }}
                                                        </div>
                                                    </div>

                                                    <div class="row"
                                                        style="border-bottom: 1px solid floralwhite;">
                                                        <div class="col-sm-3">
                                                            <h6 class="mb-0 head_text">Phone</h6>
                                                        </div>
                                                        <div class="col-sm-9 text-left">
                                                            {{ $item->phone }}
                                                        </div>
                                                    </div>

                                                    <div class="row"
                                                        style="border-bottom: 1px solid floralwhite;">
                                                        <div class="col-sm-3">
                                                            <h6 class="mb-0 head_text">Qualification</h6>
                                                        </div>
                                                        <div class="col-sm-9 text-left">
                                                            {{ $item->qualification }}
                                                        </div>
                                                    </div>

                                                    <div class="row"
                                                        style="border-bottom: 1px solid floralwhite;">
                                                        <div class="col-sm-3">
                                                            <h6 class="mb-0 head_text">Address</h6>
                                                        </div>
                                                        <div class="col-sm-9 text-left">
                                                            {{ $item->address }}
                                                        </div>
                                                    </div>
                                                    <div class="row"
                                                        style="border-bottom: 1px solid floralwhite;">
                                                        <div class="col-sm-3">
                                                            <h6 class="mb-0 head_text">Speciality</h6>
                                                        </div>
                                                        <div class="col-sm-9 text-left">
                                                            {{ $item->speciality_name }}
                                                        </div>
                                                    </div>
                                                    <div class="row"
                                                        style="border-bottom: 1px solid floralwhite;">
                                                        <div class="col-sm-3">
                                                            <h6 class="mb-0 head_text">Token Prefix</h6>
                                                        </div>
                                                        <div class="col-sm-9 text-left">
                                                            {{ $item->token_prefix }}
                                                        </div>
                                                    </div>
                                                    <div class="row"
                                                        style="border-bottom: 1px solid floralwhite;">
                                                        <div class="col-sm-3">
                                                            <h6 class="mb-0 head_text">License Country</h6>
                                                        </div>
                                                        <div class="col-sm-9 text-left">
                                                            {{ $item->license_country }}
                                                        </div>
                                                    </div>
                                                    <div class="row"
                                                        style="border-bottom: 1px solid floralwhite;">
                                                        <div class="col-sm-3">
                                                            <h6 class="mb-0 head_text">License No.</h6>
                                                        </div>
                                                        <div class="col-sm-9 text-left">
                                                            {{ $item->license_no }}
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-top:5px;">
                                                        <div class="col-sm-12">

                                                            <div class="col-md-2 pull-right">
                                                                <button type="button"
                                                                    class="btn btn-sm btn-warning pull-right"
                                                                    title="edit" data-toggle="modal"
                                                                    data-target="#addDoctorDetails"
                                                                    onclick="editItem(this);"><i
                                                                        id="editItem_{{ $item->id }}"
                                                                        class="fa fa-edit editButton padding_sm"
                                                                        title="Edit"></i>Edit
                                                                </button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>





                                        </div>
                                    </div>

                                </div>
                            </div>
                        </td>

                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="11" style="text-align: center">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>



<style>
    .text-left {
        text-align: left;
        margin-top:6px;
    }

    .head_text {
        color: #877a7a;
        float: left;
    }

    .main-body {
        padding: 15px;
    }

    .card {
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06);
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 0 solid rgba(0, 0, 0, .125);
        border-radius: .25rem;
    }

    .card-body {
        flex: 1 1 auto;
        min-height: 1px;
        padding: 1rem;
    }

    .gutters-sm {
        margin-right: -8px;
        margin-left: -8px;
    }

    .gutters-sm>.col,
    .gutters-sm>[class*=col-] {
        padding-right: 8px;
        padding-left: 8px;
    }

    .mb-3,
    .my-3 {
        margin-bottom: 1rem !important;
    }

    .bg-gray-300 {
        background-color: #e2e8f0;
    }

    .h-100 {
        height: 100% !important;
    }

    .shadow-none {
        box-shadow: none !important;
    }

</style>
