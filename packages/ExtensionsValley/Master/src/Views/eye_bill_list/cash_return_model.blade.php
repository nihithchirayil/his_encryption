<div class="modal" tabindex="-1" role="dialog" id="modalCashReturn">
    <div class="modal-dialog" role="document" style="width:78%;">
        <div class="modal-content">
            <div class="modal-header table_header_common">
                <button type="button" class="close close_white" onclick="reloadWindow();"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h3 class="modal-title">Return Bill</h3>
            </div>
            <div class="modal-body" style="min-height:300px;" id="modalCashReturnBody">

            </div>
        </div>
    </div>
</div>
