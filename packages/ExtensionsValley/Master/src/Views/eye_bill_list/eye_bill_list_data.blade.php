<style>
    .popupDiv {
        position: absolute;
    }

    .list-group-item:first-child {
        border-top-left-radius: 0px !important;
        border-top-right-radius: 0px !important;

    }

    .list-group-item {
        padding: 10px 12px !important;
    }

    .popDiv {
        display: none;
        /* background: white; */
        border-radius: 2px;
        /* box-shadow: 0px 0px 4px #d0d0d0; */
        padding: 0px;
        position: absolute;
        top: -21px;
        min-width: 162px;
        z-index: 850 !important;
        width: 150px;
        min-height: 104px;
        left: 44px;
    }

    .popDiv .show {
        display: block;
    }

    .list-group-item {
        background-color: #ffffff !important;
    }

    button.close {
        margin: 1px -6px !important;
    }

    .list-group-item {
        padding: 2px 8px !important;
    }

    .list-group-item:hover {
        background-color: whitesmoke !important;
    }

    .pop_closebtn {
        background: #333 none repeat scroll 0 0;
        border-radius: 50%;
        box-shadow: 0 0 5px #a2a2a2;
        color: #fff;
        cursor: pointer;
        font-size: 12px;
        font-weight: bold;
        height: 20px;
        padding: 2px 6px;
        position: absolute;
        right: -10px;
        top: -4px;
        min-width: 20px;
    }

    .search_header {
        background: #36A693 !important;
        color: #FFF !important;
    }

    .close {
        opacity: 1 !important;
        color: black !important;
    }
    .ps-scrollbar-x-rail{
      left:11px !important;
    }

</style>
<div class="theadscroll col-md-12 padding_sm" style="width: 101% !important;margin-left:-9px;">
   <div class="col-md-12" style="height:337px;">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
    style="font-size: 12px;margin-top:5px;">
    <tr class="table_header_common"
        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">

        <th width="60px"><i class="fa fa-lsit"></i></th>
        <th width="">Sl.No.</th>
        <th width="">Bill No.</th>
        <th width="">Bill Date</th>
        <th width="">UHID</th>
        <th width="">Patient Name</th>
        <th width="">Bill Amt</th>
        <th width="">Discount</th>
        <th width="">Net Amt.</th>
        <th width="">Paid Amt.</th>
        <th width="">Adv.Adj.</th>
        <th width="">Rtn.Amt.</th>
        <th width="">Consulting Doc.</th>
        <th width="">Pre.by</th>
        <th width="">Location</th>


    </tr>
    <tbody>
        @if (count($res) > 0)

            @foreach ($res as $data)
            @php
            $pay_color = ' ';
            $paid_color = ' ';
            $cancel_color = ' ';
            $return_color = ' ';
            $generatedFlag=false;
            $is_includeindischarge_color=' ';
            
           if (intval($data->return_amount) >= intVal($data->net_amount_wo_roundoff) && $data->paid_status ==1) {
                $return_color = 'full-return-bg';
            } elseif (intVal($data->net_amount_wo_roundoff) > intVal($data->return_amount) && $data->paid_amount != 0 && $data->return_amount != 0 && $data->paid_status ==1) {
                $return_color = 'partially-return-bg';
            }
            if($data->is_includeindischarge==1){
                $is_includeindischarge_color="discharge-bg";
            }
            if($data->pending_status == 2 && $data->cancelled_status != 3){
                $paid_color = 'pending-requested-bg';
            }
           if($bill_type=='SB'){
            $generatedIdsArray=[];
           foreach($allGeneratedIds as $ids){
            $generatedIdsArray[]=$ids->bill_head_id;
            }


            try{
                if(in_array($data->id,$generatedIdsArray)){
                    $generatedFlag=true;
                }else{
                    $generatedFlag=false;
                }
            } catch( \Exception $e){
                $generatedFlag=false;
            }

           }

        @endphp

                <tr>
                    <td class="common_td_rules">
                        @if($data->paid_amount <= 0.0 && $data->payment_code == 'cash/Card' &&  $data->cancelled_status != 3 && $data->net_amount_wo_roundoff > 0 && $data->pending_status !=2)
                        <input type="checkbox" onclick="checkSameUhid('{{$data->uhid}}',{{$data->id}})" class="check_bill checked_bill{{$data->id}}" value="{{ $data->id }}" data-uhid = "{{$data->uhid}}" data-payment-type={{$data->payment_type}}/>
                        @endif
                        <button type="button" class="pull-right"
                            onclick="displayBillDetails({{ $data->id }},'{{$data->bill_tag}}',{{$data->package_id}})"
                            style="width: 25px; height: 20px; border: none;">
                            <i class="fa fa-list"> </i>
                        </button>
<div class="popupDiv">

    <div class="popDiv" id="popup{{ $data->id }}" style="display: none;">
        <div class="col-md-1 pull-right"
        style="border-radius: 50%;width:18px;height:19px;background:#bebbbb;top: -3px;left:5px">
    <button type=" button" class="close" id="ppclose{{ $data->id }}"
            data-dismiss="modal" aria-label="Close">
            <span class="text-info" aria-hidden="true">×</span>
            </button>
        </div>
        <div class="col-md-12" >
            <ul class="list-group" style="cursor: pointer">
                @if ($data->paid_amount == 0.0 && $data->cancelled_status != 3 && $generatedFlag==false)
                    <li class="list-group-item" id="cancel{{$data->id}}"
                        onclick="displayCancelBillStatus({{ $data->id }})">
                        <b>Cancel</b>
                    </li>
                @elseif($data->paid_amount > 0.0 && $data->paid_amount > $data->return_amount && $data->is_includeindischarge==0  && $data->dr_seen_status == 0 && $generatedFlag==false)
                    <li class="list-group-item"><a href="{{URL::to('/')}}/eyebillreturn/getBillReturnView/{{$data->id}}"><b>Refund/Return</b></a></li>
                @endif
                <li class="list-group-item"><a href="{{URL::to('/')}}/eyeBilling/eyeBilling/{{$data->id}}"> <b>View/Edit</b></a></li>
                @if($data->paid_status==1 && $data->cancelled_status !=3 && $data->paid_amount > $data->return_amount)
                <li class="list-group-item" id="pending{{$data->id}}"
                    onclick="displayPendingBillStatus({{ $data->id }})">
                    <b>Pending Request</b>
                </li>
                @endif
                @if($data->bill_tag=='CONS' && intval($disable_auto_cash_recepit)==1)
                <li class="list-group-item" id="print_registration_bill" onclick="reprintRegistrationBill(<?=$data->id?>,<?=$data->visit_id?>)"><b>Print</b></li>
                @else
                <li class="list-group-item" id="print_serv_bill" onclick=""><b>Print</b></li>
                @endif
            </ul>
        </div>

    </div>
</div>
                    </td>
                    <td class="td_common_numeric_rules {{ $paid_color }}" id="paid_bg{{$data->id}}">
                        {{ ($res->currentPage() - 1) * $res->perPage() + $loop->iteration }}</td>
                    <td class="common_td_rules {{ $cancel_color }}" title="{{ $data->bill_no }}" id="cancel_bg{{$data->id}}">
                        {{ $data->bill_no }}</td>
                    @if ($data->bill_date != '')
                        <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->bill_date)) }}</td>
                    @else
                        <td class="common_td_rules">{{ $data->bill_date }}</td>
                    @endif
                    <td class="common_td_rules {{{$is_includeindischarge_color}}} " title="{{ $data->uhid }}">{{ $data->uhid }}</td>
                    <td class="common_td_rules" title="{{ $data->patient_name }}">
                        {{ $data->patient_name }}
                    </td>
                    <td class="td_common_numeric_rules">{{ number_format($data->bill_amount, 2) }}</td>
                    <td class="td_common_numeric_rules">{{ $data->discount_amount }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($data->net_amount_wo_roundoff, 2) }}
                    </td>
                    <td class="td_common_numeric_rules">{{ number_format(round($data->paid_amount),2) }}</td>
                    <td class="common_td_rules">{{ number_format($data->advance_adjusted, 2) }}</td>
                    <td class="td_common_numeric_rules {{ $return_color }}">
                        {{ number_format($data->return_amount, 2) }}</td>
                    <td class="common_td_rules">{{ $data->consulting_doctor_name }}</td>
                    <td class="common_td_rules">{{ $data->username }}</td>
                    <td class="common_td_rules">{{ $data->location_name }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="18">No Record Match</td>
            </tr>
        @endif

    </tbody>
</table>
   </div>
</div>
<div class="col-md-12">
    <div class="clearfix"></div>
    <div class="col-md-12 text-right">
        <ul class="pagination purple_pagination" style="text-align:right !important; margin: -24px -4px;">
            {!! $page_links !!}
        </ul>
    </div>
</div>


<script>
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var bill_no = $('#bill_no').val();
            var bill_tag = $('#bill_tag').val();
            var payment_type = $('#payment_type').val();
            var prepared_by = $('#prepared_by').val();
            var location = $('#location').val();
            var item_desc = $('#item_desc_hidden').val();
            var visit_type = $('#visit_type').val();
            var patient = $('#patient_hidden').val();
            var uhid = $('#patient_uhid').val();
            var ip_no = $('#ip_no').val();
            var company = $('#company').val();
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var params = {
                bill_no: bill_no,
                bill_tag: bill_tag,
                payment_type: payment_type,
                prepared_by: prepared_by,
                location: location,
                item_desc: item_desc,
                visit_type: visit_type,
                patient: patient,
                uhid: uhid,
                ip_no: ip_no,
                company: company,
                from_date: from_date,
                to_date: to_date
            };
            $.ajax({
                type: "POST",
                url: url,
                data: params,
                beforeSend: function() {
                    $('#pharmacy_bill_data').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                    $('#pharmacybilllistBtn').attr('disabled', true);
                    $("#pharmacybilllistspin").removeClass("fa fa-search");
                    $("#pharmacybilllistspin").addClass("fa fa-spinner fa-spin");

                },
                success: function(data) {
                    if (data) {
                        $('#pharmacy_bill_data').html(data);
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                    }

                },
                complete: function() {
                    $('#pharmacy_bill_data').LoadingOverlay("hide");
                    $('#pharmacybilllistBtn').attr('disabled', false);
                    $("#pharmacybilllistspin").removeClass("fa fa-spinner fa-spin");
                    $("#pharmacybilllistspin").addClass("fa fa-search");

                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                },
            });
            return false;
        });

    });
</script>
