@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/common-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
        rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-slide.css') }}" rel="stylesheet">

@endsection
<style>
    .ajaxSearchBox {
        margin-top: -12px !important;
    }
    .mate-input-box {
        width: 100% !important;
        position: relative !important;
        padding: 15px 4px 4px 4px !important;
        border-bottom: 1px solid #01A881 !important;
        box-shadow: 0 0 3px #CCC !important;
    }

    .mate-input-box label {
        position: absolute !important;
        top: -2px !important;
        left: 6px !important;
        font-size: 12px !important;
        font-weight: 700 !important;
        color: #107a8c !important;
        padding-top: 2px !important;
    }
</style>
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="user_id" value="{{ $user_id }}">
    <input type="hidden" id="head_id_hidden" value=0>
    <input type="hidden" id="bill_tag_hidden" value=0>
    <input type="hidden" id="package_id_hidden" value=0>
    <input type="hidden" id="user_name" value="{{ $user_name }}">

    <div class="right_col">
        <div class="row">
            <input id="bill_type" type="hidden" value={{ $bill_type }}>
            <div class="col-md-12 padding_sm">
                <div id="filter_area" class="col-md-12"
                    style="padding-left:0px !important;width: 101.75%;margin-left: -10px;">
                    <div class="box no-border no-margin">
                        <div class="box-body" style="padding-bottom:15px;">
                            <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                                <thead>
                                    <tr class="table_header_common">
                                        <th colspan="11">{{ $title }}

                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="row">
                                <div class="col-md-8 padding_sm" style="">
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">Bill Number</label>
                                            <input class="form-control hidden_search reset" value=""
                                                autocomplete="off" type="text" id="bill_no" name="bill_no" />
                                            <div id="bill_noAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters" value="" type="hidden" name="bill_no_hidden"
                                                value="" id="bill_no_hidden">
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box ">
                                            <label class="filter_label ">Prepared By</label>
                                            <select name="prepared_by" class="form-control select2 reset" id="prepared_by">
                                                <option value="">Select</option>
                                                @foreach ($prepared_by as $data)
                                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box ">
                                            <label class="filter_label">Location</label>
                                            <select name="location" class="form-control select2 reset" id="location">
                                                <option value="">Select Location</option>
                                                @foreach ($location_name as $data)
                                                    <option value="{{ $data->id }}">{{ $data->location_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label ">Item Desc</label>
                                            <input class="form-control hidden_search reset" value=""
                                                autocomplete="off" type="text" id="item_desc" name="item_desc" />
                                            <div id="item_descAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters" value="" type="hidden"
                                                name="item_desc_hidden reset" value="" id="item_desc_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">UHID</label>
                                            <input class="form-control hidden_search reset" value=""
                                                autocomplete="off" type="text" id="patient_uhid"
                                                name="patient_uhid" />
                                            <div id="patient_uhidAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters reset" value="" type="hidden"
                                                name="patient_uhid_hidden" value=""
                                                id="patient_uhid_hidden">
                                            <button type="button" class="btn btn-sm btn-primary advanceSearchBtn"
                                                style=" position: absolute; top: 15px; right: 0;"><i
                                                    class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">Patient Name</label>
                                            <input class="form-control hidden_search reset" value=""
                                                readonly autocomplete="off" type="text" id="patient_name"
                                                name="patient_name" />
                                            <div id="patient_nameAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters reset" value="" type="hidden"
                                                name="patient_name_hidden" value=""
                                                id="patient_name_hidden">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label>From Date</label>
                                            <input type="text" data-attr="date" autocomplete="off"
                                                name="from_date" value="{{ date('M-d-y') }}"
                                                class="form-control datepicker filters reset"
                                                placeholder="YYYY-MM-DD" id="from_date">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label>To Date</label>
                                            <input type="text" data-attr="date" autocomplete="off"
                                                name="to_date" value="{{ date('M-d-y') }}"
                                                class="form-control datepicker filters reset"
                                                placeholder="YYYY-MM-DD" id="to_date">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding-sm pull-right">
                                        <button type="button" class="btn bg-green btn-block pull-right"
                                            id="pay_bill_btn" style="height:50px;">
                                            <i class="fa fa-credit-card"></i> Pay Bill
                                        </button>
                                    </div>

                                    <div class="col-md-3 pull-right" style="">
                                        <button type="button" title="Search" class="btn btn-primary btn-block "
                                            id="pharmacybilllistBtn" onclick="getPharmacyBillList()"><i
                                                id="pharmacybilllistspin"
                                                class="padding_sm fa fa-search search_btn"></i>Search
                                            Bill</button>

                                        <button type="button" title="Reset" class="btn btn-default btn-block"
                                                id="pharmacybilllResetBtn" onclick="reset()"><i
                                                    id=""
                                                    class="padding_sm fa fa-refresh"></i>Refresh</button>
                                      
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 padding_sm"
                style="min-height: 20px;box-shadow: 0px 0px 1px 0px;width: 100%;margin-left: -2px;margin-top: 10px;">
                <div class="col-md-12 padding_sm">
                   
                    <div class="col-md-2 padding_sm paid-bg">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="paid_bill" type="radio" name="bill_return" value=2>
                            <label class="text-blue" for="paid_bill">
                                Paid Bill
                            </label>

                        </div>
                    </div>
                    <div class="col-md-2 padding_sm unpaid-bg">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="Unpaid_bill" type="radio" name="bill_return" value=3>
                            <label class="text-blue" for="Unpaid_bill">
                                Unpaid Bill
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm cancelled-bg">
                        <div class="radio radio-success inline no-margin">
                            <input class="checkit" id="cancelled_bill" type="radio" name="bill_return" value=4>
                            <label class="text-blue" for="cancelled_bill">
                                Cancelled Bill
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm full-return-bg">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="full_return_bill" type="radio" name="bill_return" value=5>
                            <label class="text-blue" for="full_return_bill">
                                Full Return Bill
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm partially-return-bg">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="partially_return_bill" type="radio" name="bill_return" value=6>
                            <label class="text-blue" for="partially_return_bill">
                                Partially Return Bill
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm" style="margin-left: -10px;">
                        <div class="col-md-12 padding_sm pending-requested-bg">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="Pending_request_bill" type="radio" name="bill_return" value=6>
                                <label class="text-blue" for="Pending_request_bill">
                                    Pending Bills
                                </label>
                            </div>
                        </div>
    
                    </div>
                </div>
                
            </div>
            <div class="col-md-12 padding_sm"
                style="height: 370px;box-shadow: 0px 0px 1px 0px;width: 100%;margin-left: -3px;margin-top: 10px;"
                id="pharmacy_bill_data">
            </div>
        </div>
    </div>

    <!-------modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="cancelBillSetup">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header table_header_common">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h3 class="modal-title">Bill Cancellation</h3>
                </div>
                <div class="modal-body" style="min-height:313px;" id="cancelBillSetupBody">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="col-md-6">
                                <div class="mate-input-box">
                                    <label class="filter_label " for="">Bill number:</label><input
                                        class="form-control" type="text" id="cancel_bill" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Patient
                                        name:</label><input class="form-control" type="text" id="cancel_patient"
                                        readonly></div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px">
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Payment
                                        Type:</label></b><input class="form-control" type="text" id="cancel_paytype"
                                        readonly></div>
                            </div>
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Cancel Requested
                                        By</label></b><input class="form-control" type="text" id="cancel_by" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px">
                            <div class="col-md-12">
                                <div class="mate-input-box" style="height:  !important;height: 141px !important;">
                                    <label class="filter_label " for="">Cancel Reason:</label>
                                    <div class="clearfix"></div>
                                    <textarea name="" id="Cancel_reason" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-6 pull-right">
                            <div class="col-md-3 pull-right"><button type="button" class="btn btn-primary"
                                    onclick="cancelBill()" id="Cancel"><i id="Cancel_spin"
                                        class="fa fa-trash padding_sm"></i>Cancel</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------print modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header table_header_common" style="">
                    <h4 class="modal-title" style="color: white;float:left">Print Configuration</h4>
                    <button type="button" class="close" onclick="" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle" checked>
                        Include Hospital Header
                        <input style="margin-left: 15px;" type="checkbox" name="is_duplicate" id="duplicate">
                        Duplicate Print
                    </div>

                    <div class="col-md-12" style="margin-top:4px;">
                        <button onclick="printBillDetails()" class="btn bg-primary pull-right" style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---------------------pending bill model------------------------->
    <div class="modal" tabindex="-1" role="dialog" id="pendinBill">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header table_header_common">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h3 class="modal-title">PENDING REQUEST</h3>
                </div>
                <div class="modal-body" style="min-height:313px;" id="pendingBillSetupBody">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="col-md-6">
                                <div class="mate-input-box">
                                    <label class="filter_label " for="">Bill number:</label><input
                                        class="form-control" type="text" id="pending_bill" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Patient
                                        name:</label><input class="form-control" type="text" id="pending_patient"
                                        readonly></div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px">
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Pending Requested
                                        By</label></b><input class="form-control" type="text" id="pending_by"
                                        readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px">
                            <div class="col-md-12">
                                <div class="mate-input-box" style="height:  !important;height: 141px !important;">
                                    <label class="filter_label " for="">Pending Reason:</label>
                                    <div class="clearfix"></div>
                                    <textarea name="" id="Pending_reason" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-6 pull-right">
                            <div class="col-md-3 pull-right"><button type="button" class="btn btn-primary"
                                    onclick="pendingBill()" id="pending"> <i id="pending_spin" class="fa fa-save"></i>
                                    Request</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------service print modal---------------->

    <div class="modal fade" id="serv_print_config_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 400px;width: 100%">
            <div class="modal-content ">
                <div class="modal-header table_header_common" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Print Configuration</h4>
                </div>
                <div class="modal-body" style="min-height: 123px">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-6 padding_sm">
                            <div class="checkbox checkbox-success inline no-margin">
                                <input type="checkbox" id="titleShow" value="1" checked>
                                <label style="margin-top: 10px;margin-left: :10px;" for="titleShow">Include Hospital
                                    Header</label>

                            </div>
                        </div>
                        <div class="col-md-6 padding_sm">
                            {{-- <div class="checkbox checkbox-warning inline no-margin">
                                <input onclick="getDoctorSlots()" type="checkbox" id="is_duplicate" value="1">
                                <label style="margin-top: 10px;margin-left: :10px;" for="is_duplicate"> Duplicate
                                    Print</label>

                            </div> --}}
                        </div>
                    </div>
                    <div class="col-md-12" style="margin: 10px 0px;">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" id="printMode" value="1" checked="true">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin: 10px 0px;display:none" id="detail_summary_print">
                        <span style="margin-right: 5%;">Print Type:</span>
                        @if ($details_flag_enabled_in_print_pb==1)
                        @php
                        $chk1='checked'; 
                        $chk2=''; 
                  @endphp
                @else
                @php
                $chk1=''; 
                $chk2='checked'; 
                 @endphp 
                        @endif
                        <input style="margin-right: 1%;" type="radio" name="detail_setup" id="detail_print" value="1" {{ $chk1 }}>Detail
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="detail_setup" id="summary_print" value="2" {{ $chk2 }}>Summary
                    </div>


                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" id="print_list_btn" type="button"onclick="PrintBill();"
                        class="btn btn-primary">Print <i id="print_list_spin" class="fa fa-print"></i></button>
                </div>
            </div>
        </div>
    </div>


    <!---------------------cash recive modal--------------------------->

    @include('Master::eye_bill_list.cash_receive_model')


    @include('Master::eye_billing.advancePatientSearch')

@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/eye_bill_list.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/eye_cash_receive.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
    @endsection
