<div class="theadscroll" style="position: relative; height: 550px;margin-top: 10px">
    <table id="doctor_slots_data" class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="8%">Appointment Time</th>
                <th width="8%">Token</th>
                <th width="15%">UHID</th>
                <th width="20%">Patient Name</th>
                <th width="10%">Mobile No.</th>
                <th width="10%">Created By</th>
                <th width="10%">Created at</th>
                <th width="2%" title="Block/Unblock Slot"><button onclick="blockAppointment()"
                        id="blockAppointmentBtn" class="btn btn-warning" type="button"><i id="blockAppointmentSpin"
                            class="fa fa-ban"></i></button></th>
                <th width="3%" title="Copy Appointment"><i class="fa fa-clone"></i></th>
                <th width="3%" title="Cancel Appointment"><i class="fa fa-trash"></i></th>
            </tr>
        </thead>
        <tbody id="doctor_slots_data_body">
            <?php
                if(count($slots)!=0){

                    foreach ($slots as $each) {
                        $class_color='';
                        if ($each->exception_type=='2') {
                        $class_color='bg-red';
                       }else if ($each->exception_type=='1') {
                        $class_color='bk_dark_orange';
                       }else if (intval($each->dr_seen_status)==1) {
                        $class_color='bk_blue';
                       }else if($each->paid_status=='1'){
                        $class_color='bg-green';
                       } else if (intval($each->appointment_status)>1) {
                        $class_color='bg-pink-active';
                       } else if (intval($date_diff)==1) {
                        $class_color='bg-grey-active';
                       }
                        if($class_color){
                        ?>
            <tr class="addAppoinment <?= $class_color ?>" id="addAppoinment<?= $each->token_no ?>">
                <td class="common_td_rules"> <?= date('h:i A', strtotime($each->from_time)) ?>
                    <input type="hidden" value="<?= $class_color ?>">
                </td>
                <td class="common_td_rules"><?= $each->token_no ?></td>
                <td class="common_td_rules"><?= $each->uhid ? $each->uhid : '-' ?></td>
                <td class="common_td_rules"><?= $each->patient_name ? $each->patient_name : '-' ?></td>
                <td class="common_td_rules"><?= $each->mobile_no ? $each->mobile_no : '-' ?></td>
                <td class="common_td_rules"><?= $each->created_by ? $each->created_by : '-' ?></td>
                <td title="<?= date('M-d-Y h:i A', strtotime($each->created_at)) ?>" class="common_td_rules">
                    <?= $each->created_at ? date('M-d-Y', strtotime($each->created_at)) : '-' ?></td>
                <?php
                    if($each->exception_type=='2'){
                ?>
                <td style="text-align: center" class="common_td_rules">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input
                            onclick="selectUnblockSlot(<?= $each->token_no ?>,'<?= $each->from_time ?>','<?= $each->to_time ?>','<?= $each->schedule_id ?>')"
                            type="checkbox" id="unblockAppoinment<?= $each->token_no ?>" value="1">
                        <label for="unblockAppoinment<?= $each->token_no ?>"></label>
                    </div>
                </td>
                <?php }else {
                    ?>
                <td style="text-align: center" class="common_td_rules">-</td>
                <?php
                }
                if(($blade_id=='1') && (intval($each->appointment_status)>1)){
                ?>
                <td style="text-align: center" class="common_td_rules"> <button title="Copy Appointment"
                        onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',2)"
                        type="button" class="btn btn-warning" style="padding: 0px 5px"
                        id="cloneAppointmentIdBtn<?= $each->appointment_id ?>"><i class="fa fa-clone"
                            id="cloneAppointmentIdSpin<?= $each->appointment_id ?>"></i></button>
                </td>
                <?php
                }else {
                    ?>
                <td style="text-align: center" class="common_td_rules">- </td>
                <?php
                }
                ?>
                <td style="text-align: center" class="common_td_rules">- </td>
            </tr>
            <?php

                }else if($each->booked_status=='1'){
                        ?>
            <tr class="addAppoinment bg-lime-active" id="addAppoinment<?= $each->token_no ?>">

                <td style="cursor: pointer"
                    onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',1)"
                    class="common_td_rules"> <?= date('h:i A', strtotime($each->from_time)) ?> <input type="hidden" value="<?= $class_color ?>"></td>
                <td style="cursor: pointer"
                    onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',1)"
                    class="common_td_rules"><?= $each->token_no ?></td>
                <td style="cursor: pointer"
                    onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',1)"
                    class="common_td_rules"><?= $each->uhid ? $each->uhid : '-' ?></td>
                <td style="cursor: pointer"
                    onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',1)"
                    class="common_td_rules"><?= $each->patient_name ? $each->patient_name : '-' ?></td>
                <td style="cursor: pointer"
                    onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',1)"
                    class="common_td_rules"><?= $each->mobile_no ? $each->mobile_no : '-' ?></td>
                <td style="cursor: pointer"
                    onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',1)"
                    class="common_td_rules"><?= $each->created_by ? $each->created_by : '-' ?></td>
                <td style="cursor: pointer"
                    onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',1)"
                    title="<?= date('M-d-Y h:i A', strtotime($each->created_at)) ?>" class="common_td_rules">
                    <?= $each->created_at ? date('M-d-Y', strtotime($each->created_at)) : '-' ?></td>
                <td style="cursor: pointer"
                    onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',1)"
                    style="text-align: center" class="common_td_rules">-</td>
                <?php
                    if($blade_id=='1'){
                ?>
                <td style="text-align: center" class="common_td_rules"> <button title="Copy Appoinment"
                        onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',2)"
                        type="button" class="btn btn-warning" style="padding: 0px 5px"
                        id="cloneAppointmentIdBtn<?= $each->appointment_id ?>"><i class="fa fa-clone"
                            id="cloneAppointmentIdSpin<?= $each->appointment_id ?>"></i></button>
                </td>
                <?php
                    }else {
                        ?>
                <td style="text-align: center" class="common_td_rules">- </td>
                <?php
                    }
                ?>

                <td style="text-align: center" class="common_td_rules"> <button title="Cancel Appointment"
                        onclick="cancelAppointment(<?= $each->appointment_id ?>)" type="button"
                        class="btn btn-danger" style="padding: 0px 5px"
                        id="cancelAppointmentIdBtn<?= $each->appointment_id ?>"><i class="fa fa-trash"
                            id="cancelAppointmentIdSpin<?= $each->appointment_id ?>"></i></button>
                </td>
            </tr>
            <?php

                }else{
                        ?>
            <tr class="addAppoinment" id="addAppoinment<?= $each->token_no ?>">

                <td onclick="addAppoinment(<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>','<?= $each->booked_status ?>')"
                    class="common_td_rules"><?= date('h:i A', strtotime($each->from_time)) ?> <input type="hidden" value="<?= $class_color ?>"></td>
                <td onclick="addAppoinment(<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>','<?= $each->booked_status ?>')"
                    class="common_td_rules"><?= $each->token_no ?></td>
                <td onclick="addAppoinment(<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>','<?= $each->booked_status ?>')"
                    class="common_td_rules"><?= $each->uhid ? $each->uhid : '-' ?></td>
                <td onclick="addAppoinment(<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>','<?= $each->booked_status ?>')"
                    class="common_td_rules"><?= $each->patient_name ? $each->patient_name : '-' ?></td>
                <td onclick="addAppoinment(<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>','<?= $each->booked_status ?>')"
                    class="common_td_rules"><?= $each->mobile_no ? $each->mobile_no : '-' ?></td>
                <td onclick="addAppoinment(<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>','<?= $each->booked_status ?>')"
                    class="common_td_rules"><?= $each->created_by ? $each->created_by : '-' ?></td>
                <td onclick="addAppoinment(<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>','<?= $each->booked_status ?>')"
                    title="<?= date('M-d-Y h:i A', strtotime($each->created_at)) ?>" class="common_td_rules">
                    <?= $each->created_at ? date('M-d-Y', strtotime($each->created_at)) : '-' ?></td>
                <td style="text-align: center" title="Block Slot">
                    <div class="checkbox checkbox-warning inline no-margin">
                        <input
                            onclick="selectBlockSlot(<?= $each->token_no ?>,'<?= $each->from_time ?>','<?= $each->to_time ?>','<?= $each->schedule_id ?>')"
                            type="checkbox" id="blockAppoinment<?= $each->token_no ?>" value="1">
                        <label for="blockAppoinment<?= $each->token_no ?>"></label>
                    </div>
                </td>
                <td onclick="addAppoinment(<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>','<?= $each->booked_status ?>')"
                    class="common_td_rules">-</td>
                <td onclick="addAppoinment(<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>','<?= $each->booked_status ?>')"
                    class="common_td_rules">-</td>
            </tr>
            <?php
                }
            }
                }else{
                    ?>
            <tr>
                <td colspan="12" style="text-align: center">No Result Found</td>
            </tr>
            <?php
                }
            ?>

        </tbody>
    </table>
</div>
