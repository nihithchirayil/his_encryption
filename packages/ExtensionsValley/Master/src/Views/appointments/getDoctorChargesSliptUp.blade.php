<div class="theadscroll" style="position: relative; height: 400px;margin-top: 10px">
    <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="10%">SI. NO.</th>
                <th width="70%">Charge Head</th>
                <th width="20%">Price</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $net_amount = 0;
            $i=1;
                if(count($doctor_pricing)!=0){
                    foreach ($doctor_pricing as $each) {
                        $net_amount += floatval($each->ret_amount);
                        ?>
            <tr>
                <td class="td_common_numeric_rules"><?= $i ?></td>
                <td class="common_td_rules"><?= $each->ret_itemname ?></td>
                <td class="td_common_numeric_rules"><?= $each->ret_amount ?></td>
            </tr>
            <?php $i++;
                    }
                    ?>
            <tr class="bg-blue">
                <td colspan="2" class="common_td_rules">Total</td>
                <td class="td_common_numeric_rules"><?= number_format($net_amount, 2, '.', '') ?></td>
            </tr>
            <?php

                }else {
                    ?>
            <tr>
                <td style="text-align: center" colspan="3"> No Result Found</td>
            </tr>
            <?php
                }
            ?>

        </tbody>
    </table>
</div>
