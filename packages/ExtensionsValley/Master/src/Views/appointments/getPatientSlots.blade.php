<div class="theadscroll" style="position: relative; height: 500px;margin-top: 10px">
    <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="20%">Doctor Name</th>
                <th width="20%">Booking Time</th>
                <th width="10%">Token</th>
                <th width="20%">UHID</th>
                <th width="20%">Patient Name</th>
                <th width="10%">Mobile No.</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(count($slots)!=0){

                    foreach ($slots as $each) {
                        $class_color='';
                        if (intval($each->appointment_status)>1) {
                        $class_color='bg-pink-active';
                       }else if (intval($each->dr_seen_status)==1) {
                        $class_color='bk_blue';
                       }else if($each->paid_status=='1'){
                           $class_color='bg-green';
                       }
                        if($class_color){
                        ?>
            <tr class="addAppoinment <?= $class_color ?>" id="addAppoinment<?= $each->token_no ?>">
                <td class="common_td_rules"> <?= $each->doctor_name ?></td>
                <td class="common_td_rules"> <?= date('M-d-Y h:i A', strtotime($each->from_time)) ?></td>
                <td class="common_td_rules"><?= $each->token_no ?></td>
                <td class="common_td_rules"><?= $each->uhid ? $each->uhid : '-' ?></td>
                <td class="common_td_rules"><?= $each->patient_name ? $each->patient_name : '-' ?></td>
                <td class="common_td_rules"><?= $each->mobile_no ? $each->mobile_no : '-' ?></td>
            </tr>
            <?php

                }else if($each->appointment_status=='1'){
                        ?>
            <tr style="cursor: pointer"
                onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',3,<?= $each->doctor_id ?>)"
                class="addAppoinment bg-lime-active" id="addAppoinment<?= $each->token_no ?>">
                <td class="common_td_rules"> <?= $each->doctor_name ?></td>
                <td class="common_td_rules"> <?= date('M-d-Y h:i A', strtotime($each->from_time)) ?></td>
                <td class="common_td_rules"><?= $each->token_no ?></td>
                <td class="common_td_rules"><?= $each->uhid ? $each->uhid : '-' ?></td>
                <td class="common_td_rules"><?= $each->patient_name ? $each->patient_name : '-' ?></td>
                <td class="common_td_rules"><?= $each->mobile_no ? $each->mobile_no : '-' ?></td>
            </tr>
            <?php

                }
            }
                }else{
                    ?>
            <tr>
                <td colspan="7" style="text-align: center">No Result Found</td>
            </tr>
            <?php
                }
            ?>

        </tbody>
    </table>
</div>
