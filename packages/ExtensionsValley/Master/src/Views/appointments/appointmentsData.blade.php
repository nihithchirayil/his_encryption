@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/colors.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" value="" id="patient_id_hidden">
    <input type="hidden" value="" id="patient_token">
    <input type="hidden" value="" id="session_id">
    <input type="hidden" value="" id="appointment_id">
    <input type="hidden" value="0" id="appointment_edit_status">
    <input type="hidden" value="1" id="blade_id">
    <input type="hidden" id="company_type_hidden" value="1">

    @include('Master::RegistrationRenewal.advancePatientSearch')

    <div class="modal fade" id="getDoctorChagesModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="getDoctorChagesModelHeader"></h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div class="col-md-12 padding_sm" id="getDoctorChagesModelDiv">

                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="right_col" style="min-height: 700px !important;">
        <div class="container-fluid">
            <div class="row padding_sm">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-8 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 675px;">
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Booking Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" onblur="getDoctorSlots()" class="form-control datepicker"
                                            value="<?= date('M-d-Y') ?>" id="patient_visit_date">
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top:10px;">
                                    <div class="mate-input-box">
                                        <label for="">Doctor</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off"
                                            class="form-control hidden_search" id="patient_doctor" value="">
                                        <input type="hidden"  value="" id="patient_doctor_hidden">
                                        <div class="ajaxSearchBox" id="patient_doctorAjaxDiv"
                                            style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999px; position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label>All Slots</label>
                                        <div class="clearfix"></div>
                                        <div class="checkbox checkbox-warning inline no-margin">
                                            <input onclick="getDoctorSlots()" type="checkbox" id="getbooking_allslots"
                                                value="1">
                                            <label style="margin-top: 10px;margin-left: :10px;"
                                                for="getbooking_allslots"></label>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label>Search</label>
                                        <div class="clearfix"></div>
                                        <input onkeyup="searchAppointmentData()" type="text"
                                            class="form-control" id="booked_patient_search">
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: 15px">
                                    <button title="Search Appointment" id="searchAppointmentBtn" type="button"
                                        class="btn btn-primary btn-block" onclick="addWindowLoad('appointmentlisting')"><i
                                            id="searchAppointmentSpin" class="fa fa-list"></i></button>
                                    <button title="Registration Renewal" type="button" class="btn btn-success btn-block"
                                        onclick="addWindowLoad('registration_renewal')"><i
                                            class="fa fa-registered"></i></button>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <span><i class="fa fa-circle lime"></i> Booked</span>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <span><i class="fa fa-circle pink"></i> Checked In</span>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <span><i class="fa fa-circle green"></i> Paid</span>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <span><i class="fa fa-circle round_blue"></i> Doctor Seen</span>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <span><i class="fa fa-circle orange"></i> Leave</span>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <span><i class="fa fa-circle red"></i> Block</span>
                                </div>

                                <div class="clearfix"></div>

                                <div id="doctor_appointmentslot_div" style="margin-top: 10px;">
                                    <div class='col-md-12 padding_sm text-center red' style='margin-top: 150px'>
                                        <h3> Please Select Doctor
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" id="patientappoinment_div">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 675px;">
                                <div class="col-md-12 padding_sm text-center green">
                                    <strong id="appointment_title"><?= $title ?></strong>
                                </div>

                                <div class="col-md-4 padding_sm" style="margin-top: 25px">
                                    <div class="radio radio-primary inline no-margin">
                                        <input onclick="checkPatientStatus()" checked type="radio" id="new_patient"
                                            name="patient_status" value="1">
                                        <label style="padding-left:2px;" for="new_patient">
                                            New Patient</label>
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm" style="margin-top: 25px">
                                    <div class="radio radio-primary inline no-margin">
                                        <input onclick="checkPatientStatus()" type="radio" id="has_uhid"
                                            name="patient_status" value="2">
                                        <label style="padding-left:2px;" for="has_uhid">
                                            Has UHID</label>
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Slot Start Time</label>
                                        <div class="clearfix"></div>
                                        <input readonly type="text" class="form-control" id="slot_start_time">

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6 padding_sm" style="margin-top: -20px;">
                                    <div class="checkbox checkbox-success inline no-margin">
                                        <input type="checkbox" class="form-control" name="is_video_consultation"  id="is_video_consultation" value="1">
                                        <label class="text-blue " for="is_video_consultation">Is Video Consultation</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div style="display: none" id="patient_uhid_status">
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 padding_sm" style="margin-top: 10px">
                                        <div class="mate-input-box">
                                            <label for="">UHID</label>
                                            <div class="clearfix"></div>
                                            <input type="hidden" class="form-control" id="patientsearch_ipstatus"
                                                value="1">
                                            <input type="text" onkeyup="searchPatientUhid(this)" autocomplete="off"
                                                class="form-control" id="patient_uhid" name="patient_uhid"
                                                value="">
                                            <div id="patient_uhid_AjaxDiv" class="ajaxSearchBox"
                                                style="margin-top: -13px !important;z-index: 99999;display: block;"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm" style="margin-top: 25px">
                                        <button title="Patient Advance Search" type="button"
                                            class="btn btn-primary btn-block advancePatientSearchBtn"
                                            onclick="advancePatientSearch(1)"><i
                                                class="fa fa-search advancePatientSearchSpin"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-5 padding_sm" style="margin-top: 15px">
                                    <div class="mate-input-box">
                                        <label class="red">Salutation</label>
                                        <div class="clearfix"></div>
                                        <select onchange="getPatientGender()" class="form-control select2"
                                            id="patientsalutation">
                                            <option value="">Select</option>
                                            <?php
                                                foreach ($salutation as $each) {
                                                    ?>
                                            <option data-sex="<?= $each->sex ?>" value="<?= $each->title ?>">
                                                <?= $each->title ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-7 padding_sm" style="margin-top: 15px">
                                    <div class="mate-input-box">
                                        <label class="red">Patient Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" id="patient_name">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">DOB</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" class="form-control patient_dob" id="patient_dob"
                                            onblur="getAge()">
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label for="">Age</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" class="form-control" id="patient_age"
                                            onkeyup="getagefromdob()" maxlength="3"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Age in</label>
                                        <div class="clearfix"></div>
                                        <select onchange="getagefromdob()" class="form-control select2"
                                            id="patientagein">
                                            <option value="1">Years</option>
                                            <option value="2">Month</option>
                                            <option value="3">Days</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3 padding_sm" style="margin-top: 10px;display: none">
                                    <div class="mate-input-box">
                                        <label for="">Year</label>
                                        <div class="clearfix"></div>
                                        <input type="text" readonly class="form-control" id="age_years"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top: 10px;display: none">
                                    <div class="mate-input-box">
                                        <label for="">Month</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" readonly class="form-control" id="age_month"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top: 10px;display: none">
                                    <div class="mate-input-box">
                                        <label for="">Day</label>
                                        <div class="clearfix"></div>
                                        <input type="text" readonly class="form-control" id="age_days"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Gender</label>
                                        <div class="clearfix"></div>
                                        <select class="form-control select2" id="patient_gender">
                                            <option value="">Select</option>
                                            <?php
                                                foreach ($gender as $each) {
                                                    ?>
                                            <option data-code="<?= $each->code ?>" value="<?= $each->id ?>">
                                                <?= $each->name ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Mobile No.</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" id="patient_phone"
                                            onkeyup="searchPatientPhone(this)" autocomplete="off"
                                            oninput="this.value = this.value.replace(/[^0-9 +]/g, '').replace(/(\..*)\./g, '$1');">
                                        <div id="patient_phone_AjaxDiv" class="ajaxSearchBox"
                                        style="margin-top: -16px !important;z-index: 99999;"></div>
                                    </div>
                                </div>

                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label for="">Email</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" class="form-control" id="patient_email"
                                            onblur="isEmail()">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label for="">Address</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" class="form-control" id="patient_address">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-12 padding_sm" style="margin-top: 10px">
                                    <div class="col-md-8 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">Total Charge</label>
                                            <div class="clearfix"></div>
                                            <label style="margin-top: 30px;" id="doctor_charges_hidden"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 padding_sm" style="margin-top: 18px">
                                        <button type="button" id="getDoctorChargesBtn" class="btn btn-primary btn-block"
                                            onclick="getDoctorChageSplitUp(0)"><i id="getDoctorChargesSpin"
                                                class="fa fa-columns"></i>
                                            Charge Split Up </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 padding_sm" style="margin-top: 20px">
                                    <div class="col-md-6 padding_sm">
                                        <button type="button" class="btn btn-warning btn-block"
                                            onclick="resetAppointmentForm(1,1,1)"><i class="fa fa-recycle"></i>
                                            Reset </button>
                                    </div>
                                    <div class="col-md-6 padding_sm">
                                        <button type="button" id="createPatientBookingBtn"
                                            class="btn btn-success btn-block" onclick="createPatientBooking()"><i
                                                id="createPatientBookingSpin" class="fa fa-save"></i>
                                            Save </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/Dashboard/js/moment/moment.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/appointments.js') }}"></script>
@endsection
