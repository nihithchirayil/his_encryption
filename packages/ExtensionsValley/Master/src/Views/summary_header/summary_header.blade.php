@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/default/css/bootstrap-timepicker.min.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<style>
    .header_bg{
        background: rgb(37,184,155) !important;
background: linear-gradient(90deg, rgba(37,184,155,1) 0%, rgba(24,147,141,1) 0%, rgba(23,182,197,1) 100%) !important;
color:white !important;
    }
    .gray_headeer_bg{
        background: rgb(37,184,155) !important;
background: linear-gradient(90deg, rgba(37,184,155,1) 0%, rgba(219,219,219,1) 0%, rgba(174,174,174,1) 100%) !important;
    }
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<div class="right_col">
    <div class="col-md-12 no-padding text-center box-body header_bg" style="height:25px;">
        <h6 style="margin-top:5px;"><b>{{$title}}</b></h6>
    </div>

    <div class="col-md-12 text-center box-body">
            <div class="col-md-3">
                <div class="mate-input-box">
                    <label class="filter_label">Doctor</label>
                    <select name="doctor" class="form-control select2 filters" id="doctor" style="color:#555555; padding:4px 12px;" onchange="selectHeaderDetails(this.value);">
                        <option value=""> select- </option>
                        @foreach($doctor_list as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="mate-input-box">
                    <label class="filter_label">Department Name</label>
                    <input type="text" class="form-control" id="department_name" placeholder="Department Name"/ value="">
                </div>
            </div>
            <div class="col-md-3 pull-right">
                <button style="float:right;" onclick="saveHeader();" type="button" name="save" class="btn btn-primary" id="save">
                    <i class="fa fa-save"></i>
                     Save
                </button>
            </div>
    </div>
    <div class="col-md-12 text-center box-body">
            <label class="filter_label">Doctor Header</label>
            <textarea class="form-control texteditor11" id="doctor_header" placeholder="Doctor Header"/></textarea>
    </div>
    <div class="col-md-12 text-center box-body">
            <label class="filter_label">Doctor Footer</label>
            <textarea class="form-control texteditor11" id="doctor_footer" placeholder="Doctor Footer"/></textarea>
    </div>
</div>
@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/js/investigation.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>

<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script type="text/javascript">

    $(document).ready(function (){
        initTinymce();
        $(".select2").select2({placeholder: "", maximumSelectionSize: 6});
    });

    function initTinymce(){
        tinymce.init({
            selector: 'textarea.texteditor11',
            height: '290',
            autoresize_min_height: '90',
            themes: "modern",
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons ',
            imagetools_cors_hosts: ['picsum.photos'],
             menubar: 'file edit view insert format tools table help',
            menubar: false,
            statusbar: false,
            toolbar: false,
             toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | outdent indent | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
            // toolbar_sticky: true,
            browser_spellcheck: true,
            autosave_ask_before_unload: true,
            autosave_interval: '30s',
            autosave_prefix: '{path}{query}-{id}-',
            autosave_restore_when_empty: false,
            autosave_retention: '2m',
            paste_enable_default_filters: false,
            image_advtab: true,
            contextmenu: false,

            importcss_append: true,

            height: 300,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: 'mceNonEditable',
            toolbar_mode: 'sliding',

            branding: false,
            statusbar: false,
            forced_root_block: '',
            setup: function(editor) {

                editor.ui.registry.addButton('Ucase', {
                    text: 'A^',
                    onAction: function() {
                        TextToUpperCase();
                    },
                });
                editor.ui.registry.addButton('Lcase', {
                    text: 'a^',
                    onAction: function() {
                        TextToLowerCase();
                    },
                });
                editor.ui.registry.addButton('Icase', {
                    text: 'I^',
                    onAction: function() {
                        TextToInterCase();
                    },
                });
                editor.ui.registry.addButton('Ccase', {
                    text: 'C^',
                    onAction: function() {
                        FirstLetterToInterCase();
                    },
                });

            },
        });
    }

    function selectHeaderDetails(doctor_id){
        var base_url = $('#base_url').val();
        var c_token = $('#c_token').val();
        $.ajax({
            url: base_url + '/summary_header/getDoctorHeader',
            type: 'GET',
            data: {doctor_id: doctor_id},
            beforeSend: function () {
                $('#doctor_header').html('');
                $('#doctor_footer').html('');
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if(obj == '0'){
                    toastr.warning('No Header and Footer Found!');
                    $('#department_name').val('');
                    tinymce.get("doctor_header").setContent('');
                    tinymce.get("doctor_footer").setContent('');

                }else{
                    $('#department_name').val(obj.dept_name);
                    tinymce.get("doctor_header").setContent(obj.doctor_headder);
                    tinymce.get("doctor_footer").setContent(obj.doctor_footer);
                }
            }
        });
    }

    function saveHeader(){
        var doctor_id = $('#doctor').val();
        var department_name = $('#department_name').val();
        var doctor_header = tinymce.get("doctor_header").getContent();
        var doctor_footer = tinymce.get("doctor_footer").getContent();
        var base_url = $('#base_url').val();
        var c_token = $('#c_token').val();
        if(doctor_id == ''){
            toastr.warning('Please Select Doctor!');
            return false;
        }
        if(department_name == ''){
            toastr.warning('Please Select Department!');
            return false;
        }
        if(doctor_header == ''){
            toastr.warning('Please Enter Header!');
            return false;
        }
        if(doctor_footer == ''){
            toastr.warning('Please Enter Footer!');
            return false;
        }
        $.ajax({
            url: base_url + '/summary_header/saveDoctorHeader',
            type: 'POST',
            data: {doctor_id: doctor_id, department_name: department_name, doctor_header: doctor_header, doctor_footer: doctor_footer, c_token: c_token},
            beforeSend: function () {
                $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (data) {
                if(data == 1){
                    toastr.success('Summary Details Saved Successfully!');
                }else{
                    toastr.error('Something Went Wrong!');
                }
            },
            complete: function () {
                $("body").LoadingOverlay("hide");
            },
        });
    }
</script>
@endsection
