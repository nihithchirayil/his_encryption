@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <style>
        .mate-input-box {
            height: 55px !important;
        }

    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row codfox_container">

            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                                <div class="mate-input-box">
                                    <label class="custom_floatlabel">UHID.</label>
                                    <input type="text" name="op_no_search" autocomplete="off" autofocus value=""
                                        class="form-control" id="op_no_search" placeholder="UHID">
                                    <input type="hidden" id="op_no_search_hidden" value="" name="op_no_search_hidden">
                                    <div id="op_no_searchCodeAjaxDiv" class="ajaxSearchBox"></div>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <?php
                                    $user_id = \Auth::user()->id;
                                    $doctor_id = \DB::table('doctor_master')
                                        ->where('user_id', $user_id)
                                        ->pluck('id');
                                    $practice_loc = \DB::table('practice_location')
                                        ->join('doctor_practice_location', 'doctor_practice_location.practice_location_id', '=', 'practice_location.id')
                                        ->where('practice_location.status', 1)
                                        ->where('doctor_practice_location.doctor_id', $doctor_id)
                                        ->orderBy('practice_location.name')
                                        ->pluck('practice_location.name', 'practice_location.id');
                                    ?>
                                    <label for="">Practice Location</label>
                                    {!! Form::select('practice_location', $practice_loc, '', ['class' => 'form-control select2 filters', 'multiple' => 'multiple-select', 'title' => 'Location', 'id' => 'practice_location', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;']) !!}
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Phone No</label>
                                    <div class="clearfix"></div>
                                    <input type="text" name="phone_no" autocomplete="off" value="" class="form-control"
                                        id="phone_no" placeholder="Phone">
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" name="from_date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                        class="form-control datepicker" id="from_date" placeholder="From Date">
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" name="to_date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                        class="form-control datepicker" id="to_date" placeholder="To Date">
                                </div>
                            </div>

                            <div class="col-md-3">
                                {{-- <div class="col-md-12" style="margin-bottom:-25px;color:blue;">
                                    <h5>Current Practice Location : <b><span style="color:darkturquoise;"
                                                id="current_practice_location"></span></b></h5>
                                </div> --}}
                                <div class="col-md-4 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block bg-info" onclick="searchPatient();" id="searchdatabtn"><i
                                            class="fa fa-search" id="searchdataspin"></i>
                                        Search</button>
                                </div>
                                <div class="col-md-4 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button type="button" class="btn btn-block btn-warning"
                                        onclick="clear_list_search();"><i class="fa fa-times"></i> Clear</button>
                                </div>
                                <div class="col-md-4 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <span class="btn btn-block btn-primary" onclick="goto_add_new_patient();"><i
                                            class="fa fa-plus"></i> Add</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="patientListDiv">

            </div>
        </div>
    </div>
    <input type="hidden" id="ins_base_url" value="{{ URL::to('/') }}">
    <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/patientList.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
@endsection
