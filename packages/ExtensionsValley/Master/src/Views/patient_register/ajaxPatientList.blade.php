<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var file_token = $('#hidden_filetoken').val();
            var op_no_search_hidden = $("#op_no_search_hidden").val();
            var params = {
                _token: file_token,
                op_no_search_hidden: op_no_search_hidden,
                is_ajax: 1
            };
            $.ajax({
                type: "POST",
                url: url,
                data: params,
                beforeSend: function() {
                    $('#patientListDiv').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(msg) {
                    $('#patientListDiv').html(msg);
                },
                complete: function() {
                    $('#patientListDiv').LoadingOverlay("hide");
                },
                error: function() {
                    toastr.error("Please Check Internet Connection");
                }
            });
            return false;
        });
    });

    function PatientEdit(id) {
        window.location = "{!! route('extensionsvalley.patient_register.patientEdit') !!}/" + id;
    }
</script>
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table">
            <thead>
                <tr class="table_header_bg">
                    <th width="5%" class="common_td_rules">UHID </th>
                    <th width="5%" class="common_td_rules">Name</th>
                    <th width="5%" class="common_td_rules">DOB</th>
                    <th width="5%" class="common_td_rules">Age</th>
                    <th width="5%" class="common_td_rules">Phone</th>
                    <th width="5%" class="common_td_rules">Last visit Date</th>
                    <th width="6%" class="common_td_rules">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php $i = 0; @endphp
                @if (isset($patient_data) && sizeof($patient_data) > 0)
                    @foreach ($patient_data as $list)
                        <tr style="cursor:pointer;">
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->uhid }}
                            </td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->patient_name }}</td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->dob }}
                            </td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->age }}
                            </td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->phone }}</td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                @if ($list->last_visit_datetime != '')
                                    {{ date('M-d-Y', strtotime($list->last_visit_datetime)) }}
                                @endif
                            </td>
                            <td class="pull-left">
                                @if (date('Y-m-d') != date('Y-m-d', strtotime($list->last_visit_datetime)))
                                    <button type="button" class="btn btn-info btn-xs"
                                        onclick="PatientRenew('{{ $list->id }}')">
                                        <i class="fa fa-repeat"></i>
                                        Renew
                                    </button>
                                @endif

                                <button type="button" class="btn btn-warning btn-xs"
                                    onclick="goToSelectedPatient('{{ $list->id }}');">
                                    <i class="fa fa-eye"></i>
                                    View
                                </button>

                                <button type="button" class="btn btn-success btn-xs"
                                    onclick="PatientEdit('{{ $list->id }}')">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </button>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
