@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    @include('Dashboard::dashboard.partials.headersidebar')
@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">


@endsection
@section('content-area')

    <!-- page content -->
    <style>
        label {
            margin-top: 4px;
        }

    </style>


    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 ">
                <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
                <input type="hidden" name="patient_id" id="patient_id" value="{{ $patient_list->id }}" />
            </div>
            <div class="col-md-12 ">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 60px;">
                        <div class="col-md-12 " style="text-align: center">
                            <h3 class="blue"><strong><?= $title ?></strong></h3>
                        </div>
                    </div>
                </div>
            </div>
            <form id="patient_registration" name="patient_registration" method="post">
                <div class="col-md-12 " style="margin-top: 10px">
                    <div class="no-border no-margin">
                        <div class="box-footer" style="padding: 5px; box-shadow: 0px 1px #ccc;">
                            <div class="col-md-12" style="margin-top:20px;">
                                <div class="col-md-2 ">
                                    <div class="mate-input-box">
                                        <label>UHID</label>
                                        <input class="form-control" value="{{ $patient_list->uhid }}" type="text"
                                            name="uhid" readonly autocomplete="off" id="uhid">
                                    </div>
                                </div>

                                <div class="col-md-1 ">
                                    <div class="mate-input-box">
                                        <label>Salutation</label>
                                        {!! Form::select('title', $title_list, $patient_list->title, ['class' => 'form-control select2', 'id' => 'title', 'style' => 'height: 34px !important;border-radius: 3px;']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="mate-input-box">
                                        <label class="red">Patient Name*</label>
                                        <input class="form-control" type="text" name="patient_name" autocomplete="off"
                                            id="patient_name" value="{{ $patient_list->patient_name }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top:10px;">
                                <div class="col-md-2 ">
                                    <div class="mate-input-box">
                                        <label class="red">DOB*</label>
                                        <input class="form-control datepicker" type="text" name="dob" autocomplete="off"
                                            id="dob" value="{{ $patient_list->dateob }}">
                                    </div>
                                </div>

                                <div class="col-md-2 ">
                                    <div class="mate-input-box">
                                        <label class="red">Gender*</label>
                                        {!! Form::select('title', $gender, $patient_list->gender, ['class' => 'form-control select2', 'placeholder' => 'Select', 'id' => 'gender', 'style' => 'height: 34px !important;border-radius: 3px;']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mate-input-box">
                                        <label class="red">Phone*</label>
                                        <input class="form-control" type="text" name="phone" autocomplete="off" id="phone"
                                            value="{{ $patient_list->phone }}">
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="mate-input-box">
                                        <label class="red">Pin Code*</label>
                                        <input type="text" class="form-control number_only" id="pincode" name="pincode"
                                            placeholder="" value="{{ $patient_list->pincode }}" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-9" style="margin-top:10px;">
                                <button type="button" data-toggle="collapse" data-target=".registration_more_info"
                                    class="btn bg-info pull-right">
                                    <i class="fa fa-arrow-down" aria-hidden="true"></i> More</button>
                                </button>
                            </div>
                            <div class="col-md-12 registration_more_info collapse" style="margin-top:10px;">
                                <div class="col-md-4 ">
                                    <div class="mate-input-box">
                                        <label>Email</label>
                                        <input class="form-control" type="text" value="{{ $patient_list->email }}"
                                            name="email" autocomplete="off" id="email">
                                    </div>
                                </div>

                                <div class="col-md-2 ">
                                    <div class="mate-input-box">
                                        <label>Age</label>
                                        <input class="form-control number_only" type="text" name="age" value=""
                                            autocomplete="off" id="age">
                                    </div>
                                </div>
                                <div class="col-md-3 ">
                                    <div class="mate-input-box" style="height: 45px">
                                        <label>Age Criteria</label>

                                        <div class="col-md-4 " style="margin-top: 5px">
                                            <div class="radio radio-primary inline no-margin">
                                                <input value="d" type="radio" class="form-control" name="age_criteria"
                                                    id="age_day">
                                                <label class="text-blue" for="age_day">Day</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 " style="margin-top: 5px">
                                            <div class="radio radio-primary inline no-margin">
                                                <input value="m" type="radio" class="form-control" name="age_criteria"
                                                    id="age_month">
                                                <label class="text-blue" for="age_month">Month</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 " style="margin-top: 5px">
                                            <div class="radio radio-primary inline no-margin">
                                                <input value="y" type="radio" class="form-control" name="age_criteria"
                                                    id="age_year">
                                                <label class="text-blue" for="age_year">Year</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12 registration_more_info collapse" style="margin-top:10px">
                                <div class="col-md-7 ">
                                    <div class="mate-input-box">
                                        <label>Address</label>
                                        <input class="form-control" type="text" value="{{ $patient_list->address }}"
                                            name="address" autocomplete="off" id="address">
                                    </div>
                                </div>

                                <div class="col-md-2 ">
                                    <div class="mate-input-box">
                                        <label>Country</label>
                                        @if ($patient_list->country != '')
                                            @php $curent_country = $patient_list->country; @endphp
                                        @else
                                            @php $curent_country = 1; @endphp
                                        @endif
                                        {!! Form::select('country', $country, $curent_country, ['class' => 'form-control select2', 'placeholder' => 'Select', 'id' => 'country', 'style' => 'height: 34px !important;border-radius: 3px;']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 registration_more_info collapse"
                                style="margin-top:10px;margin-bottom:20px;">
                                <div class="col-md-2 ">
                                    <div class="mate-input-box">
                                        <label>State</label>
                                        @if ($patient_list->state != '')
                                            @php $curent_state = $patient_list->state; @endphp
                                        @else
                                            @php $curent_state = 1; @endphp
                                        @endif
                                        {!! Form::select('state', $state, $curent_state, ['class' => 'form-control select2', 'placeholder' => 'Select', 'id' => 'state', 'style' => 'height: 34px !important;border-radius: 3px;']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="mate-input-box">
                                        <label>District</label>
                                        @if ($patient_list->district != '')
                                            @php $curent_district = $patient_list->district; @endphp
                                        @else
                                            @php $curent_district = 331; @endphp
                                        @endif
                                        {!! Form::select('district', $district, $curent_district, ['class' => 'form-control select2', 'placeholder' => 'Select', 'id' => 'district', 'style' => 'height: 34px !important;border-radius: 3px;']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="mate-input-box">
                                        <label>Area</label>
                                        @if ($patient_list->area != '')
                                            @php $curent_area = $patient_list->area; @endphp
                                        @else
                                            @php $curent_area = 1385; @endphp
                                        @endif
                                        {!! Form::select('area', $area, $curent_area, ['class' => 'form-control select2', 'placeholder' => 'Select', 'id' => 'area', 'style' => 'height: 34px !important;border-radius: 3px;']) !!}
                                    </div>
                                </div>

                                <div class="col-md-3 ">
                                    <div class="mate-input-box">
                                        <label>Remarks</label>
                                        <input class="form-control" type="text" name="remarks" autocomplete="off"
                                            id="remarks" value="{{ $patient_list->patient_remarks }}">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="h10"></div>
                            <div style="margin-top: 30px">
                                <div class="col-md-12" style="margin-top:20px;">
                                    <div class="col-md-3  pull-right"></div>
                                    <div class="col-md-2  pull-right">
                                        <button type="button" class="btn bg-green btn-block" onclick="update_patient();"
                                            id="btn_save_patient"> <i class="fa fa-save"></i> Update
                                        </button>
                                    </div>
                                    <div class="col-md-2  pull-right">
                                        <button type="reset" class="btn bg-orange btn-block" id="btn_reset_patient"><i
                                                class="fa fa-refresh"></i> Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </form>



    </div>
    </div>
@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/patientRegistration.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js') }}"></script>
@endsection
