<!-- .css -->
<style>
    #doctorHeadClass p:first-child {
        margin: 0 !important;
        padding: 0 !important;
    }

    .number_class {
        text-align: right;
    }

    @media print {
        table {
            page-break-after: auto;
        }

        tr {
            page-break-inside: auto;
            page-break-after: auto
        }

        td {
            page-break-inside: auto;
            page-break-after: auto
        }

        thead {
            display: table-header-group
        }

        tfoot {
            display: table-footer-group
        }
    }
</style>
<style type="text/css" media="print">
    @page {
        margin: 15px;
    }

    table {
        font-size: 13px;
    }

    @media print {
        .pagebreak {
            page-break-before: always;
        }

        /* page-break-after works, as well */
    }
</style>
<!-- .css -->

<!-- Bootstrap -->
<!-- <link href="{{ asset('packages/extensionsvalley/dashboard/css/bootstrap.min.css') }}" rel="stylesheet"> -->
<!-- .box-body -->
<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data col-md-12">
            <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px; "
                width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead style="display: table-header-group;">
                    <tr>
                        <td colspan="6">&nbsp;&nbsp;<strong>Registration/Consultation</strong>&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="6"> &nbsp;</td>
                        <td colspan="6"> &nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;<strong>PATIENT NAME</strong>&nbsp;&nbsp;</td>
                        <td align="center" style="border-left: none;">:</td>
                        <td style="border-left: none;">
                            &nbsp;&nbsp;<?= @$res[0]->patient_name ? $res[0]->patient_name : '' ?>&nbsp;&nbsp;
                        </td>
                        <td style="border-left: none;">
                            &nbsp;&nbsp;<strong>GENDER/AGE</strong>&nbsp;&nbsp;</td>
                        <td align="center" style="border-left: none;">&nbsp;:&nbsp;</td>
                        <td style="border-left: none;">
                            &nbsp;
                            <?= @$res[0]->gender ? $res[0]->gender . '/' . $res[0]->p_age : '' ?>&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;<strong>UHID</strong>&nbsp;&nbsp;</td>
                        <td align="center" style="border-left: none;">:</td>
                        <td style="border-left: none;">
                            &nbsp;&nbsp;<?= @$res[0]->uhid ? $res[0]->uhid : '' ?>&nbsp;&nbsp;
                        </td>
                        <td style="border-left: none;">&nbsp;&nbsp;<strong>BILL
                                NUMBER</strong>&nbsp;&nbsp;</td>
                        <td align="center" style="border-left: none;">&nbsp;:&nbsp;</td>
                        <td style="border-left: none;">
                            &nbsp;
                            <?= @$res[0]->bill_no ? $res[0]->bill_no : '' ?>&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;<strong>BILL DATE TIME</strong>&nbsp;&nbsp;</td>
                        <td align="center" style="border-left: none;">:</td>
                        <td style="border-left: none;">
                            &nbsp;&nbsp;<?= @$res[0]->billdatetime ? date('M-d-Y h:i A', strtotime($res[0]->billdatetime)) : '' ?>&nbsp;&nbsp;
                        </td>
                        <td style="border-left: none;">&nbsp;&nbsp;<strong>CONSULTATION
                                TIME</strong>&nbsp;&nbsp;</td>
                        <td align="center" style="border-left: none;">&nbsp;:&nbsp;</td>
                        <td style="border-left: none;">
                            &nbsp;
                            <?= @$res[0]->time_slot ? $res[0]->time_slot : '' ?>&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;<strong>DOCTOR</strong>&nbsp;&nbsp;</td>
                        <td align="center" style="border-left: none;">:</td>
                        <td style="border-left: none;">
                            &nbsp;&nbsp;<?= @$doc_array[0]['doctor_name'] ? $doc_array[0]['doctor_name'] : '' ?>&nbsp;&nbsp;
                        </td>
                        <td style="border-left: none;">&nbsp;&nbsp;<strong>TOKEN
                                TIME</strong>&nbsp;&nbsp;</td>
                        <td align="center" style="border-left: none;">&nbsp;:&nbsp;</td>
                        <td style="border-left: none;">
                            &nbsp;
                            <?= @$doc_array[0]['token_no'] ? $doc_array[0]['token_no'] : '' ?>&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;<strong>BILL AMOUNT</strong>&nbsp;&nbsp;</td>
                        <td align="center" style="border-left: none;">:</td>
                        <td style="border-left: none;">
                            &nbsp;&nbsp;<?= @$res[0]->net_amount_wo_roundoff ? $res[0]->net_amount_wo_roundoff : 0 ?>&nbsp;&nbsp;
                        </td>
                        <td style="border-left: none;">&nbsp;&nbsp;<strong>AMOUNT WORDS</strong>&nbsp;&nbsp;
                        </td>
                        <td align="center" style="border-left: none;">&nbsp;:&nbsp;</td>
                        <td style="border-left: none;">
                            &nbsp;
                            <?php $number_to_word = \ExtensionsValley\Master\GridController::number_to_word(round(@$res[0]->net_amount_wo_roundoff ? $res[0]->net_amount_wo_roundoff : 0));
                            echo strtoupper($number_to_word);
                            ?>&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6"> &nbsp;</td>
                        <td colspan="6"> &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;&nbsp;<strong>{{ @$res[0]->billcreatedby ? $res[0]->billcreatedby : 0 }}</strong>&nbsp;&nbsp;</td>
                    </tr>

                    <tr>
                        <td colspan="6"> &nbsp;</td>
                        <td colspan="6"> &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules" colspan="6">&nbsp;&nbsp;<strong>Signature</strong>&nbsp;&nbsp;</td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<!-- .box-body -->
