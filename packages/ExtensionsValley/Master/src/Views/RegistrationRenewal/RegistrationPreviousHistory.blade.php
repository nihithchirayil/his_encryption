<div class="theadscroll" style="position: relative; height: 450px;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="11%">Visit Date</th>
                <th width="11%">Renew Validity</th>
                <th width="5%">Visit</th>
                <th width="8%">Visit Type</th>
                <th width="10%">Consulting Doctor</th>
                <th width="8%">Department</th>
                <th width="5%">Amount</th>
                @if ($admission_required == 1)
                    <th width="11%">Admission Date</th>
                    <th width="10%">Ip Number</th>
                    <th width="10%">Admitted Doctor</th>
                    <th width="10%">Current Bed</th>
                    <th width="10%">Admitted Bed</th>
                    <th width="12%">Bed Category</th>
                @endif

            </tr>
        </thead>
        <tbody>
            @if (count($res) > 0)
                @foreach ($res as $row)
                    <tr style="cursor: pointer;">
                        <td title={{ @$row->visit_datetime ? date('M-d-Y h:i A', strtotime($row->visit_datetime)) : '--' }}
                            class=" common_td_rules">
                            {{ @$row->visit_datetime ? date('M-d-Y h:i A', strtotime($row->visit_datetime)) : '--' }}
                        </td>
                        <td title={{ @$row->renew_validity ? date('M-d-Y h:i A', strtotime($row->renew_validity)) : '--' }}
                            class=" common_td_rules">
                            {{ @$row->renew_validity ? date('M-d-Y h:i A', strtotime($row->renew_validity)) : '--' }}
                        </td>
                        <td title={{ @$row->visit_status ? $row->visit_status : '--' }} class=" common_td_rules ">
                            {{ @$row->visit_status ? $row->visit_status : '--' }}</td>
                        <td title={{ @$row->renewal_type ? $row->renewal_type : '--' }} class=" common_td_rules ">
                            {{ @$row->renewal_type ? $row->renewal_type : '--' }}</td>
                        <td title={{ @$row->condoctor ? $row->condoctor : '--' }} class=" common_td_rules ">
                            {{ @$row->condoctor ? $row->condoctor : '--' }}</td>
                        <td title={{ @$row->specialitydesc ? $row->specialitydesc : '--' }} class=" common_td_rules ">
                            {{ @$row->specialitydesc ? $row->specialitydesc : '--' }}</td>
                        <td title={{ @$row->net_amount_wo_roundoff ? $row->net_amount_wo_roundoff : '--' }}
                            class=" td_common_numeric_rules ">
                            {{ @$row->net_amount_wo_roundoff ? $row->net_amount_wo_roundoff : '0.00' }}</td>
                        @if ($admission_required == 1)
                            <td title={{ @$row->actual_admission_date ? date('M-d-Y h:i A', strtotime($row->actual_admission_date)) : '--' }}
                                class=" td_common_numeric_rules ">
                                {{ @$row->actual_admission_date ? date('M-d-Y h:i A', strtotime($row->actual_admission_date)) : '--' }}
                            </td>
                            <td title={{ @$row->admission_no ? $row->admission_no : '--' }}
                                class=" td_common_numeric_rules ">{{ @$row->admission_no ? $row->admission_no : '--' }}
                            </td>
                            <td title={{ @$row->admdoctor ? $row->admdoctor : '--' }} class=" common_td_rules ">
                                {{ @$row->admdoctor ? $row->admdoctor : '--' }}</td>
                            <td title={{ @$row->current_bed_name ? $row->current_bed_name : '--' }}
                                class=" common_td_rules ">{{ @$row->current_bed_name ? $row->current_bed_name : '--' }}
                            </td>
                            <td title={{ @$row->bed_name ? $row->bed_name : '--' }} class=" common_td_rules ">
                                {{ @$row->bed_name ? $row->bed_name : '--' }}</td>
                            <td title={{ @$row->room_type_name ? $row->room_type_name : '--' }}
                                class=" td_common_numeric_rules ">
                                {{ @$row->room_type_name ? $row->room_type_name : '--' }}</td>
                        @endif

                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
