<!-- .css -->
<style>
    #doctorHeadClass p:first-child{
        margin: 0 !important;
        padding: 0 !important;
    }
    .number_class {
        text-align: right;
    }
    @media print
    {
        #footer {
        position: absolute;
        bottom: 138px;
        display: none;
    }
        table{ page-break-after:auto; }
        tr    { page-break-inside:auto; page-break-after:auto }
        td    { page-break-inside:auto; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
    }
</style>
<style type="text/css" media="print">
    table {
        font-size : 13px;
    }
    @page{
       
        margin-left:15px;
        margin-right:15px;
        margin-top:40px;
        margin-bottom:35px;
    }

</style>
<!-- .css -->
<!-- .box-body -->

<div class="box-body">
        <div class="col-md-12" >
            <table style="margin-top:15px; " width="100%"  cellspacing="0" cellpadding="0">
                <thead>
                    @if((string)$hospital_header_status == 1)
                    <tr>
                        <th colspan="10" >
                         {!!$hospitalHeader!!}
                        </th>
                    </tr>
                    @endif
                    
                    <tr>
                   <th colspan="10" >
                        <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;@if((string)$hospital_header_status == 0) margin-top:3cm; @endif " width="100%" border="1" cellspacing="0" cellpadding="0">
                            <thead class="patient_head" style="display: table-header-group;">
                                <tr>
                                    <th colspan="10" style="font-size: 12px;" >
                                        OP PRESCRIPTION
                                    </th>
                                </tr>
                                <tr class="head">
                                    <td><strong>PATIENT NAME</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{ @$patient_name ? strtoupper($patient_name) : ''  }}</td>
                                    <td><strong>UHID</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td colspan="4" style="border-left: none;">{{ @$uhid ? $uhid : '' }}</td>
                                    
                                </tr>
                                <tr>
                                    <td style="border-left: none;"><strong>GENDER/AGE</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">@if($patient_gender==1) Male @elseif($patient_gender==2) Female @elseif($patient_gender==3) Transgender @endif / {{ $patient_age }}</td>
                                    <td style="border-left: none;"><strong>Date</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{ date('M-d-Y') }}</td>
                                </tr>
                                <tr>
                                    <td style="border-left: none;"><strong>Doctor Name</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{ $doctor_name }}</td>
                                    <td style="border-left: none;"><strong></strong></td>
                                    <td align="center" style="border-left: none;"></td>
                                    <td style="border-left: none;"></td>
                                </tr>
                             
                            </thead>
                        </table>
                    </th>
                    </tr>
                   
                </thead>
                <tbody>
                <tr>
                    <td colspan="10">
                        <table style="border-colapse:colapse;margin-top:15px;position: absolute;bottom:138px;width:100%;" cellspacing="0" cellpadding="0">
                            <tbody>
                                    <td>
                                        <div style="float: right;margin-right: 18px;">
                                            <div> {{ $doctor_name }} </div>
                                            <div style="text-align: center">(signature)</div>
                                        </div>
                                    </td>
                                </tr>
                               
                                
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        

</div>
<!-- .box-body -->

