<div class="row">
    <div class="col-md-12 paddding_sm">
        <div class="col-md-6 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 350px;">
                    <div class="col-md-12 padding_sm" style="margin-top: 10px">
                        <div class="mate-input-box">
                            <label>Pricing Company</label>
                            <div class="clearfix"></div>
                            <select onchange="getInsurancePricing('sponsor')" class="form-control pricing_select"
                                id="sponsor_insurance_company">
                                <option value="">Select</option>
                                <?php
                                    foreach ($credit_company as $each) {
                                        $selected="";
                                        if($company_id==$each->id){
                                            $selected = "selected";
                                        }
                                        ?>
                                <option <?= $selected ?> value="<?= $each->id ?>"><?= strtoupper($each->company_name) ?>
                                </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm" style="margin-top: 10px">
                        <div class="mate-input-box">
                            <label>Insurance Pricing</label>
                            <div class="clearfix"></div>
                            <input type="hidden" id="sponsor_insurance_pricing_hidden" value="<?= $pricing_id ?>">
                            <select onchange="getPricingDetalis('sponsor_insurance_pricing')"
                                class="form-control pricing_select" id="sponsor_insurance_pricing">
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 padding_sm" style="margin-top: 10px">
                        <div class="mate-input-box">
                            <label>Expiry Date</label>
                            <div class="clearfix"></div>
                            <input type="text"
                                value="<?= @$pricing_details_string[0]->sponsor_expiry_date ? $pricing_details_string[0]->sponsor_expiry_date : '' ?>"
                                class="form-control insurance_datepicker" id="sponsor_expiry_date">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>
</div>
