<div class="row">
    <div class="col-md-12 paddding_sm">
        <div class="col-md-6 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 400px;">
                    <div class="col-md-12 padding_sm">
                        <label class="blue"><strong>Patient More Details</strong></label>
                    </div>
                    <div class="col-md-4 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Marital Status</label>
                            <div class="clearfix"></div>
                            <select class="form-control more_details" id="patient_MaritalStatus">
                                <option value="">Select</option>
                                <?php
                                $marital_status= @$patient_more_details_string[0]->patient_MaritalStatus ? $patient_more_details_string[0]->patient_MaritalStatus : '';
                                foreach ($MaritalStatus as $key=>$val) {
                                    $selected="";
                                    if($marital_status==$key){
                                        $selected = "selected";
                                    }
                                    ?>
                                <option <?= $selected ?> value="<?= $key ?>">
                                    <?= strtoupper($val) ?>
                                </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Relation</label>
                            <div class="clearfix"></div>
                            <select class="form-control more_details" id="patient_relation">
                                <option value="">Select</option>
                                <?php
                                    $patient_relation= @$patient_more_details_string[0]->patient_relation ? $patient_more_details_string[0]->patient_relation : '';
                                    foreach ($relationship_title as $each) {
                                        $selected="";
                                        if($patient_relation==$each->id){
                                            $selected = "selected";
                                        }
                                        ?>
                                <option <?= $selected ?> value="<?= $each->id ?>">
                                    <?= strtoupper($each->relationship_title) ?>
                                </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Relation Name</label>
                            <div class="clearfix"></div>
                            <input type="text"
                                value="<?= @$patient_more_details_string[0]->patient_relationname ? $patient_more_details_string[0]->patient_relationname : '' ?>"
                                class="form-control" id="patient_relationname">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Occupation</label>
                            <div class="clearfix"></div>
                            <input type="text"
                                value="<?= @$patient_more_details_string[0]->patient_occupation ? $patient_more_details_string[0]->patient_occupation : '' ?>"
                                class="form-control" id="patient_occupation">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>ID Proof Type</label>
                            <div class="clearfix"></div>
                            <select class="form-control more_details" id="patient_idproof_type">
                                <option value="">Select</option>
                                <?php
                                    $patient_idproof_type= @$patient_more_details_string[0]->patient_idproof_type ? $patient_more_details_string[0]->patient_idproof_type : '';
                                    foreach ($id_proof_master as $each) {
                                        $selected="";
                                        if($patient_idproof_type==$each->id){
                                            $selected = "selected";
                                        }
                                        ?>
                                <option <?= $selected ?> value="<?= $each->id ?>">
                                    <?= strtoupper($each->name) ?>
                                </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>ID Number</label>
                            <div class="clearfix"></div>
                            <input type="text"
                                value="<?= @$patient_more_details_string[0]->patient_id_number ? $patient_more_details_string[0]->patient_id_number : '' ?>"
                                class="form-control" id="patient_id_number">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm hidden" style="margin-top: 15px">
                        <label class="blue"><strong>Patient Reference Details</strong></label>
                    </div>
                    <div class="clearfix hidden"></div>
                    <div class="col-md-4 padding_sm hidden" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Reference Type</label>
                            <div class="clearfix"></div>
                            <select class="form-control more_details" id="patient_reference_type">
                                <option value="">Select</option>
                                <?php
                                    $reference_type= @$patient_more_details_string[0]->patient_reference_type ? $patient_more_details_string[0]->patient_reference_type : '';
                                    foreach ($reffernece_type as $each) {
                                        $selected="";
                                        if($reference_type==$each->value){
                                            $selected = "selected";
                                        }
                                        ?>
                                <option <?= $selected ?> value="<?= $each->value ?>">
                                    <?= strtoupper($each->name) ?>
                                </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm hidden" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Hospital Reference</label>
                            <div class="clearfix"></div>
                            <select class="form-control more_details" id="patient_hospital_reference">
                                <option value="">Select</option>
                                <?php
                                    $hospital_reference_id= @$patient_more_details_string[0]->patient_hospital_reference ? $patient_more_details_string[0]->patient_hospital_reference : '';
                                    foreach ($hospital_reference as $each) {
                                        $selected="";
                                        if($hospital_reference_id==$each->id){
                                            $selected = "selected";
                                        }
                                        ?>
                                <option <?= $selected ?> value="<?= $each->id ?>">
                                    <?= strtoupper($each->name) ?>
                                </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm hidden" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Doctor Reference</label>
                            <div class="clearfix"></div>
                            <select class="form-control more_details" id="patient_doctor_reference">
                                <option value="">Select</option>
                                <?php
                                    $doctor_reference_id= @$patient_more_details_string[0]->patient_doctor_reference ? $patient_more_details_string[0]->patient_doctor_reference : '';
                                    foreach ($doctor_reference as $each) {
                                        $selected="";
                                        if($doctor_reference_id==$each->id){
                                            $selected = "selected";
                                        }
                                        ?>
                                <option <?= $selected ?> value="<?= $each->id ?>">
                                    <?= strtoupper($each->name) ?>
                                </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix" hidden></div>
                    <div class="col-md-4 padding_sm hidden" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Blood Group</label>
                            <div class="clearfix"></div>
                            <select class="form-control more_details" id="patient_blood_grp_hdn">
                                <option value="">Select</option>
                                <?php
                                    $patient_blood_grp= @$patient_more_details_string[0]->patient_blood_grp ? $patient_more_details_string[0]->patient_blood_grp : '';
                                    foreach ($blood_group as $each) {
                                        $selected="";
                                        if($patient_blood_grp==trim($each->group_description)){
                                            $selected = "selected";
                                        }
                                        ?>
                                <option <?= $selected ?> value="<?= trim($each->group_description) ?>">
                                    <?= strtoupper(trim($each->group_description)) ?>
                                </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8 padding_sm hidden" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Referral Remarks</label>
                            <div class="clearfix"></div>
                            <input type="text"
                                value="<?= @$patient_more_details_string[0]->patient_refferal_remarks ? $patient_more_details_string[0]->patient_refferal_remarks : '' ?>"
                                class="form-control" id="patient_refferal_remarks">
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm" style="margin-top: 15px">
                        <label class="blue"><strong>Patient Care Of Details</strong></label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>C/O Type</label>
                            <div class="clearfix"></div>
                            <div class="col-md-10 no-padding" style="margin-top: 2px;">
                                <select class="form-control more_details" id="co_type">
                                    <option value="">Select</option>
                                    <?php
                                        $co_type= @$patient_more_details_string[0]->co_type ? $patient_more_details_string[0]->co_type : '';
                                        foreach ($co_types as $each) {
                                            $selected="";
                                            if($co_type==$each->id){
                                                $selected = "selected";
                                            }
                                            ?>
                                    <option <?= $selected ?> value="<?= $each->id ?>">
                                        <?= strtoupper($each->co_name) ?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-2 no-padding">
                                <button class="btn btn-primary" onclick="addCareofType();"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>C/O Name</label>
                            <div class="clearfix"></div>
                            <input type="text"
                                value="<?= @$patient_more_details_string[0]->co_name ? $patient_more_details_string[0]->co_name : '' ?>"
                                class="form-control" id="co_name">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>C/O Mobile</label>
                            <div class="clearfix"></div>
                            <input type="text"
                                value="<?= @$patient_more_details_string[0]->co_mobile ? $patient_more_details_string[0]->co_mobile : '' ?>"
                                class="form-control" id="co_mobile">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Blood Group</label>
                            <div class="clearfix"></div>
                            <select class="form-control more_details" id="patient_blood_grp">
                                <option value="">Select</option>
                                <?php
                                    $patient_blood_grp= @$patient_more_details_string[0]->patient_blood_grp ? $patient_more_details_string[0]->patient_blood_grp : '';
                                    foreach ($blood_group as $each) {
                                        $selected="";
                                        if($patient_blood_grp==trim($each->group_description)){
                                            $selected = "selected";
                                        }
                                        ?>
                                <option <?= $selected ?> value="<?= trim($each->group_description) ?>">
                                    <?= strtoupper(trim($each->group_description)) ?>
                                </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 400px;">
                    <div class="col-md-12 padding_sm">
                        <label class="blue"><strong>VIP Patient</strong></label>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    $vip_patient_status = @$patient_more_details_string[0]->vip_patient_status ? $patient_more_details_string[0]->vip_patient_status : 0;
                    $vip_patient_checked_status = '';
                    if (intval($vip_patient_status) == 1) {
                        $vip_patient_checked_status = 'checked';
                    }
                    ?>
                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <div class="checkbox checkbox-warning inline no-margin">
                                <input <?= $vip_patient_checked_status ?> type="checkbox" id="vip_patient_status" 
                                    value="1" >
                                <label for="vip_patient_status" style="margin-left: 14px;">VIP</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Comments</label>
                            <div class="clearfix"></div>
                            <input type="text"
                                value="<?= @$patient_more_details_string[0]->vip_patient_comments ? $patient_more_details_string[0]->vip_patient_comments : '' ?>"
                                class="form-control" id="vip_patient_comments">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm" style="margin-top: 15px">
                        <label class="blue"><strong>International Patient</strong></label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <?php
                            $international_patient_status = @$patient_more_details_string[0]->international_patient_status ? $patient_more_details_string[0]->international_patient_status : 0;
                            $international_patient_checked_status = '';
                            if (intval($international_patient_status) == 1) {
                                $international_patient_checked_status = 'checked';
                            }
                            ?>
                            <div class="checkbox checkbox-warning inline no-margin">
                                <input <?= $international_patient_checked_status ?> type="checkbox"
                                    id="international_patient_status" value="1">
                                <label for="international_patient_status" style="margin-left: 14px;">International</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Passport Number</label>
                            <div class="clearfix"></div>
                            <input type="text"
                                value="<?= @$patient_more_details_string[0]->patient_passport_no ? $patient_more_details_string[0]->patient_passport_no : '' ?>"
                                class="form-control" id="patient_passport_no">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Passport Expiry</label>
                            <div class="clearfix"></div>
                            <input type="text"
                                value="<?= @$patient_more_details_string[0]->patient_passport_expiry ? $patient_more_details_string[0]->patient_passport_expiry : '' ?>"
                                class="form-control moredetail_datepicker" id="patient_passport_expiry">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-5 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Organ Donation</label>
                            <div class="clearfix"></div>
                            <?php
                            $patient_organ_donation = @$patient_more_details_string[0]->patient_organ_donation ? $patient_more_details_string[0]->patient_organ_donation : 0;
                            $patient_organ_donation_yes = '';
                            $patient_organ_donation_no = '';
                            if (intval($patient_organ_donation) == 1) {
                                $patient_organ_donation_yes = 'checked';
                            } else {
                                $patient_organ_donation_no = 'checked';
                            }
                            ?>
                            <div class="radio radio-warning inline no-margin">
                                <input <?= $patient_organ_donation_yes ?> type="radio"
                                    id="patient_organ_donation_yes" name="patient_organ_donation" value="1">
                                <label for="patient_organ_donation_yes" style="margin-top: 10px;margin-left:14px">Yes</label>
                                <input <?= $patient_organ_donation_no ?> type="radio" id="patient_organ_donation_no"
                                    name="patient_organ_donation" value="0">
                                <label for="patient_organ_donation_no"
                                    style="margin-top: 10px;margin-left: 76px;">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 padding_sm" style="margin-top: 15px">
                        <div class="mate-input-box">
                            <label>Special Notes</label>
                            <div class="clearfix"></div>
                            <input type="text"
                                value="<?= @$patient_more_details_string[0]->patient_special_notes ? $patient_more_details_string[0]->patient_special_notes : '' ?>"
                                class="form-control" id="patient_special_notes">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
