@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/colors.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" value="" id="patient_id_hidden">
    <input type="hidden" value="" id="patient_token">
    <input type="hidden" value="" id="session_id">
    <input type="hidden" value="" id="appointment_id">
    <input type="hidden" value="0" id="appointment_edit_status">
    <input type="hidden" id="company_state" value="<?= $company_state ?>">
    <input type="hidden" id="company_country" value="<?= $company_country ?>">
    <input type="hidden" id="company_district" value="<?= $company_district ?>">
    <input type="hidden" id="company_area" value="<?= $company_area ?>">
    <input type="hidden" id="default_pricing_id" value="<?= $default_pricing ?>">
    <input type="hidden" id="default_pricing_id_edit" value="0">
    <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
    <input type="hidden" id="patient_default_area" value="">
    <input type="hidden" id="bill_id" value="">
    <input type="hidden" id="ip_patient_not_need" value="1">
    <input type="hidden" value="2" id="blade_id">
    <input type="hidden" value="{{ $opPrescriptionStatus }}" id="opPrescriptionStatus">
    <input type="hidden" id="insurance_div_count" value="1">
    <input type="hidden" id="pt_profile_url" value="">
    <input type="hidden" id="op_credit_status" value="{{$op_credit_status}}">

    @include('Master::RegistrationRenewal.advancePatientSearch')
    <div id="pt_details" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header modal-header-sm table_header_bg">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span class="modal-title" style="color:white" id="pt_name_details">NA</span>
                </div>

                <div class="modal-body" id="set_previous_history"style="min-height: 500px">


                </div>

            </div>
        </div>
    </div>
    <div id="pt_profile" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 400px;">
            <div class="modal-content">
                <div class="modal-header modal-header-sm table_header_bg">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span class="modal-title" style="color:white" id="pt_name">NA</span>
                </div>

                <div class="modal-body">
                    <div class="pt_profile_body text-center" style="height:320px">
                        @php
                            $url = URL::to('/') . '/packages/extensionsvalley/patient_profile/default_img.png';
                            $img_url = str_replace('public/', '', $url);
                        @endphp
                        <img src="{{ $img_url }}" alt='patient Profile' width='300' height='300'
                            style='margin:-7px;'>
                    </div>
                    <form id="submitpatient_image">
                        <input type="hidden" name="image_pt_id" id="image_pt_id" value="0">
                        <div class="col-md-9 padding_sm">
                            <div class="mate-input-box ">
                                <label for="">Patient Image</label>
                                <input class="form-control blue" value="" id="pt_image" type="file"
                                    name="pt_image">
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box ">
                                <button id="submitPtImageBtn" type="submit" class="btn btn-primary btn-block">
                                    Submit <i id="submitPtImageSpin" class="fa fa-save"></i></button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="getPrintRegistrationModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="getPrintRegistrationModelHedder"></h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div class="col-md-12 padding_sm" id="getPrintRegistrationModelDiv">

                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-1 padding_sm pull-right">
                        <button onclick="print_generate('result_container_div','A5')" style="padding: 3px 3px"
                            type="button" class="btn btn-primary btn-block"><i class="fa fa-print"></i> Print</button>
                    </div>
                    <div class="col-md-2 padding-sm pull-right">
                        <button type="button" class="btn btn-success  pull-right" id="pay_bill_btn"
                            style="display: none">
                            <i class="fa fa-credit-card"></i> Pay Bill
                        </button>
                    </div>
                    <div class="col-md-1 no-margin pull-right">
                        <div class="radio radio-primary inline no-margin">
                            <input type="radio" name="printMode" checked="true" id="printMode_portrait"
                                value="1">
                            <label for="printMode_portrait">Portrait</label>
                        </div>
                    </div>
                    <div class="col-md-1 no-margin pull-right">
                        <div class="radio radio-primary inline no-margin">
                            <input type="radio" name="printMode" id="printMode_landscape" value="2">
                            <label for="printMode_landscape">Landscape</label>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm pull-right">
                        <div class="checkbox checkbox-warning inline no-margin">
                            <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                            <label for="showTitle">Include Print Header</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="getDoctorChagesModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="getDoctorChagesModelHeader"></h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div class="col-md-12 padding_sm" id="getDoctorChagesModelDiv">

                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="patientSetInsuranceModel" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1250px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="patientSetInsuranceModelHeader">Add Insurance</h4>
                </div>
                <div class="modal-body" style="min-height: 462px">
                    <input type="hidden" id="patient_modeldata_from_type">
                    <div class="col-md-12 padding_sm" id="patientSetInsuranceModelDiv">
                        <div class="col-md-1 pull-right" id=""><button id="add_insurance_btn"
                                class="btn btn-primary" onclick="addnewInsuranceDiv()"><i id="add_insurance_spin"
                                    class="fa fa-plus"></i> Add insurance</button></div>
                        <div id="msform" class="add_insurance_detail_div">
                            <ul id="progressbar" style="cursor: pointer;font-size: 11px;height: 39px;"
                                class="nav nav-tabs" role="tablist">
                            </ul>
                            <div class="tab-content" id="details_tab_body" style="height: 315px;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" type="button" onclick="saveInsurance()"
                        class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade makeMeHide" id="patientInsuranceModel" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1250px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="patientInsuranceModelHeader">NA</h4>
                </div>
                <div class="modal-body" style="min-height: 462px">
                    <input type="hidden" id="patient_modeldata_from_type">
                    <div class="col-md-12 padding_sm" id="patientInsuranceModelDiv">

                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" type="button" onclick="saveModelData()"
                        class="btn btn-primary">Save
                        <i class="fa fa-save"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="right_col" style="min-height: 700px !important;">
        <div class="container-fluid">
            <div class="row padding_sm">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-7 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 700px;">
                                <div class="col-md-2 padding_sm" style="margin-top: 15px">
                                    <div class="mate-input-box">
                                        <label class="red">Booking Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" onblur="getDoctorSlots()" class="form-control patient_dob"
                                            value="<?= date('M-d-Y') ?>" id="patient_visit_date">
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top:15px;">
                                    <div class="mate-input-box">
                                        <label for="">Doctor</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control hidden_search"
                                            id="patient_doctor" value="">
                                        <input type="hidden" value="" id="patient_doctor_hidden">
                                        <div class="ajaxSearchBox" id="patient_doctorAjaxDiv"
                                            style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999px; position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 15px">
                                    <div class="mate-input-box">
                                        <label>All Slots</label>
                                        <div class="clearfix"></div>
                                        <div class="checkbox checkbox-warning inline no-margin">
                                            <input onclick="getDoctorSlots()" type="checkbox" id="getbooking_allslots"
                                                value="1">
                                            <label style="margin-top: 15px;margin-left:15px;"
                                                for="getbooking_allslots"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                    <div class="mate-input-box">
                                        <label>Search</label>
                                        <div class="clearfix"></div>
                                        <input onkeyup="searchAppointmentData()" type="text" class="form-control"
                                            id="booked_patient_search">
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: 25px">
                                    <button title="Search Appointment" id="searchAppointmentBtn" type="button"
                                        class="btn btn-success btn-block" onclick="addWindowLoad('appointmentlisting')"><i
                                            id="searchAppointmentSpin" class="fa fa-list"></i></button>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <span><i class="fa fa-circle lime"></i> Booked</span>
                                </div>
                                <div class="col-md-2 no-padding" style="margin-top: 10px">
                                    <span><i class="fa fa-circle pink"></i> Checked In</span>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <span><i class="fa fa-circle green"></i> Paid</span>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <span><i class="fa fa-circle round_blue"></i> Doctor Seen</span>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <span><i class="fa fa-circle red"></i> Block</span>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <span><i class="fa fa-circle orange"></i> Leave</span>
                                </div>
                                <div id="doctor_appointmentslot_div" style="margin-top: 15px;">
                                    <div class='col-md-12 padding_sm text-center red' style='margin-top: 150px'>
                                        <h3> Please Select Doctor </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 padding_sm" id="patientappoinment_div">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 700px;">
                                <div class="col-md-12 padding_sm text-center green" style="margin-bottom: 20px;">
                                    <strong id="appointment_title"><?= $title ?></strong>

                                    </div>

                                <div class="clearfix"></div>

                                <div class="col-md-4 padding_sm">
                                    <div class="radio radio-primary inline no-margin">
                                        <input onclick="checkPatientStatus()" checked type="radio" id="new_patient"
                                            name="patient_status" value="1">
                                        <label style="padding-left:2px;" for="new_patient">
                                            New Patient</label>
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm">
                                    <div class="radio radio-primary inline no-margin">
                                        <input onclick="checkPatientStatus()" type="radio" id="has_uhid"
                                            name="patient_status" value="2">
                                        <label style="padding-left:2px;" for="has_uhid">
                                            Renewal</label>
                                    </div>
                                </div>

                                <div class="col-md-1 padding_sm" style="margin-top: -17px">
                                    <button type="button" class="btn btn-sm btn-primary btn-block" id="userPreviousHistoryBtn" style="font-size: 11px;"
                                    onclick="previousHistory();">
                                    <i id="userPreviousHistorySpin" class="fa fa-info" title="Patient Previous History"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-success btn-block" id="userProfileBtn" style="font-size: 11px;"
                                    onclick="userProfile();">
                                    <i id="userProfileSpin" class="fa fa-user" title="Patient Image"></i>
                                    </button>
                                </div>

                                <div class="col-md-4 padding_sm" style="margin-top: -20px">
                                    <div class="mate-input-box">
                                        <label class="red">Slot Start Time</label>
                                        <div class="clearfix"></div>
                                        <input readonly type="text" class="form-control" id="slot_start_time">

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 padding_sm" style="margin-top: -15px;">
                                    <div class="checkbox checkbox-success inline no-margin">
                                        <input type="checkbox" class="form-control" name="is_video_consultation"  id="is_video_consultation" value="1">
                                        <label class="text-blue " for="is_video_consultation">Is Video Consultation</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div style="display: none" id="patient_uhid_status">
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">UHID</label>
                                            <div class="clearfix"></div>
                                            <input type="text" onkeyup="searchPatientUhid(this)" autocomplete="off"
                                                class="form-control" id="patient_uhid" name="patient_uhid"
                                                value="">
                                            <div id="patient_uhid_AjaxDiv" class="ajaxSearchBox"
                                                style="margin-top: -13px !important;z-index: 99999;display: block;"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm" style="margin-top: 15px">
                                        <button title="Patient Advance Search" type="button"
                                            class="btn btn-primary btn-block advancePatientSearchBtn"
                                            onclick="advancePatientSearch(1)"><i
                                                class="fa fa-search advancePatientSearchSpin"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Salutation</label>
                                        <div class="clearfix"></div>
                                        <select onchange="getPatientGender()" class="form-control select2"
                                            id="patientsalutation">
                                            <option value="">Select</option>
                                            <?php
                                                foreach ($salutation as $each) {
                                                    ?>
                                            <option data-sex="<?= $each->sex ?>" value="<?= $each->title ?>">
                                                <?= $each->title ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Patient Name</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" class="form-control" id="patient_name">
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Gender</label>
                                        <div class="clearfix"></div>
                                        <select class="form-control select2" id="patient_gender">
                                            <option value="">Select</option>
                                            <?php
                                                foreach ($gender as $each) {
                                                    ?>
                                            <option data-code="<?= $each->code ?>" value="<?= $each->id ?>">
                                                <?= $each->name ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">DOB</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" class="form-control patient_dob"
                                            id="patient_dob" onblur="getAge()">
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label for="">Age</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" id="patient_age"
                                            onkeyup="getagefromdob()" maxlength="3"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Age in</label>
                                        <div class="clearfix"></div>
                                        <select onchange="getagefromdob()" class="form-control select2"
                                            id="patientagein">
                                            <option value="1">Years</option>
                                            <option value="2">Month</option>
                                            <option value="3">Days</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Mobile No.</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" id="patient_phone"
                                            onkeyup="searchPatientPhone(this)" autocomplete="off"
                                            oninput="this.value = this.value.replace(/[^0-9 +]/g, '').replace(/(\..*)\./g, '$1');">
                                        <div id="patient_phone_AjaxDiv" class="ajaxSearchBox"
                                            style="margin-top: -16px !important;z-index: 99999;"></div>
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label>Alternate Mob.</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" id="alternate_phone"
                                            onkeyup="searchPatientAlternatePhone(this)" autocomplete="off"
                                            oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                        <div id="patient_alternate_phone_AjaxDiv" class="ajaxSearchBox"
                                            style="margin-top: -16px !important;z-index: 99999;"></div>
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: 10px;display: none">
                                    <div class="mate-input-box">
                                        <label for="">Year</label>
                                        <div class="clearfix"></div>
                                        <input type="text" readonly class="form-control" id="age_years"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: 10px;display: none">
                                    <div class="mate-input-box">
                                        <label for="">Month</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" readonly class="form-control"
                                            id="age_month"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: 10px;display: none">
                                    <div class="mate-input-box">
                                        <label for="">Day</label>
                                        <div class="clearfix"></div>
                                        <input type="text" readonly class="form-control" id="age_days"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                </div>

                                <div class="col-md-5 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label for="">Email</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" class="form-control" id="patient_email"
                                            onblur="isEmail()">
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label>Pincode</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" class="form-control"
                                            id="patient_pincode"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: 30px">
                                    <button title="Get Location" type="button" id="getPatientLocationBtn"
                                        class="btn btn-primary btn-block" onclick="getPostalCodes()"> <i
                                            id="getPatientLocationSpin" class="fa fa-map-marker"></i>
                                    </button>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label>Country</label>
                                        <div class="clearfix"></div>
                                        <select onchange="getPatientAddress(1)" class="form-control select2"
                                            id="patient_country">
                                            <option value="">Select</option>
                                            <?php
                                                foreach ($country as $each) {
                                                    $selected = "";
                                                    if($each->id == $company_country){
                                                        $selected = "selected";
                                                    }
                                                        ?>
                                            <option <?= $selected ?> value="<?= $each->id ?>"><?= $each->name ?>
                                            </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label>State</label>
                                        <div class="clearfix"></div>
                                        <select onchange="getPatientAddress(2)" class="form-control select2"
                                            id="patient_state">
                                            <option value="">Select</option>
                                            <?php
                                                foreach ($state as $each) {
                                                    $selected = "";
                                                    if(strtolower($each->name)== strtolower($company_state)){
                                                        $selected = "selected";
                                                    }
                                                        ?>
                                            <option <?= $selected ?> value="<?= $each->id ?>"><?= $each->name ?>
                                            </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top: 10px;display: none">
                                    <div class="mate-input-box">
                                        <label class="red">District</label>
                                        <div class="clearfix"></div>
                                        <select class="form-control select2" id="patient_district">
                                            <option value="">Select</option>
                                            <?php
                                                foreach ($district as $each) {
                                                    $selected = "";
                                                    if(strtolower($each->name) == strtolower($company_district)){
                                                        $selected = "selected";
                                                    }
                                                        ?>
                                            <option <?= $selected ?> value="<?= $each->name ?>"><?= $each->name ?>
                                            </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                                    <div class="mate-input-box" id="area_select">
                                        <label>Area</label>
                                        <div class="clearfix"></div>
                                        <select class="form-control select2" onchange="getPatentLocation()"
                                            id="patient_area">
                                            <option value="">Select</option>
                                            <?php
                                                foreach ($area as $each) {
                                                    $selected = "";
                                                        ?>
                                            <option <?= $selected ?> value="<?= $each->name ?>"><?= $each->name ?>
                                            </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="mate-input-box" id="area_text" style="display: none">
                                        <label class="">Area</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" class="form-control"
                                            id="add_patient_area">
                                    </div>


                                </div>
                                <div class="col-md-1" style="margin-top: 39px;margin-left: -10px;">
                                    <div class="checkbox checkbox-success inline"
                                        style="margin-top: -7px !important;margin-left:7px !important;">
                                        <input onclick="patientAreaStatus()" id="add_area" type="checkbox"
                                            name="add_area" title="Area not found.">
                                        <label class="text-blue" for="add_area">
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label for="">Address</label>
                                        <div class="clearfix"></div>
                                        <input autocomplete="off" type="text" class="form-control"
                                            id="patient_address">
                                    </div>
                                </div>

                                <div class="col-md-6 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label for="">Remarks</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" id="patient_remarks">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Company</label>
                                        <div class="clearfix"></div>
                                        <input readonly type="text" class="form-control" id="company_pricing">
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label class="red">Pricing</label>
                                        <div class="clearfix"></div>
                                        <input readonly type="text" class="form-control" id="pricing_company">
                                        <input type="hidden" id="commpany_id_hidden">
                                        <input type="hidden" id="pricing_id_hidden" value="<?= $default_pricing ?>">
                                        <input type="hidden" id="company_type_hidden" value="1">
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: 30px">
                                    <button id="patientGeneralPricingBtn" title="General Pricing"
                                        onclick="getPricingDetalis('default_pricing_id')" type="button"
                                        class="btn bg-green btn-block"><i id="patientGeneralPricingSpin"
                                            class="fa fa-google"></i> </button>

                                </div>
                                <div class="col-md-2 padding_sm" style="margin-top: 30px">
                                    <button id="patientAllPricingDetailsBtn" onclick="getAllPricingDetails()"
                                        title="Pricing" type="button" class="btn bg-yellow-active btn-block"><i
                                            id="patientAllPricingDetailsSpin" class="fa fa-money"></i> Pricing
                                    </button>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-12 padding_sm" style="margin-top: 10px">
                                    <div class="col-md-4 padding_sm">
                                        <div class="mate-input-box">
                                            <label>Special Care</label>
                                            <div class="clearfix"></div>
                                            <select class="form-control select2" id="patient_vulnerable">
                                                <option value="">Select</option>
                                                <?php
                                                    foreach ($vulnerable as $each) {
                                                            ?>
                                                <option value="<?= $each->id ?>"><?= $each->name ?></option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="col-md-6 padding_sm">
                                            <button id="patientMoreDetailsBtn" onclick="getPatientMoreDetails()"
                                                type="button" class="btn bg-lime-active btn-block"><i
                                                    id="patientMoreDetailsSpin" class="fa fa-info"></i>
                                                More Details </button>
                                        </div>
                                        <div class="col-md-6 padding_sm">
                                            <button id="patientInsuranceBtn" onclick="getPatientInsurance()"
                                                type="button" class="btn bg-purple btn-block"><i
                                                    id="patientInsuranceSpin" class="fa fa-umbrella"></i>
                                                Insurance </button>
                                        </div>
                                        <div class="col-md-6 padding_sm">

                                            <button type="button" id="getDoctorChargesBtn"
                                                class="btn btn-primary btn-block" onclick="getDoctorChageSplitUp(0)"><i
                                                    id="getDoctorChargesSpin" class="fa fa-columns"></i>
                                                Charge Split Up </button>

                                        </div>
                                        @if (intval($enable_free_visit_in_registration) == 1)
                                            <div class="col-md-6" style="display: none" id="renewal_free_div">
                                                <div class="checkbox checkbox-warning inline no-margin">
                                                    <input onclick="changeIsFreeStatus()" style="margin-left: 10px;"
                                                        type="checkbox" name="is_reg_free" id="is_reg_free">
                                                    <label for="is_reg_free">Is Free</label>
                                                </div>
                                            </div>
                                        @endif
                                        @if ($dic_access_type == 1)
                                            <div class="col-md-6">
                                                <div class="checkbox checkbox-warning inline no-margin">
                                                    <input onclick="" style="margin-left: 10px;" type="checkbox"
                                                        name="dis_prov" id="dis_prov" onchange="checkProWantOrNot()">
                                                    <label for="dis_prov">Dis. Provision</label>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 padding_sm" style="padding: 0px;">
                                    <div class="col-md-12"
                                        style="margin-bottom:6px;padding:0px;margin-top: 0px;margin-left:0px;display:none;"
                                        id="discount_pro">
                                        <div class="col-md-12 padding_sm">

                                            <div class="box-body " style="min-height:72px;">
                                                <div class=" col-md-4"
                                                    style="margin-top:8px; padding-left: 2px !important; padding-right: 2px !important;">
                                                    <div class="mate-input-box">
                                                        <label for="discount_type">Discount Type</label>
                                                        <div class="clearfix"></div>
                                                        <select id="discount_type" class="form-control">
                                                            <option value="">Discount Type</option>
                                                            <option value="1">Percentage(%)</option>
                                                            <option value="2">Amount</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class=" col-md-4"
                                                    style="margin-top:8px; padding-left: 2px !important; padding-right: 2px !important;">
                                                    <div class="mate-input-box">
                                                        <label for="discount_type">Discount</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text"
                                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                            class="form-control number_class discount_value"
                                                            value="" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4"
                                                    style="margin-top:8px;padding-left: 2px !important; padding-right: 2px !important;">
                                                    <div class="mate-input-box">
                                                        <label for="discount_narration">Discount Narration</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" class="form-control discount_narration"
                                                            id="discount_narration" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <div class="mate-input-box" id="dis_amt"
                                                style="display: none;height: 40px !important;">
                                                <label for="">Dis. Amount</label>
                                                <div class="clearfix"></div>
                                                <input disabled type="text" class="form-control split_hidden"
                                                    id="split_hidden" value="" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm">
                                            <div class="mate-input-box" style="height: 40px !important;">
                                                <label for="">Total Charge</label>
                                                <div class="clearfix"></div>
                                                <label style="margin-top: 20px;margin-left: 15px;"
                                                    id="doctor_charges_hidden"></label>
                                                <input type="hidden" id="charges_hidden" value="">
                                                {{-- <input type="hidden" id="split_hidden" value=""> --}}
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-top: 18px">
                                            <button type="button" class="btn btn-warning btn-block"
                                                onclick="resetAppointmentForm(1,2,1)"><i class="fa fa-recycle"></i>
                                                Reset </button>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-top: 18px">
                                            <button type="button" id="createPatientRegistraionBtn"
                                                class="btn btn-success btn-block" onclick="createPatientRegistration()"><i
                                                    id="createPatientRegistraionSpin" class="fa fa-save"></i>
                                                Save </button>
                                            @if ($opPrescriptionStatus == 1)
                                                <div class="col-md-12 padding_sm" id="opPrescriptionStatus">
                                                    <div class="checkbox checkbox-warning inline no-margin">
                                                        <input onclick="" style="margin-left: 15px;" type="checkbox"
                                                            name="printOpPrescription" id="printOpPrescription">
                                                        <label for="printOpPrescription">OP PRESCRIPTION</label>
                                                    </div>

                                                </div>
                                            @endif
                                        </div>
                                        {{--
                                        <div class="col-md-2 padding_sm" style="margin-top: 18px">
                                            <button type="button" class="btn btn-warning btn-block"
                                                onclick="resetAppointmentForm(1,2,1)"><i class="fa fa-recycle"></i>
                                                Reset </button>
                                        </div>
                                        <div class="col-md-4 padding_sm" style="margin-top: 18px">
                                            <button type="button" id="createPatientRegistraionBtn"
                                                class="btn btn-success btn-block" onclick="createPatientRegistration()"><i
                                                    id="createPatientRegistraionSpin" class="fa fa-save"></i>
                                                Save </button>
                                               @if ($opPrescriptionStatus == 1)
                                                <div class="col-md-12 padding_sm" id="opPrescriptionStatus">
                                                    <div class="checkbox checkbox-warning inline no-margin">
                                                        <input onclick="" style="margin-left: 15px;" type="checkbox" name="printOpPrescription" id="printOpPrescription">
                                                        <label for="printOpPrescription">OP PRESCRIPTION</label>
                                                    </div>

                                                </div>
                                               @endif
                                        </div> --}}
                                    </div>
                                    <div class="clearfix"></div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!---------------------cash recive modal--------------------------->

    @include('Emr::pharmacy_bill_list.cash_receive_model')
@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/Dashboard/js/moment/moment.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/appointments.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/registration_renewal.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/cash_receive.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js') }}"></script>
@endsection
