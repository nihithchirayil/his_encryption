@if($insurance_div_count==1)
    @php
        $text_ins='Primary Insurance';
    @endphp
@elseif($insurance_div_count==2)
   @php
        $text_ins='Secondary Insurance';
   @endphp

@elseif($insurance_div_count==3)
   @php
        $text_ins='Tertitary Insurance';
   @endphp

@elseif($insurance_div_count==4)
    @php
        $text_ins='Quatrnary Insurance';
    @endphp

@elseif($insurance_div_count==5)
    @php
        $text_ins='Quinary Insurance';
    @endphp

@elseif($insurance_div_count==6)
  @php
        $text_ins='Senary Insurance';
  @endphp
@elseif($insurance_div_count==7)
@php
       $text_ins='Septenary Insurance';

@endphp
@elseif($insurance_div_count==8)
    @php
        $text_ins='Octonary Insurance';
    @endphp

@elseif($insurance_div_count==9)
    @php
        $text_ins='Nonary Insurance';
 
    @endphp
@elseif($insurance_div_count==10)
   @php
        $text_ins='Denary Insurance';
   @endphp
 
@else
   @php
        $text_ins=' '; 
   @endphp
@endif

<li class="nav-item" id="{{ $insurance_div_count }}_li">
    <a class="nav-link" id="{{ $insurance_div_count }}-tab" href="#{{ $insurance_div_count }}_tab"
     role="tab" aria-controls="{{ $insurance_div_count }}" style="padding:10px !important;"
        aria-selected="true"><strong>{{ $text_ins }} <i class="fa fa-times" onclick="removeThisDViv('{{ $insurance_div_count }}_li',{{ $insurance_div_count }})"></i></strong></a>
</li>