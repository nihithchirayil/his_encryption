   <ul id="progressbar" style="cursor: pointer;font-size: 11px;height: 39px;" class="nav nav-tabs" role="tablist" >
        @foreach ($patient_details as $data)
        @if($data->slno==1)
        @php
            $text_ins='Primary Insurance';
        @endphp
        @elseif($data->slno==2)
        @php
            $text_ins='Secondary Insurance';
        @endphp
        
        @elseif($data->slno==3)
        @php
            $text_ins='Tertitary Insurance';
        @endphp
        
        @elseif($data->slno==4)
        @php
            $text_ins='Quatrnary Insurance';
        @endphp
        
        @elseif($data->slno==5)
        @php
            $text_ins='Quinary Insurance';
        @endphp
        
        @elseif($data->slno==6)
        @php
            $text_ins='Senary Insurance';
        @endphp
        @elseif($data->slno==7)
        @php
           $text_ins='Septenary Insurance';
        
        @endphp
        @elseif($data->slno==8)
        @php
            $text_ins='Octonary Insurance';
        @endphp
        
        @elseif($data->slno==9)
        @php
            $text_ins='Nonary Insurance';
        
        @endphp
        @elseif($data->slno==10)
        @php
            $text_ins='Denary Insurance';
        @endphp
        
        @else
        @php
            $text_ins=' '; 
        @endphp
        @endif
         <li class="nav-item" id="{{ $data->slno }}_li">
            <a class="nav-link" id="{{ $data->slno }}-tab" href="#{{ $data->slno }}_tab"
             role="tab" aria-controls="{{ $data->slno }}" style="padding:10px !important;"
                aria-selected="true"><strong>{{ $text_ins }}</strong></a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content" id="details_tab_body" style="height: 315px;">
        @foreach ($patient_details as $data)
        <div class="tab-pane fade" style="margin-top:4px;" id="{{ $data->slno }}_tab" role="tabpanel" aria-labelledby="nav-{{$data->slno}}-tab">
            <div class="box no-border no-margin" id="{{ $data->slno }}_div">
                <input type="hidden" class="ins_detail_count" id="ins_detail_id{{ $data->slno }}" value="{{ $data->head_id }}">
                <div class="row">
                    <div class="col-md-12 paddding_sm">
                        <div class="col-md-12 padding_sm">
                            <div class="box no-border no-margin">
                                {{-- <label class="blue"> Primary Insurance</label> --}}
                                <div class="box-footer"
                                    style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 400px;">
                                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                        <div class="mate-input-box">
                                            <label class="red">Insurance Company</label>
                                            <div class="clearfix"></div>
                                            <select  class="form-control insurance_select" onchange="getInsurancePricing_ins({{ $data->slno }})"
                                                id="insurance_company{{ $data->slno }}">
                                                <option value="">Select</option>
                                                <?php
                                                    foreach ($credit_company as $each) {
                                                        $selected="";
                                                        if($data->insurance_company_id==$each->id){
                                                            $selected = "selected";
                                                        }
                                                        ?>
                                                <option <?= $selected ?> value="<?= $each->id ?>"><?= strtoupper($each->company_name) ?>
                                                </option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                        @php
                                            $sql = "select id,pricing_name from pricing_master where status=1 and company_id=$data->insurance_company_id order by pricing_name ";
                                            $result = \DB::select($sql);
                                        @endphp
                                        <div class="mate-input-box">
                                            <label class="red">Insurance Pricing</label>
                                            <div class="clearfix"></div>
                                            <input type="hidden" id="insurance_pricing_hidden" value="<?= @$data->insurance_pricing_id ? $data->insurance_pricing_id : 0  ?>">
                                            <select  class="form-control insurance_pricing" 
                                                id="insurance_pricing{{ $data->slno }}">
                                                <option value="">Select</option>
                                                <?php
                                                    foreach ($result as $each) {
                                                        $selected="";
                                                        if($data->insurance_pricing_id==$each->id){
                                                            $selected = "selected";
                                                        }
                                                        ?>
                                                <option <?= $selected ?> value="<?= $each->id ?>"><?= strtoupper($each->pricing_name) ?>
                                                </option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                        <div class="mate-input-box">
                                            <label>Authorisation Letter No</label>
                                            <div class="clearfix"></div>
                                            <input type="text"
                                                value="<?= @$data->authorisation_letter_no ? $data->authorisation_letter_no : '' ?>"
                                                class="form-control" id="primary_authorisation_letter_no{{ $data->slno }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                        <div class="mate-input-box">
                                            <label>Insurance ID No</label>
                                            <div class="clearfix"></div>
                                            <input type="text"
                                                value="<?= @$data->insurance_id_no ? $data->insurance_id_no : '' ?>"
                                                class="form-control" id="primary_insurance_id_no{{ $data->slno }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                        <div class="mate-input-box">
                                            <label>Credit Limit</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control"
                                                value="<?= @$data->credit_limit ? $data->credit_limit : '' ?>"
                                                id="primary_credit_limit{{ $data->slno }}"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                        <div class="mate-input-box">
                                            <label>Card Type</label>
                                            <div class="clearfix"></div>
                                            <select class="form-control insurance_select" id="primary_card_type{{ $data->slno }}">
                                                <option value="">Select</option>
                                                <?php
                                                $primary_card_type= @$data->card_type_id ? $data->card_type_id : '';
                                                foreach ($card_type as $key=>$val) {
                                                    $selected="";
                                                    if($primary_card_type==$key){
                                                        $selected = "selected";
                                                    }
                                                    ?>
                                                <option <?= $selected ?> value="<?= $key ?>">
                                                    <?= strtoupper($val) ?>
                                                </option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                        <div class="mate-input-box">
                                            <label>Color of Card</label>
                                            <div class="clearfix"></div>
                                            <input type="text"
                                                value="<?= @$data->color_of_card ? $data->color_of_card : '' ?>"
                                                class="form-control" id="primary_color_of_card{{ $data->slno }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                        <div class="mate-input-box">
                                            <label class="red">Expiry Date</label>
                                            <div class="clearfix"></div>
                                            <input type="text"
                                                value="<?= @$data->expiry_date ? date('M-d-Y',strtotime($data->expiry_date)) : '' ?>"
                                                class="form-control insurance_exp_datepicker" id="primary_expiry_date{{ $data->slno }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                        <div class="mate-input-box">
                                            <label>Issue Date</label>
                                            <div class="clearfix"></div>
                                            <input type="text"
                                                value="<?= @$data->issue_date ? date('M-d-Y',strtotime($data->issue_date)) : '' ?>"
                                                class="form-control insurance_issue_datepicker" id="primary_issue_date{{ $data->slno }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                        <div class="mate-input-box">
                                            <label class="red">Insurance Type</label>
                                            <div class="clearfix"></div>
                                              <select name="" data-type_id="{{ $data->slno }}" class="form-control insurance_type" id="insurance_type{{ $data->slno }}" class="form-control">
                                                <option @if($data->slno=='') selected @endif value="">Select</option>
                                                <option @if($data->slno==1) selected @endif value="1">Primary Insurance</option>
                                                <option @if($data->slno==2) selected @endif value="2">Secondary Insurance</option>
                                                <option @if($data->slno==3) selected @endif value="3">Tertitary Insurance</option>
                                                <option @if($data->slno==4) selected @endif value="4">Quatrnary Insurance</option>
                                                <option @if($data->slno==5) selected @endif value="5">Quinary Insurance</option>
                                                <option @if($data->slno==6) selected @endif value="6">Senary Insurance</option>
                                                <option @if($data->slno==7) selected @endif value="7">Septenary Insurance</option>
                                                <option @if($data->slno==8) selected @endif value="8">Octonary Insurance</option>
                                                <option @if($data->slno==9) selected @endif value="9">Nonary Insurance</option>
                                                <option @if($data->slno==10) selected @endif value="10">Denary Insurance</option>
                                              </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                        <div class="checkbox checkbox-success inline">
                                            <input  type="checkbox" id="insurance_status{{ $data->slno }}" name="checkbox"  @if($data->status==1) checked @else  @endif>
                                            <label  class="text-blue" for="insurance_status{{ $data->slno }}">
                                                Active
                                            </label>
                                        </div>
                                    </div>
                                   <div class="clearfix"></div>
                                   <h4 class="blue" style="box-shadow: 0px 2px 0px 0px rosybrown;">Principal Member</h4>
                                   <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                    <div class="mate-input-box">
                                        <label>UHID</label>
                                        <div class="clearfix"></div>
                                        <input type="text" 
                                        value="<?= @$data->pr_uhid ? $data->pr_uhid : '' ?>"
                                        class="form-control principal_search" id="principle_member_uhid{{$data->slno }}">
                                    <input type="hidden" id="principle_member_uhid{{$data->slno }}_hidden">
                                    <div id="principle_member_uhid{{$data->slno }}AjaxDiv" class="ajaxSearchBox"
                                    style="width: 100%;position:absolute;margin-top: 25px"></div>
                                    </div>
                                   </div>
                                   <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                    <div class="mate-input-box">
                                        <label>Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text"
                                            value="<?= @$data->dependent_patient_name ? $data->dependent_patient_name : '' ?>"
                                            class="form-control" id="principle_member_uhid{{$data->slno }}_name">
                                    </div>
                                   </div>
                                   <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                       <div class="mate-input-box">
                                           <label>Dependent Relation</label>
                                           <div class="clearfix"></div>
                                           <select  class="form-control primary_dependent_relation" 
                                            id="primary_dependent_relation{{ $data->slno }}">
                                            <option value="">Select</option>
                                            <?php
                                                foreach ($relationship as $each) {
                                                    $selected="";
                                                    if($data->dependent_relation_id==$each->id){
                                                        $selected = "selected";
                                                    }
                                                    ?>
                                            <option <?= $selected ?> value="<?= $each->id ?>"><?= strtoupper($each->relationship_title) ?>
                                            </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                         
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                       
                    </div>
                </div> 
            </div>
        </div>
          
        @endforeach
    </div>

