@php
    
@endphp
<div class="tab-pane fade active" style="margin-top:4px;" id="{{ $insurance_div_count }}_tab" role="tabpanel" aria-labelledby="nav-{{$insurance_div_count}}-tab">
    <div class="box no-border no-margin" id="{{ $insurance_div_count }}_div">
        <input type="hidden" class="ins_detail_count" id="ins_detail_id{{ $insurance_div_count }}" value="0">

        <div class="row">
            <div class="col-md-12 paddding_sm">
                <div class="col-md-12 padding_sm">
                    <div class="box no-border no-margin">
                        {{-- <label class="blue"> Primary Insurance</label> --}}
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 400px;">
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="mate-input-box">
                                    <label class="red">Insurance Company</label>
                                    <div class="clearfix"></div>
                                    <select  class="form-control insurance_select" onchange="getInsurancePricing_ins({{ $insurance_div_count }})"
                                        id="insurance_company{{ $insurance_div_count }}">
                                        <option value="">Select</option>
                                        <?php
                                            foreach ($credit_company as $each) {
                                               
                                                ?>
                                        <option  value="<?= $each->id ?>"><?= strtoupper($each->company_name) ?>
                                        </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="mate-input-box">
                                    <label class="red">Insurance Pricing</label>
                                    <div class="clearfix"></div>
                                    <input type="hidden" id="insurance_pricing_hidden" value="<?= @$pricing_id ? $pricing_id : 0  ?>">
                                    <select onchange=""
                                        class="form-control insurance_select" id="insurance_pricing{{ $insurance_div_count }}">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="mate-input-box">
                                    <label>Authorisation Letter No</label>
                                    <div class="clearfix"></div>
                                    <input type="text"
                                        value="<?= @$insurance_array_string[0]->primary_authorisation_letter_no ? $insurance_array_string[0]->primary_authorisation_letter_no : '' ?>"
                                        class="form-control" id="primary_authorisation_letter_no{{ $insurance_div_count }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="mate-input-box">
                                    <label>Insurance ID No</label>
                                    <div class="clearfix"></div>
                                    <input type="text"
                                        value="<?= @$insurance_array_string[0]->primary_insurance_id_no ? $insurance_array_string[0]->primary_insurance_id_no : '' ?>"
                                        class="form-control" id="primary_insurance_id_no{{ $insurance_div_count }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="mate-input-box">
                                    <label>Credit Limit</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control"
                                        value="<?= @$insurance_array_string[0]->primary_credit_limit ? $insurance_array_string[0]->primary_credit_limit : '' ?>"
                                        id="primary_credit_limit{{ $insurance_div_count }}"
                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="mate-input-box">
                                    <label>Card Type</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control insurance_select" id="primary_card_type{{ $insurance_div_count }}">
                                        <option value="">Select</option>
                                        <?php
                                        $primary_card_type= @$insurance_array_string[0]->primary_card_type ? $insurance_array_string[0]->primary_card_type : '';
                                        foreach ($card_type as $key=>$val) {
                                          
                                            ?>
                                        <option  value="<?= $key ?>">
                                            <?= strtoupper($val) ?>
                                        </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="mate-input-box">
                                    <label>Color of Card</label>
                                    <div class="clearfix"></div>
                                    <input type="text"
                                        value="<?= @$insurance_array_string[0]->primary_color_of_card ? $insurance_array_string[0]->primary_color_of_card : '' ?>"
                                        class="form-control" id="primary_color_of_card{{ $insurance_div_count }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="mate-input-box">
                                    <label class="red">Expiry Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text"
                                        value="<?= @$insurance_array_string[0]->primary_expiry_date ? $insurance_array_string[0]->primary_expiry_date : '' ?>"
                                        class="form-control insurance_exp_datepicker" id="primary_expiry_date{{ $insurance_div_count }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="mate-input-box">
                                    <label>Issue Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text"
                                        value="<?= @$insurance_array_string[0]->primary_issue_date ? $insurance_array_string[0]->primary_issue_date : '' ?>"
                                        class="form-control insurance_issue_datepicker" id="primary_issue_date{{ $insurance_div_count }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="mate-input-box">
                                    <label class="red">Insurance Type</label>
                                    <div class="clearfix"></div>
                                      <select name="" data-type_id="{{ $insurance_div_count }}" class="form-control insurance_type" id="insurance_type{{ $insurance_div_count }}" class="form-control">
                                        <option @if($insurance_div_count=='') selected @endif value="">Select</option>
                                        <option @if($insurance_div_count==1) selected @endif value="1">Primary Insurance</option>
                                        <option @if($insurance_div_count==2) selected @endif value="2">Secondary Insurance</option>
                                        <option @if($insurance_div_count==3) selected @endif value="3">Tertitary Insurance</option>
                                        <option @if($insurance_div_count==4) selected @endif value="4">Quatrnary Insurance</option>
                                        <option @if($insurance_div_count==5) selected @endif value="5">Quinary Insurance</option>
                                        <option @if($insurance_div_count==6) selected @endif value="6">Senary Insurance</option>
                                        <option @if($insurance_div_count==7) selected @endif value="7">Septenary Insurance</option>
                                        <option @if($insurance_div_count==8) selected @endif value="8">Octonary Insurance</option>
                                        <option @if($insurance_div_count==9) selected @endif value="9">Nonary Insurance</option>
                                        <option @if($insurance_div_count==10) selected @endif value="10">Denary Insurance</option>
                                      </select>
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 15px">
                                <div class="checkbox checkbox-success inline">
                                    <input type="checkbox" id="insurance_status{{ $insurance_div_count }}" name="checkbox">
                                    <label class="text-blue" for="insurance_status{{ $insurance_div_count }}">
                                        Active
                                    </label>
                                </div>
                            </div>
                           <div class="clearfix"></div>
                           <h4 class="blue" style="box-shadow: 0px 2px 0px 0px rosybrown;">Principal Member</h4>
                           <div class="col-md-3 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label>UHID</label>
                                <div class="clearfix"></div>
                                <input type="text" 
                                    value="<?= @$insurance_array_string[0]->pr_uhid ? $insurance_array_string[0]->pr_uhid : '' ?>"
                                    class="form-control principal_search" id="principle_member_uhid{{ $insurance_div_count }}">
                                <input type="hidden" id="principle_member_uhid{{ $insurance_div_count }}_hidden">
                                <div id="principle_member_uhid{{ $insurance_div_count }}AjaxDiv" class="ajaxSearchBox"
                                style="width: 100%;position:absolute;margin-top: 25px"></div>
                            </div>
                           </div>
                           <div class="col-md-3 padding_sm" style="margin-top: 15px">
                            <div class="mate-input-box">
                                <label>Name</label>
                                <div class="clearfix"></div>
                                <input type="text"
                                    value="<?= @$insurance_array_string[0]->pr_name ? $insurance_array_string[0]->pr_name : '' ?>"
                                    class="form-control" id="principle_member_uhid{{ $insurance_div_count }}_name"">
                            </div>
                           </div>
                           <div class="col-md-3 padding_sm" style="margin-top: 15px">
                               <div class="mate-input-box">
                                   <label>Dependent Relation</label>
                                   <div class="clearfix"></div>
                                   <select  class="form-control primary_dependent_relation" 
                                   id="primary_dependent_relation{{ $insurance_div_count }}">
                                   <option value="">Select</option>
                                   <?php
                                       foreach ($relationship as $each) {
                                           ?>
                                   <option  value="<?= $each->id ?>"><?= strtoupper($each->relationship_title) ?>
                                   </option>
                                   <?php
                                       }
                                   ?>
                               </select>
                                 
                               </div>
                           </div>
                        </div>
                    </div>
                </div>
        
               
            </div>
        </div> 
    </div>
</div>
