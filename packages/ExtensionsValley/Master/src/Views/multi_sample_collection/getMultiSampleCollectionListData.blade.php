<style>
    .popupDiv {
        position: absolute;
    }

    .list-group-item:first-child {
        border-top-left-radius: 0px !important;
        border-top-right-radius: 0px !important;

    }

    .list-group-item {
        padding: 10px 12px !important;
    }

    .popDiv {
        display: none;
        /* background: white; */
        border-radius: 2px;
        /* box-shadow: 0px 0px 4px #d0d0d0; */
        padding: 9px;
        position: absolute;
        top: -25px;
        min-width: 162px;
        z-index: 850 !important;
        width: 203px;
        min-height: 104px;
        left: 31px;
    }

    .popDiv .show {
        display: block;
    }

    .list-group-item {
        background-color: #ffffff !important;
    }

    button.close {
        margin: 1px -6px !important;
    }

    .list-group-item {
        padding: 2px 8px !important;
    }

    .list-group-item:hover {
        background-color: whitesmoke !important;
    }

    .pop_closebtn {
        background: #333 none repeat scroll 0 0;
        border-radius: 50%;
        box-shadow: 0 0 5px #a2a2a2;
        color: #fff;
        cursor: pointer;
        font-size: 12px;
        font-weight: bold;
        height: 20px;
        padding: 2px 6px;
        position: absolute;
        right: -10px;
        top: -4px;
        min-width: 20px;
    }

    .search_header {
        background: #36A693 !important;
        color: #FFF !important;
    }

    .close {
        opacity: 1 !important;
        color: black !important;
    }

    .ps-scrollbar-x-rail {
        left: 11px !important;
    }

    .pagination {
        margin: 0px !important;
    }

    .purple_pagination {
        margin: 0px !important;

    }
</style>

<div class="col-md-12 padding_sm">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
        style="font-size: 12px;margin-top:5px;cursor: pointer;">
        <thead>
            <tr class="table_header_common"
                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">

                <th width="2%"> <i class="fa fa-list"></i></th>
                <th width="2%">Sl.No.</th>
                <th width="10%">Patient Name</th>
                <th width="10%">UHID</th>
                <th width="10%">Sample No.</th>
                <th width="10%">Sample</th>
                <th width="10%">Visit Type</th>
                <th width="10%">Sample Collected By</th>
                <th width="10%">Sample Collected Date</th>
                <th width="10%">Acknowledge By</th>
                <th width="10%">Acknowledge Date</th>
               
            </tr>
        </thead>
        <tbody>
            @if (count($res) > 0)

                @foreach ($res as $data)
                @php
                        $color = ' ';
                        $limit_statge = 0;
                        $disabled_reason=' ';
                        $payment_mode=' ';
                        if ($data->payment_type =='cash/Card' && $data->paid_status == 0) {
                            $limit_stage = 1;
                        } else {
                            $limit_stage = 0;
                        }
                        if ($data->payment_type =='cash/Card' && $data->paid_status == 0 && ($data->status == 3 || $data->status == 5 || $data->status == 6)) {
                            $disabled_reason='Sample is unpaid and mode of payment is';
                        } else {
                            $disabled_reason=' ';
                        }
                        if ($data->payment_type == 'cash/Card') {
                            $color = 'bk_blue';
                            $payment_mode='Cash/Card';
                        } elseif ($data->payment_type == 'ipcredit') {
                            $color = 'bg-lime-active';
                            $payment_mode='Credit';
                        } elseif ($data->payment_type == 'credit') {
                            $color = 'bg-yellow-active';
                            $payment_mode="Company Credit";
                        } elseif ($data->payment_type == 'insurance') {
                            $color = 'bg-pink-active';
                            $payment_mode='Insurance';
                        }
                        $list_class = 'fa-bars';
                        $paid_color='paid-bg';
                        if ($data->paid_status == 0) {
                            $list_class = 'fa-list-ul';
                            $paid_color='unpaid-bg';
                        }

                    @endphp
                    <tr class="{{ strtolower(str_replace(' ', '_', $data->status_desc)) . '_text' }}"
                        >
                        <td>
                            <button type="button"  class="btn {{ $color }}"
                            title="{{ $disabled_reason }}{{ $payment_mode }}"
                                onclick="displayDetails({{ $data->lab_sample_detailsid }},{{ $data->status_value }})"><i class="fa {{ $list_class }}"></i>
                            </button>
                            <div class="popupDiv">
                                <div class="popupDiv">
                                    <div class="popDiv" id="popup{{ $data->lab_sample_detailsid }}" style="display: none;">
                                        <div class="col-md-1 pull-right"
                                            style="border-radius: 30%; width: 20px; height: 18px; background: #000000; top: -28px;">
                                            <button type="button" class="close"
                                                id="ppclose{{ $data->lab_sample_detailsid }}" data-dismiss="modal"
                                                style="font-size: 16px;" aria-label="Close">
                                                <i class="fa fa-times red"></i>
                                            </button>
                                        </div>
                                        <div class="col-md-12" style="margin-top: -28px;margin-left: -15px">
                                            <ul class="list-group" style="cursor: pointer">
                                                @if ($data->status != 6)
                                                    <li class="list-group-item" id="cancel{{ $data->lab_sample_detailsid }}"
                                                        onclick="displayCancelStatus()">

                                                        <b>De-collect</b>
                                                    </li>
                                                @endif
                                                @if ($data->status != 0)
                                                    <li class="list-group-item" id="cancel{{ $data->lab_sample_detailsid }}"
                                                        onclick="resultEntry({{ $data->lab_sample_detailsid }},{{ $data->status_value }},{{ $limit_stage }},1)">
                                                        <b>Result Entry </b>
                                                    </li>
                                                @endif
                                                @if($data->status == 6)
                                                <li class="list-group-item print_result">
                                                    <b >Print</b>
                                                </li>
                                                @endif
                                                @if($data->status != 1 && $data->status != 0)
                                                <li class="list-group-item viewconfirmsample">
                                                    <b >View</b>
                                                </li>
                                                @endif
                                                


                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </td>
                        <td class="td_common_numeric_rules  cancel_bg{{ $data->lab_sample_detailsid }} {{ $paid_color }}"
                            id="paid_bg{{ $data->lab_sample_detailsid }}">
                            {{ ($res->currentPage() - 1) * $res->perPage() + $loop->iteration }}
                        </td>
                        <td id="patientname{{ $data->lab_sample_detailsid }}"
                            class="common_td_rules  cancel_bg{{ $data->lab_sample_detailsid }}"
                            title="{{ $data->title }}{{ $data->patient_name }}">
                            {{ $data->title }}{{ $data->patient_name }}</td>
                        <td id="patientuhid{{ $data->lab_sample_detailsid }}"
                            class="common_td_rules  cancel_bg{{ $data->lab_sample_detailsid }}" id=""
                            title="{{ $data->uhid }}">{{ $data->uhid }}</td>

                        <td class="common_td_rules  cancel_bg{{ $data->lab_sample_detailsid }}"
                            title="{{ $data->sample_no }}">{{ $data->sample_no }}</td>
                        <td class="common_td_rules  cancel_bg{{ $data->lab_sample_detailsid }}" title="{{ $data->sample_name }}">
                            {{ $data->sample_name }}</td>

                        <td class="common_td_rules  cancel_bg{{ $data->lab_sample_detailsid }}"
                            title="{{ $data->visit_status }}">{{ $data->visit_status }}</td>
                        <td class="common_td_rules  cancel_bg{{ $data->lab_sample_detailsid }}"
                            title="{{ $data->samplecollectedusername }}">{{ $data->samplecollectedusername }}</td>
                     
                        <td class="common_td_rules  cancel_bg{{ $data->lab_sample_detailsid }}" title="{{ date('M-d-Y h:i A', strtotime($data->sample_collection_date)) }}">
                                {{ date('M-d-Y h:i A', strtotime($data->sample_collection_date)) }}</td>
                        <td class="common_td_rules  cancel_bg{{ $data->lab_sample_detailsid }}"
                            title=" {{ $data->acknowledgedusername }}">{{ $data->acknowledgedusername }}</td>
                        <td class="common_td_rules  cancel_bg{{ $data->lab_sample_detailsid }}" title="{{ @$data->sample_acknowledged_date ? date('M-d-Y h:i A', strtotime($data->sample_acknowledged_date)) : ' ' }}">
                            {{ @$data->sample_acknowledged_date ? date('M-d-Y h:i A', strtotime($data->sample_acknowledged_date)) : ' ' }}</td>
                       

                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="18">No Record Match</td>
                </tr>
            @endif

        </tbody>
    </table>
</div>
<div class="col-md-12">
    <div class="clearfix"></div>
    <div class="col-md-12 text-right">
        <ul class="pagination purple_pagination" style="text-align:right !important;">
            {!! $page_links !!}
        </ul>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(".page-link").click(function(e) {
            e.preventDefault();
            var url = $(this).attr("href");
            console.log(url);
            if (url != undefined) {
                var paid_bill = $('#paid_bill').is(":checked");
                var Unpaid_bill = $('#Unpaid_bill').is(":checked");
                var cash = $('#cash').is(":checked");
                var credit = $('#credit').is(":checked");
                var insurance = $('#insurance').is(":checked");
                var credit_company = $('#credit_company').is(":checked");
                var bill_no = $('#bill_no').val();
                var test = $('#test').val();
                var department = $('#department').val();
                var sub_department = $('#sub_department').val();
                var report_type = $('#report_type').val();
                var sample = $('#sample').val();
                var sample_status = $('#sample_status').val();
                var sample_no = $('#sample_no').val();
                var visit_type = $('#visit_type').val();
                var patient = $('#patient_hidden').val();
                var uhid = $('#uhid').val();
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var finalize_array_string = JSON.stringify(finalize_array);
                var params = {
                    credit_company: credit_company,
                    cash: cash,
                    credit: credit,
                    incurance: insurance,
                    test: test,
                    department: department,
                    Unpaid_bill: Unpaid_bill,
                    paid_bill: paid_bill,
                    sub_department: sub_department,
                    bill_no: bill_no,
                    report_type: report_type,
                    sample: sample,
                    sample_status: sample_status,
                    sample_no: sample_no,
                    visit_type: visit_type,
                    patient: patient,
                    uhid: uhid,
                    from_date: from_date,
                    to_date: to_date,
                    finalize_array_string: finalize_array_string,
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: params,
                    beforeSend: function() {
                        $('#list_container').html(' ');
                        $('#list_container').LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#009869'
                        });
                        $('#search_sample').attr('disabled', true);
                        $("#sample_spin").removeClass("fa fa-search");
                        $("#sample_spin").addClass("fa fa-spinner fa-spin");

                    },
                    success: function(data) {
                        if (data) {
                            $('#list_container').html(data);
                            $('.theadscroll').perfectScrollbar({
                                wheelPropagation: true,
                                minScrollbarLength: 30
                            });
                        }
                    },
                    complete: function() {
                        $('#search_sample').attr('disabled', false);
                        $('#list_container').LoadingOverlay("hide");
                        $("#sample_spin").removeClass("fa fa-spinner fa-spin");
                        $("#sample_spin").addClass("fa fa-search");
                        checkFinazlePatient(finalize_patient);
                    },
                    error: function() {
                        toastr.error("Error Please Check Your Internet Connection");
                    },
                });
            }
            return false;
        });
    });
</script>
