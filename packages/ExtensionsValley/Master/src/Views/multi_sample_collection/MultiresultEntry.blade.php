
<div class="box no-border no-margin">
    <div class="box-body" style="padding:5px !important;background-color: aliceblue !important;">
        <div class="row">
            <div class="col-md-12 padding_sm">
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        UHID
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <p id="result_entrypatient_uhid"><?= @$head_array[0]->uhid ? $head_array[0]->uhid : '-' ?></p>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        visit Type
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->current_visit_type ? $head_array[0]->current_visit_type : '-' ?>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Sample
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->samplename ? $head_array[0]->samplename : '-' ?>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Collected By
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->sample_collected_by ? $head_array[0]->sample_collected_by : '-' ?>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Patient Name
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->patient_name ? $head_array[0]->patient_name : '-' ?>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Email
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <input type="text" autocomplete="off"
                            value="<?= @$head_array[0]->email ? $head_array[0]->email : '' ?>" class="form-control"
                            placeholder="Email" id="patientemail">
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Sample Number
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->sample_no ? $head_array[0]->sample_no : '-' ?>
                        <input type="hidden" id="nummbericsample_no_hidden"
                            value="<?= @$head_array[0]->sample_no ? $head_array[0]->sample_no : 0 ?>">
                        <input type="hidden" id="nummbericdoctor_id_hidden"
                            value="<?= @$head_array[0]->doctor_id ? $head_array[0]->doctor_id : 0 ?>">
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Age/Gender
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= (@$head_array[0]->gender ? $head_array[0]->gender : '-'). ' / ' .  (@$head_array[0]->year ? $head_array[0]->year : '-') . ' Y' ?>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Mobile
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->phone ? $head_array[0]->phone : '-' ?>
                    </div>
                </div>
                <div class="col-md-4 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-4 bold no-margin">
                        Collected At
                    </div>
                    <div class="col-md-1 no-margin" style="margin-left:-9px !important">
                        :
                    </div>

                    <div class="col-md-7 no-margin" style="margin-left:-10px !important">
                        <?= @$head_array[0]->sample_collection_date ? $head_array[0]->sample_collection_date : '-' ?>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="box no-border no-margin">
    <div class="col-md-12 " style="padding: 6px;margin-top: 2px;width: 101%;margin-left: -5px;">
        <div class="row">
            <div class="col-md-12 padding_sm">
                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label class="">Phlebotomist Comments</label>
                        <input type="text" autocomplete="off" class="form-control sample_detail" disabled
                            id="phlebotomist_comments">
                    </div>
                </div>
                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label class="">Remarks</label>
                        <input type="text" autocomplete="off" class="form-control sample_detail" id="lab_remarks"
                            value="{{ @$details_array[0][0]->remarks ? $details_array[0][0]->remarks : ' ' }}">
                    </div>
                </div>
                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label class="">Notes</label>
                        <input type="text" autocomplete="off" class="form-control sample_detail" id="lab_notes"
                            value="{{ @$details_array[0][0]->notes ? $details_array[0][0]->notes : ' ' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12" style="padding: 0px;margin-top: -8px;width: 101%;margin-left: -6px;">
    <div class="box no-border">
        <div class="box-body clearfix">
            <div class="theadscroll" style="position: relative; height: 295px;margin-top: 10px">
                <table class='table table-condensed theadfix_wrapper table_sm table-col-bordered'
                    style="font-size: 12px;width=100%">
                    <thead>
                        <tr class="common_table_header" style="background-color:#01987a !important">
                            <th width='5%'>SL.No.</th>
                            <th class="common_td_rules" width='20%'>Sub Test</th>
                            <th class="common_td_rules" width='10%'>Description</th>
                            <th class="common_td_rules" width='10%'>Result</th>
                            <th class="common_td_rules" width='5%'><i class="fa fa-list"></i></th>
                            <th class="common_td_rules" width='10%'>Methodology</th>
                            <th class="common_td_rules" width='10%'>Unit</th>
                            <th class="common_td_rules" width='10%'>Normal Value</th>
                            <th class="common_td_rules" width='20%'>Comments</th>
                        </tr>
                    </thead>
                    <tbody id="getSampleResultDataList">
                        <?php
                       
                        if(count($details_array)!=0){
                            $service_id='';
                            $i=1;
                           
                            foreach ($details_array as $row) {
                               foreach($row as $each){
                                $list_string='';
                                if($each->actual_result){
                                    if(floatval($each->min_value) > floatval($each->actual_result)){
                                        $list_string="fa fa-arrow-down red";
                                    }else if(floatval($each->max_value) < floatval($each->actual_result)){
                                        $list_string="fa fa-arrow-up red";
                                    }
                                }
                                if($service_id !=$each->service_id){
                                    $service_id=$each->service_id;
                                    $i=1;
                                    ?>
                        <th colspan="9" class="" style="background-color: #01987a !important;color:white !important">
                            
                                <?= $each->service_desc ?>
                        </th>
                        <?php
                                }
                                ?>

                        <tr data-service_id={{ $each->service_id  }} >
                            <td class="common_td_rules">{{ $i }}
                                <input type="hidden" name="resultDetailID"
                                    value="<?= $each->resultdetailid ? $each->resultdetailid : 0 ?>">
                                <input type="hidden" name="bill_detail_id"
                                    value="<?= $each->bill_detail_id ? $each->bill_detail_id : 0 ?>">
                                <input type="hidden" id="samplestatus_hidden" value="<?= $status_value ?>">
                            </td>
                            <td class="common_td_rules" title="{{ $each->service_sub_description }}">
                                {{ @$each->service_sub_description ? $each->service_sub_description  : '' }}
                            </td>
                            <td class="common_td_rules" title="{{  $each->detail_description }}">{{ $each->detail_description }}</td>
                            @if($each->report_type!='F')
                            <td class="common_td_rules">
                                <input type="text" autocomplete="off"
                                    value="<?= @$each->actual_result ? $each->actual_result : '' ?>"
                                    class="form-control sample_detail actual_result" name="actual_result" id="">
                            </td>
                            @else
                            <td class="common_td_rules" style="position: absolute !important;">
                                <input style="border: 1px solid green;" class="form-control hidden_search_pro reset"  value='<?= @$each->actual_result ? (string)$each->actual_result : '' ?>'
                                 autocomplete="off" type="text" placeholder="Search..." 
                                id="actual_result{{  @$each->service_id }}" name="actual_result" />
                                
                            <div id="actual_result{{  @$each->service_id }}AjaxDiv" class="ajaxSearchBox" style="text-align: left;list-style: none;cursor: pointer;min-height: 134px;margin-right: 0px;margin-bottom: 0px;margin-left: 0px;width: 113% !important;z-index: 9999;position: absolute;background: rgb(255, 255, 255);border-radius: 3px;margin-top: -29px !important;position: relative;">
                                <input type="hidden" name="hidden_service" id="hidden_service" value="{{ $each->service_id  }}">
                            </div>
                           
                            </td>
                            @endif
                            <td class="common_td_rules"><i class='<?= $list_string ?>'></i></td>
                            <td class="common_td_rules">
                                <select name="methodology" class="form-control ">
                                    <option value="All">Select</option>
                                    @foreach ($methodology as $data)
                                        @php $selected=''; @endphp
                                        @if ($each->methodology == $data->id)
                                            @php $selected='selected'; @endphp
                                        @endif
                                        <option <?= $selected ?> value="{{ $data->id }}">{{ $data->methodology }}
                                        </option>
                                    @endforeach
                                </select>

                            </td>
                            <td class="common_td_rules">{{ $each->unitdesc }}</td>
                            <td class="common_td_rules">{{ $each->normalrange }}</td>
                            <td class="common_td_rules">
                                <input type="text" autocomplete="off"
                                    value="<?= @$each->detail_comments ? $each->detail_comments : '' ?>"
                                    class="form-control detail_comments" name="detail_comments" id="">

                            </td>
                            <td class="common_td_rules"> <input type="text" autocomplete="off"
                                    value="<?= @$each->detailslno ? $each->detailslno : '' ?>"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                    class="form-control detail_order" name="detail_order" id=""
                                    style="display: none"></td>
                        </tr>
                        <?php
                     $i++;
                               }
                          
                            }
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var input_id_key='';
    $('.hidden_search_pro').keyup(function(event) {
    var input_id = '';
    var set_service_id = $(this).closest('tr').attr("data-service_id");

    
    console.log(set_service_id);
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();
        input_id_key=input_id;

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = base_url + "/sampleCollection/setMultiAjaxSearch";
            $.ajax({
                type: "GET",
                url: url,
                data: 'set_service_id=' + set_service_id + '&search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function() {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function(html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function() {
                    //  $('#loading_image').hide();
                },
                error: function() {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});
function htmlDecode(input) {
    var e = document.createElement('textarea');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}
function fillSearchDetials(id, name, serach_key_id) {
    name=htmlDecode(name);
    $('#' + serach_key_id + '_hidden').val(id);
    $('#' + serach_key_id).val(name);
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
}

/* setting for enter key press in ajaxDiv listing */
$(".hidden_search_pro").on('keydown', function(event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});
</script>