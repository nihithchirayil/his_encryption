@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<div id="view_document_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:93%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="document_data" style="height:550px">

                <iframe src="" width="100%" height="100%" id="myframe"></iframe>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

</div>
<div class="modal bs-example-modal-md mkModalFloat" id="manageDocs" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">MANAGE DOCUMENT</h4>
            </div>

            <div class="modal-body">

                <div class="row">
                    @php
                        $document_types = \DB::table('document_types')
                            ->WhereNull('deleted_at')
                            ->Where('status', 1)
                            ->OrderBy('name', 'DESC')
                            ->pluck('name', 'id');
                    @endphp

                    {!! Form::open(['id' => 'manageDocsForm', 'name' => 'manageDocsForm', 'method' => 'post', 'file' => 'true', 'enctype' => 'multipart/form-data']) !!}
                    @php $i = 1; @endphp
                    <div class="col-sm-12 no-padding">

                        <div class="col-md-3 padding_sm"></div>

                        <div class="col-md-2 padding_sm"></div>

                        <div class="col-md-4 padding_sm"></div>

                        <div class="col-md-2 padding_xs visit-action-button">
                            <!-- <button class="btn btn-success btn-block" id="doc_file_upload" type="submit">
                            <i class="fa fa-upload" ></i> Upload
                        </button> -->
                        </div>

                        <!-- <div class="col-md-1 padding_xs visit-action-button">
                        <button onclick="add_new_row_files()" class="btn btn-primary btn-block">
                        <i class="fa fa-plus"></i> Add
                        </button>
                    </div> -->

                    </div>

                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <div class="doc-type-hidden" style="display: none;">
                        {!! Form::select('document_type_hidden', $document_types, null, ['class' => 'form-control', 'placeholder' => 'Select Type']) !!}
                    </div>

                    <div class="remove_00 col-sm-12 no-padding visit-action-button">
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                            {!! Form::select('document_type[]', $document_types, old('document_type'), ['class' => 'form-control document_type_upload', 'placeholder' => 'Select Type',]) !!}
                            </div>
                        </div>

                        <!-- <div class="col-md-2 padding_sm">
                        <input type="text" name="title[]" class="form-control" id="title" placeholder="Title">
                    </div>  -->

                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label>Description</label>
                                <div class="clearfix"></div>
                            <input type="text" name="description[]" class="form-control description_upload"
                                id="description" placeholder="Description">
                            </div>
                        </div>

                        <div class="col-md-3 padding_sm">
                            @php $f = $i - 1; @endphp
                            <input name="file_upload[]" type="file" id="f_upload"
                                class="inputfile inputfile-1 hidden document_upload" data-show-upload="false"
                                data-multiple-caption="{count} files selected" />
                            <label class="no-margin btn btn-block custom-uploadfile_label" for="f_upload">
                                <i class="fa fa-folder"></i>
                                <span id="uploadspan_documentsdocumenttab">
                                    Choose a file ...
                                </span>
                            </label>
                            <a class="uploadedImg" style="display:none;" href="" target="_blank"> </a>
                            <span style="color: #d14;">@if ($errors->first('f_upload.' . $f) != '') {{ 'Upload only doc,docx,pdf,jpeg,jpg,png,gif,bmp and File size < 10MB' }} @endif</span>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <button class="btn btn-success btn-block save_class" id="doc_file_upload" type="submit">
                                <i class="fa fa-upload" id="upload_file_doc"></i> Upload
                            </button>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <button type="button" class="btn btn-danger btn-block" onclick="clear_upload_file();"><i
                                    class="fa fa-trash-o"></i> Clear</button>
                        </div>
                        <!-- <div class="col-md-2 padding_sm">
                        <button type="button" class="btn btn-danger btn-block" onclick="remove_upload_file('default');"><i class="fa fa-trash-o"></i> Clear</button>
                    </div> -->
                    </div>

                    <div class="clearfix"></div>
                    <div class="ht5"></div>

                    <div class="col-md-12 no-padding">
                        <div class="theadscroll sm_margin_bottom" id="roww" style="max-height: 160px; height: auto;">
                        </div>
                    </div>

                    @php $i++; @endphp

                    {!! Form::token() !!}
                    {!! Form::close() !!}

                </div>

                <hr>

                <div class="row" id="document-data-list">
                    @include('Master::patient_document.ajax_document_list')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="visit_id" value="">
    <input type="hidden" id="patient_id" value="">
    <input type="hidden" id="encounter_id" value="">
    <input type="hidden" id="doctor_id" value="">
    <div class="right_col" style="min-height: 700px !important;">
        <div class="container-fluid">
            <table class="table table-contensed table_sm" style="margin:0px;">
                <thead>
                    <tr class="table_header_bg">
                        <th colspan="11"> {{ $title }}

                        </th>
                    </tr>
                </thead>
            </table>
                <div class="row padding_sm">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-2 padding_sm" style="margin-top:5px">
                            <div class="mate-input-box">
                                <label>UHID</label>
                                <div class="clearfix"></div>
                                <input class="form-control hidden_search" value="" autocomplete="off" type="text"  autocomplete="off" id="op_no" name="op_no" />
                        <div id="op_noAjaxDiv" class="ajaxSearchBox" style="margin-top: -10px"></div>
                        <input class="filters" type="hidden" name="op_no_hidden" value="" id="op_no_hidden">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top:5px">
                                <div class="mate-input-box">
                                    <label>Patient Name</label>
                                    <div class="clearfix"></div>
                                    <input class="form-control hidden_search" value="" autocomplete="off" type="text"  autocomplete="off" id="patient" name="patient_uhid" />
                            <div id="patientAjaxDiv" class="ajaxSearchBox" style="margin-top: -10px"></div>
                            <input class="filters" type="hidden" name="patient_hidden" value="" id="patient_hidden">
                                </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top:5px">
                            <div class="mate-input-box">
                                <label>IP No.</label>
                                <div class="clearfix"></div>
                                <input class="form-control hidden_search" value="" autocomplete="off" type="text"  autocomplete="off" id="ip_no" name="ip_no" />
                        <div id="ip_noAjaxDiv" class="ajaxSearchBox" style="margin-top: -10px"></div>
                        <input class="filters" type="hidden" name="ip_no_hidden" value="" id="ip_no_hidden">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top:5px">
                            <div class="mate-input-box">
                                <label>Visit Type</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" id="searchpatient_type">
                                    <option value="All">Select</option>
                                    <option value="IP">IP</option>
                                    <option value="OP">OP</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top:5px">
                            <div class="mate-input-box">
                                <label>Mobile No.</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_mobile" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top:5px">
                            <div class="mate-input-box">
                                <label>Email</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_email">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-2 padding_sm" style="margin-top:5px">
                            <div class="mate-input-box">
                                <label>Address</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_address">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top:5px">
                            <div class="mate-input-box">
                                <label>Area</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_area">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top:5px">
                            <div class="mate-input-box">
                                <label>PinCode</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_pincode">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top:5px">
                            <div class="mate-input-box">
                                <label>Country</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_country">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top:5px">
                            <div class="mate-input-box">
                                <label>State</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_state">
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 25px">
                            <button type="button" class="btn btn-warning btn-block"
                                onclick="resetAdvancePatientSearch()">Reset <i class="fa fa-recycle"></i>
                            </button>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 25px">
                            <button type="button" class="btn btn-primary btn-block advancePatientSearchBtn"
                                onclick="advancePatientSearch()">Search <i
                                    class="fa fa-search advancePatientSearchSpin"></i>
                            </button>
                        </div>
                    </div>

                    <div class="col-md-12 padding_sm" id="getPatientListModelDiv" style="margin-top: 15px;">

                    </div>

                </div>

        </div>
    </div>


@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/Dashboard/js/moment/moment.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/patient-documentUpload.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/patient_document.js') }}"></script>
@endsection
