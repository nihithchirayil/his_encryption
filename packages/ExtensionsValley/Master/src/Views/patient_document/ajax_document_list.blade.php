
<table class="table no-margin table-striped table-col-bordered table_sm table-condensed" style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_bg">
            <th width="12%">Date</th>
            <th>Type</th>
            <th>Description</th>
            <th width="5%">Preview</th>
            <th width="5%">Delete</th>
        </tr>
    </thead>
    <tbody>
    @if(isset($docList) && count($docList) > 0)
        @foreach($docList as $ind => $data)
        @php
            $created_at = (!empty($data->created_at)) ? date('d-M-Y', strtotime($data->created_at)) : '';
        @endphp
          <tr>
            <td class="common_td_rules">{{$created_at}}</td>
            <td class="common_td_rules">{{$data->type_name}}</td>
            <td class="common_td_rules">{{$data->description}}</td>
            <td class="common_td_rules"><a style="cursor: poiner;" download href="{{ URL::to('/').'/packages/extensionsvalley/emr/uploads/'.$data->patient_id.'/'.$data->filename }}"><i class="fa fa-download"></i></a></td>
            <td class="common_td_rules"><a style="cursor: poiner;" onclick="docImgDelete('{{$data->id}}',this)"><i id="spin_trash{{ $data->id }}" class="fa fa-trash"></i></a></td>
          </tr>
        @endforeach
    @endif
    </tbody>
</table>
<div class="clearfix"></div>
<div class="col-md-12 text-right purple_pagination document_pagination">
    @if(isset($docList) && count($docList) > 0)
    <?php echo $docList->render(); ?>
    @endif
</div>
