<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var patient_name = $('#patient').val();
            var uhid = $('#op_no').val();
            var ipno = $('#ip_no').val();
            var type = $('#searchpatient_type').val();
            var mobile = $('#searchpatient_mobile').val();
            var email = $('#searchpatient_email').val();
            var address = $('#searchpatient_address').val();
            var area = $('#searchpatient_area').val();
            var pincode = $('#searchpatient_pincode').val();
            var country = $('#searchpatient_country').val();
            var state = $('#searchpatient_state').val();
            var param = {
                _token: token,
                patient_name: patient_name,
                uhid: uhid,
                area: area,
                country: country,
                state: state,
                ipno: ipno,
                type: type,
                email: email,
                mobile: mobile,
                address: address,
                pincode: pincode
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $("#getPatientListModelDiv").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                    $('.advancePatientSearchBtn').attr('disabled', true);
                    $('.advancePatientSearchSpin').removeClass('fa fa-search');
                    $('.advancePatientSearchSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#getPatientListModelDiv').html(data);
                },
                complete: function() {
                    $("#getPatientListModelDiv").LoadingOverlay("hide");
                    $('.advancePatientSearchBtn').attr('disabled', false);
                    $('.advancePatientSearchSpin').removeClass('fa fa-spinner fa-spin');
                    $('.advancePatientSearchSpin').addClass('fa fa-search');
                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                }
            });
            return false;
        });

    });
</script>
<table class="table no-margin table_sm table-striped no-border styled-table" style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_bg">
            <th class="common_td_rules" width="10%">UHID</th>
            <th class="common_td_rules" width="10%">Patient Name</th>
            <th class="common_td_rules" width="10%">IP No.</th>
            <th class="common_td_rules" width="5%">Type</th>
            <th class="common_td_rules" width="10%">Mobile No.</th>
            <th class="common_td_rules" width="10%">Email</th>
            <th class="common_td_rules" width="15%">Address</th>
            <th class="common_td_rules" width="10%">Area</th>
            <th class="common_td_rules" width="5%">PinCode</th>
            <th class="common_td_rules" width="5%">Country</th>
            <th class="common_td_rules" width="5%">State</th>
            <th  style="text-align: center" width="5%"><i class="fa fa-file"></i></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if (count($patient_list) != 0) {
                foreach ($patient_list as $list) {
                    ?>
        <tr style="cursor: pointer" onclick="selectPatient(1,<?= $list->patient_id ?>)">
            <td class="common_td_rules">{{ $list->uhid ? $list->uhid : '-' }}</td>
            <td class="common_td_rules">{{ $list->patient_name ? $list->patient_name : '-' }}</td>
            <td class="common_td_rules">{{ $list->admission_no ? $list->admission_no : '-' }}</td>
            <td class="common_td_rules">{{ $list->current_visit_type }}</td>
            <td class="common_td_rules">{{ $list->phone ? $list->phone : '-' }}</td>
            <td class="common_td_rules">{{ $list->email ? $list->email : '-' }}</td>
            <td class="common_td_rules">{{ $list->address ? $list->address : '-' }}</td>
            <td class="common_td_rules">{{ $list->area ? $list->area : '-' }}</td>
            <td class="common_td_rules">{{ $list->pincode ? $list->pincode : '-' }}</td>
            <td class="common_td_rules">{{ $list->country ? $list->country : '-' }}</td>
            <td class="common_td_rules">{{ $list->state ? $list->state : '-' }}</td>
            <td class="" align="center" onclick="manageDocs({{ $list->patient_id }},{{ $list->doctor_id }},{{ $list->encounter_id }},{{ $list->visit_id }})"><i class="fa fa-file"></i></td>
        </tr>
        <?php
                }
            }else{
                ?>
        <tr>
            <td style="text-align: center" colspan="11" class="re-records-found">No Records Found</td>
        </tr>
        <?php
            }
            ?>

    </tbody>
</table>

<div class="clearfix"></div>
<div class="col-md-12 padding_sm text-center">
    <ul class="pagination purple_pagination pull-right">
        {!! $page_links !!}
    </ul>
</div>
