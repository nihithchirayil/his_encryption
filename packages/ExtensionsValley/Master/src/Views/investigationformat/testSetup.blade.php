
<table id ="fixTable_test_setup"class="table theadfix_wrapper table table-bordered table_sm no-margin no-border">
    <thead >
        <tr class="table_header_common">
        
        <th>Code</th>
        <th>Test Name</th>
        <th>Short Name</th>
        <th>Print Order</th>
        <th>Send TO Machine</th>
        
        </tr>
    </thead>
    <tbody>
        @foreach ($testDataDetails as $item)
    <tr>
        
        <td class="common_td_rules" >{{ $item->service_code }}</td>
        <td class="common_td_rules">{{ $item->service_desc }}</td>
        <td class="common_td_rules">{{ $item->short_name }}</td>
        <td class="common_td_rules"><input type="text" id ="chengeLabServOrder{{$item->id}}"onchange="chengeLabServOrder('{{$item->id}}');" value="{{ $item->lab_service_order }}"></td>
        @if($item->send_to_machine == 1)
        @php
           $chek='checked'; 
        @endphp
        @elseif($item->send_to_machine == 0)
@php
               $chek=' ';

@endphp      
  @endif
        <td class="common_td_rules"><input {{$chek}} type="checkbox" onclick="checkConvert('{{$item->id}}')" id="machine_{{$item->id}}"></td>

       
    </tr>
@endforeach


    </tbody>
</table>