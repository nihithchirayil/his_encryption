@if(isset($selectFindings))
@foreach($selectFindings as $item)
    <tr>
        <td class="common_td_rules">{{$item->format_value}}</td>
        @if($item->is_panic==0)
        <td class="common_td_rules"><input type="checkbox" class="is_panic{{$item->id}}" onclick="addIsPanic('{{$item->id}}')";name="" id=""></td>
        @else
        <td class="common_td_rules"><div class="col-md-12"><input type="checkbox" class="is_panic{{$item->id}}" onclick="addIsPanic('{{$item->id}}')"; checked="checked" name="" id=""> </div></td>
        @endif
        <td class="common_td_rules">{{$item->normal_range}}</td>
        <td class="common_td_rules">{{$item->format_value}}</td>
        <td class="common_td_rules"><div class="col-md-12"><button class="btn btn-danger" onclick="deleteFindings('{{$item->id}}')"><i class="fa fa-times"  ></i></button></div></td>  
    </tr>
@endforeach
@endif