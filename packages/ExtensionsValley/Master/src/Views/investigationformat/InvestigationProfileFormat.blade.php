<div class="modal fade" tabindex="-1" role="dialog" id="profileFormatModal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header table_header_common" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">INVESTIGATION PROFILE FORMAT <span class="findings_of_id"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="col-md-2 pull-right">
                <button class="pull-right col-md-2 btn btn-primary btn-block" onclick="saveProfileFormat();">save</button>
            </div>
            <div class="padding_sm">
                <div class="col-md-12 padding_sm" style="height:520px;">

                    <div id="numericFormat" style="">

                        <div class=col-md-12>

                            <div class="col-md-3 ">

                                <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                                    <!-- <div class="col-md-12">
                                        Sub Test :
                                    </div> -->
                                    <div class="col-md-12 padding_sm" style="margin-top: 2px;">
                                        <!-- <div class="col-md-12 padding_sm">
                                            <div class="checkbox checkbox-success inline">
                                                <input id="" type="checkbox" name="checkbox">
                                                <label class="text-blue" for="checkbox">
                                                    Hidden Parameter
                                                </label>
                                            </div>
                                        </div> -->
                                    </div>

                                    <div class="col-md-12" style="margin-bottom:10px;">
                                        <div class="col-md-4">
                                            Service:
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" autocomplete="off" class="form-control hidden_search" id="profile_service_item" value="" placeholder="Search service....">
                                            <input type="hidden" value="0" id="profile_service_item_hidden">
                                            

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded theadscroll" style="height:400px;">
                                    <table class="table theadfix_wrapper table table-bordered table_sm no-margin no-border floatThead-table">
                                        <thead>
                                            <tr class="table_header_common">
                                                <th></th>
                                                <th>Code</th>
                                                <th>Service</th>
                                                <th>Order</th>
                                            </tr>
                                        </thead>
                                        <tbody id='profile_service_itemAjaxDiv'>
                                            <!-- <tr>
                                        <div class="" id="profile_service_itemAjaxDiv" style="text-align: left;   cursor: pointer; max-height: 350px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999;
                                                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3);">
                                            </div>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-9 shadow-lg p-3 mb-5 bg-white rounded theadscroll" style="height:400px;">
                                <table class="table theadfix_wrapper  table-bordered table_sm no-margin no-border floatThead-table">
                                    <thead>
                                        <tr class="table_header_common">
                                            <th>Sub Profile Code</th>
                                            <th>Sub Profile Name</th>
                                            <th>Sub Test Name</th>
                                            <th>Type Order</th>
                                            <th></th>
                                            <th></th>
                                        </tr>

                                    </thead>
                                    <tbody id="profileDataList">

                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>