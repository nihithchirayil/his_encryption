<table id ="fixTable"class="table theadfix_wrapper table table-bordered table_sm no-margin no-border">
    <thead>
        <tr class="table_header_common">
        <th>Code</th>
        <th>Department</th>
        <th>Print Order</th>
        
        </tr>
    </thead>
    <tbody>
        @foreach ($subDepartmens as $item)
    <tr>
        <td class="common_td_rules" >{{ $item->sub_dept_code }}</td>
        <td class="common_td_rules">{{ $item->sub_dept_name }}</td>
        <td class="common_td_rules "><input type="text" class="printOrder{{$item->sub_dept_code}} print_order" value="{{ $item->id }}"  data-code="{{$item->sub_dept_code}}" onchange="chengePriceOrde('{{$item->sub_dept_code}}');"></td>
        
    </tr>
@endforeach


    </tbody>
</table>