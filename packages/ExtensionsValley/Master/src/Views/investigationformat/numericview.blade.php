<div class="modal fade" tabindex="-1" role="dialog" id="numericFormatModal" data-keyboard="false" data-backdrop="static" >
    <div class="modal-dialog" role="document" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header table_header_common" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">INVESTIGATION NUMERIC FORMAT <span class="findings_of_id"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="col-md-12"> 
            <button type="button" class="pull-right col-md-2 btn btn-primary" onclick="saveNumericFormat();">save</button>
                
            </div>
            <div class="padding_sm">
                <div class="col-md-12 padding_sm"style="height:520px;">
                
                <div id="numericFormat" style="">
                
                    <div class=col-md-12>
                        
                        <div class="col-md-3 ">
                            
                        <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                            <div class="col-md-12 padding_sm" style="margin-top: 2px;">
                                <div class="col-md-2 padding_sm">
                                    <div class="">
                                        
                                        <label class="text-blue" for="">
                                            Machine
                                        </label>
                                    </div>
                                </div>
                            </div> 
                        
                            <div class="col-md-12 " >   
                                <div class="mate-input-box" style="height:45px!important;">
                                        
                                    <div class="clearfix"></div>
                                        <select class="form-control select2" id='machine_name' onchange="">
                                        <option value="">Select Machine</option>
                                            @if (isset($machine_name))
                                            @foreach ($machine_name as $item)
                                                
                                                <option value="{{ $item->id }}">{{ $item->name}}</option>
                                            @endforeach
                                        @endif
                    
                                    </select>
                                </div>
                            </div>
                            
                        </div>  
                            <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                                <div class="col-md-12 padding_sm" style="margin-top: 2px;">
                                    <div class="col-md-2 padding_sm">
                                        <div class="">
                                            <label class="text-blue" for="checkbox">
                                                Gender
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="mate-input-box col-md-10" style="height:45px!important;">
                                        <div class="clearfix"></div>
                                        <select class="form-control select2" id='gender_numeric'>
                                        <option value="">Select Machine</option>
                                            @if (isset($gender))
                    
                                            @foreach ($gender as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        @endif
                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                                <div class="col-md-12 padding_sm" style="margin-top: 2px;">
                                    <div class="col-md-2 padding_sm">
                                        <div class="">  
                                            <label class="text-blue" for="checkbox">
                                                Age
                                            </label>

                                        </div>
                                    </div>
                                </div> 
                                
                                <div class="col-md-12 padding_sm" style="margin-top: 1px; margin-bottom: 10px;">
                                    <div class="col-md-3" >
                                        <div class="col-md-12">&nbsp;</div>
                                        <label class="text-blue"  >From </label>
                                        <label class="text-blue" >To </label>
                                    
                                    </div>
                                    <div class="col-md-3" >
                                        <label>Year</label>
                                        <input type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="from_year" name="quantity" min="0" max="150" class="form-control">
                                        <input type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="to_year" min="0" max="150" class="form-control">
                                    </div>
                                    <div class="col-md-3" >
                                        <label>Month</label>
                                        <input type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="from_month" min="0" max="12" class="form-control">
                                        <input type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="to_month" min="0" max="12" class="form-control">
                                    </div>
                                    <div class="col-md-3" >
                                        <label>Day</label>
                                        <input type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="from_date" min="0" max="31" class="form-control">
                                        <input type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="to_date" min="0" max="31" class="form-control">
                                    </div>
                                
                                </div>
                            </div>
                                <div class="col-md-12 padding_sm" style="margin-top: 1px;">
                                    <div class="col-md-8">
                                        <label class="text-blue padding_sm"> Minimum Range</label>  
                                        <label class="text-blue padding_sm">Maximum Range</label>
                                        <label class="text-blue padding_sm" >Panic Minimum Value </label> 
                                        <label class="text-blue padding_sm" >Panic Maximum Value </label> 
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"  class="form-control" id="minimum_range">
                                        <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control" id="maximum_range">
                                        <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control"id="panic_minimum_value">
                                        <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control" id="panic_maximum_value">
                                    </div>
                                    
                                </div>
                                
                                <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                                    <div class="col-md-12 padding_sm" style="margin-top: 2px;">
                                            <div class="col-md-12 padding_sm">
                                                <div class="">
                                                    <label class="text-blue" for="">
                                                        Normal Range Description
                                                    </label>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-md-12 padding_sm" style="margin-top: 1px;margin-bottom: 10px;">
                                            <textarea class="paddin_sm" name="" id="normalRangeDescrip" style="height: 99px; width: 205px;resize: none;" cols="30" rows="10">

                                            </textarea>
                        
                                    </div>
                                </div>
                    
                        </div>
                        <div class="col-md-9">
                            <table class="table theadfix_wrapper table table-bordered table_sm no-margin no-border floatThead-table">
                                <thead>
                                    <tr class="table_header_common">
                                        <th style="width:10%;">Minimum Value</th>
                                        <th style="width:10%;">Maximum Value</th>
                                        <th style="width:10%;">Maximum Panic Value</th>
                                        <th style="width:10%;">Minimum Panic Value</th>
                                        <th style="width:10%;">Normal Range</th>
                                        <th style="width:10%;">Gender</th>
                                        <th style="width:15%;">Age From</th>
                                        <th style="width:15%;">Age To</th>
                                        <th style="width:10%;">Machine</th>
                                        <th ></th>
                                    </tr>

                                </thead >
                                <tbody id="numericListView" >
                               
                                   
                                </tbody> 
                            </table>
                        </div>
                    </div>
                

                    </div>
                </div>
            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

