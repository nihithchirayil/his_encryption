@if(sizeof($numericList)>0)
@foreach ($numericList as $item)
@php
$daysOfMonth = 365.2425/12;
$age_from = ($item->age_from);
$fromMonth_date = "";
if($age_from>0){
$fromYear = $age_from/365.2425;
$N_from_year = (int)$fromYear;
$yearOfDate= 365.2425*$N_from_year;
$fromMonth_date = $age_from - $yearOfDate;
$fromMonth = $fromMonth_date/$daysOfMonth;
$fromMonth = (int)$fromMonth;
$daysOfMont =$daysOfMonth*$fromMonth;
$fromDays = $daysOfMont-$fromMonth_date;
$fromDays = (int)abs($fromDays) < abs($fromDays)?abs($fromDays)+1:$fromDays;
$fromDays = abs((int)$fromDays);
$from_Age = $N_from_year.'year'.$fromMonth.'month'.$fromDays.'days';
}
else{
$from_Age='0year0month0days';  
}
$age_to = ($item->age_to);
if($age_to>0){
$Year = round($age_to/365.2425);
$toYearOfDate= 365.2425*$Year;
if($toYearOfDate>$age_to){
$month_date = $toYearOfDate-$age_to;  
}else{
$month_date = $age_to-$toYearOfDate;
}
$month = round($month_date/$daysOfMonth);
$days = abs(round($month_date-($daysOfMonth*$month)));
$to_age = round($Year).'year'.round($month).'month'.round($days).'days';
}
else{
    $to_age='0year0month0days';  
}
@endphp
    <tr>
        <td class="common_td_rules" >{{ $item->min_value }}</td>
        <td class="common_td_rules">{{ $item->max_value }}</td>
        <td class="common_td_rules">{{ $item->min_panic_value }}</td>
        <td class="common_td_rules">{{ $item->max_panic_value }}</td>
        <td class="common_td_rules">{{ $item->normal_range }}</td>
        <td class="common_td_rules">{{ $item->gender }}</td>
        
        <td class="common_td_rules">{{$from_Age}}</td>
        <td class="common_td_rules">{{ $to_age }}</td>
        <td class="common_td_rules">{{ $item->machine_id }}</td>
        <td class="common_td_rules"><button type="button" class="btn btn-sm btn-danger" onclick="deleteNumericFormat('{{$item->id}}');"><i class="fa fa-times"></i></button></td>
    </tr>
@endforeach
@else
@endif
