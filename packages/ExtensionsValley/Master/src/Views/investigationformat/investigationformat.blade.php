@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/common-custom.css') }}" rel="stylesheet">

    <style>
        .box_header {
            background: #3b926a !important;
            color: #fff;
        }

        .btn-success {
            background: #3b926a !important;
        }
        .bg-white {
            background: #fff !important;
            border: 1px solid #d7d0d0 !important;
            color: #212529;
            box-shadow: 0 0 3px #d7d7d7;
            border-radius: 5px 5px 0 0;
            margin-bottom: 5px;
        }
        .box_header {
            background: #3b926a !important;
            color: #fff;
        }
        table tr:hover {
          background-color: #ffff99;
        }
        
        .tox-tinymce-aux{z-index:99999999999 !important;}

        .sub_btn {
                background-color: DodgerBlue; /* Blue background */
                border: none; /* Remove borders */
                color: white; /* White text */
                /* padding: 12px 16px; Some padding */
                font-size: 15px; /* Set a font size */
                cursor: pointer; /* Mouse pointer on hover */
            }

            /* Darker background on mouse-over */
            .sub_btn:hover {
            background-color: RoyalBlue;
            }

    </style>
@endsection
@section('content-area')


    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="edit_serv_id" value="">
    <input type="hidden" id="edit_sub_test_id" value="">
    <div class="right_col" role="main" style="background: white; height: 720px;">
        <div class="col-md-12 padding_sm" style="margin-top:5px; ">
            <div class="box no-border no-margin " id="ser_bill_detail_div_1">
                <div class="box-header table_header_common" style="margin-bottom: 5px;">
                    <span class="padding_sm ">Investigation Format </span>
                </div>

            </div>

        </div>


    </div>
    <div class=col-md-12>
        <div class="col-md-3 shadow-lg p-3 mb-5 bg-white rounded">
            <div class="col-md-12 padding_sm" style="margin-top: 1px;">
                <div class="mate-input-box" style="height:45px!important;">
                    <label for="">Department</label>
                    <div class="clearfix"></div>
                    <select class="form-control select2" id='department' onchange="subDepartmentlist();">
                        @if (isset($labdepartment))

                        @foreach ($labdepartment as $item)
                        <option value="{{ $item->id }}">{{ $item->dept_name }}</option>
                        @endforeach
                        @endif

                    </select>
                </div>
                <div class="col-md-12 padding_sm" style="margin-top: 1px;">
                    <div class="mate-input-box" style="height:45px!important;">
                        <label for="">Sub Department  </label>
                        <div class="clearfix"></div>
                        <select class="form-control select2" id='subdepartment' onchange="listInvestigations();">
                            <option value="" >Sub Department</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn btn-primary col-md-12" onclick="departmentList();" style="font-size: 10px;">
                        SETUP PRINT ORDER FOR LAB   
                    </button>
                </div>
            </div>
            <div class="col-md-12">
                <button type="button" class="btn btn-success col-md-12" onclick="departmentList();" style="font-size: 10px;">
                    SETUP PRINT ORDER FOR LAB
                </button>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 1px;">
                <div class="mate-input-box" style="height:45px!important;">
                    <label for="">Report Format</label>
                    <div class="clearfix"></div>
                    <select class="form-control select2" id='formatCode' onchange="listInvestigations();">
                        @if (isset($format_code))

                        @foreach ($format_code as $item)
                        <option value="{{ $item->code}}">{{ $item->name }}</option>
                        @endforeach
                        @endif

                    </select>
                </div>
                <div class="col-md-12 padding_sm" style="margin-top: 1px;">
                    <div class="mate-input-box" style="height:45px!important;">
                        <label for="">Test Name</label>
                        <div class="clearfix"></div>
                        <input type="text" autocomplete="off" class="form-control" onkeyup="searchbyServiceName();"  id="testName"
                        name=" " value="" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="button" class=" btn btn-primary col-md-12" onclick="testSetupDetails();" style="font-size: 10px;">
                        TEST SETUP<p>(Print Order,Short Name and Machine Test)</p>
                    </button>
                </div>
                <div  class="col-md-12 box-body theadscroll "id="search_service" style="height:300px;">
                    <table  id ="fixTable_id" class="table theadfix_wrapper  table-bordered table_sm no-margin no-border table-hover table-bordered table-striped">
                        <thead >
                            <tr class="table_header_common">
                            <th>Order</th>
                            <th>Code</th>
                            <th>Test Name</th>
                        </tr>
                    </thead>
                    <tbody class="testsView">

                    </tbody>
                </table>
            </div>
            <div class="col-md-9">
                <div class="col-md-12">
                    <h6 ><b> INVESTIGATION FORMAT OF : <span class="findings_of_id"></span></b> <button title="Add sub test" class="pull-right col-md-2 btn btn-primary " onclick="saveservice_item();"> Save</button></h6>
                    
                </div>

            </div>

            <div class="col-md-4 ">
                <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                    <div class="col-md-12 green"><b> Preview</b></div>
                    <div class="col-md-6 padding_sm" style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Print Order</label>
                            <div class="clearfix"></div>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off" class="form-control" id="printOrder" name=" " value="" required>
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Short Name</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="shortName" name="  " value="" required>
                        </div>
                    </div>
                    <div class="col-md-7 pull-right">
                        <input type="checkbox" id="sendMachine"> Send To Machine
                    </div>
                    <div class="col-md-12 padding_sm" style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Pathology Report Title</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="pathologyReportTitle" name="  " value="" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                    <div class="col-md-12 green"><b>Sample Configuration</b></div>
                    <div class="col-md-12 padding_sm" style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Sample Name</label>
                            <div class="clearfix"></div>
                            <select class="form-control select2" id='sampleName'>
                                <option value="">Select Sample</option>
                                @if (isset($sample_details))

                                @foreach ($sample_details as $item)
                                <option value="{{ $item->id}}">{{ $item->name }}</option>
                                @endforeach
                                @endif

                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for=""> No:of Sample</label>
                            <div class="clearfix"></div>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off" class="form-control" id="noOfSamples" name="  " value="" required>
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Quantity</label>
                            <div class="clearfix"></div>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off" class="form-control" id="noOfQty" name="  " value="" required>
                        </div>
                    </div>

                    <div class="col-md-6 padding_sm" style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Unit</label>
                            <div class="clearfix"></div>
                            <select class="form-control select2" id='unit'>
                                <option value="">Select Sample</option>
                                @if (isset($unit_name))

                                @foreach ($unit_name as $item)
                                <option value="{{ $item->id}}">{{ $item->name }}</option>
                                @endforeach
                                @endif

                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                        <div class="col-md-12 green"><b>Report Format</b></div>

                        <div class="col-md-12 padding_sm" style="margin-top: 1px;">
                            <div class="mate-input-box" style="height:45px!important;">
                                <div class="clearfix"></div>
                                <select class="form-control select2" id='reportFormat'>
                                    @if (isset($format_code))
        
                                    @foreach ($format_code as $item)
                                        <option value="{{ $item->code}}">{{ $item->name }}</option>
                                    @endforeach
                                    @endif
        
                                </select>
                            </div>
                            <button type="button" class=" btn btn-primary col-md-12" onclick="createViewFormat();" style="font-size: 10px;">
                                CREATE /VIEW FORMAT
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                    <div class="col-md-12 green"><b>Report Format</b></div>

                    <div class="col-md-12 padding_sm" style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <div class="clearfix"></div>
                            <select class="form-control select2" id='reportFormat'>
                                @if (isset($format_code))

                                @foreach ($format_code as $item)
                                <option value="{{ $item->code}}">{{ $item->name }}</option>
                                @endforeach
                                @endif

                            </select>
                        </div>
                        <button type="button" class=" btn btn-success col-md-12" onclick="createViewFormat();" style="font-size: 10px;">
                            CREATE /VIEW FORMAT
                        </button>
                    </div>
                </div>
                <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                    <div class="col-md-12 padding_sm" style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Remarks</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="remarks" name="  " value="" required>
                        </div>
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Notes</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="notes" name="  " value="" required>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-md-8">
                <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <input type="checkbox" id="bloodTest"> Blood Test
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" id="poc"> POC
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" id="confidental"> Confidential
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" id='outSource'> Out Source
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="clearfix"></div>
                        <div class="col-md-4">
                            <div class="mate-input-box" style="height:45px!important;">
                                <label for="">Decimal Point</label>
                                <div class="clearfix"></div>
                                <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off" class="form-control" id="decimalPoint" name="  " value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Processing Time</label>
                            <div class="clearfix"></div>

                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off" class="col-md-4" id="day" name="  " value="" required placeholder="day">
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off" class="col-md-4" id="month" name="  " value="" required placeholder="month">
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off" class="col-md-4" id="year" name="  " value="" required placeholder="year">

                        </div>
                    </div>
                </div>
                <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                    <div class="col-md-12 green">
                        <h6><b>Instructions/Precautions</b></h6>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="mate-input-box" style="height:45px!important;">
                                <label for="">Instruction For Billing Person</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="InstrucForBillPers" name="  " value="" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mate-input-box" style="height:45px!important;">
                                <label for="">Instruction For phlebotomist</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="instrucForPhelo" name="  " value="" required>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12 shadow-lg p-3 mb-5 bg-white rounded">
                    <div class="col-md-12"><label>Sub-Test</label></div>
                    <div class="col-md-3">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Sub Test Name</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="subTestName" name="  " value="" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Report Type</label>
                            <div class="clearfix"></div>
                            <select class="form-control select2" id='subReportType'>
                                @if (isset($format_code))

                                @foreach ($format_code as $item)
                                <option value="{{ $item->code}}">{{ $item->name }}</option>
                                @endforeach
                                @endif

                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Print Order</label>
                            <div class="clearfix"></div>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off" class="form-control" id="SubPrintType" name="  " value="" required>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <input type="checkbox" id="sub_test_status" class="padding_sm " checked>Status
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-sm btn-primary addNewRowBtn" onclick="saveSubTest();"><i class="fa fa-plus"></i> </button>

                    </div>

                    </div>
                    <div class="col-md-12 padding_sm box-body"style="height:220px;">
                        <table class="table theadfix_wrapper table table-bordered table_sm no-margin no-border">
                            <thead >
                                <tr class="table_header_common">
                                <th style="width:40%;">Sub-Test</th>
                                <th style="width:20%;">Report Type</th>
                                <th style="width:20%;">Print Order</th>
                                <th style="width:10%;"></th>
                                <th style="width:10%;">SET FORMAT</th>
                            </tr>
                        </thead>
                        <tbody id="subTestDetails">
                        </tbody>
                    </table>
                    <div>

                    </div>


                </div>
            </div>
        </div>
    </div>
    @include('Master::investigationformat.numericview')
    @include('Master::investigationformat.InvestigationProfileFormat')
    @include('Master::investigationformat.investigationFindings')
    @include('Master::investigationformat.TextFormat')
    
{{-- model print order for lab popup  --}}

<div class="modal fade" tabindex="-1" role="dialog" id="orderForLabModal" >
    <div class="modal-dialog" role="document" style="width:55%;">
        <div class="modal-content">
            <div class="modal-header table_header_common" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">PRINT ORDER FOR LAB</h4>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            
            </div>
            <div class="col-md-12"> 
                <i class="fa fa-search"></i> 
                 <input id="issue_search_box"
            onkeyup="searchbyName();" type="text" placeholder="Name Search.. " >
                
            </div>
            <div class="padding_sm">
            <div class="col-md-12 padding_sm"style="height:250px;overflow:scroll"> 

                </div>
            </div>
        </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        
        </div>
    </div>
</div>
{{-- TEST SETUP MODEL--}}
<div class="modal fade" tabindex="-1" role="dialog" id="testSetupModal" >
    <div class="modal-dialog" role="document" style="width:55%;">
        <div class="modal-content">
            <div class="modal-header table_header_common" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">TEST SETUP</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="col-md-12"> 
                
                 <input id="test_setup_search_box"
            onkeyup="searchByTestName();" type="text" class="pull-right" placeholder="Name Search.. " >
            <div class="pull-right padding_sm"><i class="fa fa-search "></i></div>

            <button class="pull-right" onclick="mappingToMachineTest();">save</button>
                
            </div>
            <div class="padding_sm">
                <div class="col-md-12 padding_sm"style="height:250px;overflow:scroll">
                
                <div id="testSetup" style="">
                    
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="mate-input-box" style="height:45px!important;">
                                <label for="">Search</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="test_setup_search_box" onkeyup="searchByTestName();" placeholder="Name Search.. " name="  " value="" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button class=" btn btn-success col-md-2 pull-right"style="margin-top: 10px;"onclick="mappingToMachineTest();">save</button>
                        </div>
                    </div>
                    <div class="padding_sm">
                        <div class="col-md-12 padding_sm theadscroll" style="height:250px;">

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>

{{--report format numeric model --}}
<div class="modal fade" tabindex="-1" role="dialog" id="testSetupModal" >
    <div class="modal-dialog" role="document" style="width:55%;">
        <div class="modal-content">
            <div class="modal-header table_header_common" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">TEST SETUP</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="col-md-12"> 
                
                 <input id="test_setup_search_box"
            onkeyup="searchByTestName();" type="text" class="pull-right" placeholder="Name Search.. " >
            <div class="pull-right padding_sm"><i class="fa fa-search "></i></div>

            <button class="pull-right" onclick="mappingToMachineTest();">save</button>
                
            </div>
            <div class="padding_sm">
                <div class="col-md-12 padding_sm"style="height:250px;overflow:scroll">
                
                <div id="testSetup" style="">
                    
                    </div>
                </div>
            </div>
        </div>

        {{--report format numeric model --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="testSetupModal">
            <div class="modal-dialog" role="document" style="width:55%;">
                <div class="modal-content">
                    <div class="modal-header box_header" style="background-color:rgb(91 110 91);">
                        <h4 class="modal-title" style="color: white;">TEST SETUP</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="col-md-12">

                        <input id="test_setup_search_box" onkeyup="searchByTestName();" type="text" class="pull-right" placeholder="Name Search.. ">
                        <div class="pull-right padding_sm"><i class="fa fa-search "></i></div>

                        <button class="pull-right" onclick="mappingToMachineTest();">save</button>

                    </div>
                    <div class="padding_sm">
                        <div class="col-md-12 padding_sm" style="height:250px;overflow:scroll">

                            <div id="testSetup" style="">

                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Close</button>
                    </div>
                </div>
            </div>
        </div>




        @stop
        @section('javascript_extra')

        <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
        </script>
        <script src="{{ asset('packages/extensionsvalley/emr/js/investigationFormat.js') }}"></script>

        <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
        </script>



        @endsection