<div class="modal fade" tabindex="-1" role="dialog" id="findingFormatModal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header table_header_common" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">INVESTIGATION FINDINGS OF <span class="findings_of_id"></span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    style="position: absolute; right: 10px; top: 15px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="padding_sm">
                <div class="col-md-5 box-body findingTableBody" style="height:400px;">
                    <!-- <div class="col-md-6  pull-right  ">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Search findings</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control"
                                onkeyup="searchByFindings();" id="finding_name" name=" " value="" required>
                        </div>
                    </div> -->
                    <div class="col-md-12 box-body theadscroll "style="height:395px;width:100%;">
                        <table id="finding_table" class="table theadfix_wrapper table table-bordered table_sm no-margin no-border table-hover table-bordered table-striped">
                            <thead class="table_header_common">
                                <tr class="table_header_bg">
                                    <th style="width:30%">Code</th>
                                    <th style="width:70%"><input id="finding_name" onkeyup="searchByFindings();" type="text" placeholder="Name Search.. "
                                style="color: rgb(0, 0, 0); width: 80%; display:none;">
                                <span id="finding_search_btn" style=" float: right" class="btn btn-warning">
                                <i class="fa fa-search"></i></span><span id="find_search_btn_text"
                                style="display: block;">FInding Name</th>
                                </tr>
                            </thead>
                            <tbody id=findingDetails>

                            </tbody>
                        </table>
                    </div>
                    
                </div>
                <div class="col-md-7 box-body selectFindingTableBody" style="height:400px;">
                    <!-- <h6>Normal range description</h6> -->
                    <!-- <textarea name="" id="" cols="30" rows="10" style="width: 610px; height: 76px;resize:none;"></textarea> -->
                    <div class="">
                        <table
                            class="table theadfix_wrapper table table-bordered table_sm no-margin no-border floatThead-table table-hover table-bordered table-striped">
                            <thead class="table_header_common">
                                <tr class="table_header_bg">
                                    <th style="width:35%;">Finding name</th>
                                    <th style="width:5%;">Panic</th>
                                    <th style="width:20%;">Normal range</th>
                                    <th style="width:35%;">Format value</th>
                                    <th style="width:5%;"></th>

                                </tr>
                            </thead>
                            <tbody id="selectFIndings">
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
</div>
