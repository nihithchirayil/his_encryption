@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    @include('Dashboard::dashboard.partials.headersidebar')
@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-list.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">


@endsection
@section('content-area')


    <div class="modal fade" id="customdatapopmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 500px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Custom Date Range Selection</h4>
                </div>
                <div class="modal-body" style="min-height: 80px">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-4 date_filter_div">
                            <div class="mate-input-box">
                                <label class="filter_label">From Date</label>
                                <input type="text" data-attr="date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                    class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="from_date">
                            </div>
                        </div>
                        <div class="col-md-4 date_filter_div">
                            <div class="mate-input-box">
                                <label class="filter_label">To Date</label>
                                <input type="text" data-attr="date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                    class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="to_date">
                            </div>
                        </div>
                        <div class="col-md-4 date_filter_div pull-right" style="padding-top: 10px;">
                            <button id="searchdatabtn" onclick="getCustomDateRange()"
                                class="btn btn-primary btn-block">Search <i id="searchdataspin"
                                    class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="dualgraph_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1400px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="dualgraph_modelheader">NA</h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div class="col-md-5 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 510px;">
                                <div id="dualgraph_modeldiv1">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 510px;">
                                <div id="dualgraph_modeldiv2">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 10px;">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="getmodelgraphs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1300px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="getmodelgraphsheader">NA</h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 510px;">
                            <div class="col-md-12 padding_sm" id="getmodelgraphsdiv">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="otbill_groupsmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 800px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="otbill_groupsheader">NA</h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div class="col-md-12 padding_sm" id="otbill_groupsdiv">

                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="right_col" role="main">
        <input type="hidden" id="daterange_hidden">
        <div class="row padding_sm">
            <input type="hidden" id="date_datahidden" value='<?= $date_array ?>'>
            <div class="col-md-12 padding_sm pull-right">
                <div class="col-md-10 padding_sm">
                    <div class="col-md-4 padding_sm">
                        Range Type :<strong><span class='blue' id='range_typedata'></span></strong>
                    </div>
                    <div class="col-md-3 padding_sm">
                        From Date : <strong><span class='red' id='from_datadis'></span></strong>
                    </div>
                    <div class="col-md-3 padding_sm">
                        To Date :<strong><span class='red' id='to_datadis'></span></strong>
                    </div>
                </div>
                <div class="col-md-2 padding_sm pull-right">
                    <?= $title ?>
                </div>
            </div>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 34px;">
                        <div class="col-md-10 padding_sm">
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(1)" title="Today" class="btn btn-primary btn-block">Today <i
                                        class="fa fa-calendar"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(2)" title="Yesterday"
                                    class="btn btn-success btn-block">Yesterday <i class="fa fa-calendar"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(3)" title="Week Till Date"
                                    class="btn btn-warning btn-block">Week Till Date <i class="fa fa-calendar"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(4)" title="Last Week" class="btn btn-danger btn-block">Last
                                    Week <i class="fa fa-calendar"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(5)" title="Month Till Date"
                                    class="btn btn-info btn-block">Month Till Date <i class="fa fa-calendar"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(6)" title="Last Month" class="btn bg-purple btn-block">Last
                                    Month <i class="fa fa-calendar"></i></button>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="col-md-12">
                                <button onclick="getCustomDates(7)" title="Custom Date Range Selection"
                                    class="btn bg-teal-active btn-block">Custom <i class="fa fa-calendar"></i></button>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 110px;">

                            <div class="tile_count">
                                <div class="col-md-3">
                                    <span class="count_top"><i id="list_data1" class="fa fa-list blue"></i> Total
                                        Collection</span>
                                    <button class="btn btn-primary" id="getTotalcollectionbtn"
                                        onclick="getTotalCollection()" type="button"><i id="getTotalcollectionspin"
                                            class="fa fa-bar-chart"></i></button>
                                    <div class="count td_common_dashboard_count" id="total_collection1">0</div>
                                    <span id="count_bottom1"></span>
                                </div>
                                <div class="col-md-3 ">
                                    <span class="count_top"><i id="list_data2" class="fa fa-list blue"></i> OP
                                        Collection</span>
                                    <button class="btn btn-success" id="getlabbtnop_collection"
                                        onclick="getLabRadiologyCollection('op_collection')" type="button"><i
                                            id="getlabspinop_collection" class="fa fa-bar-chart"></i></button>
                                    <div class="count td_common_dashboard_count" id="total_collection2">0</div>
                                    <span id="count_bottom2"></span>
                                </div>
                                <div class="col-md-3 ">
                                    <span class="count_top"><i id="list_data3" class="fa fa-list blue"></i> IP
                                        Collection</span>
                                    <button class="btn btn-warning" id="getlabbtnip_collection"
                                        onclick="getLabRadiologyCollection('ip_collection')" type="button"><i
                                            id="getlabspinip_collection" class="fa fa-bar-chart"></i></button>
                                    <div class="count td_common_dashboard_count" id="total_collection3">0</div>
                                    <span id="count_bottom3"></span>
                                </div>
                                <div class="col-md-3 ">
                                    <span class="count_top"><i id="list_data4" class="fa fa-list blue"></i> Casualty
                                        Collection</span>
                                    <button class="btn bg-purple" id="getlabbtncasualty_collection"
                                        onclick="getLabRadiologyCollection('casualty_collection')" type="button"><i
                                            id="getlabspincasualty_collection" class="fa fa-bar-chart"></i></button>
                                    <div class="count td_common_dashboard_count" id="total_collection4">0</div>
                                    <span id="count_bottom4"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 padding_sm" style="margin-top: 15px;">
                    <div class="col-md-4 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 340px;">
                                <span class=text-center> <strong>Total OP Patients</strong></span>
                                <div id="ip_op_statusdataopdata" class="padding_sm"></div>
                            </div>
                        </div>
                    </div>

                    <div id="patients" class="col-md-4 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 340px;">
                                <div class="col-md-12 padding_sm">
                                    <div class="col-md-6 padding_sm">
                                        <div class="tile_count">
                                            <div class="col-md-12 padding_sm" style="margin-bottom: 20px;">
                                                <span class="count_top"><i id="list_data15"
                                                        class="fa fa-list blue"></i> Total Advance Adjust</span>
                                                <div class="count td_common_dashboard_count" id="total_collection15">0
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 padding_sm" style="margin-bottom: 20px;">
                                                <span class="count_top"><i id="list_data25"
                                                        class="fa fa-list blue"></i> Total Advance Collected</span>
                                                <div class="count td_common_dashboard_count" id="total_collection25">0
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 padding_sm" style="margin-bottom: 20px;">
                                                <span class="count_top"><i id="list_data5"
                                                        class="fa fa-list blue"></i> Total Discount</span>
                                                <div class="count td_common_dashboard_count" id="total_collection5">0</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 padding_sm" style="margin-bottom: 20px;">
                                                <span class="count_top"><i id="list_data7"
                                                        class="fa fa-list blue"></i> Other Credit</span>
                                                <div class="count td_common_dashboard_count" id="total_collection7">0</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 padding_sm" style="margin-bottom: 20px;">
                                                <span class="count_top"><i id="list_data8"
                                                        class="fa fa-list blue"></i> Total Discharge</span>
                                                <div class="count td_common_dashboard_count" id="total_collection8">0</div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding_sm">
                                        <div class="tile_count">
                                            <div class="col-md-12 padding_sm" style="margin-bottom: 20px;">
                                                <span class="count_top"><i id="list_data6"
                                                        class="fa fa-list blue"></i> Company Credit</span>
                                                <button class="btn bg-orange" title="Pharmacy Company Credit"
                                                    id="getlabbtncompany_tot"
                                                    onclick="getCompanyCreditSplitup('company_tot',1,'Company Credit')"
                                                    type="button"><i id="getlabspincompany_tot"
                                                        class="fa fa-bar-chart"></i></button>
                                                <button class="btn bg-purple" title="Exclude Pharmacy Company Credit"
                                                    id="getlabbtncompany_grp"
                                                    onclick="getCompanyCreditSplitup('company_grp',2,'Company Credit')"
                                                    type="button"><i id="getlabspincompany_grp"
                                                        class="fa fa-bar-chart"></i></button>
                                                <div class="count td_common_dashboard_count" id="total_collection6">0</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 padding_sm" style="margin-bottom: 20px;">
                                                <span class="count_top"><i id="list_data22"
                                                        class="fa fa-list blue"></i> Insurance Settlement</span>
                                                <div class="count td_common_dashboard_count" id="total_collection22">0
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 padding_sm" style="margin-bottom: 20px;">
                                                <span class="count_top"><i id="list_data23"
                                                        class="fa fa-list blue"></i> Pharmacy Purchase</span>
                                                <button class="btn bg-orange" title="Pharmacy Purchase In Detail"
                                                    id="getlabbtnpharmacy_purchase_tot"
                                                    onclick="getCompanyCreditSplitup('pharmacy_purchase_tot',1,'Pharmacy Purchase')"
                                                    type="button"><i id="getlabspinpharmacy_purchase_tot"
                                                        class="fa fa-bar-chart"></i></button>
                                                <div class="count td_common_dashboard_count" id="total_collection23">0
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 padding_sm" style="margin-bottom: 20px;">
                                                <span class="count_top"><i id="list_data24"
                                                        class="fa fa-list blue"></i> General Store Purchase</span>
                                                <button class="btn bg-orange" title="Pharmacy Purchase In Detail"
                                                    id="getlabbtngeneral_store_purchase_tot"
                                                    onclick="getCompanyCreditSplitup('general_store_purchase_tot',1,'General Store Purchase')"
                                                    type="button"><i id="getlabspingeneral_store_purchase_tot"
                                                        class="fa fa-bar-chart"></i></button>
                                                <div class="count td_common_dashboard_count" id="total_collection24">0
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 15px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 340px;">
                                <span class=text-center> <strong>Total Admissions</strong></span>
                                <div id="ip_op_statusdataipdata" class="padding_sm"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 padding_sm">
                    <div class="col-md-12 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 110px;">
                                <h2 class="text-blue no-margin"><b>Collection</b></h2>
                                <div class="tile_count">
                                    <div class="col-md-2 ">
                                        <span class="count_top"><i id="list_data9" class="fa fa-list blue"></i>
                                            Lab</span>
                                        <button class="btn btn-danger" id="getlabbtnlab" onclick="getlabDetails()"
                                            type="button"><i id="getlabspinlab" class="fa fa-bar-chart"></i></button>
                                        <div class="count td_common_dashboard_count" id="total_collection9">0</div>
                                    </div>
                                    <div class="col-md-2 ">
                                        <span class="count_top"><i id="list_data10" class="fa fa-list blue"></i>
                                            Pharmacy</span>
                                        <button class="btn btn-warning" id="getlabbtnpharmary_collection"
                                            onclick="getLabRadiologyCollection('pharmary_collection')" type="button"><i
                                                id="getlabspinpharmary_collection" class="fa fa-bar-chart"></i></button>
                                        <div class="count td_common_dashboard_count" id="total_collection10">0</div>
                                    </div>
                                    <div class="col-md-2 ">
                                        <span class="count_top"><i id="list_data11" class="fa fa-list blue"></i>
                                            Radiology</span>
                                        <button class="btn bg-teal-active" id="getlabbtnradio"
                                            onclick="getLabRadiologyCollection('radio')" type="button"><i
                                                id="getlabspinradio" class="fa fa-bar-chart"></i></button>
                                        <div class="count td_common_dashboard_count" id="total_collection11">0</div>
                                    </div>
                                    <div class="col-md-2 ">
                                        <span class="count_top"><i id="list_data50" class="fa fa-list blue"></i>
                                            Cosmetic</span>
                                        <button class="btn bg-purple" id="getlabbtncosmetic"
                                            onclick="getLabRadiologyCollection('cosmetic')" type="button"><i
                                                id="getlabspincosmetic" class="fa fa-bar-chart"></i></button>
                                        <div class="count td_common_dashboard_count" id="total_collection50">0</div>
                                    </div>
                                    <div class="col-md-2 ">
                                        <span class="count_top"><i id="list_data51" class="fa fa-list blue"></i>
                                            Purchase Margin</span>
                                        <button class="btn btn-info" id="getlabbtnpharmacy_purchase"
                                            onclick="getLabRadiologyCollection('pharmacy_purchase')" type="button"><i
                                                id="getlabspinpharmacy_purchase" class="fa fa-bar-chart"></i></button>
                                        <div class="count td_common_dashboard_count" id="total_collection51">0</div>
                                    </div>
                                    <div class="col-md-2 ">
                                        <span class="count_top"><i id="list_data14" class="fa fa-list blue"></i>
                                            Other</span>
                                        <button class="btn btn-success" id="getlabbtnother_collection"
                                            onclick="getLabRadiologyCollection('other_collection')" type="button"><i
                                                id="getlabspinother_collection" class="fa fa-bar-chart"></i></button>
                                        <div class="count td_common_dashboard_count" id="total_collection14">0</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/revenueDashBoard.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js') }}"></script>
@endsection
