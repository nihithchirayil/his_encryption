<h4  class="blue" style="text-align:center;margin:0px !important"> Generic Wise Collection</h4>
<div class="theadscroll" style="position: relative;height:450px">
<table id="result_data_table" class='table table-condensed theadfix_wrapper table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass" style="background-color:#01987a;color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th width='10%'>SL.No.</th>
                <th width='70%'>Generic Name</th>
                <th width='20%'>Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $net_total=0.0;
        if (count($res)!=0) {
            $i=1;
            foreach ($res as $each) {
                ?>
                <tr>
                    <td class="common_td_rules"><?=$i?></td>
                    <td class='common_td_rules'>{{  @$each->generic_name !=null ? $each->generic_name : 'Not Mentioned' }}</td>
                    <td class='td_common_numeric_rules'><?=$each->amount?></td>
                </tr>
        <?php
            $net_total+= floatval($each->amount);
            $i++;
            }
            ?>
                <tr>
                    <th class="common_td_rules" colspan="2">Total</th>
                    <th class='td_common_numeric_rules'><?=number_format($net_total)?></th>
                </tr>
            <?php
        }else{
            ?>
                <tr>
                    <th colspan="3" style="text-align: center;"> No Result Found</th>
                </tr>
            <?php
        }
        ?>
    </tbody>
    </table>
</div>
