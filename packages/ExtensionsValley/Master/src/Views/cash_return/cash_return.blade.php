
<style>
    .ajaxSearchBox{
        display: none;
        width: 250px !important;
        height: 400px !important;
        overflow: hidden;
        padding-top: 0px !important;
        margin-top: 15px !important;
    }
    .liHover{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .ajaxSearchBox{
        z-index: 9999 !important;
    }
    .ajaxSearchBox>li{
        font-size:13px !important;
        font-family: Arial !important;
    }
    .payment_mode{
        width:175px !important;
    }
    .amount_table{
        border-spacing:10px !important;
        border:1px solid white !important;
        border-collapse: collapse !important;
    }
    .amount_table >tbody>tr{
        height:30px !important;
    }
   .payment_mode :hover{
        background-color:#02cf99;
        color:white;
   }
   .blink_me {
        animation: blinker 1s linear infinite;
    }
    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

    @media print {
        .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #efefef;
        }


        .theadscroll {max-height: none; overflow: visible;}
        .header_bg {
            background-color: #08898d;
            color: white;
        }
        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #f2fef5;
        }

        #resultDataTable{
            border: 1px solid #cedfe9 !important;
            border-collapse: collapse !important;
        }
        td{
            padding-left: 5px !important;
        }

    }
    .table_header_common {
    background: #3e75a5 !important;
    color: #FFF !important;
}
</style>

<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}"/>
<input type="hidden" id="c_token" value="{{csrf_token()}}"/>
<input type="hidden" id="return_id" value="{!!$return_id!!}"/>
<input type="hidden" id="visit_id" value="{!!$return_data['visit_id']!!}"/>
<input type="hidden" id="payment_type" value="{!!$return_data['payment_type']!!}"/>
<input type="hidden" id="advance_type" value="{!!$return_data['advance_type']!!}"/>
<input type="hidden" id="unit_id" value="{!!$return_data['unit_id']!!}"/>
<input type="hidden" id="return_type" value="{!!$return_data['return_type']!!}"/>

    <div class="col-md-12 box-body text-right" style="padding-right:10px !important;">

        <div class="col-md-3 pull-right">
            <h5 style="color:blue;">{{$title}}</h5>
        </div>

    </div>
    <div class="col-md-12 box-body" style="margin-top:8px;">
        <div class="col-md-12">
            <div class="col-md-2">
                UHID :
            </div>
            <div class="col-md-4">
                <input class="form-control" disabled="disabled" value="{{$return_data['uhid']}}" autocomplete="off" type="text" autocomplete="off" id="uhid" name="uhid"/>
            </div>
            <div class="col-md-2">
                Outstanding Amount :
            </div>
            <div class="col-md-4">
                <input class="form-control number_only" disabled="disabled" value="{{$return_data['outstanding_amount']}}" autocomplete="off" type="text"  autocomplete="off"  id="outstanding_amount" name="outstanding_amount" />
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12" style="margin-top:8px;">
            <div class="col-md-2">
                Patient Name :
            </div>
            <div class="col-md-4">
                <input class="form-control" disabled="disabled" value="{{$return_data['patient_name']}}"  autocomplete="off" type="text"  autocomplete="off"  id="patient_name" name="patient_name" />
            </div>

            <div class="col-md-2">
                Return Number :
            </div>
            <div class="col-md-4">
                <input class="form-control" value="{{$return_data['return_no']}}" autocomplete="off" type="text"  autocomplete="off"  id="return_no" name="return_no" />
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12" style="margin-top:8px;">
            <div class="col-md-6"></div>

            <div class="col-md-2">
                Return Amount :
            </div>
            <div class="col-md-4">
                <input class="form-control number_only" value="{{$return_data['return_amount']}}" autocomplete="off" type="text"  autocomplete="off"  id="return_amount" name="return_amount" />
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12" style="margin-top:8px;">
            <div class="col-md-6"></div>

            <div class="col-md-2">
                Advance Amount :
            </div>
            <div class="col-md-4">
                <input class="form-control number_only" disabled="disabled"
                    value="" autocomplete="off" type="text"  autocomplete="off"  id="advance_amount" name="advance_amount"/>
            </div>
        </div>
    </div>

    <div class="col-md-12 box-body" style="margin-top:8px;">
        <div class="col-md-2">
            <label>Payment Mode</label>
            {!! Form::select('payment_mode', $payment_mode, 1, ['class' => 'form-control select2 ', 'id' => 'payment_mode','placeholder'=>'select', 'style' => '']) !!}
        </div>
        <div class="col-md-2">
            <label>Amount</label>
            <input class="form-control number_only" disabled="disabled" value="{{$return_data['return_amount']}}" autocomplete="off" type="text"  autocomplete="off"  id="amount" name="amount" />
        </div>
        <div class="col-md-2">
            <label>Bank</label>
            {!! Form::select('bank', $bank_list, 0, ['class' => 'form-control select2 ', 'id' => 'bank','placeholder'=>'select', 'style' => '']) !!}
        </div>
        <div class="col-md-2">
            <label>Card No</label>
            <input class="form-control" value="" autocomplete="off" type="text"  autocomplete="off"  id="card_no" name="card_no"/>
        </div>
        <div class="col-md-1">
            <label>Exp Month</label>
            <input class="form-control number_only" onkeyup="checkContentLength('exp_month',2);" value="" autocomplete="off" type="text"  autocomplete="off"  id="exp_month" name="exp_month"/>
        </div>
        <div class="col-md-1">
            <label>Exp Year</label>
            <input class="form-control number_only" onkeyup="checkContentLength('exp_year',4);" value="" autocomplete="off" type="text"  autocomplete="off"  id="exp_year" name="exp_year"/>
        </div>
    </div>

    <div class="col-md-12 box-body" style="margin-top:8px;">
        <button type="button" onclick="save_return_payment();" class="btn btn-primary pull-right" id="save_return_payment">
            <i class="fa fa-save"></i> Save</button>
    </div>
