@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/default/css/bootstrap-timepicker.min.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/default/css/collection_dashboard.css")}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">


<style>
.tree-padding{
  --spacing    : 1.5rem;
  --radius     : 10px;
  padding-left : 1rem;
}

.tree-padding li{
  display      : block;
  position     : relative;
  padding-left : calc(2 * var(--spacing) - var(--radius) - 2px);
}

.tree-padding ul{
  margin-left  : calc(var(--radius) - var(--spacing));
  padding-left : 0;
}

.tree-vertical-lines ul li{
  border-left : 2px solid var(--dark-grey);
}

.tree-vertical-lines ul li:last-child{
  border-color : transparent;
}

.tree-horizontal-lines ul li::before{
  content      : '';
  display      : block;
  position     : absolute;
  top          : calc(var(--spacing) / -2);
  left         : -2px;
  width        : calc(var(--spacing) + 2px);
  height       : calc(var(--spacing) + 1px);
  border       : solid var(--dark-grey);
  border-width : 0 0 2px 2px;
}


.tree-markers li::after,
.tree-markers summary::before{
  content       : '';
  display       : block;
  position      : absolute;
  top           : calc(var(--spacing) / 2 - var(--radius));
  left          : calc(var(--spacing) - var(--radius) - 1px);
  width         : calc(2 * var(--radius));
  height        : calc(2 * var(--radius));
  border-radius : 50%;
  background    : var(--dark-grey);
}

.tree-buttons summary::before{
  content     : '+';
  z-index     : 1;
  background  : var(--accent);
  color       : #fff;
  font-weight : 400;
  line-height : calc(2 * var(--radius) - 2px);
  text-align  : center;
}

.tree-buttons details[open] > summary::before{
  content : '−';
}

:root{
  --width      : 36;
  --rounding   : 4px;
  --accent     : #696;
  --dark-grey  : #ddd;
  --grey       : #eee;
  --light-grey : #f8f8f8;
}

ol,
ul{
  padding : 1rem 2rem 0 3rem;
}

ul ul{
  padding : 0 0 0 1rem;
}


button:-moz-focus-inner{
  padding : 0;
  border  : 0;
}

button:where(:active, :focus){
  outline : none;
}

button > span,
.button{
  display             : inline-block;
  padding             : 0 0.5rem;
  border              : 2px solid var(--accent);
  border-radius       : calc(0.75rem + 2px);
  background          : var(--accent);
  color               : #fff;
  font-weight         : 400;
  -webkit-user-select : none;
  user-select         : none;
  cursor              : pointer;
}

button:where(:active, :focus) > span,
.button:where(:active, :focus),

.badge{
    width: 250px;
    height: 30px;
    padding: 8px;
    margin-left:10px;
}
.bg-yellow{
    background-color:yellow;
    color:black;
}

.tile {
  width: 100%;
	display: inline-block;
	box-sizing: border-box;
	background: #fff;
	padding: 10px;
	margin-bottom: 10px;
    min-height:111px !important;
    border-radius:3px !important;
}
.title {
	margin-top: 0px;
}

.round-tile {
    width: 250px;
    display: inline-block;
    box-sizing: border-box;
    border-radius: 5px;
    background: #fff;
    padding: 8px;
    margin-bottom: 1px;
    margin-left: 3px;
    margin-top:10px;
    height: 75px;
}

.round-tile-sub{
    width: 250px;
    display: inline-block;
    box-sizing: border-box;
    border-radius: 5px;
    background: #fff;
    padding: 4px;
    margin-bottom: 1px;
    margin-left: 3px;
    margin-top: 10px;
    height: 55px !important;
}

.round-tile-supersub{
    width: 250px;
    display: inline-block;
    box-sizing: border-box;
    border-radius: 5px;
    background: #fff;
    padding: 5px;
    margin-bottom: 1px;
    margin-left: 3px;
    margin-top: 10px;
    height: 40px;
}
.round-tile-sub_left{
    width: 155px;
    display: inline-block;
    box-sizing: border-box;
    border-radius: 5px;
    background: #fff;
    padding: 5px;
    margin-bottom: 1px;
    margin-left: 3px;
    margin-top: 10px;
    height: 70px !important;
}

.tree-markers li::after{
    visibility: hidden;
}

.right_col > li{
    padding-left :40px !important;
}

.header-bg{
    background: #ffffff!important;
    color:rgb(0, 0, 0) !important;
}
.main-header-bg{
    background: #17a2b8!important;
    color:white !important;
}

.bg-blue{
    background: rgb(45,192,222) !important;
background: linear-gradient(90deg, rgba(45,192,222,1) 0%, rgba(27,164,207,1) 0%, rgba(9,131,209,1) 100%) !important;
}
.bg-green{
background: rgb(45,192,222) !important;
background: linear-gradient(90deg, rgba(45,192,222,1) 0%, rgba(27,207,159,1) 0%, rgba(2,149,80,1) 100%) !important;
}
.bg-orange{
background: rgb(45,192,222) !important;
background: linear-gradient(90deg, rgba(45,192,222,1) 0%, rgba(239,166,49,1) 0%, rgba(209,134,15,1) 100%) !important;
}
img{
    width:40px !important;
    float:left !important;
    clear:right !important;
}

.btn-range_type{
    text-align: left;
    padding-left: 30px;
    height: 40px;
    font-size: 14px;
}

.amount_sub{
    float: right !important;
    margin-right: 100px !important;
}
.label_sub{
    margin-left:20px;
}
.title{
    padding-top: 5px !important;
}

.bottom-border-text{
    border:none !important;
    border-bottom:1px solid lightgrey !important;
    box-shadow:none;
}

</style>



@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="date_datahidden" value='{{$date_array}}'>
<div class="right_col">
    <div class="col-md-12 box-body text-right main-header-bg" style="padding-right:10px !important;">
        <h5>Collection Dashboard</h5>
    </div>

    <div class="col-md-12 no-padding pull-right" style="background-color: lightgray;
    padding: 2px !important;">
        <div class="col-md-10 padding_sm">
            <div class="col-md-4 padding_sm">
                Range Type :<strong><span class='blue' id='range_typedata'></span></strong>
            </div>
            <div class="col-md-3 padding_sm">
                From Date : <strong><span class='red' id='from_datadis'></span></strong>
            </div>
            <div class="col-md-3 padding_sm">
                To Date :<strong><span class='red' id='to_datadis'></span></strong>
            </div>
        </div>
    </div>

    <div class="col-md-12 no-padding range_type">

        <div class="col-md-2 box-body" style="min-height: 549px !important;padding-top:20px !important;">
            <div class="col-md-12 padding_sm">
                <button onclick="getCustomDates(1)" title="Today" class="btn-range_type btn btn-primary btn-block"><i class="fa fa-calendar"></i> Today</button>
            </div>
            <div class="col-md-12 padding_sm">
                <button onclick="getCustomDates(2)" title="Yesterday" class="btn-range_type btn btn-primary btn-block"><i class="fa fa-calendar"></i> Yesterday

                </button>
            </div>
            <div class="col-md-12 padding_sm">
                <button onclick="getCustomDates(3)" title="Week Till Date"
                    class="btn-range_type btn btn-primary btn-block"><i class="fa fa-calendar"></i> Week Till Date
                    </button>
            </div>
            <div class="col-md-12 padding_sm">
                <button onclick="getCustomDates(4)" title="Last Week" class="btn-range_type btn btn-primary btn-block"><i class="fa fa-calendar"></i> Last
                    Week</button>
            </div>
            <div class="col-md-12 padding_sm">
                <button onclick="getCustomDates(5)" title="Month Till Date"
                    class="btn-range_type btn btn-primary btn-block"><i class="fa fa-calendar"></i> Month Till Date</button>
            </div>
            <div class="col-md-12 padding_sm">
                <button onclick="getCustomDates(6)" title="Last Month" class="btn-range_type btn btn-primary btn-block"><i class="fa fa-calendar"></i> Last
                    Month</button>
            </div>
            <div class="col-md-12 padding_sm">
                <button onclick="getCustomDates(7)" title="Custom Date Range Selection"
                    class="btn-range_type btn btn-primary btn-block"><i class="fa fa-calendar"></i> Custom</button>
            </div>
        </div>
        <div class="col-md-4 box-body">
            <ul class="tree-padding tree-vertical-lines tree-horizontal-lines tree-summaries tree-markers tree-buttons">
                <li>
                  <details open="">
                    <summary>
                        <a class="round-tile bg-orange">
                          <div class="col-md-12">
                            <div class="col-md-3">
                              <img src="{{asset("packages/extensionsvalley/dashboard/images/cash_collection.png")}}"/>
                            </div>
                            <div class="col-md-9">
                              <h4>Total Collection</h4>
                              <p style="color: yellow;
                              font-size: 18px;
                              margin-top: -10px !important;" class="colum_val" id="total_collection_val"><i class="fa fa-inr"></i> 00.00</p>
                            </div>
                          </div>
                        </a>
                    </summary>
                    <ul>
                      <li>
                        <details>
                          <summary>
                            <a class="round-tile-sub bg-blue">
                              <div class="col-md-12">
                                <div class="col-md-3">
                                  <img src="{{asset("packages/extensionsvalley/dashboard/images/consultation.png")}}"/>
                                </div>
                                <div class="col-md-9">
                                  <h5 class="title">Consultation Charges</h5>
                                  <p class="colum_val" id="consultation_total_val">00.00</p>
                                </div>
                              </div>
                            </a>
                          </summary>
                          <ul>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">MLC :</span>
                                        <span class="colum_val amount_sub" id="mlc_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">DECC :</span>
                                        <span class="colum_val amount_sub" id="decc_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                          </ul>
                        </details>
                      </li>
                      <li>
                        <details>
                          <summary>
                            <a class="round-tile-sub bg-blue">
                                <div class="col-md-12">
                                  <div class="col-md-3">
                                    <img src="{{asset("packages/extensionsvalley/dashboard/images/services.png")}}"/>
                                  </div>
                                  <div class="col-md-9">
                                    <h5 class="title">Service Bills</h5>
                                    <p class="colum_val" id="service_total_val">00.00</p>
                                  </div>
                                </div>
                            </a>
                          </summary>
                          <ul>
                            <li>
                                <li>
                                    <a class="round-tile-supersub bg-green">
                                        <h5 class="title">
                                            <span class="label_sub">OP :</span>
                                            <span class="colum_val amount_sub" id="op_service_val">00.00</span>
                                        </h5>
                                    </a>
                                </li>
                            </li>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">IP :</span>
                                        <span class="colum_val amount_sub" id="ip_service_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">GP :</span>
                                        <span class="colum_val amount_sub" id="gp_service_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                          </ul>
                        </details>
                      </li>
                      <li>
                        <details>
                          <summary>
                            <a class="round-tile-sub bg-blue">
                                <div class="col-md-12">
                                  <div class="col-md-3">
                                    <img src="{{asset("packages/extensionsvalley/dashboard/images/rent.png")}}"/>
                                  </div>
                                  <div class="col-md-9">
                                    <h5 class="title">Rent</h5>
                                    <p class="colum_val" id="rent_total_val">00.00</p>
                                  </div>
                                </div>
                            </a>
                          </summary>
                        </details>
                      </li>

                      <li>
                        <details>
                          <summary>
                            <a class="round-tile-sub bg-blue">
                                <div class="col-md-12">
                                  <div class="col-md-3">
                                    <img src="{{asset("packages/extensionsvalley/dashboard/images/pharmacy.png")}}"/>
                                  </div>
                                  <div class="col-md-9">
                                    <h5 class="title">Pharmacy</h5>
                                    <p class="colum_val" id="pharmacy_total_val">00.00</p>
                                  </div>
                                </div>
                            </a>
                          </summary>
                          <ul>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">OP :</span>
                                        <span class="colum_val amount_sub" id="op_pharmacy_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">IP :</span>
                                        <span class="colum_val amount_sub" id="ip_pharmacy_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">GP :</span>
                                        <span class="colum_val amount_sub" id="gp_pharmacy_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                          </ul>
                        </details>
                      </li>

                      <li>
                        <details>
                          <summary>
                            <a class="round-tile-sub bg-blue">
                              <div class="col-md-12">
                                <div class="col-md-3">
                                  <img src="{{asset("packages/extensionsvalley/dashboard/images/lab.png")}}"/>
                                </div>
                                <div class="col-md-9">
                                  <h5 class="title">Lab</h5>
                                  <p class="colum_val" id="lab_total_val">00.00</p>
                                </div>
                              </div>
                            </a>
                          </summary>
                          <ul>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">OP :</span>
                                        <span class="colum_val amount_sub" id="op_lab_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">IP :</span>
                                        <span class="colum_val amount_sub" id="ip_lab_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">GP :</span>
                                        <span class="colum_val amount_sub" id="gp_lab_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                          </ul>
                        </details>
                      </li>

                      <li>
                        <details>
                          <summary>
                            <a class="round-tile-sub bg-blue">

                                <div class="col-md-12">
                                  <div class="col-md-3">
                                    <img src="{{asset("packages/extensionsvalley/dashboard/images/radiology.png")}}"/>
                                  </div>
                                  <div class="col-md-9">
                                    <h5 class="title">Radiology</h5>
                                    <p class="colum_val" id="radiology_total_val">00.00</p>
                                  </div>
                                </div>

                            </a>
                          </summary>
                          <ul>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">X-ray :</span>
                                        <span class="colum_val amount_sub" id="xray_val">00.00</span>
                                    </h5>

                                </a>
                            </li>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">CT :</span>
                                        <span class="colum_val amount_sub" id="ct_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                            <li>
                                <a class="round-tile-supersub bg-green">
                                    <h5 class="title">
                                        <span class="label_sub">USG :</span>
                                        <span class="colum_val amount_sub" id="usg_val">00.00</span>
                                    </h5>
                                </a>
                            </li>
                          </ul>
                        </details>
                      </li>

                    </ul>
                  </details>
                </li>
              </ul>
        </div>
        <div class="col-md-6 no-padding">

            <div class="col-md-12 box-body no-padding" style="min-height: 135px !important;">
                <div class="col-md-12 header-bg no-padding" style="text-align: center;background-color: #ffffff !important;height: 30px !important;">
                    <h4 style="color:darkcyan;"><b>PHARMACY</b></h4>
                </div>

                <div class="col-sm-3 padding-sm">
                    <a class="round-tile-sub_left bg-green bg-green">
                      <h5 class="title">Consumable</h5>
                      <p class="colum_val" id="pharmacy_consumable_val">00.00</p>
                    </a>
                </div>
                <div class="col-sm-3 padding-sm">
                    <a class="round-tile-sub_left bg-green">
                      <h5 class="title">Medicine</h5>
                      <p class="colum_val" id="pharmacy_non_consumable_val">00.00</p>
                    </a>
                </div>
            </div>

            <div class="col-md-12 box-body padding-sm no-padding" style="min-height: 549px !important;">
                <div class="col-md-12 header-bg no-padding" style="text-align: center;background-color: #ffffff !important;height: 30px !important;">
                    <h4 style="color:rgb(9, 129, 176);"><b>LAB</b></h4>
                </div>
                <div class="col-sm-12 no-padding" id="lab_data_container">
                    <div class="col-sm-3 no-padding" style="margin-left: 10px;">
                        <span class="round-tile-sub_left bg-blue">
                            <h5 class="title"><i>Lab Particulars</i></h5>
                            <p class="colum_val" id="pharmacy_non_consumable_val">
                                <i class="fa fa-inr"></i> 00.00</p>
                        </span>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="customdatapopmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 500px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Select Dates</h4>
            </div>
            <div class="modal-body" style="min-height: 80px">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-4 date_filter_div">
                        <div class="mate-input-box" style="padding-top:11px !important;height:36px !important;">
                            <label class="filter_label">From Date</label>
                            <input type="text" data-attr="date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="from_date">
                        </div>
                    </div>
                    <div class="col-md-4 date_filter_div">
                        <div class="mate-input-box" style="padding-top:11px !important;height:36px !important;">
                            <label class="filter_label">To Date</label>
                            <input type="text" data-attr="date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="to_date">
                        </div>
                    </div>
                    <div class="col-md-4 date_filter_div pull-right" style="padding-top: 10px;">
                        <button id="searchdatabtn" onclick="getCustomDateRange();"
                            class="btn btn-primary btn-block">Search <i id="searchdataspin"
                                class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/default/plugins/OrgChart/orgchart.js') }}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/collection_dashboard.js")}}"></script>
<script type="text/javascript">

</script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
@endsection
