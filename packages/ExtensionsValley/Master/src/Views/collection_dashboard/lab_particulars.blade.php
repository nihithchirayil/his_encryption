@if($Resultdata != null)
@foreach($Resultdata as $data)
<div class="col-sm-3 padding-sm">
    <a class="round-tile-sub_left bg-blue">
        <h5 class="title">{{ucfirst($data->dept_name)}}</h5>
        <p class="colum_val">{{number_format($data->gross_amount,2, '.', '')}}</p>
    </a>
</div>
@endforeach
@else
<div class="col-sm-3 no-padding" style="margin-left: 10px;">
    <span class="round-tile-sub_left bg-blue">
        <h5 class="title"><i>No Particulars Found!</i></h5>
    </span>
</div>
@endif
