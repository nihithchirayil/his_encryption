<!-- <div class="theadscroll always-visible" style="position: relative; height: 170px;"> -->
<div class="" style="position: relative; height: 200px;">
    <table class="table no-margin theadfix_wrapper table_sm no-border">
        <thead>
            <tr class="" style="background: #80c4ff; color: #474747; ">
                <th style="width: 10%;">Date</th>
                <th style="width: 5%;">Request No.</th>
                <th style="width: 5%;">status</th>
                <th style="width: 15%;">Doctor</th>
                <th style="width: 8%;"></th>
                <th style="width: 3%;">&nbsp;</th>
                <th style="width: 3%;">&nbsp;</th>
                <th style="width: 3%;">&nbsp;</th>
                <th style="width: 3%;">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @if(sizeof($patient_investigation_history)>0)
        @foreach ($patient_investigation_history as $ph)
        @php

            $investigation_detail_content ='';
            $status = ($ph->status == 1) ? "" : "Draft";
            $investigation_count = $ph->investigation_count;
            $investigation_detail_list = \DB::table("patient_investigation_detail")
                    ->where('patient_investigation_detail.head_id',$ph->id)
                    ->leftJoin('service_master','service_master.service_code','=','patient_investigation_detail.service_code')
                    ->whereNull('patient_investigation_detail.deleted_at')
                    ->select(\DB::raw("coalesce(service_master.service_desc, patient_investigation_detail.service_text) as service_desc"))->get();

            if(!empty($investigation_detail_list)){
                foreach($investigation_detail_list as $investigation_data){
                    $investigation_detail_content = $investigation_detail_content.$investigation_data->service_desc.', </br>';
                }
            }

        @endphp
            <tr>
                <td>{{ExtensionsValley\Emr\CommonController::getDateFormat($ph->created_at,'d-M-Y h:i A') }}</td>
                <td>@if($ph->batch_no != '') {{$ph->batch_no}} @else 0 @endif</td>
                <td>@if($ph->bill_converted_status ==1) Converted @else Pending @endif</td>
                <td>@if(isset($ph->doctor_name)) {{$ph->doctor_name}} @else - @endif</td>
                <td>
                    <span class="badge badge_purple_bg">
                        @if(isset($ph->visit_type)) {{$ph->visit_type}} @else - @endif
                    </span>
                    @if(!empty($status))
                        <span class="badge bg-orange">
                            {{$status}}
                        </span>
                    @endif
                </td>
                <td>
                    <button data-toggle="popover" data-trigger="hover" data-content="{{$investigation_detail_content}}" class="btn btn-block light_purple_bg popoverData" rel="popover" data-placement="left" onclick="showInvestigationHistoryList('{{$ph->id}}')" style="border-radius:20px;background-color:aqua;" ><i class="fa fa-eye"></i> View ({{$investigation_count}}) </button>
                </td>
                <td>
                    <button class="btn btn-block light_purple_bg" onclick="printInvestigationHistoryList('{{$ph->id}}')" title="Print" style="border-radius:20px;background-color:rgb(234, 255, 0);"><i class="fa fa-print"></i> Print </button>
                </td>

                <td>
                    @if($ph->bill_converted_status !=1)
                        <button class="btn btn-block light_purple_bg"  onclick="editInvestigationHistoryList('{{$ph->id}}')" title="Edit" style="border-radius:20px;background-color:rgb(255, 213, 0);"><i class="fa fa-edit"></i> Edit</button>
                    @endif
                </td>
                <td>
                    @if($ph->bill_converted_status !=1)
                        <button class="btn btn-block light_purple_bg" onclick="deleteInvestigationHistoryList(this,'{{$ph->id}}')" title="Delete" style="border-radius:20px;background-color:rgb(224, 161, 250);"><i class="fa fa-trash"></i> Delete </button>
                    @endif
                </td>


            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>

@if(!empty($patient_investigation_history))

<div id="pagination">{{{ $patient_investigation_history->links() }}}</div>
@endif
