<div class="col-md-12">
    @php
        $hospital_header = \DB::table('company')->where('id',1)->value('hospital_header');
    @endphp
    {!!$hospital_header!!}
</div>
<div class="col-md-12">
   <h4 style="text-align:center;">CASH RECEIPT</h4></span>
</div>
<div class="col-md-12 box-body" style="margin-top:15px;">
    <table  width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper" style="border-collapse: collapse;line-height: 22px; padding:5px; ">
        <tr>
            <td width="25%">Patient Name</td>
            <td width="2%">:</td>
            <td width="50%">{{$bill_deatil_data[0]->patient_name}}</td>
        </tr>
        <tr>
            <td>UHID</td>
            <td>:</td>
            <td>{{$bill_deatil_data[0]->uhid}}</td>

        </tr>
    </table>
</div>
<div class="col-md-12 box-body" style="margin-top:15px;">
    <table  width="100%;" border="1" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper" style="border-color:rgb(227, 227, 252); border-collapse: collapse;line-height: 22px; padding:5px; ">
        <thead style="background-color: cadetblue;color:white;">
            <tr>
                <td width="20%" style="text-align:center;"><b>Bill Number</b></td>
                <td width="23%" style="text-align:center;"><b>Bill Date</b></td>
                <td width="15%" style="text-align:center;"><b>Receipt Number</b></td>
                <td width="22%" style="text-align:center;"><b>Receipt Date</b></td>
                <td width="10%" style="text-align:center;"><b>Collection Mode</b></td>
                @if($bill_deatil_data[0]->bill_tag=='PB')
                <td width="10%" style="text-align:center;"><b>Package Amount</b></td> 
                @else
                <td width="10%" style="text-align:center;"><b>Bill Amount</b></td> 
                @endif
                <td width="10%" style="text-align:center;"><b>Discount Amount</b></td>
                <td width="10%" style="text-align:center;"><b>Paid Amount</b></td>
               
               
            </tr>
        </thead>
        <tbody style="color:rgb(42, 51, 51);">
            @if(sizeof($bill_deatil_data)>0)
            @php
                $billtagdesc = '';
                $total_paid_amount = 0;
            @endphp
                @foreach($bill_deatil_data as $key=>$value)
                    @if($billtagdesc != $value->billtagdesc)
                        <tr>
                            <td colspan="8">{{$value->billtagdesc}}</td>
                        </tr>
                        @php
                            $billtagdesc = $value->billtagdesc;
                        @endphp
                    @endif
                    <tr>
                        <td>{{$value->bill_no}}</td>
                        <td>{{date('M-d-Y h:i A',strtotime($value->bill_datetime))}}</td>
                        <td>{{$value->recipt_no}}</td>
                        <td>{{date('M-d-Y h:i A',strtotime($value->cash_collected_date))}}</td>
                        <td>{{$value->collectionmode}}</td>
                        @if($bill_deatil_data[0]->bill_tag=='PB')
                        <td>{{@$value->package_amt ? $value->package_amt : '0.00' }}</td>
                        <td style="text-align: right !important;">{{@$value->package_dis ? $value->package_dis : '0.00' }}</td>
                        <td style="text-align: right !important;">{{number_format($value->paid_amount,2)}}</td>
                        @else
                        <td>{{@$value->bill_amount ? $value->bill_amount : '0.00' }}</td>
                        <td style="text-align: right !important;">{{@$value->discount_amount ? $value->discount_amount : '0.00' }}</td>
                        <td style="text-align: right !important;">{{number_format($value->paid_amount,2)}}</td>
                        @endif
                        
                    </tr>
                        @php
                            $total_paid_amount += $value->paid_amount;
                        @endphp
                @endforeach
                    <tr style="background-color:rgb(216, 216, 216);">
                        <td colspan="7">Total Paid Amount</td>
                        <td style="text-align: right !important;">{{number_format($total_paid_amount,2)}}</td>
                    </tr>

            @endif
        </tbody>
    </table>
</div>
<div class="col-md-12 box-body" style="margin-top:15px;">

        <table  width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper" style="border-collapse: collapse;line-height: 22px; padding:5px; ">
            <tr>
                <td width='23%'>Printed User</td>
                <td width='2%'>:</td>
                <td width='25%'>{{$user_name}}</td>
                <td width='50%' style='text-align:right;'> -------------------- </td>
            </tr>
            <tr>
                <td>Printed Date</td>
                <td>:</td>
                <td>{{$bill_deatil_data[0]->printdate}}</td>
                <td style='text-align:right;'>Signature</td>

            </tr>
        </table>

    </div>
</div>
