 {{-- <style>
     body {
         font: 9pt sans-serif;
         line-height: 1.3;

         /* Avoid fixed header and footer to overlap page content */
         /* margin-top: 100px; */
         /* margin-bottom: 50px; */
     }

     /* .header_data { */
         /* position: fixed; */
         /* top: 0; */
         /* width: 100%; */
         /* height: 100px; */
         /* For testing */
     /* } */

     .footer_data {
         position: fixed;
         bottom: 0;
         width: 100%;
         /* height: 50px; */
         font-size:12px;
         color: #777;
         /* For testing */


     }
     #head_detail  td{
         font-size: 12px;
     }
     .body_data  td{
         font-size: 12px;
     }
     .print_inner{
  
  border: 1px solid black;
  border-collapse: collapse;
  height: 100px;
  width:100px;

     }


 </style>
 
 
 <table>
     <thead>
         <div class="header_data">
             <div style="border-style: dotted; border-width: 1px; border-height: 1px;"> </div>
             @if ($package > 0)
                 <div style="text-align:center;font-size : 20px;"><strong>PACKAGE BILL</strong></div>
             @else
                 <div style="text-align:center;font-size : 20px;">{{ $patient_details[0]->bill_tag }}
                 </div>
             @endif

             <div style="border-style: dotted; border-width: 1px; border-height: 1px;"> </div>

             <table style="width: 100%;">
                 <tbody id='head_detail'>
                     <tr style="width: 40%;">
                         <td><strong>PATIENT NAME </strong></td>
                         <td>: {{ $patient_details[0]->patient_name }} </td>
                         <td class="float-right"><strong> GENDER/AGE </strong> </td>
                         <td>
                             : {{ $patient_details[0]->gender }}/{{ $patient_details[0]->age }}
                         </td>
                     </tr>
                     <tr>
                         <td> <strong>UHID </strong> </td>
                         <td>: {{ $patient_details[0]->uhid }} </td>
                         <td class="pull-right"><strong> IP NUMBER </strong> </td>
                         <td>: {{ $patient_details[0]->ip_no }}</td>
                     </tr>
                     <tr style="">
                         <td><strong> ADDRESS </strong> </td>
                         <td>: {{ $patient_details[0]->address }}</td>
                         <td class="pull-right"><strong> PAYMENT TYPE</strong> </td>
                         <td>: {{ $patient_details[0]->payment_type }}</td>
                     </tr>
                     <tr>
                         <td><strong>BILL NUMBER </strong></td>
                         <td> : {{ $patient_details[0]->bill_no }} </td>
                         <td class="pull-right"><strong> BILL DATE </strong></td>
                         <td>: {{ date('M-d-Y', strtotime($patient_details[0]->created_at)) }}</td>
                     </tr>
                     <tr>
                         <td><strong>LOCATION </strong> </td>
                         <td>: {{ $patient_details[0]->location_name }} </td>
                         <td class="pull-right"><strong> INDENT LOCATION</strong></td>
                         <td> : </td>
                     </tr>
                     <tr>
                         <td><strong>DOCTOR </strong> </td>
                         <td>: {{ $patient_details[0]->consulting_doctor_name }} </td>
                         <td class="pull-right"><strong> SPONSOR</td>
                         <td> </strong>: DAYA</td>
                     </tr>

                 </tbody>

             </table>
         </div>
     </thead>
     <tfoot>
        <div class="footer_data">
            <div style="border-top: dotted; border-width: 1px; border-height: 1px;"></div> 
            @if (!empty($printout_footer_message))
                <tr>
                    <td style='padding: 3px; vertical-align: middle;' width='15%' align='center' colspan="2">
                        <span
                            style="float: right; margin-right: 180px; font-size: 11px;"><strong>{!! $printout_footer_message !!}</strong></span>
                    </td>
                </tr>
            @endif
            <div style="margin:10px;">
                Bill Created By:{{ $patient_details[0]->created_by }} at
                {{ date('M-d-Y h:i A', strtotime($patient_details[0]->created_at)) }}<br>
                Bill Printed by :{{ $user }} Printed Date:{{ date('M-d-Y h:i A') }} Printed
                Location:{{ $location_name }}
            </div>


        </div>

        <div style="border-top: dotted; border-width: 1px; border-height: 1px;"></div>
    </tfoot>
 </table>
 <div class="body_data" style="width: 100%;height:200px;">

    <body>
        
        <tbody>
            <div style="border-style: dotted; border-width: 1px; border-height: 1px; "> </div>
            <table class="content_detail" style="width: 100%; margin:0px; ">
                
                <thead>
                    <tr>
                        <th style="width:5%">#</th>
                        <th style="width:35%">Particulars</th>
                        <th style="width:10%">Qty</th>
    
                        <th style="width:15%">Price</th>
                        {{-- <th style="width:15%">MRP</th> --}}
                        {{-- <th style="width:15%">Amount</th>
                    </tr>
                  
                </thead>
                
                @if ($package > 0)
                    <tbody style="height:200px;">
                        @php
                            $i = 1;
                        @endphp
                        <tr class="text-center">
                            <td style="text-align: center">{{ $i }}</td>
                            <td style="text-align: left">{{ $package_details[0]->package_name }}</td>
    
                            <td style="text-align: center">1</td>
    
                            <td style="text-align: center">
                                {{ $package_details[0]->pricing_discount_amount }}</td>
                            {{-- <td style="text-align: center">{{ $package_details[0]->net_amount_wo_roundoff }}
                            </td> --}}
                            {{-- <td style="text-align: center">{{ $package_details[0]->net_amount_wo_roundoff }}
                            </td>
                        </tr>
                        <tr></tr>
                    </tbody>
                @else
                    <tbody>
                        @if (isset($bill_data_detail))
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($bill_data_detail as $item)
                                <tr class="text-center">
                                    <td style="text-align: center">{{ $i }}</td>
                                    <td style="text-align: left">{{ $item->item_desc }}</td>
    
                                    <td style="text-align: center">{{ $item->qty }}</td>
    
                                    <td style="text-align: center">{{ $item->pricing_discount_amount }}</td>
                                    {{-- <td style="text-align: center">{{ $item->net_amount }}</td> --}}
                                    {{-- <td style="text-align: center">{{ $item->net_amount }}</td> --}}
                                {{-- </tr>
                                @php
                                    ++$i;
                                @endphp
                            @endforeach
                        @endif
                        <tr></tr>
                    </tbody>
                @endif
    
            </table>
    
            <div style="border-top: dotted; border-width: 1px; border-height: 1px;"></div>
            <table style="width:100%;margin:10px;">
                <tbody>
    
                    <tr>
                        <td>
                            <table class="" style="margin: 10px;height:50px; width:100px;">  --}}
                                {{-- <tr class="print_inner" style="margin: 10px;height:50px; width:100px;"> --}}
                                {{-- <td></td>
                                   <td>5%</td>
                                   <td>12%</td>
                                   <td>18%</td>
                                   <td>28%</td> --}}
                                {{-- </tr> --}}
    
                                {{-- <tbody class="print_inner">
                                   <tr>
                                       <td></td>
                                   </tr>
                                   <tr>
                                       <td>CGST</td>
    
                                   </tr>
                                   <tr>
                                       <td>SGST</td>
                                   </tr>
                               </tbody> --}}
                            {{-- </table>
    
                        </td>
                        <td  style="width:40%;">
    
                            <table>
    
                                <body>
                                    <tr>
                                        <td>BILL TOTAL</td>
                                        <td>:{{ $patient_details[0]->bill_amount }}</td>
                                    </tr>
                                    <tr>
                                        <td>DISCOUNT</td>
                                        <td>:{{ $patient_details[0]->discount }}</td>
                                    </tr>
                                    <tr>
                                        <td>ADDITIONAL DISCOUNT</td>
                                        <td>:{{ $patient_details[0]->discount }}</td>
                                    </tr>
                                    <tr>
                                        <td>NET AMOUNT</td>
                                        <td>:{{ $patient_details[0]->net_amount }}</td>
                                    </tr>
                                </body>
                            </table>
                        </td>
                        <td >
                            <table class="print_inner"  style="height:80px;margin-inline: auto;text-align:center;">
                                <thead>
                                    <tr>
                                        <td>TOTAL AMOUNT</td>
                                    </tr>
                                </thead>
                                <tbody class="print_inner">
    
                                    <tr>
                                        <td style="font-size: 15px;">{{ $patient_details[0]->net_amount }}</td>
                                    </tr>
    
                                </tbody>
    
                            </table>
                           <div style="margin: 10px;"> {{ $net_amount_in_alphabet }}</div>
                        </td>
                    </tr>
                </tbody>
            </table>
    
        </tbody>
    </body>  --}}

<!-- .css -->
<!-- .css -->
<style>
    .footer {
         /* position: fixed; */
         bottom: 0;
         width: 100%;
     }
    #doctorHeadClass p:first-child{
        margin: 0 !important;
        padding: 0 !important;
    }
    .number_class {
        text-align: right;
    }
    @media print
    {
        table{ page-break-after:auto; }
        tr    { page-break-inside:auto; page-break-after:auto }
        td    { page-break-inside:auto; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
    }
    /* #hospital_header img{
        height:25px!important;
        width:25px!important;

    } */
    /* #hospital_header table{
        font-size: 12px!important;
    } */
    /* #hospital_header td{
        font-size: 9px!important;
        
    } */
    /* #hospital_header b{
        font-size: 9px!important;
        
    } */

    .patient_head {
    font-size: 10px !important;
}
#hospital_header h4{
    margin: 0px;
}

</style>
<style type="text/css" media="print">
    table {
        font-size : 12px;
    }
    @page{
        margin-left:15px;
        margin-right:15px;
        margin-top:40px; 
         margin-bottom:35px;
    }
    
     


</style>

<div class="box-body">
    <div class="col-md-12">
        <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse; " width="100%"  border="1" cellspacing="0" cellpadding="0">
            <thead>

                @if((string)$hospital_header_status == "1")
                <tr >
                    <th id="hospital_header" colspan="10"  >
                     {!!$hospitalHeader!!}
                    </th>
                </tr>
                @endif
                <tr>
                    
                
                    <th colspan="9"  style="font-size: 9px!important;" >
                        @if ($package > 0)
                        <center>
                            <strong>PACKAGE BILL</strong>
                            <span style="text-align:right; ">({{ $patient_details[0]->payment_type }})</span>
                        </center>
                        @else
                        <center>
                            <strong>{{ strtoupper($patient_details[0]->bill_tag) }}  {{ @$is_duplicate ? $is_duplicate : ' ' }}</strong>
                            <span style="text-align:right; ">({{ $patient_details[0]->payment_type }})</span>
                        </center>
                        @endif
                    </th>
                </tr>
                <tr>
                    <th colspan="9" style="@if((string)$hospital_header_status == '0') margin-top: 3cm; @endif" >
                        <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;  " width="100%" border="1" cellspacing="0" cellpadding="0">
                            <thead class="patient_head" style="display: table-header-group;">
                           
                                <tr>
                                    <td><strong>PATIENT NAME</strong></td>
                                    <td>{{ strtoupper($patient_details[0]->patient_name) }}</td>
                                    <td><strong>GENDER/AGE</strong></td>
                                    <td>{{ strtoupper($patient_details[0]->gender) }}/{{ $patient_details[0]->age }}</td>
                                    <td><strong>BILL NUMBER</strong></td>
                                    <td>{{ $patient_details[0]->bill_no }}</td>
                                </tr>
                              
                                <tr>
                                    <td><strong>UHID</strong></td>
                                    <td>{{ $patient_details[0]->uhid }}</td>
                                    <td ><strong> IP NUMBER </strong> </td>
                                    <td>{{ $patient_details[0]->ip_no }}</td>
                                    <td><strong>BILL DATE</strong></td>
                                    <td>{{ date('M-d-Y h:i A', strtotime($patient_details[0]->bill_datetime)) }}</td>
                                </tr>
                                <tr>
                                    <td><strong>DOCTOR </strong> </td>
                                    <td>{{ $patient_details[0]->consulting_doctor_name }}</td>
                                    <td><strong>LOCATION</strong></td>
                                    <td  colspan="3">{{ @$location_name!='Select Location' ? strtoupper($location_name) : '' }}</td>
                                    {{-- <td class="pull-right"><strong> SPONSOR</td> --}}
                                    {{-- <td></strong>{{ $patient_details[0]->company_name }}</td> --}}
                                </tr>
                            
                               
                            </thead>
                        </table>
                    </th>
                </tr>

                <tr>
                    <th colspan="9">
                    @if($bill_tag=='CONS')
                    <table
                    style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;font-size: 12px;"
                    width="100%" border="1" cellspacing="0" cellpadding="0">
                    <thead>
                        
                                        <tr>
                                            <th style="width:5%"><strong>#</strong></th>
                                            <th style="width:55%;text-align: center"><strong>Particular</strong></th>
                                            <th style="width:55%;text-align: center"><strong>Amount</strong></th>
                                           
                                        </tr>
                                
                    </thead>
                </table>
                    @elseif($bill_tag=='PB')
                    <table
                    style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;font-size: 12px;"
                    width="100%" border="1" cellspacing="0" cellpadding="0">
                    <thead>
                             @if ($details_flag_enabled_in_print_pb==1)
                             <tr>
                                <th width="5%"><strong>#</strong></th>
                                <th style="text-align: center" width="95%"><strong>Package Name({{ $package_details[0]->package_name }})</strong></th>
                                {{-- <td style="text-align: center" width="25%"><b>Package Amount</b></td> 
                                <td style="text-align: center" width="25%"><b>Discount Amount</b></td>
                                <th style="text-align: center" width="20%"><strong>Amount</strong></th>
                                --}}
                            </tr>
                             @else
                                   <tr>
                                <th width="5%"><strong>#</strong></th>
                                <th style="text-align: center" width="25%"><strong>Package Name</strong></th>
                                <td style="text-align: center" width="25%"><b>Package Amount</b></td> 
                                <td style="text-align: center" width="25%"><b>Discount Amount</b></td>
                                <th style="text-align: center" width="20%"><strong>Amount</strong></th>
                               
                            </tr>
                             @endif
                                      
                                
                    </thead>
                </table>
                    @else
                    <table
                    style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;font-size: 12px;"
                    width="100%" border="1" cellspacing="0" cellpadding="0">
                    <thead>
                        
                                        <tr>
                                            <th style="width:5%"><strong>#</strong></th>
                                            <th style="width:55%;text-align: center"><strong>Particular</strong></th>
                                            <th style="width:15%;text-align: center"><strong>Qty</strong></th>
                                            {{-- <th style="width:10%"><strong>Tax%</strong></th> --}}
                                            <th style="width:15%;text-align: center"><strong>Price</strong></th>
                                            <th style="width:10%;text-align: center"><strong>Amount</strong></th>
                                        </tr>
                                
                    </thead>
                </table>
                    @endif
                      
                    </th>
                </tr>
            </thead>

            <tbody>
                @php
                $i=1;
                @endphp
                @if ($package > 0)
                @php
                 $i=1;
                @endphp
                @if ($details_flag_enabled_in_print_pb==1)
                @foreach ($package_details as $rows)

                <tr class="text-center">
                    <td width="5%"  class="number_class">{{ $i }}</td>
                    <td  width="95%" style="text-align: left;padding-left:5px;">{{ $rows->service_desc }}</td>
                    {{-- <td  width="25%" class="number_class">{{@$rows->package_amt ? $rows->package_amt : '0.00' }}</td>
                    <td  width="25%" class="number_class">{{@$rows->package_dis ? $rows->package_dis : '0.00' }}</td>
                    <td  width="20%" class="number_class">{{number_format($rows->net_amount,2)}}</td>
                    --}}
                </tr>
                @php
                    $i++;
                @endphp  
                @endforeach
                @else
                <tr class="text-center">
                    @php
                        $package_amt=0;
                        $package_dis=0;
                    @endphp
                    @foreach ($package_details as $rows)
                    @php
                        $package_amt+=$rows->package_amt;
                        $package_dis+=$rows->package_dis;
                    @endphp
                    @endforeach
                    <td width="5%"  class="number_class">{{ $i }}</td>
                    <td  width="25%" style="text-align: left;padding-left:5px;">{{ $package_details[0]->package_name }}</td>
                    <td  width="25%" class="number_class">{{@$package_amt ? $package_amt : '0.00' }}</td>
                    <td  width="25%" class="number_class">{{@$package_dis ? $package_dis : '0.00' }}</td>
                    <td  width="20%" class="number_class">{{number_format($package_details[0]->net_amount,2)}}</td>
                   
                </tr>
                @endif
              
            


                @elseif($bill_tag=='CONS')
                @foreach ($bill_data_detail as $data)
                <tr>
                <td width="5%" class="number_class">{{ $loop->iteration }}</td>
                    <td width="55%" style="text-align:left;padding-left:5px;">{{ $data->item_desc }}</td>
                    <td width="10%" class="number_class">{{ $data->net_amount }}</td>
                </tr>
                @endforeach
                @else
                @foreach ($bill_data_detail as $data)
                <tr>
                    <td width="5%" class="number_class">{{ $loop->iteration }}</td>
                    <td width="55%" style="text-align:left;padding-left:5px;">{{ $data->item_desc }}</td>
                    <td width="15%" class="number_class ">{{ $data->qty }}</td>
                    {{-- <td width="10%" class="number_class ">{{ $data->tax_rate }}</td> --}}
                    <td width="15%" class="number_class">{{ $data->net_amount }}</td>
                    <td width="10%" class="number_class">{{ $data->net_amount }}</td>
                    
                </tr>
                
            @endforeach
            @endif
                <tr>
                    <th colspan="6">
                        <div class="total_display">
                            <div style="width: 350px;height: 76px;float: left;margin-top: 3px;">
                            </div>
                            <table style="font-size:10px;float:left; width: 200px;">
                                <tr>
                                    <td><strong>BILL TOTAL</strong></td>
                                    <td>:{{ $patient_details[0]->bill_amount }}</td>
                                </tr>
                                <td><strong>BILL DISCOUNT</strong></td>
                                <td>:{{$patient_details[0]->discount_amount}}</td>
                                <tr>
                                    <td><strong>NET AMOUNT</strong></td>
                                    <td>:{{ $patient_details[0]->net_amount }}</td>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2"><strong>{{ $net_amount_in_alphabet }}</strong></td>
                                </tr>

                            </table>
                            <table style="border: 1px solid;width: 130px;height: 60px;margin-top: 3px;">
                                <tr>
                                    @if ($patient_details[0]->paid_status==1)
                                    <td style="text-align: center;"><strong>Bill Amount</strong></td>
     
                                    @else
                                    <td style="text-align: center;"><strong>Amount To Pay</strong></td>
 
                                    @endif
                                </tr>
                                <tr>
                                    <td style="text-align: center; font-size: 18px;">
                                        <strong>{{ round($patient_details[0]->net_amount ) }}</strong>
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </th>
                </tr>

                <tr>
                    <th colspan="9" style="width:90%;position:fixed;bottom:0px;">
                        <div class="footer">
                            @php
                                $hed_crt = $patient_details[0]->created_at;
                            @endphp
                            <p style="font-size: 8px;border-top:0px solid;margin: 0px;">Bill Created
                                By:{{ $patient_details[0]->created_by }}, at
                                {{ date('d-m-Y h:i A', strtotime($hed_crt)) }}
                            
                         Bill Printed
                                By:{{ $user }}-Print
                                Date:{{ date('d-m-Y') }}-Printed Location:{{ strtoupper($location_name) }}</p>
                        </div>
                    </th>
                </tr>

            </tbody>
           
            

        </table>
    </div>
    
</div>

   


 
 
