      
<div class="theadscroll always-visible" style="position: relative; height:400px;">
    <table class="table theadfix_wrapper no-margin table-striped table-bordered table-condensed">
        <thead class="table_header_common">
            <tr>
                <th>Sl.No.</th>
                <th></th>
                <th>Package Name</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody class="package_head_table_body">
                @if (isset($package_list))
                @foreach($package_list as $each)
                <tr class="search_package_head_class">
                    <td style="width:5%" title="{{$loop->iteration}}" class="common_td_rules serial_no">{{$loop->iteration}}</td>
                    <td style="width:5%" title=""><input type="radio" class="select_package common_td_rules"  data-package-id="{{$each->id}}" data-package-name="{{$each->package_name}}" data-package-amount="{{$each->package_amount}}" name="select_package"></td>
                    <td title="{{$each->package_name}}" class="common_td_rules">{{$each->package_name}}</td>
                    <td style="width:15%" title="{{$each->package_amount}}" class="number_class">{{$each->package_amount}}</td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="10">No Data Found..!</td>
                </tr>
                @endif
        </tbody>
    </table>
</div>