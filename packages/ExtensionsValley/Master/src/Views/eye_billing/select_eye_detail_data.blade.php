@php
    $i = 1;
    $prescriptionJson=$select_investigation_head_data[0]->prescription_json;
    $prescriptionData = json_decode($prescriptionJson, true);
    $glassPrescriptionRight = $prescriptionData['glass_prescription_right'];
    $glassPrescriptionLeft= $prescriptionData['glass_prescription_left'];
    $indexValues = array_keys($glassPrescriptionLeft);
@endphp

<table s class="table theadfix_wrapper no-margin table-striped table-bordered table-condensed">
    <thead >
        <tr class="table_header_common">
                   <th>Glass</th>
            @foreach ($indexValues as $row)
                    <th>{{ $row }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Glass Prescription Right</td>
            @foreach ($glassPrescriptionRight as $row)
                   <td>{{ $row }}</td>
            @endforeach
        </tr>
        <tr>
            <td>Glass Prescription Left</td>
            @foreach ($glassPrescriptionLeft as $row)
                   <td>{{ $row }}</td>
            @endforeach
        </tr>
    </tbody>
</table>
