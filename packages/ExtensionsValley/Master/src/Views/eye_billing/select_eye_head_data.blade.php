

<div class="theadscroll always-visible" style="position: relative; height: 254px;">
    <table class="table theadfix_wrapper no-margin table-striped table-bordered table-condensed">
        <thead class=" table_header_common">
            <tr>
                <th>Sl.No.</th>
                <th>Select</th>
                <th>Action</th>
                <th>UHID</th>
                <th>Patient Name</th>
                {{-- <th>Type</th> --}}
                <th>Doctor</th>
                {{-- <th>Visit Type</th> --}}
                <th>Created Date</th>
                
            </tr>
        </thead>
        <tbody >
            @php
                $bill_converted_status=0;
            @endphp
            @if(count($select_investigation_head_data)>0)
            @foreach($select_investigation_head_data as $investigation)
            <tr data-invest-id="{{ $investigation->head_id }}" data-patient-id="{{ $investigation->patient_id }}"  data-doctor-id="{{ $investigation->doctor_id }}" data-patient-uhid="{{ $investigation->uhid }}"  style="@if($bill_converted_status == 1) background: #a2d296; @endif">
                <td class="common_td_rules" title="{{ ($select_investigation_head_data->currentPage() - 1) * $select_investigation_head_data->perPage() + $loop->iteration }}">{{ ($select_investigation_head_data->currentPage() - 1) * $select_investigation_head_data->perPage() + $loop->iteration }}</td>
                <td class="common_td_rules" title="@if($bill_converted_status == 1) Already converted as bill. @endif">
                    <input type="checkbox" autocomplete="off" @if($bill_converted_status == 1 ) readonly="readonly" disabled="disabled" @endif class="padding_sm invest_select_check" name="invest_select_check" value="" required>
                </td>
                <td class="common_td_rules">
                    <div style="display: flex; ">
                        <button class="btn btn-sm btn-primary fetchInvestigationDetailBtn" type="button" onclick="fetchInvestigationDetail('{{$investigation->head_id}}')"><i class="fa fa-eye"></i></button>
                        {{-- <button class="btn btn-sm btn-primary" type="button" onclick="printPatientInvestigation('{{$investigation->head_id}}')"><i class="fa fa-print"></i></button> --}}
                    </div>
                </td>
                <td class="common_td_rules" title="{{ $investigation->uhid }}">{{ $investigation->uhid }}</td>
                <td class="common_td_rules" title="{{ $investigation->patient_name }}">{{ $investigation->patient_name }}</td>
                <td class="common_td_rules" title="{{ $investigation->doctor_name }}">{{ $investigation->doctor_name }}</td>
                <td class="common_td_rules" title="{{ date('M-d-Y h:i A', strtotime($investigation->prescription_datetime)) }}">{{ date('M-d-Y h:i A', strtotime($investigation->created_at)) }}</td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="9">No records found..!</td>
            </tr>
            @endif
            
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>