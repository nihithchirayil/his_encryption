<div class="modal fade" id="getPatientListModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 100%">
        <div class="modal-content">
            <div class="modal-header table_header_common" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Advance Patient Search</h4>
            </div>
            <div class="modal-body" style="min-height: 550px">
                <div class="row padding_sm">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>UHID</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_uhid">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Patient Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_name">
                            </div>
                        </div>
                        {{-- <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>IP No.</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_ipno">
                            </div>
                        </div> --}}
                        {{-- <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Visit Type</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" id="searchpatient_type">
                                    <option value="All">Select</option>
                                    <option value="OP">OP</option>
                                    <option value="IP">IP</option>
                                </select>
                            </div>
                        </div> --}}
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Mobile No.</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_mobile">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Email</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_email">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Address</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_address">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Country</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_country">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>State</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_state">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Area</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_area">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>PinCode</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_pincode">
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 25px">
                            <button type="button" class="btn btn-warning btn-block"
                                onclick="resetAdvancePatientSearch()">Reset <i class="fa fa-recycle"></i>
                            </button>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 25px">
                            <button type="button" class="btn btn-primary btn-block advancePatientSearchBtn"
                                onclick="advancePatientSearch(2)">Search <i
                                    class="fa fa-search advancePatientSearchSpin"></i>
                            </button>
                        </div>
                    </div>
                    

                    <div class="col-md-12 padding_sm theadscroll" id="getPatientListModelDiv" style="margin-top: 15px;min-height:391px;">

                    </div>

                </div>

            </div>
            <div class="modal-footer" style="margin-top: -40px;">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/eyeAdvancePatientSearch.js') }}"></script>
