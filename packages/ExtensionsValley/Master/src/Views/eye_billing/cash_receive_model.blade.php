<div class="modal" tabindex="-1" role="dialog" id="modalCashRecive">
    <div class="modal-dialog" role="document" style="width:98%;">
        <div class="modal-content">
            <div class="modal-header table_header_common">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h3 class="modal-title">Receive Bill</h3>
            </div>
            <div class="modal-body" style="min-height:470px;" id="modalCashReciveBody">

            </div>
        </div>
    </div>
</div>
