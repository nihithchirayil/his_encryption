@php
$page = $patient_list->currentPage();
$per_page = $patient_list->perPage();
$set_sl_no = $per_page * $page - $per_page + 1;

@endphp
<div class="col-md-12" style="width: 143%">
    <table class="table no-margin table_sm table-striped no-border styled-table " style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_common">
                <th class="common_td_rules" width="2%">Sl.No.</th>
                <th class="common_td_rules" width="10%">UHID</th>
                <th class="common_td_rules" width="10%">Patient Name</th>
                <th class="common_td_rules" width="5%">Gender</th>
                {{-- <th class="common_td_rules" width="10%">IP No.</th> --}}
                {{-- <th class="common_td_rules" width="5%">Type</th> --}}
                {{-- <th class="common_td_rules" width="5%">Pricing</th> --}}
                <th class="common_td_rules" width="5%">Registration Datetime</th>
                <th class="common_td_rules" width="5%">Created By</th>
                <th class="common_td_rules" width="10%">Consulting Doctor</th>
                <th class="common_td_rules" width="5%">Last Visit Date</th>
                <th class="common_td_rules" width="10%">Mobile No.</th>
                <th class="common_td_rules" width="10%">Email</th>
                <th class="common_td_rules" width="15%">Address</th>
                <th class="common_td_rules" width="10%">Area</th>
                <th class="common_td_rules" width="5%">PinCode</th>
                <th class="common_td_rules" width="5%">Country</th>
                <th class="common_td_rules" width="5%">State</th>
            </tr>
        </thead>
        <tbody>
            <?php
                    if (count($patient_list) != 0) {
                        foreach ($patient_list as $list) {
                            ?>
            <tr style="cursor: pointer" onclick="selectPatient(1,<?= $list->patient_id ?>)">
                <td class="common_td_rules">{{ $set_sl_no }}</td>
                <td class="common_td_rules" title="{{ $list->uhid ? $list->uhid : '-' }}">
                    {{ $list->uhid ? $list->uhid : '-' }}</td>
                <td class="common_td_rules">{{ $list->patient_name ? $list->patient_name : '-' }}</td>
                <td class="common_td_rules">{{ $list->gender ? $list->gender : '-' }}</td>
                {{-- <td class="common_td_rules">{{ $list->admission_no ? $list->admission_no : '-' }}</td> --}}
                {{-- <td class="common_td_rules">{{ $list->current_visit_type }}</td> --}}
                {{-- <td class="common_td_rules">{{ $list->pricing_name }}</td> --}}
                <td class="common_td_rules"
                    title="{{ $list->registration_datetime != '' ? date('M-d-Y h:i A', strtotime($list->registration_datetime)) : ' ' }}">
                    {{ $list->registration_datetime != '' ? date('M-d-Y h:i A', strtotime($list->registration_datetime)) : '-' }}
                </td>
                <td class="common_td_rules" title="{{ $list->created_by }}">{{ $list->created_by }}</td>
                <td class="common_td_rules" title="{{ $list->consulting_doctor }}">{{ $list->consulting_doctor }}
                </td>
                <td class="common_td_rules"
                    title="{{ $list->last_visit_datetime != '' ? date('M-d-Y h:i A', strtotime($list->last_visit_datetime)) : ' ' }}">
                    {{ $list->last_visit_datetime != '' ? date('M-d-Y h:i A', strtotime($list->last_visit_datetime)) : '-' }}
                </td>
                <td class="common_td_rules">{{ $list->phone ? $list->phone : '-' }}</td>
                <td class="common_td_rules">{{ $list->email ? $list->email : '-' }}</td>
                <td class="common_td_rules" title="{{ $list->address ? $list->address : '-' }}">
                    {{ $list->address ? $list->address : '-' }}</td>
                <td class="common_td_rules">{{ $list->area ? $list->area : '-' }}</td>
                <td class="common_td_rules">{{ $list->pincode ? $list->pincode : '-' }}</td>
                <td class="common_td_rules">{{ $list->country ? $list->country : '-' }}</td>
                <td class="common_td_rules">{{ $list->state ? $list->state : '-' }}</td>
            </tr>
            @php
                $set_sl_no++;
            @endphp
            <?php
                        }
                    }else{
                        ?>
            <tr>
                <td style="text-align: center" colspan="11" class="re-records-found">No Records Found</td>
            </tr>
            <?php
                    }
                    ?>

        </tbody>
    </table>
</div>


<div class="clearfix"></div>
<div class="col-md-12 padding_sm text-center" style="margin-top:-12px;">
    <ul class="pagination purple_pagination pull-right">
        {!! $page_links !!}
    </ul>
</div>
