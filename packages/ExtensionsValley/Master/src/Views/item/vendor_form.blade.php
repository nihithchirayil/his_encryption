@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style> 
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.item.listVendor')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Vendor Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="vendor_code" 
                                    value="{{ $searchFields['vendor_code'] ?? '' }}">
                                </div>
                            </div>
    
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Vendor Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="vendor_name" 
                                    value="{{ $searchFields['vendor_name'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                {!! Form::select('status', array("-1"=> " Select Status","1"=> " Active", "0"=> " Inactive"),$searchFields['status'] ?? '', [
                                            'class'       => 'form-control',
                                        ]) !!}
                                    </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Vendor Code</th>
                                    <th>Vendor Name</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                    <th>Contact Person</th>
                                    <th>Contact Number</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($vendor_list) > 0)
                                    @foreach ($vendor_list as $vendor)
                                        <tr style="cursor: pointer;" onclick="vendorEditLoadData(this,
                                            '{{$vendor->id}}',
                                            '{{$vendor->vendor_code}}',
                                            '{{$vendor->vendor_name}}', 
                                            '{{$vendor->address_1}}', 
                                            '{{$vendor->address_2}}',
                                            '{{$vendor->address_3}}',
                                            '{{$vendor->city}}',
                                            '{{$vendor->state}}',
                                            '{{$vendor->country}}',
                                            '{{$vendor->pincode}}',
                                            '{{$vendor->contact_person}}',
                                            '{{$vendor->contact_no}}',
                                            '{{$vendor->email}}', 
                                            '{{$vendor->status}}',
                                            '{{$vendor->tin_no}}',
                                            '{{$vendor->tan_no}}',
                                            '{{$vendor->pan_no}}',
                                            '{{$vendor->gst_vendor_code}}',
                                            '{{$vendor->drug_license_no}}');" >
                                            <td class="vendor_code common_td_rules">{{ $vendor->vendor_code }}</td>
                                            <td class="vendor_name common_td_rules">{{ $vendor->vendor_name }}</td>
                                            <td class="address common_td_rules">{{ $vendor->address_1 }}</td>
                                            <td class="city common_td_rules">{{ $vendor->city }}</td>
                                            <td class="state common_td_rules">{{ $vendor->state }}</td>
                                            <td class="country common_td_rules">{{ $vendor->country }}</td>
                                            <td class="contact_person common_td_rules">{{ $vendor->contact_person }}</td>
                                            <td class="contact_no common_td_rules">{{ $vendor->contact_no }}</td>
                                            <td class="status common_td_rules">{{ $vendor->status_name }}</td>
                                            
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="vendor_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.item.saveVendor')}}" method="POST" id="vendorForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="vendor_id" id="vendor_id" value="0">
                        <div class="col-md-12 padding_sm">
                           <div class="mate-input-box">
                            <label for="">Vendor Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" required  name="vendor_name" value="{{request()->old('vendor_name')}}" id="vendor_name">
                            <span class="error_red">{{ $errors->first('vendor_name') }}</span>
                          </div> 
                        </div>
                        <div class="col-md-12 padding_sm vendor_code_div hide">
                            <div class="form-group control-required">
                                <div class="mate-input-box">
                            <label for="">Vendor Code<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="vendor_code" id="vendor_code">
                        </div>
                        </div>
                        </div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">First Address</label><span class="error_red"></span>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control"  name="address_1" value="{{request()->old('address_1')}}" id="address_1">
                            <span class="error_red">{{ $errors->first('address_1') }}</span>
                          </div>  
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Second Address</label><span class="error_red"></span>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control"   name="address_2" value="{{request()->old('address_2')}}" id="address_2">
                            <span class="error_red">{{ $errors->first('address_2') }}</span>
                          </div>  
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Third Address</label><span class="error_red"></span>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control"   name="address_3" value="{{request()->old('address_3')}}" id="address_3">
                            <span class="error_red">{{ $errors->first('address_3') }}</span>
                          </div>  
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">City</label><span class="error_red"></span>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control"   name="city" value="{{request()->old('city')}}" id="city">
                            <span class="error_red">{{ $errors->first('city') }}</span>
                          </div>  
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">State</label><span class="error_red"></span>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control"   name="state" value="{{request()->old('state')}}" id="state">
                            <span class="error_red">{{ $errors->first('state') }}</span>
                          </div>  
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Country</label><span class="error_red"></span>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control"  name="country" value="{{request()->old('country')}}" id="country">
                                <span class="error_red">{{ $errors->first('country') }}</span>
                              </div>  
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Pin Code</label><span class="error_red"></span>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control"  name="pincode" value="{{request()->old('pincode')}}" id="pincode">
                                <span class="error_red">{{ $errors->first('pincode') }}</span>
                              </div>  
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Contact Person</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{request()->old('contact_person')}}" class="form-control" name="contact_person" id="contact_person">
                        </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Contact Number</label>
                                <div class="clearfix"></div>
                                <input type="text" value="{{request()->old('contact_no')}}" class="form-control" name="contact_no" id="contact_no">
                            </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Email ID</label>
                                <div class="clearfix"></div>
                                <input type="email" class="form-control" value="{{request()->old('email')}}" name="email" id="email"><span class="error_red">{{ $errors->first('email') }}</span>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">TIN Number</label>
                                <div class="clearfix"></div>
                                <input type="text" value="{{request()->old('tin_no')}}" class="form-control" name="tin_no" id="tin_no">
                            </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">TAN Number</label>
                                <div class="clearfix"></div>
                                <input type="text" value="{{request()->old('tan_no')}}" class="form-control" name="tan_no" id="tan_no">
                            </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">PAN Number</label>
                                <div class="clearfix"></div>
                                <input type="text" value="{{request()->old('pan_no')}}" class="form-control" name="pan_no" id="pan_no">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">GST Vendor Code</label>
                                <div class="clearfix"></div>
                                <input type="text" value="{{request()->old('gst_vendor_code')}}" class="form-control" name="gst_vendor_code" id="gst_vendor_code">
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Drug License Number</label>
                                <div class="clearfix"></div>
                                <input type="text" value="{{request()->old('drug_license_no')}}" class="form-control" name="drug_license_no" id="drug_license_no">
                            </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">{{__('Active')}}</option>
                                    <option value="0">{{__('Inactive')}}</option>
                                </select>
                                <span class="error_red">{{ $errors->first('status') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

    });

    function vendorEditLoadData(obj,id,code,name,add_1,add_2,add_3,city,state,country,pin,contact_person,contact_no,email,status,tin,tan,pan,gst,drug_no){
        $('#vendor_id').val(id);
        $('#vendor_code').prop('readonly',true).val(code);
        $('#vendor_name').val(name);
        $('#address_1').val(add_1);
        $('#address_2').val(add_2);
        $('#address_3').val(add_3);
        $('#city').val(city);
        $('#state').val(state);
        $('#country').val(country);
        $('#pincode').val(pin);
        $('#contact_person').val(contact_person);
        $('#contact_no').val(contact_no);
        $('#email').val(email);
        $('#status').val(status);
        $('#tin_no').val(tin);
        $('#tan_no').val(tan);
        $('#pan_no').val(pan);
        $('#gst_vendor_code').val(gst);
        $('#drug_license_no').val(drug_no);
        $('#vendorForm').attr('action', '{{$updateUrl}}');
        $(".vendor_code_div").removeClass('hide');
    }
    function resetFunction(){
         $('#vendorForm')[0].reset();
         $('#vendor_code').prop('readonly',false).val("");
         $(".vendor_code_div").addClass('hide');
    }
@include('Purchase::messagetemplate')
</script>
    
@endsection
