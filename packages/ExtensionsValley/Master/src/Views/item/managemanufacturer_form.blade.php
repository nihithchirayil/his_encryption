@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style> 
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.item.listmanufacturer')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Manufacturer Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="manufacturer_code" id="manufacturer_code_search" 
                                    value="{{ $searchFields['manufacturer_code'] ?? '' }}">
                                </div>
                            </div>
    
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Manufacturer Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="manufacturer_name" id="manufacturer_name_search"
                                    value="{{ $searchFields['manufacturer_name'] ?? '' }}">
                                </div>
                            </div>
                          
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 550px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Manufacturer</th>
                                    <th>Manufacturer Code</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Country</th>
                                    <th>Contact</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($list) > 0)
                                    @foreach ($list as $data)
                                <tr style="cursor: pointer;" onclick="manufacturerEditLoadData(this,
                                    '{{$data->manufacturer_id}}',
                                    '{{$data->manufacturer_code}}',
                                    '{{$data->manufacturer_name}}', 
                                    '{{$data->address}}', 
                                    '{{$data->state_id}}',
                                    '{{$data->mobile_no}}',
                                    '{{$data->country_id}}',
                                    '{{$data->status}}')
                                      " >
                                            <td class="location_type common_td_rules">{{ $data->manufacturer_name }}</td>
                                            <td class="location_code common_td_rules">{{ $data->manufacturer_code }}</td>
                                            <td class="location_name common_td_rules">{{ $data->address }}</td>
                                            <td class="product_type common_td_rules">{{ $data->state }}</td>
                                            <td class="contact_person common_td_rules">{{ $data->country }}</td>
                                            <td class="contact_no common_td_rules">{{ $data->mobile_no }}</td>
                                            <td class="status common_td_rules">{{ $data->status_name }}</td>
                                            
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="location_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right" style="margin-top:-31px;">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.item.savemanufacturer')}}" method="POST" id="manufacturerForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="manufacturer_id" id="manufacturer_id" value="0">
                        <div class="col-md-12 padding_sm">
                            <div class="form-group {{ $errors->has('manufacturer_name') ? 'has-error' : '' }} control-required">
                                <div class="mate-input-box">
                                <label for="">Manufacturere Name<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" value="{{request()->old('manufacturer_name')}}" required name="manufacturer_name" id="manufacturer_name">

                                        <span class="error_red">{{ $errors->first('manufacturer_name') }}</span>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="form-group {{ $errors->has('manufacturer_code') ? 'has-error' : '' }} control-required">
                                <div class="mate-input-box">
                            <label for="">Manufacturer Code<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" value="{{request()->old('manufacturer_code')}}" required name="manufacturer_code" id="manufacturer_code">
                            <span class="error_red">{{ $errors->first('manufacturer_code') }}</span>
                            <div class="" id="manufacturer_code_status">
                            </div>
                        </div>
                        </div>
                        </div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Country</label>
                            <div class="clearfix"></div>
                            {!! Form::select('country', array("0"=> " Select Country ") + $country->toArray(),request()->old('country'), [
                                'class'       => 'form-control',
                                'id'         => 'country',
                            ]) !!}                         
                             </div>  
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">State</label>
                                <div class="clearfix"></div>
                                {!! Form::select('state', array("0"=> "Select State") + $state->toArray(),request()->old('state'), [
                                            'class'       => 'form-control',
                                            'id'         => 'state',
                                        ]) !!}
                                   </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Contact</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{request()->old('contact')}}" class="form-control" name="contact" id="contact">
                        </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Address</label>
                            <div class="clearfix"></div>
                            <textarea class="form-control" value="{{request()->old('address')}}" name="address" id="address"></textarea>
                            </div>
                        </div>
                       
                        <div class="col-md-12 padding_sm">
                          <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Status<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1">{{__('Active')}}</option>
                                        <option value="0">{{__('Inactive')}}</option>
                                    </select>
                                    <span class="error_red">{{ $errors->first('name') }}</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 padding_sm">
                            </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                           <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script type="text/javascript">




    $(document).ready(function () {

      
        // $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        $('#manufacturer_code').keyup(function () {

            var manufacturer_code_str = $(this).val();
            manufacturer_code_str = manufacturer_code_str.replace(/[^a-zA-Z0-9]/g, '-');

            manufacturer_code_str = manufacturer_code_str.replace(/\-+/g, '');

            manufacturer_code_str = manufacturer_code_str.toUpperCase();

            var manufacturer_code = manufacturer_code_str.trim();

            $('#manufacturer_code').val(manufacturer_code);

            if (manufacturer_code != '') {
                var url = '{{route('extensionsvalley.item.checkmanufacturercode')}}';
                $.ajax({
                    method: "GET",
                    url: url,
                    data: 'manufacturer_code=' + manufacturer_code,
                    success: function (data) {
                        console.log(data);
                        if (data == 1) {
                            $('#manufacturer_code').css('color', 'green');
                            $('#manufacturer_code_status').html(
                                '<button tabindex="-1" type="button" title="Available" class="btn btn-success btn-sm">Available code</button>'
                            );

                        } else {
                            $('#manufacturer_code').css('color', 'red');
                            $('#manufacturer_code_status').html(
                                '<button tabindex="-1" type="button" title="Not available" class="btn btn-danger btn-sm">Existing code</button>'
                            );
                        }
                    }

                });

            }


        });

    });

    function manufacturerEditLoadData(obj,manufacturer_id,manufacturer_code,manufacturer_name,address,state_id,mobile_no,country_id,status){
        var edit_icon_obj = $(obj);
        $('#manufacturer_name').val(manufacturer_name);
        $('#manufacturer_id').val(manufacturer_id);
        $('#manufacturer_code').prop('readonly',true).val(manufacturer_code);
        $('#country').val(country_id);
        $('#state').val(state_id);
        $('#contact').val(mobile_no);
        $('#address').val(address);
        $('#status').val(status);
        $('#manufacturerForm').attr('action', '{{$updateUrl}}');
    }
    $("#country").on('change', function() {
      var cnt=$('#country').val();
      if((cnt!='')||(cnt!=null)||(cnt!=undefined)){
        var url = '{{route('extensionsvalley.item.relativestate')}}';
                $.ajax({
                    method: "GET",
                    url: url,
                    data: {cnt: cnt},
                    beforeSend: function() {
                     $('#state').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                     $( "#state" ).prop( "disabled", true ); //Disable
                    },
                    success: function(html) {
                    if (html) {
                    $("#state").empty();
                    $("#state").html('<option value="">Select State</option>');
                    $.each(html, function(key, value) {
                        ($("#state").append('<option value="' + value.id + '">' + value.name + '</option>'));
                    });
                    $('#state').select();
                } else {
                    $("#state").empty();
                }

            },

            error: function() {
                Command: toastr["warning"]("Please  select a proper item !");
                $( "#state" ).prop( "disabled", true ); //Disable

            },
            complete: function() {
            $( "#state" ).prop( "disabled", false );
            }

            });

            } else {
            $('#country').focus();
            $("#state").empty();
            }
        });

    function resetFunction(){
         $('#locationForm')[0].reset();
         $("#location_code").prop("readonly", false);
    }
@include('Purchase::messagetemplate')
</script>
    
@endsection
