@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style> 
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.item.listManufacturer')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Manufacturer Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="manufacturer_code" 
                                    value="{{ $searchFields['manufacturer_code'] ?? '' }}">
                                </div>
                            </div>
    
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Manufacturer Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="manufacturer_name" 
                                    value="{{ $searchFields['manufacturer_name'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                {!! Form::select('status', array("-1"=> " Select Status","1"=> " Active", "0"=> " Inactive"),$searchFields['status'] ?? '', [
                                            'class'       => 'form-control',
                                        ]) !!}
                                    </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Manufacturer Name</th>
                                    <th>Manufacturer Code</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($manufacturer_list) > 0)
                                    @foreach ($manufacturer_list as $list)
                                        <tr style="cursor: pointer;" onclick="editLoadData(this,
                                            '{{$list->id}}',
                                            '{{$list->manufacturer_code}}',
                                            '{{$list->manufacturer_name}}',
                                            '{{$list->status}}');" >
                                            <td class="manufacturer_name common_td_rules">{{ $list->manufacturer_name }}</td>
                                            <td class="manufacturer_code common_td_rules">{{ $list->manufacturer_code }}</td>
                                            <td class="status common_td_rules">{{ $list->status_name }}</td>     
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="manufacturer_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.item.saveManufacturer')}}" method="POST" id="manufacturerForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="manufacturer_id" id="manufacturer_id" value="0">
                        <div class="col-md-12 padding_sm">
                           <div class="mate-input-box">
                            <label for="">Manufacturer Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" required  name="manufacturer_name" value="{{request()->old('manufacturer_name')}}" id="manufacturer_name">
                            <span class="error_red">{{ $errors->first('manufacturer_name') }}</span>
                          </div> 
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="form-group {{ $errors->has('manufacturer_code') ? 'has-error' : '' }} control-required">
                                <div class="mate-input-box">
                            <label for="">Manufacturer Code<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" value="{{request()->old('manufacturer_code')}}" required name="manufacturer_code" id="manufacturer_code">
                            <span class="error_red">{{ $errors->first('manufacturer_code') }}</span>
                            <div class="" id="manufacturer_code_status">
                            </div>
                        </div>
                        </div>
                        </div>
                       
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">{{__('Active')}}</option>
                                    <option value="0">{{__('Inactive')}}</option>
                                </select>
                                <span class="error_red">{{ $errors->first('status') }}</span>
                                </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        $('#manufacturer_code').keyup(function () {

            var location_code_str = $(this).val();
            location_code_str = location_code_str.replace(/[^a-zA-Z0-9]/g, '-');

            location_code_str = location_code_str.replace(/\-+/g, '');

            location_code_str = location_code_str.toUpperCase();

            var location_code = location_code_str.trim();

            $('#manufacturer_code').val(location_code);

            if (location_code != '') {
                var url = '{{route('extensionsvalley.item.checkManufacturerCode')}}';
                $.ajax({
                    method: "GET",
                    url: url,
                    data: 'manufacturer_code=' + location_code,
                    success: function (data) {
                        console.log(data);
                        if (data == 0) {
                            $('#manufacturer_code').css('color', 'green');
                            $('#manufacturer_code_status').html(
                                '<button tabindex="-1" type="button" title="Available" class="btn btn-success btn-sm"><i class="fa fa-check fa-xs" aria-hidden="true"></i></button>'
                            );

                        } else {
                            $('#manufacturer_code').css('color', 'red');
                            $('#manufacturer_code_status').html(
                                '<button tabindex="-1" type="button" title="Not available" class="btn btn-danger btn-sm"><i class="fa fa-ban fa-xs" aria-hidden="true"></i></button>'
                            );
                        }
                    }

                });

            }


        });

    });

    function editLoadData(obj,id,code,name,status){
        $('#manufacturer_id').val(id);
        $('#manufacturer_code').prop('readonly',true).val(code);
        $('#manufacturer_name').val(name);
        $('#status').val(status);
        $('#manufacturerForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
         $('#manufacturerForm')[0].reset();
         $('#manufacturer_code').prop('readonly',false).val("");
    }
@include('Purchase::messagetemplate')
</script>
    
@endsection
