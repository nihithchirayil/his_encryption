<style>
   #data_table {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }
    #data_table td, #result_data_table th {
      border: 1px solid #ddd;
      padding: 8px;
    }
    #data_table tr:nth-child(even){background-color: #f2f2f2;}
    #data_table tr:hover {background-color: #ddd;}
    #data_table th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #2e6da4;
      color: white;
    }
    </style>
<div class="row">
    <div class=" " id="" style="position: relative;height:562px;border-radius:10px;overflow:hidden;" >
       
      
           
    
            

              <table id="data_table" style="font-size: 12px;">
                <thead>
                   <tr class="table_header_bg">
                     <th style="padding: 2px;cursor: pointer;">Item Name</th>
                     <th style="padding: 2px; ">Category</th>
                     <th style="padding: 2px;">Sub Category</th>
                   </tr>
                </thead>
                <tbody>
                      @if(sizeof($purchaseData)>0)
                        @foreach($purchaseData as $data)
                            <tr>
                                <td style="padding: 2px;cursor: pointer;" onclick="getCollectionReportDatabatch('{{$data->item_code}}','{{$data->item_desc}}')" class="batch_no_row" ><input type="hidden" name="item_code[]"style="font-weight: bold;" class="form-control item_code_hidden" readonly="readonly" value="{{$data->item_code}}">{{$data->item_desc}}</td>
                                <td>{{$data->category_name}}</td>
                                <td>{{$data->subcategory_name}}</td>
                            </tr>       
                               
                        @endforeach  
                    @else
                     <tr style="border: none:text_align:center"><td colspan="3" style="text-align: center">No Result Found !!</td></tr>
                  @endif
               </tbody>
            </table>
            <div class="col-md-12" style="text-align:right;">
                <nav id="purchase_paging"> <?php echo $paginator->render(); ?>
                </nav>
            </div>
         </div>

       </div>
 
 
 






<script type="text/javascript">  
//---------------------------------------------------------------------------pagination------------------------------------------------------------------------------------------------
    function getURLParameter(url, name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,1])[1];
   }

    $("#ResultsViewArea #purchase_paging .pagination a").click(function(e){
        e.preventDefault();
        var location= $('#location').val();
        var category= $('#category').val();
        var sub_category= $('#sub_category').val();
        var item_code= $('#product_name_hidden').val();
        var item_type= $('#item_type').val();
        var url = $(this).attr('href');
        var page_no = getURLParameter(url, 'page');
        var url = route_json.CollectionData;
        var param = {location: location,category: category,sub_category: sub_category,item_code: item_code,item_type: item_type,page_no:page_no};
        $.ajax({
            url:url,
            data: param,
            dataType: 'html',
            beforeSend: function () {
            $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            }).done(function (data) {
                $('#ResultsViewArea').LoadingOverlay("hide");
                $('#ResultsViewArea').html(data);
            }).fail(function () {
                alert('Posts could not be loaded.');
            });
    }); 
</script>
    
