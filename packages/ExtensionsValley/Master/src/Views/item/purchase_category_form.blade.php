@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<div class="right_col">
    <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{ $title }}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <form action="{{ route('extensionsvalley.item.listpurchase') }}" id="categorySearchForm"
                            method="POST">
                            {!! Form::token() !!}


                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Purchase category </label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="purchase_name"
                                        id="purchase_name_search" value="{{ $searchFields['purchase_name'] ?? '' }}">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                        class="fa fa-times"></i> Clear</a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 550px;">
                        <table
                            class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Purchase category</th>
                                    <th>Ledger Name</th>
                                    <th>Store</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($list) > 0)
                                @foreach ($list as $data)
                                <tr style="cursor: pointer;" onclick="purchaseEditLoadData(this,

                                            '{{ $data->purchase_id }}',
                                            '{{ $data->name }}',
                                            '{{ $data->ledger_id }}',
                                            '{{ $data->status }}',
                                            '{{ $data->active_status }}')
                                              ">
                                    <td class="location_type common_td_rules">{{ $data->name }}</td>
                                    <td class="location_name common_td_rules">{{ $data->ledger_name }}</td>

                                    <td class="status common_td_rules">{{ $data->store_name }}</td>
                                    <td class="status common_td_rules">{{ $data->status_name }}</td>

                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="5" class="location_code">No Records found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right" style="margin-top:-31px;">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{ route('extensionsvalley.item.savepurchase') }}" method="POST" id="purchaseForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="purchase_id" id="purchase_id" value="0">
                        <div class="col-md-12 padding_sm">
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} control-required">
                                <div class="mate-input-box">
                                    <label for="">Purchase Category<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" value="{{ request()->old('name') }}"
                                        required name="name" id="name">

                                    <span class="error_red">{{ $errors->first('name') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Ledger Name</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" style='color:#555555;' name="ledger_name"
                                    id="ledger_name">
                                    <option value="">Select Ledger Name</option>
                                    <?php foreach($ledger_name as $key=>$val){
                                       ?>
                                    <option value="<?= $key ?>">
                                        <?= $val ?>
                                    </option>
                                    <?php
                                   }
                                   ?>

                                </select>
                            </div>
                        </div>


                        <div class="col-md-12 padding_sm">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Store<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="store" id="store">
                                        <?php
                                            foreach ($store_type as $each) {
                                                ?>
                                        <option value="<?= $each->value ?>">
                                            <?= $each->name ?>
                                        </option>
                                        <?php
                                            }
                                            ?>
                                    </select>
                                    <span class="error_red">{{ $errors->first('name') }}</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Status<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1">{{ __('Active') }}</option>
                                        <option value="0">{{ __('Inactive') }}</option>
                                    </select>
                                    <span class="error_red">{{ $errors->first('name') }}</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 padding_sm">
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script type="text/javascript">
    $(document).ready(function() {


            // $('#menu_toggle').trigger('click');
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });


        });

        function purchaseEditLoadData(obj, purchase_id, name, ledger_name, status, active_status) {
            // alert(ledger_name);

            var edit_icon_obj = $(obj);
            $('#purchase_id').val(purchase_id);

            $('#name').val(name);
            // $('#manufacturer_code').prop('readonly',true).val(manufacturer_code);
            $('#ledger_name').val(ledger_name);
            $('#ledger_name').select2();

            // $('#state').val(state_id);
            // $('#contact').val(mobile_no);
            $('#store').val(status);
            $('#status').val(active_status);
            $('#purchaseForm').attr('action', '{{ $updateUrl }}');
        }


        function resetFunction() {
            $('#locationForm')[0].reset();
            $("#location_code").prop("readonly", false);
        }
        @include('Purchase::messagetemplate')
</script>

@endsection
