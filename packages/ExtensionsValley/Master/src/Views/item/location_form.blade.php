@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style> 
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.item.listLocation')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Location Code</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="code" id="location_code_search" 
                                    value="{{ $searchFields['code'] ?? '' }}">
                                </div>
                            </div>
    
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Location Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="name" id="location_name_search"
                                    value="{{ $searchFields['name'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Location Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('type', array("0"=> " Select Location Type") + $locationType->toArray(),$searchFields['type'] ?? '', [
                                            'class'       => 'form-control',
                                        ]) !!}
                                    </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 550px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Location Type</th>
                                    <th>Location Code</th>
                                    <th>Location Name</th>
                                    <th>Product Type</th>
                                    <th>Contact Person</th>
                                    <th>Contact Number</th>
                                    <!-- <th>Email ID</th> -->
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($location_list) > 0)
                                    @foreach ($location_list as $location)
                                <tr style="cursor: pointer;" onclick="locationEditLoadData(this,
                                    '{{$location->location_id}}',
                                    '{{$location->location_code}}',
                                    '{{$location->name}}', 
                                    '{{$location->type}}', 
                                    '{{$location->product_type}}',
                                    '{{$location->contact_person}}',
                                    '{{$location->contact_no}}',
                                    '{{$location->email_id}}',
                                    '{{$location->status}}',
                                    '{{$location->deals_pharmacy_item}}',
                                    '{{$location->deals_materials_item}}',
                                    '{{$location->is_nursing_station}}',
                                    '{{$location->is_store}}',
                                    '{{$location->purchase_type}}',
                                    '{{$location->default_pharmacy}}','{{ $location->sequence_name }}','{{ $location->po_sequence_name }}' )" >
                                            <td class="location_type common_td_rules">{{ $location->type_name }}</td>
                                            <td class="location_code common_td_rules">{{ $location->location_code }}</td>
                                            <td class="location_name common_td_rules">{{ $location->name }}</td>
                                            <td class="product_type common_td_rules">{{ $location->product_type_name }}</td>
                                            <td class="contact_person common_td_rules">{{ $location->contact_person }}</td>
                                            <td class="contact_no common_td_rules">{{ $location->contact_no }}</td>
                                          {{-- <td class="email_id">{{ $location->email_id }}</td>--}}
                                            <td class="status common_td_rules">{{ $location->status_name }}</td>
                                            
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="location_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right" style="margin-top:-31px;">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.item.saveLocation')}}" method="POST" id="locationForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="location_id" id="location_id" value="0">
                        <div class="col-md-12 padding_sm">
                            <div class="form-group {{ $errors->has('location_type') ? 'has-error' : '' }} control-required">
                                <div class="mate-input-box">
                                <label for="">Location Type<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                {!! Form::select('location_type', array("0"=> " Select Location Type") + $locationType->toArray(),request()->old('location_type'), [
                                            'class'       => 'form-control',
                                            'id'          => 'location_type',
                                            'required'   => 'required'
                                        ]) !!}
                                        <span class="error_red">{{ $errors->first('location_type') }}</span>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="form-group {{ $errors->has('location_code') ? 'has-error' : '' }} control-required">
                                <div class="mate-input-box">
                            <label for="">Location Code<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" value="{{request()->old('location_code')}}" required name="location_code" id="location_code">
                            <span class="error_red">{{ $errors->first('location_code') }}</span>
                            <div class="" id="location_code_status">
                            </div>
                        </div>
                        </div>
                        </div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Location Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" required  name="location_name" value="{{request()->old('location_name')}}" id="location_name">
                            <span class="error_red">{{ $errors->first('location_name') }}</span>
                          </div>  
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Default Product Type</label><!-- <span class="error_red">*</span> -->
                                <div class="clearfix"></div>
                                {!! Form::select('default_product_type', array("0"=> " Select Product Type") + $itemType->toArray(),request()->old('default_product_type'), [
                                            'class'       => 'form-control',
                                            'id'         => 'product_type',
                                        ]) !!}
                                       {{-- <span class="error_red">{{ $errors->first('default_product_type') }}</span>--}}
                                   </div>
                        </div>
                        <div class="col-md-12 padding_sm" id="specialcase" style="display: none" >
                            <div class="mate-input-box">
                                <label for="">Purchase  Type</label>
                                <div class="clearfix"></div>
                               
                                {!! Form::select('purchase_type', array("0"=> " Select purchase Type") + $itemType1->toArray(),request()->old('purchase_type'), [
                                    'class'       => 'form-control',
                                    'id'         => 'purchase_type_code',
                                ]) !!}
                                     
                                   </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Contact Person</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{request()->old('contact_person')}}" class="form-control" name="contact_person" id="contact_person">
                        </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Contact Number</label>
                            <div class="clearfix"></div>
                            <input type="text" value="{{request()->old('contact_no')}}" class="form-control" name="contact_no" id="contact_no">
                        </div></div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Email ID</label>
                            <div class="clearfix"></div>
                            <input type="email" class="form-control" value="{{request()->old('email_id')}}" name="email_id" id="email_id"><span class="error_red">{{ $errors->first('email_id') }}</span>
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Deals Pharmacy Item</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="deals_pharmacy_item" id="deals_pharmacy_item" > 
                            </div></div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Deals Materials Item</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="deals_materials_item" id="deals_materials_item" > 
                            </div></div>

                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Is Nursing Station</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="is_nursing_station" id="is_nursing_station" > 
                            </div></div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Is Store</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="is_store" id="is_store"> 
                            </div></div>

                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Is Default Pharmacy</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" class="" name="default_pharmacy" id="default_pharmacy" > 
                            </div></div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Status<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1">{{__('Active')}}</option>
                                        <option value="0">{{__('Inactive')}}</option>
                                    </select>
                                    <span class="error_red">{{ $errors->first('name') }}</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 padding_sm" id="sequence_name_div" style="display: none" >
                            <div class="col-md-5 padding_sm" id="select_grn_code" >
                                <div class="mate-input-box">
                                <label for="" id="grn_tra_na">Select GRN Transaction</label>
                                <div class="clearfix"></div>
                               <select  class="form-control" name="select_grn_seq_name" id="select_grn_seq_name">
                                <option value="">Select</option>
                                @foreach ($grn_seqs as $data)
                                    <option value="{{ $data->code }}">{{ $data->code }}</option>
                                @endforeach
                               </select>
                            </div>
                        </div>
                            <div class="col-md-5 padding_sm" id="add_grn_code" style="display: none" >
                                <div class="mate-input-box">
                                <label for="" id="grn_tra_na">New GRN Transaction</label>
                                <div class="clearfix"></div>
                                <input type="text" value="" class="form-control " name="grn_seq_name" id="grn_seq_name">
                                <input type="hidden" value="" name="grn_add_code"  id="grn_add_code">
                            </div>
                            <label id="grn_code_sugg" style="color: gray"></label>
                        </div>
                        <div class="col-md-1"><button type="button"style="width: 32px;height: 43px;margin-left: -9px;" class="btn btn-block btn-warning" id="new_grn_btn"><i class="fa fa-plus"></i></button></div>
                        <div class="col-md-5 padding_sm" id="select_po_code"  >
                            <div class="mate-input-box">
                                <label for="" id="po_tra_na">Select PO Transaction</label>
                                <div class="clearfix"></div>
                                <select  class="form-control" name="select_po_seq_name" id="select_po_seq_name">
                                <option value="">Select</option>
                                    @foreach ($po_seqs as $data)
                                        <option value="{{ $data->code }}">{{ $data->code }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                            <div class="col-md-5 padding_sm" id="add_po_code" style="display: none">
                                <div class="mate-input-box">
                                    <label for="" id="po_tra_na">New PO Transaction</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="" class="form-control " name="po_seq_name" id="po_seq_name">
                                    <input type="hidden" value="0" name="po_add_code"  id="po_add_code">


                                </div>
                                <label id="po_code_sugg" style="color: gray"></label>
                            </div>
                            <div class="col-md-1"><button id="new_po_btn" type="button"style="width: 32px;height: 43px;margin-left: -9px;" class="btn btn-block btn-warning"><i class="fa fa-plus"></i></button></div>

                        </div>
                        <div class="col-md-12 padding_sm">
                            </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                           <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg" type="submit" onclick=""><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script type="text/javascript">

$('#location_type').on('change',function(){
   var loc= $('#location_type').val();
   if(loc==11 ||loc==8 ){
       $('#specialcase').css('display','block');
   }else{
    $('#specialcase').css('display','none');
    $('#purchase_type_code').val('');
   }
});


    $(document).ready(function () {

        var loc= $('#location_type').val();
        if(loc==11 ||loc==8 ){
            $('#specialcase').css('display','block');
        }
        // $('#menu_toggle').trigger('click');
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        $('#location_code').keyup(function () {

            var location_code_str = $(this).val();
            location_code_str = location_code_str.replace(/[^a-zA-Z0-9]/g, '-');

            location_code_str = location_code_str.replace(/\-+/g, '');

            location_code_str = location_code_str.toUpperCase();

            var location_code = location_code_str.trim();

            $('#location_code').val(location_code);

            if (location_code != '') {
                var url = '{{route('extensionsvalley.item.checkLocationCode')}}';
                $.ajax({
                    method: "GET",
                    url: url,
                    data: 'location_code=' + location_code,
                    success: function (data) {
                        console.log(data);
                        if (data == 0) {
                            $('#location_code').css('color', 'green');
                            $('#location_code_status').html(
                                '<button tabindex="-1" type="button" title="Available" class="btn btn-success btn-sm"><i class="fa fa-check fa-xs" aria-hidden="true"></i></button>'
                            );

                        } else {
                            $('#location_code').css('color', 'red');
                            $('#location_code_status').html(
                                '<button tabindex="-1" type="button" title="Not available" class="btn btn-danger btn-sm"><i class="fa fa-ban fa-xs" aria-hidden="true"></i></button>'
                            );
                        }
                    }

                });

            }


        });

    });

    function locationEditLoadData(obj,location_id,location_code,location_name,type,product_type,contact_person,contact_no,email_id,status,deals_pharmacy_item, deals_materials_item, is_nursing_station,is_store,purchase_type,default_pharmacy,seq_name,po_seq_name){
        var edit_icon_obj = $(obj);
        $('#location_id').val(location_id);
        $('#location_code').prop('readonly',true).val(location_code);
        $('#location_name').val(location_name);
        $('#location_type').val(type);
        $('#product_type').val(product_type);
        $('#contact_person').val(contact_person);
        $('#contact_no').val(contact_no);
        $('#email_id').val(email_id);
        $('#status').val(status);
        var loc= $('#location_type').val();
        if(loc==11 ||loc==8 ){
            $('#specialcase').css('display','block');
            $('#purchase_type_code').val(purchase_type);
        }else{
             $('#specialcase').css('display','none');
        }
        if (deals_pharmacy_item == 1){
            $("#deals_pharmacy_item").prop("checked", true);
        } else{
            $("#deals_pharmacy_item").prop("checked", false);
        }
        if (deals_materials_item == 1){
            $("#deals_materials_item").prop("checked", true);
        } else{
            $("#deals_materials_item").prop("checked", false);
        }
        if (is_nursing_station == 1){
            $("#is_nursing_station").prop("checked", true);
        } else{
            $("#is_nursing_station").prop("checked", false);
        }
        if (is_store == 1){
            $("#is_store").prop("checked", true);
            $('#sequence_name_div').show();
            $('#select_grn_seq_name').val(seq_name);
            $('#select_po_seq_name').val(po_seq_name);
        } else{
            $("#is_store").prop("checked", false);
        }
        if (default_pharmacy == 1){
            $("#default_pharmacy").prop("checked", true);
        } else{
            $("#default_pharmacy").prop("checked", false);
        }
        $('#locationForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
         $('#locationForm')[0].reset();
         $("#location_code").prop("readonly", false);
    }

    $('#is_store').on('change',function(){
        if($('#is_store').is(':checked')){
             $('#sequence_name_div').show();
        }else{
            $('#sequence_name_div').hide();
        }
    })
    $('#grn_seq_name').on('keyup',function(){
        var value=$(this).val();
        if(value){
        var currentYear = new Date().getFullYear().toString().slice(-2);
        var nextYear = (parseInt(currentYear) + 1).toString();
        var updatedValue = value + '-' + currentYear+''+nextYear+ '/';
        $('#grn_code_sugg').html('<b>Default prefix:</b> '+updatedValue);
        }else{
            $('#grn_code_sugg').html('');  
        }
       
    })
    $('#po_seq_name').on('keyup',function(){
        var value=$(this).val();
        if(value){
        var currentYear = new Date().getFullYear().toString().slice(-2);
        var nextYear = (parseInt(currentYear) + 1).toString();
        var updatedValue = value + '-' + currentYear+''+nextYear+ '/';
        $('#po_code_sugg').html('<b>Default prefix:</b> '+updatedValue);
        }else{
            $('#po_code_sugg').html('');  
        }
       
    })
    $('#new_grn_btn').on('click',function(){
        if($('#select_grn_code').is(':visible')){
            $('#select_grn_code').hide();
            $('#add_grn_code').show();
            $('#grn_add_code').val(1);
        }else{
            $('#select_grn_code').show();
            $('#add_grn_code').hide();
            $('#grn_add_code').val(0);
        }
      
    })
    $('#new_po_btn').on('click',function(){
        if($('#select_po_code').is(':visible')){
            $('#select_po_code').hide();
            $('#add_po_code').show();
            $('#po_add_code').val(1);

        }else{
            $('#select_po_code').show();
            $('#add_po_code').hide();
            $('#po_add_code').val(0);

        }
      
    })
    
    document.getElementById('locationForm').addEventListener('submit', function(event) {
        var grn_add_code=$('#grn_add_code').val();
        var po_add_code=$('#po_add_code').val();
        var grn_sequence_code='';
        var op_sequence_code='';
        if($('#is_store').is(':checked')){
            if(grn_add_code==1){
              grn_sequence_code=$('#grn_seq_name').val();
        }else{
            grn_sequence_code=$('#select_grn_seq_name').val();
        }
        if(po_add_code==1){
            op_sequence_code=$('#po_seq_name').val();
        }else{
            op_sequence_code=$('#select_po_seq_name').val();
        }
        if(grn_sequence_code==''){
             toastr.warning('GRN sequence name not selected.');       
             event.preventDefault(); // Prevent form submission
        }else if(op_sequence_code==''){
            toastr.warning('OP sequence name not selected.');  
            event.preventDefault(); // Prevent form submission
        }
        }
       
    });
   
@include('Purchase::messagetemplate')
</script>
    
@endsection
