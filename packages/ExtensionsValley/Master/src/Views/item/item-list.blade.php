@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<style>
    .close_btn_freq_search {
        position: absolute;
        z-index: 99;
        color: #FFF !important;
        background: #000;
        right: -11px;
        top: -1px;
        border-radius: 100%;
        text-align: center;
        width: 20px;
        height: 20px;
        line-height: 20px;
        cursor: pointer;
    }

    .frequency-list-div {
        position: absolute;
        z-index: 99;
        background: #FFF;
        box-shadow: 0 0 6px #ccc;
        padding: 10px;
        width: 50%;
        top: -200px
    }

    .close_btn_route_search {
        position: absolute;
        z-index: 99;
        color: #FFF !important;
        background: #000;
        right: -11px;
        top: -1px;
        border-radius: 100%;
        text-align: center;
        width: 20px;
        height: 20px;
        line-height: 20px;
        cursor: pointer;
    }

    .route-list-div {
        position: absolute;
        z-index: 99;
        background: #FFF;
        box-shadow: 0 0 6px #ccc;
        padding: 10px;
        width: 50%;
        bottom: 30px
    }

    table.fr.table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #f9f9f9 !important;
    }

    .select2-container--default .select2-selection--single {
        border: 1px solid #CCC !important;
    }

    .select_style .select2-container--default .select2-selection--single .select2-selection__rendered {
        white-space: normal;
    }

    .select_style .select2-container--default .select2-selection--single {
        overflow: hidden !important;
    }

    .ajaxSearchBox {
        display: none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 200px;
        margin: 0px 0px 0px 0px;
        overflow-y: auto;
        width: 34%;
        z-index: 599;
        position: absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
</style>
@endsection
@section('content-area')

<div class="right_col">
    <input type="hidden" id="base_url" value="{{ URL::To('/') }}">
    <div class="row" style="text-align: left; font-size: 12px; font-weight: bold;"> {{ $title }}</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <form id="itemSearchForm" method="GET" action="{{ route('extensionsvalley.item.listItem') }}">
                            {!! Form::token() !!}
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Product Code</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="item_code" value="{{ $searchFields['item_code'] ?? '' }}">
                                </div>
                            </div>

                            {{-- <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Product Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="item_desc"
                                            value="{{ $searchFields['item_desc'] ?? '' }}">
                    </div>
                </div> --}}
                <div class="col-md-4 padding_sm">
                    <div class="col-md-9 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Product Name</label>
                            <div class="clearfix"></div>
                            <input type="text" name="item_desc" autocomplete="off" id="item_search" class="form-control" value="{{ $searchFields['item_desc'] ?? '' }}" onkeyup="product_search(this,event)">
                            <div class="ajaxSearchBox item_searchdiv" id="item_searchdiv" style="text-align: left; list-style: none;
                                         cursor: pointer; max-height: 400px; margin: -2px 0px 0px 0px; overflow-y: auto; width: 100%; z-index: 3000;margin-top: 14px;margin-left: -4px; position:absolute; background: #ffffff;  border-radius: 3px; border: 1px solid rgba(0, 0, 0, 0.3);">
                            </div>
                            <input type="hidden" name="item_search_id" class="form-control" value="{{ $searchFields['item_search_id'] ?? '' }}" id="item_search_id">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        @php
                        $item_contain_search = @$searchFields['item_contain_search'] ? $searchFields['item_contain_search'] : 0;
                        $checked = '';
                        @endphp
                        @if (intval($item_contain_search) == 1)
                        @php $checked="checked"; @endphp
                        @endif
                        <div class="checkbox checkbox-success" style="margin-top: 10px">
                            <input {{ $checked }} id="item_contain_search" name="item_contain_search" type="checkbox">
                            <label for="item_contain_search"> Contain Search</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label for="">Generic Name</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control" autocomplete="off" name="generic_name_text_search" id="generic_name_search" value="{{ $searchFields['generic_name_text_search'] ?? '' }}" onkeyup="select_generic_name(this.id,'2',event)">
                    </div>
                    <div class="ajaxSearchBox generic_name_text_search_box" id="generic_name_box-generic_name_search" style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                                                                                                                                                                                                         margin: -2px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                                                                                                                                                                                         border: 1px solid rgba(0, 0, 0, 0.3);">
                    </div>
                    <input type="hidden" class="form-control" name="generic_name" value="{{ $searchFields['generic_name'] ?? '' }}" id="generic_name_id">
                </div>
                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label for="">Item Type</label>
                        <div class="clearfix"></div>
                        {!! Form::select(
                        'item_type',
                        ['-1' => ' Select Item Type'] + $itemType->toArray(),
                        $searchFields['item_type'] ?? '',
                        [
                        'class' => 'form-control item_type',
                        ],
                        ) !!}
                    </div>
                </div>
                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label for="">Category</label>
                        <div class="clearfix"></div>
                        {!! Form::select(
                        'category',
                        ['-1' => ' Select Category'] + $Category->toArray(),
                        $searchFields['category'] ?? '',
                        [
                        'class' => 'form-control category',
                        ],
                        ) !!}
                    </div>
                </div>
                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label for="">Sub category</label>
                        <div class="clearfix"></div>
                        {!! Form::select(
                        'subcategory',
                        ['-1' => ' Select SubCategory'] + $SubCategory->toArray(),
                        $searchFields['subcategory'] ?? '',
                        [
                        'class' => 'form-control subcategory',
                        ],
                        ) !!}
                    </div>
                </div>

                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label for="">Status</label>
                        <div class="clearfix"></div>
                        {!! Form::select(
                        'status',
                        ['-1' => ' Select Status', '1' => ' Active', '0' => ' Inactive'],
                        $searchFields['status'] ?? '',
                        [
                        'class' => 'form-control',
                        ],
                        ) !!}
                    </div>
                </div>
                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label for="">HSN Code</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control" name="hsn_code" value="{{ $searchFields['hsn_code'] ?? '' }}">
                    </div>
                </div>
                <div class="col-md-1 padding_sm">
                    <label for="">&nbsp;</label>
                    <div class="clearfix"></div>
                    <span class="btn btn-block light_purple_bg" onclick="serachFormSubmit();"><i class="fa fa-search"></i> Search</span>
                </div>
                <div class="col-md-1 padding_sm">
                    <label for="">&nbsp;</label>
                    <div class="clearfix"></div>
                    <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                </div>

                <div class="col-md-1 padding_sm">
                    <label for="">&nbsp;</label>
                    <div class="clearfix"></div>
                    <div onclick="addProductNew();" class="btn btn-block light_purple_bg"><i class="fa fa-plus"></i> Add</div>
                </div>
                <div class="col-md-1 padding_sm">
                    <label for="">&nbsp;</label>
                    <div class="clearfix"></div>
                    <div onclick="redirecttovendor();" class="btn btn-block light_purple_bg"><i class="fa fa-share"></i> Vendor </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="box no-border no-margin">
        <div class="box-body clearfix">
            <div class="theadscroll always-visible" style="position: relative; height: 350px;">
                <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" style="border: 1px solid #CCC;">
                    <thead>
                        <tr class="table_header_bg">
                            <th width="10%">Item Type</th>
                            <th width="10%">Category</th>
                            <th width="8%">Subcategory</th>
                            <th width="8%">Product Code</th>
                            <th width="15%">Product Name</th>
                            <th width="8%">Product By</th>
                            <th width="8%">HSN Code</th>
                            <th width="10%">Generic Name</th>
                            <th width="8%">Status</th>
                            <th width="10%">Barcode</th>
                            <th width="5%" style="text-align: center"><i class="fa fa-barcode"></i></td>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!empty($item))
                        @foreach ($item as $itm)
                        <tr style="cursor: pointer;">
                            <td onclick="editProduct('{{ $itm->id }}','{{ $itm->item_code_generate_flag }}');" style=" text-align: left">{{ $itm->item_type_name }}</td>
                            <td onclick="editProduct('{{ $itm->id }}','{{ $itm->item_code_generate_flag }}');" style=" text-align: left">{{ $itm->category_name }}</td>
                            <td onclick="editProduct('{{ $itm->id }}','{{ $itm->item_code_generate_flag }}');" style=" text-align: left">{{ $itm->subcategory_name }}</td>
                            <td onclick="editProduct('{{ $itm->id }}','{{ $itm->item_code_generate_flag }}');" style=" text-align: left">{{ $itm->item_code }}</td>
                            <td onclick="editProduct('{{ $itm->id }}','{{ $itm->item_code_generate_flag }}');" style=" text-align: left">{{ $itm->item_desc }}</td>
                            <td onclick="editProduct('{{ $itm->id }}','{{ $itm->item_code_generate_flag }}');" style=" text-align: left">{{ $itm->username }}</td>
                            <td onclick="editProduct('{{ $itm->id }}','{{ $itm->item_code_generate_flag }}');" style=" text-align: left">{{ $itm->hsn_code }}</td>
                            <td onclick="editProduct('{{ $itm->id }}','{{ $itm->item_code_generate_flag }}');" style=" text-align: left">{{ $itm->generic_name }}</td>
                            <td onclick="editProduct('{{ $itm->id }}','{{ $itm->item_code_generate_flag }}');" style=" text-align: left">{{ $itm->status_name }}</td>
                            <?php
                            if ($itm->barcode) {
                            ?>
                                <td style=" text-align: left">{{ $itm->barcode }}</td>
                                <td style="text-align: left"><button id="change_barcodeprint_status_btn<?= $itm->id ?>" title="Print Barcode" type="button" class="btn btn-primary" onclick="change_barcodeprint_status(<?= $itm->id ?>)"><i id="change_barcodeprint_status_spin<?= $itm->id ?>" class="fa fa-print"></i> Print Barcode</button> </td>
                            <?php
                            } else {
                            ?>
                                <td id="barcodeDataDiv<?= $itm->id ?>" style=" text-align: left"><button id="genarate_barcodebtn<?= $itm->id ?>" title="Generate Barcode" type="button" class="btn btn-success" onclick="generateProductBarcode(<?= $itm->id ?>)">
                                        <i id="genarate_barcodespin<?= $itm->id ?>" class="fa fa-barcode"></i> Barcode</button>
                                </td>
                                <td style="text-align: left"><button disabled id="change_barcodeprint_status_btn<?= $itm->id ?>" title="Print Barcode" type="button" class="btn btn-primary" onclick="change_barcodeprint_status(<?= $itm->id ?>)"><i id="change_barcodeprint_status_spin<?= $itm->id ?>" class="fa fa-print"></i> Print Barcode</button> </td>
                            <?php
                            }
                            ?>

                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 text-right">
                <ul class="pagination purple_pagination" style="text-align:right !important;">
                    {!! $page_links !!}
                </ul>
            </div>
        </div>
    </div>
</div>

</div>


<form action="{{ route('extensionsvalley.item.saveItem') }}" method="POST" id="itemForm">
    {!! Form::token() !!}
    <!-- Modal -->
    <div id="add_list_modal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 1000px; width: 100%;">
            <?php $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <a href="{{ $actual_link }}" class="close">&times;</a>
                    <h4 class="modal-title titleName">Manage Product Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="col-md-12 padding_sm">
                                <div class="box no-border no-margin">
                                    <div class="box-body clearfix">

                                        <div class="col-md-2 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Item Type</label>
                                                <div class="clearfix"></div>
                                                {!! Form::select('item_type', ['0' => ' Select Item Type'] + $itemType->toArray(), null, [
                                                'class' => 'form-control item_type_list',
                                                'id' => 'item_type',
                                                ]) !!}
                                            </div>
                                        </div>

                                        <div class="col-md-3 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Category</label>
                                                <div class="clearfix"></div>
                                                {!! Form::select('category', ['0' => ' Select Category'], null, [
                                                'class' => 'form-control category_list',
                                                'id' => 'category',
                                                ]) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Sub Category</label>
                                                <div class="clearfix"></div>
                                                {!! Form::select('subcategory', ['0' => ' Select SubCategory'], null, [
                                                'class' => 'form-control subcategory_list',
                                                'id' => 'subcategory',
                                                ]) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">MRP</label>
                                                <div class="clearfix"></div>
                                                <input onkeyup="setspecialprice(this)" type="text" class="form-control td_common_numeric_rules" id="product_mrp" name="mrp" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="{{ $searchFields['hsn_code'] ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Special Price</label>
                                                <div class="clearfix"></div>
                                                <input type="text" class="form-control td_common_numeric_rules" id="special_price" name="special_price" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="{{ $searchFields['hsn_code'] ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="col-md-4 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Product Name</label>
                                                <div class="clearfix"></div>
                                                <input type="text" required="" name="item_desc" id="item_desc" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Product Code</label>
                                                <div class="clearfix"></div>
                                                <input type="text" name="item_code" id="item_code" class="form-control item_code">
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Re-order Level</label>
                                                <div class="clearfix"></div>
                                                <input type="number" name="reorder_level" min="0" id="reorder_level" class="form-control" @if ($role_ids==0) disabled title="Only can edit Stock Admin !!" @endif>
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Barcode</label>
                                                <div class="clearfix"></div>
                                                <input type="text" name="barcode" id="barcode" class="form-control" onblur="">
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">HSN Code</label>
                                                <div class="clearfix"></div>
                                                <input type="text" name="hsn_code" id="hsn_code" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Manufacturer</label>
                                                <div class="clearfix"></div>
                                                <div class="clearfix"></div>
                                                <input type="text" name="manufacturer_name_search" autocomplete="off" id="manufacturer_name_search" class="form-control" onkeyup="searchManufacturerName(this,event)">
                                                <div class="ajaxSearchBox" id="manufacturer_div" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px; margin: -2px 0px 0px 0px; overflow-y: auto; width: 100%; z-index: 3000;margin-top: 14px;margin-left: -4px;  position:absolute; background: #ffffff;  border-radius: 3px; border: 1px solid rgba(0, 0, 0, 0.3);">
                                                </div>
                                                <input type="hidden" name="manufacturer" id="manufacturer">
                                            </div>
                                        </div>
                                        <div class="col-md-4 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Doctor Name</label>
                                                <div class="clearfix"></div>
                                                <input type="text" name="doctor_name" autocomplete="off" id="doctor_name" class="form-control" onkeyup="searchDoctorName(this,event)">
                                                <div class="ajaxSearchBox" id="doctor_namediv" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px; margin: -2px 0px 0px 0px; overflow-y: auto; width: 100%; z-index: 3000;margin-top: 14px;margin-left: -4px; position:absolute; background: #ffffff;  border-radius: 3px; border: 1px solid rgba(0, 0, 0, 0.3);">
                                                </div>
                                                <input type="hidden" name="doctor_id" id="doctor_id">
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Calculate Quantity</label>
                                                <div class="clearfix"></div>
                                                <select class="form-control" name="calculate_quantity" id="calculate_quantity">
                                                    <option value="1">{{ __('Yes') }}</option>
                                                    <option value="0">{{ __('No') }}</option>
                                                </select>
                                                {{-- <span class="error_red">{{ $errors->first('status') }}</span> --}}
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm" id="ledger_group_div" style="display:none">
                                            <div class="mate-input-box">
                                                <label for="">&nbsp;</label>
                                                <div class="clearfix"></div>
                                                {!! Form::select('ledger_group_id', ['0' => ' Select Ledger'], null, [
                                                'class' => 'form-control ledger_group_id',
                                                'id' => 'ledger_group_id',
                                                ]) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm">
                                            <label for="">&nbsp;</label>
                                            <div class="clearfix"></div>
                                            <span class="btn btn-block light_purple_bg" data-toggle="modal" id="tax_exp">Taxes and Expenses</span>
                                        </div>
                                        <div class="clearfix"></div>
                                       <div class="col-md-4" id="looka_like_sounda_like_div" style="display: none">
                                        <div class="col-md-6 padding_sm">
                                            <div class="checkbox checkbox-success">
                                                <input id="checkbox_lk" class="styled lk_like" type="checkbox" name="checkbox_lk" name="status">
                                                <label for="checkbox_lk"> Look A Like</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding_sm">
                                            <div class="checkbox checkbox-success">
                                                <input id="checkbox_sd" class="styled sd_like" type="checkbox" name="checkbox_sd" name="status">
                                                <label for="checkbox_sd"> Sound A Like</label>
                                            </div>
                                        </div>
                                       </div>
                                        <div class="col-md-1 padding_sm">
                                            <div class="checkbox checkbox-success">
                                                <input id="checkbox_reuse" class="styled" type="checkbox" name="checkbox_reuse">
                                                <label for="checkbox_reuse">Reuse</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" id="look_a_like_div" style="display: none">
                                            <div class="mate-input-box">
                                                <label for="">Look A Like Product</label>
                                                <div class="clearfix"></div>
                                                <input type="text" name="lk_product" autocomplete="off" id="lk_product" class="form-control" onkeyup="lookAlikeProduct(this,event)">
                                                <div class="ajaxSearchBox" id="lk_productdiv" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px; margin: -2px 0px 0px 0px; overflow-y: auto; width: 100%; z-index: 3000;margin-top: 14px;margin-left: -4px; position:absolute; background: #ffffff;  border-radius: 3px; border: 1px solid rgba(0, 0, 0, 0.3);">
                                                </div>
                                                <input type="hidden" name="lk_product_id" class="lk_product_id" id="lk_product_id">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" id="sound_a_like_div" style="display: none">
                                            <div class="mate-input-box">
                                                <label for="">Sound A Like Product</label>
                                                <div class="clearfix"></div>
                                                <input type="text" name="sd_product" autocomplete="off" id="sd_product" class="form-control" onkeyup="soundAlikeProduct(this,event)">
                                                <div class="ajaxSearchBox" id="sd_productdiv" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px; margin: -2px 0px 0px 0px; overflow-y: auto; width: 100%; z-index: 3000;margin-top: 14px;margin-left: -4px; position:absolute; background: #ffffff;  border-radius: 3px; border: 1px solid rgba(0, 0, 0, 0.3);">
                                                </div>
                                                <input type="hidden" name="sd_product_id" class="sd_product_id" id="sd_product_id">
                                            </div>
                                        </div>
                                        <div class="col-md-1 padding_sm" id="reuse_div" style="display: none">
                                            <div class="mate-input-box">
                                                <label for="">Reuse Cnt</label>
                                                <div class="clearfix"></div>
                                                <input type="text" name="reuse_count" autocomplete="off" id="reuse_count" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                            </div>
                                        </div>

                                        <div id="tax_exp_div" class="col-md-3 padding_sm pull-right" style="display:none">

                                            <!-- Modal content-->
                                            <div>
                                                <table class="table table-bordered table-striped table_sm">
                                                    <thead>
                                                        <tr class="table_header_light_purple_bg">
                                                            <th style="width: 108px;">Description</th>
                                                            <th style="width: 32%;">%</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id='charges_table'>
                                                        @foreach ($charges_head as $charge)
                                                        <tr>
                                                            <td>{{ $charge->name }}</td>
                                                            <?php $charge_name = preg_replace('/[^A-Za-z0-9\-]/', '', $charge->name); ?>
                                                            <td><input class="form-control" name="charge[]" id="{{ strtolower(str_replace(' ', '', $charge_name)) }}" type="text">
                                                                <input class="form-control" name="charge_id[]" id="{{ strtolower($charge->id) }}" value="{{ strtolower($charge->id) }}" type="hidden">
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-4 padding_sm">
                                <div class="box no-border no-margin">
                                    <div class="box-body">

                                        <div class="col-md-4 padding_xs">
                                            <h5 class="text-bold" style="margin: 3px 0 0 0;"><b>UOM</b></h5>
                                        </div>
                                        <div class="col-md-4 padding_xs">
                                            <label for="">Standard UOM</label>
                                        </div>
                                        <div class="col-md-4 padding_xs">
                                            {!! Form::select('std_uom', ['0' => ' Select UOM'] + $Uom->toArray(), null, [
                                            'class' => 'form-control',
                                            'id' => 'std_uom',
                                            ]) !!}
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ht10"></div>

                                        <div class="theadscroll always-visible" style="position: relative; height: 150px;">
                                            <table class="table theadfix_wrapper table-col-bordered no-margin table_sm no-margin uom_table" style="border: 1px solid #CCC;">
                                                <thead>
                                                    <tr class="table_header_bg">
                                                        <th width="55%">Purchase UOM</th>
                                                        <th>Conv Factor</th>
                                                        <th>Default</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php for ($i = 0; $i < 5; $i++) { ?>
                                                        <tr>
                                                            <td>
                                                                {!! Form::select('purchase_uom[]', ['0' => ' Select UOM'] + $Uom->toArray(), null, [
                                                                'class' => 'form-control',
                                                                'id' => 'pur_uom' . $i,
                                                                'onchange' => 'setDefault(' . $i . ')',
                                                                ]) !!}
                                                            </td>
                                                            <td>
                                                                <input type="text" id="pur_con_fact{{ $i }}" name="pur_con_fact[]" class="form-control">
                                                            </td>
                                                            <td>
                                                                <div class="col-md-1 padding_sm">
                                                                    <div class="radio radio-primary radio-inline">
                                                                        <input type="radio" class="pur_default_cls" id="pur_default{{ $i }}" value="{{ $i }}" name="pur_default">
                                                                        <label for="pur_default{{ $i }}">
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <!--                                            <input type="text" id="pur_default{{ $i }}" name="pur_default[]" class="form-control">-->
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ht5"></div>
                                        <div class="theadscroll always-visible" style="position: relative; height: 150px;">
                                            <table class="table theadfix_wrapper table-col-bordered no-margin table_sm no-margin uom_table" style="border: 1px solid #CCC;">
                                                <thead>
                                                    <tr class="table_header_bg">
                                                        <th width="55%">Selling UOM</th>
                                                        <th>Conv Factor</th>
                                                        <th>Default</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php for ($i = 0; $i < 5; $i++) { ?>
                                                        <tr>
                                                            <td>
                                                                {!! Form::select('selling_uom[]', ['0' => ' Select UOM'] + $Uom->toArray(), null, [
                                                                'class' => 'form-control',
                                                                'id' => 'sel_uom' . $i,
                                                                'onchange' => 'setDefaultSell(' . $i . ')',
                                                                ]) !!}
                                                            </td>
                                                            <td>
                                                                <input type="text" id="sel_con_fact{{ $i }}" name="sel_con_fact[]" class="form-control">
                                                            </td>
                                                            <td>
                                                                <div class="col-md-1 padding_sm">
                                                                    <div class="radio radio-primary radio-inline">
                                                                        <input type="radio" class="sel_default_cls" id="sel_default{{ $i }}" value="{{ $i }}" name="sel_default">
                                                                        <label for="sel_default{{ $i }}">
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <!--                                                                    <input type="text" id="sel_default{{ $i }}" name="sel_default[]" class="form-control">-->
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 padding_sm">
                                <div class="theadscroll always-visible" style="position: relative; height: 295px; border: 1px solid #CCC;">
                                    <table class="table no-border no-margin table_sm no-margin">
                                        <tbody>
                                            <tr>
                                                <td width="3%">
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox1" class="styled" type="checkbox" checked="" name="status">
                                                        <label for="checkbox1"> The item is active</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox2" class="styled" type="checkbox" name="purchasable_ind">
                                                        <label for="checkbox2">The item is purchasable</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox3" class="styled" type="checkbox" name="sellable_ind">
                                                        <label for="checkbox3">The item is sellable</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox5" class="styled" type="checkbox" name="inventory_tracked_ind">
                                                        <label for="checkbox5">Inventory is tracked for this
                                                            item</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox6" class="styled" type="checkbox" name="fixed_asset_ind" onclick="showLedgergroup(this);">
                                                        <label for="checkbox6"> This is a fixed asset</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox7" class="styled" type="checkbox" name="consumable_ind">
                                                        <label for="checkbox7">Consumable item</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox8" class="styled" type="checkbox" name="chemo_ind">
                                                        <label for="checkbox8">This is a chemo item</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox9" class="styled" type="checkbox" name="narcotic_ind">
                                                        <label for="checkbox9"> This is a narcotic item</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox10" class="styled" type="checkbox" name="consignment_ind">
                                                        <label for="checkbox10">This item is a consignment
                                                            item</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox11" class="styled" type="checkbox" name="returnable_ind">
                                                        <label for="checkbox11">This item is returnable</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox12" class="styled" type="checkbox" name="perishable_ind">
                                                        <label for="checkbox12">This item is perishable</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox13" class="styled" type="checkbox" name="price_editable_ind">
                                                        <label for="checkbox13"> The price of the item is
                                                            editable</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox14" class="styled" type="checkbox" name="disposable_ind">
                                                        <label for="checkbox14">The item is disposable</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox16" class="styled" type="checkbox" name="is_high_risk_item">
                                                        <label for="checkbox16">The item is high risk</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox15" class="styled" type="checkbox" name="decimal_qty_ind">
                                                        <label for="checkbox15">The item can be sold in decimal
                                                            qty</label>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox17" class="styled" type="checkbox" name="is_high_end_antibiotic">
                                                        <label for="checkbox17">The item is high end
                                                            antibiotic</label>
                                                    </div>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht5"></div>
                                <div class="col-md-12 no-padding min_sellable_div" style="display: none;">
                                    <label for="">Min Sellable Qty and Price</label>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 padding_xs">
                                        <input type="text" class="form-control" placeholder="Qty" id="min_sellable_qty" name="min_sellable_qty">
                                    </div>
                                    <div class="col-md-6 padding_xs">
                                        <input type="text" class="form-control" placeholder="Price" id="min_sellable_price" name="min_sellable_price">
                                    </div>
                                </div>
                            </div>




                            <div class="col-md-4 padding_sm clinical_info_div" style="display: none;">
                                <div class="box no-margin no-border">
                                    <div class="box-body">
                                        <h5 class="no-margin"><b>Clinical Information</b></h5>
                                        <div class="clearfix"></div>
                                        <div class="ht5"></div>
                                        <!--   {{-- <div class="theadscroll always-visible" style="position: relative; height: 130px;">
                                                <table class="table theadfix_wrapper table-col-bordered no-margin table_sm no-margin" style="border: 1px solid #CCC;" id="tableid">
                                                    <thead>
                                                        <tr class="table_header_bg">
                                                            <th>Chemical Composition</th>
                                                            <th style="text-align: center;width: 12%" >Dose</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php //for ($i = 0; $i < 5; $i++) {
                                                        ?>
                                                            <tr>
                                                                <td>
                                                                    <input type="text" id="chemical_id{{$i}}" name="chemical_name[]" class="form-control" readonly="">
<!--                                                                    {!! Form::select('chemical_name[]', array("0"=> "Select Chemical Name") + $ChemicalComposition->toArray(),null, [
                                                                    'class'       => 'form-control',
                                                                    'id'          => 'chemical_id'.$i
                                                                    ]) !!}
                                                                -->
                                        <!--  </td>
                                                                <td align="center">
                                                                    <input type="text" id="dose{{$i}}" name="dose[]" class="form-control" readonly="">
                                                                </td>
                                                            </tr>
                                                        <?php // }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div> --}} -->
                                        <div class="clearfix"></div>
                                        <div class="ht5"></div>
                                        <table class="table no-margin table_sm no-margin" style="border: 1px solid #CCC;">
                                            <tbody>
                                                <tr>
                                                    <td>VED Type</td>
                                                    <td>
                                                        @php $ved_type_array = ['0' => ' Select VED', 'vitals' => ' Vitals', 'essential' => ' Essential', 'desirable' => ' Desirable']; @endphp
                                                        {!! Form::select('ved_type', $ved_type_array, null, [
                                                        'class' => 'form-control',
                                                        'id' => 'ved_type',
                                                        ]) !!}
                                                        <!--                                                            <input type="text" name="ved_type" id="ved_type" class="form-control">-->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Generic Name <i class="fa fa-plus" title="Add New Generic Name" onclick="saveNewGenericName()" id="add_new_ledger_spin" style="cursor:pointer;padding-left: 8px"></i></td>
                                                    <td class="select_style">
                                                        <input type="text" class="form-control" autocomplete="off" name="generic_name_text_input" id="generic_name_text_input" value="" onkeyup="select_generic_name(this.id,'1',event)">
                                                        <div class="ajaxSearchBox generic_name_text_search_box" id="generic_name_box-generic_name_text_input" style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                                                                                                                                                                                                         margin: -2px 0px 0px 0px;overflow-y: auto; width: 65%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                                                                                                                                                                                         border: 1px solid rgba(0, 0, 0, 0.3);">
                                                        </div>
                                                        <input type="hidden" class="form-control" name="generic_name" value="" id="generic_name">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Schedule</td>
                                                    @php
                                                    $schedule_types = [
                                                    'General' => 'General',
                                                    'Schedule X' => 'Schedule X',
                                                    'Schedule H1' => 'Schedule H1',
                                                    'Schedule H' => 'Schedule H',
                                                    'Schedule B' => 'Schedule B',
                                                    'Schedule A' => 'Schedule A',
                                                    ];
                                                    @endphp
                                                    <td>
                                                        {!! Form::select('schedule', $schedule_types, null, [
                                                        'class' => 'form-control select2',
                                                        'id' => 'schedule',
                                                        ]) !!}
                                                        <!--                                                            <input type="text" name="schedule" id="schedule" class="form-control">-->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Dose</td>
                                                    <td><input type="text" name="strength" id="strength" class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td>Consume Type</td>
                                                    <?php
                                                    $config_head_status = \DB::table('config_head')
                                                        ->where('name', 'consume_type')
                                                        ->pluck('id');
                                                    $config_head_id = isset($config_head_status[0]) ? $config_head_status[0] : 0;
                                                    $config_detail_status = \DB::table('config_detail')
                                                        ->where('head_id', $config_head_id)
                                                        ->pluck('name', 'value'); ?>

                                                    <td>{!! Form::select('consume_type', ['-1' => ' Select'] + $config_detail_status->toArray(), null, [
                                                        'class' => 'form-control',
                                                        'id' => 'consume_type',
                                                        ]) !!} </td>

                                                    <!--                                                            <input type="text" name="consume_type" id="consume_type" class="form-control"></td>-->
                                                </tr>
                                                <tr>
                                                    <td>Route</td>
                                                    <td><input type="text" name="route" id="route" class="form-control" autocomplete="off">
                                                        <input type="hidden" name="route_id" id="route_id" class="form-control" autocomplete="off">
                                                        <!-- Route List -->
                                                        <div class="route-list-div" style="display: none;">
                                                            <a style="float: left;" class="close_btn_route_search">X</a>
                                                            <div class="route_theadscroll" style="position: relative;">
                                                                <table id="RouteTable" class="table fr table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                                    {{-- <thead>
                                                                <tr class="light_purple_bg">
                                                                  <th>Route</th>
                                                                </tr>
                                                              </thead> --}}
                                                                    <tbody id="ListRouteSearchData">

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- Route List -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Frequency</td>
                                                    <td>
                                                        <input type="text" name="frequency" id="frequency" class="form-control" autocomplete="off">

                                                        <input type="hidden" name="search_freq_value_hidden" value="">
                                                        <input type="hidden" name="search_freq_name_hidden" value="">

                                                        <!-- Frequency List -->
                                                        <div class="frequency-list-div" style="display: none;">
                                                            <a style="float: left;" class="close_btn_freq_search">X</a>
                                                            <div class="freq_theadscroll" style="position: relative;">
                                                                <table id="FrequencyTable" class="table fr table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                                    {{-- <thead>
                                                <tr class="light_purple_bg">
                                                  <th>Frequency</th>
                                                </tr>
                                              </thead> --}}
                                                                    <tbody id="ListFrequencySearchData">

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Duration</td>
                                                    <td>
                                                        <input type="number" min="0" name="duration" id="duration" class="form-control" autocomplete="off">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Directions</td>
                                                    <td>
                                                        <textarea id="directions" wrap="off" cols="35" rows="1" name="directions"></textarea>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    @if (isset($itemCodeTypeValue))
                    <input type="hidden" name="itemCodeTypeValue" value="{{ $itemCodeTypeValue }}" id="itemCodeTypeValue">
                    @endif
                    <input type="hidden" name="hidden_edit_id" id="hidden_edit_id">
                    <span type="submit" class="btn btn-success" onclick="inputFormSubmit();"><i class="fa fa-save"></i> Submit</span>
                </div>
                <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
                <input type="hidden" name="current_url" id="current_url" value="">
                <input type="hidden" name="ledger_id" id="ledger_id" value="">
</form>
</div>

</div>
</div>

<div id="generic_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Generic Name Master</h4>
            </div>
            <div class="modal-body" id="append_generic">

            </div>

        </div>

    </div>
</div>
<div id="manufacturer_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form id="manufacturerForm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Manufacturer Name Master</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Manufacturer Name<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" required name="manufacturer_name" value="{{ request()->old('manufacturer_name') }}" id="manufacturer_name">
                                    <span class="error_red">{{ $errors->first('manufacturer_name') }}</span>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="form-group {{ $errors->has('manufacturer_code') ? 'has-error' : '' }} control-required">
                                    <div class="mate-input-box">
                                        <label for="">Manufacturer Code<span class="error_red">*</span></label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" value="{{ request()->old('manufacturer_code') }}" required name="manufacturer_code" id="manufacturer_code">
                                        <span class="error_red">{{ $errors->first('manufacturer_code') }}</span>
                                        <div class="" id="manufacturer_code_status">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Status<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1">{{ __('Active') }}</option>
                                        <option value="0">{{ __('Inactive') }}</option>
                                    </select>
                                    <span class="error_red">{{ $errors->first('status') }}</span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span onclick="saveNewManufacturerName();" class="btn light_purple_bg"><i class="fa fa-save"></i>
                        Save</span>
                </div>
            </div>
        </form>
    </div>
</div>
@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // $('#menu_toggle').trigger('click');
        $(document).on('click', '.grn_drop_btn', function(e) {
            e.stopPropagation();
            $(".grn_btn_dropdown").hide();
            $(this).next().slideDown('');
        });
        $(document).on('click', '.btn_group_box', function(e) {
            e.stopPropagation();
        });
        $(document).on('click', function() {
            $(".grn_btn_dropdown").hide();
        });
        $(".select_button li").click(function() {
            $(this).toggleClass('active');
        });
        $(document).on('click', '.notes_sec_list ul li', function() {
            var disset = $(this).attr("id");
            $('.notes_sec_list ul li').removeClass("active");
            $(this).addClass("active");
            $(this).closest('.notes_box').find(".note_content").css("display", "none");
            $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
        });
        var $table = $('table.theadfix_wrapper');
        $table.floatThead({
            scrollContainer: function($table) {
                return $table.closest('.theadscroll');
            }

        });
        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });
        $('.modal').on('shown.bs.modal', function(e) {
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
            $('.theadscroll').perfectScrollbar('update');
            $('table.theadfix_wrapper').floatThead('reflow');
        });
        @if(isset($itemCodeTypeValue) && $itemCodeTypeValue == 1)
        $('.item_code').attr('disabled', true);
        $('.item_code').removeAttr('required');
        @elseif(isset($itemCodeTypeValue) && $itemCodeTypeValue == 0)
        $('.item_code').removeAttr('disabled');
        $('.item_code').attr('required', 'required');
        @endif
        $(document).on('click', '#checkbox15', function(e) {
            if ($("#checkbox15").prop('checked') == true) {
                $('.min_sellable_div').show();
            } else {
                $('.min_sellable_div').hide();
            }
        });
        $(".item_type_list").on('change', function() {

            var item_type = $('.item_type_list').val();
            if (item_type == 1) {
                $('.clinical_info_div').show();
                $('#looka_like_sounda_like_div').show();
            } else {
                $('.clinical_info_div').hide();
                $('#looka_like_sounda_like_div').hide();
            }
            if (item_type) {
                var url = "{{ route('extensionsvalley.item.ajaxCategoryByItemType') }}";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'item_type=' + item_type,
                    beforeSend: function() {},
                    success: function(res) {
                        // console.log(res);
                        if (res) {
                            $(".category_list").empty();
                            $(".subcategory_list").empty();
                            $(".category_list").append(
                                '<option value="-1"> Select Category</option>');
                            $.each(res, function(key, value) {
                                $(".category_list").append('<option value="' + key +
                                    '">' + value + '</option>');
                            });
                        } else {
                            $(".category_list").empty();
                            $(".subcategory_list").empty();
                        }

                    },
                    error: function() {
                        alert('Please check your internet connection and try again');
                    },
                    complete: function() {}
                });
            } else {
                $('.item_type_list').focus();
                $(".category_list").empty();
                $(".subcategory_list").empty();
            }
        });
        $(".category_list").on('change', function() {

            var category = $('.category_list').val();
            if (category) {
                var url = "{{ route('extensionsvalley.item.ajaxSubcategoryByCategory') }}";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'category=' + category,
                    beforeSend: function() {},
                    success: function(res) {
                        // console.log(res);
                        if (res) {
                            $(".subcategory_list").empty();
                            $(".subcategory_list").append(
                                '<option value="-1">Select Subcategory</option>');
                            $.each(res, function(key, value) {
                                $(".subcategory_list").append('<option value="' +
                                    key + '">' + value + '</option>');
                            });
                        } else {
                            $(".subcategory_list").empty();
                        }

                    },
                    error: function() {
                        alert('Please check your internet connection and try again');
                    },
                    complete: function() {}
                });
            } else {
                $('.category_list').focus();
                $(".subcategory_list").empty();
            }
        });
        $(".item_type").on('change', function() {

            var item_type = $('.item_type').val();
            if (item_type) {
                var url = "{{ route('extensionsvalley.item.ajaxCategoryByItemType') }}";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'item_type=' + item_type,
                    beforeSend: function() {},
                    success: function(res) {
                        // console.log(res);
                        if (res) {
                            $(".category").empty();
                            $(".subcategory").empty();
                            $(".category").append(
                                '<option value="-1"> Select Category</option>');
                            $.each(res, function(key, value) {
                                $(".category").append('<option value="' + key +
                                    '">' + value + '</option>');
                            });
                        } else {
                            $(".category").empty();
                            $(".subcategory").empty();
                        }

                    },
                    error: function() {
                        alert('Please check your internet connection and try again');
                    },
                    complete: function() {}
                });
            } else {
                $('.item_type').focus();
                $(".category").empty();
                $(".subcategory").empty();
            }
        });
        $(".category").on('change', function() {

            var category = $('.category').val();
            if (category) {
                var url = "{{ route('extensionsvalley.item.ajaxSubcategoryByCategory') }}";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'category=' + category,
                    beforeSend: function() {},
                    success: function(res) {
                        // console.log(res);
                        if (res) {
                            $(".subcategory").empty();
                            $(".subcategory").append(
                                '<option value="-1"> Select Subcategory</option>');
                            $.each(res, function(key, value) {
                                $(".subcategory").append('<option value="' + key +
                                    '">' + value + '</option>');
                            });
                        } else {
                            $(".subcategory").empty();
                        }

                    },
                    error: function() {
                        alert('Please check your internet connection and try again');
                    },
                    complete: function() {}
                });
            } else {
                $('.category').focus();
                $(".subcategory").empty();
            }
        });
    });

    function validate() {
        if ($("#item_type").val() == 0) {
            toastr.warning('Plese Select Item Type !!');
            return false;
        }
        if ($("#category").val() == 0) {
            toastr.warning('Plese Select Category !!');
            return false;
        }
        if ($("#subcategory").val() == 0) {
            toastr.warning('Plese Select Sub Category !!');
            return false;
        }
        if ($("#item_desc").val() == 0) {
            toastr.warning('Plese Enter Product Name !!');
            return false;
        }
    }

    function editProduct(id, flag) {
        // alert("id="+id);
        if (flag == 1) {
            $('.item_code').attr('disabled', true);
            $('.item_code').removeAttr('required');
        } else {
            $('.item_code').removeAttr('disabled');
            $('.item_code').attr('required', 'required');
        }
        $('.titleName').html('');
        $('.titleName').html('Edit Product Details');
        if (id != '') {
            var url = "{{ route('extensionsvalley.item.ajaxGetItem') }}";
            $.ajax({
                type: "GET",
                url: url,
                data: 'item_id=' + id,
                beforeSend: function() {
                    $('#itemForm')[0].reset();
                    // $("input[type=hidden]").val('');
                    $('.min_sellable_div').hide();
                    // $("#_token").val("{{ Session::token() }}");
                },
                success: function(data) {
                    var current_url = window.location.href;
                    $('#hidden_edit_id').val(id);
                    var reorder_level = data['reorder_level'];
                    var item = data['item'];
                    var look_alike_drug = data['look_alike_drug'];
                    var sound_alike_drug = data['sound_alike_drug'];
                    if (look_alike_drug.length > 0) {
                        $('#checkbox_lk').prop('checked', true);
                        $('#look_a_like_div').fadeIn();
                        $('#lk_product').val(look_alike_drug[0].lk_item);
                        $('#lk_product_id').val(look_alike_drug[0].lk_item_id);
                    }
                    if (sound_alike_drug.length > 0) {
                        $('#checkbox_sd').prop('checked', true);
                        $('#sound_a_like_div').fadeIn();
                        $('#sd_product').val(sound_alike_drug[0].sd_item);
                        $('#sd_product_id').val(sound_alike_drug[0].sd_item_id);
                    }
                    //var ledger_group_id = data['ledger_group_id'];
                    selectCategoryInEdit(item.item_type_id, item.category_id, item.subcategory_id);
                    for (var i = 0; i < data['sel_uom'].length; i++) {
                        var s_uom = data['sel_uom'][i];
                        $("#sel_uom" + i).val(s_uom.uom_id);
                        $("#sel_con_fact" + i).val(s_uom.conv_factor);
                        //                    $("#sel_default" + i).val(s_uom.default);
                        if (s_uom.default == 1) {
                            $("#sel_default" + i).prop('checked', 'checked');
                        }
                    }
                    for (var j = 0; j < data['pur_uom'].length; j++) {
                        var p_uom = data['pur_uom'][j];
                        $("#pur_uom" + j).val(p_uom.uom_id);
                        $("#pur_con_fact" + j).val(p_uom.conv_factor);
                        if (p_uom.default == 1) {
                            $("#pur_default" + j).prop('checked', 'checked');
                        }
                    }
                    //For Item Chemical Composition Starts
                    // for (var k = 0; k < data['itm_comp'].length; k++) {
                    // var itm_comp = data['itm_comp'][k];
                    // $("#chemical_id" + k).val(itm_comp.chemical_id);
                    // $("#dose" + k).val(itm_comp.dose);
                    // }
                    //For Item Chemical Composition Ends
                    for (var z = 0; z < data['itm_charge'].length; z++) {
                        var itm_charge = data['itm_charge'][z];
                        var item_id = itm_charge.name;
                        var item_ids = item_id.replace(/[^a-zA-Z ]/g, "");
                        var item_idss = item_ids.replace(/ /g, '');
                        $("#" + item_idss).val(itm_charge.amount_p);
                    }
                    $("#item_type").val(item.item_type_id);
                    if(item.item_type_id==1) {
                         $('.clinical_info_div').show();
                         $('#looka_like_sounda_like_div').show();
                     } else {
                         $('.clinical_info_div').hide();
                         $('#looka_like_sounda_like_div').hide();
                     }
                    $("#category").val(item.category_id);
                    $("#subcategory").val(item.subcategory_id);
                    $("#item_desc").val(item.item_desc);
                    $("#item_code").val(item.item_code);
                    $("#barcode").val(item.barcode);
                    $("#product_mrp").val(item.mrp);
                    $("#special_price").val(item.special_price);
                    $("#hsn_code").val(item.hsn_code);
                    $("#manufacturer_name_search").val(item.manufacturer_name);
                    $("#manufacturer").val(item.manufacturer_id);
                    $("#doctor_name").val(item.doctor_name);
                    $("#doctor_id").val(item.doctor_id);
                    $("#std_uom").val(item.uom_id);
                    $("#min_sellable_qty").val(item.min_sellable_qty);
                    $("#min_sellable_price").val(item.min_sellable_price);
                    $("#ved_type").val(item.ved_type);
                    $("#generic_name").val(item.generic_name_id);
                    $('#generic_name_text_input').val(item.generic_name);
                    $("#schedule").val(item.schedule);
                    $('#schedule').select2().trigger('change');
                    $("#strength").val(item.strength);
                    $("#consume_type").val(item.consume_type);
                    $("#route").val(item.route);
                    $("#frequency").val(item.frequency);
                    $("#route_id").val(item.rout_id);
                    $("#search_freq_value_hidden").val(item.freq_id);
                    $("#directions").val(item.directions);
                    $("#duration").val(item.duration);
                    $("#reorder_level").val(reorder_level);
                    $("#calculate_quantity").val(item.calculate_quantity);
                    if (item.status == 0) {
                        $("#checkbox1").removeAttr('checked');
                    } else {
                        $("#checkbox1").prop('checked', 'checked');
                    }
                    if (item.purchasable_ind == 1) {
                        $("#checkbox2").prop('checked', 'checked');
                    } else {
                        $("#checkbox2").removeAttr('checked');
                    }
                    if (item.sellable_ind == 1) {
                        $("#checkbox3").prop('checked', 'checked');
                    } else {
                        $("#checkbox3").removeAttr('checked');
                    }
                    if (item.inventory_tracked_ind == 1) {
                        $("#checkbox5").prop('checked', 'checked');
                    } else {
                        $("#checkbox5").removeAttr('checked');
                    }
                    if (item.fixed_asset_ind == 1) {
                        $("#checkbox6").prop('checked', 'checked');
                        $("#ledger_group_div").show();
                        //$("#ledger_group_id").val(ledger_group_id.ledger_group_id);
                        //$("#ledger_id").val(ledger_group_id.id)
                    } else {
                        $("#checkbox6").removeAttr('checked');
                    }
                    if (item.consumable_ind == 1) {
                        $("#checkbox7").prop('checked', 'checked');
                    } else {
                        $("#checkbox7").removeAttr('checked');
                    }
                    if (item.chemo_ind == 1) {
                        $("#checkbox8").prop('checked', 'checked');
                    } else {
                        $("#checkbox8").removeAttr('checked');
                    }
                    if (item.narcotic_ind == 1) {
                        $("#checkbox9").prop('checked', 'checked');
                    } else {
                        $("#checkbox9").removeAttr('checked');
                    }
                    if (item.consignment_ind == 1) {
                        $("#checkbox10").prop('checked', 'checked');
                    } else {
                        $("#checkbox10").removeAttr('checked');
                    }
                    if (item.returnable_ind == 1) {
                        $("#checkbox11").prop('checked', 'checked');
                    }
                    if (item.perishable_ind == 1) {
                        $("#checkbox12").prop('checked', 'checked');
                    } else {
                        $("#checkbox12").removeAttr('checked');
                    }
                    if (item.price_editable_ind == 1) {
                        $("#checkbox13").prop('checked', 'checked');
                    } else {
                        $("#checkbox13").removeAttr('checked');
                    }
                    if (item.disposable_ind == 1) {
                        $("#checkbox14").prop('checked', 'checked');
                    } else {
                        $("#checkbox14").removeAttr('checked');
                    }
                    if (item.is_high_risk_item == 1) {
                        $("#checkbox16").prop('checked', 'checked');
                    } else {
                        $("#checkbox16").removeAttr('checked');
                    }
                    if (item.is_high_end_antibiotic == 1) {
                        $("#checkbox17").prop('checked', 'checked');
                    } else {
                        $("#checkbox17").removeAttr('checked');
                    }
                    if (item.decimal_qty_ind == 1) {
                        $("#checkbox15").prop('checked', 'checked');
                        $('.min_sellable_div').show();
                    } else {
                        $("#checkbox15").removeAttr('checked');
                    }
                    if (item.is_reuse > 0) {
                        $('#checkbox_reuse').prop('checked', true);
                        $('#reuse_div').fadeIn();
                        $('#reuse_count').val(item.reuse_quantity);
                    }
                    $("#add_list_modal").modal('show');
                    $("#current_url").val(current_url);
                    $("#itemForm").attr("action", "{{ route('extensionsvalley.item.editItem') }}");
                },
                error: function() {
                    alert('Please check your internet connection and try again');
                },
                complete: function() {}
            });
        } else {
            alert("Please Select Any Record!");
        }

    }

    function addProductNew() {
        $('.titleName').html('Add New Product');
        $('.min_sellable_div').hide();
        $('#itemForm')[0].reset();
        // $("input[type=hidden]").val('');
        $("#add_list_modal").modal('show');
        // $("#_token").val("{{ Session::token() }}");
        $("#category").empty();
        $("#subcategory").empty();
        $('#looka_like_sounda_like_div').hide();
    }

    //************************************ FREEQUENCY SEARCH ENDS ***********************************************
    var freq_timeout = null;
    var freq_last_search_string = '';
    $('#frequency').on('keyup', function() {
        event.preventDefault();
        /* Act on the event */
        var obj = $(this);

        searchFrequency(obj);
    });

    $('#frequency').on('focus', function() {
        event.preventDefault();
        /* Act on the event */
        var obj = $(this);

        searchFrequency(obj, 1);
    });

    function searchFrequency(obj, all = 0) {
        var search_freq_string = $(obj).val();
        var patient_id = $('#patient_id').val();
        var freq_list = $('.frequency-list-div');
        if ((search_freq_string == "" || search_freq_string.length < 2) && all == 0) {
            freq_last_search_string = '';
            return false;
        } else {
            $(freq_list).show();
            clearTimeout(freq_timeout);
            freq_timeout = setTimeout(function() {
                if (search_freq_string == freq_last_search_string && all == 0) {
                    return false;
                }
                var url = "{{ route('extensionsvalley.item.ajaxGetFreequency') }}";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {
                        search_freq_string: search_freq_string,
                        patient_id: patient_id,
                        all: all
                    },
                    beforeSend: function() {
                        $('#FrequencyTable > tbody').html(
                            '<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>'
                        );
                    },
                    success: function(data) {

                        let response = data;
                        let res_data = "";


                        var freq_search_list = $('#ListFrequencySearchData');


                        if (response.length > 0) {
                            for (var i = 0; i < response.length; i++) {

                                let frequency = response[i].frequency;
                                let frequency_value = response[i].frequency_value;

                                res_data += '<tr  style="cursor:pointer"><td>' + frequency +
                                    '</td><input type="hidden" name="list_freq_value_hid[]" id="list_freq_value_hid-' +
                                    i + '" value="' + frequency_value +
                                    '"><input type="hidden" name="list_freq_name_hid[]" id="list_freq_name_hid-' +
                                    i + '" value="' + frequency + '"></tr>';
                            }
                        } else {
                            res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                        }

                        $(freq_search_list).html(res_data);
                        freq_last_search_string = search_freq_string;
                        $(".freq_theadscroll").animate({
                            scrollTop: 0
                        }, "slow");

                    },
                    complete: function() {
                        $('.freq_theadfix_wrapper').floatThead("reflow");
                    }
                });
            }, 500)

        }
    }
    //when select frequency
    $(document).on('dblclick', '#ListFrequencySearchData tr', function(event) {
        event.preventDefault();
        /* Act on the event */

        let tr = $(this);
        let name = $(tr).find('input[name="list_freq_name_hid[]"]').val();
        let value = $(tr).find('input[name="list_freq_value_hid[]"]').val();

        if (name != '' && value != '') {
            $('input[name="frequency"]').val(name);
            $('input[name="search_freq_value_hidden"]').val(value);
            $('input[name="search_freq_name_hidden"]').val(name);

            $(".frequency-list-div").hide();
        }

    });
    $(document).on('click', '.close_btn_freq_search', function(event) {
        event.preventDefault();
        /* Act on the event */
        $(".frequency-list-div").hide();
    });
    //************************************* FREEQUENCY SEARCH ENDS ***************************************

    //************************************* ROUTE SECTTION STARTS ************************************************

    //Route search
    var route_timeout = null;
    var route_last_search_string = '';
    $('#route').on('keyup', function() {
        event.preventDefault();
        /* Act on the event */
        var obj = $(this);

        searchRoute(obj);

    });

    $('#route').on('focus', function() {
        event.preventDefault();
        /* Act on the event */
        var obj = $(this);

        searchRoute(obj, 1);
    });

    function searchRoute(obj, all = 0) {
        var search_route_string = $(obj).val();
        var route_list = $('.route-list-div');

        if ((search_route_string == "" || search_route_string.length < 2) && all == 0) {
            route_last_search_string = '';
            return false;
        } else {
            var route_list = $('.route-list-div');
            $(route_list).show();
            clearTimeout(route_timeout);
            route_timeout = setTimeout(function() {
                if (search_route_string == route_last_search_string && all == 0) {
                    return false;
                }
                var url = "{{ route('extensionsvalley.item.ajaxGetRoute') }}";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {
                        search_route_string: search_route_string,
                        all: all
                    },
                    beforeSend: function() {
                        $('#RouteTable > tbody').html(
                            '<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>'
                        );
                    },
                    success: function(data) {

                        let response = data;
                        let res_data = "";

                        var route_search_list = $('#ListRouteSearchData');


                        if (response.length > 0) {
                            for (var i = 0; i < response.length; i++) {

                                let route = response[i].route;
                                let route_value = response[i].route_value;

                                res_data += '<tr style="cursor:pointer"><td>' + route +
                                    '</td><input type="hidden" name="list_route_value_hid[]" id="list_route_value_hid-' +
                                    i + '" value="' + route_value + '"></tr>';
                            }
                        } else {
                            res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                        }

                        $(route_search_list).html(res_data);
                        route_last_search_string = search_route_string;
                        $(".route_theadscroll").animate({
                            scrollTop: 0
                        }, "slow");

                    },
                    complete: function() {
                        $('.route_theadfix_wrapper').floatThead("reflow");
                    }
                });
            }, 500)

        }
    }

    //close route search
    $(document).on('click', '.close_btn_route_search', function(event) {
        event.preventDefault();
        /* Act on the event */
        $(".route-list-div").hide();
    });


    //when select route
    $(document).on('dblclick', '#ListRouteSearchData tr', function(event) {
        event.preventDefault();
        /* Act on the event */

        let tr = $(this);
        let route = $(tr).text();
        let value = $(tr).find('input[name="list_route_value_hid[]"]').val();
        if (route != '') {
            $('input[name="route"]').val(route);
            $('input[name="route_id"]').val(value);

            $(".route-list-div").hide();
        }

    });
    //************************************* ROUTE ENDS *********************************************
    $("#tax_exp").click(function() {
        $("#tax_exp_div").toggle();
    });

    function setDefault(i) {
        if ($('.pur_default_cls').is(':checked')) {
            return;
        } else if (i == 0) {
            $("#pur_default0").prop("checked", "checked");
        }
    }

    function setDefaultSell(i) {
        if ($('.sel_default_cls').is(':checked')) {
            return;
        } else if (i == 0) {
            $("#sel_default0").prop("checked", "checked");
        }
    }

    function select_generic_name(id, val_type, event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var ajax_div = 'generic_name_box-' + id;
        if (value.match(keycheck)) {
            var item_desc = $('#' + id).val();
            if (item_desc == "") {
                $("#" + ajax_div).html("");
            } else {
                $.ajax({
                    type: "GET",
                    url: "",
                    data: 'generic_desc=' + item_desc + '&search_generic_name=' + val_type,
                    beforeSend: function() {
                        $("#" + ajax_div).html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function(html) {
                        $("#" + ajax_div).html(html).show();
                        $("#" + ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function() {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
        }
    }

    function fillGenericValues(id, name, val_type) {
        if (val_type == 2) {
            $("#generic_name_search").val(name);
            $("#generic_name_id").val(id);
        }
        if (val_type == 1) {
            $("#generic_name_text_input").val(name);
            $("#generic_name").val(id);
        }
    }

    function serachFormSubmit() {
        $("#itemSearchForm").submit();
    }

    function zeroTest(element) {
        var flag = false;
        $.each(element, function(index, value) {
            if (value != '0') {
                flag = true;
            }
        });

        return flag;
    }

    function inputFormSubmit() {
        if ($("#item_type").val() == 0 || $("#item_type").val() == '-1') {
            $("#item_type").focus();
            toastr.warning('Plese Select Item Type !!');
            return false;
        }
        if ($("#category").val() == "-1") {
            $("#category").focus();
            toastr.warning('Plese Select Category !!');
            return false;
        }
        if ($("#subcategory").val() == "-1") {
            $("#subcategory").focus();
            toastr.warning('Plese Select Sub Category !!');
            return false;
        }
        if ($("#item_desc").val() == 0) {
            $("#item_desc").focus();
            toastr.warning('Plese Enter Product Name !!');
            return false;
        }
        if ($("#item_desc").val() == '-1') {
            $("#item_desc").focus();
            toastr.warning('Plese Enter Product Name !!');
            return false;
        }
        if ($("#std_uom").val() == '0') {
            toastr.warning('Plese Enter Standard UOM !!');
            return false;
        }
        if ($("#checkbox_lk").is(':checked')) {
            if ($('#lk_product').val().trim() == '') {
                toastr.warning('Plese Enter look a like product !!');
                return false;
            }

        }
        if ($("#checkbox_sd").is(':checked')) {
            if ($('#sd_product').val().trim() == '') {
                toastr.warning('Plese Enter sound a like product !!');
                return false;
            }
        }
        if ($("#checkbox_reuse").is(':checked')) {
            if ($('#reuse_count').val().trim() == '' || parseFloat(($('#reuse_count').val()) < 1)) {
                toastr.warning('Plese Enter Re usable count !!');
                return false;
            }
        }

        var purchase_uom = $("select[name='purchase_uom[]']")
            .map(function() {
                return $(this).val();
            }).get();
        var allZeros = zeroTest(purchase_uom);
        if (!allZeros) {
            toastr.warning('Plese Enter Purchase UOM !!');
            return false;
        }


        var selling_uom = $("select[name='selling_uom[]']")
            .map(function() {
                return $(this).val();
            }).get();
        var allZeros = zeroTest(selling_uom);
        if (!allZeros) {
            toastr.warning('Plese Enter Selling UOM !!');
            return false;
        }

        $("#itemForm").submit();
    }

    function selectCategoryInEdit(item_type, category_id, sub_cat_id) {
        if (item_type == 1) {
            $('.clinical_info_div').show();
        } else {
            $('.clinical_info_div').hide();
        }
        if (item_type) {
            var url = "{{ route('extensionsvalley.item.ajaxCategoryByItemType') }}";
            $.ajax({
                type: "GET",
                url: url,
                data: 'item_type=' + item_type,
                beforeSend: function() {},
                success: function(res) {
                    // console.log(res);
                    if (res) {
                        $("#category").empty();
                        $("#subcategory").empty();
                        $("#category").append('<option value="-1"> Select Category</option>');
                        $.each(res, function(key, value) {
                            if (category_id == key) {
                                var selects = "selected";
                            } else {
                                var selects = "";
                            }
                            $("#category").append('<option value="' + key + '" ' + selects + '>' +
                                value + '</option>');
                        });
                        if (category_id) {
                            var url = "{{ route('extensionsvalley.item.ajaxSubcategoryByCategory') }}";
                            $.ajax({
                                type: "GET",
                                url: url,
                                data: 'category=' + category_id,
                                beforeSend: function() {},
                                success: function(res) {
                                    // console.log(res);
                                    if (res) {
                                        $("#subcategory").empty();
                                        $("#subcategory").append(
                                            '<option value="-1"> Select Sub category</option>'
                                        );
                                        $.each(res, function(key, value) {
                                            if (sub_cat_id == key) {
                                                var selectss = "selected";
                                            } else {
                                                var selectss = "";
                                            }
                                            $("#subcategory").append('<option value="' +
                                                key + '" ' + selectss + '>' +
                                                value + '</option>');
                                        });
                                    } else {
                                        $("#subcategory").empty();
                                    }

                                },
                                error: function() {
                                    alert(
                                        'Please check your internet connection and try again'
                                    );
                                },
                                complete: function() {}
                            });
                        }
                    } else {
                        $("#category").empty();
                        $("#subcategory").empty();
                    }

                },
                error: function() {
                    alert('Please check your internet connection and try again');
                },
                complete: function() {}
            });
        } else {
            $('#item_type').focus();
            $("#category").empty();
            $("#subcategory").empty();
        }
    }

    function searchDoctorName(obj, e) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = String.fromCharCode(e.keyCode);
        if (value.match(keycheck) || e.keyCode == 8 || e.keyCode == 46) {
            var doctor_name = $(obj).val();
            if (doctor_name == "") {
                $("#doctor_namediv").html("");
            } else {
                var url = "";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'doctor_name=' + doctor_name,
                    beforeSend: function() {
                        $("#doctor_namediv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function(html) {
                        $("#doctor_namediv").html(html).show();
                        $("#doctor_namediv").find('li').first().addClass('liHover');
                    },
                    complete: function() {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown('doctor_namediv', e);
        }
    }

    function fillDoctorNameDetails(name, id) {
        $("#doctor_name").val(name);
        $("#doctor_id").val(id);
    }

    function searchManufacturerName(obj, e) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = String.fromCharCode(e.keyCode);
        if (value.match(keycheck) || e.keyCode == 8 || e.keyCode == 46) {
            var manufacturer_name = $(obj).val();
            if (manufacturer_name == "") {
                $("#manufacturer_div").html("");
            } else {
                var url = "";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'manufacturer_name=' + manufacturer_name,
                    beforeSend: function() {
                        $("#manufacturer_div").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function(html) {
                        $("#manufacturer_div").html(html).show();
                        $("#manufacturer_div").find('li').first().addClass('liHover');
                    },
                    complete: function() {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown('manufacturer_div', e);
        }
    }

    function fillManufacturerNameDetails(name, id) {
        $("#manufacturer_name_search").val(name);
        $("#manufacturer").val(id);
    }

    function saveNewGenericName() {
        var generic_name_search = $("#generic_name_text_input").val();
        var generic_id = $("#generic_name").val();
        var url = "{{ route('extensionsvalley.master.list_generic_name') }}";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                data: 1,
            },
            beforeSend: function() {
                $.LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });

            },
            success: function(data) {
                if (data) {
                    var html_content = $(data).find('#generic_form_whole');
                    $("#append_generic").html(html_content);
                    // $("#generic_name_update").val(generic_name_search);
                    $("#generic_cancel").hide();
                    $('#generic_form_whole').removeClass('col-md-4 padding_sm');
                    //    getexistingsubCategory(generic_id);
                    //  $('#therapeutic_category').val(data.);
                    $("#generic_modal").modal('show');

                    $.LoadingOverlay("hide");
                }
            },
            complete: function() {
                $(".ajaxSearchBox").hide();
            }
        });
    }

    function getexistingsubCategory(generic_id) {
        var generic_id = $("#generic_name").val();
        var url = "{{ route('extensionsvalley.master.getexistingsubCategory') }}";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                generic_id: generic_id,
            },
            beforeSend: function() {


            },
            success: function(data) {
                console.log(data)
                if (data) {
                    $('#eutic_category').val(data[0].thearapeutic_category_code);
                    $('#status_update').val(data[0].status);
                    $('#therapeutic_subcategory').val(data[0].generic_sub_category_code);

                }
            },
            complete: function() {

            }
        });
    }
    // function saveGenericName(){

    //    var url = $('#generic_form').attr('action');
    //    if($("#generic_name_update").val()==''){
    //        alert("Please enter generic name");
    //        return false;
    //    }
    //   $.ajax({
    //         type: "GET",
    //         url: url ,
    //         data: $("#generic_form").serialize(),
    //         beforeSend: function () {
    //                  $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
    //         },
    //         success: function (data) {
    //             var json_data = JSON.parse(data);
    //             if (json_data.success == 1) {
    //                 toastr.success(json_data.message);
    //                 $("#generic_name_text_input").val(json_data.generic_name);
    //                 $("#generic_name").val(json_data.generic_id);
    //                 $.LoadingOverlay("hide");
    //                 $('#generic_modal').modal('hide');

    //             }else{
    //                 toastr.warning("Error Occured !!!");
    //             }
    //         },
    //         complete: function () {
    //         }
    //     });
    // }
    function saveGenericName() {

        var url = $('#generic_form').attr('action');
        if ($("#generic_name_update").val() == '') {
            toastr.warning("Please enter generic name");
            return false;
        }
        $.ajax({
            type: "GET",
            url: url,
            data: $("#generic_form").serialize(),
            beforeSend: function() {
                $('#generic_form_whole').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function(data) {
                if (data) {
                    toastr.success("Successfully Added");
                    document.getElementById('generic_form').reset();
                }
            },
            complete: function() {
                $('#generic_form_whole').LoadingOverlay("hide");
            }
        });
    }

    function addManufacturerName() {
        var manufacturer_name = $("#manufacturer_name_search").val();
        $('#manufacturer_modal').modal('show');
        $("#manufacturer_name").val(manufacturer_name);
    }

    function saveNewManufacturerName() {
        var url = "{{ route('extensionsvalley.item.save_manufacturer_item') }}";
        if ($("#manufacturer_name").val() == '') {
            toastr.warning("Please enter manufacturer name");
            return false;
        }
        if ($("#manufacturer_code").val() == '') {
            toastr.warning("Please enter manufacturer code");
            return false;
        }
        $.ajax({
            type: "GET",
            url: url,
            data: $("#manufacturerForm").serialize(),
            beforeSend: function() {
                $.LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#337AB7'
                });
            },
            success: function(data) {
                var json_data = JSON.parse(data);
                if (json_data.success == 1) {
                    toastr.success(json_data.message);
                    $("#manufacturer_name_search").val(json_data.manufacturer_name);
                    $("#manufacturer").val(json_data.manufacturer_id);
                    $.LoadingOverlay("hide");
                    $('#manufacturer_modal').modal('hide');

                } else if (json_data.success == 2) {
                    toastr.warning("This manufacturer existing, please enter another !!!");
                    $.LoadingOverlay("hide");
                } else {
                    toastr.error("Error Occured !!!");
                    $.LoadingOverlay("hide");
                    $('#manufacturer_modal').modal('hide');
                }
            },
            complete: function() {}
        });
    }
    //-------------------------------------- redirect to ventor button function-------------------------------------------------
    function redirecttovendor() {

        window.location.href = window.location.href.replace('listItem', 'listVendor');

    };
    //--------------------------------------------------------------------------------------------------------------------------
    $(document).on('change', '#therapeutic_category', function() {

        var ward = $('#therapeutic_category').val();
        var load = $('#warning1').val();
        if (ward) {
            var url = "{!! route('extensionsvalley.master.relativetherapeuticsubcategory') !!}";
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'json',
                data: {
                    'ward': ward
                },
                beforeSend: function() {
                    $('#therapeutic_subcategory').append(
                        '<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>'
                    );
                    $("#therapeutic_subcategory").prop("disabled", true); //Disabl
                },
                success: function(html) {
                    if (html) {
                        $("#therapeutic_subcategory").empty()
                        $("#therapeutic_subcategory").html(
                            '<option value="">Select Therapeutic Subcategory</option>');
                        $.each(html, function(key, value) {
                            ($("#therapeutic_subcategory").append('<option value="' + value
                                .subcategory_code + '">' + value.subcategory_name +
                                '</option>'));
                        });
                        $('#therapeutic_subcategory').select();
                    } else {
                        $("#therapeutic_subcategory").empty();
                    }

                },

                error: function() {
                    Command: toastr["warning"]("Please  select a proper item !");
                    $("#therapeutic_subcategory").prop("disabled", false); //Disable



                },
                complete: function() {

                    $('#warning1').remove();
                    $("#therapeutic_subcategory").prop("disabled", false);


                }

            });

        } else {
            $('#therapeutic_category').focus();
            $("#therapeutic_subcategory").empty();
        }

    });

    function showLedgergroup(e) {
        if ($(e).is(':checked')) {
            $("#ledger_group_div").show();
        } else {
            $("#ledger_group_div").hide();
        }

    }

    function checkBarcodeExistorNot(val) {
        var base_url = $('#base_url').val();
        var url = base_url + "/item/checkBarcodeExistorNot";
        $.ajax({
            type: "POST",
            data: {
                data: val
            },
            url: url,
            beforeSend: function() {},
            success: function(data) {
                console.log(data);
                toastr.warning("" + data.message + "");
                $("#barcode").val('');

            },
            complete: function() {},
            error: function() {}
        });


    }

    function generateProductBarcode(item_id) {
        var confirm = window.confirm('Are you sure want to generate barcode ?');
        if (confirm) {
            var base_url = $('#base_url').val();
            var token = $('#token_hiddendata').val();
            var url = base_url + "/item/generateBarcode";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: token,
                    item_id: item_id
                },
                beforeSend: function() {
                    $('#genarate_barcodebtn' + item_id).attr('disabled', true);
                    $('#genarate_barcodespin' + item_id).removeClass('fa fa-barcode');
                    $('#genarate_barcodespin' + item_id).addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    if (data) {
                        toastr.success("Barcode Generated Successfully");
                        $('#genarate_barcodebtn' + item_id).remove();
                        $('#barcodeDataDiv' + item_id).html(data);
                        $('#change_barcodeprint_status_btn' + item_id).attr('disabled', false);
                    } else {
                        toastr.error('Please check your internet connection and try again');
                    }
                },
                error: function() {
                    toastr.error('Please check your internet connection and try again');
                },
                complete: function() {
                    $('#genarate_barcodebtn' + item_id).attr('disabled', false);
                    $('#genarate_barcodespin' + item_id).removeClass('fa fa-spinner fa-spin');
                    $('#genarate_barcodespin' + item_id).addClass('fa fa-barcode');
                }
            });
        }
    }

    function htmlDecode(input) {
        var e = document.createElement('textarea');
        e.innerHTML = input;
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
    }


    function change_barcodeprint_status(item_id) {
        var base_url = $('#base_url').val();
        var token = $('#token_hiddendata').val();
        var url = base_url + "/item/changeBarcodePrintStatus";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                item_id: item_id
            },
            beforeSend: function() {
                $('#change_barcodeprint_status_btn' + item_id).attr('disabled', true);
                $('#change_barcodeprint_status_spin' + item_id).removeClass('fa fa-print');
                $('#change_barcodeprint_status_spin' + item_id).addClass('fa fa-spinner fa-spin');
            },
            success: function(data) {
                if (data) {
                    toastr.success("Print Successfully Updated");
                } else {
                    toastr.error('Please check your internet connection and try again');
                }
            },
            error: function() {
                toastr.error('Please check your internet connection and try again');
            },
            complete: function() {
                $('#change_barcodeprint_status_btn' + item_id).attr('disabled', false);
                $('#change_barcodeprint_status_spin' + item_id).removeClass('fa fa-spinner fa-spin');
                $('#change_barcodeprint_status_spin' + item_id).addClass('fa fa-print');
            }
        });
    }

    function product_search(obj, e, type = 0) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = String.fromCharCode(e.keyCode);
        if (value.match(keycheck) || e.keyCode == 8 || e.keyCode == 46) {
            var item_search = $(obj).val();
            if (item_search == "") {
                $("#item_searchdiv").html("");
            } else {
                var url = "";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'item_search=' + item_search,
                    beforeSend: function() {
                        $("#item_searchdiv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function(html) {
                        $("#item_searchdiv").html(html).show();
                        $("#item_searchdiv").find('li').first().addClass('liHover');
                    },
                    complete: function() {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown('item_searchdiv', e);
        }
    }

    function fillProductSearch(e, code, name, id) {
        $("#item_search").val(htmlDecode(name));
        $("#item_search_id").val(id);
    }

    function setspecialprice(obj) {
        $('#special_price').val(obj.value);
    }

    $('.lk_like').on('change', function() {
        if ($(this).is(':checked')) {
            $('#look_a_like_div').fadeIn();
        } else {
            $('#look_a_like_div').fadeOut();
        }
    })
    $('.sd_like').on('change', function() {
        if ($(this).is(':checked')) {
            $('#sound_a_like_div').fadeIn();
        } else {
            $('#sound_a_like_div').fadeOut();
        }
    })
    $('#checkbox_reuse').on('change', function() {
        if ($(this).is(':checked')) {
            $('#reuse_div').fadeIn();
        } else {
            $('#reuse_div').fadeOut();
            $('#reuse_count').val('');
        }
    });

    function lookAlikeProduct(obj, e, type = 0) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = String.fromCharCode(e.keyCode);
        if (value.match(keycheck) || e.keyCode == 8 || e.keyCode == 46) {
            var product = $(obj).val();
            if (product == "") {
                $("#lk_productdiv").html("");
            } else {
                var url = "";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'lk_product=' + product,
                    beforeSend: function() {
                        $("#lk_productdiv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function(html) {
                        $("#lk_productdiv").html(html).show();
                        $("#lk_productdiv").find('li').first().addClass('liHover');
                    },
                    complete: function() {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown('lk_productdiv', e);
        }
    }

    function soundAlikeProduct(obj, e, type = 0) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = String.fromCharCode(e.keyCode);
        if (value.match(keycheck) || e.keyCode == 8 || e.keyCode == 46) {
            var sd_product = $(obj).val();
            if (sd_product == "") {
                $("#sd_productdiv").html("");
            } else {
                var url = "";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'sd_product=' + sd_product,
                    beforeSend: function() {
                        $("#sd_productdiv").html(
                            '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                        ).show();
                    },
                    success: function(html) {
                        $("#sd_productdiv").html(html).show();
                        $("#sd_productdiv").find('li').first().addClass('liHover');
                    },
                    complete: function() {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown('sd_productdiv', e);
        }
    }

    function fillProductStockDetails2(e, code, name, id) {
        $("#lk_product").val(name);
        $("#lk_product_id").val(id);
    }

    function fillProductStockDetails1(e, code, name, id) {
        $("#sd_product").val(name);
        $("#sd_product_id").val(id);
    }
    document.getElementById('myForm').addEventListener('submit', function(event) {
    var selectElement = document.getElementById('mySelect');
    var selectedValue = selectElement.value;

    if (selectedValue === '') {
      event.preventDefault();
      alert('Please select an option');
    } else {
      // Form validation is successful, allow the form to be submitted
    }
    });
    @include('Purchase::messagetemplate')
</script>

@endsection

