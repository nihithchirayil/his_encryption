<div class="x_panel">
    <div class="col-md-12 padding_sm">
        <div class="col-md-10 padding_sm">
                <label>Location Name</label> : {{$location_name}}
        </div>
    </div>
    </div>
    <div class="box no-border no-margin" style="height: 700px;">
    <div class="box-body clearfix">
        <div class="theadscroll" style="position: relative; height: 650px;">
            <table class="table no-margin  table-striped no-border theadfix_wrapper table-condensed styled-table"
                style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th width="30%">
                            <input id="issue_search_box" type="text" placeholder="Name Search.. " style="color: #000;display: none;width: 90%;" value="@if (isset($search_key)){{$search_key}}@endif">
                            <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                            <span id="item_search_btn_text"> Item Name</span>
                        </th>
                        <th>Reorder Level</th>
                        <th>Rack Name</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @if(sizeof($item_list) > 0)
                        @foreach ($item_list as $location)
                    <tr style='cursor: pointer;'onclick='changedetail("{{$location->item_desc}}","{{$location->id}}", "{{$location->location_id}}", "{{$location->reorder_level}}", "{{$location->rack}}", "{{$location->status}}");' >
                                <td class="item_name common_td_rules">{{ $location->item_desc }}</td>
                                <td class="reorder_level common_td_rules">{{ $location->reorder_level }}</td>
                                <td class="location_name common_td_rules">{{ $location->rack_name }}</td>
                                <td class="status common_td_rules">{{ $location->status_name }}</td>            
                    </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="location_code">No Records found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        @if($pr==1)   
        <div class="col-md-12" style="text-align:right;">
            <nav id="purchase_paging"> <?php echo $paginator->render(); ?>
            </nav>
        </div>
        @endif
        @if($pr==2)
        <div class="col-md-12" style="text-align:right;">
            <nav id="headsearch_paging"> <?php echo $paginator_h->render(); ?>
            </nav>
        </div>
        @endif
        </div>
    
        <div class="clearfix"></div>
    </div>
</div>
   
<!-- Modal -->
<div id="updateproductlocation_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Details :<p id="head_edit_box"></p></h4>
            </div>
           
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="theadscroll">
                            <input type="hidden" id="location_id_loc" value="">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Select Rack</label>
                                <div class="clearfix"></div>
                                {!! Form::select('rack', array(""=> " Select"), '',
                                ['class'=>"form-control", 'id'=>"rack_loc", 'required'=>"required"]) !!}
                                <input type="hidden" id="rack_loc_hidden" value="">
                            </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Re-order Level</label>
                                <div class="clearfix"></div>
                                <input type="number" id="reorder_level_loc" required autocomplete="off" name="reorder_level_loc" class="form-control">
                                <input type="hidden" id="reorder_level_loc_hidden" value="">
                            </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                <select class="form-control" name="status_loc" id="status_loc">
                                    <option value="1">{{__('Active')}}</option>
                                    <option value="0">{{__('Inactive')}}</option>
                                </select>
                                <input type="hidden" name="" id="status_loc_hidden" value="">
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span onclick="savechange(event);" class="btn light_purple_bg" id="savechange_basedonlocationsearch"><i class="fa fa-save" id="add_spin"></i> Save</span>
            </div>
            <input type="hidden" name="_token" id="_token" value="{{ Session::token() }}" />
        </form>
        </div>

    </div>
</div>
    





<script type="text/javascript">  
//----------------------------------------------------------pagination-------------------------------------------------------------------------------------------------------------------
$(document).ready(function() { 
    if($('#issue_search_box').val()){
        $("#issue_search_box").toggle();
        $("#item_search_btn_text").toggle();
}
 }); 
    function getURLParameter(url, name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,1])[1];
    }

    $("#locationlistDiv #purchase_paging .pagination a").click(function(e){
        e.preventDefault();
        var location_search_id =$("#location_search").val();
        var location_search_name =$("#location_search :selected").text();
        var url = $(this).attr('href');
        var page_no = getURLParameter(url, 'page');
        var url = "{{route('extensionsvalley.item.location_wise_product_list')}}";
        var param = {location_search_id: location_search_id,location_search_name: location_search_name,page_no:page_no};
        $.ajax({
            url:url,
            data: param,
            dataType: 'html',
            beforeSend: function () {
            $('#locationlistDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            }).done(function (data) {
                $('#locationlistDiv').LoadingOverlay("hide");
                $('#locationlistDiv').html(data);
            }).fail(function () {
                alert('Posts could not be loaded.');
            });
    });
 //------------------------------------------------------td onclick edit popup-------------------------------------------------------------------------------------------------------------    
    function changedetail(item_desc,id,location_id,reorder_level,rack,status){
    var item = $("#location_search").val();
    if(item == ''||item== 0){
        toastr.warning("Please select Any location");
    }
            $("#updateproductlocation_modal").modal('show');
            $('#head_edit_box').text(item_desc);
            $("#location_id_loc").val(id);
           // $("#location_id_loc").val(location_id);
            $("#reorder_level_loc").val(reorder_level);
            $("#reorder_level_loc_hidden").val(reorder_level);
            $("#rack_loc").val(rack);
            $("#rack_loc_hidden").val(rack);
            $("#status_loc").val(status);
            $("#status_loc_hidden").val(status);
            selectLocationRackloc(location_id,rack);
        }
//-----------------------------------------------------------------relative select box of rack onclick td----------------------------------------------------------------------------------------------
        function selectLocationRackloc(location_id,rack){
        if (location_id) {
        var url = "";
        $.ajax({
                type: "GET",
                url: url,
                data: 'rackByLocationId=1&location_id=' + location_id ,
                beforeSend: function () {
                    $("#rack_loc").empty();
                    $("#savechange_basedonlocationsearch").attr('disabled', true);
                    $('#rack_loc').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                },
                success: function (res) {
                // console.log(res);return false;
                if (res != 0){
                    $("#rack_loc").append('<option value=""> Select</option>');
                    $.each(res, function(key, value){
                        if(rack==key){
                            var selects = "selected";
                        }else{
                             var selects = "";
                        }
                        $("#rack_loc").append('<option value="' + key + '" '+ selects +'>' + value + '</option>');
                        $("#savechange_basedonlocationsearch").attr('disabled', false);
                        $('#rack_loc').LoadingOverlay("hide");
                });
                } else{
                    $("#rack_loc").empty();
                    $("#rack_loc").append('<option value=""> Select</option>');
                    $("#savechange_basedonlocationsearch").attr('disabled', false);
                    $('#rack_loc').LoadingOverlay("hide");



                }

                },
                error: function () {
                    alert('Please check your internet connection and try again');
                    $("#savechange_basedonlocationsearch").attr('disabled', false);
                    $('#rack_loc').LoadingOverlay("hide");



                },
                complete: function () {
                }
        });
    } else {
    $("#rack_loc").empty();
    }
}
//-----------------------------------------save buttin funtionality(popup)-------------------------------------------------------------------------------------
        function savechange(e){
        e.preventDefault();
        $('#savechange_basedonlocationsearch').attr('disabled', true);
        var product_location_id=$("#location_id_loc").val();
        var reorder_level = $("#reorder_level_loc").val();
        var reorder_level_old = $("#reorder_level_loc_hidden").val();
        var rack = $("#rack_loc").val();
        var rack_old = $("#rack_loc_hidden").val();
        var status = $("#status_loc").val();
        var status_old = $("#status_loc_hidden").val();
        var location_search_id=$("#location_search").val();
        var location_search_name=$("#location_search :selected").text();
        if(reorder_level == "" && rack == ""){
            toastr.warning("Please Fill Rack Or Re-order Level");
            $('#savechange_basedonlocationsearch').attr('disabled', false);
            return false;
        }
       
        if (product_location_id) {
        var param = { update_data: '1',reorder_level_old: reorder_level_old,rack_old: rack_old,status_old: status_old, product_location_id: product_location_id,reorder_level: reorder_level, rack: rack, status: status ,location_search_id: location_search_id,location_search_name: location_search_name};
        var url = "{{route('extensionsvalley.item.edit_based_on_location_search')}}"
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function() {
                $("#add_spin").removeClass('fa fa-save');
                $("#add_spin").addClass('fa fa-spinner fa-spin');
            },
            success: function(res) {
                console.log(res);
                if(res == 1){
                    toastr.success("Details Updated Successfully.");
                    $("#add_spin").removeClass('fa fa-spinner fa-spin');
                    $("#add_spin").addClass('fa fa-save');
                    $('#savechange_basedonlocationsearch').attr('disabled', false);
                    $("#updateproductlocation_modal").modal('hide');
                   // itemwisesearchtableData(location_search_id,location_search_name);
                }
                if(res == 2){
                    toastr.warning("No Change Found.");
                    $("#add_spin").removeClass('fa fa-spinner fa-spin');
                    $("#add_spin").addClass('fa fa-save');
                    $('#savechange_basedonlocationsearch').attr('disabled', false);

                }
            },
            complete: function() {
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
                $('#savechange_basedonlocationsearch').attr('disabled', false);

            }
        });
    } else {
        toastr.warning("Please Select Any Record");
        $('#savechange_basedonlocationsearch').attr('disabled', false);

    }
}

//------------------------------------------------------------------search on table head--------------------------------------------------------------------------------------
$("#item_search_btn").click(function(){
    $("#issue_search_box").toggle();
    $("#item_search_btn_text").toggle();
     $('#issue_search_box').focus();

    });
    $('#issue_search_box').on('keyup',function(){
        var item_search_key=$('#issue_search_box').val();
        $('#item_name_hidden_search').val(item_search_key);
        var location_search_name =$("#location_search :selected").text();
        var location_search_id =$("#location_search").val();
        tableheadsearch(item_search_key,location_search_name,location_search_id)
        });
function tableheadsearch(item_search_key,location_search_name,location_search_id){
    if (item_search_key) {
        var url = "{{route('extensionsvalley.item.headsearch')}}";
        var param = {item_search_key: item_search_key,location_search_name: location_search_name,location_search_id: location_search_id};
        $.ajax({
            type: "GET",
            url: url,
            data: param,
          
            success: function(data) {
                $('#locationlistDiv').html(data);
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } 


}
//-------------------------------------------------------pagination based on head search function----------------------------------------------------------------------------------------------------
$("#locationlistDiv #headsearch_paging .pagination a").click(function(e){
    function getURLParameter_h(url, name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,1])[1];
    }
        e.preventDefault();
        var item_search_key=$('#item_name_hidden_search').val();
        var location_search_name =$("#location_search :selected").text();
        var location_search_id =$("#location_search").val();
         var url = $(this).attr('href');
        var page_no = getURLParameter_h(url, 'page');
        var url = "{{route('extensionsvalley.item.headsearch')}}";
        var param = {item_search_key: item_search_key,location_search_name: location_search_name,location_search_id: location_search_id,page_no:page_no};
        $.ajax({
            url:url,
            data: param,
            dataType: 'html',
            beforeSend: function () {
            $('#locationlistDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            }).done(function (data) {
                $('#locationlistDiv').LoadingOverlay("hide");
                $('#locationlistDiv').html(data);
            }).fail(function () {
                alert('Posts could not be loaded.');
            });
    }); 
 
//--------------------------------------------------end-----------------------------------------------------------------------------------------------------------------------------------------------
    
</script>
    
    