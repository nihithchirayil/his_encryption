@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style> 
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.item.listTaxMaster')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Tax Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="tax_name" 
                                    value="{{ $searchFields['tax_name'] ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                {!! Form::select('status', array("-1"=> " Select Status","1"=> " Active", "0"=> " Inactive"),$searchFields['status'] ?? '', [
                                            'class'       => 'form-control',
                                        ]) !!}
                                    </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Tax Code</th>
                                    <th>Tax Name</th>
                                    <th>Tax %</th>
                                    <th>Parent Tax %</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($tax_list) > 0)
                                    @foreach ($tax_list as $tax)
                                        <tr style="cursor: pointer;" onclick="taxEditLoadData(this,
                                            '{{$tax->id}}',
                                            '{{$tax->tax_code}}',
                                            '{{$tax->tax_name}}', 
                                            '{{$tax->tax_percent}}',
                                            '{{$tax->parent_tax_percent}}',
                                            '{{$tax->status}}');" >
                                            <td class="tax_code common_td_rules">{{ $tax->tax_code }}</td>
                                            <td class="tax_name common_td_rules">{{ $tax->tax_name }}</td>
                                            <td class="tax_percent common_td_rules">{{ $tax->tax_percent }}</td>
                                            <td class="parent_tax_percent common_td_rules">{{ $tax->parent_tax_percent }}</td>
                                            <td class="status common_td_rules">{{ $tax->status_name }}</td>
                                            
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="tax_code">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.item.saveTaxMaster')}}" method="POST" id="taxForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="tax_master_id" id="tax_master_id" value="0">
                        <div class="col-md-12 padding_sm">
                           <div class="mate-input-box">
                            <label for="">Tax Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" required  name="tax_name" value="{{request()->old('tax_name')}}" id="tax_name">
                            <span class="error_red">{{ $errors->first('tax_name') }}</span>
                          </div> 
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="form-group {{ $errors->has('tax_code') ? 'has-error' : '' }} control-required">
                                <div class="mate-input-box">
                            <label for="">Tax Code<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" value="{{request()->old('tax_code')}}" required name="tax_code" id="tax_code">
                            <span class="error_red">{{ $errors->first('tax_code') }}</span>
                            <div class="" id="tax_code_status">
                            </div>
                        </div>
                        </div>
                        </div>

                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Tax %</label><span class="error_red"></span>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control"  name="tax_percent" value="{{request()->old('tax_percent')}}" id="tax_percent">
                            <span class="error_red">{{ $errors->first('tax_percent') }}</span>
                          </div>  
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Parent Tax %</label><span class="error_red"></span>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control"   name="parent_tax_percent" value="{{request()->old('parent_tax_percent')}}" id="parent_tax_percent">
                            <span class="error_red">{{ $errors->first('parent_tax_percent') }}</span>
                          </div>  
                        </div>
                        
                        <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status<span class="error_red">*</span></label>
                                <div class="clearfix"></div>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">{{__('Active')}}</option>
                                    <option value="0">{{__('Inactive')}}</option>
                                </select>
                                <span class="error_red">{{ $errors->first('status') }}</span>
                                </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        $('#tax_code').keyup(function () {

            var location_code_str = $(this).val();
            location_code_str = location_code_str.replace(/[^a-zA-Z0-9]/g, '-');

            location_code_str = location_code_str.replace(/\-+/g, '');

            location_code_str = location_code_str.toUpperCase();

            var location_code = location_code_str.trim();

            $('#tax_code').val(location_code);

            if (location_code != '') {
                var url = '{{route('extensionsvalley.item.checkTaxCode')}}';
                $.ajax({
                    method: "GET",
                    url: url,
                    data: 'tax_code=' + location_code,
                    success: function (data) {
                        console.log(data);
                        if (data == 0) {
                            $('#tax_code').css('color', 'green');
                            $('#tax_code_status').html(
                                '<button tabindex="-1" type="button" title="Available" class="btn btn-success btn-sm"><i class="fa fa-check fa-xs" aria-hidden="true"></i></button>'
                            );

                        } else {
                            $('#tax_code').css('color', 'red');
                            $('#tax_code_status').html(
                                '<button tabindex="-1" type="button" title="Not available" class="btn btn-danger btn-sm"><i class="fa fa-ban fa-xs" aria-hidden="true"></i></button>'
                            );
                        }
                    }

                });

            }


        });

    });

    function taxEditLoadData(obj,id,code,name,tax_percent,parent_tax_percent,status){
        $('#tax_master_id').val(id);
        $('#tax_code').prop('readonly',true).val(code);
        $('#tax_name').val(name);
        $('#tax_percent').val(tax_percent);
        $('#parent_tax_percent').val(parent_tax_percent);
        $('#status').val(status);
        $('#taxForm').attr('action', '{{$updateUrl}}');
    }
    function resetFunction(){
         $('#taxForm')[0].reset();
    }
@include('Purchase::messagetemplate')
</script>
    
@endsection
