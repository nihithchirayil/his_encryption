<style>
    .floatThead-container {
        left: 15px !important;
    }

    .bootstrap-datetimepicker-widget.dropdown-menu {
        width: auto;
    }

    .table td {
        position: relative !important;
    }
</style>
<div class="row">
    <div class="theadscroll always-visible" id="result_container_div"
        style="position: relative;height:90%;border-radius:10px;width:100%;">

        @if ($res > 0)

            <div class="panel-body" style="min-height: 300px;">
                {{-- <h6><b>Product Name</b>:{{$item_desc}}</h6><h6 style=""><b>Location</b>:{{$location_name}}</h6> --}}
                @csrf

                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
                    style="font-size: 12px;">
                    <thead>
                        <tr class="table_header_bg">
                            <th style="padding: 2px;">Batch No.</th>
                            <th style="padding: 2px; ">Stock</th>
                            <th style="padding: 2px;">Purchase Cost</th>
                            <th style="padding: 2px;">Mrp</th>
                            <th style="padding: 2px;">Selling Price</th>
                            <th style="padding: 2px;"width="10%">Tax</th>
                            <th style="padding: 2px;">Expiry Date</th>
                            <th style="padding: 2px;"width='8%'><i class="fa fa-save"></i></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($res as $data)
                            <tr>
                                <td class="batch_no_row"><input type="text"
                                        name="old_batch[]"style="font-weight: bold;" class="form-control old_batch"
                                        readonly="readonly" value="{{ $data->batch_no }}"></td><input
                                    type="hidden"class="item_code_save" value="{{ $data->item_code }}"><input
                                    type="hidden"class="item_desc_save" value="{{ $data->item_desc }}">
                                <td class="added_qty_row"><input type="text" name="added_qty[]"
                                        class="form-control old_stock"readonly="readonly" value="{{ $data->stock }}">
                                </td>
                                <td class="cost_row"><input type="number" name="cost[]" class="form-control new_cost"
                                        value="{{ $data->unit_purchase_cost }}"><input type="hidden" name="cost_old[]"
                                        class="form-control old_cost"readonly="readonly"
                                        value="{{ $data->unit_purchase_cost }}"></td>
                                <td class="mrp_row"><input type="number" name="mrp[]" class="form-control new_mrp"
                                        value="{{ $data->unit_mrp }}"><input type="hidden" name="mrp_old[]"
                                        class="form-control old_mrp"readonly="readonly" value="{{ $data->unit_mrp }}">
                                </td>
                                <td class="selling_price_row"><input type="number" name="selling_price[]"
                                        class="form-control new_selling_price"value="{{ $data->unit_sales_rate }}"><input
                                        type="hidden" name="selling_price_old[]" class="form-control old_selling_price"
                                        value="{{ $data->unit_sales_rate }}"></td>
                                <td class="tax_row"><input type="number" name="tax[]"
                                        class="form-control new_tax_rate"
                                        value="{{ $data->unit_tax_rate != 'NaN' && $data->unit_tax_rate != null ? $data->unit_tax_rate : '0' }}"><input
                                        type="hidden" name="tax_old[]" class="form-control old_tax_rate"
                                        value="{{ $data->unit_tax_rate != 'NaN' && $data->unit_tax_rate != null ? $data->unit_tax_rate : '0' }}">
                                </td>
                                <td class="expiry_month_row"><input class="form-control  new_expiry_date" type="text"
                                        autocomplete="off" placeholder="MM-YYYY" value="{{ $data->expiry_date }}"
                                        data="date">
                                    <input class="form-control old_expiry_date" name="expiry_date_old[]"
                                        type="hidden"value="{{ $data->expiry_date }}">
                                </td>
                                <td width="5%"><button class="btn btn-primary btn-sm form-action-btn"
                                        name="search_results" id="as_user" style="display: none;font-size:12px;"><i
                                            class="fa fa-save" id="add_spin"></i>
                                        save
                                    </button></td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6" style="text-align: center;">No Results Found!</td>
                        </tr>
        @endif
        </tbody>
        </table>
    </div>
</div>
</div>



<script type="text/javascript">
    //-----------------------------------------------datepicker in popup--------------------------------------------------------------------------------
    $(document).ready(function() {
        $('.new_expiry_date').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    });
    //-------------------------------------------------blur function to check the changes in old and new data------------------------------------------------
    $(document).on('blur', '.new_mrp', function() {
        var new_mrp = parseFloat($(this).parents('tr').find('.new_mrp').val());
        var old_mrp = parseFloat($(this).parents('tr').find('.old_mrp').val());
        if (new_mrp != old_mrp) {
            $(this).parents('tr').find('#as_user').css('display', 'block');
        }
    });
    $(document).on('blur', '.new_cost', function() {
        var new_cost = parseFloat($(this).parents('tr').find('.new_cost').val());
        var old_cost = parseFloat($(this).parents('tr').find('.old_cost').val());
        if (new_cost != old_cost) {
            $(this).parents('tr').find('#as_user').css('display', 'block');
        }
    });
    $(document).on('blur', '.new_selling_price', function() {
        var new_selling_price = parseFloat($(this).parents('tr').find('.new_selling_price').val());
        var old_selling_price = parseFloat($(this).parents('tr').find('.old_selling_price').val());
        if (new_selling_price != old_selling_price) {
            $(this).parents('tr').find('#as_user').css('display', 'block');
        }
    });
    $(document).on('blur', '.new_tax_rate', function() {
        var new_tax_rate = parseFloat($(this).parents('tr').find('.new_tax_rate').val());
        var old_tax_rate = parseFloat($(this).parents('tr').find('.old_tax_rate').val());
        if (new_tax_rate != old_tax_rate) {
            $(this).parents('tr').find('#as_user').css('display', 'block');
        }
    });
    $(document).on('blur', '.new_expiry_date', function() {
        var new_expiry_date = ($(this).parents('tr').find('.new_expiry_date').val());
        var old_expiry_date = ($(this).parents('tr').find('.old_expiry_date').val());
        if (new_expiry_date != old_expiry_date) {
            $(this).parents('tr').find('#as_user').css('display', 'block');
        }
    });
    //-------------------------------------------------------------------------------function to fetch data from perticular td---------------------------------------------------------------------------
    $('body').off().on('click', '#as_user', function(event) {
        event.stopPropagation();
        event.bubbles = false;
        $(this).parents('tr').find('#as_user').attr("disabled", "disabled");
        var location_code = $('#location').val();
        var location_name = $("#location option:selected").html()
        var item_code = ($(this).parents('tr').find('.item_code_save').val());
        var item_desc = ($(this).parents('tr').find('.item_desc_save').val());
        var item_type = $('#item_type').val();
        var old_batch = ($(this).parents('tr').find('.old_batch').val());
        var new_cost = getNum(parseFloat($(this).parents('tr').find('.new_cost').val()));
        var old_cost = getNum(parseFloat($(this).parents('tr').find('.old_cost').val()));
        var new_mrp = getNum(parseFloat($(this).parents('tr').find('.new_mrp').val()));
        var old_mrp = getNum(parseFloat($(this).parents('tr').find('.old_mrp').val()));
        var new_tax_rate = getNum(parseFloat($(this).parents('tr').find('.new_tax_rate').val()));
        var old_tax_rate = getNum(parseFloat($(this).parents('tr').find('.old_tax_rate').val()));
        var new_selling_price = getNum(parseFloat($(this).parents('tr').find('.new_selling_price').val()));
        var old_selling_price = getNum(parseFloat($(this).parents('tr').find('.old_selling_price').val()));
        var new_expiry_date = ($(this).parents('tr').find('.new_expiry_date').val());
        var old_expiry_date = ($(this).parents('tr').find('.old_expiry_date').val());
        var parent_batch = ($(this).parents('tr').find('.old_batch').val());
        var save_spin = $(this).parents('tr').find('#add_spin');
        var save_button = $(this).parents('tr').find('#as_user');
        var new_old_cost = $(this).parents('tr').find('.old_cost');
        var new_old_mrp = $(this).parents('tr').find('.old_mrp');
        var new_old_tax_rate = $(this).parents('tr').find('.old_tax_rate');
        var new_old_selling_price = $(this).parents('tr').find('.old_selling_price');
        var new_old_expiry_date = $(this).parents('tr').find('.old_expiry_date');

        function getNum(val) {
            if (isNaN(val)) {
                return 0;
            }
            return val;
        }

        var param = {
            item_desc: item_desc,
            old_selling_price: old_selling_price,
            new_selling_price: new_selling_price,
            parent_batch: parent_batch,
            old_batch: old_batch,
            new_cost: new_cost,
            old_cost: old_cost,
            new_mrp: new_mrp,
            old_mrp: old_mrp,
            new_tax_rate: new_tax_rate,
            old_tax_rate: old_tax_rate,
            new_expiry_date: new_expiry_date,
            old_expiry_date: old_expiry_date,
            old_batch: old_batch,
            location_code: location_code,
            item_code: item_code,
            item_type: item_type,
            _token: " {{ csrf_token() }}",
        };
        var url = route_json.editItemDetalis;
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            dataType: "JSON",
            beforeSend: function() {
                save_spin.removeClass('fa fa-save');
                save_spin.addClass('fa fa-spinner fa-spin');
                save_button.attr("disabled", "disabled");
            },
            success: function(data) {
                save_spin.removeClass('fa fa-spinner fa-spin');
                save_spin.addClass('fa fa-save');
                if (data == 2) {
                    toastr.warning(" No Change Found !! ");
                    save_button.removeAttr("disabled");
                    save_button.css('display', 'none');

                }
                if (data['result'] == 1) {
                    toastr.success(item_desc + ": Detail Updated successfully");
                    new_old_cost.val(data['new_cost']);
                    new_old_mrp.val(data['new_mrp']);
                    new_old_tax_rate.val(data['new_tax_rate']);
                    new_old_selling_price.val(data['new_selling_price']);
                    new_old_expiry_date.val(data['new_expiry_date']);
                    save_button.removeAttr("disabled");
                    save_button.css('display', 'none');
                }
                if (data == 0) {
                    toastr.error("Network error !!");
                    location.reload();

                }
            }
        });

    });
</script>
