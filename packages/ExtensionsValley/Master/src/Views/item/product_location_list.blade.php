<div class="x_panel">
<div class="col-md-12 padding_sm">
    <div class="col-md-10 padding_sm">
            <label>Item Name</label> : <?=strtoupper($item_name)?>
    </div>
</div>
</div>
<div class="box no-border no-margin">
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 350px;">
        <table class="table no-margin  table-striped no-border theadfix_wrapper table-condensed styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th>Location Name</th>
                    <th>Reorder Level</th>
                    <th>Rack Name</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if(count($location_list) > 0)
                    @foreach ($location_list as $location)
                <tr style='cursor: pointer;' onclick='editLocation("{{$location->id}}", "{{$location->location_name}}", "{{$location->location_id}}", "{{$location->reorder_level}}", "{{$location->rack}}", "{{$location->status}}");' >
                            <td class="location_name common_td_rules">{{ $location->location_name }}</td>
                            <td class="reorder_level common_td_rules">{{ $location->reorder_level }}</td>
                            <td class="location_name common_td_rules">{{ $location->rack_name }}</td>
                            <td class="status common_td_rules">{{ $location->status_name }}</td>            
                </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5" class="location_code">No Records found</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
</div>
</div>

