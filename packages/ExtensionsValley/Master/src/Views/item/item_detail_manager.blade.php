@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" value='<?= $route_data ?>' id="route_value">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">

    <div class="right_col">
        <div class="row">
            <div class="col-md-2 padding_sm pull-right">
                Item Detail Manager
            </div>
        </div>
        <div class="container-fluid">
            <div class="col-md-12 padding_sm">
                <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div class="row" id="filters_container">
                                <div class="col-md-12 padding_sm">
                                    {!! Form::open(['name' => 'search_from', 'id' => 'search_from']) !!}
                                    <?php
                                         $i = 1;

                            foreach ($showSearchFields as $fieldType => $fieldName) {
                                $fieldTypeArray[$i] = explode('-', $fieldType);

                                if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                    $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                    ?>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">{{ $fieldTypeArray[$i][2] }}</label>
                                            <input class="form-control hidden_search" value="" autocomplete="off"
                                                type="text" placeholder="Enter Item Name"
                                                id="{{ $fieldTypeArray[$i][1] }}" name="{{ $fieldTypeArray[$i][1] }}" />
                                            <div id="{{ $fieldTypeArray[$i][1] }}AjaxDiv" class="ajaxSearchBox"
                                                style="width: 100%;position: absolute;min-height:20px;max-height:209px;margin-top:20px;">
                                            </div>
                                            <input class="filters" value="{{ $hiddenFields[$hidden_filed_id] }}"
                                                type="hidden" name="{{ $fieldTypeArray[$i][1] }}_hidden" value=""
                                                id="{{ $fieldTypeArray[$i][1] }}_hidden">
                                        </div>
                                    </div>

                                    <?php
                                }  else if ($fieldTypeArray[$i][0] == 'combo') {
                                    ?>
                                    <div class="col-md-2 padding_sm date_filter_div">
                                        <div class="mate-input-box">
                                            <label>{{ $fieldTypeArray[$i][2] }}</label>
                                            <div class="clearfix"></div>
                                            {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, [
                                                'class' => 'form-control select2 filters ',
                                                'placeholder' => $fieldTypeArray[$i][2],
                                                'id' => $fieldTypeArray[$i][1],
                                            ]) !!}
                                        </div>
                                    </div>
                                    <?php
                                }

                                $i++;

                            }
                            ?>

                                    <div class="col-md-2 padding_sm" style="margin-top: 18px;">
                                        <div class="clearfix"></div>
                                        <a class="btn btn-primary pull-right" onclick="getCollectionReportData();"
                                            name="search_results" id="batchwise_search">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                            Get Item Detail
                                        </a>
                                    </div>
                                    {!! Form::token() !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="row" id="datatable">
                            <div class="theadscroll always-visible" id="ResultDataContainer"
                                style="height: 586px;padding: 0px;display: block;font-family: poppinsregular;width: 100%;padding-left: -53px;">
                                <div
                                    style="background: white; display: block;margin: 0 auto;margin-bottom: 0.5cm;width: 100%;">
                                    <div style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm;;
                                    width: 100%;
                                    padding: 16px;min-height:586px;"
                                        id="ResultsViewArea">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="datapopup" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content" style="width:1000px;max-height:821px; min-height:400px;">
                <div style="background-color:#2e6da4;text-align:center;" id="popup2"
                    class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title"></h4>
                </div>
                <div class="modal-body" style="width:100%;min-height:300px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="popup" class="theadscroll always-visible"
                                style="position: relative;width:100%;min-height:400px;max-height:600px;">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/itemdetailmanager.js') }}"></script>

    <script type="text/javascript"></script>
@endsection
