@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 200px;
        margin: 0px 0px 0px 0px;
        overflow-y: auto;
        width: 34%;
        z-index: 599;
        position:absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
    table td{
        position: relative !important;
    }
</style>
@endsection
@section('content-area')
<div class="right_col" role="main">
    <div class="row padding_sm">
    <div class="col-md-2 padding_sm pull-right">
        <?=$title?>
    </div>
    <input type="hidden" id="base_url" value="{{URL::to('/')}}">
    </div>
     <form id="add_item_location_form">
            {!! Form::token() !!}
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">
                    <div class="col-md-3 padding_sm" id="itemnamesearch_box">
                        <div class="mate-input-box" >
                        <label> Item Name</label>
                        <div class="clearfix"></div>
                        <input type="text" name="item_name" autocomplete="off" id="item_name" class="form-control" onkeyup="searchItemName(this,event)">
                        <div class="ajaxSearchBox" id="itemnamediv" style="text-align: left; list-style: none;  cursor: pointer; max-height: 400px; margin: -2px 0px 0px 0px; overflow-y: auto; width: 100%; z-index: 3000;  position:absolute; background: #ffffff;  border-radius: 3px;height: 270px; border: 1px solid rgba(0, 0, 0, 0.3);"> 
                        </div>
                        <input type="hidden" name="item_id" id="item_id">
                    </div>
                    </div>
                    <div class="col-md-3 padding_sm" id="locationnamesearch_box">
                        <div class="mate-input-box">
                            <label>Location</label>
                        <div class="clearfix"></div><input type="hidden" id="item_name_hidden_search" value="">
                            {!! Form::select('location_search',array("0"=> " Choose Location") + ExtensionsValley\Master\Models\Location::getLocations()->toArray(), (request()->has('id')) ?  request()->get('id') : null, ['class' => 'select2','id' => 'location_search', 'style' => 'width: 100%; color:#555555; padding:4px 12px;']) !!}
                    </div>
                    </div>
                    <div class="col-md-2 padding_sm" style="margin-top: 10px;" id="addlocation_button">
                        <button type="button" onclick="addLocation();"  id="add_location" class="btn btn-primary">Add Location <i id="add_locationspin"></i></button>
                    </div>
                    <div class="col-md-1 padding_sm" style="float: right; margin-top: 10px;">
                        <div class="clearfix"></div>
                        <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel" id="itemlocationlistDiv">
                   <span class="text-center"><label style="text-align: center;">Please Select Any Item</label></span>
                </div>
            </div>
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel" id="locationlistDiv" style="display: none">
                   <span class="text-center"><label style="text-align: center;">Please Select Any Location</label></span>
                </div>
            </div>
        </div>
</div>

<!-- Modal -->
<div id="addlocation_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Product Location</h4>
            </div>
           
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="theadscroll">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Select Location</label>
                                <div class="clearfix"></div>
                                {!! Form::select('location_id', array(""=> " Select") + $locations->toArray(), '',
                                ['class'=>"form-control", 'id'=>"location_id", 'required'=>"required"]) !!}
                            </div></div>

                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Select Rack</label>
                                <div class="clearfix"></div>
                                {!! Form::select('rack', array(""=> " Select"), '',
                                ['class'=>"form-control", 'id'=>"rack", 'required'=>"required"]) !!}
                            </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Re-order Level</label>
                                <div class="clearfix"></div>
                                <input type="number" id="reorder_level" required autocomplete="off" name="reorder_level" class="form-control">
                            </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">{{__('Active')}}</option>
                                    <option value="0">{{__('Inactive')}}</option>
                                </select>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span onclick="saveProductLocation(event);" class="btn light_purple_bg"><i class="fa fa-save"></i> Save</span>
            </div>
            <input type="hidden" name="_token" id="_token" value="{{ Session::token() }}" />
        </form>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="editlocation_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
<form>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Product Location</h4>
            </div>
           
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="theadscroll">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Select Location</label>
                                <div class="clearfix"></div>
                                <input type="hidden" name="product_location_hidden_id" id="product_location_hidden_id">
                                 {!! Form::select('e_location_id', array(""=> " Select") + $locations->toArray(), '',
                                ['class'=>"form-control", 'id'=>"e_location_id", 'required'=>"required"]) !!}
                        </div>
                        </div>

                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Select Rack</label>
                                <div class="clearfix"></div>
                                {!! Form::select('e_rack', array(""=> " Select") + $racks->toArray(), '',
                                ['class'=>"form-control", 'id'=>"e_rack", 'required'=>"required"]) !!}
                            </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Re-order Level</label>
                                <div class="clearfix"></div>
                                <input type="number" required autocomplete="off" name="e_reorder_level" id="e_reorder_level" class="form-control">
                            </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                                <select class="form-control" name="e_status" id="e_status">
                                    <option value="1">{{__('Active')}}</option>
                                    <option value="0">{{__('Inactive')}}</option>
                                </select>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn light_purple_bg" onclick="updateProductLocation(event);" ><i class="fa fa-save"></i> Save</span>
            </div>
        </div>
</form>
    </div>
</div>
@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script type="text/javascript">
$(document).ready(function () {

var $table = $('table.theadfix_wrapper');

$table.floatThead({
    scrollContainer: function ($table) {
        return $table.closest('.theadscroll');
    }
});

$('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
    minScrollbarLength: 30
});

$('.fixed_header').floatThead({
    position: 'absolute',
    scrollContainer: true
});

$("#location_id").on('change', function() {

    var location_id = $('#location_id').val();
    if (location_id) {
        var url = "";
        $.ajax({
                type: "GET",
                url: url,
                data: 'rackByLocationId=1&location_id=' + location_id,
                beforeSend: function () {
                },
                success: function (res) {
                // console.log(res);return false;
                if (res != 0){
                    $("#rack").empty();
                    $("#rack").append('<option value=""> Select</option>');
                    $.each(res, function(key, value){
                        $("#rack").append('<option value="' + key + '">' + value + '</option>');
                    });
                } else{
                    $("#rack").empty();
                    $("#rack").append('<option value=""> Select</option>');
                }

                },
                error: function () {
                    alert('Please check your internet connection and try again');
                },
                complete: function () {
                }
        });
    } else {
    $('#location_id').focus();
    $("#rack").empty();
    }
});  
$("#e_location_id").on('change', function() {
    var location_id = $('#e_location_id').val();
    if (location_id) {
        var url = "";
        $.ajax({
                type: "GET",
                url: url,
                data: 'rackByLocationId=1&location_id=' + location_id ,
                beforeSend: function () {
                    $("#e_rack").empty();
                },
                success: function (res) {
                // console.log(res);return false;
                if (res != 0){
                    $("#e_rack").append('<option value=""> Select</option>');
                    $.each(res, function(key, value){
                        $("#e_rack").append('<option value="' + key + '">' + value + '</option>');
                    });
                } else{
                    $("#e_rack").empty();
                    $("#e_rack").append('<option value=""> Select</option>');
                }

                },
                error: function () {
                    alert('Please check your internet connection and try again');
                },
                complete: function () {
                }
        });
    } else {
    $('#e_location_id').focus();
    $("#e_rack").empty();
    }
}); 
});
function addLocation(){
    var item = $("#item_name").val();
    if(item == ''){
        toastr.warning("Please select Any Item");
    }else{
        var item_id = $("#item_id").val();
        if(item_id == ""){
            toastr.warning("Please select Valid Item");
        }else{
            $("#addlocation_modal").modal('show');
        }

    }
}
function searchItemName(obj,e) { 
        var keycheck = /[a-zA-Z0-9 ]/; 
        var value = String.fromCharCode(e.keyCode);
        if (value.match(keycheck) || e.keyCode == 8 || e.keyCode == 46) {
            var item_name = $(obj).val();
            if (item_name == "") {
                $("#itemnamediv").html("");
            } else {
            var url = "";
                $.ajax({
                type: "GET",
                    url: url,
                    data:  'item_name=' + item_name,
                    beforeSend: function () {
                    $("#itemnamediv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#itemnamediv").html(html).show();
                        $("#itemnamediv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            }

            }else {
            ajaxProgressiveKeyUpDown('itemnamediv', e);
            }
}
function itemLocationTable(item_id,name) {
    $("#item_name").val(name);
    $("#item_id").val(item_id);
    if (item_id) {
        var url = "";
        var param = { item_id: item_id, name: name };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function() {
                $('#add_location').attr('disabled', true);
                $('#itemlocationlistDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function(data) {
                $('#itemlocationlistDiv').html(data);
                setTimeout(function() {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            },
            complete: function() {
                $('#add_location').attr('disabled', false);
                $('#itemlocationlistDiv').LoadingOverlay("hide");
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please select Any Table");
        $("#item_name").focus();
    }
}
function editLocation(id,location_name,location_id,reorder_level,rack,status){
    var item = $("#item_name").val();
    if(item == ''){
        toastr.warning("Please select Any Item");
    }else{
        var item_id = $("#item_id").val();
        if(item_id == ""){
            toastr.warning("Please select Valid Item");
        }else{
            $("#editlocation_modal").modal('show');
            $("#product_location_hidden_id").val(id);
            // $("#e_location_name").val(location_name);
            $("#e_location_id").val(location_id);
            // $("#e_location_id").trigger('change');
            $("#e_reorder_level").val(reorder_level);
            $("#e_rack").val(rack);
            $("#e_status").val(status);
            selectLocationRack(location_id,rack);
        }

    }
}
function selectLocationRack(location_id,rack){
    if (location_id) {
        var url = "";
        $.ajax({
                type: "GET",
                url: url,
                data: 'rackByLocationId=1&location_id=' + location_id ,
                beforeSend: function () {
                    $("#e_rack").empty();
                },
                success: function (res) {
                // console.log(res);return false;
                if (res != 0){
                    $("#e_rack").append('<option value=""> Select</option>');
                    $.each(res, function(key, value){
                        if(rack==key){
                            var selects = "selected";
                        }else{
                             var selects = "";
                        }
                        $("#e_rack").append('<option value="' + key + '" '+ selects +'>' + value + '</option>');
                    });
                } else{
                    $("#e_rack").empty();
                    $("#e_rack").append('<option value=""> Select</option>');
                }

                },
                error: function () {
                    alert('Please check your internet connection and try again');
                },
                complete: function () {
                }
        });
    } else {
    $('#e_location_id').focus();
    $("#e_rack").empty();
    }
}
function updateProductLocation(e){
    e.preventDefault(); 
        var hidden_id = $("#product_location_hidden_id").val();
        var location_name = $("#e_location_name").val();
        var location_id = $("#e_location_id").val();
        var reorder_level = $("#e_reorder_level").val();
        var rack = $("#e_rack").val();
        var status = $("#e_status").val();
        var name = $("#item_name").val();
        var item_id = $("#item_id").val();
        if(location_name == ""){
            toastr.warning("Please Select Location");return false;
        }else if(location_id == ""){
            toastr.warning("Please Select Valid Location");return false;
        }
        if(reorder_level == "" && rack == ""){
            toastr.warning("Please Fill Rack Or Re-order Level");return false;
        }
       
        if (hidden_id) {
        var url = "";
        var param = { update_data: '1', hidden_id: hidden_id, location_name: location_name, location_id: location_id, reorder_level: reorder_level, rack: rack, status: status };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function() {
            },
            success: function(res) {
                if(res == 1){
                    toastr.success("Details Updated Successfully.");
                    $("#editlocation_modal").modal('hide');
                    itemLocationTable(item_id,name);
                }else{
                    toastr.error("Details Not Updated Successfully.");
                    toastr.warning("Please Select Any Record");
                }  
            },
            complete: function() {
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Record");
    }
}
function saveProductLocation(e){
    e.preventDefault(); 
        var location_id = $("#location_id").val();
        var reorder_level = $("#reorder_level").val();
        var rack = $("#rack").val();
        var status = $("#status").val();
        var name = $("#item_name").val();
        var item_id = $("#item_id").val();
        var _token = $("#_token").val();
        if(item_id == ""){
            toastr.warning("Please Select Item");return false;
        }
        if(location_id == ""){
            toastr.warning("Please Select Valid Location");return false;
        }
       if(reorder_level == "" && rack == ""){
            toastr.warning("Please Fill Rack Or Re-order Level");return false;
        }
        if (item_id) {
        var url = "{{route('extensionsvalley.item.saveProductLocation')}}";
        var param = { save_data: '1', _token: _token, item_id: item_id, location_id: location_id, reorder_level: reorder_level, rack: rack, status: status };
        $.ajax({
            type: "POST",
            url: url,
            data: param,
            beforeSend: function() {
            },
            success: function(res) { 
                if(res == 1){
                    toastr.success("Details Saved Successfully.");
                    $("#addlocation_modal").modal('hide');
                    $("#add_item_location_form")[0].reset();
                    itemLocationTable(item_id,name);
                }else{
                    toastr.error("Details Not Saved Successfully.");
                    toastr.warning("Location Already Exist");
                }  
            },
            complete: function() {
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please Select Any Item");
    }
}
//----------------------------------------------------------------------------------------------------------------------------------------------location search--------------------------------
$('#location_search').on('change',function(){
    $('#itemlocationlistDiv').css('display','none');
    $('#locationlistDiv').css('display','block');
    $('#itemnamesearch_box').css('display','none');
    $('#addlocation_button').css('display','none');
    var location_search_id=$("#location_search").val();
    var location_search_name=$("#location_search :selected").text();
    itemwisesearchtableData(location_search_id,location_search_name);
});
   
$('#item_name').on('change',function(){
    $('#locationnamesearch_box').css('display','none');
});
function itemwisesearchtableData(location_search_id,location_search_name){
    if (location_search_id) {
        var url = "{{route('extensionsvalley.item.location_wise_product_list')}}"
        var param = { location_search_id: location_search_id,location_search_name: location_search_name};
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function() {
                $('#locationlistDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function(data) {
                $('#locationlistDiv').html(data);
                setTimeout(function() {
                    $('.theadfix_wrapper').floatThead({
                        position: 'absolute',
                        scrollContainer: true
                    });
                }, 400);

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });
            },
            complete: function() {
                $('#locationlistDiv').LoadingOverlay("hide");
            },
            error: function() {
                toastr.error("Error Please Check Your Internet Connection");
            }
        });
    } else {
        toastr.warning("Please select Any Table");
        $("#location_search").focus();
    }


}

@include('Purchase::messagetemplate')
</script>
@endsection