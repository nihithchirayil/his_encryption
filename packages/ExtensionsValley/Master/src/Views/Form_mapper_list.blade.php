{{-- @extends('Dashboard::dashboard.dashboard') --}}
@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset(" packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset(" packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">

<link href="{{asset(" packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset(" packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
    #main {
        min-height: 700px;
        /* background-color: aliceblue; */
        margin: 3px 0px 0px -2px;
        box-shadow: 0px 1px 3px;
        border-radius: 2px;
        padding: 0px !important;

    }

    #heading {
        background-color: white;
        height: 33px;
        color: #555;
        ;


    }

    #fill_details {
        height: 141px;
    }

    .th {
        text-align: center;
        border: 1px solid whitesmoke;
        color: white;
    }

    table {
        width: 100%;
        background-color: #f1f7f7 !im;
    }

    .btn {
        font-size: 11px;
        padding: 1px 6px;
        box-shadow: none;
        background-color: #2e4863;
    }

    #table {
        box-shadow: 0px 0px 1px;
        padding: 2px 0px 0px 0px;
        margin: -57px 1px 2px 3px;
        min-height: 526px;
        background-color: aliceblue;
        width: 99.5%;
    }

    .td {
        border: 1px solid white;
        padding-left: 2px;
        font-size: 12px;
    }

    .foo tr:nth-of-type(even) {
        background-color: #f3f3f3;
    }

    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #FFF;
    }

    .nav>li>a:hover,
    .nav>li>a:active,
    .nav>li>a:focus {
        color: #444;
        background: #f7f7f7;
    }

    /* width */
    .dropdown-menu::-webkit-scrollbar {
        width: 10px;
        border-radius: 0px 0px 0px 0px !important;
    }

    /* Track */
    .dropdown-menu::-webkit-scrollbar-track {
        background: white;
    }

    /* Handle */
    .dropdown-menu::-webkit-scrollbar-thumb {
        background: #888;
    }

    /* Handle on hover */
    .dropdown-menu::-webkit-scrollbar-thumb:hover {
        background: #555;
    }

    .theadscroll::-webkit-scrollbar {
        width: 10px;
        border-radius: 0px 0px 0px 0px !important;
    }

    /* Track */
    .theadscroll::-webkit-scrollbar-track {
        background: white;
        "cursor: pointer

    }

    /* Handle */
    .theadscroll::-webkit-scrollbar-thumb {
        background: #888;
    }

    /* Handle on hover */
    .theadscroll::-webkit-scrollbar-thumb:hover {
        background: #555;
    }

    .dropdown-menu li:nth-of-type(even) {
        background-color: #99FFFF;
    }

    .floatThead-container {
        width: 1402px !important;
    }
</style>


@endsection
@section('content-area')
<!-- page content -->
<div class="right_col" role="main">
    <div class="row codfox_container">
        <div id="main" class="col-md-12 padding_sm">
            <h2 style="padding: 10px;"> Form Mapper</h2>
            @if (strlen($total_records)>0)
            <div id="total_record" class="pull-right" style="margin-top:2px;margin-right: 30px;font-size: 14px;">Total
                Record:<b class="total">{{$total_records}}</b></div>
            @else
            <div id="total_record" class="pull-right" style="margin-top:2px;margin-right: 30px;font-size: 14px;"></div>
            @endif
            <div class="pull-right" style="margin-top:-25px; padding-right:39px;">
                <a><button id="add" class="btn btn-primary btn-sm form-action-btn " onclick="add();">Add</button></a>


            </div>

            <ul class="nav nav-tabs" id="myTab">
                <li><a data-toggle="tab" href="#usrMapperList">User List</a></li>
                <li><a data-toggle="tab" href="#rolMapperList">Role List</a></li>
            </ul>

            <div class="tab-content">
                <!--user list Start -->
                <div id="usrMapperList" class="col-md-12 tab-pane fade in active" style="padding-top: 11px;">
                    <div class="row" style="margin-top: 10px;">
                        <form action="{{route('extensionsvalley.master.Form_mapper_list')}}" id="usrrequestSearchForm"
                            method="POST">
                            <input type="hidden" name="user_tab" value="1">

                            <div class="col-md-12">
                                <div class="col-md-5">

                                    <label class="filter_label">User Name</label>
                                    <input class="form-control hidden_search"
                                        value=" @if(old('user_name')){{old('user_name')}}@else{{$searchFields['user_name']}}@endif "
                                        autocomplete="off" type="text" id="user_name" name="user_name"
                                        placeholder="User Name" />
                                    <input type="hidden" id="user_id" name="user_id" value="" />
                                    <div id="user_list" class="dropit" style="position: relative"></div>
                                </div>

                                <div class="col-md-2 text-right" style="padding-top: 22px;">
                                    <a class="btn btn-primary btn-sm form-action-btn" onclick="search_user();"
                                        name="search_results" id="search_results_user">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                        Search
                                    </a>
                                </div>
                            </div>

                            {!! Form::token() !!}
                        </form>
                    </div>
                    <br>
                    <br />
                    <div class="theadscroll always-visible" style="height: 462px; position: relative;padding:7px;">
                        <table class="table table-striped theadfix_wrapper table-bordered "
                            style="cursor: pointer;width:79%;">
                            <thead>

                                <th style="width: 25%;background-color:antiquewhite">Form</th>
                                <th style="width: 35%;background-color:antiquewhite">Department</th>
                                <th style="width: 25%;background-color:antiquewhite">User</th>
                                <th style="width: 15%;;background-color:antiquewhite">&nbsp</th>

                            </thead>
                            <tbody class="foo">

                                <tr>
                                    @if(isset($res) && sizeof($res) > 0)
                                    @foreach ($res as $item)
                                    <td class="td">{{$item->form_name}}</td>
                                    <td class="td">{{$item->department_name}}</td>
                                    <td class="td">{{$item->user_name}}</td>
                                    <td class="text-center" width="5%">
                                        <a class="btn btn-default form-action-btn"
                                            onclick="deleteData(this,{{$item->id}});" title="Delete"
                                            style="background-color: aliceblue"><i class="fa fa-remove"></i></a>
                                    </td>

                                </tr>

                                @endforeach


                                @else
                                <tr>
                                    <td colspan="4" style="box-shadow: 0px 0px 1px!important;">No Data Found</td>
                                </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- role List Start -->
                <div id="rolMapperList" class="tab-pane fade" style="padding-top: 11px;">

                    <div class="row" style="margin-top: 10px;">
                        <form action="{{route('extensionsvalley.master.Form_mapper_list')}}" id="rolerequestSearchForm"
                            method="POST">
                            <input type="hidden" name="role_tab" value="2">

                            <div class="col-md-12">
                                <div class="col-md-5">
                                    @php
                                    $sql="select name,id from groups order by name";
                                    $ward = DB::select($sql);
                                    @endphp

                                    Select Role
                                    <select class="form-control" title="Select Group" id="role" autofocus="true"
                                        style="color:#555555; padding:4px 12px;" name="role">
                                        <option value="">Select Role</option> @foreach($ward as $items_each)<option
                                            value="{{$items_each->id}}" {{
                                            old('role',$searchFields['role'])==$items_each->
                                            id ? 'selected' : '' }}>{{ $items_each->name}}</option>@endforeach
                                    </select>
                                </div>
                                <div class="col-md-2 text-right" style="padding-top: 22px;">

                                    <a class="btn btn-primary btn-sm form-action-btn" onclick="search_role();"
                                        name="search_results_role" id="search_results_role">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                        Search
                                    </a>
                                </div>

                            </div>

                            {!! Form::token() !!}
                        </form>

                        <br>
                        <div class="col-xs-12">
                            <br />
                            <div class="theadscroll always-visible"
                                style="max-height: 462px; position: relative;overflow-y: auto;padding:7px;width: 1413px !important">
                                <table class="table table-striped theadfix_wrapper table-bordered "
                                    style="cursor: pointer">
                                    <thead style="">

                                        <th style="width: 45%;background-color:antiquewhite">Form</th>
                                        <th style="width: 45%;background-color:antiquewhite">Role</th>
                                        <th style="width:10%;;background-color:antiquewhite">&nbsp</th>

                                    </thead>
                                    <tbody class="foo">


                                        <tr>
                                            @if(isset($res) && sizeof($res) > 0)
                                            @foreach ($res as $item)
                                            <td class="td">{{$item->form_name}}</td>

                                            <td class="td">{{$item->role_name}}</td>
                                            <td class="text-center" width="5%">
                                                <a class="btn btn-default form-action-btn"
                                                    onclick="deleteData(this,{{$item->id}});" title="Delete"
                                                    style="background-color: aliceblue"><i class="fa fa-remove"></i></a>
                                            </td>

                                        </tr>

                                        @endforeach




                                        @else
                                        <tr>
                                            <td colspan="3" style="box-shadow: 0px 0px 1px!important;">No Data Found
                                            </td>
                                        </tr>
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- role List End -->
    </div>
</div>
<!-------popup modal----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 900px;height: 273px;">
            <div class="modal-header" style="border: none;background-color:aliceblue">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: black;">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" style="color: rgb(71, 8, 71); text-align:center;padding-top:10px">Add Data</h4>

            </div>

            </ul>
            <div class="tab-content" style="padding-top:48px;">
                <!--user list Start -->
                <div id="usrMapperListpopup" class="tab-pane fade in active"
                    style="padding-top: 11px;padding-left:60px;">

                    <div class="row" style="margin-top: -34px;">
                        <form action="{{route('extensionsvalley.master.Form_mapper_list')}}"
                            id="usrrequestSearchFormpopup" method="POST">


                            <div class="col-md-12">
                                <div class="col-md-5">
                                    @php
                                    $sql="select distinct name,id from forms_list where deleted_at is null order by
                                    name";
                                    $ward = DB::select($sql);
                                    @endphp

                                    {{-- {{$items_each->id}}"{{ old('ward',$searchFields['ward']) == $items_each->id ?
                                    'selected' : '' }} --}}
                                    Select Form
                                    <select class="form-control" title="Select Form" id="form_pop" autofocus="true"
                                        style="color:#555555; padding:4px 12px;" name="form_pop">
                                        <option value="">Select Form</option> @foreach($ward as $items_each)<option
                                            value="{{$items_each->id}}">{{ $items_each->name}}</option>@endforeach
                                    </select>
                                    <div id="form_error" style="display: none; color:rgb(138, 21, 21)"> Required Field.
                                    </div>
                                </div>

                                <div class="col-md-5" style="padding-top: 2px;">
                                    @php
                                    $sql="select name,id from speciality where status=1 order by name";
                                    $ward = DB::select($sql);
                                    @endphp

                                    {{-- {{$items_each->id}}"{{ old('ward',$searchFields['ward']) == $items_each->id ?
                                    'selected' : '' }} --}}
                                    Select Department
                                    <select class="form-control" title="Select depatment" id="department_pop"
                                        autofocus="true" style="color:#555555; padding:4px 12px;" name="department_pop">
                                        <option value="">Select Department</option> @foreach($ward as $items_each)
                                        <option value="{{$items_each->id}}">{{ $items_each->name}}</option>
                                        @endforeach
                                    </select>

                                </div>


                            </div>
                            <div class="col-md-12">
                                <div class="col-md-5" style="padding-top:10px;">

                                    <label class="filter_label">User Name</label>
                                    <input class="form-control hidden_search" value="" autocomplete="off" type="text"
                                        id="user_name_pop" name="user_name_pop" placeholder="User Name" />
                                    <input type="hidden" id="user_id_pop" name="user_id_pop" value="" />
                                    <div id="user_list_pop" class="dropit_pop" style="position: relative"></div>
                                    <div id="user_error" style="display: none; color:rgb(138, 12, 12)"> Required Field.
                                    </div>
                                </div>
                                <div class="col-md-5" style="padding-top: 15px;">
                                    @php
                                    $sql="select name,id from groups order by name";
                                    $ward = DB::select($sql);
                                    @endphp

                                    Select Group
                                    <select class="form-control" title="Select role" id="role_pop" autofocus="true"
                                        style="color:#555555; padding:4px 12px;" name="role_pop">
                                        <option value="">Select Role</option> @foreach($ward as $items_each)<option
                                            value="{{$items_each->id}}">{{ $items_each->name}}</option>@endforeach
                                    </select>
                                    <div id="role_error" style="display: none; color:rgb(138, 12, 12)"> Required Field.
                                    </div>
                                </div>



                            </div>
                            <div class="col-md-3" style="margin-top:50px;float: right; padding-left:86px;">
                                <a class="btn btn-primary btn-sm form-action-btn" onclick="save_as_user();"
                                    name="search_results" id="as_user"><i class="fa fa-save" id="add_spin"></i>
                                    save
                                </a>
                            </div>

                            {!! Form::token() !!}
                        </form>

                    </div>

                </div>


            </div>



        </div>
    </div>
</div>
@stop
@section('javascript_extra')

<script type="text/javascript">
    $(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
        $('#myTab a[href="' + activeTab + '"]').tab('show');
    }

    $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');

});
//----------------------------------search userlist------------------------------------------------------------------------------------------------------------------------------
function search_user(){

$('#main').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
$('#usrrequestSearchForm').submit();
}
function search_role(){

$('#main').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
$('#rolerequestSearchForm').submit();
}
function add() {
    $('#print_config_modal').modal('toggle');
}


//-------------------------------------------------------------------------------------------------AJAX AUTOCOMPLETE SEARCH----------------------------------------------------------------------------------------------------------------------------------------------
$(document).ready(function(){

$('#user_name').keyup(function(){
       var query = $(this).val();
       if(query == ''){
           query= '@';
       }
        var url = "{{route('extensionsvalley.master.fetchit')}}";
        $.ajax({

         type:"GET",
         url:url,
         data:{'query':query},
         beforeSend: function () {
                    $('#user_list').fadeIn();
                    $('#user_list').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                    },
         success:function(data){

                   $('#user_list').html(data);
         },
         complete: function () {
                        $('#user_list').LoadingOverlay("hide");
                    },
        });

   });

   $(document).on('click', '.dropit li', function(){
       $('#user_name').val($(this).text());
       $('#user_id').val($(this).val());
       $('#user_list').fadeOut();
   });


});
// $(document).click(function(){
//   $("#user_list").hide();
// });

//-------------------------------------------------------------------------------------------------AJAX AUTOCOMPLETE SEARCH FOR POPUP----------------------------------------------------------------------------------------------------------------------------------------------
$(document).ready(function(){

$('#user_name_pop').keyup(function(){
       var query = $(this).val();
       if(query == ''){
           query= '@';
       }
        var url = "{{route('extensionsvalley.master.fetchit')}}";
        $.ajax({

         type:"GET",
         url:url,
         data:{'query':query},
         beforeSend: function () {
                    $('#user_list_pop').fadeIn();
                    $('#user_list_pop').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                    },
         success:function(data){

                   $('#user_list_pop').html(data);
         },
         complete: function () {
                        $('#user_list_pop').LoadingOverlay("hide");
                    },
        });

   });

   $(document).on('click', '.dropit_pop li', function(){
       $('#user_name_pop').val($(this).text());
       $('#user_id_pop').val($(this).val());
       $('#user_list_pop').fadeOut();
   });


});
// $(document).click(function(){
//   $("#user_list_pop").hide();
// });

//---------------------------------------------------------------------------------------SAVE FUNCTION-----------------------------------------------------------------------------------------------

function save_as_user(){

    var dept_id= $('#department_pop').val();
    var form_id= $('#form_pop').val();
    var role_id= $('#role_pop').val();

    var user_id= $('#user_id_pop').val();
  if((form_id=='')&&(user_id=='')&&(role_id=='')){
    $("#form_error").css("display","block");
    $("#user_error").css("display","block");
    $("#role_error").css("display","block");
    return;
  }
  if(form_id==''){
    $("#form_error").css("display","block");
    return;
  }else{
    $("#form_error").css("display","none");
  }
  if((user_id=='')&&(role_id=='')){
    $("#user_error").css("display","block");
    $("#role_error").css("display","block");
    return;
  }
  $('#as_user').attr('disabled', true);
  var url="{!! route('extensionsvalley.master.insert') !!}";
  $.ajax({
                type: "POST",
                url: url,
                data: {dept_id:dept_id,form_id:form_id,user_id:user_id,role_id:role_id,_token:" {{ csrf_token() }}",},
                dataType: "JSON",
                beforeSend: function () {

                                $("#add_spin").removeClass('fa fa-save');
                                $("#add_spin").addClass('fa fa-spinner fa-spin');
                            },
                success: function(data) {
                    $("#add_spin").removeClass('fa fa-spinner fa-spin');
                    $("#add_spin").addClass('fa fa-save');

                if(data == 1){
                   $('#print_config_modal').modal('toggle');

                     location.reload();
                     setTimeout(
                     function () {
                        $('#main').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                     }, 5500);


                 }else{
                    $('#role').val('');
                    $('#user_name').val('');
                    location.reload();

                }
                }
    });



}
//----------------------------------------------to make header sticky ---------------------------------------------------------------------------------------------------
var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                 scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                 }
            });
//--------------------------------------------------delete function userlist------------------------------------------------------------------------------------------

function deleteData(obj,id){
    var total=$("b.total").text();
    var chg=total-1;

    if(id != ""){
         bootbox.confirm({
            message: "Do you want to delete it?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },


            callback: function (result) {
                var url="{!! route('extensionsvalley.master.deleteit') !!}";
                if(result == true){
                    var url = url;
                   // alert(id);
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: {id:id},
                        beforeSend: function () {
                           // $('#main').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                        },
                        success: function (data) {
                            if(data == 1){
                                $(obj).parent().parent().remove();
                                $("b.total").text(chg);



                            }else{
                            }
                        },
                        error: function () {
                        },
                        complete: function () {
                        }
                    });
                }
            }
        });

    }else{
    }
 }
//----------------------------------------------------onchange function for required fields-------------------------------------------------------------
$('#form_pop').on('change', function() {
   $("#form_error").css("display","none");
  });
$('#user_name_pop').on('change', function() {
   $("#user_error").css("display","none");
   $("#role_error").css("display","none");

  });
$('#role_pop').on('change', function() {
   $("#role_error").css("display","none");
   $("#user_error").css("display","none");
 });


</script>
@endsection
