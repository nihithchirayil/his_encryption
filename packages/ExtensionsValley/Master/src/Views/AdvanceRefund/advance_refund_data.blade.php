@if($offset == 0)
<div class="row">
    <div class="col-md-12 theadscroll" id="load_data" style="position: relative; height: 350px;">
        <table class="table table_round_border theadfix_wrapper styled-table" id="receipt_table1">
            <thead>
                <tr class="table_header_bg">
                    <th style="text-align:center;width:15%">UHID</th>
                    <th style="text-align:center">Patient name</th>
                    <th style="text-align:center;width:12%">Insurance Adv. Amount</th>
                    <th style="text-align:center;width:12%">Patient Adv. Amount</th>
                    <th style="text-align:center;width:15%">Refund Amnt</th>
                    <th style="text-align:center;width:12%">Remarks</th>
                    <th style="text-align:center;width:15%">Refund</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($receivedData))
                @php $pat_adv = $inv_adv =0; @endphp
                @foreach($receivedData as $rec)
                @php 
                $inv_adv = !empty($rec->insurance_advance) ? $rec->insurance_advance:0;
                $pat_adv = !empty($rec->patient_advance	) ? $rec->patient_advance:0;
                $total = $inv_adv + $pat_adv;
                @endphp
                <tr>
                    <td class="common_td_rules">{{ !empty($rec->uhid) ? $rec->uhid :'' }}</td>
                    <td class="common_td_rules">{{ !empty($rec->patient_name) ?$rec->patient_name :'' }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($inv_adv, 2, '.', ',')}}</td>
                    <td class="td_common_numeric_rules">{{ number_format($pat_adv, 2, '.', ',')}}</td>
                    <td class="td_common_numeric_rules">
                        <input type="text" name="refund_amnt[]" id="refund_amnt_id_{{$rec->id}}" onkeyup="number_validation(this)" class="form-control">
                        <input type="hidden" name="refund_ins_amnt[]" id="refund_ins_id_{{$rec->id}}"  value="{{$inv_adv}}" class="form-control">
                        <input type="hidden" name="refund_pat_amnt[]" id="refund_pat_id_{{$rec->id}}" value="{{$pat_adv}}" class="form-control">
                    </td>
                      <td class="common_td_rules">
                        <input type="text" name="refund_remarks[]" id="refund_remarks_id_{{$rec->id}}" class="form-control">
                      </td>
                    <td class=" common_td_rules text-center">
                        <button class='btn btn-success rfndbtn' type="button" onclick="refundAdvAmount('{{$rec->id}}', 'pat')">Pat. Adv <i class="fa fa-reply" id="refund_{{$rec->id}}"></i></button>
                        <button class='btn btn-info rfndbtn' style="background_color:orange" type="button" onclick="refundAdvAmount('{{$rec->id}}', 'ins')">Ins Adv<i class="fa fa-reply" id="refund_{{$rec->id}}"></i></button>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">No Records Found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>

<script>

    $('#load_data').on('scroll', function() {
    var scrollHeight = $('#load_data').height();
    var scrollPosition = $('#load_data').scrollTop() + $('#load_data').innerHeight();
    if (scrollPosition + 3 >= $('#load_data')[0].scrollHeight){
    offset = offset + limit;
    console.log(limit + '##' + offset + '##' + total_rec);
    if (offset < total_rec){
    setTimeout(function(){
    searchList(limit, offset);
    }, 500);
    }
    }
    })
</script>

@else
@if(isset($receivedData))
@php $pat_adv = $inv_adv =0; @endphp
@foreach($receivedData as $rec)
@php 
$inv_adv = !empty($rec->insurance_advance) ? $rec->insurance_advance:0;
$pat_adv = !empty($rec->patient_advance	) ? $rec->patient_advance:0;
$total = $inv_adv + $pat_adv;
@endphp
<tr>
    <td class="common_td_rules">{{ !empty($rec->uhid) ? $rec->uhid :'' }}</td>
    <td class="common_td_rules">{{ !empty($rec->patient_name) ?$rec->patient_name :'' }}</td>
    <td class="td_common_numeric_rules">{{ number_format($inv_adv, 2, '.', ',')}}</td>
    <td class="td_common_numeric_rules">{{ number_format($pat_adv, 2, '.', ',')}}</td>
    <td class="td_common_numeric_rules">
        <input type="text" name="refund_amnt[]" id="refund_amnt_id_{{$rec->id}}" onkeyup="number_validation(this)" class="form-control">
        <input type="hidden" name="refund_ins_amnt[]" id="refund_ins_id_{{$rec->id}}"  value="{{$inv_adv}}" class="form-control">
        <input type="hidden" name="refund_pat_amnt[]" id="refund_pat_id_{{$rec->id}}" value="{{$pat_adv}}" class="form-control">
    </td>
    <td class="common_td_rules">
        <input type="text" name="refund_remarks[]" id="refund_remarks_id_{{$rec->id}}" class="form-control">
    </td>
    <td class=" common_td_rulestext-center">
        <button class='btn btn-success rfndbtn' type="button" onclick="refundAdvAmount('{{$rec->id}}', 'pat')">Pat. Adv <i class="fa fa-reply" id="refund_{{$rec->id}}"></i></button>
        <button class='btn btn-info rfndbtn' style="background_color:orange" type="button" onclick="refundAdvAmount('{{$rec->id}}', 'ins')">Ins Adv<i class="fa fa-reply" id="refund_{{$rec->id}}"></i></button>
    </td>
</tr>
@endforeach
@else
<tr>
    <td colspan="5">No Records Found</td>
</tr>
@endif
@endif
