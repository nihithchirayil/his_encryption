@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<div class="right_col" >
    <div class="row" style="text-align: right;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                            {!! Form::token() !!}
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Patient Name/UHID</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control hidden_search" name="patient_name" id="patient_name" value="" autocomplete="false">
                                    <input type="hidden" class="form-control" name="patient_name_hidden" id="patient_name_hidden" value="">
                                    <div id="AjaxDiv" class="ajaxSearchBox" style="margin: 14px 0px 0px 0px !important;z-index: 9999999;max-height: 250px !important"></div>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg" onclick="searchList(20,0)"><i id="searchdataspin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                            </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" id="common_list_div">

            </div>
        </div>

    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script type="text/javascript">
    var limit = 20;
    var offset = 0;
    var total_rec = 0;
    $(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function ($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
    position: 'absolute',
            scrollContainer: true
    });
    searchList(15,0);
    });


    function searchList(limit = 15,offset =0){

        var url='<?=route('extensionsvalley.master.advanceRefundData')?>';
        var patient_id = $("#patient_name_hidden").val();
        var param1 = {patient_id:patient_id,limit:limit,offset:offset};
        $.ajax({
        type: "GET",
            url: url,
            data:param1,
            beforeSend: function () {
                $('#searchdatabtn').attr('disabled',true);
                $('#searchdataspin').removeClass('fa fa-search');
                $('#searchdataspin').addClass('fa fa-spinner');
                $('#common_list_div').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (msg) {
                var obj = JSON.parse(msg);
                 msg = obj.viewData;
                total_rec = obj.total_rec;
                if(offset == 0){
                    $('#common_list_div').html(msg);
                }else{
                    $('#receipt_table1').append(msg);

                }
            },
            complete: function () {
                $('#searchdatabtn').attr('disabled',false);
                $('#searchdataspin').removeClass('fa fa-spinner');
                $('#searchdataspin').addClass('fa fa-search');
                $('#common_list_div').LoadingOverlay("hide");


                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');


            },error: function(){
                toastr.error("Please Check Internet Connection");
            }
        });
   }

   $('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = 1; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');

    if (keycheck || event.keyCode == '8') {
        if ($('#patient_name_hidden').val() != "") {
            $('#patient_name_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var datastring = '';
        if (search_key == "") {
            $("#AjaxDiv").html("");
        } else {
            var url='<?=route('extensionsvalley.master.advanceRefund')?>';
            $.ajax({
                type: "GET",
                url: url,
                data: 'op_no_search=' + search_key,
                beforeSend: function () {

                    $("#AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#AjaxDiv").html("No results found!").show();

                        $("#AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#AjaxDiv").html(html).show();
                        $("#AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down('AjaxDiv', event);
    }
});

function fillSearchDetials(id,uhid,name,input_id) {
    $('#patient_name_hidden').val(id);
    $('#patient_name').val(name+'['+ uhid+']');
     $("#AjaxDiv").hide();
}

function refundAdvAmount(id,type){
     var url='<?=route('extensionsvalley.master.refundAdvAmount')?>';
     var refund_amount = $("#refund_amnt_id_"+id).val();
     var refund_remarks = $("#refund_remarks_id_"+id).val();
     if(refund_amount == '' || refund_amount ==0){ 
               toastr.error("Please enter valid amount");
               return;
           }
        var param1 = {patient_id:id,refund_amount:refund_amount,type:type,refund_remarks:refund_remarks};
        var refund_ins_id = $("#refund_ins_id_"+id).val();
        var refund_pat_id =$("#refund_pat_id_"+id).val();
        if(type == 'ins'){
            if(parseFloat(refund_ins_id) < parseFloat(refund_amount)){
               toastr.error("Given amount greater than the advance amount");
               return;
            }
        }else{ console.log(refund_pat_id+'==');console.log(refund_amount+'==');
            if(parseFloat(refund_pat_id) < parseFloat(refund_amount)){
               toastr.error("Given amount greater than the advance amount");
               return;
            }
        }
        $.ajax({
        type: "GET",
            url: url,
            data:param1,
            beforeSend: function () {
                $('.rfndbtn').attr('disabled',true);
                $('#common_list_div').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (msg) {
                if(msg == 3){
                    toastr.error("Given amount greater than the advance amount");
                }
                if(msg == 1){
                    toastr.success("Amount returned successfully");
                    window.location.reload();
                }
            },
            complete: function () {
                $('.rfndbtn').attr('disabled',false);
                $('#common_list_div').LoadingOverlay("hide");
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });

                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');


            },error: function(){
                toastr.error("Please Check Internet Connection");
            }
        });
    }  
    function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}
</script>

@endsection
