

<div id="template-content-load-div" class="col-md-12 no-padding">
    {!!$TemplateviewData!!}
</div>

{{-- <div class="col-md-2" style="height:488px!important;">
    <div class="list-group">
        <a href="#ac1" class="list-group-item list-group-item-action nav_class">Present Complaints</a>
        <a href="#ac2" class="list-group-item list-group-item-action nav_class">Past history</a>
        <a href="#ac3" class="list-group-item list-group-item-action nav_class">Pupils</a>
        <a href="#ac4" class="list-group-item list-group-item-action nav_class">CNS</a>
        <a href="#ac5" class="list-group-item list-group-item-action nav_class">RS</a>
        <a href="#ac6" class="list-group-item list-group-item-action nav_class">CVS</a>
        <a href="#ac7" class="list-group-item list-group-item-action nav_class">Abdomen</a>
        <a href="#ac8" class="list-group-item list-group-item-action nav_class">Injuries if any</a>
        <a href="#ac9" class="list-group-item list-group-item-action nav_class">Others</a>
        <a href="#ac10" class="list-group-item list-group-item-action nav_class">Provisional diagnosis</a>
        <a href="#ac11" class="list-group-item list-group-item-action nav_class">Others</a>
      </div>
</div>



<div id="template-content-load-div" class="col-md-7 box-body theadscroll" style="position:relative;height:488px;background-image: url({{ asset('packages/extensionsvalley/default/img/medical_bg_new1.jpg') }}) !important; background-repeat: repeat !important;padding:15px;">
    <div class="col-md-12 no-border" id="ac1">
        <label class="text-blue"><h6 style="margin-left:10px;"><b>Present Complaints</b></h6></label>
        <div class="" style="padding:10px !important;margin-top:-15px;">
            <textarea style="height:90px;border:none; !important;padding-left:10px !important;" class=" form-control" id="present_complaints"></textarea>

                <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom" title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'1009')">
                    <i class="fa fa-star-o"></i>
                </a>

                <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom" title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('pupils11','112','1260796')">
                    <i class="fa fa-history"></i>
                </a>
        </div>
    </div>

    <div class="col-md-12 no-border" id="ac2">
        <label class="text-blue"><h6 style="margin-left:10px;margin-bottom:-5px;"><b>Past History</b></h6></label>
        <ul class="select_button no-padding" style="margin-left:10px;">
            <li data-value="DM" class="">
                <i class="fa fa-check-circle"></i>DM
                <input type="hidden" name="PASTHISTORY_gbox_DM" id="gcheckbox-0" value="" disabled="disabled">
            </li>

            <li data-value="Hyper tension">
                <i class="fa fa-check-circle"></i>Hyper tension
                <input type="hidden" name="PASTHISTORY_gbox_Hyper tension" id="gcheckbox-1" disabled="disabled" value="">
            </li>

            <li data-value="CAD">
                <i class="fa fa-check-circle"></i>CAD
                <input type="hidden" name="PASTHISTORY_gbox_CAD" id="gcheckbox-2" disabled="disabled" value="">
            </li>

            <li data-value="TB">
                <i class="fa fa-check-circle"></i>TB
                <input type="hidden" name="PASTHISTORY_gbox_TB" id="gcheckbox-3" disabled="disabled" value="">
            </li>

            <li data-value="ASTMA">
                <i class="fa fa-check-circle"></i>ASTMA
                <input type="hidden" name="PASTHISTORY_gbox_ASTMA" id="gcheckbox-4" disabled="disabled" value="ASTMA">
            </li>

            <li data-value="Seizures">
                <i class="fa fa-check-circle"></i>Seizures
                <input type="hidden" name="PASTHISTORY_gbox_Seizures" id="gcheckbox-5" disabled="disabled" value="Seizures">
            </li>

            <li data-value="Bleeding disorders">
                <i class="fa fa-check-circle"></i>Bleeding disorders
                <input type="hidden" name="PASTHISTORY_gbox_Bleeding disorders" id="gcheckbox-6"  disabled="disabled" value="Bleeding disorders">
            </li>

            <li data-value="Dyslipidemia">
                <i class="fa fa-check-circle"></i>Dyslipidemia
                <input type="hidden" name="PASTHISTORY_gbox_Dyslipidemia" id="gcheckbox-7" disabled="disabled" value="Dyslipidemia">
            </li>

            <li data-value="Others" data-toggle="collapse" href="#pastHistoryOthers">
                <i class="fa fa-check-circle"></i>Others
                <input type="hidden" name="PASTHISTORY_gbox_Others" id="gcheckbox-8" disabled="disabled" value="">
            </li>
        </ul>
        <div class="col-md-12 collapse" id="pastHistoryOthers" style="margin-top:8px;">
            <textarea style="height:90px;border:none; !important;padding-left:10px !important;" class=" form-control" id="past_history_others_text"></textarea>
        </div>
    </div>
    <div class="col-md-12 no-border">
        <label class="text-blue col-md-12"><h5 style="margin-left:10px;text-align:center;"><b>SYSTEMIC EXAMINATION</b></h5></label>

        <div class="col-md-12"  id="ac3">
            <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Pupils</b></h6></label>
            <textarea style="height:90px;border:none; !important;padding-left:10px !important;" class=" form-control" id="Pupils"></textarea>

            <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom" title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'1009')">
                <i class="fa fa-star-o"></i>
            </a>

            <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom" title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('pupils11','112','1260796')">
                <i class="fa fa-history"></i>
            </a>
        </div>
        <div class="col-md-12" id="ac4">
            <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>CNS</b></h6></label>
            <textarea style="height:90px;border:none; !important;padding-left:10px !important;" class=" form-control" id="CNS"></textarea>

            <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom" title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'1009')">
                <i class="fa fa-star-o"></i>
            </a>

            <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom" title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('pupils11','112','1260796')">
                <i class="fa fa-history"></i>
            </a>
        </div>
        <div class="col-md-12" id="ac5">
            <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>RS</b></h6></label>
            <textarea style="height:90px;border:none; !important;padding-left:10px !important;" class=" form-control" id="rs"></textarea>

            <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom" title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'1009')">
                <i class="fa fa-star-o"></i>
            </a>

            <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom" title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('pupils11','112','1260796')">
                <i class="fa fa-history"></i>
            </a>
        </div>
        <div class="col-md-12" id="ac6">
            <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>CVS</b></h6></label>
            <textarea style="height:90px;border:none; !important;padding-left:10px !important;" class=" form-control" id="cvs"></textarea>

            <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom" title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'1009')">
                <i class="fa fa-star-o"></i>
            </a>

            <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom" title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('pupils11','112','1260796')">
                <i class="fa fa-history"></i>
            </a>

        </div>
        <div class="col-md-12" id="ac7">
            <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Abdomen</b></h6></label>
            <textarea style="height:90px;border:none; !important;padding-left:10px !important;" class=" form-control" id="abdomen"></textarea>

            <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom" title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'1009')">
                <i class="fa fa-star-o"></i>
            </a>

            <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom" title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('pupils11','112','1260796')">
                <i class="fa fa-history"></i>
            </a>

        </div>
        <div class="col-md-12" id="ac8">
            <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Injuries if any</b></h6></label>
            <textarea style="height:90px;border:none; !important;padding-left:10px !important;" class=" form-control" id="injuries_if_any"></textarea>

            <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom" title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'1009')">
                <i class="fa fa-star-o"></i>
            </a>

            <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom" title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('pupils11','112','1260796')">
                <i class="fa fa-history"></i>
            </a>

        </div>
        <div class="col-md-12" id="ac9">
            <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>others</b></h6></label>
            <textarea style="height:90px;border:none; !important;padding-left:10px !important;" class=" form-control" id="systemic_examination_others"></textarea>
            <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom" title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'1009')">
                <i class="fa fa-star-o"></i>
            </a>

            <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom" title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('pupils11','112','1260796')">
                <i class="fa fa-history"></i>
            </a>

        </div>
        <div class="col-md-12" id="ac10">
            <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Provisional diagnosis</b></h6></label>
            <textarea style="height:90px;border:none; !important;padding-left:10px !important;" class=" form-control" id="provisional_diagnosis"></textarea>
            <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom" title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'1009')">
                <i class="fa fa-star-o"></i>
            </a>

            <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom" title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('pupils11','112','1260796')">
                <i class="fa fa-history"></i>
            </a>

        </div>
        <div class="col-md-12" id="ac11">
            <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Others</b></h6></label>
            <textarea style="height:90px;border:none; !important;padding-left:10px !important;" class=" form-control" id="emergency_assesment_others"></textarea>
            <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom" title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'1009')">
                <i class="fa fa-star-o"></i>
            </a>

            <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom" title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('pupils11','112','1260796')">
                <i class="fa fa-history"></i>
            </a>
            <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom" title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'1009')">
                <i class="fa fa-star-o"></i>
            </a>

            <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom" title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('pupils11','112','1260796')">
                <i class="fa fa-history"></i>
            </a>
        </div>
    </div>
</div> --}}
{{-- <div class="col-md-3 box-body">
    <label class="text-blue"><h6 style="margin-left:10px;"><b>Favourites</b></h6></label>
</div> --}}

