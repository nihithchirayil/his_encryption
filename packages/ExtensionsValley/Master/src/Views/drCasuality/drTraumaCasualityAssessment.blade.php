

<div class="col-md-12 no-border">
    <label class="text-blue col-md-12"><h5 style="margin-left:10px;text-align:center;"><b>MEDICO LEGAL CASE-(GENERAL ASSESSMNET)</b></h5></label>
</div>

<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Alleged History of</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>
<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>In detail(in words)</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>

<div class="col-md-6">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Brought Time</b></h6></label>
    <input type="text" class="form-control custom-text-box" id="brought_time" style="padding-left:10px !important;" class=" form-control" id="alleged_history_of"/>
</div>
<div class="col-md-6">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Place</b></h6></label>
    <input type="text" class="form-control custom-text-box" id="brought_time" style="padding-left:10px !important;" class=" form-control" id="alleged_history_of"/>
</div>
<div class="col-md-6">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Brought by</b></h6></label>
    <input type="text" class="form-control custom-text-box" id="brought_by" style="padding-left:10px !important;" class=" form-control" id="alleged_history_of"/>
</div>
<div class="col-md-6">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Date & Time</b></h6></label>
    <input type="text" class="form-control custom-text-box" id="brought_by" style="padding-left:10px !important;" class=" form-control" id="alleged_history_of"/>
</div>

<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Identification marks</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>

<div class="col-md-6">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Airway</b></h6></label>
    <input type="text" class="form-control custom-text-box" id="brought_by" style="padding-left:10px !important;" class=" form-control" id="alleged_history_of"/>
</div>
<div class="col-md-6">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Breathing</b></h6></label>
    <input type="text" class="form-control custom-text-box" id="brought_by" style="padding-left:10px !important;" class=" form-control" id="alleged_history_of"/>
</div>
<div class="col-md-6">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Circulation</b></h6></label>
    <input type="text" class="form-control custom-text-box" id="brought_by" style="padding-left:10px !important;" class=" form-control" id="alleged_history_of"/>
</div>
<div class="col-md-6">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Glassgow coma scale</b></h6></label>
    <input type="text" class="form-control custom-text-box" id="brought_by" style="padding-left:10px !important;" class=" form-control" id="alleged_history_of"/>
</div>


<div class="col-md-12 no-border">
    <label class="text-blue"><h6 style="margin-left:10px;margin-bottom:-5px;"><b>Eye Opening</b></h6></label>
    <ul class="select_button no-padding" style="margin-left:10px;">
        <li data-value="Spontaneous - 4" class="">
            <i class="fa fa-check-circle"></i>Spontaneous - 4
            <input type="hidden" name="PASTHISTORY_gbox_DM" id="gcheckbox-0" value="" disabled="disabled">
        </li>
        <li data-value="To speech - 3">
            <i class="fa fa-check-circle"></i>To speech - 3
            <input type="hidden" name="PASTHISTORY_gbox_Hyper tension" id="gcheckbox-1" disabled="disabled" value="">
        </li>
        <li data-value="CAD">
            <i class="fa fa-check-circle"></i>To pain - 2
            <input type="hidden" name="PASTHISTORY_gbox_CAD" id="gcheckbox-2" disabled="disabled" value="">
        </li>
        <li data-value="CAD">
            <i class="fa fa-check-circle"></i>To none - 1
            <input type="hidden" name="PASTHISTORY_gbox_CAD" id="gcheckbox-2" disabled="disabled" value="">
        </li>
    </ul>
</div>
<div class="col-md-12 no-border">
    <label class="text-blue"><h6 style="margin-left:10px;margin-bottom:-5px;"><b>Motor Response</b></h6></label>
    <ul class="select_button no-padding" style="margin-left:10px;">
        <li data-value="Spontaneous - 4" class="">
            <i class="fa fa-check-circle"></i>Obeys commands - 6
            <input type="hidden" name="PASTHISTORY_gbox_DM" id="gcheckbox-0" value="" disabled="disabled">
        </li>
        <li data-value="To speech - 3">
            <i class="fa fa-check-circle"></i>Localizes pain - 5
            <input type="hidden" name="PASTHISTORY_gbox_Hyper tension" id="gcheckbox-1" disabled="disabled" value="">
        </li>
        <li data-value="CAD">
            <i class="fa fa-check-circle"></i>Withdrawns from pain - 4
            <input type="hidden" name="PASTHISTORY_gbox_CAD" id="gcheckbox-2" disabled="disabled" value="">
        </li>
        <li data-value="CAD">
            <i class="fa fa-check-circle"></i>Abnormal /Flexion - 3
            <input type="hidden" name="PASTHISTORY_gbox_CAD" id="gcheckbox-2" disabled="disabled" value="">
        </li>
        <li data-value="CAD">
            <i class="fa fa-check-circle"></i>Extension - 2
            <input type="hidden" name="PASTHISTORY_gbox_CAD" id="gcheckbox-2" disabled="disabled" value="">
        </li>
        <li data-value="CAD">
            <i class="fa fa-check-circle"></i>None - 1
            <input type="hidden" name="PASTHISTORY_gbox_CAD" id="gcheckbox-2" disabled="disabled" value="">
        </li>
    </ul>
</div>

<div class="col-md-12 no-border">
    <label class="text-blue"><h6 style="margin-left:10px;margin-bottom:-5px;"><b>Verbal Response</b></h6></label>
    <ul class="select_button no-padding" style="margin-left:10px;">
        <li data-value="Spontaneous - 4" class="">
            <i class="fa fa-check-circle"></i>Oriented-5
            <input type="hidden" name="PASTHISTORY_gbox_DM" id="gcheckbox-0" value="" disabled="disabled">
        </li>
        <li data-value="To speech - 3">
            <i class="fa fa-check-circle"></i>Confused Conversation-4
            <input type="hidden" name="PASTHISTORY_gbox_Hyper tension" id="gcheckbox-1" disabled="disabled" value="">
        </li>
        <li data-value="CAD">
            <i class="fa fa-check-circle"></i>Inappropriate Words-3
            <input type="hidden" name="PASTHISTORY_gbox_CAD" id="gcheckbox-2" disabled="disabled" value="">
        </li>
        <li data-value="CAD">
            <i class="fa fa-check-circle"></i>Incomprehensive Sound-2
            <input type="hidden" name="PASTHISTORY_gbox_CAD" id="gcheckbox-2" disabled="disabled" value="">
        </li>
        <li data-value="CAD">
            <i class="fa fa-check-circle"></i>None-1
            <input type="hidden" name="PASTHISTORY_gbox_CAD" id="gcheckbox-2" disabled="disabled" value="">
        </li>
    </ul>
</div>

<div class="col-md-12 no-border">
    <label class="text-blue"><h6 style="margin-left:10px;margin-bottom:-5px;"><b>History of</b></h6></label>
    <ul class="select_button no-padding" style="margin-left:10px;">
        <li data-value="Spontaneous - 4" class="">
            <i class="fa fa-check-circle"></i>Loc +
            <input type="hidden" name="PASTHISTORY_gbox_DM" id="gcheckbox-0" value="" disabled="disabled">
        </li>
        <li data-value="To speech - 3">
            <i class="fa fa-check-circle"></i>Vomiting +
            <input type="hidden" name="PASTHISTORY_gbox_Hyper tension" id="gcheckbox-1" disabled="disabled" value="">
        </li>
        <li data-value="To speech - 3">
            <i class="fa fa-check-circle"></i>Seizures +
            <input type="hidden" name="PASTHISTORY_gbox_Hyper tension" id="gcheckbox-1" disabled="disabled" value="">
        </li>
        <li data-value="To speech - 3">
            <i class="fa fa-check-circle"></i>ENT Bleed +
            <input type="hidden" name="PASTHISTORY_gbox_Hyper tension" id="gcheckbox-1" disabled="disabled" value="">
        </li>
    </ul>
</div>

<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Pupils</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>

<div class="col-md-12 no-border">
    <label class="text-blue"><h6 style="margin-left:10px;margin-bottom:-5px;"><b>Peripheral pulse</b></h6></label>
    <ul class="select_button no-padding" style="margin-left:10px;">
        <li data-value="Spontaneous - 4" class="">
            <i class="fa fa-check-circle"></i>RUL Pulse +
            <input type="hidden" name="PASTHISTORY_gbox_DM" id="gcheckbox-0" value="" disabled="disabled">
        </li>
        <li data-value="To speech - 3">
            <i class="fa fa-check-circle"></i>LUL Pulse +
            <input type="hidden" name="PASTHISTORY_gbox_Hyper tension" id="gcheckbox-1" disabled="disabled" value="">
        </li>
        <li data-value="To speech - 3">
            <i class="fa fa-check-circle"></i>RLL Pulse +
            <input type="hidden" name="PASTHISTORY_gbox_Hyper tension" id="gcheckbox-1" disabled="disabled" value="">
        </li>
        <li data-value="To speech - 3">
            <i class="fa fa-check-circle"></i>LLL Pulse +
            <input type="hidden" name="PASTHISTORY_gbox_Hyper tension" id="gcheckbox-1" disabled="disabled" value="">
        </li>
    </ul>
</div>

<div class="col-md-12 no-border">
    <label class="text-blue col-md-12"><h5 style="margin-left:10px;text-align:center;"><b>INJURIES</b></h5></label>
</div>

<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Head,Neck & ENT</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>

<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Thorax</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>
<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Abdomen</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>
<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Spine & Back</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>
<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Perinium/Rectum/Urethra/Vagina</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>
<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Other Injuries</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>
<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Dental Injuries</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>
<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Provisional diagnosis</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>
<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Co morbidities</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>
<div class="col-md-12">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Orders</b></h6></label>
    <textarea style="height:120px !important;padding-left:10px !important;" class=" form-control" id="alleged_history_of"></textarea>
</div>

<div class="col-md-6">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Cross Consultation</b></h6></label>
    <input type="text" class="form-control custom-text-box" id="brought_by" style="padding-left:10px !important;" class=" form-control" id="alleged_history_of"/>
</div>
<div class="col-md-6">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Dispatch</b></h6></label>
    <input type="text" class="form-control custom-text-box" id="brought_by" style="padding-left:10px !important;" class=" form-control" id="alleged_history_of"/>
</div>
<div class="col-md-6">
    <label class="text-blue"><h6 style="margin-bottom:-5px;"><b>Seen By</b></h6></label>
    <input type="text" class="form-control custom-text-box" id="brought_by" style="padding-left:10px !important;" class=" form-control" id="alleged_history_of"/>
</div>
