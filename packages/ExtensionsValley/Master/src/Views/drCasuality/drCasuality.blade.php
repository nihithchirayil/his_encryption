@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/default/css/bootstrap-timepicker.min.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/default/css/casuality.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/default/css/new_emr.css")}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<style>
    .header_bg{
        background: rgb(37,184,155) !important;
        color:white !important;
    }
    .gray_headeer_bg{
        background: rgb(37,184,155) !important;
        color:white !important;
    }
    .mate-input-box label{
        top:-18px !important;
    }

    .headerbtn{
        box-shadow: 2px 3px 3px 0px #726e6e7a !important;
        border: 1px solid #d6e9d6;
        min-width: 115px;
        border-radius: 15px;
    }


    /*new assesment template design*/
    .list-group-item:focus{
        background-color:#1e90ff !important;
        color:rgb(255, 255, 255) !important;
    }

    .bg-radio_grp.active{
        background-color: orange !important;
        color: white !important;
        margin-right: 6px !important;
        box-shadow: 3px 2px 3px #55ff79;
    }
    .bg-radio_grp{
        /* width: 190px;
        height: 35px;
        padding-top: 7px; */
        border-radius: 11px;
    }


    #drCasualityAssessmentBlock textarea{
        background-attachment: local;
        background-image: linear-gradient(to right, white 8px, transparent 10px),
            linear-gradient(to left, white 7px, transparent 10px),
            repeating-linear-gradient(white, white 25px, #e2e1e1 27px, #f4cbcb 26px, white 27px);
        line-height: 27px;
        padding: 8px 10px;
        box-shadow:none !important;
        font-size: 13px !important;
    }

    .select_button>li{
        background:rgb(202 246 255) !important;
        width:225px !important;
        white-space: nowrap !important;
        text-overflow:ellipsis !important;
        overflow: hidden !important;
        font-size: 13px !important;
    }
    .select_button>li.active{
        background:#3498db !important;
        color:white !important;
    }

    .radio_container > input[type="radio"]:checked + label{
        background:#3498db !important;
        color:white !important;
    }
    .vital_body{
        background: rgb(255,255,255) !important;
        background: linear-gradient(90deg, rgba(255,255,255,1) 2%, rgba(201,246,255,1) 59%, rgba(145,237,255,1) 100%) !important;
    }
    .allergy_body{
        background-color: #ffffff !important;
        background-image: linear-gradient(90deg, #ffffff 0%, #c9f6ff 19%, #c9f6ff 39%, #c9f6ff 60%, #c9f6ff 80%, #ffffff 100%) !important;
    }

    .patient_details_head{
        background: rgb(45,192,222) !important;
        background: linear-gradient(90deg, rgba(45,192,222,1) 0%, rgba(201,246,255,1) 0%, rgba(255,255,255,1) 100%) !important;
    }

    .patient_details_head,.allergy_body,.vital_body{
        border-color: rgba(201,246,255,1) !important;
    }
    .list-group-item:hover{
        font-weight:500;
    }
    .list-group-item{
        background-color:rgb(202 246 255) !important;
    }


    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        background-color: rgb(202 246 255) !important;
        text-align: center;
        font-size: 13px !important;
    }

    /* .nav-selection .list-group-item{
        color: white !important;
        background-color: #3498db !important;

    }
    .nav-selection .list-group-item:hover{
        //color: white !important;
        //background-color: #216897 !important;

    } */

    .nav_click{
        background:rgb(238, 59, 59) !important;

    }

    .table-striped>tbody>tr:nth-of-type(odd){
        background-color:#efffff !important;
    }

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="patient_id" value="{{$patient_id}}">
<input type="hidden" id="visit_id" value="{{$visit_id}}">
<input type="hidden" id="user_id" value="{{$user_id}}">
<input type="hidden" id="encounter_id" value="{{$encounter_id}}">
<input type="hidden" id="ca_head_id" value="0">
<input type="hidden" id="doctor_id" value="{{$doctor_id}}">
<input type="hidden" id="prescription_head_id" value="0">
<input type="hidden" id="prescription_consumable_head_id" value="0">
<input type="hidden" id="form_id" value="{{ $edit_form_id = 0}}">
<input type="hidden" id="fav_form_id" value="112">
<input type="hidden" id="form_list_id" value="{{ $form_list_id =0 }}">
<input type="hidden" id="ca_edit_status" value="0">
<div class="right_col">
    {{-- <div class="col-md-12 no-padding text-center box-body header_bg">
        <h5><b>CASUALITY PATIENT RECORD</b></h5>
   </div> --}}
   <div class="col-md-12 no-padding">
       {{--  header section  --}}
       @include('Master::casuality.casuality_head')
   </div>
   <div class="col-md-12" style="margin:10px">
        <button class="btn bg-blue headerbtn " onclick="combinedView()">
            <i class="fa fa-history" aria-hidden="true"></i> History
        </button>

        <button class="btn bg-blue headerbtn " onclick="specialNotes()" data-toggle="modal" data-target="#speial_note_modal"> <i class="fa fa-book" aria-hidden="true">
            </i>Special Notes
        </button>

        <button class="btn bg-blue headerbtn " onclick="referDoctor();">
            <i class="fa fa-user-plus" aria-hidden="true"></i> Refer Patient
        </button>

        <button data-toggle="modal" onclick="LabResultsTrend();" data-target="#modal_lab_results" class="btn bg-blue headerbtn  btn-nurse-task labResultBtn blink-btn">
            <i class="fa fa-flask"></i> Lab Results
        </button>

        <button class="btn bg-blue headerbtn " id="btn_advnc_dtail" onclick="manageDocs();">
            <i class="fa fa-archive" aria-hidden="true"></i> Documents
        </button>

        <button class="btn bg-blue headerbtn " id="btn_private_notes">
            <i class="fa fa-list" aria-hidden="true"></i> Private Notes
        </button>

        <button class="btn bg-blue headerbtn showDischargeSummary" onclick="showDischargeSummary();">
            <i class="fa fa-tasks" aria-hidden="true"></i> Discharge Summary
        </button>

        <button type="button" @if(strtoupper($visit_status)=='OP')disabled="disabled"@endif onclick="editDischargeSummary('{{$visit_id}}');" class="btn bg-blue headerbtn">
            <span><i class="fa fa-plus"></i></span> Create Summary
        </button>


    </div>
    <div class="col-md-12 " style="margin:8px;">

        <input class="form-control" type="radio" id="template_type1"  checked="checked" name="template_type" style="appearance:auto;display:initial;width:18px;box-shadow: none;">
        <label style="font-size:14px !important;" for="template_type1">Emergency Assesment</label>

        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

        <input class="form-control" type="radio" id="template_type2" name="template_type" style="appearance:auto;display:initial;width:18px;box-shadow: none;">
        <label style="font-size:14px !important;" for="template_type2">Trauma Initial Assesment</label>
    </div>

    <div class="col-md-12 no-padding" id="drCasualityAssessmentBlock" style="margin-top:10px;">
        {{-- <label class="text-blue col-md-12"><h4 style="margin-left:10px;text-align:center;"><b>EMERGENCY INITIAL ASSESSMENT</b></h4></label> --}}
        @include('Master::drCasuality.drCasualityAssessment')
    </div>
    {{-- <div class="col-md-12 box-body" id="drTraumaCasualityAssessmentBlock" style="margin-top:10px;">
        <label class="text-blue col-md-12"><h4 style="margin-left:10px;text-align:center;"><b>TRAUMA INITIAL ASSESSMENT</b></h4></label>
        @include('Master::drCasuality.drTraumaCasualityAssessment')
    </div> --}}

    <div class="col-md-12 no-padding" style="margin-top:30px;">
        @include('Emr::emr.includes.patient_investigations')
    </div>

    <div class="col-md-12 box-body text-blue" style="margin-top:10px;">
        <h5><b>Prescription</b></h5>
    </div>
    <div class="col-md-12 no-padding">
            @include('Emr::emr.includes.patient_prescription')
    </div>

   <div class="col-md-12 box-body no-padding" style="margin-bottom:25px;padding:20px;">
       <button style="height:30px;width:125px;margin-top:10px;margin-bottom:10px;" type="button" class="btn bg-green pull-right" onclick="SaveCasuality(0);"><i class="fa fa-save"></i> Save</button>
       <button type="button" style="height:30px;width:125px;margin-top:10px;margin-bottom:10px;" class="btn bg-green pull-right" onclick="SaveCasuality(1);"><i class="fa fa-print"></i> Save & Print</button>
   </div>
</div>
@include('Emr::emr.includes.custom_modals')
@stop
@section('javascript_extra')

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/drcasuality.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>

<script
src="{{ asset('packages/extensionsvalley/emr/js/clinical_notes_new_template.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>


<script src="{{ asset('packages/extensionsvalley/emr/js/allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/emr/js/investigation.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">


    <script type="text/javascript">

</script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="{{ asset('packages/extensionsvalley/emr/js/vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/emr/js/manage-document.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
