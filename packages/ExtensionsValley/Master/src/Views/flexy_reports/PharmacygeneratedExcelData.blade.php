<div class="row">
    <div class="theadscroll no-padding" id="table_container_div" style="position: relative; max-height: 400px;">
        <table class='table table-condensed theadfix_wrapper table_sm table-col-bordered' style="font-size: 12px;">
            <thead>
                <tr class="headerclass"
                    style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                    <th width='6%'>Estimation Time </th>
                    @foreach($filter_key as $each)
                    <th width='8%'>{{ $each }}</th>
                    @endforeach
                    <th width='5%'>Status</th>
                    <th width='5%'><button type="button"
                            onclick="pharmacygeneratedExcelData('Pharmacysalesreport',2)"
                            class="btn btn-warning">
                            <i class="fa fa-recycle"></i>
                        </button></th>
                </tr>


            </thead>
            <tbody>
                @if (count($excel_generation) != 0)
                @foreach ($excel_generation as $data)
                @php
                $filters_data = @$data->report_filters ? json_decode($data->report_filters, true) : array();
                @endphp
                <tr>
                    <td class="common_td_rules">{{ $data->execution_time }}</td>
                    @foreach ($filter_key as $each)
                    <td class="common_td_rules">{{ @$filters_data[$each] ? $filters_data[$each] : '-' }}</td>
                    @endforeach
                    <td class="common_td_rules">{{ $data->run_status }}</td>

                    <?php
                    if ($data->generated_excel) {
                        if (file_exists(base_path() . '/public/packages/uploads/Purchase/' . $data->generated_excel)) {
                            ?>
                    <td style="text-align: center">
                        <a class="btn btn-primary"
                            href="{{ URL::to('/') }}/packages/uploads/Purchase/{{ $data->generated_excel }}"
                            download="Pharmacy Sales Report">
                            <i class="fa fa-download"></i>
                        </a>
                    </td>
                    <?php
                        }else{
                            ?>
                    <td style="text-align: center">-</td>
                    <?php
                        }
                    }else{
                            ?>
                    <td style="text-align: center">-</td>
                    <?php
                        }
                    ?>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="20" style="text-align: center">
                        No Result Found
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
