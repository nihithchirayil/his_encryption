<div class="row">

    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> OP Registration Renewal Detail </b>
        </h4>
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;width: 100%">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="5%;">Sl.No.</th>
                        <th width="15%;">Uhid</th>
                        <th width="32%;">Patient Details</th>
                        <th width="12%;">Reg. Fees</th>
                        <th width="12%;">Cons. Fees </th>
                        <th width="12%;">Hospital Fees</th>
                        <th width="12%;">Booking Fees</th>
                    </tr>
                </thead>
                <?php
                $doctor_name = '';
                ?>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
            $i=0;
            $slno=1;
            $nettotcons_fee=0;
            $nettotal_bill_net_amount=0;
         foreach ($res as $data){
             $net_amount=floatval($data->amount)-floatval($data->amount);
             if($doctor_name != $data->doctor_name){
                 $i=0;
                 $doctor_name = $data->doctor_name;
                 $nettotcons_fee=0;
                 $slno=1;
               ?>
                        <tr class="headerclass"
                            style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="7" class="common_td_rules">{{ $doctor_name }}</th>
                        </tr>
                        <?php
             }
             ?>
                        <tr>
                            <td class="common_td_rules">{{ $slno }}</td>
                            <td class="common_td_rules">{{ $data->uhid }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="td_common_numeric_rules">{{ 0.0 }}</td>
                            <td class="td_common_numeric_rules">{{ $data->amount }}</td>
                            <td class="td_common_numeric_rules">{{ 0.0 }}</td>
                            <td class="td_common_numeric_rules">{{ 0.0 }}</td>
                        </tr>
                        <?php
             $nettotcons_fee+= floatval($data->amount);
             if($i==$doctor_cnt[$doctor_name]){
             ?>
                        <tr style="height: 30px;">
                            <th class="common_td_rules" colspan="3" style="text-align: left;">Total</th>
                            <th class="td_common_numeric_rules">0.0</th>
                            <th class="td_common_numeric_rules"><?= $nettotcons_fee ?></th>
                            <th class="td_common_numeric_rules">0.0</th>
                            <th class="td_common_numeric_rules">0.0</th>

                        </tr>
                        <?php
                    $nettotal_bill_net_amount+=$nettotcons_fee;
             }
             $i++;
             $slno++;
         }
         ?>
                        <tr style="height: 30px;">
                            <th class="common_td_rules" colspan="3" style="text-align: left;">Net Total</th>
                            <th class="td_common_numeric_rules">0.0</th>
                            <th class="td_common_numeric_rules"><?= $nettotal_bill_net_amount ?></th>
                            <th class="td_common_numeric_rules">0.0</th>
                            <th class="td_common_numeric_rules">0.0</th>

                        </tr>
                    @else
                        <tr>
                            <td colspan="11" style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
        </font>
    </div>
</div>
