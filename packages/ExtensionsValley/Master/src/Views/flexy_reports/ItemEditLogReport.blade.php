<?php
use ExtensionsValley\Master\FlexyReportController;
?>
@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">

    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<style>
    .header_bg{
        background-color: #02967f;
        color:white;
    }
    a{color:black !important;}
    li>a{
      color:#E7E7E7 !important;
    }
    ul{padding-left:5px !important;font-weight: 200 !important;}
    li>a:hover{color:black !important;}
    .table_name{color:rgb(114, 4, 4) !important;}
    .gradient_bg{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
        color:white !important;
    }
    .bottom-border-text{
    border:none !important;
    border-bottom:1px solid lightgrey !important;
    box-shadow:none;
    }
    label{
        color:cornflowerblue !important;
    }

    .select2-selection__choice{
        background-color: #e6f1e8 !important;
        border: 1px solid #dbd8d8 !important;
        font-size: 10px !important;
    }
    .mate-input-box{
        height:51px !important;
    }

    .mate-input-box>text{
        margin-top:3px !important;
    }
    .select2-selection--multiple{
        padding-bottom:0px !important;
    }
    .table_header_bg{
        height: 30px !important;
        text-align:right !important;
        padding-right :40px !important;
        margin-bottom : 6px !important;
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .table_header_bg>h5{
        margin-top:7px;
        font-weight: 700;
    }
    .ajaxSearchBox{
        z-index: 9999 !important;
    }
    .ajaxSearchBox>li{
        font-size:13px !important;
        font-family: Arial !important;

    }
    .liHover{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .hospital_header>table{
        .border-bottom:1px solid #a5a4a4;
    }

    .td_common_numeric_rules{
        border-left: solid 1px #bbd2bd !important;
        text-align: right !important;
        font-size:11px !important;
    }
    .common_td_rules{
        text-align: left !important;
        overflow: hidden !important;
        border-right: solid 1px #bbd2bd !important;
        border-left: solid 1px #bbd2bd !important;
        max-width: 100px;
        text-overflow: ellipsis;
        white-space: nowrap;
        font-size:11px !important;
    }
    .ajaxSearchBox{
        display: none;
        width: 250px !important;
        height: 400px !important;
        overflow: hidden;
        padding-top: 0px !important;
        margin-top: 15px !important;
    }

    .table_sm th, .table_sm td{
        font-size:11px !important;
    }

    .hospital_header_excel{
        background-color:#07ad8c; color: white;
    }

    @media print {

        .theadscroll {max-height: none; overflow: visible;}
        #ResultsViewArea{width:50% !important;}
        table{
            display: none !important;
        }

      }

    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #efefef;
    }

</style>

@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" name="report_id" id="report_id" value="{{$report_id}}">
<input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
<input type="hidden" id="exceller_data"
    value="<?= base64_encode('<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>') ?>">

<div class="right_col">
    <div class="row">
        <div class="col-md-12 box-body" style="min-height:120px !important;">
            <div class="col-md-12 no-padding table_header_bg">
                <h5>{{$menu_name}}</h5>
            </div>


            {!!Form::open(array('name'=>$form_name, 'method' => 'get', 'id'=> $form_name))!!}

            <div class="box-body" style="padding-bottom:15px;">

                <div class="col-xs-2 date_filter_div"><div class="mate-input-box"><label class="filter_label">From Date</label>
                        <input type="text" data-attr="date" name="from_date_from"value="{{date('M-d-Y')}}" class="form-control filters" placeholder="MMM-DD-YYYY" id="from_date" autocomplete="off"></div></div><div class="col-xs-2 date_filter_div"><div class="mate-input-box"><label class="filter_label">To Date</label>
                    <input type="text" data-attr="date" name="to_date_to"value="{{date('M-d-Y')}}" class="form-control filters" placeholder="MMM-DD-YYYY" id="to_date" autocomplete="off"></div></div><div class= "col-xs-3">
                    <div class="mate-input-box">
                        <label class="filter_label">Item</label>
                        <input class="form-control hidden_search" value="" autocomplete="off" type="text"  autocomplete="off" id="item_name" name="item_name" />
                        <div id="item_nameAjaxDiv" class="ajaxSearchBox"></div>
                        <input class="filters" type="hidden" name="item_name_hidden" value="" id="item_name_hidden"></div>
                    </div>@php $fieldName = FlexyReportController::flexyReportMasterClassData('user_group');@endphp 
<div class="col-xs-2 date_filter_div hidden"><div class='mate-input-box'><label class='filter_label'>User</label><select multiple='multiple-select' class='form-control select2 bottom-border-text filters' style='width:100%;color:#555555; padding:2px 10px;' name='user_group' id='user_group'><option value='All'>All</option> @foreach($fieldName as $key=>$val)<option value='{{$key}}'>{{$val}}</option> @endforeach</select></div></div>
                    </div><div class= "col-xs-3">
                    <div class="mate-input-box">
                        <label class="filter_label">User</label>
                        <input class="form-control hidden_search" value="" autocomplete="off" type="text"  autocomplete="off" id="users" name="users" />
                        <div id="usersAjaxDiv" class="ajaxSearchBox"></div>
                        <input class="filters" type="hidden" name="users_hidden" value="" id="users_hidden"></div>
                    </div>
                 <div class="clearfix"></div>
                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <div class="clearfix"></div>

                    <button type="button" class="btn bg-green btn-block" style="color:white !important;width:101px !important;" onclick="getResultData();" name="search_results" id="search_results">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        Search
                    </button>

                </div>
                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">

                    <button type="reset" class="btn bg-green btn-block" style="color:white !important;width:101px !important;" name="" id="">
                        <i class="fa fa-repeat" aria-hidden="true"></i>
                        Reset
                    </button>

                </div>


                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="button" onclick="printReportData();" class="btn bg-green btn-block" style="color:white !important;width:101px !important;" name="print_results" id="print_results">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>

                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="button" onclick="exceller()" class="btn bg-green btn-block" style="color:white !important;width:101px !important;" name="csv_results" id="csv_results">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel
                    </button>
                </div>



            </div>
            {!! Form::token() !!} {!! Form::close() !!}
        </div>

            <div class="col-md-12 no-padding">
                <div id="ResultDataContainer"
                    style=" padding: 10px; display:none;font-family:poppinsregular;">
                    <style>
                           @media print {
                                .theadscroll {max-height: none; overflow: visible;}


                            }
                    </style>
                    <div style="float: left; width: 100% !important;" id="ResultsViewArea">


                    </div>
                </div>
            </div>

    </div>
</div>
@include('Master::flexy.report_print_modal')



@stop
@section('javascript_extra')

<script type="text/javascript">
    $(document).ready(function () {

        setTimeout(function(){
          // clearselect2();
           $(".select2").select2({ placeholder: "", maximumSelectionSize: 10 });
        }, 2000);


//fixed header script ends here
        setTimeout(function () {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        }, 300);
        //$(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
        $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
        $("input[data-attr='datetime']").datetimepicker({ format: 'MMM-DD-YYYY hh:mm:A' });
    });

    function getResultData(){
        var form_name = '{{$form_name}}';
        //alert(form_name);return;
        var form_data = $('#'+form_name).serialize();
        var base_url = $('#base_url').val();
        //var url = '{{$dataurl}}';
        var url = base_url+'/flexy_report/'+'{{$report_name}}';


        var filters_list = new Object();
        var filters_value = '';
        var filters_id = '';


        $('.filters').each(function() {
            filters_id = this.id;

            filters_value = $('#' + filters_id).val();
            if (Array.isArray(filters_value)) {

                var filter_aarry = jQuery.grep(filters_value, function(n, i) {
                    return (n !== "" && n != null);
                });


                if (filter_aarry.length == 0) {
                    filters_value = '';
                }

                if(filter_aarry.find(function(element) {
                    return element == 'All';
                })){
                    filters_value = '';
                }
            }
            filters_id = filters_id.replace('_hidden', '');

            if (filters_value && filters_value !== "" && filters_value !== "All" && filters_value !== [] && filters_value !== null && filters_value !='All') {
                filters_list[filters_id] = filters_value;
            }
        });



        $.ajax({
            url: url,
            type: 'POST',
            data: filters_list,
            beforeSend: function () {
                $('#ResultDataContainer').show();

                $('#ResultDataContainer').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('.ResultDataContainerBody').show();
                $('#ResultsViewArea').html(data);

            },
            complete: function () {
                $('#ResultDataContainer').LoadingOverlay("hide");

                $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
                });

                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table){
                        return $table.closest('.theadscroll');
                    }
                });


            }
        });
    }




    //----Hidden Filed Search--------------------------------------------------------------------

    $('.hidden_search').keyup(function (event) {
        var input_id = '';
        var base_url = $('#base_url').val();
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        input_id = $(this).attr('id');

        var current;
        if (value.match(keycheck) || event.keyCode == '8') {
            if ($('#' + input_id + '_hidden').val() != "") {
                $('#' + input_id + '_hidden').val('');
            }
            var search_key = $(this).val();
            search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
            search_key = search_key.trim();

            var department_hidden = $('#department_hidden').val();
            var datastring = '';
            if (input_id == 'sub_department') {
                datastring = '&department_id=' + department_hidden;
            }
            if (input_id == 'scheme') {
                datastring = '&company_id=' + company_hidden;
            }

            if (search_key == "") {
                $("#" + input_id + "AjaxDiv").html("");
                $("#" + input_id + "AjaxDiv").hide();

            } else {
                var url = base_url+'/flexy/FlexyReportProgressiveSearch';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                    beforeSend: function () {

                        $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {

                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {

                    },
                    error: function () {
                        bootbox.alert('nw error');
                        return;
                    }

                });
            }
        } else {
            ajax_list_key_down(input_id + 'AjaxDiv', event);
        }
    });

    function fillSearchDetials(id, name, serach_key_id) {
        $('#' + serach_key_id + '_hidden').val(id);
        $('#' + serach_key_id).val(name);
        $('#' + serach_key_id).attr('title', name);
        $("#" + serach_key_id + "AjaxDiv").hide();
    }


    /* setting for enter key press in ajaxDiv listing */
    $(".hidden_search").on('keydown', function (event) {
        var input_id = '';
        input_id = $(this).attr('id');
        if (event.keyCode === 13) {
            ajaxlistenter(input_id + 'AjaxDiv');
            return false;
        }
    });


$("#category").on('change', function() {
    var category = $('#category').val();
    if (category == '') {
      var category = 'a';

    }

    if(category) {
        $.ajax({
            type: "GET",
            url: '',
            data: 'category=' + category,
            beforeSend: function () {
                $('#s2id_sub_category').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                $( "#sub_category" ).prop( "disabled", true ); //Disable
            },
            success: function (html) {
                if(html){
                    $("#sub_category").empty();
                    $("#sub_category").html('<option value="">Select Sub catogery</option>');
                    $.each(html,function(key,value){
                        $("#sub_category").append('<option value="'+key+'">'+value+'</option>');
                    });
                }else{
                    $("#sub_category").empty();
                }
            },

            error: function () {
                Command: toastr["error"]("Please check your internet connection and try again!");
                $( "#sub_category" ).prop( "disabled", false );
            },
            complete: function () {
                $('#warning1').remove();
                $( "#sub_category" ).prop( "disabled", false );
            }

        });

    }else{
            $("#category").focus();
            $("#sub_category").empty();
        }

    });


    //---------generate csv---------------
function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

   function exceller() {
        $(".theadfix_wrapper").floatThead('destroy');
        $('#total_row').css('display', '');
        $('.hospital_header_excel').css('visibility', 'hidden');
        $('.hospital_header_excel').css('display', '');
        setTimeout(function() {
            var template_date = $('#exceller_data').val();
            var uri = 'data:application/vnd.ms-excel;base64,',
                template = atob(template_date),
                base64 = function(s) {
                    return window.btoa(unescape(encodeURIComponent(s)))
                },
                format = function(s, c) {
                    return s.replace(/{(\w+)}/g, function(m, p) {
                        return c[p];
                    })
                }
            var toExcel = document.getElementById("excelContainer").innerHTML;
            var ctx = {
                worksheet: name || '',
                table: toExcel
            };
            var link = document.createElement("a");
            var menu_name = '{{$menu_name}}';
            var current_time = '{{$current_date_time}}';
            link.download = menu_name+'-'+current_time+".xls";
            link.href = uri + base64(format(template, ctx))
            link.click();

            InitTheadScroll();
            $('#total_row').css('display', 'none');
            $('.hospital_header_excel').css('display', 'none');
            $('.hospital_header_excel').css('visibility', '');
        }, 1000);

    }

//---------generate csv ends---------


//---------print report--------
function printReportData() {
    $('#print_config_modal').modal('toggle');
}

 function print_generate(printData) {

    $('#resultDataTable').css('width', '98.5%');
    $(".theadfix_wrapper").floatThead('destroy');
    $('.theadscroll').perfectScrollbar('destroy');
    $('#total_row').css('display', '');
    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;



    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
    }

    mywindow.document.write(
        '<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>'
    );
    mywindow.document.write(
        '<style>.table{border: 1px solid #e8e8e8 !important;color:#3b3d3d !important;}</style>');
    mywindow.document.write(
        '<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>'
    );
    mywindow.document.write(
        '<style>thead{background-color:#02967f !important;color:white !important;word-wrap:break-word !important;}td{word-wrap:break-word !important;padding-left:5px !important;padding-right:3px !important;}</style>'
    );
    mywindow.document.write(
        '<style> tr:nth-child(even) td {background-color:#f2f4f5 !important;-webkit-print-color-adjust: exact;} tbody></style>'
    );

    mywindow.document.write(showw);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    setTimeout(function() {
        mywindow.print();
        mywindow.close();
        InitTheadScroll();
        $('#total_row').css('display', 'none');
    }, 1000);


    return true;
}

function InitTheadScroll() {
    $('.theadscroll').perfectScrollbar({
        minScrollbarLength: 30
    });

    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });
}



$("button[type='reset']").on('click', function(evt) {
        $(".select2").val([]).select2();
        $(".filters").val('');
        $('#ResultsViewArea').html('');
        $("body").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
        window.location.reload();

    });

    function clearselect2() {
        $('select').select2({
                placeholder: '',
                allowClear: true,
        });
    }






</script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>



@endsection
