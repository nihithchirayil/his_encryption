-@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">

    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<style>
    .header_bg{
        background-color: #02967f;
        color:white;
    }
    a{color:black !important;}
    li>a{
      color:#E7E7E7 !important;
    }
    ul{padding-left:5px !important;font-weight: 200 !important;}
    li>a:hover{color:black !important;}
    .table_name{color:rgb(114, 4, 4) !important;}
    .gradient_bg{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
        color:white !important;
    }
    .bottom-border-text{
    border:none !important;
    border-bottom:1px solid lightgrey !important;
    box-shadow:none;
    }
    label{
        color:cornflowerblue !important;
    }

    .select2-selection__choice{
        background-color: #e6f1e8 !important;
        border: 1px solid #dbd8d8 !important;
        font-size: 10px !important;
    }
    .mate-input-box{
        height:51px !important;
    }

    .mate-input-box>text{
        margin-top:3px !important;
    }
    .select2-selection--multiple{
        padding-bottom:0px !important;
    }
    .table_header_bg{
        height: 30px !important;
        text-align:right !important;
        padding-right :40px !important;
        margin-bottom : 6px !important;
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .table_header_bg>h5{
        margin-top:7px;
        font-weight: 700;
    }
    .ajaxSearchBox{
        z-index: 9999 !important;
    }
    .ajaxSearchBox>li{
        font-size:13px !important;
        font-family: Arial !important;

    }
    .liHover{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .hospital_header>table{
        .border-bottom:1px solid #a5a4a4;
    }

    .td_common_numeric_rules{
        border-left: solid 1px #bbd2bd !important;
        text-align: right !important;
    }
    .common_td_rules{
        text-align: left !important;
        overflow: hidden !important;
        border-right: solid 1px #bbd2bd !important;
        border-left: solid 1px #bbd2bd !important;
        max-width: 100px;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
    .ajaxSearchBox{
        display: none;
        width: 250px !important;
        height: 400px !important;
        overflow: hidden;
        padding-top: 0px !important;
        margin-top: 15px !important;
    }

    @media print {

        .theadscroll {max-height: none; overflow: visible;}
        #ResultsViewArea{width:50% !important;}
        table{
            display: none !important;
        }

      }

    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #efefef;
    }

</style>

@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" name="report_id" id="report_id" value="{{$report_id}}">
<input type="hidden" id="hospital_header" value="{{ $hospital_header }}">

<div class="right_col">
    <div class="row">
        <div class="col-md-12 box-body" style="min-height:120px !important;">
            <div class="col-md-12 no-padding table_header_bg">
                <h5>{{$menu_name}}</h5>
            </div>


            {!!Form::open(array('name'=>$form_name, 'method' => 'get', 'id'=> $form_name))!!}

            <div class="box-body" style="padding-bottom:15px;">

                <div class="col-xs-2 date_filter_div"><div class="mate-input-box"><label class="filter_label">From Date</label>
                        <input type="text" data-attr="date" name="from_date_from"value="{{date('M-d-Y')}}" class="form-control filters" placeholder="MMM-DD-YYYY" id="from_date" autocomplete="off"></div></div><div class="col-xs-2 date_filter_div"><div class="mate-input-box"><label class="filter_label">To Date</label>
                    <input type="text" data-attr="date" name="to_date_to"value="{{date('M-d-Y')}}" class="form-control filters" placeholder="MMM-DD-YYYY" id="to_date" autocomplete="off"></div></div><div class= "col-xs-3">
                    <div class="mate-input-box">
                        <label class="filter_label">Item Name</label>
                        <input class="form-control hidden_search" value="" autocomplete="off" type="text"  autocomplete="off" id="item_name" name="item_name" />
                        <div id="item_nameAjaxDiv" class="ajaxSearchBox"></div>
                        <input class="filters" type="hidden" name="item_name_hidden" value="" id="item_name_hidden"></div>
                    </div><div class="col-xs-2 date_filter_div">
                    <div class="mate-input-box">
                        <label class="filter_label ">Location</label><select name="location" class="form-control select2 filters" id="location" style="color:#555555; padding:4px 12px;"><option value=""> select- </option><option value="68">1ST FLOOR ROOM                </option><option value="172">1ST SOUTH DELUX ROOM</option><option value="536">2ND A BLOCK</option><option value="63">2ND A-BLOCK                   </option><option value="586">2ND A (STEPDOWN ICU)</option><option value="64">2ND B-BLOCK                   </option><option value="96">2ND CUBICLE          </option><option value="70">2ND CUBICLE & WARD              </option><option value="99">2ND PREMIUM                   </option><option value="65">2ND SOUTH BLOCK               </option><option value="72">3RD A-BLOCK                   </option><option value="144">3RD B-BLOCK                   </option><option value="69">3RD BLOCK-SOUTH DELUX ROOM           </option><option value="585">3RD CUBICLE & FW(COVID WARD)</option><option value="161">3RD IP PHARMACY</option><option value="36">3RD NEW WARD                  </option><option value="176">3RD SOUTH ANNEX SUIT ROOM</option><option value="201">ACADEMIC SECTION</option><option value="150">ADMINISTRATIVE OFFICE</option><option value="544">AMBULANCE </option><option value="67">ANAESTHESIA DEPMT             </option><option value="60">ASSET DEPT                    </option><option value="584">ASSISTANT MANAGER (IT)</option><option value="563">ASSISTANT PURCHASE MANAGER</option><option value="193">AUDIOLOGY</option><option value="516">BIO MEDICAL DEPARTMENT</option><option value="567">BROCHOSCOPY</option><option value="6">CANTEEN</option><option value="568">CANTEEN AREA SCREENING</option><option value="21">CARDIAC ICU (HDICU)            </option><option value="82">CASH                          </option><option value="152">Casuality Pharmacy                   </option><option value="20">CASUALTY                      </option><option value="506">CASUALTY IP</option><option value="554">CASUALTY OT</option><option value="75">CATH ICU                  </option><option value="510">CATHLAB</option><option value="74">CATH LAB                      </option><option value="503">CCU 1</option><option value="164">CCU 2</option><option value="588">CCU 3</option><option value="167">CHEMOTHERAPY</option><option value="134">CIVIL DEPARTMENT</option><option value="88">CLEANING                      </option><option value="124">CLINICAL PHARMACY & TRAINING DIVISION</option><option value="529">C M O</option><option value="599">COMMON AREA</option><option value="555">CONFERENCE HALL</option><option value="55">CONSUMABLE OUTLET             </option><option value="552"> COORDINATOR TRAUMA CARE</option><option value="128">CSSD</option><option value="90">CT                            </option><option value="111">CT OT                         </option><option value="91">CT SCAN                       </option><option value="93">CUWT</option><option value="504">CVTS ICU</option><option value="556">CVTS OT</option><option value="548">DAYA ARTS FESTIVAL CORE COMMITTEE</option><option value="110">DENTAL DEPT</option><option value="117">Department IP PHARMACY                   </option><option value="593">DEPARTMENT OF STATISTICIAN</option><option value="118">Department OP Pharmacy</option><option value="116">Department Pharmacy1</option><option value="115">Deprtment Store</option><option value="56">DIALYSIS</option><option value="189">DIRECTORS ROOM</option><option value="538">DISASTER STORE</option><option value="125">DISCHARGE SUMMARY</option><option value="572">DR.ABDUL JALEEL OP</option><option value="32">DR.ANANDAN ' S OP             </option><option value="157">DR.ANJU OP</option><option value="147">DR.ANTONY OP</option><option value="525">DR ARUN GANGADHAR</option><option value="512">DR ARUN GANGADHAR OP</option><option value="139">DR.ARUN OP</option><option value="530">DR.ASHA KISHORE OP</option><option value="551">DR.ASHA KRISHNA</option><option value="181">DR ASHA KRISHNA OP</option><option value="33">DR.BALU MOHAN'S OP            </option><option value="559">DR BINEESH OP</option><option value="136">DR.FIROSH KHAN OP</option><option value="141">DR.HARISH VALSON OP</option><option value="86">DR.IQBAL OP                   </option><option value="37">DR.JAIN CHIMMEN'S OP          </option><option value="574">DR JALEEL OP</option><option value="38">DR.JOSEPH. P U'S OP           </option><option value="527">DR K B MOHAN OP</option><option value="40">DR.LEELAMANI'S OP             </option><option value="28">DR.MEERA RAVEENDRAN'S OP      </option><option value="524">DR MUKESH MUKUNDAN</option><option value="42">DR.M V RAMAN'S OP             </option><option value="135">DR.NARAYANNAN UNNI OP</option><option value="561">DR.P.A.ABDUL AZEEZ ENT OP</option><option value="140">DR.PAUL JOHNY OP</option><option value="30">DR.P RAKHESH'S OP             </option><option value="61">DR.PREMKUMAR OP               </option><option value="45">DR.PRIYADARSHINI'S OP         </option><option value="557">DR RAFEEQ  OP</option><option value="169">DR RAHMANKHAN OP</option><option value="145">DR.RAJAN OP</option><option value="534">DR RAKESH OP</option><option value="49">DR.RAVEENDRAN T. K'S OP       </option><option value="565">DR.REHMAN KHAN OP</option><option value="199">DR REJITH OP</option><option value="542">DR REJITH OP</option><option value="143">DR.ROY OP</option><option value="149">DR.SAJEER OP</option><option value="156">DR.SANTHOSH BABU OP</option><option value="569">DR.SARASWATHY OP</option><option value="178">DR SATHISH OP</option><option value="595">DR SPACY OP</option><option value="50">DR.SREEKUMAR'S OP             </option><option value="148">DR.SREENIVASAN OP</option><option value="590">DR SUJA OP</option><option value="185">DR SUSEELA OP</option><option value="520">DR ULLAS OP</option><option value="54">DR.ULLAS' OP                  </option><option value="547">DR.VIDYA KISHORE OP</option><option value="146">DR.YOGANADHAN OP</option><option value="532">ECG</option><option value="80">ECG                           </option><option value="25">EEG                           </option><option value="122">ELECTRICAL</option><option value="566">EMERGENCY DEPARTMENT</option><option value="81">ENDOSCOPY                     </option><option value="533">E N T OP</option><option value="581">ENT OP</option><option value="34">E. N. T'S OP                  </option><option value="570">ER</option><option value="131">ESI</option><option value="518">ETP</option><option value="519">E T P</option><option value="543">FACILITY AND SECURITY MANAGER</option><option value="571">FEVER CLINIC</option><option value="16">FINANCE                       </option><option value="5">FRONT OFFICE</option><option value="190">GASTRO BLOCK</option><option value="192">GASTRO CASUALITY</option><option value="165">GASTRO ENDOSCOPY</option><option value="553">GASTRO MEDICAL(GENERAL)</option><option value="502">GASTRO MEDICAL ICU</option><option value="163">GASTRO OP</option><option value="535">GASTRO OP</option><option value="166">GASTRO OT</option><option value="594">GASTRO OT</option><option value="196">GASTRO PHARMACY</option><option value="509">GASTRO RECOVERY</option><option value="537">GASTRO RECOVERY ICU</option><option value="596">GATE NO.5</option><option value="114">General Return Store</option><option value="10">General Store1</option><option value="507">GENERAL WARD(DR ROY)</option><option value="560">GROUND FLOOR CASH CABIN</option><option value="598">GROUND FLOOR(GW WARD)</option><option value="24">G.SURGERY                     </option><option value="31">GYNEC ICU                     </option><option value="501">HDU</option><option value="528">HDU</option><option value="564">HEALTH CHECKUP AND SHARE HOLDERS ROOM</option><option value="35">HEALTH CHECK UP'S  OP         </option><option value="137">HIC OFFICE</option><option value="171">HISTOPATHOLOGY-LAB</option><option value="526">HOSPITAL ENGINEERS</option><option value="539">HOUSEKEEPING DEPARTMENT</option><option value="121">HR DEPARTMENT</option><option value="194">ICU AMBULANCE</option><option value="129">IMC OP</option><option value="85">INSURANCE                     </option><option value="522">INTERNATIONAL LOUNGE</option><option value="104">IP PHARMACY                   </option><option value="153">IT Department</option><option value="582">JAN OUSHADHI</option><option value="591">JOSCO</option><option value="583">JR ICU (NEW)</option><option value="119">Kidney Transplantation ICU (KTICU)</option><option value="511">KTU</option><option value="4">Lab </option><option value="78">LABOUR ROOM                   </option><option value="589">LABOUR ROOM(COVID)</option><option value="120">LAUNDARY</option><option value="600">LEO MARKETING ROOM</option><option value="184">LIBRARY</option><option value="179">Loss Of Stock</option><option value="1">Main Pharmacy</option><option value="187">MANAGER ROOM</option><option value="523">MARKETING TEAM</option><option value="101">MICU(MDCCU)                     </option><option value="579">MORTUARY</option><option value="513">MOT</option><option value="546">MRD</option><option value="29">MRD OUTLET</option><option value="175">MWT</option><option value="62">NEONATAL ICU(NICU)     </option><option value="44">NEPHRO OP                     </option><option value="514">NEURO OT</option><option value="71">NMICU                         </option><option value="130">N.S OFFICE</option><option value="108">NURSING STATION               </option><option value="89">NURSING SUPERINT OFFICE       </option><option value="123">NUTRITION</option><option value="198">NUTRITION DEPARTMENT</option><option value="197">ONCOLOGY</option><option value="541">O P DEPARTMENTS</option><option value="7">OP Pharmacy</option><option value="27">ORTHOPAEDICS OP               </option><option value="9">OT</option><option value="545">OT</option><option value="515">OT(ORTHO)</option><option value="154">Oxygen Department</option><option value="531">PAIN AND PALLIATIVE</option><option value="182">PAIN AND PALLIATIVE</option><option value="159">PHARMACY CASH</option><option value="195">pharmacy store consumables</option><option value="133">PHOTOSTAT</option><option value="549">PHYSIOTHERAPY DEPARTMENT</option><option value="132">PLAY SCHOOL</option><option value="576">PO ICU</option><option value="558">PULMONOLOGY DEPARTMENT</option><option value="540">QUALITY CONTROL</option><option value="160">QUALITY MANAGER</option><option value="580">QUARANTINE</option><option value="2">Radiology</option><option value="505">RECOVERY ICU</option><option value="606">RECOVERY ICU 2</option><option value="578">REPAIRS AND MAINTENANCE</option><option value="8">Return Store</option><option value="18">SCANNING                      </option><option value="573">SCREENING AREA (EMERGENCY)</option><option value="158">SECURITY IN CHARGE</option><option value="151">SERVER ROOM</option><option value="84">SKIN                          </option><option value="550">SPEECH THERAPY DEPARTMENT</option><option value="587">STEPDOWN ICU(COVID ICU)</option><option value="3">Store 1</option><option value="597">STRATEGY MANAGER</option><option value="180">Sub Pharmacy</option><option value="138">SURGERY OP</option><option value="601">SURGICAL CRITICAL CARE</option><option value="66">SURGICAL ICU                  </option><option value="603">TESTLOCATION</option><option value="94">TFW</option><option value="142">TRANSPLANTATION CO-ORDINATOR</option><option value="19">TRAUMA ICU(TRICU)             </option><option value="562">TRIAGE SECOND</option><option value="26">UROLOGY OP                    </option><option value="79">USG                           </option><option value="191">VICE PRESIDENT</option><option value="575">VIROLOGY LAB</option><option value="155">Water Department</option><option value="17">X RAY</option></select> </div> </div>
                 <div class="clearfix"></div>
                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <div class="clearfix"></div>

                    <button type="button" class="btn bg-green btn-block" style="color:white !important;width:101px !important;" onclick="getResultData();" name="search_results" id="search_results">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        Search
                    </button>

                </div>
                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">

                    <button type="reset" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="" id="">
                        <i class="fa fa-repeat" aria-hidden="true"></i>
                        Reset
                    </button>

                </div>


                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="button" onclick="printReportData();" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="print_results" id="print_results">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>

                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="button" onclick="exceller()" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="csv_results" id="csv_results">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel
                    </button>
                </div>



            </div>
            {!! Form::token() !!} {!! Form::close() !!}
        </div>

            <div class="col-md-12 no-padding">
                <div id="ResultDataContainer"
                    style=" padding: 10px; display:none;font-family:poppinsregular;">
                    <style>
                           @media print {
                                .theadscroll {max-height: none; overflow: visible;}


                            }
                    </style>
                    <div style="float: left; width: 100% !important;" id="ResultsViewArea">


                    </div>
                </div>
            </div>

    </div>
</div>
@include('Master::flexy.report_print_modal')



@stop
@section('javascript_extra')

<script type="text/javascript">
    $(document).ready(function () {



//fixed header script ends here
        setTimeout(function () {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        }, 300);
        $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
        $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
        $("input[data-attr='datetime']").datetimepicker({ format: 'MMM-DD-YYYY hh:mm:A' });
    });

    function getResultData(){
        var form_name = '{{$form_name}}';
        //alert(form_name);return;
        var form_data = $('#'+form_name).serialize();
        var base_url = $('#base_url').val();
        //var url = '{{$dataurl}}';
        var url = base_url+'/flexy_report/'+'{{$report_name}}';


        var filters_list = new Object();
        var filters_value = '';
        var filters_id = '';


        $('.filters').each(function () {
            filters_id = this.id;

            filters_value = $('#' + filters_id).val();
            filters_id = filters_id.replace('_hidden', '');

            if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
                filters_list[filters_id] = filters_value;
            }
        });



        $.ajax({
            url: url,
            type: 'POST',
            data: filters_list,
            beforeSend: function () {
                $('#ResultDataContainer').show();

                $('#ResultDataContainer').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('.ResultDataContainerBody').show();
                $('#ResultsViewArea').html(data);

            },
            complete: function () {
                $('#ResultDataContainer').LoadingOverlay("hide");

                $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
                });

                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table){
                        return $table.closest('.theadscroll');
                    }
                });


            }
        });
    }




    //----Hidden Filed Search--------------------------------------------------------------------

    $('.hidden_search').keyup(function (event) {
        var input_id = '';
        var base_url = $('#base_url').val();
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        input_id = $(this).attr('id');

        var current;
        if (value.match(keycheck) || event.keyCode == '8') {
            if ($('#' + input_id + '_hidden').val() != "") {
                $('#' + input_id + '_hidden').val('');
            }
            var search_key = $(this).val();
            search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
            search_key = search_key.trim();

            var department_hidden = $('#department_hidden').val();
            var datastring = '';
            if (input_id == 'sub_department') {
                datastring = '&department_id=' + department_hidden;
            }
            if (input_id == 'scheme') {
                datastring = '&company_id=' + company_hidden;
            }

            if (search_key == "") {
                $("#" + input_id + "AjaxDiv").html("");
            } else {
                var url = base_url+'/flexy/FlexyReportProgressiveSearch';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                    beforeSend: function () {

                        $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {

                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {

                    },
                    error: function () {
                        bootbox.alert('nw error');
                        return;
                    }

                });
            }
        } else {
            ajax_list_key_down(input_id + 'AjaxDiv', event);
        }
    });

    function fillSearchDetials(id, name, serach_key_id) {
        $('#' + serach_key_id + '_hidden').val(id);
        $('#' + serach_key_id).val(name);
        $('#' + serach_key_id).attr('title', name);
        $("#" + serach_key_id + "AjaxDiv").hide();
    }


    /* setting for enter key press in ajaxDiv listing */
    $(".hidden_search").on('keydown', function (event) {
        var input_id = '';
        input_id = $(this).attr('id');
        if (event.keyCode === 13) {
            ajaxlistenter(input_id + 'AjaxDiv');
            return false;
        }
    });


$("#category").on('change', function() {
    var category = $('#category').val();
    if (category == '') {
      var category = 'a';

    }

    if(category) {
        $.ajax({
            type: "GET",
            url: '',
            data: 'category=' + category,
            beforeSend: function () {
                $('#s2id_sub_category').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                $( "#sub_category" ).prop( "disabled", true ); //Disable
            },
            success: function (html) {
                if(html){
                    $("#sub_category").empty();
                    $("#sub_category").html('<option value="">Select Sub catogery</option>');
                    $.each(html,function(key,value){
                        $("#sub_category").append('<option value="'+key+'">'+value+'</option>');
                    });
                }else{
                    $("#sub_category").empty();
                }
            },

            error: function () {
                Command: toastr["error"]("Please check your internet connection and try again!");
                $( "#sub_category" ).prop( "disabled", false );
            },
            complete: function () {
                $('#warning1').remove();
                $( "#sub_category" ).prop( "disabled", false );
            }

        });

    }else{
            $("#category").focus();
            $("#sub_category").empty();
        }

    });


    //---------generate csv---------------
function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function exceller(type, fn, dl) {
    var type = 'xlsx';
    var elt = document.getElementById('ResultsViewArea');
    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
    return dl ?
        XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
        XLSX.writeFile(wb, fn || ('excel_data.' + (type || type)));
}

//---------generate csv ends---------


//---------print report--------
function printReportData() {
    $('#print_config_modal').modal('toggle');



}

function print_generate(printData) {
    $('#resultDataTable').css('width', '98.5%');
    $(".theadfix_wrapper").floatThead('destroy');
    $('.theadscroll').perfectScrollbar('destroy');

    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;


    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait;margin:20px;text-align:center;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape;margin:20px;text-align:center;}</style>');
    }
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    setTimeout(function(){
        mywindow.print();
        mywindow.close();
    }, 1000);


    return true;
}

$("button[type='reset']").on('click', function(evt) {
        $(".select2").val([]).select2();
        $("input[type='hiddden']").val('');

    });






</script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>



@endsection
