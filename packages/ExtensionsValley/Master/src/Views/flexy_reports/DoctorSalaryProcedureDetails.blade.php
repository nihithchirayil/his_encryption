<div class="col-md-12 no-padding">
    <div class="col-md-6">
        <h5 style="color:rgb(7, 7, 87);">{{$doctor_name}}</h5>
    </div>
    <div class="col-md-3 pull-right no-padding">
        <button type="button" class="btn bg-green pull-right" onclick="UpdateProcedureServices();">
            <i class="fa fa-save"></i> Save Percentage
        </button>
        <button type="button" class="btn btn-primary pull-right" onclick="add_percentage();">
            <i class="fa fa-plus"></i> Add Percentage
        </button>
        <button type="button" class="btn btn-primary pull-right" onclick="printDiv('procedure_services_table_data');">
            <i class="fa fa-print"></i> Print
        </button>
    </div>
</div>
<div class="col-md-12 theadscroll" id="procedure_services_table_data" style="height:380px;">
    <table border="1" id="procedure_services_table" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;">
        <thead>
            <tr style="background-color: paleturquoise;font-weight:600;">
                <td width="10%">Si.No</td>
                <td width="30%">Service Name</td>
                <td width="20%">Net Amount</td>
                <td width="20%">Percentage</td>
                <td width="20%">Total</td>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
                $total_amt = 0;
                $total_percentage = 0;
                $actual_amount = 0;
                $total_actual_amt = 0;
            @endphp
            @if (sizeof($result_data)>0)
                @foreach($result_data as $data)
                <tr>
                    <td>{{$i}}</td>
                    <td style="text-align:left">{{$data->service_name}}</td>
                    <td style="">{{$data->net_amount}}</td>
                    <td>
                        <input type="text" onkeyup="recalculateActualAmount(this.value,'{{$data->net_amount}}','{{$data->id}}')" class="form-control doctor_procedure_percentage" data-attr-id="{{$data->id}}" style="text-align:center;"  name="dr_percentage[]" id="dr_percentage_{{$i}}" value="{{$data->percentage}}"/>
                        <span id="dr_percentage_label_{{$i}}" style="display:none;" class="doctor_procedure_percentage_label">{{$data->percentage}}</span>
                    </td>
                    @php
                        $actual_amount = $data->net_amount * $data->percentage / 100;


                    @endphp
                    <td id="actual_amt_{{$data->id}}">{{$actual_amount}}</td>
                </tr>
                    @php
                        $i++;
                        $total_amt+=$data->net_amount;
                        $total_actual_amt+=$actual_amount;
                    @endphp
                @endforeach
                <tr>
                    <td colspan="2"><b>Total</b></td>
                    <td><b>{{$total_amt}}</b></td>
                    <td></td>
                    <td><b>{{$total_actual_amt}}</b></td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
<input type="hidden" name="percentage_doctor_id" id="percentage_doctor_id" value="{{$doctor_id}}"/>
<input type="hidden" name="percentage_doctor_name" id="percentage_doctor_name" value="{{$doctor_name}}"/>

