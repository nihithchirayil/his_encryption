@php
    //dd($already_added_services);
@endphp
<ul style="list-style-type: none;" id="services_list_block">
    @foreach($service_list as $service)
        <li>
            <div class="checkbox checkbox-success">
                <input id="service_{{$service->id}}" data-attr-desc="{{$service->service_desc}}" class="" type="checkbox" onchange="add_or_remove_service('{{$service->id}}','{{$service->service_desc}}');" value="{{$service->id}}" @if(in_array($service->service_desc, $already_added_services)) checked="checked" @endif/>
                <label style="color:black !important;" for="service_{{$service->id}}" class="service_{{$service->id}}">
                {{$service->service_desc}}</label>
            </div>
        </li>
    @endforeach
</ul>
