@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" name="report_id" id="report_id" value="{{ $report_id }}">
    <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
    <input type="hidden" id="hospital_address" value="{{ $hospital_address }}">

    <div class="right_col">
        <div class="row">
            <div class="col-md-12 box-body" style="min-height:120px !important;">
                <div class="col-md-12 table_header_bg">
                    <h5 class="pull-right">{{ $menu_name }}</h5>
                </div>


                {!! Form::open(['name' => $form_name, 'method' => 'get', 'id' => $form_name]) !!}

                <div class="box-body" style="padding-bottom:15px;">
                    <div class="col-md-2 date_filter_div">
                        <div class="mate-input-box"><label class="filter_label">Invoice From Date</label>
                            <input value="<?= date('M-d-Y') ?>" type="text" data-attr="date" name="invoce_date_from"
                                value="" class="form-control filters" placeholder="MMM-DD-YYYY" id="invoce_date_from"
                                autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-2 date_filter_div">
                        <div class="mate-input-box"><label class="filter_label">Invoice To Date</label>
                            <input value="<?= date('M-d-Y') ?>" type="text" data-attr="date" name="invoce_date_to" value=""
                                class="form-control filters" placeholder="MMM-DD-YYYY" id="invoce_date_to"
                                autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label class="filter_label">Invoice No.</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control filters" id="bill_no_search">
                            <div id="ajaxBillNoSearchBox" style="width:100% !important;margin-top:-17px"
                                class="ajaxSearchBox"></div>
                            <input type="hidden" class="filters" id="bill_id_hidden">
                        </div>
                    </div>

                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label class="filter_label">GRN No.</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control filters" id="grn_number">
                            <div id="ajaxGrnNoSearchBox" style="width:100% !important;margin-top:-17px"
                                class="ajaxSearchBox"></div>
                            <input type="hidden" class="filters" id="grn_number_hidden">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label class="filter_label">Return No.</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control filters" id="return_number">
                            <div id="ajaxreturnNoSearchBox" style="width:100% !important;margin-top:-17px"
                                class="ajaxSearchBox"></div>
                            <input type="hidden" class="filters" id="return_number_hidden">
                        </div>
                    </div>

                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label class="filter_label">Vendor Name</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control filters" id="vendor_name_search">
                            <div id="ajaxVendorSearchBox" style="width:100% !important;margin-top:-17px"
                                class="ajaxSearchBox"></div>
                            <input type="hidden" class="filters" id="vendoritem_code_hidden">
                        </div>
                    </div>
                    <!-- <div class="col-md-2">
                        <div class="mate-input-box">
                            <label class="filter_label">Category</label>
                            <div class="clearfix"></div>
                            {!! Form::select('gst_category', ['1' => 'Pharmacy', '2' => 'Other', '3' => 'All'], 1, [
                                    'class' => 'form-control filters',
                                    'id' => 'gst_category',
                                ]) 
                            !!}
                        </div>
                    </div> -->
                    <div class="clearfix"></div>
                    <div class="col-md-1 padding-sm" style="float:right;padding-top: 25px;">
                        <div class="clearfix"></div>
                        <button type="button" class="btn bg-green btn-block"
                            style="color:white !important;width:101px !important;" onclick="getResultData();"
                            name="search_results" id="search_results">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            Search
                        </button>
                    </div>
                    <div class="col-md-1 padding-sm" style="float:right;padding-top: 25px;">
                        <button type="reset" class="btn bg-primary btn-block"
                            style="color:white !important;width:101px !important;" name="" id="">
                            <i class="fa fa-repeat" aria-hidden="true"></i>
                            Reset
                        </button>
                    </div>


                    <div class="col-md-1 padding-sm" style="float:right;padding-top: 25px;">
                        <button type="button" onclick="printReportData();" class="btn bg-primary btn-block"
                            style="color:white !important;width:101px !important;" name="print_results" id="print_results">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>

                    <div class="col-md-1 padding-sm" style="float:right;padding-top: 25px;">
                        <button type="button" onclick="exceller_template_without_header('Purchase GST Report', 40)"
                            class="btn bg-primary btn-block" style="color:white !important;width:101px !important;"
                            name="csv_results" id="csv_results">
                            <i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel
                        </button>
                    </div>



                </div>
                {!! Form::token() !!} {!! Form::close() !!}
            </div>

            <div class="col-md-12 no-padding">
                <div id="ResultDataContainer" style=" padding: 10px; display:none;font-family:poppinsregular;">
                    <style>
                        @media print {
                            .theadscroll {
                                max-height: none;
                                overflow: visible;
                            }

                        }

                    </style>
                    <div style="float: left; width: 100% !important;" id="ResultsViewArea">


                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop
@section('javascript_extra')

    <script type="text/javascript">
        $(document).ready(function() {



            //fixed header script ends here
            setTimeout(function() {
                $('.multiple_selectbox').multiselect();
                $("option:selected").prop("selected", false);
            }, 300);
            $(".select2").select2({
                placeholder: "",
                maximumSelectionSize: 6
            });
            $("input[data-attr='date']").datetimepicker({
                format: 'MMM-DD-YYYY'
            });
        });

        function getResultData() {
            var form_name = '{{ $form_name }}';
            var form_data = $('#' + form_name).serialize();
            var base_url = $('#base_url').val();
            //var url = '{{ $dataurl }}';
            var url = base_url + '/flexy_report/' + '{{ $report_name }}';


            var filters_list = new Object();
            var filters_value = '';
            var filters_id = '';


            $('.filters').each(function() {
                filters_id = this.id;

                filters_value = $('#' + filters_id).val();
                filters_id = filters_id.replace('_hidden', '');

                if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
                    filters_list[filters_id] = filters_value;
                }
            });
            $.ajax({
                url: url,
                type: 'POST',
                data: filters_list,
                beforeSend: function() {
                    $('#ResultDataContainer').show();

                    $('#ResultDataContainer').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                },
                success: function(data) {
                    $('.ResultDataContainerBody').show();
                    $('#ResultsViewArea').html(data);

                },
                complete: function() {
                    $('#ResultDataContainer').LoadingOverlay("hide");

                    $('.theadscroll').perfectScrollbar({
                        minScrollbarLength: 30
                    });

                    var $table = $('table.theadfix_wrapper');
                    $table.floatThead({
                        scrollContainer: function($table) {
                            return $table.closest('.theadscroll');
                        }
                    });


                }
            });
        }

        $('#return_number').keyup(function(event) {
            var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
            var value = event.key; //get the charcode and convert to char
            if (event.keyCode == 13) {
                ajaxlistenter('ajaxreturnNoSearchBox');
                return false;
             } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
                var grn_number = $('#return_number').val();
                if (grn_number == "") {
                    $('#ajaxreturnNoSearchBox').hide();
                    $("#return_number_hidden").val('');
                } else {
                    var base_url = $('#base_url').val();
                    var token = $('#c_token').val();
                    var url = base_url + "/purchase/grnNumberSearch";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            _token: token,
                            search_key_id: 'return_number',
                            search_key: grn_number
                        },
                        beforeSend: function() {
                            $("#ajaxreturnNoSearchBox").html(
                                '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            ).show();
                        },
                        success: function(html) {
                            $("#ajaxreturnNoSearchBox").html(html).show();
                            $("#ajaxreturnNoSearchBox").find('li').first().addClass('liHover');
                        },
                        complete: function() {},
                        error: function() {
                            toastr.error('Please check your internet connection and try again');
                        }
                    });
                }
            } else {
                ajax_list_key_down('ajaxreturnNoSearchBox', event);
            }
        });

        $('#grn_number').keyup(function(event) {
            var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
            var value = event.key; //get the charcode and convert to char
            if (event.keyCode == 13) {
                ajaxlistenter('ajaxGrnNoSearchBox');
                return false;
             } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
                var grn_number = $('#grn_number').val();
                if (grn_number == "") {
                    $('#ajaxGrnNoSearchBox').hide();
                    $("#grn_number_hidden").val('');
                } else {
                    var base_url = $('#base_url').val();
                    var token = $('#c_token').val();
                    var url = base_url + "/purchase/grnNumberSearch";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            _token: token,
                            search_key_id: 'grn_number',
                            search_key: grn_number
                        },
                        beforeSend: function() {
                            $("#ajaxGrnNoSearchBox").html(
                                '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            ).show();
                        },
                        success: function(html) {
                            $("#ajaxGrnNoSearchBox").html(html).show();
                            $("#ajaxGrnNoSearchBox").find('li').first().addClass('liHover');
                        },
                        complete: function() {},
                        error: function() {
                            toastr.error('Please check your internet connection and try again');
                        }
                    });
                }
            } else {
                ajax_list_key_down('ajaxGrnNoSearchBox', event);
            }
        });


        $('#vendor_name_search').keyup(function(event) {
            var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
            var value = event.key; //get the charcode and convert to char
            if (event.keyCode == 13) {
                ajaxlistenter('ajaxVendorSearchBox');
                return false;
             } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
                var vendor_name = $('#vendor_name_search').val();
                if (vendor_name == "") {
                    $('#ajaxVendorSearchBox').hide();
                    $("#vendoritem_code_hidden").val('');
                } else {
                    var base_url = $('#base_url').val();
                    var url = base_url + "/purchase/vendor_search";
                    var token = $('#c_token').val();
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            _token: token,
                            vendor_name: vendor_name,
                            row_id: 1
                        },
                        beforeSend: function() {
                            $("#ajaxVendorSearchBox").html(
                                '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            ).show();
                        },
                        success: function(html) {
                            $("#ajaxVendorSearchBox").html(html).show();
                            $("#ajaxVendorSearchBox").find('li').first().addClass('liHover');
                        },
                        complete: function() {},
                        error: function() {
                            toastr.error('Please check your internet connection and try again');
                        }
                    });
                }
            } else {
                ajax_list_key_down('ajaxVendorSearchBox', event);
            }
        });



        $('#bill_no_search').keyup(function(event) {
            var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
            var value = event.key; //get the charcode and convert to char
            if (event.keyCode == 13) {
                ajaxlistenter('ajaxBillNoSearchBox');
                return false;
             } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
                var bill_no = $('#bill_no_search').val();
                if (bill_no == "") {
                    $('#ajaxBillNoSearchBox').hide();
                    $("#bill_id_hidden").val('');
                } else {
                    var base_url = $('#base_url').val();
                    url = base_url + "/purchase/grnNumberSearch";
                    var token = $('#c_token').val();
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            _token: token,
                            search_key_id: 'invoice_number',
                            search_key: bill_no
                        },
                        beforeSend: function() {
                            $("#ajaxBillNoSearchBox").html(
                                '<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            ).show();
                        },
                        success: function(html) {
                            $("#ajaxBillNoSearchBox").html(html).show();
                            $("#ajaxBillNoSearchBox").find('li').first().addClass('liHover');
                        },
                        complete: function() {},
                        error: function() {
                            toastr.error('Please check your internet connection and try again');
                        }
                    });
                }
            } else {
                ajax_list_key_down('ajaxBillNoSearchBox', event);
            }
        });

        function htmlDecode(input) {
            var e = document.createElement('textarea');
            e.innerHTML = input;
            return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
        }


        function fillInvoice_no(grn_id, bill_no) {
            bill_no = htmlDecode(bill_no);
            $('#bill_no_search').val(bill_no);
            $('#bill_id_hidden').val(grn_id);
            $('#ajaxBillNoSearchBox').hide();
        }

        function fillItemValues1(list, id, vendor_code, gst_vendor_code, vendor_name, email, row_id) {
            vendor_name = htmlDecode(vendor_name);
            $('#vendor_name_search').val(vendor_name);
            $('#vendoritem_code_hidden').val(vendor_code);
            $('#ajaxVendorSearchBox').hide();
        }


        //----Hidden Filed Search--------------------------------------------------------------------

        $('.hidden_search').keyup(function(event) {
            var input_id = '';
            var base_url = $('#base_url').val();
            var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
            var value = event.key; //get the charcode and convert to char
            input_id = $(this).attr('id');

            var current;
            if (value.match(keycheck) || event.keyCode == '8') {
                if ($('#' + input_id + '_hidden').val() != "") {
                    $('#' + input_id + '_hidden').val('');
                }
                var search_key = $(this).val();
                search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
                search_key = search_key.trim();

                var department_hidden = $('#department_hidden').val();
                var datastring = '';
                if (input_id == 'sub_department') {
                    datastring = '&department_id=' + department_hidden;
                }
                if (input_id == 'scheme') {
                    datastring = '&company_id=' + company_hidden;
                }

                if (search_key == "") {
                    $("#" + input_id + "AjaxDiv").html("");
                } else {
                    var url = base_url + '/flexy/FlexyReportProgressiveSearch';
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                        beforeSend: function() {

                            $("#" + input_id + "AjaxDiv").html(
                                '<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                            ).show();
                        },
                        success: function(html) {

                            $("#" + input_id + "AjaxDiv").html(html).show();
                            $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                        },
                        complete: function() {

                        },
                        error: function() {
                            bootbox.alert('nw error');
                            return;
                        }

                    });
                }
            } else {
                ajax_list_key_down(input_id + 'AjaxDiv', event);
            }
        });

        function fillSearchDetials(id, name, serach_key_id) {
            $('#' + serach_key_id + '_hidden').val(id);
            $('#' + serach_key_id).val(name);
            $('#' + serach_key_id).attr('title', name);
            $(".ajaxSearchBox").hide();
        }


        /* setting for enter key press in ajaxDiv listing */
        $(".hidden_search").on('keydown', function(event) {
            var input_id = '';
            input_id = $(this).attr('id');
            if (event.keyCode === 13) {
                ajaxlistenter(input_id + 'AjaxDiv');
                return false;
            }
        });


        $("#category").on('change', function() {
            var category = $('#category').val();
            if (category == '') {
                var category = 'a';

            }

            if (category) {
                $.ajax({
                    type: "GET",
                    url: '',
                    data: 'category=' + category,
                    beforeSend: function() {
                        $('#s2id_sub_category').append(
                            '<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>'
                        );
                        $("#sub_category").prop("disabled", true); //Disable
                    },
                    success: function(html) {
                        if (html) {
                            $("#sub_category").empty();
                            $("#sub_category").html('<option value="">Select Sub catogery</option>');
                            $.each(html, function(key, value) {
                                $("#sub_category").append('<option value="' + key + '">' +
                                    value + '</option>');
                            });
                        } else {
                            $("#sub_category").empty();
                        }
                    },

                    error: function() {
                        Command: toastr["error"](
                            "Please check your internet connection and try again!");
                        $("#sub_category").prop("disabled", false);
                    },
                    complete: function() {
                        $('#warning1').remove();
                        $("#sub_category").prop("disabled", false);
                    }

                });

            } else {
                $("#category").focus();
                $("#sub_category").empty();
            }

        });
    </script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
@endsection
