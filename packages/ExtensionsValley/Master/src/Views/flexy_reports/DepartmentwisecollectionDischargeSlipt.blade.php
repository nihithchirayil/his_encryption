<div class="row">
    <div class="col-md-12 padding_sm" id="result_detail_container_div">
        <table id="result_data_detail_table" class='table table-striped table-bordered table-condensed table_sm'
            style="width:100%; font-size: 12px;">
            <thead>
                <tr>
                    <th style="background-color:#07ad8c; color: black;" width='5%'>SL. No.</th>
                    <th style="background-color:#07ad8c; color: black;" width='12%'>Admitting Doctor</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>UHID</th>
                    <th style="background-color:#07ad8c; color: black;" width='13%'>Patient Name</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Bill No</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Bill Date</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Payment Type</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Paid</th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Created By</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Bill Amt.</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Total Dis.</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Net Amt.</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Adv. Adj.</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Paid Amt.</th>
                    <th style="background-color:#07ad8c; color: black;" width='5%'><i class="fa fa-list"></i></th>

                </tr>
            </thead>

            <tbody>
                @if (count($result_data) != 0)
                    @php
                        $i = 1;
                        $total_paid_amount = 0;
                        $total_net_amount = 0;
                        $total_adj_amount = 0;
                    @endphp
                    @foreach ($result_data as $data)
                        @php
                        @endphp
                        <tr>
                            <td class="common_td_rules">{{ $i }}</td>
                            <td class="common_td_rules">{{ $data->doctor_name }}</td>
                            <td class="common_td_rules" id="dischrage_uhid{{ $data->discharge_id }}">{{ $data->uhid }}
                            </td>
                            <td class="common_td_rules" id="dischrage_patientname{{ $data->discharge_id }}">
                                {{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->bill_no }}</td>
                            <td class="common_td_rules">{{ $data->bill_date }}</td>
                            <td class="common_td_rules">{{ $data->payment_type }}</td>
                            <td class="common_td_rules">{{ $data->paid_status }}</td>
                            <td class="common_td_rules">{{ $data->created_by }}</td>
                            <td class="td_common_numeric_rules">{{ $data->bill_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $data->total_discount }}</td>
                            <td class="td_common_numeric_rules">{{ $data->net_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $data->advance_adj }}</td>
                            <td class="td_common_numeric_rules">{{ $data->paid_amount }}</td>
                            <td class="td_common_numeric_rules"><button
                                    id="getDischargeDetailBtn{{ $data->discharge_id }}" class="btn btn-primary"
                                    onclick="getDischargeDetail({{ $data->discharge_id }})" type="button">
                                    <i id="getDischargeDetailSpin{{ $data->discharge_id }}"
                                        class="fa fa-list"></i></button>
                            </td>
                        </tr>
                        @php
                            $i++;
                            $total_paid_amount += floatval($data->paid_amount);
                            $total_net_amount += floatval($data->net_amount);
                            $total_adj_amount += floatval($data->advance_adj);
                        @endphp
                    @endforeach
                    <tr>
                        <th class="common_td_rules" colspan="11">Total</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_net_amount, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_adj_amount, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_paid_amount, 2, '.', '') }}</th>
                    </tr>
                @else
                    <tr>
                        <th colspan="5" style="text-align: center;">No records Found!</th>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
