<h5 style="color:rgb(7, 7, 87);">{{$doctor_name}}</h5>
<button type="button" class="btn btn-primary pull-right" onclick="printDiv('bills_analyisis_details_data');">
    <i class="fa fa-print"></i> Print
</button>
<div id="bills_analyisis_details_data">
<table border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;">
    <thead>
        <tr style="background-color: paleturquoise;font-weight:600;">
            <td style="width:5%">SiNo</td>
            <td style="width:65%">Department</td>
            <td style="width:10%">Amount</td>
            <td style="width:10%">Percentage</td>
            <td style="width:10%">Net Amount</td>
        </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
            $total_amt = 0;
            $total_percentage = 0;
            $total_final = 0;
            $final = 0;
        @endphp
        @if(sizeof($result_data)>0)
            @foreach($result_data as $data)
                <tr>
                    <td>{{$i}}</td>
                    <td style="text-align:left">{{$data->department_name}}</td>
                    <td style="text-align:right;padding-left:5px;">{{$data->net_amount}}</td>
                    <td>
                        @php
                            $current_percentage = ($data->net_amount*$data->percentage)/100;
                        @endphp
                        <input class="form-control bill_analysis_dr_percentage " style="text-align:center;" onchange="change_percentage('{{$current_percentage}}','{{$data->net_amount}}','{{$data->id}}',this.value);" type="text" name="dr_percentage[]" id="dr_percentage_{{$data->id}}" value="{{$data->percentage}}"/>
                        <span style="display:none;" class="bill_analysis_dr_percentage_label" id="bill_analysis_dr_percentage_label_{{$data->id}}">{{$data->percentage}}</span>

                    </td>
                    <td style="text-align:right;padding-left:5px;">
                        {{number_format(($data->percentage*$data->net_amount)/100,2)}}
                        @php
                            $final = ($data->percentage*$data->net_amount)/100;
                        @endphp
                    </td>
                </tr>
                @php
                    $total_amt += $data->net_amount;
                    $total_percentage += ($data->net_amount*$data->percentage)/100;
                    $total_final += $final;
                    $final = 0;
                    $i++;
                @endphp
            @endforeach
            <tr>
                <td colspan="2"><b>Total</b></td>
                <td style="text-align:right;padding-left:5px;"><b>{{number_format($total_amt,2)}}</b></td>
                <td></td>
                <td style="text-align:right;padding-left:5px;"><b>{{number_format($total_final,2)}}</b></td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<input type="hidden" name="total_amt" id="total_amt" value="{{$total_amt}}"/>
<input type="hidden" name="total_percentage" id="total_percentage" value="{{$total_percentage}}"/>
<input type="hidden" name="salary_id" id="salary_id" value="{{$salary_id}}"/>

