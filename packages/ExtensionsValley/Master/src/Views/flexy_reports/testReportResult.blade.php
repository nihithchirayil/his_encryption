<p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
        {{--  <p style="font-size: 12px;">Total Count<b> : {{ sizeof($result_data) }}</b></p>  --}}


        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> {{$report_name}} </b></h4>

<div class="col-md-12" style="margin-bottom:20px;">
    @if($status ==1)
        @if(sizeof($result_data)>0)
        <div class="theadscroll ps-container ps-theme-default" style="position: relative;overflow-x: auto !important;">
            <table class="table table-condensed table_sm table-col-bordered" style="border: 1px solid #CCC;">
                <thead>
                    <tr>
                        @php
                            $head_array = $result_data[0];
                        @endphp
                            <th class="header_bg">#</th>
                        @foreach($head_array as $key => $value)
                            @php
                                $key =  str_replace("_"," ",$key);
                                $key = ucwords($key);
                            @endphp
                            <th class="header_bg">{{$key}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach($result_data as $key => $value)
                        <tr>
                            <td>{{$i}}</td>
                            @foreach($value as $key1 => $value1)
                                <td style="text-align:left">{{$value1}}</td>
                            @endforeach
                        </tr>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
        @else
        <div class="col-md-12"><h5 style="color:red;">No Results Found!</h5></div>
        @endif
    @else
    <div class="alert alert-danger">
        {{$result_data}}
    </div>
    @endif
