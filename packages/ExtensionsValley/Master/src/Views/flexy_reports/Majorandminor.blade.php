-@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">

    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<style>
    .header_bg{
        background-color: #02967f;
        color:white;
    }
    a{color:black !important;}
    li>a{
      color:#E7E7E7 !important;
    }
    ul{padding-left:5px !important;font-weight: 200 !important;}
    li>a:hover{color:black !important;}
    .table_name{color:rgb(114, 4, 4) !important;}
    .gradient_bg{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
        color:white !important;
    }
    .bottom-border-text{
    border:none !important;
    border-bottom:1px solid lightgrey !important;
    box-shadow:none;
    }
    label{
        color:cornflowerblue !important;
    }

    .select2-selection__choice{
        background-color: #e6f1e8 !important;
        border: 1px solid #dbd8d8 !important;
        font-size: 10px !important;
    }
    .mate-input-box{
        height:51px !important;
    }

    .mate-input-box>text{
        margin-top:3px !important;
    }
    .select2-selection--multiple{
        padding-bottom:0px !important;
    }
    .table_header_bg{
        height: 30px !important;
        text-align:right !important;
        padding-right :40px !important;
        margin-bottom : 6px !important;
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .table_header_bg>h5{
        margin-top:7px;
        font-weight: 700;
    }
    .ajaxSearchBox{
        z-index: 9999 !important;
    }
    .ajaxSearchBox>li{
        font-size:13px !important;
        font-family: Arial !important;

    }
    .liHover{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .hospital_header>table{
        .border-bottom:1px solid #a5a4a4;
    }

    .td_common_numeric_rules{
        border-left: solid 1px #bbd2bd !important;
        text-align: right !important;
    }
    .common_td_rules{
        text-align: left !important;
        overflow: hidden !important;
        border-right: solid 1px #bbd2bd !important;
        border-left: solid 1px #bbd2bd !important;
        max-width: 100px;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
    .ajaxSearchBox{
        display: none;
        width: 250px !important;
        height: 400px !important;
        overflow: hidden;
        padding-top: 0px !important;
        margin-top: 15px !important;
    }

    @media print {

        .theadscroll {max-height: none; overflow: visible;}
        #ResultsViewArea{width:50% !important;}
        table{
            display: none !important;
        }

      }

    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #efefef;
    }

</style>

@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" name="report_id" id="report_id" value="{{$report_id}}">
<input type="hidden" id="hospital_header" value="{{ $hospital_header }}">

<div class="right_col">
    <div class="row">
        <div class="col-md-12 box-body" style="min-height:120px !important;">
            <div class="col-md-12 no-padding table_header_bg">
                <h5>{{$menu_name}}</h5>
            </div>


            {!!Form::open(array('name'=>$form_name, 'method' => 'get', 'id'=> $form_name))!!}

            <div class="box-body" style="padding-bottom:15px;">

                <div class="col-xs-2 date_filter_div"><div class="mate-input-box"><label class="filter_label">From Date</label>
                        <input type="text" data-attr="date" name="from_date_from"value="{{date('M-d-Y')}}" class="form-control filters" placeholder="MMM-DD-YYYY" id="from_date" autocomplete="off"></div></div><div class="col-xs-2 date_filter_div"><div class="mate-input-box"><label class="filter_label">To Date</label>
                    <input type="text" data-attr="date" name="to_date_to"value="{{date('M-d-Y')}}" class="form-control filters" placeholder="MMM-DD-YYYY" id="to_date" autocomplete="off"></div></div><div class="col-xs-2 date_filter_div">
                    <div class="mate-input-box">
                        <label class="filter_label ">Doctor Name Select Box</label><select name="doctor_name" class="form-control select2 filters" id="doctor_name" style="color:#555555; padding:4px 12px;"><option value=""> select- </option><option value="38">ABDUL  AZEEZ.V.K </option><option value="188">ABDUL RAHIMAN K M  </option><option value="167">ABILA  ALFRED</option><option value="2440">ACCESS LABS </option><option value="2417">ADITHYA  KUMAR</option><option value="267">AIZEL  SHERIEF P</option><option value="2385">AMAL  MOHAMMED</option><option value="283">AMITHA  </option><option value="2427">ANAND  A </option><option value="211">ANESTHESIA  </option><option value="2442">ANJANA  </option><option value="2388">ANOOP R PRASAD</option><option value="10">ANTONY   JOSEPHTHOPPIL </option><option value="74">ARJUN  V DEV</option><option value="123">ARUN  C</option><option value="2405">ARUN  GANGADHAR</option><option value="245">ARUN  KRISHNA A.K</option><option value="260">ARUN.S.NAIR  </option><option value="2439">ARYA  </option><option value="93">ASHA  KISHORE</option><option value="237">ASHA  KRISHNA</option><option value="2400">ASWIN  KUMAR K</option><option value="2401">ASWIN KUMAR  K</option><option value="2415">AUSTIN RAJ   R.S</option><option value="210">BALU  MOHAN</option><option value="203">BASHEER.N.K  </option><option value="269">BERDY  JOSE</option><option value="2495">BINDHU</option><option value="2410">BINEESH BALAKRISHNAN  </option><option value="2432">BINSTON THOMAS </option><option value="2425">BOOKING FILE </option><option value="2424">BOOKING (RT PCR-DDRC) </option><option value="60">BRAHMAPUTHRAN  </option><option value="2423">BRIJESH  </option><option value="2426">CARDIAC THORASIC SURGEON 1 </option><option value="2444">CIJU RAVEENDRANATH </option><option value="2445">COVID HOME CARE  </option><option value="2443">COVID VACCINE  REGISTRATION </option><option value="81">DARWIN  D THERATTIL  </option><option value="278">DATA  SCAN</option><option value="2496">DEEPTHI SATHI</option><option value="2454">DEMODOC</option><option value="286">DENTAL(MEDICAL  CAMP)</option><option value="61">DIETITIAN  </option><option value="222">DR PRAVEEN CHANDRA K  </option><option value="2494">DR RAJAN V K</option><option value="34">dummy  dummy</option><option value="225">E  DIVAKARAN</option><option value="96">EMERGENCY  </option><option value="111">EMERGENCY MEDICINE &  TRAUMA CARE</option><option value="2394">EMIL JOSEPH  </option><option value="2396">EXTERNAL  </option><option value="185">FIROSH  KHAN</option><option value="2447">FRANCIS ABEL  </option><option value="12">GENERAL MEDICINE  </option><option value="276">GENERAL  PATIENT</option><option value="2402">GEORGE JOSE VALOORAN</option><option value="288">GINO  P.D</option><option value="62">G.  SURGERY</option><option value="2421">GYNAECOLOGY(DR LEELAMANI)  </option><option value="251">HALEEMA  BEEGUM T.R</option><option value="18">HARISH  VALSAN </option><option value="50">HEALTH  CHECK UP </option><option value="2387">HEALTH PACKAGE DOCTOR HEALTH PACKAGE DOCTOR</option><option value="227">INTERNAL  MEDICINE A </option><option value="228">INTERNAL  MEDICINE B </option><option value="229">INTERNAL  MEDICINE C </option><option value="130">INTERNAL MEDICINE &  CRITICAL CARE DEPT</option><option value="232">INTERNAL  MEDICINE D </option><option value="253">INTERNAL  MEDICINE E </option><option value="121">IQBAL  P T</option><option value="2438">JAIDEEP  </option><option value="184">JAIN  CHIMMEN</option><option value="164">JEFF JAMES CHAKOLA</option><option value="2413">JOSEPH K.G  </option><option value="169">JOSEPH.  P U  </option><option value="2411">JOSEPH  VARGHEESE</option><option value="2455">KARKINOS</option><option value="133">K. A. VARGHESE  </option><option value="275">KAVYA  P</option><option value="2398">KIRAN   THILAKARAJ</option><option value="2412">KRISHNAKUMAR  </option><option value="2409">KRISHNAPRASAD  </option><option value="243">K U SHEPHY  </option><option value="146">LEELAMANI  A</option><option value="281">LISHA  P V</option><option value="2451">MAHESH KRISHNA</option><option value="8">MALLIKA  K</option><option value="97">MEERA  RAVEENDRAN</option><option value="129">MITHUN CHACKO JOHN</option><option value="238">MOBIN PAUL</option><option value="262">MOHAN  K.B</option><option value="69">MS  RAJAN  </option><option value="270">MUKESH  MUKUNDAN</option><option value="289">MUKESH  (PAIN&PALLIATIVE)</option><option value="2403">MUKESH PAIN&PALLIATIVE </option><option value="138">MUKUNDAN  </option><option value="143">M  V RAMAN  </option><option value="165">NARAYANAN  UNNI</option><option value="214">NAZIYA MOHAMMED </option><option value="39">NEENA P </option><option value="2392">NITHIN SELVAKUMAR</option><option value="105">OUTSIDE DOCTOR  </option><option value="20">P.A.ABDUL  AZEEZ</option><option value="291">PAIN  INTERVENTION(DR MUKESH)</option><option value="48">PAUL  JOHNY</option><option value="140">PHYSIOTHERAPY  </option><option value="2434">POST COVID CLINIC </option><option value="2448">POST DISCHARGE  HOME CARE </option><option value="2497">PREETHI RAJ</option><option value="2407">PREMKUMAR  </option><option value="181">PRIYADARSHINI  K</option><option value="2391">P.S SHAREEK  .</option><option value="2414">RAFEEQ A.K  </option><option value="250">RAHMAN  KHAN A</option><option value="2428">RAJI  BENJAMIN </option><option value="194">RAKEESH  P</option><option value="153">RAVEENDRAN  T. K.</option><option value="261">RAVIRAM  .</option><option value="115">REJITH  R S</option><option value="279">REMA  MANOHAR</option><option value="2450">RINU ROSE</option><option value="56">ROHINI K P  </option><option value="2437">ROY ABRAHAM  </option><option value="57">ROY  VARGHESE  </option><option value="271">SACHIN  S</option><option value="284">SAHEER  N</option><option value="272">SAJAN  KOSHI</option><option value="2419">SAJAN SEBASTIAN  </option><option value="30">SAJEER  K SIDDIK</option><option value="2418">SAJIN  </option><option value="113">SAJITH  PRASAD</option><option value="219">SANTHOSH  BABU MR</option><option value="266">Satish  Haridasan</option><option value="2422">SHYAMNATH G  </option><option value="186">SIMON  MATHEW</option><option value="2397">SITHARA  </option><option value="2433">SPACY POULOSE AMBOOKKEN</option><option value="244">SREEJITH  V ERATTE</option><option value="2395">SREEKANTH  </option><option value="2399">SREENIVASAN VAZHOOR RAMSING</option><option value="2429">SUJA BOBY </option><option value="2452">SUJEI SUKUMARAN</option><option value="285">SURGICAL&CLINICAL  ONCOLOGY</option><option value="241">SURGICAL  GASTROENTROLOGY</option><option value="190">T C THILAKAN</option><option value="2446">TELECONSULTATION  </option><option value="2493">test</option><option value="2463">test doc</option><option value="2453">TEST DOC</option><option value="2487">testdoc1</option><option value="2465">test doc 2</option><option value="2462">testdocs</option><option value="2460">testdoctor</option><option value="2459">testdoctor</option><option value="2464">test doctor</option><option value="2461">testdoctor2</option><option value="2476">test doctor4</option><option value="2492">test_doctor_name</option><option value="2466">TEST DOCTORS</option><option value="2441">TINTU  </option><option value="221">TV  RAJAN</option><option value="159">ULLAS  C S</option><option value="6">VARGHESE K A  </option><option value="255">VENUGOPAL  M</option><option value="280">VIDYA  KISHORE(CLINICAL PSYCOLOGIST)</option><option value="2449">VIJAY  THOMAS CHERIAN</option><option value="2436">VINEETH KUMAR  </option><option value="2431">VIVEK  VISWAMBHARAN</option><option value="80">YOGANADAN NAMBOODIRI M N</option></select> </div> </div><div class="col-xs-2 date_filter_div">
                    <div class="mate-input-box">
                        <label class="filter_label ">Department Select Box</label><select name="department" class="form-control select2 filters" id="department" style="color:#555555; padding:4px 12px;"><option value=""> select- </option><option value="31">A</option><option value="29">ADVANCE</option><option value="162">ANAESTASIA FOR ENDOSCOPY</option><option value="56">ANAESTHESIA</option><option value="203">ANGIOGRAM</option><option value="142">ANGIOGRAM CD CHARGE</option><option value="201">ANGIOPLASTY BILL - MULTI</option><option value="202">ANGIOPLASTY BILL - SINGLE</option><option value="229">ASSISTANT SURGEON CHARGE</option><option value="23">AUDIOLOGY</option><option value="209">AVM CATHLAB</option><option value="235">C</option><option value="193">CARDIAC TMT ELECTRODE</option><option value="24">CARDIOLOGY</option><option value="165">CARDIOTHERASIC(MAJOR)</option><option value="166">CARDIOTHERASIC(MINOR)</option><option value="55">CARDIO THORASIC</option><option value="144">C ARM BILL</option><option value="6">CASUALITY</option><option value="36">CASUALTY - EMERGENCY BILL</option><option value="18">CATH LAB</option><option value="8">CHEMO THERAPY</option><option value="175">COLONOSCOPY</option><option value="184">COLONOSCOPY REFERRAL</option><option value="224">COVID DIETARY</option><option value="217">COVID VACCINATION</option><option value="232">CSSD(PLASMA STERILIZATION)</option><option value="210">CT/MRI</option><option value="4">DENTAL</option><option value="238">DENTALDEP</option><option value="148">DENTAL SURGERY (MAJOR)</option><option value="149">DENTAL SURGERY (MINOR)</option><option value="247">DEP</option><option value="221">DEPART NITHIN</option><option value="1">DERMATOLOGY</option><option value="26">DIALYSIS</option><option value="12">DIETARY</option><option value="42">DISCHARGE PROCEDURES</option><option value="188">DR.PREMKUMAR ARTHROSCOPY</option><option value="189">DR.PREMKUMAR GENERAL</option><option value="187">DR.PREMKUMAR KNEE</option><option value="155">DR.ROY PACKAGES</option><option value="15">EEG</option><option value="139">ENDOSCOPY BILL</option><option value="172">ENDOSCOPY BILL(MEDICAL GASTROENTEROLOGY)</option><option value="154">ENDOSCOPY (DR.SREEKUMAR)</option><option value="22">ENT</option><option value="181">ENT SURGERY - DR REJITH ( MAJOR )</option><option value="180">ENT SURGERY - DR REJITH ( MINOR )</option><option value="134">ENT SURGERY ( MAJOR )</option><option value="135">ENT SURGERY ( MINOR )</option><option value="163">EPIDURAL ANAESTASIA</option><option value="177">ERCP</option><option value="178">EUS</option><option value="186">EUS REFERRAL</option><option value="216">FAMILY MEDICINE</option><option value="200">FNAC</option><option value="157">GASTROENTEROLOGY</option><option value="28">GASTROENTEROLOGY</option><option value="173">GASTROENTEROLOGY MAJOR</option><option value="174">GASTROENTEROLOGY MINOR</option><option value="197">GASTRO ICU</option><option value="49">GENERAL MEDCNE</option><option value="16">GENERAL SURGERY</option><option value="124">GENERAL SURGERY ( MAJOR )</option><option value="204">GENERAL SURGERY  MAJOR ( T.C THILAKAN )</option><option value="125">GENERAL SURGERY ( MINOR )</option><option value="205">GENERAL SURGERY  MINOR ( T.C THILAKAN )</option><option value="126">GYNAECOLOGY  ( MAJOR )</option><option value="127">GYNAECOLOGY  ( MINOR )</option><option value="54">HAEMATOLOGY</option><option value="153">HARMONIC MAJOR</option><option value="195">HDU BILL</option><option value="44">HEALTH PACKAGE DOCTOR</option><option value="225">HOSPITAL</option><option value="35">ICU & OTHER SERVICES</option><option value="39">IntelMed Technology</option><option value="45">INTERNAL MEDCNE</option><option value="244">IP</option><option value="37">JEEVA LAB</option><option value="19">LAB</option><option value="7">LABOUR ROOM PROCEDURES</option><option value="38">LAB REAGENT</option><option value="191">LIFE CELL</option><option value="9">MAMOGRAM</option><option value="10">MANOMETRY</option><option value="179">MANOMETRY</option><option value="194">MDCCU BILL</option><option value="0">MISC</option><option value="48">NEPHROLOGY</option><option value="47">NEUROLOGY</option><option value="17">NEURO REHABILITATION</option><option value="46">NEURO SURGERY</option><option value="140">NEURO SURGERY (MAJOR)</option><option value="141">NEURO SURGERY (MINOR)</option><option value="241">NEW DEP</option><option value="222">NO DEPARTMENT</option><option value="51">ONCOLOGY</option><option value="171">OPHTHALMOLOGY (MAJOR)</option><option value="147">OPHTHALMOLOGY (MINOR)</option><option value="52">OPTHALMOLOGY</option><option value="158">ORTHO DR AIZEL SHEREEF (MAJOR)</option><option value="159">ORTHO DR AIZEL SHEREEF (MINOR)</option><option value="130">ORTHO PAEDIC ( MAJOR )</option><option value="131">ORTHO PAEDIC ( MINOR )</option><option value="21">ORTHOPAEDIC PROCEDURES</option><option value="227">ORTHO PAEDIC SUJEI ( MAJOR )</option><option value="226">ORTHO PAEDIC SUJEI ( MINOR )</option><option value="30">OT</option><option value="152">OT CATHLAB</option><option value="206">OTHER HEALTH CARE INCOME</option><option value="215">OTHER HEALTH CARE INCOME(ECHS)</option><option value="214">OTHER HEALTH CARE INCOME (ESI)</option><option value="212">OTHER HEALTHCARE INCOME(KAS)</option><option value="34">OTHER SERVICE</option><option value="138">OT INSTRUMENT BILL</option><option value="50">PAEDIATRIC</option><option value="218">PAEDIATRIC NEURO DEVELOPMENTAL REHABILITAION</option><option value="211">PAIN&PALLIATIVE</option><option value="199">PATH CENTRE</option><option value="192">PATHOLOGY</option><option value="168">PERICARDIAL TAPPING</option><option value="32">PET SCAN(ASTER)</option><option value="43">PHARMACY</option><option value="207">PHYSICAL MEDICINE</option><option value="57">PLASTIC AND RECONSTRUCTIVE</option><option value="169">PLASTIC SURGERY DR.RAJESH(MAJOR)</option><option value="170">PLASTIC SURGERY DR.RAJESH(MINOR)</option><option value="146">PREPARATION BILL</option><option value="3">PROCEDURE CHARGES</option><option value="5">PSYCHIATRY BILL</option><option value="11">PSYCHOTERAPY</option><option value="2">PULMONOLOGY</option><option value="14">RADIOLOGY</option><option value="145">RECONSTRUCTION SURGERY DR.M.V.RAMAN[MAJOR]</option><option value="156">RECONSTRUCTION SURGERY DR.M.V.RAMAN[MINOR]</option><option value="167">RECOVERY CHARGE</option><option value="25">REGISTRATION CHARGES</option><option value="27">REST OF BILLS</option><option value="41">ROOM RENT TYPE</option><option value="208">SPEECH THERAPY PROCEDURES</option><option value="137">S. PROCEDURE OT( CYSTOSCOPY )</option><option value="136">S. PROCEDURE OT(STENT REMOVEL)</option><option value="196">STEPDOWN ICU</option><option value="164">STERRAD AND OTHERS BILL</option><option value="143">SURGERY BILL</option><option value="223">SURGICAL CRITICAL CARE</option><option value="220">SURGICAL CRITICAL CARE</option><option value="33">TELEMEDICINE</option><option value="250">TEST DEPART</option><option value="40">THYROCARE</option><option value="150">TPI (CATH LAB)</option><option value="213">TRANSPORTATION CHARGE(COVID)</option><option value="176">UPPER GASTROINTESTINAL ENDOSCOPY</option><option value="185">UPPER GASTROINTESTINAL ENDOSCOPY REFERRAL</option><option value="53">UROLOGY</option><option value="128">UROLOGY ( MAJOR )</option><option value="129">UROLOGY ( MINOR )</option><option value="219">VACCINATION ADLUX&DAYA</option><option value="160">VASCULAR MAJOR DR.ROY VARGHESE</option><option value="161">VASCULAR MINOR DR.ROY VARGHESE</option><option value="132">VASCULAR SURGERY ( MAJOR ) DR VINEETH</option><option value="133">VASCULAR SURGERY ( MINOR ) DR VINEETH</option><option value="13">VASCULAR SURGERY - PROCEDURES</option><option value="151">VENOGRAM CATHLAB</option><option value="20">VERTIGO CLINIC</option><option value="198">X-RAY BILL</option></select> </div> </div>
                 <div class="clearfix"></div>
                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <div class="clearfix"></div>

                    <button type="button" class="btn bg-green btn-block" style="color:white !important;width:101px !important;" onclick="getResultData();" name="search_results" id="search_results">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        Search
                    </button>

                </div>
                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">

                    <button type="reset" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="" id="">
                        <i class="fa fa-repeat" aria-hidden="true"></i>
                        Reset
                    </button>

                </div>


                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="button" onclick="printReportData();" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="print_results" id="print_results">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>

                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="button" onclick="exceller()" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="csv_results" id="csv_results">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel
                    </button>
                </div>



            </div>
            {!! Form::token() !!} {!! Form::close() !!}
        </div>

            <div class="col-md-12 no-padding">
                <div id="ResultDataContainer"
                    style=" padding: 10px; display:none;font-family:poppinsregular;">
                    <style>
                           @media print {
                                .theadscroll {max-height: none; overflow: visible;}


                            }
                    </style>
                    <div style="float: left; width: 100% !important;" id="ResultsViewArea">


                    </div>
                </div>
            </div>

    </div>
</div>
@include('Master::flexy.report_print_modal')



@stop
@section('javascript_extra')

<script type="text/javascript">
    $(document).ready(function () {



//fixed header script ends here
        setTimeout(function () {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        }, 300);
        $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
        $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
        $("input[data-attr='datetime']").datetimepicker({ format: 'MMM-DD-YYYY hh:mm:A' });
    });

    function getResultData(){
        var form_name = '{{$form_name}}';
        //alert(form_name);return;
        var form_data = $('#'+form_name).serialize();
        var base_url = $('#base_url').val();
        //var url = '{{$dataurl}}';
        var url = base_url+'/flexy_report/'+'{{$report_name}}';


        var filters_list = new Object();
        var filters_value = '';
        var filters_id = '';


        $('.filters').each(function () {
            filters_id = this.id;

            filters_value = $('#' + filters_id).val();
            filters_id = filters_id.replace('_hidden', '');

            if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
                filters_list[filters_id] = filters_value;
            }
        });



        $.ajax({
            url: url,
            type: 'POST',
            data: filters_list,
            beforeSend: function () {
                $('#ResultDataContainer').show();

                $('#ResultDataContainer').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('.ResultDataContainerBody').show();
                $('#ResultsViewArea').html(data);

            },
            complete: function () {
                $('#ResultDataContainer').LoadingOverlay("hide");

                $('.theadscroll').perfectScrollbar({
                minScrollbarLength: 30
                });

                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table){
                        return $table.closest('.theadscroll');
                    }
                });


            }
        });
    }




    //----Hidden Filed Search--------------------------------------------------------------------

    $('.hidden_search').keyup(function (event) {
        var input_id = '';
        var base_url = $('#base_url').val();
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        input_id = $(this).attr('id');

        var current;
        if (value.match(keycheck) || event.keyCode == '8') {
            if ($('#' + input_id + '_hidden').val() != "") {
                $('#' + input_id + '_hidden').val('');
            }
            var search_key = $(this).val();
            search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
            search_key = search_key.trim();

            var department_hidden = $('#department_hidden').val();
            var datastring = '';
            if (input_id == 'sub_department') {
                datastring = '&department_id=' + department_hidden;
            }
            if (input_id == 'scheme') {
                datastring = '&company_id=' + company_hidden;
            }

            if (search_key == "") {
                $("#" + input_id + "AjaxDiv").html("");
            } else {
                var url = base_url+'/flexy/FlexyReportProgressiveSearch';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                    beforeSend: function () {

                        $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {

                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {

                    },
                    error: function () {
                        bootbox.alert('nw error');
                        return;
                    }

                });
            }
        } else {
            ajax_list_key_down(input_id + 'AjaxDiv', event);
        }
    });

    function fillSearchDetials(id, name, serach_key_id) {
        $('#' + serach_key_id + '_hidden').val(id);
        $('#' + serach_key_id).val(name);
        $('#' + serach_key_id).attr('title', name);
        $("#" + serach_key_id + "AjaxDiv").hide();
    }


    /* setting for enter key press in ajaxDiv listing */
    $(".hidden_search").on('keydown', function (event) {
        var input_id = '';
        input_id = $(this).attr('id');
        if (event.keyCode === 13) {
            ajaxlistenter(input_id + 'AjaxDiv');
            return false;
        }
    });


$("#category").on('change', function() {
    var category = $('#category').val();
    if (category == '') {
      var category = 'a';

    }

    if(category) {
        $.ajax({
            type: "GET",
            url: '',
            data: 'category=' + category,
            beforeSend: function () {
                $('#s2id_sub_category').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                $( "#sub_category" ).prop( "disabled", true ); //Disable
            },
            success: function (html) {
                if(html){
                    $("#sub_category").empty();
                    $("#sub_category").html('<option value="">Select Sub catogery</option>');
                    $.each(html,function(key,value){
                        $("#sub_category").append('<option value="'+key+'">'+value+'</option>');
                    });
                }else{
                    $("#sub_category").empty();
                }
            },

            error: function () {
                Command: toastr["error"]("Please check your internet connection and try again!");
                $( "#sub_category" ).prop( "disabled", false );
            },
            complete: function () {
                $('#warning1').remove();
                $( "#sub_category" ).prop( "disabled", false );
            }

        });

    }else{
            $("#category").focus();
            $("#sub_category").empty();
        }

    });


    //---------generate csv---------------
function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function exceller(type, fn, dl) {
    var type = 'xlsx';
    var elt = document.getElementById('ResultsViewArea');
    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
    return dl ?
        XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
        XLSX.writeFile(wb, fn || ('excel_data.' + (type || type)));
}

//---------generate csv ends---------


//---------print report--------
function printReportData() {
    $('#print_config_modal').modal('toggle');



}

function print_generate(printData) {
    $('#resultDataTable').css('width', '98.5%');
    $(".theadfix_wrapper").floatThead('destroy');
    $('.theadscroll').perfectScrollbar('destroy');

    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    if ($('#showTitle').is(":checked")) {
        printhead = $("#hospital_header").val();
        showw = showw + atob(printhead);
    }


    var mywindow = window.open('', 'my div', 'height=3508,width=2480');

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;


    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait;margin:20px;text-align:center;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape;margin:20px;text-align:center;}</style>');
    }
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    setTimeout(function(){
        mywindow.print();
        mywindow.close();
    }, 1000);


    return true;
}






</script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>



@endsection
