
<style>
    @media print {
        .theadscroll {max-height: none; overflow: visible;}
        .header_bg {
            background-color: #08898d;
            color: white;
        }
        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #f2fef5;
        }

        #resultDataTable{
            border: 1px solid #cedfe9 !important;
            border-collapse: collapse !important;
        }
        td{
            padding-left: 5px !important;
        }
        }
    .global-footer {
        position: fixed;
        width: 90.5%;
        bottom: 38px;
        /* left: 0; */
        /* padding: 1em */
    }
</style>

<div class="col-md-12" style="margin-bottom:15px;">
    @if($status == 1)
        @if(sizeof($result_data)>0)
        <div id="ResultsPrintViewArea">
            @php
                                $head_array = $result_data[0];
                                $total_colums = explode(",", $flexy_report_totals);
                                $total_array = [];
                                $total_array_index = [];
                                $drill_down_array = [];
                                $size_array = [];
                                $j = 0;

                                $head_array_size = count(array_keys((array)$head_array));
                                $custom_column_width = explode(',', $custom_column_width);
                                if ($drill_down_columns) {
                                    $drill_down_columns = str_replace("@","",$drill_down_columns);
                                    $drill_down_columns = explode(',', $drill_down_columns);
                                }
                                $drill_down_values = '';
                                $numeric_colums = explode(',', $numeric_colums);



                            @endphp
            <span class="no-padding" id="excelContainer">
            <div class="theadscroll no-padding" id="table_container_div" style="position: relative; max-height: 400px;">
                <table border='0'; width="100%;" class=" no-padding" style="border-collapse: collapse;table-layout:fixed;border-color:white;">
                    <tr class="table_header_row">
                        <th style="text-align:left;font-weight:100 !important;" colspan="{{$head_array_size+1}}">
                            Report Print Date: {{date('M-d-Y h:i A')}}
                        </th>
                    </tr>
                    <tr class="table_header_row">
                        <th style="text-align:left;font-weight:100 !important;" colspan="{{$head_array_size+1}}">
                            Report Date: {{$from_date}} To {{$to_date}}
                        </th>
                    </tr>
                    <tr class="table_header_row">
                        <th style="text-align:left;font-weight:100 !important;" colspan="{{$head_array_size+1}}">
                            Number Of Records: {{count($result_data)}}
                        </th>
                    </tr>
                    @php
                        $company_details = \DB::table('company')->where('id',1)->first();
                    @endphp
                    <tr class="hospital_header_excel" style="display:none;">
                        <td style="text-align: center !important;" colspan="{{$head_array_size+1}}">
                            <span><strong>{{$company_details->name}}</strong></span><br>
                            <span>{{$company_details->address}}</span><br>
                            <span>Phone :{{$company_details->phone}}</span>
                            <span>Booking No :{{$company_details->booking_no}}</span>
                        </td>
                    </tr>
                    <tr class="table_header_row no-padding"  style="background-color: #07ad8c;color: white;">
                        <th style="font-size: 18px !important;text-align:center;" colspan="{{$head_array_size+1}}">
                            {{$menu_name}}
                        </th>
                    </tr>
                </table>
                <table id="resultDataTable" border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;">

                    <thead>
                        <tr style="background-color:#07ad8c; color: white;">
                                <th style="width:5%">Sl.No</th>
                            @foreach($head_array as $key => $value)
                            @if($key != 'speciality')
                                @php
                                    if(in_array($key, $total_colums)){
                                        $total_array[$key] = 0;
                                        $total_array_index[$key] = $j;
                                    }
                                    $key =  str_replace("_"," ",$key);
                                    if($key=='uhid'){
                                        $key = strtoupper($key);
                                    }
                                    else {
                                        $key = ucwords($key);
                                    }
                                    $j++;
                                @endphp

                                @if(substr($key, 0, 1) != '@') <!--- if first letter is not @  then only show-->
                                    <th style="@if(isset($custom_column_width[$j-1]))width:{{$custom_column_width[$j-1]}}% @endif">{{$key}}</th>
                                @endif
                                @endif
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                            $result_data = collect($result_data)->groupBy('speciality');        
                        @endphp
                        @foreach($result_data as $key => $value)
                            @php
                                $drill_down_values = '';
                                if(!empty($drill_down_columns)){

                                    $value2 = (array) $value;
                                    $new_array = [];
                                    foreach($value2 as $drill_key=>$drill_value){

                                        $new_key = str_replace("@","",$drill_key);
                                        $new_value = str_replace("@","",$drill_value);

                                        if($new_key != $drill_key){
                                            $value2[$new_key] = $new_value;
                                            $new_array[$new_key] = $new_value;
                                        } else {
                                            $new_array[$drill_key] = $drill_value;
                                        }

                                    }
                                    if($has_drill_down ==1){
                                        foreach($drill_down_columns as $drill_value){
                                            $drill_down_values.= "'".$new_array[$drill_value]."',";
                                        }
                                        $drill_down_values = rtrim($drill_down_values, ",");
                                    }

                                }
                            @endphp


                            <?php $j = 1; ?>
                            <tr style="background-color: paleturquoise;"><td style="text-align:left;" colspan="6"><b>{{$key}}</b></td></tr>

                            @foreach($value as $key1 => $value1)
                            <tr @if($has_drill_down == 1) onclick="@@@REPORTNAME@@@@DrilldownData({{$drill_down_values}});" @endif>
                                <td>{{$j}}</td>
                                    <td class="common_td_rules">{{$value1->doctor}}</td>
                                    <td class="td_common_numeric_rules">{{$value1->registration}}</td>
                                    <td class="td_common_numeric_rules">{{$value1->renewal}}</td>
                                    <td class="td_common_numeric_rules">{{$value1->free_visit}}</td>
                                    <td class="td_common_numeric_rules">{{$value1->total}}</td>
                                    @php
                                    $total_array['registration'] += (float)$value1->registration;
                                    $total_array['renewal'] += (float)$value1->renewal;
                                    $total_array['free_visit'] += (float)$value1->free_visit;
                                    $total_array['total'] += (float)$value1->total;
                                    @endphp

                            @php
                                $j++;
                            @endphp
                            </tr>
                            @endforeach
                        @endforeach
                        </tr>

                        @if($flexy_report_totals !='')
                            <tr id="total_row" style="background-color: paleturquoise;display:none">
                                <td><b>Total</b></td>
                                @foreach ($head_array as $key3 => $val3)
                                @if($key3 != 'speciality')
                                    @if(in_array($key3,array_keys($total_array)))
                                        @if(substr($key3, 0, 1) != '@')
                                            <td class="td_common_numeric_rules"><b>{{number_format($total_array[$key3],2,'.','')}}</b></td>
                                        @endif
                                    @else
                                        @if(substr($key3, 0, 1) != '@')
                                            <td style="text-align:left"><b></b></td>
                                        @endif
                                    @endif
                                @endif
                                @endforeach
                            </tr>
                        @endif


                    </tbody>
                </table>
            </div>
            </span>
        </div>


            @if($flexy_report_totals !='')
                    <table border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper" style="border-collapse: collapse;table-layout:fixed;">
                        <tbody>
                            <tr style="background-color: paleturquoise;">
                                <td style="width:5%;"><b>Total</b></td>
                                @php $k=0; @endphp
                                @foreach ($head_array as $key3 => $val3)
                                @if($key3 != 'speciality')
                                    @if(in_array($key3,array_keys($total_array)))
                                        @if(substr($key3, 0, 1) != '@')

                                            <td style="width:{{$custom_column_width[$k]}}%"
                                            class="td_common_numeric_rules">
                                                <b>{{number_format($total_array[$key3],2,'.','')}}</b>

                                            </td>

                                        @endif
                                        @php $k++; @endphp
                                    @else
                                        @if(substr($key3, 0, 1) != '@')

                                            <td style="width:{{$custom_column_width[$k]}}%" style="text-align:left">
                                                <b></b>
                                            </td>

                                        @endif
                                        @php $k++; @endphp
                                    @endif
                                    @endif
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
                </div>
            @endif

        @else
        <div id="ResultsPrintViewArea" class="col-md-12"><h5 style="color:red;">No Results Found!</h5></div>
        @endif
    @else
    <div class="alert alert-danger">
        {{$result_data}}
    </div>
    @endif
</div>
