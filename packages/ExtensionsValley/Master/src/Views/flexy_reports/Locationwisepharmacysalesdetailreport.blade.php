<?php
use ExtensionsValley\Master\FlexyReportController;
?>
@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<style>
    .header_bg {
        background-color: #02967f;
        color: white;
    }

    a {
        color: black !important;
    }

    li>a {
        color: #E7E7E7 !important;
    }

    ul {
        padding-left: 5px !important;
        font-weight: 200 !important;
    }

    li>a:hover {
        color: black !important;
    }

    .table_name {
        color: rgb(114, 4, 4) !important;
    }

    .gradient_bg {
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
        color: white !important;
    }

    .bottom-border-text {
        border: none !important;
        border-bottom: 1px solid lightgrey !important;
        box-shadow: none;
    }

    label {
        color: cornflowerblue !important;
    }

    .select2-selection__choice {
        background-color: #e6f1e8 !important;
        border: 1px solid #dbd8d8 !important;
        font-size: 10px !important;
    }

    .mate-input-box {
        height: 51px !important;
    }

    .mate-input-box>text {
        margin-top: 3px !important;
    }

    .select2-selection--multiple {
        padding-bottom: 0px !important;
    }

    .table_header_bg {
        height: 30px !important;
        text-align: right !important;
        padding-right: 40px !important;
        margin-bottom: 6px !important;
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }

    .table_header_bg>h5 {
        margin-top: 7px;
        font-weight: 700;
    }

    .ajaxSearchBox {
        z-index: 9999 !important;
    }

    .ajaxSearchBox>li {
        font-size: 13px !important;
        font-family: Arial !important;

    }

    .liHover {
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }

    .hospital_header>table {
        .border-bottom: 1px solid #a5a4a4;
    }

    .td_common_numeric_rules {
        border-left: solid 1px #bbd2bd !important;
        text-align: right !important;
        font-size: 11px !important;
    }

    .common_td_rules {
        text-align: left !important;
        overflow: hidden !important;
        border-right: solid 1px #bbd2bd !important;
        border-left: solid 1px #bbd2bd !important;
        max-width: 100px;
        text-overflow: ellipsis;
        white-space: nowrap;
        font-size: 11px !important;
    }

    .ajaxSearchBox {
        display: none;
        width: 328px !important;
        height: 400px !important;
        overflow: hidden;
        padding-top: 0px !important;
        margin-top: 14px !important;
    }

    .table_sm th,
    .table_sm td {
        font-size: 11px !important;
    }

    .hospital_header_excel {
        background-color: #07ad8c;
        color: white;
    }

    @media print {

        .theadscroll {
            max-height: none;
            overflow: visible;
        }

        #ResultsViewArea {
            width: 50% !important;
        }

        table {
            display: none !important;
        }

    }

    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #efefef;
    }
</style>

@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="c_token" value="{{ csrf_token() }}">
<input type="hidden" name="report_id" id="report_id" value="{{ $report_id }}">
<input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
<input type="hidden" id="hospital_address" value="{{ $hospital_address }}">
<input type="hidden" id="exceller_data" value="<?= base64_encode('<html xmlns:o="
    urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
    xmlns="http://www.w3.org/TR/REC-html40">

<head>
    <!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->
</head>

<body>
    <table>{table}</table>
</body>

</html>') ?>">

<div class="right_col">
    <div class="row">
        <div class="col-md-12 box-body" style="min-height:120px !important;">
            <div class="col-md-12 no-padding table_header_bg">
                <h5>{{ $menu_name }}</h5>
            </div>


            {!! Form::open(['name' => $form_name, 'method' => 'get', 'id' => $form_name]) !!}

            <div class="box-body" style="padding-bottom:15px;">
                <div class="col-md-9" style="padding-top:16px">
                    <div class="col-md-2 date_filter_div">
                        <div class="mate-input-box"><label class="filter_label">From Date</label>
                            <input type="text" data-attr="date" name="from_date_from" value="{{ date('M-d-Y') }}"
                                class="form-control filters" placeholder="MMM-DD-YYYY" id="from_date"
                                autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-2 date_filter_div">
                        <div class="mate-input-box"><label class="filter_label">To Date</label>
                            <input type="text" data-attr="date" name="to_date_to" value="{{ date('M-d-Y') }}"
                                class="form-control filters" placeholder="MMM-DD-YYYY" id="to_date" autocomplete="off">
                        </div>
                    </div>@php $fieldName = FlexyReportController::flexyReportMasterClassData('location_code');@endphp
                    <div class="col-md-3 date_filter_div">
                        <div class='mate-input-box'><label class='filter_label'>Location</label><select
                                class='form-control select2 bottom-border-text filters'
                                style='color:#555555; padding:4px 12px;' name='location' id='location'>
                                <option value='All'>All</option>
                                @foreach ($fieldName as $key => $val)
                                <option value='{{ $key }}'>{{ $val }}</option>
                                @endforeach
                            </select></div>
                    </div>
                    <div class="col-md-2">
                        <div class="mate-input-box">
                            <label class="filter_label">Payment Type</label>
                            <select class="form-control select2 filters" multiple id="payment_type" name="payment_type">
                                <option value="">Select</option>
                                @foreach ($payment_type as $each)
                                <option value="{{ $each->code }}">{{ $each->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="mate-input-box" style="height:55px !important;overflow-y: scroll;">
                            <label class="filter_label">Item Desc.</label>
                            <div class="clearfix"></div>
                            <!-- <input type="text" autocomplete="off" class="form-control" id="item_desc_search">
                            <div id="itemDescSearchBox" class="ajaxSearchBox"></div>
                            <input type="hidden" class="filters" id="item_id"> -->
                            <select multiple='multiple-select' class='form-control select2 bottom-border-text item_filter' style='width:100%;color:#555555; padding:2px 10px;' name='item_select[]' id='item_select'><option value='All'>All</option> 
                            @foreach($item_list as $key=>$val)
                            <option value='{{$val->item_id}}'>{{$val->item_desc}}</option> 
                            @endforeach
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3" style="padding-top:26px">
                    <div class="col-md-2" style="padding-top:16px">
                        <div class="checkbox checkbox-success inline no-margin">
                            <input checked type="checkbox" id="paid_bills_only" name="paid_bills_only">
                            <label class="black" style="padding-left:2px;color: #000 !important" for="paid_bills_only">
                                <strong>Paid</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-5" style="padding-top:16px">
                        <div class="checkbox checkbox-success inline no-margin">
                            <input type="checkbox" id="unpaid_bills_only" name="unpaid_bills_only">
                            <label class="black" style="padding-left:2px;color: #000 !important"
                                for="unpaid_bills_only">
                                <strong>Unpaid/Not settled</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-5" style="padding-top:16px">
                        <div class="checkbox checkbox-success inline no-margin">
                            <input type="checkbox" id="settled_bills_only" name="settled_bills_only">
                            <label class="black" style="padding-left:2px;color: #000 !important"
                                for="settled_bills_only">
                                <strong>Settled/Partially Settled</strong>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-top:16px">
                <div class="col-md-3" style="margin-left: 10px;">
                    <div class="mate-input-box">
                        <label class="filter_label">UHID</label>
                        <input class="form-control hidden_search" value="" autocomplete="off" type="text"  autocomplete="off" id="patient_uhid" name="patient_uhid" />
                        <div id="patient_uhidAjaxDiv" class="ajaxSearchBox"></div>
                        <input class="filters" type="hidden" name="patient_uhid_hidden" value="" id="patient_uhid_hidden"></div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="mate-input-box">
                        <label class="filter_label">Doctor</label>
                        <div class="clearfix"></div>
                        <select class="form-control select2" style='color:#555555;' name="doctor_name_search" id="doctor_name_search">
                            <option value='All'>Doctor</option>
                            @foreach ($doctor as $each)
                    <option value="{{ $each->id }}">{{ $each->doctor_name }}</option>
                    @endforeach
                    </select>
                    </div>

                </div>
                <div class="col-md-1 padding_sm">
                    <div class="mate-input-box">
                        <label class='filter_label'>Visit Status</label>
                        <select name='visit_type' id='visit_type' class="form-control">
                            <option selected value='ALL'>ALL</option>
                            <option value='IP'>IP</option>
                            <option value='OP'>OP</option>
                        </select>
                    </div>
                </div>
                @php
                    $schedule_types = [
                        'ALL' => 'ALL',
                        'General' => 'General',
                        'Schedule X' => 'Schedule X',
                        'Schedule H1' => 'Schedule H1',
                        'Schedule H' => 'Schedule H',
                        'Schedule B' => 'Schedule B',
                        'Schedule A' => 'Schedule A',
                    ];
                @endphp
                <div class="col-md-2 padding_sm">
                    <div class="mate-input-box">
                        <label class='filter_label'>Schedule</label>
                        {!! Form::select('schedule', $schedule_types, null, [
                            'class' => 'form-control select2',
                            'id' => 'schedule',
                            ]) !!}
                    </div>
                </div>
                <div class="col-md-2 padding_sm" style="top:16px">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input type="checkbox" id="consolidated" name="consolidated">
                        <label class="black" style="padding-left:2px;color: #000 !important"
                            for="consolidated">
                            <strong>Consolidated</strong>
                        </label>
                    </div>
                </div>
            </div>
                <div class="clearfix"></div>
                <div class="col-md-1 padding-sm" style="float:right;padding-top: 25px;">
                    <div class="clearfix"></div>
                    <button type="button" class="btn bg-green btn-block" onclick="getResultData();"
                        name="search_results" id="search_results">
                        <i class="fa fa-search"></i>
                        Search
                    </button>

                </div>
                <div class="col-md-1 padding-sm" style="float:right;padding-top: 25px;">

                    <button type="reset" class="btn btn-primary btn-block" name="" id="">
                        <i class="fa fa-repeat"></i>
                        Reset
                    </button>

                </div>
                <div class="col-md-1 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="button" onclick="printReportData();" class="btn btn-primary btn-block"
                        name="print_results" id="print_results">
                        <i class="fa fa-print"></i> Print
                    </button>
                </div>

                <div class="col-md-1 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="button" onclick="dynamic_exceller_template('Locationwisepharmacysalesdetailreport')" class="btn btn-primary btn-block"
                        name="csv_results" id="csv_results">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel
                    </button>
                </div>


                <div class="col-md-2 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="button" onclick="downloadExcelData()" class="btn btn-primary btn-block"
                        name="csv_results" id="csvResultsBtn">
                        <i id="csvResultsSpin" class="fa fa-file-excel-o"></i> Generate Bulk Excel
                    </button>
                </div>
                <div class="col-md-2 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="button" onclick="generatedExcelData('Locationwisepharmacysalesdetailreport',1)"
                        class="btn btn-primary btn-block" name="csv_results" id="generatedExcelDataBtn">
                        <i id="generatedExcelDataSpin" class="fa fa-list"></i> Generated Excel Data
                    </button>
                </div>
            </div>
        </div>
        {!! Form::token() !!} {!! Form::close() !!}
    </div>

    <div class="col-md-12 no-padding" style="    overflow-y: hidden !important;">
        <div class="theadscroll always-visible" id="ResultDataContainer"
            style=" padding: 10px; display:none;font-family:poppinsregular;width: 1626px;">
            {{-- <style>
                @media print {
                    .theadscroll {
                        max-height: none;
                        overflow: visible;
                    }
                }
            </style> --}}
            <div class ="" style="float: left; width: 100% !important;position: relative; height: 400px;" id="ResultsViewArea">


            </div>
        </div>
    </div>

</div>

@include('Master::flexy.report_print_modal')
@stop
@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/locationWisePharmacySalesReport.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
