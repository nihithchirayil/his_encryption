<div class="col-md-12 no-padding">
    <div class="col-md-5">
       <h5><b>Doctor : {{$doctor_name}}</b></h5>
       <input type="hidden" id="proceure_save_doctor_id" value="{{$doctor_id}}"/>
    </div>
    <div class="col-md-3 pull-right no-padding">
        <button type="button" style="width:110px;" class="btn bg-green pull-right" onclick="save_percentage();">
            <i class="fa fa-save"></i> Save
        </button>
    </div>
</div>
<div class="col-md-12 no-padding" style="margin-top:5px;">
    <div class="col-md-2">Select department :</div>
    <div class="col-md-4" style="text-align:left;">
        {!! Form::select('select_department', $departments, 0, ['class' => 'form-control select2 filters bottom-border-text','onchange'=>'select_sub_department(this.value);','placeholder' => 'select department','id' => 'select_department','style' => 'width:100%;color:#555555; padding:2px 12px;z-index:999999;']) !!}
    </div>
</div>
<div class="col-md-12 no-padding" style="margin-top:5px;">
    <div class="col-md-2">Select sub department :</div>
    <div class="col-md-4" style="text-align:left;">
        {!! Form::select('select_sub_department', $sub_deparments, 0, ['class' => 'form-control select2 filters bottom-border-text','onchange'=>'select_percentage_services(this.value);','placeholder' => 'select sub department','id' => 'select_sub_department','style' => 'width:100%;color:#555555; padding:2px 12px;z-index:999999;']) !!}
    </div>
</div>

<div class="col-md-12" style="margin-top:10px;">
    <div class="col-md-6 box-body" style="height:420px;padding:10px !important;border-radius:5px;">
        <div class="col-md-12" style="background-color: aquamarine;
        height:18px;
        padding-left: 10px !important;">
            <div class="col-md-1">
                <div class="checkbox checkbox-success" style="margin-left:-5px;">
                    <input id="select_all_services" class="" type="checkbox" value="">
                    <label style="color:black !important;" for="select_all_services" class="">
                    </label>
                </div>
            </div>
            <div class="col-md-11">
                <b>Available Services</b>
            </div>
        </div>
        <div class="col-md-12 theadscroll available-services" style="height:380px;position: absolute;margin-top:21px;">

        </div>
    </div>

    <div class="col-md-5 box-body" style="height:420px;padding:10px !important;margin-left:10px;">
        <div class="col-md-12" style="background-color: aquamarine;
        height:18px;
        padding-left: 10px !important;">
            <b>Added Services</b>
        </div>
        <div class="col-md-12 theadscroll added-services" style="height:380px;position: absolute;margin-top:21px;border-radius:5px;">
            @foreach($added_services as $services)
            <div class="col-md-12 added_service_item" style=" font-size: 11px;
            border-bottom: 1px solid #d8d8d8;
            font-weight: 600;
            color:black;"  data-service-id="{{$services->id}}" data-service-desc="{{$services->service_name}}" data-service-percentage="{{$services->percentage}}">
                <input type="hidden" name="added_services[]" value="{{$services->id}}">
                {{$services->service_name}}
                <button type="button" style="margin-bottom:1px;" name="remove_service" value="Remove" class="btn pull-right btn-danger" onclick="remove_service('{{$services->id}}','{{$services->service_name}}');">
                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                </button>
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="col-md-12"></div>
