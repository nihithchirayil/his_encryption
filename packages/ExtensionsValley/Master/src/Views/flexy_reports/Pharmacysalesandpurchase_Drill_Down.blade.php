<div class="col-md-12" style="margin-bottom:20px;">
@if(sizeof($result_data)>0)
    <div class="theadscroll" id="drill_down_table_container" style="position: relative; max-height: 400px;">
        <table id="drill_down_table" border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper" style="border-collapse: collapse;">
            <thead>
                <tr>
                    @php
                        $head_array = $result_data[0];
                    @endphp
                        <th style="width:5%" class="header_bg">Sl.No</th>
                    @foreach($head_array as $key => $value)
                        @php
                            $key = str_replace('_', ' ', $key);
                            $key = ucwords($key);
                        @endphp
                        <th style="" class="header_bg">{{$key}}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach($result_data as $key => $value)
                    <tr>
                        <td>{{$i}}</td>
                        @foreach($value as $key1 => $value1)
                            @php
                                if(is_numeric($value1)){
                                    $value1 = number_format($value1, 2);
                                    $cls = "td_common_numeric_rules";
                                }else{
                                    $cls = "common_td_rules";
                                }
                            @endphp
                            <td class="{{$cls}}">{{$value1}}</td>
                        @endforeach
                    </tr>
                    @php
                        $i++;
                    @endphp
                @endforeach
            </tbody>
        </table>

    </div>

@else
<div class="alert alert-danger">
        No restults Found
</div>
@endif
</div>
