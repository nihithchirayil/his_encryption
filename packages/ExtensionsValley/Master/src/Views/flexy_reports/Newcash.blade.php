@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<style>
    .header_bg{
        background-color: #02967f;
        color:white;
    }
    .ajaxSearchBox{
        display: none;
        width: 250px !important;
        height: 400px !important;
        overflow: hidden;
        padding-top: 0px !important;
        margin-top: -29px !important;
    }
    a{color:black !important;}
    li>a{
      color:#E7E7E7 !important;
    }
    ul{padding-left:5px !important;font-weight: 200 !important;}
    li>a:hover{color:black !important;}
    .table_name{color:rgb(114, 4, 4) !important;}
    .gradient_bg{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
        color:white !important;
    }
    .bottom-border-text{
    border:none !important;
    border-bottom:1px solid lightgrey !important;
    box-shadow:none;
    }
    label{
        color:cornflowerblue !important;
    }
    .bg-radio_grp{
    background-color:#b9b9b9 !important;
    color:white !important;
    margin-right:6px !important;
    /* box-shadow: 3px 2px 3px #bdbdbd; */
    }

    .bg-radio_grp.active{
        background-color:#36a693 !important;
        color:white !important;
        margin-right:6px !important;
        box-shadow: 3px 2px 3px #55ff79;
    }
    .select2-selection__choice{
        background-color: #e6f1e8 !important;
        border: 1px solid #dbd8d8 !important;
        font-size: 10px !important;
    }
    .mate-input-box{
        height:51px !important;
    }

    .mate-input-box>text{
        margin-top:3px !important;
    }
    .select2-selection--multiple{
        padding-bottom:0px !important;
    }
    .table_header_bg{
        height: 30px !important;
        text-align:right !important;
        padding-right :40px !important;
        margin-bottom : 6px !important;
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .table_header_bg>h5{
        margin-top:7px;
        font-weight: 700;
    }
    .ajaxSearchBox{
        z-index: 9999 !important;
    }
    .ajaxSearchBox>li{
        font-size:13px !important;
        font-family: Arial !important;

    }
    .liHover{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .hospital_header>table{
        .border-bottom:1px solid #a5a4a4;
    }


</style>

@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" name="report_id" id="report_id" value="{{$report_id}}">

<div class="right_col">
    <div class="row">
        <div class="col-md-12 box-body" style="min-height:120px !important;">
            <div class="col-md-12 no-padding table_header_bg">
                <h5>{{$title}}</h5>
            </div>


            {!!Form::open(array('name'=>$form_name, 'method' => 'get', 'id'=> $form_name))!!}

            <div class="box-body" style="padding-bottom:15px;">
                <?php
                $i = 1;

                foreach ($showSearchFields as $fieldType => $fieldName) {
                    $fieldTypeArray[$i] = explode('-', $fieldType);

                    if ($fieldTypeArray[$i][0] == 'progressive_search') {
                        $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                        ?>

                        <div class= "col-xs-3">
                            <div class="mate-input-box">
                                <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                <input class="form-control hidden_search" value="" autocomplete="off" type="text"  autocomplete="off"  id="{{$fieldTypeArray[$i][1]}}" name="{{$fieldTypeArray[$i][1]}}" />
                                <div id="{{$fieldTypeArray[$i][1]}}AjaxDiv" class="ajaxSearchBox"></div>
                                <input class="filters" value="{{$hiddenFields[$hidden_filed_id]}}"  type="hidden" name="{{$fieldTypeArray[$i][1]}}_hidden" value="" id="{{$fieldTypeArray[$i][1]}}_hidden">
                            </div>
                        </div>

                        <?php
                    } else if ($fieldTypeArray[$i][0] == 'text') {
                        ?>
                        <div class="col-md-3">
                            <div class="mate-input-box">

                                <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                <input type="text" name="{{$fieldTypeArray[$i][1]}}" <?php if (isset($fieldTypeArray[$i][3])) {
                        echo $fieldTypeArray[$i][3];
                    } ?>  value="" class="form-control filters" id="{{$fieldTypeArray[$i][1]}}" data="{{isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : ''}}" <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?> autocomplete="off" >

                            </div>

                        </div>
                        <?php
                    } else if ($fieldTypeArray[$i][0] == 'from_date') {

                        ?>
                        <div class="col-xs-2 date_filter_div">
                            <div class="mate-input-box">
                                <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                <input type="text" data-attr="date" name="{{$fieldTypeArray[$i][1]}}_from" <?php
                        if (isset($fieldTypeArray[$i][3])) {
                            echo $fieldTypeArray[$i][3];
                        }
                        ?>  value="" class="form-control filters" placeholder="MMM-DD-YYYY" id="{{$fieldTypeArray[$i][1]}}"  autocomplete="off">

                                </div>
                            </div>
                            <?php
                    }else if ($fieldTypeArray[$i][0] == 'to_date') {
                        ?>
                            <div class="col-xs-2 date_filter_div">
                                <div class="mate-input-box">
                                    <label class="filter_label">{{$fieldTypeArray[$i][2]}} </label>
                                    <input type="text" data-attr="date" name="{{$fieldTypeArray[$i][1]}}_to" <?php
                        if (isset($fieldTypeArray[$i][3])) {
                            echo $fieldTypeArray[$i][3];
                        }
                                ?>  value="" class="form-control filters" placeholder="MMM-DD-YYYY" id="{{$fieldTypeArray[$i][1]}}"  autocomplete="off">

                                </div>
                            </div>
                            <?php

                    } else if ($fieldTypeArray[$i][0] == 'combo') {
                        ?>
                        <div class="col-xs-2 date_filter_div">
                            <div class="mate-input-box">
                                <label class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control filters ','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1], 'style' => 'color:#555555; padding:4px 12px;']) !!}
                            </div>
                        </div>
                        <?php
                    } else if ($fieldTypeArray[$i][0] == 'multi_combo') {

                        ?>
                        <div class="col-xs-3 date_filter_div">
                            <div class="mate-input-box">
                                <label style="width: 100%;"class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>

                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control select2 filters ','multiple'=>'multiple-select','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1], 'style' => 'width:100%;color:#555555; padding:2px 10px;']) !!}

                            </div>
                        </div>
                        <?php
                    }else if ($fieldTypeArray[$i][0] == 'check_box') {

                        ?>
                        <div class="col-md-3">
                            <div class="mate-input-box">

                                <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                <input type="checkbox" name="{{$fieldTypeArray[$i][1]}}" <?php if (isset($fieldTypeArray[$i][3])) {
                        echo $fieldTypeArray[$i][3];
                    } ?>  value="" class="form-control filters" id="{{$fieldTypeArray[$i][1]}}" data="{{isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : ''}}" <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?> autocomplete="off" >

                            </div>

                        </div>
                        <?php
                    }else if ($fieldTypeArray[$i][0] == 'radio') {

                        ?>
                        <div class="col-md-3">
                            <div class="mate-input-box">

                                <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                <input type="radio" name="{{$fieldTypeArray[$i][1]}}" <?php if (isset($fieldTypeArray[$i][3])) {
                        echo $fieldTypeArray[$i][3];
                        } ?>  value="" class="form-control filters" id="{{$fieldTypeArray[$i][1]}}" data="{{isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : ''}}" <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?> autocomplete="off" >

                            </div>

                        </div>
                        <?php
                    }
                    $i++;

                }
                ?>

                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <div class="clearfix"></div>

                    <button type="button" class="btn bg-green btn-block" style="color:white !important;width:101px !important;" onclick="getResultData();" name="search_results" id="search_results">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        Search
                    </button>

                </div>
                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">

                    <button type="reset" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="" id="">
                        <i class="fa fa-repeat" aria-hidden="true"></i>
                        Reset
                    </button>

                </div>


                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="button" onclick="printReportData();" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="print_results" id="print_results">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>

                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <button onclick="generate_csv()" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="csv_results" id="csv_results">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel
                    </button>
                </div>



            </div>
            {!! Form::token() !!} {!! Form::close() !!}
        </div>

            <div class="col-md-12 padding_sm">
                <div id="ResultDataContainer"
                    style="min-height: 650px; padding: 10px; display:none;font-family:poppinsregular">
                    <div style="background:#686666;">
                        <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; border: 1px solid #dbdbdb;
                                                        width: 98%;position: absolute;
                                                        padding: 30px;" id="ResultsViewArea">

                        </page>
                    </div>
                </div>
            </div>

    </div>
</div>
@include('Master::flexy.report_print_modal')
@stop
@section('javascript_extra')

<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
        $('.multiple_selectbox').multiselect();
        $("option:selected").prop("selected", false);
        }, 300);
        $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
        $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
    });

    function getResultData(){
        var form_name = '{{$form_name}}';
        //alert(form_name);return;
        var form_data = $('#'+form_name).serialize();
        var base_url = $('#base_url').val();
        //var url = '{{$dataurl}}';
        var url = base_url+'/flexy_report/'+'{{$report_name}}';


        var filters_list = new Object();
        var filters_value = '';
        var filters_id = '';


        $('.filters').each(function () {
            filters_id = this.id;

            filters_value = $('#' + filters_id).val();
            filters_id = filters_id.replace('_hidden', '');

            if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
                filters_list[filters_id] = filters_value;
            }
        });



        $.ajax({
            url: url,
            type: 'POST',
            data: filters_list,
            beforeSend: function () {
                $('#ResultDataContainer').show();

                $('#ResultDataContainer').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                $('.ResultDataContainerBody').show();
                $('#ResultsViewArea').html(data);

            },
            complete: function () {
                $('#ResultDataContainer').LoadingOverlay("hide");
            }
        });
    }




    //----Hidden Filed Search--------------------------------------------------------------------

    $('.hidden_search').keyup(function (event) {
        var input_id = '';
        var base_url = $('#base_url').val();
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        input_id = $(this).attr('id');

        var current;
        if (value.match(keycheck) || event.keyCode == '8') {
            if ($('#' + input_id + '_hidden').val() != "") {
                $('#' + input_id + '_hidden').val('');
            }
            var search_key = $(this).val();
            search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
            search_key = search_key.trim();

            var department_hidden = $('#department_hidden').val();
            var datastring = '';
            if (input_id == 'sub_department') {
                datastring = '&department_id=' + department_hidden;
            }
            if (input_id == 'scheme') {
                datastring = '&company_id=' + company_hidden;
            }

            if (search_key == "") {
                $("#" + input_id + "AjaxDiv").html("");
            } else {
                var url = base_url+'/flexy/FlexyReportProgressiveSearch';
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                    beforeSend: function () {

                        $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {

                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {

                    },
                    error: function () {
                        bootbox.alert('nw error');
                        return;
                    }

                });
            }
        } else {
            ajax_list_key_down(input_id + 'AjaxDiv', event);
        }
    });

    function fillSearchDetials(id, name, serach_key_id) {
        $('#' + serach_key_id + '_hidden').val(id);
        $('#' + serach_key_id).val(name);
        $('#' + serach_key_id).attr('title', name);
        $("#" + serach_key_id + "AjaxDiv").hide();
    }

    /* setting for enter key press in ajaxDiv listing */
    $(".hidden_search").on('keydown', function (event) {
        var input_id = '';
        input_id = $(this).attr('id');
        if (event.keyCode === 13) {
            ajaxlistenter(input_id + 'AjaxDiv');
            return false;
        }
    });


$("#category").on('change', function() {
    var category = $('#category').val();
    if (category == '') {
      var category = 'a';

    }

    if(category) {
        $.ajax({
            type: "GET",
            url: '',
            data: 'category=' + category,
            beforeSend: function () {
                $('#s2id_sub_category').append('<p id="warning1" style=color:red;padding-left:2px;>please wait......</p>');
                $( "#sub_category" ).prop( "disabled", true ); //Disable
            },
            success: function (html) {
                if(html){
                    $("#sub_category").empty();
                    $("#sub_category").html('<option value="">Select Sub catogery</option>');
                    $.each(html,function(key,value){
                        $("#sub_category").append('<option value="'+key+'">'+value+'</option>');
                    });
                }else{
                    $("#sub_category").empty();
                }
            },

            error: function () {
                Command: toastr["error"]("Please check your internet connection and try again!");
                $( "#sub_category" ).prop( "disabled", false );
            },
            complete: function () {
                $('#warning1').remove();
                $( "#sub_category" ).prop( "disabled", false );
            }

        });

    }else{
            $("#category").focus();
            $("#sub_category").empty();
        }

    });


    //---------generate csv---------------
function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#result_data_table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function generate_csv() {
    var html = document.querySelector("#result_data_table").outerHTML;
    export_table_to_csv(html, "table.csv");
};

//---------generate csv ends---------


//---------print report--------
function printReportData() {
    $('#print_config_modal').modal('toggle');
}

function print_generate(printData) {
    //$('#print_config_modal').modal('hide');
    var printMode = $('input[name=printMode]:checked').val();
    //alert('dddd'); return;
    var showw = "";

    var mywindow = window.open('', 'my div', 'height=3508,width=2480');
    if($('#showTitle').is(":checked")){
        printhead =$("#hospital_header").val();
        showw=showw+atob(printhead);
    }
    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;

    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait;margin:0;margin-left:15px;text-align:center;box-shadow:none !important;border:1px solid #a9a9a9;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape;margin:0;marginleft:15px;text-align:center;box-shadow:none !important;border:1px solid #a9a9a9;}</style>');
    }
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print()
    mywindow.close();
    return true;
}


</script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
