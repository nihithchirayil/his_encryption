<style>
    .box{
        border: 1px solid lightgrey !important;
    }
    </style>
<div class="col-md-12" style="margin-bottom:20px;">
    @if (sizeof($result_data) > 0)
        @php
            $result_data = collect($result_data)->groupBy('dept_name');
            $i = 0;
            $j=0;

        @endphp
        <div class="theadscroll" id="drill_down_table_container" style="position: relative; max-height: 400px;">
            @foreach ($result_data as $key => $list1)
            @if($i == 0)
            <table border='0'>
                <tbody>

                   
                        <tr>
                            <td width="3%" ><b>Patient Name</b></td>
                            <td width="1%" ><b>:</b></td>
                            <td width="3%" ><b>{{ $list1[0]->patient_name }}</b></td>
                            <td width="3%" ><b>UHID</b></td>
                            <td width="1%" ><b>:</b></td>
                            <td width="3%" ><b>{{ $list1[0]->uhid }}</b></td>
                        </tr>                        
                        <tr>
                            <td width="3%" ><b>Bill Number</b></td>
                            <td width="1%" ><b>:</b></td>
                            <td width="3%" ><b>{{ $list1[0]->bill_no }}</b></td>
                            <td width="3%" ><b>Bill Date</b></td>
                            <td width="1%" ><b>:</b></td>
                            <td width="3%" ><b>{{ $list1[0]->bill_date }}</b></td>
                        </tr>


                </tbody>
            </table>
            @endif
            @php $i++; @endphp
            @endforeach
            @foreach ($result_data as $key => $list)
            
                <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
                    style="border: 1px solid #CCC;min-width:250px !important;    margin-top: 12px !important;">
                    <tbody>

                        <?php 
                        $total = 0;
                        foreach ($list as $k => $val) {
                            $total += $val->net_amount;
                        }
                        ?>

                        <tr style="background-color: darkgray">
                            <td class="common_td_rules"><b>{{ $key }}</b></td>
                            <td class="common_td_rules"></td>
                            <td class="common_td_rules"></td>
                            <td class="common_td_rules"></td>
                            <td class="td_common_numeric_rules" align="center"><b style="margin-right: 69px;">
                                {{ number_format($total, 2) }}</b>
                            </td>
                        </tr>


                    </tbody>
                </table>
                <table id="drill_down_table" border="1" width="100%;"
                    class="table table-striped table-bordered table-hover table-condensed table_sm "
                    style="border-collapse: collapse;">
                    <thead>
                        <tr>

                            <th style="width:1%" class="header_bg">Sl.No</th>
                            <th style="width:5%" class="header_bg">Bill No</th>
                            <th style="width:5%" class="header_bg">Bill Date</th>
                            <th style="width:5%" class="header_bg">Item Desc</th>
                            <th style="width:5%" class="header_bg">Quantity</th>
                            <th style="width:5%" class="header_bg">Rate</th>
                            <th style="width:5%" class="header_bg">Gross Amount</th>

                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($list as $key => $value)
                            <tr>
                                <td class="common_td_rules">{{ $i++ }}</td>
                                <td class="common_td_rules">{{ $value->bill_no }}</td>
                                <td class="common_td_rules">{{ $value->bill_date }}</td>
                                <td class="common_td_rules">{{ $value->item_desc }}</td>
                                <td class="common_td_rules">{{ $value->quantity }}</td>
                                <td class="common_td_rules">{{ $value->selling_price }}</td>
                                <td class="common_td_rules">{{ $value->net_amount }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endforeach
            @foreach ($result_data as $key => $list1)
            @if($j == 0)

            <?php 
                        $total = 0;
                        $discount = 0;
                        $paid_amount = 0;
                        foreach ($list1 as $k => $val) {
                            $total += $val->bill_amount;
                            $discount += $val->total_discount;
                            $paid_amount += $val->paid_amount;
                        }
                        ?>
            <table class="box" border='0' style="float:right;width:30%">
                <tbody>

                   
                        <tr>
                            <td width="5%" ><b>Total Bill Amount</b></td>
                            <td width="3%" ><b>:</b></td>
                            <td width="3%" ><b>{{ number_format($total, 2) }}</b></td>
                        </tr>
                        <tr>

                            <td width="3%" ><b>Bill Discount</b></td>
                            <td width="3%" ><b>:</b></td>
                            <td width="3%" ><b>{{ number_format($discount, 2) }}</b></td>
                        </tr>                        
                        <tr>
                          
                            <td width="3%" ><b>Paid Amount</b></td>
                            <td width="3%" ><b>:</b></td>
                            <td width="3%" ><b>{{ number_format($paid_amount, 2) }}</b></td>
                        </tr>


                </tbody>
            </table>
            @endif
            @php $j++ @endphp
            @endforeach
        </div>
    @else
        <div class="alert alert-danger">
            No restults Found
        </div>
    @endif
</div>
