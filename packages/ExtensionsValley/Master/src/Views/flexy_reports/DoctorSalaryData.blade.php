
<style>
    @media print {
        .theadscroll {max-height: none; overflow: visible;}
        .header_bg {
            background-color: #08898d;
            color: white;
        }
        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #f2fef5;
        }

        #resultDataTable{
            border: 1px solid #cedfe9 !important;
            border-collapse: collapse !important;
        }
        td{
            padding-left: 5px !important;
        }
        }
    .global-footer {
        position: fixed;
        width: 90.5%;
        bottom: 38px;
        /* left: 0; */
        /* padding: 1em */
    }
</style>

<div class="col-md-12" style="margin-bottom:15px;">
        @if(!empty($result_data)>0)
        <div id="ResultsPrintViewArea">
            @php
                $head_array = $result_data[0];
                $total_colums = explode(",", $flexy_report_totals);
                $total_array = [];
                $total_array_index = [];
                $drill_down_array = [];
                $size_array = [];
                $j = 0;

                $head_array_size = 0;
                $custom_column_width = explode(',', $custom_column_width);
                $drill_down_columns = str_replace("@","",$drill_down_columns);
                $drill_down_columns = explode(',', $drill_down_columns);
                $drill_down_values = '';
                $numeric_colums = explode(',', $numeric_colums);
            @endphp
            <span class="no-padding" id="excelContainer">
            <div class="theadscroll no-padding" id="table_container_div" style="position: relative; max-height: 400px;">
                <table border='0'; width="100%;" class=" no-padding" style="border-collapse: collapse;table-layout:fixed;border-color:white;">
                    <tr class="table_header_row">
                        <th style="text-align:left;font-weight:100 !important;" colspan="{{$head_array_size+1}}">
                            Report Print Date: {{date('M-d-Y h:i A')}}
                        </th>
                    </tr>
                    <tr class="table_header_row">
                        <th style="text-align:left;font-weight:100 !important;" colspan="{{$head_array_size+1}}">
                            Report Date: {{$from_date}} To {{$to_date}}
                        </th>
                    </tr>
                    <tr class="table_header_row">
                        <th style="text-align:left;font-weight:100 !important;" colspan="{{$head_array_size+1}}">
                            Number Of Records: {{count($result_data)}}
                        </th>
                    </tr>
                    @php
                        $company_details = \DB::table('company')->where('id',1)->first();
                    @endphp
                    <tr class="hospital_header_excel" style="display:none;">
                        <td style="text-align: center !important;" colspan="{{$head_array_size+1}}">
                            <span><strong>{{$company_details->name}}</strong></span><br>
                            <span>{{$company_details->address}}</span><br>
                            <span>Phone :{{$company_details->phone}}</span>
                            <span>Booking No :{{$company_details->booking_no}}</span>
                        </td>
                    </tr>
                    <tr class="table_header_row no-padding"  style="background-color: #07ad8c;color: white;">
                        <th style="font-size: 18px !important;text-align:center;" colspan="{{$head_array_size+1}}">
                            {{$menu_name}}
                        </th>
                    </tr>
                </table>
                <table id="resultDataTable" border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;">

                    <thead>
                        <tr style="background-color:#07ad8c; color: white;">

                                <th style="width:4%">Sl.No</th>
                                <th style="width:7%">Speciality</th>
                                <th style="width:9%">Doctor Name</th>
                                <th style="width:7%">Op Amount</th>
                                <th style="width:7%">Ip Amount</th>
                                <th style="width:7%">Procedure Charges</th>
                                <th style="width:6%">Ot Amount</th>
                                <th style="width:6%">Health Checkup</th>
                                <th style="width:6%">Insurance Amount</th>
                                <th style="width:7%">Bill Analysis</th>
                                <th style="width:7%">Fixed Salary</th>
                                <th style="width:7%">Additional Amount</th>
                                <th style="width:7%">Net Amount</th>
                                <th style="width:5%">Tds(%)</th>
                                <th style="width:5%">Final</th>
                                <th style="width:5%">#</th>
                            @php
                                $op_total = 0;
                                $ip_total = 0;
                                $procedure_total = 0;
                                $ot_total = 0;
                                $bill_analysis_total = 0;
                                $health_checkup_total = 0;
                                $insurance_total = 0;
                                $fixed_salary_total = 0;
                                $net_total = 0;
                                $additional_total = 0;
                                $balance_total =0;
                            @endphp
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                            // echo '<pre>';
                            //     print_r($result_data);
                            //     exit;
                        @endphp
                        @foreach($result_data as $data)
                            <tr>
                                <td>{{$i}}</td>
                                <td style="text-align:left">{{$data->speciality}}</td>
                                <td style="text-align:left">{{$data->doctor_name}}</td>
                                <td onclick="op_details('{{$data->doctor_id}}');"  class="td_common_numeric_rules op_details">{{$data->op_amount}}</td>
                                <td onclick="ip_details('{{$data->doctor_id}}');"  class="td_common_numeric_rules ip_details">{{$data->ip_amount}}</td>
                                <td onclick="procedure_details('{{$data->id}}','{{$data->doctor_id}}');" class="td_common_numeric_rules bill_analysis">{{$data->procedure_charges}}</td>
                                <td onclick="ot_details('{{$data->doctor_id}}');" class="td_common_numeric_rules ot_details">{{$data->ot_amount}}</td>
                                <td class="td_common_numeric_rules">{{$data->health_checkup}}</td>

                                <td class="td_common_numeric_rules insurance_amount" data-attr-id='{{$data->id}}'>
                                    <i style="display:none;float:left;" class="fa fa-check insurance_amount_check" aria-hidden="true" onclick="changeInsuranceAmount(this,'{{$data->id}}');"></i>
                                    <input style="width:75%;display:none;float:right;" type="text" name="insurance_amount" class="insurance_amount_text" data-attr-id='{{$data->id}}' value="{{$data->insurance_amount}}"/>
                                    <span id="insurance_amount_span_{{$data->id}}" style="display:block" class="insurance_amount_span">{{$data->insurance_amount}}</span>
                                </td>
                                <td onclick="bill_analysis_detail('{{$data->id}}','{{$data->doctor_id}}');" class="td_common_numeric_rules bill_analysis">{{$data->bill_analysis}}</td>


                                <td class="td_common_numeric_rules fixed_salary">

                                    <i style="display:none;float:left;" class="fa fa-check fixed_salary_check" aria-hidden="true" onclick="changeFixedSalary(this,'{{$data->id}}');"></i>

                                    <input style="width:75%;display:none;float:right;" type="text" name="fixed_salary" class="fixed_salary_text" data-attr-id='{{$data->id}}' value="{{$data->fixed_salary}}"/>

                                    <span id="fixed_salary_span_{{$data->id}}" style="display:block" class="fixed_salary_span">{{$data->fixed_salary}}</span>


                                <td class="td_common_numeric_rules additional_amount" data-attr-id='{{$data->id}}'>
                                    <i style="display:none;float:left;" class="fa fa-check additional_amount_check" aria-hidden="true" onclick="changeAdditionalAmount(this,'{{$data->id}}');"></i>

                                    <input style="width:75%;display:none;float:right;" type="text" name="additional_amount" class="additional_amount_text" data-attr-id='{{$data->id}}' value="{{$data->additional_amount}}"/>

                                    <span id="additional_amount_span_{{$data->id}}" style="display:block" class="additional_amount_span">{{$data->additional_amount}}</span>

                                </td>
                                <td class="td_common_numeric_rules net_amount" style="padding-right:15px !important;">
                                    @php
                                        $net_amount = 0;
                                        $net_amount = $data->op_amount + $data->ip_amount + $data->procedure_charges + $data->ot_amount + $data->health_checkup + $data->insurance_amount + $data->bill_analysis + $data->fixed_salary+$data->additional_amount;
                                    @endphp
                                    {{$net_amount}}
                                </td>
                                <td class="td_common_numeric_rules tds">

                                    <i style="display:none;float:left;" class="fa fa-check tds_check" aria-hidden="true" onclick="changeTds(this,'{{$data->id}}','{{$data->doctor_id}}');"></i>

                                    <input style="width:75%;display:none;float:right;" type="text" name="tds" class="tds_text" data-attr-id='{{$data->id}}' value="{{$data->tds}}"/>

                                    <span id="tds_span_{{$data->id}}" style="display:block" class="tds_span">{{$data->tds}}</span>

                                </td>
                                <td class="td_common_numeric_rules balance_amount">
                                    @php
                                        $tds = $data->tds;
                                        $tds_amount = 0;
                                        $tds_amount = ($net_amount*$tds)/100;
                                        $balance_amount = $net_amount - $tds_amount;
                                        @endphp
                                        {{$balance_amount}}
                                </td>
                                <td>
                                    <button onclick="salaryRecalculate('{{$data->doctor_id}}');" type="button" class="btn btn-sm btn-primary">
                                        <i id="recalculate_spin_{{$data->doctor_id}}" class="fa fa-cog" aria-hidden="true"></i>
                                    </button>
                                </td>
                            </tr>
                            @php
                                $i++;
                                $op_total += $data->op_amount;
                                $ip_total += $data->ip_amount;
                                $procedure_total += $data->procedure_charges;
                                $ot_total += $data->ot_amount;
                                $bill_analysis_total += $data->bill_analysis;
                                $health_checkup_total += $data->health_checkup;
                                $insurance_total += $data->insurance_amount;
                                $fixed_salary_total += $data->fixed_salary;
                                $additional_total += $data->additional_amount;
                                $net_total += $net_amount;
                                $balance_total += $balance_amount;
                            @endphp
                        @endforeach
                            <tr style="background-color: paleturquoise;font-weight:600;">
                                <td colspan="3">Total</td>
                                <td class="td_common_numeric_rules">{{$op_total}}</td>
                                <td class="td_common_numeric_rules">{{$ip_total}}</td>
                                <td class="td_common_numeric_rules">{{$procedure_total}}</td>
                                <td class="td_common_numeric_rules">{{$ot_total}}</td>
                                <td class="td_common_numeric_rules">{{$health_checkup_total}}</td>
                                <td class="td_common_numeric_rules">{{$insurance_total}}</td>
                                <td class="td_common_numeric_rules">{{$bill_analysis_total}}</td>
                                <td class="td_common_numeric_rules">{{$fixed_salary_total}}</td>
                                <td class="td_common_numeric_rules">{{$additional_total}}</td>
                                <td class="td_common_numeric_rules" style="padding-right:15px !important;">{{$net_total}}</td>
                                <td></td>
                                <td class="td_common_numeric_rules" style="padding-right:15px !important;">{{$balance_total}}</td>
                            </tr>

                    </tbody>
                </table>
            </div>
            </span>
        </div>

    @else
    <div class="alert alert-danger">
        No data found!
    </div>
    @endif
</div>
