<?php
use ExtensionsValley\Master\FlexyReportController;
?>
@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">

    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<style>
    .header_bg{
        background-color: #02967f;
        color:white;
    }
    a{color:black !important;}
    li>a{
      color:#E7E7E7 !important;
    }
    ul{padding-left:5px !important;font-weight: 200 !important;}
    li>a:hover{color:black !important;}
    .table_name{color:rgb(114, 4, 4) !important;}
    .gradient_bg{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
        color:white !important;
    }
    .bottom-border-text{
    border:none !important;
    border-bottom:1px solid lightgrey !important;
    box-shadow:none;
    }
    label{
        color:cornflowerblue !important;
    }

    .insurance_amount_text,.additional_amount_text{
        border-color: #fdfcfb !important;
    }
    .insurance_amount_check,.additional_amount_check{
        margin-top: 5px !important;
        color: green !important;
        cursor: pointer;
    }

    .select2-selection__choice{
        background-color: #e6f1e8 !important;
        border: 1px solid #dbd8d8 !important;
        font-size: 10px !important;
    }
    .mate-input-box{
        height:51px !important;
    }

    .mate-input-box>text{
        margin-top:3px !important;
    }
    .select2-selection--multiple{
        padding-bottom:0px !important;
    }
    .table_header_bg{
        height: 30px !important;
        text-align:right !important;
        padding-right :40px !important;
        margin-bottom : 6px !important;
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .table_header_bg>h5{
        margin-top:7px;
        font-weight: 700;
    }
    .ajaxSearchBox{
        z-index: 9999 !important;
    }
    .ajaxSearchBox>li{
        font-size:13px !important;
        font-family: Arial !important;

    }
    .liHover{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .hospital_header>table{
        .border-bottom:1px solid #a5a4a4;
    }

    .td_common_numeric_rules{
        border-left: solid 1px #bbd2bd !important;
        text-align: right !important;
        font-size:11px !important;
    }
    .common_td_rules{
        text-align: left !important;
        overflow: hidden !important;
        border-right: solid 1px #bbd2bd !important;
        border-left: solid 1px #bbd2bd !important;
        max-width: 100px;
        text-overflow: ellipsis;
        white-space: nowrap;
        font-size:11px !important;
    }
    .ajaxSearchBox{
        display: none;
        width: 250px !important;
        height: 400px !important;
        overflow: hidden;
        padding-top: 0px !important;
        margin-top: 15px !important;
    }

    .table_sm th, .table_sm td{
        font-size:11px !important;
    }

    .hospital_header_excel{
        background-color:#07ad8c; color: white;
    }

    @media print {

        .theadscroll {max-height: none; overflow: visible;}
        #ResultsViewArea{width:50% !important;}
        table{
            display: none !important;
        }

      }

    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #efefef;
    }

    .bill_analysis:hover{
        color: #00a65a !important;
        background-color: #badcf7 !important;
        font-weight:600 !important;
    }
    .ip_details:hover{
        color: #00a65a !important;
        background-color: #badcf7 !important;
        font-weight:600 !important;
    }
    .op_details:hover{
        color: #00a65a !important;
        background-color: #badcf7 !important;
        font-weight:600 !important;
    }
    .ot_details:hover{
        color: #00a65a !important;
        background-color: #badcf7 !important;
        font-weight:600 !important;
    }
    .bottom-border-text{
        border:none !important;
        border-bottom:1px solid lightgrey !important;
        box-shadow:none;
    }

    .box-body{
        background-color:white !important;
    }

    .checkbox label{
        font-size: 11px;
        font-weight: 600;
    }

</style>

@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" name="report_id" id="report_id" value="{{$report_id}}">
<input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
<input type="hidden" id="exceller_data"
    value="<?= base64_encode('<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>') ?>">

<div class="right_col">
    <div class="row">
        <div class="col-md-12 box-body" style="min-height:120px !important;">
            <div class="col-md-12 no-padding table_header_bg">
                <h5>{{$menu_name}}</h5>
            </div>
            {!!Form::open(array('name'=>$form_name, 'method' => 'get', 'id'=> $form_name))!!}

            <div class="box-body" style="padding-bottom:15px;">
                <div class= "col-xs-3">
                    <div class="mate-input-box">
                        <label class="filter_label">Doctor</label>
                        <input class="form-control hidden_search" value="" autocomplete="off" type="text"  autocomplete="off" id="doctor_name" name="doctor_name" />
                        <div id="doctor_nameAjaxDiv" class="ajaxSearchBox"></div>
                        <input class="filters" type="hidden" name="doctor_name_hidden" value="" id="doctor_name_hidden">
                    </div>
                </div>
                    @php
                        $fieldName = FlexyReportController::flexyReportMasterClassData('speciality');
                    @endphp
                <div class="col-xs-2 date_filter_div">
                    <div class='mate-input-box'>
                        <label class='filter_label'>Speciality</label>
                        <select multiple='multiple-select' class='form-control select2 bottom-border-text filters' style='width:100%;color:#555555; padding:2px 10px;' name='speciality' id='speciality'>
                            <option value='All'>All</option>
                            @foreach($fieldName as $key=>$val)
                            <option value='{{$key}}'>{{$val}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-2">
                    <div class='mate-input-box'>
                        <label class="filter_label">Select Month</label>
                        <input type="text" class="form-control monthpicker filters" name="month" id="month" value="{{date('M-Y')}}">
                    </div>
                </div>
                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <div class="clearfix"></div>
                    <button type="button" class="btn bg-green btn-block" style="color:white !important;width:101px !important;" onclick="getResultData();" name="search_results" id="search_results">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        Search
                    </button>
                </div>
                <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
                    <button type="reset" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="" id="">
                        <i class="fa fa-repeat" aria-hidden="true"></i>
                        Reset
                    </button>
                </div>
            </div>
            {!! Form::token() !!} {!! Form::close() !!}
        </div>
    </div>

    <div class="nav-tabs-custom" style="margin-top:10px;">
        <ul class="nav nav-tabs sm_nav">
            <li class="active"><a style="color:rgb(90, 155, 235) !important;" data-toggle="tab"
                aria-expanded="true" data-attr-id="normal_salary" href="#normal_salary">Doctors salary</a></li>
            <li><a style="color:rgb(90, 155, 235) !important;" data-toggle="tab"
                aria-expanded="false" data-attr-id="departmentwise_salary" href="#departmentwise_salary">Departmentwise salary</a></li>
            <li><a style="color:rgb(90, 155, 235) !important;" data-toggle="tab"
                aria-expanded="false" data-attr-id="consolidated" href="#consolidated">Consolidated salary</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="normal_salary">
                @include('Master::flexy_reports.doctor_salary.normal_salary')
            </div>
            <div class="tab-pane fade active in" id="departmentwise_salary">
                @include('Master::flexy_reports.doctor_salary.departmentwise_salary')
            </div>
            <div class="tab-pane fade active in" id="consolidated">
                @include('Master::flexy_reports.doctor_salary.consolidated_salary')
            </div>
        </div>
    </div>
</div>
@include('Master::flexy_reports.doctor_salary.doctor_salary_modals')


@include('Master::flexy.report_print_modal')

@stop
@section('javascript_extra')
<script>

    function getResultData(){
        var form_name = '{{$form_name}}';
        //alert(form_name);return;
        var form_data = $('#'+form_name).serialize();
        var base_url = $('#base_url').val();
        //var url = '{{$dataurl}}';
        var url = base_url+'/flexy_report/'+'{{$report_name}}';

        var report_type = $('.nav-tabs').find('li.active').find('a').attr("data-attr-id");

        var filters_list = new Object();
        var filters_value = '';
        var filters_id = '';


        $('.filters').each(function() {
            filters_id = this.id;

            filters_value = $('#' + filters_id).val();
            if (Array.isArray(filters_value)) {

                var filter_aarry = jQuery.grep(filters_value, function(n, i) {
                    return (n !== "" && n != null);
                });


                if (filter_aarry.length == 0) {
                    filters_value = '';
                }

                if(filter_aarry.find(function(element) {
                    return element == 'All';
                })){
                    filters_value = '';
                }
            }
            filters_id = filters_id.replace('_hidden', '');

            if (filters_value && filters_value !== "" && filters_value !== "All" && filters_value !== [] && filters_value !== null && filters_value !='All') {
                filters_list[filters_id] = filters_value;
            }
        });
        filters_list['report_type'] = report_type;


        $.ajax({
            url: url,
            type: 'POST',
            data: filters_list,
            beforeSend: function () {
                $('.ResultDataContainer').show();
                $('.ResultDataContainer1').show();
                if(report_type=='normal_salary'){
                    $('#ResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                }else if(report_type == 'departmentwise_salary'){
                    $('#departmentwiseResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                }else{
                    $('#ConsolidatedResultsViewArea').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                }
            },
            success: function (data) {

                $('.ResultDataContainerBody').show();
                if(report_type=='normal_salary'){
                    $('#normal_salary_results #ResultsViewArea').html(data);
                }else if(report_type == 'departmentwise_salary'){
                    $('#departmentwise_salary_results #departmentwiseResultsViewArea').html(data);
                }else{
                    $('#consolidated_salary_results #ConsolidatedResultsViewArea').html(data);
                }
            },
            complete: function () {
                $('.ResultDataContainer').LoadingOverlay("hide");

                if(report_type=='normal_salary'){
                    $('#ResultsViewArea').LoadingOverlay("hide");
                }else if(report_type=='departmentwise_salary'){
                    $('#departmentwiseResultsViewArea').LoadingOverlay("hide");
                }else{
                    $('#ConsolidatedResultsViewArea').LoadingOverlay("hide");
                }

                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });

                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table){
                        return $table.closest('.theadscroll');
                    }
                });

                $('.insurance_amount').dblclick(function () {
                    //$(this).attr('contenteditable', 'true');
                    $(this).find('.insurance_amount_text').css('display', 'block');
                    $(this).find('.insurance_amount_check').css('display', 'block');
                    $(this).find('.insurance_amount_span').css('display', 'none');
                });

                $('.additional_amount').dblclick(function () {
                    var report_type = $('.nav-tabs').find('li.active').find('a').attr("data-attr-id");
                    if(report_type == 'normal_salary'){
                        $(this).find('.additional_amount_text').css('display', 'block');
                        $(this).find('.additional_amount_check').css('display', 'block');
                        $(this).find('.additional_amount_span').css('display', 'none');
                    }
                });

                $('.fixed_salary').dblclick(function () {
                    var report_type = $('.nav-tabs').find('li.active').find('a').attr("data-attr-id");
                    if(report_type == 'normal_salary'){
                        $(this).find('.fixed_salary_text').css('display', 'block');
                        $(this).find('.fixed_salary_check').css('display', 'block');
                        $(this).find('.fixed_salary_span').css('display', 'none');
                    }
                });
                $('.tds').dblclick(function () {
                    var report_type = $('.nav-tabs').find('li.active').find('a').attr("data-attr-id");
                    if(report_type == 'normal_salary'){
                        $(this).find('.tds_text').css('display', 'block');
                        $(this).find('.tds_check').css('display', 'block');
                        $(this).find('.tds_span').css('display', 'none');
                    }
                });


                $('.insurance_amount').blur(function(){
                    var id = $(this).attr("data-attr-id");
                    var value = $(this).text();
                    changeInuranceAmount(id, value);
                });

                $('.insurance_amount').keydown(function(event){
                    if(event.keyCode == 8 || event.keyCode == 46)
                        return true;
                    if(event.keyCode >= 96 && event.keyCode <= 105)
                        return true;
                    if(isNaN(parseInt(String.fromCharCode(event.keyCode),10)))
                    return false;
                });

                var is_imc_disabled = $('#is_imc_disabled').val();
                if(is_imc_disabled == 1){
                    $('#imc_calculation_area').find('input, textarea, button, select').attr('disabled','disabled');
                    $('#imc_calculation_area').css('opacity','0.5');
                }
                var is_anes_disabled = $('#is_anes_disabled').val();
                if(is_anes_disabled == 1){
                    $('#anesthesia_calculation_area').find('input, textarea, button, select').attr('disabled','disabled');
                    $('#anesthesia_calculation_area').css('opacity','0.5');
                }

            }
        });
    }

    function exceller() {
        $(".theadfix_wrapper").floatThead('destroy');
        $('#total_row').css('display', '');
        $('.hospital_header_excel').css('visibility', 'hidden');
        $('.hospital_header_excel').css('display', '');
        $('.additional_amount_text').remove();
        $('.additional_amount_check').remove();
        $('.insurance_amount_text').remove();
        $('.insurance_amount_check').remove();
        setTimeout(function() {
            var template_date = $('#exceller_data').val();
            var uri = 'data:application/vnd.ms-excel;base64,',
                template = atob(template_date),
                base64 = function(s) {
                    return window.btoa(unescape(encodeURIComponent(s)))
                },
                format = function(s, c) {
                    return s.replace(/{(\w+)}/g, function(m, p) {
                        return c[p];
                    })
                }
            var toExcel = document.getElementById("excelContainer").innerHTML;
            console.log(toExcel);
            var ctx = {
                worksheet: name || '',
                table: toExcel
            };
            var link = document.createElement("a");
            var menu_name = '{{$menu_name}}';
            var current_time = '{{$current_date_time}}';
            link.download = menu_name+'-'+current_time+".xls";
            link.href = uri + base64(format(template, ctx))
            link.click();

            getResultData();

            InitTheadScroll();
            $('#total_row').css('display', 'none');
            $('.hospital_header_excel').css('display', 'none');
            $('.hospital_header_excel').css('visibility', '');
        }, 1000);

    }

    $('.nav-tabs').find('li').find('a').on('click', function(){
        setTimeout(function () {
            getResultData();
        },500);

        setTimeout(function () {
            $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30
                });

            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table){
                    return $table.closest('.theadscroll');
                }
            });

        },1000);
    });



</script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/doctor_salary.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>

@endsection
