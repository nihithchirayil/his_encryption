<style>
    @media print {
        .theadscroll {
            max-height: none;
            overflow: visible;
        }

        .header_bg {
            background-color: #08898d;
            color: white;
        }

        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #f2fef5;
        }

        #resultDataTable {
            border: 1px solid #cedfe9 !important;
            border-collapse: collapse !important;
        }

        td {
            padding-left: 5px !important;
        }
    }

    .global-footer {
        position: fixed;
        width: 90.5%;
        bottom: 38px;
        /* left: 0; */
        /* padding: 1em */
    }
</style>

<div class="col-md-12" style="margin-bottom:15px;">
    @if ($status == 1)
        @if (sizeof($result_data) > 0)
            <div id="ResultsPrintViewArea">
                @php
                    $head_array = ['uhid', 'patient_name', 'doctor_name', 'bill_no', 'bill_tag', 'bill_no', 'company', 'tds_amount', 'net_amount', 'settled_amt', 'settled_date', 'rem_amt'];
                    // print_r($head_array);
                    // exit;
                    $total_colums = explode(',', $flexy_report_totals);
                    $total_array = [];
                    $total_array_index = [];
                    $drill_down_array = [];
                    $size_array = [];
                    $j = 0;
                    
                    $head_array_size = count(array_keys((array) $head_array));
                    $custom_column_width = explode(',', $custom_column_width);
                    $drill_down_columns = str_replace('@', '', $drill_down_columns);
                    $drill_down_columns = explode(',', $drill_down_columns);
                    $drill_down_values = '';
                    $numeric_colums = explode(',', $numeric_colums);
                    
                @endphp
                <span class="no-padding" id="excelContainer">
                    <div class="theadscroll no-padding" id="table_container_div"
                        style="position: relative; max-height: 400px;">
                        <table border='0'; width="100%;" class=" no-padding"
                            style="border-collapse: collapse;table-layout:fixed;border-color:white;">
                            <tr class="table_header_row">
                                <th style="text-align:left;font-weight:100 !important;"
                                    colspan="{{ $head_array_size + 1 }}">
                                    Report Print Date: {{ date('M-d-Y h:i A') }}
                                </th>
                            </tr>
                            <tr class="table_header_row">
                                <th style="text-align:left;font-weight:100 !important;"
                                    colspan="{{ $head_array_size + 1 }}">
                                    Report Date: {{ $from_date }} To {{ $to_date }}
                                </th>
                            </tr>
                            <tr class="table_header_row">
                                <th style="text-align:left;font-weight:100 !important;"
                                    colspan="{{ $head_array_size + 1 }}">
                                    Number Of Records: {{ count($result_data) }}
                                </th>
                            </tr>
                            @php
                                $company_details = \DB::table('company')
                                    ->where('id', 1)
                                    ->first();
                            @endphp
                            <tr class="hospital_header_excel" style="display:none;">
                                <td style="text-align: center !important;" colspan="{{ $head_array_size + 1 }}">
                                    <span><strong>{{ $company_details->name }}</strong></span><br>
                                    <span>{{ $company_details->address }}</span><br>
                                    <span>Phone :{{ $company_details->phone }}</span>
                                    <span>Booking No :{{ $company_details->booking_no }}</span>
                                </td>
                            </tr>
                            <tr class="table_header_row no-padding" style="background-color: #07ad8c;color: white;">
                                <th style="font-size: 18px !important;text-align:center;"
                                    colspan="{{ $head_array_size + 1 }}">
                                    {{ $menu_name }}
                                </th>
                            </tr>
                        </table>
                        <table id="resultDataTable" border="1" width="100%;"
                            class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding"
                            style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;">

                            <thead>
                                <tr style="background-color:#07ad8c; color: white;"
                                    class="__web-inspector-hide-shortcut__">
                                    <th style="width:5%">Sl.No</th>
                                    <th width="20%">UHID</th>
                                    <th width="10%">Patient Name</th>
                                    <th width="15%">Doctor Name</th>
                                    <th width="15%">Bill No</th>
                                    <th width="10%">Bill Tag</th>
                                    <th width="10%">Bill Date</th>
                                    <th width="15%">Company</th>
                                    <th width="10%">Tds Amount</th>
                                    <th width="10%">Net Amount</th>
                                    <th width="10%">Settled Amt</th>
                                    <th width="10%">Settled Date</th>
                                    <th width="10%">Rem Amt</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                    $tds_amount_total=0;
                                    $net_amount_total=0;
                                    $settled_amt_total=0;
                                    $rem_amt_total=0;
                                    
                                @endphp

                                @foreach ($result_data as $key => $value)
                                    @php
                                    $bill_div_id =  str_replace("/","_",$value['bill_no']);
                                    @endphp
                                    <tr  onclick="checkDetail('{{ $bill_div_id }}')" style="cursor: pointer">
                                        <td>{{ $i }}</td>
                                        <td class="common_td_rules">{{ $value['uhid'] }}</td>
                                        <td class="common_td_rules">{{ $value['patient_name'] }}</td>
                                        <td class="common_td_rules">{{ $value['doctor_name'] }}</td>
                                        <td class="common_td_rules">{{ $value['bill_no'] }}</td>
                                        <td class="common_td_rules">{{ $value['bill_tag'] }}</td>
                                        <td class="common_td_rules">{{ $value['bill_date'] }}</td>
                                        <td class="common_td_rules">{{ $value['company'] }}</td>
                                        <td class="td_common_numeric_rules">{{ $value['tds_amount'] }}</td>
                                        <td class="td_common_numeric_rules">{{ round($value['net_amount']) }}</td>
                                        <td class="td_common_numeric_rules">{{ $value['settled_amt'] }}</td>
                                        <td class="td_common_numeric_rules">{{ $value['settled_date'] }}</td>
                                        <td class="td_common_numeric_rules">{{ $value['rem_amt'] }}</td>


                                    </tr>
                                    <tr class="detailed_div" style="display: none" id="detailed_{{ $bill_div_id }}">
                                        <td colspan="12">
                                            <table border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm  no-padding">
                                                <thead>
                                                    <tr style="background-color:#81bbde; color: white;">
                                                        <th width="5%">SL.No</th>
                                                        <th width="60%">Item Desc</th>
                                                        <th width="20%">Quantity</th>
                                                        <th width="20%">Price</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $total_inner=0;
                                                        $total_inner_qty=0;
                                                    @endphp
                                                    @foreach ($value['detail_array'] as $key => $item)
                                                        <tr>
                                                            <td class="common_td_rules">{{ $key + 1 }}</td>
                                                            <td class="common_td_rules">{{ $item['item_desc'] }}</td>
                                                            <td class="td_common_numeric_rules">{{ $item['quantity'] }}</td>
                                                            <td class="td_common_numeric_rules">{{ $item['item_price'] }}</td>
                                                        </tr>
                                                        @php
                                                            $total_inner+=$item['item_price'];
                                                            $total_inner_qty+=$item['quantity'];
                                                        @endphp
                                                    @endforeach
                                                    <tr><td><b>total</b></td><td></td><td class="td_common_numeric_rules"><b>{{ $total_inner_qty }}</b></td><td class="td_common_numeric_rules"><b>{{ $total_inner }}</b></td></tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                    @php
                                        $i++;
                                        $tds_amount_total+=$value['tds_amount'];
                                        $net_amount_total+=$value['net_amount'];
                                        $settled_amt_total+=$value['settled_amt'];
                                        $rem_amt_total+=$value['rem_amt'];

                                    @endphp
                                @endforeach
                                  <tr><td><b>total</b></td><td colspan="7"></td><td class="td_common_numeric_rules"><b>{{ $tds_amount_total }}</b></td><td class="td_common_numeric_rules"><b>{{ $net_amount_total }}</b></td><td class="td_common_numeric_rules"><b>{{ $settled_amt_total }}</b></td><td></td><td class="td_common_numeric_rules"><b>{{ $rem_amt_total }}</b></td></tr>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                </span>
            </div>
        @else
            <div id="ResultsPrintViewArea" class="col-md-12">
                <h5 style="color:red;">No Results Found!</h5>
            </div>
        @endif
    @else
        <div class="alert alert-danger">
            {{ $result_data }}
        </div>
    @endif
</div>
