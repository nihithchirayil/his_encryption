<div class="row">
    <div class="col-md-12 padding_sm" id="result_detail_container_div">
        <table id="result_data_detail_table" class='table table-striped table-bordered table-condensed table_sm'
            style="width:100%; font-size: 12px;">
            <thead>
                <tr>
                    <th style="background-color:#07ad8c; color: black;"  width='5%'>SL. No.</th>
                    <th style="background-color:#07ad8c; color: black;" width='30%'>UHID</th>
                    <th style="background-color:#07ad8c; color: black;" width='35%'>Patient Name</th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Total Advance</th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Refund Amount </th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Net Amount</th>
                </tr>
            </thead>

            <tbody>
                @if (count($result_data) != 0)
                    @php
                        $i = 1;
                        $total_net_amount = 0;
                        $total_refund_amount = 0;
                        $total_gross_amount = 0;
                    @endphp
                    @foreach ($result_data as $data)
                        @php
                            $net_amount = floatval($data->total_advance) - floatval($data->refund_amount);
                        @endphp
                        <tr>
                            <td class="common_td_rules">{{ $i }}</td>
                            <td class="common_td_rules">{{ $data->uhid }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="td_common_numeric_rules">{{ $data->total_advance }}</td>
                            <td class="td_common_numeric_rules">{{ $data->refund_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $net_amount }}</td>
                        </tr>
                        @php
                            $i++;
                            $total_refund_amount += floatval($data->refund_amount);
                            $total_gross_amount += floatval($data->total_advance);
                            $total_net_amount += floatval($net_amount);
                        @endphp
                    @endforeach
                    <tr>
                        <th class="common_td_rules" colspan="3">Total</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_gross_amount, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_refund_amount, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_net_amount, 2, '.', '') }}</th>
                    </tr>
                @else
                    <tr>
                        <th colspan="5" style="text-align: center;">No records Found!</th>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
