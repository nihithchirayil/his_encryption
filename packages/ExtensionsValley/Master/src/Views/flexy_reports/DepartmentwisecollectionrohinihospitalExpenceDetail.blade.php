<div class="row">
    <div class="col-md-12 padding_sm" id="result_detail_container_div">
        <table id="result_data_detail_table" class='table table-striped table-bordered table-condensed table_sm'
            style="width:100%; font-size: 12px;">
            <thead>
                <tr>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>SL. No.</th>
                    <th style="background-color:#07ad8c; color: black;" width='20%'>Voucher No</th>
                    <th style="background-color:#07ad8c; color: black;" width='20%'>Bill No</th>
                    <th style="background-color:#07ad8c; color: black;" width='20%'>Expense Type</th>
                    <th style="background-color:#07ad8c; color: black;" width='20%'>Expense Date</th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Net Amt.</th>

                </tr>
            </thead>

            <tbody>
                @if (count($result_data) != 0)
                    @php
                        $i = 1;
                        $total_net_amount = 0;
                    @endphp
                    @foreach ($result_data as $data)
                        @php
                        @endphp
                        <tr>
                            <td class="common_td_rules">{{ $i }}</td>
                            <td class="common_td_rules">{{ $data->voucher_no }}</td>
                            <td class="common_td_rules">{{ $data->bill_no }}</td>
                            <td class="common_td_rules">{{ $data->expence_name }}</td>
                            <td class="common_td_rules">{{ $data->expence_date }}</td>
                            <td class="td_common_numeric_rules">{{ $data->net_amount }}</td>

                        </tr>
                        @php
                            $i++;
                            $total_net_amount += floatval($data->net_amount);
                        @endphp
                    @endforeach
                    <tr>
                        <th class="common_td_rules" colspan="5">Total</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_net_amount, 2, '.', '') }}</th>
                    </tr>
                @else
                    <tr>
                        <th colspan="6" style="text-align: center;">No records Found!</th>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
