<div class="row">
  <div class="col-md-12" id="result_container_div">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b>{{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}} </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?=$from?> To <?=$to?></p>
            <?php
                $collect = collect($res); $total_records=count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Speciality Wise Pharmacy Sales Detail Report </b></h4>
            <font size="16px" face="verdana" >
                    <table class="table no-margin table_sm  table-striped no-border table-condensed"
                        id="result_data_table" width='100%'>
      <thead>
    <tr class="table_header_bg" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th width="15%;">Doctor Name</th>
                <th width="15%;">Speciality</th>
                <th width="15%;">IP Total</th>
                <th width="15%;">OP Total</th>
                <th width="15%;">Insurance Total</th>
                <th width="15%;">Cash Total</th>
                <th width="10%;">Net Amount</th>
            </tr>
    </thead>
    @if(sizeof($res)>0)
    <tbody>
        <?php
        $total_amount = 0.0;
        $net_amount = 0.0;
        $cash_amount = 0.0;
        $ins_amount = 0.0;
        $ip_amount = 0.0;
        $op_amount = 0.0;
        foreach ($res as $data){
                $net_amount = $data->cash_total+$data->insurance_total;
                $total_amount += $net_amount;
                $cash_amount += $data->cash_total;
                $ins_amount += $data->insurance_total;
                $ip_amount +=  $data->ip_total;
                $op_amount += $data->op_total;
              ?>

            <tr>
                <td class="common_td_rules">{{$data->doctor_name}}</td>
                <td class="common_td_rules">{{$data->speciality}}</td>
                <td class="td_common_numeric_rules">{{ $data->ip_total }}</td>
                <td class="td_common_numeric_rules">{{ $data->op_total }}</td>
                <td class="td_common_numeric_rules">{{ $data->insurance_total }}</td>
                <td class="td_common_numeric_rules">{{ $data->cash_total }}</td>
                <td class="td_common_numeric_rules">{{ $net_amount }}</td>
            </tr>
            <?php
        }

        ?>
        <tr style="height: 30px;background-color:rgb(230 218 173);color:black;border-spacing: 0 1em;font-family:sans-serif">
            <th colspan="2" class="common_td_rules">Total</th>
            <th class="td_common_numeric_rules"><?=number_format((float)$ip_amount, 2, '.', '')?></th>
            <th class="td_common_numeric_rules"><?=number_format((float)$op_amount, 2, '.', '')?></th>
            <th class="td_common_numeric_rules"><?=number_format((float)$ins_amount, 2, '.', '')?></th>
            <th class="td_common_numeric_rules"><?=number_format((float)$cash_amount, 2, '.', '')?></th>
            <th class="td_common_numeric_rules"><?=number_format((float)$total_amount, 2, '.', '')?></th>
        </tr>
    </tbody>
    @else
    <tr>
        <td colspan="4" style="text-align: center;">No Results Found!</td>
    </tr>
    @endif

</table>
</font>
</div>
</div>
