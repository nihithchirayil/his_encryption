<div class="row">
    <div class="col-md-12 padding_sm" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date:<b> <?= $from_date ?> To <?= $to_date ?> </b></p>
        <?php
        $collect = collect($result_data);
        $total_records = count($collect);
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b>Department Wise Collection Rohini Hospital</b>
        </h4>
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-striped table-bordered table-condensed table_sm'
                style="width:100%; font-size: 12px;">
                <thead>
                    <tr>
                        <th style="background-color:#07ad8c; color: black;" width='5%'>SL. No.</th>
                        <th style="background-color:#07ad8c; color: black;" width='35%'>Dept Name</th>
                        <th style="background-color:#07ad8c; color: black;" width='10%'>Gross Amount</th>
                        <th style="background-color:#07ad8c; color: black;" width='10%'>Refund </th>
                        <th style="background-color:#07ad8c; color: black;" width='10%'>Expence </th>
                        <th style="background-color:#07ad8c; color: black;" width='10%'>Advance Adj. </th>
                        <th style="background-color:#07ad8c; color: black;" width='10%'>Net Amount</th>
                        <th style="background-color:#07ad8c; color: black;" width='5%'><i class="fa fa-list"></i>
                        </th>
                        <th style="background-color:#07ad8c; color: black;" width='5%'><i class="fa fa-list"></i>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($result_data) != 0)
                        @php
                            $i = 1;
                            $total_gross_amount=0;
                            $total_refund_amount_amount=0;
                            $total_expence_amount_amount=0;
                            $total_advance_adjusted_amount=0;
                            $total_net_amount=0;
                        @endphp
                        @foreach ($result_data as $data)
                            <tr>
                                <td class="common_td_rules">{{ $i }}</td>
                                <td class="common_td_rules">{{ $data->department }}</td>
                                <td class="td_common_numeric_rules">{{  number_format($data->gross_amount, 2, '.', '') }}</td>
                                <td class="td_common_numeric_rules">{{  number_format($data->refund_amount, 2, '.', '') }}</td>
                                <td class="td_common_numeric_rules">{{  number_format($data->expence, 2, '.', '') }}</td>
                                <td class="td_common_numeric_rules">{{  number_format($data->advance_adjusted, 2, '.', '') }}</td>
                                <td class="td_common_numeric_rules">{{  number_format($data->net_amount, 2, '.', '') }}</td>
                                @if (trim($data->department) != 'ADVANCE IP')
                                    <td style="text-align: center"><button style="padding: 0px 7px"
                                            id="patient_detail_data_list1{{ $i }}" type="button"
                                            onclick="getDrillDownData('{{ trim($data->department) }}',1,{{ $i }},1)"
                                            class="btn btn-primary"><i id="patient_detail_data_spin1{{ $i }}"
                                                class="fa fa-list"></i></button>
                                    </td>
                                @else
                                    <td style="text-align: center">-</td>
                                @endif
                                @if (trim($data->department) == 'ADVANCE IP' || trim($data->department) == 'DISCHARGE')
                                    <td style="text-align: center"><button style="padding: 0px 7px"
                                            id="patient_detail_data_list2{{ $i }}" type="button"
                                            onclick="getDrillDownData('{{ trim($data->department) }}',2,{{ $i }},1)"
                                            class="btn btn-warning"><i
                                                id="patient_detail_data_spin2{{ $i }}"
                                                class="fa fa-list"></i></button>
                                    </td>
                                @else
                                    <td style="text-align: center">-</td>
                                @endif
                            </tr>
                            @php
                            $i++;
                            $total_refund_amount_amount += floatval($data->refund_amount);
                            $total_expence_amount_amount += floatval($data->expence);
                            $total_gross_amount += floatval($data->gross_amount);
                            $total_advance_adjusted_amount += floatval($data->advance_adjusted);
                            $total_net_amount += floatval($data->net_amount);
                        @endphp
                        @endforeach
                        <tr>
                        <th class="common_td_rules" colspan="2">Total</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_gross_amount, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_refund_amount_amount, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_expence_amount_amount, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_advance_adjusted_amount, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_net_amount, 2, '.', '') }}</th>
                        <th>-</th>
                        <th>-</th>
                    </tr>
                    @else
                        <tr>
                            <td colspan="8" style="text-align: center;">No records Found!</td>
                        </tr>
                    @endif
                </tbody>
            </table>

            {{------------------ IP CREDIT ---------------------}}
            <h4 style="text-align: center;"><b>Ip Credit</b></h4>
            <table id="result_data_table" class='table table-striped table-bordered table-condensed table_sm'
            style="width:100%; font-size: 12px;">
            <thead>
                <tr>
                    <th style="background-color:#07ad8c; color: black;" width='5%'>SL. No.</th>
                    <th style="background-color:#07ad8c; color: black;" width='35%'>Dept Name</th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Gross Amount</th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Refund </th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Expence </th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Advance Adj. </th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Net Amount</th>
                    <th style="background-color:#07ad8c; color: black;" width='5%'><i class="fa fa-list"></i>
                    </th>
                    <th style="background-color:#07ad8c; color: black;" width='5%'><i class="fa fa-list"></i>
                    </th>
                </tr>
            </thead>

            <tbody>
                @if (count($res1) != 0)
                    @php
                        $i = 1;
                        $total_gross_amount=0;
                        $total_refund_amount_amount=0;
                        $total_expence_amount_amount=0;
                        $total_advance_adjusted_amount=0;
                        $total_net_amount=0;
                    @endphp
                    @foreach ($res1 as $data)
                        <tr>
                            <td class="common_td_rules">{{ $i }}</td>
                            <td class="common_td_rules">{{ $data->department }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($data->gross_amount, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($data->refund_amount, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($data->expence, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($data->advance_adjusted, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($data->net_amount, 2, '.', '') }}</td>
                            @if (trim($data->department) != 'ADVANCE IP')
                                <td style="text-align: center"><button style="padding: 0px 7px"
                                        id="patient_detail_data_list1{{ $i }}" type="button"
                                        onclick="getDrillDownData('{{ trim($data->department) }}',1,{{ $i }},2)"
                                        class="btn btn-primary"><i id="patient_detail_data_spin1{{ $i }}"
                                            class="fa fa-list"></i></button>
                                </td>
                            @else
                                <td style="text-align: center">-</td>
                            @endif
                            @if (trim($data->department) == 'ADVANCE IP' || trim($data->department) == 'DISCHARGE')
                                <td style="text-align: center"><button style="padding: 0px 7px"
                                        id="patient_detail_data_list2{{ $i }}" type="button"
                                        onclick="getDrillDownData('{{ trim($data->department) }}',2,{{ $i }},2)"
                                        class="btn btn-warning"><i
                                            id="patient_detail_data_spin2{{ $i }}"
                                            class="fa fa-list"></i></button>
                                </td>
                            @else
                                <td style="text-align: center">-</td>
                            @endif
                        </tr>
                        @php
                        $i++;
                        $total_refund_amount_amount += floatval($data->refund_amount);
                        $total_expence_amount_amount += floatval($data->expence);
                        $total_gross_amount += floatval($data->gross_amount);
                        $total_advance_adjusted_amount += floatval($data->advance_adjusted);
                        $total_net_amount += floatval($data->net_amount);
                    @endphp
                    @endforeach
                    <tr>
                    <th class="common_td_rules" colspan="2">Total</th>
                    <th class="td_common_numeric_rules">{{ number_format($total_gross_amount, 2, '.', '') }}</th>
                    <th class="td_common_numeric_rules">{{ number_format($total_refund_amount_amount, 2, '.', '') }}</th>
                    <th class="td_common_numeric_rules">{{ number_format($total_expence_amount_amount, 2, '.', '') }}</th>
                    <th class="td_common_numeric_rules">{{ number_format($total_advance_adjusted_amount, 2, '.', '') }}</th>
                    <th class="td_common_numeric_rules">{{ number_format($total_net_amount, 2, '.', '') }}</th>
                    <th>-</th>
                    <th>-</th>
                </tr>
                @else
                    <tr>
                        <td colspan="8" style="text-align: center;">No records Found!</td>
                    </tr>
                @endif
            </tbody>
            </table>


            {{------------------ NOT PAID ---------------------}}
            <h4 style="text-align: center;"><b>Not Paid</b></h4>
            <table id="result_data_table" class='table table-striped table-bordered table-condensed table_sm'
            style="width:100%; font-size: 12px;">
            <thead>
                <tr>
                    <th style="background-color:#07ad8c; color: black;" width='5%'>SL. No.</th>
                    <th style="background-color:#07ad8c; color: black;" width='35%'>Dept Name</th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Gross Amount</th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Refund </th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Expence </th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Advance Adj. </th>
                    <th style="background-color:#07ad8c; color: black;" width='10%'>Net Amount</th>
                    <th style="background-color:#07ad8c; color: black;" width='5%'><i class="fa fa-list"></i>
                    </th>
                    <th style="background-color:#07ad8c; color: black;" width='5%'><i class="fa fa-list"></i>
                    </th>
                </tr>
            </thead>

            <tbody>
                @if (count($res2) != 0)
                    @php
                        $i = 1;
                        $total_gross_amount=0;
                        $total_refund_amount_amount=0;
                        $total_expence_amount_amount=0;
                        $total_advance_adjusted_amount=0;
                        $total_net_amount=0;
                    @endphp
                    @foreach ($res2 as $data)
                        <tr>
                            <td class="common_td_rules">{{ $i }}</td>
                            <td class="common_td_rules">{{ $data->department }}</td>
                            <td class="td_common_numeric_rules">{{  number_format($data->gross_amount, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{  number_format($data->refund_amount, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{  number_format($data->expence, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{  number_format($data->advance_adjusted, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{  number_format($data->net_amount, 2, '.', '') }}</td>
                            @if (trim($data->department) != 'ADVANCE IP')
                                <td style="text-align: center"><button style="padding: 0px 7px"
                                        id="patient_detail_data_list1{{ $i }}" type="button"
                                        onclick="getDrillDownData('{{ trim($data->department) }}',1,{{ $i }},3)"
                                        class="btn btn-primary"><i id="patient_detail_data_spin1{{ $i }}"
                                            class="fa fa-list"></i></button>
                                </td>
                            @else
                                <td style="text-align: center">-</td>
                            @endif
                            @if (trim($data->department) == 'ADVANCE IP' || trim($data->department) == 'DISCHARGE')
                                <td style="text-align: center">
                                    <button style="padding: 0px 7px"
                                        id="patient_detail_data_list2{{ $i }}" type="button"
                                        onclick="getDrillDownData('{{ trim($data->department) }}',2,{{ $i }},3)"
                                        class="btn btn-warning"><i
                                            id="patient_detail_data_spin2{{ $i }}"
                                            class="fa fa-list"></i>
                                    </button>
                                </td>
                            @else
                                <td style="text-align: center">-</td>
                            @endif
                        </tr>
                        @php
                        $i++;
                        $total_refund_amount_amount += floatval($data->refund_amount);
                        $total_expence_amount_amount += floatval($data->expence);
                        $total_gross_amount += floatval($data->gross_amount);
                        $total_advance_adjusted_amount += floatval($data->advance_adjusted);
                        $total_net_amount += floatval($data->net_amount);
                    @endphp
                    @endforeach
                    <tr>
                    <th class="common_td_rules" colspan="2">Total</th>
                    <th class="td_common_numeric_rules">{{ number_format($total_gross_amount, 2, '.', '') }}</th>
                    <th class="td_common_numeric_rules">{{ number_format($total_refund_amount_amount, 2, '.', '') }}</th>
                    <th class="td_common_numeric_rules">{{ number_format($total_expence_amount_amount, 2, '.', '') }}</th>
                    <th class="td_common_numeric_rules">{{ number_format($total_advance_adjusted_amount, 2, '.', '') }}</th>
                    <th class="td_common_numeric_rules">{{ number_format($total_net_amount, 2, '.', '') }}</th>
                    <th>-</th>
                    <th>-</th>
                </tr>
                @else
                    <tr>
                        <td colspan="8" style="text-align: center;">No records Found!</td>
                    </tr>
                @endif
            </tbody>
            </table>


        </font>
    </div>
</div>
