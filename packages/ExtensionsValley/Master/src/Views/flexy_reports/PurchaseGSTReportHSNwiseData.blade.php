<div id="result_container_div" class="theadscroll" style="position: relative; height: 430px;">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <th class="common_td_rules" colspan="19">
                <strong> Purchase </strong>
            </th>
            <tr class="table_header_bg" style="cursor: pointer;">
                <th>SL.No</th>
                <th>PUR IN.NO</th>
                <th>SUPL INV.NO</th>
                <th>SUPL INV DATE</th>
                <th>SUPPLIER</th>
                <th>GST NO</th>
                <th>Item Desc</th>
                <th>HSN Code</th>
                <th>Qty</th>
                <th>GST %</th>
                <th>PURCHASE AMOUNT</th>
                <th>CGST %</th>
                <th>CGST AMOUNT</th>
                <th>SGST %</th>
                <th>SGST AMOUNT</th>
                <th>IGST %</th>
                <th>IGST AMOUNT</th>
                <th>DISCOUNT</th>
                <th>BILLAMT</th>
            </tr>
        </thead>
        <tbody>
            <?php

             if (count($purchase_head) != 0) {
                 $net_amount_tot=0.0;
                 $i=1;
                 foreach ($purchase_head as $each) {

                    // dd($each);
                    //  $tax_amt = ($each->PURCHASE0+$each->PURCHASE3+$each->CGST15+$each->SGST15+$each->IGST3+$each->PURCHASE5+$each->CGST25+$each->SGST25+$each->IGST5+$each->PURCHASE12+$each->CGST6+$each->SGST6+$each->IGST12+$each->PURCHASE18+$each->CGST9+$each->SGST9+$each->IGST18+$each->PURCHASE28+$each->CGST14+$each->SGST14+$each->IGST28+$each->CESS)-(floatval($each->discount));
                    //  $roundoff = floatval($tax_amt) -$each->net_amount;

                 ?>
            <tr>
                <td class="common_td_rules"><?= $i ?></td>
                <td class="common_td_rules"><?= $each->grn_no ?></td>
                <td class="common_td_rules"><?= $each->bill_no ?></td>
                <td class="common_td_rules"><?= $each->bill_date ?></td>
                <td class="common_td_rules"><?= $each->vendor_name ?></td>
                <td class="common_td_rules"><?= $each->gst_vendor_code ?></td>
                <td class="common_td_rules"><?= $each->item_desc ?></td>
                <td class="common_td_rules"><?= $each->hsn_code ?></td>
                <td class="common_td_rules"><?= $each->all_total_quantity ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->grn_tax_total_rate) ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->purchase_amount) ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->grn_tax_total_rate) / 2 ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->tax_amount)/2 ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->grn_tax_total_rate) / 2 ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->tax_amount)/2 ?></td>
                <td class="td_common_numeric_rules">@if($each->is_igst == 1)<?= floatval($each->grn_tax_total_rate) ?> @else 0 @endif</td>
                <td class="td_common_numeric_rules"><?= floatval($each->igst_amount) ?></td>
                <td class="td_common_numeric_rules"><?= $each->bill_discount_amount ?></td>
                <td class="td_common_numeric_rules"><?= $each->net_amount ?></td>
                </td>
            </tr>
            <?php
            $i++;
             }
         } else {
             ?>
            <tr class="common_td_rules">
                <td colspan="33" style="text-align: center;"> No Result Found</td>
            </tr>
            <?php
         }
         ?>

        </tbody>



        <thead>
            <th class="common_td_rules" colspan="19">
                <strong> Purchase Return </strong>
            </th>
            <tr class="table_header_bg" style="cursor: pointer;">
                <th>SL.No</th>
                <th>PUR IN.NO</th>
                <th>SUPL INV.NO</th>
                <th>SUPL INV DATE</th>
                <th>SUPPLIER</th>
                <th>GST NO</th>
                <th>Item Desc</th>
                <th>HSN Code</th>
                <th>Qty</th>
                <th>GST %</th>
                <th>PURCHASE AMOUNT</th>
                <th>CGST %</th>
                <th>CGST AMOUNT</th>
                <th>SGST %</th>
                <th>SGST AMOUNT</th>
                <th>IGST %</th>
                <th>IGST AMOUNT</th>
                <th>DISCOUNT</th>
                <th>BILLAMT</th>
            </tr>
        </thead>
        <tbody>
            <?php

             if (count($purchase_return) != 0) {
                 $net_amount_tot=0.0;
                 $i=1;
                 foreach ($purchase_return as $each) {
                    //  $tax_amt = ($each->PURCHASE0+$each->PURCHASE3+$each->CGST15+$each->SGST15+$each->IGST3+$each->PURCHASE5+$each->CGST25+$each->SGST25+$each->IGST5+$each->PURCHASE12+$each->CGST6+$each->SGST6+$each->IGST12+$each->PURCHASE18+$each->CGST9+$each->SGST9+$each->IGST18+$each->PURCHASE28+$each->CGST14+$each->SGST14+$each->IGST28+$each->CESS)-(floatval($each->discount));
                    //  $roundoff = floatval($tax_amt) -$each->net_amount;

                 ?>
            <tr>
                <td class="common_td_rules"><?= $i ?></td>
                <td class="common_td_rules"><?= $each->purchase_return_no ?></td>
                <td class="common_td_rules"><?= $each->invoice_no ?></td>
                <td class="common_td_rules"><?= $each->credit_approved_date ?></td>
                <td class="common_td_rules"><?= $each->vendor_name ?></td>
                <td class="common_td_rules"><?= $each->gst_vendor_code ?></td>
                <td class="common_td_rules"><?= $each->item_desc ?></td>
                <td class="common_td_rules"><?= $each->hsn_code ?></td>
                <td class="common_td_rules"><?= $each->quantity ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->grn_tax_total_rate) ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->purchase_amount) ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->grn_tax_total_rate) / 2 ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->tax_amount)/2 ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->grn_tax_total_rate) / 2 ?></td>
                <td class="td_common_numeric_rules"><?= floatval($each->tax_amount)/2 ?></td>
                <td class="td_common_numeric_rules">@if($each->is_igst == 1)<?= floatval($each->grn_tax_total_rate) ?> @else 0 @endif</td>
                <td class="td_common_numeric_rules"><?= floatval($each->igst_amount) ?></td>
                <td class="td_common_numeric_rules"><?= $each->bill_discount_amount ?></td>
                <td class="td_common_numeric_rules"><?= $each->net_amount ?></td>
                </td>
            </tr>
            <?php
            $i++;
             }
         } else {
             ?>
            <tr class="common_td_rules">
                <td colspan="33" style="text-align: center;"> No Result Found</td>
            </tr>
            <?php
         }
         ?>

        </tbody>
    </table>
</div>
