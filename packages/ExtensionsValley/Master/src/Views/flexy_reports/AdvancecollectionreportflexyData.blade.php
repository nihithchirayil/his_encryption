<p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>

        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b>Advance Collection Report</b></h4>
<style>
    @media print {
        .theadscroll {max-height: none; overflow: visible;}
        .header_bg {
            background-color: #08898d;
            color: white;
        }
        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #f2fef5;
        }

        #resultDataTable{
            border: 1px solid #cedfe9 !important;
            border-collapse: collapse !important;
        }
        td{
            padding-left: 5px !important;
        }
        }
</style>

<div class="col-md-12" style="margin-bottom:20px;">
    @if($status == 1)
        @if(sizeof($result_data)>0)
        <div class="theadscroll" id="table_container_div" style="position: relative; max-height: 500px;">
            <table id="resultDataTable" border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper" style="border-collapse: collapse;">
                <thead>
                    <tr>
                        @php
                            $head_array = $result_data[0];
                            $total_colums = explode(",", $flexy_report_totals);
                            $total_array = [];
                            $total_array_index = [];
                            $drill_down_array = [];
                            $j = 0;
                            $head_array_size = count(array_keys((array)$head_array));
                            $custom_column_width = explode(',', $custom_column_width);
                            $drill_down_columns = str_replace("@","",$drill_down_columns);
                            $drill_down_columns = explode(',', $drill_down_columns);
                            $drill_down_values = '';

                        @endphp

                            <th style="width:10%" class="header_bg">Sl.No</th>
                        @foreach($head_array as $key => $value)
                            @php
                                if(in_array($key, $total_colums)){
                                    $total_array[$key] = 0;
                                    $total_array_index[$key] = $j;
                                }
                                $key =  str_replace("_"," ",$key);
                                if($key=='uhid'){
                                    $key = strtoupper($key); 
                                }
                                else {
                                    $key = ucwords($key);
                                }
                                $j++;
                            @endphp

                            @if(substr($key, 0, 1) != '@') <!--- if first letter is not @  then only show-->
                                <th style="@if(isset($custom_column_width[$j-1]))width:{{$custom_column_width[$j-1]}}% @endif" class="header_bg">{{$key}}</th>
                            @endif
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;

                    @endphp
                    @foreach($result_data as $key => $value)


                        <!---drill drill_down_columns--->
                        @php
                            $drill_down_values = '';
                             if(!empty($drill_down_columns)){

                                $value2 = (array) $value;
                                $new_array = [];
                                foreach($value2 as $drill_key=>$drill_value){

                                    $new_key = str_replace("@","",$drill_key);
                                    $new_value = str_replace("@","",$drill_value);

                                    if($new_key != $drill_key){
                                        $value2[$new_key] = $new_value;
                                        $new_array[$new_key] = $new_value;
                                    } else {
                                        $new_array[$drill_key] = $drill_value;
                                    }

                                }
                                if($has_drill_down ==1){
                                    foreach($drill_down_columns as $drill_value){
                                         $drill_down_values.= "'".$new_array[$drill_value]."',";
                                    }
                                    $drill_down_values = rtrim($drill_down_values, ",");
                                }

                             }
                        @endphp


                        <tr @if($has_drill_down == 1) onclick="@@@REPORTNAME@@@@DrilldownData({{$drill_down_values}});" @endif>
                            <td>{{$i}}</td>
                            @foreach($value as $key1 => $value1)
                                @if(substr($value1, 0, 1) != '@')
                                    @php
                                        if(is_numeric($value1)){
                                            $value1 = number_format($value1, 2);
                                            $cls = "td_common_numeric_rules";
                                        }else{
                                            $cls = "common_td_rules";
                                        }
                                    @endphp
                                    <td class="{{$cls}}">{{$value1}}</td>
                                @endif

                                @php
                                   if(in_array($key1,array_keys($total_array))){
                                        $total_array[$key1] += (int)$value1;
                                   }
                                @endphp

                            @endforeach
                        </tr>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                    </tr>
                    @if($flexy_report_totals !='')
                        <tr style="background-color: paleturquoise;">
                            <td><b>Total</b></td>
                            @foreach ($head_array as $key3 => $val3)
                                @if(in_array($key3,array_keys($total_array)))
                                    @if(substr($key3, 0, 1) != '@')
                                        <td class="td_common_numeric_rules"><b>{{number_format($total_array[$key3],2)}}</b></td>
                                    @endif
                                @else
                                    @if(substr($key3, 0, 1) != '@')
                                        <td style="text-align:left"><b></b></td>
                                    @endif
                                @endif
                            @endforeach
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        @else
        <div class="col-md-12"><h5 style="color:red;">No Results Found!</h5></div>
        @endif
    @else
    <div class="alert alert-danger">
        {{$result_data}}
    </div>
    @endif
