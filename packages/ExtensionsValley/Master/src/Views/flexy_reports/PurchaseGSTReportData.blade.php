<div class="col-md-12" id="result_container_div">
    <div class="col-md-12" id="PurchaseGSTReportData">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= date('M-d-Y', strtotime($from_date)) ?> To
            <?= date('M-d-Y', strtotime($to_date)) ?></p>

        <div class="col-md-12" style="margin-bottom:20px;">
            <div class="theadscroll" id="table_container_div" style="position: relative; max-height: 500px;">
                <table id="result_data_table" border="1" width="100%;"
                    class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper"
                    style="border-collapse: collapse;">
                    <tbody>


                        @if ($hospital_code == 'ALMA')
                            <tr class="headerclass"
                                style="background-color: rgb(165 169 165);color:#000;border-spacing: 0 1em;font-family:sans-serif">
                                <th colspan="41">Purchase GST Report</th>
                            </tr>
                            <tr class="headerclass"
                                style="background-color: rgb(165 169 165);color:#000;border-spacing: 0 1em;font-family:sans-serif">
                                <th width="width:5%">BILL NO(Invoice No)</th>
                                <th>PO No.</th>
                                <th>DATE</th>
                                <th>GSTIN</th>
                                <th>SUPPLIERNAME</th>
                                <th>PURCHASES MEDICINE 0%</th>
                                <th>PURCHASES MEDICINE 5%</th>
                                <th>INPUT CGST 2.5%</th>
                                <th>INPUT SGST 2.5%</th>
                                <th>Cess</th>
                                <th>Interstate Purchase 5%</th>
                                <th>IGST</th>
                                <th>Cess</th>
                                <th>PURCHASES MEDICINE 12%</th>
                                <th>INPUT CGST 6%</th>
                                <th>INPUT SGST 6%</th>
                                <th>Cess</th>
                                <th>IGST Purchase 12%</th>
                                <th>IGST</th>
                                <th>Cess</th>
                                <th>PURCHASES MEDICINE 18%</th>
                                <th>INPUT CGST 9%</th>
                                <th>INPUT SGST 9 %</th>
                                <th>Cess</th>
                                <th>IGST Purchase 18%</th>
                                <th>IGST</th>
                                <th>Cess</th>
                                <th>Purchase 28%</th>
                                <th>CGST</th>
                                <th>SGST</th>
                                <th>Cess</th>
                                <th>IGST Purchase 28%</th>
                                <th>IGST</th>
                                <th>Cess</th>
                                <th>Int Purchase Exempt</th>
                                <th>Net Amount</th>
                                <th>Voucher TypeName</th>
                                <th>State</th>
                                <th>Registration Type</th>
                                <th>Invoice Number</th>
                                <th>Invoice Date</th>
                            </tr>
                            @if (count($purchase_array) != 0)
                                <?php
                                $i = 1;
                                $PURCHASE0_total = 0.0;
                                $PURCHASE3 = 0.0;
                                $CGST15 = 0.0;
                                $SGST15 = 0.0;
                                $IGST3 = 0.0;
                                $PURCHASE5 = 0.0;
                                $CGST25 = 0.0;
                                $SGST25 = 0.0;
                                $IGST5 = 0.0;
                                $PURCHASE12 = 0.0;
                                $CGST6 = 0.0;
                                $SGST6 = 0.0;
                                $IGST12 = 0.0;
                                $PURCHASE18 = 0.0;
                                $CGST9 = 0.0;
                                $SGST9 = 0.0;
                                $IGST18 = 0.0;
                                $PURCHASE28 = 0.0;
                                $CGST14 = 0.0;
                                $SGST14 = 0.0;
                                $IGST28 = 0.0;
                                $roundoff_tot = 0.0;
                                $net_amount = 0.0;
                                $total_cgst = 0.0;
                                $total_sgst = 0.0;
                                $total_igst = 0.0;
                                $IGST_INTERSATEPURCHASE0 = 0.0;
                                $IGST_INTERSATEPURCHASE5 = 0.0;
                                $IGST_INTERSATEPURCHASE12 = 0.0;
                                $IGST_INTERSATEPURCHASE18 = 0.0;
                                $IGST_INTERSATEPURCHASE28 = 0.0;
                                $total_netpurchase = 0.0;
                                $total_nettax = 0.0;
                                ?>
                                @foreach ($purchase_array as $each)
                                    <?php
                                    $i++;
                                    $PURCHASE0_total += $each['PURCHASE0'];
                                    $PURCHASE3 += $each['PURCHASE3'];
                                    $CGST15 += $each['CGST15'];
                                    $SGST15 += $each['SGST15'];
                                    $IGST3 += $each['IGST3'];
                                    $PURCHASE5 += $each['PURCHASE5'];
                                    $CGST25 += $each['CGST25'];
                                    $SGST25 += $each['SGST25'];
                                    $IGST5 += $each['IGST5'];
                                    $PURCHASE12 += $each['PURCHASE12'];
                                    $CGST6 += $each['CGST6'];
                                    $SGST6 += $each['SGST6'];
                                    $IGST12 += $each['IGST12'];
                                    $PURCHASE18 += $each['PURCHASE18'];
                                    $CGST9 += $each['CGST9'];
                                    $SGST9 += $each['SGST9'];
                                    $IGST18 += $each['IGST18'];
                                    $PURCHASE28 += $each['PURCHASE28'];
                                    $CGST14 += $each['CGST14'];
                                    $SGST14 += $each['SGST14'];
                                    $IGST28 += $each['IGST28'];
                                    $IGST_INTERSATEPURCHASE0 += $each['IGST_INTERSATEPURCHASE0'];
                                    $IGST_INTERSATEPURCHASE5 += $each['IGST_INTERSATEPURCHASE5'];
                                    $IGST_INTERSATEPURCHASE12 += $each['IGST_INTERSATEPURCHASE12'];
                                    $IGST_INTERSATEPURCHASE18 += $each['IGST_INTERSATEPURCHASE18'];
                                    $IGST_INTERSATEPURCHASE28 += $each['IGST_INTERSATEPURCHASE28'];
                                    $roundoff_tot += $each['round_off'];
                                    $net_amount += $each['net_amount'];
                                    $total_cgst += $each['total_cgst'];
                                    $total_sgst += $each['total_sgst'];
                                    $total_igst += $each['total_igst'];
                                    $total_netpurchase += $each['total_purchase'];
                                    $total_nettax += $each['total_tax'];
                                    ?>
                                    <tr>
                                        <td class="common_td_rules">{{ $each['invoice_no'] }}</td>
                                        <td class="common_td_rules">{{ $each['po_no'] }}</td>
                                        <td class="common_td_rules">{{ $each['approved_date'] }}</td>
                                        <td class="common_td_rules">{{ $each['gst_vendor_code'] }}</td>
                                        <td class="common_td_rules">{{ $each['vendor_name'] }}</td>
                                        <td class="td_common_numeric_rules"><?= $each['PURCHASE0'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['PURCHASE5'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['CGST25'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['SGST25'] ?></td>
                                        <td class="td_common_numeric_rules"></td>
                                        <td class="td_common_numeric_rules"><?= @$each['IGST_INTERSATEPURCHASE5'] !=0 ? $each['IGST_INTERSATEPURCHASE5'] :' ' ?></td>
                                        <td class="td_common_numeric_rules"><?= @$each['IGST5'] !=0 ? $each['IGST5'] : ' ' ?></td>
                                        <td class="td_common_numeric_rules"></td>
                                        <td class="td_common_numeric_rules"><?= $each['PURCHASE12'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['CGST6'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['SGST6'] ?></td>
                                        <td class="td_common_numeric_rules"></td>
                                        <td class="td_common_numeric_rules"><?= @$each['IGST_INTERSATEPURCHASE12'] !=0 ? $each['IGST_INTERSATEPURCHASE12'] : ' ' ?></td>
                                        <td class="td_common_numeric_rules"><?= @$each['IGST12'] !=0 ? $each['IGST12'] : ' '  ?></td>
                                        <td class="td_common_numeric_rules"></td>
                                        <td class="td_common_numeric_rules"><?= $each['PURCHASE18'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['CGST9'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['SGST9'] ?></td>
                                        <td class="td_common_numeric_rules"></td>
                                        <td class="td_common_numeric_rules"><?= @$each['IGST_INTERSATEPURCHASE18'] !=0 ? $each['IGST_INTERSATEPURCHASE18'] : ' ' ?>
                                        </td>
                                        <td class="td_common_numeric_rules"><?= @$each['IGST18'] !=0 ? $each['IGST18'] : ' ' ?></td>
                                        <td class="td_common_numeric_rules"></td>
                                        <td class="td_common_numeric_rules"><?= @$each['PURCHASE28'] !=0 ? $each['PURCHASE28'] : ' ' ?></td>
                                        <td class="td_common_numeric_rules"><?= @$each['CGST14']!=0 ? $each['CGST14'] : ' ' ?></td>
                                        <td class="td_common_numeric_rules"><?= @$each['SGST14']!=0 ? $each['SGST14'] : ' ' ?></td>
                                        <td class="td_common_numeric_rules"></td>
                                        <td class="td_common_numeric_rules"><?= @$each['IGST_INTERSATEPURCHASE28'] !=0 ? $each['IGST_INTERSATEPURCHASE28'] : ' '  ?>
                                        </td>
                                        <td class="td_common_numeric_rules"><?= @$each['IGST28']!=0 ? $each['IGST28'] : ' ' ?></td>
                                        <td class="td_common_numeric_rules"></td>
                                        <td class="td_common_numeric_rules"><?= @$each['IGST_INTERSATEPURCHASE0'] !=0 ? $each['IGST_INTERSATEPURCHASE0'] : ' '  ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['net_amount'] ?></td>
                                        <td class="td_common_numeric_rules">Purchase</td>
                                        <td class="td_common_numeric_rules"><?= $each['state'] ?></td>
                                        <td class="td_common_numeric_rules">Regular</td>
                                        <td class="common_td_rules">{{ $each['invoice_no'] }}</td>
                                        <td class="common_td_rules">{{ $each['invoice_date'] }}</td>
                                    </tr>
                                @endforeach
                                <tr class="bg-blue">
                                    <td colspan="5" class="common_td_rules">Total</td>
                                    <td class="td_common_numeric_rules"><?= $PURCHASE0_total ?></td>
                                    <td class="td_common_numeric_rules"><?= $PURCHASE5 ?></td>
                                    <td class="td_common_numeric_rules"><?= $CGST25 ?></td>
                                    <td class="td_common_numeric_rules"><?= $SGST25 ?></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE5 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST5 ?></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"><?= $PURCHASE12 ?></td>
                                    <td class="td_common_numeric_rules"><?= $CGST6 ?></td>
                                    <td class="td_common_numeric_rules"><?= $SGST6 ?></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE12 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST12 ?></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"><?= $PURCHASE18 ?></td>
                                    <td class="td_common_numeric_rules"><?= $CGST9 ?></td>
                                    <td class="td_common_numeric_rules"><?= $SGST9 ?></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE18 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST18 ?></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"><?= $PURCHASE28 ?></td>
                                    <td class="td_common_numeric_rules"><?= $CGST14 ?></td>
                                    <td class="td_common_numeric_rules"><?= $SGST14 ?></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE28 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST28 ?></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE0 ?></td>
                                    <td class="td_common_numeric_rules"><?= $net_amount ?></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="39" style="text-align: center">
                                        No Result Found
                                    </td>
                                </tr>
                            @endif
                         @else
                            <tr class="headerclass"
                                style="background-color: rgb(165 169 165);color:#000;border-spacing: 0 1em;font-family:sans-serif">
                                <th colspan="39">Purchase GST Report</th>
                            </tr>
                            <tr class="headerclass"
                                style="background-color: rgb(165 169 165);color:#000;border-spacing: 0 1em;font-family:sans-serif">
                                <th width="width:5%">Sl.No</th>
                                <th>Approved Date</th>
                                <th>Grn No</th>
                                <th>Invoice No</th>
                                <th>Vendor Name</th>
                                <th>Invoice Date </th>
                                <th>Status</th>
                                <th>Location Name</th>
                                <th>Store Type</th>
                                <th>Purchase 0%</th>
                                <th>Interstate Purchase 0%</th>
                                <th>Purchase 5%</th>
                                <th>CGST 2.5 %</th>
                                <th>SGST 2.5%</th>
                                <th>Interstate Purchase 5%</th>
                                <th>IGST 5%</th>
                                <th>Purchase 12%</th>
                                <th>CGST 6%</th>
                                <th>SGST 6%</th>
                                <th>Interstate Purchase 12%</th>
                                <th>IGST 12%</th>
                                <th>Purchase 18%</th>
                                <th>CGST 9%</th>
                                <th>SGST 9%</th>
                                <th>Interstate Purchase 18%</th>
                                <th>IGST 18 %</th>
                                <th>Purchase 28%</th>
                                <th>CGST 14%</th>
                                <th>SGST 14%</th>
                                <th>Interstate Purchase 28%</th>
                                <th>IGST 28%</th>
                                <th>Roundoff</th>
                                <th>Total Purchase</th>
                                <th>Total Tax</th>
                                <th>Total CGST</th>
                                <th>Total SGST</th>
                                <th>Total IGST</th>
                                <th>Total Net Amount</th>
                            </tr>

                            @if (count($purchase_array) != 0)
                                <?php
                                $i = 1;
                                $PURCHASE0_total = 0.0;
                                $PURCHASE3 = 0.0;
                                $CGST15 = 0.0;
                                $SGST15 = 0.0;
                                $IGST3 = 0.0;
                                $PURCHASE5 = 0.0;
                                $CGST25 = 0.0;
                                $SGST25 = 0.0;
                                $IGST5 = 0.0;
                                $PURCHASE12 = 0.0;
                                $CGST6 = 0.0;
                                $SGST6 = 0.0;
                                $IGST12 = 0.0;
                                $PURCHASE18 = 0.0;
                                $CGST9 = 0.0;
                                $SGST9 = 0.0;
                                $IGST18 = 0.0;
                                $PURCHASE28 = 0.0;
                                $CGST14 = 0.0;
                                $SGST14 = 0.0;
                                $IGST28 = 0.0;
                                $roundoff_tot = 0.0;
                                $net_amount = 0.0;
                                $total_cgst = 0.0;
                                $total_sgst = 0.0;
                                $total_igst = 0.0;
                                $IGST_INTERSATEPURCHASE0 = 0.0;
                                $IGST_INTERSATEPURCHASE5 = 0.0;
                                $IGST_INTERSATEPURCHASE12 = 0.0;
                                $IGST_INTERSATEPURCHASE18 = 0.0;
                                $IGST_INTERSATEPURCHASE28 = 0.0;
                                $total_netpurchase = 0.0;
                                $total_nettax = 0.0;
                                ?>
                                @foreach ($purchase_array as $each)
                                    <?php
                                    $PURCHASE0_total += $each['PURCHASE0'];
                                    $PURCHASE3 += $each['PURCHASE3'];
                                    $CGST15 += $each['CGST15'];
                                    $SGST15 += $each['SGST15'];
                                    $IGST3 += $each['IGST3'];
                                    $PURCHASE5 += $each['PURCHASE5'];
                                    $CGST25 += $each['CGST25'];
                                    $SGST25 += $each['SGST25'];
                                    $IGST5 += $each['IGST5'];
                                    $PURCHASE12 += $each['PURCHASE12'];
                                    $CGST6 += $each['CGST6'];
                                    $SGST6 += $each['SGST6'];
                                    $IGST12 += $each['IGST12'];
                                    $PURCHASE18 += $each['PURCHASE18'];
                                    $CGST9 += $each['CGST9'];
                                    $SGST9 += $each['SGST9'];
                                    $IGST18 += $each['IGST18'];
                                    $PURCHASE28 += $each['PURCHASE28'];
                                    $CGST14 += $each['CGST14'];
                                    $SGST14 += $each['SGST14'];
                                    $IGST28 += $each['IGST28'];
                                    $IGST_INTERSATEPURCHASE0 += $each['IGST_INTERSATEPURCHASE0'];
                                    $IGST_INTERSATEPURCHASE5 += $each['IGST_INTERSATEPURCHASE5'];
                                    $IGST_INTERSATEPURCHASE12 += $each['IGST_INTERSATEPURCHASE12'];
                                    $IGST_INTERSATEPURCHASE18 += $each['IGST_INTERSATEPURCHASE18'];
                                    $IGST_INTERSATEPURCHASE28 += $each['IGST_INTERSATEPURCHASE28'];
                                    $roundoff_tot += $each['round_off'];
                                    $net_amount += $each['net_amount'];
                                    $total_cgst += $each['total_cgst'];
                                    $total_sgst += $each['total_sgst'];
                                    $total_igst += $each['total_igst'];
                                    $total_netpurchase += $each['total_purchase'];
                                    $total_nettax += $each['total_tax'];
                                    ?>

                                    <tr>
                                        <td class="td_common_numeric_rules">{{ $i }}.</td>
                                        <td class="common_td_rules">{{ $each['approved_date'] }}</td>
                                        <td class="common_td_rules">{{ $each['bill_no'] }}</td>
                                        <td class="common_td_rules">{{ $each['invoice_no'] }}</td>
                                        <td class="common_td_rules">{{ $each['vendor_name'] }}</td>
                                        <td class="common_td_rules">{{ $each['invoice_date'] }}</td>
                                        <td class="common_td_rules">{{ $each['approve_status'] }}</td>
                                        <td class="common_td_rules">{{ $each['location_name'] }}</td>
                                        <td class="common_td_rules">{{ $each['store_type'] }}</td>
                                        <td class="td_common_numeric_rules"><?= $each['PURCHASE0'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['IGST_INTERSATEPURCHASE0'] ?>
                                        </td>
                                        <td class="td_common_numeric_rules"><?= $each['PURCHASE5'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['CGST25'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['SGST25'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['IGST_INTERSATEPURCHASE5'] ?>
                                        </td>
                                        <td class="td_common_numeric_rules"><?= $each['IGST5'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['PURCHASE12'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['CGST6'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['SGST6'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['IGST_INTERSATEPURCHASE12'] ?>
                                        </td>
                                        <td class="td_common_numeric_rules"><?= $each['IGST12'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['PURCHASE18'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['CGST9'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['SGST9'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['IGST_INTERSATEPURCHASE18'] ?>
                                        </td>
                                        <td class="td_common_numeric_rules"><?= $each['IGST18'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['PURCHASE28'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['CGST14'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['SGST14'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['IGST_INTERSATEPURCHASE28'] ?>
                                        </td>
                                        <td class="td_common_numeric_rules"><?= $each['IGST28'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['round_off'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['total_purchase'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['total_tax'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['total_cgst'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['total_sgst'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['total_igst'] ?></td>
                                        <td class="td_common_numeric_rules"><?= $each['net_amount'] ?></td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach

                                <tr class="bg-blue">
                                    <td colspan="9" class="common_td_rules">Total</td>
                                    <td class="td_common_numeric_rules"><?= $PURCHASE0_total ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE0 ?></td>
                                    <td class="td_common_numeric_rules"><?= $PURCHASE5 ?></td>
                                    <td class="td_common_numeric_rules"><?= $CGST25 ?></td>
                                    <td class="td_common_numeric_rules"><?= $SGST25 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE5 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST5 ?></td>
                                    <td class="td_common_numeric_rules"><?= $PURCHASE12 ?></td>
                                    <td class="td_common_numeric_rules"><?= $CGST6 ?></td>
                                    <td class="td_common_numeric_rules"><?= $SGST6 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE12 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST12 ?></td>
                                    <td class="td_common_numeric_rules"><?= $PURCHASE18 ?></td>
                                    <td class="td_common_numeric_rules"><?= $CGST9 ?></td>
                                    <td class="td_common_numeric_rules"><?= $SGST9 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE18 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST18 ?></td>
                                    <td class="td_common_numeric_rules"><?= $PURCHASE28 ?></td>
                                    <td class="td_common_numeric_rules"><?= $CGST14 ?></td>
                                    <td class="td_common_numeric_rules"><?= $SGST14 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE28 ?></td>
                                    <td class="td_common_numeric_rules"><?= $IGST28 ?></td>
                                    <td class="td_common_numeric_rules"><?= $roundoff_tot ?></td>
                                    <td class="td_common_numeric_rules"><?= $total_netpurchase ?></td>
                                    <td class="td_common_numeric_rules"><?= $total_nettax ?></td>
                                    <td class="td_common_numeric_rules"><?= $total_cgst ?></td>
                                    <td class="td_common_numeric_rules"><?= $total_sgst ?></td>
                                    <td class="td_common_numeric_rules"><?= $total_igst ?></td>
                                    <td class="td_common_numeric_rules"><?= $net_amount ?></td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="39" style="text-align: center">
                                        No Result Found
                                    </td>
                                </tr>
                            @endif
                        @endif


                            @if ($hospital_code != 'ALMA')
                                <tr class="headerclass"
                                    style="background-color: rgb(165 169 165);color:#000;border-spacing: 0 1em;font-family:sans-serif">
                                    <th width="width:5%">Sl.No</th>
                                    <th>Approved Date</th>
                                    <th>Grn No</th>
                                    <th>Invoice No</th>
                                    <th>Vendor Name</th>
                                    <th>Invoice Date </th>
                                    <th>Status</th>
                                    <th>Location Name</th>
                                    <th>Store Type</th>
                                    <th>Purchase 0%</th>
                                    <th>Interstate Purchase Return 0%</th>
                                    <th>Purchase 5%</th>
                                    <th>CGST 2.5 %</th>
                                    <th>SGST 2.5%</th>
                                    <th>Interstate Purchase Return 5%</th>
                                    <th>IGST 5%</th>
                                    <th>Purchase 12%</th>
                                    <th>CGST 6%</th>
                                    <th>SGST 6%</th>
                                    <th>Interstate Purchase Return 12%</th>
                                    <th>IGST 12%</th>
                                    <th>Purchase 18%</th>
                                    <th>CGST 9%</th>
                                    <th>SGST 9%</th>
                                    <th>Interstate Purchase Return 18%</th>
                                    <th>IGST 18 %</th>
                                    <th>Purchase 28%</th>
                                    <th>CGST 14%</th>
                                    <th>SGST 14%</th>
                                    <th>Interstate Purchase Return 28%</th>
                                    <th>IGST 28%</th>
                                    <th>Roundoff</th>
                                    <th>Total Purchase Return</th>
                                    <th>Total Tax</th>
                                    <th>Total CGST</th>
                                    <th>Total SGST</th>
                                    <th>Total IGST</th>
                                    <th>Total Net Amount</th>
                                </tr>
                                @if (count($purchase_return_array) != 0)
                                    <?php
                                    $i = 1;
                                    $PURCHASE0_total = 0.0;
                                    $PURCHASE3 = 0.0;
                                    $CGST15 = 0.0;
                                    $SGST15 = 0.0;
                                    $IGST3 = 0.0;
                                    $PURCHASE5 = 0.0;
                                    $CGST25 = 0.0;
                                    $SGST25 = 0.0;
                                    $IGST5 = 0.0;
                                    $PURCHASE12 = 0.0;
                                    $CGST6 = 0.0;
                                    $SGST6 = 0.0;
                                    $IGST12 = 0.0;
                                    $PURCHASE18 = 0.0;
                                    $CGST9 = 0.0;
                                    $SGST9 = 0.0;
                                    $IGST18 = 0.0;
                                    $PURCHASE28 = 0.0;
                                    $CGST14 = 0.0;
                                    $SGST14 = 0.0;
                                    $IGST28 = 0.0;
                                    $roundoff_tot = 0.0;
                                    $net_amount = 0.0;
                                    $total_cgst = 0.0;
                                    $total_sgst = 0.0;
                                    $total_igst = 0.0;
                                    $IGST_INTERSATEPURCHASE0 = 0.0;
                                    $IGST_INTERSATEPURCHASE5 = 0.0;
                                    $IGST_INTERSATEPURCHASE12 = 0.0;
                                    $IGST_INTERSATEPURCHASE18 = 0.0;
                                    $IGST_INTERSATEPURCHASE28 = 0.0;
                                    $total_netpurchase = 0.0;
                                    $total_nettax = 0.0;
                                    ?>
                                    @foreach ($purchase_return_array as $each)
                                        <?php

                                        $PURCHASE0_total += $each['PURCHASE0'];
                                        $PURCHASE3 += $each['PURCHASE3'];
                                        $CGST15 += $each['CGST15'];
                                        $SGST15 += $each['SGST15'];
                                        $IGST3 += $each['IGST3'];
                                        $PURCHASE5 += $each['PURCHASE5'];
                                        $CGST25 += $each['CGST25'];
                                        $SGST25 += $each['SGST25'];
                                        $IGST5 += $each['IGST5'];
                                        $PURCHASE12 += $each['PURCHASE12'];
                                        $CGST6 += $each['CGST6'];
                                        $SGST6 += $each['SGST6'];
                                        $IGST12 += $each['IGST12'];
                                        $PURCHASE18 += $each['PURCHASE18'];
                                        $CGST9 += $each['CGST9'];
                                        $SGST9 += $each['SGST9'];
                                        $IGST18 += $each['IGST18'];
                                        $PURCHASE28 += $each['PURCHASE28'];
                                        $CGST14 += $each['CGST14'];
                                        $SGST14 += $each['SGST14'];
                                        $IGST28 += $each['IGST28'];
                                        $IGST_INTERSATEPURCHASE0 += $each['IGST_INTERSATEPURCHASE0'];
                                        $IGST_INTERSATEPURCHASE5 += $each['IGST_INTERSATEPURCHASE5'];
                                        $IGST_INTERSATEPURCHASE12 += $each['IGST_INTERSATEPURCHASE12'];
                                        $IGST_INTERSATEPURCHASE18 += $each['IGST_INTERSATEPURCHASE18'];
                                        $IGST_INTERSATEPURCHASE28 += $each['IGST_INTERSATEPURCHASE28'];
                                        $roundoff_tot += $each['round_off'];
                                        $net_amount += $each['net_amount'];
                                        $total_cgst += $each['total_cgst'];
                                        $total_sgst += $each['total_sgst'];
                                        $total_igst += $each['total_igst'];
                                        $total_netpurchase += $each['total_purchase'];
                                        $total_nettax += $each['total_tax'];
                                        ?>

                                        <tr>
                                            <td class="td_common_numeric_rules">{{ $i }}.</td>
                                            <td class="common_td_rules">{{ $each['approved_date'] }}</td>
                                            <td class="common_td_rules">{{ $each['bill_no'] }}</td>
                                            <td class="common_td_rules">{{ $each['invoice_no'] }}</td>
                                            <td class="common_td_rules">{{ $each['vendor_name'] }}</td>
                                            <td class="common_td_rules">{{ $each['invoice_date'] }}</td>
                                            <td class="common_td_rules">{{ $each['approve_status'] }}</td>
                                            <td class="common_td_rules">{{ $each['location_name'] }}</td>
                                            <td class="common_td_rules">{{ $each['store_type'] }}</td>
                                            <td class="td_common_numeric_rules"><?= $each['PURCHASE0'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['IGST_INTERSATEPURCHASE0'] ?>
                                            </td>
                                            <td class="td_common_numeric_rules"><?= $each['PURCHASE5'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['CGST25'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['SGST25'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['IGST_INTERSATEPURCHASE5'] ?>
                                            </td>
                                            <td class="td_common_numeric_rules"><?= $each['IGST5'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['PURCHASE12'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['CGST6'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['SGST6'] ?></td>
                                            <td class="td_common_numeric_rules">
                                                <?= $each['IGST_INTERSATEPURCHASE12'] ?>
                                            </td>
                                            <td class="td_common_numeric_rules"><?= $each['IGST12'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['PURCHASE18'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['CGST9'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['SGST9'] ?></td>
                                            <td class="td_common_numeric_rules">
                                                <?= $each['IGST_INTERSATEPURCHASE18'] ?>
                                            </td>
                                            <td class="td_common_numeric_rules"><?= $each['IGST18'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['PURCHASE28'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['CGST14'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['SGST14'] ?></td>
                                            <td class="td_common_numeric_rules">
                                                <?= $each['IGST_INTERSATEPURCHASE28'] ?>
                                            </td>
                                            <td class="td_common_numeric_rules"><?= $each['IGST28'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['round_off'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['total_purchase'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['total_tax'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['total_cgst'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['total_sgst'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['total_igst'] ?></td>
                                            <td class="td_common_numeric_rules"><?= $each['net_amount'] ?></td>
                                        </tr>
                                        @php
                                        $i++;
                                        @endphp
                                    @endforeach

                                    <tr class="bg-blue">
                                        <td colspan="9" class="common_td_rules">Total</td>
                                        <td class="td_common_numeric_rules"><?= $PURCHASE0_total ?></td>
                                        <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE0 ?></td>
                                        <td class="td_common_numeric_rules"><?= $PURCHASE5 ?></td>
                                        <td class="td_common_numeric_rules"><?= $CGST25 ?></td>
                                        <td class="td_common_numeric_rules"><?= $SGST25 ?></td>
                                        <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE5 ?></td>
                                        <td class="td_common_numeric_rules"><?= $IGST5 ?></td>
                                        <td class="td_common_numeric_rules"><?= $PURCHASE12 ?></td>
                                        <td class="td_common_numeric_rules"><?= $CGST6 ?></td>
                                        <td class="td_common_numeric_rules"><?= $SGST6 ?></td>
                                        <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE12 ?></td>
                                        <td class="td_common_numeric_rules"><?= $IGST12 ?></td>
                                        <td class="td_common_numeric_rules"><?= $PURCHASE18 ?></td>
                                        <td class="td_common_numeric_rules"><?= $CGST9 ?></td>
                                        <td class="td_common_numeric_rules"><?= $SGST9 ?></td>
                                        <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE18 ?></td>
                                        <td class="td_common_numeric_rules"><?= $IGST18 ?></td>
                                        <td class="td_common_numeric_rules"><?= $PURCHASE28 ?></td>
                                        <td class="td_common_numeric_rules"><?= $CGST14 ?></td>
                                        <td class="td_common_numeric_rules"><?= $SGST14 ?></td>
                                        <td class="td_common_numeric_rules"><?= $IGST_INTERSATEPURCHASE28 ?></td>
                                        <td class="td_common_numeric_rules"><?= $IGST28 ?></td>
                                        <td class="td_common_numeric_rules"><?= $roundoff_tot ?></td>
                                        <td class="td_common_numeric_rules"><?= $total_netpurchase ?></td>
                                        <td class="td_common_numeric_rules"><?= $total_nettax ?></td>
                                        <td class="td_common_numeric_rules"><?= $total_cgst ?></td>
                                        <td class="td_common_numeric_rules"><?= $total_sgst ?></td>
                                        <td class="td_common_numeric_rules"><?= $total_igst ?></td>
                                        <td class="td_common_numeric_rules"><?= $net_amount ?></td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="39" style="text-align: center">
                                            No Result Found
                                        </td>
                                    </tr>
                                @endif
                            @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
