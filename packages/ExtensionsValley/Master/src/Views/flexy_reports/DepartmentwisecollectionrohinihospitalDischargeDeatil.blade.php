<div class="row">
    <div class="col-md-12 padding_sm" id="result_discharge_detail_container_div">
        <table id="result_data_discharge_detail_table" class='table table-striped table-bordered table-condensed table_sm'
            style="width:100%; font-size: 12px;">
            <thead>
                <tr>
                    <th style="background-color:#07ad8c; color: black;" width='5%'>SL. No.</th>
                    <th style="background-color:#07ad8c; color: black;" width='13%'>Doctor</th>
                    <th style="background-color:#07ad8c; color: black;" width='15%'>Item Desc.</th>
                    <th style="background-color:#07ad8c; color: black;" width='7%'>Bill No</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Bill Date</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Payment Type</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Bill Amt.</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Return Amt.</th>
                    <th style="background-color:#07ad8c; color: black;" width='6%'>Net Amt.</th>

                </tr>
            </thead>

            <tbody>
                @if (count($result_data) != 0)
                    @php
                        $i = 1;
                        $total_gross_amount = 0;
                        $total_net_amount = 0;
                        $total_return_amount = 0;
                        $total_advance_amount = 0;
                    @endphp
                    @foreach ($result_data as $data)
                        @php
                        @endphp
                        <tr>
                            <td class="common_td_rules">{{ $i }}</td>
                            <td class="common_td_rules">{{ $data->consulting_doctor_name }}</td>
                            <td class="common_td_rules">{{ $data->item_desc }}</td>
                            <td class="common_td_rules">{{ $data->bill_no }}</td>
                            <td class="common_td_rules">{{ $data->bill_date }}</td>
                            <td class="common_td_rules">{{ $data->payment_type }}</td>
                            <td class="td_common_numeric_rules">{{ $data->gross }}</td>
                            <td class="td_common_numeric_rules">{{ $data->return_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $data->net }}</td>

                        </tr>
                        @php
                            $i++;
                            $total_gross_amount += floatval($data->gross);
                            $total_net_amount += floatval($data->net);
                            $total_return_amount += floatval($data->return_amount);
                        @endphp
                    @endforeach
                    <tr>
                        <th class="common_td_rules" colspan="2">Total</th>
                        <th class="td_common_numeric_rules" colspan="4">Advance Adj. {{ $advance_collected }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_gross_amount, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($total_return_amount, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">
                            {{ number_format($total_net_amount - $total_return_amount - $advance_collected, 2, '.', '') }}
                        </th>
                    </tr>
                @else
                    <tr>
                        <th colspan="11" style="text-align: center;">No records Found!</th>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
