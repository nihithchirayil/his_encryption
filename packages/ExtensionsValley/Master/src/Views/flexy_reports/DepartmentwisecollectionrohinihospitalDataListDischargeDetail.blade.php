<div class="row">
    <label> <b>Advance Collected Amount : {{number_format($advance_collected)}}</b></label><br>
    <div class="col-md-12 padding_sm" id="result_detail_container_div">
        <table id="result_data_detail_table" class='table table-striped table-bordered table-condensed table_sm'
            style="width:100%; font-size: 12px;">
            <thead>
                <tr>
                    <th style="background-color:#07ad8c; color: black;" width='5%'>SL.No.</th>
                    <th style="background-color:#07ad8c; color: black;" width='75%'>Bill Tag</th>
                    <th style="background-color:#07ad8c; color: black;" width='20%'>Net Amount</th>

                </tr>
            </thead>

            <tbody>
                @if (count($result_data) != 0)
                    @php
                        $i = 1;
                        $total_net_amount = 0;
                    @endphp
                    @foreach ($result_data as $data)
                        @php
                        @endphp
                        <tr>
                            <td class="common_td_rules">{{ $i }}</td>
                            <td class="common_td_rules">{{ $data->bill_tag }}</td>
                            <td class="td_common_numeric_rules">{{ $data->net }}</td>

                        </tr>
                        @php
                            $i++;
                            $total_net_amount += floatval($data->net);
                        @endphp
                    @endforeach
                    <tr>
                        <th class="common_td_rules" colspan="2">Total</th>
                        <th class="td_common_numeric_rules">
                            {{ number_format($total_net_amount - $advance_collected, 2, '.', '') }}
                        </th>
                    </tr>
                @else
                    <tr>
                        <th colspan="3" style="text-align: center;">No records Found!</th>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
