<div class="col-md-12" id="result_container_div">
    <div class="col-md-12" id="PurchaseGSTReportData">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= date('M-d-Y', strtotime($from_date)) ?> To
            <?= date('M-d-Y', strtotime($to_date)) ?></p>

        <div class="col-md-12" style="margin-bottom:20px;">
            <div class="theadscroll" id="table_container_div" style="position: relative; max-height: 500px;">
                <table id="result_data_table" border="1" width="100%;"
                    class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper"
                    style="border-collapse: collapse;">
                    <tbody>
                        <tr class="headerclass"
                            style="background-color:#1ABB9C;color:#000;border-spacing: 0 1em;font-family:sans-serif">
                            <th width="width:5%">BILL NO(Invoice No)</th>
                            <th>DATE</th>
                            <th>GSTIN</th>
                            <th>SUPPLIERNAME</th>
                            <th>PURCHASES MEDICINE 0%</th>
                            <th>PURCHASES MEDICINE 5%</th>
                            <th>INPUT CGST 2.5%</th>
                            <th>INPUT SGST 2.5%</th>
                            <th>Cess</th>
                            <th>Interstate Purchase 5%</th>
                            <th>IGST</th>
                            <th>Cess</th>
                            <th>PURCHASES MEDICINE 12%</th>
                            <th>INPUT CGST 6%</th>
                            <th>INPUT SGST 6%</th>
                            <th>Cess</th>
                            <th>IGST Purchase 12%</th>
                            <th>IGST</th>
                            <th>Cess</th>
                            <th>PURCHASES MEDICINE 18%</th>
                            <th>INPUT CGST 9%</th>
                            <th>INPUT SGST 9 %</th>
                            <th>Cess</th>
                            <th>IGST Purchase 18%</th>
                            <th>IGST</th>
                            <th>Cess</th>
                            <th>Purchase 28%</th>
                            <th>CGST</th>
                            <th>SGST</th>
                            <th>Cess</th>
                            <th>IGST Purchase 28%</th>
                            <th>IGST</th>
                            <th>Cess</th>
                            <th>Int Purchase Exempt</th>
                            <th>Net Amount</th>
                            <th>Voucher TypeName</th>
                            <th>State</th>
                            <th>Registration Type</th>
                            <th>Invoice Number</th>
                            <th>Invoice Date</th>
                        </tr>
                        @if (count($grn_array) != 0)
                            @foreach ($grn_array as $data)
                                <tr>
                                    <td class="common_td_rules">{{ $data->bill_no }}</td>
                                    <td class="common_td_rules">{{ $data->bill_date }}</td>
                                    <td class="common_td_rules">{{ $data->gst_vendor_code }}</td>
                                    <td class="common_td_rules">{{ $data->vendor_name }}</td>
                                    <td class="td_common_numeric_rules">
                                        {{ $data->exemptedsales }}
                                    </td>

                                    <td class="td_common_numeric_rules">
                                        {{ $data->salesfive }}

                                    </td>
                                    <td class="td_common_numeric_rules">
                                        {{ $data->salesfivetax / 2 }}
                                    </td>
                                    <td class="td_common_numeric_rules">
                                        {{ $data->salesfivetax / 2 }}
                                    </td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>

                                    <td class="td_common_numeric_rules">
                                        {{ $data->salestwelve }}
                                    </td>
                                    <td class="td_common_numeric_rules">
                                        {{ $data->salestwelvetax / 2 }}
                                    </td>
                                    <td class="td_common_numeric_rules">
                                        {{ $data->salestwelvetax / 2 }}
                                    </td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>

                                    <td class="td_common_numeric_rules">
                                        {{ $data->saleseighteen }}
                                    </td>
                                    <td class="td_common_numeric_rules">
                                        {{ $data->saleseighteentax / 2 }}
                                    </td>
                                    <td class="td_common_numeric_rules">
                                        {{ $data->saleseighteentax / 2 }}
                                    </td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules"></td>
                                    <td class="td_common_numeric_rules">
                                        {{ $data->net_amount }}
                                    </td>
                                    <td class="common_td_rules">Purchase</td>
                                    <td class="common_td_rules">Kerala</td>
                                    <td class="common_td_rules">Regular</td>
                                    <td class="common_td_rules">{{ $data->bill_no }}</td>
                                    <td class="common_td_rules">{{ $data->bill_date }}</td>

                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="40" style="text-align: center">
                                    No Result Found
                                </td>
                            </tr>
                        @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
