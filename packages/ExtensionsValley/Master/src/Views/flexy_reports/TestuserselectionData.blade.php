<p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>

        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> {{$report_name}} </b></h4>
<style>
    @media print {
        .theadscroll {max-height: none; overflow: visible;}
        .header_bg {
            background-color: #08898d;
            color: white;
        }
        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #f2fef5;
        }

        #resultDataTable{
            border: 1px solid #cedfe9 !important;
            border-collapse: collapse !important;
        }
        td{
            padding-left: 5px !important;
        }
        }
</style>

<div class="col-md-12" style="margin-bottom:20px;">
    @if($status == 1)
        @if(sizeof($result_data)>0)
        <div class="theadscroll" id="table_container_div" style="position: relative; max-height: 500px;">
            <table id="resultDataTable" border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper" style="border-collapse: collapse;">
                <thead>
                    <tr>
                        @php
                            $head_array = $result_data[0];
                            $total_colums = explode(",", $flexy_report_totals);
                            $total_array = [];
                            $total_array_index = [];
                            $j = 0;
                            $head_array_size = count(array_keys((array)$head_array));
                            $custom_column_width = explode(',', $custom_column_width);

                        @endphp

                            <th style="width:10%" class="header_bg">Sl.No</th>
                        @foreach($head_array as $key => $value)
                            @php
                                if(in_array($key, $total_colums)){
                                    $total_array[$key] = 0;
                                    $total_array_index[$key] = $j;
                                }
                                $key =  str_replace("_"," ",$key);
                                $key = ucwords($key);
                                $j++;
                            @endphp

                            @if(substr($key, 0, 1) != '@') <!--- if first letter is not @  then only show-->
                                <th style="@if(isset($custom_column_width[$j-1]))width:{{$custom_column_width[$j-1]}}% @endif" class="header_bg">{{$key}}</th>
                            @endif
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;

                    @endphp
                    @foreach($result_data as $key => $value)
                        <tr>
                            <td>{{$i}}</td>
                            @foreach($value as $key1 => $value1)
                                @if(substr($value1, 0, 1) != '@')
                                    <td style="text-align:left">{{$value1}}</td>
                                @endif

                                @php
                                   if(in_array($key1,array_keys($total_array))){
                                        $total_array[$key1] += (int)$value1;
                                   }
                                @endphp

                            @endforeach
                        </tr>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                    </tr>
                    @if($flexy_report_totals !='')
                        <tr style="background-color: paleturquoise;">
                            <td><b>Total</b></td>
                            @foreach ($head_array as $key3 => $val3)
                                @if(in_array($key3,array_keys($total_array)))
                                    @if(substr($key3, 0, 1) != '@')
                                        <td style="text-align:left;bac"><b>{{$total_array[$key3]}}</b></td>
                                    @endif
                                @else
                                    @if(substr($key3, 0, 1) != '@')
                                        <td style="text-align:left"><b></b></td>
                                    @endif
                                @endif
                            @endforeach
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        @else
        <div class="col-md-12"><h5 style="color:red;">No Results Found!</h5></div>
        @endif
    @else
    <div class="alert alert-danger">
        {{$result_data}}
    </div>
    @endif
