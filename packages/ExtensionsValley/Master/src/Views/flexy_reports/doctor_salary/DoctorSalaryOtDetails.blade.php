<div class="col-md-12 no-padding">
    <div class="col-md-6">
        <h5 style="color:rgb(7, 7, 87);">{{$doctor_name}}</h5>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-primary btn-sm" onclick="printDiv('doctor_salary_ot_details_table_data')"><i class="fa fa-print"></i> Print</button>
    </div>
</div>
<div id="doctor_salary_ot_details_table_data" class="col-md-12 theadscroll" style="height:380px;">
    <h4 style="color:rgb(7, 7, 87);"><b>Doctor Salary OT Details - ( {{$salary_month_label}} )</b></h4>
    <h5 style="color:rgb(7, 7, 87);"><b>Doctor : {{$doctor_name}}</b></h5>
    <table border="1" id="procedure_services_table" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;">
        <thead>
            <tr style="background-color: paleturquoise;font-weight:600;">
                <td width="5%">Si.No</td>
                <td width="12%">Bill No</td>
                <td width="13%">Bill Tag</td>
                <td width="10%">Bill Date</td>
                <td width="20%">Patient Name</td>
                <td width="10%">Uhid</td>
                <td width="10%">Doctor Part</td>
                <td width="10%">Anesthesia Part</td>
                <td width="10%">Hospital Share</td>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
                $total_doc_part = 0;
                $total_ans_hsp = 0;
                $total_hosp_share = 0;

            @endphp
            @if (sizeof($result_data)>0)
                @foreach($result_data as $data)
                <tr>
                    <td>{{$i}}</td>
                    <td style="text-align:left">{{$data->bill_no}}</td>
                    <td style="text-align:left">{{$data->bill_tag}}</td>
                    <td style="text-align:left">{{$data->bill_date}}</td>
                    <td style="text-align:left">{{$data->patient_name}}</td>
                    <td style="text-align:left">{{$data->uhid}}</td>
                    <td style="">{{number_format($data->doc_part,2)}}</td>
                    <td style="">{{number_format($data->ans_hsp,2)}}</td>
                    <td style="">{{number_format($data->hosp_share,2)}}</td>
                </tr>
                    @php
                    $total_doc_part += $data->doc_part;
                    $total_ans_hsp += $data->ans_hsp;
                    $total_hosp_share += $data->hosp_share;
                    $i++;
                    @endphp
                @endforeach
                <tr>
                    <td colspan='6' style=""><b>Total</b></td>
                    <td><b>{{number_format($total_doc_part,2)}}</b></td>
                    <td><b>{{number_format($total_ans_hsp,2)}}</b></td>
                    <td><b>{{number_format($total_hosp_share,2)}}</b></td>
                </tr>
            @else
                <tr>
                    <td colspan="10">No Data Found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>


