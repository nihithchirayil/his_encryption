<div id="modal_bill_analysis" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:75%">
        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h5 style="color:rgb(3, 3, 53);" class="modal-title">Bill Analysis</span></h4>
            </div>
            <div class="modal-body" style="height:465px;padding:15px;">
                <div class="theadscroll" id="bill_analysis_data" style="height:450px;position:absolute;">
                </div>
            </div>
        </div>

    </div>
</div>

<div id="modal_procedure_details" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:75%">
        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h5 style="color:#ffffff;" class="modal-title">Procedure Details</span></h4>
            </div>
            <div class="modal-body" style="height:465px;padding:15px;">
                <div class="theadscroll" id="procedure_details_data" style="height:530px;position:absolute;">
                </div>
            </div>
        </div>

    </div>
</div>

<div id="modal_ip_details" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:85%">
        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h5 style="color:#ffffff;" class="modal-title">Ip Details</span></h4>
            </div>
            <div class="modal-body" style="height:465px;padding:15px;">
                <div class="theadscroll" id="ip_details_data" style="height:530px;position:absolute;">
                </div>
            </div>
        </div>

    </div>
</div>

<div id="modal_op_details" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:85%">
        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h5 style="color:#ffffff;" class="modal-title">Op Details</span></h4>
            </div>
            <div class="modal-body" style="height:465px;padding:15px;">
                <div class="theadscroll" id="op_details_data" style="height:530px;position:absolute;">
                </div>
            </div>
        </div>

    </div>
</div>
<div id="modal_ot_details" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:85%">
        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h5 style="color:#ffffff;" class="modal-title">OT Details</span></h4>
            </div>
            <div class="modal-body" style="height:465px;padding:15px;">
                <div class="theadscroll" id="ot_details_data" style="height:530px;position:absolute;">
                </div>
            </div>
        </div>

    </div>
</div>

<div id="modal_add_service" class="modal bs-example-modal-lg mkModalFloat in" role="dialog"
    data-backdrop="static" aria-hidden="false">
    <div class="modal-dialog modal-lg" style="width:85%">
        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h5 style="color:#ffffff;" class="modal-title">Add Services</span></h4>
            </div>
            <div class="modal-body" id="add_service_data" style="height:550px;padding:15px;">
            </div>
        </div>
    </div>
</div>
