<!----Anesthesia------------------------>
<input type="hidden" id="ans_hsp" value="@if($gastro_data[0]->ans_hsp !=''){{$gastro_data[0]->ans_hsp}}@else 0 @endif"/>
<input type="hidden" id="ans_gastro" value="@if($gastro_data[0]->ans_gastro !=''){{$gastro_data[0]->ans_gastro}}@else 0 @endif"/>
<div class="theadscroll no-padding" style="position: relative; max-height: 400px;">
    <table id="resultDataTable" border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;margin-top:25px;">

        <thead>
            <tr class="table_header_row no-padding"  style="background-color: #07ad8c;color: white;">
                <th colspan="5" style="font-size: 18px !important;text-align:center;">
                    ANESTHESIA
                </th>
            </tr>
            <tr style="background-color:#07ad8c; color: white;">

                    <th style="width:5%">Sl.No</th>
                    <th style="width:65%">Doctor Name</th>
                    <th style="width:10%">Ip Amount</th>
                    <th style="width:10%">Ot Amount</th>
                    <th style="width:10%">Net Amount</th>
                @php
                    $ip_total = 0;
                    $ot_total = 0;
                    $net_total = 0;
                @endphp
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;

            @endphp
            @foreach($result_data as $data)
                @if(trim($data->speciality) == 'ANAESTHESIA')
                    <tr>
                        <td>{{$i}}</td>
                        <td style="text-align:left">{{$data->doctor_name}}</td>
                        <td class="td_common_numeric_rules">{{$data->ip_amount}}</td>
                        <td class="td_common_numeric_rules">{{$data->ot_amount}}</td>
                        <td class="td_common_numeric_rules net_amount">
                            @php
                                $net_amount = 0;
                                $net_amount =  $data->ip_amount +  $data->ot_amount;
                            @endphp
                            {{$net_amount}}
                        </td>
                    </tr>
                    @php
                        $i++;
                        $ip_total += $data->ip_amount;
                        $ot_total += $data->ot_amount;
                        $net_total += $net_amount;
                    @endphp
                @endif
            @endforeach
                <tr>
                    <td>{{$i+1}}</td>
                    <td style="text-align:left">Anesthesia Consultation Charge</td>
                    <td class="td_common_numeric_rules">100</td>
                    <td></td>
                    <td class="td_common_numeric_rules">100</td>
                </tr>
                @php
                    $ip_total += 100;
                    $net_total += 100;
                @endphp
                <tr style="background-color: paleturquoise;font-weight:600;">
                    <td colspan="2">Total</td>
                    <td class="td_common_numeric_rules">{{$ip_total}}
                    <input type="hidden" id="anesthetia_ip_total" value="{{$ip_total}}"/>
                    </td>
                    <td class="td_common_numeric_rules">{{$ot_total}}
                    <input type="hidden" id="anesthetia_ot_total" value="{{$ot_total}}"/>
                    </td>
                    <td class="td_common_numeric_rules">{{$net_total}}</td>
                </tr>
        </tbody>
    </table>
</div>


<div class="col-md-12 box-body" id="anesthesia_calculation_area" style="margin-bottom:15px;padding:30px;">
    <div class="col-md-12" style="margin-top:10px;">
        <button type="button" class="btn btn-primary pull-right" onclick="saveAnesthesiaData();">
            <i class="fa fa-save"></i> Save Changes
        </button>
    </div>
    <div class="col-md-5">
        <div class="col-md-12" style="margin-bottom:10px;">
            <div class="col-md-8">
                <b style="font-size:11px;">IP For Each Doctor <span style="float:right;">:</span></b>
            </div>
            <div class="col-md-4" style="font-size:11px;font-weight:600;">
                <span class="ip_for_each_doctor"></span>
                <span style="float:right;color: darkgray;">
                    (<span class="total_anesthetia_ip"></span>/4)
                </span>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom:10px;">
            <div class="col-md-8">
                <b style="font-size:11px;">Anesthesia Doctor OT <span style="float:right;">:</span></b>
            </div>
            <div class="col-md-4" style="font-size:11px;font-weight:600;">
                @if($gastro_data[0]->ans_hsp !=''){{$gastro_data[0]->ans_hsp}}@else 0 @endif<span style="float:right;color:darkgray;">(+)</span>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom:10px;">
            <div class="col-md-8">
                <b style="font-size:11px;">Varghese/Rohini <span style="float:right;">:</span></b>
            </div>

            <div class="col-md-4" style="font-size:11px;font-weight:600;">
                <span class="total_anesthesia_ot"></span> <span style="float:right;color:darkgray;">(+)</span>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom:10px;">
            <div class="col-md-8">
                <b style="font-size:11px;">Gastro <span style="float:right;">:</span></b>
            </div>

            <div class="col-md-4" style="font-size:11px;font-weight:600;">
                @if($gastro_data[0]->ans_gastro !=''){{$gastro_data[0]->ans_gastro}}@else 0 @endif<span style="float:right;color:darkgray;">(-)</span>
            </div>
        </div>

        <div class="col-md-12" style="margin-bottom:10px;">
            <div class="col-md-8">
                <b style="font-size:11px;">Insurance <span style="float:right;">:</span></b>
            </div>
            <div class="col-md-4" style="font-size:11px;font-weight:600;">
                <input type="text" id="anesthesia_insurance_charge" class="form-control" value="0" style="font-size:11px;font-weight:600;width:80px;float:left;clear:right;margin-right:3px;">
                <span style="float:right;margin-top:3px;color:darkgray;">(+)</span>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom:10px;">
            <div class="col-md-8">
                <b style="font-size:13px;">Total Amount <span style="float:right;">:</span></b>
            </div>
            <div class="col-md-4" style="font-size:13px;font-weight:600;">
                <span class="anesthesia_total_amount"></span>
            </div>
        </div>
    </div>

    <div class="col-md-7">
        <table border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;margin-top:25px;">
            <thead>
                <tr style="background-color:#07ad8c; color: white;">
                    <th style="width:5%">#</th>
                    <th style="width:25%">Doctor Name</th>
                    <th style="width:17.5%">Ip Amount</th>
                    <th style="width:17.5%">Insurance Amount</th>
                    <th style="width:17.5%">Ot Amount</th>
                    <th style="width:17.5%">Final Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td style="text-align:left;">K. A. VARGHESE</td>
                    <td class="total_ip_for_individual"></td>
                    <td class="total_insurance_for_individual"></td>
                    <td class="ot_individual_varghese"></td>
                    <td class="net_amount_varghese"></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td style="text-align:left;">ROHINI K P</td>
                    <td class="total_ip_for_individual"></td>
                    <td class="total_insurance_for_individual"></td>
                    <td class="ot_individual_rohini"></td>
                    <td class="net_amount_rohini"></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td style="text-align:left;">MUKESH MUKUNDAN</td>
                    <td class="total_ip_for_individual"></td>
                    <td class="total_insurance_for_individual"></td>
                    <td class="ot_individual_mukesh"></td>
                    <td class="net_amount_mukesh"></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td style="text-align:left;">SACHIN S</td>
                    <td class="total_ip_for_individual"></td>
                    <td class="total_insurance_for_individual"></td>
                    <td class="ot_individual_sachin"></td>
                    <td class="net_amount_sachin"></td>
                </tr>
            </tbody>
        </table>
    </div>




    {{-- <div class="col-md-12">
        <div class="col-md-4">
            <div class="col-md-12">
                <div class="col-md-4">
                    <b style="font-size:11px;">Total OT</b>
                </div>
                <div class="col-md-4">
                    <label id="anesthetia_total_ot" style="font-size:11px;
                        font-weight:600;">{{$ot_total}}</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4">
                    <b style="font-size:11px;">Gastro Charge</b>
                </div>
                <div class="col-md-4">

                </div>
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <div class="col-md-4">
                    <b style="font-size:11px;">Net Amount</b>
                </div>
                <div class="col-md-4">
                    <label id="anesthetia_ot_gastro_addition" style="font-size:11px;
                        font-weight:600;"></label>
                </div>
            </div>
            <div class="col-md-12" style="margin-top:20px;">

            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <div class="col-md-4">
                    <b style="font-size:11px;">Insurance Amount:</b>
                </div>
                <div class="col-md-4">
                    <label id="anesthetia_total_insurance_amount" style="font-size:11px;
                        font-weight:600;"></label>
                </div>
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <div class="col-md-4">
                    <b style="font-size:11px;">Final amount:</b>
                </div>
                <div class="col-md-4">
                    <label id="anesthetia_insurance_and_net_sum" style="font-size:11px;
                        font-weight:600;"></label>
                </div>
            </div>
        </div>
        <div class="col-md-8">

        </div>
    </div> --}}

</div>
