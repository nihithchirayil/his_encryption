 <!----Gastro------------------------>
 <div class="theadscroll no-padding" style="position: relative; max-height: 400px;">
    <table id="resultDataTable" border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;margin-top:25px;">

        <thead>
            <tr class="table_header_row no-padding"  style="background-color: #07ad8c;color: white;">
                <th colspan="12" style="font-size: 18px !important;text-align:center;" colspan="{{$head_array_size+1}}">
                    GASTROENTROLOGY
                </th>
            </tr>
            <tr style="background-color:#07ad8c; color: white;">

                    <th style="width:5%">Sl.No</th>
                    <th style="width:15%">Speciality</th>
                    <th style="width:17%">Doctor Name</th>
                    <th style="width:7%">Op Amount</th>
                    <th style="width:7%">Ip Amount</th>
                    <th style="width:7%">Procedure Charges</th>
                    <th style="width:7%">Ot Amount</th>
                    <th style="width:7%">Health Checkup</th>
                    <th style="width:7%">Insurance Amount</th>
                    <th style="width:7%">Bill Analysis</th>
                    <th style="width:7%">Fixed Salary</th>
                    <th style="width:7%">Net Amount</th>
                @php
                    $op_total = 0;
                    $ip_total = 0;
                    $procedure_total = 0;
                    $ot_total = 0;
                    $bill_analysis_total = 0;
                    $health_checkup_total = 0;
                    $insurance_total = 0;
                    $fixed_salary_total = 0;
                    $net_total = 0;
                @endphp
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;

            @endphp
            @foreach($result_data as $data)
                @if(trim($data->speciality) == 'GASTROENTEROLOGY')
                    <tr>
                        <td>{{$i}}</td>
                        <td style="text-align:left">{{$data->speciality}}</td>
                        <td style="text-align:left">{{$data->doctor_name}}</td>
                        <td class="td_common_numeric_rules">{{$data->op_amount}}</td>
                        <td class="td_common_numeric_rules">{{$data->ip_amount}}</td>
                        <td onclick="procedure_details('{{$data->id}}','{{$data->doctor_id}}');" class="td_common_numeric_rules bill_analysis">{{$data->procedure_charges}}</td>
                        <td class="td_common_numeric_rules">{{$data->ot_amount}}</td>
                        <td class="td_common_numeric_rules">{{$data->health_checkup}}</td>
                        <td class="td_common_numeric_rules insurance_amount" data-attr-id='{{$data->id}}'>
                            <i style="display:none;float:left;" class="fa fa-check insurance_amount_check" aria-hidden="true" onclick="changeInsuranceAmount(this,'{{$data->id}}');"></i>
                            <input style="width:75%;display:none;float:right;" type="text" name="insurance_amount" class="insurance_amount_text" data-attr-id='{{$data->id}}' value="{{$data->insurance_amount}}"/>
                            <span id="insurance_amount_span_{{$data->id}}" style="display:block" class="insurance_amount_span">{{$data->insurance_amount}}</span>
                        </td>
                        <td onclick="bill_analysis_detail('{{$data->id}}','{{$data->doctor_id}}');" class="td_common_numeric_rules bill_analysis">{{$data->bill_analysis}}</td>
                        <td class="td_common_numeric_rules">{{$data->fixed_salary}}</td>
                        <td class="td_common_numeric_rules net_amount">
                            @php
                                $net_amount = 0;
                                $net_amount = $data->op_amount + $data->ip_amount + $data->procedure_charges + $data->ot_amount + $data->health_checkup + $data->insurance_amount + $data->bill_analysis + $data->fixed_salary;
                            @endphp
                            {{$net_amount}}</td>
                    </tr>
                    @php
                        $i++;
                        $op_total += $data->op_amount;
                        $ip_total += $data->ip_amount;
                        $procedure_total += $data->procedure_charges;
                        $ot_total += $data->ot_amount;
                        $bill_analysis_total += $data->bill_analysis;
                        $health_checkup_total += $data->health_checkup;
                        $insurance_total += $data->insurance_amount;
                        $fixed_salary_total += $data->fixed_salary;
                        $net_total += $net_amount;
                    @endphp
                @endif
            @endforeach
                <tr style="background-color: paleturquoise;">
                    <td colspan="3">Total</td>
                    <td class="td_common_numeric_rules">{{$op_total}}</td>
                    <td class="td_common_numeric_rules">{{$ip_total}}</td>
                    <td class="td_common_numeric_rules">{{$procedure_total}}</td>
                    <td class="td_common_numeric_rules">{{$ot_total}}</td>
                    <td class="td_common_numeric_rules">{{$health_checkup_total}}</td>
                    <td class="td_common_numeric_rules">{{$insurance_total}}</td>
                    <td class="td_common_numeric_rules">{{$bill_analysis_total}}</td>
                    <td class="td_common_numeric_rules">{{$fixed_salary_total}}</td>
                    <td class="td_common_numeric_rules">{{$net_total}}</td>
                </tr>
        </tbody>
    </table>
</div>
