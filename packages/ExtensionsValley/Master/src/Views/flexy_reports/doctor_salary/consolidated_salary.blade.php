<div class="col-md-12">
    <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
        <button type="button" onclick="printReportData();" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="print_results" id="print_results">
            <i class="fa fa-print" aria-hidden="true"></i> Print
        </button>
    </div>

    <div class="col-xs-1 padding-sm" style="float:right;padding-top: 25px;">
        <button type="button" onclick="exceller()" class="btn bg-primary btn-block" style="color:white !important;width:101px !important;" name="csv_results" id="csv_results">
            <i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel
        </button>
    </div>
</div>
<div class="col-md-12 no-padding" id="consolidated_salary_results">
    <div class="ResultDataContainer"
        style=" padding:10px;display:none;font-family:poppinsregular;">
        <style>
            @media print {
                .theadscroll {max-height: none; overflow: visible;}
            }
        </style>
        <div style="float: left; width: 100% !important;" id="ConsolidatedResultsViewArea">
        </div>
    </div>
</div>
