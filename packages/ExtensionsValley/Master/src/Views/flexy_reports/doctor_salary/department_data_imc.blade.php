<div class="theadscroll no-padding" style="position: relative; max-height: 400px;">
    <table id="resultDataTable" border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;">

        <thead>
            <tr class="table_header_row no-padding"  style="background-color: #07ad8c;color: white;">
                <th colspan="3" style="font-size: 18px !important;text-align:center;">
                    INTERNAL MEDICINE
                </th>
            </tr>
            <tr style="background-color:#07ad8c; color: white;">
                    <th width="5% !important;">Sl.No</th>
                    <th width="65% !important;">Doctor Name</th>
                    <th width="30% !important;">Bill Analysis</th>
                @php
                    $bill_analysis_total = 0;
                @endphp
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
            @endphp
            @foreach($result_data as $data)
                @if(trim($data->speciality) == 'INTERNAL MEDICINE')
                    <tr>
                        <td>{{$i}}</td>
                        <td style="text-align:left">{{$data->doctor_name}}</td>
                        <td onclick="bill_analysis_detail('{{$data->id}}','{{$data->doctor_id}}');" class="td_common_numeric_rules bill_analysis">{{$data->bill_analysis}}</td>
                    </tr>
                    @php
                        $i++;
                        $bill_analysis_total += $data->bill_analysis;
                    @endphp
                @endif
            @endforeach
                <tr style="background-color: paleturquoise;">
                    <td colspan="2">Total</td>
                    <td class="td_common_numeric_rules">{{$bill_analysis_total}}</td>
                </tr>
        </tbody>
    </table>
</div>
<div class="col-md-12 box-body" id="imc_calculation_area" style="margin-bottom:15px;padding:30px;">
    <div class="col-md-12" style="margin-top:10px;">
        <button type="button" class="btn btn-primary pull-right" onclick="saveIMCData();">
            <i class="fa fa-save"></i> Save Changes
        </button>
        <b>Bill Analysis Total : {{$bill_analysis_total}} + Insurance Settled Amount</b>
        <input class="number_only" style="width:100px;
        border: 1px solid gainsboro;
        text-align: right;
        padding-right: 10px;
        margin-left: 10px;" data-attr-bill_analysis_total="{{$bill_analysis_total}}" tpye="text" name="ins_settled_amt" id="ins_settled_amt" value="0"/>&nbsp;&nbsp; = &nbsp;&nbsp;<label style="font-size: 13px;font-weight: 600;" id="bill_analyis_and_ins_total">{{$bill_analysis_total}}</label></b>
    </div>
    <div class="col-md-12" style="margin-top:8px;margin-bottom:25px;">
        <div class="col-md-6" style="font-size:11px;">
            <div class="col-md-3"><b>Doctor Name</b></div>
            <div class="col-md-3"><b>Salary</b></div>
            <div class="col-md-3"><b>Additional</b></div>
            <div class="col-md-3"><b>Total</b></div>
        </div>
        <div class="clearfix"></div>
        @php
            $temp_doctors = \DB::table('temporary_doctor_master')
            ->where('speciality','34')
            ->where('salary_type','F')
            ->orderBy('doctor_name')->select('doctor_name','salary','id')->get();
        @endphp
        <br>
        @foreach($temp_doctors as $temp_doctor)

            <div class="col-md-6">
                <div class="col-md-3">
                    <b style="font-size:11px;">{{$temp_doctor->doctor_name}}</b>
                </div>
                <div class="col-md-3">{{$temp_doctor->salary}}</div>
                <div class="col-md-3">
                    <input style="width:100px;
                        border: 1px solid gainsboro;
                        text-align: right;
                        padding-right: 10px;" data-attr-doctor_salary="{{$temp_doctor->salary}}"  data-attr-doctor_id="{{$temp_doctor->id}}" id="doctor_additional_{{$temp_doctor->id}}" tpye="text" class="doctor_additional number_only" value="0"/>
                </div>
                <div class="col-md-3">
                    <label style="font-size:12px;
                    font-weight:600;" data-attr-doctor_id="{{$temp_doctor->id}}" class="doctor_settled_total">{{$temp_doctor->salary}}</label>
                </div>
            </div>
            <div class="clearfix"></div>

        @endforeach

        <div class="clearfix"></div>
        <div class="col-md-6" style="margin-top:15px">
            <div class="col-md-3">
                <b style="font-size:11px;">Total Salary</b>
            </div>
            <div class="col-md-3">
                <label id="doctors_settled_total_salary" style="font-size:13px;
                font-weight:600;"></label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6" style="margin-bottom:15px;">
            <div class="col-md-3">
                <b style="font-size:11px;">Remaining Amount</b>
            </div>
            <div class="col-md-3">
                <label id="doctors_settled_remaining_amount" style="font-size:13px;
                font-weight:600;"></label>
            </div>
        </div>

        @php
            $main_doctors = \DB::table('temporary_doctor_master')
            ->where('speciality','34')
            ->where('salary_type','FV')
            ->orderBy('doctor_name')->select('doctor_name','salary','id')->get();
            @endphp
        @foreach ($main_doctors as $data)
            <div class="clearfix"></div>
            <div class="col-md-6">
                <div class="col-md-4">
                    <b style="font-size:11px;">{{strtoupper($data->doctor_name)}}</b>
                </div>
                <div class="col-md-3" >
                    <label style="font-size:13px;
                    font-weight:600;" class="imc_main_doctors" data-attr-id="{{$data->id}}"></label>
                </div>
            </div>
        @endforeach

    </div>
</div>
