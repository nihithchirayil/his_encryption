
<style>
    @media print {
        .theadscroll {max-height: none; overflow: visible;}
        .header_bg {
            background-color: #08898d;
            color: white;
        }
        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #f2fef5;
        }

        #resultDataTable{
            border: 1px solid #cedfe9 !important;
            border-collapse: collapse !important;
        }
        td{
            padding-left: 5px !important;
        }
        }
    .global-footer {
        position: fixed;
        width: 90.5%;
        bottom: 38px;
        /* left: 0; */
        /* padding: 1em */
    }
    .card{
        border: 1px solid gainsboro;
        border-radius: 5px;
        margin-top: 5px;
        background-color: white;
    }
    .btn-link{
        font-size: 12px;
        font-weight: 600;

    }
    .btn-link:hover{
        text-decoration: none;
    }
</style>

<div class="col-md-12" style="margin-bottom:15px;">
    <input type="hidden" name="is_imc_disabled" id="is_imc_disabled" value="{{$is_imc_disabled}}">
    <input type="hidden" name="is_anes_disabled" id="is_anes_disabled" value="{{$is_anes_disabled}}">
        @if(!empty($result_data)>0)
        <div id="ResultsPrintViewArea">
            @php
                $head_array = $result_data[0];
                $total_colums = explode(",", $flexy_report_totals);
                $total_array = [];
                $total_array_index = [];
                $drill_down_array = [];
                $size_array = [];
                $j = 0;
                $head_array_size = 0;
                $custom_column_width = explode(',', $custom_column_width);
                $drill_down_columns = str_replace("@","",$drill_down_columns);
                $drill_down_columns = explode(',', $drill_down_columns);
                $drill_down_values = '';
                $numeric_colums = explode(',', $numeric_colums);
            @endphp

            <div id="accordion">

                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            INTERNAL MEDICINE
                        </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body"  style="padding:10px;">
                            @include('Master::flexy_reports.doctor_salary.department_data_imc')
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            ANESTHESIA
                        </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body" style="padding:10px;">
                            @include('Master::flexy_reports.doctor_salary.department_data_anesthesia')
                        </div>
                    </div>
                </div>

                <div class="card">
                <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        GASTROENTROLOGY
                    </button>
                    </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body" style="padding:10px;">
                        @include('Master::flexy_reports.doctor_salary.department_data_gastro')
                    </div>
                </div>
                </div>
            </div>
        </div>

    @else
        <div class="alert alert-danger">
            No data found!
        </div>
    @endif
</div>
<script>
    $('#ins_settled_amt').on('keyup', function(){
        var bill_analysis_total = $('#ins_settled_amt').attr('data-attr-bill_analysis_total');
        var ins_settled_amt = $('#ins_settled_amt').val();
        if(ins_settled_amt == ''){
            ins_settled_amt = 0;
        }
        var bill_analyis_and_ins_total = parseFloat(bill_analysis_total) + parseFloat(ins_settled_amt);
        $('#bill_analyis_and_ins_total').html('');
        $('#bill_analyis_and_ins_total').html(bill_analyis_and_ins_total);
    });

    $('.doctor_additional').on('keyup', function(){
        var doctor_additional = $(this).val();
        var doctor_salary = $(this).attr('data-attr-doctor_salary');
        if(doctor_additional == ''){
            doctor_additional = 0;
        }
        var doctor_salary_total = parseFloat(doctor_salary) + parseFloat(doctor_additional);
        $(this).parent().parent().find('.doctor_settled_total').html('');
        $(this).parent().parent().find('.doctor_settled_total').html(doctor_salary_total);
        doctor_settled_total_total = 0;
        $('.doctor_settled_total').each(function(){
            if($(this).html() !=''){
                doctor_settled_total_total += parseFloat($(this).html());
            }
        });
        $('#doctors_settled_total_salary').html(doctor_settled_total_total);

        //remaing amount------------------
        var bill_analysis_total = $('#ins_settled_amt').attr('data-attr-bill_analysis_total');
        var doctor_settled_total_total = $('#doctors_settled_total_salary').html();
        var remaining_amount = parseFloat(bill_analysis_total) - parseFloat(doctor_settled_total_total);
        $('#doctors_settled_remaining_amount').html('');
        $('#doctors_settled_remaining_amount').html(remaining_amount);

        $(".imc_main_doctors[data-attr-id='100014']").html(RoundNum(remaining_amount/2));
        $(".imc_main_doctors[data-attr-id='100015']").html(RoundNum(remaining_amount/4));
        $(".imc_main_doctors[data-attr-id='100013']").html(RoundNum(remaining_amount/4));
    });


    $('.number_only').keydown(function(event){
        if(event.keyCode == 8 || event.keyCode == 46)
            return true;
        if(event.keyCode >= 96 && event.keyCode <= 105)
            return true;

        if(isNaN(parseInt(String.fromCharCode(event.keyCode),10)))
        return false;
    });


    function saveIMCData(){
        var salary_100014 = $(".imc_main_doctors[data-attr-id='100014']").html();
        var salary_100015 = $(".imc_main_doctors[data-attr-id='100015']").html();
        var salary_100013 = $(".imc_main_doctors[data-attr-id='100013']").html();
        var base_url = $('#base_url').val();
        if(salary_100014 ==''){
            bootbox.alert("Please enter salary details!");
            return false;
        }
        var junior_dr_data = [];

        $('.doctor_additional').each(function(){
            var dr_id = $(this).attr('data-attr-doctor_id');
            junior_dr_data.push({
                'id': dr_id,
                'additional_salary': $(this).val(),
                'salary': $('.doctor_settled_total[data-attr-doctor_id="'+dr_id+'"]').html(),
            });
        });
        //console.log()
        var month = $('#month').val();

        var data = {
            '_token': $('meta[name="csrf-token"]').attr('content'),
            'salary_100014': salary_100014,
            'salary_100015': salary_100015,
            'salary_100013': salary_100013,
            'junior_dr_data': junior_dr_data,
            'month': month
        };
        $.ajax({
            url: base_url+'/flexy_report/saveIMCData',
            type: "POST",
            data: data,
            beforeSend: function() {
                $('#imc_calculation_area').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if(data == 1){
                    bootbox.alert('Salary Processed successfully!');
                    $('#imc_calculation_area').find('input, textarea, button, select').attr('disabled','disabled');
                    $('#imc_calculation_area').css('opacity','0.5');
                }
                else{
                    bootbox.alert("Something went wrong!");
                }

                $('#imc_calculation_area').LoadingOverlay("hide");
            }
        });
    }

    $('.btn-link').click(function(){
        $('.theadscroll').perfectScrollbar("update");
        $(".theadfix_wrapper").floatThead('reflow');
    });

    $('#anesthesia_insurance_charge').on('keyup', function(){
        calculate_anesthetia();
    });

    function calculate_anesthetia(){
        var anesthesia_insurance_charge = $('#anesthesia_insurance_charge').val();
        var anesthetia_ip_total = $('#anesthetia_ip_total').val();
        var ans_gastro = $('#ans_gastro').val();
        var ans_hsp = $('#ans_hsp').val();
        var total_ot = $('#anesthetia_ot_total').val();
        var anesthesia_total_amount = parseFloat(ans_hsp)+parseFloat(total_ot)-parseFloat(ans_gastro)+parseFloat(anesthesia_insurance_charge);


        $('.ip_for_each_doctor').html(parseFloat(anesthetia_ip_total/4));

        $('.total_anesthetia_ip').html(anesthetia_ip_total);
        $('.total_anesthesia_ot').html(total_ot);
        $('.anesthesia_total_amount').html(anesthesia_total_amount);

        $('.total_insurance_for_individual').html('');
        $('.total_insurance_for_individual').html(parseFloat(anesthesia_insurance_charge/4));
        $('.total_ip_for_individual').html('');
        $('.total_ip_for_individual').html(parseFloat(anesthetia_ip_total/4));

        var ot_individual_varghese = parseFloat((anesthesia_total_amount)*(3.3/10));
        var ot_individual_rohini = parseFloat((anesthesia_total_amount)*(3/10));
        var ot_individual_mukesh = parseFloat((anesthesia_total_amount)*(1.85/10));
        var ot_individual_sachin = parseFloat((anesthesia_total_amount)*(1.85/10));

        $('.ot_individual_varghese').html('');
        $('.ot_individual_rohini').html('');
        $('.ot_individual_mukesh').html('');
        $('.ot_individual_sachin').html('');
        $('.ot_individual_varghese').html(RoundNum(ot_individual_varghese));
        $('.ot_individual_rohini').html(RoundNum(ot_individual_rohini));
        $('.ot_individual_mukesh').html(RoundNum(ot_individual_mukesh));
        $('.ot_individual_sachin').html(RoundNum(ot_individual_sachin));

        var net_amount_varghese = parseFloat(ot_individual_varghese)+parseFloat(anesthetia_ip_total/4)+parseFloat(anesthesia_insurance_charge/4);
        var net_amount_rohini = parseFloat(ot_individual_rohini)+parseFloat(anesthetia_ip_total/4)+parseFloat(anesthesia_insurance_charge/4);
        var net_amount_mukesh = parseFloat(ot_individual_mukesh)+parseFloat(anesthetia_ip_total/4)+parseFloat(anesthesia_insurance_charge/4);
        var net_amount_sachin = parseFloat(ot_individual_sachin)+parseFloat(anesthetia_ip_total/4)+parseFloat(anesthesia_insurance_charge/4);

        $('.net_amount_varghese').html(RoundNum(net_amount_varghese));
        $('.net_amount_rohini').html(RoundNum(net_amount_rohini));
        $('.net_amount_mukesh').html(RoundNum(net_amount_mukesh));
        $('.net_amount_sachin').html(RoundNum(net_amount_sachin));

    }

    function saveAnesthesiaData(){
        var dataArray = [];
        var base_url = $('#base_url').val();
        var token = $('#token').val();
        var month = $('#month').val();
        var net_amount_varghese = $('.net_amount_varghese').html();
        var net_amount_rohini = $('.net_amount_rohini').html();
        var net_amount_mukesh = $('.net_amount_mukesh').html();
        var net_amount_sachin = $('.net_amount_sachin').html();

        var ot_amount_varghese = $('.ot_individual_varghese').html();
        var ot_amount_rohini = $('.ot_individual_rohini').html();
        var ot_amount_mukesh = $('.ot_individual_mukesh').html();
        var ot_amount_sachin = $('.ot_individual_sachin').html();

        var anaes_gastro_amount = $('#ans_gastro').val();
        var anaes_ins_amount = $('#anesthesia_insurance_charge').val();

        var total_ip_for_individual = $('.total_ip_for_individual').html();
        var total_insurance_for_individual = $('.total_insurance_for_individual').html();

        dataArray[0] = {'doctor_id':133,'net_amount':net_amount_varghese,'ot_amount':ot_amount_varghese};
        dataArray[1] = {'doctor_id':56,'net_amount':net_amount_rohini,'ot_amount':ot_amount_rohini};
        dataArray[2] = {'doctor_id':270,'net_amount':net_amount_mukesh,'ot_amount':ot_amount_mukesh};
        dataArray[3] = {'doctor_id':271,'net_amount':net_amount_sachin,'ot_amount':ot_amount_sachin};

        var dataparams = {
            'doctors_data':dataArray,
            'month':month,
            'anaes_gastro_amount':anaes_gastro_amount,
            'anaes_ins_amount':anaes_ins_amount,
            'total_ip_for_individual':total_ip_for_individual,
            'total_insurance_for_individual':total_insurance_for_individual,
            '_token':token
        };

        $.ajax({
            url: base_url+'/flexy_report/DoctorSalarySaveAnesthesiaData',
            type: "POST",
            data: dataparams,
            beforeSend: function() {
                $('#anesthesia_calculation_area').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (data) {
                if(data == 1){
                    bootbox.alert('Salary Processed successfully!');
                    $('#anesthesia_calculation_area').find('input, textarea, button, select').attr('disabled','disabled');
                    $('#anesthesia_calculation_area').css('opacity','0.5');
                }
                else{
                    bootbox.alert("Something went wrong!");
                }

                $('#anesthesia_calculation_area').LoadingOverlay("hide");
            }
        });

    }

</script>
