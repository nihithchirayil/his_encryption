<div class="col-md-12 no-padding">
    <div class="col-md-6">
        {{-- <h5 style="color:rgb(7, 7, 87);">{{$doctor_name}}</h5> --}}
    </div>
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-primary btn-sm" onclick="printDiv('doctor_salary_op_details_table_data')"><i class="fa fa-print"></i> Print</button>
    </div>
</div>
<div id="doctor_salary_op_details_table_data" class="col-md-12 theadscroll" style="height:300px;">
    <h4 style="color:rgb(7, 7, 87);"><b>Doctor Salary OP Details - ( {{$salary_month_label}} )</b></h4>
    <h5 style="color:rgb(7, 7, 87);"><b>Doctor : {{$doctor_name}}</b></h5>
    <table border="1" id="procedure_services_table" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;">
        <thead>
            <tr style="background-color: paleturquoise;font-weight:600;">
                <td width="5%">Si.No</td>
                <td width="18%">Doctor Name</td>
                <td width="7%">Consultation Fee</td>
                <td width="7%">Hospital Fee</td>
                <td width="7%">MLC Charge</td>
                <td width="7%">Convenience Fee</td>
                <td width="7%">Total Op Amount</td>
                <td width="7%">Consultation Refund Amount</td>
                <td width="7%">Hospital Refund Amount</td>
                <td width="7%">Convience Refund Amount</td>
                <td width="7%">Total Refund Amount</td>
                <td width="7%">Net Consultation Amount</td>
                <td width="7%">Net Amount</td>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
                $total_consultation_fee = 0;
                $total_hospital_fee = 0;
                $total_mlc_charge = 0;
                $total_convenience_fee = 0;
                $total_total_op_amount = 0;
                $total_consultation_refund_amt = 0;
                $total_hosp_refund_amt = 0;
                $total_conv_refund_amt = 0;
                $total_tot_refund_amount = 0;
                $total_net_consultation_amount = 0;
                $total_net_amount = 0;

            @endphp
            @if (sizeof($result_data)>0)
                @foreach($result_data as $data)
                <tr>
                    <td>{{$i}}</td>
                    <td style="text-align:left">{{$data->doctor_name}}</td>
                    <td style="">{{number_format($data->consultation_fee,2)}}</td>
                    <td style="">{{number_format($data->hospital_fee,2)}}</td>
                    <td style="">{{number_format($data->mlc_charge,2)}}</td>
                    <td style="">{{number_format($data->convenience_fee,2)}}</td>
                    <td style="">{{number_format($data->total_op_amount,2)}}</td>
                    <td style="">{{number_format($data->consultation_refund_amt,2)}}</td>
                    <td style="">{{number_format($data->hosp_refund_amt,2)}}</td>
                    <td style="">{{number_format($data->conv_refund_amt,2)}}</td>
                    <td style="">{{number_format($data->tot_refund_amount,2)}}</td>
                    <td style="">
                        @php
                            $net_consultation_amount = $data->consultation_fee - $data->consultation_refund_amt;
                        @endphp
                        {{number_format($net_consultation_amount,2)}}
                    </td>
                    <td style="">{{number_format($data->net_amount,2)}}</td>
                </tr>
                    @php
                        $total_consultation_fee += $data->consultation_fee;
                        $total_hospital_fee += $data->hospital_fee;
                        $total_mlc_charge += $data->mlc_charge;
                        $total_convenience_fee += $data->convenience_fee;
                        $total_total_op_amount += $data->total_op_amount;
                        $total_consultation_refund_amt += $data->consultation_refund_amt;
                        $total_hosp_refund_amt += $data->hosp_refund_amt;
                        $total_conv_refund_amt += $data->conv_refund_amt;
                        $total_tot_refund_amount += $data->tot_refund_amount;
                        $total_net_consultation_amount += $net_consultation_amount;
                        $total_net_amount += $data->net_amount;
                    $i++;
                    @endphp
                @endforeach
                <tr>
                    <td></td>
                    <td style="text-align:left"><b>Total</b></td>
                    <td><b>{{number_format($total_consultation_fee,2)}}</b></td>
                    <td><b>{{number_format($total_hospital_fee,2)}}</b></td>
                    <td><b>{{number_format($total_mlc_charge,2)}}</b></td>
                    <td><b>{{number_format($total_convenience_fee,2)}}</b></td>
                    <td><b>{{number_format($total_total_op_amount,2)}}</b></td>
                    <td><b>{{number_format($total_consultation_refund_amt,2)}}</b></td>
                    <td><b>{{number_format($total_hosp_refund_amt,2)}}</b></td>
                    <td><b>{{number_format($total_conv_refund_amt,2)}}</b></td>
                    <td><b>{{number_format($total_tot_refund_amount,2)}}</b></td>
                    <td><b>{{number_format($total_net_consultation_amount,2)}}</b></td>
                    <td><b>{{number_format($total_net_amount,2)}}</b></td>
                </tr>
            @else
                <tr>
                    <td colspan="13" style="text-align:center;">No Data Found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
<div class="col-md-12" style="margin-top:10px">
    <input type="hidden" name="op_amount_hidden"  id="op_amount_hidden" value="{{$op_amount}}"/>
    <input type="hidden" name="op_doctor_id_hidden"  id="op_doctor_id_hidden" value="{{$doctor_id}}"/>
    <input type="hidden" name="op_salary_month_hidden"  id="op_salary_month_hidden" value="{{$salary_month}}"/>
    <div class="col-md-3">
        Op Percentage:
    </div>
    <div class="col-md-3">
        <input type="text" value="{{$op_percentage}}" onkeyup="recalculate_op_amount();" class="form-control" name="op_percentage" id="op_percentage"/>
    </div>
</div>
<div class="col-md-12" style="margin-top:10px">
    <div class="col-md-3">
        Op Calculated amount:
    </div>
    <div class="col-md-3">
        <input type="text" value="{{$op_calculated_amount}}" class="form-control" name="op_calculated_amount" id="op_calculated_amount"/>
    </div>
    <div class="col-md-3">
        <button onclick="save_calculated_op_amount();" class="btn btn-primary">
            <i class="fa fa-save"></i> Save
        </button>
    </div>
</div>


