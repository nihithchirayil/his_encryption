<div class="col-md-12 no-padding">
    <div class="col-md-6">
        <h5 style="color:rgb(7, 7, 87);">{{$doctor_name}}</h5>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-primary btn-sm" onclick="printDiv('doctor_salary_ip_details_table_data')"><i class="fa fa-print"></i> Print</button>
    </div>
</div>
<div id="doctor_salary_ip_details_table_data" class="col-md-12 theadscroll" style="height:380px;">
    <h4 style="color:rgb(7, 7, 87);"><b>Doctor Salary IP Details - ( {{$salary_month_label}} )</b></h4>
    <h5 style="color:rgb(7, 7, 87);"><b>Doctor : {{$doctor_name}}</b></h5>
    <table border="1" id="procedure_services_table" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;">
        <thead>
            <tr style="background-color: paleturquoise;font-weight:600;">
                <td width="10%">Si.No</td>
                <td width="50%">Service Name</td>
                <td width="40%">Net Amount</td>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
                $total_amt = 0;
            @endphp
            @if (sizeof($result_data)>0)
                @foreach($result_data as $data)
                <tr>
                    <td>{{$i}}</td>
                    <td style="text-align:left">{{$data->service_name}}</td>
                    <td style="">{{number_format($data->net_amount,2)}}</td>
                </tr>
                    @php
                    $total_amt += $data->net_amount;
                    $i++;
                    @endphp
                @endforeach
                <tr>
                    <td></td>
                    <td style="text-align:left"><b>Total</b></td>
                    <td><b>{{number_format($total_amt,2)}}</b></td>
                </tr>
            @else
                <tr>
                    <td colspan="3">No Data Found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>


