<div class="row" id="print_data">
    <div class="col-md-12 " id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b>
                    <?= date('M-d-Y h:i A') ?>
                </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From:
                <?= $from ?> To
                <?= $to ?>
            </p>
            @if($consolidated == 1)
            <input type="hidden" id="printdata_spanspan" value="12">
            @else
            <input type="hidden" id="printdata_spanspan" value="20">
            @endif
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            $consolidated = (int)$consolidated;
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Location Wise Pharmacy Sales Detail
                    Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                @if($consolidated == 1)
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='1%'>Sl.No</th>
                        <th width='5%'>Transaction Type</th>
                        <th width='5%'>Bill No</th>
                        <th width='3%'>Bill Date</th>
                        <th width='10%'>Doctor Name</th>
                        <th width='5%'>Location Name</th>
                        <th width='5%'>UHID</th>
                        <th width='5%'>Patient Name</th>
                        <th width='3%'>Visit Status</th>
                        <th width='3%'>Paid Status</th>
                        <th width='7%'>Bill Created User</th>
                        <th width='3%'>Net Amount</th>

                    </tr>
                </thead>
                <tbody>
                    @if (count($res) != 0)
                    @php
                    $colored_tr='';
                    $unit_mrp = 0;
                    $qty = 0;
                    $selling_price = 0;
                    $unit_tax_amount = 0;
                    $net_amount = 0;
                    @endphp
                    @foreach ($res as $data)

                    <tr style="background:{{ $colored_tr }}">
                        <td class="td_common_numeric_rules">{{ $loop->iteration }}.</td>
                        <td class="common_td_rules">{{ $data->transaction_type }}</td>
                        <td class="common_td_rules">{{ $data->bill_no }}</td>
                        <td class="common_td_rules">{{ $data->bill_date }}</td>
                        <td class="common_td_rules">{{ $data->doctor_name }}</td>
                        <td class="common_td_rules">{{ $data->location_name }}</td>
                        <td class="common_td_rules">{{ $data->uhid }}</td>
                        <td class="common_td_rules">{{ $data->patient_name }}</td>
                        <td class="common_td_rules">{{ $data->visit_status }}</td>
                        <td class="common_td_rules">{{ $data->paid_status }}</td>
                        <td class="common_td_rules">{{ $data->bill_created_user }}</td>
                        <td class="td_common_numeric_rules">{{ $data->net_amount }}</td>

                    </tr>
                    @php
                    $net_amount += floatval($data->net_amount);
                    @endphp
                    @endforeach
                    <tr style="background-color: #c9c9c9;">
                        <th colspan="11" class="common_td_rules">Total</th>
                        <th class="td_common_numeric_rules">{{ number_format($net_amount, 2, '.', '') }}</th>

                    </tr>
                    @else
                    <tr>
                        <td colspan="20" style="text-align: center">
                            No Result Found
                        </td>
                    </tr>
                    @endif
                </tbody>
                @else
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='1%'>Sl.No</th>
                        <th width='5%'>Transaction Type</th>
                        <th width='9%'>Bill No</th>
                        <th width='5%'>Bill Date</th>
                        <th width='5%'>Doctor Name</th>
                        <th width='5%'>Location Name</th>
                        <th width='5%'>UHID</th>
                        <th width='5%'>Patient Name</th>
                        <th width='5%'>Visit Status</th>
                        <th width='5%'>Item Desc</th>
                        <th width='5%'>Batch</th>
                        <th width='5%'>Expiry</th>
                        <th width='5%'>convert Status</th>
                        <th width='5%'>Paid Status</th>
                        <th width='5%'>Bill Created User</th>
                        <th width='5%'>Unit Mrp</th>
                        <th width='5%'>Qty</th>
                        <th width='5%'>Selling Price</th>
                        <th width='5%'>Unit Tax Amount</th>
                        <th width='5%'>Net Amount</th>

                    </tr>


                </thead>
                <tbody>
                    @if (count($res) != 0)
                    @php
                    $colored_tr='';
                    $unit_mrp = 0;
                    $qty = 0;
                    $selling_price = 0;
                    $unit_tax_amount = 0;
                    $net_amount = 0;
                    @endphp
                    @foreach ($res as $data)
                    @if ($data->billconverted_status == 1)
                    @php
                    $colored_tr='';
                    @endphp
                    @else
                    @php
                    $colored_tr='';
                    @endphp
                    @endif
                    <tr style="background:{{ $colored_tr }}">
                        <td class="td_common_numeric_rules">{{ $loop->iteration }}.</td>
                        <td class="common_td_rules">{{ $data->transaction_type }}</td>
                        <td class="common_td_rules">{{ $data->bill_no }}</td>
                        <td class="common_td_rules">{{ $data->bill_date }}</td>
                        <td class="common_td_rules">{{ $data->doctor_name }}</td>
                        <td class="common_td_rules">{{ $data->location_name }}</td>
                        <td class="common_td_rules">{{ $data->uhid }}</td>
                        <td class="common_td_rules">{{ $data->patient_name }}</td>
                        <td class="common_td_rules">{{ $data->visit_status }}</td>
                        <td class="common_td_rules">{{ $data->item_desc }}</td>
                        <td class="common_td_rules">{{ $data->batch }}</td>
                        <td class="common_td_rules">{{ $data->expiry }}</td>
                        <td class="common_td_rules">{{ @$data->converted_status !='' ? $data->converted_status :
                            'Direct' }}</td>
                        <td class="common_td_rules">{{ $data->paid_status }}</td>
                        <td class="common_td_rules">{{ $data->bill_created_user }}</td>
                        <td class="td_common_numeric_rules">{{ $data->unit_mrp }}</td>
                        <td class="td_common_numeric_rules">{{ $data->qty }}</td>
                        <td class="td_common_numeric_rules">{{ $data->selling_price }}</td>
                        <td class="td_common_numeric_rules">{{ $data->unit_tax_amount }}</td>
                        <td class="td_common_numeric_rules">{{ $data->net_amount }}</td>

                    </tr>
                    @php
                    $unit_mrp += floatval($data->unit_mrp);
                    $qty += floatval($data->qty);
                    $selling_price += floatval($data->selling_price);
                    $unit_tax_amount += floatval($data->unit_tax_amount);
                    $net_amount += floatval($data->net_amount);
                    @endphp
                    @endforeach
                    <tr style="background-color: #c9c9c9;">
                        <th colspan="15" class="common_td_rules">Total</th>
                        <th class="td_common_numeric_rules">{{ number_format($unit_mrp, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($qty, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($selling_price, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($unit_tax_amount, 2, '.', '') }}</th>
                        <th class="td_common_numeric_rules">{{ number_format($net_amount, 2, '.', '') }}</th>

                    </tr>
                    @else
                    <tr>
                        <td colspan="20" style="text-align: center">
                            No Result Found
                        </td>
                    </tr>
                    @endif
                </tbody>
                @endif
            </table>

        </div>
    </div>
</div>
