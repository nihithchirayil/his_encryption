<table id="result_data_table" class='table table-condensed theadfix_wrapper table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass" style="background-color:#01987a;color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th width='10%'>Sl. No.</th>
                <th>Bill No</th>
                <th>Bill Tag</th>
                <th>Patient Name</th>
                <th>Bill Date</th>
                <th>Discount Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $net_total=0.0;
        if (count($res)!=0) {
            $i=1;
            foreach ($res as $each) {
                ?>
                <tr>
                    <td class="common_td_rules"><?=$i?></td>
                    <td class="common_td_rules"><?=$each->bill_no?></td>
                    <td class="common_td_rules"><?=$each->bill_tag?></td>
                    <td class="common_td_rules"><?=$each->patient_name?></td>
                    <td class='td_common_numeric_rules'><?=date('M-d-Y', strtotime($each->payment_accounting_date))?></td>
                    <td class='td_common_numeric_rules'><?=$each->amount?></td>
                </tr>
        <?php
            $net_total+= floatval($each->amount);
            $i++;
            }
            ?>
                <tr>
                    <th class="common_td_rules" colspan="5">Total</th>
                    <th class='td_common_numeric_rules'><?=$net_total?></th>
                </tr>
            <?php
        }else{
            ?>
                <tr>
                    <th colspan="6" style="text-align: center;"> No Result Found</th>
                </tr>
            <?php
        }
        ?>
    </tbody>
    </table>
