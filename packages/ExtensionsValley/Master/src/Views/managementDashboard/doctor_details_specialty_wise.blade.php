<table id="result_data_table" class='table table-condensed theadfix_wrapper table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass" style="background-color:#01987a;color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th width='10%'>Sl. No.</th>
                <th>Doctor Name</th>
                <th>Total Amount</th>
        </tr>
    </thead>
    <tbody>
        @php
        $net_total=0.0;
        @endphp
        @if (count($res) > 0)
            @foreach($res as $key => $val)
           
            <tr>
                <td class="common_td_rules" colspan="3" style="background: #d1e2f1;"><b>{{$val['speciality_name']}}</b></td>
            </tr>

            @foreach($val['doctor_list'] as $doctor)
            <tr>
                <td class="common_td_rules">{{$loop->iteration}}</td>
                <td class="common_td_rules">{{$doctor['doctor_name']}}</td>
                <td class="td_common_numeric_rules">{{$doctor['amount']}}</td>
            </tr>

            @php
            $net_total= $net_total + (float)$doctor['amount'];
            @endphp

            @endforeach

            @endforeach
            <tr>    
                <th class="common_td_rules" colspan="2">Total</th>
                <th class='td_common_numeric_rules'><?=$net_total?></th>
            </tr>
        @else
            <tr>
                <th colspan="3" style="text-align: center;"> No Result Found</th>
            </tr>
        @endif
    </tbody>
    </table>
