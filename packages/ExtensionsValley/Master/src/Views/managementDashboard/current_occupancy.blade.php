<table id="result_data_table" class='table table-condensed theadfix_wrapper table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass" style="background-color:#01987a;color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th width='10%'>Sl. No.</th>
                <th>Patient Name</th>
                <th>Bed Name</th>
                <th>Room Type</th>
                <th>Admission Date</th>
                <th>Admission No</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($res)!=0) {
            $i=1;
            foreach ($res as $each) {
                ?>
                <tr>
                    <td class="common_td_rules"><?=$i?></td>
                    <td class="common_td_rules" title="<?=$each->patient_name?>"><?=$each->patient_name?></td>
                    <td class="common_td_rules" title="<?=$each->bed_name?>"><?=$each->bed_name?></td>
                    <td class="common_td_rules" title="<?=$each->room_type_name?>"><?=$each->room_type_name?></td>
                    <td class='td_common_numeric_rules'><?=date('M-d-Y', strtotime($each->actual_admission_date))?></td>
                    <td class='td_common_numeric_rules'><?=$each->admission_no?></td>
                </tr>
        <?php
            $i++;
            }
        }else{
            ?>
                <tr>
                    <th colspan="6" style="text-align: center;"> No Result Found</th>
                </tr>
            <?php
        }
        ?>
    </tbody>
    </table>
