<?php
if(sizeof($resultData)>0){
    if($input_id == 'cash_collected_by'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->name)) }}","{{ $input_id }}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'op_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->uhid)) }}","{{ $input_id }}")'>
    {{ $item->uhid }} | {{ $item->patient_name }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'phone'){
        if (!empty($resultData)) {
            $patient_name='';
            $patient_phone='';
            foreach ($resultData as $each) {
                if(trim($patient_name)!=trim($each->patient_name)){
                    $patient_name=trim($each->patient_name);
                    $patient_phone=trim($each->mobile_no);
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',5,<?= $each->doctor_id ?>)">
    {{ trim($each->mobile_no) }} | {{ trim($each->patient_name) }}

</li>
<?php
            }
        }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'bill_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->bill_no)) }}","{{ $input_id }}")'>
    {{ $item->bill_no }} | {{ $item->bill_tag }} | {{ $item->net_amount_wo_roundoff }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'patient'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->patient_name)) }}","{{ $input_id }}")'>
    {{ $item->uhid }} | {{ $item->patient_name }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'bill_type'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->code)) }}","{{ htmlentities(ucfirst($item->name)) }}","{{ $input_id }}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'company'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->corp_company_type_id)) }}","{{ htmlentities(ucfirst($item->name)) }}","{{ $input_id }}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'scheme'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->tariff_name)) }}","{{ $input_id }}")'>
    <?php echo htmlentities(ucfirst($item->tariff_name)); ?>
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'department'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->department_code)) }}","{{ htmlentities(ucfirst($item->department_name)) }}","{{ $input_id }}")'>
    <?php echo htmlentities(ucfirst($item->department_name)); ?>
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'sub_department'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->subdepartment_code)) }}","{{ htmlentities(ucfirst($item->sub_name)) }}","{{ $input_id }}")'>
    <?php echo htmlentities(ucfirst($item->sub_name)); ?>
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'item'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->code)) }}","{{ htmlentities(ucfirst($item->name)) }}","{{ $input_id }}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>[<i><?php echo htmlentities(ucfirst($item->type)); ?></i>]
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'item_name'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->code)) }}","{{ htmlentities(ucfirst($item->name)) }}","{{ $input_id }}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'doctor_name'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->doctor_name)) }}","{{ $input_id }}")'>
    <?php echo htmlentities(ucfirst($item->doctor_name)); ?>
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'manufacturer'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->manufacturer_name)) }}","{{ $input_id }}")'>
    <?php echo htmlentities(ucfirst($item->manufacturer_name)); ?>
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'generic_name'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->generic_name)) }}","{{ $input_id }}")'>
    <?php echo htmlentities(ucfirst($item->generic_name)); ?>
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'ip_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->uhid))}}","{{$input_id}}")'>
                {{$item->uhid}} | {{$item->patient_name}}
            </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'alternate_phone'){
        if (!empty($resultData)) {
            $patient_name='';
            foreach ($resultData as $each) {
                if(trim($patient_name)!=trim($each->patient_name)){
                    $patient_name=trim($each->patient_name);
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick="editAppointment(<?= $each->appointment_id ?>,<?= $each->token_no ?>,'<?= date('h:i A', strtotime($each->from_time)) ?>','<?= $each->schedule_id ?>',5,<?= $each->doctor_id ?>)">
    {{ trim($each->alternate_mobile_no) }} | {{ trim($each->patient_name) }}

</li>
<?php
            }
        }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'bill_nonew'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->bill_no)) }}","{{ $input_id }}")'>
    {{ $item->bill_no }} | {{ $item->bill_tag }} | {{ $item->net_amount }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    } else if($input_id == 'bill_number'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->bill_tag)) }}","{{ htmlentities(ucfirst($item->bill_no)) }}","{{ $input_id }}")'>
                {{ $item->bill_no }} | {{ $item->bill_tag }}
            </li>
            <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
}else{
    ?>
<li>No results found!</li>
<?php
}
?>
