@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<div class="right_col" >
    <div class="row" style="text-align: right;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                            {!! Form::token() !!}
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Item Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control hidden_search" name="item_name" id="item_name" value="" autocomplete="false">
                                    <input type="hidden" class="form-control" name="item_name_hidden" id="item_name_hidden" value="">
                                    <div id="AjaxDiv" class="ajaxSearchBox" style="margin: 14px 0px 0px 0px !important;z-index: 9999999;max-height: 250px !important"></div>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label ">Location</label>
                                    <div class="clearfix"></div>
                                        <select class="form-control select2" id="location" onchange="selectStock()">
                                            <option value="">Location</option>
                                            <?php
                                            foreach($location as $each){
                                                ?>
                                                <option value='<?= $each->location_code ?>'><?= $each->location_name ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Stock</label>
                                <input type="text" class="form-control" id="store_stock" readonly >
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Days To Be Considered</label>
                                <input type="text" class="form-control" id="no_sales_consider" name="no_sales_consider" >
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Re-Order Days</label>
                                <input type="text" class="form-control" id="re_order_days" name="re_order_days" >
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Ordering Days</label>
                                <input type="text" class="form-control" id="ordering_days" name="ordering_days" >
                                </div>
                            </div>

                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="searchdatabtn" type="button" class="btn btn-block btn-info" onclick="viewCalculate()"><i id="searchdataspin" class="fa fa-eye"></i>
                                    View</button>
                            </div>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" id="common_list_div">

            </div>
        </div>

    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function ($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    $('.fixed_header').floatThead({
    position: 'absolute',
            scrollContainer: true
    });
    });




   $('#item_name').keyup(function (event) {
    var input_id = '';
    var base_url = $("#base_url").val();
    var keycheck = 1; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');

    if (keycheck || event.keyCode == '8') {
        if ($('#item_name_hidden').val() != "") {
            $('#item_name_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var datastring = '';
        if (search_key == "") {
            $("#AjaxDiv").html("");
        } else {
            var url= base_url+'/item/reOrderLevelMaster';
            $.ajax({
                type: "GET",
                url: url,
                data: 'item_search=' + search_key,
                beforeSend: function () {

                    $("#AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    if (html == 0) {
                        $("#AjaxDiv").html("No results found!").show();

                        $("#AjaxDiv").find('li').first().addClass('liHover');

                        return "<span>No result found!!</span>";

                    } else {
                        $("#AjaxDiv").html(html).show();
                        $("#AjaxDiv").find('li').first().addClass('liHover');

                    }

                },

                complete: function () {
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }
    } else {
        ajax_list_key_down('AjaxDiv', event);
    }
});
function selectStock(){
    var location_code = $("#location").val();
    var item_code = $("#item_name_hidden").val();
    var base_url = $("#base_url").val();
    var url= base_url+'/item/reOrderLevelMaster';
            $.ajax({
                type: "GET",
                url: url,
                data: 'location_code=' + location_code+'&item_code='+item_code,
                beforeSend: function () {
                    $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                },
                success: function (html) {
                    $.LoadingOverlay("hide");
                        $("#store_stock").val(html);
                },
                complete: function () {
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
}
function fillProductStockDetails(e,id,name) {
    $('#item_name_hidden').val(id);
    $('#item_name').val(name);
    if($("#location").val() != ''){
        selectStock();
    }
     $("#AjaxDiv").hide();
}
    function number_validation(e) {
    var valid = /^\d{0,12}(\.\d{0,5})?$/.test(e.value),
            val = e.value;
    if (!valid) {
        e.value = val.substring(0, val.length - 1);
    }
}
function viewCalculate(){
    var base_url = $("#base_url").val();
    var url= base_url+'/item/reOrderLevelMasterCreation';
    var item_name_hidden = $("#item_name_hidden").val();
    var location = $("#location").val();
    var store_stock = $("#store_stock").val();
    var no_sales_consider = $("#no_sales_consider").val();
    var re_order_days = $("#re_order_days").val();
    var ordering_days = $("#ordering_days").val();
    var item_name = $("#item_name").val();
    if(item_name == ''){
        toastr.warning('Please select item');
        return false;
    }
    if(location == ''){
        toastr.warning('Please select Location');
        return false;
    }
    if(parseFloat(re_order_days) < 1 || re_order_days == ''){
        toastr.warning('Please enter re order days');
        return false;
    }
    if(parseFloat(ordering_days) < 1 || ordering_days == ''){
        toastr.warning('Please enter ordering days');
        return false;
    }
    if(parseFloat(ordering_days) <= parseFloat(re_order_days)){
        toastr.warning('Ordering days should greater than re order days');
        return false;
    }
    param = {item_name:item_name_hidden,location:location,store_stock:store_stock,no_sales_consider:no_sales_consider,re_order_days:re_order_days,ordering_days:ordering_days}
    if (no_sales_consider){
    $.ajax({
                type: "GET",
                url: url,
                data: param,
                beforeSend: function () {
                    $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                },
                success: function (html) {
                    $.LoadingOverlay("hide");
                        $("#common_list_div").html(html);
                },
                complete: function () {
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
        }else{
            toastr.warning('Please enter days to be considered');
            $("#no_sales_consider").focus();

        }
}
function saveAll(){
    var base_url = $("#base_url").val();
    var url= base_url+'/item/reOrderLevelMasterCreationSave';
    var item_name_hidden = $("#item_name_hidden").val();
    var location = $("#location").val();
    var store_stock = $("#store_stock").val();
    var no_sales_consider = $("#no_sales_consider").val();
    var re_order_days = $("#re_order_days").val();
    var ordering_days = $("#ordering_days").val();
    var sale_qty = $("#sale_qty").val();
    var avg_sales_day = $("#avg_sales_day").val();
    var re_order_level_qty = $("#re_order_level_qty").val();
    var last_appr_reorder = $("#last_appr_reorder").val();
    var last_appr_reorder_date = $("#last_appr_reorder_date").val();
    var ordering_qty = $("#ordering_qty").val();
    if($('#is_manual_update').prop('checked') == true){
       var is_manual_update = 1;
    }else{
        var is_manual_update = 0;
    }
    var item_name = $("#item_name").val();
    if(item_name == ''){
        toastr.warning('Please select item');
        return false;
    }
    if(location == ''){
        toastr.warning('Please select Location');
        return false;
    }
    if(parseFloat(re_order_days) < 1 || re_order_days == ''){
        toastr.warning('Please enter re order days');
        return false;
    }
    if(parseFloat(ordering_days) < 1 || ordering_days == ''){
        toastr.warning('Please enter ordering days');
        return false;
    }
    if(parseFloat(ordering_days) <= parseFloat(re_order_days)){
        toastr.warning('Ordering days should greater than re order days');
        return false;
    }
    if(parseFloat(re_order_level_qty) < 1 || re_order_level_qty == ''){
        toastr.warning('Please enter Re-order level quantity');
        return false;
    }
    if(parseFloat(ordering_qty) < 1 || ordering_qty == ''){
        toastr.warning('Please enter ordering quantity');
        return false;
    }

    param = {is_manual_update:is_manual_update,ordering_qty:ordering_qty,last_appr_reorder_date:last_appr_reorder_date,re_order_level_qty:re_order_level_qty,avg_sales_day:avg_sales_day,sale_qty:sale_qty,item_name:item_name_hidden,location:location,store_stock:store_stock,no_sales_consider:no_sales_consider,re_order_days:re_order_days,ordering_days:ordering_days}
    $.ajax({
                type: "GET",
                url: url,
                data: param,
                beforeSend: function () {
                 $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                },
                success: function (html) {
                    $.LoadingOverlay("hide");
                        if(html==1){
                            Command: toastr["success"]("Successfully updated!");
                        }
                },
                complete: function () {
                    $("#item_name").val('');
                    $("#item_name_hidden").val('');
                    $("#no_sales_consider").val('');
                    $("#re_order_days").val('');
                    $("#ordering_days").val('');
                    $("#sale_qty").val('');
                    $("#avg_sales_day").val('');
                    $("#re_order_level_qty").val('');
                    $("#last_appr_reorder").val('');
                    $("#last_appr_reorder_date").val('');
                    $("#ordering_qty").val('');
                    $('#is_manual_update').prop('checked',false)
                    $("#calculated_div").hide();
                },
                error: function () {
                    $.LoadingOverlay("hide");
                    Command: toastr["error"]("Network Error!");
                    return;
                },

            });
}
function cleardata(){
    window.location.reload();
}
</script>

@endsection
