<div class="right_col" id="calculated_div">
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                            {!! Form::token() !!}
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Sale Qty</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control hidden_search" name="sale_qty" id="sale_qty" value="{{$sales_qty}}" autocomplete="false">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label ">Avg Sales/Day</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" name="avg_sales_day" id="avg_sales_day" value="{{$avg_sales_perday}}" autocomplete="false">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label ">Re-Order Level Qty</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" name="re_order_level_qty" id="re_order_level_qty" value="{{$reorder_level_qty}}" autocomplete="false">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Last Approved Re-Order Qty</label>
                                <input type="text" class="form-control" id="last_appr_reorder" name="last_appr_reorder" value="{{$last_qty}}" readonly>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Last Approved Date</label>
                                <input type="text" class="form-control" id="last_appr_reorder_date" name="last_appr_reorder_date" value="{{$approved_at}}" readonly >
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Ordering Qty</label>
                                <input type="text" class="form-control" id="ordering_qty" name="ordering_qty" value="{{$ordering_qty}}">
                                </div>
                            </div>
                            @if(isset($sales_qty) && $sales_qty == 0)
                            @php $checked = 'checked' @endphp
                            @else
                            @php $checked = '' @endphp
                            @endif
                            <div class="col-md-3 padding_sm">
                                <div class="clearfix"></div>
                                <input type="checkbox" value="" id="is_manual_update" name="is_manual_update" {{$checked}}> Update manually
                            </div>
                            <div class="col-md-1 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg" onclick="saveAll()"><i id="searchdataspin" class="fa fa-save"></i>
                                    Save</button>
                            </div>
                            <div class="col-md-1 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <buttton class="btn btn-block btn-warning" onclick="cleardata();"><i class="fa fa-times"></i> Clear</buttton>
                            </div>


                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" id="common_list_div">
            </div>
        </div>

    </div>
</div>

