{{-- @extends('Dashboard::dashboard.dashboard') --}}
@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">

<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>

    #main{
    min-height: 700px;
    /* background-color: aliceblue; */
    margin: 3px 0px 0px -2px;
    box-shadow: 0px 1px 3px;
    border-radius: 2px;
    padding: 0px !important;

    }
    #heading{
    background-color:white;
    height: 33px;
    color: #555;
;


    }
    #fill_details{
    height: 141px;
    }
    .th {
    text-align: center;
    border: 1px solid whitesmoke;
    color: white;
    }
    table{
    width: 100%;
    background-color: #f1f7f7 !im;
    }
    .btn {
    font-size: 11px;
    padding: 1px 6px;
    box-shadow: none;
    background-color: #2e4863;
    }
    #table {
    box-shadow: 0px 0px 1px;
    padding: 2px 0px 0px 0px;
    margin: -57px 1px 2px 3px;
    min-height: 526px;
    background-color: aliceblue;
    width: 99.5%;
    }
    .td{
    border: 1px solid white;
    padding-left: 2px;
    font-size: 12px;
    }
     #foo tr:nth-of-type(even) {
    background-color: #f3f3f3;
    }
    .table-striped>tbody>tr:nth-of-type(odd) {
    background-color: #FFF;
}
.errspan {
    float: right;
    margin-right: 6px;
    margin-top: -23px;
    position: relative;
    z-index: 2;
    color: #0e0e0e;
    font-size: 18px;
    }


/* width */
.dropdown-menu::-webkit-scrollbar {
  width: 10px;
}

/* Track */
.dropdown-menu::-webkit-scrollbar-track {
  background:white;
}

/* Handle */
.dropdown-menu::-webkit-scrollbar-thumb {
  background: #888;
}

/* Handle on hover */
.dropdown-menu::-webkit-scrollbar-thumb:hover {
  background: #555;
}
.dropdown-menu li:nth-of-type(even) {
    background-color:#99FFFF;
    }






</style>


@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <div  id="main" class="col-md-12 padding_sm" >
            <form action="{{route('extensionsvalley.master.Appointment_bookings_details')}}" id="requestSearchForm" method="POST">
                {!! Form::token() !!}
                <div id="heading" class="col-md-12 " style="font-size: 22px;"> Booking  Details

                        @if (strlen($total_records)>0)
                        <div id="total_record" class="pull-right" style="margin-top:2px;margin-right: 30px;font-size: 14px;">Total Record:{{$total_records}}</div>
                        @else
                        <div id="total_record" class="pull-right" style="margin-top:2px;margin-right: 30px;font-size: 14px;"></div>
                        @endif

                </div>

                <div id="fill_details" class="col-md-12 ">
                            <div style="border-bottom:1px solid #F4F6F5;padding: 11px 0px;">

                            <div class="row">

                                <!-- From Date starts -->
                                <div class="col-md-2 padding_sm ">
                                    <div class="form-group">
                                    <div class="mate-input-box" >
                                        <label class="filter_label">From Date</label>

                                        <input type='text' data-attr="date" id="from_date" name="from_date" autocomplete="off"  value="@if(old('from_date')){{old('from_date')}}@else{{$searchFields['from_date']}}@endif" class="form-control datepicker" placeholder="dd-mm-yyyy" /> <i class="fa fa-calendar errspan"></i>

                                    </div>
                                </div>
                                </div>
                                <!-- From Date ends -->
                                <!-- To Date starts -->
                                <div class="col-md-2 padding_sm">
                                    <div class="form-group">
                                    <div class="mate-input-box" id='datetimepicker'>
                                        <label class="custom_floatlabel">To Date</label>

                                        <input type='text'   data-attr="date" id="to_date"  autocomplete="off" name="to_date" value="@if(old('to_date')){{old('to_date')}}@else{{$searchFields['to_date']}}@endif" class="form-control date_floatinput" placeholder="dd-mm-yyyy" /> <i class="fa fa-calendar errspan"></i>
                                    </div>
                                </div>
                                </div>
                                <!-- To Date ends -->

                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="filter_label">Doctor Name</label>

                                            <input class="form-control hidden_search" value="@if(old('doctor')){{old('doctor')}}@else{{$searchFields['doctor']}}@endif" autocomplete="off" type="text"  id="doctor_name" name="doctor_name" placeholder="Doctor Name"/>
                                            <div id="doctor_list"></div>

                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <div class="" style="padding-top: 3px;padding-left: 43px;">
                                        <label style="font-weight: bold;"> Sort By Patient Name</label>
                                    </div >
                                    <div style="padding-left: 44px;"><input style="cursor: pointer" type="checkbox" name="sort_by" value="1" /> </div>
                                </div>



                                <div class="col-md-2 padding_sm">

                                    <div class="clearfix"></div>
                                    <button  type="button" class="btn btn-block light_purple_bg" onclick="search_clear();">
                                        Clear</button>
                                    <div class="clearfix"></div>
                                    <button  type="button" class="btn btn-block light_purple_bg" onclick="employeeSearch();"><i class="fa fa-search" id="search"></i>
                                        Search</button>
                                </div>
                        </div>

                    </div>

                </div>


                <div id="table" style="cursor: pointer" class="col-md-12">
                    <table>
                        <thead style="background-color: #26B99A;">
                            <th class="th" style="width:5%;">  S.No.</th>
                            <th class="th" style="width:10%;">  UHID</th>
                            <th class="th" style="width:15%;">  Patient Name</th>
                            <th class="th" style="width:10%">  Phone</th>
                            <th class="th" style="width:30%"   >Address</th>
                            <th class="th" style="width:10%">  Visiting Date</th>
                            <th class="th" style="width:5%;">  Token No.</th>
                            <th class="th" style="width:15%;">  Doctor</th>

                        </thead>


                        <tbody id="foo" style="display:contents;" class="">

                                        <tr>
                                            @if(isset($res) && sizeof($res) > 0)
                                            @foreach ($res as $item)
                                            <td class="td">{{($res->currentPage() - 1) * $res->perPage() + $loop->iteration}}.</td>
                                            <td class="td">{{$item->uhid}}</td>
                                            <td class="td">{{$item->patient_name}}</td>
                                            <td class="td">{{$item->mobile_no}}</td>
                                            <td class="td">{{$item->address}}</td>
                                            <td class="td">{{date('d-M-Y ',strtotime($item->booking_date))}}</td>
                                            <td class="td">{{$item->token_no}}</td>
                                            <td class="td">{{$item->doctor_name}}</td>





                                         </tr>

                                    @endforeach



                                 @else
                                    <tr>
                                        <td colspan="4" style="box-shadow: 0px 0px 1px!important;">No Data Found</td>
                                    </tr>
                                @endif

                            </tbody>


                    </table>



                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 text-center">
                    <nav style="text-align:right;">{{ $res->appends(array_merge(request()->all()))->links() }}
                    </nav>
                </div>




                {!! Form::token() !!}
                {!! Form::close() !!}




        </div>
    </div>
</div>
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>


<script type="text/javascript">

//----------------------------DATE PICKER----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

$("input[data-attr='date']").datetimepicker({ format: 'DD-MMM-YYYY' });


//---------------------------ON SEARCH FUNCTION-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


function employeeSearch() {

    $("#search").removeClass('fa fa-search');
    $("#search").addClass('fa fa-spinner fa-spin');
    $('#main').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
    $('#requestSearchForm').submit();


}

//-------------------------CELAR FUNCTION------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------p

function search_clear(){


       $('#from_date').val('');
       $('#to_date').val('');
       $('#doctor_name').val('');

}
$(function(){
    var test = localStorage.input === 'true'? true: false;
    $('input').prop('checked', test || false);
});

$('input').on('change', function() {
    localStorage.input = $(this).is(':checked');
    console.log($(this).is(':checked'));
});

//-------------------------------------------------------------------------------------------------AJAX AUTOCOMPLETE SEARCH----------------------------------------------------------------------------------------------------------------------------------------------
$(document).ready(function(){

$('#doctor_name').keyup(function(){
       var query = $(this).val();
       if(query == ''){
           query= '@';
       }
        var url = "{{route('extensionsvalley.master.fetch')}}";
        $.ajax({

         type:"GET",
         url:url,
         data:{'query':query},
         beforeSend: function () {
                    $('#doctor_list').fadeIn();
                    $('#doctor_list').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                    },
         success:function(data){

                   $('#doctor_list').html(data);
         },
         complete: function () {
                        $('#doctor_list').LoadingOverlay("hide");
                    },
        });

   });

   $(document).on('click', 'li', function(){
       $('#doctor_name').val($(this).text());
       $('#doctor_list').fadeOut();
   });


});
$(document).click(function(){
  $("#doctor_list").hide();
});






</script>
@endsection

