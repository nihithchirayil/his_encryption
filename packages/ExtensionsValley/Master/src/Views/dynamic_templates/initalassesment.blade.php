<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template_mini.css")}}" rel="stylesheet">
<div class="col-md-12 box-body theadscroll" style="min-height:100vh !important;position:relative;">
    <div class="col-md-12" style="height:10vh !important;">

        <h4 class="dynamic-template-name">Inital Assesment</h4>

        <!--<button class="btn btn-primary pull-right btn-main" onclick="saveTemplate();"><i class="fa fa-save"></i> Save</button>
        <button class="btn bg-blue pull-right btn-main" onclick="resetTemplate();"><i class="fa fa-repeat"></i> Reset</button>
        <button class="btn bg-blue pull-right btn-main" onclick="printTemplate();"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
        <button class="btn bg-blue pull-right btn-main" onclick="loadTemplateHistory();"><i class="fa fa-history" aria-hidden="true"></i> History</button>-->
    </div>
    <div class="col-md-12" style="min-height:80vh !important;">
        <h4><div class="col-md-2  data_id_control_id"    attr_control_id="60" attr_dataset_id="14">

        <label>Name</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00021"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00021" id="fc00021" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-1  data_id_control_id"    attr_control_id="34" attr_dataset_id="14">

        <label>Age</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_age"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="age" id="age" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-1  data_id_control_id"    attr_control_id="61" attr_dataset_id="14">

        <label>Sex</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00022"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00022" id="fc00022" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="62" attr_dataset_id="14">

        <label>OP No</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00023"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00023" id="fc00023" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="63" attr_dataset_id="14">

        <label>IP No</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00024"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00024" id="fc00024" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-2  data_id_control_id"    attr_control_id="64" attr_dataset_id="14">

        <label>Ward/Room No</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00025"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00025" id="fc00025" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-9  data_id_control_id"    attr_control_id="65" attr_dataset_id="14">

        <label>Department</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00026"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00026" id="fc00026" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="66" attr_dataset_id="14">

        <label>Consultant</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00027"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00027" id="fc00027" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-8  data_id_control_id"    attr_control_id="58" attr_dataset_id="0">

        <label>Presenting Complaints &amp; History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00019"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00019" id="fc00019" style="height:150px !important;" placeholder=""></textarea>
                    </span>
    </div>


<div class="col-md-4  data_id_control_id"    attr_control_id="59" attr_dataset_id="0">

        <label>Diagnosis</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00020"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00020" id="fc00020" style="height:150px !important;" placeholder=""></textarea>
                    </span>
    </div></h4>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/master/dynamic_templates/js/initalassesment.js?version=" . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
