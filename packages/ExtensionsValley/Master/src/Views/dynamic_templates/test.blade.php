<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template_mini.css")}}" rel="stylesheet">
<div class="col-md-12 box-body theadscroll" style="min-height:100vh !important;position:relative;">
    <div class="col-md-12" style="height:10vh !important;">

        <h4 class="dynamic-template-name">Test</h4>

        <!--<button class="btn btn-primary pull-right btn-main" onclick="saveTemplate();"><i class="fa fa-save"></i> Save</button>
        <button class="btn bg-blue pull-right btn-main" onclick="resetTemplate();"><i class="fa fa-repeat"></i> Reset</button>
        <button class="btn bg-blue pull-right btn-main" onclick="printTemplate();"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
        <button class="btn bg-blue pull-right btn-main" onclick="loadTemplateHistory();"><i class="fa fa-history" aria-hidden="true"></i> History</button>-->
    </div>
    <div class="col-md-12" style="min-height:80vh !important;">
        <h4><div class="btn-group col-md-12  data_id_control_id" data-toggle="buttons"    attr_control_id="67" attr_dataset_id="0">

        <label>Past History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00028"></span>
        <span class="hide_print">
            <ul class="select_button no-padding check-form-input">

                    <li data-value="Hyper Tension" class="">
                        <i class="fa fa-check-circle"></i> Hyper Tension
                        <input type="hidden" name="fc00028" id="fc00028_1" value="">
                    </li>

                    <li data-value="Diabetes Mellitus" class="">
                        <i class="fa fa-check-circle"></i> Diabetes Mellitus
                        <input type="hidden" name="fc00028" id="fc00028_2" value="">
                    </li>

                    <li data-value="Heart Disease" class="">
                        <i class="fa fa-check-circle"></i> Heart Disease
                        <input type="hidden" name="fc00028" id="fc00028_3" value="">
                    </li>

                    <li data-value="TB" class="">
                        <i class="fa fa-check-circle"></i> TB
                        <input type="hidden" name="fc00028" id="fc00028_4" value="">
                    </li>

                    <li data-value="Asthma" class="">
                        <i class="fa fa-check-circle"></i> Asthma
                        <input type="hidden" name="fc00028" id="fc00028_5" value="">
                    </li>

                    <li data-value="Seizures" class="">
                        <i class="fa fa-check-circle"></i> Seizures
                        <input type="hidden" name="fc00028" id="fc00028_6" value="">
                    </li>

                    <li data-value="Bleeding Disorders" class="">
                        <i class="fa fa-check-circle"></i> Bleeding Disorders
                        <input type="hidden" name="fc00028" id="fc00028_7" value="">
                    </li>

                    <li data-value="Dyslipidemia" class="">
                        <i class="fa fa-check-circle"></i> Dyslipidemia
                        <input type="hidden" name="fc00028" id="fc00028_8" value="">
                    </li>

                    <li data-value="Others" class="">
                        <i class="fa fa-check-circle"></i> Others
                        <input type="hidden" name="fc00028" id="fc00028_9" value="">
                    </li>
                            </ul>
        </span>
    </div>




<div class="btn-group col-md-12  data_id_control_id" data-toggle="buttons"    attr_control_id="70" attr_dataset_id="0">

        <label>Personal History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00031"></span>
        <span class="hide_print">
            <ul class="select_button no-padding check-form-input">

                    <li data-value="Alcohol" class="">
                        <i class="fa fa-check-circle"></i> Alcohol
                        <input type="hidden" name="fc00031" id="fc00031_1" value="">
                    </li>

                    <li data-value="IV Drug Abuse" class="">
                        <i class="fa fa-check-circle"></i> IV Drug Abuse
                        <input type="hidden" name="fc00031" id="fc00031_2" value="">
                    </li>

                    <li data-value="Smoking" class="">
                        <i class="fa fa-check-circle"></i> Smoking
                        <input type="hidden" name="fc00031" id="fc00031_3" value="">
                    </li>

                    <li data-value="Others" class="">
                        <i class="fa fa-check-circle"></i> Others
                        <input type="hidden" name="fc00031" id="fc00031_4" value="">
                    </li>
                            </ul>
        </span>
    </div>




<div class="col-md-4  data_id_control_id"    attr_control_id="100" attr_dataset_id="0">

        <label>Date</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00061"></span>
        <span class="hide_print">
            <input type="text" class="form-control datepicker datepicker-form-input" name="fc00061" id="fc00061" placeholder="" value=" ">
        </span>
    </div>



<div class="clearfix"></div>


    <label contenteditable="true">Image Editor</label>
    <div class="clearfix"></div>
    <span class="show_print" id="show_print_canvas_img"></span>
    <span class="hide_print">
        <span id="image_canvas_container">

            <div id="drawr-container" style="border: 2px solid rgb(209, 209, 209); margin: 20px; width: 90% !important; height: 450px !important; overflow: hidden; user-select: none;" class="drawr-container">
                <canvas class="sfx-canvas" width="934" height="447" style="z-index: 5; position: absolute; width: 934.037px; height: 447.037px; top: 503.778px; left: 347.296px;"></canvas><canvas id="canvas_img" class="demo-canvas drawr-test3 active-drawr" width="934" height="447" style="display: block; user-select: none; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAIAAAAC64paAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAB3RJTUUH4wUIDDYyGYFdggAAAC5JREFUOMtjfPXqFQNuICoqikeWiYECMKp5ZGhm/P//Px7p169fjwbYqGZKNAMA5EEI4kUyPZcAAAAASUVORK5CYII=&quot;); transform: translate(0px, 0px); width: 934.037px; height: 447.037px;"></canvas>
            </div><div class="drawr-toolbox drawr-toolbox-zoom" style="position: absolute; z-index: 6; cursor: move; width: 80px; height: auto; color: rgb(255, 255, 255); padding: 2px; background: linear-gradient(rgb(69, 72, 77) 0%, rgb(0, 0, 0) 100%); border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.75) 0px 2px 5px -2px; user-select: none; font-family: sans-serif; font-size: 12px; text-align: center; top: 323.171px; left: 884.014px; display: none;"><div style="padding:5px 0px 5px 0px">Zoom</div><div style="clear:both;font-weight:bold;text-align:center;padding:5px 0px 5px 0px">zoom</div><div style="clear:both;display: inline-block;width: 50px;height: 60px;margin-top:5px;padding: 0;"><input class="slider-component slider-zoom" value="100" style="background:transparent;width: 50px;height: 50px;margin: 0;transform-origin: 25px 25px;transform: rotate(90deg);" type="range" min="0" max="400" step="1"><span>100</span></div></div><div class="drawr-toolbox drawr-toolbox-settings" style="position: absolute; z-index: 6; cursor: move; width: 80px; height: auto; color: rgb(255, 255, 255); padding: 2px; background: linear-gradient(rgb(69, 72, 77) 0%, rgb(0, 0, 0) 100%); border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.75) 0px 2px 5px -2px; user-select: none; font-family: sans-serif; font-size: 12px; text-align: center; top: 323.171px; left: 884.014px; display: none;"><div style="padding:5px 0px 5px 0px">Settings</div><div class="drawrpallete-wrapper" style="position: relative; display: inline-block;"><input type="text" class="color-picker active-drawrpalette" style="display: none;"><button class="color-picker" style="width: 40px; height: 40px; border: 2px solid rgb(204, 204, 204); background-color: rgb(0, 0, 0); cursor: pointer; padding: 0px; font-size: 2em; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAG0lEQVR42mNgwAfKy8v/48I4FeA0AacVDFQBAP9wJkE/KhUMAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-position: 24px 25px;">&nbsp;</button><div style="background: rgb(238, 238, 238); width: 255px; height: 238px; position: absolute; z-index: 8; display: none;"><canvas style="display:block;" class="drawrpallete-canvas" width="255" height="210"></canvas><div style="height:28px;text-align:right;margin-top:-2px;padding:0px 5px;"><button class="cancel" style="color: rgb(0, 0, 0);">cancel</button><button style="margin-left: 5px; width: 40px; color: rgb(0, 0, 0);" class="ok">ok</button></div></div></div><div style="clear:both;font-weight:bold;text-align:center;padding:5px 0px 5px 0px">alpha</div><div style="clear:both;display: inline-block;width: 50px;height: 60px;margin-top:5px;padding: 0;"><input class="slider-component slider-alpha" value="NaN" style="background:transparent;width: 50px;height: 50px;margin: 0;transform-origin: 25px 25px;transform: rotate(90deg);" type="range" min="0" max="100" step="1"><span>50</span></div><div style="clear:both;font-weight:bold;text-align:center;padding:5px 0px 5px 0px">size</div><div style="clear:both;display: inline-block;width: 50px;height: 60px;margin-top:5px;padding: 0;"><input class="slider-component slider-size" value="undefined" style="background:transparent;width: 50px;height: 50px;margin: 0;transform-origin: 25px 25px;transform: rotate(90deg);" type="range" min="2" max="100" step="1"><span>6</span></div></div><div class="drawr-toolbox drawr-toolbox-brush" style="position: absolute; z-index: 6; cursor: move; width: 80px; height: auto; color: rgb(255, 255, 255); padding: 2px; background: linear-gradient(rgb(69, 72, 77) 0%, rgb(0, 0, 0) 100%); border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.75) 0px 2px 5px -2px; user-select: none; font-family: sans-serif; font-size: 12px; text-align: center; top: 323.171px; left: 29.9769px; display: block;"><div style="padding:5px 0px 5px 0px">Brushes</div><a class="drawr-tool-btn type-brush" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-lead-pencil mdi-24px"></i></a><a class="drawr-tool-btn type-brush" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-fountain-pen-tip mdi-24px"></i></a><a class="drawr-tool-btn type-brush" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-spray mdi-24px"></i></a><a class="drawr-tool-btn type-brush active" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: orange; color: rgb(255, 255, 255); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-brush mdi-24px"></i></a><a class="drawr-tool-btn type-brush" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-eraser mdi-24px"></i></a><a class="drawr-tool-btn type-brush" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-eyedropper mdi-24px"></i></a><a class="drawr-tool-btn type-brush" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-vector-square mdi-24px"></i></a><a class="drawr-tool-btn type-brush" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-square mdi-24px"></i></a><a class="drawr-tool-btn type-brush" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-cursor-move mdi-24px"></i></a><a class="drawr-tool-btn type-brush" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-marker mdi-24px"></i></a><a class="drawr-tool-btn type-brush" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-format-text mdi-24px"></i></a><a class="drawr-tool-btn type-toggle" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-palette-outline mdi-24px"></i></a><a class="drawr-tool-btn type-toggle" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-magnify mdi-24px"></i></a><a class="drawr-tool-btn type-action" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px; opacity: 1;"><i class="mdi mdi-undo-variant mdi-24px"></i></a><a class="drawr-tool-btn type-action" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-folder-open mdi-24px"></i></a><a class="drawr-tool-btn type-action" style="cursor: pointer; float: left; display: block; margin: 0px; outline: none; text-align: center; padding: 0px; width: 50%; background: rgb(238, 238, 238); color: rgb(0, 0, 0); border: 0px; min-height: 30px; user-select: none; border-radius: 0px;"><i class="mdi mdi-content-save mdi-24px"></i></a></div>

        </span>
        <input type="file" id="file-picker" style="display:none;">
        <div class="col-md-10" style="margin-left: 19px;float: inline-start;background: #545454;" id="image_gallery">
        </div>
        <script type="text/javascript">

            $("#drawr-container .demo-canvas").drawr({ "enable_transparency" : true, enable_transparency_image: true });
            $("#drawr-container .demo-canvas").drawr("start");

            //add custom save button.
            var buttoncollection = $("#drawr-container .demo-canvas").drawr("button", {
                "icon":"mdi mdi-folder-open mdi-24px"
            }).on("touchstart mousedown",function(){
                //alert("demo of a custom button with your own functionality!");
                $("#file-picker").click();
            });
            var buttoncollection = $("#drawr-container .demo-canvas").drawr("button", {
                "icon":"mdi mdi-content-save mdi-24px"
            }).on("touchstart mousedown",function(){
                var imagedata = $("#drawr-container .demo-canvas").drawr("export","image/png");
                var element = document.createElement('a');
                element.setAttribute('href', imagedata);// 'data:text/plain;charset=utf-8,' + encodeURIComponent("sillytext"));
                element.setAttribute('download', "test.png");
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
            });
            $("#file-picker")[0].onchange = function(){
                var file = $("#file-picker")[0].files[0];
                if (!file.type.startsWith('image/')){ return }
                var reader = new FileReader();
                reader.onload = function(e) {
                    $("#drawr-container .demo-canvas").drawr("load",e.target.result);
                    var img_html = "<div class='col-md-1 tile-image pull-left' style='text-align:center;margin-bottom:11px;margin-top:10px;background-color:#545454 !important;'><i class='fa fa-times-circle' aria-hidden='true'></i><img class='image_gallery_content' style='width:50px;height:50px;border:1px solid white;border-radius:4px;' src="+e.target.result+"></img></div>";
                    $('#image_gallery').append(img_html);
                };
                reader.readAsDataURL(file);
            };

            $(document).on("click", ".image_gallery_content", function(){
                var img_html = $(this).attr('src');
                $("#drawr-container .demo-canvas").drawr("load",img_html);
            });

        </script>
    </span></h4>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/master/dynamic_templates/js/test.js?version=" . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>

<script src="{{asset("packages/extensionsvalley/master/form_template/js/jquery.drawr.combined.js")}}"></script>

