
<div class="col-md-12">
    @if($template_data !='')
        @php
            $template_data = json_decode($template_data);
            // exit(gettype($template_control_data));
            $template_data = (array) $template_data;
            // $template_control_data = (array) $template_control_data;
            // echo "<pre>";
            // print_r($template_data);
            // exit;
        @endphp
    @endif
    <table class="table patientCombinedHistoryTable    no-border no-margin">
        <tr>
            <td class="border_dashed_bottom"><b>{{$template_name}}</b></td>
        </tr>
        <tr>
            <td class="border_dashed_bottom"><b>{{$doctor_name}}</b><span style="float:right;">{{$template_created_at}}</span></td>
        </tr>
        @if(!empty($template_data))
            @foreach($template_data as $key=>$value)
                @php
                    $colum_name = '';
                    foreach($template_control_data as $data){
                        if($data->id_name == $key){
                            $colum_name = $data->control_label;
                            if(in_array($data->master_control_id, [5,6])){
                                $value_array = $group_value_data_array_container[$key];
                                $value_array = json_decode($value_array, TRUE);
                                $value = $value_array[$value];
                            }
                        }
                    }
                @endphp
                <tr>
                    <td>{{$colum_name}}</td>
                    @if(gettype($value) !='array')
                    <td>{{$value}}</td>
                    @else
                    <td>@php
                        print_r($value);
                    @endphp</td>
                    @endif
                </tr>
            @endforeach
        @endif
    </table>
</div>
