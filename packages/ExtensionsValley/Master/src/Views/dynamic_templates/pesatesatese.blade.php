<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template_mini.css")}}" rel="stylesheet"><style>
    #dynamic_template_data .form-control {
    /* background: #f8f8f8 !important; */
    background:white !important;
    border: 1px solid #e4e3e3 !important;
    border-radius:4px;
    box-shadow: none;
    font-size: 12px !important;
    font-family:'Poppins', sans-serif !important;
    font-weight: 600;
    margin-bottom: 15px;
    transition: all ease-in-out 0.4s;
    height:25px !important;
    padding-left:8px !important;
    color:#5c5c5c;
}

#dynamic_template_data label{
   font-family:'Poppins', sans-serif !important;
   font-weight:600 !important;
   color:#525252 !important;
   font-size: 12px !important;
}
</style>
<div class="col-md-12 box-body theadscroll" style="min-height:100vh !important;position:relative;">
    <div class="col-md-12" style="">

        <h5 class="dynamic-template-name" style="text-align:center;">Pesa Tesa Tese</h5>

        <!--<button class="btn btn-primary pull-right btn-main" onclick="saveTemplate();"><i class="fa fa-save"></i> Save</button>
        <button class="btn bg-blue pull-right btn-main" onclick="resetTemplate();"><i class="fa fa-repeat"></i> Reset</button>
        <button class="btn bg-blue pull-right btn-main" onclick="printTemplate();"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
        <button class="btn bg-blue pull-right btn-main" onclick="loadTemplateHistory();"><i class="fa fa-history" aria-hidden="true"></i> History</button>-->
    </div>
    <div class="col-md-12" style="min-height:80vh !important;">
        <h4><div class="col-md-12  data_id_control_id box-body patient-header"    attr_control_id="22" attr_dataset_id="0">


            <div class="col-md-3">
                <label>Uhid</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id10">
            </div>
            <div class="col-md-3">
                <label>Patient Name</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id11">
            </div>
            <div class="col-md-3">
                <label>Age/Gender</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id12">
            </div>
            <div class="col-md-3">
                <label>Phone</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id13">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-3">
                <label>Address</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id14">
            </div>
            <div class="col-md-3">
                <label>Area</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id15">
            </div>
            <div class="col-md-3">
                <label>Visit Date</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id16">
            </div>
            <div class="col-md-3">
                <label>Consulting Doctor</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id17">
            </div>

        </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0">

        <fieldset style="min-height:100px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true" style="color:blue;">HORMONE LEVELS ON</span></legend>
            <div style="min-height:100px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12 "   >  <table class="table table-bordered static_table_component"><tbody><tr class="static_table_component_tr_1" style="background:cornflowerblue;"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);" style="color:white;"  ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="opacity: 1;">

    <label class="label-class" contenteditable="true" style="color:white !important;font-weight:600 !important;" >FSH</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"  style="color:white;" ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true" style="color:white !important;font-weight:600 !important;">LH</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   style="color:white;"><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="opacity: 1;">

    <label class="label-class" contenteditable="true" style="color:white !important;font-weight:600 !important;">Testosterone</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"  style="color:white;" ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true" style="color:white !important;font-weight:600 !important;">Prolactin</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);" style="color:white;"  ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true" style="color:white !important;font-weight:600 !important;">TSH</label>
</div>
<div class="clearfix"></div>

</td></tr><tr class="static_table_component_tr_2"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="223" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00183"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00183" id="fc00183" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="219" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00179"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00179" id="fc00179" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="220" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00180"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00180" id="fc00180" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="221" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00181"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00181" id="fc00181" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="222" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00182"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00182" id="fc00182" placeholder="" value="">
                    </span>
    </div>


</td></tr></tbody></table> </div></div>
        </fieldset>
    <div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0">

        <fieldset style="min-height:100px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true" style="color:blue;">PROCEDURE DETAILS</span></legend>
            <div style="min-height:100px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12 "    style="opacity: 1; display: block;">  <table class="table table-bordered static_table_component"><tbody><tr class="static_table_component_tr_1" style="background: cornflowerblue;"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="text-align: center; display: block;">

    <label class="label-class" contenteditable="true" style="color:white !important;font-weight:600 !important;">TESA / PESA / TESE</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="text-align: center; display: block;">

    <label class="label-class" contenteditable="true" style="color:white !important;font-weight:600 !important;">FINDINGS&nbsp; &nbsp; &nbsp; &nbsp;</label>
</div>
<div class="clearfix"></div>

</td></tr><tr class="static_table_component_tr_2"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="228" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00188"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00188" id="fc00188" style="height:px !important;" placeholder=""></textarea>
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="229" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00189"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00189" id="fc00189" style="height:px !important;" placeholder=""></textarea>
                    </span>
    </div>


</td></tr></tbody></table> </div></div>
        </fieldset>
    </div>


</div></h4>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/master/dynamic_templates/js/pesatesatese.js?version=" . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
