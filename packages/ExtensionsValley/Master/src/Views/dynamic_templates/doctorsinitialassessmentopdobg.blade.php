<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template_mini.css")}}" rel="stylesheet">
<style>
    #dynamic_template_data .form-control {
    /* background: #f8f8f8 !important; */
    background:white !important;
    border: 1px solid #e4e3e3 !important;
    border-radius:4px;
    box-shadow: none;
    font-size: 12px !important;
    font-family:'Poppins', sans-serif !important;
    font-weight: 600;
    margin-bottom: 15px;
    transition: all ease-in-out 0.4s;
    height:25px !important;
    padding-left:8px !important;
    color:#5c5c5c;
}

#dynamic_template_data label{
   font-family:'Poppins', sans-serif !important;
   font-weight:600 !important;
   color:#525252 !important;
   font-size: 12px !important;
}
</style>
<div class="col-md-12 box-body theadscroll" style="min-height:100vh !important;position:relative;">
    <div class="col-md-12" style="height:20px !important;">

        <h5 class="dynamic-template-name" style="text-align:center;">Doctors Initial Assessment Opd Obg</h5>
    </div>
    <div class="col-md-12" style="min-height:80vh !important;">
        <h4><div class="col-md-6  data_id_control_id"    attr_control_id="33" attr_dataset_id="4">

        <label>patient name</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_patient_name"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="patient_name" id="patient_name" placeholder="Select" value="" wfd-id="id10">
                    </span>
    </div>


<div class="col-md-1  data_id_control_id"    attr_control_id="34" attr_dataset_id="14">

        <label>Age</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_age"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="age" id="age" placeholder="" value="" wfd-id="id11">
                    </span>
    </div>


<div class="col-md-5  data_id_control_id"    attr_control_id="62" attr_dataset_id="14">

        <label>OP No</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00023"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00023" id="fc00023" placeholder="" value="" wfd-id="id12">
                    </span>
    </div>





<div class="col-md-6  data_id_control_id"    attr_control_id="112" attr_dataset_id="14">

        <label>Doctor name</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00072"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00072" id="fc00072" placeholder="" value="" wfd-id="id13">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="113" attr_dataset_id="0">

        <label>Father/Husband Name</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00073"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00073" id="fc00073" placeholder="" value="" wfd-id="id14">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="114" attr_dataset_id="0">

        <label>occupation</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00074"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00074" id="fc00074" placeholder="" value="" wfd-id="id15">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="115" attr_dataset_id="4">

        <label>Contact No</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00075"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00075" id="fc00075" placeholder="" value="" wfd-id="id16">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="44" attr_dataset_id="9">

        <label>Allergic History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00010"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00010" id="fc00010" placeholder="" value="" wfd-id="id17">
                    </span>




</div><div class="col-md-6  data_id_control_id"    attr_control_id="116" attr_dataset_id="0" style="display: block;">

        <label>BG&amp;Rh Typing</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00076"></span>
        <span class="hide_print">
            <select class="form-control select2 select-form-input" name="fc00076" id="fc00076" placeholder="">
                                                                            <option value="2">
                            A-</option>
                                            <option value="1">
                            A+</option>
                                            <option value="8">
                            AB-</option>
                                            <option value="7">
                            AB+</option>
                                            <option value="4">
                            B-</option>
                                            <option value="3">
                            B+</option>
                                            <option value="6">
                            O-</option>
                                            <option value="5">
                            O+</option>
                                                </select>
        </span>
    </div>












<div class="col-md-8  data_id_control_id"    attr_control_id="19" attr_dataset_id="0">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true"></span></legend>
            <div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12  data_id_control_id"    attr_control_id="58" attr_dataset_id="0" style="opacity: 1;">

        <label>Presenting Complaints &amp; History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00019"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00019" id="fc00019" style="height:150px !important;" placeholder=""></textarea>
                    </span>
    </div>














<div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true">General Examinations</label>
</div>
<div class="clearfix">


</div>




<div class="col-md-3  data_id_control_id"    attr_control_id="84" attr_dataset_id="0" style="display: block;">

        <label>Height</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00045"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00045" id="fc00045" placeholder="" value="" wfd-id="id18">
                    </span>
    </div><div class="col-md-3  data_id_control_id"    attr_control_id="85" attr_dataset_id="0" style="opacity: 1;">

        <label>Weight</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00046"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00046" id="fc00046" placeholder="" value="" wfd-id="id19">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="86" attr_dataset_id="0">

        <label>BMI</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00047"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00047" id="fc00047" placeholder="" value="" wfd-id="id20">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="122" attr_dataset_id="0">

        <label>Waist Circumference</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00082"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00082" id="fc00082" placeholder="" value="" wfd-id="id21">
                    </span>
    </div>


</div>









<div class="col-md-3  data_id_control_id"    attr_control_id="123" attr_dataset_id="0" style="opacity: 1;">

        <label>PR</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00083"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00083" id="fc00083" placeholder="" value="" wfd-id="id22">
                    </span>
    </div>

















<div class="col-md-3  data_id_control_id"    attr_control_id="124" attr_dataset_id="0">

        <label>BP</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00084"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00084" id="fc00084" placeholder="" value="" wfd-id="id23">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="125" attr_dataset_id="0">

        <label>Pallor</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00085"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00085" id="fc00085" placeholder="" value="" wfd-id="id24">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="126" attr_dataset_id="0">

        <label>Edema</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00086"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00086" id="fc00086" placeholder="" value="" wfd-id="id25">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="127" attr_dataset_id="0">

        <label>CVS</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00087"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00087" id="fc00087" placeholder="" value="" wfd-id="id26">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="128" attr_dataset_id="0">

        <label>RS</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00088"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00088" id="fc00088" placeholder="" value="" wfd-id="id27">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="74" attr_dataset_id="0">

        <label>Local Examination</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00035"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00035" id="fc00035" style="height:150px !important;" placeholder=""></textarea>
                    </span>
    </div>


</fieldset>
    </div>


<div class="col-md-4  data_id_control_id"    attr_control_id="17" attr_dataset_id="0">

        <fieldset style="min-height: 300PX;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true"></span></legend>
            <div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12  data_id_control_id"    attr_control_id="45" attr_dataset_id="9">

        <label>obstretic history</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00011"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00011" id="fc00011" style="height:px !important;" placeholder=""></textarea>
                    </span>
    </div>





<div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="display: block;">

    <label class="label-class" contenteditable="true">Menstrual History</label>
</div>
<div class="clearfix"></div>

<div class="col-md-12  data_id_control_id"    attr_control_id="118" attr_dataset_id="0">

        <label>LMP</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00078"></span>
        <span class="hide_print">
            <input type="text" class="form-control datepicker datepicker-form-input" name="fc00078" id="fc00078" placeholder="" value=" " wfd-id="id28" style="">
        </span>
    </div>
    <div class="col-md-12  data_id_control_id"    attr_control_id="119" attr_dataset_id="0">

        <label>PMP</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00079"></span>
        <span class="hide_print">
            <input type="text" class="form-control datepicker datepicker-form-input" name="fc00079" id="fc00079" placeholder="" value=" " style="" wfd-id="id29">
        </span>
    </div>
    <div class="col-md-12  data_id_control_id"    attr_control_id="120" attr_dataset_id="9">

        <label>Personal History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00080"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00080" id="fc00080" style="height:px !important;" placeholder=""></textarea>
                    </span>
    </div>
    <div class="col-md-12  data_id_control_id"    attr_control_id="121" attr_dataset_id="0">

        <label>Nutritional Screening</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00081"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00081" id="fc00081" style="height:px !important;" placeholder=""></textarea>
                    </span>
    </div>


</div>
        </fieldset>
    </div>
<div class="clearfix">









</div></h4>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/master/dynamic_templates/js/doctorsinitialassessmentopdobg.js?version=" . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
