<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template_mini.css")}}" rel="stylesheet">
<div class="col-md-12 box-body theadscroll" style="min-height:100vh !important;position:relative;">
    <div class="col-md-12" style="height:10vh !important;">

        <h4 class="dynamic-template-name">Data Point Test</h4>

        <!--<button class="btn btn-primary pull-right btn-main" onclick="saveTemplate();"><i class="fa fa-save"></i> Save</button>
        <button class="btn bg-blue pull-right btn-main" onclick="resetTemplate();"><i class="fa fa-repeat"></i> Reset</button>
        <button class="btn bg-blue pull-right btn-main" onclick="printTemplate();"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
        <button class="btn bg-blue pull-right btn-main" onclick="loadTemplateHistory();"><i class="fa fa-history" aria-hidden="true"></i> History</button>-->
    </div>
    <div class="col-md-12" style="min-height:80vh !important;">
        <h4><div class="col-md-12  data_id_control_id"    attr_control_id="59" attr_dataset_id="0" attr_data_point="Dia">
        
        <label>Diagnosis</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00020"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00020" id="fc00020" style="height:150px !important;" placeholder=""></textarea>
                    </span>
    </div>

    
<div class="col-md-12  data_id_control_id"    attr_control_id="150" attr_dataset_id="0" attr_data_point="PH">
        
        <label>Patient Past History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00109"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00109" id="fc00109" style="height:px !important;" placeholder=""></textarea>
                    </span>
    </div>

    
<div class="col-md-12  data_id_control_id"    attr_control_id="58" attr_dataset_id="0" attr_data_point="PC">
        
        <label>Presenting Complaints &amp; History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00019"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00019" id="fc00019" style="height:150px !important;" placeholder=""></textarea>
                    </span>
    </div></h4>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/master/dynamic_templates/js/datapointtest.js?version=" . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
