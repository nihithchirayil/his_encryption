<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template_mini.css")}}" rel="stylesheet">
<style>
    #dynamic_template_data .form-control {
    /* background: #f8f8f8 !important; */
    background:white !important;
    border: 1px solid #e4e3e3 !important;
    border-radius:4px;
    box-shadow: none;
    font-size: 12px !important;
    font-family:'Poppins', sans-serif !important;
    font-weight: 600;
    margin-bottom: 15px;
    transition: all ease-in-out 0.4s;
    height:25px !important;
    padding-left:8px !important;
    color:#5c5c5c;
}

#dynamic_template_data label{
   font-family:'Poppins', sans-serif !important;
   font-weight:600 !important;
   color:#090101 !important;
   font-size: 12px !important;
}
</style>
<div class="col-md-12 theadscroll" style="height:77vh !important;position:relative;">
    <div class="col-md-12" style="height:2vh !important;">

        <h5 class="dynamic-template-name" style="text-align:center;">Ktu</h5>

        <!--<button class="btn btn-primary pull-right btn-main" onclick="saveTemplate();"><i class="fa fa-save"></i> Save</button>
        <button class="btn bg-blue pull-right btn-main" onclick="resetTemplate();"><i class="fa fa-repeat"></i> Reset</button>
        <button class="btn bg-blue pull-right btn-main" onclick="printTemplate();"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
        <button class="btn bg-blue pull-right btn-main" onclick="loadTemplateHistory();"><i class="fa fa-history" aria-hidden="true"></i> History</button>-->
    </div>
    <div class="col-md-12" style="min-height:80vh !important;">
        <h4><div class="col-md-12  data_id_control_id"    attr_control_id="97" attr_dataset_id="0" attr_data_point="">

        <label>Advice On Discharge</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00058"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00058" id="fc00058" style="height:120px !important;" placeholder=""></textarea>
                    </span>
    </div>










<div class="btn-group col-md-12  data_id_control_id" data-toggle="buttons"    attr_control_id="181" attr_dataset_id="0">

        <label>Urinary Output</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_urinary_output_status"></span>
        <span class="hide_print">
                                                                                            <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="1" name="urinary_output_status" id="urinary_output_status">Yes
                </label>
                                                                                <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="2" name="urinary_output_status" id="urinary_output_status">No
                </label>
                                                                                <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="0" name="urinary_output_status" id="urinary_output_status">NA
                </label>
                    </span>

    </div><div class="btn-group col-md-12  data_id_control_id" data-toggle="buttons"    attr_control_id="248" attr_dataset_id="0">

        <label>Vulnerability</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00181"></span>
        <span class="hide_print">
            <ul class="select_button no-padding check-form-input">

                    <li data-value="Handicapped" class="">
                        <i class="fa fa-check-circle"></i> Handicapped
                        <input type="hidden" name="fc00181" id="fc00181_Handicapped" value="Handicapped">
                    </li>

                    <li data-value="Language Barrier" class="">
                        <i class="fa fa-check-circle"></i> Language Barrier
                        <input type="hidden" name="fc00181" id="fc00181_LanguageBarrier" value="Language Barrier">
                    </li>

                    <li data-value="Immunocompromised" class="">
                        <i class="fa fa-check-circle"></i> Immunocompromised
                        <input type="hidden" name="fc00181" id="fc00181_Immunocompromised" value="Immunocompromised">
                    </li>

                    <li data-value="Age" class="">
                        <i class="fa fa-check-circle"></i> Age
                        <input type="hidden" name="fc00181" id="fc00181_Age" value="Age">
                    </li>

                    <li data-value="Pregnancy" class="">
                        <i class="fa fa-check-circle"></i> Pregnancy
                        <input type="hidden" name="fc00181" id="fc00181_Pregnancy" value="Pregnancy">
                    </li>

                    <li data-value="Anaemia" class="">
                        <i class="fa fa-check-circle"></i> Anaemia
                        <input type="hidden" name="fc00181" id="fc00181_Anaemia" value="Anaemia">
                    </li>
                            </ul>
        </span>
    </div></h4>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/master/dynamic_templates/js/ktu.js?version=" . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
