<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template_mini.css")}}" rel="stylesheet">
<style>
    #dynamic_template_data .form-control {
    /* background: #f8f8f8 !important; */
    background:white !important;
    border: 1px solid #e4e3e3 !important;
    border-radius:4px;
    box-shadow: none;
    font-size: 12px !important;
    font-family:'Poppins', sans-serif !important;
    font-weight: 600;
    margin-bottom: 15px;
    transition: all ease-in-out 0.4s;
    height:25px !important;
    padding-left:8px !important;
    color:#5c5c5c;
}

#dynamic_template_data label{
   font-family:'Poppins', sans-serif !important;
   font-weight:600 !important;
   color:#525252 !important;
   font-size: 12px !important;
}
</style>
<div class="col-md-12 box-body theadscroll" style="min-height:100vh !important;position:relative;">
    <div class="col-md-12" style="">

        <h5 class="dynamic-template-name" style="text-align:center;" >Case Record</h5>

        <!--<button class="btn btn-primary pull-right btn-main" onclick="saveTemplate();"><i class="fa fa-save"></i> Save</button>
        <button class="btn bg-blue pull-right btn-main" onclick="resetTemplate();"><i class="fa fa-repeat"></i> Reset</button>
        <button class="btn bg-blue pull-right btn-main" onclick="printTemplate();"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
        <button class="btn bg-blue pull-right btn-main" onclick="loadTemplateHistory();"><i class="fa fa-history" aria-hidden="true"></i> History</button>-->
    </div>
    <div class="col-md-12" style="min-height:80vh !important;">
        <h4><div class="col-md-12  data_id_control_id box-body patient-header"    attr_control_id="22" attr_dataset_id="0" style="opacity: 1;">


            <div class="col-md-3">
                <label>Uhid</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id10">
            </div>
            <div class="col-md-3">
                <label>Patient Name</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id11">
            </div>
            <div class="col-md-3">
                <label>Age/Gender</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id12">
            </div>
            <div class="col-md-3">
                <label>Phone</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id13">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-3">
                <label>Address</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id14">
            </div>
            <div class="col-md-3">
                <label>Area</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id15">
            </div>
            <div class="col-md-3">
                <label>Visit Date</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id16">
            </div>
            <div class="col-md-3">
                <label>Consulting Doctor</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id17">
            </div>

        </div>


<div class="col-md-12 "   >  <table class="table table-bordered static_table_component"><tbody><tr class="static_table_component_tr_1" style="background: cornflowerblue;"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ></td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="display: block; opacity: 1;">

    <label class="label-class" contenteditable="true" style="color:white !important;font-weight:600 !important;">Female</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true" style="color:white !important;font-weight:600 !important;">Male</label>
</div>
<div class="clearfix"></div>

</td></tr><tr class="static_table_component_tr_2"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true">Name</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="193" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00153"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00153" id="fc00153" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="194" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00154"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00154" id="fc00154" placeholder="" value="">
                    </span>
    </div>


</td></tr><tr class="static_table_component_tr_3"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true">Age</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="195" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00155"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00155" id="fc00155" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="196" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00156"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00156" id="fc00156" placeholder="" value="">
                    </span>
    </div>


</td></tr><tr class="static_table_component_tr_4"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true">Occupation</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   >


<div class="col-md-12  data_id_control_id"    attr_control_id="197" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00157"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00157" id="fc00157" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="198" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00158"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00158" id="fc00158" placeholder="" value="">
                    </span>
    </div>


</td></tr><tr class="static_table_component_tr_5"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="opacity: 1;">

    <label class="label-class" contenteditable="true">Address</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="199" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00159"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00159" id="fc00159" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="200" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00160"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00160" id="fc00160" placeholder="" value="">
                    </span>
    </div>


</td></tr><tr class="static_table_component_tr_6"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true">Tel:</label>

    <div class="clearfix"></div>


</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="201" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00161"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00161" id="fc00161" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="202" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00162"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00162" id="fc00162" placeholder="" value="">
                    </span>
    </div>


</td></tr></tbody></table>
    <div class="clearfix"></div>



    <div class="clearfix">









    <div class="clearfix">








<div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0" style="display: block;">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true" style='color:blue;'>History</span></legend>
            <div style="/* min-height:300px; */" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12  data_id_control_id"    attr_control_id="129" attr_dataset_id="0" style="opacity: 1;">

        <label>Married For</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00089"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00089" id="fc00089" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="130" attr_dataset_id="0">

        <label>Tried for</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00090"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00090" id="fc00090" placeholder="" value="">
                    </span>
    </div>


</div>
        </fieldset>
    </div><div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0" style="opacity: 1;">

        <fieldset style="/* min-height: 103px; */" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true" style='color:blue;'>Menstrual History</span></legend>
            <div class="col-md-6  data_id_control_id"    attr_control_id="131" attr_dataset_id="0" style="display: block;">

        <label>P.M.C</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00091"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00091" id="fc00091" placeholder="" value="" wfd-id="id18">
                    </span>



</div><div class="col-md-6  data_id_control_id"    attr_control_id="132" attr_dataset_id="0" style="display: block;">

        <label>L.M.C</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00092"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00092" id="fc00092" placeholder="" value="" wfd-id="id19">
                    </span>
    </div><div style="min-height: 60px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            </div>
        </fieldset>
    </div>


</div>


</div>





</div><div style="min-height: 300px; display: block;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            </div>
<div class="clearfix">



    <div class="clearfix"><div class="col-md-12  data_id_control_id"    attr_control_id="45" attr_dataset_id="9" style="opacity: 1;">

        <label style='color:blue;'>Obstretic history</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00011"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00011" id="fc00011" style="height:px !important;" placeholder=""></textarea>
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0" style="display: block;">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true" style='color:blue;'>Past History</span></legend>
            <div class="col-md-6  data_id_control_id"    attr_control_id="133" attr_dataset_id="0" style="display: block;">

        <label>Medical History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00093"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00093" id="fc00093" style="height:140px !important;" placeholder=""></textarea>
                    </span>
    </div><div class="col-md-6  data_id_control_id"    attr_control_id="68" attr_dataset_id="0" style="display: block;">

        <label>Surgical History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00029"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00029" id="fc00029" style="height: 140px !important;" placeholder=""></textarea>
                    </span>
    </div><div class="col-md-6  data_id_control_id"    attr_control_id="69" attr_dataset_id="0" style="opacity: 1; display: block;">

        <label>Family History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00030"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00030" id="fc00030" style="height: 140px !important;" placeholder=""></textarea>
                    </span>
    </div><div class="col-md-6  data_id_control_id"    attr_control_id="120" attr_dataset_id="9" style="display: block;">

        <label>Personal History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00080"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00080" id="fc00080" style="height:140px !important;" placeholder=""></textarea>
                    </span>

    <div class="clearfix"></div>


</div><div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            </div>
        </fieldset>
    </div>
    <div class="clearfix">















    <div class="clearfix"><div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0" style="display: block; opacity: 1;">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true" style='color:blue;'>Husband's History</span></legend>
            <div class="col-md-12  data_id_control_id"    attr_control_id="141" attr_dataset_id="0" style="display: block;">

        <label>Coital History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00101"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00101" id="fc00101" style="height:75px !important;" placeholder=""></textarea>
                    </span>
    </div><div class="col-md-12  data_id_control_id"    attr_control_id="135" attr_dataset_id="0" style="display: block;">

        <label>Surgical History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00095"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00095" id="fc00095" style="height:75px !important;" placeholder=""></textarea>
                    </span>



<div class="col-md-0  data_id_control_id"    attr_control_id="139" attr_dataset_id="0">

        <label>Medical Disease</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00099"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00099" id="fc00099" style="height:75px !important;" placeholder=""></textarea>
                    </span>
    </div>





</div><div class="col-md-12  data_id_control_id"    attr_control_id="138" attr_dataset_id="0" style="opacity: 1; display: block;">

        <label>Family History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00098"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00098" id="fc00098" style="height:75px !important;" placeholder=""></textarea>
                    </span>
    </div><div class="col-md-12  data_id_control_id"    attr_control_id="140" attr_dataset_id="0" style="display: block;">

        <label>Habits</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00100"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00100" id="fc00100" style="height:75px !important;" placeholder=""></textarea>
                    </span>
    </div><div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            </div>
        </fieldset>
    </div>














<div class="col-md-6  data_id_control_id"    attr_control_id="16" attr_dataset_id="0" style="display: block;">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true" style='color:blue;'>Treatment Taken Wife</span></legend>
            <div style="min-height: 165px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12  data_id_control_id"    attr_control_id="143" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00103"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00103" id="fc00103" style="height:140px !important;" placeholder=""></textarea>
                    </span>
    </div>


</div>
        </fieldset>
    </div><div class="col-md-6  data_id_control_id"    attr_control_id="16" attr_dataset_id="0" style="display: block;">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true" style='color:blue;'>Treatment Taken Husband</span></legend>
            <div style="min-height: 200px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12  data_id_control_id"    attr_control_id="142" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00102"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00102" id="fc00102" style="height:140px !important;" placeholder=""></textarea>
                    </span>
    </div>


</div>
        </fieldset>
    </div>



    <div class="clearfix"><div class="col-md-6  data_id_control_id"    attr_control_id="16" attr_dataset_id="0" style="display: block;">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true" style='color:blue;'>Data collection done by</span></legend>
            <div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12  data_id_control_id"    attr_control_id="145" attr_dataset_id="0">

        <label>Name</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00105"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00105" id="fc00105" placeholder="" value="" wfd-id="id20">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="146" attr_dataset_id="0">

        <label>EMP ID</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00106"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00106" id="fc00106" placeholder="" value="" wfd-id="id21">
                    </span>
    </div>






<div class="col-md-12  data_id_control_id"    attr_control_id="147" attr_dataset_id="0">

        <label>Date &amp; Time</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00107"></span>
        <span class="hide_print">
            <input type="text" class="form-control datetimepicker datetimepicker-form-input" name="fc00107" id="fc00107" placeholder="" value=" " wfd-id="id22">
        </span>
    </div>



</div>
        </fieldset>



</div><div class="col-md-6  data_id_control_id"    attr_control_id="16" attr_dataset_id="0" style="display: block; opacity: 1;">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true" style='color:blue;'>Data verifed by</span></legend>
            <div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >








<div class="col-md-12  data_id_control_id"    attr_control_id="149" attr_dataset_id="0">

        <label>Signature</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00109"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00109" id="fc00109" placeholder="" value="" wfd-id="id23">
                    </span>
    </div><div class="col-md-12  data_id_control_id"    attr_control_id="148" attr_dataset_id="0" style="display: block;">

        <label>Name</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00108"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00108" id="fc00108" placeholder="" value="" wfd-id="id24">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="150" attr_dataset_id="0" style="opacity: 1;">

        <label>Date &amp; Time</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00110"></span>
        <span class="hide_print">
            <input type="text" class="form-control datetimepicker datetimepicker-form-input" name="fc00110" id="fc00110" placeholder="" value=" " wfd-id="id25">
        </span>
    </div>



</div>
        </fieldset>
    </div><div style="min-height: 300px; display: block;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            </div><div style="min-height: 300px; display: block;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="20" attr_dataset_id="0" style="opacity: 1;">

        <fieldset style="/* min-height: 141px; */" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Examination</span></legend>
            <div style="min-height: 145px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-3  data_id_control_id"    attr_control_id="85" attr_dataset_id="0">

        <label>Weight</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00046"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00046" id="fc00046" placeholder="" value="" wfd-id="id26">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="151" attr_dataset_id="0">

        <label>Thyroid</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00111"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00111" id="fc00111" placeholder="" value="" wfd-id="id27">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="152" attr_dataset_id="0">

        <label>Hirsutism</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00112"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00112" id="fc00112" placeholder="" value="" wfd-id="id28">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="124" attr_dataset_id="0">

        <label>BP</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00084"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00084" id="fc00084" placeholder="" value="" wfd-id="id29">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="153" attr_dataset_id="0">

        <label>Galactorrhoea</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00113"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00113" id="fc00113" placeholder="" value="" wfd-id="id30">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="86" attr_dataset_id="0" style="opacity: 1;">

        <label>BMI</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00047"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00047" id="fc00047" placeholder="" value="" wfd-id="id31">
                    </span>
    </div>


</div>
        </fieldset>
    </div><div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0" style="display: block;">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Systemic Examination</span></legend>
            <div style="min-height: 200px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >


<div class="clearfix"></div>

<div class="col-md-3  data_id_control_id"    attr_control_id="162" attr_dataset_id="0">

        <label>respiratory</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00122"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00122" id="fc00122" placeholder="" value="" wfd-id="id32">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="158" attr_dataset_id="0">

        <label>PA</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00118"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00118" id="fc00118" placeholder="" value="" wfd-id="id33">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="159" attr_dataset_id="0">

        <label>PS</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00119"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00119" id="fc00119" placeholder="" value="" wfd-id="id34">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="160" attr_dataset_id="0" style="opacity: 1;">

        <label>PV</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00120"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00120" id="fc00120" placeholder="" value="" wfd-id="id35">
                    </span>
    </div>





<div class="col-md-3  data_id_control_id"    attr_control_id="161" attr_dataset_id="0">

        <label>CVS</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00121"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00121" id="fc00121" placeholder="" value="" wfd-id="id36">
                    </span>
    </div>


</div>



</fieldset>
    <div style="min-height: 300px; display: block;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Transvaginal Sonography</span></legend>
            <div style="min-height: 153px;display: block;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-6  data_id_control_id"    attr_control_id="163" attr_dataset_id="0">

        <label>Uterus</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00123"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00123" id="fc00123" placeholder="" value="" wfd-id="id37">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="164" attr_dataset_id="0">

        <label>Right Ovary</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00124"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00124" id="fc00124" placeholder="" value="" wfd-id="id38">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="165" attr_dataset_id="0">

        <label>Left Ovary</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00125"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00125" id="fc00125" placeholder="" value="" wfd-id="id39">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="166" attr_dataset_id="0">

        <label>POD</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00126"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00126" id="fc00126" placeholder="" value="" wfd-id="id40">
                    </span>
    </div>


</div>
        </fieldset>
    </div>


</div><div style="min-height: 300px; display: block;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >










<div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Investigation Wife</span></legend>
            <div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-6  data_id_control_id"    attr_control_id="167" attr_dataset_id="0">

        <label>Haemogram</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00127"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00127" id="fc00127" placeholder="" value="" wfd-id="id41">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="168" attr_dataset_id="0">

        <label>Sr.LH</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00128"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00128" id="fc00128" placeholder="" value="" wfd-id="id42">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="169" attr_dataset_id="0">

        <label>Urine</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00129"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00129" id="fc00129" placeholder="" value="" wfd-id="id43">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="170" attr_dataset_id="0">

        <label>Sr.Estrodiol</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00130"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00130" id="fc00130" placeholder="" value="" wfd-id="id44">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="171" attr_dataset_id="0">

        <label>HIV</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00131"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00131" id="fc00131" placeholder="" value="" wfd-id="id45">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="172" attr_dataset_id="0">

        <label>Sr.Progestone</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00132"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00132" id="fc00132" placeholder="" value="" wfd-id="id46">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="173" attr_dataset_id="0">

        <label>HbsAg</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00133"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00133" id="fc00133" placeholder="" value="" wfd-id="id47">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="174" attr_dataset_id="0">

        <label>VDRL</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00134"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00134" id="fc00134" placeholder="" value="" wfd-id="id48">
                    </span>
    </div>


</div>



<div class="col-md-6  data_id_control_id"    attr_control_id="175" attr_dataset_id="0" style="opacity: 1;">

        <label>HCV</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00135"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00135" id="fc00135" placeholder="" value="" wfd-id="id49">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="176" attr_dataset_id="0">

        <label>RBS</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00136"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00136" id="fc00136" placeholder="" value="" wfd-id="id50">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="177" attr_dataset_id="0">

        <label>AMH</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00137"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00137" id="fc00137" placeholder="" value="" wfd-id="id51">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="178" attr_dataset_id="0">

        <label>TSH</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00138"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00138" id="fc00138" placeholder="" value="" wfd-id="id52">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="179" attr_dataset_id="0">

        <label>Sr.FSH</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00139"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00139" id="fc00139" placeholder="" value="" wfd-id="id53">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="180" attr_dataset_id="0">

        <label>Sr.Prolactin</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00140"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00140" id="fc00140" placeholder="" value="" wfd-id="id54">
                    </span>
    </div>


</fieldset>
    </div>


</div>





<div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Investigation Husband</span></legend>
            <div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12  data_id_control_id"    attr_control_id="181" attr_dataset_id="0">

        <label>HSA in our hospital</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00141"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00141" id="fc00141" placeholder="" value="" wfd-id="id55">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="182" attr_dataset_id="0">

        <label>Sr.Fsh</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00142"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00142" id="fc00142" placeholder="" value="" wfd-id="id56">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="183" attr_dataset_id="0">

        <label>Sr.Testosterone</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00143"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00143" id="fc00143" placeholder="" value="" wfd-id="id57">
                    </span>
    </div>


</div>
        </fieldset>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="184" attr_dataset_id="0">

        <label>Provisional Diaganosis</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00144"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00144" id="fc00144" style="height:75px !important;" placeholder=""></textarea>
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="185" attr_dataset_id="0">

        <label>Treatment Plan</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00145"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00145" id="fc00145" style="height:px !important;" placeholder=""></textarea>
                    </span>
    </div>


</div>

















<div style="min-height: 300px; display: block;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            </div>














</div>


</div>


</div>


</div>


</div></h4>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/master/dynamic_templates/js/caserecord.js?version=" . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
