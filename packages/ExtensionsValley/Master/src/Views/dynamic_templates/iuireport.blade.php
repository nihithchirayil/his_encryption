<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template_mini.css")}}" rel="stylesheet">
<div class="col-md-12 box-body theadscroll" style="min-height:100vh !important;position:relative;">
    <div class="col-md-12" style="height:10vh !important;">

        <h4 class="dynamic-template-name">Iui Report</h4>

        <!--<button class="btn btn-primary pull-right btn-main" onclick="saveTemplate();"><i class="fa fa-save"></i> Save</button>
        <button class="btn bg-blue pull-right btn-main" onclick="resetTemplate();"><i class="fa fa-repeat"></i> Reset</button>
        <button class="btn bg-blue pull-right btn-main" onclick="printTemplate();"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
        <button class="btn bg-blue pull-right btn-main" onclick="loadTemplateHistory();"><i class="fa fa-history" aria-hidden="true"></i> History</button>-->
    </div>
    <div class="col-md-12" style="min-height:80vh !important;">
        <h4><div class="col-md-12  data_id_control_id box-body patient-header"    attr_control_id="22" attr_dataset_id="0">


            <div class="col-md-3">
                <label>Uhid</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id10">
            </div>
            <div class="col-md-3">
                <label>Patient Name</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id11">
            </div>
            <div class="col-md-3">
                <label>Age/Gender</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id12">
            </div>
            <div class="col-md-3">
                <label>Phone</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id13">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-3">
                <label>Address</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id14">
            </div>
            <div class="col-md-3">
                <label>Area</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id15">
            </div>
            <div class="col-md-3">
                <label>Visit Date</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id16">
            </div>
            <div class="col-md-3">
                <label>Consulting Doctor</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" id="id17">
            </div>

        </div><div style="min-height: 300px; display: block;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            </div><div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0" style="display: block;">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">COLLECTION DETAILS</span></legend>

        <div class="col-md-12  data_id_control_id"    attr_control_id="156" attr_dataset_id="0">

        <label>Collection Time</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00116"></span>
        <span class="hide_print">
            <input type="text" class="form-control datetimepicker datetimepicker-form-input" name="fc00116" id="fc00116" placeholder="" value=" ">
        </span>
    </div>



<div class="col-md-12  data_id_control_id"    attr_control_id="186" attr_dataset_id="0">

        <label>Spillage</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00146"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00146" id="fc00146" placeholder="" value="">
                    </span>
    </div>


<div class="btn-group col-md-12  data_id_control_id" data-toggle="buttons"    attr_control_id="187" attr_dataset_id="0">

        <label>Place Of Collection</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00147"></span>
        <span class="hide_print">
                                                                                            <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="1" name="fc00147" id="fc00147">Hospital
                </label>
                                                                                <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="2" name="fc00147" id="fc00147">Home
                </label>
                    </span>

    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="188" attr_dataset_id="0">

        <label>Abstinence</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00148"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00148" id="fc00148" placeholder="" value="">
                    </span>









</div>


<div class="col-md-6  data_id_control_id"    attr_control_id="189" attr_dataset_id="0">

        <label>Days</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00149"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00149" id="fc00149" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="192" attr_dataset_id="0" style="display: block;">

        <label>Liquefaction</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00152"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00152" id="fc00152" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="190" attr_dataset_id="0">

        <label>Appearance</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00150"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00150" id="fc00150" placeholder="" value="">
                    </span>
    </div>


</fieldset>
    <div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">MICROSCOPY</span></legend>
            <div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12 "    style="display: block;">  <table class="table table-bordered static_table_component"><tbody><tr class="static_table_component_tr_1"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ></td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="opacity: 1;text-align: center;">

    <label class="label-class" contenteditable="true">Pre Wash</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="
    text-align: center;
">

    <label class="label-class" contenteditable="true">Post Wash</label>
</div>
<div class="clearfix"></div>

</td></tr><tr class="static_table_component_tr_2"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="opacity: 1;">

    <label class="label-class" contenteditable="true">Count</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="opacity: 1;text-align: center;">

    <label class="label-class" contenteditable="true" style="
    text-align: center;
">Million/ml</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="
    text-align: center;
">

    <label class="label-class" contenteditable="true">Million/ml</label>
</div>
<div class="clearfix"></div>

</td></tr><tr class="static_table_component_tr_3"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true">Total motility</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="203" attr_dataset_id="0" style="opacity: 1;">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00163"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00163" id="fc00163" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="208" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00168"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00168" id="fc00168" placeholder="" value="">
                    </span>
    </div>


</td></tr><tr class="static_table_component_tr_4"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="opacity: 1;">

    <label class="label-class" contenteditable="true">Rapid Progressive</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="204" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00164"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00164" id="fc00164" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="210" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00170"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00170" id="fc00170" placeholder="" value="">
                    </span>
    </div>


</td></tr><tr class="static_table_component_tr_5"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="display: block;">

    <label class="label-class" contenteditable="true">Slow Progressive</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="205" attr_dataset_id="0" style="opacity: 1;">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00165"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00165" id="fc00165" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="211" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00171"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00171" id="fc00171" placeholder="" value="">
                    </span>
    </div>


</td></tr><tr class="static_table_component_tr_6"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0" style="display: block;">

    <label class="label-class" contenteditable="true">Non Progressive</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="206" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00166"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00166" id="fc00166" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="212" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00172"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00172" id="fc00172" placeholder="" value="">
                    </span>
    </div>


</td></tr><tr class="static_table_component_tr_7"><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true">Immotile</label>
</div>
<div class="clearfix"></div>

</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="207" attr_dataset_id="0" style="opacity: 1;">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00167"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00167" id="fc00167" placeholder="" value="">
                    </span>
    </div>


</td><td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   ><div class="col-md-12  data_id_control_id"    attr_control_id="215" attr_dataset_id="0">

        <label></label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00175"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00175" id="fc00175" placeholder="" value="">
                    </span>
    </div>


</td></tr></tbody></table> </div></div>
        </fieldset>
    <div class="col-md-6  data_id_control_id"    attr_control_id="216" attr_dataset_id="0">

        <label>Name Of IVF Consultant</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00176"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00176" id="fc00176" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="217" attr_dataset_id="0" style="opacity: 1;">

        <label>Name Of Embryologist</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00177"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00177" id="fc00177" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true">Sign Of IVF Consultant</label>
</div>
<div class="clearfix"><div class="col-md-6  data_id_control_id"    attr_control_id="111" attr_dataset_id="0">

    <label class="label-class" contenteditable="true">Sign Of Embryologist</label>
</div>
<div class="clearfix"></div>

</div>

</div>


</div></h4>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/master/dynamic_templates/js/iuireport.js?version=" . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
