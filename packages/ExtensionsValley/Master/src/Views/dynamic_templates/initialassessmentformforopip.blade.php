<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template_mini.css")}}" rel="stylesheet"><style>
    #dynamic_template_data .form-control {
    /* background: #f8f8f8 !important; */
    background:white !important;
    border: 1px solid #e4e3e3 !important;
    border-radius:4px;
    box-shadow: none;
    font-size: 12px !important;
    font-family:'Poppins', sans-serif !important;
    font-weight: 600;
    margin-bottom: 15px;
    transition: all ease-in-out 0.4s;
    height:25px !important;
    padding-left:8px !important;
    color:#5c5c5c;
}

#dynamic_template_data label{
   font-family:'Poppins', sans-serif !important;
   font-weight:600 !important;
   color:#525252 !important;
   font-size: 12px !important;
}
</style>
<div class="col-md-12 box-body theadscroll" style="min-height:100vh !important;position:relative;">
    <div class="col-md-12" style="height:10vh !important;">

        <h5 class="dynamic-template-name" style="text-align:center;">Initial Assessment Form For Op Ip</h5>

        <!--<button class="btn btn-primary pull-right btn-main" onclick="saveTemplate();"><i class="fa fa-save"></i> Save</button>
        <button class="btn bg-blue pull-right btn-main" onclick="resetTemplate();"><i class="fa fa-repeat"></i> Reset</button>
        <button class="btn bg-blue pull-right btn-main" onclick="printTemplate();"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
        <button class="btn bg-blue pull-right btn-main" onclick="loadTemplateHistory();"><i class="fa fa-history" aria-hidden="true"></i> History</button>-->
    </div>
    <div class="col-md-12" style="min-height:80vh !important;">
        <h4><div class="col-md-2 "   >

        <label>Name</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00021"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00021" id="fc00021" placeholder="" value="" fdprocessedid="t3ai6l">
                    </span>
    </div>


<div class="col-md-1 "    style="opacity: 1;">

        <label>Age</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_age"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="age" id="age" placeholder="" value="" fdprocessedid="eu45q">
                    </span>
    </div>


<div class="col-md-1 "   >

        <label>Sex</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00022"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00022" id="fc00022" placeholder="" value="" fdprocessedid="brjdcq">
                    </span>
    </div>


<div class="col-md-3 "   >

        <label>OP No</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00023"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00023" id="fc00023" placeholder="" value="" fdprocessedid="2142ck">
                    </span>
    </div>


<div class="col-md-3 "    style="opacity: 1;">

        <label>IP No</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00024"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00024" id="fc00024" placeholder="" value="" fdprocessedid="gejlna">
                    </span>
    </div>


<div class="col-md-2 "    style="opacity: 1;">

        <label>Ward/Room No</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00025"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00025" id="fc00025" placeholder="" value="" fdprocessedid="lsuc6e">
                    </span>
    </div>


<div class="col-md-9 "   >

        <label>Department</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00026"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00026" id="fc00026" placeholder="" value="" fdprocessedid="vn5fs4">
                    </span>
    </div>


<div class="col-md-3 "    style="opacity: 1;">

        <label>Consultant</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00027"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00027" id="fc00027" placeholder="" value="" fdprocessedid="g744pj">
                    </span>
    </div><div class="col-md-8 "    style="opacity: 1;">

        <label>Presenting Complaints &amp; History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00019"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00019" id="fc00019" style="height:150px !important;" placeholder="">
            </textarea>
                    </span>
    </div>


<div class="col-md-4 "   >

        <label>Diagnosis</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00020"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00020" id="fc00020" style="height:150px !important;" placeholder="">
            </textarea>
                    </span>
    </div>


<div class="btn-group col-md-12 " data-toggle="buttons"   >

        <label>Past History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_check"></span>
        <span class="hide_print">
            <ul class="select_button no-padding check-form-input">

                <li data-value="Hyper Tension" class="">
                    <i class="fa fa-check-circle"></i> Hyper Tension
                    <input type="hidden" name="fc00028_1" id="fc00028_1" value="" disabled="">
                </li>

                <li data-value="Diabetes Mellitus" class="">
                    <i class="fa fa-check-circle"></i> Diabetes Mellitus
                    <input type="hidden" name="fc00028_2" id="fc00028_2" value="" disabled="">
                </li>

                <li data-value="Heart Disease" class="">
                    <i class="fa fa-check-circle"></i> Heart Disease
                    <input type="hidden" name="fc00028_3" id="fc00028_3" value="" disabled="">
                </li>

                <li data-value="TB" class="">
                    <i class="fa fa-check-circle"></i> TB
                    <input type="hidden" name="fc00028_4" id="fc00028_4" value="TB">
                </li>

                <li data-value="Asthma" class="">
                    <i class="fa fa-check-circle"></i> Asthma
                    <input type="hidden" name="fc00028_5" id="fc00028_5" value="Asthma">
                </li>

                <li data-value="Seizures" class="">
                    <i class="fa fa-check-circle"></i> Seizures
                    <input type="hidden" name="fc00028_6" id="fc00028_6" value="Seizures">
                </li>

                <li data-value="Bleeding Disorders" class="">
                    <i class="fa fa-check-circle"></i> Bleeding Disorders
                    <input type="hidden" name="fc00028_7" id="fc00028_7" value="Bleeding Disorders">
                </li>

                <li data-value="Dyslipidemia" class="">
                    <i class="fa fa-check-circle"></i> Dyslipidemia
                    <input type="hidden" name="fc00028_8" id="fc00028_8" value="" disabled="">
                </li>

                <li data-value="Others" class="">
                    <i class="fa fa-check-circle"></i> Others
                    <input type="hidden" name="fc00028_9" id="fc00028_9" value="Others">
                </li>
                        </ul>
        </span>
    </div>




<div class="col-md-12 "   >

        <label>Surgical History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00029"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00029" id="fc00029" style="height:100px !important;" placeholder="">
            </textarea>
                    </span>
    </div>


<div class="col-md-12 "   >

        <label>Family History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00030"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00030" id="fc00030" style="height:50px !important;" placeholder="">
            </textarea>
                    </span>
    </div>


<div class="btn-group col-md-12 " data-toggle="buttons"   >

        <label>Personal History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_check"></span>
        <span class="hide_print">
            <ul class="select_button no-padding check-form-input">

                <li data-value="Alcohol" class="">
                    <i class="fa fa-check-circle"></i> Alcohol
                    <input type="hidden" name="fc00031_1" id="fc00031_1" value="Alcohol">
                </li>

                <li data-value="IV Drug Abuse" class="">
                    <i class="fa fa-check-circle"></i> IV Drug Abuse
                    <input type="hidden" name="fc00031_2" id="fc00031_2" value="IV Drug Abuse">
                </li>

                <li data-value="Smoking" class="">
                    <i class="fa fa-check-circle"></i> Smoking
                    <input type="hidden" name="fc00031_3" id="fc00031_3" value="Smoking">
                </li>

                <li data-value="Others" class="">
                    <i class="fa fa-check-circle"></i> Others
                    <input type="hidden" name="fc00031_4" id="fc00031_4" value="Others">
                </li>
                        </ul>
        </span>
    </div>




<div class="col-md-12 "    style="opacity: 1;">

        <label>If Any Please Specify, Number Of Years Of Usage</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00032"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00032" id="fc00032" placeholder="" value="" fdprocessedid="5tazu7">
                    </span>
    </div><div class="col-md-12 "    style="opacity: 1;">

        <label>Gynaec &amp; Obstetric History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00033"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00033" id="fc00033" placeholder="" value="" fdprocessedid="s0n13f">
                    </span>
    </div>





<div class="col-md-6 "   >

        <label>Local Examination</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00035"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00035" id="fc00035" style="height:150px !important;" placeholder="">
            </textarea>
                    </span>
    </div>


<div class="col-md-6 "    style="opacity: 1;">

        <label>Investigation</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00036"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00036" id="fc00036" style="height:150px !important;" placeholder="">
            </textarea>
                    </span>
    </div>


<div class="col-md-12 "   >

        <fieldset style="min-height:100px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Systemic Examination</span></legend>
            <div style="min-height:100px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >




<div class="col-md-12 "   >

        <label>CVC</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00037"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00037" id="fc00037" placeholder="" value="" fdprocessedid="cltp4">
                    </span>
    </div>


<div class="col-md-12 "   >

        <label>Respiratory System</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00038"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00038" id="fc00038" placeholder="" value="" fdprocessedid="50ie6g">
                    </span>
    </div>


<div class="col-md-12 "    style="opacity: 1;">

        <label>GIT</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00039"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00039" id="fc00039" placeholder="" value="" fdprocessedid="f6nxoj">
                    </span>
    </div>


<div class="col-md-12 "   >

        <label>CNS</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00040"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00040" id="fc00040" placeholder="" value="" fdprocessedid="zskkg">
                    </span>
    </div>


</div>
        </fieldset>
    </div><div class="col-md-12 "    style="display: block;">

                <table class="table table-striped table-bordered">
            <thead>
                                <tr style="background: cornflowerblue;color:white;">
                    <td colspan="5"><h6 style="text-align:center;font-weight:600;">Physical Examination : Vital Signs</h6></td>
                </tr>
                            </thead>
            <tbody>
                                    <tr>
                                                                        <td style="color:#807e7e !important;">
                                <h6 style="font-weight: 600;margin: 0px;">BP</h6>
                            </td>
                                                                                                <td style="color:#807e7e !important;">
                                <h6 style="font-weight: 600;">PR</h6>
                            </td>
                                                                                                <td style="color:#807e7e !important;">
                                <h6 style="font-weight: 600;">RR</h6>
                            </td>
                                                                                                <td style="color:#807e7e !important;">
                                <h6 style="font-weight: 600;">TEMP</h6>
                            </td>
                                                                                                <td style="color:#807e7e !important;">
                                <h6 style="font-weight: 600;">SPO2</h6>
                            </td>
                                                                </tr>
                                    <tr>
                                                                        <td style="padding:0px !important;">
                                <span class="show_print" id="show_print_fc00034_1_0"></span>
                                <span class="hide_print">
                                    <input name="fc00034_1_0" id="fc00034_1_0" style="margin:0px !important;background:none !important;" type="text" class="form-control table-form-input" fdprocessedid="4chljn">
                                </span>
                            </td>
                                                                                                <td style="padding:0px !important;">
                                <span class="show_print" id="show_print_fc00034_1_1"></span>
                                <span class="hide_print">
                                    <input name="fc00034_1_1" id="fc00034_1_1" style="margin:0px !important;background:none !important;" type="text" class="form-control table-form-input" fdprocessedid="jvavd">
                                </span>
                            </td>
                                                                                                <td style="padding:0px !important;">
                                <span class="show_print" id="show_print_fc00034_1_2"></span>
                                <span class="hide_print">
                                    <input name="fc00034_1_2" id="fc00034_1_2" style="margin:0px !important;background:none !important;" type="text" class="form-control table-form-input" fdprocessedid="7wqbjn">
                                </span>
                            </td>
                                                                                                <td style="padding:0px !important;">
                                <span class="show_print" id="show_print_fc00034_1_3"></span>
                                <span class="hide_print">
                                    <input name="fc00034_1_3" id="fc00034_1_3" style="margin:0px !important;background:none !important;" type="text" class="form-control table-form-input" fdprocessedid="c5dp7l">
                                </span>
                            </td>
                                                                                                <td style="padding:0px !important;">
                                <span class="show_print" id="show_print_fc00034_1_4"></span>
                                <span class="hide_print">
                                    <input name="fc00034_1_4" id="fc00034_1_4" style="margin:0px !important;background:none !important;" type="text" class="form-control table-form-input" fdprocessedid="e3xgdc">
                                </span>
                            </td>
                                                                </tr>
                            </tbody>
        </table>
    </div><div class="col-md-12 "   >

        <fieldset style="min-height: 150px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Nutritional Screening</span></legend>
            <div style="min-height: 150px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-3 "    style="opacity: 1;">

        <label>Height</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00045"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00045" id="fc00045" placeholder="" value="" fdprocessedid="c6omni">
                    </span>
    </div>


<div class="col-md-3 "   >

        <label>Weight</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00046"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00046" id="fc00046" placeholder="" value="" fdprocessedid="o4svyrn">
                    </span>
    </div>


<div class="col-md-3 "   >

        <label>BMI</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00047"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00047" id="fc00047" placeholder="" value="" fdprocessedid="8xb1yi">
                    </span>
    </div>


<div class="col-md-3 "   >

        <label>IBW</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00048"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00048" id="fc00048" placeholder="" value="" fdprocessedid="s32hl">
                    </span>
    </div>


<div class="btn-group col-md-6 " data-toggle="buttons"   >

    <label>Recent Weight Change</label>
    <div class="clearfix"></div>
    <span class="show_print" id="show_print_radio"></span>
    <span class="hide_print">
                                                                    <label class="btn bg-blue  radio_label">
                <input type="radio" class="radio-form-input" value="1" name="fc00043" id="fc00043">Yes
            </label>
                                                            <label class="btn bg-blue  radio_label">
                <input type="radio" class="radio-form-input" value="2" name="fc00043" id="fc00043">No
            </label>
            </span>

  </div>


<div class="btn-group col-md-6 " data-toggle="buttons"   >

    <label>Dietitian Consultation</label>
    <div class="clearfix"></div>
    <span class="show_print" id="show_print_radio"></span>
    <span class="hide_print">
                                                                    <label class="btn bg-blue  radio_label">
                <input type="radio" class="radio-form-input" value="1" name="fc00044" id="fc00044">Yes
            </label>
                                                            <label class="btn bg-blue  radio_label">
                <input type="radio" class="radio-form-input" value="2" name="fc00044" id="fc00044">No
            </label>
            </span>

  </div>


</div>
        </fieldset>
    </div>














<div class="col-md-12 "    style="display: block;">

        <label>Allergies (If Any)</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00049"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00049" id="fc00049" style="height:px !important;" placeholder="">
            </textarea>
                    </span>
    </div><div class="btn-group col-md-12 " data-toggle="buttons"    style="display: block;">

    <label>PAIN ASSESSMENT SCALE</label>
    <div class="clearfix"></div>
    <span class="show_print" id="show_print_radio"></span>
    <span class="hide_print">
        <div id="static_component_pain_assessment_scale" style="display: flex;">
            <div class="pain_scale_radio">
                <input type="radio" id="option_0" name="pain_assessment_scale" value="0">
                <label for="option_0" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_0.png') }}">
                    <span>0 <br> No <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_1" name="pain_assessment_scale" value="1">
                <label for="option_1" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_1.png') }}">
                    <span>1 <br> Just <br> noticeable</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_2" name="pain_assessment_scale" value="2">
                <label for="option_2" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_2.png') }}">
                    <span>2 <br> Mild <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_3" name="pain_assessment_scale" value="3">
                <label for="option_3" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_3.png') }}">
                    <span>3 <br> Uncomfortable <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_4" name="pain_assessment_scale" value="4">
                <label for="option_4" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_4.png') }}">
                    <span>4 <br> Annoying <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_5" name="pain_assessment_scale" value="5">
                <label for="option_5" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_5.png') }}">
                    <span>5 <br> Moderate <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_6" name="pain_assessment_scale" value="6">
                <label for="option_6" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_6.png') }}">
                    <span>6 <br> Just <br> bearable</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_7" name="pain_assessment_scale" value="7">
                <label for="option_7" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_7.png') }}">
                    <span>7 <br> Strong <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_8" name="pain_assessment_scale" value="8">
                <label for="option_8" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_8.png') }}">
                    <span>8 <br> Severe <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_9" name="pain_assessment_scale" value="9">
                <label for="option_9" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_9.png') }}">
                    <span>9 <br> Horrible <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_10" name="pain_assessment_scale" value="10">
                <label for="option_10" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_10.png') }}">
                    <span>10 <br> Worst <br> pain</span>
                </label>
            </div>
        </div>
    </span>
</div><div style="min-height: 300px; display: block;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12 "    style="display: block;">

        <fieldset style="min-height:100px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Admission Orders &amp; Patient Care Plan</span></legend>
            <div style="min-height:100px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12 "    style="display: block;">

        <label>Isolation If Any</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00050"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00050" id="fc00050" placeholder="" value="" fdprocessedid="uiue7">
                    </span>
    </div>


<div class="col-md-12 "   >

        <label>Vital Signs(Q1h/Q2h/Q3h/Q4h/Q5h)</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00051"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00051" id="fc00051" placeholder="" value="" fdprocessedid="6j0kuo">
                    </span>
    </div>


<div class="col-md-12 "    style="opacity: 1;">

        <label>Diet(Regular/Specific)</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00052"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00052" id="fc00052" placeholder="" value="" fdprocessedid="qpqhl">
                    </span>
    </div>








<div class="col-md-12 "    style="display: block;">

        <label>Activity(Bed Rest / As Tolerated)</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00053"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00053" id="fc00053" placeholder="" value="" fdprocessedid="ajpdp">
                    </span>
    </div><div class="col-md-12 "    style="display: block;">

        <label>Plan</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00054"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00054" id="fc00054" style="height:150px !important;" placeholder="">
            </textarea>
                    </span>
    </div><div class="btn-group col-md-6 " data-toggle="buttons"    style="display: block; opacity: 1;">

    <label>Expected Outcome Briefed To</label>
    <div class="clearfix"></div>
    <span class="show_print" id="show_print_radio"></span>
    <span class="hide_print">
                                                                    <label class="btn bg-blue  radio_label">
                <input type="radio" class="radio-form-input" value="1" name="fc00055" id="fc00055">Patient
            </label>
                                                            <label class="btn bg-blue  radio_label">
                <input type="radio" class="radio-form-input" value="2" name="fc00055" id="fc00055">Bystander
            </label>
            </span>

  </div><div class="btn-group col-md-6 " data-toggle="buttons"    style="display: block;">

    <label>Expected Cost Briefed To</label>
    <div class="clearfix"></div>
    <span class="show_print" id="show_print_radio"></span>
    <span class="hide_print">
                                                                    <label class="btn bg-blue  radio_label">
                <input type="radio" class="radio-form-input" value="1" name="fc00056" id="fc00056">Patient
            </label>
                                                            <label class="btn bg-blue  radio_label">
                <input type="radio" class="radio-form-input" value="2" name="fc00056" id="fc00056">Bystander
            </label>
            </span>

  </div><div class="col-md-12 "    style="display: block;">

        <label>Cross Consultation</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00057"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00057" id="fc00057" style="height:120px !important;" placeholder="">
            </textarea>
                    </span>
    </div>


</div><div class="col-md-12 "    style="display: block;">

        <label>Advice On Discharge</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00058"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00058" id="fc00058" style="height:120px !important;" placeholder="">
            </textarea>
                    </span>
    </div>
        </fieldset>
    </div>




















<div class="col-md-12 "   >

        <label>Post Operative Complaint</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00059"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00059" id="fc00059" style="height:150px !important;" placeholder="">
            </textarea>
                    </span>
    </div>


<div class="col-md-12 "   >

        <fieldset style="min-height: 250px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Follow-up 1st Visit</span></legend>
            <div style="min-height: 250px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-4 "    style="display: block; opacity: 1;">

        <label>Date</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00061"></span>
        <span class="hide_print">
            <input type="text" class="form-control datepicker datepicker-form-input" name="fc00061" id="fc00061" placeholder="" value=" " fdprocessedid="mohz2l">
        </span>
    </div><div class="col-md-12 "    style="display: block;">

        <label>Complaints</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00060"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00060" id="fc00060" style="height:120px !important;" placeholder="">
            </textarea>
                    </span>
    </div>


</div>
        </fieldset>
    </div>


<div class="col-md-12 "   >

        <label>Further Follow-up</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00063"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00063" id="fc00063" style="height:200px !important;" placeholder="">
            </textarea>
                    </span>
    </div>


<div class="col-md-9 "    style="opacity: 1;">

        <label>Doctor's Name &amp; Signature</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00064"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00064" id="fc00064" placeholder="" value="" fdprocessedid="52zjc8">
                    </span>
    </div>


<div class="col-md-3 "    style="opacity: 1;">

        <label>Date &amp; Time</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00066"></span>
        <span class="hide_print">
            <input type="text" class="form-control datetimepicker datetimepicker-form-input" name="fc00066" id="fc00066" placeholder="" value=" " fdprocessedid="7gji15">
        </span>
    </div>



<div class="col-md-9 "    style="opacity: 1;">

        <label>Consultant Name &amp; Signature</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00065"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00065" id="fc00065" placeholder="" value="" fdprocessedid="b61oq">
                    </span>
    </div>


<div class="col-md-3 "    style="opacity: 1;">

        <label>Date &amp; Time</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00062"></span>
        <span class="hide_print">
            <input type="text" class="form-control datetimepicker datetimepicker-form-input" name="fc00062" id="fc00062" placeholder="" value=" " fdprocessedid="3z7aci">
        </span>
    </div>



<div class="col-md-9 "   >

    <h4 class="header-class">(To be signed within 24 hours of admission)</h4>
</div>
<div class="clearfix"></div>


</div></h4>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/master/dynamic_templates/js/initialassessmentformforopip.js?version=" . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
