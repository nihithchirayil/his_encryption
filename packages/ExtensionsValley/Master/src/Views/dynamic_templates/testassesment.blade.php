<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template_mini.css")}}" rel="stylesheet">
<div class="col-md-12 box-body theadscroll" style="min-height:100vh !important;position:relative;">
    <div class="col-md-12" style="height:10vh !important;">

        <h4 class="dynamic-template-name">Test Assesment</h4>

        <!--<button class="btn btn-primary pull-right btn-main" onclick="saveTemplate();"><i class="fa fa-save"></i> Save</button>
        <button class="btn bg-blue pull-right btn-main" onclick="resetTemplate();"><i class="fa fa-repeat"></i> Reset</button>
        <button class="btn bg-blue pull-right btn-main" onclick="printTemplate();"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
        <button class="btn bg-blue pull-right btn-main" onclick="loadTemplateHistory();"><i class="fa fa-history" aria-hidden="true"></i> History</button>-->
    </div>
    <div class="col-md-12" style="min-height:80vh !important;">
        <h4><div class="col-md-2  data_id_control_id"    attr_control_id="60" attr_dataset_id="14">

        <label>Name</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00021"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00021" id="fc00021" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-1  data_id_control_id"    attr_control_id="34" attr_dataset_id="14">

        <label>Age</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_age"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="age" id="age" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-1  data_id_control_id"    attr_control_id="61" attr_dataset_id="14">

        <label>Sex</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00022"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00022" id="fc00022" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="62" attr_dataset_id="14">

        <label>OP No</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00023"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00023" id="fc00023" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="63" attr_dataset_id="14">

        <label>IP No</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00024"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00024" id="fc00024" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-2  data_id_control_id"    attr_control_id="64" attr_dataset_id="14">

        <label>Ward/Room No</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00025"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00025" id="fc00025" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-9  data_id_control_id"    attr_control_id="65" attr_dataset_id="14">

        <label>Department</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00026"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00026" id="fc00026" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="66" attr_dataset_id="14">

        <label>Consultant</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00027"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00027" id="fc00027" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-8  data_id_control_id"    attr_control_id="58" attr_dataset_id="0">

        <label>Presenting Complaints &amp; History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00019"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00019" id="fc00019" style="height:150px !important;" placeholder=""></textarea>
                    </span>
    </div>


<div class="col-md-4  data_id_control_id"    attr_control_id="59" attr_dataset_id="0">

        <label>Diagnosis</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00020"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00020" id="fc00020" style="height:150px !important;" placeholder=""></textarea>
                    </span>
    </div><div class="btn-group col-md-12  data_id_control_id" data-toggle="buttons"    attr_control_id="67" attr_dataset_id="0">

        <label>Past History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00028"></span>
        <span class="hide_print">
            <ul class="select_button no-padding check-form-input">

                    <li data-value="Hyper Tension" class="">
                        <i class="fa fa-check-circle"></i> Hyper Tension
                        <input type="hidden" name="fc00028" id="fc00028_1" value="">
                    </li>

                    <li data-value="Diabetes Mellitus" class="">
                        <i class="fa fa-check-circle"></i> Diabetes Mellitus
                        <input type="hidden" name="fc00028" id="fc00028_2" value="">
                    </li>

                    <li data-value="Heart Disease" class="">
                        <i class="fa fa-check-circle"></i> Heart Disease
                        <input type="hidden" name="fc00028" id="fc00028_3" value="">
                    </li>

                    <li data-value="TB" class="">
                        <i class="fa fa-check-circle"></i> TB
                        <input type="hidden" name="fc00028" id="fc00028_4" value="">
                    </li>

                    <li data-value="Asthma" class="">
                        <i class="fa fa-check-circle"></i> Asthma
                        <input type="hidden" name="fc00028" id="fc00028_5" value="">
                    </li>

                    <li data-value="Seizures" class="">
                        <i class="fa fa-check-circle"></i> Seizures
                        <input type="hidden" name="fc00028" id="fc00028_6" value="">
                    </li>

                    <li data-value="Bleeding Disorders" class="">
                        <i class="fa fa-check-circle"></i> Bleeding Disorders
                        <input type="hidden" name="fc00028" id="fc00028_7" value="">
                    </li>

                    <li data-value="Dyslipidemia" class="">
                        <i class="fa fa-check-circle"></i> Dyslipidemia
                        <input type="hidden" name="fc00028" id="fc00028_8" value="">
                    </li>

                    <li data-value="Others" class="">
                        <i class="fa fa-check-circle"></i> Others
                        <input type="hidden" name="fc00028" id="fc00028_9" value="">
                    </li>
                            </ul>
        </span>
    </div>




<div class="col-md-12  data_id_control_id"    attr_control_id="68" attr_dataset_id="0">

        <label>Surgical History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00029"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00029" id="fc00029" style="height:100px !important;" placeholder=""></textarea>
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="69" attr_dataset_id="0">

        <label>Family History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00030"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00030" id="fc00030" style="height:50px !important;" placeholder=""></textarea>
                    </span>
    </div>


<div class="btn-group col-md-12  data_id_control_id" data-toggle="buttons"    attr_control_id="70" attr_dataset_id="0">

        <label>Personal History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00031"></span>
        <span class="hide_print">
            <ul class="select_button no-padding check-form-input">

                    <li data-value="Alcohol" class="">
                        <i class="fa fa-check-circle"></i> Alcohol
                        <input type="hidden" name="fc00031" id="fc00031_1" value="">
                    </li>

                    <li data-value="IV Drug Abuse" class="">
                        <i class="fa fa-check-circle"></i> IV Drug Abuse
                        <input type="hidden" name="fc00031" id="fc00031_2" value="">
                    </li>

                    <li data-value="Smoking" class="">
                        <i class="fa fa-check-circle"></i> Smoking
                        <input type="hidden" name="fc00031" id="fc00031_3" value="">
                    </li>

                    <li data-value="Others" class="">
                        <i class="fa fa-check-circle"></i> Others
                        <input type="hidden" name="fc00031" id="fc00031_4" value="">
                    </li>
                            </ul>
        </span>
    </div>




<div class="col-md-12  data_id_control_id"    attr_control_id="71" attr_dataset_id="0">

        <label>If Any Please Specify, Number Of Years Of Usage</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00032"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00032" id="fc00032" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="72" attr_dataset_id="0">

        <label>Gynaec &amp; Obstetric History</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00033"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00033" id="fc00033" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="74" attr_dataset_id="0">

        <label>Local Examination</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00035"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00035" id="fc00035" style="height:150px !important;" placeholder=""></textarea>
                    </span>
    </div>


<div class="col-md-6  data_id_control_id"    attr_control_id="75" attr_dataset_id="0">

        <label>Investigation</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00036"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00036" id="fc00036" style="height:150px !important;" placeholder=""></textarea>
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Systemic examination</span></legend>
            <div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12  data_id_control_id"    attr_control_id="76" attr_dataset_id="0">

        <label>CVC</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00037"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00037" id="fc00037" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="77" attr_dataset_id="0">

        <label>Respiratory System</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00038"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00038" id="fc00038" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="78" attr_dataset_id="0">

        <label>GIT</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00039"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00039" id="fc00039" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="79" attr_dataset_id="0">

        <label>CNS</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00040"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00040" id="fc00040" placeholder="" value="">
                    </span>
    </div>


</div>
        </fieldset>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="73" attr_dataset_id="0">

                <table class="table table-striped table-bordered">
            <thead>
                                <tr style="background: cornflowerblue;color:white;">
                    <td colspan="5"><h6 style="text-align:center;font-weight:600;">Physical Examination : Vital Signs</h6></td>
                </tr>

                            </thead>
            <tbody>
                                    <tr>
                                                                        <td style="color:#807e7e !important;">
                                <h6 style="font-weight: 600;">BP</h6>
                            </td>
                                                                                                <td style="color:#807e7e !important;">
                                <h6 style="font-weight: 600;">PR</h6>
                            </td>
                                                                                                <td style="color:#807e7e !important;">
                                <h6 style="font-weight: 600;">RR</h6>
                            </td>
                                                                                                <td style="color:#807e7e !important;">
                                <h6 style="font-weight: 600;">TEMP</h6>
                            </td>
                                                                                                <td style="color:#807e7e !important;">
                                <h6 style="font-weight: 600;">SPO2</h6>
                            </td>
                                                                </tr>
                                    <tr>
                                                                        <td style="padding:0px !important;">
                                <span class="show_print" id="show_print_fc00034_1_0"></span>
                                <span class="hide_print">
                                    <input name="fc00034_1_0" id="fc00034_1_0" style="margin:0px !important;background:none !important;" type="text" class="form-control table-form-input">
                                </span>
                            </td>
                                                                                                <td style="padding:0px !important;">
                                <span class="show_print" id="show_print_fc00034_1_1"></span>
                                <span class="hide_print">
                                    <input name="fc00034_1_1" id="fc00034_1_1" style="margin:0px !important;background:none !important;" type="text" class="form-control table-form-input">
                                </span>
                            </td>
                                                                                                <td style="padding:0px !important;">
                                <span class="show_print" id="show_print_fc00034_1_2"></span>
                                <span class="hide_print">
                                    <input name="fc00034_1_2" id="fc00034_1_2" style="margin:0px !important;background:none !important;" type="text" class="form-control table-form-input">
                                </span>
                            </td>
                                                                                                <td style="padding:0px !important;">
                                <span class="show_print" id="show_print_fc00034_1_3"></span>
                                <span class="hide_print">
                                    <input name="fc00034_1_3" id="fc00034_1_3" style="margin:0px !important;background:none !important;" type="text" class="form-control table-form-input">
                                </span>
                            </td>
                                                                                                <td style="padding:0px !important;">
                                <span class="show_print" id="show_print_fc00034_1_4"></span>
                                <span class="hide_print">
                                    <input name="fc00034_1_4" id="fc00034_1_4" style="margin:0px !important;background:none !important;" type="text" class="form-control table-form-input">
                                </span>
                            </td>
                                                                </tr>
                            </tbody>
        </table>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Nutritional Screening</span></legend>
            <div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-3  data_id_control_id"    attr_control_id="84" attr_dataset_id="0" style="opacity: 1;">

        <label>Height</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00045"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00045" id="fc00045" placeholder="" value="">
                    </span>
    </div><div class="col-md-3  data_id_control_id"    attr_control_id="86" attr_dataset_id="0" style="display: block; opacity: 1;">

        <label>BMI</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00047"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00047" id="fc00047" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-3  data_id_control_id"    attr_control_id="85" attr_dataset_id="0" style="opacity: 1;">

        <label>Weight</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00046"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00046" id="fc00046" placeholder="" value="">
                    </span>
    </div>











<div class="col-md-3  data_id_control_id"    attr_control_id="87" attr_dataset_id="0">

        <label>IBW</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00048"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00048" id="fc00048" placeholder="" value="">
                    </span>
    </div>


<div class="btn-group col-md-6  data_id_control_id" data-toggle="buttons"    attr_control_id="82" attr_dataset_id="0">

        <label>Recent Weight Change</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_radio"></span>
        <span class="hide_print">
                                                                                            <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="1" name="fc00043" id="fc00043">Yes
                </label>
                                                                                <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="2" name="fc00043" id="fc00043">No
                </label>
                    </span>

    </div>


<div class="btn-group col-md-6  data_id_control_id" data-toggle="buttons"    attr_control_id="83" attr_dataset_id="0">

        <label>Dietitian Consultation</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_radio"></span>
        <span class="hide_print">
                                                                                            <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="1" name="fc00044" id="fc00044">Yes
                </label>
                                                                                <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="2" name="fc00044" id="fc00044">No
                </label>
                    </span>

    </div>


</div>
        </fieldset>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="88" attr_dataset_id="0">

        <label>Allergies (If Any)</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00049"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00049" id="fc00049" style="height:px !important;" placeholder=""></textarea>
                    </span>
    </div>


<div class="btn-group col-md-12 " data-toggle="buttons"   >

    <label>PAIN ASSESSMENT SCALE</label>
    <div class="clearfix"></div>
    <span class="show_print" id="show_print_pain"></span>
    <span class="hide_print">
        <div id="static_component_pain_assessment_scale" style="display: flex;">
            <div class="pain_scale_radio">
                <input type="radio" id="option_0" name="pain_assessment_scale" value="0">
                <label for="option_0" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_0.png') }}">
                    <span>0 <br> No <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_1" name="pain_assessment_scale" value="1">
                <label for="option_1" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_1.png') }}">
                    <span>1 <br> Just <br> noticeable</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_2" name="pain_assessment_scale" value="2">
                <label for="option_2" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_2.png') }}">
                    <span>2 <br> Mild <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_3" name="pain_assessment_scale" value="3">
                <label for="option_3" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_3.png') }}">
                    <span>3 <br> Uncomfortable <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_4" name="pain_assessment_scale" value="4">
                <label for="option_4" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_4.png') }}">
                    <span>4 <br> Annoying <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_5" name="pain_assessment_scale" value="5">
                <label for="option_5" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_5.png') }}">
                    <span>5 <br> Moderate <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_6" name="pain_assessment_scale" value="6">
                <label for="option_6" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_6.png') }}">
                    <span>6 <br> Just <br> bearable</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_7" name="pain_assessment_scale" value="7">
                <label for="option_7" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_7.png') }}">
                    <span>7 <br> Strong <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_8" name="pain_assessment_scale" value="8">
                <label for="option_8" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_8.png') }}">
                    <span>8 <br> Severe <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_9" name="pain_assessment_scale" value="9">
                <label for="option_9" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_9.png') }}">
                    <span>9 <br> Horrible <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_10" name="pain_assessment_scale" value="10">
                <label for="option_10" class="radio-image">
                    <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_10.png') }}">
                    <span>10 <br> Worst <br> pain</span>
                </label>
            </div>
        </div>
    </span>
</div>


<div class="col-md-12  data_id_control_id"    attr_control_id="93" attr_dataset_id="0" style="display: block;">

        <label>Plan</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00054"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00054" id="fc00054" style="height:150px !important;" placeholder=""></textarea>
                    </span>
    </div><div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0" style="display: block; opacity: 1;">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Admission Orders And Patient Care Plan</span></legend>
            <div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-12  data_id_control_id"    attr_control_id="89" attr_dataset_id="0">

        <label>Isolation If Any</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00050"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00050" id="fc00050" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="90" attr_dataset_id="0">

        <label>Vital Signs(Q1h/Q2h/Q3h/Q4h/Q5h)</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00051"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00051" id="fc00051" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="91" attr_dataset_id="0">

        <label>Diet(Regular/Specific)</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00052"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00052" id="fc00052" placeholder="" value="">
                    </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="92" attr_dataset_id="0">

        <label>Activity(Bed Rest / As Tolerated)</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00053"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input" name="fc00053" id="fc00053" placeholder="" value="">
                    </span>
    </div>


</div>
        </fieldset>
    </div><div class="btn-group col-md-6  data_id_control_id" data-toggle="buttons"    attr_control_id="95" attr_dataset_id="0" style="display: block;">

        <label>Expected Cost Briefed To</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00056"></span>
        <span class="hide_print">
                                                                                            <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="1" name="fc00056" id="fc00056">Patient
                </label>
                                                                                <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="2" name="fc00056" id="fc00056">Bystander
                </label>
                    </span>

    </div>





<div class="btn-group col-md-6  data_id_control_id" data-toggle="buttons"    attr_control_id="94" attr_dataset_id="0">

        <label>Expected Outcome Briefed To</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00055"></span>
        <span class="hide_print">
                                                                                            <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="1" name="fc00055" id="fc00055">Patient
                </label>
                                                                                <label class="btn bg-blue  radio_label">
                    <input type="radio" class="radio-form-input" value="2" name="fc00055" id="fc00055">Bystander
                </label>
                    </span>

    </div><div class="col-md-12  data_id_control_id"    attr_control_id="21" attr_dataset_id="0" style="display: block;">

        <fieldset style="min-height:300px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Follow-up 1st Visit</span></legend>
            <div style="min-height:300px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >

            <div class="col-md-4  data_id_control_id"    attr_control_id="100" attr_dataset_id="0">

        <label>Date</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00061"></span>
        <span class="hide_print">
            <input type="text" class="form-control datepicker datepicker-form-input" name="fc00061" id="fc00061" placeholder="" value=" ">
        </span>
    </div>


<div class="col-md-12  data_id_control_id"    attr_control_id="99" attr_dataset_id="0">

        <label>Complaints</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_fc00060"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00060" id="fc00060" style="height:120px !important;" placeholder=""></textarea>
                    </span>
    </div>


</div>
        </fieldset>
    </div></h4>
    </div>
</div>
<script src="{{asset("packages/extensionsvalley/master/dynamic_templates/js/testassesment.js?version=" . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
