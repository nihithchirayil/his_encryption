<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template_mini.css")}}" rel="stylesheet">
<style>
   #dynamic_template_data .form-control {
   /* background: #f8f8f8 !important; */
   background:white !important;
   border: 1px solid #e4e3e3 !important;
   border-radius:4px;
   box-shadow: none;
   font-size: 12px !important;
   font-family:'Poppins', sans-serif !important;
   font-weight: 600;
   margin-bottom: 15px;
   transition: all ease-in-out 0.4s;
   height:25px !important;
   padding-left:8px !important;
   color:#5c5c5c;
   }
   #dynamic_template_data label{
   font-family:'Poppins', sans-serif !important;
   font-weight:600 !important;
   color:#525252 !important;
   font-size: 12px !important;
   }
   #dynamicTemplatePrintPreviewModalData .form-control {
   /* background: #f8f8f8 !important; */
   background:white !important;
   border: 1px solid #e4e3e3 !important;
   border-radius:4px;
   box-shadow: none;
   font-size: 12px !important;
   font-family:'Poppins', sans-serif !important;
   font-weight: 600;
   margin-bottom: 15px;
   transition: all ease-in-out 0.4s;
   height:25px !important;
   padding-left:8px !important;
   color:#5c5c5c;
   }
   #dynamicTemplatePrintPreviewModalData label{
   font-family:'Poppins', sans-serif !important;
   font-weight:600 !important;
   color:#525252 !important;
   font-size: 12px !important;
   }
   .show_print{
    background: white !important;
    /* border: 1px solid #dfdfdf !important; */
   }
</style>
<div class="col-md-12 theadscroll no-padding no-margin" style="min-height:100vh !important;position:relative;">
   <div class="col-md-12">
      <h5 class="dynamic-template-name">Clinical Assessment Form New</h5>
   </div>
   <div class="col-md-12 no-padding " style="">
      <h4>
         <div class="col-md-4  data_id_control_id no-padding"    attr_control_id="17" attr_dataset_id="0">
            <fieldset style="min-height:478px;" class="scheduler-border">
               <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Patient History</span></legend>
               <div style="min-height:100px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >
                  <div class="col-md-12  data_id_control_id"    attr_control_id="219" attr_dataset_id="9" attr_data_point="">
                     <label>Allergic History</label>
                     <div class="clearfix"></div>
                     <span class="show_print"  id="show_print_fc00152"></span>
                     <span class="hide_print">
                     <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00152" id="fc00152" style="height:px !important;" placeholder=""></textarea>
                     </span>
                  </div>
                  <div class="col-md-12  data_id_control_id"    attr_control_id="220" attr_dataset_id="9" attr_data_point="">
                     <label>Chronic Illness History</label>
                     <div class="clearfix"></div>
                     <span class="show_print"  id="show_print_fc00153"></span>
                     <span class="hide_print">
                     <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00153" id="fc00153" style="height:px !important;" placeholder=""></textarea>
                     </span>
                  </div>
               </div>
               <div class="col-md-12  data_id_control_id no-margin"    attr_control_id="69" attr_dataset_id="9" attr_data_point="">
                  <label>Family/Social/Economic History</label>
                  <div class="clearfix"></div>
                  <span class="show_print"  id="show_print_fc00030"></span>
                  <span class="hide_print">
                  <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00030" id="fc00030" style="height:50px !important;" placeholder=""></textarea>
                  </span>
               </div>
               <div class="col-md-12  data_id_control_id"    attr_control_id="221" attr_dataset_id="9" attr_data_point="">
                  <label>Investigation History</label>
                  <div class="clearfix"></div>
                  <span class="show_print"  id="show_print_fc00154"></span>
                  <span class="hide_print">
                  <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00154" id="fc00154" style="height:px !important;" placeholder=""></textarea>
                  </span>
               </div>
            </fieldset>
         </div>
         <div class="col-md-8  data_id_control_id no-padding"    attr_control_id="19" attr_dataset_id="0" style="opacity: 1;">
            <fieldset style="min-height:100px;" class="scheduler-border">
               <legend class="scheduler-border"><span class="contenteditable" contenteditable="true">Patient Assessment</span></legend>
               <div style="min-height:100px;" class="control-group dest_copy  ui-" ondragover="dragover_handler(event);"   >
                  <div class="col-md-12  data_id_control_id"    attr_control_id="222" attr_dataset_id="9" attr_data_point="">
                     <label>Chief Complaints</label>
                     <div class="clearfix"></div>
                     <span class="show_print"  id="show_print_fc00155"></span>
                     <span class="hide_print">
                     <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00155" id="fc00155" style="height:px !important;" placeholder=""></textarea>
                     </span>
                  </div>
                  <div class="col-md-12  data_id_control_id"    attr_control_id="212" attr_dataset_id="9" attr_data_point="">
                     <label>History of Present Illness (HPI)</label>
                     <div class="clearfix"></div>
                     <span class="show_print"  id="show_print_fc00145"></span>
                     <span class="hide_print">
                     <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00145" id="fc00145" style="height:px !important;" placeholder=""></textarea>
                     </span>
                  </div>
               </div>
               <div class="col-md-12 "   >
                  <table class="table table-bordered static_table_component">
                     <tbody>
                        <tr class="static_table_component_tr_1">
                           <td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   >
                              <div class="col-md-12  data_id_control_id"    attr_control_id="216" attr_dataset_id="9" attr_data_point="">
                                 <label>General Examinations</label>
                                 <div class="clearfix"></div>
                                 <span class="show_print"  id="show_print_fc00149"></span>
                                 <span class="hide_print">
                                 <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00149" id="fc00149" style="height:px !important;" placeholder=""></textarea>
                                 </span>
                              </div>
                           </td>
                           <td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   >
                              <div class="col-md-12  data_id_control_id"    attr_control_id="217" attr_dataset_id="9" attr_data_point="">
                                 <label>Systematic Examinations</label>
                                 <div class="clearfix"></div>
                                 <span class="show_print"  id="show_print_fc00150"></span>
                                 <span class="hide_print">
                                 <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00150" id="fc00150" style="height:px !important;" placeholder=""></textarea>
                                 </span>
                              </div>
                           </td>
                        </tr>
                        <tr class="static_table_component_tr_2">
                           <td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   >
                              <div class="col-md-12  data_id_control_id"    attr_control_id="218" attr_dataset_id="9" attr_data_point="">
                                 <label>Treatment Plan/Management</label>
                                 <div class="clearfix"></div>
                                 <span class="show_print"  id="show_print_fc00151"></span>
                                 <span class="hide_print">
                                 <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00151" id="fc00151" style="height:px !important;" placeholder=""></textarea>
                                 </span>
                              </div>
                           </td>
                           <td class="control-group dest_copy ui-" ondragover="dragover_handler(event);"   >
                              <div class="col-md-12  data_id_control_id"    attr_control_id="59" attr_dataset_id="0" attr_data_point="Dia">
                                 <label>Diagnosis</label>
                                 <div class="clearfix"></div>
                                 <span class="show_print"  id="show_print_fc00020"></span>
                                 <span class="hide_print">
                                 <textarea class="form-control textarea-form-input" autocomplete="off" name="fc00020" id="fc00020" style="" placeholder=""></textarea>
                                 </span>
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </fieldset>
         </div>
      </h4>
   </div>
</div>
<script src="{{asset("packages/extensionsvalley/master/dynamic_templates/js/clinicalassessmentformnew.js?version=" . env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
