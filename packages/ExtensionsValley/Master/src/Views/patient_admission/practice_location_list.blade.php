<div class="col-md-12 padding_sm">
    <div class="row select_default_doctor_modal_data">
        @foreach ($practice_location as $data)


            <div class="col-md-3 padding_sm" style="margin-bottom: 10px;">
                <div onclick="set_practice_location('{{ $data->id }}','{{ $data->name }}');"
                    class="select-doctor-box" data-practice_location-id="{{ $data->id }}" title="Click to Select">
                    <div style="display:none;" class="selected_dr_div "><i class="fa fa-check selected_dr_check"></i>
                    </div>
                    <i class="fa fa-hospital-o fa-3x select_default_dr_icon" aria-hidden="true"></i>
                    <div class="clearfix"></div>
                    <span class="select_dr_name">
                        <h5>{{ $data->name }}</h5>
                    </span>
                </div>
            </div>

        @endforeach
    </div>
</div>
