<div class="col-md-12 box-body" style="padding:15px;">
    <input type="hidden" name="admit_patient_id" id="admit_patient_id" value="{{$patient_id}}">

    <div class="col-md-6">
        <div class="col-md-12">
            <label><b>Admitting Doctor</b></label>
            {!! Form::select('admitting_doctor', $doctor_list,'', ['class' => 'form-control select2', 'id'=>'admitting_doctor', 'placeholder' => 'Select', 'required' => 'required']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="col-md-12">
            <label><b>Admit Location</b></label>
            {!! Form::select('admit_location', $nursing_stations_list,'', ['class' => 'form-control select2', 'id'=>'admit_location', 'placeholder' => 'Select', 'required' => 'required']) !!}
        </div>
    </div>
    <div class="col-md-6" style="margin-top:15px;">
        <div class="col-md-12">
            <label><b>Choose Room/Bed</b></label>
            {!! Form::select('admit_bed', $beds,'', ['class' => 'form-control select2', 'id'=>'admit_bed', 'placeholder' => 'Select', 'required' => 'required']) !!}
        </div>
    </div>

    <div class="col-md-12" style="margin-top:35px;">
        <div class="col-md-12">
            <button type="button" class="btn bg-blue pull-right"  onclick="proceed_admission();" id="admit_patient">Admit Patient</button>
        </div>
    </div>
</div>
