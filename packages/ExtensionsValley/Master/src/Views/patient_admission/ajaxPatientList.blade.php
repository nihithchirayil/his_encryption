<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var file_token = $('#hidden_filetoken').val();
            var op_no_search_hidden = $("#op_no_search_hidden").val();
            var params = {
                _token: file_token,
                op_no_search_hidden: op_no_search_hidden,
                is_ajax: 1
            };
            $.ajax({
                type: "POST",
                url: url,
                data: params,
                beforeSend: function() {
                    $('#patientListDiv').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(msg) {
                    $('#patientListDiv').html(msg);
                },
                complete: function() {
                    $('#patientListDiv').LoadingOverlay("hide");
                },
                error: function() {
                    toastr.error("Please Check Internet Connection");
                }
            });
            return false;
        });
    });

    function PatientEdit(id) {
        window.location = "{!! route('extensionsvalley.patient_register.patientEdit') !!}/" + id;
    }
</script>
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table">
            <thead>
                <tr class="table_header_bg">
                    <th width="5%" class="common_td_rules">Name</th>
                    <th width="5%" class="common_td_rules">UHID </th>
                    <th width="5%" class="common_td_rules">Area</th>
                    <th width="5%" class="common_td_rules">Age/Gender</th>
                    <th width="5%" class="common_td_rules">Phone</th>
                    <th width="5%" class="common_td_rules">Last visit Date</th>
                    <th width="5%" class="common_td_rules">Visit Status</th>
                    <th width="5%" class="common_td_rules">Room/Bed</th>
                    <th width="5%" class="common_td_rules">Admitted by</th>
                    <th width="5%" class="common_td_rules">Location</th>
                    <th width="8%" class="common_td_rules">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php $i = 0; @endphp
                @if (isset($patient_data) && sizeof($patient_data) > 0)
                    @foreach ($patient_data as $list)
                        <tr style="cursor:pointer; @if(trim($list->visit_status)=='IP')background-color:#a8f0ca;@endif">
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->patient_name }}
                            </td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->uhid }}
                            </td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->area }}
                            </td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->age }}/{{ $list->gender}}
                            </td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->phone }}</td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                @if ($list->last_visit_datetime != '')
                                    {{ date('M-d-Y', strtotime($list->last_visit_datetime)) }}
                                @endif
                            </td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->visit_status }}</td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->bed_name }}</td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->nursing_station }}</td>
                            <td onclick="goToSelectedPatient('{{ $list->id }}')" class="common_td_rules">
                                {{ $list->doctor_name }}</td>
                            <td style="text-align:left;">
                                {{-- @if (date('Y-m-d') != date('Y-m-d', strtotime($list->last_visit_datetime)))
                                    <button type="button" title="Renew" class="btn btn-info btn-xs"
                                        onclick="PatientRenew('{{ $list->id }}')">
                                        <i class="fa fa-repeat"></i>
                                    </button>
                                @endif --}}

                                <button type="button" title="View" class="btn btn-warning btn-xs"
                                    onclick="goToSelectedPatient('{{ $list->id }}');">
                                    <i class="fa fa-eye"></i>
                                </button>

                                {{-- <button type="button" title="Edit" class="btn btn-success btn-xs"
                                    onclick="PatientEdit('{{ $list->id }}')">
                                    <i class="fa fa-pencil"></i>
                                </button> --}}
                                @if(trim($list->visit_status)=='OP')
                                <button type="button" title="Admit" class="btn bg-blue btn-xs"
                                    onclick="AdmitPatient('{{ $list->id }}')">
                                    <i class="fa fa-bed"></i>
                                </button>
                                @endif
                                @if(trim($list->visit_status)!='OP')
                                <button type="button" title="Change Admitting Doctor" class="btn bg-blue btn-xs"
                                    onclick="ChangeAdmitDoctor('{{ $list->id }}','{{ $list->visit_id }}','{{ $list->admitting_doctor }}')">
                                    <i class="fa fa-user-md"></i>
                                </button>
                                @endif
                            </td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
