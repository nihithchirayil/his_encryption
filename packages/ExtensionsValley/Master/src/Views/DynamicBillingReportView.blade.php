@extends('Emr::emr.page')
{{-- <!-- Title --> --}}
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style.css') !!}
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/dashboard/plugins/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/dashboard/plugins/custom-input-icons/custom-input-icons.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/dashboard/plugins/perfect-scrollbar/css/perfect-scrollbar.css")}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css">

{!! Html::style('packages/extensionsvalley/webdatarocks/webdatarocks.min.css') !!}
{!! Html::script('packages/extensionsvalley/webdatarocks/webdatarocks.toolbar.min.js') !!}
{!! Html::script('packages/extensionsvalley/webdatarocks/webdatarocks.js') !!}


<script src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://cdn.webdatarocks.com/latest/webdatarocks.googlecharts.js"></script>
<script src="https://www.gstatic.com/charts/loader.js"></script>




<style>
    .avlailable_check{
        margin-top: 2px !important;
        margin-right: 5px !important;
    }
    .order_by_select{
        float:right !important;
    }
    .filters_title {
        margin-top:0px !important;
        margin-bottom:0px !important;
        margin-left:2px !important;
    }

    .filter_label{
        font-weight: 50 !important;
        font-size: 11px !important;
        color: #266c94 !important;
    }

    .col-xs-2{
        padding-right:7px !important;
        padding-left:7px !important;
    }

    .btn_add_to_order_by{
        float: right;
        width: 18px;
        height: 16px;
        padding: 0px;
    }
    .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 350px;
        margin: 3px 0px 0px 0px;
        overflow-y: auto;
        width: 250px;
        z-index: 599;
        position:relative;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);

    }
    .ajaxSearchBox>li{
        padding:2px 2px 5px 5px;
        font-size: 10px !important;
        font-weight: 400 !important;
        font-family: "sans-serif";
        border-bottom: 1px solid grey;
    }

    .liHover{
        background: #E1D5F4 !important;
        color:white;
        padding:2px 2px 5px 5px;
        font-size: 10px !important;
        font-weight: 400;
        font-family: "sans-serif";
        border-bottom: 1px solid white;
    }
    .bg-blue-active {
        background-color: #E1D5F4  !important;
    }
    .filter_label {
        color: #1c3525 !important;
    }
    .bg-primary {
        color: black;
        background-color: #768a7e;
    }
    .box.box-primary {
        border-top:1px solid #E1D5F4 ;
    }
    .btn-info{
        color: black !important;
        background-color: #E1D5F4  !important;
        border-color: #768a7e !important;
        background-image: linear-gradient(to bottom,#bedecb 0,#2aabd2 100%);
    }
    .liHover {
        background: #8e948f;
        color: white;
    }

    .webDataRocksClass #wdr-toolbar-wrapper>#wdr-toolbar>.wdr-toolbar-group-right>li>a span, .webDataRocksClass #wdr-toolbar-wrapper>#wdr-toolbar>li>a span {
        font-size: 12px !important;
        top: 40px;
    }

    .webDataRocksClass #wdr-toolbar-wrapper>#wdr-toolbar>li>a>svg, .webDataRocksClass #wdr-toolbar-wrapper>#wdr-toolbar>li>a>svg>path {
        width: 20px;
    }
    .webDataRocksClass #wdr-toolbar-wrapper {
        height: 56px;
    }
    .webDataRocksClass #wdr-toolbar-wrapper>#wdr-toolbar {
        height: 50px;
    }
    .webDataRocksClass #wdr-toolbar-wrapper>#wdr-toolbar>a {
        height: 58px;
    }
    .webDataRocksClass #wdr-pivot-view .wdr-grid-layout .wdr-resize-handles .wdr-wrapper {
        background: rgba(225, 213, 244, 0.4);
    }
    .webDataRocksClass #wdr-toolbar-wrapper #wdr-toolbar li a svg, #wdr-toolbar-wrapper #wdr-toolbar li a svg path {
        fill: #9473C8;
        width:20px;
    }
    #wdr-toolbar-wrapper #wdr-toolbar .wdr-dropdown {
        top: 60px;
    }
    #wdr-pivot-view .wdr-fields-view-wrap #wdr-btn-open-fields {
        height:30px;
        width:30px;
        padding: 2px;
    }
    #wdr-pivot-view .wdr-grid-layout.wdr-flat-view .wdr-header {
        font-weight: 300;
        font-size: 11px;
    }

    #wdr-pivot-view #wdr-grid-view div.alter1 {
        background-color: #eff9ff;
    }

    #wdr-pivot-view #wdr-grid-view div.alter2 {
        background-color: #cceeff;
    }

    #wdr-pivot-view #wdr-grid-view div.alter3 {
        background-color: yellowgreen;
    }

    input::placeholder {
        font-size: 9px;
    }
    .modal-header{
        background-color: #c176bc;
    }
    .modal-title{
        color: white;
    }
    .select2-selection__choice{
        font-size: 11px;
        background:#c176bc !important;
        color: white;
    }
    .select2-results__option{
        font-size: 11px;
    }
    .select2-selection__choice__remove{
       color: white !important;
    }
    .multiselect{
    background-image: linear-gradient(to bottom,#fff 0,#ecd3ec 100%);
    }


</style>
<!--<link href="https://getbootstrap.com/docs/4.0/examples/starter-template/starter-template.css" rel="stylesheet">-->
@endsection
@section('content-area')
<section class="content" style="padding:0px;">

    {!!Form::open(array('route' => $addEditButtonUrl,'name' => 'dyanamic_report_container', 'method' => 'post','class'=>'formShortcuts', 'id'=>'dyanamic_report_container'))!!}
    <input type="hidden" value='<?= $route_data ?>' id="route_value">
    <input type="hidden" value='<?= $report_id ?>' id="report_id_hidden">
    <input type="hidden" value='<?= $report_name ?>' id="report_name">
    <div class="right_col">
    <div class="col-md-12" style="padding-top: 15px;">
        <!------available colunms------------------------------------>
        <div class="col-md-2 collapse_content padding_sm">
            <div class="box no-border no-margin">
                <div class="box-body" style="padding-bottom:15px;">
                    <table class="table table_sm">
                        <thead>
                            <tr class="table_header_bg">
                                <th>Available Fileds</th>
                            </tr>
                        </thead>
                        <tbody>



                            @foreach($available_list as $data)
                            @php
                                $key = trim($data->key);
                                $value = trim($data->value);
                                $column_name = trim($data->column_name);
                                $column_name = str_replace("\"","#@#",$column_name);
                            @endphp
                            <tr>
                                <td class="available_row_list" id="available_row_{{$key}}"><label style="padding:0px;margin: 0px;">
                                    <input type="checkbox" style="" class="custom-control-input pull-left avlailable_check"
                                           id="available_list_{{$key}}" name="available_list_{{$key}}" onchange="addOrRemoveToSelected(this.id,'{{$value}}','{{$key}}','{{$column_name}}');">
                                    {!!$value!!}
                                    </label>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!---------------selected columns----------------------------->
        <div class="col-md-2 collapse_content padding_sm">
            <div class="box no-border no-margin">
                <div class="box-body" style="padding-bottom:15px;margin-bottom: 8px;">
                    <table class="table table_sm">
                        <thead>
                            <tr class="table_header_bg">
                                <th>Selected Fields</th>
                            </tr>
                        </thead>
                        <tbody class="t_sortable" id='selected_fields_list'>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box no-border no-margin">
                <div class="box-body" style="padding-bottom:15px;margin-bottom: 15px;">
                    <table class="table table_sm">
                        <thead>
                            <tr class="table_header_bg">
                                <th title="Result order can change using drag and drop columns in the list">
                                    <span style="float:right"><i class="fa fa-arrows" aria-hidden="true"></i></span>
                                    Sort</th>
                            </tr>
                        </thead>
                        <tbody id='orderby_fields_list'>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!------------filters area--------------------------------------------------------->
        <div id="filter_area" class="col-md-8" style="padding-left:0px !important;">
            <div class="box no-border no-margin">
                <div class="box-body" style="padding-bottom:15px;">
                    <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                        <thead><tr class="table_header_bg">
                                <th colspan="11">Filters
                                    <span style="float:right;">
                                        <a style="color: #000;"><i class="fa fa-expand filter_expand"></i></a>
                                    </span>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <?php
                                $i = 1;

                                foreach ($showSearchFields as $fieldType => $fieldName) {
                                    $fieldTypeArray[$i] = explode('-', $fieldType);

                                    if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                        $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                        ?>

                                        <div class= "col-xs-2">
                                            <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                            <input class="form-control hidden_search" value="" autocomplete="off" type="text"  id="{{$fieldTypeArray[$i][1]}}" name="{{$fieldTypeArray[$i][1]}}" />
                                            <div id="{{$fieldTypeArray[$i][1]}}AjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters" value="{{$hiddenFields[$hidden_filed_id]}}"  type="hidden" name="{{$fieldTypeArray[$i][1]}}_hidden" value="" id="{{$fieldTypeArray[$i][1]}}_hidden">
                                        </div>

                                        <?php
                                    } else if ($fieldTypeArray[$i][0] == 'text') {
                                        ?>
                                        <div class="col-xs-2">
                                            <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                            <input type="text" name="{{$fieldTypeArray[$i][1]}}" <?php if (isset($fieldTypeArray[$i][3])) {
                                    echo $fieldTypeArray[$i][3];
                                } ?>  value="" class="form-control filters" id="{{$fieldTypeArray[$i][1]}}" data="{{isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : ''}}" <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?> >
                                        </div>
                                        <?php
                                    } else if ($fieldTypeArray[$i][0] == 'date') {

                                        ?>
                                        <div class="col-xs-2 date_filter_div">
                                            <label class="filter_label">{{$fieldTypeArray[$i][2]}} From</label>
                                            <input type="text" data-attr="date" name="{{$fieldTypeArray[$i][1]}}_from" <?php
                                if (isset($fieldTypeArray[$i][3])) {
                                    echo $fieldTypeArray[$i][3];
                                }
                                ?>  value="" class="form-control filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_from">

                                        </div>
                                        <div class="col-xs-2 date_filter_div">
                                            <label class="filter_label">{{$fieldTypeArray[$i][2]}} To</label>
                                            <input type="text" data-attr="date" name="{{$fieldTypeArray[$i][1]}}_to" <?php
                                if (isset($fieldTypeArray[$i][3])) {
                                    echo $fieldTypeArray[$i][3];
                                }
                                        ?>  value="" class="form-control filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_to">

                                        </div>
                                        <?php

                                    } else if ($fieldTypeArray[$i][0] == 'combo') {
                                        ?>
                                        <div class="col-xs-2 date_filter_div">
                                            <label class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                            {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control filters ','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1], 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                        </div>
                                        <?php
                                    } else if ($fieldTypeArray[$i][0] == 'multi_combo') {
                                        ?>
                                        <div class="col-xs-2 date_filter_div">
                                            <label style="width: 100%;"class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                            {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control filters multiple_selectbox','multiple'=>'multiple','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1], 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                        </div>
                                        <?php
                                    }
                                    $i++;

                                }
                                ?>

                                <div class="col-xs-4" style="float:right;padding: 0px;padding-top: 25px;">
                                    <div class="clearfix"></div>
                                    <a class="btn light_purple_bg" onclick="getDynamicReportData();"  name="search_results" id="search_results">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                        Search
                                    </a>
                                    <a class="btn light_purple_bg disabled" onclick="showDynamicReportDataGraph();"  name="search_results" id="btn_graph">
                                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                                    </a>
                                    <!--                    <a class="btn light_purple_bg" name="print_results" id="print_results">
                                                            <i class="fa fa-print" aria-hidden="true"></i>
                                                            Print
                                                        </a>-->
                                    <a class="btn light_purple_bg" data-toggle="modal" data-target="#favouriteModal" name="add_to_favourites" id="add_to_favourites">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        Add To Favorites
                                    </a>
                                    <a class="btn light_purple_bg" name="report_list_btn" id="report_list_btn" onclick="getReportsList();">
                                        <i class="fa fa-list" aria-hidden="true"></i>
                                        Reports List
                                    </a>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-----google chart---------------------------->
            <div class="box no-border no-margin" id="wdr-graph_component" style="display:none" >
            </div>
                 <div id="googlechart-container" style="height:175px;display:none"></div>

            <div class="box no-border no-margin">
                <div class="" style="margin-bottom: 8px;">

                    <div id="wdr-component" class="webDataRocksClass" style="">

                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>

    {!! Form::token() !!} {!! Form::close() !!}
</section>
<!-- Modal popup favourite -->
<div id="favouriteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add to Favorite</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                            <div class="col-md-12">
                                <label for="">Report Name</label>
                                <div class="clearfix"></div>
                                <input type="text" name="report_name" id="report_name" class="form-control">
                            </div>
                </div>
                <div class="row" style="margin-top:15px;">
                    <div class="col-md-12">
                        <label>Access Given To</label>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="radio radio-primary radio-inline" id="">
                                <input name="favourite_type" id="favourite_public" value="1" checked="" type="radio">
                                <label class="no-margin" for="favourite_public">
                                    Public
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="radio radio-primary radio-inline" id="">
                                <input name="favourite_type" id="favourite_rolewise" value="2" checked="" type="radio">
                                <label class="no-margin" for="favourite_rolewise">
                                    Roles
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3alter3">
                            <div class="radio radio-primary radio-inline" id="">
                                <input name="favourite_type" id="favourite_userwise" value="3" checked="" type="radio">
                                <label class="no-margin" for="favourite_userwise">
                                    Users
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top:15px;display: none" id="favourite_user_role_section">
                    <div class="col-md-12">
                        <label><i class="fa fa-tasks" aria-hidden="true"></i> Select User Roles</label>
                    </div>
                    <?php $roles = \DB::table('role')->where('status',1)->where('default_user_id','=','0')->pluck('name','id'); ?>
                    <div class="col-md-12">
                        {!! Form::select('role[]',$roles, null , ['class' => 'form-control selectsearch','id' => 'user_roles', 'style' => 'color:#555555; height:50px !important; padding:4px 12px;', 'multiple'=>'multiple']) !!}
                         <span style="color: #d14;"> {{ $errors->first('role') }}</span>
                    </div>
                </div>
                <div class="row" style="margin-top:15px; display: none" id="favourite_user_section">
                    <div class="col-md-12">
                        <label><i class="fa fa-user" aria-hidden="true"></i> Select Users</label>
                    </div>
                    <?php $users = \DB::table('users')->where('status',1)->where('deleted_at',Null)->pluck('name','id'); ?>
                    <div class="col-md-12">
                        {!! Form::select('favourite_users[]',$users, null , ['class' => 'form-control selectsearch','id' => 'favourite_users', 'style' => 'color:#555555; height:50px !important; padding:4px 12px;', 'multiple'=>'multiple']) !!}
                         <span style="color: #d14;"> {{ $errors->first('users') }}</span>
                    </div>
                </div>




            </div>
            <div class="modal-footer">
                <button style="margin: 0 5px 0 0;" data-dismiss="modal" class="btn light_purple_bg"><i class="fa fa-times"></i> Cancel</button>
                <button type="button" class="btn light_purple_bg" onclick="saveAddToFavourite();"><i class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
</div>

<!----modal popup favorite report list---------->
<div id="reportListModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reports List</h4>
            </div>
            <div class="modal-body" id='report_list_data'>


            </div>
        </div>
    </div>
</div>
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/DynamicReportBilling.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

@endsection

