@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">

@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <input type="hidden" id="hidden_filetoken" value="<?= csrf_token() ?>">
        <input type="hidden" id="base_url" value="{{URL::To('/')}}">
        <input type="hidden" id="edit" value="">
        <input type="hidden" id="date_formatt" value="{{ config('hrm.default_date_format_js') }}">
        <div class="row" style="margin-bottom: 10px"> </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <h4 class="blue"><strong> <?= $title ?> </strong></h4>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-9 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix" style="height:70px">
                        <div class="col-md-12 padding_sm">


                            <div class="col-md-4 padding_sm" style="margin-top:10px;">
                                <div class="mate-input-box">
                                    <label for="">Uom Name/Code</label>
                                    <div class="clearfix"></div>
                                    <input type="text" onkeyup=" " autocomplete="off" class="form-control hidden_search"
                                        id="uom" value="">
                                    <input type="hidden" value="" id="uom_hidden">
                                    <div class="ajaxSearchBox" id="uomAjaxDiv"
                                        style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999px;
                                                                                                                position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-1 pull-right  padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" onclick="search()" id="searchBtn" title="Search"
                                    class="btn btn-block btn-primary"><i id="searchSpin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 padding_sm">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div id="searchDataDiv"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3 padding_sm">
                <div class="box no-border">
                    <input type="hidden" name="hidden_id" id="hidden_id" value="">
                    <div class="box-body clearfix" style="height: 627px">
                        <h5 class="green text-center"><strong> Add Uom </strong></h5>
                        <hr>
                        <div class="col-md-12 padding_sm">
                            <input type="hidden" name="comp_hidden_id" id="comp_hidden_id" value="">
                            <div class="col-md-12 padding_sm" style="margin-top:10px;">
                                <div class="mate-input-box">
                                    <label for="">Uom Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" onkeyup=" " autocomplete="off" class="form-control "
                                        id="uom_save" value="">
                                    
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">In Active</option>
                                    </select>
                                    <div class="clearfix"></div>

                                </div>
                            </div>

                    </div>
                        <div class="col-md-12 padding_sm" style="margin-top:10px;">
                            <div class="col-md-4 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <button type="button" id="savebtnBtn" class="btn btn-block btn-warning" title="Save"
                                    onclick="saveuom()"><i id="savebtnSpin" class="fa fa-save"></i> Save
                                </button>
                            </div>
                            <div class="col-md-4 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-info clearFormButton" title="Clear"
                                    onclick="formReset()"><i class="fa fa-times"></i> Clear </button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>



    </div>
    </div>
    </div>





    <!-- /page content -->

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/js/uommaster.js")}}"></script>
@endsection
