@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <style>
        .hoverclass:hover {
            background-color: rgb(230, 221, 221);
        }

        footer {
            min-width: 100%;
        }

    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">

    <input type="hidden" value='<?= $route_data ?>' id="route_value">
    <div class="right_col" style="min-width: 110% !important;">
        <div class="container-fluid">



            <div class="col-md-12 paddding_sm">
                <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
                    <div class="box no-border no-margin">
                        <div class="box-body" style="padding-bottom:15px;">
                            <table class="table table-contensed table_sm" style="margin-bottom:10px;">


                                <thead>
                                    <tr class="table_header_bg">
                                        <th colspan="11">
                                            Bill Paid Or Not Paid Report
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::open(['name' => 'search_from', 'id' => 'search_from']) !!}
                                    <div>
                                        <?php
                            $i = 1;

                            foreach ($showSearchFields as $fieldType => $fieldName) {
                                $fieldTypeArray[$i] = explode('-', $fieldType);

                                if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                    $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                    ?>

                                        <div class="col-md-12">
                                            <div class="mate-input-box">
                                                <label class="filter_label">{{ $fieldTypeArray[$i][2] }}</label>
                                                <input class="form-control hidden_search" value="" autocomplete="off"
                                                    type="text" id="{{ $fieldTypeArray[$i][1] }}"
                                                    name="{{ $fieldTypeArray[$i][1] }}" />
                                                <div id="{{ $fieldTypeArray[$i][1] }}AjaxDiv" class="ajamdearchBox">
                                                </div>
                                                <input class="filters"
                                                    value="{{ $hiddenFields[$hidden_filed_id] }}" type="hidden"
                                                    name="{{ $fieldTypeArray[$i][1] }}_hidden" value=""
                                                    id="{{ $fieldTypeArray[$i][1] }}_hidden">
                                            </div>
                                        </div>

                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'text') {
                                    ?>
                                        <div class="col-md-12">
                                            <label class="filter_label">{{ $fieldTypeArray[$i][2] }}</label>
                                            <input type="text" name="{{ $fieldTypeArray[$i][1] }}" <?php if (isset($fieldTypeArray[$i][3])) {
    echo $fieldTypeArray[$i][3];
} ?>
                                                value="" class="form-control filters" id="{{ $fieldTypeArray[$i][1] }}"
                                                data="{{ isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : '' }}"
                                                <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?>>
                                        </div>
                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'date') {

                                    ?>
                                        <div class="col-md-2 date_filter_div">
                                            <div class="mate-input-box">
                                                <label class="filter_label">{{ $fieldTypeArray[$i][2] }} From</label>
                                                <input type="text" data-attr="date" autocomplete="off"
                                                    name="{{ $fieldTypeArray[$i][1] }}_from" <?php
if (isset($fieldTypeArray[$i][3])) {
    echo $fieldTypeArray[$i][3];
}
?>
                                                    value="{{ date('M-d-Y') }}" class="form-control datepicker filters"
                                                    placeholder="YYYY-MM-DD" id="{{ $fieldTypeArray[$i][1] }}_from">
                                            </div>
                                        </div>
                                        <div class="col-md-2 date_filter_div">
                                            <div class="mate-input-box">
                                                <label class="filter_label">{{ $fieldTypeArray[$i][2] }} To</label>
                                                <input type="text" data-attr="date" autocomplete="off"
                                                    name="{{ $fieldTypeArray[$i][1] }}_to" <?php
if (isset($fieldTypeArray[$i][3])) {
    echo $fieldTypeArray[$i][3];
}
?>
                                                    value="{{ date('M-d-Y') }}" class="form-control datepicker filters"
                                                    placeholder="YYYY-MM-DD" id="{{ $fieldTypeArray[$i][1] }}_to">
                                            </div>
                                        </div>
                                        <?php

                                } else if ($fieldTypeArray[$i][0] == 'combo') {
                                    ?>
                                        <div class="col-md-3 date_filter_div">
                                            <label class="filter_label ">{{ $fieldTypeArray[$i][2] }}</label>
                                            {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control select2  filters datapicker ', 'placeholder' => $fieldTypeArray[$i][2], 'id' => $fieldTypeArray[$i][1]]) !!}
                                        </div>
                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'multi_combo') {
                                    ?>
                                        <div class="col-md-4 padding_sm">
                                            <div class="mate-input-box">
                                                <label class="filter_label"
                                                    style="width: 100%;">{{ $fieldTypeArray[$i][2] }}</label>
                                                <div class="clearfix"></div>
                                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control select2 filters', 'multiple' => 'multiple-select', 'placeholder' => ' ', 'id' => $fieldTypeArray[$i][1], 'style' => 'width:100%;color:#555555; padding:2px 12px;']) !!}
                                            </div>

                                        </div>
                                        <?php
                                }
                                $i++;

                            }
                            ?>

                                        <div class="clearfix"></div>
                                        <div class="col-md-2 padding_sm pull-right">

                                        </div>
                                        <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                            <a class="btn light_purple_bg btn-block" onclick="BillPaidOrNotPaidData();"
                                                name="search_results" id="search_results">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                                Search
                                            </a>
                                        </div>

                                        <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                            <a class="btn light_purple_bg disabled btn-block" onclick="exceller();"
                                                name="csv_results" id="csv_results">
                                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                Excel
                                            </a>
                                        </div>
                                        <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                            <a class="btn light_purple_bg disabled btn-block" onclick="printReportData();"
                                                name="print_results" id="print_results">
                                                <i class="fa fa-print" aria-hidden="true"></i>
                                                Print
                                            </a>
                                        </div>
                                        <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                            <a class="btn light_purple_bg btn-block" onclick="search_clear();"
                                                name="clear_results" id="clear_results">
                                                <i class="fa fa-refresh" aria-hidden="true"></i>
                                                Reset
                                            </a>
                                        </div>


                                        {!! Form::token() !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12 padding_sm">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #CCC; border: 2px solid #FFF;min-height: 34px;"
                        id="ResultsViewArea">
                    </div>
                </div>
            </div>






        </div>
    </div>

    <!-------print modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" id="printMode" value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" checked="true" type="radio" name="printMode"
                            id="printMode" value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!----------detail modal----------------------->
    <div class="modal" tabindex="-1" role="dialog" id="detail_modal">
        <div class="modal-dialog" role="document" style="width: 85%;">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body theadscroll" id="bill_paid_detail_modal_data"
                    style="position: relative;height:520px;">

                </div>
            </div>
        </div>
    </div>




@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>

    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/reports/default/javascript/bill_paid_notpaid.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
@endsection
