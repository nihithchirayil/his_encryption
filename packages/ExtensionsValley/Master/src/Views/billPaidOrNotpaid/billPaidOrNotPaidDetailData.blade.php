<table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper table-striped'
    style="font-size: 12px;">
    <thead>
        <tr style="background-color:rgb(91 110 91);color:white;">
            <td><b>SI.No</b></td>
            <td><b>UHID.</b></td>
            <td><b>Patient Name</b></td>
            <td><b>Bill No.</b></td>
            <td><b>Bill Date</b></td>
            <td><b>Paid Cash</b></td>
            <td><b>Paid Cash Debit</b></td>
            <td><b>Not Paid Cash</b></td>
            <td><b>Not Paid Credit</b></td>
            <td><b>Total Bill</b></td>
        </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
            $tot_paid_cash = 0;
            $tot_paid_cash_debit = 0;
            $tot_not_paid_cash = 0;
            $tot_not_paid_credit = 0;
            $tot_tot_bill = 0;
            $cls = '';
        @endphp
        @foreach ($res as $data)
            @if ($data->paid_cash != 0)
                @php
                    $cls = 'background-color: #82ada9';
                @endphp
            @elseif($data->paid_cash_debit != 0)
                @php
                    $cls = 'background-color: #8182a9';
                @endphp
            @elseif($data->not_paid_cash != 0)
                @php
                    $cls = 'background-color: #aab389';
                @endphp
            @elseif($data->not_paid_credit != 0)
                @php
                    $cls = 'background-color: #ebc267';
                @endphp
            @endif
            <tr style="{{ $cls }}">
                <td class="common_td_rules">{{ $i }}</td>
                <td class="common_td_rules">{{ $data->opnumber }}</td>
                <td class="common_td_rules">{{ $data->patientname }}</td>
                <td class="common_td_rules">{{ $data->bill_no }}</td>
                <td class="common_td_rules">{{ date('M-d-Y h:i A', strtotime($data->bill_date)) }}</td>
                <td class="td_common_numeric_rules">{{ $data->paid_cash }}</td>
                <td class="td_common_numeric_rules">{{ $data->paid_cash_debit }}</td>
                <td class="td_common_numeric_rules">{{ $data->not_paid_cash }}</td>
                <td class="td_common_numeric_rules">{{ $data->not_paid_credit }}</td>
                <td class="td_common_numeric_rules">{{ $data->tot_bill }}</td>
            </tr>
            @php
                $i++;
                $tot_paid_cash = $tot_paid_cash + $data->paid_cash;
                $tot_paid_cash_debit = $tot_paid_cash_debit + $data->paid_cash_debit;
                $tot_not_paid_cash = $tot_not_paid_cash + $data->not_paid_cash;
                $tot_not_paid_credit = $tot_not_paid_credit + $data->not_paid_credit;
                $tot_tot_bill = $tot_tot_bill + $data->tot_bill;
            @endphp
        @endforeach
        <tr style="background-color:lightblue;">
            <td class="common_td_rules" colspan="5"><b>Total</b></td>
            <td class="td_common_numeric_rules"><b>{{ $tot_paid_cash }}</b></td>
            <td class="td_common_numeric_rules"><b>{{ $tot_paid_cash_debit }}</b></td>
            <td class="td_common_numeric_rules"><b>{{ $tot_not_paid_cash }}</b></td>
            <td class="td_common_numeric_rules"><b>{{ $tot_not_paid_credit }}</b></td>
            <td class="td_common_numeric_rules"><b>{{ $tot_tot_bill }}</b></td>
        </tr>
    </tbody>
</table>
<div class="col-md-12" style="margin-top:-15px;">
    <div class="col-md-3">
        <h6><span class="badge" style="background-color:#82ada9">&nbsp;&nbsp;</span> Bill Collection</h6>
    </div>
    <div class="col-md-3">
        <h6><span class="badge" style="background-color:#8182a9">&nbsp;&nbsp;</span> Debit Collection</h6>
    </div>
    <div class="col-md-3">
        <h6><span class="badge" style="background-color:#aab389">&nbsp;&nbsp;</span> Not Paid Cash</h6>
    </div>
    <div class="col-md-3">
        <h6><span class="badge" style="background-color:#ebc267">&nbsp;&nbsp;</span> Not Paid Credit</h6>
    </div>
</div>
<br>
