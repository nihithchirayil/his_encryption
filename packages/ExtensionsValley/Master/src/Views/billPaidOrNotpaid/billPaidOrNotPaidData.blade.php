<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Bill Paid Or Not Paid Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="2%;">SI.No</th>
                        <th width="10%;">Bill Name</th>
                        <th width="3%;">Total Count</th>
                        <th width="3%;">Paid Count</th>
                        <th width="3%;">Paid Cash</th>
                        <th width="3%;">Debit Collection</th>
                        <th width="3%;">Todays Billed Collection</th>
                        <th width="3%;">Total Bill</th>
                        <th width="3%;">Total Cash Collected</th>
                        <th width="3%;">Total Discount</th>
                        <th width="3%;">Not Paid Count</th>
                        <th width="3%;">Not Paid Cash</th>
                        <th width="3%;">Not Paid Credit</th>
                        <th width="3%;">Insurance Amount</th>
                        <th width="3%;">ESI Amount</th>
                        <th width="3%;">ECHS Cash</th>
                        <th width="3%;">Total Insuance</th>
                        <th width="3%;">Pharmacy Refund</th>
                        <th width="3%;">Service Refund</th>
                        <th width="3%;">Reg Refund</th>
                    </tr>
                </thead>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
                        $i = 1;
                        $SUM_TOTALCOUNT = 0.0;
                        $SUM_PAIDCOUNT = 0.0;
                        $SUM_NOTPAIDCOUNT = 0.0;
                        $SUM_INSURANCEAMT = 0.0;
                        $SUM_ESIAMT = 0.0;
                        $SUM_ECHSAMT = 0.0;
                        $SUM_TOTALINSURANCE = 0.0;
                        $SUM_PAIDCASH = 0.0;
                        $SUM_PAIDCASHPOST = 0.0;
                        $SUM_NOTPAIDCREDIT = 0.0;
                        $SUM_NOTPAIDCASH = 0.0;
                        $SUM_DEBITCOLLECTION = 0.0;
                        $SUM_TOTALBILL = 0.0;
                        $SUM_TOTALDISCOUNT = 0.0;
                        $SUM_PHARMACYREFUND = 0.0;
                        $SUM_SERVICEREFUND = 0.0;
                        $SUM_REGISTRATIONREFUND = 0.0;
                        $SUM_TOTALCASHCOLLECTED = 0.0;

          foreach ($res as $data){
                ?>

                        <tr onclick="getDetailData('{{ $data->bill_code }}','{{ $data->BILLNAME }}')"
                            class="hoverclass" title="{{ $data->BILLNAME }}">
                            <td class="common_td_rules">{{ $i }}</td>
                            <td class="common_td_rules">{{ $data->BILLNAME }}</td>
                            <td class="td_common_numeric_rules">{{ $data->TOTALCOUNT }}</td>
                            <td class="td_common_numeric_rules">{{ $data->PAIDCOUNT }}</td>
                            <td class="td_common_numeric_rules">{{ $data->PAIDCASH }}</td>
                            <td class="td_common_numeric_rules">{{ $data->DEBITCOLLECTION }}</td>
                            <td class="td_common_numeric_rules">{{ $data->PAIDCASHPOST }}</td>
                            <td class="td_common_numeric_rules">{{ $data->TOTALBILL }}</td>
                            <td class="td_common_numeric_rules">{{ $data->TOTALCASHCOLLECTED }}</td>
                            <td class="td_common_numeric_rules">{{ $data->TOTALDISCOUNT }}</td>
                            <td class="td_common_numeric_rules">{{ $data->NOTPAIDCOUNT }}</td>
                            <td class="td_common_numeric_rules">{{ $data->NOTPAIDCASH }}</td>
                            <td class="td_common_numeric_rules">{{ $data->NOTPAIDCREDIT }}</td>
                            <td class="td_common_numeric_rules">{{ $data->INSURANCEAMT }}</td>
                            <td class="td_common_numeric_rules">{{ $data->ESIAMT }}</td>
                            <td class="td_common_numeric_rules">{{ $data->ECHSAMT }}</td>
                            <td class="td_common_numeric_rules">
                                {{ $data->ESIAMT + $data->ECHSAMT + $data->INSURANCEAMT }}</td>
                            <td class="td_common_numeric_rules">{{ $data->PHARMACYREFUND }}</td>
                            <td class="td_common_numeric_rules">{{ $data->SERVICEREFUND }}</td>
                            <td class="td_common_numeric_rules">{{ $data->REGISTRATIONREFUND }}</td>
                        </tr>
                        <?php
                        $SUM_TOTALCOUNT = $SUM_TOTALCOUNT+$data->TOTALCOUNT;
                        $SUM_PAIDCOUNT = $SUM_PAIDCOUNT+$data->PAIDCOUNT;
                        $SUM_NOTPAIDCOUNT = $SUM_NOTPAIDCOUNT+$data->NOTPAIDCOUNT;
                        $SUM_INSURANCEAMT = $SUM_INSURANCEAMT+$data->INSURANCEAMT;
                        $SUM_ESIAMT = $SUM_ESIAMT+$data->ESIAMT;
                        $SUM_ECHSAMT = $SUM_ECHSAMT+$data->ECHSAMT;
                        $SUM_PAIDCASH = $SUM_PAIDCASH+$data->PAIDCASH;
                        $SUM_PAIDCASHPOST = $SUM_PAIDCASHPOST+$data->PAIDCASHPOST;
                        $SUM_NOTPAIDCREDIT = $SUM_NOTPAIDCREDIT+$data->NOTPAIDCREDIT;
                        $SUM_NOTPAIDCASH = $SUM_NOTPAIDCASH+$data->NOTPAIDCASH;
                        $SUM_DEBITCOLLECTION = $SUM_DEBITCOLLECTION+$data->DEBITCOLLECTION;
                        $SUM_TOTALBILL = $SUM_TOTALBILL+$data->TOTALBILL;
                        $SUM_TOTALDISCOUNT = $SUM_TOTALDISCOUNT+$data->TOTALDISCOUNT;
                        $SUM_PHARMACYREFUND = $SUM_PHARMACYREFUND+$data->PHARMACYREFUND;
                        $SUM_SERVICEREFUND = $SUM_SERVICEREFUND+$data->SERVICEREFUND;
                        $SUM_REGISTRATIONREFUND = $SUM_REGISTRATIONREFUND+$data->REGISTRATIONREFUND;
                        $SUM_TOTALCASHCOLLECTED = $SUM_TOTALCASHCOLLECTED+$data->TOTALCASHCOLLECTED;
                        $SUM_TOTALINSURANCE = $SUM_TOTALINSURANCE+$data->ESIAMT + $data->ECHSAMT + $data->INSURANCEAMT;
                        $i++;
            }

          ?>
                        <tr style="font-weight:700;background-color:rgb(91 110 91);color:white;">
                            <td colspan="2" class="common_td_rules">Total</td>
                            <td class="td_common_numeric_rules">{{ $SUM_TOTALCOUNT }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_PAIDCOUNT }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_PAIDCASH }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_DEBITCOLLECTION }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_PAIDCASHPOST }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_TOTALBILL }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_TOTALCASHCOLLECTED }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_TOTALDISCOUNT }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_NOTPAIDCOUNT }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_NOTPAIDCASH }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_NOTPAIDCREDIT }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_INSURANCEAMT }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_ESIAMT }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_ECHSAMT }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_TOTALINSURANCE }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_PHARMACYREFUND }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_SERVICEREFUND }}</td>
                            <td class="td_common_numeric_rules">{{ $SUM_REGISTRATIONREFUND }}</td>

                        </tr>

                    </tbody>
                @else
                    <tr>
                        <td colspan="4" style="text-align: center;">No Results Found!</td>
                    </tr>
                @endif

            </table>
            </font>

            <!----Insurance Income--->
            <div class="row">
                <div class="col-md-8">
                    <table class='table table-condensed table_sm table-col-bordered'>
                        <thead style="background-color:rgb(91 110 91);color:white;">
                            <tr>
                                <td colspan="3">
                                    <h4>Insurance Income</h4>
                                </td>
                            </tr>
                            <tr>
                                <td>Hospital Income</td>
                                <td>Doctor Income</td>
                                <td>Total</td>
                            </tr>
                        </thead>
                        <tbody>
                            <td>{{ $res_insurance_income[0]->doc ?? '0.00' }}</td>
                            <td>{{ $res_insurance_income[0]->hosp ?? '0.00' }}</td>
                            <td>{{ $res_insurance_income[0]->pat_paid ?? '0.00' }}</td>
                        </tbody>
                    </table>
                </div>
            </div>



            <!----other income---->
            <br>
            <div class="row">
                <div class="col-md-8">
                    <table class='table table-condensed table_sm table-col-bordered'>
                        <thead style="background-color:rgb(91 110 91);color:white;">
                            <tr>
                                <td colspan="3">
                                    <h4>Other Income</h4>
                                </td>
                            </tr>
                            <tr>
                                <td>Account Head</td>
                                <td>Amount</td>
                            </tr>
                        </thead>
                        <tbody>
                            <td>&nbsp;0.00</td>
                            <td>&nbsp;0.00</td>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
