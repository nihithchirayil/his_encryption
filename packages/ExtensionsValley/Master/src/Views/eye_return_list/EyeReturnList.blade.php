@extends('Emr::emr.page')
@section('content-header')
    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-slide.css') }}" rel="stylesheet">

    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/common-custom.css') }}" rel="stylesheet">

    <!-- <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet"> -->
    {{-- ---- --}}
{{--
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-slide.css') }}" rel="stylesheet">


    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/master/default/css/bootstrap-timepicker.min.css') }}"
        rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
        rel="stylesheet"> --}}

    <style>
        .header_bg {
            background: rgb(37, 184, 155) !important;
            background: linear-gradient(90deg, rgba(37, 184, 155, 1) 0%, rgba(24, 147, 141, 1) 0%, rgba(23, 182, 197, 1) 100%) !important;
            color: white !important;
        }

        .gray_headeer_bg {
            background: rgb(37, 184, 155) !important;
            background: linear-gradient(90deg, rgba(37, 184, 155, 1) 0%, rgba(219, 219, 219, 1) 0%, rgba(174, 174, 174, 1) 100%) !important;
        }

        .box_header {
            background: #3b926a !important;
            /* background: #5397b1 !important; */
            color: #FFF !important;
        }

        .text-blue {
            color: steelblue !important;
        }

        .bottom-border-text {
            border: none !important;
            border-bottom: 1px solid lightgrey !important;
            box-shadow: none;
        }

        label {
            font-size: 13px !important;
        }

        .mate-input-box {
            height: 40px !important;

        }

        .ajaxSearchBox {
            margin-top: 11px !important;
        }

    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="service_return_flag" value="{{ $service_return_flag}}">
    <div class="right_col">
        <div class="col-md-12 no-padding  box-body table_header_common" style="height:25px !important;">
            <h6 style="margin-top: 5px;margin-left: 3px;"><b>{{ $title }}</b></h6>
        </div>
        <div class="col-md-12 box-body" style="margin-top:5px;">

            <div class="col-md-1 date_filter_div">
                <div class="mate-input-box">
                    <label class="filter_label">From Date</label>
                    <input type="text" data-attr="date" name="from_date" class="form-control datepicker"
                        placeholder="MMM-DD-YYYY" id="from_date" autocomplete="off" value="{{ date('M-d-Y') }}">
                </div>
            </div>
            <div class="col-md-1 date_filter_div">
                <div class="mate-input-box">
                    <label class="filter_label">To Date</label>
                    <input type="text" data-attr="date" name="to_date" class="form-control datepicker "
                        placeholder="MMM-DD-YYYY" id="to_date" autocomplete="off" value="{{ date('M-d-Y') }}">
                </div>
            </div>
            <div class="col-xs-2 padding_sm">

                <div class="mate-input-box">
                    <label class="filter_label">UHID/Patient name</label>
                    <input class="form-control hidden_search" value="" autocomplete="off" type="text" autocomplete="off"
                        id="patient_uhid" name="patient_uhid" />
                    <div id="patient_uhidAjaxDiv" class="ajaxSearchBox"></div>
                    <input class="filters" type="hidden" name="patient_uhid_hidden" value=""
                        id="patient_uhid_hidden">
                </div>


            </div>


            @if ($service_return_flag == 1)
                <div class="col-md-2 padding_sm">
                    <div class="mate-input-box">
                        <label class="filter_label">BillTag</label>
                        {!! Form::select('bill_tag', $bill_tag_list, 0, ['class' => 'form-control select2  ', 'id' => 'bill_tag', 'placeholder' => 'select', 'style' => '']) !!}
                    </div>
                </div>
            @endif
            <div class="col-md-2 padding_sm">
                <div class="mate-input-box">
                    <label class="filter_label" for="">Return No.</label>
                    <div class="clearfix"></div>
                    <input type="text" id="return_no" class="form-control " value="">
                </div>
                <div class="clearfix"></div>


            </div>
            <div class="col-md-2 padding_sm" style="margin-top: 11px;">
                <button type="button" class="btn btn-primary pull-right " name="search_return_bills"
                    id="search_return_bills">
                    <i class="fa fa-search"></i> Search
                </button>
                <button type="button" class="btn btn-default pull-right" name="reset_return_bills" id="reset_return_bills">
                    <i class="fa fa-refresh"></i> Refresh
                </button>
            </div>


            <div class="col-md-12 box-body" id="bill_return_list_container" style="margin-top:5px;height:387px">

            </div>
            <div class="col-md-12 box-body" style="height: 31px;margin-top: 2px;">

                <div class="col-md-6 padding_sm" style="margin:0px;">

                    <div class="col-md-6 paid-bg">
                        <div class="checkbox checkbox-success inline">

                            <input type="checkbox" id="paid_bills" name="paid_bills" class="checkit" value="1" />
                            <label for="paid_bills">Paid</label>&nbsp;&nbsp;
                        </div>
                    </div>
                    <div class="col-md-6 unpaid-bg">
                        <div class="checkbox checkbox-success inline">

                            <input type="checkbox" id="unpaid_bills" name="unpaid_bills" class="checkit" value="1" />
                            <label for="unpaid_bills">Unpaid</label>&nbsp;&nbsp;
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        {{-- <div class="checkbox checkbox-success inline">

                            <input type="checkbox" id="cancelled_bills" name="cancelled_bills" class="checkit"
                                value="1" />
                            <label for="cancelled_bills">Cancelled</label>&nbsp;&nbsp;
                        </div> --}}
                    </div>





                </div>

            </div>


        </div>


        <div id="return_detail_modal" class="modal fade" role="dialog" style="z-index:10000;">
            <div class="modal-dialog" style="width: 90%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header table_header_common modal-header-sm">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Return Bill Details</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" id="return_detail_content">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!----Print Modal----------------->
        <div id="return_print_modal" class="modal fade" role="dialog" style="z-index:10000;">
            <div class="modal-dialog" style="width: 90%;">
                <!-- Modal content-->
                <div class="modal-content" style="min-height:560px;">
                    <div class="modal-header table_header_common modal-header-sm">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Return Bill Print</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" id="return_print_content">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" onclick="print_generate();" class="btn bg-green pull-right">
                            <i class="fa fa-print"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>

        @include('Master::eye_bill_list.cash_return_model')
    </div>

    @stop
    @section('javascript_extra')

        <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('packages/extensionsvalley/emr/js/eye_cash_return.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.datepicker').datetimepicker({
                    format: 'MMM-DD-YYYY'
                });
                $('#search_return_bills').click();
            });

            $('#reset_return_bills').click(function() {
                var current_date = $('#current_date').val();
                $('#from_date').val(current_date);
                $('#to_date').val(current_date);
                $('#patient_uhid_hidden').val('');
                $('#patient_uhid').val('');
                $('#return_no').val('');
                $('.checkit').prop('checked', false);
                $('#search_return_bills').click();
                if($('#bill_tag').is(':visible')){
                   $('#bill_tag').val('');
                }

            })
            $('.checkit').click(function() {
                $('#search_return_bills').click();
            })
            $('#search_return_bills').click(function() {
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var service_return_flag = $('#service_return_flag').val();
                var patient_id = $('#patient_uhid_hidden').val();
                if($('#bill_tag').is(':visible')){
                    var bill_tag = $('#bill_tag').val();
                }else{
                    var bill_tag = 'PH';
                }

                var return_no = $('#return_no').val();
                var cash_card = $('#cash_card').is(':checked') ? 1 : 0;
                var credit = $('#credit').is(':checked') ? 1 : 0;
                var insurance = $('#insurance').is(':checked') ? 1 : 0;
                var company_credit = $('#company_credit').is(':checked') ? 1 : 0;
                var paid_bills = $('#paid_bills').is(':checked') ? 1 : 0;
                var unpaid_bills = $('#unpaid_bills').is(':checked') ? 1 : 0;
                var cancelled_bills = $('#cancelled_bills').is(':checked') ? 1 : 0;
                var token = $('#c_token').val();

                var url = "{{ route('extensionsvalley.eye_return_list.eyeReturnSearch') }}";
                var data = {
                    _token: token,
                    service_return_flag:service_return_flag,
                    from_date: from_date,
                    to_date: to_date,
                    patient_id: patient_id,
                    cash_card: cash_card,
                    credit: credit,
                    bill_tag: bill_tag,
                    insurance: insurance,
                    company_credit: company_credit,
                    paid_bills: paid_bills,
                    unpaid_bills: unpaid_bills,
                    cancelled_bills: cancelled_bills,
                    return_no: return_no
                };
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        $('#bill_return_list_container').html(data);
                    },
                    complete: function() {
                        $("body").LoadingOverlay("hide");
                        $('.theadscroll').perfectScrollbar({
                            minScrollbarLength: 30
                        });

                        var $table = $('table.theadfix_wrapper');
                        $table.floatThead({
                            scrollContainer: function($table) {
                                return $table.closest('.theadscroll');
                            }
                        });
                    }
                });
            });

            function print_generate() {

                $('.theadscroll').perfectScrollbar('destroy');
                $('#total_row').css('display', '');
                //$('#print_config_modal').modal('hide');
                var printMode = $('input[name=printMode]:checked').val();
                //alert('dddd'); return;
                var showw = "";




                var mywindow = window.open('', 'my div', 'height=3508,width=2480');

                var msglist = document.getElementById('return_print_content');
                showw = showw + msglist.innerHTML;



                if (printMode == 1) {
                    mywindow.document.write('<style>@page{size:portrait; position: absolute;margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
                } else {
                    mywindow.document.write('<style>@page{size:landscape; position: absolute; margin-top: 80px; margin-bottom: 80px; margin-right: 20px; margin-left: 20px; overflow: auto;}</style>');
                }

                // mywindow.document.write(
                //     '<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>'
                // );
                mywindow.document.write(
                    '<style>.table{border: 1px solid #e8e8e8 !important;color:#3b3d3d !important;}</style>');
                mywindow.document.write(
                    '<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>'
                );
                // mywindow.document.write(
                //     '<style>thead{background-color:#02967f !important;color:white !important;word-wrap:break-word !important;}td{word-wrap:break-word !important;padding-left:5px !important;padding-right:3px !important;}</style>'
                // );
                mywindow.document.write(
                    '<style> tr:nth-child(even) td {background-color:#f2f4f5 !important;-webkit-print-color-adjust: exact;} tbody></style>'
                );

                mywindow.document.write(showw);

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10

                setTimeout(function() {
                    mywindow.print();
                    mywindow.close();
                    InitTheadScroll();
                    $('#total_row').css('display', 'none');
                }, 1000);


                return true;
                }

            function PrintCashReturn(return_id,return_no,return_type){
                var location_id = localStorage.getItem('location_id') ? localStorage.getItem('location_id') : 0;
                var location_name = localStorage.getItem('location_name') ? localStorage.getItem('location_name') : 0;
                var url = "{{ route('extensionsvalley.eyebillreturn.print_bill_detail') }}";
                var data = {
                    return_id: return_id,
                    return_no: return_no,
                    printMode: '1',
                    include_hospital_header: '1',
                    location_name:location_name,
                    location_id:location_id,
                    bill_tag:return_type

                };
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        if(parseInt(data) == 3 ){
                            toastr.success('Bill print success.');
                        } else {
                            $('#return_print_content').html(data);
                            $('#return_print_modal').modal('show');
                        }

                        // $('#return_detail_modal').modal('show');
                    },
                    complete: function() {
                        $("body").LoadingOverlay("hide");
                    }
                });
            }

            function selectDetailData(return_id) {
                var token = $('#c_token').val();
                var service_return_flag = $('#service_return_flag').val();
                var url = "{{ route('extensionsvalley.eye_return_list.eyeReturnDetail') }}";
                var data = {
                    _token: token,
                    return_id: return_id,
                    service_return_flag:service_return_flag
                };
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    beforeSend: function() {
                        $('#return_detail_modal').modal('show');
                        $("#return_detail_content").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        $('#return_detail_content').html(data);
                    },
                    complete: function() {
                        $("#return_detail_content").LoadingOverlay("hide");

                        resetBootstrapTable();
                    }
                });
            }


            //----Hidden Filed Search--------------------------------------------------------------------

            $('.hidden_search').keyup(function(event) {
                var input_id = '';
                var base_url = $('#base_url').val();
                var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
                var value = event.key; //get the charcode and convert to char
                input_id = $(this).attr('id');

                var current;
                if (value.match(keycheck) || event.keyCode == '8') {
                    if ($('#' + input_id + '_hidden').val() != "") {
                        $('#' + input_id + '_hidden').val('');
                    }
                    var search_key = $(this).val();
                    search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
                    search_key = search_key.trim();

                    var department_hidden = $('#department_hidden').val();
                    var datastring = '';
                    if (input_id == 'sub_department') {
                        datastring = '&department_id=' + department_hidden;
                    }
                    if (input_id == 'scheme') {
                        datastring = '&company_id=' + company_hidden;
                    }

                    if (search_key == "") {
                        $("#" + input_id + "AjaxDiv").html("");
                        $("#" + input_id + "AjaxDiv").hide();

                    } else {
                        var url = base_url + '/flexy/FlexyReportProgressiveSearch';
                        $.ajax({
                            type: "GET",
                            url: url,
                            data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                            beforeSend: function() {

                                $("#" + input_id + "AjaxDiv").html(
                                    '<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>'
                                ).show();
                            },
                            success: function(html) {

                                $("#" + input_id + "AjaxDiv").html(html).show();
                                $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                            },
                            complete: function() {

                            },
                            error: function() {
                                bootbox.alert('nw error');
                                return;
                            }

                        });
                    }
                } else {
                    ajax_list_key_down(input_id + 'AjaxDiv', event);
                }
            });

            function fillSearchDetials(id, name, serach_key_id) {
                $('#' + serach_key_id + '_hidden').val(id);
                $('#' + serach_key_id).val(name);
                $('#' + serach_key_id).attr('title', name);
                $("#" + serach_key_id + "AjaxDiv").hide();
            }


            /* setting for enter key press in ajaxDiv listing */
            $(".hidden_search").on('keydown', function(event) {
                var input_id = '';
                input_id = $(this).attr('id');
                if (event.keyCode === 13) {
                    ajaxlistenter(input_id + 'AjaxDiv');
                    return false;
                }
            });
        </script>
        <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    @endsection
