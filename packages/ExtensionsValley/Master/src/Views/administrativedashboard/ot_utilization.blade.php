<div class="box no-border no-margin">
    <div class="box-body clearfix">
        <div class="theadscroll" style="position: relative; height: 350px;">
            <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                style="border: 1px solid #CCC;">
                <thead>
                    <tr style="cursor: pointer;background: #01987a;color:white;">
                        <th>Patient</th>
                        <th>Uhid</th>
                        <th>Surgery</th>
                        <th>Surgeon</th>
                        <th>Anaesthetist</th>
                        <th>Theater</th>
                        <th>Start Time</th>
                        <th>Stop Time</th>
                    </tr>
                </thead>
                <tbody>

                    @if (count($item) > 0)
                        @foreach ($item as $item)

                            <tr style="cursor: pointer;">

                                <td class="common_td_rules">{{ $item->patient_name }}<input type="hidden"
                                        class="fav_check" name="patient_name[]" value="{{ $item->id }}"><input
                                        type="hidden" class="form-control" name="patient_name[]"
                                        value="{{ $item->patient_name }}">
                                </td>
                                <td class="common_td_rules">{{ $item->uhid }}<input type="hidden" class="fav_check"
                                        name="uhid[]" value="{{ $item->id }}"><input type="hidden"
                                        class="form-control" name="uhid[]" value="{{ $item->uhid }}">
                                </td>

                                <td class="common_td_rules">{{ $item->surgery }}<input type="hidden"
                                        class="fav_check" name="surgery_id[]" value="{{ $item->id }}"><input
                                        type="hidden" class="form-control" name="surgery_name[]"
                                        value="{{ $item->surgery }}">
                                </td>
                                <td class="common_td_rules">{{ $item->doctor_name }}<input type="hidden"
                                        class="fav_check" name="doctor_name[]" value="{{ $item->id }}"><input
                                        type="hidden" class="form-control" name="doctor_name[]"
                                        value="{{ $item->doctor_name }}">
                                </td>
                                <td class="common_td_rules">{{ $item->anaesthetist }}<input type="hidden"
                                        class="fav_check" name="anaesthetist[]" value="{{ $item->id }}"><input
                                        type="hidden" class="form-control" name="anaesthetist[]"
                                        value="{{ $item->anaesthetist }}">
                                </td>
                                <td class="common_td_rules">{{ $item->theater }}<input type="hidden"
                                        class="fav_check" name="theater_name[]" value="{{ $item->id }}"><input
                                        type="hidden" class="form-control" name="theater[]"
                                        value="{{ $item->theater }}">
                                </td>
                                <td class="common_td_rules">
                                    {{ date('M-d-Y h:i A', strtotime($item->start_time)) }}<input type="hidden"
                                        class="form-control" name="fav_start_time[]"
                                        value="{{ date('h:i A', strtotime($item->start_time)) }}"><input
                                        type="hidden" class="form-control" name="fav_start_date[]"
                                        value="{{ date('M-d-Y', strtotime($item->start_time)) }}">
                                </td>
                                <td class="common_td_rules">
                                    {{ date('M-d-Y h:i A', strtotime($item->stop_time)) }}<input type="hidden"
                                        class="form-control" name="fav_stop_time[]"
                                        value="{{ date('h:i A', strtotime($item->stop_time)) }}"><input type="hidden"
                                        class="form-control" name="fav_stop_date[]"
                                        value="{{ date('M-d-Y', strtotime($item->stop_time)) }}">
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="12" class="location_code">No Records found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>

        <div class="clearfix"></div>
        <div class="col-md-12 text-center">
            <ul class="pagination purple_pagination pull-right">
                {!! $page_links !!}
            </ul>
        </div>
    </div>
</div>
<input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
