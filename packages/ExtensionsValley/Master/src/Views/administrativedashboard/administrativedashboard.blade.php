@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    @include('Dashboard::dashboard.partials.headersidebar')
@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-list.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">

    <style>
        .btn_table {
            width: 100%;
        }

        .btn_table td {
            width: 16.5%;
            padding: 2px;
        }

    </style>
@endsection
@section('content-area')



    <div class="right_col" role="main">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        <div class="row no-padding">
            <div class="col-md-12 padding_sm pull-right">
                <input type="hidden" id="date_datahidden" value='<?= $date_array ?>'>
                <div class="col-md-9 padding_sm">
                    <div class="col-md-3 padding_sm">
                        Range Type : <strong><span class='blue' id='range_typedata'>Today</span></strong>
                    </div>
                    <div class="col-md-3 padding_sm">
                        From Date : <strong><span class='red' id='from_datadis'>{{ date('M-d-Y') }}</span></strong>
                    </div>
                    <div class="col-md-3 padding_sm">
                        To Date : <strong><span class='red' id='to_datadis'>{{ date('M-d-Y') }}</span></strong>
                    </div>
                </div>
                <div class="col-md-3 padding_sm pull-right">
                    <h5 style="color:dodgerblue;"><?= $title ?></h5>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 34px;margin-bottom:10px;">
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(1)" title="Today" class="btn btn-primary btn-block">Current
                                    Month <i class="fa fa-calendar"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(2)" title="Today" class="btn btn-primary btn-block">Previous
                                    Month <i class="fa fa-calendar"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(3)" title="Week Till Date"
                                    class="btn btn-primary btn-block">Last 3 Months <i class="fa fa-calendar"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(4)" title="Last Week" class="btn btn-primary btn-block">Last
                                    6 Months <i class="fa fa-calendar"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(5)" title="Month Till Date"
                                    class="btn btn-primary btn-block">Last 1 Year <i class="fa fa-calendar"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button onclick="getCustomDates(7)" title="Custom Date Range Selection"
                                    class="btn btn-primary btn-block">Custom <i class="fa fa-calendar"></i></button>
                            </div>

                        </div>

                    </div>
                </div>
            </div>



            <div class="col-md-12 no-padding">
                <div class="col-md-3">


                    <div class="col-md-12 no-padding">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:125px;">
                                <span class=text-center> <strong>ICU Occupancy Rate</strong></span>
                                <div id="icu_occupancy" class="no-padding"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 no-padding">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:125px; cursor: pointer;">
                                <span class=text-center> <strong>OT Utilization Rate</strong></span>
                                <div onclick="getOtUtilizationDetail();" id="ot_utilization" class="no-padding"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:125px;">
                                <span class=text-center> <strong>Ward Occupancy Rate</strong></span>
                                <div id="ward_occupancy" class="no-padding"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:125px;">
                                <span class=text-center> <strong>Room Occupancy Rate</strong></span>
                                <div id="room_occupancy" class="no-padding"></div>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="col-md-6 no-padding">

                    <div class="no-padding col-md-12">
                        <table class="table" width="100%" cellspacing="0" cellpadding="" border="0">
                            <tr>
                                <td style="width:50%;">
                                    <div class="bg-info" style="padding-left:15px;">
                                        <span class=""><i id="mortality_rate"
                                                class="blue fa fa-line-chart"></i> Mortality Rate</span>

                                        <div class="count td_common_dashboard_count" id="mortality_count">0.0%</div>

                                    </div>
                                </td>
                                <td style="width:50%;">
                                    <div class="bg-info" style="padding-left:15px;">
                                        <span title="no of Equipment down time/ no of equipment hours"
                                            class=""><i id="critical_equipment_downtime"
                                                class="blue fa fa-clock-o "></i> Critical Equipment Down Time</span>

                                        <div id="CriticalDowntime" class="count td_common_dashboard_count" id="">0</div>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="bg-info" style="padding-left:15px;">
                                        <span title="no of Equipment down time/ no of equipment hours"
                                            class=""><i id="" class="blue fa fa-calendar"></i> No of Sentinal
                                            Events</span>

                                        <div id="sentinalevent_count" class="count td_common_dashboard_count"></div>
                                    </div>
                                </td>
                                <td>
                                    <div class="bg-info" style="padding-left:15px;">
                                        <span title="" class=""><i id="" class="blue fa fa-clock-o"></i>
                                            Average Discharge Time</span>

                                        <div id="AvgDischargeTime" class="count td_common_dashboard_count"></div>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="bg-info" style="padding-left:15px;">
                                        <span title="" class=""><i id="" class="blue fa fa-clock-o"></i>
                                            Meetings overdue</span>

                                        <div class="count td_common_dashboard_count"></div>
                                    </div>

                                </td>
                                <td>
                                    <div class="bg-info" style="padding-left:15px;">
                                        <span title="" class=""><i id="" class="blue fa fa-clock-o"></i>
                                            Overall Compliance to Standards </span>

                                        <div id="overall_compliance" class="count td_common_dashboard_count"></div>
                                    </div>

                                </td>

                            </tr>
                        </table>
                    </div>

                    <div class="col-md-12">
                        <h4 style="text-align: center">Infection Control Parameters</h4>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>VAP(Ventilator Associated Pneumonia)</td>
                                    <td id="vat_value">15/1000 Ventilator Days</td>
                                </tr>
                                <tr>
                                    <td>Pressure Ulcer Rate</td>
                                    <td id="pressure_ulcer_rate_value">4%</td>
                                </tr>
                                <tr>
                                    <td>Surgical Site Infection</td>
                                    <td id="surgical_site_infection_value">3%</td>
                                </tr>
                                <tr>
                                    <td>Hand Hygene Rate</td>
                                    <td id="hand_hygiene_rate_value">67%</td>
                                </tr>
                                <tr>
                                    <td>Catheter Associated UTI</td>
                                    <td id="catheter_associated_uti_value">10 / 100 Catheter Days</td>
                                </tr>
                            </thead>
                        </table>
                    </div>



                </div>

                <div class="col-md-3">

                    <div class="col-md-12 no-padding">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:125px;">
                                <span class=text-center> <strong>TAT Outliers For Lab</strong></span>
                                <div id="tat_outleries" class="no-padding"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:125px;">
                                <span class=text-center> <strong>IP Satisfaction Index</strong></span>
                                <div id="ip_satisfaction" class="no-padding"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:125px;">
                                <span class=text-center> <strong>OP Satisfaction Index</strong></span>
                                <div id="op_satisfaction" class="no-padding"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:125px;">
                                <span class=text-center> <strong>Employee Satisfaction Index</strong></span>
                                <div id="employee_satisfaction" class="no-padding"></div>
                            </div>
                        </div>
                    </div>


                </div>


            </div>
        </div>
    </div>

    <!---- Custom date range selection-------------------------->
    <div class="modal fade" id="customdatapopmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 500px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Custom Date Range Selection</h4>
                </div>
                <div class="modal-body" style="min-height: 80px">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-4 date_filter_div">
                            <div class="mate-input-box">
                                <label class="filter_label">From Date</label>
                                <input type="text" data-attr="date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                    class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="from_date">
                            </div>
                        </div>
                        <div class="col-md-4 date_filter_div">
                            <div class="mate-input-box">
                                <label class="filter_label">To Date</label>
                                <input type="text" data-attr="date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                    class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="to_date">
                            </div>
                        </div>
                        <div class="col-md-4 date_filter_div pull-right" style="padding-top: 10px;">
                            <button id="searchdatabtn" onclick="getCustomDateRange()"
                                class="btn btn-primary btn-block">Search <i id="searchdataspin"
                                    class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>
    <!---- Custom date range selection ends--------------------->

    <!---- Drilldown data selection-------------------------->
    <div class="modal fade" id="drilldownmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="min-width: 90%;min-height:500px;">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="drilldowntitle"></h4>
                </div>
                <div class="modal-body" style="min-height:500px;">
                    <div class="col-md-12 padding_sm" id="drilldownbody">

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!---- Drilldown data selection ends--------------------->

@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/administrativeDashBoard.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js') }}"></script>
@endsection
