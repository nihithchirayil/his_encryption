@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-8 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        <form action="{{route('extensionsvalley.master.favouriteList')}}" id="categorySearchForm" method="POST">
                            {!! Form::token() !!}
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Doctor Name</label>
                                <div class="clearfix"></div>
                                {!! Form::select('doctor_id', array("0" => " Select Doctor") + $doctor->toArray(), $searchFields['doctor_id'] ?? '',
                                ['class'=>"form-control", 'id'=>"doctor_id"]) !!}
                            </div></div>

                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Group Name</label>
                                <div class="clearfix"></div>
                                    <input type="text" value="{{ $searchFields['fav_grp_name'] ?? '' }}" name="fav_grp_name" autocomplete="off" id="fav_grp"
                                       class="form-control fav_grp"
                                       placeholder="Name" onkeyup="searchFavGroup(this.id, event)">
                                       <div class="ajaxSearchBox" id="fav_grp_div" style=""></div>
                                       <input type="hidden" value="{{ $searchFields['fav_grp_id'] ?? '' }}" name="fav_grp_id" id="fav_grp_id">
                            </div></div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                    Search</button>
                            </div>
                             <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>  Clear</a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; max-height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th>Doctor Name</th>
                                    <th>Group Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($fav_list) > 0)
                                    @foreach ($fav_list as $list)
                                        <tr style="cursor: pointer;" onclick="editLoadData(this,
                                            '{{$list->group_name}}',
                                            '{{$list->group_id}}',
                                            '{{$list->user_id}}')">
                                            <td class="doctor_name common_td_rules">{{ $list->doctor_name }}</td>
                                            <td class="group_name common_td_rules">{{ $list->group_name }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="fav_list">No Records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.saveFavouriteItems')}}" method="POST" id="fav_listForm">
                        {!! Form::token() !!}
                        <input type="hidden" name="grp_id" id="grp_id" value="0">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Group Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" required="" class="form-control" name="group_name" id="group_name">
                            <span class="error_red">{{ $errors->first('group_name') }}</span>
                        </div>
                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Doctor Name<span class="error_red">*</span></label>
                            <div class="clearfix"></div>
                            {!! Form::select('doctor', array("0" => " Select Doctor") + $doctor->toArray(),'',
                                ['class'=>"form-control",'required'=>"required", 'id'=>"doctor"]) !!}
                                <span class="error_red">{{ $errors->first('doctor') }}</span>
                        </div>
                        </div>

                        <div class="col-md-12 padding_sm">
                        <div id="addRowDiv" class="theadscroll always-visible" style="position: relative; height: 320px;">
                          <table id="addNewRow" class="table">
                            <tbody id="newrowid">

                            </tbody>
                          </table>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>Cancel</a>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script type="text/javascript">
    var j=0;
    $(document).ready(function () {
        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


        $('.fixed_header').floatThead({
            position: 'absolute',
            scrollContainer: true
        });

        $("#doctor").on('change', function() {
            $("#newrowid").html("");
            $("#addNewRow").html("");
            addNewServiceRow('','0','0','0');
        });

    });

    function editLoadData(obj,group_name,group_id,user_id){
        var edit_icon_obj = $(obj);
        $("#group_name").val(group_name);
        $("#grp_id").val(group_id);
        $("#doctor").val(user_id);
        $.ajax({
                    type: "GET",
                    url: "",
                    data: 'group_id=' + group_id + '&doctor_id=' + user_id,
                    beforeSend: function () {
                        // $("#newrowid").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                        $("#newrowid").html("");
                        $("#addNewRow").html("");
                    },
                    success: function (res) {
                            console.log(res);
                        if(res != ""){
                            j=0;
                            var type = '0';
                            for(var i=0 ; i < res.length ; i++){
                                addNewServiceRow('',res[i].fav_itm_id,
                                    res[i].service_code,res[i].type);
                                type = res[i].type;
                            }
                            addNewServiceRow('','0','0',type);
                        }else{
                            addNewServiceRow('','0','0','0');
                        }
                    },
                    complete: function () {
                        $('.theadscroll').perfectScrollbar("update");
                    }
                });
    }
    function fillGroupDetails(e, id, name) {
                        $('#fav_grp').val(name);
                        $('#fav_grp_id').val(id);
                        $('#fav_grp_div').hide();
    }
    function searchFavGroup(id, event) {
        var keycheck = /[a-zA-Z0-9 ]/;
        var value = event.key;
        var ajax_div = 'fav_grp_div';

        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
            var group = $('#' + id).val();
            if (group == "") {
                $("#" + ajax_div).html("");
            } else {
                $.ajax({
                    type: "GET",
                    url: "",
                    data: 'fav_grp_name=' + group,
                    beforeSend: function () {
                        $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#" + ajax_div).html(html).show();
                        $("#" + ajax_div).find('li').first().addClass('liHover');
                    },
                    complete: function () {
                        $('.theadscroll').perfectScrollbar("update");
                        $(".theadfix_wrapper").floatThead('reflow');
                    }
                });
            }

        } else {
            ajaxProgressiveKeyUpDown(ajax_div, event);
        }
    }

    function addNewServiceRow(obj,fav_itm_id,service_code,type){
        console.log(fav_itm_id,service_code,type,j);
        var table = document.getElementById("addNewRow");
        var newrow = table.rows.length;
        var row = table.insertRow(newrow);
        row.id = "newRow" + newrow;
        <?php  $m = 1; $n = 0; ?>
        <?php   foreach($addFieldsArray as $addFields) { ?>//
                        var cell{{$m}} = row.insertCell("{{$n}}");
                        cell{{$m}}.innerHTML = "<?php  echo $addFields; ?>";
        <?php
                        $m++; $n++;
                }
        ?>
        if(fav_itm_id == 0 && obj != ''){
            var itm_type = $(obj).closest("tr").find("select[name='fav_type[]']").val();
            $('#newRow'+j).find(".fav_type").val(itm_type);
        }
        $('#newRow'+j).find("#fav_itm_id").val(fav_itm_id);
        if(type != 0){
            $('#newRow'+j).find(".fav_type").val(type);
        }
        if(service_code != 0){
            $('#newRow'+j).find(".service_code").val(service_code);
        }
       j++;
    }
@include('Purchase::messagetemplate')
</script>

@endsection
