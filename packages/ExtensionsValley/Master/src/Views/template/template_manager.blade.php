@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    @include('Dashboard::dashboard.partials.headersidebar')
@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-list.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">

    <style>
        .btn_table {
            width: 100%;
        }

        .btn_table td {
            width: 16.5%;
            padding: 2px;
        }

    </style>
@endsection
@section('content-area')

    <div class="right_col" role="main">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <div class="row no-padding">
            <div class="box no-border no-margin">
                <div class="box-footer">
                    <div class="col-md-12">
                        {!! Form::label('Select Doctor', 'Select Doctor', ['class' => 'control-label']) !!}
                        {!! Form::select('doctor_id', $doctors, null, ['class' => 'form-control select2', 'id' => 'doctor_id']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
