@if(sizeof($result_data)>0)
    <div class="theadscroll" style="position: relative; height: 500px;">
        @php
            $i=1;
        @endphp
        <table border="1" width="100%;" class="table table-striped table-bordered table-condensed table_sm theadfix_wrapper" style="border-collapse: collapse;">
            <thead>
                <tr class="table_header_bg">
                    <td>Si.No</td>
                    <td>Date</td>
                    <td>Patient</td>
                    <td>UHID</td>
                    <td>Age/Gen</td>
                    <td>Form Name</td>
                    <td>Doctor</td>
                    <td>Prepared By</td>
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody>
                @foreach($result_data as $key=>$value)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{date('M-d-Y h:i A',strtotime($value->created_at))}}</td>
                        <td>{{$value->patient_name}}</td>
                        <td>{{$value->uhid}}</td>
                        <td>{{$value->age}}/{{$value->gender}}</td>
                        <td>{{$value->name}}</td>
                        <td>{{$value->doctor_name}}</td>
                        <td>{{$value->created_by}}</td>
                        <td>
                            <button type="button" class="btn btn-primary btn-sm" onclick="view_form('{{$value->id}}')"><i class="fa fa-window"></i></button>
                            <button type="button" class="btn btn-danger btn-sm" onclick="finalize_form('{{$value->id}}')"><i class="fa fa-right"></i></button>
                        <td>{{date('d-m-Y',strtotime($value->created_at))}}</td>
                        <td>{{$value->form_name}}</td>
                        <td>{{$value->doctor_name}}</td>
                        <td>{{$value->prepared_by}}</td>
                        <td>
                            <a href="{{url('/')}}/view_form/{{$value->id}}" class="btn btn-primary btn-xs" title="View"><i class="fa fa-eye"></i></a>
                            <a href="{{url('/')}}/edit_form/{{$value->id}}" class="btn btn-primary btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                            <a href="{{url('/')}}/delete_form/{{$value->id}}" class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    @php
                        $i++;
                    @endphp
                @endforeach
            </tbody>
        </table>
    </div>
@else
    <div class="col-md-12">
        <div class="alert alert-danger">
            <strong>No Results Found</strong>
        </div>
    </div>
@endif
