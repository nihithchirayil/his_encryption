@if(sizeof($result_data)>0)
        <div class="theadscroll" style="position: relative; height: 500px;">
            <table width="100%;" class="table table-striped table-bordered table_sm theadfix_wrapper" style="border-collapse: collapse;">
                <thead>
                    <tr style="background-color:cadetblue;color:white;">
                        {{-- <td style="width:10% !important">Sl.No</td> --}}
                        <td style="width:10% !important">Key</td>
                        <td style="width:80% !important">Content</td>
                        <td style="width:10% !important">Actions</td>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $key = 0;
                    @endphp
                    @foreach($result_data as $key=>$value)
                    <tr id="template_row_{{$value->id}}" onclick="usg_add_to_template({{$value->id}})">
                        {{-- <td>{{$key+1}}</td> --}}
                        <td><b>{{$value->name}}</b></td>
                        <td style="padding:5px !important;cursor:pointer;">{!!$value->content!!}</td>
                        <td>
                            <button type="button" title="Edit" class="btn btn-sm bg-green" onclick="usg_edit_favoutite({{$value->id}})"><i class="fa fa-edit"></i></button>
                            <button type="button" title="Delete" class="btn btn-sm bg-red" onclick="usg_delete_favoutite({{$value->id}})"><i class="fa fa-trash-o"></i></button>
                            <button type="button" title="Add To Template" class="btn btn-sm bg-blue" onclick="usg_add_to_template({{$value->id}})"><i class="fa fa-plus"></i></button>
                        </td>

                    </tr>
                    @php
                        // $key++;
                    @endphp
                    @endforeach
            </table>
        </div>
@else
    <div class="alert alert-info">
        <strong>Info!</strong> No Data Found.
    </div>
@endif
