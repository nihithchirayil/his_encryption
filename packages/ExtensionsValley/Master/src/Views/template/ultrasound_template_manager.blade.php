@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')

    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">

    <style>
        .mce-item-table,
        .mce-item-table td,
        .mce-item-table th,
        .mce-item-table caption {
            border-top: 1pt solid #000000 !important;
            border-right: 2px solid #000000 !important;
            border-bottom: 1pt solid #000000 !important;
            border-left: 2px solid #000000 !important;
        }

        .headerbtn {
            border: 1px solid green;
            min-width: 115px;
        }

        .tox .tox-tbtn {
            transform: scale(0.7) !important;
            height: 20px !important;
            /* width: unset !important; */
        }

        .tox .tox-mbtn {
            height: 20px !important;

        }

        .tox .tox-toolbar,
        .tox .tox-toolbar__overflow,
        .tox .tox-toolbar__primary {
            background: none !important;
            border-top: 1px solid #CCC;
            border-bottom: 1px solid #CCC;
        }

        .tox-tbtn__select-label {
            font-size: 16px !important;
            font-weight: 600 !important;
        }

        .custom_input {
            background: none;
            padding: 0;
            border-color: #e2e7eb;
            border-radius: 3px;
            color: #555555;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            background-color: white;
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
            min-height: 26px;
        }

        .common_td_rules:hover {
            border: 1px solid #e2e7eb;
            box-shadow: none;
            background-color: rgb(196, 253, 213);
        }

    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="row col-md-12" style="text-align: right; font-size: 12px;font-weight: bold;"> {{ $title }}
            <div class="clearfix"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-3">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div class="theadscroll" style="position: relative; height: 550px;">
                                <table class="table no-margin theadfix_wrapper table-striped no-border"
                                    style="border: 1px solid #CCC;">
                                    <thead>
                                        <tr class="table_header_bg" style="cursor: pointer;">
                                            <th width="75%">Form Name</th>
                                            <th width="25%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="template_list_content">
                                        @if (count($item) > 0)
                                            @foreach ($item as $item)

                                                <tr style="cursor: pointer;" onclick="edit_template('{{ $item->id }}');"
                                                    id="template_row_{{ $item->id }}">

                                                    <td class="common_td_rules">{{ $item->form_name }}
                                                        <input type="hidden" class="fav_check" name="form_name[]"
                                                            value="{{ $item->id }}"><input type="hidden"
                                                            class="form-control" name="form_name[]"
                                                            value="{{ $item->form_name }}">
                                                    </td>

                                                    <td>
                                                        <button class='btn btn-sm btn-default delete-critical-downtime'
                                                            onclick="delete_template('{{ $item->id }}');"><i
                                                                class="fa fa-trash"></i>
                                                        </button>
                                                        <button class='btn btn-sm btn-default edit-critical-downtime'
                                                            onclick="edit_template('{{ $item->id }}');"><i
                                                                class="fa fa-edit"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="12" class="location_code">No Records found</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-12 text-center">
                                <ul class="pagination purple_pagination pull-right">
                                    {!! $page_links !!}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 no-border no-margin ">
                    <div class=" clearfix"></div>
                    <div class="box no-border">
                        <div class="col-md-12" style="margin-bottom:10px;">
                            <div class="col-md-4">
                                {!! Form::label('form_name', 'Form Name', ['class' => '']) !!}
                                {{ Form::text('form_name', null, ['class' => 'form-control col-md-3', 'placeholder' => '', 'style' => 'height:34px !important;border-radius:3px;']) }}
                            </div>
                            <div class="col-md-4" style="z-index: 9000;">

                                {!! Form::label('department', 'Department', ['class' => '']) !!}
                                <div class="clearfix"></div>
                                {!! Form::select('department', $departments, 999999999, ['class' => 'form-control select2 filters', 'multiple' => 'multiple-select', 'placeholder' => ' ', 'id' => 'department', 'style' => 'width:100%;color:#555555;z-index:70;  padding:2px 12px;border-color:#e2e7eb;']) !!}

                            </div>
                            <div class="col-md-4">

                                {!! Form::label('group', 'Group', ['class' => '']) !!}
                                <div class="clearfix"></div>
                                {!! Form::select('group', $groups, 999999999, ['class' => 'form-control select2 filters', 'multiple' => 'multiple-select', 'placeholder' => ' ', 'id' => 'group', 'style' => 'width:100%;color:#555555;z-index:70;  padding:2px 12px;']) !!}

                            </div>
                        </div>
                        <div class="col-md-12" style="margin-bottom:10px;">
                            <div class="col-md-4">
                                {!! Form::label('user_name_label', 'User', ['class' => '']) !!}
                                <div class="clearfix"></div>
                                {!! Form::select('user', $users, 999999999, [
    'class' => 'form-control select2 filters',
    'multiple' => 'multiple-select',
    'placeholder' => ' ',
    'id' => 'user',
    'style' => 'width:100%;color:#555555; padding:2px 12px; margin-bottom: 15px;
                                ',
]) !!}
                            </div>

                            <div class="col-md-4" style="margin-top:10px;">
                                <button title="Add To Favourites" onclick="add_to_template_favourites();" type="button" class="btn btn-info" id="favourite_btn"
                                    style="margin-top:15px;"><i class="fa fa-star"></i>
                                </button>
                                <button title="Favourites" type="button" class="btn btn-info" id="favourite_btn" onclick="favourite_template();"
                                    style="margin-top:15px;"><i class="fa fa-list"></i> Favourites List
                                </button>
                            </div>

                            <div class="col-md-4 pull-right" style="margin-top:10px;">
                                <button class="btn bg-green pull-right" id="add_form_btn" onclick="saveTemplate();">
                                    <i class="fa fa-save"></i> Save

                                </button>

                                <button class="btn btn-primary pull-right" id="reset_form_btn" onclick="resetTemplate();">

                                    <i class="fa fa-refresh"></i> Reset
                                </button>
                            </div>
                            <div class="col-md-12" style="margin-top:10px;">
                                <textarea class="tiny_editor" name="template_content" id="template_content"></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
                    <input type="hidden" name="form_id" id="form_id" value="" />
                    <input type="hidden" name="token" id="token" value="{{ csrf_token() }}" />
                </div>
            </div>
        </div>
    </div>

    <!----favoutite name modal window------->
    <div class="modal fade" id="favourite_name_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 9999999999">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: orange;color:white;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Favourite Name</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="favourite_name">Favourite Name</label>
                        <input type="text" autocomplete='off' style="height:34px !important;border-radius:3px;" class="form-control" id="favourite_name" name="favourite_name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="save_to_favourites();">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!----------modal window list favourite template------------------------->

        <div class="modal fade" id="favourite_template_list_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 9999999999">
            <div class="modal-dialog" role="document" style="width:95%;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: orange;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Favourite List</h4>
                    </div>
                    <div class="modal-body" style="height:550px;">
                        <div class="col-md-12" style="width: 100%;" id="favourite_template_list_data">

                        </div>
                    </div>

                </div>
            </div>
        </div>

    <!----------Favourite template edit modal------------------------------>
        <div class="modal fade" id="fav_template_edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 9999999999">
            <div class="modal-dialog" role="document" style="width:70.5%;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: rgb(14, 124, 60);color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Favourite Content</h4>
                    </div>
                    <div class="modal-body" id="fav_edit_modal_content" style="height:500px;">
                        <div class="col-md-12" style="width: 100%;">
                             {!! Form::label('fav_temp_edit_lable', 'Name', ['class' => '']) !!}
                            <input type="hidden" style="height:34px !important;border-radius:3px;" name="fav_template_id" id="fav_template_id" value="" />
                            <input type="text" class="form-control" id="favourite_name_edit"/>
                        </div>
                        <div class="col-md-12" style="margin-top:10px;">
                            {!! Form::label('', 'Content', ['class' => '']) !!}
                            <textarea class="tiny_editor" name="favourite_template_content" id="favourite_template_content"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="save_fav_template_edit();">Save changes</button>
                    </div>

                </div>
            </div>
        </div>

        <!-- script content -->
        {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
        {!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}

        {!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
        {!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
        {!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
        {!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
        {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
        {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}
        <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
        <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                setTimeout(function() {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                    $("option:selected").prop("selected", false);
                }, 300);


                var table = $('table.theadfix_wrapper');

                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });

                tinymce.init({
                    selector: 'textarea.tiny_editor',
                    max_height: 455,
                    autoresize_min_height: '90',
                    plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons ',
                    imagetools_cors_hosts: ['picsum.photos'],
                    menubar: 'file edit view insert format tools table help',
                    toolbar: 'undo redo | bold italic underline strikethrough | customInsertButton | fontselect fontsizeselect formatselect |Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | outdent indent | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl | lineheight ',
                    browser_spellcheck: true,
                    toolbar_sticky: true,
                    autosave_ask_before_unload: true,
                    autosave_interval: '30s',
                    autosave_prefix: '{path}{query}-{id}-',
                    autosave_restore_when_empty: false,
                    autosave_retention: '2m',
                    paste_enable_default_filters: false,
                    image_advtab: true,
                    contextmenu: false,
                    importcss_append: true,
                    height: 580,
                    image_caption: true,
                    quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
                    noneditable_noneditable_class: 'mceNonEditable',
                    toolbar_mode: 'sliding',
                    branding: false,
                    statusbar: false,
                    forced_root_block: '',
                    nowrap: false,
                    width: "910",
                    table_default_styles: {
                        width: '100%',
                        border: '1px solid black',
                        wordbreak: 'break-word',
                    },
                    max_width: 910,
                    paste_enable_default_filters: false,
                    table_sizing_mode: 'relative',
                    overflow: 'auto',
                    table_responsive_width: true,
                    nonbreaking_force_tab: true,
                    content_style: "body { line-height:1.1;font-size:13px;}",
                    setup: function(editor) {

                        editor.on('change', function(e) {
                            this.props.updateSectionContents(event.target.getContent());
                            WrapTinyMCeTableContent();
                        });



                        editor.ui.registry.addButton('Ucase', {
                            text: 'A^',
                            onAction: function() {
                                TextToUpperCase();
                            },
                        });
                        editor.ui.registry.addButton('Lcase', {
                            text: 'a^',
                            onAction: function() {
                                TextToLowerCase();
                            },
                        });
                        editor.ui.registry.addButton('Icase', {
                            text: 'I^',
                            onAction: function() {
                                TextToInterCase();
                            },
                        });
                        editor.ui.registry.addButton('Ccase', {
                            text: 'C^',
                            onAction: function() {
                                FirstLetterToInterCase();
                            },
                        });


                    },


                });

            });

            function WrapTinyMCeTableContent() {
                var editor = tinymce.activeEditor;
                var selected_text = tinymce.get('template_content').getContent();
                if (selected_text != '') {

                    tinymce.get('template_content').setContent('<div style="width:100%;word-break:break-word !important; "' +
                        selected_text +
                        '</div>');
                }
            }

            function saveTemplate() {
                var form_name = $('#form_name').val();
                var department = $('#department').val();
                var group = $('#group').val();
                var user = $('#user').val();
                var url = $('#domain_url').val() + "/ultrasound/saveTemplate";
                var template_content = tinymce.get('template_content').getContent();
                template_content = encodeURIComponent(template_content.replace('&', '\&'));
                template_content = template_content.trim();
                // console.log(template_content);
                //return;
                var form_id = $('#form_id').val();
                if (form_name == '') {
                    alert('Please enter form name');
                    return false;
                }
                if (template_content == '') {
                    alert('Please enter template content');
                    return false;
                }
                var data = {
                    form_name: form_name,
                    department: department,
                    group: group,
                    user: user,
                    template_content: template_content,
                    form_id: form_id
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        $("body").LoadingOverlay("hide", true);
                        var obj = JSON.parse(data);

                        // return;
                        if (obj.status == 1) {
                            Command: toastr["success"](obj.message);
                            $('#template_list_content').append(atob(obj.html));
                        }
                        else if (obj.status == 2) {
                            Command: toastr["success"](obj.message);
                        }
                        else {
                            Command: toastr["error"](obj.message);
                        }
                    },

                });
            }

            function edit_template(id) {
                var url = $('#domain_url').val() + "/ultrasound/editTemplate";
                var data = {
                    id: id
                };
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        $("body").LoadingOverlay("hide", true);
                        var obj = JSON.parse(data);
                        console.log(obj.departments);
                        //  return;
                        if (obj.status == 1) {
                            $('#form_name').val(obj.form_name);
                            $('#department').select2("val", obj.departments);
                            $('#group').select2("val", obj.groups);
                            $('#user').select2("val", obj.users);
                            tinymce.get('template_content').setContent(obj.template_content);
                            $('#form_id').val(obj.form_id);
                        } else {
                            Command: toastr["error"](obj, message);
                        }
                    },

                });
            }

            function delete_template(id) {
                var url = $('#domain_url').val() + "/ultrasound/deleteTemplate";
                var data = {
                    id: id
                };
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        $("body").LoadingOverlay("hide", true);
                        var obj = JSON.parse(data);

                        if (obj.status == 1) {
                            Command: toastr["success"](obj.message);
                            $('#template_row_' + id).remove();
                            var form_id = $('#form_id').val();
                            if (form_id == id) {
                                resetTemplate();
                            }
                        }
                        else {
                            Command: toastr["error"](obj.message);
                        }
                    },

                });
            }

            function resetTemplate() {
                tinymce.get('template_content').setContent('');
                $('#form_id').val('');
                $('#form_name').val('');
                $('#department').select2("val", '');
                $('#group').select2("val", '');
                $('#user').select2("val", '');
            }

            function add_to_template_favourites(){
                var template_content = tinymce.get('template_content').getContent();

                if (template_content == '') {
                    Command: toastr["warning"]('Please enter template content');
                    return false;
                }else{
                    $('#favourite_name_modal').modal('show');
                }
            }

            function save_to_favourites(){

                var url = $('#domain_url').val() + "/ultrasound/addToTemplateFavourites";
                var favourite_name = $('#favourite_name').val();
                var template_content = tinymce.get('template_content').getContent();
                template_content = encodeURIComponent(template_content.replace('&', '\&'));
                template_content = template_content.trim();
                var token = $('#token').val();
                if (template_content == '') {
                    Command: toastr["warning"]('Please enter template content');
                    return false;
                }else if(favourite_name == ''){
                    Command: toastr["warning"]('Please enter favourite name');
                    return false;
                } else{
                    var data = {
                        favourite_name: favourite_name,
                        template_content: template_content,
                        _token: token
                    };


                    $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        var obj = JSON.parse(data);
                        if (obj.status == 1) {
                            Command: toastr["success"](obj.message);
                        }
                        else {
                            Command: toastr["error"](obj.message);
                        }
                    },
                    complete: function() {
                        $("body").LoadingOverlay("hide", true);
                        $('#favourite_name_modal').modal('hide');
                    }

                });

                }

            }

            function favourite_template(){
                var url = $('#domain_url').val() + "/ultrasound/favouriteTemplateList";
                var data = {
                    showtemplate:'showtemplate'
                };
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        $('#favourite_template_list_modal').modal('show');
                        $("#favourite_template_list_data").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        $("#favourite_template_list_data").html(data);
                        $('.theadscroll').perfectScrollbar({
                            minScrollbarLength: 30
                        });

                        var $table = $('table.theadfix_wrapper');
                            $table.floatThead({
                                scrollContainer: function($table){
                                    return $table.closest('.theadscroll');
                                }
                        });
                    },
                    complete: function() {
                        $("#favourite_template_list_data").LoadingOverlay("hide", true);
                    }

                });
            }

            function usg_delete_favoutite(id){
                var url = $('#domain_url').val() + "/ultrasound/deleteFavouriteTemplate";
                var data = {
                    id: id
                };
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        $("#favourite_template_list_data").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        var obj = JSON.parse(data);
                        if (obj.status == 1) {
                            Command: toastr["success"](obj.message);
                            $('#template_row_' + id).remove();
                        }
                        else {
                            Command: toastr["error"](obj.message);
                        }
                    },
                    complete: function() {
                        $("#favourite_template_list_data").LoadingOverlay("hide", true);
                    }

                });
            }

            function usg_add_to_template(id){
                var url = $('#domain_url').val() + "/ultrasound/addToTemplate";
                var data = {
                    id: id
                };
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        $("#favourite_template_list_data").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        $("#favourite_template_list_data").LoadingOverlay("hide", true);
                        var obj = JSON.parse(data);
                        if (obj.status == 1) {
                            Command: toastr["success"]('Favourite Content Added!');
                            tinymce.activeEditor.execCommand('mceInsertContent', false, obj.content);
                            $('#favourite_template_list_modal').modal('hide');

                        }
                        else {
                            Command: toastr["error"](obj.message);
                        }
                    },

                });
            }

            function usg_edit_favoutite(id){
                // $('#fav_template_edit_modal').modal('show');
                var url = $('#domain_url').val() + "/ultrasound/editFavouriteTemplate";
                var data = {
                    id: id
                };
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        $('#fav_template_edit_modal').modal('show');
                        $("#fav_edit_modal_content").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        var obj = JSON.parse(data);
                        console.log(obj.template_data[0].content);
                        if (obj.status == 1) {
                            $('#fav_template_id').val(obj.template_data[0].id);
                            $('#favourite_name_edit').val(obj.template_data[0].name);
                            tinymce.activeEditor.setContent(obj.template_data[0].content);

                        }
                        else {
                            Command: toastr["error"](obj.message);
                        }
                    },
                    complete: function(data){
                        $("#fav_edit_modal_content").LoadingOverlay("hide", true);
                    },

                });
            }

            function usg_save_fav_template_edit(){
                var url = $('#domain_url').val() + "/ultrasound/saveFavouriteTemplateEdit";
                var favourite_name = $('#favourite_name_edit').val();
                var template_content = tinymce.activeEditor.getContent();
                template_content = encodeURIComponent(template_content.replace('&','\&'));
                template_content = template_content.trim();
                var id = $('#fav_template_id').val();
                var token = $('#token').val();
                if (favourite_name == '') {
                    Command: toastr["warning"]('Please enter favourite name');
                    return false;
                } else{
                    var data = {
                        favourite_name: favourite_name,
                        template_content: template_content,
                        id: id,
                        _token: token
                    };
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        beforeSend: function() {
                            $("#fav_edit_modal_content").LoadingOverlay("show", {
                                background: "rgba(255, 255, 255, 0.7)",
                                imageColor: '#337AB7'
                            });
                        },
                        success: function(data) {
                            var obj = JSON.parse(data);
                            if (obj.status == 1) {
                                Command: toastr["success"]('Favourite Content Updated!');
                                $('#fav_template_edit_modal').modal('hide');
                                favourite_template();

                            }
                            else {
                                Command: toastr["error"](obj.message);
                            }
                        },
                        complete: function(data){
                            $("#fav_edit_modal_content").LoadingOverlay("hide", true);
                        },

                    });
                }
            }

        </script>

    @endsection
