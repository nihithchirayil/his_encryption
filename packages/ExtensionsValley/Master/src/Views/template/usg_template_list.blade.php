<div class="box no-border no-margin">
    <div class="box-body clearfix">
        <div class="theadscroll" style="position: relative; max-height: 380px;">
            <table class="table no-margin theadfix_wrapper table-striped table-bordered styled-table"
                style="border: 1px solid #000;">
                @php
                    $i = 1;
                @endphp
                <thead>
                    <tr class="table_header_bg">
                        <th>Si.No</th>
                        <th>Patient</th>
                        <th>uhid</th>
                        <th>Age/Gender</th>
                        <th>Report</th>
                        <th>Status</th>
                        <th>View</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($res as $value)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$value->patient_name}}</td>
                            <td>{{$value->uhid}}</td>
                            <td>{{$value->age}}/{{$value->gender}}</td>
                            <td>{{$value->form_name}}</td>
                            <td style="color:blue">
                                @if($value->is_finalized == 1)
                                    <span style="color:green;">
                                        Finalized by: <br>
                                        {{$value->doctor_name}}
                                    </span>
                                @else
                                    Pending
                                @endif
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary" onclick="view_usg_report({{$value->id}},{{$value->form_id}},{{$value->patient_id}});">View</button>

                            </td>
                            <td id="{{$value->id}}_action">
                                @if($value->is_finalized != 1)
                                <button type="button" title="Finalize" class="btn btn-primary" onclick="finalize_usg_report({{$value->id}});"><i class="fa fa-check"></i></button>
                                @else
                                <button type="button" title="De-Finalize" class="btn btn-warning" onclick="definalize_usg_report({{$value->id}});"><i class="fa fa-ban" aria-hidden="true"></i></button>
                                @endif

                            </td>
                        </tr>
                        @php
                        $i++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
