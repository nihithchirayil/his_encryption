<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    _token: token,
                    from_date: from_date,
                    to_date: to_date,
                    
                },
                beforeSend: function() {
                    $('#searchSurgeryBillListDiv').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                    $('#searchSurgeryMasterBtn').attr('disabled', true);
                    $('#searchSurgeryMasterSpin').removeClass('fa fa-search');
                    $('#searchSurgeryMasterSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchSurgeryBillListDiv').html(data);
                },
                complete: function() {
                    $('#searchSurgeryBillListDiv').LoadingOverlay("hide");
                    $('#searchSurgeryMasterBtn').attr('disabled', false);
                    $('#searchSurgeryMasterSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchSurgeryMasterSpin').addClass('fa fa-search');
                },
            });
            return false;
        });

    });
</script>
<div class="theadscroll" style="position: relative; height: 450px;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th>SL NO</th>
                <th>Bill No</th>
                <th>Bill Date</th>
                <th>UHID</th>
                <th>IP Number</th>
                <th>Patient Name</th>
                <th>Doctor Name</th>
                <th>Bill Amount</th>
                <th>Discount Amount</th>
                <th>Net Amt.</th>
                <th>Paid Amt.</th>
                <th>Action</th>

            </tr>
        </thead>
        <tbody>
            @if (count($surgery_list) > 0)
                @foreach ($surgery_list as $data)
                    <tr>
                        <td class="common_td_rules">{{($surgery_list->currentPage() - 1) * $surgery_list->perPage() + $loop->iteration}}</td>
                        <td class="common_td_rules">{{ $data->bill_no }}</td>
                        <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->bill_date)) }}</td>
                        <td class="common_td_rules">{{ $data->uhid }}</td>
                        <td class="common_td_rules">{{ $data->ip_no }}</td>
                        <td class="common_td_rules">{{ $data->patient_name }}</td>
                        <td class="common_td_rules">{{ $data->consulting_doctor_name }}</td>
                        <td class="common_td_rules">{{ $data->bill_amount }}</td>
                        <td class="common_td_rules">{{ $data->discount_amount }}</td>
                        <td class="common_td_rules">{{ $data->net_amount_wo_roundoff }}</td>
                        <td class="common_td_rules">{{ $data->paid_amount }}</td>
                        <td class="common_td_rules">
                            <button type="button" class="btn btn-sm btn-default" id="schedule_surgery" onclick="" title="Edit BIll">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-default" id="schedule_surgery" onclick="" title="View Bill">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-default" id="schedule_surgery" onclick="cancelSurgeryBIll({{$data->head_id}})" title="Cancel Bill">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </td>     

                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5" class="location_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right" style="margin-top:-31px;">
    <ul class="pagination purple_pagination" style="text-align:right !important;">
        {!! $page_links !!}
    </ul>
</div>