@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/wizard.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/wizard_checklist.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <div class="right_col">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        <input type="hidden" id="current_date" value="{{date('M-d-Y')}}">
        <div class="col-md-12 pull-right" style="text-align: right; font-size: 12px; font-weight: bold;">
            {{ $title }}</div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <form action="{{ route('extensionsvalley.ot_schedule.OTSchedule') }}" id="categorySearchForm"
                                method="POST">
                                {!! Form::token() !!}
                                <div class="col-md-1 padding_sm">
                                    <div class="mate-input-box">
                                        <label style="top:0px;" for="">From Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" name="from_date" autocomplete="off"
                                            value="" class="form-control datepicker" id="from_date"
                                            placeholder="From Date">
                                    </div>
                                </div>

                                <div class="col-md-1 padding_sm">
                                    <div class="mate-input-box">
                                        <label style="top:0px;" for="">To Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" name="to_date" autocomplete="off" value=""
                                            class="form-control datepicker" id="to_date" placeholder="To Date">
                                    </div>
                                </div>

                                <div class="col-md-5 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="filter_label"> Surgery Bill Number</label>
                                        <input class="form-control hidden_search reset" value="" autocomplete="off"
                                            type="text" id="surgery_bill_no" name="surgery_bill_no" />
                                        <div id="surgery_bill_noAjaxDiv" class="ajaxSearchBox"></div>
                                        <input class="filters" value="" type="hidden" name="surgery_bill_no_hidden"
                                            value="" id="surgery_bill_no_hidden">
                                    </div>
                                </div>


                                <div class="col-md-1 padding_sm" style="margin-top: -10px">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button type="button" id="searchSurgeryBillBtn" onclick="searchSurgeryBill();" class="btn btn-block btn-primary"><i id="searchSurgeryMasterSpin" class="fa fa-search"></i>
                                        Search</button>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: -10px">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button type="button" onclick="resetSurgeryList();" class="btn btn-block btn-warning"><i class="fa fa-recycle"></i>
                                        Reset</button>
                                </div>
                                <div class="col-md-2 padding_sm pull-right" style="margin-top: -10px">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button type="button"   onclick="addNewSurgeryBIll();" class="btn btn-block btn-success"><i class="fa fa-plus"></i>
                                        Add New Surgery BIll</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix" id="searchSurgeryBillListDiv">

                    </div>
                </div>
            </div>

        </div>
    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/surgeryBillList.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

@endsection