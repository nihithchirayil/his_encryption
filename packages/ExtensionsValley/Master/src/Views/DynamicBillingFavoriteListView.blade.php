@if(sizeof($resultData))

<div class="theadscroll" style="position: relative; ">
    <table class="table table_sm no-margin theadfix_wrapper table-striped">
        <thead>
            <tr style="background-color: #ddd;">
                <th>SiNo</th>
                <th>Report Name</th>
                <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            {{--*/$i=1;/*--}}
            
             @foreach($resultData as $data)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($data->report_name)}}</td>
                <td style="text-align:center">
                    <button type="button" class="btn btn-success" style="padding: 0px 5px;" onclick="goToReport('{{$data->id}}')"><i class="fa fa-play"></i></button>
                   @if($is_admin == 1)
                    <button type="button" class="btn btn-danger" style="padding: 0px 5px;" onclick="deleteReportFavorite('{{$data->id}}')"><i class="fa fa-trash"></i></button>
                   @endif
                </td>
            </tr>
            {{--*/$i++;/*--}}
            @endforeach
        </tbody>
    <tr>
        
    </tr>
    </table>

</div>    
@else
<label>No reports Found</label>
@endif