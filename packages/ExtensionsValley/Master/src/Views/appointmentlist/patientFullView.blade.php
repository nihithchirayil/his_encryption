<div class="row">
    <div class="col-md-12 padding_sm">
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Slot</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->time_slotdesc ? $patient_details[0]->time_slotdesc : '-' ?>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Token</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->token_no ? $patient_details[0]->token_no : '-' ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Appointment Date</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->booking_date_timedesc ? $patient_details[0]->booking_date_timedesc : '-' ?>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>UHID</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->uhid ? $patient_details[0]->uhid : '-' ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Patient Name</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->patient_name ? $patient_details[0]->patient_name : '-' ?>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Mobile</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->mobile_no ? $patient_details[0]->mobile_no : '-' ?>
            <?= @$patient_details[0]->alternate_mobile_no ? ','.$patient_details[0]->alternate_mobile_no : '' ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Gender</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->genderdesc ? $patient_details[0]->genderdesc : '-' ?>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Doctor Name</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->doctor_name ? $patient_details[0]->doctor_name : '-' ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Speciality</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->specialityname ? $patient_details[0]->specialityname : '-' ?>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Email</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->email ? $patient_details[0]->email : '-' ?>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>DOB</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->dob ? $patient_details[0]->dob : '-' ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Address</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->address ? $patient_details[0]->address : '-' ?>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Walk Status</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->walkstatus ? $patient_details[0]->walkstatus : '-' ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Created At</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->created_at ? $patient_details[0]->created_at : '-' ?>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <strong>Created By</strong>
        </div>
        <div class="col-md-1 padding_sm" style="margin-top: 10px">
            <strong>:</strong>
        </div>
        <div class="col-md-3 padding_sm" style="margin-top: 10px">
            <?= @$patient_details[0]->createduser ? $patient_details[0]->createduser : '-' ?>
        </div>
    </div>
</div>
