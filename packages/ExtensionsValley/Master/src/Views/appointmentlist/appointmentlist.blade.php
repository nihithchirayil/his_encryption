@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/colors.css') }}" rel="stylesheet">

    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
        rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-slide.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/appoinmentList.css') }}" rel="stylesheet">

@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="head_id_hidden" value="0">
    <input type="hidden" id="bill_head_hidden" value="0">
    <input type="hidden" id="doc_id" value="{{ $doc_id }}">
    <input type="hidden" id="doc_name" value="{{ $doc_name }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">

    <div class="right_col">
        <div class="row">
            <div class="col-md-12 padding_sm">
                <div id="filter_area" class="col-md-12"
                    style="padding-left:0px !important;width: 101.75%;margin-left: -10px;">
                    <div class="box no-border no-margin">
                        <div class="box-body" style="padding-bottom:15px;">
                            <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th colspan="11">{{ $title }}

                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="row padding_sm">
                                <div class="col-md-12 padding_sm">
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">Appointment Number</label>
                                            <input class="form-control  reset" value="" autocomplete="off"
                                                type="text" id="app_no" name="app_no" />
                                            <div id="app_noAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters reset" value="" type="hidden" name="app_no_hidden"
                                                value="" id="app_no_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">Doctor</label>
                                            <input class="form-control hidden_search reset" value="" autocomplete="off"
                                                type="text" id="doctor" name="doctor" />
                                            <div id="doctorAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters reset" value="" type="hidden" name="doctor_hidden"
                                                value="" id="doctor_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box ">
                                            <label class="filter_label ">Speciality</label>
                                            <select name="speciality" class="form-control reset" id="speciality">
                                                <option value="">Select</option>
                                                @foreach ($specilaity as $data)
                                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">UHID</label>
                                            <input class="form-control hidden_search reset" value="" autocomplete="off"
                                                type="text" id="uhid" name="uhid" />
                                            <div id="uhidAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters reset" value="" type="hidden" name="uhid_hidden" value=""
                                                id="uhid_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">Patient Name</label>
                                            <input class="form-control hidden_search reset" value="" autocomplete="off"
                                                type="text" id="patient" name="patient" />
                                            <div id="patientAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters reset" value="" type="hidden" name="patient_hidden"
                                                value="" id="patient_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">Mobile Number</label>
                                            <input class="form-control reset" value="" autocomplete="off"
                                                onkeypress='return event.charCode >= 48 && event.charCode <= 57' type="text"
                                                id="mobile" name="mobile" />

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">Token Number</label>
                                            <input class="form-control  reset" value="" autocomplete="off" type="text"
                                                id="token" name="token" />
                                            <div id="tokenAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters reset" value="" type="hidden" name="token_hidden"
                                                value="" id="token_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm" style="margin-top: 2px;">
                                        <div class="col-md-12 padding_sm ">
                                            <div class="radio radio-success inline no-margin ">
                                                <input class="checkit" id="created" type="radio" name="date_radio"
                                                    value=1>
                                                <label class="text-blue" for="created">
                                                    <strong class="green"> Search By Created Date
                                                    </strong>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 padding_sm ">
                                            <div class="radio radio-success inline no-margin ">
                                                <input class="checkit" id="appointment" type="radio" checked
                                                    name="date_radio" value=2>
                                                <label class="text-blue" for="appointment">
                                                    <strong class="green">
                                                        Search By Appointment Date
                                                    </strong> </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">From Date</label>
                                            <input class="form-control reset datepicker" value="{{ date('M-d-Y') }}"
                                                autocomplete="off" type="text" id="from_date" name="from_date" />
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">To Date</label>
                                            <input class="form-control reset datepicker" value="{{ date('M-d-Y') }}"
                                                autocomplete="off" type="text" id="to_date" name="to_date" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="col-md-12 padding_sm ">
                                            <div class="radio radio-success inline no-margin ">
                                                <input class="checkit " id="paid_app" type="radio" name="paid_status"
                                                    value=5>
                                                <label class="text-blue" for="paid_app">
                                                    <strong class="green">
                                                        Paid Appointment
                                                    </strong>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 padding_sm ">
                                            <div class="radio radio-success inline no-margin ">
                                                <input class="checkit" id="unpaid_app" type="radio"
                                                    name="paid_status" value=6>
                                                <label class="text-blue" for="unpaid_app">
                                                    <strong class="green">
                                                        Unpaid Appointment
                                                    </strong>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-2" style="margin-top: -6px">
                                        <div class="col-md-12 padding_sm">
                                            <button type="button" title="search" id="btn_save"
                                                onclick="getAppointmentList()" class="btn btn-primary btn-block"><i
                                                    id="btn_spin" class="fa fa-search padding_sm"></i>Search
                                                Appointments</button>
                                        </div>

                                        <div class="col-md-12 padding_sm">
                                            <button type="button" title="refresh" id="btn_refresh" onclick="resetapp()"
                                                class="btn btn-warning btn-block"><i id="btn_refresh"
                                                    class="fa fa-refresh padding_sm"></i>Refresh</button>

                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3 padding_sm lime">
                                    <div class="radio radio-success inline no-margin ">
                                        <input class="checkit" id="booked" type="radio" name="appoint" value=1>
                                        <label class="text-blue" for="booked">
                                            Booked
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm pink">
                                    <div class="radio radio-success inline no-margin ">
                                        <input class="checkit" id="checked_in" type="radio" name="appoint" value=2>
                                        <label class="text-blue" for="checked_in">
                                            Checked In
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm round_blue">
                                    <div class="radio radio-success inline no-margin ">
                                        <input class="checkit" id="visited" type="radio" name="appoint" value=3>
                                        <label class="text-blue" for="visited">
                                            Visited </label>
                                    </div>
                                </div>
                                <div class="col-md-3 padding_sm dark_orange">
                                    <div class="radio radio-success inline no-margin">
                                        <input class="checkit" id="cancelled_bill" type="radio" name="appoint"
                                            value=4>
                                        <label class="text-blue" for="cancelled_bill">
                                            Cancelled Bill
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-12 padding_sm"
            style="height: 400px;box-shadow: 0px 0px 1px 0px;width: 100%;margin-left: -3px;margin-top: 10px;"
            id="appointmentlist">
        </div>
    </div>

    <!-------modal---------------->

    <div class="modal fade" id="patientFullViewModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1300px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Patient Full View</h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 510px;">
                            <div class="col-md-12 padding_sm" id="patientFullViewModelDiv">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="cancelAppSetup">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header table_header_bg">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;color:white">&times;</span></button>
                    <h3 class="modal-title">Appointment Cancellation</h3>
                </div>
                <div class="modal-body" style="min-height:256px;" id="apppointmentSetupBody">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="col-md-4">
                                <div class="mate-input-box">
                                    <label class="filter_label " for="">Appointment number:</label><input
                                        class="form-control" type="text" id="appointment_no" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mate-input-box"><label class="filter_label " for="">Token No:</label><input
                                        class="form-control" type="text" id="token_no_c" readonly></div>
                            </div>
                            <div class="col-md-4">
                                <div class="mate-input-box"><label class="filter_label " for="">Patient
                                        name:</label><input class="form-control" type="text" id="cancel_patient"
                                        readonly></div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px">
                            <div class="col-md-4">
                                <div class="mate-input-box"><label class="filter_label " for="">Booking
                                        Date:</label></b><input class="form-control" type="text" id="booking_date"
                                        readonly></div>
                            </div>
                            <div class="col-md-4">
                                <div class="mate-input-box"><label class="filter_label " for="">Slot
                                        Time:</label></b><input class="form-control" type="text" id="time_slotdesc"
                                        readonly></div>
                            </div>
                            <div class="col-md-4">
                                <div class="mate-input-box"><label class="filter_label " for="">Created
                                        User:</label></b><input class="form-control" type="text" id="createduser"
                                        readonly></div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px">
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Doctor
                                        Name:</label></b><input class="form-control" type="text" id="doctor_name"
                                        readonly></div>
                            </div>
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label "
                                        for="">Speciality:</label></b><input class="form-control" type="text"
                                        id="specialityname" readonly></div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="col-md-12">
                                <div class="mate-input-box" style="height:  !important;height: 105px !important;">
                                    <label class="filter_label " for="">Cancel Reason:</label>
                                    <div class="clearfix"></div>
                                    <textarea name="" id="Cancel_reason" class="form-control" style="height: 24px !important;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-6 pull-right">
                            <div class="col-md-3 pull-right"><button type="button" class="btn btn-primary"
                                    onclick="cancelAppointment()" id="Cancel"><i id="Cancel_spin"
                                        class="fa fa-trash padding_sm"></i>Cancel</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
       <!-------print modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="serv_print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;float:left">Print Configuration</h4>

                    <div class="col-md-12" style="margin-top:-25px;">
                        

                        <div class="col-md-2 pull-right">
                            <button type="button"  class="close pull-right" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>


                </div>
                <div class="modal-body" style="height:145px;">
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <div class="col-md-10">
                            <div id="bill_saved_name">

                            </div>
                            <div id="bill_saved_bill_no">

                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                        <input style="margin-left: 15px;" type="checkbox" name="is_duplicate" id="duplicate">
                        Duplicate Print
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                       
                        <!-- <button onclick="" id="PayNow" class="btn btn-primary pull-right" style="color:white;display:block;">
                            <i class="fa fa-credit-card" aria-hidden="true"></i> Pay Now
                        </button> -->
                        <button onclick=" PrintBill();" class="btn bg-primary pull-right" style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>

                    </div>


                </div>
            </div>
        </div>
    </div>
@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/appointmentlisting.js') }}"></script>
@endsection
