<table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
    style="font-size: 12px;margin-top:5px;cursor: pointer;">
    <tr class="table_header_bg"
        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
        <th width="3%">#</th>
        <th width="3%"><i class="fa fa-list"></i></th>
        <th width="5%">Slot</th>
        <th width="5%">Token</th>
        <th width="10%">Appointment Date</th>
        <th width="5%">Paid</th>
        <th width="12%">UHID</th>
        <th width="15%">Patient Name</th>
        <th width="10%">Mobile</th>
        <th width="10%">Gender</th>
        <th width="15%">Doctor Name</th>
    </tr>
    <tbody>
        @if (count($res) > 0)

            @foreach ($res as $data)
                @php
                    $color=' ';
                    if ($data->dr_seen_status == 1) {
                        $color = 'round_blue';
                    } elseif ($data->status == 1) {
                        $color = 'lime';
                    } elseif ($data->status == 2) {
                        $color = 'light_pink';
                    } elseif ($data->status == 3) {
                        $color = 'dark_orange';
                    }

                @endphp
                <tr class="<?= $color ?>">
                    <td class="common_td_rules" title="">
                        {{ ($res->currentPage() - 1) * $res->perPage() + $loop->iteration }}</td>
                    <td>
                        <button type="button" class="pull-right" onclick="displayDetails({{ $data->id}},{{$data->bill_head_id }})"
                            style=" width: 40px; height: 20px; border: none;"> <i class="fa fa-list"></i>
                        </button>
                        <div class="popupDiv">
                            <div class="popDiv" id="popup{{ $data->id }}" style="display: none;">
                                <div class="col-md-1 pull-right"
                                    style="border-radius: 50%;width:18px;height:19px;background:#bebbbb;top: -3px;"">
                                    <button type=" button" class="close" id="ppclose{{ $data->id }}"
                                        data-dismiss="modal" aria-label="Close">
                                        <span class="text-info" aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="col-md-12" style="margin-top: -3px;">
                                    <ul class="list-group" style="cursor: pointer">
                                        @if ($data->status == 1)
                                            <li class="list-group-item" id="cancel{{ $data->id }}"
                                                onclick="displayCancelStatus({{ $data->id }})">
                                                <b>Cancel</b>
                                            </li>
                                        @endif
                                        @if ($data->visit_id && $data->visit_id != 0)
                                            <li class="list-group-item print_serv_bill" id="registration_print{{ $data->id }}"
                                                onclick="">
                                                <b>Print</b>
                                            </li>
                                        @endif
                                        <li class="list-group-item"
                                            onclick="listAppoinmentDetailView({{ $data->id }},{{ $data->visit_id }})">
                                            <b>Detail View</b>
                                        </li>
                                      @if ($opPrescriptionStatus==1)
                                      <li class="list-group-item"
                                      onclick="OpPrescriptionPrint({{ $data->id }},{{ $data->visit_id }})">
                                      <b>OP Prescription Print</b>
                                  </li>
                                      @endif
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </td>
                    <td class="common_td_rules  cancel_bg{{ $data->id }}" title="">
                        {{ $data->time_slotdesc }}</td>
                    <td class="common_td_rules  cancel_bg{{ $data->id }}">
                        {{ $data->token_no }}</td>
                    <td class="common_td_rules  cancel_bg{{ $data->id }} ">{{ $data->booking_date_timedesc }}
                    </td>
                    @if ($data->paidstatus == 1)
                        <td class=" cancel_bg{{ $data->id }}">
                            <div class="col-md-2 padding_sm ">
                                <div class="checkbox checkbox-success inline">
                                    <input id="paidstatus" type="checkbox" name="paidstatus" disabled checked>
                                    <label class="text-blue" for="paidstatus">
                                    </label>
                                </div>
                            </div>
                        </td>
                    @else
                        <td class=" cancel_bg{{ $data->id }}">
                            <div class="col-md-2 padding_sm ">
                                <div class="checkbox checkbox-success inline">
                                    <input id="paidstatus1" type="checkbox" name="paidstatus" disabled>
                                    <label class="text-blue" for="paidstatus">
                                    </label>
                                </div>
                            </div>
                        </td>
                    @endif
                    <td class="common_td_rules  cancel_bg{{ $data->id }}" id=""
                        title="{{ $data->uhid }}">
                        {{ $data->uhid ? $data->uhid : '-' }}</td>
                    <td class="common_td_rules  cancel_bg{{ $data->id }}" title="{{ $data->patient_name }}">
                        {{ $data->patient_name }}
                    </td>
                    <td class="common_td_rules  cancel_bg{{ $data->id }}" title="{{ $data->mobile_no }}">
                        {{ $data->mobile_no }}</td>
                    <td class="common_td_rules  cancel_bg{{ $data->id }}" title="">
                        {{ $data->genderdesc }}</td>
                    <td class="common_td_rules  cancel_bg{{ $data->id }}" title="{{ $data->doctor_name }}">
                        {{ $data->doctor_name }}</td>

                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">No Record Match</td>
            </tr>
        @endif

    </tbody>
</table>
<div class="clearfix"></div>
<div class="col-md-12 text-right" style="margin-top: 30px;">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: -24px -4px;">
        {!! $page_links !!}
    </ul>
</div>

<script>
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url == undefined) {
                return;
            }
            var created = $('#created').is(":checked");
            var appointment = $('#appointment').is(":checked");
            var booked = $('#booked').is(":checked");
            var cancelled_bill = $('#cancelled_bill').is(":checked");
            var checked_in = $('#checked_in').is(":checked");
            var visited = $('#visited').is(":checked");
            var paid_app = $('#paid_app').is(":checked");
            var unpaid_app = $('#unpaid_app').is(":checked");
            var app_no = $('#app_no').val();
            var doctor_hidden = $('#doctor_hidden').val();
            var speciality = $('#speciality').val();
            var uhid = $('#uhid').val();
            var patient_hidden = $('#patient_hidden').val();
            var mobile = $('#mobile').val();
            var token = $('#token').val();
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var params = {
                created: created,
                appointment: appointment,
                booked: booked,
                cancelled_bill: cancelled_bill,
                checked_in: checked_in,
                visited: visited,
                paid_app: paid_app,
                unpaid_app: unpaid_app,
                app_no: app_no,
                speciality: speciality,
                doctor_hidden: doctor_hidden,
                uhid: uhid,
                patient_hidden: patient_hidden,
                mobile: mobile,
                token: token,
                from_date: from_date,
                to_date: to_date,

            };
            $.ajax({
                type: "POST",
                url: url,
                data: params,
                beforeSend: function() {
                    $('#appointmentlist').html(' ');
                    $('#appointmentlist').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                    $('#btn_save').attr('disabled', true);
                    $("#btn_spin").removeClass("fa fa-search");
                    $("#btn_spin").addClass("fa fa-spinner fa-spin");

                },
                success: function(data) {
                    if (data) {
                        $('#appointmentlist').html(data);
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                    }

                },
                complete: function() {
                    $('#btn_save').attr('disabled', false);
                    $('#appointmentlist').LoadingOverlay("hide");
                    $("#btn_spin").removeClass("fa fa-spinner fa-spin");
                    $("#btn_spin").addClass("fa fa-search");

                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                },
            });
            return false;
        });
    });
</script>
