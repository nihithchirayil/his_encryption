
<div class="table-responsive">
 <table class="table table-striped table-bordered">
    <thead style="background-color: #26B99A;">
        <th class="th">UHID</th>
        <th class="th">Patient Name</th>
        <th class="th">Gender</th>
        <th class="th">Dt.Of Birth</th>
        <th class="th">Mobile</th>
        <th class="th">Age</th>
        <th class="th">Reg.Date</th>
        <th class="th">Visit St</th>
        <th class="th">Ward</th>
        <th class="th">Bed</th>
        <th class="th">IP Status</th>
        <th class="th">Visit</th>
        <th class="th">Nurse</th>
        <th class="th">Document</th>
    </thead>


    <tbody id="foo" style="display:contents;" class="">

                    <tr>
                        @if(isset($res) && sizeof($res) > 0)
                        @foreach ($res as $item)
                        <td class="td">{{$item->uhid}}</td>
                        <td class="td">{{$item->patient_name}}</td>
                        <td class="td">{{$item->gender}}</td>
                        <td class="td">{{date('d-M-Y ',strtotime($item->dob))}}
                        <td class="td">{{$item->phone}}</td>
                        <td class="td">{{$item->age}}</td>
                        <td class="td">{{date('d-M-Y ',strtotime($item->reg))}}</td>
                        <td class="td">{{$item->visit_status}}</td>
                         @if((isset($item->ward))&& (isset($item->bed_name)))
                         <td class="td">{{$item->ward}}</td>
                         <td class="td">{{$item->bed_name}}</td>
                         @else
                         <td class="td"></td>
                         <td class="td"></td>
                         @endif


                        <td class="td">{{$item->ip_status}}</td>
                        @if ($item->visit_status =='IP')
                        <td class="td"onclick="patient_visit({{$item->id}});"><i class="fa fa-list" ></i></td>
                        <td class="td"onclick="nurse_patient({{$item->id}});"><i class="fa fa-user-md" ></i></td>
                        @else
                        <td class="td"><i class="fa fa-list" ></i></td>
                        <td class="td""><i class="fa fa-user-md" ></i></td>
                        @endif
                        <td class="td" style="text-align: center"><i class="fa fa-upload"></i></td>

                     </tr>

                @endforeach



             @else
                <tr>
                    <td colspan="4" style="box-shadow: 0px 0px 1px!important;">No Data Found</td>
                </tr>
            @endif

        </tbody>


</table>
<div class="clearfix"></div>
<div class="col-md-12 text-center">
    <nav style="text-align:right;">{{ $res->appends(array_merge(request()->all()))->links() }}
    </nav>
</div>
</div>
