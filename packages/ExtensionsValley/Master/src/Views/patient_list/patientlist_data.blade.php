    <div class="col-md-12" id="result_container_div" style="padding: 0px">
          
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="box_header"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='3%'>Sn. No.</th>
                        <th width='13%'>Uhid</th>
                        <th width='13%'><i class="fa fa-user"></i></th>
                       
                    </tr>


                </thead>
                <tbody>
                    @if (count($res) != 0)
                        @foreach ($res as $data)
                            <tr>
                                <td class="td_common_numeric_rules">{{ $loop->iteration }}.</td>
                                <td class="common_td_rules">{{ $data->patient_name }}</td>
                                <td onclick="dispalyPatientProfile({{$data->id}})"> <i class="fa fa-user"></i></td>
                            </tr>


                        @endforeach
                    @else
                        <tr>
                            <td colspan="11" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
    </div>
