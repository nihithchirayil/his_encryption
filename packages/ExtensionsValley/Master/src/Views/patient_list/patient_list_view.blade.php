@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">

    <style>
        .box_header {
            background: #5397b1 !important;
            color: #FFF !important;
        }
        .ajaxSearchBox {
            display: none;
            text-align: left;
            list-style: none;
            cursor: pointer;
            max-height: 200px;
            margin: 0px 0px 0px 0px;
            overflow-y: auto;
            width: 34%;
            z-index: 599;
            position: absolute;
            background: #ffffff;
            border-radius: 3px;
            border: 1px solid rgba(0, 0, 0, 0.3);

        }
        h6{
            font-size: 14px;
            padding: -7px !important;
            margin: 3px;padding: 1px;"
        }
        #pr_listing {
        text-align: center;
        color: #6f727559;
        font-size: 36px;

    }


        label {}

        table td {
            position: relative !important;
        }

        .mate-input-box {
            width: 100%;
            position: relative;
            padding: 15px 4px 4px 4px !important;
            border-bottom: 2px solid #01A881;
            box-shadow: 0 0 3px #ccc;
            border-radius: 6px 6px 0 0;
            margin-bottom: 10px;
            height: 40px !important;
        }
      

    </style>
@endsection
@section('content-area')
    <div class="right_col">
        <div class="row" style="margin-bottom: 10px"> </div>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
        <div class=" col-md-12 row codfox_container">
            <div class="col-md-6 padding_sm content" style="margin-top:-16px;">
                <h4 class="blue "><strong><?= $title ?></strong></h4>
            </div>


        </div>
        <div class="clearfix"></div>
        <div id="filter_area" class="col-md-12" style="padding-left:0px !important;width: 101.75%;margin-left: -10px;">
            <div class="box no-border no-margin">
                <div class="box-body" style="padding-bottom:15px;">
                    <div class="row">
                        <div class="col-md-12 padding_sm" style="">
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">UHID</label>
                                    <input class="form-control hidden_search reset" value="" autocomplete="off" type="text"
                                        id="uhid" name="uhid" />
                                    <div id="uhidAjaxDiv" class="ajaxSearchBox"></div>
                                    <input class="filters" value="" type="hidden" name="uhid_hidden" value=""
                                        id="bill_no_hidden">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">Patient Name</label>
                                    <input class="form-control hidden_search reset" value="" autocomplete="off" type="text"
                                        id="patient" name="patient" />
                                    <div id="patientAjaxDiv" class="ajaxSearchBox"></div>
                                    <input class="filters" value="" type="hidden" name="patient_hidden" value=""
                                        id="patient_hidden">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">IP Number</label>
                                    <input class="form-control hidden_search reset" value="" autocomplete="off" type="text"
                                        id="patient" name="patient" />
                                    <div id="patientAjaxDiv" class="ajaxSearchBox"></div>
                                    <input class="filters" value="" type="hidden" name="patient_hidden" value=""
                                        id="patient_hidden">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">Mobile Number</label>
                                    <input class="form-control  reset" value="" autocomplete="off" type="text"
                                        onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="number"
                                        name="number" />

                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">Area</label>
                                    <input class="form-control reset" value="" autocomplete="off" type="text" id="area"
                                        name="area" />

                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">Address</label>
                                    <input class="form-control reset" value="" autocomplete="off" type="text" id="address"
                                        name="address" />

                                </div>
                            </div>


                        </div>
                        <div class="col-md-12 padding_sm" style="">
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">Gender</label>
                                    <select name="gender" class="form-control reset" id="gender">
                                        <option value="">Select</option>


                                    </select>
                                </div>

                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">Current Bed</label>
                                    <input class="form-control  reset" value="" autocomplete="off" type="text" id="bed"
                                        name="bed" />

                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">Addtional Bed</label>
                                    <input class="form-control  reset" value="" autocomplete="off" type="text" id="add_bed"
                                        name="add_bed" />

                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">Parent Company</label>

                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">Company</label>

                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label class="green">From Date</label>
                                    <input type="text" data-attr="date" autocomplete="off" name="from_date"
                                        value="{{ date('M-d-y') }}" class="form-control datepicker filters reset"
                                        placeholder="YYYY-MM-DD" id="from_date">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label class="green">To Date</label>
                                    <input type="text" data-attr="date" autocomplete="off" name="to_date"
                                        value="{{ date('M-d-y') }}" class="form-control datepicker filters reset"
                                        placeholder="YYYY-MM-DD" id="to_date">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">Blood</label>
                                    <select name="blood" class="form-control reset" id="blood">
                                        <option value="">Select</option>


                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label green">Visit Type</label>
                                    <select name="visit" class="form-control reset" id="visit">
                                        <option value="">Select</option>


                                    </select>
                                </div>
                            </div>



                        </div>
                        <div class="col-md-12">

                            <div class="col-md-2" style="padding:0px">
                                <div class="col-md-6" style="padding:0px">
                                    <div class="mate-input-box">
                                        <label class="filter_label green">Expired</label>
                                        <select name="gender" class="form-control reset" id="gender">
                                            <option value="">Select</option>
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                            <option value="3">All</option>


                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6" style="padding:0px">
                                    <div class="mate-input-box">
                                        <label class="filter_label green">Merged</label>
                                        <select name="gender" class="form-control reset" id="gender">
                                            <option value="">Select</option>
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                            <option value="3">All</option>


                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-10">
                                <div class="col-md-2 padding_sm share">
                                    <div class="checkbox checkbox-success inline">
                                        <input id="shr" type="checkbox" name="shr">
                                        <label class="text-blue" for="shr">
                                            Share Holder
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm reserved">
                                    <div class="checkbox checkbox-success inline">
                                        <input id="res" type="checkbox" name="res">
                                        <label class="text-blue " for="res">
                                            Reserved Patients
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm insurance">
                                    <div class="checkbox checkbox-success inline">
                                        <input id="ins" type="checkbox" name="ins">
                                        <label class="text-blue " for="ins">
                                            Insured Patients

                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm employee">
                                    <div class="checkbox checkbox-success inline">
                                        <input id="emp" type="checkbox" name="emp">
                                        <label class="text-blue " for="emp">
                                            Employee
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-2 pull-right" style="">
                                    <button type="button" title="Search" class="btn btn-primary btn-block "
                                        id="patient_btn" onclick="searchPatientList()"><i id="patient_spin"
                                            class="padding_sm fa fa-search"></i>Search Patients</button>
                                </div>
                                <div class="col-md-2 pull-right" style="">

                                    <button type="button" title="Reset" class="btn btn-default  btn-block " id=""
                                        onclick="reset()"><i id="" class="padding_sm fa fa-refresh"></i>Refresh</button>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-12 " style="height: 226px;padding: 0px;margin-top: 4px;width: 101%;margin-left: -9px;"
        id="patient_list_box">
        <div class="box no-border">
            <div class="box-body clearfix">
                <div id="patient_list_container" style="text-align: center;height:365px;">
                  
                                        <i class="fa fa-users" id="change_color" style="width: 58%;font-size: 365px;height: 52%;color: whitesmoke;"></i>

                </div>
            </div>
        </div>



    </div>
    </div>



    <!-- Modal -->
    <div id="patient_profile" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="width: 1250px;margin-left: -295px;height: 625px;margin-top: -28px;">
                <div class="modal-header modal-header-sm box_header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h6 class="modal-title"> <i class="fa fa-user-circle padding_sm" style="font-size: 23px;"></i><b>Patient Profile</b></h6>
                </div>

                <div class="modal-body" style="padding: 0px">
                    <div class="col-md-12 padding_sm" style="padding: -1px;">
                        <div class="box no-border">
                                <div class="col-md-2" style="height: 571px;padding:0px;margin-top: 3px;box-shadow: 0px 0px 3px 0px lightblue;"
                                id="profile_content">
                                <div class="col-md-12"><h4 class="hover-underline-animation pull-right" ">OP</h4></div>
                                <div class="col-md-12" style="width: 100%;border:2px solid lightblue;border-radius:302px;margin-top: 15px;">
                                    <img id="candidate_image" src="https://cdn.balkan.app/shared/empty-img-none.svg"
                                    alt="Candidate Image" style="width:102%;height:43%;">
                                </div>
                               <div class="col-md-12" style="margin-top: 30px;padding: 2px;">
                                <div class="col-md-12"><i class="fa fa-id-card"></i><h6 class="hover-underline-animation pull-right" >DHGP/065367887</h6></div>
                                <div class="col-md-12"> <i class="fa fa-user"></i><h6 class="hover-underline-animation pull-right" >Mr.DUMMY PATIENT 01</h6></div>
                                <div class="col-md-12"><i class="fa fa-gender"></i><h6 class="hover-underline-animation pull-right" >MALE</h6></div>
                                <div class="col-md-12"><i class="fa fa-birthday-cake"></i><h6 class="hover-underline-animation pull-right" >MALE</h6></div>
                                <div class="col-md-12"><i class="fa fa-envelope"></i><h6 class="hover-underline-animation pull-right" >dshvd@jhdj.com</h6></div>
                                <div class="col-md-12"><i class="fa fa-phone"></i><h6 class="hover-underline-animation pull-right" >9898988998</h6></div>
                                <div class="col-md-12"><i class="fa fa-globe"></i><h6 class="hover-underline-animation pull-right" >asdjsdjk sdhshd,sadshd</h6></div>
                               </div>

                                </div>

                                <div class="col-md-10" style="padding:0px;height: 524px;">
                                    
                                    <div class="col-md-12 padding_sm" style="height: 524px">
                                        <div class="col-md-12 ">
                                            <div class="col-md-12" id="profile_nav" style="padding:0px">
                                                <ul id="progressbar" class="nav nav-tabs"
                                                    style="cursor: pointer;font-size:14px;margin-left: -11px;padding:0px;width: 102.25%;"
                                                    role="tablist">
                                                    <li id="doctor_detail" style="display: inline !important;padding:-4px;">
                                                        <a id="doctor_detail-tab" href="#doctor_detail_tab" role="tab"
                                                            aria-controls="doctor_detail" aria-selected="true"><strong>COMMON DETAILS</strong></a>
                                                    </li>
                                                    <li id="op_pricing" style="display: inline !important;padding:-4px;">
                                                        <a id="op_pricing-tab" href="#op_pricing_tab" role="tab"
                                                            aria-controls="op_pricing" aria-selected="true"><strong>VISIT DETAILS</strong></a>
                                                    </li>
                                                    <li id="ip_pricing" style="display: inline !important;padding:-4px;">
                                                        <a id="ip_pricing-tab" href="#ip_pricing_tab" role="tab"
                                                            aria-controls="ip_pricing" aria-selected="true"><strong>ADMISSION DETAILS</strong></a>
                                                    </li>
                                                    <li id="leave_block" style="display: inline !important;padding:-4px;">
                                                        <a id="leave_block-tab" href="#leave_block_tab" role="tab"
                                                            aria-controls="leave_block" aria-selected="true"><strong>ALL BILLS</strong></a>
                                                    </li>
                                                    <li id="leave_block" style="display: inline !important;padding:-4px;">
                                                        <a id="leave_block-tab" href="#leave_block_tab" role="tab"
                                                            aria-controls="leave_block" aria-selected="true"><strong>INSURANCE DETAILS</strong></a>
                                                    </li>
                                                    <li id="leave_block" style="display: inline !important;padding:-4px;">
                                                        <a id="leave_block-tab" href="#leave_block_tab" role="tab"
                                                            aria-controls="leave_block" aria-selected="true"><strong>ADVANCE COLLECTION</strong></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="padding:0px;height: 524px;margin-top:3px;box-shadow: 0px 0px 3px 0px lightblue;"
                                            id="profile_content">
                                            <div class="tab-content">
                                                <div class="tab-pane fade " id="doctor_detail_tab" role="tabpanel" style=""
                                                    aria-labelledby="nav-doctor_detail-tab">
                                                   
                                                </div>
                                                <div class="tab-pane fade" id="op_pricing_tab" role="tabpanel" style=""
                                                    aria-labelledby="nav-op_pricing-tab">
                                                   
                                                </div>
                                                <div class="tab-pane fade" id="ip_pricing_tab" role="tabpanel" style=""
                                                    aria-labelledby="nav-ip_pricing-tab">
                                                </div>
                                                <div class="tab-pane" id="leave_block_tab" role="tabpanel" style=""
                                                    aria-labelledby="nav-leave_block-tab">
                                                   
                                                </div>
                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    
                        </div>
                    
                    
                    
                    </div>
                </div>
              
            </div>

        </div>
    </div>

@stop
@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/js/patientlist.js') }}"></script>
@endsection