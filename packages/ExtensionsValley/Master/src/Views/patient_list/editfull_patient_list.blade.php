<ul class="nav nav-tabs" role="tablist" id="tabs-nav">
    <li class="nav-item active">
        <a class="nav-link" id="model_patient_basic_details" data-toggle="tab" href="#patient_basic_details_tab"
            role="tab" aria-controls="patient_basic_details_tab" aria-selected="false" aria-expanded="true">Patient
            Basic Details</a>
    </li>
</ul>

<div id="tabs-content" class="no-padding">
    @php
    $patient_name = isset($patient_list[0]->patient_name) ? $patient_list[0]->patient_name : '';
    $gender_select = isset($patient_list[0]->gender) ? $patient_list[0]->gender : '';
    $area = isset($patient_list[0]->area) ? $patient_list[0]->area : '';
    $address = isset($patient_list[0]->address) ? $patient_list[0]->address : '';
    $country_select = isset($patient_list[0]->country) ? $patient_list[0]->country : '';
    $pincode = isset($patient_list[0]->pincode) ? $patient_list[0]->pincode : '';
    $phone = isset($patient_list[0]->phone) ? $patient_list[0]->phone : '';
    $email = isset($patient_list[0]->email) ? $patient_list[0]->email : '';
    $state_select = isset($patient_list[0]->state) ? $patient_list[0]->state : '';
    $patient_image_url = isset($patient_list[0]->patient_image_url) ? $patient_list[0]->patient_image_url : '';
    $patient_dob = isset($patient_list[0]->dob) ? $patient_list[0]->dob : '';
    $patient_title = isset($patient_list[0]->title) ? trim($patient_list[0]->title) : '';   
    @endphp
    <div id="patient_basic_details_tab" class="tab-content tab-content1" style="display: block;">
        <div class="col-md-12 padding_sm">
            <div class="col-md-3 padding_sm">
                <div class="pt_profile_body text-center" style="margin: 15px 0px">
                    @php
                    if ($patient_image_url) {
                    $img_url = url('/') . '/packages/extensionsvalley/patient_profile/' . $patient_image_url;
                    }
                    else {
                    $url = URL::to('/') . '/packages/extensionsvalley/patient_profile/default_img.png';
                    $img_url = str_replace('public/', '', $url);
                    }
                    @endphp
                    <img src="{{ $img_url }}" alt='patient Profile' width='150' height='150' style='margin:-7px;'>
                </div>
                <div class="mate-input-box ">
                    <label for="">Patient Image</label>
                    <input class="form-control blue" value="" id="pt_image" type="file" name="pt_image">
                </div>
            </div>
            <div class="col-md-9 no-padding" style="margin-top: 10px">
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box">
                        <label>Salutation</label>
                        <div class="clearfix"></div>
                        <select onchange="getPatientGender()" class="form-control select2"  name ="txt_patientsalutation" id="txt_patientsalutation">
                            <option value="">Select</option>
                            <?php
                                foreach ($salutation as $each) {
                                    $selected = "";
                                    if (strtoupper($each->title) == strtoupper($patient_title)){
                                    $selected = "selected";
                                    }
                            ?>
                            <option {{$selected}} data-sex="<?= $each->sex ?>" value="<?= $each->title ?>">
                                <?= $each->title ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box">
                        <label>Patient Name</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control" name="txt_patient_name" id="txt_patient_name"
                            value="{{$patient_name}}" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box">
                        <label>Gender</label>
                        <div class="clearfix"></div>
                        <select class="form-control select2" name="txt_patient_gender" id="txt_patient_gender">
                            <option value="">Select</option>
                            <?php
                                foreach ($gender as $each) {
                                $selected = "";
                                if (strtoupper($each->id) == strtoupper($gender_select)){
                                $selected = "selected";
                                }
                            ?>
                            <option {{$selected}} data-code="<?= $each->code ?>" value="<?= $each->id ?>">
                                <?= $each->name ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box">
                        <label>DOB</label>
                        <div class="clearfix"></div>
                        <input autocomplete="off" type="text" class="form-control txt_patient_date"
                            name="txt_patient_dob" id="txt_patient_dob" value="{{$patient_dob}}" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box">
                        <label>Mobile No.</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control" name="txt_patient_mobile" id="txt_patient_mobile"
                            value="{{$phone}}"
                            oninput="this.value = this.value.replace(/[^0-9 +]/g, '').replace(/(\..*)\./g, '$1');"
                            autocomplete="off">
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box">
                        <label>Email</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control" name="txt_patient_email" id="txt_patient_email"
                            value="{{$email}}" onblur="isEmail()" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box">
                        <label>Address</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control" name="txt_patient_address" id="txt_patient_address"
                            value="{{$address}}" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box">
                        <label>Country</label>
                        <div class="clearfix"></div>
                        <select onchange="getPatientAddress(1)" class="form-control select2" name="txt_patient_country"
                            id="txt_patient_country">
                            <option value="">Select</option>
                            <?php
                                foreach ($country as $each) {
                                $selected = "";
                                if (strtoupper($each->name) == strtoupper($country_select)){
                                $selected = "selected";
                                }
                                ?>
                            <option <?= $selected ?> value="<?= $each->id ?>"><?= $each->name ?>
                            </option>
                            <?php
                                }
                           ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box">
                        <label>State</label>
                        <div class="clearfix"></div>
                        <select name="txt_patient_state" id="txt_patient_state" class="form-control select2"
                            id="patient_state">
                            <option value="">Select</option>
                            <?php
                                foreach ($state as $each) {
                                    $selected = "";
                                    if(strtolower($each->name) == strtolower($state_select)){
                                        $selected = "selected";
                                    }
                            ?>
                            <option <?= $selected ?> value="<?= $each->id ?>"><?= $each->name ?>
                            </option>
                            <?php
                               }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box">
                        <label>Area</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control" name="txt_patient_area" id="txt_patient_area"
                            value="{{$area}}" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="mate-input-box">
                        <label>PinCode</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control" name="txt_patient_pincode" id="txt_patient_pincode"
                            value="{{$pincode}}"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            autocomplete="off">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

</script>