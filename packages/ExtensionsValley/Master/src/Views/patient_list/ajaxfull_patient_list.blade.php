@php
$page = $patient_list->currentPage();
$per_page = $patient_list->perPage();
$set_sl_no = $per_page * $page - $per_page + 1;

@endphp
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th>Sl.No.</th>
                <th>UHID</th>
                <th>Patient Name</th>
                <th>Gender</th>
                <th>IP No.</th>
                <th>Type</th>
                <th>Email</th>
                <th>Mobile No.</th>
                <th>DOB</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody class="theadscroll" style="position: relative; max-height: 50vh;">
            <?php
                if (count($patient_list) != 0) {
                    foreach ($patient_list as $list) {
                        ?>
            <tr>
                <td>{{ $set_sl_no }}</td>
                <td title="{{ $list->uhid ? $list->uhid : '-' }}">
                    {{ $list->uhid ? $list->uhid : '-' }}</td>
                <td>{{ $list->patient_name ? $list->patient_name : '-' }}</td>
                <td>{{ $list->gender ? $list->gender : '-' }}</td>
                <td>{{ $list->admission_no ? $list->admission_no : '-' }}</td>
                <td>{{ $list->current_visit_type }}</td>
                <td>{{ $list->email ? $list->email : '-' }}</td>
                <td>{{ $list->phone ? $list->phone : '-' }}</td>
                <td>{{ $list->dob ? $list->dob : '-' }}</td>
                <td><button type="button" id="button" class="btn btn-sm btn-warning "
                        onclick="editPatientData(this,'{{$list->patient_id}}');"><i class="fa fa-edit"></i>
                        Edit</button></td>
            </tr>
            @php
            $set_sl_no++;
            @endphp
            <?php
                 }
                    } else {
            ?>
            <tr>
                <td style="text-align: center" colspan="11" class="re-records-found">No Records Found</td>
            </tr>
            <?php
                }
            ?>
        </tbody>
    </table>

<div class="clearfix"></div>
<div class="col-md-12 padding_sm text-center">
    <ul class="pagination purple_pagination pull-right">
        {!! $page_links !!}
    </ul>
</div>