@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">

<style>
/* Tabs */
.nav-tabs>li.active>a,
.nav-tabs>li.active>a:focus,
.nav-tabs>li.active>a:hover {
    color: #01987a;
    cursor: default;
    background-color: #fff;
    border: 1px solid #01987a;
    border-bottom-color: #01987a;
    height: 40px !important;
    font-weight: bold;
}

table tr:hover td {
    text-overflow: initial;
    white-space: normal;
}
</style>
@endsection
@section('content-area')

<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="token" value="{{ csrf_token() }}">
<input type="hidden" id="hidden_patient_id" value="0">

<div class="modal fade" id="getPatientListModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title"> Patient Details</h4>
            </div>
            {!! Form::open(['id' => 'savePatientDetailsForm', 'name' => 'savePatientDetailsForm', 'method' => 'post',
            'file' =>
            'true', 'enctype' => 'multipart/form-data']) !!}
            <div class="modal-body" id="getPatientListModelBody">

            </div>
            <div class="modal-footer" style="margin-top: -40px;">
                <button style="padding: 3px 3px" class="btn btn-info" id="btnSave_patientDetails"><i class="fa fa-save"
                        type="submit"></i> Save
                </button>
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>

            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="right_col">
    <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 no-padding">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>UHID</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control hidden_search" id="searchpatient_uhid"
                                    autocomplete="off">
                                <div id="searchpatient_uhidAjaxDiv" class="ajaxSearchBox"></div>
                                <input class="filters reset" value="" type="hidden" name="searchpatient_uhid_hidden"
                                    value="" id="searchpatient_uhid_hidden">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Patient Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control hidden_search" id="searchpatient_name"
                                    autocomplete="off">
                                <div id="searchpatient_nameAjaxDiv" class="ajaxSearchBox"></div>
                                <input class="filters reset" value="" type="hidden" name="searchpatient_name_hidden"
                                    value="" id="searchpatient_name_hidden">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>IP No.</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_ipno" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Visit Type</label>
                                <div class="clearfix"></div>
                                <select class="form-control select2" id="searchpatient_type">
                                    <option value="All">Select</option>
                                    <option value="OP">OP</option>
                                    <option value="IP">IP</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Mobile No.</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_mobile" autocomplete="off"
                                    oninput="this.value = this.value.replace(/[^0-9 +]/g, '').replace(/(\..*)\./g, '$1');">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Email</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_email" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Address</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_address" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Country</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_country" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>State</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_state" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Area</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_area" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>PinCode</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="searchpatient_pincode" autocomplete="off"
                                    oninput="this.value = this.value.replace(/[^0-9 +]/g, '').replace(/(\..*)\./g, '$1');">
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 25px">
                            <button type="button" class="btn btn-warning btn-block"
                                onclick="resetAdvancePatientSearch()">Reset <i class="fa fa-recycle"></i>
                            </button>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 25px">
                            <button type="button" class="btn btn-primary btn-block PatientSearchBtn">Search <i
                                    class="fa fa-search"></i>
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="box no-border no-margin">
            <div class="box-body clearfix">
                <div class="listPatientDataDivs theadscroll table_body_contents" data-search-url="{{ $search_url }}"
                    data-action-url="{{ $search_url }}" style="position: relative; max-height: 60vh;"
                    id="listPatientDataDivs">

                </div>
            </div>
        </div>
    </div>
</div>


@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/patient_list.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">
@include('Purchase::messagetemplate')
</script>

@endsection