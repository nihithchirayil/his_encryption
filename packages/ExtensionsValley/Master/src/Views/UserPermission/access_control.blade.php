<style>
.fixed_row{
    background: #56847c;
    height: 25px;
    color: white;
}
.scroll_row {
    float: left;
    height: 285px;
    overflow-x: hidden;
    overflow-y: auto;
    width: 100%;
}
ul.menu_container {
    font-size: 13px;
    margin: 0;
    padding: 0;
}

ul li.mainmenu{
    list-style-type: none;
    padding: 5px 0;
    float: left;
    width: 100%;
    margin: 1px 0;
    background: #f5f5f5;
}
ul.subheading {
    float: left;
    margin: 10px 0 0 0;
    padding: 0 0 0 30px;
    width: 100%;
}
ul.subheading li {
    /* background: #e0e0e0; */
    height: 30px;
    border: 1px solid #fffcfc;
    list-style-type: none;
}
.minusbtn, .plusbtn {
    cursor: pointer;
    font-size: 16px;
    margin: 2px 0 0;
}
.bottom-border-text{
    border: none !important;
    border-bottom: 1px solid rgb(53, 137, 255) !important;
    box-shadow: none;
}
.box-body,.card_body{
    background: white !important;
}
.select2-container{
    box-shadow:none;
    border-bottom: 2px solid #cbe6ff !important;
}

.box-body{
    border-color:#cbe6ff !important;
}

.fixed_row{
    background:#01987a !important;
}

ul li.mainmenu{
    background:#fff !important;
    border-collapse: collapse !important;
}
.menu_list_continer_table>span{
    font-size: 12px !important;
    font-weight: 600 !important;
    color: darkslategrey !important;
}
.slider{
    background-color:#01987a !important;
}
.badge{
    background-color:#fff !important;
}
</style>
<div class="col-md-2" style="padding-top: 15px !important;border: 1px solid #E6E9ED;width:20% !important">
    <div class="col-md-12" style="margin-top:10px;">
        <label for="is_desktop_check"><b>Is Desktop</b></label>
        <div class="clearfix"></div>
        <label class="switch">
            <input type="checkbox" id="is_desktop_check" class="is_desktop" value="1" onchange="ajaxAclList();">
            <span class="slider round"></span>
        </label>
    </div>
    <div class="theadscroll" style="position: relative; height: 550px;">
        <table class="table no-margin theadfix_wrapper table-striped  table-condensed"
               style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @if(count($user_groups) > 0)
                @foreach($user_groups as $ug)
                <tr style="cursor: pointer;" id="alter_mnu_{{$ug->group_id}}" class="alter_mnu">
                    <th class="common_td_rules" style="text-align: left !important" data-display="2" onclick="alterMenuAccess('{{$ug->group_id}}')">
                        <b>{{$ug->name}}</b>
                    </th>
                </tr>
                @foreach($groupSql as $gs)
                @if($gs->group_id == $ug->group_id)
                <tr style="cursor: pointer;display: none;" class="groups_{{$ug->group_id}}">
                    <td class="common_td_rules" colspan="2">{{$gs->name}}</td>
                </tr>
                @endif
                @endforeach
                @endforeach
                @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>

</div>
<div class="col-md-8" style="width:60% !important">
    <input type="hidden" id="role_id" value="">
<div style="background-color:#fff !important;">
    <div class="col-md-12" style="border-radius:5px;margin-top:10px;">
            <div class="table-responsive menu_list_continer_table" style="padding:0px !important;">
                <div class="mainmenu_container">
                    <div class="fixed_row" style="border-top-left-radius:5px; border-top-right-radius:7px;">
                        <div class="col-md-2" style="padding:3px 0px 0px 10px;">Sl.No</div>
                        <div class="col-xs-8" style="padding: 3px 0px;">Menu</div>
                        <div class="col-xs-2" style="padding:0px 0px 0px 30px;">
                            <input type="checkbox" onchange="allCheckMenu(this,'view')">
                            <label style="font-size:13px;">Select All</label>
                        </div>
                        {{-- <div class="col-xs-1" style="padding-left: 9px;"><input type="checkbox" onchange="allCheckMenu(this,'add')"> Add</div>
                        <div class="col-xs-1" style="padding-left: 8px;"><input type="checkbox" onchange="allCheckMenu(this,'edit')"> Edit</div> --}}
                        <!-- <div class="col-xs-2" style="padding-left: 7px;"><input type="checkbox" onchange="allCheckMenu(this,'level1')"> Approval Level 1</div>
                        <div class="col-xs-2" style="padding-left: 5px;"><input type="checkbox" onchange="allCheckMenu(this,'level2')"> Approval Level 2</div> -->
                    </div>
                    <div class="scroll_row theadscroll" id="aclListBody" style='height:370px;position: relative;'>

                    </div>
                    <div class="col-md-12 hide" style="padding:10px 30px;">
                        <?php if(isset($accessPerm['edit']) && ($accessPerm['edit']==1)){ ?>
                            <input type="button" onclick="resetAclList();" style="float: right; margin-right: 10px;" value='Reset' class="btn btn-primary">
                        <?php } ?>
                    </div>
                </div>
            </div>
    </div>
</div>
</div>
<div class="col-md-2" style="padding-top: 15px !important;border: 1px solid #E6E9ED" id="current_menu_list">
    <div class="theadscroll" style="position: relative; height: 550px;">
        <table class="table no-margin theadfix_wrapper table-striped  table-condensed"
               style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th>Menu</th>
                </tr>
            </thead>
            <tbody>
        </table>
    </div>
</div>
    {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
<script type="text/javascript">
    function showHideControls2(fieldId,fieldType){
        var fieldText = '';
        if(fieldType === 'combo'){
            fieldText =  $('#'+fieldId+' :selected').text();
        }else if(fieldType === 'text'){
            fieldText =  $('#'+fieldId).val();
        }else if(fieldType === 'checkbox'){

            var chkStatus = $(fieldId).is(':checked');
            var chkclass = $(fieldId).prop('class');
            var element_parent = $(fieldId).parent().parent();
            var id = $(element_parent).prop('id').split('_')[1];

            if(chkStatus == true){
                $(element_parent).children().find('.' + chkclass).prop('checked', true);
                while(id > 0){
                    $('#' + chkclass + '_' + id).prop('checked', true);
                    var head = $('#' + chkclass + '_' + id).parent().parent();
                    id =  $(head).prop('id').split('_')[1];
                }
            }
            else{
                $(element_parent).children().find('.' + chkclass).prop('checked', false);
                var check_status = true;
                while((id > 0 ) && check_status) {
                    var checksiblings = $('#' + chkclass + '_' + id).parent().siblings().find('.' + chkclass).is(':checked');
                    if(checksiblings == false) {
                        $('#' + chkclass + '_' + id).prop('checked', false);
                        var head = $('#' + chkclass + '_' + id).parent().parent();
                        id =  $(head).prop('id').split('_')[1];
                    } else {
                        check_status = false;
                    }
                }
            }
            if($("#role_id").val() >  0 && $("#role_id").val() != ""){
                saveAclData(fieldId,fieldType);
            }

        }

    }

    var multi_select_list = new Array();
    function saveAclData(element,field_type){

        var el_id = jQuery(element).attr('id');
        var menu_id = jQuery(element).attr('data-menuid');
        var action_type = jQuery(element).attr('data-actiontype');
        var action_type_name = action_type.split('_')[1] ? action_type.split('_')[1]: action_type;
        var action_val = 0;
        if(jQuery(element).is(':checked')){
            action_val = 1;
        }

        if($("#parient_"+menu_id).length > 0){

            multi_select_list.push({'menu_id': menu_id
                ,'action_type':action_type
                ,'action_val':action_val
                ,'role_id':$("#role_id").val()
            });


            $("#parient_"+menu_id).find('.'+action_type_name).each(function(){

                multi_select_list.push({'menu_id': $(this).attr('data-menuid')
                    ,'action_type':action_type
                    ,'action_val':action_val
                    ,'role_id':$("#role_id").val()
                });
            });

            acldata = multi_select_list;

        } else {
                parent_id = jQuery(element).attr('data-parentid');

                if(parent_id > 0 && action_val == 1){
                    parent_parent =  jQuery("#"+action_type_name+"_"+parent_id).attr('data-parentid');
                    multi_select_list.push({'menu_id': parent_parent
                        ,'action_type':action_type
                        ,'action_val':action_val
                        ,'role_id':$("#role_id").val()
                    });
                }

                multi_select_list.push({'menu_id': menu_id
                    ,'action_type':action_type
                    ,'action_val':action_val
                    ,'role_id':$("#role_id").val()
                });

                if(action_val == 1){
                    multi_select_list.push({'menu_id': parent_id
                        ,'action_type':action_type
                        ,'action_val':action_val
                        ,'role_id':$("#role_id").val()
                    });
                }
                acldata = multi_select_list;
        }

        var datalist = JSONH.pack(acldata)
        datalist = JSON.stringify(datalist);

        var url = "{{route('extensionsvalley.admin.acldataupdate')}}";
        $.ajax({
            type: "POST",
            url: url,
            data:{acldata:datalist,_token:"{{ csrf_token() }}"},
            beforeSend: function () {
                $("#aclListBody").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (responsedata) {
                if(responsedata == 1){
                    Command: toastr["success"]('Saved Successfully!');
                }
            multi_select_list = [];
            },
            complete: function () {
                $("#aclListBody").LoadingOverlay("hide");
            }
        });
    }

    /* ACList Reset Function*/
    function resetAclList() {
        var confirm_reset = confirm("Do you really want to reset?");
        if(confirm_reset) {
            $('#allview').prop('checked', false);
            $('#alladd').prop('checked', false);
            $('#alledit').prop('checked', false);
            $('#alllevel1').prop('checked', false);
            $('#alllevel2').prop('checked', false);
            ajaxAclList();
        }
    }
    function ajaxAclList() {
        var  url = "{{route('extensionsvalley.admin.accesscontrol')}}";
        var role_id = $('#role_id').val();
        var is_desktop = 0;
        if($(".is_desktop").prop('checked')){
            is_desktop = 1;
        }
        if (role_id == "") {
            $("#aclListBody").html("");
        } else {
            $.ajax({
                type: "GET",
                url: url,
                data: "role_id=" + role_id+ "&is_desktop="+is_desktop,
                beforeSend: function () {
                    $("#aclListBody").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
                },
                success: function (aclData) {
                    $("#aclListBody").html(aclData);
                    $('.menu_list_continer_table').css('background-color','rgb(197, 240, 255)');
                },
                complete: function () {
                currentMenuList(role_id);


                }
            });
        }

    }

    function allCheckMenu(id,class_name) {
        if ($(id).prop('checked')) {
            $('input.' + class_name).prop('checked', true);
            callBulkAjax(class_name,1);
        }
        else {
            $('input.' + class_name).prop('checked', false);
            callBulkAjax(class_name,0);
        }

    }

    function callBulkAjax(action_type,action_val){

        if($("#role_id").val() == 0 || $("#role_id").val() == ""){
            return false;
        }
        var action_type1 = '';
        if(action_type == "view"){
            action_type1 = "has_view";
        } else if (action_type == "add"){
            action_type1 = "has_add";
        } else if (action_type == "edit"){
            action_type1 = "has_edit";
        }
        $("#parient_0").find('.'+action_type).each(function(){
                    multi_select_list.push({'menu_id': $(this).attr('data-menuid'),'action_type':action_type1,'action_val':action_val ,'role_id':$("#role_id").val()});
        });
        acldata = multi_select_list;
        var datalist = JSONH.pack(acldata)
        datalist = JSON.stringify(datalist);
        var url = "{{route('extensionsvalley.admin.acldataupdate')}}";
        $.ajax({
            type: "POST",
            url: url,
            data:{acldata:datalist,_token:"{{ csrf_token() }}"},
            beforeSend: function () {
                $("#aclListBody").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
            },
            success: function (responsedata) {
                if(responsedata == 1){
                    Command: toastr["success"]('Saved Successfully!');
                }
            multi_select_list = [];
            },
            complete: function () {
                $("#aclListBody").LoadingOverlay("hide");
            }
        });
    }

    function menudrop(id,action) {
        $('#' + action + '_' + id).hide();
        if(action == 'plus') {
            $('#minus_' + id).show();
            $('#parient_' + id).slideDown();
        } else {
            $('#plus_' + id).show();
            $('#parient_' + id).slideUp();
        }
    }
    function alterMenuAccess(group_id){
    $(".alter_mnu").css('background-color','#fff');
    $("#role_id").val(group_id);
    $("#role_id").val(group_id);
    $("#group_id_loc").val(group_id);
    $("#group_id_bill").val(group_id);
    $("#alter_mnu_"+group_id).css('background-color','#abc8db');
    ajaxAclList();
}
function currentMenuList(role_id){
    var is_desktop = 0;
        if($(".is_desktop").prop('checked')){
            is_desktop = 1;
        }
$.ajax({
                type: "GET",
                url: "{{route('extensionsvalley.master.fullUserPermissionCurrentMenu')}}",
                data: "group_id=" + role_id+"&is_desktop="+is_desktop,
                beforeSend: function () {
                   //$("#current_menu_list").LoadingOverlay("show", { background  : "rgba(255, 255, 255, 0.7)",imageColor : '#337AB7'});
                },
                success: function (aclData) {
                    $("#current_menu_list").html(aclData);
                   // $("#current_menu_list").LoadingOverlay("hide");
                },
                complete: function () {
                $('.theadscroll').perfectScrollbar({
                    minScrollbarLength: 30,
                    suppressScrollX: true
                });
                $('.theadfix_wrapper').floatThead({
                    position: 'absolute',
                    scrollContainer: true
                });
                $("#aclListBody").LoadingOverlay("hide");
                }
            });
}
</script>
