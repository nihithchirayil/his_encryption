<div class="col-md-6">
<div class="x_panel">
    <input type="hidden" value="{{$group_id}}" id="group_id_bill">          
        <div class="row">
            <div id="parent" class="table-responsive theadscroll" style="position: relative; height: 550px;">
                <table id="fixTable_bill" class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                       style="border: 1px solid #CCC;">
                    <thead>
                        <tr class="table_header_bg">
                            <th style="width:5%" class="common_td_rules">#</th>
                            <th style="width:40%" class="common_td_rules">
                            <input id="issue_search_box_bill" class="issue_search_box" onkeyup="searchbyName('issue_search_box_bill','fixTable_bill');" type="text" placeholder="Name Search.. " style="color: #000;display: none;width: 90%;" >
                            <span id="item_search_btn_bil"  onclick="searchitem('issue_search_box_bill','item_search_btn_text_bill','item_search_btn_bill')" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                            <span id="item_search_btn_text_bill">Name</span>
                        </th>
                            <th style="width:4%">Active</th>
                        </tr>
                    </thead>
                    <tbody class="assignListData">
                        @if(isset($resultData) && count($resultData))
                        <?php $i = 1; ?>
                        @foreach($resultData as $data)
                        <tr>

                            <td class="common_td_rules">{{$i}}</td>
                            <td class="common_td_rules">
                                <span id="child_id-{{$i}}" style="border: none;">{{$data->name}}</span>
                                <input type="hidden" class="ug_id_bill" name="ug_id_bill[]"  value="{{$data->bill_group_id}}">
                            </td>
                            <td title="">
                                <input style="" type="checkbox" name="ass_ug_chk_bill[]" class="ass_ug_chk_bill" <?php
                                if ($data->id != 0) {
                                    echo 'checked';
                                } else {
                                    
                                }
                                ?>  >
                                <input type="hidden" name="hidden_ass_ug_id_bill[]" id="field_view-{{$i}}" value="{{$data->id}}" class="hidden_ass_ug_id_bill" >
                            </td> 

                        </tr> 
                        <?php $i++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">No Items Found.</td>
                        </tr>
                        @endif
                    </tbody>  
                </table>
            </div>
        </div>
    </div>
</div>
 <div class="col-md-2" style="padding-top: 15px !important;border: 1px solid #E6E9ED;width: 20% !important">
        <div class="theadscroll" style="position: relative; height: 550px;">
            <table class="table no-margin theadfix_wrapper table-striped  table-condensed"
                   style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th>{{$group_name}}</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($billTagFlowSql_cur) && count($billTagFlowSql_cur) > 0)           
                    @foreach($billTagFlowSql_cur as $g1s)
                    <tr style="cursor: pointer;" >
                        <td class="common_td_rules">{{$g1s->name}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5" class="vendor_code">No Records found</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>