<div class="x_panel">
        <div class="row">
        <div class="col-md-2" style="padding-top: 15px !important;border: 1px solid #E6E9ED;width: 20% !important">
    <div class="theadscroll" style="position: relative; height: 550px;">
        <table class="table no-margin theadfix_wrapper table-striped  table-condensed"
               style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th>Groups</th>
                </tr>
            </thead>
            <tbody>
                @if(count($user_groups) > 0)
                @foreach($user_groups as $ug)
                <tr style="cursor: pointer;" id="">
                    <th class="common_td_rules" style="text-align: left !important">
                        <b>{{$ug->name}}</b>
                    </th>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-9" style="padding-top: 15px !important;border: 1px solid #E6E9ED;">
        <div id="parent" class="table-responsive theadscroll" style="position: relative; height: 550px;">
            <table id="fixTable_grp" class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                   style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th style="width:15%" class="common_td_rules">#</th>
                        <th width="30%">
                            <input id="issue_search_box_grp" onkeyup="searchbyName('issue_search_box_grp','fixTable_grp');" type="text" placeholder="Name Search.. " style="color: #000;display: none;width: 90%;" >
                            <span id="item_search_btn_grp"  onclick="searchitem('issue_search_box_grp','item_search_btn_text_grp','item_search_btn_grp')" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                            <span id="item_search_btn_text_grp">Name</span>
                        </th> 
                        <th style="width:4%">Active</th>
                    </tr>
                </thead>
                <tbody class="assignListData">
                    @if(isset($resultData) && count($resultData))
                    <?php $i = 1; ?>
                    @foreach($resultData as $data)
                    <tr>

                        <td class="common_td_rules">{{$i}}</td>
                        <td class="common_td_rules">
                            <span id="child_id-{{$i}}" style="border: none;">{{$data->name}}</span>
                            <input type="hidden" class="ug_id_user" name="ug_id_user[]"  value="{{$data->user_group_id}}">
                            <input type="hidden" class="ass_val_user" name="ass_val_user[]"  value="{{$ass_val_user}}">
                        </td>
                        <td title="">
                            <input style="" type="checkbox" name="ass_ug_chk_user[]" class="ass_ug_chk_user" <?php
                            if ($data->id != 0) {
                                echo 'checked';
                            } else {
                                
                            }
                            ?>  >
                            <input type="hidden" name="hidden_ass_ug_id_user[]" id="field_view-{{$i}}" value="{{$data->id}}" class="hidden_ass_ug_id_user" >
                        </td> 

                    </tr> 
<?php $i++; ?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="3">No Items Found.</td>
                    </tr>
                    @endif
                </tbody>  
            </table>
        </div>
    </div>
</div>
</div>
</div>
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
@stop
