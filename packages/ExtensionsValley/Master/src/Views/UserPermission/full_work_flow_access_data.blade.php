<div class="col-md-2" style="padding-top: 15px !important;border: 1px solid #E6E9ED;width: 20% !important">
    <div class="theadscroll" style="position: relative; height: 550px;">
        <table class="table no-margin theadfix_wrapper table-striped  table-condensed"
               style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th>Groups</th>
                </tr>
            </thead>
            <tbody>
                @if(count($user_groups) > 0)
                @foreach($user_groups as $ug)
                <tr style="cursor: pointer;" class="work_flow_row" id="work_flow_id_{{$ug->group_id}}">
                    <th class="common_td_rules" style="text-align: left !important" data-display="2" onclick="alterWorkFlowTag('{{$ug->group_id}}')">
                        <b>{{$ug->name}}</b>
                    </th>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-7" id="assign_work_flow_tag">
    <div class="theadscroll" style="position: relative; height: 350px;">
        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
               style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th>Menu Name</th>
                    <th>Group Name</th>
                    <th>Worksheet</th>
                    <th>Submit</th>
                    <th>Verify</th>
                    <th>Approve</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if(count($workFlowSql) > 0)
                @foreach ($workFlowSql as $menu)
                <tr style="cursor: pointer;" onclick="editLoadData(this,
                    '{{$menu->id}}',
                    '{{$menu->acl_key}}',
                    '{{$menu->group_id}}',
                    '{{$menu->worksheet}}',
                    '{{$menu->submit}}',
                    '{{$menu->verify}}',
                    '{{$menu->approve}}',
                    '{{$menu->status}}');" >
                    <td class="acl_key common_td_rules">{{ $menu->menu_name }}</td>
                    <td class="group_id common_td_rules">{{ $menu->group_name }}</td>
                    <td class="worksheet common_td_rules">{{ $menu->worksheet_status }}</td>
                    <td class="submit common_td_rules">{{ $menu->submit_status }}</td>
                    <td class="verify common_td_rules">{{ $menu->verify_status }}</td>
                    <td class="approve common_td_rules">{{ $menu->approve_status }}</td>
                    <td class="status common_td_rules">{{ $menu->status_name }}</td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-2" id="work_flow_div" style="width: 20% !important">
        <input type="hidden" name="menu_access_id" id="menu_access_id"  class="work_flow_entry" value="0">
        <div class="col-md-12 padding_sm">
            <div class="mate-input-box">
                <label for="">Menu Name<span class="error_red">*</span></label>
                <div class="clearfix"></div>
                {!! Form::select('acl_key',array("-1"=> " Select Menu") + $menu_items->toArray(),null,
                            ['class' => 'form-control  select2 acl_key work_flow_entry','placeholder' => 'Menu','title' => 'Menu',
                            'id' => 'acl_key','style' => 'color:#555555;']) !!}

                <span class="error_red">{{ $errors->first('acl_key') }}</span>
            </div>
        </div>
        <div class="col-md-12 padding_sm">
            <div class="mate-input-box">
                <label for="">Group Name<span class="error_red">*</span></label>
                <div class="clearfix"></div>
                <?php
                    $listArray = array();
                    foreach ($user_groups as $value) {
                        $listArray[$value->group_id] = $value->name;
                    }
                ?>
                {!! Form::select('group_id', $listArray, 0, ['class' => 'form-control select2 bottom-border-text work_flow_entry','placeholder' => '','id' => 'group_id', 'style' => ' color:#555555; padding:4px 12px;']) !!}

                <span class="error_red">{{ $errors->first('group_id') }}</span>
            </div>
        </div>
        <div class="col-md-12 padding_sm">
            <div class="col-md-6 padding_sm">
                <div class="mate-input-box">
                    <label for="">Wrksht.Status</label>
                    <div class="clearfix"></div>
                    <input type="checkbox" class="work_flow_entry" name="worksheet" id="worksheet" >
                </div></div>
            <div class="col-md-6 padding_sm">
                <div class="mate-input-box">
                    <label for="">Submit Status</label>
                    <div class="clearfix"></div>
                    <input type="checkbox" class="work_flow_entry" name="submit" id="submit" >
                </div></div>
        </div>
        <div class="col-md-12 padding_sm">
            <div class="col-md-6 padding_sm">
                <div class="mate-input-box">
                    <label for="">Verify Status</label>
                    <div class="clearfix"></div>
                    <input type="checkbox" class="work_flow_entry" name="verify" id="verify" >
                </div></div>
            <div class="col-md-6 padding_sm">
                <div class="mate-input-box">
                    <label for="">Approve Status</label>
                    <div class="clearfix"></div>
                    <input type="checkbox" class="work_flow_entry" name="approve" id="approve" >
                </div></div>
        </div>
        <div class="col-md-12 padding_sm">
            <div class="mate-input-box">
                <label for="">Status<span class="error_red">*</span></label>
                <div class="clearfix"></div>
                <select class="form-control" name="status" id="status">
                    <option value="1">{{__('Active')}}</option>
                    <option value="0">{{__('Inactive')}}</option>
                </select>
                <span class="error_red">{{ $errors->first('status') }}</span>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="ht10"></div>
        <div class="col-md-6 padding_sm">
            <a onclick="clearWorkFlow()" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
        </div>
        <div class="col-md-6 padding_sm">
            <button class="btn btn-block light_purple_bg"  onclick="saveWorkflow()"><i class="fa fa-save"></i> Save</button>
        </div>
</div>
