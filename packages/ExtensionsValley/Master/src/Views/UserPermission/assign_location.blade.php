
<?php
$action = 'extensionsvalley.master.locationgroup';
?>
<div class="col-md-6">
    <div class="x_panel">
        {!!Form::open(array('route' => $action, 'method' => 'post'))!!}
        <input type="hidden" value="{{$group_id}}" id="group_id_loc">
        <div class="row">
            <div id="parent" class="table-responsive theadscroll" style="position: relative; height: 550px;">
                <table id="fixTable_loc" class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                       style="border: 1px solid #CCC;">
                    <thead>
                        <tr class="table_header_bg">
                            <th style="width:5%" class="common_td_rules">#</th>
                            <th style="width:30%" class="common_td_rules">
                            <input id="issue_search_box_loc" class="issue_search_box" onkeyup="searchbyName('issue_search_box_loc','fixTable_loc');" type="text" placeholder="Name Search.. " style="color: #000;display: none;width: 90%;" >
                            <span id="item_search_btn_loc"  onclick="searchitem('issue_search_box_loc','item_search_btn_text_loc','item_search_btn_loc')" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                            <span id="item_search_btn_text_loc">Name</span>
                        </th>
                            <th style="width:4%">Active</th>
                        </tr>
                    </thead>
                    <tbody class="assignListData">
                        @if(isset($resultData) && count($resultData))
                        <?php $i = 1; ?>
                        @foreach($resultData as $data)
                        <tr>

                            <td class="common_td_rules">{{$i}}</td>
                            <td class="common_td_rules">
                                <span id="child_id-{{$i}}" style="border: none;">{{$data->name}}</span>
                                <input type="hidden" class="ug_id" name="ug_id[]"  value="{{$data->location_group_id}}">
                                <input type="hidden" class="ass_val" name="ass_val[]"  value="{{$ass_val}}">
                            </td>
                            <td title="">
                                <input style="" type="checkbox" name="ass_ug_chk[]" class="ass_ug_chk" <?php
                                if ($data->id != 0) {
                                    echo 'checked';
                                } else {
                                    
                                }
                                ?>  >
                                <input type="hidden" name="hidden_ass_ug_id[]" id="field_view-{{$i}}" value="{{$data->id}}" class="hidden_ass_ug_id" >
                            </td> 

                        </tr> 
                        <?php $i++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">No Items Found.</td>
                        </tr>
                        @endif
                    </tbody>  
                </table>
            </div>
        </div>
        {!! Form::token() !!}
        {!! Form::close() !!}
    </div>
</div>
<div class="col-md-2" style="padding-top: 15px !important;border: 1px solid #E6E9ED;width: 20% !important">
    <div class="theadscroll" style="position: relative; height: 550px;">
        <table class="table no-margin theadfix_wrapper table-striped  table-condensed"
               style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th>{{$group_name}}</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($locationFlowSql_cur) && count($locationFlowSql_cur) > 0)           
                @foreach($locationFlowSql_cur as $g1s)
                <tr style="cursor: pointer;" >
                    <td class="common_td_rules">{{$g1s->name}}</td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>

