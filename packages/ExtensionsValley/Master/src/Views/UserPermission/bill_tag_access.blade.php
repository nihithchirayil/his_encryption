<div class="col-md-2" style="padding-top: 15px !important;border: 1px solid #E6E9ED;width: 20% !important">
    <div class="theadscroll" style="position: relative; height: 550px;">
        <table class="table no-margin theadfix_wrapper table-striped  table-condensed"
               style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th>Groups</th>
                </tr>
            </thead>
            <tbody>
                @if(count($user_groups) > 0)
                @foreach($user_groups as $ug)
                <tr style="cursor: pointer;" class="asn_bill_tag_row" id="bill_tag_tow_{{$ug->group_id}}">
                    <th class="common_td_rules" style="text-align: left !important" data-display="2" onclick="alterBillTag('{{$ug->group_id}}')">
                        <b>{{$ug->name}}</b>
                    </th>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>


<div id="assign_bill_tag">
    <div class="col-md-6">

    </div>
    <div class="col-md-2" style="padding-top: 15px !important;border: 1px solid #E6E9ED;width: 20% !important">
        <div class="theadscroll" style="position: relative; height: 550px;">
            <table class="table no-margin theadfix_wrapper table-striped  table-condensed"
                   style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($billTagFlowSql_cur) && count($billTagFlowSql_cur) > 0)           
                    @foreach($billTagFlowSql_cur as $g1s)
                    <tr style="cursor: pointer;" >
                        <td class="common_td_rules">{{$g1s->name}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5" class="vendor_code">No Records found</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
