@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>
    .select2-container--default .select2-selection--single {
        border-radius: 0px !important; line-height: 0px !important;

    }
    .select2-container {
        width: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 22px !important;
}
ul.pagination {
    margin: 0;
}
.select2-container--default .select2-results>.select2-results__options {
    max-height: 370px !important;
    overflow-y: auto;
}
</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<div class="right_col" >
    <div class="row" style="text-align: right;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        {!! Form::token() !!}
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label for="">User</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" autofocus class="form-control hidden_search" name="user_names" id="user_names" value="" autocomplete="false">
                                <input type="hidden" class="form-control" name="user_name_hidden" id="user_name_hidden" value="">
                                <div id="AjaxDiv" class="ajaxSearchBox" style="margin: 14px 0px 0px 0px !important;z-index: 9999999;max-height: 250px !important"></div>
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg" onclick="searchList('tab_id10')"><i id="searchdataspin" class="fa fa-search"></i>
                                Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                        </div>
                        <div class="col-md-2 padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button id="newuserbtn" type="button" class="btn btn-info" onclick="addNewUser()"><i id="newuserbtni" class="fa fa-plus"></i>
                                Add new user</button>
                        </div>
                        <div class="col-md-1 padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button id="newgrpbtn" type="button" class="btn btn-default" data-toggle="modal" data-target="#grpModal" ><i id="newgrpbtn1" class="fa fa-plus"></i>
                                Add new Group</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" id="common_list_div">

            </div>
        </div>

    </div>
</div>
<div id="search_user_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="height:750px">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New User</h4>
            </div>
            <div class="modal-body" >
                <div id="append_user_master" style="height: 690px;overflow: scroll;"></div>
            </div>

        </div>

    </div>
</div>
<div id="grpModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="height: 270px;">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Group</h4>
            </div>
            <div class="modal-body" id="append_user_grp">
            <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Group Name</label><span class="error_red">*</span>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" required  name="grp_name" value="" id="grp_name">
                        </div>
                    </div>
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                            <label for="">Status</label><span class="error_red">*</span>
                            <div class="clearfix"></div>
                            <select class="form-control" name="status_grp" id="status_grp">
                                <option value="1">{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                        </div>
                    </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="col-md-6 padding_sm">
                            <button class="btn btn-block light_purple_bg" onclick="saveGroupName()"><i class="fa fa-save"></i> Save</button>
                        </div>
            </div>

        </div>

    </div>
</div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/full_user_permission.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/js/addNewUser.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
@endsection
