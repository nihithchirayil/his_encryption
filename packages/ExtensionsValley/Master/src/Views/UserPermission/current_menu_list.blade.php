<?php $i = 0; ?>
<div class="theadscroll" style="position: relative; height: 550px;">
    <table class="table no-margin theadfix_wrapper table-striped  table-condensed" style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th>{{ $group_name }}</th>
            </tr>
        </thead>
        <tbody>
            @if (isset($grid[0]))
                @foreach ($grid[0] as $parient_id => $level_data)
                    @if (isset($level_data['view1']) && $level_data['view1'] == '1')
                        <tr class="bg-blue">
                            <td class="common_td_rules">{{ $level_data['name'] }}</td>
                        </tr>
                    @endif
                    @if ($max_level != 0)
                        <?php sub_category($max_level, $i, 0, 1, $parient_id, $grid); ?>
                    @endif
                @endforeach
            @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
<?php function sub_category($level,$count_parent,$count_child,$sub_level,$parient,$grid_data) { ?>
@if (isset($grid_data[$sub_level][$parient]) && !empty($grid_data[$sub_level][$parient]))
    <?php $j = 0;
    $next_level = $sub_level + 1; ?>

    <?php
            foreach ($grid_data[$sub_level][$parient] as $next_parent_id => $next_level_data) {
                if (isset($next_level_data['view1']) && $next_level_data['view1'] == '1'){
                ?>
    <tr>
        <td class="common_td_rules">{{ $next_level_data['name'] }}</td>
        @if ($level != $sub_level)
            <?php sub_category($level, $count_parent, $j, $next_level, $next_parent_id, $grid_data); ?>
        @endif
    </tr>
    <?php }
    } ?>

@endif
<?php }

    ?>
