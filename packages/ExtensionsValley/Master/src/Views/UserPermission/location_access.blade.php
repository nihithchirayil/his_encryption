<div class="col-md-2" style="padding-top: 15px !important;border: 1px solid #E6E9ED;width: 20% !important">
    <div class="theadscroll" style="position: relative; height: 550px;">
        <table class="table no-margin theadfix_wrapper table-striped  table-condensed"
               style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th>Groups</th>
                </tr>
            </thead>
            <tbody>
                @if(count($user_groups) > 0)
                @foreach($user_groups as $ug)
                <tr style="cursor: pointer;" class="cur_location_names" id="loc_{{$ug->group_id}}">
                    <th class="common_td_rules" style="text-align: left !important" onclick="alterLocation('{{$ug->group_id}}')">
                        <b>{{$ug->name}}</b>
                    </th>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div id="assign_loc">
    <div class="col-md-6"></div>
    <div class="col-md-2" style="padding-top: 15px !important;border: 1px solid #E6E9ED;width: 20% !important">
    <div class="theadscroll" style="position: relative; height: 550px;">
        <table class="table no-margin theadfix_wrapper table-striped  table-condensed"
               style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($locationFlowSql_cur) && count($locationFlowSql_cur) > 0)           
                @foreach($locationFlowSql_cur as $g1s)
                <tr style="cursor: pointer;" >
                    <td class="common_td_rules">{{$g1s->name}}</td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
</div>
