    <div class="theadscroll" style="position: relative; height: 350px;">
        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
               style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th>Menu Name</th>
                    <th>Group Name</th>
                    <th>Worksheet</th>
                    <th>Submit</th>
                    <th>Verify</th>
                    <th>Approve</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if(count($workFlowSql) > 0)
                @foreach ($workFlowSql as $menu)
                <tr style="cursor: pointer;" onclick="editLoadData(this,
                    '{{$menu->id}}',
                    '{{$menu->acl_key}}',
                    '{{$menu->group_id}}',
                    '{{$menu->worksheet}}',
                    '{{$menu->submit}}',
                    '{{$menu->verify}}',
                    '{{$menu->approve}}',
                    '{{$menu->status}}');" >
                    <td class="acl_key common_td_rules">{{ $menu->menu_name }}</td>
                    <td class="group_id common_td_rules">{{ $menu->group_name }}</td>
                    <td class="worksheet common_td_rules">{{ $menu->worksheet_status }}</td>
                    <td class="submit common_td_rules">{{ $menu->submit_status }}</td>
                    <td class="verify common_td_rules">{{ $menu->verify_status }}</td>
                    <td class="approve common_td_rules">{{ $menu->approve_status }}</td>
                    <td class="status common_td_rules">{{ $menu->status_name }}</td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>