<div class="col-sm-12 box no-margin no-border" style="margin:0px">
    <div class="nav-tabs-custom blue_theme_tab no-margin" id="tabs">
        <ul class="nav nav-tabs sm_nav text-center no-border" style="background-color:white;">
            <li class="active" style="width: 26%"> 
                <a style="padding: 4px 8px;" href="#tab10" data-toggle="tab" aria-expanded="true" id="tab_id10">{{$user_name}}</a>
            </li>
            <li class="" style="width: 9%"> 
                <a style="padding: 4px 8px;" href="#tab1" data-toggle="tab" aria-expanded="true" id="tab_id1" onclick ='tab1_click()' >Access control</a>
            </li>
            <li class="" style="width: 10%"> 
                <a style="padding: 4px 8px;" href="#tab3" data-toggle="tab" aria-expanded="false" id="tab_id3" onclick ='tab3_click()' >Location</a>
            </li>
            <li class="" style="width: 9.5%"> 
                <a style="padding: 4px 8px;" href="#tab4" data-toggle="tab" aria-expanded="false" id="tab_id4" onclick ='tab4_click()'>Bill tag</a>
            </li>
            <li class="" style="width: 9.5%"> 
                <a style="padding: 4px 8px;" href="#tab2" data-toggle="tab" aria-expanded="false" id="tab_id2" onclick ='tab2_click()'>Work flow access</a>
            </li>
            <li class="" style="width: 10%"> 
                <a style="padding: 4px 8px;" href="#tab5" data-toggle="tab" aria-expanded="false" id="tab_id5" onclick ='tab5_click()'>User Groups</a>
            </li>

        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="tab10">
                <div class="box no-margin no-border">
                    <div class="box-body bg-white clearfix">
                        <div class="row">
                            <div class="col-md-12 access_control">
                                @include('Master::UserPermission.access_main')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab1">
                <div class="box no-margin no-border">
                    <div class="box-body bg-white clearfix">
                        <div class="row">
                            <div class="col-md-12 access_control">
                                @include('Master::UserPermission.access_control')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab2">
                <div class="box no-margin no-border">
                    <div class="box-body bg-white clearfix">
                        <div class="row">
                            <div class="col-md-12 access_control">
                                @include('Master::UserPermission.full_work_flow_access_data')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab3">
                <div class="row">
                    <div class="col-md-12 access_control">
                        @include('Master::UserPermission.location_access')
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab5">
                <div class="box no-margin no-border">
                    <div class="box-body bg-white clearfix">
                        <div class="row">
                            <div class="col-md-12 access_control" id="assign_user_groups">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab4">
                <div class="row">
                    <div class="col-md-12 access_control">
                    @include('Master::UserPermission.bill_tag_access')
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>