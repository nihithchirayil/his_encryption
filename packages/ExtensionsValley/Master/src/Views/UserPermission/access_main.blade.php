<style>
.fixed_row{
    background: #56847c;
    height: 25px;
    color: white;
}
.scroll_row {
    float: left;
    height: 285px;
    overflow-x: hidden;
    overflow-y: auto;
    width: 100%;
}
ul.menu_container {
    font-size: 13px;
    margin: 0;
    padding: 0;
}

ul li.mainmenu{
    list-style-type: none;
    padding: 5px 0;
    float: left;
    width: 100%;
    margin: 1px 0;
    background: #f5f5f5;
}
ul.subheading {
    float: left;
    margin: 10px 0 0 0;
    padding: 0 0 0 30px;
    width: 100%;
}
ul.subheading li {
    /* background: #e0e0e0; */
    height: 30px;
    border: 1px solid #fffcfc;
    list-style-type: none;
}
.minusbtn, .plusbtn {
    cursor: pointer;
    font-size: 16px;
    margin: 2px 0 0;
}
.bottom-border-text{
    border: none !important;
    border-bottom: 1px solid rgb(53, 137, 255) !important;
    box-shadow: none;
}
.box-body,.card_body{
    background: white !important;
}
.select2-container{
    box-shadow:none;
    border-bottom: 2px solid #cbe6ff !important;
}

.box-body{
    border-color:#cbe6ff !important;
}

.fixed_row{
    background:#4497e4 !important;
}

ul li.mainmenu{
    background:#fff !important;
    border-collapse: collapse !important;
}
.menu_list_continer_table>span{
    font-size: 12px !important;
    font-weight: 600 !important;
    color: darkslategrey !important;
}
.slider{
    background-color:#4497e4 !important;
}
.badge{
    background-color:#fff !important;
}
</style>
<div class="col-md-10" style="padding-top: 15px !important;border: 1px solid #E6E9ED;width:75% !important">
    <div class="theadscroll" style="position: relative; height: 550px;">
        <table class="table no-margin theadfix_wrapper table-striped  table-condensed"
               style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                </tr>
            </thead>
            <tbody>
                @if(count($user_groups) > 0)
                @foreach($user_groups as $ug)
                <tr style="cursor: pointer;">
                    <th class="common_td_rules" style="text-align: left !important;width:25% !important" >
                        <b>{{$ug->name}}</b>
                    </th>
                     <th class="common_td_rules" style="text-align: center !important;width:10% !important">
                        <button class="btn btn-small btn-warning " onclick="changeAccess('{{$ug->group_id}}','menu')">
                            <i class=" fa fa-solid fa-list fa-2xl"></i> &nbsp;
                        </button>
                    </th>
                    <th style="width:10% !important;text-align:center !important" class="common_td_rules" > 
                        <button class="btn btn-small btn-success" onclick="changeAccess('{{$ug->group_id}}','loc')">
                            <i class=" fa fa-solid fa-map-marker fa-2xl"></i> &nbsp;
                        </button>
                    </th>
                    <th style="width:10% !important;text-align:center !important" class="common_td_rules" > 
                        <button class="btn btn-small btn-info" onclick="changeAccess('{{$ug->group_id}}','bill_tag')">
                            <i class="fa fa-solid fa-money fa-2xl"></i>
                        </button>
                    </th>
                    <th style="width:10% !important;text-align:center !important" class="common_td_rules" > 
                        <button class="btn btn-small btn-danger" onclick="changeAccess('{{$ug->group_id}}','work_flow')">
                            <i class=" fa fa-solid fa-chain fa-2xl"></i>
                        </button>
                    </th>
                    <th style="width:10% !important;text-align:center !important" class="common_td_rules" > 
                        <button class="btn btn-small btn-default" onclick="changeAccess('{{$ug->group_id}}','user_access')">
                            <i class=" fa fa-solid fa-user fa-2xl"></i>
                        </button>
                    </th>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
