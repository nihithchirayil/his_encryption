<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url != undefined) {
                var grn_from_date = $('#grn_from_date').val();
                var grn_to_date = $('#grn_to_date').val();
                var approve_from_date = $('#approve_from_date').val();
                var approve_to_date = $('#approve_to_date').val();
                var bill_no = $('#bill_no_search').val();
                var received_only = $('input[name=received_only]:checked').val();
                var param = {
                    _token: token,
                    grn_from_date: grn_from_date,
                    grn_to_date: grn_to_date,
                    approve_from_date: approve_from_date,
                    approve_to_date: approve_to_date,
                    bill_no: bill_no,
                    received_only: received_only
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#pagination_list_datadiv").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                        $('#search_grnbtn').attr('disabled', true);
                        $('#search_grnSpin').removeClass('fa fa-search');
                        $('#search_grnSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function(data) {
                        $('#pagination_list_datadiv').html(data);
                    },
                    complete: function() {
                        $("#pagination_list_datadiv").LoadingOverlay("hide");
                        $('#search_grnbtn').attr('disabled', false);
                        $('#search_grnSpin').removeClass('fa fa-spinner fa-spin');
                        $('#search_grnSpin').addClass('fa fa-search');
                    },
                    error: function() {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
                return false;
            }
        });
    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th class="common_td_rules" width="10%">GRN No.</th>
                    <th class="common_td_rules" width="10%">Bill No</th>
                    <th class="common_td_rules" width="9%">Bill Date</th>
                    <th class="common_td_rules" width="9%">GRN Date</th>
                    <th class="common_td_rules" width="9%">Approved Date</th>
                    <th class="common_td_rules" width="10%">Received By</th>
                    <th class="common_td_rules" width="9%">Received Date</th>
                    <th class="common_td_rules" width="27%">Comments</th>
                    <th style="text-align: center" width="7%">Receive</th>
                </tr>
            </thead>
            <tbody>
                <?php
            if (count($receive_grn) != 0) {
                foreach ($receive_grn as $list) {
                    ?>
                <tr>
                    <td id="grn_nolistdata<?= $list->track_id ?>" class="common_td_rules">
                        {{ $list->grn_no ? $list->grn_no : '-' }}</td>
                    <td class="common_td_rules">{{ $list->bill_no ? $list->bill_no : '-' }}</td>
                    <td class="common_td_rules">{{ $list->bill_date ? $list->bill_date : '-' }}</td>
                    <td class="common_td_rules">{{ $list->grn_date ? $list->grn_date : '-' }}</td>
                    <td class="common_td_rules">{{ $list->approved_date ? $list->approved_date : '-' }}</td>
                    <td class="common_td_rules">{{ $list->name ? $list->name : '-' }}</td>
                    <td class="common_td_rules">{{ $list->received_date ? $list->received_date : '-' }}</td>
                    <?php
                        if($list->status=='1'){
                            ?>
                    <td class="common_td_rules">{{ $list->remarks ? $list->remarks : '' }}</td>
                    <td style="text-align: center">
                        -
                    </td>
                    <?php
                        }else{
                            ?>
                    <td class="common_td_rules">
                        <input type="text" id="receive_grn_comments<?= $list->track_id ?>" autocomplete="off" value="{{ $list->remarks ? $list->remarks : '' }}" class="form-control" id="accounting_date">
                    </td>
                    <td style="text-align: center">
                        <button title="Receive GRN" id="receive_grn_btn<?= $list->track_id ?>" type="button"
                            class="btn btn-success" onclick="save_receive_grn(<?= $list->track_id ?>)"><i
                                id="receive_grn_spin<?= $list->track_id ?>" class="fa fa-save"></i> Receive
                        </button>
                    </td>
                    <?php
                        }
                    ?>

                </tr>
                <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="8" style="text-align: center">No Records Found</td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center" style="margin-top: 60px">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
