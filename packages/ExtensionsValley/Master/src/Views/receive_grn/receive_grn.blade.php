@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">

    <div class="right_col" style="min-height: 700px !important;">
        <div class="container-fluid">
            <div class="row padding_sm">
                <div class="col-md-12 padding_sm">
                    <h6 class="pull-right blue"><b>{{ $title }}</b></h6>
                </div>
                <div class="col-md-12 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 65px;">
                            <div class="col-md-12 padding_sm">
                                <div class="col-md-6 padding_sm">
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label>GRN From Date</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control datepicker" id="grn_from_date">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label>GRN To Date</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control datepicker" id="grn_to_date">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label>Approve From Date</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control datepicker" id="approve_from_date">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label>Approve To Date</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control datepicker" id="approve_to_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">GRN No</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" class="form-control" id="grn_no_search">
                                            <div id="ajaxGRNNoSearchBox" class="ajaxSearchBox" style="margin-top: -15px">
                                            </div>
                                            <input type="hidden" id="grn_id_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">Bill No</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" class="form-control"
                                                id="bill_no_search">
                                            <div id="ajaxBillNoSearchBox" class="ajaxSearchBox" style="margin-top: -15px">
                                            </div>
                                            <input type="hidden" id="bill_id_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <div class="clearfix"></div>
                                            <div class="checkbox checkbox-warning inline no-margin">
                                                <input type="checkbox" id="received_only" value="1">
                                                <label for="received_only">Received Only</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1 padding_sm">
                                        <button title="Reset" style="margin-top:17px" onclick="resetSearchData()"
                                            type="button" class="btn btn-warning btn-block"><i
                                                class="fa fa-recycle"></i></button>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <button style="margin-top:17px" onclick="searchGrnReceiveList()" type="button"
                                            class="btn btn-primary btn-block" id="search_grnbtn">Search <i
                                                id="search_grnSpin" class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
                <div class="col-md-12 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 450px;">
                            <div id="pagination_list_datadiv"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/receive_grn.js') }}"></script>
@endsection
