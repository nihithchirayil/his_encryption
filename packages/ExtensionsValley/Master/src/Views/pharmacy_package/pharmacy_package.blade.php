@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <style>
        .box_header {
            background: #3b926a !important;
            color: #fff;
        }

        .btn-success {
            background: #7ad6ab !important;
        }

        .bg-white {
            background: #fff !important;
            border: 1px solid #d7d0d0 !important;
            color: #212529;
            box-shadow: 0 0 3px #d7d7d7;
            border-radius: 5px 5px 0 0;
            margin-bottom: 5px;
        }

        .box_header {
            background: #3b926a !important;
            color: #fff;
        }
    </style>
@endsection
@section('content-area')


    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">

    <div class="right_col" role="main" style="background: white; height: 590px;">
        <input type="hidden" id="newPkgId" value="">
        <input type="hidden" id="edit_head_id" value="">
        <div class="col-md-12 padding_sm" style="margin-top:5px; ">
            <div class="box no-border no-margin " id="">
                <div class="col-md-12 box-header box_header " style="margin-bottom: 5px;">
                    <span class="padding_sm pull-right">Pharmacy Package</span>
                </div>
            </div>
        </div>
        <div class=col-md-12>
            <div class="col-md-3 box-body theadscroll" id="packageHead" style="height:500px;position:relative;">
                <table id="package_head_data"
                    class="table theadfix_wrapper table table-bordered table_sm no-margin no-border">
                    <thead class="box_header">
                        <tr>
                            <th style="width: 5%;"><button class="btn btn-sm btn-danger" title="Delete packages"
                                    onclick="deletePackage();"><i class="fa fa-times"></i></button></th>
                            <th style="width:95%;"> <span id="pkghd"> PACKAGES </span> <input id="packageName"
                                    onkeydown="addNewPackage();" type="text" placeholder=""
                                    style="color: rgb(0, 0, 0); width: 80%; display:none ">
                                <button type="button" class="btn btn-sm btn-success  pull-right padding_sm"
                                    title="Add new package" id="pkgBtn"><i class="fa fa-plus"></i> </button>
                            </th>
                        </tr>

                    </thead>
                    <tbody id=package_head_list></tbody>
                </table>
            </div>
            <div class="col-md-9">
                <div  class="col-md-12  " style="height:300px;">
                    <div class="col-md-12">
                        <h5><i id=pkg_edit class="" onclick=editPackage();></i> &nbsp &nbsp <span
                                id="package_name"></span></h5>
                    </div>
                    <div class="col-md-8 padding_sm " style="margin-top: 1px;">
                        <div class="col-md-12">
                            <div class="mate-input-box" style="height:45px!important;">
                                <label for="">Item Name</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control hidden_search" id="item_name"
                                    value="">
                                <input type="hidden" value="0" id="item_name_hidden">
                                <div class="ajaxSearchBox" id="item_nameAjaxDiv"
                                    style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999;
                                                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm " style="margin-top: 1px;">
                        <div class="col-md-12">
                            <div class="mate-input-box" style="height:45px!important;">
                                <label for="">Amount</label>
                                <div class="clearfix"></div>
                                <input type="text"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    autocomplete="off" class="form-control" id="amount" name="" value=""
                                    required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-9 padding_sm " style="margin-top: 1px;">
                            <div class="mate-input-box" style="height:45px!important;">
                                <label for="">Quantity</label>
                                <div class="clearfix"></div>
                                <input type="text"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    autocomplete="off" class="form-control" id="Qty" name=""
                                    value="" required>
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm " style="margin-top: 1px;">
                            <button type="button" style="margin-top: 25px;" class="btn btn-sm btn-success padding_sm"
                                title="Add new item" id="addItem" onclick="addNewpackageItem();"><i
                                    class="fa fa-plus"></i> </button>
                        </div>
                    </div>
                    <div class="col-md-12 ">
                        <h6> PACKAGE DETAILS</b></h6>
                    </div>
                    <div class="col-md-12 box-body   theadscroll" id="package_detail" style="height:250px;">
                        <table class="table theadfix_wrapper table table-bordered table_sm no-margin no-border">
                            <thead class="box_header">
                                <tr class="table_header_bg">
                                    <th style="width:70%;"class="">Item Name</th>
                                    <th style="width:15%;" class="">Amount</th>
                                    <th style="width:15%;" class="">Item Quantity</th>
                                </tr>
                            </thead>
                            <tbody id="packageDetails"></tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    </div>







@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/pharmacypackage.js') }}"></script>

    <script
        src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>



@endsection
