@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
    
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-slide.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/common-custom.css') }}" rel="stylesheet">

    <style>
        .ajaxSearchBox{
            margin-top:-12px !important;
        }
        .search_header {
                background: #36A693 !important;
                color: #FFF !important;
            }
        
            .mate-input-box{
                width: 100% !important;
                position: relative !important;
                padding: 15px 4px 4px 4px !important;
                border-bottom: 1px solid #01A881 !important;
                box-shadow: 0 0 3px #CCC !important;
                height:41px!important;
            }
            .mate-input-box label{
                position: absolute !important;
                top: -2px !important;
                left: 6px !important;
                font-size: 12px !important;
                font-weight: 700 !important;
                color: #107a8c !important;
                padding-top: 2px !important;
            }
    </style>
@endsection
@section('content-area')

    <div class="right_col">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="bill_no_hidden" value="{{ $bill_no }}">
        <input type="hidden" id="ph" value="{{ $ph }}">
        <input type="hidden" id="hospital_header" value="{{ $header }}">
        <input type="hidden" id="print_id" value=0>
        <input type="hidden" id="print_no" value=0>
        <input type="hidden" id="return_screen" value=1>
        <input type="hidden" id="search_based_on" value="">
        <input type="hidden" id="enable_dotmatrix_printout_clinic" value="{{$enable_dotmatrix_printout_clinic}}">
        <div class="row codfox_container">

            <div class="col-md-12 " id="">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <table class="table table-contensed table_sm" style="margin-bottom:4px;">
                            <thead>
                                <tr class="table_header_common">
                                    <th colspan="11">
                                       {{$title}} 
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="col-md-4 "
                                    style="min-height: 261px;box-shadow:0px 0px 1px 0px;padding:0px;margin-bottom:4px"
                                    id="filter_area">

                                    <div class="col-md-12 green" style="margin-bottom: 10px;font-weight: 600 !important;padding:0px;height:21px;">
                                        <div class="col-md-4">Search Filters</div>
                                        <div class="col-md-8"  style="padding: 0px;display: none;" id="bill_type_select" >
                                            <div class="col-md-6 radio radio-success inline no-margin">
                                                <input type="radio" id="pharmacy_bill"
                                                    title="Pharmacy bill" name="bill_type"
                                                    onclick=""
                                                    class="pharmacy_bill" value="" onclick="" checked>
                                                <label for="pharmacy_bill">Pharmacy Return</label>
            
                                            </div>
                                            <div class="col-md-6 radio radio-success inline no-margin">
                                                <input type="radio" id="service_bill"
                                                    title="Service bill" name="bill_type"
                                                    onclick=""
                                                    class="service_bill" value="" onclick="">
                                                <label for="service_bill">Service Return</label>
            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 padding_sm" >
                                        <div class="col-md-6 padding_sm" style="">
                                            <div class="mate-input-box">
                                                <label>UHID</label>
                                                <div class="clearfix"></div>
                                                <input class="form-control hidden_search return" value="" autocomplete="off"
                                                    type="text" id="patient_uhid" name="patient_uhid" />
                                                <div id="patient_uhidAjaxDiv" class="ajaxSearchBox"
                                                    style="width: 100%;position:absolute;margin-top:22px !important;margin-left:-3px">
                                                </div>
                                                <input class="filters" value="" type="hidden" name="patient_uhid_hidden"
                                                    value="" id="patient_uhid_hidden" />
                                                    <button type="button" class="btn btn-sm btn-primary advanceSearchBtn" style=" position: absolute; top: 15px; right: 0;"><i class="fa fa-search"></i></button>

                                            </div>
                                        </div>
                                        <div class="col-md-6 padding_sm" style="">
                                            <div class="mate-input-box">
                                                <label>Bill Tag</label>
                                                <div class="clearfix"></div>
                                                <select name="bill_tag" id="bill_tag" class="form-control return" disabled >
                                                    <option value="">Select Bill Tag</option>
                                                    @foreach ($bill_tag as $data)
                                                        <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 padding_sm">
                                        <div class="col-md-6 padding_sm" >
                                            <div class="mate-input-box">
                                                <label>Bill Number</label>
                                                <div class="clearfix"></div>
                                                <input class="form-control hidden_search return" value="" autocomplete="off"
                                                    type="text" id="bill_no" name="bill_no" />
                                                <div id="bill_noAjaxDiv" class="ajaxSearchBox"
                                                    style="width: 100%;position:absolute;margin-top:22px !important;margin-left:-3px">
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-6 padding_sm" >
                                            <div class="mate-input-box">
                                                <label>Patient Name</label>
                                                <div class="clearfix"></div>
                                                <input class="form-control hidden_search return" value="" autocomplete="off" readonly
                                                    type="text" id="patient_name" name="patient_name" />
                                                <div id="patient_nameAjaxDiv" class="ajaxSearchBox"
                                                    style="width: 100%;position:absolute;margin-top:22px !important;margin-left:-3px">
                                                </div>
                                                <input class="filters" value="" type="hidden" name="patient_name_hidden"
                                                    value="" id="patient_name_hidden" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 padding_sm">
                                        <div class="col-md-6 padding_sm" >
                                            <div class="mate-input-box">
                                                <label>Payment Type</label>
                                                <div class="clearfix"></div>
                                                <select name="payment_type" id="payment_type" class="form-control return" disabled>
                                                    <option value="">Select Type</option>
                                                    @foreach ($payment_type as $data)
                                                        <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-6 padding_sm">
                                            <div class="mate-input-box">
                                                <label>Location</label>
                                                <div class="clearfix"></div>
                                                <select name="location" id="location" class="form-control  return" disabled >
                                                    <option value="">Select Location</option>
                                                    @foreach ($location as $data)
                                                        <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 padding_sm" >
                                        <div class="col-md-6 padding_sm">
                                            <div class="mate-input-box">
                                                <label>From Date</label>
                                                <div class="clearfix"></div>
                                                <input class="form-control datepicker return" value="" autocomplete="off"
                                                    type="text" id="from_date" name="from_date" />
                                            </div>

                                        </div>
                                        <div class="col-md-6 padding_sm" style="">
                                            <div class="mate-input-box">
                                                <label>To Date</label>
                                                <div class="clearfix"></div>
                                                <input class="form-control datepicker return" value="" autocomplete="off"
                                                    type="text" id="to_date" name="to_date" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 padding_sm " >
                                        <div class="col-md-4 padding_sm pull-right">
                                            <button type="button" class="btn btn-primary btn-block" title="Search" id="search_bill"
                                                onclick="getBillList()" style=""> <i id="bill_spin"
                                                    class="fa fa-search paddimg_sm"></i>
                                                Search Bill</button>
                                        </div>
                                        <div class="col-md-4 padding_sm pull-right">
                                            <button type="button" class="btn btn-default btn-block" title="reset" id="reset_bill"
                                                onclick="reset()" style=""> <i id=""
                                                    class="fa fa-refresh paddimg_sm"></i>
                                                Reset</button>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-8 " id="bill_detail"
                                    style="min-height: 261px;box-shadow:0px 0px 1px 0px;padding:0px">
                                    <div class="col-md-12 green" style="padding:0px;font-weight: 600 !important;">
                                       <div class="col-md-2"> Bill Details:</div>
                                       <div class="col-md-5" style="margin-left: -62px;"><input class="form-control" id="issue_search_box1" autocomplete="off"
                                        type="text" placeholder="Search By Bill no./ Item Desc... "
                                       style="color: #000;display:block;width: 90%;"></div>
                                    </div>
                                    <div class="col-md-12" style="padding: 1px;height:194px" >
                                        <table class="table table-contensed table_sm" style="margin-bottom:4px;">
                                            <thead>
                                                <tr class="table_header_common center_text">
                                                    <th width="15%">Bill No.</th>
                                                    <th width="30%">Item Desc</th>
                                                    <th width="05%">Qty</th>
                                                    <th width="10%">Rem. Qty</th>
                                                    <th width="10%">Price</th>
                                                    <th width="10%">Discount</th>
                                                    <th width="10%">Net Amt.</th>
                                                    <th width="05%"> <i class="fa fa-plus"></i></th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div id="bill_detail_data">

                                        </div>
                                       
                                    </div>
                                    <div class="col-md-12"
                                    style="padding: 0px;height: 45px;box-shadow:0px 0px 1px 0px">
                                        <div class="col-md-12" style="margin-top: 2px;padding:0px;">
                                            <div class="col-md-2 padding_sm">
                                                <div class="mate-input-box">
                                                    <label>Item</label>
                                                    <input class="form-control" value="" autocomplete="off" type="text"
                                                        id="return_itm" name="return_itm" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-1 padding_sm">
                                                <div class="mate-input-box">
                                                    <label>Price</label>
                                                    <input class="form-control" value="" autocomplete="off" type="text"
                                                        id="org_price" name="org_price" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-1 padding_sm">
                                                <div class="mate-input-box">
                                                    <label>Qty</label>
                                                    <input class="form-control" value="" autocomplete="off" type="text"
                                                        id="org_qty" name="org_qty" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-2 padding_sm">
                                                <div class="mate-input-box">
                                                    <label>Net Amount</label>
                                                    <input class="form-control" value="" autocomplete="off" type="text"
                                                        id="net_amount" name="net_amount" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-2 padding_sm">
                                                <div class="mate-input-box">
                                                    <label>Return Qty</label>
                                                    <input class="form-control" onkeyup="calculateTaxAmount()" value=""
                                                        autocomplete="off" type="text"
                                                        onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                                        id="return_qty" name="return_qty" />
                                                </div>
                                            </div>
                                            <div class="col-md-2 padding_sm">
                                                <div class="mate-input-box">
                                                    <label>Item Return Total</label>
                                                    <input class="form-control" value="" autocomplete="off" type="text"
                                                        onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                                        id="return_total" name="return_total" / readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-1 padding_sm">
                                                <button type="button" title="Return" id="return"
                                                    class="btn btn-primary btn-block" style="margin-top: 14px;"
                                                    onclick="addSingleTdData()">Return</button>
                                            </div>
                                            <div class="col-md-1 padding_sm">
                                                <button type="button" title=" All Return" id="allreturn"
                                                    onclick="" class="btn btn-primary btn-block"
                                                    style="margin-top: 14px;">Return All</button>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="margin-top: 2px;padding:0px;">
                                            <div class="col-md-5 padding_sm">

                                            </div>
                                            <div class="col-md-2 padding_sm">

                                            </div>
                                          
                                          
                                            <input type="hidden" id="bill_no_hidden" value="">
                                            <input type="hidden" id="batch_no_hidden" value="">
                                            <input type="hidden" id="exp_date" value="">
                                            <input type="hidden" id="unit_tax_amount" value="">
                                            <input type="hidden" id="bill_discount_split" value="">
                                            <input type="hidden" id="item_id_hidden" value="">
                                            <input type="hidden" id="billheadid" value="">
                                            <input type="hidden" id="selling_price_with_tax" value="">
                                            <input type="hidden" id="tax_percent" value="">
                                            <input type="hidden" id="other_tax_per" value="">
                                            <input type="hidden" id="other_tax_amnt" value="">
                                            <input type="hidden" id="bill_detail_id" value="">
                                            <input type="hidden" id="tax_amount" value="">
                                            <input type="hidden" id="net_amt" value="">
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 ">
                                <div class="col-md-4 " style="min-height: 304px;box-shadow:0px 0px 1px 0px;padding:0px"
                                    id="bill_history">
                                    <div class="col-md-12" style="padding:1px;">
                                        <div class="col-md-8 green" style="font-weight: 600 !important;">
                                            Bill History
                                        </div>

                                        <div class="col-md-4 ">
                                            <button type="button" title="All Bill" class="btn btn-default pull-right" id="select_All"
                                                onclick="getAllBillDetails()" style="margin-top: 3px;">ALL</button>
                                        </div>
                                    </div>
                                    <table class="table table-contensed table_sm" style="margin-bottom:4px;">
                                        <thead>
                                            <tr class="table_header_common center_text">
                                                <th width="35%">Bill No.</th>
                                                <th width="25%">Bill Date</th>
                                                <th width="15%">Net Amt.</th>
                                                <th width="25%">Bill Tag</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <div id="bill_history_data" style="height: 221px;">

                                    </div>

                                </div>
                                <div class="col-md-8 " style="min-height: 300px;box-shadow:0px 0px 1px 0px;padding:0px"
                                    id="bill_full_return">
                                    <div class="col-md-12 green" style="margin-bottom: 12px;font-weight: 600 !important;">
                                        Bill Return Details
                                    </div>
                                    <div class="col-md-12 theadscroll " style="padding:1px;height:195px;">
                                        <table class="table table-contensed table_sm theadfix_wrapper" style="margin-bottom:4px;"
                                            id="bill_return_data">
                                            <thead>
                                                <tr class="table_header_common center_text">
                                                    <th width="15%">Bill No</th>
                                                    <th width="15%">Item Desc</th>
                                                    <th width="10%">Return Qty</th>
                                                    <th width="10%">Return Amout</th>
                                                    <th width="5%"><i class="fa fa-trash"></i></th>

                                                </tr>
                                            </thead>
                                            {{-- <div id="bill_return_body">
                                            </div> --}}

                                        </table>

                                    </div>
                                    <div class="col-md-12"
                                        style="padding: 0px;height:78px;box-shadow:0px 0px 1px 0px;">
                                        <div class="col-md-12" style="margin-top: 10px;">
                                            <div class="col-md-2" style="padding:0px;">
                                                <div class="mate-input-box">
                                                    <label>Return qty</label>
                                                    <input class="form-control" value="" autocomplete="off" type="text"
                                                        id="show_return_qty" name="show_return_qty" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="mate-input-box">
                                                    <label>Rtn.Tax Amt.</label>
                                                    <input class="form-control" value="" autocomplete="off" type="text"
                                                        id="return_tax" name="return_tax" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="mate-input-box">
                                                    <label>Return Amt.</label>
                                                    <input class="form-control" value="" autocomplete="off" type="text"
                                                        id="return_amt" name="return_amt" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mate-input-box" style="display: none" id="wanttext">
                                                    <label>Return Reason</label>
                                                    <textarea name="return_reason" id="return_reason2" class="form-control" style="height: 25px !important"></textarea>
                                                </div>
                                                <div class="mate-input-box" id="wantSelect">
                                                    <label>Return Reason</label>
                                                    <select name="return_reason" id="return_reason1" class="form-control ">
                                                        <option value="">Select Reason</option>
                                                        @foreach ($reason as $data)
                                                            <option value="{{ $data->id }}">{{ $data->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="col-md-1 padding_sm" id="text_display">
                                                <div class="checkbox checkbox-success inline" style="padding: 14px;">
                                                    <input id="text_checkbox" type="checkbox" name="checkbox" onclick="wantTextArea()" >
                                                    <label class="text-blue" for="checkbox">
                                                       <b>Other Reason</b></label>
                                                </div>
                                            </div>
                                            {{-- saveReturnDetails(); --}}
                                            <div class="col-md-1" style="margin-top: 11px;">
                                                <button type="button" title="save & print" class="btn btn-success" id="save_return" onclick="saveReturnDetails();" disabled>
                                                    <i id="spin_return" class="fa fa-save padding_sm"></i>Save</button>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="col-md-4" style="margin-top: 2px"></div> <div class="col-md-8" id="error_status" style="display: none;color:red;background-color:rgb(233 159 159 / 14%);margin-top: 2px"> <h6 class="pull-right" id="error_status" style="color:red"></h6></div>
                </div>
            </div>
        </div>
    </div>
     <!-------print modal---------------->
     <div class="modal" tabindex="-1" role="dialog" id="print_config_modal"  tabindex="-1" data-keyboard="false" data-backdrop="static"
     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header table_header_common" style="">
                    <h4 class="modal-title" style="color: white;float:left">Print Configuration</h4>
                    <button type="button" class="close" onclick="getBillList()" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                   
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle" checked>
                        Include Hospital Header
                        <input style="margin-left: 15px;" type="checkbox" name="is_duplicate" id="duplicate">
                        Duplicate Print
                    </div>
                    <div class="col-md-12" style="margin-top:4px;">
                        <button onclick="PrintBill()" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                        <button onclick="payReturn()"  title="Pay Bill" id="pay_button" type="button" class="btn btn-sm bg-green pull-right">
                            <i class="fa fa-credit-card"></i> Pay 
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('Master::eye_billing.advancePatientSearch')
    
    @include('Master::eye_bill_list.cash_return_model')


@stop

@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/eye_return.js') }}"></script>
    <script src="{{asset("packages/extensionsvalley/emr/js/eye_cash_return.js")}}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
@endsection
