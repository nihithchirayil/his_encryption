
<!-- Prescription Modal -->
<div id="insurance_list_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90%">
        @php
         $show_claim_amount = \DB::table('config_detail')->where('name','show_claim_amount')->value('value');   
         $show_claim_amount = @$show_claim_amount ? $show_claim_amount : 0;
        @endphp
        <input type="hidden" id="show_claim_amount" value ={{ $show_claim_amount }}>                              
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div>
                            <div class="col-md-6" style="width: 316px;">
                                <label><b>Name:</b></label> 
                                <label id="pat_name_lbl" style="color:#1a98f1;padding-right: 4px;"></label>
                            </div>
                            <div class="col-md-3" style="width: 152px;">
                                <label><b>Bill Amount:</b></label> 
                                <label id="claim_lbl" style="color:#1a98f1;"></label>
                            </div>
                            @if($show_claim_amount == 1)
                            <div style="display: flex;">
                                <label><b>Claim Amount:&nbsp;</b></label>
                                <div class="clearfix"></div>
                                <input type="text" id="claim_amount_entry" style="color:#1a98f1;width: 45%;" value="" onkeyup="number_validation(this);calculate_settlement_amount()" class="form-control first_entry_calcuation" >                              
                            </div>
                           @endif
                            <div class="" style="width: 50%;margin-left: 451px;margin-top: 4px;">
                                <label><b>Balance Amount:</b></label>
                                <label id="balance_lbl" style="color:#1a98f1;padding-right: 4px;"></label>
                                <input type="hidden" id="balance_lbl_txt" value="">
                            </div>
                        </div>
                        <input type="hidden" value="" id="view_modal_bill_from">
                        <input type="hidden" value="" id="bill_tag_ins">
                        <div class="theadscroll always-visible" style="position: relative;height: 200px;">
                            <table class="table table_round_border styled-table">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th style="width: 15%">Date</th>
                                        <th style="width: 20%" title="Settled amount + TDS +Writeoff">Settl.+TDS+writ..</th>
                                        <th style="width: 15%" title="Settled amount">Settled am..</th>
                                        <th style="width: 15%">TDS</th>
                                        <th style="width: 15%">Write-off</th>
                                        <th style="width: 15%">Company</th>
                                        <th><i class="fa fa-trash-o" style="width: 5%"> </i></th>
                                    </tr>
                                </thead>
                                <tbody id="insurance_list_modal_content">

                                </tbody>
                            </table>
                        </div>
                        <div class="theadscroll always-visible" style="position: relative;height: 200px;">
                        <table class="table table_round_border styled-table" style="width: 50%">
                            <thead>
                                <tr class="table_header_bg" style=" background-color: green">
                                    <th colspan="3" style="text-align: center"> Advance Collected</th></tr>
                            </thead>
                            <tr>
                                <th style="color:#1a98f1">
                                    Patient Advance
                                </th>
                                <td id="pat_adv"></td>
                                <td><input type="text" class="form-control" value="0" id="pay_patient_adj_amount" onkeyup="number_validation(this);checkAdvance(this);calculate_settlement_amount();" autocomplete="off"></td>
                            </tr>
                            <tr>
                                <th style="color:#1a98f1">
                                    Insurance Advance
                                </th>
                                <td id="pat_ins_adv"></td>
                                <td><input type="text" class="form-control" value="0" id="pay_insurance_adj_amount" onkeyup="number_validation(this);checkAdvance(this);calculate_settlement_amount();" autocomplete="off"></td>
                            </tr>
                            <tr>
                                <th style="color:#1a98f1">
                                    Other Advance
                                </th>
                                <td id="pat_ins_adv"></td>
                                <td><input type="text" class="form-control" value="0" id="pay_other_adj_amount" onkeyup="number_validation(this);calculate_settlement_amount();" autocomplete="off"></td>
                            </tr>
                        </table>
                    </div>
                    </div>
                    <div class="col-md-6" id="payment_mode_details" style="display: none">
                        <div>
                            <label class="blinking">Please enter Payment Details</label>
                        </div>
                        <input type="hidden" id="ins_head_id_for_payment_mod" value="">
                        <input type="hidden" id="total_settled_amnt_for_validation" value="">
                        <div class="theadscroll always-visible" style="position: relative;height: 300px;">
                            <input type="hidden" value="" id="balance_payment_amount">

                            <table class="table table_round_border styled-table" id="payment_entry_table">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th width="3%" title="Payment Mode">Pay.Mode</th>
                                        <th width="3%">Amount</th>
                                        <th width="3%">Card/Bank</th>
                                        <th width="3%">Card/Cheque</th>
                                        <th width="4%">Cheque Date</th>
                                        <th width="4%" title="Card holder name">Card hldr</th>
                                        <th width="1%">
                                            <i id="" class="fa fa-plus" onclick="add_ins_payment_row()" style="cursor: pointer;color: #eea236;"></i></th>
                                    </tr>
                                </thead>
                                <tbody id="payment_list_modal_content">

                                </tbody>
                            </table>
                            <span id="show_balance_payment_amount" style="color:red;float: right"></span>
                            <input type="hidden" value="0" id="show_balance_payment_amount_hidden">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>
            <input type="hidden" value="" id="all_save_btn">

            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-success" onclick="save_insurance(1)" style="float: right"><i id="save_ins_payment_spin_btn" class="fa fa-save"> </i>&nbsp;&nbsp;Save</button>
                <button type="button" class="btn btn-sm btn-danger" onclick="save_insurance(5)" style="float: right"><i id="save_fully_ins_payment_spin_btn" class="fa fa-thumbs-up"> </i>&nbsp;&nbsp;Fully Settle</button>
                <button type="button" class="btn btn-sm btn-warning" data-dismiss="modal">
                <i class="fa fa-times"> </i>  Close</button>
            </div>
        </div>

    </div>
</div>

