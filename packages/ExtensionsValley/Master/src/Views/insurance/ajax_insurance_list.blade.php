<div class="theadscroll" style="position: relative; height: 60vh;">
    <table id="table_insurance_settlememtList" class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" table_page_count='0'>
        <thead>
            <tr class="table_header_bg">
                @if($company_code == 'DAYAH')
                <th width="1%"></th>
                @endif
                <th width="5%" class="common_td_rules">Invoice No </th>
                <th width="5%" class="common_td_rules">Bill Date</th>
                <th width="5%" class="common_td_rules">Company</th>
                <th width="2%" class="common_td_rules">UHID</th>
                <th width="4%" class="common_td_rules">Patient Name</th>
                <th width="4%" class="common_td_rules">Bill Amount</th>
                <th width="6%" class="common_td_rules">Claim Amount</th>
                <th width="4%" class="common_td_rules">Advance Adj.</th>
                <th width="4%" class="common_td_rules">Settled Amount</th>
                <th width="4%" class="common_td_rules">TDS</th>
                <th width="4%" class="common_td_rules">Remaining Amount</th>
                <th width="4%" class="common_td_rules">Patient Payable</th>
                <th width="4%" class="common_td_rules">Paid Amount</th>
                <th width="6%" class="common_td_rules">Write-off</th>
                <th width="5%">Action</th>
            </tr>
        </thead>
        <tbody>
            @php $i = 0; @endphp
            @if(isset($insurance_list) && sizeof($insurance_list) > 0)
            @foreach($insurance_list as $list)
            <?php
            $disabled_status = '';
            if (intval($list->settled_status) == 2) {
                $bg_color = "background-color: #bee9e7 !important";
            } else if (intval($list->settled_status) == 1) {
                $bg_color = "background-color:#cae5a7 !important";
                $disabled_status = 'disabled';
            } else {
                $bg_color = "";
            }
            $total_settlement_amount = '';
            $total_sum_tds_amount = '';
            $total_sum_write_off_amount = '';
            $total_advance_adjusted_amt = '';
            if (intval($list->settled_status) == 1) {
                $total_settlement_amount = $list->sum_settled_amount;
                $total_sum_tds_amount = $list->sum_tds_amount;
                $total_sum_write_off_amount = $list->sum_write_off_amount;
                $total_advance_adjusted_amt = $list->advance_adjusted_amt;
            }

            ?>
            <tr onclick="" style="cursor:pointer;<?= $bg_color; ?>">
                <input type="hidden" value="{{$list->bill_id}}" name="bill_id" id="bill_head_id_<?= $i ?>">
                <input type="hidden" value="{{$list->bill_from}}" name="bill_from" id="bill_from_<?= $i ?>">
                <input type="hidden" value="{{$list->visit_id}}" name="visit_id" id="visit_id_<?= $i ?>">
                <input type="hidden" value="{{$list->company_id}}" name="company_id" id="company_id_<?= $i ?>">
                <input type="hidden" value="{{$list->rec_amnt}}" name="rec_amnt" id="rec_amnt_<?= $i ?>">
                <input type="hidden" value="{{$list->bill_tag}}" name="bill_tag" id="bill_tag<?= $i ?>">
                @if($company_code == 'DAYAH')
                <td>
                    @if(isset( $list->ledger_name) && $list->ledger_name !='')
                    <i class="fa fa-check" style="color:green" title="Ledger Mapped"></i>

                    @else
                    <i class="fa fa-times" style="color:red" title="Ledger Not Mapped"></i>
                    @endif
                </td>
                @endif
                <td class="common_td_rules">{{ $list->bill_no }}</td>
                <td class="common_td_rules">{{ $list->bill_date }}</td>
                <td class="common_td_rules">{{ $list->company_name }}</td>
                <td class="common_td_rules">{{ $list->uhid }}</td>
                <td class="common_td_rules">{{ $list->pat }}</td>
                <td class="td_common_numeric_rules net_amount_<?= $i ?>">{{ $list->net_amount }}</td>
                @if ($show_claim_amount != 1)
                <td class="td_common_numeric_rules">{{ $list->claim_amount }}</td>
                @else
                <td class="td_common_numeric_rules"  style="display: flex">
                <input type="text" id="claim_amount_entry{{$i}}" style="" value="{{ $list->claim_amount_new }}" onkeyup="number_validation(this, {{$i}});calculate_settlement_amount_txt(this, {{$i}})" class="form-control first_entry_calcuation claim_amount_entry" {{$disabled_status}}>
                <button onclick='save_claim_amount(this, {{$i}})' class="btn btn-sm light_purple_bg" style="padding: 2px 4px !important;margin-left:2px" {{$disabled_status}}>
                        <i id="save_settlement_btn{{$i}}" class="fa fa-save"></i> </button>
                </td>
                @endif
                <td class="td_common_numeric_rules" style='background-color:#f9dcdc61'
                    title='Insurance Advance: {{$list->pat_insurance_advance}}'>
                    <!-- {{ $list->advance_adjusted_amt }} -->
                    <input type="text" id="advance_adjusted_amt{{$i}}" ins_advance='{{ $list->pat_insurance_advance }}'
                        value="{{$total_advance_adjusted_amt}}" oninput="number_validation(this);calculate_settlement_amount_txt(this, {{$i}});"
                        onkeyup="checkAdvancetxt(this);"
                        class="form-control first_entry_calcuation pay_insurance_adj_amount" autocomplete='off' {{$disabled_status}}>
                </td>

                <td class="td_common_numeric_rules" style='background-color:#f9dcdc61'>
                    <input type="text" id="settle_amount_entry{{$i}}" value="{{$total_settlement_amount}}"
                        oninput="number_validation(this);calculate_settlement_amount_txt(this, {{$i}});"
                        class="form-control first_entry_calcuation settle_amount_entry" autocomplete='off' {{$disabled_status}}>
                </td>
                <td class="td_common_numeric_rules" style='background-color:#f9dcdc61'>
                    <input type="text" id="tds_amount_entry{{$i}}" value="{{$total_sum_tds_amount}}"
                        oninput="number_validation(this);calculate_settlement_amount_txt(this, {{$i}});"
                        class="form-control first_entry_calcuation tds_amount_entry" autocomplete='off' {{$disabled_status}}>
                </td>
                <td class="td_common_numeric_rules rec_amnt_td rec_amnt_new_{{$i}}">{{ $list->rec_amnt_new }}</td>
                <td class="td_common_numeric_rules">
                    {{$list->insurance_patient_paid }}
                </td>
                <td class="td_common_numeric_rules">
                    {{$list->paid_amount }}
                </td>

                <td class="td_common_numeric_rules" style='background-color:#f9dcdc61'>
                    <input type="text" id="write_off_amount_entry{{$i}}" value="{{$total_sum_write_off_amount}}"
                        oninput="number_validation(this);calculate_settlement_amount_txt(this, {{$i}})"
                        class="form-control first_entry_calcuation write_off_amount_entry" autocomplete='off' {{$disabled_status}}>
                </td>
                <td>
                    @if ($list->settled_status != 1)
                    <div class="col-md-5 no-padding" style='font-size: 16px;color: #01987a;'>
                        <button onclick='save_settlement_detail(this, {{$i}})' class="btn btn-sm light_purple_bg">
                        <i id="save_settlement_btn{{$i}}" class="fa fa-save"></i> </button>
                    </div>
                    @endif
                    <div class="col-md-5 no-padding">
                        <button
                        onclick="view_insurance('{{$list->bill_id}}','{{$list->bill_from}}','{{$i}}',
                        '{{$list->rec_amnt}}','{{$list->pat}}','{{$list->uhid}}','{{$list->claim_amount}}','{{$list->pat_patient_advance}}','{{$list->pat_insurance_advance}}','{{$list->bill_tag}}','{{ $list->claim_amount_new }}')"
                        class="btn btn-sm light_purple_bg">
                        <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa-eye"></i> </button>
                    </div>
                </td>
            </tr>
            <?php $i++; ?>
            @endforeach
            @endif
        </tbody>
    </table>
</div>


<!-- <div class="clearfix"></div> -->
<!-- <div class="col-md-12 text-center">
    <div class="col-md-3" style="padding-top: 40px;">
        <div style='width:20px;height:20px;display:block;float:left;background-color:#cae5a7;'></div>
        <label style="margin: 2px 0 0 10px;float: left;padding-right: 15px;">Settled</label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <div style='width:20px;height:20px;display:block;float:left;background-color:#bee9e7;'></div>
        <label style="margin: 2px 0 0 10px;float: left;">Partially settled</label>
    </div>
</div> -->

@if(isset($paginator))
<div class="clearfix"></div>
<div class="col-md-12 text-right" style="max-height: 75px; height: 100%; margin-top: 0px;">
    <ul class="reorder_level_pages pagination purple_pagination" style="text-align:right !important;">
        {!! $paginator->render() !!}
    </ul>
</div>
@endif
