@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')

<div class="right_col" >
    <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;padding-right: 80px">{{$title}}</div>
    <div class="row codfox_container">

        <div class="col-md-12 padding_sm" id="ledger_master_form_whole">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <form action="{{route('extensionsvalley.master.addInsuranceAdvance')}}" method="POST" id="ledger_master_form">
                        {!! Form::token() !!}
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                <label class="custom_floatlabel">UHID.</label>
                                <input type="text" name="op_no_search" autocomplete="off"  autofocus value="" class="form-control" id="op_no_search" placeholder="UHID">
                                <input type="hidden" id="op_no_search_hidden" value="" name="op_no_search_hidden">
                                <div id="op_no_searchCodeAjaxDiv" class="ajaxSearchBox"></div>
                            </div>
                                <div class="ajaxSearchBox group_asset_name_box" id="group_asset_name_box-1"
                                     style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                     margin: -2px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                     border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                            </div>
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Amount</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="Amount" id="amount">
                                    <span style="color: #d14;"> {{ $errors->first('amount') }}</span>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="padding-top:20px !important">
                                <div class="h10"></div>
                                <input type="submit" class="btn btn-block light_purple_bg" value="Save">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">
                                    $(document).ready(function () {
                                        $(window).keydown(function (event) {
                                            if (event.keyCode == 13) {
                                                event.preventDefault();
                                                return false;
                                            }
                                        });
                                        var $table = $('table.theadfix_wrapper');
                                        $table.floatThead({
                                            scrollContainer: function ($table) {
                                                return $table.closest('.theadscroll');
                                            }

                                        });
                                        $('.theadscroll').perfectScrollbar({
                                            wheelPropagation: true,
                                            minScrollbarLength: 30
                                        });
                                        $('.fixed_header').floatThead({
                                            position: 'absolute',
                                            scrollContainer: true
                                        });
                                    });
$('#op_no_search').keyup(function (event) {
    var keycheck = /[a-zA-Z0-9 ]/;
    var value = event.key;
    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
        var op_no_search = $(this).val();
        op_search = op_no_search.trim();
        if (op_search == "") {
            $("#op_no_searchCodeAjaxDiv").html("");
            $("#op_no_search_hidden").val("");
        } else {
            try {
                var url = '';
                var param = {op_no_search: op_no_search,op_no_search_prog:1};
                $.ajax({
                    type: "GET",
                    url: '',
                    data: param,
                    beforeSend: function () {
                        $("#op_no_searchCodeAjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        if (html == 'No Result Found') {
                            toLocationPub = '';
                        }
                        $("#op_no_searchCodeAjaxDiv").html(html).show();
                        $("#op_no_searchCodeAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    }
                });
            } catch (err) {
                document.getElementById("demo").innerHTML = err.message;
            }
        }
    } else {
        ajaxProgressiveKeyUpDown('op_no_searchCodeAjaxDiv', event);
    }
});
$('#op_no_search').on('keydown', function (event) {
    if (event.keyCode === 13) {
        ajaxlistenter('op_no_searchCodeAjaxDiv');
        return false;
    }
});
function fill_patient_id(list, patient_id, uhid) {
    var itemCodeListDivId = $(list).parent().attr('id');
    $('#op_no_search_hidden').val(patient_id);
    $('#op_no_search').val(uhid);
    $('#' + itemCodeListDivId).hide();
}
@include('Purchase::messagetemplate')
</script>

@endsection
