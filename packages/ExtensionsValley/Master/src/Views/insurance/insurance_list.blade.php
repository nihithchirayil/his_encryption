@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
.blinking{
    animation:blinkingText 0.8s infinite;

}
@keyframes blinkingText{
    0%{     color: #FF0000;    }
    49%{    color: #FF0000; }
    60%{    color: transparent; }
    99%{    color:transparent;  }
    100%{   color: #FF0000;    }
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-xs-3 padding_sm" style="padding-bottom: 10px;">
                            <div class="mate-input-box">
                                <label class="custom_floatlabel">UHID.</label>
                                <input type="text" name="op_no_search" autocomplete="off"  autofocus value="" class="form-control" id="op_no_search" placeholder="UHID">
                                <input type="hidden" id="op_no_search_hidden" value="" name="op_no_search_hidden">
                                <div id="op_no_searchCodeAjaxDiv" class="ajaxSearchBox"></div>
                            </div>
                        </div>
                        <div class="col-xs-3 padding_sm" style="padding-bottom: 10px;">
                            <div class="mate-input-box">
                                <label class="custom_floatlabel">IPNO.</label>
                                <input type="text" name="ip_no_search" autocomplete="off"  autofocus value="" class="form-control" id="ip_no_search" placeholder="IPNO">
                                 <input type="hidden" id="ip_no_search_hidden" value="" name="ip_no_search_hidden">
                                <div id="ip_no_searchCodeAjaxDiv" class="ajaxSearchBox"></div>
                            </div>
                        </div>
                        <div class="col-xs-3 padding_sm" style="padding-bottom: 10px;">
                            <div class="mate-input-box">
                                <label class="custom_floatlabel">Bill No.</label>
                                <input type="text" name="bill_no" autocomplete="off"  autofocus value="" class="form-control" id="bill_no" placeholder="Bill No">
                                <div id="bill_no_searchCodeAjaxDiv" class="ajaxSearchBox"></div>
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                            <?php
                            $cred_comp = \DB::table('credit_company')->where('status', 1)->orderBy('company_name')->pluck('company_name', 'id');
                            ?>
                            <label for="">Company</label>
                            {!! Form::select('company',$cred_comp,'',['class' => 'form-control custom_floatinput select2','placeholder' => 'Company','title' => 'Company','id' => 'company','style' => 'color:#555555; padding:2px 12px;']) !!}
                        </div>
                        </div>

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="">From Date</label>
                            <div class="clearfix"></div>
                            <input type="text" name="from_date" autocomplete="off"
                                   value=""
                                   class="form-control datepicker" id="from_date"
                                   placeholder="From Date">
                            </div>
                        </div>

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="">To Date</label>
                            <div class="clearfix"></div>
                            <input type="text" name="to_date" autocomplete="off"
                                   value=""
                                   class="form-control datepicker" id="to_date"
                                   placeholder="To Date">
                            </div>
                        </div>

                        <div class="col-md-2 padding_sm">
                            <div class="checkbox checkbox-success inline no-margin " style="margin-top: 20px!important">
                                <input type="checkbox" class="settlement_status" id="settlement_status" name="settlement_status" style="margin-left: -20px !important;" value='1'>
                                <label style="padding-left: 2px;" for="settlement_status"> Settled</label>                                
                            </div>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button class="btn btn-block light_purple_bg" id="insurance_settlement_list" onclick="insurance_settlement_list()"><i class="fa fa-search"></i> Search</button>
                        </div>     
                        
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block btn-warning" onclick="clear_search();" ><i class="fa fa-times"></i> Clear</span>
                        </div>              
                        {!! Form::token() !!} 
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="height: 80vh;">
                <div class="box-body clearfix">                 
                    <div class="clearfix"></div>
                    <div class="col-md-12 text-center" id="insurence_settlement_list">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('Master::insurance.custom_modals')
</div>
<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
<input type="hidden" id="cash_payment_mod_hidden" value="<?php echo htmlspecialchars($cash_payment_mod_json, ENT_COMPAT); ?>">
<input type="hidden" id="bank_hidden" value="<?php echo htmlspecialchars($bank_details_json, ENT_COMPAT); ?>">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/insurance.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script type="text/javascript">

</script>
@endsection
