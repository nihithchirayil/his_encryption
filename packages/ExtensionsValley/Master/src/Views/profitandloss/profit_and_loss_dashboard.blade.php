@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    @include('Dashboard::dashboard.partials.headersidebar')
@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-list.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/profit_and_loss_dashboard.css') }}" rel="stylesheet">


@endsection
@section('content-area')
<div class="right_col" role="main">
    <div class="col-md-12 padding_sm">
        <div class="box no-border no-margin">
            <div class="box-footer"
                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 34px;">
                <div class="col-md-10 padding_sm">
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(1)" title="Current Year" class="btn btn-primary btn-block">Current Year <i
                                class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(2)" title="Previous Year"
                            class="btn btn-success btn-block">Previous Year <i class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(3)" title="Last 3 Year"
                            class="btn btn-warning btn-block">Last 3 Year <i class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2">
                        <button onclick="getCustomDates(7)" title="Custom Date Range Selection"
                            class="btn bg-teal-active btn-block">Custom <i class="fa fa-calendar"></i></button>
                    </div>
                    <!-- <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(4)" title="Custom" class="btn btn-danger btn-block">Custom <i class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(5)" title="Month Till Date"
                            class="btn btn-info btn-block">Month Till Date <i class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(6)" title="Last Month" class="btn bg-purple btn-block">Last
                            Month <i class="fa fa-calendar"></i></button>
                    </div> -->
                </div>


            </div>
        </div>
    </div>

    <div class="col-md-12 padding_sm" style="margin-top: 25px;">
        <div class="col-md-6 padding_sm" >
            <div class="col-md-12 no-padding" style="background:aliceblue;">
                <figure class="highcharts-figure">
                    <div id="container" style="height:175px;"></div>
                    <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC; background:white;">
                        <tbody>
                            <tr>
                                <td></td>
                                <td>Jan</td>
                                <td>Feb</td>
                                <td>Mar</td>
                                <td>Apr</td>
                                <td>May</td>
                                <td>Jun</td>
                                <td>Jul</td>
                                <td>Aug</td>
                            </tr>
                            <tr>
                                <td>Income</td>
                                <td style="color:green;">49042531</td>
                                <td style="color:green;">49031550</td>
                                <td style="color:green;">47131790</td>
                                <td style="color:green;">45617210</td>
                                <td style="color:green;">49959236</td>
                                <td style="color:green;">50035138</td>
                                <td style="color:green;">50146717</td>
                                <td style="color:green;">50031564</td>
                            </tr>
                            <tr>
                                <td>Expense</td>
                                <td style="color:orange;">1914590</td>
                                <td style="color:orange;">1994813</td>
                                <td style="color:orange;">2147412</td>
                                <td style="color:orange;">2183815</td>
                                <td style="color:orange;">2028639</td>
                                <td style="color:orange;">1815390</td>
                                <td style="color:orange;">1964332</td>
                                <td style="color:orange;">1844689</td>
                            </tr>
                        </tbody>
                    </table>

                </figure>
            </div>
            <div class="col-md-12 no-padding" style="margin-top: 10px;">
                <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC;">
                    <tbody>
                        <tr class="headder_bg">
                            <td>Bank</td>
                            <td>Opening Balance</td>
                            <td>Receipts</td>
                            <td>Payments</td>
                            <td>Closing Balance</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>HDFC BANK</td>
                            <td>155361659</td>
                            <td>245122</td>
                            <td>55422</td>
                            <td>15151515</td>
                        </tr>
                        <tr>
                            <td>SBI</td>
                            <td>152221142</td>
                            <td>359452</td>
                            <td>336459</td>
                            <td>17458369</td>
                        </tr>
                        <tr>
                            <td>South Indian</td>
                            <td>152221142</td>
                            <td>245122</td>
                            <td>55422</td>
                            <td>15151515</td>
                        </tr>
                        <tr>
                            <td>ICICI</td>
                            <td>152221142</td>
                            <td>245122</td>
                            <td>55422</td>
                            <td>15151515</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 no-padding" style="margin-top: 10px;">
                <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC;">
                    <tbody>
                        <tr class="headder_bg">
                            <td>Bank - OD</td>
                            <td>Opening Balance</td>
                            <td>Receipts</td>
                            <td>Payments</td>
                            <td>Closing Balance</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>HDFC BANK</td>
                            <td>152221142</td>
                            <td>245122</td>
                            <td>55422</td>
                            <td>15151515</td>
                        </tr>
                        <tr>
                            <td>SBI</td>
                            <td>152221142</td>
                            <td>245122</td>
                            <td>55422</td>
                            <td>15151515</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-md-6 no-padding">

            <div class="col-md-12 no-padding">
                <div class="col-md-3 no-padding">
                    <div class="col-md-12 padding_sm">
                        <div style=" height: 210px; background: #cbd6f5; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="font-size:14px;"><b> Net Profit </b> </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #ecf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span> Current Month <b>15000542</b></span>
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #ecf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span> Last Month <b>15000542</b> </span>
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #ecf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span>FY 21-22 <b>15000542</b> </span>   
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="col-md-9 no-padding">
                    
                    
                    <div class="col-md-6 padding_sm">
                        <div style="height: 210px;  background: #f5e4cb; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="font-size:14px;"><b> Income </b> </div>
                            <div class="block" style="margin-bottom: 5px; display:flex; background: #bdf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:50%;"> Direct Income</div><div style="width:50%; text-align:right;"><b>15095070</b></td></div>
                            </div>
                            <div class="block" style="margin-bottom: 5px; display:flex; background: #bdf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:50%;">Indirect Income</div><div style="width:50%; text-align:right;"><b>17026890</b></div>
                            </div>
                            <div class="block" style="margin-bottom: 5px; display:flex; background: #bdf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:55%;">Pharmacy Sales</div><div style="width:47%; text-align:right;"><b>15044199</b></div>
                            </div>
                            <div class="block" style="margin-bottom: 5px; display:flex; background: #bdf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:50%;">Other Income </div><div style="width:50%; text-align:right;"><b>1043500</b></div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6 padding_sm">
                        <div style=" height: 210px;  background: #9adeb8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="font-size:14px;"><b> Expense </b> </div>
                            <div class="block" style="margin-bottom: 5px;  display:flex; background: #b1cdff; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:50%">Direct Expense </div><div style="width:50%; text-align:right;"><b>8006800</b></div>
                            </div>
                            <div class="block" style="margin-bottom: 5px;  display:flex; background: #b1cdff; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:55%">Indirect Expense </div><div style="width:45%; text-align:right;"><b>20602600</b></div>
                            </div>
                            <div class="block" style="margin-bottom: 5px;  display:flex; background: #b1cdff; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:50%">Fixed Assets </div><div style="width:50%; text-align:right;"><b>8040000</b></div>
                            </div>
                            <div class="block" style="margin-bottom: 5px;  display:flex; background: #b1cdff; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <div style="width:62%">Purchase Accounts </div><div style="width:38%; text-align:right;"><b>17780000</b></div>
                            </div>
                        </div>
                        
                    </div>
                </div>


            </div>


            <div class="col-md-12 no-padding" style="margin-top:10px;">
                <div class="col-md-3 no-padding">
                    <div class="col-md-12 padding_sm">
                        <div style=" height: 330px; background: #d5cbf5; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="font-size:14px;"><b> Doctors Salary </b> </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #baffd2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span> Fixed Pay <b>1100000</b></span>
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #baffd2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span> OP Consultation <b>2000000</b> </span>
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #baffd2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span>Procedures <b>2202500</b> </span>   
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #baffd2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span>Surgeon Fee <b>2516000</b> </span>   
                            </div>
                            <div class="block" style="margin-bottom: 5px; text-align: center; background: #baffd2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <span>Outsourced <b>504000</b> </span>   
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="col-md-9 no-padding">
                    
                    
                    <div class="col-md-6 padding_sm">
                        <div style="background: #dcf1a8; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Total OP for the month </div>
                                <div style="width: 30%; text-align:right;">  <b> 12000 </b> </div> 
                            </div>
                            <div class="block" style="margin-bottom: 5px; margin-top:5px; background: #faffb2; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                                <table style="width:100%;"><tr><td style="white-space: nowrap;">New </td><td style="text-align:center;"> Renewal</td><td style="white-space: nowrap; text-align:right;">Followup</td></tr></table>
                                <table style="width:100%;"><tr><td style="white-space: nowrap;"><b>2500</b></td><td><b>2000</b></td><td style="white-space: nowrap; text-align:center;"><b>8000</b></td></tr></table>
                            </div>
                        </div>
                        <div style="background: #dcf1a8;  height: 75px; color: #404040; margin-top:5px; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Total Unpaid Amount </div>
                                <div style="width: 30%; text-align:right;">  <b> 395300 </b> </div> 
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Unpaid OT Amount </div>
                                <div style="width: 30%; text-align:right;">  <b> 207100 </b> </div>
                            </div>
                        </div>
                        <div style="background: #dcf1a8; color: #404040; margin-top:5px; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Company Credits </div>
                                <div style="width: 30%; text-align:right;">  <b> 5904500 </b> </div> 
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Current Outstanding </div>
                                <div style="width: 30%; text-align:right;">  <b> 107050 </b> </div>
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Settled This Month  </div>
                                <div style="width: 30%; text-align:right;">  <b> 5050600 </b> </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 padding_sm">
                        <div style="background: #ffbf7d;height: 102px; color: #404040; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 70%;"> No of IP Patient </div>
                                <div style="width: 30%; text-align:right;">  <b> 600 </b> </div> 
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Average Length of Stay </div>
                                <div style="width: 30%; text-align:right;">  <b> 3.5 </b> </div>
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 70%;"> IP Income </div>
                                <div style="width: 30%; text-align:right;">  <b> 15056000 </b> </div>
                            </div>
                        </div>
                        <div style="background: #ffbf7d;  color: #404040; margin-top:5px; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Pharmacy Purchase </div>
                                <div style="width: 30%; text-align:right;">  <b> 8060500 </b> </div> 
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Pharmacy Sales </div>
                                <div style="width: 30%; text-align:right;">  <b> 15044199 </b> </div>
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Margin %  </div>
                                <div style="width: 30%; text-align:right;">  <b> 47.8 </b> </div>
                            </div>
                        </div>
                        <div style="background: #ffbf7d; color: #404040; margin-top:5px; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;"> 
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Total Discount </div>
                                <div style="width: 30%; text-align:right;">  <b> 405600 </b> </div> 
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Shareholder Discount </div>
                                <div style="width: 30%; text-align:right;">  <b> 300500 </b> </div>
                            </div>
                            <div style="display:flex; ">
                                <div style="width: 70%;"> Discount - Others </div>
                                <div style="width: 30%; text-align:right;">  <b> 105100 </b> </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 padding_sm" >
                        <div class="col-md-12 padding_sm" style="background: #ffcce8; color: #404040; margin-top:5px; border-radius: 13px; border: 2px solid #b5b5b5; padding: .5em;" >
                            <div class="col-md-4 padding_sm">
                                <div style="display:flex; ">
                                    <div style="width: 100%;"> <b> Closing Stock </b> </div>
                                </div>
                                <div style="display:flex; ">
                                    <div style="width: 70%;"> Lab Coonsumables </div>
                                    <div style="width: 30%; text-align:right;">  <b> 7040600 </b> </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-2 padding_sm">
                                <div style="display:flex; ">
                                    <div style="width: 70%;"> Medicine </div>
                                    <div style="width: 30%; text-align:right;">  <b> 7040600 </b> </div> 
                                </div>
                                <div style="display:flex; ">
                                    <div style="width: 70%;"> X-Ray Films </div>
                                    <div style="width: 30%; text-align:right;">  <b> 1030870 </b> </div>
                                </div>
                                <div style="display:flex; ">
                                    <div style="width: 70%;"> CT Films </div>
                                    <div style="width: 30%; text-align:right;">  <b> 506800 </b> </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>


            </div>

        </div>
    </div>
</div>

@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/profit_and_loss_dashboard.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js') }}"></script>
@endsection
