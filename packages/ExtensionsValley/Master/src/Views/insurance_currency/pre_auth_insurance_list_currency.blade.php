<div class="right_col" role="main">
    <div class="row codfox_container">
        <input type="hidden" name="patient_id" id="patient_id" value="{{ @$patient_data[0]->id ? $patient_data[0]->id : '' }}">
        <input type="hidden" name="bill_from" id="bill_from" value="{{ @$bill_from ? $bill_from : '' }}">
        <input type="hidden" name="bill_id" id="bill_id" value="{{ @$bill_id ? $bill_id : '' }}">
        <input type="hidden" name="pre_auth_req_tbl_id" id="pre_auth_req_tbl_id" value="{{ @$patient_data[0]->irq_id ? $patient_data[0]->irq_id : '' }}">
        <input type="hidden" name="company_id" id="company_id" value="{{ @$patient_data[0]->company_id ? $patient_data[0]->company_id : '' }}">
        <input type="hidden" name="request_provider_id" id="request_provider_id" value="{{ @$patient_data[0]->request_provider_id ? $patient_data[0]->request_provider_id : '' }}">
        <div class="col-md-12 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Request Id.</label>
                            <input type="text" name="request_id" autocomplete="off"  value="{{ @$patient_data[0]->request_id ? $patient_data[0]->request_id : '' }}"
                                class="form-control" id="request_id" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Request Date</label>
                            <input type="text" name="request_date" autocomplete="off"  value="{{ @$patient_data[0]->req_date ? $patient_data[0]->req_date : '' }}"
                                class="form-control ins_datepicker" id="request_date" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Requesting Provider</label>
                            <input type="text" name="request_provider" autocomplete="off"  
                            value="{{ @$patient_data[0]->request_provider ? $patient_data[0]->request_provider : '' }}"
                                class="form-control" id="request_provider" readonly>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Patient Name</label>
                            <input type="text" name="patient_name" autocomplete="off"
                                value="{{ @$patient_data[0]->patient_name ? $patient_data[0]->patient_name : '' }}"
                                class="form-control" id="patient_name" readonly>
                        </div>
                    </div>

                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">DOB</label>
                            <input type="text" name="patient_dob" autocomplete="off" 
                                value="{{ @$patient_data[0]->dob ? $patient_data[0]->dob : '' }}" class="form-control"
                                id="patient_dob" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Gender</label>
                            <input type="text" name="patient_gender" autocomplete="off"
                                value="{{ @$patient_data[0]->gender_name ? $patient_data[0]->gender_name : '' }}"
                                class="form-control" id="patient_gender" readonly>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Insurance Name</label>
                            <input type="text" name="insurance_name" autocomplete="off"  value="{{ @$patient_data[0]->company_name ? $patient_data[0]->company_name : '' }}"
                                class="form-control" id="insurance_name" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Policy ID</label>
                            <input type="text" name="policy_id" autocomplete="off"  value = "{{ @$patient_data[0]->insurance_id_no ? $patient_data[0]->insurance_id_no : '' }}"
                                class="form-control" id="policy_id" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Member</label>
                            <input type="text" name="member" autocomplete="off" value="{{ @$patient_data[0]->dependent_patient_name ? $patient_data[0]->dependent_patient_name : '' }}" class="form-control"
                                id="member" readonly>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Pre Authorization</label>
                            <input type="text" name="authorization" autocomplete="off" autofocus value=""
                                class="form-control" id="authorization" placeholder="">
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Valid From</label>
                            <input type="text" name="valid_from" autocomplete="off" autofocus value=""
                                class="form-control ins_datepicker" id="valid_from" placeholder="">
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Valid To</label>
                            <input type="text" name="valid_to" autocomplete="off" autofocus value=""
                                class="form-control ins_datepicker" id="valid_to" placeholder="">
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Co pay Percentage</label>
                            <input type="text" name="co_pay" autocomplete="off" autofocus value="" class="form-control"
                                id="co_pay" onkeyup="number_validation(this)">
                        </div>
                    </div>
                    <!-- <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">&nbsp;</label>
                            <input type="checkbox" name="co_pay_is_perc" autocomplete="off" value="1" id="co_pay_is_perc" > 
                            <span style="top: 2px;left: 6px;font-size: 12px;font-weight: 700;">&nbsp;Is percentage</span>
                        </div>
                    </div> -->
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Pre Auth Days</label>
                            <input type="text" name="pre_auth_days" autocomplete="off" autofocus value=""
                                class="form-control" id="pre_auth_days" onkeyup="number_validation(this)">
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Pre Auth Amount</label>
                            <input type="text" name="pre_auth_amount" autocomplete="off" autofocus value=""
                                class="form-control" id="pre_auth_amount" onkeyup="number_validation(this)">
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class = "fileUploaddiv">
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;" id="file_upload_append">
                        <div class="fileUpload">
                            <input type="file" class="btn-success" name="upload_document[]" id="upload_document_main" multiple >
                        </div>
                    </div>
                    <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;" id="file_upload_append">
                            <span id ="file_count"></span> <span onclick="clearFirstFiles(this)" style="color:red;padding-left:10px;cursor:pointer;display:none" id="clear_all_files">Delete</span>
                    </div>
                    <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                       <span onclick="fileuploadDiv(this)" style="padding-left:10px;cursor:pointer" class="btn btn-warning">Add More</span>
                    </div>
                    <div class="clearfix"></div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-12 padding_sm" style="padding-bottom: 10px;">
                        <label style="top: 2px;left: 6px;font-size: 12px;font-weight: 700;" 
                            >Comments</label>
                        <textarea rows="4" cols="50" Style="Width:100%" name="comments" id="comments"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>