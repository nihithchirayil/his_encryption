@php $i = 0; @endphp
@if ($offset == 0)
<div class="row">
    <div class="col-md-12 theadscroll" id="load_data" style="position: relative; height: 550px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th width="2%" class="common_td_rules">UHID</th>
                    <th width="4%" class="common_td_rules">Patient Name</th>
                    <th width="5%" class="common_td_rules">Invoice No </th>
                    <th width="5%" class="common_td_rules">Bill Date</th>
                    <th width="5%" class="common_td_rules">Company</th>
                    <th width="4%" class="common_td_rules">Bill Amount</th>
                    <th width="4%" class="common_td_rules">Payor Payable</th>
                    <th width="4%" class="common_td_rules">Remaining Amount</th>
                    <th width="2%" class="common_td_rules">Req.</th>
                    <th width="2%" class="common_td_rules">Pre Auth</th>
                    <th width="2%" class="common_td_rules">Final Auth</th>
                </tr>
            </thead>
            <tbody id="tbody03">
                @if(isset($insurance_list) && sizeof($insurance_list) > 0)
                @foreach($insurance_list as $list)

                <?php if($list->settled_status==2){ 
                                  $bg_color="background-color: #bee9e7 !important"; 
                              }else if( $list->settled_status == 1 ){ $bg_color="background-color:#cae5a7 !important";}else{ $bg_color = ""; } ?>
                <tr style="cursor:pointer;<?= $bg_color; ?>">
                    <input type="hidden" value="{{$list->bill_id}}" name="bill_id" id="bill_head_id_<?= $i ?>">
                    <input type="hidden" value="{{$list->bill_from}}" name="bill_from" id="bill_from_<?= $i ?>">
                    <input type="hidden" value="{{$list->visit_id}}" name="visit_id" id="visit_id_<?= $i ?>">
                    <input type="hidden" value="{{$list->company_id}}" name="company_id" id="company_id_<?= $i ?>">
                    <input type="hidden" value="{{$list->rec_amnt}}" name="rec_amnt" id="rec_amnt_<?= $i ?>">
                    <td class="common_td_rules">{{ $list->uhid }}</td>
                    <td class="common_td_rules">{{ $list->pat }}</td>
                    <td class="common_td_rules">{{ $list->bill_no }}</td>
                    <td class="common_td_rules">{{ $list->bill_date }}</td>
                    <td class="common_td_rules">{{ $list->company_name }}</td>
                    <td class="td_common_numeric_rules">{{ $list->net_amount }}</td>
                    <td class="td_common_numeric_rules">{{ $list->claim_amount }}</td>
                    <td class="td_common_numeric_rules">{{ $list->rec_amnt }}</td>
                    <td>
                        @if(isset($list->irq_id) && $list->irq_id == 0)
                        <button title="Pre auth request"
                            onclick="viewpreAuthRequest('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->patid }}','{{$list->company_id}}','{{ $list->pat }}','{{ $list->consulting_doctor_id }}','{{$i}}')"
                            class="btn btn-sm btn-success">
                            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa fa-paper-plane"></i>
                        </button>
                        @else
                        <button title="Pre auth request"
                            onclick="viewpreAuthRequestUpdate('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->patid }}','{{$list->company_id}}','{{ $list->pat }}','{{ $list->consulting_doctor_id }}','{{$list->irq_id}}')"
                            class="btn btn-sm btn-warning">
                            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa fa-info"></i </button>
                            @endif
                    </td>
                    <td>
                        @if(isset($list->irq_id) && $list->irq_id != '')
                        @if(isset($list->ipq_id) && $list->ipq_id == 0)
                        <button title="Pre Auth"
                            onclick="viewpreAuth('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->uhid }}','{{$i}}')"
                            class="btn btn-sm btn-info">
                            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa-thumbs-o-up"></i>
                        </button>
                        @else
                        <button title="Pre auth request"
                            onclick="viewpreAuthUpdate('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->patid }}','{{$list->company_id}}','{{ $list->pat }}','{{ $list->consulting_doctor_id }}','{{$list->ipq_id}}')"
                            class="btn btn-sm btn-warning">
                            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa fa-info"></i>
                        </button>
                        @endif
                        @endif
                    </td>
                    <td>
                        @if(isset($list->ipq_id) && $list->ipq_id != '')
                        @if(isset($list->ifq_id) && $list->ifq_id == 0)
                        <button title="Pre Auth"
                            onclick="viewpreAuthFinal('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->uhid }}','{{$i}}')"
                            class="btn btn-sm btn-info">
                            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa-handshake-o"></i>
                        </button>
                        @else
                        <button title="Pre auth request"
                            onclick="viewpreAuthUpdateFinal('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->patid }}','{{$list->company_id}}','{{ $list->pat }}','{{ $list->consulting_doctor_id }}','{{$list->ipq_id}}')"
                            class="btn btn-sm btn-warning">
                            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa fa-info"></i>
                        </button>
                        @endif
                        @endif
                    </td>





                </tr>
                <?php $i++; ?>
                @endforeach
                @endif

            </tbody>
        </table>
    </div>
</div>

<script>
$('#load_data').on('scroll', function() {
    var scrollHeight = $('#load_data').height();
    var scrollPosition = $('#load_data').scrollTop() + $('#load_data').innerHeight();
    if (scrollPosition + 3 >= $('#load_data')[0].scrollHeight) {

        offset = offset + limit;
        if (offset < total_rec) {
            setTimeout(function() {
                searchInsuranceCurrencyData(limit, offset);
            }, 500);
        }
    }
})
</script>
@else
@if(isset($insurance_list) && sizeof($insurance_list) > 0)
@foreach($insurance_list as $list)

<?php if($list->settled_status==2){ 
                                  $bg_color="background-color: #bee9e7 !important"; 
                              }else if( $list->settled_status == 1 ){ $bg_color="background-color:#cae5a7 !important";}else{ $bg_color = ""; } ?>
<tr style="cursor:pointer;<?= $bg_color; ?>">
    <input type="hidden" value="{{$list->bill_id}}" name="bill_id" id="bill_head_id_<?= $i ?>">
    <input type="hidden" value="{{$list->bill_from}}" name="bill_from" id="bill_from_<?= $i ?>">
    <input type="hidden" value="{{$list->visit_id}}" name="visit_id" id="visit_id_<?= $i ?>">
    <input type="hidden" value="{{$list->company_id}}" name="company_id" id="company_id_<?= $i ?>">
    <input type="hidden" value="{{$list->rec_amnt}}" name="rec_amnt" id="rec_amnt_<?= $i ?>">
    <td class="common_td_rules">{{ $list->uhid }}</td>
    <td class="common_td_rules">{{ $list->pat }}</td>
    <td class="common_td_rules">{{ $list->bill_no }}</td>
    <td class="common_td_rules">{{ $list->bill_date }}</td>
    <td class="common_td_rules">{{ $list->company_name }}</td>
    <td class="td_common_numeric_rules">{{ $list->net_amount }}</td>
    <td class="td_common_numeric_rules">{{ $list->claim_amount }}</td>
    <td class="td_common_numeric_rules">{{ $list->rec_amnt }}</td>
    <td>
        @if(isset($list->irq_id) && $list->irq_id == 0)
        <button title="Pre auth request"
            onclick="viewpreAuthRequest('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->patid }}','{{$list->company_id}}','{{ $list->pat }}','{{ $list->consulting_doctor_id }}','{{$i}}')"
            class="btn btn-sm btn-success">
            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa fa-paper-plane"></i>
        </button>
        @else
        <button title="Pre auth request"
            onclick="viewpreAuthRequestUpdate('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->patid }}','{{$list->company_id}}','{{ $list->pat }}','{{ $list->consulting_doctor_id }}','{{$list->irq_id}}')"
            class="btn btn-sm btn-warning">
            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa fa-info"></i>
        </button>
        @endif
    </td>
    <td>
        @if(isset($list->irq_id) && $list->irq_id != '')
        @if(isset($list->ipq_id) && $list->ipq_id == 0)
        <button title="Pre Auth"
            onclick="viewpreAuth('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->uhid }}','{{$i}}')"
            class="btn btn-sm btn-info">
            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa-thumbs-o-up"></i>
        </button>
        @else
        <button title="Pre auth request"
            onclick="viewpreAuthUpdate('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->patid }}','{{$list->company_id}}','{{ $list->pat }}','{{ $list->consulting_doctor_id }}','{{$list->ipq_id}}')"
            class="btn btn-sm btn-warning">
            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa fa-info"></i>
        </button>
        @endif
        @endif
    </td>
    <td>
        @if(isset($list->ipq_id) && $list->ipq_id != '')
        @if(isset($list->ifq_id) && $list->ifq_id == 0)
        <button title="Pre Auth"
            onclick="viewpreAuthFinal('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->uhid }}','{{$i}}')"
            class="btn btn-sm btn-info">
            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa-handshake-o"></i>
        </button>
        @else
        <button title="Pre auth request"
            onclick="viewpreAuthUpdateFinal('{{$list->bill_id}}','{{$list->bill_from}}','{{ $list->patid }}','{{$list->company_id}}','{{ $list->pat }}','{{ $list->consulting_doctor_id }}','{{$list->ipq_id}}')"
            class="btn btn-sm btn-warning">
            <i id="view_ins_spin_{{$list->bill_id}}" class="fa fa fa-info"></i>
        </button>
        @endif
        @endif
    </td>


</tr>
<?php $i++; ?>
@endforeach
@endif
@endif