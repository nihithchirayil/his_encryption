<div class="right_col" role="main">
    <div class="row codfox_container">
        <input type="hidden" name="patient_id" id="patient_id" value="{{@$patient_id ? $patient_id : 0}}">
        <input type="hidden" name="company_id" id="company_id" value="{{@$company_id ? $company_id : 0}}">
        <input type="hidden" name="bill_id" id="bill_id" value="{{@$bill_id ? $bill_id : 0 }}">
        <input type="hidden" name="bill_from" id="bill_from" value="{{@$bill_from ? $bill_from: ''}}">
        <input type="hidden" name="consulting_doctor_id" id="consulting_doctor_id" value="{{@$consulting_doctor_id ? $consulting_doctor_id : 0}}">
        <div class="col-md-12 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Request Id.</label>
                            <input type="text" name="request_id" autocomplete="off"  value="{{@$request_id ? $request_id : 0}}"
                                class="form-control" id="request_id" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Request Date</label>
                            <input type="text" name="request_date" autocomplete="off"  value=""
                                class="form-control ins_datepicker" id="request_date" >
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Requesting Provider</label>
                            <input type="text" name="request_provider" autocomplete="off"  
                            value="{{@$consulting_doctor_name ? $consulting_doctor_name : ''}}" class="form-control" id="request_provider" readonly>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Patient Name</label>
                            <input type="text" name="patient_name" autocomplete="off"
                                value="{{ @$patient_name ? $patient_name : '' }}"
                                class="form-control" id="patient_name" readonly >
                        </div>
                    </div>
                                       
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class = "fileUploaddiv">
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;" id="file_upload_append">
                        <div class="fileUpload">
                            <input type="file" class="btn-success" name="upload_document[]" id="upload_document_main" multiple >
                        </div>
                    </div>
                    <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;" id="file_upload_append">
                            <span id ="file_count"></span> <span onclick="clearFirstFiles(this)" style="color:red;padding-left:10px;cursor:pointer;display:none" id="clear_all_files">Delete</span>
                    </div>
                    <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                       <span onclick="fileuploadDiv(this)" style="padding-left:10px;cursor:pointer" class="btn btn-warning">Add More</span>
                    </div>
                    <div class="clearfix"></div>
                    </div>

                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-12 padding_sm" style="padding-bottom: 10px;">
                        <label style="top: 2px;left: 6px;font-size: 12px;font-weight: 700;" 
                            >Comments</label>
                        <textarea rows="4" cols="50" Style="Width:100%" name="comments" id="comments"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>