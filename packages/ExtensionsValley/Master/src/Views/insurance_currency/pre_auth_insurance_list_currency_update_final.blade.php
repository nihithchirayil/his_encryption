<div class="right_col" role="main">
    <div class="row codfox_container">
        <input type="hidden" name="patient_id" id="patient_id" value="{{ @$patient_data[0]->id ? $patient_data[0]->id : '' }}">
        <input type="hidden" name="bill_from" id="bill_from" value="{{ @$bill_from ? $bill_from : '' }}">
        <input type="hidden" name="bill_id" id="bill_id" value="{{ @$bill_id ? $bill_id : '' }}">
        <input type="hidden" name="pre_auth_req_tbl_id" id="pre_auth_req_tbl_id" value="{{ @$patient_data[0]->irq_id ? $patient_data[0]->irq_id : '' }}">
        <input type="hidden" name="company_id" id="company_id" value="{{ @$patient_data[0]->company_id ? $patient_data[0]->company_id : '' }}">
        <input type="hidden" name="request_provider_id" id="request_provider_id" value="{{ @$patient_data[0]->request_provider_id ? $patient_data[0]->request_provider_id : '' }}">
        <div class="col-md-12 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Request Id.</label>
                            <input type="text" name="request_id" autocomplete="off"  value="{{ @$patient_data[0]->request_id ? $patient_data[0]->request_id : '' }}"
                                class="form-control" id="request_id" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Request Date</label>
                            <input type="text" name="request_date" autocomplete="off"  value="{{ @$patient_data[0]->req_date ? $patient_data[0]->req_date : '' }}"
                                class="form-control ins_datepicker" id="request_date" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Requesting Provider</label>
                            <input type="text" name="request_provider" autocomplete="off"  
                            value="{{ @$patient_data[0]->request_provider ? $patient_data[0]->request_provider : '' }}"
                                class="form-control" id="request_provider" readonly>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Patient Name</label>
                            <input type="text" name="patient_name" autocomplete="off"
                                value="{{ @$patient_data[0]->patient_name ? $patient_data[0]->patient_name : '' }}"
                                class="form-control" id="patient_name" readonly>
                        </div>
                    </div>

                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel"> DOB</label>
                            <input type="text" name="patient_dob" autocomplete="off" 
                                value="{{ @$patient_data[0]->dob ? $patient_data[0]->dob : '' }}" class="form-control"
                                id="patient_dob" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel"> Gender</label>
                            <input type="text" name="patient_gender" autocomplete="off"
                                value="{{ @$patient_data[0]->gender_name ? $patient_data[0]->gender_name : '' }}"
                                class="form-control" id="patient_gender" readonly>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Insurance Name</label>
                            <input type="text" name="insurance_name" autocomplete="off"  value="{{ @$patient_data[0]->company_name ? $patient_data[0]->company_name : '' }}"
                                class="form-control" id="insurance_name" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Policy ID</label>
                            <input type="text" name="policy_id" autocomplete="off"  value = "{{ @$patient_data[0]->policy_id ? $patient_data[0]->policy_id : '' }}"
                                class="form-control" id="policy_id" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Member</label>
                            <input type="text" name="member" autocomplete="off" value="{{ @$patient_data[0]->member ? $patient_data[0]->member : '' }}" class="form-control"
                                id="member" readonly>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Final Authorization</label>
                            <input type="text" name="authorization" autocomplete="off" autofocus value="{{ @$patient_data[0]->authorization ? $patient_data[0]->authorization : '' }}"
                                class="form-control" id="authorization" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Valid From</label>
                            <input type="text" name="valid_from" autocomplete="off" autofocus value="{{ @$patient_data[0]->valid_from ? date('M-d-Y',strtotime($patient_data[0]->valid_from)) : '' }}"
                                class="form-control ins_datepicker" id="valid_from" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Valid To</label>
                            <input type="text" name="valid_to" autocomplete="off" autofocus value="{{ @$patient_data[0]->valid_to ? date('M-d-Y',strtotime($patient_data[0]->valid_to)) : '' }}"
                                class="form-control ins_datepicker" id="valid_to" readonly>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Final Co pay Percentage</label>
                            <input type="text" name="co_pay" autocomplete="off" autofocus readonly class="form-control"
                                id="co_pay" value="{{ @$patient_data[0]->co_pay ? $patient_data[0]->co_pay : '' }}">
                        </div>
                    </div>
                    
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Final Auth Days</label>
                            <input type="text" name="pre_auth_days" autocomplete="off" autofocus value="{{ @$patient_data[0]->pre_auth_days ? $patient_data[0]->pre_auth_days : '' }}"
                                class="form-control" id="pre_auth_days" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Final Auth Amount</label>
                            <input type="text" name="pre_auth_amount" autocomplete="off" autofocus value="{{ @$patient_data[0]->pre_auth_amount ? $patient_data[0]->pre_auth_amount : '' }}"
                                class="form-control" id="pre_auth_amount" readonly>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                         <?php
                         foreach($patient_data as $pd){
                         if($pd->filename) {
                        $upload_view_path = url('/packages/extensionsvalley/insuranceNew/uploads/request/');
                        $filePath = $upload_view_path . "/" . $pd->filename; 
                         ?>
                    <a title="Download File" href="<?= $filePath ?>" download style="padding: 0px 5px;"
                        class="btn btn-warning"><i class="fa fa-download"></i>{{$pd->original_file_name}}</a>
                       <?php } } ?>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-12 padding_sm" style="padding-bottom: 10px;">
                        <label style="top: 2px;left: 6px;font-size: 12px;font-weight: 700;" 
                            >Comments</label>
                        <textarea rows="4" cols="50" Style="Width:100%" name="comments" id="comments">{{ @$patient_data[0]->comments ? $patient_data[0]->comments : '' }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>