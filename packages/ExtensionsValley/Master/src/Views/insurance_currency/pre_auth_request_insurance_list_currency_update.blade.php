<div class="right_col" role="main">
    <div class="row codfox_container">
        <input type="hidden" name="patient_id" id="patient_id" value="{{@$patient_id ? $patient_id : 0}}">
        <input type="hidden" name="company_id" id="company_id" value="{{@$company_id ? $company_id : 0}}">
        <input type="hidden" name="bill_id" id="bill_id" value="{{@$bill_id ? $bill_id : 0 }}">
        <input type="hidden" name="bill_from" id="bill_from" value="{{@$bill_from ? $bill_from: ''}}">
        <input type="hidden" name="consulting_doctor_id" id="consulting_doctor_id" value="{{@$consulting_doctor_id ? $consulting_doctor_id : 0}}">
        <div class="col-md-12 padding_sm">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Request Id.</label>
                            <input type="text" name="request_id" autocomplete="off"  value="{{@$request_id ? $request_id : 0}}"
                                class="form-control" id="request_id" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Request Date</label>
                            <input type="text" name="request_date" autocomplete="off"  value="{{@$data_req[0]->request_date ? $data_req[0]->request_date : ''}}"
                                class="form-control" id="request_date" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Requesting Provider</label>
                            <input type="text" name="request_provider" autocomplete="off"  
                            value="{{@$consulting_doctor_name ? $consulting_doctor_name : ''}}" class="form-control" id="request_provider" readonly>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                        <div class="mate-input-box">
                            <label class="custom_floatlabel">Patient Name</label>
                            <input type="text" name="patient_name" autocomplete="off"
                                value="{{ @$patient_name ? $patient_name : '' }}"
                                class="form-control" id="patient_name" readonly >
                        </div>
                    </div>
                                       
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-4 padding_sm" style="padding-bottom: 10px;">
                    <?php
                    foreach($data_req as $rq)
                    if($rq->filename) {
                        $upload_view_path = url('/packages/extensionsvalley/insuranceNew/uploads/request/');
                        $filePath = $upload_view_path . "/" . $rq->filename; 
                         ?>
                    <a title="Download File" href="<?= $filePath ?>" download style="padding: 0px 5px;"
                        class="btn btn-warning"><i class="fa fa-download">{{$rq->original_file_name}}</i></a>
                       <?php } ?>
                    </div>
                    <div class="clearfix" style="padding-bottom:10px !important"></div>
                    <div class="col-xs-12 padding_sm" style="padding-bottom: 10px;">
                        <label style="top: 2px;left: 6px;font-size: 12px;font-weight: 700;" 
                            >Comments</label>
                        <textarea rows="4" cols="50" Style="Width:100%" name="comments" id="comments" readonly>{{@$data_req[0]->comments ? $data_req[0]->comments : ''}}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>