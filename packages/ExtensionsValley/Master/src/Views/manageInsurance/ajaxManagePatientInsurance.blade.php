<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var from_date = $('#bill_from_date').val();
                var to_date = $('#bill_to_date').val();
                var patient_id = $('#patient_id_hidden').val();
                var visit_type = $('#visit_type').val();
                var insurance_company = $('#insurance_company').val();
                var insurance_schema = $('#insurance_schema').val();
                var insurance_doctor = $('#insurance_doctor').val();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        from_date: from_date,
                        to_date: to_date,
                        patient_id: patient_id,
                        visit_type: visit_type,
                        insurance_company: insurance_company,
                        insurance_schema: insurance_schema,
                        insurance_doctor: insurance_doctor
                    },
                    beforeSend: function () {
                        $("#searchmanagePatientInsuranceBtn").attr("disabled", true);
                        $("#searchmanagePatientInsuranceSpin").removeClass("fa fa-search");
                        $("#searchmanagePatientInsuranceSpin").addClass("fa fa-spinner fa-spin");
                        $("#managePatientInsuranceDiv").LoadingOverlay("show", {
                            background: "rgba(89, 89, 89, 0.6)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function (html) {
                        $('#managePatientInsuranceDiv').html(html);
                    },
                    complete: function () {
                        $("#searchmanagePatientInsuranceBtn").attr("disabled", false);
                        $("#searchmanagePatientInsuranceSpin").removeClass("fa-spinner fa-spin");
                        $("#searchmanagePatientInsuranceSpin").addClass("fa fa-search");
                        $("#managePatientInsuranceDiv").LoadingOverlay("hide");
                    },
                    error: function () {
                        toastr.error(
                            "Error Please Check Your Internet Connection and Try Again"
                        );
                    },
                });
            }
            return false;
        });

    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th class="common_td_rules" width="20%">Patient Name</th>
                    <th class="common_td_rules" width="20%">UHID</th>
                    <th class="common_td_rules" width="20%">Doctor Name</th>
                    <th class="common_td_rules" width="20%">Insurance Company</th>
                    <th class="common_td_rules" width="20%">Visit Date Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
            if (count($insurance_list) != 0) {
                foreach ($insurance_list as $list) {
                    ?>
                <tr style="cursor: pointer" class="editPatientInsurance"
                    id="editPatientInsuranceBtn{{ $list->patient_id  }}"
                    onclick="editPatientInsurance({{ $list->patient_id }},{{ $list->current_visit_id }})">
                    <td class="common_td_rules"> {{ $list->patient_name }}</td>
                    <td class="common_td_rules"> {{ $list->uhid }}</td>
                    <td class="common_td_rules"> {{ $list->consulting_doctor }}</td>
                    <td id="company_name{{ $list->patient_id  }}" class="common_td_rules"> {{ $list->company_name }}
                    </td>
                    <td class="common_td_rules">{{ date('M-d-Y h:i A', strtotime($list->visit_datetime)) }}</td>
                </tr>
                <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="7" style="text-align: center">No Records Found</td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
