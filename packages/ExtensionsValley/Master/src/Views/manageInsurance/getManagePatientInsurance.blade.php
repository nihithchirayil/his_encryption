@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<div class="right_col" role="main">
    <div class="row padding_sm">
        <div class="col-md-2 padding_sm pull-right">
            <?= $title ?>
        </div>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="token" value="{{ csrf_token() }}">
        <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    </div>
    <div class="row padding_sm">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #8aa2f9;min-height: 80px;">
                        <div class="col-md-10 padding_sm">
                            <div class="col-md-1 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" id="bill_from_date"
                                        value="{{ date('M-d-Y') }}">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" id="bill_to_date"
                                        value="{{ date('M-d-Y') }}">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label for="">Patient Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="patient_name"
                                        name="patient_name" value="">
                                    <div id="patient_idAjaxDiv" class="ajaxSearchBox"
                                        style="margin-top: -15px; z-index: 99999; max-height: 300px !important; display: none;">
                                    </div>
                                    <input type="hidden" name="patient_id_hidden" value="" id="patient_id_hidden">
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label for="">Visit Type</label>
                                    <div class="clearfix"></div>
                                    <select id="visit_type" class="form-control select2">
                                        <option value="">All</option>
                                        <option value="OP">OP</option>
                                        <option value="IP">IP</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label for="">Insurance Company</label>
                                    <div class="clearfix"></div>
                                    <select onchange="getCompanyPricing('insurance_company',0)" id="insurance_company"
                                        name="insurance_company" class="form-control select2">
                                        <option value="">Select</option>
                                        @foreach ($insurance_company as $each)
                                        <option value="{{ $each->id }}">{{ $each->company_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label for="">Insurance Schema</label>
                                    <div class="clearfix"></div>
                                    <select id="insurance_companyschema" name="insurance_companyschema"
                                        class="form-control select2">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label for="">Doctor</label>
                                    <div class="clearfix"></div>
                                    <select id="insurance_doctor" name="insurance_doctor" class="form-control select2">
                                        <option value="">Select</option>
                                        @foreach ($doctor_master as $each)
                                        <option value="{{ $each->id }}">{{ $each->doctor_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="col-md-6 padding_sm pull-right" style="margin-top: 22px">
                                <button type="button" id="searchmanagePatientInsuranceBtn"
                                    onclick="searchmanagePatientInsurance()" class="btn btn-primary btn-block">
                                    Search <i class="fa fa-search" id="searchmanagePatientInsuranceSpin"></i>
                                </button>
                            </div>
                            <div class="col-md-6 padding_sm pull-right" style="margin-top: 22px">
                                <button type="button" onclick="resetsearchForm()" class="btn btn-warning btn-block">
                                    Reset
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-8 padding_sm" style="margin-top: 10px">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #8aa2f9;min-height: 500px;">
                        <div id="managePatientInsuranceDiv"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 padding_sm" style="margin-top: 10px">
                <input type="hidden" name="edit_patient_id_hidden" value="0" id="edit_patient_id_hidden">
                <input type="hidden" name="current_visit_id_hidden" value="0" id="current_visit_id_hidden">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #8aa2f9;min-height:500px;">
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label for="">Insurance Company</label>
                                <div class="clearfix"></div>
                                <select onchange="getCompanyPricing('edit_insurance_company',0)"
                                    id="edit_insurance_company" name="edit_insurance_company"
                                    class="form-control select2">
                                    <option value="">Select</option>
                                    @foreach ($insurance_company as $each)
                                    <option value="{{ $each->id }}">{{ $each->company_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label for="">Insurance Schema</label>
                                <div class="clearfix"></div>
                                <select id="edit_insurance_companyschema" name="edit_insurance_companyschema"
                                    class="form-control select2">
                                    <option value="">Select</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label for="">Policy Id</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="edit_policy_id">
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label for="">Insurance ID</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" id="edit_insurance_id">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label for="">Credit Limit</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    id="edit_credit_limit">
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label for="">Valid Till</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" id="edit_valid_till">
                            </div>
                        </div>
                        <div class="col-md-12 padding_sm" style="margin-top: 10px">
                            <h5 class="blue"><strong>Principal Member</strong></h5>
                            <hr>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Dependent Patient Name/UHID</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="dependent_patient_name"
                                    name="dependent_patient_name" value="">
                                <div id="dependentpatient_idAjaxDiv" class="ajaxSearchBox"
                                    style="margin-top: -15px; z-index: 99999; max-height: 300px !important; display: none;">
                                </div>
                                <input type="hidden" name="dependentpatient_id_hidden" value=""
                                    id="dependentpatient_id_hidden">
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Dependent Patient Name</label>
                                <div class="clearfix"></div>
                                <input readonly style="background: #FFF" type="text" class="form-control"
                                    id="dependentpatient_name">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label for="">Dependent Patient Relation</label>
                                <div class="clearfix"></div>
                                <select id="dependentpatient_relation" name="dependentpatient_relation"
                                    class="form-control select2">
                                    <option value="">Select</option>
                                    @foreach ($relationship_master as $each)
                                    <option value="{{ $each->id }}">{{ $each->description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 padding_sm pull-right" style="margin-top: 22px">
                            <button type="button" id="savePatientInsuranceBtn" onclick="savePatientInsurance()"
                                class="btn btn-success btn-block">
                                Save <i class="fa fa-save" id="savePatientInsuranceSpin"></i>
                            </button>
                        </div>
                        <div class="col-md-6 padding_sm pull-right" style="margin-top: 22px">
                            <button type="button" onclick="reseteditForm()" class="btn btn-warning btn-block">
                                Reset
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('javascript_extra')

<script
    src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/master/default/javascript/managePatientInsurance.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
