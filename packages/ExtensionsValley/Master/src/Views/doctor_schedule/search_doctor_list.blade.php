<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var doctor_id = $('#doctor_id').val();
                var speciality_id = $('#speciality_id').val();
                var param = {
                    _token: token,
                    speciality_id: speciality_id,
                    doctor_id: doctor_id
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $('#getDoctrorListBtn').attr('disabled', true);
                        $('#getDoctrorListSpin').removeClass('fa fa-search');
                        $('#getDoctrorListSpin').addClass('fa fa-spinner fa-spin');
                        $("#getDoctrorListData").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        $('#getDoctrorListData').html(data);
                    },
                    complete: function() {
                        $('#getDoctrorListBtn').attr('disabled', false);
                        $('#getDoctrorListSpin').removeClass('fa fa-spinner fa-spin');
                        $('#getDoctrorListSpin').addClass('fa fa-search');
                        $("#getDoctrorListData").LoadingOverlay("hide");
                    },
                    error: function() {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
            return false;
        });

    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th class="common_td_rules" width="25%">Doctor Name</th>
                    <th class="common_td_rules" width="10%">Phone</th>
                    <th class="common_td_rules" width="20%">Email</th>
                    <th class="common_td_rules" width="20%">Qualification</th>
                    <th class="common_td_rules" width="25%">Speciality</th>
                </tr>
            </thead>
            <tbody>
                <?php
            if (count($doctor_list) != 0) {
                foreach ($doctor_list as $list) {
                    ?>
                <tr class="editDoctorMasterRow" id="editDoctorMasterRow{{ $list->doctor_id }}" style="cursor: pointer" onclick="editDoctorMaster({{ $list->doctor_id }})">
                    <td class="common_td_rules">{{ $list->doctor_name }}</td>
                    <td class="common_td_rules">{{ $list->phone }}</td>
                    <td class="common_td_rules">{{ $list->email }}</td>
                    <td class="common_td_rules">{{ $list->qualification }}</td>
                    <td class="common_td_rules">{{ $list->speciality }}</td>
                </tr>
                <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="8" class="re-records-found">No Records Found</td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
