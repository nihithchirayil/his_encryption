@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="token" value="{{ csrf_token() }}">
    <input type="hidden" id="doctor_list_id" value="0">

    <div class="right_col" style="min-height: 700px !important;">
        <div class="container-fluid">
            <div class="row padding_sm">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-6 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 600px;">
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label for="">Doctor</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control" id="doctor_name">
                                        <div id="doctor_name_AjaxDiv" class="ajaxSearchBox"
                                            style="margin-top:13px;z-index: 99999;margin-left: -4px;max-height:250px"></div>
                                        <input type="hidden" autocomplete="off" class="form-control" id="doctor_id">
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                    <div class="mate-input-box">
                                        <label for="">Speciality</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control" id="speciality_name">
                                        <div id="speciality_name_AjaxDiv" class="ajaxSearchBox"
                                            style="margin-top:13px;z-index: 99999;margin-left: -4px;max-height:250px"></div>
                                        <input type="hidden" autocomplete="off" class="form-control" id="speciality_id">
                                    </div>
                                </div>
                                <div class="col-md-2 pull-right padding_sm" style="margin-top: 22px">
                                    <button class="btn btn-primary btn-block" id="getDoctrorListBtn"
                                        onclick="getDoctorList();">
                                        <i id="getDoctrorListSpin" class="fa fa-search"></i> Search
                                    </button>
                                </div>
                                <div class="col-md-2 pull-right padding_sm" style="margin-top: 22px">
                                    <button class="btn btn-warning btn-block" onclick="resetForm();">
                                        <i class="fa fa-recycle"></i> Reset
                                    </button>
                                </div>
                                <div class="col-md-12 padding_sm" id="getDoctrorListData">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" id="patientappoinment_div">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 600px;">
                                <div class="col-md-12 padding_sm text-center green">
                                    <strong id="appointment_title">Add Doctor Schedule Master</strong>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 padding_sm" style="margin-top: 10px;min-height: 500px;">
                                    <ul class="nav nav-tabs sm_nav">
                                        <li class="active"><a data-toggle="tab" href="#doctorBasicDetailsli"
                                                style="border-radius: 0px;" aria-expanded="false">Basic Details</a></li>
                                        <li class=""><a data-toggle="tab" href="#doctorScheduleDetailsli"
                                                style="border-radius: 0px;" aria-expanded="false">Schedule</a></li>
                                        <li class=""><a data-toggle="tab" href="#doctorPricingDetailsli"
                                                style="border-radius: 0px;" aria-expanded="true">Pricing</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="doctorBasicDetailsli" class="tab-pane active">
                                            <div class="box no-border no-margin">
                                                <div class="box-body clearfix">
                                                    <div class="row padding_sm">
                                                        <div class="col-md-6 padding_sm" style="margin-top: 12px">
                                                            <div class="mate-input-box">
                                                                <label for="">Doctor Name</label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" autocomplete="off"
                                                                    class="form-control" id="add_doctor_name">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 padding_sm" style="margin-top: 10px">
                                                            <div class="mate-input-box">
                                                                <label for="">Speciality</label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" autocomplete="off"
                                                                    class="form-control" id="speciality_name_add">
                                                                <div id="speciality_name_add_AjaxDiv"
                                                                    class="ajaxSearchBox"
                                                                    style="margin-top:13px;z-index: 99999;margin-left: -4px;max-height:250px">
                                                                </div>
                                                                <input type="hidden" autocomplete="off"
                                                                    class="form-control" id="speciality_id_add">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                            <div class="mate-input-box">
                                                                <label for="">Email</label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" onblur="isEmail()"
                                                                    autocomplete="off" class="form-control"
                                                                    id="add_doctor_email">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                            <div class="mate-input-box">
                                                                <label for="">Phone</label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" class="form-control"
                                                                    id="add_doctor_phone" autocomplete="off"
                                                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-12 padding_sm" style="margin-top: 5px">
                                                            <div class="mate-input-box">
                                                                <label for="">Qualification</label>
                                                                <div class="clearfix"></div>
                                                                <textarea autocomplete="off" class="form-control" id="add_doctor_qualification"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                            <div class="mate-input-box">
                                                                <label for="">UserName</label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" onblur="checkDoctorUserName()"
                                                                    autocomplete="off" class="form-control"
                                                                    id="add_doctor_user_name">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                            <div class="mate-input-box">
                                                                <label for="">Password</label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" class="form-control"
                                                                    id="add_doctor_password" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div id="doctorScheduleDetailsli" class="tab-pane">
                                            <div class="box no-border no-margin">
                                                <div class="box-body clearfix">
                                                    <div class="row padding_sm">
                                                        <div class="col-md-2 padding_sm"
                                                            style="margin-top: 5px;background-color: #34f7f747">
                                                            <label><strong>Day</strong></label>
                                                        </div>
                                                        <div class="col-md-5 padding_sm"
                                                            style="margin-top: 5px;background-color:#b5f7095c">
                                                            <label><strong>Morning</strong></label>
                                                        </div>
                                                        <div class="col-md-5 padding_sm"
                                                            style="margin-top: 5px;background-color:#ef8f095c">
                                                            <label><strong>Evening</strong></label>
                                                        </div>
                                                        <?php
                                                                $timestamp = strtotime('next Sunday');
                                                                $days = [];
                                                                for ($i = 0; $i < 7; $i++) {
                                                                    ?>
                                                        <div class="col-md-2 padding_sm"
                                                            style="margin-top: 5px;background-color: #34f7f747">
                                                            <div class="col-md-12 padding_sm">
                                                                <label
                                                                    style="margin-top:20px;"><strong><?= strftime('%A', $timestamp) ?></strong></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-10 padding_sm" style="margin-top: 5px">
                                                            <div class="col-md-2 no-padding no-margin"
                                                                style="background-color:#b5f7095c">
                                                                <div class="mate-input-box">
                                                                    <label><?= intval($i) == 0 ? 'From' : '' ?></label>
                                                                    <div class="clearfix"></div>
                                                                    <input
                                                                        ondblclick="updateAllRow('morning','from',<?= $i ?>)"
                                                                        type="time" attr-id='from_morning'
                                                                        attr-increment="<?= $i ?>"
                                                                        attr-name="<?= strftime('%A', $timestamp) ?>"
                                                                        class="form-control doctor_schedule from_morning"
                                                                        autocomplete="off" id="from_morning_<?= $i ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 no-padding no-margin"
                                                                style="background-color:#b5f7095c">
                                                                <div class="mate-input-box">
                                                                    <label><?= intval($i) == 0 ? 'To' : '' ?></label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="time" attr-id='to_morning'
                                                                        class="form-control doctor_schedule to_morning"
                                                                        autocomplete="off" attr-increment="<?= $i ?>"
                                                                        attr-name="<?= strftime('%A', $timestamp) ?>"
                                                                        ondblclick="updateAllRow('morning','to',<?= $i ?>)"
                                                                        id="to_morning_<?= $i ?>">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-1 no-padding no-margin"
                                                                style="background-color:#b5f7095c">
                                                                <div class="mate-input-box">
                                                                    <label><?= intval($i) == 0 ? 'Walkin' : '' ?></label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="text"
                                                                        class="form-control doctor_schedule walkin_morning"
                                                                        autocomplete="off" attr-id='walkin_morning'
                                                                        ondblclick="updateAllRow('morning','walkin',<?= $i ?>)"
                                                                        id="walkin_morning_<?= $i ?>" maxlength='3'
                                                                        attr-increment="<?= $i ?>"
                                                                        attr-name="<?= strftime('%A', $timestamp) ?>"
                                                                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-1 no-padding no-margin"
                                                                style="background-color:#b5f7095c">
                                                                <div class="mate-input-box">
                                                                    <label><?= intval($i) == 0 ? 'Dur.' : '' ?></label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="text"
                                                                        class="form-control doctor_schedule dur_morning"
                                                                        autocomplete="off" attr-id='dur_morning'
                                                                        attr-name="<?= strftime('%A', $timestamp) ?>"
                                                                        ondblclick="updateAllRow('morning','dur',<?= $i ?>)"
                                                                        id="dur_morning_<?= $i ?>" maxlength='3'
                                                                        attr-increment="<?= $i ?>"
                                                                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 no-padding no-margin"
                                                                style="background-color:#ef8f095c">
                                                                <div class="mate-input-box">
                                                                    <label><?= intval($i) == 0 ? 'From' : '' ?></label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="time" attr-id='from_evening'
                                                                        class="form-control doctor_schedule from_evening"
                                                                        autocomplete="off" attr-increment="<?= $i ?>"
                                                                        attr-name="<?= strftime('%A', $timestamp) ?>"
                                                                        ondblclick="updateAllRow('evening','from',<?= $i ?>)"
                                                                        id="from_evening_<?= $i ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 no-padding no-margin"
                                                                style="background-color:#ef8f095c">
                                                                <div class="mate-input-box">
                                                                    <label><?= intval($i) == 0 ? 'To' : '' ?></label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="time"
                                                                        class="form-control doctor_schedule to_evening"
                                                                        autocomplete="off" attr-id='to_evening'
                                                                        attr-increment="<?= $i ?>"
                                                                        attr-name="<?= strftime('%A', $timestamp) ?>"
                                                                        ondblclick="updateAllRow('evening','to',<?= $i ?>)"
                                                                        id="to_evening_<?= $i ?>">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-1 no-padding no-margin"
                                                                style="background-color:#ef8f095c">
                                                                <div class="mate-input-box">
                                                                    <label><?= intval($i) == 0 ? 'Walkin' : '' ?></label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="text"
                                                                        class="form-control doctor_schedule walkin_evening"
                                                                        autocomplete="off" attr-id='walkin_evening'
                                                                        attr-name="<?= strftime('%A', $timestamp) ?>"
                                                                        ondblclick="updateAllRow('evening','walkin',<?= $i ?>)"
                                                                        id="walkin_evening_<?= $i ?>" maxlength='3'
                                                                        attr-increment="<?= $i ?>"
                                                                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-1 no-padding no-margin"
                                                                style="background-color:#ef8f095c">
                                                                <div class="mate-input-box">
                                                                    <label><?= intval($i) == 0 ? 'Dur.' : '' ?></label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="text"
                                                                        class="form-control doctor_schedule dur_evening"
                                                                        autocomplete="off" attr-id='dur_evening'
                                                                        attr-name="<?= strftime('%A', $timestamp) ?>"
                                                                        ondblclick="updateAllRow('evening','dur',<?= $i ?>)"
                                                                        id="dur_evening_<?= $i ?>" maxlength='3'
                                                                        attr-increment="<?= $i ?>"
                                                                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <?php
                                                                    $timestamp = strtotime('+1 day', $timestamp);
                                                                }
                                                                ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div id="doctorPricingDetailsli" class="tab-pane">
                                            <div class="box no-border no-margin">
                                                <div class="box-body clearfix">
                                                    <div class="row padding_sm">
                                                        <div class="col-md-12 padding_sm">
                                                            <div class="mate-input-box">
                                                                <label>OP Consultation Fee</label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" class="form-control"
                                                                    autocomplete="off" id="op_consultation"
                                                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-12 padding_sm">
                                                            <div class="mate-input-box">
                                                                <label>Renewal Charges</label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" class="form-control"
                                                                    autocomplete="off" id="op_renewal_consultation"
                                                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-12 padding_sm">
                                                            <div class="mate-input-box">
                                                                <label>Renewal Rule Days</label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" class="form-control"
                                                                    autocomplete="off" id="renewal_days"
                                                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-12 padding_sm">
                                                            <div class="mate-input-box">
                                                                <label>No of Free Visit</label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" class="form-control"
                                                                    autocomplete="off" id="renewal_times"
                                                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12 padding_sm">
                                    <div class="col-md-3 padding_sm pull-right">
                                        <button type="button" id="saveSalaryMasterBtn" onclick="saveSalaryMaster()"
                                            class="btn btn-primary btn-block">Save <i class="fa fa-save"
                                                id="saveSalaryMasterSpin"></i></button>
                                    </div>
                                    <div class="col-md-3 padding_sm pull-right">
                                        <button type="button" onclick="resetSalaryMaster()"
                                            class="btn btn-warning btn-block">Reset <i class="fa fa-recycle"></i></button>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/doctor_schedule.js') }}"></script>
@endsection
