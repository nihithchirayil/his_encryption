<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var from_date = $('#Settled_from_date').val();
                var to_date = $('#Settled_to_date').val();
                var bill_no_tag = $('#bill_no_tag').val();
                var bill_id_hidden = $('#bill_id_hidden').val();
                var insurance_company = $('#insurance_company').val();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        from_date: from_date,
                        to_date: to_date,
                        bill_no_tag: bill_no_tag,
                        bill_id_hidden: bill_id_hidden,
                        insurance_company: insurance_company
                    },
                    beforeSend: function() {
                        $("#searchOpInsuranceSettlementBtn").attr("disabled", true);
                        $("#searchOpInsuranceSettlementSpin").removeClass("fa fa-search");
                        $("#searchOpInsuranceSettlementSpin").addClass(
                            "fa fa-spinner fa-spin");
                        $("#OpInsuranceSettlementDiv").LoadingOverlay("show", {
                            background: "rgba(89, 89, 89, 0.6)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(html) {
                        $('#OpInsuranceSettlementDiv').html(html);
                    },
                    complete: function() {
                        $("#searchOpInsuranceSettlementBtn").attr("disabled", false);
                        $("#searchOpInsuranceSettlementSpin").removeClass(
                            "fa-spinner fa-spin");
                        $("#searchOpInsuranceSettlementSpin").addClass("fa fa-search");
                        $("#OpInsuranceSettlementDiv").LoadingOverlay("hide");
                    },
                    error: function() {
                        toastr.error(
                            "Error Please Check Your Internet Connection and Try Again"
                        );
                    },
                });
            }
            return false;
        });

    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 400px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th class="common_td_rules" width="15%">Bill No</th>
                    <th class="common_td_rules" width="20%">Bill Tag</th>
                    <th class="common_td_rules" width="10%">TDS Amount</th>
                    <th class="common_td_rules" width="15%">Write Off Amount</th>
                    <th class="common_td_rules" width="10%">Settled Amount</th>
                    <th class="common_td_rules" width="10%">Settled Date</th>
                    <th class="common_td_rules" width="20%">Company Name</th>
                </tr>
            </thead>
            <tbody>
                <?php
            if (count($insurance_list) != 0) {
                foreach ($insurance_list as $list) {
                    ?>
                <tr>
                    <td class="common_td_rules"> {{ $list->bill_no }}</td>
                    <td class="common_td_rules"> {{ $list->bill_tag }}</td>
                    <td class="td_common_numeric_rules"> {{ $list->tds_amount }}</td>
                    <td class="td_common_numeric_rules"> {{ $list->write_off_amount }}</td>
                    <td class="td_common_numeric_rules"> {{ $list->total_settled_amount }}</td>
                    <td class="common_td_rules">{{ date('M-d-Y', strtotime($list->settled_date)) }}</td>
                    <td class="common_td_rules">{{ $list->company_name }}</td>
                </tr>
                <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="7" class="re-records-found">No Records Found</td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
