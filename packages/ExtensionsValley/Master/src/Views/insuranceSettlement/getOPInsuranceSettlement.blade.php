@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <div class="right_col" role="main">
        <div class="row padding_sm">
            <div class="col-md-2 padding_sm pull-right">
                <?= $title ?>
            </div>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="token" value="{{ csrf_token() }}">
            <input type="hidden" id="first_date" value="{{ $first_date }}">
            <input type="hidden" id="last_date" value="{{ $last_date }}">
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #8aa2f9;min-height: 80px;">
                            <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label for="">Settled From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" id="Settled_from_date"
                                        value="{{ $first_date }}">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label for="">Settled To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control datepicker" id="Settled_to_date"
                                        value="{{ $last_date }}">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label for="">Bill No.</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="bill_no"
                                        name="bill_no" value="">
                                    <div id="bill_no_AjaxDiv" class="ajaxSearchBox"
                                        style="margin-top:-16px;z-index: 99999;max-height: 300px !important"></div>
                                    <input type="hidden" value="" id="bill_no_tag">
                                    <input type="hidden" value="" id="bill_id_hidden">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label for="">Insurance Company</label>
                                    <div class="clearfix"></div>
                                    <select id="insurance_company" name="insurance_company" class="form-control select2">
                                        <option value="All">Select</option>
                                        @foreach ($insurance_company as $each)
                                            <option value="{{ $each->id }}">{{ $each->company_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 22px">
                                <button type="button" id="searchOpInsuranceSettlementBtn" onclick="searchOpInsuranceSettlement()"
                                    class="btn btn-primary btn-block"> Search <i class="fa fa-search"
                                        id="searchOpInsuranceSettlementSpin"></i> </button>
                            </div>
                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 22px">
                                <button type="button" onclick="resetsearchForm()" class="btn btn-warning btn-block"> Reset
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-9 padding_sm" style="margin-top: 10px">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #8aa2f9;min-height: 500px;">
                            <div id="OpInsuranceSettlementDiv"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #8aa2f9;min-height:500px;">

                            <div class="box no-border no-margin">
                                <div class="box-footer"
                                    style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #8aa2f9;min-height: 180px;">
                                    <div class="col-md-12 padding_sm text-left" style="margin-top: 10px">
                                        <label><strong>Download Format </strong></label>
                                    </div>
                                    <div class="col-md-6 padding_sm" style="margin-top: 10px">
                                        <div class="mate-input-box">
                                            <label for="">From Date</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control datepicker"
                                                id="from_download_format" value="{{ $first_date }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding_sm" style="margin-top: 10px">
                                        <div class="mate-input-box">
                                            <label for="">To Date</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control datepicker" id="to_download_format"
                                                value="{{ $last_date }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12 padding_sm text-center" style="margin-top: 10px">
                                        <a style="display: none" id="downloadfile" href="/images/myw3schoolsimage.jpg"
                                            download>
                                        </a>
                                        <button id="downloadUploadFormatBtn" type="button"
                                            onclick="downloadUploadFormat()" class="btn btn-primary btn-block">Download
                                            Upload Format <i id="downloadUploadFormatSpin"
                                                class="fa fa-download"></i></button>
                                    </div>
                                </div>
                                <div class="box-footer"
                                    style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #8aa2f9;min-height: 125px;margin-top: 10px;">
                                    <div class="col-md-12 padding_sm text-left" style="margin-top: 10px">
                                        <label><strong>Upload Settlement </strong></label>
                                    </div>
                                    <form id="uploadBulkAppoinments">
                                        <div class="col-md-12 padding_sm" style="margin-top: -10px">
                                            <label for="">&nbsp;</label>
                                            <div class="clearfix"></div>
                                            <input style="height:25px !important" type="file"
                                                class="form-control btn bg-blue" name="upload_document"
                                                id="upload_document" />
                                        </div>
                                        <div class="col-md-12 padding_sm text-center" style="margin-top: 10px">
                                            <button type="submit" id="uploadBulkAppoinmetsBtn"
                                                class="btn btn-success btn-block">Upload Data <i
                                                    id="uploadBulkAppoinmetsSpin" class="fa fa-upload"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript_extra')

    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/opInsuranceSettlement.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
@endsection
