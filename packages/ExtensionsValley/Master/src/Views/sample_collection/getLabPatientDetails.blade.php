@php
$resultdetailid = '';
$bill_detail_id = '';
$remark = '';
$notes ='';

foreach ($details_array as $cols) {
    $resultdetailid = @$cols->resultdetailid ? $cols->resultdetailid : '';
    $bill_detail_id = @$cols->bill_detail_id ? $cols->bill_detail_id : '';
    $remark = @$cols->remarks ? $cols->remarks : '';
    $notes = @$cols->notes ? $cols->notes : '';
    break;
}
@endphp
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        UHID
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <?= @$head_array[0]->uhid ? $head_array[0]->uhid : '-' ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Patient Name
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <?= @$head_array[0]->patient_name ? $head_array[0]->patient_name : '-' ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Patient Type
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <?= @$head_array[0]->current_visit_type ? $head_array[0]->current_visit_type : '-' ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Age/Gender
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <?= (@$head_array[0]->gender ? $head_array[0]->gender : '-'). ' / ' .  (@$head_array[0]->year ? $head_array[0]->year : '-') . ' Y' ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Mobile
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <?= @$head_array[0]->phone ? $head_array[0]->phone : '-' ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Email
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <input type="text" autocomplete="off" value="<?= @$head_array[0]->email ? $head_array[0]->email : '' ?>"
            class="form-control" placeholder="Email" id="patientemail">
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Sample
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <?= @$head_array[0]->samplename ? $head_array[0]->samplename : '-' ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Sample Number
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <?= @$head_array[0]->sample_no ? $head_array[0]->sample_no : '-' ?>
        <input type="hidden" id="textsample_no_hidden"
            value="<?= @$head_array[0]->sample_no ? $head_array[0]->sample_no : 0 ?>">
        <input type="hidden" id="textdoctor_id_hidden"
            value="<?= @$head_array[0]->doctor_id ? $head_array[0]->doctor_id : 0 ?>">
        <input type="hidden" id="resultDetailID_hidden" value="<?= $resultdetailid ?>">
        <input type="hidden" id="bill_detail_id_hidden" value="<?= $bill_detail_id ?>">
        <input type="hidden" id="samplestatus_hidden" value="<?= $samplestatus ?>">
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Sample Collected At
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <?= @$head_array[0]->sample_collection_date ? $head_array[0]->sample_collection_date : '-' ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Sample Collected By
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <?= @$head_array[0]->sample_collected_by ? $head_array[0]->sample_collected_by : '-' ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Reference Doctor
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <input type="text" autocomplete="off" value="" class="form-control" placeholder="" id="ref_doctor">
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Report Number
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-3 no-margin">
        <input type="text" autocomplete="off" value="" class="form-control" placeholder=""
            id="report_number1">
    </div>:
    <div class="col-md-3 no-margin" style="margin-top: -19px !important;">
        <input type="text" autocomplete="off" value="" class="form-control" placeholder=""
            id="report_number2">
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 4px">
    <div class="col-md-5 bold no-margin ">
        Remarks
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <textarea name="lab_remarks" id="lab_remarks_T" class="form-control" style="resize:none">{{ $remark }}</textarea>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12 padding_sm" style="margin-top: 10px">
    <div class="col-md-5 bold no-margin ">
        Notes
    </div>
    <div class="col-md-1 no-margin">
        :
    </div>
    <div class="col-md-6 no-margin">
        <textarea name="lab_notes" id="lab_notes_T" class="form-control" style="resize:none">{{ $notes }}</textarea>
    </div>
</div>
