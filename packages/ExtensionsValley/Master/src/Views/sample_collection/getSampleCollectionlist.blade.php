@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/colors.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/common-custom.css') }}" rel="stylesheet">


    <style>
        .box_header {
            background: #3b926a !important;
            color: #FFF !important;
        }

        button.close {
            margin: -13px -9px !important;
        }

        .bold {
            font-weight: bold;
        }

        .ajaxSearchBox {
            display: none;
            text-align: left;
            list-style: none;
            cursor: pointer;
            max-height: 200px;
            margin: 9px 0px 0px -4px !important;
            overflow-y: auto;
            width: 34%;
            z-index: 599;
            position: absolute;
            background: #ffffff;
            border-radius: 3px;
            border: 1px solid rgba(0, 0, 0, 0.3);

        }

        .select2 {
            overflow: hidden !important;
        }

        h6 {
            font-size: 14px;
            padding: -7px !important;
            margin: 3px;
            padding: 1px;
            "

        }

        #pr_listing {
            text-align: center;
            color: #6f727559;
            font-size: 36px;

        }


        .mate-input-box label {
            position: absolute;
            top: -1px !important;
            left: 6px;
            font-size: 12px;
            font-weight: 700;
        }

        table td {
            position: relative !important;
        }

        .mate-input-box {
            width: 100%;
            position: relative;
            padding: 15px 4px 4px 4px !important;
            border-bottom: 2px solid #01A881;
            box-shadow: 0 0 3px #ccc;
            border-radius: 6px 6px 0 0;
            margin-bottom: 10px;
            height: 40px !important;
        }
    </style>
@endsection
@section('content-area')
    @include('Master::RegistrationRenewal.advancePatientSearch')
    <div class="modal fade" tabindex="-1" role="dialog" id="cancelSetup">
        <div class="modal-dialog" style="max-width: 1100px;width: 100%">
            <div class="modal-content">
                <div class="modal-header table_header_common">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;color:white">&times;</span></button>
                    <h3 class="modal-title">DE-COLLECT SAMPLE</h3>
                </div>
                <div class="modal-body" style="min-height:145px;" id="">

                    <div class="col-md-12" style="margin-top: 10px">
                        <div class="col-md-12">
                            <div class="mate-input-box" style="height:  !important;height: 105px !important;">
                                <label class="filter_label " for="">De-collect Reason:</label>
                                <div class="clearfix"></div>
                                <textarea name="" id="Cancel_reason" class="form-control" style="height: 89px !important;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-6 pull-right">
                            <div class="col-md-3 pull-right"><button type="button" class="btn btn-primary"
                                    onclick="cancelReport()" id="Cancel" style="margin-left: -31px;"><i id="Cancel_spin"
                                        class="fa fa-trash padding_sm"></i>De-collect</button></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-------------View Lab Result----------------->
         <div class="modal fade" id="viewaonlyresult" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1100px;width: 100%">
            <div class="modal-content">
                <div class="modal-header table_header_common" >
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                            <h3 class="modal-title" id="setmodalhead">VIEW RESULT</h3>
                </div>
                <div class="modal-body" style="min-height:400px;" id="viewandconfirem_box">

                </div>
                <div class="modal-footer">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-6 pull-right">
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--------Modal SAMPLE AND CONFIRM RESULT---------------->
    <div class="modal fade" id="viewandconfirmresult" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1100px;width: 100%">
            <div class="modal-content">
                <div class="modal-header table_header_common" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                            <h3 class="modal-title" id="setmodalhead">VIEW AND CONFIRM RESULT</h3>
                </div>
                <div class="modal-body" style="min-height:400px;" id="viewandconfirmresult_box">

                </div>
                <div class="modal-footer">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-6 pull-right">
                            <div class="col-md-3 pull-right"><button type="button" class="btn btn-primary"
                                    onclick="confirmSample()" id="finalised_btn" style="margin-left: -31px;"><i
                                        id="finalised_spin" class="fa fa-check padding_sm"></i>FINALIZE</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="labProfileBillsModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1100px;width: 100%">
            <div class="modal-content">
                <div class="modal-header table_header_common" >
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="labProfileBillsModelHeader">NA</h4>
                </div>
                <div class="modal-body" style="min-height: 500px">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-4 padding_sm">
                            <div class="box no-border no-margin">
                                <div class="box-footer" id="labProfileBillsModelDiv"
                                    style="padding: 5px;box-shadow: 0px 1px #ccc;border: 2px solid #CED9FF;min-height: 479px;background-color: aliceblue;">

                                </div>
                            </div>
                        </div>

                        <div class="col-md-8 padding_sm">
                            <div class="box no-border no-margin">
                                <div class="box-footer"
                                    style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 479px;">
                                    <div class="col-md-12 padding_sm">
                                        <textarea class="form-control" value="" type="text" autocomplete="off" id="labProfileData"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" onclick="confirmSampleCollectionResultEntry(1)" type="button"
                        class="btn btn-primary SampleentryDataBtn SampleDataBtn">Save <i
                            class="fa fa-save SampleentryDataSpin padding_sm"></i></button>
                    <button style="padding: 3px 3px" onclick="confirmSampleCollectionResultEntry(2)" type="button"
                        class="btn btn-primary SamplecertifyDataBtn SampleDataBtn">Provisionally Certified <i
                            class="fa fa-check padding_sm SamplecertifyDataBtnSpin"></i></button>
                    <button style="padding: 3px 3px" onclick="confirmSampleCollectionResultEntry(3)" type="button"
                        class="btn btn-primary SamplefinalizeDataBtn SampleDataBtn">Finalize <i
                            class="fa fa-check padding_sm SamplefinalizeDataBtnSpin"></i></button>
                    <button style="padding: 3px 3px" onclick="confirmSampleCollectionResultEntry(4)" type="button"
                        class="btn btn-primary SampledefinalizeDataBtn SampleDataBtn">De-finalize <i
                            class="fa fa-check padding_sm SampledefinalizeDataBtnSpin"></i></button>
                    <button style="padding: 3px 3px;display:none;" onclick="confirmSampleCollectionResultEntry(5)"
                        type="button"
                        class="btn btn-primary SamplesaveandfinalizeDataBtnF SamplesaveandfinalizeDataBtn SampleDataBtn">Save
                        & finalize <i class="fa fa-save padding_sm SampledesaveandfinalizeDataSpin"></i></button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="labNumericBillsModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1100px;width: 100%">
            <div class="modal-content">
                <div class="modal-header table_header_common" >
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="labNumericBillsModelHeader">NA</h4>
                </div>
                <div class="modal-body" style="min-height: 400px">
                    <div id="labNumericBillsModeDiv"></div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px;display:none" onclick="confirmSampleCollectionResultEntry(1)"
                        type="button" class="btn btn-primary SampleentryDataBtn SampleDataBtn">Save <i
                            class="fa fa-save SampleentryDataSpin padding_sm"></i></button>
                    <button style="padding: 3px 3px;display:none;" onclick="confirmSampleCollectionResultEntry(2)"
                        type="button" class="btn btn-primary SamplecertifyDataBtn SampleDataBtn">Provisionally Certified <i
                            class="fa fa-check padding_sm SamplecertifyDataSpin"></i></button>
                    <button style="padding: 3px 3px;display:none;" onclick="confirmSampleCollectionResultEntry(3)"
                        type="button" class="btn btn-primary SamplefinalizeDataBtn SampleDataBtn">Finalize <i
                            class="fa fa-check padding_sm SamplefinalizeDataSpin"></i></button>
                    <button style="padding: 3px 3px;display:none;" onclick="confirmSampleCollectionResultEntry(4)"
                        type="button" class="btn btn-primary SampledefinalizeDataBtn SampleDataBtn">De-finalize <i
                            class="fa fa-check padding_sm SampledefinalizeDataSpin"></i></button>
                    <button style="padding: 3px 3px;display:none;" onclick="confirmSampleCollectionResultEntry(5)"
                        type="button"
                        class="btn btn-primary SamplesaveandfinalizeDataBtnN SamplesaveandfinalizeDataBtn SampleDataBtn">Save
                        & finalize <i class="fa fa-save SampledesaveandfinalizeDataSpin"></i></button>
                </div>
            </div>
        </div>
    </div>

    <!-------print modal---------------->
    <div class="modal fade" tabindex="-1" role="dialog" id="print_config_modal" data-keyboard="false"
        data-backdrop="static">
        <div class="modal-dialog" style="max-width: 500px;width: 100%">
            <div class="modal-content">
                <div class="modal-header table_header_common" style="">
                    <h4 class="modal-title" style="color: white;float:left">Print Configuration</h4>
                    <button type="button" class="close" onclick="" data-dismiss="modal" aria-label="Close"
                        id="sample_print_close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:90px;">


                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header

                    </div>
                    <div class="col-md-12" style="margin-top:4px;">
                        <button class="btn bg-primary pull-right" onclick="setSampleView(1)" id="print_sample"
                            style="color:white">
                            <i class="fa fa-print" id="print_spin" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="right_col">
        <div class="row" style="margin-bottom: 10px"> </div>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
        <input type="hidden" id="is_lab_admin" value="{{ $is_lab_admin }}">
        <input type="hidden" id="head_id_hidden" value="">
        <input type="hidden" id="sample_id_hidden" value="">
        <input type="hidden" id="sample_number_hidden" value="">
        <input type="hidden" id="lab_id" value="">
        <input type="hidden" id="report_code" value="">
        <input type="hidden" id="doctor_id_hidden" value="">
        <input type="hidden" id="bill_detail_id" value="">
        <input type="hidden" id="sampstatus" value="">
        <input type="hidden" id="limit_stage" value="">
        <input type="hidden" id="auto_acknowledge" value="{{ $auto_acknowledge }}">
        <div class=" col-md-12 row codfox_container">
            <div class="col-md-6 padding_sm content" style="margin-top:-16px;">
                <h4 class="blue "><strong><?= $title ?></strong></h4>
            </div>
        </div>
        <div class="clearfix"></div>
        <div id="filter_area" class="col-md-12"
            style="padding-left:0px !important;width: 101.75%;margin-left: -10px;">
            <div class="box no-border no-margin">
                <div class="box-body" style="padding:5px !important;">
                    <div class="row">
                        <div class="col-md-12 padding_sm" style="">
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label class="">From Date</label>
                                    <input type="text" data-attr="date" autocomplete="off" name="from_date"
                                        value="{{ date('M-d-y') }}" class="form-control datepicker filters reset"
                                        placeholder="YYYY-MM-DD" id="from_date">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                    <label class="">To Date</label>
                                    <input type="text" data-attr="date" autocomplete="off" name="to_date"
                                        value="{{ date('M-d-y') }}" class="form-control datepicker filters reset"
                                        placeholder="YYYY-MM-DD" id="to_date">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label ">UHID</label>
                                    <input class="form-control hidden_search reset" value="" autocomplete="off" type="text"
                                        id="patient_uhid" name="patient_uhid" />
                                    <div id="patient_uhidAjaxDiv" class="ajaxSearchBox"></div>
                                    <input class="filters" value="" type="hidden" name="patient_uhid_hidden"
                                        value="" id="patient_uhid_hidden">
                                    <button type="button" class="btn btn-sm btn-primary advanceSearchBtn"
                                        style=" position: absolute; top: 15px; right: 0;"><i
                                            class="fa fa-search"></i></button>

                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label ">Patient Name</label>
                                    <input class="form-control hidden_search reset" value="" autocomplete="off" type="text"
                                        id="patient_name" name="patient_name" />
                                    <div id="patient_nameAjaxDiv" class="ajaxSearchBox"></div>
                                    <input class="filters" value="" type="hidden" name="patient_name_hidden"
                                        value="" id="patient_name_hidden">

                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label ">Visit Type</label>
                                    <select name="visit" class="form-control reset" id="visit">
                                        <option value="">Select</option>
                                        @foreach ($visit as $data)
                                            <option value="{{ $data->id }}">{{ $data->name }}</option>
                                        @endforeach


                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label class="filter_label ">Lab</label>
                                    <select name="department" class="form-control reset select2" id="department">
                                        <option value="All">Select</option>
                                        @foreach ($dept as $data)
                                            <option value="{{ $data->id }}">{{ $data->dept_name }}</option>
                                        @endforeach


                                    </select>

                                </div>
                            </div>
                            <div class="col-md-2" id="sub_box">
                                <div class="mate-input-box">
                                    <label class="filter_label ">Department</label>
                                    <select name="sub_department" class="form-control reset select2" id="sub_department">
                                        <option value="All">Select</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-7" style="margin-left: -20px;">


                                    <div class="col-md-3 padding_sm" id="">
                                        <div class="mate-input-box">
                                            <label class="filter_label  " id="add_class">Payment Type</label>
                                            <select name="payment_type" class="form-control" id="payment_type">
                                                <option value="All">Select</option>
                                                <option value="cash/Card" class="round_blue">Cash/Card</option>
                                                <option value="ipcredit" class="lime">Credit</option>
                                                <option value="insurance" class="pink">Insurance</option>
                                                <option value="credit" class="round_yellow">Company Credit</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label ">Test Name</label>
                                            <input class="form-control hidden_search reset" value="" autocomplete="off"
                                                type="text" id="test" name="test" />
                                            <div id="testAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters" value="" type="hidden" name="test_hidden"
                                                value="" id="test_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label ">Bill Number</label>
                                            <input class="form-control hidden_search reset" value="" autocomplete="off"
                                                type="text" id="bill_no" name="bill_no" />
                                            <div id="bill_noAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters" value="" type="hidden" name="bill_no_hidden"
                                                value="" id="bill_no_hidden">

                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label ">Report Type</label>
                                            <select name="report_type" class="form-control reset select2" id="report_type">
                                                <option value="All">Select</option>
                                                @foreach ($report as $data)
                                                    <option value="{{ $data->code }}">{{ $data->name }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5" style="margin-left: -3px;">
                                    <div class="col-md-4 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label ">Sample</label>
                                            <select name="sample" class="form-control reset select2" id="sample">
                                                <option value="All">Select</option>
                                                @foreach ($sample as $data)
                                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                @endforeach

                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-4 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label ">Sample Number</label>
                                            <input class="form-control hidden_search reset" value="" autocomplete="off"
                                                type="text" id="sample_no" name="sample_no">
                                            <div id="sample_noAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters" value="" type="hidden" name="sample_no_hidden"
                                                value="" id="sample_no_hidden">

                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm" style="margin-top: 13px">
                                        <div class="radio radio-success inline no-margin ">
                                            <input class="checkit" id="paid" type="radio" name="pay" value=1>
                                            <label class="text-blue" for="paid">
                                                Paid
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm" style="margin-top: 13px">
                                        <div class="radio radio-success inline no-margin ">
                                            <input class="checkit" id="unpaid" type="radio" name="pay" value=2>
                                            <label class="text-blue" for="unpaid">
                                                Unpaid
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-2 padding_sm">
                                <button type="button" class="btn btn-info btn-sm btn-block" id="aknowledge_sample"> <i
                                        id="aknowledge_spin" class="fa fa-eye"></i> AKNOWLEDGE</button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button type="button" id="viewconfirmsample" class="btn btn-info btn-sm btn-block"> <i
                                        id="viewconfirmsample_spin" class="fa fa-eye padding_sm"></i> VIEW & CONFIRM
                                    RESULT</button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button type="button" class="btn btn-info btn-sm btn-block" id="print_result"> <i
                                        id="print_sample_spin" class="fa fa-print padding_sm"></i> PRINT</button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button type="button" class="btn btn-info btn-sm btn-block" id="viewandmailreport"> <i
                                        class="fa fa-eye"></i> VIEW & MAIL REPORT</button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <button type="button" class="btn btn-info btn-sm btn-block">SMS REPORT</button>
                            </div>
                            <div class="col-md-1  padding_sm">
                                <button type="button" title="Refresh" class="btn btn-warning btn-block" onclick="reset()">
                                    <i id="" class="fa fa-refresh"></i> Refresh</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <button type="button" title="Search" class="btn btn-primary btn-block" id="search_sample"
                                    onclick="getSampleList(1)"> <i id="sample_spin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 padding_sm"
            style="min-height: 24px; box-shadow: 0px 0px 1px 0px; width: 100.80%; margin-left: -8px; margin-top: 6px;">
            @foreach ($status as $data)
                <div class="col-md-2 padding_sm {{ strtolower(str_replace(' ', '_', $data->name)) . '_text' }}"
                    style="margin-top: 4px">
                    <div class="checkbox checkbox-warning inline no-margin">
                        <input onclick="getCheckedSamples()" class="sample_status checkit"
                            id="<?= str_replace(' ', '', $data->name) . $data->id ?>" type="checkbox" name="sample"
                            value=<?= $data->id ?>>
                        <label class="text-blue" for="<?= str_replace(' ', '', $data->name) . $data->id ?>">
                            {{ ucwords(strtolower($data->name)) }}
                        </label>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-md-12 " style="padding: 0px;margin-top: 4px;width: 101%;margin-left: -9px;">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <div id="list_container" style="min-height: 338px;">

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/samplecollectionlist.js') }}"></script>
@endsection
