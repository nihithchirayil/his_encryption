<style>
    .number_class {
        text-align: right;
    }

    @media print {
        #footer {
            position: fixed;
            bottom: 0px;
            width: 100%;
        }

        table {
            page-break-after: auto;
        }

        tr {
            page-break-inside: auto;
            page-break-after: auto
        }

        td {
            page-break-inside: auto;
            page-break-after: auto
        }

        thead {
            display: table-header-group
        }

        tfoot {
            display: table-footer-group
        }
    }
</style>
<style type="text/css" media="print">
    table {
        font-size: 13px;
    }

    @page {
        margin-left: 15px;
        margin-right: 15px;
        margin-top: 40px;
        margin-bottom: 35px;
    }
</style>
{{-- @php
    print_r($total_array);
    exit;
@endphp --}}
@if(sizeof($patient_details)>0)
<table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;font-size: 14px;" width="100%"
    border="1" cellspacing="0" cellpadding="0">
    <thead>
        @if ($hospital_header_status == 1)
            <tr>
                <th colspan="5">
                    {!! $hospitalHeader !!}
                </th>
            </tr>
        @endif
        <tr>
            <th colspan="5" style="font-weight: 800;font-size: 18px">
                <center>LABORATORY REPORT</center>
            </th>
        </tr>

        <tr>
            <th colspan="5">
                <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;  " width="100%"
                    border="1" cellspacing="0" cellpadding="0">
                    <thead class="patient_head" style="display: table-header-group;">
                        <tr>
                            <td><strong>UHID</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td style="border-left: none;font-weight: 500;">{{ $patient_details['uhid'] }}</td>
                            <td style="border-left: none;font-weight: 500;"><strong>MOBILE NUMBER</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td style="border-left: none;font-weight: 500;">{{ $patient_details['patient_mobileno'] }}
                            </td>
                        </tr>
                        <tr class="head">
                            <td><strong>PATIENT NAME</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td style="border-left: none;font-weight: 500;">{{ $patient_details['patient_name'] }}
                            </td>
                            <td><strong>EMAIL ID</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td style="border-left: none;font-weight: 500;">{{ $patient_details['email'] }}</td>
                        </tr>
                        <tr>
                            <td style="border-left: none;font-weight: 500;"><strong>GENDER/AGE</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td style="border-left: none;font-weight: 500;">{{ $patient_details['genderdesc'] }} /
                                {{ $patient_details['age'] }}
                            </td>
                            <td><strong>REFERRED BY</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td style="border-left: none;font-weight: 500;">
                                {{ $patient_details['consulting_doctor_name'] }}</td>
                           

                        </tr>

                       
                        <tr>
                            <td class="sample_generated_text" style="border-left: none;font-weight: 500;"><strong>SAMPLE COLLECTED BY</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td class="sample_generated_text" style="border-left: none;font-weight: 500;">{{ $patient_details['sample_collected_user'] }}
                            </td>
                            <td class="sample_generated_text"><strong>SAMPLE COLLECTED DATE</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td class="sample_generated_text" style="border-left: none;font-weight: 500;">
                                {{ @$patient_details['sample_collection_date'] ? date('M-d-Y h:i A', strtotime($patient_details['sample_collection_date'])) :' ' }}</td>
                        </tr>
                        <tr>
                            <td style="border-left: none;font-weight: 500;" class="acknowledged_text"><strong >SAMPLE ACKNOWLEDGE BY</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td style="border-left: none;font-weight: 500;" class="acknowledged_text">{{ $patient_details['acknowledgedusers'] }}
                            </td>
                            <td class="acknowledged_text"><strong>SAMPLE ACKNOWLEDGE DATE</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td class="acknowledged_text" style="border-left: none;font-weight: 500;">
                                {{ @$patient_details['sample_acknowledged_date'] ? date('M-d-Y h:i A', strtotime($patient_details['sample_acknowledged_date'])) :' ' }}</td>
                        </tr>
                        <tr>
                            <td class="result_entered_text" style="border-left: none;font-weight: 500;"><strong>RESULT ENTERD BY</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td class="result_entered_text" style="border-left: none;font-weight: 500;">{{ $patient_details['result_entry_users'] }}
                            </td>
                            <td class="result_entered_text"><strong>RESULT ENTERD DATE</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td class="result_entered_text" style="border-left: none;font-weight: 500;">
                                {{ @$patient_details['result_enterd_date'] ? date('M-d-Y h:i A', strtotime($patient_details['result_enterd_date'])) :' ' }}</td>
                        </tr>
                        <tr>
                            <td class="provisionally_certified_text" style="border-left: none;font-weight: 500;"><strong>PROVISIONALLY CERTIFIED BY</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td class="provisionally_certified_text" style="border-left: none;font-weight: 500;">{{ $patient_details['result_provisionally_certified_by'] }}
                            </td>
                            <td class="provisionally_certified_text"><strong>PROVISIONALLY CERTIFIED DATE</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td class="provisionally_certified_text" style="border-left: none;font-weight: 500;">
                                {{ @$patient_details['result_provisionally_certified_date'] ? date('M-d-Y h:i A', strtotime($patient_details['result_provisionally_certified_date'])) :' ' }}</td>
                        </tr>
                        <tr>
                            <td style="border-left: none;font-weight: 500;" class="finalized_text"><strong>RESULT FINALIZED BY</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td class="finalized_text" style="border-left: none;font-weight: 500;">{{ $patient_details['result_finalized_user'] }}
                            </td>
                            <td class="finalized_text"><strong>RESULT FINALIZED DATE</strong></td>
                            <td align="center" style="border-left: none;font-weight: 500;">:</td>
                            <td class="finalized_text" style="border-left: none;font-weight: 500;">
                                {{ @$patient_details['result_finalized_date'] ? date('M-d-Y h:i A', strtotime($patient_details['result_finalized_date'])) :' ' }}</td>
                        </tr>
                       
                       

                    </thead>
                </table>
            </th>
        </tr>


        <tr>
            @if ($patient_details['report_type'] != 'T')
                <th colspan="5">
                    <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;font-size: 12px;"
                        width="100%" border="1" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr style="background-color:darkgreen;color:white">
                                <th width="40%"><strong>
                                        <center>TEST NAME</center>
                                    </strong></th>
                                <th width="20%"><strong>
                                        <center>RESULT</center>
                                    </strong></th>
                                <th width="2%"><strong>
                                        <center></center>
                                    </strong></th>
                                <th width="18%"><strong>
                                        <center>UNIT</center>
                                    </strong></th>
                                <th width="20%"><strong>
                                        <center>REF RANGE</center>
                                    </strong></th>

                            </tr>
                        </thead>
                    </table>
                </th>
            @else
                <th colspan="5"></th>
            @endif

        </tr>
    </thead>
    <tbody>
        @if (count($total_array) > 0)
            @php
                $sub_dept_name = '';
                $service = '';

            @endphp
            @foreach ($total_array as $key1 => $value1)
                <tr>
                    <th class="" colspan="5">
                        <center style="font-size: 17px;">{{ $key1 }}</center>
                    </th>
                </tr>
                @foreach ($value1 as $key => $value)
                    @php
                        $list_string = '';

                        if ($value->actual_result) {
                            if (floatval($value->min_value) > floatval($value->actual_result)) {
                                $list_string = 'L';
                            } elseif (floatval($value->max_value) < floatval($value->actual_result)) {
                                $list_string = 'H';
                            }
                        }
                    @endphp

                    @if ($service != $value->service_desc)
                        @php $service = $value->service_desc; @endphp

                        @if ($value->servicereporttype != 'N' && $value->servicereporttype != 'F')
                            <tr>
                                <th colspan="5" style="text-align:left;font-size: 13px;">{{ $value->service_desc }}
                                </th>
                            </tr>
                        @endif
                    @endif

                    @if ($value->servicereporttype != 'T' && $value->actual_result != 'All' && (intval($value->actual_result) > 0 || $value->actual_result))
                        <tr>
                            <td class="" style="padding-left: 11px;" width="40%">
                                {{ $value->detail_description }}</td>
                            <td class="number_class" width="20%">
                                {{ $value->actual_result }} </td>
                            <td class="number_class" width="2%" style="color: red  ">{{ $list_string }}</td>
                            <td class="number_class" width="18%">{{ $value->unitdesc }} </td>
                            <td class="number_class" width="20%">
                                {{ @$value->refrange != '0-0' ? $value->refrange : ' ' }} </td>
                        </tr>
                    @elseif($value->servicereporttype == 'T')
                        <tr>
                            <td colspan="5"> {!! $patient_details['actual_result'] != 'All' ? $patient_details['actual_result'] : '' !!}</td>
                        </tr>
                    @endif
                @endforeach
            @endforeach
            <tr>
                <th colspan="5">

                </th>
            </tr>

        @endif
        <tr>
            <th colspan="5">
                <div style="text-align: center;font-size: 12px;font-weight: 100;">------------------------End of the
                    report----------------------</div>
            </th>
        </tr>
        <tr style="font-size: 12px;">
            <th colspan="5">
                <div id="footer">
                    <div style="float: left">
                        <div style="float: left;font-weight: 100;">{{ $user_details['user_name'] }}</div> <br>
                        <div style="float: left;font-weight: 100;">{{ $user_details['user_qualification'] }}</div>
                        <br>
                        <div style="float: left;font-weight: 100;">{{ $user_details['user_designation'] }}</div> <br>
                    </div>
                    <div>
                        <div style="float: right;font-weight: 100;">{{ $UserDepHeadName }}</div> <br>
                        {{-- <div style="float: right;font-weight: 100;">{{ $UserDepHeadSig }}</div> <br> --}}
                        <div style="float: right;font-weight: 100;">{{ $UserDepHead }}</div> <br>
                    </div>
                </div>

            </th>

        </tr>
    </tbody>
</table>
@else
<div>No Record Found</div>
@endif

