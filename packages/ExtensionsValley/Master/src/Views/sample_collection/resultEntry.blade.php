@php

$remark = '';
$notes = '';
foreach ($details_array as $cols) {
    $remark = @$cols->remarks ? $cols->remarks : '';
    $notes = @$cols->notes ? $cols->notes : '';
    break;
}

@endphp
<div class="box no-border no-margin">
    <div class="box-body" style="padding:5px !important;background-color: aliceblue !important;">
        <div class="row">
            <div class="col-md-12 padding_sm">
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        UHID
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <p id="result_entrypatient_uhid"><?= @$head_array[0]->uhid ? $head_array[0]->uhid : '-' ?></p>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        visit Type
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->current_visit_type ? $head_array[0]->current_visit_type : '-' ?>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Sample
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->samplename ? $head_array[0]->samplename : '-' ?>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Collected By
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->sample_collected_by ? $head_array[0]->sample_collected_by : '-' ?>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Patient Name
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->patient_name ? $head_array[0]->patient_name : '-' ?>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Email
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <input type="text" autocomplete="off"
                            value="<?= @$head_array[0]->email ? $head_array[0]->email : '' ?>" class="form-control"
                            placeholder="Email" id="patientemail">
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Sample Number
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->sample_no ? $head_array[0]->sample_no : '-' ?>
                        <input type="hidden" id="nummbericsample_no_hidden"
                            value="<?= @$head_array[0]->sample_no ? $head_array[0]->sample_no : 0 ?>">
                        <input type="hidden" id="nummbericdoctor_id_hidden"
                            value="<?= @$head_array[0]->doctor_id ? $head_array[0]->doctor_id : 0 ?>">
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Age/Gender
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= (@$head_array[0]->gender ? $head_array[0]->gender : '-'). ' / ' .  (@$head_array[0]->year ? $head_array[0]->year : '-') . ' Y' ?>
                    </div>
                </div>
                <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-5 bold no-margin ">
                        Mobile
                    </div>
                    <div class="col-md-1 no-margin">
                        :
                    </div>
                    <div class="col-md-6 no-margin">
                        <?= @$head_array[0]->phone ? $head_array[0]->phone : '-' ?>
                    </div>
                </div>
                <div class="col-md-4 padding_sm" style="margin-top: 10px;">
                    <div class="col-md-4 bold no-margin">
                        Collected At
                    </div>
                    <div class="col-md-1 no-margin" style="margin-left:-9px !important">
                        :
                    </div>

                    <div class="col-md-7 no-margin" style="margin-left:-10px !important">
                        <?= @$head_array[0]->sample_collection_date ? $head_array[0]->sample_collection_date : '-' ?>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="box no-border no-margin">
    <div class="col-md-12 " style="padding: 6px;margin-top: 2px;width: 101%;margin-left: -5px;">
        <div class="row">
            <div class="col-md-12 padding_sm">
                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label class="">Phlebotomist Comments</label>
                        <input type="text" autocomplete="off" class="form-control sample_detail" disabled
                            id="phlebotomist_comments">
                    </div>
                </div>
                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label class="">Remarks</label>
                        <input type="text" autocomplete="off" class="form-control sample_detail" id="lab_remarks"
                            value="{{ @$remark ? $remark : ' ' }}">
                    </div>
                </div>
                <div class="col-md-4 padding_sm">
                    <div class="mate-input-box">
                        <label class="">Notes</label>
                        <input type="text" autocomplete="off" class="form-control sample_detail" id="lab_notes"
                            value="{{ @$notes ? $notes : ' ' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12" style="padding: 0px;margin-top: -8px;width: 101%;margin-left: -6px;">
    <div class="box no-border">
        <div class="box-body clearfix">
            <div class="theadscroll" style="position: relative; height: 295px;margin-top: 10px">
                <table class='table table-condensed theadfix_wrapper table_sm table-col-bordered'
                    style="font-size: 12px;width=100%">
                    <thead>
                        <tr class="common_table_header" style="background-color:#01987a !important">
                            <th width='5%'>SL.No.</th>
                            <th class="common_td_rules" width='20%'>Sub Test</th>
                            <th class="common_td_rules" width='10%'>Description</th>
                            <th class="common_td_rules" width='10%'>Result</th>
                            <th class="common_td_rules" width='5%'><i class="fa fa-list"></i></th>
                            <th class="common_td_rules" width='10%'>Methodology</th>
                            <th class="common_td_rules" width='10%'>Unit</th>
                            <th class="common_td_rules" width='10%'>Normal Value</th>
                            <th class="common_td_rules" width='20%'>Comments</th>
                        </tr>
                    </thead>
                    <tbody id="getSampleResultDataList">
                        <?php
                        if(count($details_array)!=0){
                            $service_id='';
                            $i=1;
                            foreach ($details_array as $each) {
                                $list_string='';
                                if($each->actual_result){
                                    if(floatval($each->min_value) > floatval($each->actual_result)){
                                        $list_string="fa fa-arrow-down red";
                                    }else if(floatval($each->max_value) < floatval($each->actual_result)){
                                        $list_string="fa fa-arrow-up red";
                                    }
                                }
                                if($service_id !=$each->service_id){
                                    $service_id=$each->service_id;
                                    $i=1;
                                    ?>
                        <tr class="" style="background-color: #01987a !important;color:white !important">
                            <td colspan="9">
                                <?= $each->service_desc ?>
                            </td>
                        </tr>
                        <?php
                                }
                                ?>

                        <tr>
                            <td class="common_td_rules">{{ $i }}
                                <input type="hidden" name="resultDetailID"
                                    value="<?= $each->resultdetailid ? $each->resultdetailid : 0 ?>">
                                <input type="hidden" name="bill_detail_id"
                                    value="<?= $each->bill_detail_id ? $each->bill_detail_id : 0 ?>">
                                <input type="hidden" id="samplestatus_hidden" value="<?= $samplestatus ?>">
                            </td>
                            <td class="common_td_rules" title="{{ $each->service_sub_description }}">
                                {{ @$each->service_sub_description ? $each->service_sub_description  : '' }}
                            </td>
                            <td class="common_td_rules" title="{{  $each->detail_description }}">{{ $each->detail_description }}</td>
                            <td class="common_td_rules">
                                <input type="text" autocomplete="off"
                                    value="<?= @$each->actual_result ? $each->actual_result : '' ?>"
                                    class="form-control sample_detail" name="actual_result" id="actual_result">
                            </td>
                            <td class="common_td_rules"><i class='<?= $list_string ?>'></i></td>
                            <td class="common_td_rules">
                                <select name="methodology" class="form-control ">
                                    <option value="All">Select</option>
                                    @foreach ($methodology as $data)
                                        @php $selected=''; @endphp
                                        @if ($each->methodology == $data->id)
                                            @php $selected='selected'; @endphp
                                        @endif
                                        <option <?= $selected ?> value="{{ $data->id }}">{{ $data->methodology }}
                                        </option>
                                    @endforeach
                                </select>

                            </td>
                            <td class="common_td_rules">{{ $each->unitdesc }}</td>
                            <td class="common_td_rules">{{ $each->normalrange }}</td>
                            <td class="common_td_rules">
                                <input type="text" autocomplete="off"
                                    value="<?= @$each->detail_comments ? $each->detail_comments : '' ?>"
                                    class="form-control " name="detail_comments" id="detail_comments">

                            </td>
                            <td class="common_td_rules"> <input type="text" autocomplete="off"
                                    value="<?= @$each->detailslno ? $each->detailslno : '' ?>"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                    class="form-control " name="detail_order" id="detail_order"
                                    style="display: none"></td>
                        </tr>
                        <?php
                     $i++;
                            }
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
