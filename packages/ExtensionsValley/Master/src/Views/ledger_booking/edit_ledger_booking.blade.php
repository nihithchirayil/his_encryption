@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
.reference_no {
  width: 100% !important;
  box-sizing: border-box;
  display: inline-block;
  white-space: pre-wrap;
  overflow-wrap: break-word;
  min-height:100%;
  border: 1px solid #ccc;
  height:26px;
}
.ajaxSearchBox{
    max-height: 500px !important;
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <?php $c_payment_mod = \DB::table('payment_mode')->Where('status', 1)->orderBy('name', 'asc')->pluck('name', 'id'); ?>
        <div class="col-md-12 padding_sm">
            {!!Form::open(array('url' => $saveUrl, 'method' => 'get', 'id'=>'form1'))!!}
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        
                        <?php 
                        $voucher_type = (isset($voucher_details[0]->voucher_type))?$voucher_details[0]->voucher_type:'';
                        $voucher_no = (isset($voucher_details[0]->voucher_no))?$voucher_details[0]->voucher_no:'';
                        $entry_date = (isset($voucher_details[0]->bill_entry_date))?$voucher_details[0]->bill_entry_date:'';
                        $purchase_invoice_date = (isset($detail_list_is_bank[0]->invoice_date))?$detail_list_is_bank[0]->invoice_date:'';
                        $purchase_invoice_no = (isset($detail_list_is_bank[0]->invoice_no))?$detail_list_is_bank[0]->invoice_no:''
                        ?>
                        <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Voucher Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('v_type', array(""=> " Select") + $v_type->toArray(),$voucher_type,
                                        ['class'=>"form-control v_type", 'id'=>"v_type_1",'onchange' => "selectExpenseIncome(this.value)"]) !!}
                            </div></div>  
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Date</label>
                                <div class="clearfix"></div>
                                <input type="text" value="<?= date('d-m-YY',strtotime($entry_date)); ?>" id="entry_date" name="entry_date" autocomplete="off"    class="form-control datepicker">
                            </div></div>
                        <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                             <div class="mate-input-box">
                                <label for="">Invoice No</label>
                            <input type="text" value="<?= $purchase_invoice_no ?>" id="purchase_invoice_no" class="form-control" value="" name="purchase_invoice_no">
                             </div>
                        </div>
                         <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Invoice Date</label>
                                <div class="clearfix"></div>
                                <input type="text" value="<?php if($purchase_invoice_date !=''){ echo date('d-m-YY',strtotime($purchase_invoice_date)); }else{ echo "";} ?>" id="purchase_invoice_date" name="purchase_invoice_date" autocomplete="off"    class="form-control datepicker">
                            </div></div>
                         <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block light_purple_bg" data-toggle="modal" data-target="#adv_search_modal" ><i class="fa fa-search"></i>Adv.Search</span>
                        </div>
                        <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                            <input type="hidden" id="type" value="{{$type[0]??''}}" name="type">
                        </div>
                        <div class="col-md-3 padding_sm  pull-right" style=" padding-top: 10px !important">
                            <label for=""style="float:right">Voucher No : <span style="color: red"><b><?= $voucher_no?></b></span></label>
                                <div class="clearfix"></div>
                                
                            </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="height: 450px;">
                <div class="box-body clearfix">
<!--                    <div class="theadscroll" style="position: relative; height: 200px;">-->
                        <h6 id="title_show"><b>@if($type[0]==1){{'Debit Account'}} @php $first_cr_dr = 'dr' @endphp @else {{'Credit Account'}}  @php $first_cr_dr = 'cr' @endphp @endif</b></h6>
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="pay_rec_id">
                            <thead>
                                <tr class="table_header_bg">
                                    <th width="30%">Ledger</th>
                                    <th width="5%">Opn.Balance</th>
                                    <th width="1%">&nbsp;</th>
                                    <th width="20%">Amount</th>
                                    <th width="37%" id="th_nar">Narration</th>
                                    <th style="width:11%" id="th_ref">Ref.Details</th>
                                    <th width="4%" style="display: none">Type</th>
                                     <th width="1%"><i class="fa fa-plus" style="cursor:pointer"onclick="fillLedgerValues()"></i></th>
                                </tr>
                            </thead>
                            <tbody id="payment_receipt_tbody">
                                @php $j=1 @endphp
                                @if(isset($detail_list_is_bank) && sizeof($detail_list_is_bank) > 0)

                                @foreach($detail_list_is_bank as $is_bank)
                                <tr id="edit_ledger_booking_{{$is_bank->id}}">
                                    <td>
                                        <input type="text" value="{{$is_bank->ledger_name}}"  name="ledger[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event, 1)" id="ledger_item_desc-{{$j}}">
                                        <input type="hidden" value="{{$is_bank->ledger_id}}" name="ledger_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-{{$j}}">
                                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-{{$j}}" 
                                             style="text-align: left; list-style: none;  cursor: pointer;
                                             margin: -2px 0px 0px 0px;overflow-y: auto; width: 28%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                    </td>
                                        <td><span class="opening_balance_val" style="color:red">
                                         <?php if(isset($is_bank->is_asset) && $is_bank->is_asset ==1){ 
                                             echo $is_bank->opening_balence;
                                         }?>
                                        </span></td>
                                    <td style="display: none">
                                         <input type="hidden" value="{{$is_bank->group_id}}" id="group_id_{{$j}}" name="group_id[]" autocomplete="off"  readonly=""  class="form-control">
                                    </td>
                                    <td><i class="fa fa-plus" style="cursor:pointer" onclick="saveNewLedger(this)"></i></td>
                                    <td>
                                        <input type="text" value="{{$is_bank->amount}}" name="amnt[]" class="form-control amount" id="amount_{{$j}}" onkeyup="number_validation(this); calculate_table_total('amount', 'total_amount_by_bill');"> 
                                    </td>
                                    <td>
                                        <input type="text" value="{{$is_bank->naration}}" name="naration_head[]" class="form-control" id="naration_head_{{$j}}" > 
                                    </td>
                                    <td>
                                        <div class="container" >
                                            <textarea autocomplete="off"  placeholder="Eg:bill1,bill2" onclick="autoheight(this.id);widhAdjust(2);" onkeyup="autoheight(this.id);widhAdjust(2);" onblur="widhAdjust(1);" class="reference_no" wrap="off" cols="35" rows="1"  name="refe_no[]" id="refe_no_{{$j}}"  >{{$is_bank->ref_detail ?? ''}}</textarea>
                                         </div>
                                      </td>
                                    <td style="display: none">
                                        <input type="text" value="{{$is_bank->cr_dr}}" name="dr_cr[]" class="form-control inc_exp" id="dr_cr_{{$j}}"  readonly="" > 
                                        <input type="hidden" value="{{$is_bank->id}}" id="detail_id_{{$j}}" name=detail_id[]"  autocomplete="off"  class="form-control">
                                    </td>
                                    <td>
                                        <i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer" onclick="delete_booking_entry('{{$is_bank->id}}')"></i>
                                    </td>
                                </tr>
                                @php $j++ @endphp
                                @endforeach
                                @endif

                                <tr>
                                    <td>
                                        <input type="text" value=""  name="ledger[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event, 1)" id="ledger_item_desc-{{$j}}">
                                        <input type="hidden" value="" name="ledger_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-{{$j}}">
                                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-{{$j}}" 
                                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                                             margin: -2px 0px 0px 0px;overflow-y: auto; width: 28%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                    </td>
                                     <td><span class="opening_balance_val" style="color:red"></span></td>
                                    <td style="display: none">
                                         <input type="hidden" value="" id="group_id_{{$j}}" name="group_id[]" autocomplete="off"  readonly=""  class="form-control">
                                    </td>
                                    <td><i class="fa fa-plus" style="cursor:pointer" onclick="saveNewLedger(this)"></i></td>
                                    <td>
                                        <input type="text" value="" name="amnt[]" class="form-control amount" id="amount_{{$j}}" onkeyup="number_validation(this); calculate_table_total('amount', 'total_amount_by_bill');"> 
                                    </td>
                                    <td>
                                        <input type="text" value="" name="naration_head[]" class="form-control" id="naration_head_{{$j}}" > 
                                    </td>
                                    <td>
                                        <div class="container" >
                                            <textarea autocomplete="off" placeholder="Eg:bill1,bill2" onclick="autoheight(this.id);widhAdjust(2);"  onkeyup="autoheight(this.id);widhAdjust(2);" onblur="widhAdjust(1)" class="reference_no" wrap="off" cols="35" rows="1"  name="refe_no[]" id="refe_no_{{$j}}"  ></textarea>
                                           </div>
                                       </td>
                                    <td style="display: none">
                                        <input type="text" value="{{$first_cr_dr}}" name="dr_cr[]" class="form-control inc_exp" id="dr_cr_{{$j}}"  readonly="" >
                                    </td>
                                    <td>
                                        <i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                        <div class="col-xs-2 padding_sm pull-right" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <label class="custom_floatlabel">Total Amount</label>
                                <input type="text" id="total_amount_by_bill" class="form-control" readonly="" value="" >                          
                            </div>
                        </div>
<!--                    </div>-->
                    <div class="clearfix"></div> <div class="ht10"></div>
                    <div>
                        <h6 id="title_show_btm"><b>@if($type[0]==1){{'Credit Account'}} @php $second_cr_dr = 'cr' @endphp @else {{'Debit Account'}} @php $second_cr_dr = 'dr' @endphp  @endif</b></h6>
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="pay_rec_id_2" >
                            <thead>
                                <tr class="table_header_bg">
                                    <th style="width:30%" id="th_ledg">Ledger</th>
                                    <th width="5%">Opn.Balance</th>
                                    <th style="width:1%">&nbsp;</th>
                                    <th style="width:20%">Amount</th>
                                    <th style="width:37%" id="th_nar"></th>
                                    <th width="3%"  style="display: none">Type</th>
                                    <th width="1%"><i class="fa fa-plus " style="cursor:pointer"onclick="fill_newrow()"></i></th>
                                </tr>
                            </thead>
                            <tbody id="payment_receipt_tbody_two">
                                @php $k=1;@endphp
                                @if(isset($detail_list_not_is_bank) && sizeof($detail_list_not_is_bank) > 0)

                                @foreach($detail_list_not_is_bank as $is_not)
                                <?php
                                        $ref_tem_no = '';
                                        $ref_tem_dt = '';
                                        $data_set = '';
                                ?>
                                <tr id="edit_ledger_booking_{{$is_not->id}}">
                                    <td>
                                        <input type="text" value="{{$is_not->ledger_name}}"  name="ledger_cd[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event, 0)" id="ledger_item_desc_cd-{{$k}}">
                                        <input type="hidden" value="{{$is_not->ledger_id}}" name="ledger_id_cd[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden_cd-{{$k}}">
                                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box_cd-{{$k}}" 
                                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                                             margin: -2px 0px 0px 0px;overflow-y: auto; width: 27%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                    </td>
                                    <td><span class="opening_balance_val" style="color:red">
                                         <?php if(isset($is_bank->is_asset) && $is_bank->is_asset ==1){ 
                                             echo $is_not->opening_balence;
                                         }?>
                                        </span></td>
                                    <td><i class="fa fa-plus" style="cursor:pointer" onclick="saveNewLedger(this)"></i></td>
                                    <td>
                                        <input type="text" value="{{$is_not->amount}}" name="amnt_cd[]" class="form-control amnt_cd" id="amount_cd_{{$k}}" onkeyup="number_validation(this); calculate_table_total('amnt_cd', 'total_amount_cd');"> 
                                       <input type="hidden" value="{{$is_not->ref_detail}}" id="refe_details_{{$k}}" name="refe_details[]" autocomplete="off"  class="form-control">
                                        <input type="hidden" value="{{$is_not->group_id}}" id="group_id_cd_{{$k}}" name="group_id_cd[]" autocomplete="off"  readonly=""  class="form-control">
                                        <input type="hidden" value="{{$is_not->voucher_no}}" id="voucher_no_edit_{{$k}}" name="voucher_no_edit[]" autocomplete="off"  readonly=""  class="form-control">
                                    </td>
                                    <td title="{{$is_not->naration}}">
                                        <input type="hidden" value="{{$is_not->naration}}" name="naration[]" id="naration_{{$k}}" autocomplete="off"   class="form-control"  >
                                    </td>
                                    
                                    <td style="display: none">
                                        <input type="text" value="{{$is_not->cr_dr}}" name="dr_cr_cd[]" class="form-control inc_exp_cd" id="dr_cr_cd{{$k}}"  readonly="" > 
                                        <input type="hidden" value="{{$is_not->id}}" id="detail_id_cd_{{$k}}" name=detail_id_cd[]"  autocomplete="off"  class="form-control">
                                    </td>
                                    <td>
                                        <i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer" onclick="delete_booking_entry('{{$is_not->id}}')"></i>
                                    </td>
                                </tr>
                                @php $k++; @endphp
                                @endforeach
                                @endif
                                <tr>
                                    <td>
                                        <input type="text" value=""  name="ledger_cd[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event, 0)" id="ledger_item_desc_cd-{{$k}}">
                                        <input type="hidden" value="" name="ledger_id_cd[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden_cd-{{$k}}">
                                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box_cd-{{$k}}" 
                                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                                             margin: -2px 0px 0px 0px;overflow-y: auto; width: 27%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                    </td>
                                     <td><span class="opening_balance_val" style="color:red"></span></td>
                                    <td><i class="fa fa-plus" style="cursor:pointer" onclick="saveNewLedger(this)"></i></td>
                                    <td>
                                        <input type="text" value="" name="amnt_cd[]" class="form-control amnt_cd" id="amount_cd_{{$k}}" onkeyup="number_validation(this); calculate_table_total('amnt_cd', 'total_amount_cd');"> 
                                        <input type="hidden" value="" id="refe_details_{{$k}}" name="refe_details[]" autocomplete="off"  class="form-control">
                                        <input type="hidden" value="" id="group_id_cd_{{$k}}" name="group_id_cd[]" autocomplete="off"  readonly=""  class="form-control">
                                    </td>
                                    <td>
                                        <input type="hidden" value="" name="naration[]" id="naration_{{$k}}" autocomplete="off"   class="form-control"  >
                                    </td>
                                    
                                    <td style="display: none">
                                        <input type="text" value="{{$second_cr_dr}}" name="dr_cr_cd[]" class="form-control inc_exp_cd" id="dr_cr_cd{{$k}}"  readonly="" > 
                                    </td>
                                    <td>
                                        <i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                        <div class="col-xs-2 padding_sm pull-right" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <label class="custom_floatlabel">Total Amount</label>
                                <input type="text" id="total_amount_cd" class="form-control" readonly="" value="" >                          
                            </div>
                        </div>
                        <div class="col-xs-2 padding_sm pull-right" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <label class="custom_floatlabel" >Total Difference </label>
                                <label class="custom_floatlabel"  style="color: red"><h4 id="ttl_dif_label"></h4></label>
                                <input type="hidden" id="ttl_diff" class="form-control" readonly="" value="" >                          
                            </div>
                        </div>
                        <div class="col-xs-12 padding_sm pull-right" style="padding-bottom: 10px;">
                             <div class="clearfix"></div> <div class="ht10"></div>
                       <i class="fa fa-save" id="add_share_holder_spin"></i> Update</span>
                       <span type="button" class="btn btn-info pull-right" onclick="saveEditForm(0)">
                        <i class="fa fa-thumbs-up" id="add_share_holder_spin_t"></i> Update & add new</span>
                       <span type="button" class="btn btn-warning pull-right" onclick="backToList()">
                       <i class="fa fa-times" id="add_share_holder_spin" ></i>Cancel</span>
                        <span type="button" class="btn btn-default pull-right" onclick="refreshAllData()" >
                         <i class="fa fa-refresh" id="add_share_holder_spin" ></i>Refresh</span>
                        </div>

                    </div>
                    
                </div>
                <div class="clearfix"></div>
                <input type="hidden" value="{{$hd_id}}" name="head_id">
            </div>
        </div>
        {!! Form::token() !!} {!! Form::close() !!}
    </div>
</div>
</div>

<div id="search_ref_modal1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reference Details</h4>
            </div>
            <div class="modal-body" id="append_multi">
                <input type="hidden" id="search_ref_modal_id" value="">
                <table class="table table_round_border styled-table" id="refere_table_list">
                    <thead>
                        <tr class="table_header_bg">
                            <th width="5%">Reference No</th>
                            <th width="5%">Reference Date</th>
                            <th width="3%"><i class="fa fa-plus " onclick="insert_multi_row()" style="cursor:pointer"></i></th>
                        </tr>
                    </thead>
                    <tbody id="refere_table_list_body">
                    </tbody>
                </table>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal" onclick="saveMultiReference()">
                    <i class="fa fa-thumbs-up " style="cursor:pointer"></i>Ok</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"> 
                    <i class="fa fa-times " style="cursor:pointer"></i>Close</button>
                
            </div>
        </div>

    </div>
</div>
<div id="search_ref_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ledger Master</h4>
            </div>
            <div class="modal-body" id="append_ledger_master">
                
            </div>
            
        </div>

    </div>
</div>
<div id="adv_search_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit List</h4>
            </div>
            <div class="modal-body" id="fill_advanced_edit_tbody">
                <div class="row">
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Party</label>
                            <div class="clearfix"></div>
                            <input type="hidden" value="" name="ledger_id"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-11">
                            <input type="text" value=""  name="ledger_name1"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc_adv(this.id, event, 1)" id="ledger_item_desc-11">
                        </div>
                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-11" 
                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                             margin: -2px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Voucher Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('v_type_adv', array(""=> " Select") + $v_type->toArray(),'',
                                        ['class'=>"form-control v_type", 'id'=>"v_type_adv"]) !!}
                    </div></div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Voucher No</label>
                            <div class="clearfix"></div>
                            <input type="text" name="voucherno_adv_edit" id="voucherno_adv_edit" value="" class=" form-control">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Amount</label>
                            <div class="clearfix"></div>
                            <input type="text" name="amount_adv_edit" id="amount_adv_edit" value="" class=" form-control">
                        </div>
                    </div>
                     <div class="clearfix"></div> <div class="ht10"></div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Ref.No</label>
                            <div class="clearfix"></div>
                            <input type="text" name="ref_adv_edit" id="ref_adv_edit" value="" class=" form-control">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" value="" id="from_entry_date_adv" name="from_entry_date_adv" autocomplete="off"    class="form-control datepicker">
                    </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" value="" id="to_entry_date_adv" name="to_entry_date_adv" autocomplete="off"    class="form-control datepicker">
                    </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Narration</label>
                            <div class="clearfix"></div>
                            <input type="text" name="narration_adv_edit" id="narration_adv_edit" value="" class=" form-control">
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <span class="btn btn-block light_purple_bg" onclick="AdvanceEditSearch();" ><i class="fa fa-search"></i>Search</span>
                    </div>
                    <div id="edit_apend_modal"></div>
                </div>
            </div>
            
        </div>

    </div>
</div>
<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
<input type="hidden" id="from_type" value="<?= $from_type ?>">
<input type="hidden" id="bank_recep" value="<?php echo htmlspecialchars($bank_details_json_rec, ENT_COMPAT); ?>">
<input type="hidden" id="bank_hidden" value="<?php echo htmlspecialchars($bank_details_json, ENT_COMPAT); ?>">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/ledger_booking.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script type="text/javascript">
$(document).ready(function () {
    window.onload = function() {
    var typ_vl = localStorage.getItem("type_val");
var exp_pay_type =localStorage.getItem("exp_pay_type");
if(typ_vl != null){
$("#v_type_1").val(typ_vl);
$("#type").val(exp_pay_type);
$('#v_type_1').val(typ_vl).change();
}
localStorage.removeItem("type_val");
localStorage.removeItem("exp_pay_type");
}
calculate_table_total('amount','total_amount_by_bill');
calculate_table_total('amnt_cd','total_amount_cd');
   if (window.location.href.indexOf("add_ledger_booking_entry") > -1) 
    {
         insertDefaultBankDetails(1);
    }
    $("#amount_1").focus();
    setTimeout(function(){
    $('#ledger_item_desc-1').focus();
            },300);

      setTimeout(function(){
        var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split('?'); 
  if(sURLVariables[1] != 'undefined'){
      var roww_id = (/id=(\d+)/.exec(sPageURL)[1]);
      $("#edit_ledger_booking_"+roww_id).css("background-color","#edb2b2");
      $('table tr#edit_ledger_booking_'+roww_id).find('input:first').focus();
  }
            },300);
$(document).bind('keydown', function(e) { 
  if(e.ctrlKey && (e.which == 83)) { // SAVE AND CONTINUE +s
    e.preventDefault();
    saveForm(0);
    return false;
  }else if(e.ctrlKey && (e.which == 80) && !e.altKey){ //purchase +p
      e.preventDefault();
      $('#v_type_1').val(1).change();
      return false;
  }else if(e.ctrlKey && e.altKey && (e.which == 80)){//payment +alt+p
      e.preventDefault();
      $('#v_type_1').val(12).change();
      return false;
  }else if(e.ctrlKey &&(e.which == 76)){//sales +l
      e.preventDefault();
      $('#v_type_1').val(2).change();
      return false;
  }else if(e.ctrlKey && (e.which == 74)){//journel +j
      e.preventDefault();
      $('#v_type_1').val(3).change();
      return false;
  }else if(e.ctrlKey && (e.which == 82) && !e.shiftKey){ //Receipt +r
      e.preventDefault();
      $('#v_type_1').val(5).change();
      return false;
  }else if(e.ctrlKey && (e.which == 67)){//Credit Note Import  +c
      e.preventDefault();
      $('#v_type_1').val(6).change();
      return false;
  }else if(e.ctrlKey && (e.which == 73)){// Sales import +i 
      e.preventDefault();
      $('#v_type_1').val(7).change();
      return false;
  }else if(e.ctrlKey && (e.which == 77)&& !e.shiftKey){// Receipt Import +m
      e.preventDefault();
      $('#v_type_1').val(8).change();
      return false;
  }else if(e.ctrlKey && (e.which == 79)){//Contra +o
      e.preventDefault();
      $('#v_type_1').val(9).change();
      return false;
  }else if(e.ctrlKey && (e.which == 69)){//credit note +e
      e.preventDefault();
      $('#v_type_1').val(10).change();
      return false;
  }else if(e.ctrlKey && (e.which == 68)){//debit note +d
      e.preventDefault();
      $('#v_type_1').val(11).change();
      return false;
  }else if(e.ctrlKey && (e.which == 65)){//advance search popup +a
      e.preventDefault();
     editAdvanceSearch();
      setTimeout(function(){
        $("#ledger_item_desc-11").focus();
    },300);
      return false;
  }else if(e.altKey && (e.which == 65)){//advance search fn alt+a
      e.preventDefault();
      AdvanceEditSearch();
      return false;
  }else if(e.altKey && (e.which == 65)){//save exit alt+s
      e.preventDefault();
      saveForm(1);
      return false;
  }else if(e.ctrlKey && (e.which == 8)){//cancel alt+backspace
      e.preventDefault();
      backToList();
      return false;
  }
});
});
function autoheight(element) {
var el = document.getElementById(element);
    el.style.height = "5px";
    if(el.scrollHeight < 300)
    el.style.height = (el.scrollHeight)+"px";
}
</script>
@endsection
