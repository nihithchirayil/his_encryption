@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        {!!Form::open(array('url' => $searchUrl, 'method' => 'get', 'id'=>'form'))!!}

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Type</label>
                            <div class="clearfix"></div>
                            <?php $payment_receipt =  array("" => "Select",'1' => 'Income', '2' => 'Expense'); ?>
                                {!! Form::select('payment_receipt',$payment_receipt, $searchFields['payment_receipt'] ?? '',['class' => 'form-control','title' => 'Payment/Receipt','id' => 'payment_receipt','style' => 'color:#555555; padding:2px 12px;']) !!}
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Voucher Type</label>
                            <div class="clearfix"></div>
                                {!! Form::select('vouchertype',array(""=> " Select") + $v_type->toArray(), $searchFields['vouchertype'] ?? '',['class' => 'form-control','title' => 'Voucher Type','id' => 'vouchertype','style' => 'color:#555555; padding:2px 12px;']) !!}
                            </div>
                        </div>
                         <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Voucher No</label>
                            <div class="clearfix"></div>
                            <input type="text" name="voucherno" value="{{$searchFields['voucherno'] ?? ''}}" class=" form-control">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Ledger</label>
                            <div class="clearfix"></div>
                             <input type="hidden" value="{{$searchFields['ledger_id'] ?? ''}}" name="ledger_id"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-1">
                           <input type="text" value="{{$searchFields['ledger_name'] ?? ''}}"  name="ledger_name"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event, 1)" id="ledger_item_desc-1">
                            </div>
                            <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-1"
                                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                             margin: -2px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                        </div>



                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button class="btn btn-block light_purple_bg" onclick="reqSearch();" ><i class="fa fa-search"></i> Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block btn-warning" onclick="clear_search();" ><i class="fa fa-times"></i> Clear</span>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block btn-primary" onclick="addItem();" ><i class="fa fa-plus"></i> Add</span>
                        </div>

                        {!! Form::token() !!} {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="height: 450px;">
                <div class="box-body clearfix" style="border-radius:6px;background-color: #fff !important">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th width="2%" class="common_td_rules">#</th>
<!--                                    <th width="2%" class="common_td_rules">Type</th>-->
                                    <th width="2%" class="common_td_rules">Voucher No</th>
                                    <th width="2%" class="common_td_rules">Voucher Type</th>
                                    <th width="2%" class="common_td_rules">Created Date</th>
                                    <th width="1%" class="common_td_rules"><i class="fa fa-trash-o"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($ledger_list) && sizeof($ledger_list) > 0)
                                @php $i=1; @endphp
                                @foreach($ledger_list as $list)
                                @if($list->processed == 1)
                                @php $processed = "background-color:#fddada"; @endphp
                                @else
                                @php $processed = ""; @endphp
                                @endif
                                <tr style="cursor:pointer;{{$processed}}">
                                    <td onclick="selectLedger({{$list->head_id}})" class="common_td_rules">{{ $i }}</td>
<!--                                    <td class="common_td_rules">{{ $list->type }}</td>-->
                                    <td onclick="selectLedger({{$list->head_id}})" class="common_td_rules">{{ $list->voucher_no }}</td>
                                    <td onclick="selectLedger({{$list->head_id}})" class="common_td_rules">{{ $list->voucher_type }}</td>
                                    <td onclick="selectLedger({{$list->head_id}})" class="common_td_rules">{{ $list->created_at }}</td>
                                    <td width="1%" class="common_td_rules"  onclick="deleteBookingEntryFromList({{$list->head_id}})"><i class="fa fa-trash-o"></i></td>
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6 pull-left">
                       <span style='width:20px;height:20px;display:block;float:left;background-color:#f7e5e5;margin: 35px 0;'></span>
                            <label style="margin: 2px 0 0 10px;float: left;;margin: 35px 0;">&nbsp;&nbsp;Processed bills</label>
                    </div>
                    <div class="col-md-6 text-right pull-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

@stop
@section('javascript_extra')
 <script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
 <script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
 <script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script type="text/javascript">

    function reqSearch() {
    var makeSearchUrl = '';
    $('#form').submit();
    }
    function selectLedger(id){
    window.location = "{!! route('extensionsvalley.master.edit_ledger_booking_entry') !!}/" + id;
    }
    function addItem(){
        window.location = "{!! route('extensionsvalley.master.add_ledger_booking_entry') !!}";
    }


   @if(Session::has('mes'))
    var type = "{{ Session::get('type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('mes') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('mes') }}");
            break;

        case 'success':
            toastr.success("Added Successfully");
            break;

        case 'error':
            toastr.error("{{ Session::get('mes') }}");
            break;
    }
  @endif
    function select_item_desc(id, event, is_bank) {
    var keycheck = /[a-zA-Z0-9 ]/;
    id = id.split('-');
    id = id[1];
    var value = event.key;
    if (is_bank == 1) {
        var ajax_div = 'search_ledger_item_box-' + id;
        var ledger_desc = $('#ledger_item_desc-' + id).val();
    } else {
        var ajax_div = 'search_ledger_item_box_cd-' + id;
        var ledger_desc = $('#ledger_item_desc_cd-' + id).val();
    }

    if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {


        if (ledger_desc == "") {
            $("#" + ajax_div).html("");
        } else {
            $.ajax({
                type: "GET",
                url: "",
                data: 'ledger_desc=' + ledger_desc + '&search_ledger_desc=1&is_bank=' + is_bank,
                beforeSend: function () {
                    $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
                success: function (html) {
                    $("#" + ajax_div).html(html).show();
                    $("#" + ajax_div).find('li').first().addClass('liHover');
                },
                complete: function () {
                    $('.theadscroll').perfectScrollbar("update");
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

    } else {
        ajaxProgressiveKeyUpDown(ajax_div, event);
    }
}
function fillLedgerValues(e, ledger_name, id, ledger_group_id) {
$("#ledger_item_desc-1").val(ledger_name.replace(/&amp;/g, '&'));
$("#ledger_item_desc_hidden-1").val(id);
}
function deleteBookingEntryFromList(id){
        if (confirm("Are you sure you want to Delete ?")) {
        var params = {delete_list: 1, head_id: id};
        $.ajax({
            type: "GET",
            url: '',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
            },
            success: function (data) {
                if (data == 1) {
                    $.LoadingOverlay("hide");
                    $('#edit_ledger_booking_' + id).remove();
                    toastr.success('Deleted Succcessfuly !!');
                    window.location.reload();
                }
            },
            complete: function () {

            }
        });
    }
    }
</script>
@endsection
