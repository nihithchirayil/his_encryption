@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
.reference_no {
  width: 100% !important;
  box-sizing: border-box;
  display: inline-block;
  white-space: pre-wrap;
  overflow-wrap: break-word;
  min-height:100%;
  border: 1px solid #ccc;
  height:26px;
}
.ajaxSearchBox{
    max-height: 500px !important;
}
.opening_balance_cls{
    display: none;
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <?php $c_payment_mod = \DB::table('payment_mode')->Where('status', 1)->orderBy('name', 'asc')->pluck('name', 'id'); ?>
        <div class="col-md-12 padding_sm">
            {!!Form::open(array('url' => $saveUrl, 'method' => 'get', 'id'=>'form'))!!}
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Voucher Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('v_type', array(""=> " Select") + $v_type->toArray(),'',
                                        ['class'=>"form-control v_type", 'id'=>"v_type_1",'onchange' => "selectExpenseIncome(this.value)"]) !!}
                            </div></div>
                        <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Date</label>
                                <div class="clearfix"></div>
                                <input type="text" value="<?= date('d-m-YY') ?>" id="entry_date" name="entry_date" autocomplete="off"    class="form-control datepicker">
                            </div></div>                         
                        
                        <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                             <div class="mate-input-box">
                                <label for="">Invoice No</label>
                            <input type="hidden" id="type" value="" name="type">
                            <input type="text" id="purchase_invoice_no" class="form-control" value="" name="purchase_invoice_no">
                             </div>
                        </div>
                         <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Invoice Date</label>
                                <div class="clearfix"></div>
                                <input type="text" value="" id="purchase_invoice_date" name="purchase_invoice_date" autocomplete="off"    class="form-control datepicker">
                            </div></div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block light_purple_bg" data-toggle="modal" data-target="#adv_search_modal" ><i class="fa fa-search"></i>Adv.Search</span>
                        </div>
                        <div class="col-md-1">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                           <span class="btn btn-warning" data-toggle="modal" data-target="#help_modal" >
                               <i class="fa fa-info"></i>&nbsp;&nbsp;Help</span>
                        </div>
                        <div class="col-md-2 padding_sm  pull-right" style=" padding-top: 10px !important">
                            <label for=""style="float:right">Voucher No : <span style="color: red" id="generated_vouchr_label"><b></b></span></label>
                            <input type="hidden" value="" name="generated_vouchr_text" id="generated_vouchr_text">
                            <div class="clearfix"></div>
                        </div>
                       
                         
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="height: 450px;">
                <div class="box-body clearfix">
<!--                    <div class="theadscroll" style="position: relative; height: 250px;">-->
                        <h6 id="title_show"><b>Debit Account</b></h6>
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="pay_rec_id">
                            <thead>
                                <tr class="table_header_bg">
                                    <th width="30%">Ledger</th>
                                    <th width="5%">Opn.Balance</th>
                                    <th width="1%">&nbsp;</th>
                                    <th width="20%">Amount</th>
                                    <th width="37%" id="th_nar">Narration</th>
                                    <th style="width:11%" id="th_ref">Ref.Details</th>
                                    <th width="4%" style="display: none">Type</th>
                                    <th width="1%"><i class="fa fa-plus" style="cursor:pointer"onclick="fillLedgerValues()"></i></th>
                                </tr>
                            </thead>
                            <tbody id="payment_receipt_tbody">
                                <tr>
                                    <td>
                                        <input type="text" value=""  name="ledger[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event, 1)" id="ledger_item_desc-1">
                                        <input type="hidden" value="" name="ledger_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-1">
                                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-1" 
                                             style="text-align: left; list-style: none;  cursor: pointer; 
                                             margin: -2px 0px 0px 0px;overflow-y: auto; width: 28%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                    </td>
                                    <td >                                        
                                        <span class="opening_balance_val" style=" color:red "></span>
                                    </td>
                                    <td>                                        
                                        <i class="fa fa-plus" style="cursor:pointer;padding-right: 3px;" onclick="saveNewLedger(this)"></i>
                                    </td>
                                    <td style="display: none">>
                                        <input type="hidden" value="" id="group_id_1" name="group_id[]" autocomplete="off"  readonly=""  class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" value="" name="amnt[]" class="form-control amount" id="amount_1" onkeyup="number_validation(this);calculate_table_total('amount', 'total_amount_by_bill');"> 
                                    </td>
                                    <td>
                                        <input type="text" value="" name="naration_head[]" class="form-control" id="naration_head_1" > 
                                    </td>
                                    <td>
                                           <div class="container" >
                                               <textarea autocomplete="off" placeholder="Eg:bill1,bill2"  onclick="autoheight(this.id);widhAdjust(2);" onkeyup="autoheight(this.id);widhAdjust(2);" onblur="widhAdjust(1)" class="reference_no" wrap="off" cols="35" rows="1"  name="refe_no[]" id="refe_no_1"  ></textarea>
                                           </div>
                                        <input type="hidden" value="" id="refe_details_1" name="refe_details[]" autocomplete="off"  class="form-control">

                                    </td>
                                    <td style="display: none">
                                        <input type="text" value="dr" name="dr_cr[]" class="form-control inc_exp" id="dr_cr_1"  readonly="" > 
                                    </td>
                                    <td><i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i></td>
                                    
                                </tr>

                            </tbody>
                        </table>
                        <div class="col-xs-2 padding_sm pull-right" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <label class="custom_floatlabel">Total Amount</label>
                                <input type="text" id="total_amount_by_bill" class="form-control" readonly="" value="" >                          
                            </div>
                        </div>
<!--                    </div>-->
                    <div class="clearfix"></div> <div class="ht10"></div>
                    <div>
                        <h6 id="title_show_btm"><b>Credit Account</b></h6>
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="pay_rec_id_add">
                            <thead>
                                <tr class="table_header_bg">
                                    <th style="width:30%" id="th_ledg">Ledger</th>
                                    <th width="5%">Opn.Balance</th>
                                    <th style="width:1%">&nbsp;</th>
                                    <th style="width:20%">Amount</th>
                                    <th style="width:37%" ></th>
                                    <th style="width:1%"><i class="fa fa-plus" style="cursor:pointer"onclick="fill_newrow()"></i></th>
                                </tr>
                            </thead>
                            <tbody id="payment_receipt_tbody_two">
                                <tr>
                                    <td>
                                        <input type="text" value=""  name="ledger_cd[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event, 0)" id="ledger_item_desc_cd-1" onblur="pending_ammount_calc(this)">
                                        <input type="hidden" value="" name="ledger_id_cd[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden_cd-1">
                                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box_cd-1" 
                                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                                             margin: -2px 0px 0px 0px;overflow-y: auto; width: 27%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                    </td>
                                    <td >                                        
                                        <span class="opening_balance_val" style="color:red"></span>
                                    </td>
                                    <td><i class="fa fa-plus" style="cursor:pointer" onclick="saveNewLedger(this)"></i></td>
                                    <td>
                                        <input type="text" value="" name="amnt_cd[]" class="form-control amnt_cd" id="amount_cd_1" onkeyup="number_validation(this);calculate_table_total('amnt_cd', 'total_amount_cd');"> 
                                        </td>
                                    <td>
                                        <input type="hidden" value="" name="naration[]" id="naration_1" autocomplete="off"   class="form-control"  >
                                        <input type="hidden" value="cr" name="dr_cr_cd[]" class="form-control inc_exp_cd" id="dr_cr__cd1"  readonly="" > 
                                        <input type="hidden" value="" id="group_id_cd_1" name="group_id_cd[]" autocomplete="off"  readonly=""  class="form-control">
                                    </td>
                                    <td>
                                        <i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                        <div class="col-xs-2 padding_sm pull-right" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <label class="custom_floatlabel">Total Amount</label>
                                <input type="text" id="total_amount_cd" class="form-control" readonly="" value="" >                          
                            </div>
                        </div>
                        <div class="col-xs-2 padding_sm pull-right" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <label class="custom_floatlabel" >Total Difference </label>
                                <label class="custom_floatlabel"  style="color: red"><h4 id="ttl_dif_label"></h4></label>
                                <input type="hidden" id="ttl_diff" class="form-control" readonly="" value="" >                          
                            </div>
                        </div>
                        <div class="col-xs-12 padding_sm pull-right" style="padding-bottom: 10px;">
                            <span type="button" class="btn btn-success pull-right" onclick="saveForm(1)">
                        <i class="fa fa-save" id="add_share_holder_spin"></i> Save</span>
                         <span type="button" class="btn btn-info pull-right" onclick="saveForm(0)">
                        <i class="fa fa-thumbs-up" id="add_share_holder_spin_t"></i> Save & continue</span>
                    <span type="button" class="btn btn-warning pull-right" onclick="backToList()" >
                         <i class="fa fa-times" id="add_share_holder_spin" ></i>Cancel</span>
                    <span type="button" class="btn btn-default pull-right" onclick="refreshAllData()" >
                         <i class="fa fa-refresh" id="add_share_holder_spin" ></i>Refresh</span>
                        </div>
                    </div>
                    
                </div>
                <div class="clearfix"></div>

            </div>
        </div>
        {!! Form::token() !!} {!! Form::close() !!}
    </div>
</div>
</div>

<div id="search_ref_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ledger Master</h4>
            </div>
            <div class="modal-body" id="append_ledger_master">
                
            </div>
            
        </div>

    </div>
</div>

<div id="help_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Keys</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive theadscroll"  style="height: 450px;">
                            <table class="table table_round_border styled-table">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th style="text-align:center;padding: 1px;">Function</th>
                                        <th style="text-align:center;padding: 1px;">Keys</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>SAVE AND CONTINUE</td>
                                        <td>Cntrl+s</td>
                                    </tr>
                                    <tr>
                                        <td>Purchase</td>
                                        <td>Cntrl+p</td>
                                    </tr>
                                    <tr>
                                        <td>Payment</td>
                                        <td>Cntrl+alt+p</td>
                                    </tr>
                                    <tr>
                                        <td>Sales</td>
                                        <td>Cntrl+l</td>
                                    </tr>
                                    <tr>
                                        <td>Journel</td>
                                        <td>Cntrl+j</td>
                                    </tr>
                                    <tr>
                                        <td>Receipt</td>
                                        <td>Cntrl+r</td>
                                    </tr>
                                    <tr>
                                        <td>Credit Note Import</td>
                                        <td>Cntrl+c</td>
                                    </tr>
                                    <tr>
                                        <td>Sales import</td>
                                        <td>Cntrl+i</td>
                                    </tr>
                                    <tr>
                                        <td>Receipt Import</td>
                                        <td>Cntrl+m</td>
                                    </tr>
                                    <tr>
                                        <td>Contra</td>
                                        <td>Cntrl+o</td>
                                    </tr>
                                    <tr>
                                        <td>Credit Note</td>
                                        <td>Cntrl+e</td>
                                    </tr>
                                    <tr>
                                        <td>Debit Note</td>
                                        <td>Cntrl+d</td>
                                    </tr>
                                    <tr>
                                        <td>Advance Search</td>
                                        <td>Cntrl+a</td>
                                    </tr>
                                    <tr>
                                        <td>Advance Search Function</td>
                                        <td>alt+a</td>
                                    </tr>
                                    <tr>
                                        <td>Cancel</td>
                                        <td>alt+backspace</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
    <div id="adv_search_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit List</h4>
            </div>
            <div class="modal-body" id="fill_advanced_edit_tbody">
                <div class="row">
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Party</label>
                            <div class="clearfix"></div>
                            <input type="hidden" value="" name="ledger_id"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-11">
                            <input type="text" value="" autofocus="" name="ledger_name1"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc_adv(this.id, event, 1)" id="ledger_item_desc-11">
                        </div>
                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-11" 
                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                             margin: -2px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Voucher Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('v_type_adv', array(""=> " Select") + $v_type->toArray(),'',
                                        ['class'=>"form-control v_type", 'id'=>"v_type_adv"]) !!}
                    </div></div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Voucher No</label>
                            <div class="clearfix"></div>
                            <input type="text" name="voucherno_adv_edit" id="voucherno_adv_edit" value="" class=" form-control">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Amount</label>
                            <div class="clearfix"></div>
                            <input type="text" name="amount_adv_edit" id="amount_adv_edit" value="" class=" form-control">
                        </div>
                    </div>
                     <div class="clearfix"></div> <div class="ht10"></div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Ref.No</label>
                            <div class="clearfix"></div>
                            <input type="text" name="ref_adv_edit" id="ref_adv_edit" value="" class=" form-control">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" value="" id="from_entry_date_adv" name="from_entry_date_adv" autocomplete="off"    class="form-control datepicker">
                    </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" value="" id="to_entry_date_adv" name="to_entry_date_adv" autocomplete="off"    class="form-control datepicker">
                    </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Narration</label>
                            <div class="clearfix"></div>
                            <input type="text" name="narration_adv_edit" id="narration_adv_edit" value="" class=" form-control">
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <span class="btn btn-block light_purple_bg" onclick="AdvanceEditSearch();" ><i class="fa fa-search"></i>Search</span>
                    </div>
                    <div id="edit_apend_modal"></div>
                </div>
            </div>
            
        </div>

    </div>
</div>
<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
<input type="hidden" id="bank_recep" value="<?php echo htmlspecialchars($bank_details_json_rec, ENT_COMPAT); ?>">
<input type="hidden" id="bank_hidden_payment" value="<?php echo htmlspecialchars($bank_details_json, ENT_COMPAT); ?>">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/ledger_booking.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>

<script type="text/javascript">
function autoheight(element) {
var el = document.getElementById(element);
    el.style.height = "5px";
    if(el.scrollHeight < 300)
    el.style.height = (el.scrollHeight)+"px";
}
</script>
@endsection
