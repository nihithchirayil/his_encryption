<div class="row">
    <div class="col-md-12">
        <div class="table-responsive theadscroll"  style="height: 450px;">
            <table class="table table_round_border styled-table">
                <thead>
                    <tr class="table_header_bg">
                        <th style="text-align:center;padding: 1px;">Party</th>
                        <th style="text-align:center;padding: 1px;">Amount</th>
                        <th style="text-align:center;padding: 1px;">Voucher No</th>
                        <th style="text-align:center;padding: 1px;">Narration</th>
                        <th style="text-align:center;padding: 1px;">Date</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($edit_list_qry))
                    @foreach($edit_list_qry as $st)
                    <tr onclick="selectLedger({{$st->head_id}},{{$st->booking_id}})" style="cursor:pointer;">
                        <td class="common_td_rules">{{ $st->ledger_name }}</td>
                        <td class="td_common_numeric_rules">{{ !empty($st->amount) ?$st->amount :'' }}</td>
                        <td class="common_td_rules">{{ !empty($st->voucher_no) ? $st->voucher_no:'' }}</td>
                        <td class="common_td_rules">{{ !empty($st->naration) ?$st->naration :'' }}</td>
                        <td class="common_td_rules">{{ !empty($st->bill_date) ?$st->bill_date :'' }}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5">No Records Found</td>
                    </tr>
                    @endif                        
                </tbody>
            </table>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="h10"></div>

    <div class="row" style="">
        <div style="padding:10px 30px;">
            <div class="col-md-12" style="text-align:right;">
                <nav id="purchase_paging"> <?php echo $paginator->render(); ?>
                </nav>
            </div>
        </div>
    </div>                                            
</div>
<script>
    $("#purchase_paging .pagination a").click(function (e) {
    e.preventDefault();
    var url = $("#ins_base_url").val();
    var advance_txt_edit = $("#advance_txt_edit").val();
    $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
    var param = {advance_text: advance_txt_edit, page: $(this).attr('href').split('page=')[1]};
    $.ajax({
    url: url + "/master/advanced_ledger_edit_list",
            data: param,
            dataType: 'html',
    }).done(function (data) {
    $('#fill_advanced_edit_tbody').html(data);
    $.LoadingOverlay("hide");
    }).fail(function () {
    $.LoadingOverlay("hide");
    alert('Posts could not be loaded.');
    });
    });
    function selectLedger(id, booking_id){
    window.location = "{!! route('extensionsvalley.master.edit_ledger_booking_entry') !!}/" + id + "?booking_id=" + booking_id;
    }
   
</script>