@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')
<div class="right_col" role="main">
    <div class="row padding_sm">
    <div class="col-md-2 padding_sm pull-right">
        <?=$title?>
    </div>
    <input type="hidden" id="base_url" value="{{URL::to('/')}}">
    </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">
                    <div class="col-md-3 padding_sm">
                        <label> Table Name</label>
                        <select onchange="tableConfiguration()" class="form-control select2" id="table_name">
                        <option value=""> Select Table</option>
                            <?php
                            foreach($table_list as $each){
                                echo "<option value='$each->table_name'>$each->table_name</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-2 padding_sm" style="margin-top: 25px;">
                        <button type="button" onclick="tableConfiguration()" id="tableConfigurationbtn" class="btn btn-primary">Config <i id="tableConfigurationspin" class="fa fa-cogs"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel" id="tableConfigurationlistDiv">
                   <span class="text-center"><label style="text-align: center;">Please Select Any Table</label></span>
                </div>
            </div>
        </div>
</div>

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/master/default/javascript/AuditConfiguration.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
@endsection
