<div class="x_panel">
<div class="col-md-12 padding_sm">
    <div class="col-md-10 padding_sm">
            <label>Table Name</label> : <?=strtoupper($table_name)?>
    </div>

    <div class="col-md-1 padding_sm">
        <div class="col-md-7 padding_sm"><label>Delete</label></div>
        <div class="col-md-1 padding_sm">
        <?php
        $checked_status = "";
        if ($audit_delete_staus == '1') {
            $checked_status = "checked=''";
        }
        ?>
         <input <?=$checked_status?> class="styled" id="delete_status" type="checkbox" value="0">
        </div>
    </div>
    <div class="col-md-1 padding_sm pull-right">
    <button type="button" onclick="saveTableConfiguration()" id="savetableConfigurationbtn" class="btn btn-success">Save <i id="savetableConfigurationspin" class="fa fa-save"></i></button>
    </div>
</div>
</div>
<div class="theadscroll" style="position: relative; height: 350px;">
<?php
foreach ($table_coumns as $val) {
    $checked_status = "";
    if(in_array($val->column_name,$audit_array)){
        $checked_status = "checked=''";   
    }
    ?>

<div class="col-md-3">
    <div class="row padding_sm">
        <div class="col-md-8 padding_sm"><label>{{ucfirst($val->column_name)}}</label></div>
        <div class="col-md-2 padding_sm">
        <input <?=$checked_status?> class="styled columnname_check" type="checkbox" value="<?=$val->column_name?>">
        </div>
    </div>
</div>
    <?php
}
?>
</div>

