@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">

<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>

    #main{
    min-height: 700px;
    /* background-color: aliceblue; */
    margin: 3px 0px 0px -2px;
    box-shadow: 0px 1px 3px;
    border-radius: 2px;
    padding: 0px !important;

    }
    #heading{
    background-color:white;
    height: 33px;
    color: #555;
;


    }
    #fill_details{
    height: 141px;
    }
    .th {
    text-align: center;
    border: 1px solid whitesmoke;
    color: white;
    }
    table{
    width: 100%;
    background-color: #f1f7f7 !im;
    }
    .btn {
    font-size: 11px;
    padding: 1px 6px;
    box-shadow: none;
    background-color: #2e4863;
    }
    #table {
    box-shadow: 0px 0px 1px;
    padding: 2px 0px 0px 0px;
    margin: 1px 1px 2px 3px;
    min-height: 470px;
    background-color: aliceblue;
    width: 99.5%;
    cursor: pointer;
    }
    .td{
    border: 1px solid white;
    padding-left: 2px;
    font-size: 12px;
    }
     #foo tr:nth-of-type(even) {
    background-color: #f3f3f3;
    }
    .table-striped>tbody>tr:nth-of-type(odd) {
    background-color: #FFF;
}
.errspan {
    float: right;
    margin-right: 6px;
    margin-top: -23px;
    position: relative;
    z-index: 2;
    color: #0e0e0e;
    font-size: 18px;
    }
/* width */
.dropdown-menu::-webkit-scrollbar {
  width: 10px;
  border-radius: 0px 0px 6px 6px !important;

}

/* Track */
.dropdown-menu::-webkit-scrollbar-track {
  background:white;
}

/* Handle */
.dropdown-menu::-webkit-scrollbar-thumb {
  background: #888;
}

/* Handle on hover */
.dropdown-menu::-webkit-scrollbar-thumb:hover {
  background: #555;
}
.dropdown-menu li:nth-of-type(even) {
    background-color:#99FFFF;
    }

.btn-default{
    color:white !important;
    background-color: #2e4863 !important;
}


</style>


@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="patient_id" value=""/>
<input type="hidden" id="visit_id" value=""/>
<input type="hidden" id="encounter_id" value=""/>
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <div  id="main" class="col-md-12 padding_sm" >
            <form action="{{route('extensionsvalley.master.show_full_patient_list')}}" id="requestSearchForm" method="POST">
                {!! Form::token() !!}
                <div id="heading" class="col-md-12 " style="font-size: 22px;"> Patient Details

                        @if (strlen($total_records)>0)
                        <div id="total_record" class="pull-right" style="margin-top:2px;margin-right: 30px;font-size: 14px;">Total Record:{{$total_records}}</div>
                        @else
                        <div id="total_record" class="pull-right" style="margin-top:2px;margin-right: 30px;font-size: 14px;"></div>
                        @endif

                </div>
                <div id="fill_details" class="col-md-12 ">
                            <div style="border-bottom:1px solid #F4F6F5;padding: 11px 0px;">

                            <div class="row">
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">Patient Name</label>
                                                <input class="form-control hidden_search" value="@if(old('patient_name')){{old('patient_name')}}@else{{$searchFields['patient_name']}}@endif" autocomplete="off" type="text"  id="patient_name" name="patient_name" placeholder="Patient Name"/>
                                                 <div id="patient_list"class="patient_list_id"></div>

                                        </div>
                                    </div>
                                    <div class="col-md-1 padding_sm">
                                        <div class="mate-input-box">
                                        <label for="">OP No.</label>
                                        <input type="text" name="patient_id" id="patient_id" value="@if(old('patient_id')){{old('patient_id')}}@else{{$searchFields['patient_id']}}@endif" class="form-control" placeholder="OP No">

                                    </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">Area</label>
                                            <input type="text" name="area" id="area_col" value="@if(old('area')){{old('area')}}@else{{$searchFields['area']}}@endif" class="form-control" placeholder="Area">
                                        </div>
                                    </div>
                                <!-- From Date starts -->
                                <div class="col-md-2 padding_sm ">
                                    <div class="form-group">
                                    <div class="mate-input-box" >
                                        <label class="filter_label">Visit Date From</label>
                                        <input type='text' data-attr="date" id="from_date" name="from_date" autocomplete="off"  value="@if(old('from_date')){{old('from_date')}}@else{{$searchFields['from_date']}}@endif" class="form-control datepicker" placeholder="dd-mm-yyyy" /><i class="fa fa-calendar errspan"></i>

                                    </div>
                                </div>
                                </div>
                                <!-- From Date ends -->
                                <!-- To Date starts -->
                                <div class="col-md-2 padding_sm">
                                    <div class="form-group">
                                    <div class="mate-input-box" id='datetimepicker'>
                                        <label class="custom_floatlabel">Visit Date To</label>
                                        <input type='text'   data-attr="date" id="to_date"  autocomplete="off" name="to_date" value="@if(old('to_date')){{old('to_date')}}@else{{$searchFields['to_date']}}@endif" class="form-control date_floatinput" placeholder="dd-mm-yyyy" /><i class="fa fa-calendar errspan"></i>
                                    </div>
                                </div>
                                </div>
                                <!-- To Date ends -->
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label class="custom_floatlabel">Visit Status</label>

                                <select class="form-control custom_floatinput" id="visit_status" onchange="selectStatus(this.value)" name="visit_status"><option  value="">Visit Status</option><option value="ip"{{ old('visit_status',$searchFields['visit_status']) == 'ip' ? 'selected' : '' }}>IP</option><option value="op"{{ old('visit_status',$searchFields['visit_status']) == 'op' ? 'selected' : '' }}>OP</option></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 padding_sm">
                                <div class="mate-input-box">
                                <label class="custom_floatlabel">Gender</label>
                                    <select class="form-control custom_floatinput" id="gender" name="gender"><option  value="">Gender</option><option value="1"{{ old('gender',$searchFields['gender']) == '1' ? 'selected' : '' }}>Male</option><option value="2"{{ old('gender',$searchFields['gender']) == '2' ? 'selected' : '' }}>Female</option><option value="3"{{ old('gender',$searchFields['gender']) == '3' ? 'selected' : '' }}>Transgender</option></select>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                @php
                                $sql="select ward_name,id from ward_master where status = 1 order by ward_name";
                                $ward = DB::select($sql);
                                @endphp

                                        <div class="mate-input-box">
                                        <label for="">Ward</label>
                                        <select class="form-control custom_floatinput" id="ward" name="ward"><option  value="">Select Ward</option> @foreach($ward as $items_each)<option value="{{$items_each->id}}"{{ old('ward',$searchFields['ward']) == $items_each->id ? 'selected' : '' }}>{{ $items_each->ward_name}}</option>@endforeach
                                        </select>
                                        </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                        <div class="mate-input-box">
                                        <label for="">Room Type</label>
                                        <select class="form-control custom_floatinput" id='room_type' value="" name="room_type"><option value="">Select</option>

                                        </select>
                                    </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                        <label for="">Mobile</label>
                                        <input type="text" name="mobile" id="mobile" value="@if(old('mobile')){{old('mobile')}}@else{{$searchFields['mobile']}}@endif" class="form-control" placeholder="Mobile No.">
                                    </div>
                            </div>
                            <!-- From Date starts -->
                                <div class="col-md-2 padding_sm">
                                    <div class="form-group">
                                    <div class="mate-input-box" id='datetimepicker'>
                                        <label class="custom_floatlabel">Reg. From Date</label>
                                        <input type='text' data-attr="date" id="reg_from_date"  autocomplete="off" name="reg_from_date" value="@if(old('reg_from_date')){{old('reg_from_date')}}@else{{$searchFields['reg_from_date']}}@endif" class="form-control date_floatinput" placeholder="dd-mm-yyyy" /><i class="fa fa-calendar errspan"></i>

                                    </div>
                                </div>
                                </div>
                                <!-- From Date ends -->
                                <!-- To Date starts -->
                                <div class="col-md-2 padding_sm">
                                    <div class="form-group">
                                    <div class="mate-input-box" id='datetimepicker'>
                                        <label class="custom_floatlabel">Reg. To Date</label>
                                        <input type='text' data-attr="date" id="reg_to_date"  autocomplete="off" name="reg_to_date" value="@if(old('reg_to_date')){{old('reg_to_date')}}@else{{$searchFields['reg_to_date']}}@endif" class="form-control date_floatinput" placeholder="dd-mm-yyyy" /><i class="fa fa-calendar errspan"></i>

                                    </div>
                                </div>
                                </div>
                                <!-- To Date ends -->
                                <div class="col-md-2 padding_sm">
                                    <div class="clearfix"></div>
                                    <button  type="button" class="btn btn-block light_purple_bg" onclick="search_clear();">
                                        Clear</button>
                                    <div class="clearfix"></div>
                                    <button  type="button" class="btn btn-block light_purple_bg" onclick="employeeSearch();"><i class="fa fa-search" id="search"></i>
                                        Search</button>
                                </div>
                        </div>

                    </div>

                </div>


                <div id="table" class="col-md-12">
                    <table class="table table-striped table">
                        <thead style="background-color: #26B99A;">
                            <th class="th">OP No</th>
                            <th class="th">Patient Name</th>
                            <th class="th">Gender</th>
                            <th class="th">Dt.Of Birth</th>
                            <th class="th">Mobile</th>
                            <th class="th">Age</th>
                            <th class="th">Reg.Date</th>
                            <th class="th">Visit St</th>
                            <th class="th">Ward</th>
                            <th class="th">Bed</th>
                            <th class="th">IP Status</th>
                            <th class="th">Visit</th>
                            <th class="th">Nurse</th>
                            <th class="th">Vitals</th>
                            <th class="th">Document</th>
                        </thead>

                        <tbody id="foo" style="display:contents;" class="">

                                        <tr>
                                            @if(isset($res) && sizeof($res) > 0)
                                            @foreach ($res as $item)
                                            <td class="td">{{$item->uhid}}</td>
                                            <td class="td">{{$item->patient_name}}</td>
                                            <td class="td">{{$item->gender}}</td>
                                            <td class="td">{{date('d-M-Y ',strtotime($item->dob))}}
                                            <td class="td">{{$item->phone}}</td>
                                            <td class="td">{{$item->age}}</td>
                                            <td class="td">{{date('d-M-Y ',strtotime($item->reg))}}</td>
                                            <td class="td">{{$item->visit_status}}</td>
                                             @if((isset($item->ward))&& (isset($item->bed_name)))
                                             <td class="td">{{$item->ward}}</td>
                                             <td class="td">{{$item->bed_name}}</td>
                                             @else
                                             <td class="td"></td>
                                             <td class="td"></td>
                                             @endif


                                            <td class="td">{{$item->ip_status}}</td>
                                            @if ($item->visit_status =='IP')
                                            <td class="td"onclick="patient_visit({{$item->id}});"><i class="fa fa-list" ></i></td>
                                            <td class="td"onclick="nurse_patient({{$item->id}});"><i class="fa fa-user-md" ></i></td>
                                            @else
                                            <td class="td"><i class="fa fa-list" ></i></td>
                                            <td class="td""><i class="fa fa-user-md" ></i></td>
                                            @endif
                                            <td class="td" style="text-align: center">
                                                {{--  <i class="fa fa-line-chart"></i>  --}}

                                                <button type="button" style="" class="btn btn-sm bg-blue" data-toggle="modal" data-target="#editvitalModal" onclick="showpatientVital('{{$item->id}}');">
                                                    <i id="add_vital_modal_btn" class="fa fa-line-chart"></i>
                                                </button>

                                            </td>
                                            <td class="td" style="text-align: center">
                                                <i class="fa fa-upload"></i>
                                            </td>

                                         </tr>

                                    @endforeach



                                 @else
                                    <tr>
                                        <td colspan="14" style="box-shadow: 0px 0px 1px!important;">No Patients Found</td>
                                    </tr>
                                @endif

                            </tbody>


                    </table>



                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 text-center">
                    <nav style="text-align:right;">{{ $res->appends(array_merge(request()->all()))->links() }}
                    </nav>
                </div>




                {!! Form::token() !!}
                {!! Form::close() !!}




        </div>
    </div>
</div>

<!-- Modal window for prescription -->
<div class="modal fade" id="ModalPatientListVital" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 80%;height:450px;">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-info bg-green">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Patient Vitals</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12" id="patientListVitalData11">
                    <input type='hidden' id="ssssssssssdddd"/>

                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<!-- Modal window for prescription  ends-->
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/vitals.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>
<script type="text/javascript">


//----------------------------------------------DATEPICKER---------------------------------------------------------------------------------------------

$("input[data-attr='date']").datetimepicker({ format: 'DD-MMM-YYYY' });

function employeeSearch() {

    $("#search").removeClass('fa fa-search');
    $("#search").addClass('fa fa-spinner fa-spin');
    $('#main').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
    $('#requestSearchForm').submit();


}

//-------------------------------------------------RELATIVE SELECT BOX----------------------------------------------------------------------------------------
$("#ward").on('change', function() {
    var ward = $('#ward').val();
    var room_type = $('#room_type').val();
    if (ward == '') {
      var ward = 'a';

    }


 if(ward) {

      if (ward != undefined) {
      $("#visit_status").val('ip');
      }
         var url = "{{route('extensionsvalley.master.ajaxRoomType')}}";

        $.ajax({
                    type: "GET",
                    url: url,
                    dataType: 'json',
                    data: {'ward':ward},
                    beforeSend: function () {
                    $('#room_type').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                    },
                    success: function (html) {


                                if(html){

                                       $("#room_type").empty();

                                        $("#room_type").html('<option value="">Select</option>');
                                        $.each(html,function(key,value){
                                            ($("#room_type").append('<option value="'+value.id+'"{{ old('room_type',$searchFields['room_type']) == $items_each->id ? 'selected' : '' }}>'+value.room_name+'</option>'));
                                        });

                                }else{
                                        $("#room_type").empty();
                                }

                    },

                    error: function () {
                        Command: toastr["warning"]("Please  select a proper item !");


                    },
                    complete: function () {
                        $('#room_type').LoadingOverlay("hide");
                    }

                });

        }else {
                $('#ward').focus();
                $("#room_type").empty();
            }

    });
//--------------------------------------LOGICAL FUNCTION------------------------------------------------------------------------------------------------------------------------------
    function selectStatus(val){
        if(val=='op'){
            $("#ward").val('');
            $('#room_type').val('');
        }
    }



//---------------------------------------CLEAR FUNCTION-----------------------------------------------------------------------------------------------------------------------------------
    function search_clear(){
         $("#ward").val('');
        $('#room_type').val('');
        $('#from_date').val('');
        $('#to_date').val('');
        $('#reg_from_date').val('');
        $('#reg_to_date').val('');
        $('#visit_status').val('');
        $('#mobile').val('');
        $('#area_col').val('');
        $('#gender').val('');
        $('#patient_id').val('');
        $('#patient_name').val('');




    }

//-------------------------------------------------------------------------------------------------AJAX AUTOCOMPLETE SEARCH----------------------------------------------------------------------------------------------------------------------------------------------

        $('#patient_name').keyup(function(){
            var query = $(this).val();
            if(query == ''){
                query= '@';
            }
                var url = "{{route('extensionsvalley.master.fetchdata')}}";
                $.ajax({

                type:"GET",
                url:url,
                data:{'query':query},
                beforeSend: function () {
                            $('#patient_list').fadeIn();
                            $('#patient_list').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                            },
                success:function(data){

                        $('#patient_list').html(data);
                },
                complete: function () {
                                $('#patient_list').LoadingOverlay("hide");
                            },
                });

        });

        $(document).on('click', '.patient_list_id li', function(){
            $('#patient_name').val($(this).text());
            $('#patient_list').fadeOut();
        });

        $(document).on('click', '.patient_list_id li', function(){
            $('#patient_name').val($(this).text());
            $('#patient_list').fadeOut();
        });
        $(document).click(function(){
          $("#patient_list").hide();
        });

//-------------------------------------------------------------------AUTO FILL DETAILS---------------------------------------------------------------------------------------------------------------------------------------------------

$(document).on('click', 'li', function(){
    var search_id= $(this).val();
    //alert(search_id);
  if(search_id !=''){
    var url = "{{route('extensionsvalley.master.autofill')}}";
                $.ajax({
                type:"GET",
                url:url,
                data:{'search_id':search_id},
                beforeSend: function () {
                           $('#patient_id').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                           $('#area_col').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                           $('#mobile').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                           $('#gender').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                            },
                success:function(data){
                   // alert(data);
                    $.each(data,function(key,value){
                   $('#patient_id').value='';
                   $('#patient_id').val(value.uhid);
                   $('#area_col').value='';
                   $('#area_col').val(value.area);
                   $('#mobile').value='';
                   $('#mobile').val(value.phone);
                   $('#gender').value='';
                   $("#gender ").val(value.gender).change();;


                    });

                },
                complete: function () {
                                $('#patient_id').LoadingOverlay("hide");
                                $('#area_col').LoadingOverlay("hide");
                                $('#mobile').LoadingOverlay("hide");
                                $('#gender').LoadingOverlay("hide");
                            },


           });
    }
  });

function patient_visit(patient_id){
    window.location.href=window.location.href.replace('master/show_full_patient_list','emr/patient-view/'+patient_id);
};
function nurse_patient(patient_id){
       window.location.href=window.location.href.replace('master/show_full_patient_list', 'nursing/nurse_patient-view/'+ patient_id);
};


function showpatientVital(patient_id){
    var url = "{{route('extensionsvalley.master.showpatientVital')}}";
    $.ajax({

        type:"GET",
        url:url,
        data:'patient_id='+patient_id,
        beforeSend: function () {
            $('#ModalPatientListVital').modal('toggle');
        },
        success:function(data){
            var obj = JSON.parse(data);
            $('#patient_id').val(obj.patient_id);
            $('#visit_id').val(obj.visit_id);
            $('#encounter_id').val(obj.encounter_id);
            $('#patientListVitalData11').html(obj.viewData);
        },
        complete: function () {
            clear_vital_vlues();
            $("#weight_value").focus();
        },

    });
}

</script>
@endsection
