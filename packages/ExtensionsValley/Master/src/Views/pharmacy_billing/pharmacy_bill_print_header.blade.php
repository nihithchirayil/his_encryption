@if(!empty($hospitalHeader) && $hospital_header_status == 1)
    {!!$hospitalHeader!!}
@endif
<div class="col-md-12" style="margin:20px; text-align:center; @if($hospital_header_status == 0) margin-top: 3cm; @endif" >
    <h3 style="margin:0px">PHARMACY <h6 style="margin:0px">{{@$is_duplicate ? $is_duplicate : ' '}}</h6></h3>
    <h3 style="margin:5px">@isset($pharmacy_bill_data[0]->print_header) ({{$pharmacy_bill_data[0]->print_header}}) @endisset</h3>
</div>
<table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;" width="100%" border="1" cellspacing="0" cellpadding="0">
    <thead style="display: table-header-group;">
        <tr class="head">
            <td><strong>PATIENT NAME</strong></td>
            <td align="center" style="border-left: none;">:</td>
            <td style="border-left: none;">@isset($pharmacy_bill_data[0]->print_header) {{$pharmacy_bill_data[0]->patient_name}} @endisset</td>
            <td style="border-left: none;"><strong>GENDER/AGE</strong></td>
            <td align="center" style="border-left: none;">:</td>
            <td style="border-left: none;">{{$pharmacy_bill_data[0]->gender}} / {{$pharmacy_bill_data[0]->age}}</td>
        </tr>
        <tr>
            <td><strong>{{strtoupper(\WebConf::getConfig('op_no_label'))}}</strong></td>
            <td align="center" style="border-left: none;">:</td>
            <td style="border-left: none;">{{$pharmacy_bill_data[0]->uhid}}</td>
            <td style="border-left: none;"><strong>IP NUMBER</strong></td>
            <td align="center" style="border-left: none;">:</td>
            <td style="border-left: none;">{{$pharmacy_bill_data[0]->admission_no}}</td>
        </tr>
        <tr>
            <td><strong>ADDRESS</strong></td>
            <td align="center" style="border-left: none;">:</td>
            <td style="border-left: none;">{{$pharmacy_bill_data[0]->address}}</td>
            <td style="border-left: none;"><strong>PAYMENT TYPE</strong></td>
            <td align="center" style="border-left: none;">:</td>
            <td style="border-left: none;">{{$pharmacy_bill_data[0]->payment_type}}</td>
        </tr>
        <tr>
            <td><strong>BILL NUMBER</strong></td>
            <td align="center" style="border-left: none;">:</td>
            <td style="border-left: none;">{{$pharmacy_bill_data[0]->bill_no}}</td>
            <td style="border-left: none;"><strong>BILL DATE</strong></td>
            <td align="center" style="border-left: none;">:</td>
            <td style="border-left: none;">{{date('M-d-Y h:i A', strtotime($pharmacy_bill_data[0]->bill_datetime))}}</td>
        </tr>
        <tr>
            <td><strong>LOCATION</strong></td>
            <td align="center" style="border-left: none;">:</td>
            <td style="border-left: none;">{{$pharmacy_bill_data[0]->location_name}}</td>
            <td style="border-left: none;"><strong>DOCTOR</strong></td>
            <td align="center" style="border-left: none;">:</td>
            <td style="border-left: none;">{{$pharmacy_bill_data[0]->doctor_name}}</td>
        </tr>
        <tr>
            <td><strong>SPONSOR</strong></td>
            <td align="center" style="border-left: none;">:</td>
            <td style="border-left: none;">{{$pharmacy_bill_data[0]->pricing_name}}</td>
            <td style="border-left: none;"><strong></strong></td>
            <td align="center" style="border-left: none;"></td>
            <td style="border-left: none;"></td>
        </tr>
    </thead>
  </table>
