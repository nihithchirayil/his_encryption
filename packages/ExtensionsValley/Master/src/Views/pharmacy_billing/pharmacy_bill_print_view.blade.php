<?php
$company_code = \DB::table('company')->where('status', 1)->value('code');
?>
<!-- .css -->
<style>
    #doctorHeadClass p:first-child{
        margin: 0 !important;
        padding: 0 !important;
    }
    .number_class {
        text-align: right;
    }
    @media print
    {
        table{ page-break-after:auto; }
        tr    { page-break-inside:auto; page-break-after:auto }
        td    { page-break-inside:auto; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
    }
</style>
<style type="text/css" media="print">
    table {
        font-size : 13px;
    }
    @page{
        margin-left:15px;
        margin-right:15px;
        margin-top:40px;
        margin-bottom:35px;
    }

</style>
<!-- .css -->
<!-- .box-body -->
<div class="box-body">
        <div class="col-md-12" >
            <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px; " width="100%" border="1" cellspacing="0" cellpadding="0">
                <thead>
                    @if((string)$hospital_header_status == "1")
                    <tr>
                        <th colspan="12" >
                         {!!$hospitalHeader!!}
                        </th>
                    </tr>
                    @endif
                    @if($gst_no)
                        <tr>
                            <td colspan="12">
                                <table style="border-color:rgb(247, 246, 246) !important; border-collapse:collapse; width:100%;" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td style="text-align:center; border-bottom: 0;" colspan="2"><strong>GST No</strong>: {{$gst_no}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <th colspan="12" style="font-size: 12px;" >
                        PHARMACY BILL {{@$is_duplicate ? $is_duplicate : ' '}}
                        </th>
                    </tr>
                    <tr>
                    <th colspan="12" style="@if((string)$hospital_header_status == '0') margin-top; 3cm; @endif " >
                        <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;  " width="100%" border="1" cellspacing="0" cellpadding="0">
                            <thead class="patient_head" style="display: table-header-group;">
                            @if($printMode == "1")
                                <tr class="head">
                                    <td><strong>PATIENT NAME</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">@isset($pharmacy_bill_data[0]->print_header) {{$pharmacy_bill_data[0]->patient_name}} @endisset</td>
                                    <td><strong>ADDRESS</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td colspan="4" style="border-left: none;">{{$pharmacy_bill_data[0]->address}}</td>
                                </tr>
                                <tr>
                                    <td><strong>{{strtoupper(\WebConf::getConfig('op_no_label'))}}</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->uhid}}</td>
                                    <td style="border-left: none;"><strong>BILL DATE</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{date('M-d-Y h:i A', strtotime($pharmacy_bill_data[0]->bill_datetime))}}</td>
                                    <td><strong>SPONSOR</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->pricing_name}}</td>
                                </tr>
                                <tr>
                                    @if($company_code != 'KEYHOLE')
                                    <td style="border-left: none;"><strong>IP NUMBER</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->admission_no}}</td>
                                    @endif
                                    <td><strong>BILL NUMBER</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;" @if($company_code == 'KEYHOLE') colspan="4" @endif>{{$pharmacy_bill_data[0]->bill_no}}</td>
                                    <td style="border-left: none;"><strong>DOCTOR</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->doctor_name}}</td>
                                    
                                </tr>
                                <tr>
                                    <td style="border-left: none;"><strong>GENDER/AGE</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->gender}} / {{$pharmacy_bill_data[0]->age}}</td>
                                    <td style="border-left: none;"><strong>PAYMENT TYPE</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->payment_type}}</td>
                                    <td><strong>LOCATION</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->location_name}}</td>
                                </tr>
                            @else
                                <tr>
                                    <td><strong>PATIENT NAME</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">@isset($pharmacy_bill_data[0]->print_header) {{$pharmacy_bill_data[0]->patient_name}} @endisset</td>
                                    <td style="border-left: none;"><strong>GENDER/AGE</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->gender}} / {{$pharmacy_bill_data[0]->age}}</td>
                                    <td><strong>{{strtoupper(\WebConf::getConfig('op_no_label'))}}</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;" @if($company_code == 'KEYHOLE') colspan="4" @endif>{{$pharmacy_bill_data[0]->uhid}}</td>
                                    @if($company_code != 'KEYHOLE')
                                    <td style="border-left: none;"><strong>IP NUMBER</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->admission_no}}</td>
                                    @endif
                                    <td><strong>SPONSOR</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->pricing_name}}</td>
                                    
                                </tr>
                                <tr>
                                    <td><strong>BILL NUMBER</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->bill_no}}</td>
                                    <td style="border-left: none;"><strong>BILL DATE</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{date('M-d-Y h:i A', strtotime($pharmacy_bill_data[0]->bill_datetime))}}</td>
                                    <td><strong>LOCATION</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->location_name}}</td>
                                    <td style="border-left: none;"><strong>DOCTOR</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td style="border-left: none;">{{$pharmacy_bill_data[0]->doctor_name}}</td>
                                    <td><strong>ADDRESS</strong></td>
                                    <td align="center" style="border-left: none;">:</td>
                                    <td colspan="4" style="border-left: none;">{{$pharmacy_bill_data[0]->address}}</td>
                                </tr>
                            @endif
                            </thead>
                        </table>
                    </th>
                    </tr>
                    <tr>
                    <th colspan="12">
                        <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;font-size: 12px;" width="100%" border="1" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th width="2%"><strong><center>#</center></strong></th>
                                    <th width="26%"><strong><center>Particulars</center></strong></th>
                                    <th width="7%"><strong><center>HSN Code</center></strong></th>
                                    <th width="8%"><strong><center>Batch</center></strong></th>
                                    <th width="9%"><strong><center>Exp Date</center></strong></th>
                                    <th width="7%"><strong><center>Price</center></strong></th>
                                    <th width="7%"><strong><center>Qty</center></strong></th>
                                    @if(isset($company_code) && $company_code != 'ALMA')
                                    <th width="5%"><strong><center>GST%</center></strong></th>
                                    <th width="8%"><strong><center>GST Amt</center></strong></th>
                                    @endif
                                    <th width="7%"><strong><center>MRP</center></strong></th>
                                    <th width="7%"><strong><center>Discount</center></strong></th>
                                    <th width="7%"><strong><center>Amount</center></strong></th>
                                </tr>
                            </thead>
                        </table>
                    </th>
                    </tr>
                </thead>
                <tbody>
                @if(count($pharmacy_bill_data)>0)
                @foreach($pharmacy_bill_data as $bill)
                <tr style="border-bottom: 1px solid #ccc;">
                    <td width="2%" > <center>{{$loop->iteration}}</center> </td>
                    <td width="26%" style="padding-left:5px;">
                        <div>{{ $bill->item_desc }}</div>
                        <div>Generic : {{ $bill->generic_name }}</div>
                    </td>
                    <td width="7%" style="text-align:left;padding-left:5px;"> {{ $bill->hsn_code  }} </td>
                    <td width="8%" style="text-align:left;padding-left:5px;"> {{ $bill->batch }} </td>
                    <td width="9%" style="text-align:left;padding-left:5px;"> {{ date('M-d-Y', strtotime($bill->expiry)) }} </td>
                    <td width="7%" class="number_class"> {{ $bill->selling_price_wo_tax }} </td>
                    <td width="7%" class="number_class"> {{ $bill->qty }} </td>
                    @if(isset($company_code) && $company_code != 'ALMA')
                    <td width="5%" class="number_class"> {{ $bill->tax_percent }} </td>
                    <td width="8%" class="number_class"> {{  number_format((float)$bill->unit_tax, 2, '.', '') }} </td>
                    @endif
                    <td width="7%" class="number_class"> {{ $bill->unit_mrp }} </td>
                    <td width="7%" class="number_class"> {{ $bill->item_discount_amount }} </td>
                    <td width="7%" class="number_class"> {{ $bill->net_amount }} </td>
                </tr>
                @endforeach

                @endif
                <tr>
                    <td colspan="12">
                        <table style="border-colapse:colapse;margin-top:15px; width:100%;" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width:50%;"></td>
                                <td style="width:25%;">
                                    <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse; width:100%;" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="text-align:left;"><strong>BILL TOTAL</strong></td>
                                            <td style="text-align:center;"><strong>:</strong></td>
                                            <td style="text-align:right;"><strong>{{$pharmacy_bill_data[0]->bill_amount}}</strong></td>
                                        </tr>
                                        {{-- @if($company_code != 'KEYHOLE') --}}
                                        <tr>
                                            <td style="text-align:left;"><strong>BILL DISCOUNT</strong></td>
                                            <td style="text-align:center;"><strong>:</strong></td>
                                            <td style="text-align:right;"><strong>{{$pharmacy_bill_data[0]->discount_amount_ttl}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;"><strong>TOTAL DISCOUNT</strong></td>
                                            <td style="text-align:center;"><strong>:</strong></td>
                                            <td style="text-align:right;"><strong>{{number_format(($pharmacy_bill_data[0]->discount_amount_ttl + $pharmacy_bill_data[0]->total_item_dis), 2) }}</strong></td>
                                        </tr>
                                        {{-- @endif --}}
                                        <tr>
                                            <td style="text-align:left;"><strong>NET AMOUNT</strong></td>
                                            <td style="text-align:center;"><strong>:</strong></td>
                                            <td style="text-align:right;"><strong>{{$pharmacy_bill_data[0]->net_amount_total}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;"><strong>CGST</strong></td>
                                            <td style="text-align:center;"><strong>:</strong></td>
                                            <td style="text-align:right;"><strong>{{($pharmacy_bill_data[0]->tax_total/2)}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;"><strong>SGST</strong></td>
                                            <td style="text-align:center;"><strong>:</strong></td>
                                            <td style="text-align:right;"><strong>{{($pharmacy_bill_data[0]->tax_total/2)}}</strong></td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width:10%;"></td>
                                <td style="width:15%">
                                    <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse; width:100%; height: 100px;" border="1" cellspacing="0" cellpadding="0">
                                        <tr>
                                            @if ($pharmacy_bill_data[0]->paid_status==1 || $company_code == 'MINUTE')
                                                <td><strong><center>BILL AMOUNT</center></strong></td>
                                             @else
                                                <td><strong><center>AMOUNT TO PAY</center></strong></td>
                                            @endif
                                        </tr>
                                        {{-- applying bill_amount as net_amount_total (amount to pay ) based on company code --}}
                                        @php
                                            $net_amount = ($company_code == 'MINUTE')
                                                ? $pharmacy_bill_data[0]->bill_amount
                                                : $pharmacy_bill_data[0]->net_amount_total;
                                        @endphp
                                        <tr>
                                            <td><strong><center>{{round($net_amount)}}<center></strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="10">
                                    <div style="text-align:right;"> {{  \ExtensionsValley\Master\GridController::number_to_word(round($pharmacy_bill_data[0]->net_amount_total)) }} </div>
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>

                <tr>
                    <td colspan="12">
                        <table style="border-colapse:colapse;margin-top:15px; width:100%;" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="display:flex; width:100%">
                                            <div style="text-align:left; width:30%;">{{$pharmacy_bill_data[0]->printout_footer_message}}</div>
                                            @if($company_code != 'KEYHOLE')
                                            <div style="text-align:right; width:60%;">
                                            <span style="margin-top:30px; position:absolute;">(signature)</span>
                                            </div>
                                            @else
                                            <div style="text-align:right; width:55%;">
                                            <span style="margin-top:30px; position:absolute;">(signature pharmacist)</span>
                                            </div>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Bill Created By : {{$pharmacy_bill_data[0]->created_text}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Bill Printed By : {{$pharmacy_bill_data[0]->printed_text}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        

</div>
<!-- .box-body -->

