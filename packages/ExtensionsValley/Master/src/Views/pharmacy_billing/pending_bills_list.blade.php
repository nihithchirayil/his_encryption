
<!-- Booking list Modal -->
<div id="pending_bills_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header box_header modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pending Bills List</h4>
            </div>
            <div class="modal-body" style="height:520px;">
                <div class="col-md-12" id="pending_bills_list_data"></div>
            </div>
        </div>

    </div>
</div>
<!-- Booking list Modal -->

<script src="{{ asset('packages/extensionsvalley/emr/js/pending_bills_list.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>