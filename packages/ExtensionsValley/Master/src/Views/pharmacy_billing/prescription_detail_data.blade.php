

@if(count($prescription_detail_data)>0)
@foreach($prescription_detail_data as $presc)
<tr>
    <td class="common_td_rules" title="{{ $loop->iteration }}">{{ $loop->iteration }}</td>
    <td class="common_td_rules" title="{{ $presc->item_desc }}">{{ $presc->item_desc }}</td>
    <td class="common_td_rules" title="{{ $presc->generic_name }}">{{ $presc->generic_name }}</td>
    <td class="common_td_rules" title="{{ $presc->frequency }}">{{ $presc->frequency }}</td>
    <td class="number_class" title="{{ $presc->quantity }}">{{ $presc->quantity }}</td>
    <td class="number_class" title="{{ ($presc->quantity-$presc->billed_qty) }}">{{ ($presc->quantity-$presc->billed_qty) }}</td>
    <td class="number_class" title="{{ $presc->r_total_stock }}">{{ $presc->r_total_stock }}</td>
    <td class="common_td_rules" title="{{ $presc->notes }}">{{ $presc->notes }}</td>
    <td title="">
        @if((int)$presc->billed_qty == 0)
        <button class="btn btn-sm btn-primary" onclick="showSubtituteItemModal('{{ $presc->id }}','{{ $presc->head_id }}','{{ $presc->item_code }}','{{ $presc->item_desc }}', '{{ $presc->generic_name }}', '{{ ($presc->quantity-$presc->billed_qty) }}');" type="button"><i class="fa fa-refresh"></i></button>
        @endif
    </td>
</tr>
@endforeach
@endif