<!------- out side patient registration modal ---------------->
<div class="modal" role="dialog" id="outSidePatientRegistrationModal" >
    <div class="modal-dialog" role="document" >
        <div class="modal-content">
            <div class="modal-header table_header_common" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Register Outside Patient</h4>
                <button type="button" class="close" onclick="closeOutsidePatientRegModal()" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height: 300px;">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-12 no-padding">
                        <div class="col-md-2 padding_sm" style="margin-top: -6px;">
                            <div class="radio radio-success inline no-margin ">
                                <input class="patient_wise" id="patient_wise" type="radio" name="hide_gender" value=1>
                                <label class="text-blue" for="patient">
                                    Patient
                                </label>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: -6px;">
                            <div class="radio radio-success inline no-margin ">
                                <input class="company_wise" id="company_wise" type="radio" name="hide_gender" value=2>
                                <label class="text-blue" for="company">
                                    Company
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm" style="margin-top: 7px;">
                        <div class="mate-input-box">
                            <label for="" class="required_field">Patient Name</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="out_side_patient_name" name="out_side_patient_name" value="" required>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding hide_gender_age">
                        <div class="col-md-6 padding_sm">
                            <div class="mate-input-box">
                                <label for="" class="required_field">Gender</label>
                                <div class="clearfix"></div>
                                {!! Form::select('out_side_patient_gender',$gender_list, null,['class' => 'form-control abc','placeholder' => 'Gender','title' => 'Gender','id' => 'out_side_patient_gender', 'style' => 'color:#555555; padding:2px 12px; ']) !!}
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <div class="mate-input-box">
                                <label for="" class="required_field">Age</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control" id="out_side_patient_age" name="out_side_patient_age" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="">Phone Number</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="out_side_patient_phone" name="out_side_patient_phone" value="" required>
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm">
                        <div class="mate-input-box">
                            <label for="doctor">Doctor</label>
                            <div class="clearfix"></div>
                            {!! Form::select('out_side_patient_doctor',$doctor_list, null,['class' => 'form-control select2','placeholder' => 'Doctor','title' => 'Doctor', 'disabled'=>'disabled', 'id' => 'out_side_patient_doctor','style' => 'color:#555555; padding:2px 12px; ']) !!}
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm">
                        <div class="mate-input-box">
                            <label for="out_side_patient_external_doctor_check">Outside Doctor</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" autocomplete="off" class="padding_sm" id="out_side_patient_external_doctor_check" name="out_side_patient_external_doctor_check" checked="checked" value="" required>
                        </div>
                    </div>
                    
                    <div class="col-md-6 padding_sm">
                        <div class="mate-input-box">
                            <label for="out_side_patient_external_doctor_name">External Doctor Name</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="out_side_patient_external_doctor_name" name="out_side_patient_external_doctor_name" value="" required>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="closeOutsidePatientRegModal()">Close</button>
                <button type="button" class="btn btn-default btn-primary" onclick="saveOutsidePatientReg();" >Save</button>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('/packages/extensionsvalley/emr/js/outside_patient_registration.js') }}"></script>
