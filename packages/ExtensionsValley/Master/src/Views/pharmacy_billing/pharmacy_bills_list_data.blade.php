<div class="theadscroll always-visible" style="position: relative; height:400px;">
    <table class="table theadfix_wrapper no-margin table-striped table-bordered table-condensed">
        <thead class="box_header">
            <tr>
                <th>Sl.No.</th>
                <th>Bill No</th>
                <th>Bill Tag</th>
                <th>Bill Date</th>
                <th>UHID</th>
                <th>IP Number</th>
                <th>Consulting Doctor</th>
                <th>Admitting Doctor</th>
                <th>Bill Amount</th>
                <th>Net Amount</th>
                <th>Payment Type</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($pending_bills))
            @for ($i = 0; $i < sizeof($pending_bills); $i++)
                @php
                    $bill_no = $pending_bills[$i]->bill_no;
                    $bill_tag = $pending_bills[$i]->bill_tag_name;
                    $bill_date = (!empty($pending_bills[$i]->bill_date)) ? ExtensionsValley\Emr\CommonController::getDateFormat($pending_bills[$i]->bill_date,'M-d-Y') : '';
                    $uhid = $pending_bills[$i]->uhid;
                    $ip_no = $pending_bills[$i]->ip_no;
                    $consulting_doctor = $pending_bills[$i]->consulting_doctor_name;
                    $admitting_doctor = $pending_bills[$i]->admitting_doctor_name;
                    $bill_amount = $pending_bills[$i]->bill_amount;
                    $net_amount = $pending_bills[$i]->net_amount;
                    $payment_type = $pending_bills[$i]->payment_type_desc;
                @endphp
                <tr>
                    <td title="{{$i+1}}">{{$i+1}}</td>
                    <td title="{{$bill_no}}">{{$bill_no}}</td>
                    <td title="{{$bill_tag}}">{{$bill_tag}}</td>
                    <td title="{{$bill_date}}">{{$bill_date}}</td>
                    <td title="{{$uhid}}">{{$uhid}}</td>
                    <td title="{{$ip_no}}">{{$ip_no}}</td>
                    <td title="{{$consulting_doctor}}">{{$consulting_doctor}}</td>
                    <td title="{{$admitting_doctor}}">{{$admitting_doctor}}</td>
                    <td title="{{$bill_amount}}">{{$bill_amount}}</td>
                    <td title="{{$net_amount}}">{{$net_amount}}</td>
                    <td title="{{$payment_type}}">{{$payment_type}}</td>
                </tr>
            @endfor
            @else
            <td colspan="10">No Data Found..!</td>
            @endif
        </tbody>
    </table>
</div>