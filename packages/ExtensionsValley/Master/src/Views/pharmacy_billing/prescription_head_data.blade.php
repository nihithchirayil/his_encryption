

<div class="theadscroll always-visible" style="position: relative; height: 254px;">
    <table class="table theadfix_wrapper no-margin table-striped table-bordered table-condensed">
        <thead class="table_header_common">
            <tr>
                <th>Sl.No.</th>
                <th>Select</th>
                <th>Action</th>
                <th>UHID</th>
                <th>Patient Name</th>
                <th>Created Date</th>
                <th>Pharmacy Location</th>
                <th>Doctor</th>
                <th>Nursing Location</th>
            </tr>
        </thead>
        <tbody >
            @if(count($prescription_head_data)>0)
            @foreach($prescription_head_data as $presc)
            <tr data-presc-id="{{ $presc->id }}" data-patient-id="{{ $presc->patient_id }}" data-patient-uhid="{{ $presc->uhid }}" data-outside-medicine-count="{{ $presc->outside_medicine_count }}" style="@if($presc->billconverted_status == 0) background: #a2d296; @endif">
                <td class="common_td_rules" title="{{ ($prescription_head_data->currentPage() - 1) * $prescription_head_data->perPage() + $loop->iteration }}">{{ ($prescription_head_data->currentPage() - 1) * $prescription_head_data->perPage() + $loop->iteration }}</td>
                <td class="common_td_rules" title="@if($presc->billconverted_status == 0) Already converted as bill. @elseif($presc->outside_medicine_count > 0) Please substitute the outside medicine to proceed. @endif">
                    <input type="checkbox" autocomplete="off" @if($presc->billconverted_status == 0 || $presc->outside_medicine_count > 0) readonly="readonly" disabled="disabled" @endif class="padding_sm presc_select_check" name="presc_select_check" value="" required>
                </td>
                <td class="common_td_rules">
                    <div style="display: flex; ">
                        <button class="btn btn-sm btn-primary" type="button" onclick="fetchPrescriptionDetailData('{{$presc->id}}')"><i class="fa fa-eye"></i></button>
                        <button class="btn btn-sm btn-primary" type="button" onclick="printPescriptionView('{{$presc->id}}')"><i class="fa fa-print"></i></button>
                    </div>
                </td>
                <td class="common_td_rules" title="{{ $presc->uhid }}">{{ $presc->uhid }}</td>
                <td class="common_td_rules" title="{{ $presc->patient_name }}">{{ $presc->patient_name }}</td>
                <td class="common_td_rules" title="{{ date('M-d-Y h:i A', strtotime($presc->created_at)) }}">{{ date('M-d-Y h:i A', strtotime($presc->created_at)) }}</td>
                <td class="common_td_rules" title="{{ $presc->pharmacy_location }}">{{ $presc->pharmacy_location }}</td>
                <td class="common_td_rules" title="{{ $presc->doctor_name }}">{{ $presc->doctor_name }}</td>
                <td class="common_td_rules" title="{{ $presc->nursing_location }}">{{ $presc->nursing_location }}</td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="9">No records found..!</td>
            </tr>
            @endif
            
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>