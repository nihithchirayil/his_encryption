<div class="theadscroll" style="position: relative; height: 400px;margin-top: 10px">
    <table class="table theadfix_wrapper no-margin table_sm table-striped no-border styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr style="text-align: center" class="common_table_header">
                <th colspan="6"><?= $samplename ?></th>

            </tr>
            <tr style="text-align: center !important" class="common_main_table_header">
                <th width="5%">SL.No.</th>
                <th width="30%">Test Name</th>
                <th width="15%">Bill Number</th>
                <th width="15%">Bill Date</th>
                <th width="15%">Sample Collected Date</th>
                <th width="20%">Sample Collected By</th>
            </tr>
        </thead>
        <tbody id="SampleGenetationTbody">
            @if (count($result) > 0)
                @php
                    $i = 1;
                @endphp
                @foreach ($result as $each)
                    @php $row_class ='green'; @endphp
                    @if (intval($each->status) == 0)
                        @php $row_class ='red'; @endphp
                    @endif
                    <tr class="row_class {{ $row_class }}" id="tableSapmpleGenetationRow{{ $i }}">
                        <td class="common_td_rules row_count_class">
                            {{ $i }}.</td>
                        <td id="tableSampleGenetationItemDesc{{ $i }}" class="common_td_rules">
                            {{ $each->item_desc }}</td>
                        <td class="common_td_rules">{{ $each->bill_no }}
                            <input type="hidden" name="tableSampleGenetationSamplabbillsID"
                                value="{{ $each->id }}">
                            <input type="hidden" name="tableSampleGenetationSampleNo"
                                value="{{ $each->generated_sample_no }}">
                            <input type="hidden" name="tableSampleGenetationsampleId" value="{{ $each->sample_id }}">
                            <input type="hidden" name="tableSampleGenetationSampleIsOutsource"
                                value="{{ $each->is_outsource }}">
                            <input type="hidden" name="tableSampleGenetationSamplePatientId"
                                value="{{ $each->patient_id }}">
                            <input type="hidden" name="tableSampleGenetationSampleVisitId"
                                value="{{ $each->visit_id }}">
                            <input type="hidden" name="tableSampleGenetationSamplevisitStatus"
                                value="{{ $each->visit_status }}">
                            <input type="hidden" name="tableSampleGenetationSampleDoctorId"
                                value="{{ $each->doctor_id }}">
                            <input type="hidden" name="tableSampleGenetationSampleLocationId"
                                value="{{ $each->location_id }}">
                            <input type="hidden" name="tableSampleGenetationSampleStatus"
                                value="{{ $each->status }}">
                        </td>
                        <td>
                            <?= date('M-d-Y h:i:A', strtotime($each->bill_datetime)) ?>
                        </td>
                        <td>
                            <?= @$each->sample_collection_date ? $each->sample_collection_date : '-' ?>
                        </td>
                        <td>
                            <?= @$each->sample_collected_by ? $each->sample_collected_by : '-' ?>
                        </td>
                    </tr>
                    @php $i++; @endphp
                @endforeach
            @else
                <tr>
                    <td colspan="6" style="text-align: center">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
