@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/common-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/colors.css') }}" rel="stylesheet">
    <style>
        .mate-input-box {
            height: 54px !important;
        }
    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" value="" id="patient_id_hidden">
    <input type="hidden" value="5" id="blade_type">
    <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">


    @include('Master::RegistrationRenewal.advancePatientSearch')

    <div class="modal fade" id="getSampleDetailsModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="getSampleDetailsModelHeader"></h4>
                </div>
                <div class="modal-body" style="min-height: 450px">
                    <div class="col-md-12 padding_sm" id="getSampleDetailsModelDiv">

                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px"title="Genarate Sample" onclick="GenarateSample()" type="button"
                        id="GenarateSampleBtn" class="btn btn-primary">Genarate
                        Sample <i id="GenarateSampleSpin" class="fa fa-file"></i></button>
                </div>
            </div>
        </div>
    </div>


    <div class="right_col" style="min-height: 700px !important;">
        <div class="">
            <div class="row padding_sm">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-12 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 160px;">
                                <div class="col-md-12 padding_sm">
                                    <div class="row padding_sm">
                                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label for="">From Date</label>
                                                <div class="clearfix"></div>
                                                <input type="text" autocomplete="off" class="form-control datepicker"
                                                    id="bill_from_date" onblur="searchCollection()" name="bill_from_date"
                                                    value="<?= date('M-d-Y') ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label for="">To Date</label>
                                                <div class="clearfix"></div>
                                                <input type="text" autocomplete="off" class="form-control datepicker"
                                                    id="bill_to_date" onblur="searchCollection()" name="bill_to_date"
                                                    value="<?= date('M-d-Y') ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label for="">UHID</label>
                                                <div class="clearfix"></div>
                                                <input type="hidden" class="form-control" id="patientsearch_ipstatus"
                                                    value="0">
                                                <input type="text" onkeyup="searchPatientUhid(this)"
                                                    autocomplete="off" class="form-control" id="patient_uhid"
                                                    name="patient_uhid" value="">
                                                <button type="button" onclick="advancePatientSearch(1)"
                                                    class="btn btn-success pull-right" style="margin-top: -28px;">
                                                    <i class="fa fa-search"></i></button>
                                                <div id="patient_uhid_AjaxDiv" class="ajaxSearchBox"
                                                    style="margin-top:-13px;z-index: 99999;"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label>Sample</label>
                                                <div class="clearfix"></div>
                                                <select onchange="searchCollection()" class="form-control select2"
                                                    id="searchSample">
                                                    <option value="">Select</option>
                                                    <?php
                                                        foreach ($sample_master as $each) {
                                                            ?>
                                                    <option value="<?= $each->id ?>"><?= $each->name ?></option>
                                                    <?php
                                                        }
                                                    ?>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label>Visit Type</label>
                                                <div class="clearfix"></div>
                                                <select onchange="searchCollection()" class="form-control select2"
                                                    id="searchPatientStatus">
                                                    <option value="">Select</option>
                                                    <option value="IP">IP</option>
                                                    <option value="OP">OP</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label for="">Bill Number</label>
                                                <div class="clearfix"></div>
                                                <input type="text" onkeyup="searchBillNumber(this)" autocomplete="off"
                                                    class="form-control" id="patient_billno" name="patient_billno"
                                                    value="">
                                                <input type="hidden" id="billno_hidden" value="">
                                                <div id="patient_billno_AjaxDiv" class="ajaxSearchBox"
                                                    style="margin-top:-13px;z-index: 99999;"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label>Lab</label>
                                                <div class="clearfix"></div>
                                                <select onchange="searchCollection()" class="form-control select2"
                                                    id="searchLab">
                                                    <option value="">Select</option>
                                                    <?php
                                                        foreach ($department_master as $each) {
                                                            ?>
                                                    <option value="<?= $each->id ?>"><?= $each->dept_name ?></option>
                                                    <?php
                                                        }
                                                    ?>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-top: 30px">
                                            <div class="checkbox checkbox-success inline">
                                                <input checked onclick="searchCollection()" id="pending_test"
                                                    type="checkbox" name="checkbox">
                                                <label class="text-blue" for="pending_test">
                                                    Pending Test
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1 padding_sm" style="margin-top: 26px">
                                            <button id="searchCollectionBtn" type="button" onclick="searchCollection()"
                                                class="btn btn-primary btn-block">
                                                <i id="searchCollectionSpin" class="fa fa-search"></i> Search</button>
                                        </div>
                                        <div class="col-md-1 padding_sm" style="margin-top: 26px">
                                            <button id="reset" type="button" onclick="reset_collection()"
                                                class="btn btn-warning btn-block">
                                                <i id="" class="fa fa-refresh"></i> Reset</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm" id="patientappoinment_div" style="margin-top: 10px">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 600px;">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/Dashboard/js/moment/moment.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/SampleCollection.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
@endsection
