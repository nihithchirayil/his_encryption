<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var bill_from_date = $('#bill_from_date').val();
            var bill_to_date = $('#bill_to_date').val();
            var patient_id = $('#patient_id_hidden').val();
            var searchSample = $('#searchSample').val();
            var billno = $('#patient_billno').val();
            var searchLab = $('#searchLab').val();
            var PatientStatus = $('#searchPatientStatus').val();
            var param = {
                _token: token,
                bill_from_date: bill_from_date,
                bill_to_date: bill_to_date,
                patient_id: patient_id,
                searchSample: searchSample,
                billno: billno,
                searchLab: searchLab,
                PatientStatus: PatientStatus
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $('#searchCollectionBtn').attr('disabled', true);
                    $('#searchCollectionSpin').removeClass('fa fa-search');
                    $('#searchCollectionSpin').addClass('fa fa-spinner fa-spin');
                    $("#patientappoinment_div").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(data) {
                    $('#patientappoinment_div').html(data);
                },
                complete: function() {
                    $('#searchCollectionBtn').attr('disabled', false);
                    $('#searchCollectionSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchCollectionSpin').addClass('fa fa-search');
                    $("#patientappoinment_div").LoadingOverlay("hide");
                }
            });
            return false;
        });

    });
</script>
<table class="table table_sm no-margin table-striped table-col-bordered table-condensed" style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_common">
            <th width="3%">SL.No.</th>
            <th width="15%">UHID</th>
            <th width="35%">Patient Name</th>
            <th width="15%">Sample</th>
            <th width="8%">Visit</th>
            <th width="8%">Total Test</th>
            <th width="8%">Pending Test</th>
            <th width="8%" class="text_wrap">Converted Test</th>
            <th width="3%"><i class="fa fa-history"></i></th>
            <th width="3%"><i class="fa fa-plus"></i></th>
        </tr>
    </thead>
    <tbody>
        @if (count($item) > 0)
            @php $i =1; @endphp
            @foreach ($item as $each)
                <tr>
                    <td class="td_common_numeric_rules " id="">
                        {{ ($item->currentPage() - 1) * $item->perPage() + $loop->iteration }}</td>
                    <td id="patient_sample_uhid<?= $i ?>" class="common_td_rules">{{ $each->uhid }}</td>
                    <td id="patient_sample_patient_name<?= $i ?>" class="common_td_rules">{{ $each->patient_name }}
                    </td>
                    <td id="patient_sample_samplename<?= $i ?>" class="common_td_rules">{{ $each->samplename }}</td>
                    <td id="patient_sample_visit_status<?= $i ?>" class="common_td_rules">
                        {{ $each->visit_status }}</td>
                    <td class="common_td_rules">{{ $each->testcount }}</td>
                    <td class="common_td_rules">{{ $each->pending }}</td>
                    <td class="common_td_rules">{{ $each->samplegenerated }}</td>
                    <td class="common_td_rules">
                        <button style="padding: 0px 4px;" title="Sample Genarate History" type="button"
                            id="historyGenearateSampleBtn{{ $i }}"
                            onclick="addGenearateSample(<?= $i ?>,<?= $each->sample_id ?>,<?= $each->item_dept_id ?>,'history')"
                            class="btn btn-warning"><i id="historyGenearateSampleSpin{{ $i }}"
                                class="fa fa-history"></i></button>
                    </td>
                    @if (intval($each->pending) != 0)
                        <td class="common_td_rules">
                            <button style="padding: 0px 4px;" title="Genarate Sample" type="button"
                                id="addGenearateSampleBtn{{ $i }}"
                                onclick="addGenearateSample(<?= $i ?>,<?= $each->sample_id ?>,<?= $each->item_dept_id ?>,'add')"
                                class="btn btn-primary"><i id="addGenearateSampleSpin{{ $i }}"
                                    class="fa fa-plus"></i></button>
                        </td>
                    @else
                        <td class="common_td_rules">-</td>
                    @endif

                </tr>
                @php $i++; @endphp
            @endforeach
        @else
            <tr>
                <td colspan="10" style="text-align: center">No Records found</td>
            </tr>
        @endif
    </tbody>
</table>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination">
        {!! $page_links !!}
    </ul>
</div>
