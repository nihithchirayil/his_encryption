<?php
$country = ExtensionsValley\Master\Models\Country::where('status', '=', '1')->orderBy('name')->pluck('name', 'id');
$area = ExtensionsValley\Master\Models\Area::Where('status', 1)->OrderBy('name')->pluck('name', 'name');
$card_details = \DB::table('loyality_card')->where('status', 1)->orderby('card_name')->pluck('card_name', 'id');
$company_details = \DB::table('credit_company')->where('status', 1)->orderby('company_name')->pluck('company_name', 'id');
$pricing_details = \DB::table('pricing_master')->where('status', 1)->orderby('pricing_name')->pluck('pricing_name', 'id');
$genders = \DB::table('gender')->OrderBy('name')->pluck('name', 'id');
if (isset($benef_active_count) && $benef_active_count > 5) {
    $benef_active_count_span = '<span>Exceed the lmit</span>';
} else {
    $benef_active_count_span = '<a class="" data-toggle="modal" href="#benefModel" > <button class="btn btn_green_bg">
                                    <i class="fa fa-plus"></i> </button></a>';
}
?>
<input type="hidden" value="{{$benef_active_count}}" name="benef_active_count" id="benef_active_count">
<div class="container-fluid" style="padding: 15px;">
    <div class="row codfox_container">
        <div class="clearfix"></div>
        <div class="ht10"></div>
        <div class="col-md-12 padding_sm">
            <div class="col-md-6 padding_sm">
                <div class="panel panel-default no-margin">
                    <div class="panel-heading panel_bg clearfix">
                        <div class="col-md-8 padding_sm">
                            Card Holder
                        </div>
                        <div class="col-md-4 text-right">
                            {{$family_holder_data->uhid}}
                        </div>
                    </div>
                    <div class="panel-body form_group_last_no_margin" style="padding: 10px;">
                        {!!Form::open(array('url' => $updateUrl,'id'=>'cardeditForm', 'method' => 'post'))!!}
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-12 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Select Card</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('card_no', array(""=> "Select") + $card_details->toArray(),$family_holder_data->daya_family_card_no,['autocomplete'=>'off', 'class'=>'form-control','id' => 'upd_card_no']) !!}                            
                                    </div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Name</label>
                                        <div class="clearfix"></div>
                                        <input  type="text" value="{{$family_holder_data->patient_name}}" class="form-control" name="name" placeholder="First Name"></div></div>
                                <?php
                                if ($family_holder_data->is_director == 1) {
                                    $chkd = 'checked';
                                } else {
                                    $chkd = '';
                                }
                                ?>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <input type="checkbox" <?= $chkd; ?> id="isDirector" name="is_director" /> <label for="" class="header_label">Is Director</label></div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="">DOB</label>
                                        <input type="text" data-attr="date1"  autocomplete="off" name="date" value="{{date('M-d-Y',strtotime($family_holder_data->dob))}}" class="form-control" placeholder="MM-DD-YYYY" >
                                       </div></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Area</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('area', $area,$family_holder_data->area,['autocomplete'=>'off', 'class'=>'form-control','id' => 'area']) !!}
                                        </div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Pincode</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$family_holder_data->pincode}}"  class="form-control" name="pincode" id="pincode" autocomplete="off" placeholder="Pincode">
                                       </div></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Mobile No</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$family_holder_data->phone}}" class="form-control" name="mobile_no" id="mobile_no" autocomplete="off" placeholder="Mobile No">
                                       </div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Email</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$family_holder_data->email}}" class="form-control" name="email" id="email" autocomplete="off" placeholder="Email">
                                        </div></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Pan No</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$family_holder_data->pan_no}}" class="form-control" name="pan_no" id="pan_no" autocomplete="off" placeholder="Pan No">
                                        </div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Nationality</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('nationality', $country,$family_holder_data->nationality?$family_holder_data->nationality:1,['autocomplete'=>'off', 'class'=>'form-control','id' => 'nationality']) !!}
                                       </div></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Company</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('company', array(""=> "Select") + $company_details->toArray(),$family_holder_data->company_id,['autocomplete'=>'off', 'class'=>'form-control','id' => 'company_id']) !!}
                                       </div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Pricing</label>
                                            <div class="clearfix"></div>
                                            {!! Form::select('pricing_id', array(""=> "Select") + $pricing_details->toArray(),$family_holder_data->pricing_id,['autocomplete'=>'off', 'class'=>'form-control','id' => 'pricing_id']) !!}
                                       </div></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Passport No</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$family_holder_data->passport_no}}" class="form-control" name="passport_no" id="passport_no" autocomplete="off" placeholder="Passport No">
                                       </div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-5 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Gender</label>
                                        <div class="clearfix"></div>
                                        @php $gender_array = array("0"=> " Select") + $genders->toArray() @endphp
                                        {!! Form::select('gender', $gender_array,$family_holder_data->gender,['autocomplete'=>'off', 'class'=>'form-control','id' => 'gender']) !!}
                                        
                                    </div></div>
                            </div>
                        </div>
                      
                        <div class="col-md-12 padding_sm">
                            <div class="form-group clearfix">
                                    <div class="mate-input-box">
                                       <label for="">Address</label>
                                        <div class="clearfix"></div>
                                        <textarea class="form-control" id="address" name="address" placeholder="Address">{{$family_holder_data->address}}</textarea>
                                       </div>

                            </div>          
                        </div>



                        <input type="hidden" value="{{$family_holder_data->id}}" name="edit_id" id="edit_id">
                        <div class="col-md-12 padding_sm text-right">
                         
                            <span class="btn btn-danger " style= "margin-top" onclick="deleteCardHolder({{$family_holder_data->id}})"><i class="fa fa-times"></i> Delete</span>
                               <button type="button" style="margin-top" class="btn btn-success" onclick="updateCardHolder()" ><i class="fa fa-save" id="update_card_holder_spin"></i> Update</button>
                        </div>
                        {!! Form::token() !!}
                        {!! Form::close() !!} 

                    </div>
                </div>
            </div>
            <div class="col-md-6 padding_sm">
                <div class="panel panel-default no-margin">
                    <div class="panel-heading panel_bg clearfix">
                        <div class="col-md-8 padding_sm">
                            Beneficiaries
                        </div>
                        <div class="col-md-4 text-right">
                            <?= $benef_active_count_span ?>
                            <!--                            <a class="" data-toggle="modal" href="#benefModel" > <button class="btn btn_green_bg">
                                                                <i class="fa fa-plus"></i> </button></a>-->
                        </div>
                    </div>
                    <div class="panel-body form_group_last_no_margin" style="padding: 10px;">
                        <?php
                        if (isset($benef_data) && $benef_data != '') {
                            $i = 0;
                            foreach ($benef_data as $dtls) {
                                if ($dtls->family_status == 0) {
                                    $bg_color = 'style=" background-color: #efe1e1;cursor:pointer"';
                                } else {
                                    $bg_color = 'style="background-color: #d3f5de;cursor:pointer"';
                                }
                                ?>
                                <div class="col-md-12 padding_sm benef_edit_drop_box" >
                                    <div class="form-group clearfix"  <?= $bg_color ?>>
                                        <!--                                        <div class="input-group">-->
                                        <label class="benef_drop_btn" id="show_name_<?= $i ?>" style="cursor:pointer;width: 100%">
                                        <h4> <span id="show_name_update_<?= $i ?>"><?= $dtls->patient_name; ?> </span>
                                        <span id="show_rel_update_<?= $i ?>">{{$dtls->relationship_title ? '('.$dtls->relationship_title.')' :''}}</span>
                                        <span class="pull-right">{{$dtls->uhid ? $dtls->uhid :''}}</span>
                                        </h4> </label>
        <!--                                            <input type="text" class="form-control benef_drop_btn" style="cursor:pointer" <?= $bg_color ?> value="<?= $dtls->patient_name; ?>" id="show_name_<?= $i ?>">-->
                                        <!--                                            <div class="input-group-btn">
                                                                                        <button class="btn btn_green_bg benef_drop_btn"><i class="fa fa-edit"></i></button>
                                                                                    </div>-->
                                        <!--                                        </div>-->
                                        <div class="beneficiary_dropdown">
                                            <form  id="benef_edit_<?= $i ?>" method="post">
                                                <div class="col-md-12 padding_sm text-right">
                                                    <span class="btn btn-success" onclick="updateBenef('<?= $i ?>')"><i class="fa fa-save" id="update_benif_spin_<?= $i ?>"></i> Save</span>
                                                    <?php if ($dtls->family_status == 1) { ?>
                                                        <span class="btn btn-primary" onclick="blockBenef('<?= $i ?>', '<?= $dtls->id ?>', '1')"><i class="fa fa-ban" id="block_benif_spin_<?= $i ?>"></i> Block</span>
                                                    <?php } else if ($benef_active_count <= 5 && $dtls->family_status == 0) { ?>
                                                        <span class="btn btn-danger" onclick="blockBenef('<?= $i ?>', '<?= $dtls->id ?>', '0')"><i class="fa fa-ban" id="unblock_benif_spin_<?= $i ?>"></i> UnBlock</span>
                                                    <?php } ?>
                                                    <span class="btn btn-danger benef_drop_close_btn" id="edit_close_button_<?= $i ?>"><i class="fa fa-times"></i> Close</span>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="ht5"></div>
<!--                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Select Card</label>
                                                        <div class="clearfix"></div>
                                                {!! Form::select('benef_card_no', array(""=> "Select") + $card_details->toArray(),$dtls->daya_family_card_no,['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_card_no']) !!}
                                                    </div>
                                                </div>-->
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">name</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" id="name_<?= $i; ?>"value="<?= $dtls->patient_name; ?>"class="form-control" name="benef_name" id="benef_name" autocomplete="off" placeholder="name">
                                                    </div></div>
                                                <?php
                                                $relationship = \DB::table('relationship_master')->where('status', 1)->where('id','<>',1)->orderBy('view_order')->pluck('description', 'id');
                                                ?>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Relationship</label>
                                                        <div class="clearfix"></div>
                                                        {!! Form::select('relationship', $relationship,$dtls->dependent_relation,['autocomplete'=>'off', 'class'=>'form-control','id' => 'relationship_'.$i]) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label class="">DOB</label>
                                                         <input type="text" data-attr="date1"  autocomplete="off" name="benef_date" value="{{date('M-d-Y',strtotime($dtls->dob))}}" class="form-control" placeholder="MM-DD-YYYY" >
                                                        
                                                    </div>
                                                </div>
                                               
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Area</label>
                                                        <div class="clearfix"></div>
                                                        {!! Form::select('benef_area', $area,$dtls->area,['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_area']) !!}
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Pincode</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" class="form-control" value="<?= $dtls->pincode; ?>"  name="benef_pincode" id="benef_pincode" autocomplete="off" placeholder="Pincode">
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Mobile No</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" class="form-control" value="<?= $dtls->phone; ?>" name="benef_mobile_no" id="benef_mobile_no" autocomplete="off" placeholder="Mobile No">
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Email</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" class="form-control" value="<?= $dtls->email; ?>" name="benef_email" id="benef_email" autocomplete="off" placeholder="Email">
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Pan No</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" class="form-control" value="<?= $dtls->pan_no; ?>" name="benef_pan_no" id="benef_pan_no" autocomplete="off" placeholder="Pan No">
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Nationality</label>
                                                        <div class="clearfix"></div>
                                                        {!! Form::select('benef_nationality', $country,$dtls->nationality?$dtls->nationality:1,['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_nationality']) !!}
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Passport No</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" class="form-control" value="<?= $dtls->passport_no; ?>"  name="benef_passport_no" id="benef_passport_no" autocomplete="off" placeholder="Passport No">
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Company</label>
                                                        <div class="clearfix"></div>
                                                        {!! Form::select('benef_company', array(""=> "Select") + $company_details->toArray(),$dtls->company_id,['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_company_id']) !!}
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Pricing</label>
                                                        <div class="clearfix"></div>
                                                        {!! Form::select('benef_pricing_id', array(""=> "Select") + $pricing_details->toArray(),$dtls->pricing_id,['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_pricing_id']) !!}</div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Gender</label>
                                                        <div class="clearfix"></div>
                                                        @php $gender_array = array("0"=> " Select") + $genders->toArray() @endphp
                                                        {!! Form::select('gender_benef', $gender_array,$dtls->gender,['autocomplete'=>'off', 'class'=>'form-control','id' => 'gender_benef']) !!}

                                                    </div></div>
                                    
                                              <div class="col-md-5 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">Address</label>
                                            <div class="clearfix"></div>
                                            <textarea class="form-control" id="benef_address" name="benef_address" placeholder="Address"><?= $dtls->address; ?></textarea>
                                        </div></div>
                                    <input type="hidden" name="benef_edit_id" id="benef_id" value="{{$dtls->id}}"                    
                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <?php
                    $i++;
                }
            }
            ?>


        </div>
    </div>
</div>

</div>
</div>
</div>


<div class="modal" id="benefModel"  role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background:#26B99A; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">New Beneficiarey</h4>
            </div>
            {!!Form::open(array('id'=>'benefaddForm', 'method' => 'post'))!!}  
            <div class="modal-body"> 
                <div class="row">
<!--                    <div class="col-md-12 no-padding">
                        <div class="form-group clearfix">
                            <div class="col-md-8 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Select Card</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('benef_card_no', array(""=> "Select") + $card_details->toArray(),'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'card_no']) !!}
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="col-md-12 no-padding">
                        <div class="form-group clearfix">
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="benef_name" id="benef_name" autocomplete="off" placeholder="name">
                                </div>
                            </div>
                            <?php
                            $relationship = \DB::table('relationship_master')->where('status', 1)->where('id','<>',1)->orderBy('view_order')->pluck('description', 'id');
                            ?>
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Relationship</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('relationship', $relationship,'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'relationship']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="form-group clearfix">
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label class="">DOB</label>
                                    <input type="text" data-attr="date1"  autocomplete="off" name="benef_date" value="" class="form-control" placeholder="MM-DD-YYYY" id="dob2">
                                </div>
                            </div>
                            
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Area</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('benef_area', $area,'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_area']) !!}
                                </div>
                            </div>
                           
                        </div></div>
                    <div class="col-md-12 no-padding">
                        <div class="form-group clearfix">
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Pincode</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="benef_pincode" id="benef_pincode" autocomplete="off" placeholder="Pincode">
                                </div>
                            </div>

                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Mobile No</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="benef_mobile_no" id="benef_mobile_no" autocomplete="off" placeholder="Mobile No">
                                </div>
                            </div>

                          
                        </div></div>
                    <div class="col-md-12 no-padding">
                        <div class="form-group clearfix">
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Email</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="benef_email" id="benef_email" autocomplete="off" placeholder="Email">
                                </div>
                            </div>

                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Pan No</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="benef_pan_no" id="benef_pan_no" autocomplete="off" placeholder="Pan No">
                                </div>
                            </div>

                            
                        </div></div>
                    <div class="col-md-12 no-padding">
                        <div class="form-group clearfix">
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Passport No</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="benef_passport_no" id="benef_passport_no" autocomplete="off" placeholder="Passport No">
                                </div>
                            </div>
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Nationality</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('benef_nationality', $country,1,['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_nationality']) !!}
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="form-group clearfix">
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Company</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('benef_company', array(""=> "Select") + $company_details->toArray(),'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_company_id']) !!}
                                </div>
                            </div>
                            <div class="col-md-5 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Pricing</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('benef_pricing_id', array(""=> "Select") + $pricing_details->toArray(),'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_pricing_id']) !!}</div></div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                    <div class="form-group clearfix">
                        <div class="col-md-5 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Gender</label>
                                <div class="clearfix"></div>
                                @php $gender_array = array("0"=> " Select") + $genders->toArray() @endphp
                                {!! Form::select('gender_benef', $gender_array,'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'gender_benef']) !!}

                            </div></div>
                    </div>
                    </div>
            <div class="col-md-12 no-padding">
                <div class="form-group clearfix">
                    <div class="col-md-10 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Address</label>
                            <div class="clearfix"></div>
                            <textarea class="form-control" id="benef_address" name="benef_address" placeholder="Address"></textarea>
                        </div>
                    </div>

                </div>
            </div>
            <input type="hidden" name="parent_card_holder" id="parent_card_holder" value="{{$family_holder_data->id}}"                    
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal" id="close_benef_modal"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</button>
        <button type="button" class="btn btn-success" onclick="saveBenef()"><i class="fa fa-save" id="benif_spin"></i>&nbsp;&nbsp;Save</button>
    </div>
    {!! Form::token() !!}
    {!! Form::close() !!} 
</div>
</div>
</div>
</div>
<script> 
    $(document).ready(function () {
    $("input[data-attr='date1']").datetimepicker({ format: 'MMM-DD-YYYY' });
 
});
</script>