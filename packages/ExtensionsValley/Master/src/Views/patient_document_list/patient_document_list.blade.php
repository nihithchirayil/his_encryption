@extends('Dashboard::dashboard.dashboard')
@section('content-header')

@include('Dashboard::dashboard.partials.headersidebar')
@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-list.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">

@endsection
@section('content-area')


<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="token" value="{{ csrf_token() }}">
<input type="hidden" id="hidden_patient_id" value="0">

<div class="modal fade" id="getpatientdocument" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getpatientdocumentHeader">Patient Document List</h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
                <div class="row padding_sm">
                    <div class="col-md-12 padding_sm">
                        <input type="hidden" id="hidden_document_id" value="">
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label>Patient Name / UHID</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control hidden_search" id="docpatient_name"
                                    autocomplete="off" value="">
                                <div id="docpatient_nameAjaxDiv"
                                    style="margin-left:-5px;position: absolute !important;margin-top:22px"
                                    class="ajaxSearchBox">
                                </div>
                                <input value="" type="hidden" name="docpatient_name_hidden" id="docpatient_name_hidden">
                            </div>
                        </div>

                        <div class="col-md-9 padding_sm">
                            <form id="upload_file">
                                <div id="file_uploaddiv">
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="">Document Type</label>
                                            <div class="clearfix"></div>
                                            <select class="form-control" name="document_type" id="document_type">
                                                <option value="">Select</option>
                                                @foreach ($document_type as $each)
                                                <option value="{{ $each->id }}">{{ $each->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @php $i = 1; @endphp
                                    <div class="col-md-7 padding_sm">
                                        @php $f = $i - 1; @endphp
                                        <input type="hidden" id="profile_base64image">
                                        <div class="mate-input-box">
                                            <label class="">File<span class="uploadedfile blue"
                                                    style="margin-left: 64px;"></span>
                                            </label>
                                            <div class="clearfix"></div>
                                            <div class="fileUpload" style="margin-top: 7px;">
                                                <input type="file" class="btn-warning" name="file_upload[]"
                                                    id="file_upload">
                                                <span style="color: #d14;">
                                                    @if ($errors->first('file_upload.' .
                                                    $f) != '') {{ 'Upload only doc,pdf,jpeg,jpg,png and File
                                                    size < 10MB' }} @endif </span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-1 padding_sm" style="margin-bottom:10px;">
                                        <label for="">&nbsp;</label>
                                        <div class="clearfix"></div>
                                        <input type="submit" value="Upload" class="btn btn-block btn-success saveButton"
                                            id="saveButton">
                                    </div>
                                    <div class="col-md-1 padding_sm" style="margin-bottom:10px;">
                                        <label for="">&nbsp;</label>
                                        <div class="clearfix"></div>
                                        <input type="reset" class="btn btn-block btn-warning restform"
                                            onclick="resetdocument();" id="resetform">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm" id="getpatientdocumentData">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div id="view_patient_document_modal" class="modal fade " role="dialog" style="z-index: 10025;">
    <div class="modal-dialog" style="width:93%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="viewfilename"></h4>
            </div>
            <div class="modal-body" id="document_data" style="height:550px">

                <iframe src="" width="100%" height="100%" id="myframe"></iframe>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="right_col">
    <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 no-padding">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-1 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" id="document_from_date"
                                    autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepicker" id="document_to_date"
                                    autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label>Patient Name / UHID</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control hidden_search" id="searchpatient_name"
                                    autocomplete="off">
                                <div id="searchpatient_nameAjaxDiv" style="margin-top:-15px !important"
                                    class="ajaxSearchBox"></div>
                                <input class="filters reset" value="" type="hidden" name="searchpatient_name_hidden"
                                    value="" id="searchpatient_name_hidden">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                            <div class="mate-input-box">
                                <label for="">Document Type</label>
                                <div class="clearfix"></div>
                                <select class="form-control" name="patient_document_type" id="patient_document_type">
                                    <option value="">Select</option>
                                    @foreach ($document_type as $each)
                                    <option value="{{ $each->id }}">{{ $each->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-1 padding_sm" style="margin-top: 25px">
                            <button type="button" class="btn btn-warning btn-block"
                                onclick="resetDocPatientSearch()">Reset <i class="fa fa-recycle"></i>
                            </button>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 25px">
                            <button type="button" id="getPatientListBtn" onclick="getPatientList()"
                                class="btn btn-primary btn-block">Search <i id="getPatientListSpin"
                                    class="fa fa-search"></i>
                            </button>
                        </div>
                        <div class="col-md-1 padding_sm pull-right" style="margin-top: 25px">
                            <button type="button" class="btn btn-success btn-block" onclick="addDocPatient()">ADD
                                &nbsp;<i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="box no-border no-margin">
            <div class="box-body clearfix">
                <div class="listPatientDataDivs theadscroll table_body_contents" id="listPatientDataDivs">

                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/highcharts/highcharts.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/patient_document_list.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js') }}"></script>
@endsection
