<style>
    .patient_doc_cl {
        background: #e79142 !important;
    }

    .patient_doc_cl>a {
        background: #e79142 !important;
    }
</style>
<div class="nav-tabs-custom blue_theme_tab no-margin">
    <div class="col-md-3 padding_sm">
        <div class="theadscroll patientwisediv" style="height: 300px;" id="patientwisediv_contaner">
            <ul class="vertical_tab_btn investigation_fav_tabs_list text-center" style="border:none !important;">
                <li id="patientlevelvisitdata"
                    style="height: 30px;cursor: pointer;background: #1a554bc4;margin-left:-34px;font-weight: 600;color: #fff;margin-bottom: 6px;"
                    class="document_btn_cl" onclick="patient_level(this);" visit_id='0'>
                    <a href="#visit" data-toggle="tab" class="visit_level" aria-expanded="true"
                        style="width:100%;color: #fff !important;font-weight: 600;text-align: center;height: 100% !important;">
                        Patient Level
                    </a>
                </li>
                @if (sizeof($visit_patient) > 0)
                @foreach ($visit_patient as $doc)
                @php
                if($doc->id == $doc->current_visit_id){
                $color = "background:#3dafd4;";
                }else{
                $color = "background: #1a554bc4";
                }
                @endphp
                <li style="height: 30px;cursor: pointer;{{ $color }};margin-left:-34px; font-weight: 600; color: #fff; margin-top: 5px;"
                    class="document_btn_cl" visit_level='visit_level' onclick="patient_level(this,{{ $doc->id }});"
                    visit_id="{{ $doc->id }}">

                    <a href="#visit" data-toggle="tab" class="visit_level" aria-expanded="true"
                        style="width:100%;color: #fff !important;font-weight: 600;text-align: center;height: 100% !important;"
                        visit_id="{{ $doc->id }}">
                        {{ date('M-d-Y',strtotime($doc->visit_date)) }}
                    </a>

                </li>
                @endforeach
                @endif
            </ul>
        </div>
    </div>
    <div class="col-md-9 padding_sm">
        <div class="theadscroll" id="add_patient_documentdiv" style="height: 300px;">
        </div>
    </div>
    <div class="clearfix"></div>
</div>
