@php
$i = 1;
@endphp
<table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
    style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_bg">
            <th>Sl.No.</th>
            <th>Document Type</th>
            <th>File Name</th>
            <th>Preview</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
       $upload_path = public_path() . '/packages/extensionsvalley/emr/uploads/';
                    if (count($patient_document) != 0) {
                       foreach ($patient_document as $list) {
                            ?>
        <tr style="cursor: pointer;">
            <td class="common_td_rules">{{ $i++ }}</td>
            <td class="common_td_rules">
                {{ $list->name ? $list->name : '-' }}</td>
            <td class="common_td_rules">{{ $list->filename ? $list->filename : '-' }}</td>
            <td>
                @php
                $filePath = $upload_path . "/" . $list->patient_id . "/" . $list->filename;
                @endphp
                @if (!file_exists($filePath))
                --
                @else
                <button type="button" id="viewdocumentfileBtn{{  $list->id }}" class=""
                    style="background: royalblue;border: aliceblue;color: white;"
                    onclick="viewfile(this,'{{ $list->id }}','{{ $list->mime_type }}','{{ $list->file_absolute_path }}','{{ $list->filename }}')"><i
                        id="viewdocumentfileSpin{{  $list->id }}" class="fa fa-eye"></i>
                </button>
                @endif
            </td>
            <td>
                <button type="button" class="document_id" style="color: white;background: chocolate;border: black;"
                    onclick="editdocument(this,'{{ $list->id }}','{{ $list->document_type }}','{{ $list->file_absolute_path }}')"><i
                        class="fa fa-edit"></i>
                </button>
            </td>
            <td>
                <button type="button" class="" style="color: white;background: #cc3014;;border: black;"
                    document_id="{{ $list->id }}"
                    onclick="deletedocument(this,'{{ $list->id }}','{{ $list->visit_id }}')"><i class="fa fa-trash"></i>
                </button>
            </td>
        </tr>

        <?php
                    }
                        } else {
                ?>
        <tr>
            <td style="text-align: center" colspan="11" class="re-records-found">No Records Found</td>
        </tr>
        <?php
                    }
                ?>
    </tbody>
</table>
