<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if(url && url!='undefined'){
            var from_date = $("#document_from_date").val();
            var to_date = $("#document_to_date").val();
            var patient_id = $("#searchpatient_name_hidden").val();
            var param = {
                patient_id: patient_id,
                from_date: from_date,
                to_date: to_date
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("#getPatientListBtn").attr("disabled", true);
                    $("#getPatientListSpin").removeClass("fa fa-search");
                    $("#getPatientListSpin").addClass("fa fa-spinner fa-spin");
                },
                success: function (res) {
                    $("#listPatientDataDivs").html(res);
                    $(".theadscroll").perfectScrollbar("update");
                },
                complete: function () {
                    $("#getPatientListBtn").attr("disabled", false);
                    $("#getPatientListSpin").removeClass("fa fa-spinner fa-spin");
                    $("#getPatientListSpin").addClass("fa fa-search");
                }
            });
        }
            return false;
        });

    });
</script>
<div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; height: 430px;">
        <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th width="20%">UHID</th>
                    <th width="20%">Patient Name</th>
                    <th width="14%">Gender</th>
                    <th width="20%">Email</th>
                    <th width="15%">Mobile No.</th>
                    <th width="10%">DOB</th>
                    <th width="3%"><i class="fa fa-edit"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php
            if (count($patient_list) != 0) {
                foreach ($patient_list as $list) {
                    ?>
                <tr>
                    <td class="common_td_rules" style="text-align: left;" title="{{ $list->uhid ? $list->uhid : '-' }}">
                        {{ $list->uhid ? $list->uhid : '-' }}</td>
                    <td class="common_td_rules" id="patientName{{$list->patient_id}}" style="text-align: left;">{{
                        $list->patient_name ? $list->patient_name :
                        '-'}}</td>
                    <td class="common_td_rules" style="text-align: left;">{{ $list->gender ? $list->gender : '-' }}</td>
                    <td class="common_td_rules" style="text-align: left;">{{ $list->email ? $list->email : '-' }}</td>
                    <td class="common_td_rules" style="text-align: left;">{{ $list->phone ? $list->phone : '-' }}</td>
                    <td class="common_td_rules" style="text-align: left;">{{ $list->dob ? $list->dob : '-' }}</td>
                    <td><button type="button" class="btn btn-warning" id="editPatientDocumentBtn{{$list->patient_id}}"
                            onclick="editPatientDocument({{$list->patient_id}})"><i
                                id="editPatientDocumentSpin{{$list->patient_id}}" class="fa fa-edit"></i></button></td>
                </tr>
                <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="7" class="re-records-found">No Records Found</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
