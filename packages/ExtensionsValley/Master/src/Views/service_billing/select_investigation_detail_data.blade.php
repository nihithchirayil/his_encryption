@php
    $i = 1;
@endphp
@isset($select_investigation_head_data)
@foreach ($select_investigation_head_data as $item)
    <tr style="width:100%" data-id="{{ $item->id }}" data-head_id="{{$item->head_id}}" data-item_name="{{$item->service_disc}}" data-investigation-type="{{$item->type}}" >
        <td class="td_common_numeric_rules" title="{{ $i }}">{{ $i }}</td>
        <td class="common_td_rules">{{ $item->service_disc }}</td>
        <td class="common_td_rules">{{ $item->dept_name }}</td>
        <td class="common_td_rules">{{ $item->sub_dept_name }}</td>
        <td class="common_td_rules">{{ $item->qty }}</td>
        <td class="common_td_rules">{{ $item->rem_qty }}</td>
        <td class="common_td_rules">{{ date('M-d-Y h:i A', strtotime($item->created_at))}}</td>
        <td class="common_td_rules">
            @if((int)$item->billed_qty == 0)
            <button id="replace_button" onclick="replaceItems(this);"><i class="fa fa-exchange"></i> </button>
            @endif
        </td>
        {{-- <td class="common_td_rules">
            <button onclick="cancel_item({{$item->id}},{{$item->head_id}}, '{{$item->type}}')"><i class="fa fa-trash"></i></button>
        </td> --}}
    </tr>
    @php
        $i++;
    @endphp
@endforeach
@endisset