@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-slide.css') }}" rel="stylesheet">

    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/common-custom.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet"> -->
    <style>
        .ajaxSearchBox {
            display:none;
            text-align: left;
            list-style: none;
            cursor: pointer;
            max-height: 350px;
            margin: 3px 0px 0px 0px;
            overflow-y: auto;
            width: 150px;
            z-index: 599;
            position:absolute;
            background: #ffffff;
            border-radius: 3px;
            border: 1px solid rgba(0, 0, 0, 0.3);

        }
        .ajaxSearchBox>li{
            padding:2px 2px 5px 5px;
            font-size: 10px !important;
            font-weight: 400 !important;
            font-family: "sans-serif";
            border-bottom: 1px solid #3e75a5;
        }

        .liHover{
            background: #3e75a5 !important;
            color:white;
            padding:2px 2px 5px 5px;
            font-size: 10px !important;
            font-weight: 400;
            font-family: "sans-serif";
            border-bottom: 1px solid white;
        }
        .search_header {
            background: #36A693 !important;
            color: #FFF !important;
        }
        .box_header {
            background: #3b926a !important;
            /* background: #5397b1 !important; */
            color: #FFF !important;
        }
        .mate-input-box{
            width: 100%;
            position: relative;
            padding: 15px 4px 4px 4px !important;
            border-bottom: 1px solid #01A881;
            box-shadow: 0 0 3px #CCC;
        }
        .mate-input-box label{
            position: absolute;
            top: -2px;
            left: 6px;
            font-size: 12px;
            font-weight: 700;
            color: #107a8c;
            padding-top: 2px;
        }

        .patient_details_table>tbody>tr>td{
            border: 1px solid #CCC !important;
        }

        .list-medicine-search-data-row-listing{
            text-align:left;
        }

        .alerts-border {
            border: 1px #ff0000 solid;
            animation: blink 1s;
            animation-iteration-count: 3;
        }

        @keyframes blink { 50% { border-color:#fff ; }  }

        .number_class{
            text-align:right;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            padding: 2px;
            white-space: nowrap;
        }

        .pagination{
            margin: 10px 0 0 0 !important;
        }

        .service-list-div-row-listing{
            width: 70%;
            top: 35%;
            left: 28%;
            z-index: 9999;
            position: absolute;
            background: white;
        }
        .active_item{
            background: #337ab7 !important;
            color: white;
            cursor: pointer;
        }
        .service-list-div-row-listing tbody tr:hover{
            background: #337ab7 !important;
            color: white;
            cursor: pointer;
        }
        .ajaxSearchBox{
            z-index: 1016 !important;
        }

        .required_field:after{
            color: red;
            content: "*";
            margin-left: 2px;
        }
        .select2-chosen, .select2-choice > span:first-child, .select2-container .select2-choices .select2-search-field input{
            padding: 2px 12px;
        }
        .select2-container .select2-choice{
            height: 22px;
            line-height: 1.42857;
        }

        .select2-arrow{
            top: -4px !important;
        }

        .modal .select2-container{
            z-index: 1052 !important;
        }
    </style>
@endsection
@section('content-area')
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="patient_id" value="{{$patient_id}}">
    <input type="hidden" id="visit_id" value="">
    <input type="hidden" id="room_type" value="">
    <input type="hidden" id="bill_id" value="{{$bill_id}}">
    <input type="hidden" id="bill_no" value="">
    <input type="hidden" id="get_uhid" value="{{$uhid}}">
    <input type="hidden" id="external_doctor_id" value="{{$external_doctor_id}}">
    <input type="hidden" id="package_bill_tag_id" value="{{$package_bill_tag_id}}">
    <input type="hidden" id="pending_bill_show_config" value="{{$pending_bill_show_config}}">
    <input type="hidden" id="enable_edit_paid_bills" value="{{$enable_edit_paid_bills}}">
    <input type="hidden" id="discharge_bill_head_id" value="{{$discharge_bill_head_id}}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">

    <!-- page content -->
    <div class="right_col" role="main" style="background:white">
        <div class=" col-md-12  padding_sm">
            <div class="box no-border" style="margin-bottom: 10px;">
                <div class="box-header table_header_common">
                    <span class="padding_sm">{{$title}} <span class="service_bill_no"></span></span>
                </div>
                <div class="box-body clearfix">
                    <div class=" col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="" class="required_field">UHID</label>
                            <div class="clearfix"></div>
                            <input class="form-control hidden_search filters" value="" autocomplete="off" type="text" id="op_no" name="op_no" style="width:80%" />
                            <div id="op_noAjaxDiv" class="ajaxSearchBox" style="width: 300px !important; margin-top: 17px; max-height: 505px !important;"></div>
                            <input class="filters" value="" type="hidden" name="op_no_hidden" id="op_no_hidden">
                            <button type="button" class="btn btn-sm btn-primary advanceSearchBtn" style=" position: absolute; top: 15px; right: 0;"><i class="fa fa-search"></i></button>
                        </div>
                        <div class="mate-input-box">
                            <label for="" class="required_field">Bill Tag</label>
                            <div class="clearfix"></div>
                            {!! Form::select('bill_tag',$bill_tag, null,['class' => 'form-control','placeholder' => 'Bill Tag','title' => 'Bill Tag','id' => 'bill_tag', 'style' => 'color:#555555; padding:2px 12px; ']) !!}
                        </div>

                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box" style="height: 43px;">
                            <label for="">Outside Patient</label>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm">
                                <input type="checkbox" autocomplete="off" class="padding_sm" id="out_side_patient" name="out_side_patient" value="" required="">
                            </div>
                        </div>
                        <div class="mate-input-box" style="height: 43px;">
                            <label for="">Outside Doctor</label>
                            <div class="clearfix"></div>
                            <div class="col-md-1 padding_sm">
                                <input id="out_side_doctor" type="checkbox" onclick="" class="" name="outSideregistration">
                            </div>
                            <div class="col-md-11 padding_sm">
                                <input type="text" autocomplete="off" class="form-control" readonly="readonly" id="outside_doctor_name" name="docFromoutside" value="" required="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm">
                        <table class="table table_sm patient_details_table  no-margin no-border">
                            <tbody>
                                <tr>
                                    <td><b>Patient Name</b></td>
                                    <td style="width:40%;"><span id="patient_name_bill"></span></td>
                                    <td><b>Doctor</b></td>
                                    <td style="max-width: 140px !important;">
                                    {!! Form::select('consulting_doctor',$doctor_list, null,['class' => 'form-control select2','placeholder' => 'Doctor','title' => 'Doctor','id' => 'consulting_doctor','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b> IP No </b></td>
                                    <td><span id="ip_number" ></span></td>
                                    <td><b> Sponsor </b></td>
                                    <td>
                                        {!! Form::select('company',$company_list, null,['class' => 'form-control','placeholder' => 'Sponser','title' => 'Sponser','id' => 'company', 'style' => 'color:#555555; padding:2px 12px; ', 'readonly'=>'readonly', 'disabled'=>'true']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b> Room </b></td>
                                    <td><span id="current_room" ></span></td>
                                    <td><b> Pricing </b></td>
                                    <td>
                                    {!! Form::select('pricing',$pricing_list, null,['class' => 'form-control','placeholder' => 'Pricing','title' => 'Pricing','id' => 'pricing', 'style' => 'color:#555555; padding:2px 12px; ', 'readonly'=>'readonly', 'disabled'=>'true']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b> Billing Location </b></td>
                                    <td>
                                        {!! Form::select('billing_location',$location_list, null,['class' => 'form-control','placeholder' => 'Billing Location','title' => 'Billing Location','id' => 'billing_location', 'style' => 'color:#555555; padding:2px 12px; ', 'readonly'=>'readonly']) !!}
                                    </td>
                                    <td><b> Payment Type </b></td>
                                    <td>
                                        <select class= "form-control" id='payment_type' >
                                            <option value="">Payment Type</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>


                    <div class="col-md-2 padding_sm" >
                        <table class="table table_sm  no-margin no-border">
                            <tbody>
                                <tr>
                                    <td><button style="text-align: left; margin-bottom: 3px;" id="pending_bills_btn" type="button" class="btn btn-sm btn-block btn-primary"> <i class="fa fa-clock-o" aria-hidden="true"></i> Pending Bills </button></td>
                                </tr>
                                <tr>
                                    <td><button style="text-align: left; margin-bottom: 3px;" id="select_investigation_btn" type="button" class="btn btn-sm btn-block btn-primary"> <i class="fa fa-flask" aria-hidden="true"></i> Select Investigation </button></td>
                                </tr>
                                <tr>
                                    <td><button style="text-align: left; margin-bottom: 3px;" id="select_package_btn" type="button" class="btn btn-sm btn-block btn-primary" disabled> <i class="fa fa-briefcase" aria-hidden="true"></i> Select Package </button></td>
                                </tr>
                                <tr>
                                    <td><button style="text-align: left; margin-bottom: 3px;" id="service_bill_list_btn" type="button" class="btn btn-sm btn-block btn-primary"> <i class="fa fa-list" aria-hidden="true"></i> Bill List </button></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>
        </div>

        <div class="col-md-12 padding_sm selected_package_head_div" style="display:none;">
            <div >Package : <span class="selected_package_head_name" style="font-size: 16px;color: #3e75a5;font-weight: 600;"></span></div>
        </div>

        <div class="col-md-12 padding_sm">
            <div class="theadscroll" style="position: relative; height: 295px; border: 1px solid #dcdcdc; background:white;">
                <table class="table no-margin table-striped theadfix_wrapper table-col-bordered table_sm center_align table-condensed " style="position:absolute;">
                    <thead>
                        <tr class="table_header_common">
                            <th width="2%">#</th>
                            <th width="69%">Item Description</th>
                            <th width="8%">Price</th>
                            <th width="8%">Quantity</th>
                            <th width="8%">Total Amount</th>
                            <th width="5%"><button type="button" class="btn btn-sm btn-success addNewRowBtn" style="padding: 0px 5px; margin: 0;"><i class="fa fa-plus"></i> </button></th>
                        </tr>
                    </thead>
                    <tbody style="min-height: 350px;" class="service_billing_table_body">
                    </tbody>
                </table>
            </div>
        </div>


        <!-- Service Item search list div start -->
        <div class="service-list-div-row-listing" style="display: none;">
            <a style="float: left;" class="close_btn_service_search">X</a>
            <table  class="table table-bordered no-margin table_sm table-striped service_list_theadfix_wrapper">
                <thead>
                    <tr class="table_header_common">
                        <th>Service Description</th>
                        <th>Department</th>
                        <th>Sub Department</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody class="list-service-search-data-row-listing">
                </tbody>
            </table>
        </div>
        <!-- Service Item search list div end -->

        <div class="col-md-12 padding_sm" style="margin-top: 5px; padding: 0px !important;">

            <div class="col-md-3 padding_sm" >
                <div class="box-header table_header_common">
                    <span class="padding_sm">Bill Discount</span>
                </div>
                <div class="box-body " style="min-height:111px;">
                    <div class=" col-md-6" style="margin-top:8px; padding-left: 2px !important; padding-right: 2px !important;">
                        <div class="mate-input-box">
                            <label for="discount_type">Discount Type</label>
                            <div class="clearfix"></div>
                            <select id="discount_type" class="form-control">
                                <option value="">Discount Type</option>
                                <option value="1">Percentage(%)</option>
                                <option value="2">Amount</option>
                            </select>
                        </div>
                    </div>
                    <div class=" col-md-6" style="margin-top:8px; padding-left: 2px !important; padding-right: 2px !important;">
                        <div class="mate-input-box">
                            <label for="discount_type">Discount</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control number_class discount_value" value="" />
                        </div>
                    </div>
                    <div class="col-md-12" style="padding-left: 2px !important; padding-right: 2px !important;">
                        <div class="mate-input-box">
                            <label for="discount_narration">Discount Narration</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control discount_narration" id="discount_narration" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 padding_sm" >
                <div class="box-header table_header_common">
                    <span class="padding_sm">Amount Details</span>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped table_sm"  style="margin:0px;">
                        <tbody>
                            <tr>
                                <td style="width: 20%; ">Outstanding Amount</td>
                                <td style="width: 30%; ">
                                    <input class="form-control outstanding_amount number_class " readonly disabled type="text" value="">
                                </td>
                                <td style="width: 20%; ">Total Discount</td>
                                <td style="width: 30%; ">
                                    <input class="form-control total_discount number_class" readonly disabled type="text" value="">
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 20%; ">Advance</td>
                                <td style="width: 30%; ">
                                    <input class="form-control patient_advance number_class" readonly disabled type="text" value="">
                                </td>
                                <td style="width: 20%; ">Balance In Adv.</td>
                                <td style="width: 30%; ">
                                    <input class="form-control balance_in_advance number_class" readonly disabled type="text" value="">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%; ">Patient Payable</td>
                                <td style="width: 30%; ">
                                    <input class="form-control patient_payable number_class" readonly disabled type="text" value="">
                                </td>
                                <td style="width: 20%; ">Payor Payable</td>
                                <td style="width: 30%; ">
                                    <input class="form-control payor_payable number_class" readonly disabled type="text" value="">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%; ">Bill Amount</td>
                                <td style="width: 30%; ">
                                    <input class="form-control bill_amount number_class" readonly disabled type="text" value="">
                                </td>
                                <td style="width: 20%; ">Net Amount</td>
                                <td style="width: 30%; ">
                                    <input class="form-control net_amount number_class" readonly disabled type="text" value="" style="font-size:20px !important; color: #5397b1;">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-2 padding_sm">
                <div class="col-md-8 padding_sm">
                    <button type="button" class="btn btn-block btn-success saveServiceBillBtn" onclick="saveServiceBill();"> <i class="fa fa-save" aria-hidden="true"></i> Save </button>
                </div>
                <div class="col-md-8 padding_sm">
                    <button type="button" class="btn btn-block btn-primary" onclick="resetServiceBill();"> <i class="fa fa-refresh" aria-hidden="true"></i> Reset </button>
                </div>
            </div>

        </div>

    </div>

    <!-------print modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Service Bill</h4>
                    <button type="button" class="close print_config_close_btn" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:190px;">
                    <div class="col-md-12">
                        <p> Bill saved successfully. </p>
                        <p> Bill No : <strong class="save_message_bill_no"></strong> </p>
                    </div>

                    <div class="col-md-12">
                        <hr/>

                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin: 10px 0px;display:none" id="detail_summary_print">
                        <span style="margin-right: 5%;">Print Type:</span>
                        @if ($details_flag_enabled_in_print_pb==1)
                          @php
                                $chk1='checked';
                                $chk2='';
                          @endphp
                        @else
                        @php
                        $chk1='';
                        $chk2='checked';
                         @endphp
                        @endif
                        <input style="margin-right: 1%;" type="radio" name="detail_setup" id="detail_print" value="1" {{ $chk1 }}>Detail
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="detail_setup" id="summary_print" value="2" {{ $chk2 }}>Summary
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="include_hospital_header" id="include_hospital_header" checked>
                        Include Hospital Header
                    </div>
                </div>
                <div class="modal-footer">
                    <button onclick="printBillDetails();" class="btn bg-primary pull-right" style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print Bill
                    </button>
                    <!-- <button onclick="payBill();" class="btn bg-primary pull-right pay_now_button" style="color:white; display:none;">
                        <i class="fa fa-print" aria-hidden="true"></i> Pay Now
                    </button> -->
                    <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-default print_config_close_btn">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!------- package details modal---------------->
    <div class="modal" role="dialog" id="package_list_modal">
        <div class="modal-dialog  modal-lg" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header table_header_common">
                    <h4 class="modal-title" style="color: white;">Package Details</h4>
                    <button type="button" class="close package_modal_close_btn" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height: 500px;">
                    <div class="col-md-12 no-padding">
                        <div class="col-md-7 padding_sm" style="margin-top: 10px;">
                            <input type="text" class="form-control searchPakacgeListItem" style="width:50%;" placeholder="search.."  />
                        </div>
                        <div class="col-md-5  padding_sm" style="margin-top: 10px;">
                            <input type="text" class="form-control searchPakacgeDetailItem" style="width:75%;" placeholder="search.." />
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="col-md-7 padding_sm packageListDiv" style="margin-top: 10px;">

                        </div>
                        <div class="col-md-5  padding_sm packageDetailsDiv" style="margin-top: 10px;">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-success apply_package_btn">Apply</button>
                </div>
            </div>
        </div>
    </div>

    <!-------select investigation modal---------------->
    <div class="modal" role="dialog" id="select_investigation_modal">
        <div class="modal-dialog  modal-lg" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header table_header_common">
                    <h4 class="modal-title" style="color: white;">Select Investigation</h4>
                    <button type="button" class="close include_exclude_modal_close_btn" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height: 630px;">
                    <div class=" col-md-12 padding_sm">
                        <div class="box no-border" style="margin-bottom: 10px;">
                            <div class="clearfix">
                                <div class="col-md-12 no-padding">
                                    <div class="col-md-3 box-body no-padding" style="height: 565px;">
                                        <div class=" table_header_common">
                                            <span class="padding_sm">Investigation Details</span>
                                        </div>
                                        <div class="col-md-12 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="search_by_date">Search By Date</label>
                                                <div class="clearfix"></div>
                                                <input type="checkbox" autocomplete="off" class="padding_sm" id="search_by_date" name="search_by_date" checked="checked" value="" required>
                                            </div>
                                        </div>

                                        <div class="col-md-6 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">From Date</label>
                                                <div class="clearfix"></div>
                                                <input type="text" data-attr="date" class="form-control datepicker from_date_invest" name="from_date" value="">
                                            </div>
                                        </div>

                                        <div class="col-md-6 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">To Date</label>
                                                <div class="clearfix"></div>
                                                <input type="text" data-attr="date" class="form-control datepicker to_date_invest" name="to_date" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-12 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">UHID</label>
                                                <div class="clearfix"></div>
                                                <input class="form-control hidden_search filters" value="" autocomplete="off" type="text" id="patient_uhid" name="patient_uhid" style="width:80%" />
                                                <div id="patient_uhidAjaxDiv" class="ajaxSearchBox" style="width:295px;"></div>
                                                <input class="filters" value="" type="hidden" name="patient_uhid_hidden" id="patient_uhid_hidden">
                                                <button type="button" class="btn btn-sm btn-primary advanceSearchBtn" style=" position: absolute; top: 15px; right: 0;"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                        <div class="col-md-12 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Patient Name</label>
                                                <div class="clearfix"></div>
                                                <input type="text" autocomplete="off" class="form-control"  readonly="readonly" id="patient_name_invest" name="patient_name_invest" value="" required>
                                            </div>
                                        </div>

                                        <div class="col-md-12 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="doctor">Doctor</label>
                                                <div class="clearfix"></div>
                                                {!! Form::select('doctor_invest',$doctor_list, null,['class' => 'form-control select2','placeholder' => 'Doctor','title' => 'Doctor','id' => 'doctor_invest','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                            </div>
                                        </div>

                                        <div class="col-md-12 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Investigation Type</label>
                                                <div class="clearfix"></div>
                                                <input type="text" autocomplete="off" class="form-control"  id="investigation_type_text" name="investigation_type_text" value="" required>
                                            </div>
                                        </div>

                                        <div class="col-md-6 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="doctor">Visit Type</label>
                                                <div class="clearfix"></div>
                                                {!! Form::select('visit_type_invest',array("OP"=>"OP", "IP"=>"IP"), null,['class' => 'form-control','placeholder' => 'Visit Type','title' => 'Visit Type','id' => 'visit_type_invest','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                            </div>
                                        </div>

                                        <div class="col-md-6 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="doctor">Status</label>
                                                <div class="clearfix"></div>
                                                {!! Form::select('status_invest',array("1"=>"Pending", "2"=>"Full Converted", "3"=>"Partial Converted"), null,['class' => 'form-control','placeholder' => 'Status','title' => 'Status', 'readonly'=>'readonly', 'disabled'=>'disabled', 'id' => 'status_invest','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Pending/Partial Converted</label>
                                                <div class="clearfix"></div>
                                                <input type="checkbox" autocomplete="off" class="padding_sm" id="pending_or_partial" name="pending_or_partial" checked="checked" value="" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-md-offset-6 padding_sm">
                                            <button type="button" class="btn btn-block btn-primary" onclick="fetchIndentList();"> <i class="fa fa-search" aria-hidden="true"></i> Search </button>
                                        </div>

                                    </div>

                                    <div class="col-md-9 box-body padding_sm">
                                        <div class="col-md-12 no-padding" style="text-align: right;">
                                            <button type="button" class="btn btn-sm btn-primary" onclick="convertToBill();"> <i class="fa fa-list" aria-hidden="true"></i> Convert To Bill </button>
                                        </div>
                                        <div class="col-md-12 no-padding select_investigation_head_data" >
                                            <div class="theadscroll always-visible" style="position: relative; height: 254px;">
                                                <table class="table theadfix_wrapper no-margin table-striped table-bordered table-condensed">
                                                    <thead class=" table_header_common">
                                                        <tr>
                                                            <th>Sl.No.</th>
                                                            <th>Action</th>
                                                            <th>UHID</th>
                                                            <th>Patient Name</th>
                                                            <th>Type</th>
                                                            <th>Doctor</th>
                                                            <th>Visit Type</th>
                                                            <th>Created Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody >
                                                        <tr>
                                                            <td colspan="8">No records found..!</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-9 box-body padding_sm" style="margin-top:10px;">
                                        <div class="col-md-12 no-padding">
                                            <div class="theadscroll always-visible" style="position: relative; height: 244px;">
                                                <table class="table theadfix_wrapper no-margin table-striped table-bordered table-condensed">
                                                    <thead class=" table_header_common">
                                                        <tr>
                                                            <th class="text-center">#</th>
                                                            <th class="text-center">Service Description</th>
                                                            <th class="text-center">Department</th>
                                                            <th class="text-center">Subdepartment</th>
                                                            <th class="text-center">Qty</th>
                                                            <th class="text-center">Rem Qty</th>
                                                            <th class="text-center">Created At</th>
                                                            <th class="text-center">Replace</th>
                                                            <!-- <th class="text-center">Cancel</th> -->
                                                        </tr>
                                                    </thead>
                                                    <tbody id="select_investigation_detail_data">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!------- substitute modal ---------------->
    <div class="modal" tabindex="-1" role="dialog" id="substitute_item_modal" >
        <div class="modal-dialog" role="document" style="width:45%;">
            <div class="modal-content">
                <div class="modal-header table_header_common" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Substitute Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 10px; top: 15px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height: 275px;">
                    <div class="col-md-12 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Item Description</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control"  readonly="readonly" id="sub_item_desc" name="sub_item_desc" value="" required>
                        </div>
                    </div>

                    <div class="col-md-12 no-padding">
                        <div class="col-md-12 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Substitute Item</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control"  id="substitute_item_desc" name="substitute_item_desc" value="" required>
                                <div id="substitute_item_descAjaxDiv" class="ajaxSearchBox" style="width:295px;"></div>
                                <input class="filters" value="" type="hidden" name="substitute_item_desc_hidden" id="substitute_item_desc_hidden">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-success substitute_item_btn">Replace</button>
                </div>
            </div>
        </div>
    </div>



    <!-- Custom Modals -->
    @include('Master::RegistrationRenewal.advancePatientSearch')
    @include('Master::pharmacy_billing.outside_patient_registration')
    @include('Master::pharmacy_billing.pending_bills_list')
    @include('Emr::pharmacy_bill_list.cash_receive_model')

@stop
@section('javascript_extra')


    <script src="{{ asset('packages/extensionsvalley/jquery-mousewheel-master/jquery.mousewheel.min.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/service_billing.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"> </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/cash_receive.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>


@endsection
