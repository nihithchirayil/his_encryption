      
<div class="theadscroll always-visible" style="position: relative; height:400px;">
    <table class="table theadfix_wrapper no-margin table-striped table-bordered table-condensed">
        <thead class="table_header_common">
            <tr>
                <th>Sl.No.</th>
                <th>Package Name</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody class="package_detail_table_body">
                @if (isset($package_details))
                @foreach($package_details as $each)
                <tr class="search_package_detail_class" data-service-code="{{$each->service_code}}" data-service-desc="{{$each->service_desc}}" data-package-amount="{{$each->package_amount}}">
                    <td style="width:5%" title="{{$loop->iteration}}" class="common_td_rules">{{$loop->iteration}}</td>
                    <td title="{{$each->service_desc}}" class="common_td_rules">{{$each->service_desc}}</td>
                    <td style="width:15%" title="{{$each->package_amount}}" class="number_class">{{$each->package_amount}}</td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="10">No Data Found..!</td>
                </tr>
                @endif
        </tbody>
    </table>
</div>