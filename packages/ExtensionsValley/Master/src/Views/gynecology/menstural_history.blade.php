<div class="col-md-12"><hr></hr></div>
<div class="col-md-12 text-center" style="display: flex;">
    <h4 style="width: 90%; margin: 0px;"> MENSTURAL HISTORY </h4>
    <i class="tiny_block_toggle fa fa-window-minimize pull-right" data-toggle="collapse" href=".menstural_history_container" ></i>
</div>
<div class="col-md-12 menstural_history_container" style="margin-top:20px;">
    <table class="table no-margin table-bordered table-condensed" style="border: 1px solid #CCC;">
        <thead>
            <tr class="headder_bg">
                <th>LMP Date</th>
                <th>Regular/Irregular</th>
                <th>Duration Of Cycle</th>
            </tr>
        </thead>
        <tbody class="menstural_history_table_body">
            <tr>
                <td>
                    <input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="" class="form-control datepicker table_text menstural_lmp_date" placeholder="MMM-DD-YYYY" id="" style="" required onblur="addNewMensturalHistroyRow()">
                </td>

                <td>
                    {!! Form::select('gender', $menstural_types,'0', ['class' => 'form-control table_text menstural_type', 'placeholder' => 'Select Type', 'id' => 'menstural_types', 'style' => 'font-weight:700;border:none;']) !!}
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text menstural_duration_of_cycle" placeholder="" id="" style="">
                </td>
            </tr>
        </tbody>
    </table>
</div>
