<div class="theadscroll" style="position: relative; height: 300px;">
    <table class="table no-margin theadfix_wrapper table_sm table-bordered  table-condensed uss_history_table">
        <thead>
            <tr class="" style="background: aliceblue; color: #000000; ">
                <th>Sl.No</th>
                <th>Request No.</th>
                <th>Doctor</th>
                <th>Investigation</th>
                <th>Type</th>
                <th>Remarks</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @if(sizeof($uss_details)>0)
        @foreach ($uss_details as $uss)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>@if($uss->batch_no != '') {{$uss->batch_no}} @else 0 @endif</td>
                <td>@if(isset($uss->doctor_name)) {{$uss->doctor_name}} @else - @endif</td>
                <td>@if(isset($uss->service_text)) {{$uss->service_text}} @else - @endif</td>
                <td>@if(isset($uss->investigation_type)) {{$uss->investigation_type}} @else - @endif</td>
                <td>@if(isset($uss->remarks)) {{$uss->remarks}} @else - @endif</td>
                <td>{{ExtensionsValley\Emr\CommonController::getDateFormat($uss->created_at,'d-M-Y h:i A') }}</td>
                <td>@if($uss->bill_converted_status == 0 && $visit_date == date('Y-m-d')) <button type="button" class="btn btn-sm btn-danger deleteUSSButton" data-investigation-detail-id="{{$uss->detail_id}}" ><i class="fa fa-trash deleteUSSIcon"></i></button>@endif</td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="8"> No history found..!</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>