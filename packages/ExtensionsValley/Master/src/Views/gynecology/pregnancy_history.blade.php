<div class="col-md-12 text-center" style="display: flex; margin-top:10px; ">
    <h4 style="width: 90%; margin: 0px;"> PREGNANCY HISTORY </h4>
    <i class=" tiny_block_toggle fa fa-window-minimize pull-right" data-toggle="collapse" href=".preg_history_container"></i>
</div>
<div class="col-md-12 preg_history_container" style="margin-top:20px;">
    <div class="col-md-2 col-md-offset-1">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="gravida">
            <label class="form-check-label" for="gravida">
            Gravida
            </label>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="para">
            <label class="form-check-label" for="para">
            Para
            </label>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="abortion">
            <label class="form-check-label" for="abortion">
                Abortion
            </label>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="live">
            <label class="form-check-label" for="live">
            Live
            </label>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="ectopic">
            <label class="form-check-label" for="ectopic">
            Ectopic
            </label>
        </div>
    </div>
</div>

<div class="col-md-12 preg_history_container" style="margin-top:15px;">
    <table class="table no-margin table-bordered table-condensed preg_history_table" style="border: 1px solid #CCC;">
        <thead>
            <tr class="headder_bg">
                <th>Pregnancy</th>
                <th>DOB</th>
                <th>Gender</th>
                <th>Birth Weight(gms)</th>
                <th>Remarks</th>
            </tr>
        </thead>
        <tbody class="preg_history_body">
            <tr>
                <td>
                    <input type="text" autocomplete="off" name="pregnancy_name[]" autofocus="" value="" class="form-control table_text pregnancy" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="" class="form-control datepicker table_text pregnancy_dob" placeholder="MMM-DD-YYYY" id="" style="">
                </td>
                <td>
                    {!! Form::select('gender', $gender_list,'0', ['class' => 'form-control table_text pregnancy_gender', 'placeholder' => 'Select Gender', 'id' => 'gender', 'style' => 'font-weight:700;border:none;']) !!}
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text pregnancy_birth_weight" placeholder="" id="" style="">
                </td>
                <td><input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text pregnancy_remarks" placeholder="" id="" style=""></td>
            </tr>


        </tbody>
    </table>
</div>
<div class="col-md-12 preg_history_container" style="margin-top:15px;">
    <div class="col-md-3">
        <h5>Conception :</h5>
    </div>
    <div class="col-md-4" style="margin-top:7px;">

        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn bg-radio_grp active" id="conception_natural_label">
                <input type="radio" name="conception_options" id="conception_natural" autocomplete="off" checked="" value="1"> Natural
            </label>
            <label class="btn bg-radio_grp" data-toggle="collapse" href="#assisted_container">
                <input type="radio" name="conception_options" id="conception_assisted" autocomplete="off" value="2" > Assisted
            </label>
        </div>
    </div>

    <div class="col-md-2">
        <h5>ET Date :</h5>
    </div>
    <div class="col-md-3">
        <input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="" class="form-control datepicker" placeholder="MMM-DD-YYYY" id="et_date" style="margin-top: 7px;">
    </div>



    <div class="col-md-12 no-padding collapse" id="assisted_container" style="margin-top:5px;">
        <div class="col-md-3 ">
            <h5>If Assisted :</h5>
        </div>
        <div class="col-md-9" style="margin-top:7px;">
            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control" placeholder="" id="assisted_text" style="">
        </div>
    </div>

    <div class="col-md-12 no-padding" style="margin-top:5px;">
        <div class="col-md-8" style="">

        <ul class="select_button no-padding" id="preg_history_group_items">
            <li data-value="Induced">
                <i class="fa fa-check-circle"></i>Induced
                <input type="hidden" name="induced" id="" disabled="disabled" value="">
            </li>
            <li data-value="IUI">
                <i class="fa fa-check-circle"></i>IUI
                <input type="hidden" name="iui" id="" disabled="disabled" value="">
            </li>
            <li data-value="IVF">
                <i class="fa fa-check-circle"></i>IVF
                <input type="hidden" name="ivf" id="" disabled="disabled" value="">
            </li>
            <li data-value="GIFT">
                <i class="fa fa-check-circle"></i>GIFT
                <input type="hidden" name="gift" id="" disabled="disabled" value="">
            </li>
            <li data-value="ICSI">
                <i class="fa fa-check-circle"></i>ICSI
                <input type="hidden" name="icsi" id="" disabled="disabled" value="">
            </li>
            <li data-value="Other" data-toggle="collapse" href="#assisted_other_option">
                <i class="fa fa-check-circle"></i>Other
                <input type="hidden" name="other" id="" disabled="disabled" value="">
            </li>
        </ul>

        </div>
    </div>

    <div class="col-md-12 no-padding collapse" id="assisted_other_option" style="margin-top:5px;">
        <div class="col-md-3 ">
            <h5>Others :</h5>
        </div>
        <div class="col-md-9" style="margin-top:7px;">
            <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control" placeholder="" id="conception_option_other_text" style="">
        </div>
    </div>

    <div class="col-md-12 no-padding" style="margin-top:5px;">
        <div class="col-md-3" style="">
            <h5>Inj. Hig Date :</h5>
        </div>
        <div class="col-md-4">
            <input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="" class="form-control datepicker" placeholder="MMM-DD-YYYY" id="inj_high_date" style="margin-top: 5px;">
        </div>
    </div>
</div>

