<div class="theadscroll" style="position: relative; height: 300px;">
    <table class="table no-margin theadfix_wrapper table_sm table-bordered  table-condensed prescription_history_table">
        <thead>
            <tr class="" style="background: aliceblue; color: #000000; ">
                <th>Sl.No</th>
                <th>Request No.</th>
                <th>Doctor</th>
                <th>Medicine</th>
                <th>Dose</th>
                <th>Frequency</th>
                <th>Days</th>
                <th>Quantity</th>
                <th>Route</th>
                <th>Instructions</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @if(sizeof($prescription_details)>0)
        @foreach ($prescription_details as $prescription)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>@if($prescription->prescription_id != '') {{$prescription->prescription_id}} @else 0 @endif</td>
                <td>@if(isset($prescription->doctor_name)) {{$prescription->doctor_name}} @else - @endif</td>
                <td>@if(isset($prescription->item_desc)) {{$prescription->item_desc}} @else - @endif</td>
                <td>@if(isset($prescription->dose)) {{$prescription->dose}} @else - @endif</td>
                <td>@if(isset($prescription->frequency)) {{$prescription->frequency}} @else - @endif</td>
                <td>@if(isset($prescription->duration)) {{$prescription->duration}} @else - @endif</td>
                <td>@if(isset($prescription->quantity)) {{$prescription->quantity}} @else - @endif</td>
                <td>@if(isset($prescription->route)) {{$prescription->route}} @else - @endif</td>
                <td>@if(isset($prescription->notes)) {{$prescription->notes}} @else - @endif</td>
                <td>{{ExtensionsValley\Emr\CommonController::getDateFormat($prescription->created_at,'d-M-Y h:i A') }}</td>
                <td>@if($prescription->billconverted_status == 0 && $visit_date == date('Y-m-d')) <button type="button" class="btn btn-sm btn-danger deletePrescriptionButton" data-prescription-detail-id="{{$prescription->detail_id}}" ><i class="fa fa-trash deletePrescriptionIcon"></i></button>@endif</td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="12"> No history found..!</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>