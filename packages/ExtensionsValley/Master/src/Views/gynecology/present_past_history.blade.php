<div class="col-md-12 text-center" style="display:flex; margin-top:10px; " >
    <h4 style="width: 90%; margin: 0px;" > PRESENT AND PAST PREGNANCY CHART </h4>
    <i class="tiny_block_toggle fa fa-window-minimize pull-right" data-toggle="collapse" href=".present_history_container" ></i>
</div>

<div class="col-md-12 present_history_container" style="margin-top:15px; margin-bottom:15px;">
    <table class="table no-margin table-bordered table-condensed" style="border: 1px solid #CCC;">
        <thead>
            <tr class="headder_bg">
                <th style="width:70%">Type</th>
                <th style="width:15%">Present</th>
                <th style="width:15%">Past</th>
            </tr>
        </thead>
        <tbody class="pregnancy_chart_table_body" >
            <tr data-name="rec_abortion">
                <td class="past_history_tr">Rec.Abortion</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="jaundice">
                <td class="past_history_tr">Jaundice</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="diabetes">
                <td class="past_history_tr">Diabetes</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="hypertension">
                <td class="past_history_tr">Hypertension</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="gest_diabetes">
                <td class="past_history_tr">Gest.Diabetes</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="pih">
                <td class="past_history_tr">PIH</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="pre_eclamsia">
                <td class="past_history_tr">Pre Eclamsia</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="renal">
                <td class="past_history_tr">Renel</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="thyroid">
                <td class="past_history_tr">Thyroid</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="epilepsy">
                <td class="past_history_tr">Epilepsy</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="asthma">
                <td class="past_history_tr">Asthma</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="heart_disease">
                <td class="past_history_tr">Heart Disease</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="surgery">
                <td class="past_history_tr">Surgery</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="drugs">
                <td class="past_history_tr">Drugs</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            <tr data-name="fever_with_eruption">
                <td class="past_history_tr">Fever With Eruption</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            <tr data-name="expo_to_radiation">
                <td class="past_history_tr">Expo.to.Radiation</td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="present_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
                <td class="text-center" style="padding-left:25px;">
                    <input type="checkbox" class="past_status"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                </td>
            </tr>
            </tr>
        </tbody>
    </table>
</div>