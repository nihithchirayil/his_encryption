
<div class="col-md-12 no-padding">
    <div class="col-md-10">
        <h5><b>USS</b></h5>
    </div>
    <div class="col-md-2 padding_sm ussHistoryContainer" style="text-align:right;">
        <button onclick="fetchUSSHistory('{{ $visit_date }}');" type="button" class="btn btn-sm btn-warning" > <i class="fa fa-list fetchUSSIcon" > </i>  USS HISTORY</button>
        <div class="popover left in" role="tooltip" style="top: -170px; left: -940px; display: none;">
            <div class="arrow" style="top: 60%;"></div>
            <div class="popover-content">

            </div>
        </div>
    </div>
</div>
@php 
    $investigation_array1 = [];
    $ivestigation_array2 = [];
    @endphp
    @foreach($data['investigations'] as $key => $val)
    @php 
    $investigation_array1[] = $val['service_code'];
    $investigation_array2[$val['service_code']] = $val;
    @endphp
    @endforeach
<div class="col-md-8 uss_container">
    <div class="col-md-12">
        @foreach($uss_inv1 as $inv)
        @php 
        $checked = '';
        $disabled = '';
        if(in_array($inv->service_code, $investigation_array1)){
            $checked = 'checked';
            $detail_id = $investigation_array2[$inv->service_code]['detail_id'];
            $bill_converted_status = \DB::table('patient_investigation_detail')->where('id', $detail_id)->value('bill_converted_status');
            if($bill_converted_status == 1){
                $disabled = 'disabled';
            }
        }
        @endphp
        <div class="col-md-6">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="{{$inv->service_code}}" id="{{$inv->service_code}}" {{$checked}} {{$disabled}} >
                <label class="form-check-label" for="hb">
                {{$inv->service_desc}}
                </label>
            </div>
        </div>
        @endforeach
    </div>
</div>
