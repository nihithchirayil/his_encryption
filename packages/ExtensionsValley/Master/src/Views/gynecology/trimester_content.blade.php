<div class="tabbable boxed parentTabs">
    <ul class="nav nav-tabs week_head">
        @foreach($trimester_data as $key => $val)
        @php
        if((int)$key == 1)
            $week = $key.'st Week';
        else if((int)$key == 2)
            $week = $key.'nd Week';
        else if((int)$key == 3)
            $week = $key.'rd Week';
        else
            $week = $key.'th Week';
        @endphp
        <li class=""><a href="#set{{$key}}">{{$week}}</a></li>
        @endforeach

        @if($week_exist_flag == 0)
            @php
            if($active_week == 1)
                $active_week_name = $active_week.'st Week';
            else if($active_week == 2)
                $active_week_name = $active_week.'nd Week';
            else if($active_week == 3)
                $active_week_name = $active_week.'rd Week';
            else
                $active_week_name = $active_week.'th Week';
            @endphp
            @if($active_week > 0 && $active_week <=12)
                @if($trimester == 1 && $visit_date == date('Y-m-d'))
                <li class=""><a href="#set{{$active_week}}">{{$active_week_name}}</a></li>
                @endif
            @endif
            @if($active_week > 12 && $active_week <=26)
                @if($trimester == 2 && $visit_date == date('Y-m-d'))
                <li class=""><a href="#set{{$active_week}}">{{$active_week_name}}</a></li>
                @endif
            @endif
            @if($active_week > 26)
                @if($trimester == 3 && $visit_date == date('Y-m-d'))
                <li class=""><a href="#set{{$active_week}}">{{$active_week_name}}</a></li>
                @endif
            @endif
        @endif
    </ul>
    <div class="tab-content">
        @foreach($trimester_data as $key => $val)
        <div class="tab-pane fade in week_detail" data-week-number="{{$key}}" id="set{{$key}}">
            <div class="tabbable">
                <ul class="nav nav-tabs days_head">
                    @foreach($val as $key1 => $val1)
                    <li class="@if($loop->first) active @endif"><a class="@if($loop->first) active @endif"  href="#sub{{$key1}}">{{date(\WebConf::getConfig('date_format_web'), strtotime($key1))}}</a></li>
                    @endforeach

                    @if($loop->last)
                        @if($day_exist_flag == 0 && ($active_week > 0 && $active_week <=12) && $trimester == 1 && $week_exist_flag == 1 && $visit_date == date('Y-m-d') )
                        <li><a class="" href="#sub{{$visit_date}}">{{date(\WebConf::getConfig('date_format_web'))}}</a></li>
                        @elseif($day_exist_flag == 0 && ($active_week > 12 && $active_week <=26) && $trimester == 2 && $week_exist_flag == 1 && $visit_date == date('Y-m-d') )
                        <li><a class="" href="#sub{{$visit_date}}">{{date(\WebConf::getConfig('date_format_web'))}}</a></li>
                        @elseif($day_exist_flag == 0 && ($active_week > 26) && $trimester == 3 && $week_exist_flag == 1 && $visit_date == date('Y-m-d') )
                        <li><a class="" href="#sub{{$visit_date}}">{{date(\WebConf::getConfig('date_format_web'))}}</a></li>
                        @endif
                    @endif
                    
                </ul>
                <div class="tab-content ">
                    @foreach($val as $key1 => $val1)
                    <div class="tab-pane fade in days_detail @if($loop->first) active @endif" data-visit-date="{{$key1}}" id="sub{{$key1}}">
                        @include('Master::gynecology.trimester_content_inner', ['data' => $val1, 'visit_date'=> $key1, 'doctor_id' => $doctor_id])
                    </div>
                    @endforeach

                    @if($loop->last)
                    @if($day_exist_flag == 0 && ($active_week > 0 && $active_week <=12) && $trimester == 1 && $week_exist_flag == 1 && $visit_date == date('Y-m-d') )
                    <div class="tab-pane fade in days_detail" data-visit-date="{{$visit_date}}" id="sub{{$visit_date}}">
                        @include('Master::gynecology.trimester_content_inner', ['data' => ['general_examination' => [], 'systemic_examination' => [], 'pelvic_examination' => [], 'obstetric_examination'=> [], 'investigations' => [], 'uss' => [] ], 'visit_date'=> $visit_date, 'doctor_id' => $doctor_id ])
                    </div>
                    @elseif($day_exist_flag == 0 && ($active_week > 12 && $active_week <=26) && $trimester == 2 && $week_exist_flag == 1 && $visit_date == date('Y-m-d'))
                    <div class="tab-pane fade in days_detail" data-visit-date="{{$visit_date}}" id="sub{{$visit_date}}">
                        @include('Master::gynecology.trimester_content_inner', ['data' => ['general_examination' => [], 'systemic_examination' => [], 'pelvic_examination' => [], 'obstetric_examination'=> [], 'investigations' => [], 'uss' => [] ], 'visit_date'=> $visit_date, 'doctor_id' => $doctor_id ])
                    </div>
                    @elseif($day_exist_flag == 0 && ($active_week > 26) && $trimester == 3 && $week_exist_flag == 1 && $visit_date == date('Y-m-d'))
                    <div class="tab-pane fade in days_detail" data-visit-date="{{$visit_date}}" id="sub{{$visit_date}}">
                        @include('Master::gynecology.trimester_content_inner', ['data' => ['general_examination' => [], 'systemic_examination' => [], 'pelvic_examination' => [], 'obstetric_examination'=> [], 'investigations' => [], 'uss' => [] ], 'visit_date'=> $visit_date, 'doctor_id' => $doctor_id ])
                    </div>
                    @endif
                    @endif
                    
                    
                </div>
            </div>
        </div>
        @endforeach

        @if($day_exist_flag == 0 && ($active_week > 0 && $active_week <=12) && $trimester == 1 && $week_exist_flag == 0 && $visit_date == date('Y-m-d') )
        <div class="tab-pane fade in week_detail" data-week-number="{{$active_week}}" id="set{{$active_week}}">
            <div class="tabbable">
                <ul class="nav nav-tabs days_head">
                    <li class="active"><a class="active"  href="#sub{{date('Y-m-d')}}">{{date(\WebConf::getConfig('date_format_web'), strtotime(date('Y-m-d')))}}</a></li>
                </ul>
                <div class="tab-content ">
                    <div class="tab-pane fade in days_detail active" data-visit-date="{{date('Y-m-d')}}" id="sub{{date('Y-m-d')}}">
                        @include('Master::gynecology.trimester_content_inner', ['data' => ['general_examination' => [], 'systemic_examination' => [], 'pelvic_examination' => [], 'obstetric_examination'=> [], 'investigations' => [], 'uss' => [] ], 'visit_date'=> date('Y-m-d'), 'doctor_id' => $doctor_id])
                    </div>
                </div>
            </div>
        </div>
        @elseif($day_exist_flag == 0 && ($active_week > 12 && $active_week <=26) && $trimester == 2 && $week_exist_flag == 0 && $visit_date == date('Y-m-d'))
        <div class="tab-pane fade in week_detail" data-week-number="{{$active_week}}" id="set{{$active_week}}">
            <div class="tabbable">
                <ul class="nav nav-tabs days_head">
                    <li class="active"><a class="active"  href="#sub{{date('Y-m-d')}}">{{date(\WebConf::getConfig('date_format_web'), strtotime(date('Y-m-d')))}}</a></li>
                </ul>
                <div class="tab-content ">
                    <div class="tab-pane fade in days_detail active" data-visit-date="{{date('Y-m-d')}}" id="sub{{date('Y-m-d')}}">
                        @include('Master::gynecology.trimester_content_inner', ['data' => ['general_examination' => [], 'systemic_examination' => [], 'pelvic_examination' => [], 'obstetric_examination'=> [], 'investigations' => [], 'uss' => [] ], 'visit_date'=> date('Y-m-d'), 'doctor_id' => $doctor_id])
                    </div>
                </div>
            </div>
        </div>
        @elseif($day_exist_flag == 0 && ($active_week > 26) && $trimester == 3 && $week_exist_flag == 0 && $visit_date == date('Y-m-d'))
        <div class="tab-pane fade in week_detail" data-week-number="{{$active_week}}" id="set{{$active_week}}">
            <div class="tabbable">
                <ul class="nav nav-tabs days_head">
                    <li class="active"><a class="active "  href="#sub{{date('Y-m-d')}}">{{date(\WebConf::getConfig('date_format_web'), strtotime(date('Y-m-d')))}}</a></li>
                </ul>
                <div class="tab-content ">
                    <div class="tab-pane fade in days_detail active " data-visit-date="{{date('Y-m-d')}}" id="sub{{date('Y-m-d')}}">
                        @include('Master::gynecology.trimester_content_inner', ['data' => ['general_examination' => [], 'systemic_examination' => [], 'pelvic_examination' => [], 'obstetric_examination'=> [], 'investigations' => [], 'uss' => [] ], 'visit_date'=> date('Y-m-d'), 'doctor_id' => $doctor_id])
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>
</div>

