<div class="theadscroll" style="position: relative; height: 300px;">
    <table class="table no-margin theadfix_wrapper table_sm table-bordered  table-condensed investigation_history_table">
        <thead>
            <tr class="" style="background: aliceblue; color: #000000; ">
                <th>Sl.No</th>
                <th>Request No.</th>
                <th>Doctor</th>
                <th>Investigation</th>
                <th>Type</th>
                <th>Remarks</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @if(sizeof($investigation_details)>0)
        @foreach ($investigation_details as $investigation)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>@if($investigation->batch_no != '') {{$investigation->batch_no}} @else 0 @endif</td>
                <td>@if(isset($investigation->doctor_name)) {{$investigation->doctor_name}} @else - @endif</td>
                <td>@if(isset($investigation->service_text)) {{$investigation->service_text}} @else - @endif</td>
                <td>@if(isset($investigation->investigation_type)) {{$investigation->investigation_type}} @else - @endif</td>
                <td>@if(isset($investigation->inv_remarks)) {{$investigation->inv_remarks}} @else - @endif</td>
                <td>{{ExtensionsValley\Emr\CommonController::getDateFormat($investigation->created_at,'d-M-Y h:i A') }}</td>
                <td>@if($investigation->bill_converted_status == 0 && $visit_date == date('Y-m-d')) <button type="button" class="btn btn-sm btn-danger deleteInvestigationButton" data-investigation-detail-id="{{$investigation->detail_id}}" ><i class="fa fa-trash deleteInvestigationIcon"></i></button>@endif</td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="8"> No history found..!</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>