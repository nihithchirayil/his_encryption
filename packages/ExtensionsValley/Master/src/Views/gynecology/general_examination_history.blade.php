<div class="theadscroll" style="position: relative; height: 300px;">
    <table class="table no-margin table-bordered table-condensed theadfix_wrapper general_examination_history_table" style="border: 1px solid #CCC;">
        <thead>
            <tr class="headder_bg">
                <th>Date</th>
                <th>Height</th>
                <th>Weight</th>
                <th>BMI</th>
                <th>Pulse</th>
                <th>Respiration</th>
                <th>Temperature </th>
                <th>Pallor</th>
                <th>Oadema</th>
                <th>Breast</th>
                <th>Abdomen</th>
                <th>Thyroid</th>
                <th>ACNE</th>
                <th>Striae</th>
                <th>Hirsutism</th>
                <th>Galactorrhea</th>
            </tr>
        </thead>
        <tbody>
            @foreach($general_examination as $gen_exam )
            <tr>
                <td style="white-space: nowrap;">@if(@isset($gen_exam['date'])) {{date('M-d-Y', strtotime($gen_exam['date']))}} @endif </td>
                <td>@if(@isset($gen_exam['height'])) {{$gen_exam['height']}} @endif </td>
                <td>@if(@isset($gen_exam['weight'])) {{$gen_exam['weight']}} @endif</td>
                <td>@if(@isset($gen_exam['bmi'])) {{$gen_exam['bmi']}} @endif</td>
                <td>@if(@isset($gen_exam['pulse'])) {{$gen_exam['pulse']}} @endif</td>
                <td>@if(@isset($gen_exam['respiration'])) {{$gen_exam['respiration']}} @endif</td>
                <td>@if(@isset($gen_exam['temperature'])) {{$gen_exam['temperature']}} @endif</td>
                <td>@if(@isset($gen_exam['pallor'])) {{$gen_exam['pallor']}} @endif</td>
                <td>@if(@isset($gen_exam['oadema'])) {{$gen_exam['oadema']}} @endif</td>
                <td>@if(@isset($gen_exam['breast'])) {{$gen_exam['breast']}} @endif</td>
                <td>@if(@isset($gen_exam['abdomen'])) {{$gen_exam['abdomen']}} @endif</td>
                <td>@if(@isset($gen_exam['thyroid'])) {{$gen_exam['thyroid']}} @endif</td>
                <td>@if(@isset($gen_exam['acne'])) {{$gen_exam['acne']}} @endif</td>
                <td>@if(@isset($gen_exam['striae'])) {{$gen_exam['striae']}} @endif</td>
                <td>@if(@isset($gen_exam['hirsutism'])) {{$gen_exam['hirsutism']}} @endif</td>
                <td>@if(@isset($gen_exam['galactorrhea'])) {{$gen_exam['galactorrhea']}} @endif</td>
            </tr>
            
            @endforeach
        </tbody>
    </table>
</div>
