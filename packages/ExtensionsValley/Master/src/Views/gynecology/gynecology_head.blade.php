<div class="col-md-12" style="padding: 0 2px !important; ">
    <div class="box no-border no-margin" id="gynech_date_details">
        <div class="box-body" style="height:125px;border-radius: 5px;">
            <div class="col-xs-6">
                <label class="filter_label">LMP :</label>
            </div>
            <div class="col-xs-6 date_filter_div">
                <div class="mate-input-box">
                    <input type="text" data-attr="date" autocomplete="off" name="lmp_date"  value="" class="form-control datepicker" placeholder="MMM-DD-YYYY" id="lmp_date" onblur="calculateEDDBasedOnLMP();" style="">
                </div>
            </div>

            <div class="col-xs-6">
                <label class="filter_label">EDD Based on LMP:</label>
            </div>
            <div class="col-xs-6 date_filter_div">
                <div class="mate-input-box">
                    <input type="text" data-attr="date" autocomplete="off" name="edd_based_on_lmp_date"  value="" class="form-control datepicker" placeholder="MMM-DD-YYYY" id="edd_based_on_lmp_date" style="">
                </div>
            </div>

            <div class="col-xs-6">
                <label class="filter_label">EDD Based on USS</label>
            </div>
            <div class="col-xs-6 date_filter_div">
            <div class="mate-input-box">
                    <input type="text" data-attr="date" autocomplete="off" name="edd_based_on_uss"  value="" class="form-control datepicker" placeholder="MMM-DD-YYYY" id="edd_based_on_uss" style="">
                </div>
            </div>
            
            <div class="col-xs-6">
                <label class="filter_label">Gestational Age</label>
            </div>
            <div class="col-xs-6 date_filter_div">
            <div class="mate-input-box">
                    <input type="text" autocomplete="off" name="gestation_age" value="" class="form-control " placeholder="" id="gestation_age" style="">
                </div>
            </div>

        </div>
    </div>
</div>