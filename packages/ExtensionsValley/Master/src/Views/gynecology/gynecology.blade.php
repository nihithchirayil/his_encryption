@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/default/css/gynecology.css")}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="patient_id" value="{{$patient_id}}">
<input type="hidden" id="visit_id" value="{{$visit_id}}">
<input type="hidden" id="doctor_id" value="{{$doctor_id}}">
<input type="hidden" id="encounter_id" value="{{$encounter_id}}">
<input type="hidden" id="prescription_head_id" value="0">
<input type="hidden" id="prescription_consumable_head_id" value="0">
<input type="hidden" id="prescription_changes_draft_mode" value="0">
<div class="right_col">
   <div class="box-body" style="margin-top:40px;">
      <div class="col-md-3 padding_sm">
         <div class="col-md-12 padding_sm">
            <div class="box no-border no-margin">
               <div class="box-body" style="min-height: 125px;border-radius:5px;">
                  <table class="table table_sm  no-margin no-border">
                     <tbody>
                        <tr>
                           <td rowspan="3" style="width:14%;align:left">
                              <div class="user_patient_img pos_rel pop-div">
                                 @if($patient_info[0]->patient_image !='')
                                 <img class="img-circle user_img" src="data:image/jpg;base64,{!!$patient_info[0]->patient_image!!}" style="height:60px;" alt="" id='poplink'/>
                                 @else
                                 <div class="img-circle user_img" style="color: darkgray;height: 60px; padding-left: 16px; font-size: 37px;" alt="" id='poplink'>
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                 </div>
                                 @endif
                                 <div class="smiley_status">
                                 <img src="{{asset("packages/extensionsvalley/dashboard/images/smiley.png")}}" alt="">
                                 <img class="hidden" src="{{asset("packages/extensionsvalley/dashboard/images/sad_smiley.png")}}" alt="">
                              </div>
                                 <div class="pop-content">
                                 @include('Emr::emr.includes.patient_info')
                                 </div>
                              </div>
                           </td>
                           <td><b>{{$patient_info[0]->patient_name}}({{$patient_info[0]->age}}/{{$patient_info[0]->gender}})</b> </td>
                        </tr>
                        <tr>
                           <td><b>UHID :</b>{{$patient_info[0]->uhid}}</td>
                           <td>&nbsp;</td>
                        </tr>

                        <tr>
                           <td><b>Blood Group :</b>{{$patient_info[0]->blood_group}}</td>
                        </tr>
                        <tr>
                           <td style="padding-top:15px !important; " colspan="2"><b>Last Visited Date :</b>{{date('M-d-Y', strtotime($patient_info[0]->last_visit_datetime)) }}</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>

      <div class="col-md-3 padding_sm">
         @include('Emr::emr.includes.patient_allergy')
      </div>

      <div class="col-md-3 padding_sm">
         <div class="col-md-12" style="padding: 0 2px !important; ">
            @include('Emr::emr.includes.patient_vitals')
         </div>
      </div>

      <div class="col-md-3 padding_sm">
        @include('Master::gynecology.gynecology_head')
      </div>
      <div class="clearfix"></div>
      <div class="ht25"></div>
   </div>
<!----tinymce area-------------------------------------------------------------------->
<div class="col-md-12 no-padding" style="margin-top:15px;box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);border-radius: 3px;border: 1px solid #d2d6de;background: #ffffff none repeat scroll 0 0;height: auto !important;overflow: visible !important;padding: 0px !important;padding-top:3px !important;height:3600px;">

    <div class="box-header with-border">
        <i id="tiny_block_toggle" class="fa fa-window-minimize pull-right" onclick="expandTinymce();" style="margin-right:5px;"></i>
        <i id="tiny_block_toggle" class="fa fa-window-maximize pull-right" onclick="expandTinymce();" style="margin-right:5px;"></i>

    </div>
    <div class="col-md-12" >
            <div class="col-md-6 box-body" style="width: 49%;  margin:5px !important;">
               @include('Master::gynecology.history_data')
            </div>
            <div class="col-md-6 box-body" style="width: 49%;  margin:5px !important;">
               @include('Master::gynecology.present_past_history')
            </div>
    </div>
    <div class=col-md-12>
      @include('Master::gynecology.present_data')
   </div>


    <div class="col-md-12" style="margin-top:10px !important;">
        @include('Master::gynecology.trimester')
    </div>
</div>
<!----tinymce area ends--------------------------------------------------------------->

<div class="col-md-12 no-padding" style="margin-top:15px;box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);border-radius: 3px;border: 1px solid #d2d6de;background: #ffffff none repeat scroll 0 0;height: auto !important;overflow: visible !important;padding: 0px !important;padding-top:3px !important;height:3600px; text-align:right;">
   <div class="col-md-1 col-md-offset-9 padding_sm" > 
      <button type="button" class="btn btn-block btn-primary " onclick="clickToTop();" > <i class="fa fa-arrow-up"> </i> Click to Top </button>
   </div>
   <div class="col-md-2 padding_sm" > 
      <button type="button" class="btn btn-block btn-success " onclick="saveGynecologyDetails();" > <i class="fa fa-save"> </i> Save </button>
   </div>
</div>

<!-- prescritpion modal  -->

<div class="modal fade gynec_prescription_modal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:90%;">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Prescription</h4>
            </div>
            <div class="modal-body">
                <div class="row gynec_prescription_modal_data">
                    @include('Emr::emr.includes.patient_prescription', ['doctor_id' => $doctor_id])
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm saveGynecPrescriptionBtn">Apply</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<!--  -->

<!-- Custom Modals -->
@include('Emr::emr.includes.custom_modals')
</div>
@stop
@section('javascript_extra')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/gynecology.js")}}"></script>
<!-- Vitals -->
<script src="{{ asset('packages/extensionsvalley/emr/js/vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
<!-- Allergy -->
<script src="{{ asset('packages/extensionsvalley/emr/js/allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>

<script type="text/javascript">

$(document).ready(function(){
   fetch_all_allergy('{{ $patient_id }}');
})

</script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
@endsection
