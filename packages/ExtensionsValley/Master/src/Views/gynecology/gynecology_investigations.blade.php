
<!-- investigation start -->
<div class="col-md-12 no-padding">
    <div class="col-md-10">
        <h5><b>INVESTIGATIONS</b></h5>
    </div>
    <div class="col-md-2 padding_sm investigationHistoryContainer" style="text-align:right;">
        <button onclick="fetchInvestigationHistory('{{ $visit_date }}');" type="button" class="btn btn-sm btn-warning" > <i class="fa fa-list fetchInvestigationIcon" > </i> INVESTIGATION HISTORY</button>
        <div class="popover left in" role="tooltip" style="top: -50px; left: -845px; display: none;">
            <div class="arrow" style="top: 60%;"></div>
            <div class="popover-content">

            </div>
        </div>
    </div>
</div>
<div class="col-md-12 investigation_container">
    <div class="col-md-6 bottom-padding">
        <b>General</b>
    </div>
    <div class="col-md-6 bottom-padding">
        <b>Hormonal Factor</b>
    </div>
    @php 
    $investigation_array1 = [];
    $ivestigation_array2 = [];
    @endphp
    @foreach($data['investigations'] as $key => $val)
    @php 
    $investigation_array1[] = $val['service_code'];
    $investigation_array2[$val['service_code']] = $val;
    @endphp
    @endforeach
    <!--- General Content --->
    <div class="col-md-6">
    @foreach($general_inv1 as $inv)
        @php 
        $checked = '';
        $disabled = '';
        if(in_array($inv->service_code, $investigation_array1)){
            $checked = 'checked';
            $detail_id = $investigation_array2[$inv->service_code]['detail_id'];
            $bill_converted_status = \DB::table('patient_investigation_detail')->where('id', $detail_id)->value('bill_converted_status');
            if($bill_converted_status == 1){
                $disabled = 'disabled';
            }
        }
        @endphp
        <div class="col-md-6">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="{{$inv->service_code}}" id="{{$inv->service_code}}" {{ $checked }}  {{ $disabled }} >
                <label class="form-check-label" for="{{$inv->service_code}}">
                {{$inv->service_desc}}
                </label>
            </div>
        </div>
    @endforeach
    </div>
    <!--- Hormonal Content --->
    <div class="col-md-6 ">
    @foreach($hormonal_inv1 as $inv)
        @php 
        $checked = '';
        $disabled = '';
        if(in_array($inv->service_code, $investigation_array1)){
            $checked = 'checked';
            $detail_id = $investigation_array2[$inv->service_code]['detail_id'];
            $bill_converted_status = \DB::table('patient_investigation_detail')->where('id', $detail_id)->value('bill_converted_status');
            if($bill_converted_status == 1){
                $disabled = 'disabled';
            }
        }
        @endphp
        <div class="col-md-6">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="{{$inv->service_code}}" id="{{$inv->service_code}}" {{ $checked }}  {{ $disabled }} >
                <label class="form-check-label" for="{{$inv->service_code}}">
                {{$inv->service_desc}}
                </label>
            </div>
        </div>
    @endforeach
    </div>

</div>