
<div class="col-md-6">
    <div class="box no-border">
        <div class="tinybody">
            <button class="accordion">MEDICAL HISTORY</button>
            <div class="panel tinypanel">
                <textarea class="texteditor11 form-control" id="medical_history"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6 ">
    <div class="box no-border">
        <div class="tinybody">
            <button class="accordion">SURGICAL HISTORY</button>
            <div class="panel tinypanel">
                <textarea class="texteditor11 form-control" id="surgical_history"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="box no-border">
        <div class="tinybody">
            <button class="accordion">FAMLIY HISTORY</button>
            <div class="panel tinypanel">
                <textarea class="texteditor11 form-control" id="family_history"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6 ">
    <div class="box no-border">
        <div class="tinybody">
            <button class="accordion">OUTSIDE INVESTIGATION</button>
            <div class="panel tinypanel">
                <textarea class="texteditor11 form-control" id="outside_investigation"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6 ">
    <div class="box no-border">
        <div class="tinybody">
            <button class="accordion">PRECEDING COMPLAINTS</button>
            <div class="panel tinypanel">
                <textarea class="texteditor11 form-control" id="preceding_complaints"></textarea>
            </div>
        </div>
    </div>
</div>


<div class="col-md-12"><hr></hr></div>

@include('Master::gynecology.pregnancy_history')
@include('Master::gynecology.menstural_history')
