
<div class="col-md-12 trimester_head trimester_head_active" data-element="trimester1">
    <h4> <span class="badge badge-success">1 </span> TRIMESTER - (1-12 Weeks)</h4>
</div>
<div class="col-md-12 trimester no-padding" id="trimester1" style="display:none;"></div>
<div class="clearfix"></div>

<div class="col-md-12 trimester_head " data-element="trimester2">
    <h4> <span class="badge badge-success">2 </span> TRIMESTER - (13-26 Weeks)</h4>
</div>
<div class="col-md-12 trimester" id="trimester2" style="display:none;"></div>
<div class="clearfix"></div>

<div class="col-md-12 trimester_head " data-element="trimester3" >
    <h4> <span class="badge badge-success">3 </span> TRIMESTER - (27-40 Weeks) </h4>
</div>
<div class="col-md-12 trimester" id="trimester3" style="display:none;"></div>