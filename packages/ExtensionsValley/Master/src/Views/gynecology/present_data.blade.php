
<div class="col-md-12"><hr></hr></div>
<div class="col-md-12 text-left" style="display:flex; margin-top:10px; " >
    <h5 style="width: 100%; margin: 0px;" > <b> PRESENT OBSERVATIONS </b> </h5>
</div>
<div class="col-md-12"><hr></hr></div>

<div class="col-md-12 no-padding" >
    <div class="col-md-3 ">
        <div class="box no-border">
            <div class="tinybody">
                <button class="accordion">PATIENT DIAGNOSIS</button>
                <div class="panel tinypanel">
                    <textarea class="texteditor11 form-control" id="patient_diagnosis"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 ">
        <div class="box no-border">
            <div class="tinybody">
                <button class="accordion">FINDINGS AND IMPRESSIONS</button>
                <div class="panel tinypanel">
                    <textarea class="texteditor11 form-control" id="findings_and_impressions"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box no-border">
            <div class="tinybody">
                <button class="accordion">DIET REMARKS</button>
                <div class="panel tinypanel">
                    <textarea class="texteditor11 form-control" id="diet_remarks"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box no-border">
            <div class="tinybody">
                <button class="accordion">PRESENTING COMPLAINTS</button>
                <div class="panel tinypanel">
                    <textarea class="texteditor11 form-control" id="presenting_complaints"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box no-border">
            <div class="tinybody">
                <button class="accordion"> GENERAL AND SYSTEMIC EXAMINATION </button>
                <div class="panel tinypanel">
                    <textarea class="texteditor11 form-control" id="general_and_systemic_examination"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box no-border">
            <div class="tinybody">
                <button class="accordion"> FOLLOW UP ADVICE </button>
                <div class="panel tinypanel">
                    <textarea class="texteditor11 form-control" id="followup_advise"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box no-border">
            <div class="tinybody">
                <button class="accordion"> ANOMALY SCAN STATUS (GA 18-22) </button>
                <div class="panel tinypanel">
                    <textarea class="texteditor11 form-control" id="anomaly_scan_status"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box no-border">
            <div class="tinybody">
                <button class="accordion"> GROWTH SCAN (GA 30) </button>
                <div class="panel tinypanel">
                    <textarea class="texteditor11 form-control" id="growth_scan"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box no-border">
            <div class="tinybody">
                <button class="accordion"> GROWTH FETAL POSITION (GA 36) </button>
                <div class="panel tinypanel">
                    <textarea class="texteditor11 form-control" id="growth_fetal_position"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>