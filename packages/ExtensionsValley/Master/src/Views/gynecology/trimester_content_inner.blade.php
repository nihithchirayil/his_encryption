

<div class="col-md-12 no-padding"  style="margin-top: 20px;">
    
    <div class="col-md-9">
        <h5><b>GENERAL EXAMINATION</b></h5>
    </div>
    <div class="col-md-3 padding_sm generalExaminationHistoryContainer" style="text-align:right;">
        <button onclick="fetchGeneralExaminationHistory();" type="button" class="btn btn-sm btn-warning" > <i class="fa fa-list fetchGeneralExaminationIcon" > </i>  GENERAL HISTORY</button>
        <div class="popover left in" role="tooltip" style="top: -180px; left: -940px; display: none;">
            <div class="arrow" style="top: 60%;"></div>
            <div class="popover-content">

            </div>
        </div>
    </div>

</div>
<div class="col-md-8 general_examination" style="margin-top:15px;">
    <div class="theadscroll" style="position: relative; height:300px;">
        <table class="table no-margin theadfix_wrapper table-bordered table-condensed" style="border: 1px solid #CCC;">

            <tbody>
                <tr>
                    <td>
                        <div class="col-md-4">Height ( CM ):</div>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['general_examination']['height'])) {{$data['general_examination']['height']}} @endif" class="form-control gen_exam_height" >
                        </div>
                    </td>
                    <td>
                        <div class="col-md-4">Thyroid:</div>
                        <div class="col-md-8">
                            <div class="btn-group btn-group-toggle gen_exam_thyroid" data-toggle="buttons">
                                <label class="btn bg-radio_grp  @if(@isset($data['general_examination']['thyroid']) && $data['general_examination']['thyroid']  == 'normal') active @endif">
                                <input type="radio" name="thyroid" autocomplete="off" value="normal" > Normal
                                </label>
                                <label class="btn bg-radio_grp @if(@isset($data['general_examination']['thyroid']) && $data['general_examination']['thyroid'] == 'enlarge') active @endif">
                                <input type="radio" name="thyroid" autocomplete="off" value="enlarge" > Enlarge
                                </label>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-md-4">Weight ( kg ):</div>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['general_examination']['weight'])) {{$data['general_examination']['weight']}} @endif" class="form-control gen_exam_weight" >
                        </div>
                    </td>
                    <td>
                        <div class="col-md-4">Breast:</div>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['general_examination']['breast'])) {{$data['general_examination']['breast']}} @endif" class="form-control gen_exam_breast" >
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-md-4">BMI:</div>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['general_examination']['bmi'])) {{$data['general_examination']['bmi']}} @endif" class="form-control gen_exam_bmi" >
                        </div>
                    </td>
                    <td>
                        <div class="col-md-4">Abdomen:</div>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['general_examination']['abdomen'])) {{$data['general_examination']['abdomen']}} @endif" class="form-control gen_exam_abdomen" >
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-md-4">Pulse:</div>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['general_examination']['pulse'])) {{$data['general_examination']['pulse']}} @endif" class="form-control gen_exam_pulse" >
                        </div>
                    </td>
                    <td>
                        <div class="col-md-4">ACNE:</div>
                        <div class="col-md-8">
                            <div class="btn-group btn-group-toggle gen_exam_acne" data-toggle="buttons">
                                <label class="btn bg-radio_grp @if(@isset($data['general_examination']['acne']) && $data['general_examination']['acne']  == 'yes') active @endif">
                                <input type="radio" name="acne" autocomplete="off" value="yes" checked> Yes
                                </label>
                                <label class="btn bg-radio_grp @if(@isset($data['general_examination']['acne']) && $data['general_examination']['acne']  == 'no') active @endif">
                                <input type="radio" name="acne" autocomplete="off" value="no"> No
                                </label>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-md-4">Resp Rate:</div>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['general_examination']['respiration'])) {{$data['general_examination']['respiration']}} @endif" class="form-control gen_exam_respiration" >
                        </div>
                    </td>
                    <td>
                        <div class="col-md-4">Striae:</div>
                        <div class="col-md-8">
                            <div class="btn-group btn-group-toggle gen_exam_striae" data-toggle="buttons">
                                <label class="btn bg-radio_grp @if(@isset($data['general_examination']['striae']) && $data['general_examination']['striae']  == 'yes') active @endif">
                                <input type="radio" name="striae" autocomplete="off" value="yes" checked> Yes
                                </label>
                                <label class="btn bg-radio_grp @if(@isset($data['general_examination']['striae']) && $data['general_examination']['striae']  == 'no') active @endif">
                                <input type="radio" name="striae" autocomplete="off" value="no"> No
                                </label>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-md-4">Temprature (°C) :</div>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['general_examination']['temperature'])) {{$data['general_examination']['temperature']}} @endif" class="form-control gen_exam_temperature" >
                        </div>
                    </td>
                    <td>
                        <div class="col-md-4">Hirsutism:</div>
                        <div class="col-md-8">
                            <div class="btn-group btn-group-toggle gen_exam_hirsutism" data-toggle="buttons">
                                <label class="btn bg-radio_grp @if(@isset($data['general_examination']['hirsutism']) && $data['general_examination']['hirsutism']  == 'yes') active @endif">
                                <input type="radio" name="hirsutism" autocomplete="off" value="yes" checked> Yes
                                </label>
                                <label class="btn bg-radio_grp @if(@isset($data['general_examination']['hirsutism']) && $data['general_examination']['hirsutism']  == 'no') active @endif">
                                <input type="radio" name="hirsutism" autocomplete="off" value="no"> No
                                </label>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-md-4">Pallor:</div>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['general_examination']['pallor'])) {{$data['general_examination']['pallor']}} @endif" class="form-control gen_exam_pallor" >
                        </div>
                    </td>
                    <td>
                        <div class="col-md-4">Galactorrhea:</div>
                        <div class="col-md-8">
                            <div class="btn-group btn-group-toggle gen_exam_galactorrhea" data-toggle="buttons">
                                <label class="btn bg-radio_grp @if(@isset($data['general_examination']['galactorrhea']) && $data['general_examination']['galactorrhea']  == 'absent') active @endif">
                                <input type="radio" name="options" id="option1" autocomplete="off" value="absent" checked> Absent
                                </label>
                                <label class="btn bg-radio_grp @if(@isset($data['general_examination']['galactorrhea']) && $data['general_examination']['galactorrhea']  == 'present') active @endif">
                                <input type="radio" name="options" id="option2" autocomplete="off" value="present"> Present
                                </label>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-md-4">Oadema:</div>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['general_examination']['oadema'])) {{$data['general_examination']['oadema']}} @endif" class="form-control gen_exam_oadema" >
                        </div>
                    </td>
                    <td>

                    </td>
                </tr>


            </tbody>
        </table>
    </div>
</div>
<div class="col-md-8 no-padding">
    <h5><b>SYSTEMIC EXAMINATION</b></h5>
</div>
<div class="col-md-8 no-padding">
    <div class="col-md-4">
        <h5>CVS :</h5>
    </div>
    <div class="col-md-8">
        <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['systemic_examination']['cvs'])) {{$data['systemic_examination']['cvs']}} @endif" class="form-control sys_exam_cvs" >
    </div>
</div>
<div class="col-md-8 no-padding">
    <div class="col-md-4">
        <h5>CNS :</h5>
    </div>
    <div class="col-md-8">
        <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['systemic_examination']['cns'])) {{$data['systemic_examination']['cns']}} @endif" class="form-control sys_exam_cns" >
    </div>
</div>
<div class="col-md-8 no-padding">
    <div class="col-md-4">
        <h5>Respiratory System :</h5>
    </div>
    <div class="col-md-8">
        <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['systemic_examination']['resp_system'])) {{$data['systemic_examination']['resp_system']}} @endif" class="form-control sys_exam_resp_system" >
    </div>
</div>

<div class="col-md-8 no-padding">
    <h5><b>PELVIC EXAMINATION</b></h5>
</div>
<div class="col-md-8 no-padding">
    <div class="col-md-4">
        <h5>Introitus :</h5>
    </div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle introitus" data-toggle="buttons">
            <label class="btn bg-radio_grp @if(@isset($data['pelvic_examination']['introitus']) && $data['pelvic_examination']['introitus']  == 'normal') active @endif">
              <input type="radio" name="introitus" autocomplete="off" value="normal"> Normal
            </label>
            <label class="btn bg-radio_grp @if(@isset($data['pelvic_examination']['introitus']) && $data['pelvic_examination']['introitus']  == 'abnormal') active @endif">
              <input type="radio" name="introitus" autocomplete="off" value="abnormal"> Abnormal
            </label>
        </div>
    </div>
</div>
<div class="col-md-8 no-padding">
    <div class="col-md-4">
        <h5>CX :</h5>
    </div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle cx" data-toggle="buttons">
            <label class="btn bg-radio_grp @if(@isset($data['pelvic_examination']['cx']) && $data['pelvic_examination']['cx']  == 'normal') active @endif">
              <input type="radio" name="cx" autocomplete="off" value="normal" > Normal
            </label>
            <label class="btn bg-radio_grp @if(@isset($data['pelvic_examination']['cx']) && $data['pelvic_examination']['cx']  == 'abnormal') active @endif">
              <input type="radio" name="cx" autocomplete="off"  value="abnormal"> Abnormal
            </label>
        </div>
    </div>
</div>
<div class="col-md-8 no-padding">
    <div class="col-md-4">
        <h5>Uterus Size :</h5>
    </div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle uterus_size" data-toggle="buttons">
            <label class="btn bg-radio_grp @if(@isset($data['pelvic_examination']['uterus_size']) && $data['pelvic_examination']['uterus_size']  == 'small') active @endif">
              <input type="radio" name="uterus_size" autocomplete="off" value="small"  > Small
            </label>
            <label class="btn bg-radio_grp @if(@isset($data['pelvic_examination']['uterus_size']) && $data['pelvic_examination']['uterus_size']  == 'normal') active @endif">
              <input type="radio" name="uterus_size" autocomplete="off" value="normal"> Normal
            </label>
            <label class="btn bg-radio_grp @if(@isset($data['pelvic_examination']['uterus_size']) && $data['pelvic_examination']['uterus_size']  == 'enlarged') active @endif">
              <input type="radio" name="uterus_size" autocomplete="off" value="enlarged"> Enlarged
            </label>
        </div>
    </div>
</div>
<div class="col-md-8 no-padding">
    <div class="col-md-4">
        <h5>R/V A/V :</h5>
    </div>
    <div class="col-md-8">
        <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($data['pelvic_examination']['rv_av'])) {{$data['pelvic_examination']['rv_av']}} @endif" class="form-control rv_av">
    </div>
</div>
<div class="col-md-8 no-padding">
    <div class="col-md-4">
        <h5>Mobility :</h5>
    </div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle mobility" data-toggle="buttons">
            <label class="btn bg-radio_grp @if(@isset($data['pelvic_examination']['mobility']) && $data['pelvic_examination']['mobility']  == 'yes') active @endif">
              <input type="radio" name="mobility" autocomplete="off" value="yes"> Yes
            </label>
            <label class="btn bg-radio_grp @if(@isset($data['pelvic_examination']['mobility']) && $data['pelvic_examination']['mobility']  == 'no') active @endif">
              <input type="radio" name="mobility" autocomplete="off" value="no"> No
            </label>
        </div>
    </div>
</div>
<div class="col-md-8 no-padding">
    <div class="col-md-4">
        <h5>Adnexa :</h5>
    </div>
    <div class="col-md-8">
        <div class="btn-group btn-group-toggle adnexa" data-toggle="buttons">
            <label class="btn bg-radio_grp @if(@isset($data['pelvic_examination']['adnexa']) && $data['pelvic_examination']['adnexa']  == 'normal') active @endif">
              <input type="radio" name="adnexa" autocomplete="off" value="normal"> Normal
            </label>
            <label class="btn bg-radio_grp @if(@isset($data['pelvic_examination']['adnexa']) && $data['pelvic_examination']['adnexa']  == 'abnormal') active @endif">
              <input type="radio" name="adnexa" autocomplete="off" value="abnormal"> Abnormal
            </label>
        </div>
    </div>
</div>

@include('Master::gynecology.gynecology_investigations', ['data' => $data, 'visit_date' => $visit_date])
@include('Master::gynecology.gynecology_uss', ['data' => $data])
@include('Master::gynecology.gynecology_obstetrics', ['data' => $data])


<div class="col-md-8 no-padding" style="margin-top:20px;" >
    <div class="col-md-6 ">
        <div class="box no-border">
            <div class="tinybody">
                <button class="accordion" ><h5><b>SCAN DETAILS</b></h5></button>
                <!-- <div class="panel tinypanel"> -->
                    <textarea class="texteditor11 form-control scan_details" >@if(@isset($data['scan_details'])){{$data['scan_details']}} @endif</textarea>
                <!-- </div> -->
            </div>
        </div>
    </div>
    <div class="col-md-6 ">
        <div class="box no-border">
            <div class="tinybody">
                <button class="accordion" ><h5><b>TREATMENT PLAN</b></h5></button>
                <!-- <div class="panel tinypanel"> -->
                    <textarea class="texteditor11 form-control treatment_plan" >@if(@isset($data['treatment_plan'])){{$data['treatment_plan']}} @endif</textarea>
                <!-- </div> -->
            </div>
        </div>
    </div>

</div>
<div class="clearfix"></div>

@include('Master::gynecology.gynecology_prescription', ['doctor_id' => $doctor_id])

<div class="clearfix"></div>

<div class="col-md-8 no-padding" style="margin-top:15px;margin-bottom:15px;">
    <h5><b>NEXT REVIEW :</b></h5>
    <input type="text" data-attr="date" autocomplete="off" name="" autofocus="" value="@if(@isset($data['next_review_date'])){{$data['next_review_date']}} @endif" class="form-control datepicker table_text next_review_date" placeholder="MMM-DD-YYYY" style="">
</div>
