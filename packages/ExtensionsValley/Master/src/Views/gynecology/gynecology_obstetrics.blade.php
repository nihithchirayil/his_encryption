
<div class="col-md-12 no-padding">
    
    <div class="col-md-10">
        <h5><b>OBSTETRIC EXAMINATION</b></h5>
    </div>
    <div class="col-md-2 padding_sm obstetricsHistoryContainer" style="text-align:right;">
        <button onclick="fetchObstetricsHistory('{{ $visit_date }}');" type="button" class="btn btn-sm btn-warning" > <i class="fa fa-list fetchObsterticsIcon" > </i>  OBSTETRIC HISTORY</button>
        <div class="popover left in" role="tooltip" style="top: -180px; left: -975px; display: none;">
            <div class="arrow" style="top: 60%;"></div>
            <div class="popover-content">

            </div>
        </div>
    </div>
</div>

<div class="col-md-12 " style="margin-top:15px;">
    <table class="table no-margin table-bordered table-condensed obstetric_examination_table" style="border: 1px solid #CCC;">
        <thead>
            <tr class="headder_bg">
                <th>Gestational Age</th>
                <th>Height</th>
                <th>Weight</th>
                <th>BP</th>
                <th>Fundal Height</th>
                <th>Presentation</th>
                <th>Engaged or Not Engaged</th>
                <th>FHS</th>
                <th>Remarks</th>
            </tr>
        </thead>
        <tbody class="obtetric_examination_table_body">
            @foreach($data['obstetric_examination'] as $obstetrics )
            <tr>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($obstetrics['gestation_age'])) {{$obstetrics['gestation_age']}} @endif" class="form-control table_text gestation_age" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($obstetrics['height'])) {{$obstetrics['height']}} @endif" class="form-control table_text height" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($obstetrics['weight'])) {{$obstetrics['weight']}} @endif" class="form-control table_text weight" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($obstetrics['bp'])) {{$obstetrics['bp']}} @endif" class="form-control table_text bp" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($obstetrics['fundal_height'])) {{$obstetrics['fundal_height']}} @endif" class="form-control table_text fundal_height" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($obstetrics['presentation'])) {{$obstetrics['presentation']}} @endif" class="form-control table_text presentation" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($obstetrics['engaged_or_not_engaged'])) {{$obstetrics['engaged_or_not_engaged']}} @endif" class="form-control table_text engaged_or_not_engaged" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($obstetrics['fhs'])) {{$obstetrics['fhs']}} @endif" class="form-control table_text fhs" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="@if(@isset($obstetrics['remarks'])) {{$obstetrics['remarks']}} @endif" class="form-control table_text remarks" placeholder="" id="" style="">
                </td>
            </tr>
            
            @endforeach
            <tr>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text gestation_age" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text height" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text weight" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text bp" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text fundal_height" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text presentation" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text engaged_or_not_engaged" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text fhs" placeholder="" id="" style="">
                </td>
                <td>
                    <input type="text" autocomplete="off" name="" autofocus="" value="" class="form-control table_text remarks" placeholder="" id="" style="">
                </td>
            </tr>
        </tbody>
    </table>
</div>
