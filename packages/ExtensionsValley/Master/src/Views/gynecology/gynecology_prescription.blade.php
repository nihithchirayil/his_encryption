
<div class="col-md-12 no-padding">
    <div class="col-md-10">
        <button type="button" class="btn btn-sm btn-primary showPrescriptionFormBtn ">  Prescription </button>
    </div>
    <div class="col-md-2 padding_sm PrescriptionHistoryContainer" style="text-align:right;">
        <button onclick="fetchPrescriptionHistory('{{ $visit_date }}');" type="button" class="btn btn-sm btn-warning" > 
            <i class="fa fa-list fetchPrescriptionIcon"></i>  PRESCRIPTION HISTORY
        </button>
        <div class="popover left in" role="tooltip" style="top: -50px; left: -1000px; display: none;">
            <div class="arrow" style="top: 60%;"></div>
            <div class="popover-content">

            </div>
        </div>
    </div>
</div>
