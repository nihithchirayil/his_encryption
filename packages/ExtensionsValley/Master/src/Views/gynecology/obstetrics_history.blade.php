<div class="theadscroll" style="position: relative; height: 300px;">
    <table class="table no-margin table-bordered table-condensed theadfix_wrapper obstetric_examination_history_table" style="border: 1px solid #CCC;">
        <thead>
            <tr class="headder_bg">
                <th>Date</th>
                <th>Gestational Age</th>
                <th>Height</th>
                <th>Weight</th>
                <th>BP</th>
                <th>Fundal Height</th>
                <th>Presentation</th>
                <th>Engaged or Not Engaged</th>
                <th>FHS</th>
                <th>Remarks</th>
            </tr>
        </thead>
        <tbody>
            @foreach($obstetrics_examination as $obstetrics )
            <tr>
                <td>@if(@isset($obstetrics['obstetric_date'])) {{$obstetrics['obstetric_date']}} @endif </td>
                <td>@if(@isset($obstetrics['gestation_age'])) {{$obstetrics['gestation_age']}} @endif</td>
                <td>@if(@isset($obstetrics['height'])) {{$obstetrics['height']}} @endif</td>
                <td>@if(@isset($obstetrics['weight'])) {{$obstetrics['weight']}} @endif</td>
                <td>@if(@isset($obstetrics['bp'])) {{$obstetrics['bp']}} @endif</td>
                <td>@if(@isset($obstetrics['fundal_height'])) {{$obstetrics['fundal_height']}} @endif</td>
                <td>@if(@isset($obstetrics['presentation'])) {{$obstetrics['presentation']}} @endif</td>
                <td>@if(@isset($obstetrics['engaged_or_not_engaged'])) {{$obstetrics['engaged_or_not_engaged']}} @endif</td>
                <td>@if(@isset($obstetrics['fhs'])) {{$obstetrics['fhs']}} @endif</td>
                <td>@if(@isset($obstetrics['remarks'])) {{$obstetrics['remarks']}} @endif</td>
            </tr>
            
            @endforeach
        </tbody>
    </table>
</div>
