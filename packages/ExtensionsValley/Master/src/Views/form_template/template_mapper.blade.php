@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template.css")}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
    rel="stylesheet">
<style>
    td.common_td_rules {
        text-align: left !important;
        font-size: 12px;
    }

    .tab-panel li {
        width: 30% !important;
    }
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<div class="right_col">
    <div class="row no-padding">
        <div class="col-md-12 box-body" style="height:84vh !important;">
            <div class="col-md-3">
                <label>Templates</label>
                <div class="clearfix"></div>
                <select class="form-control select2" id="template_list" name="template_list">
                    <option value="0">Select</option>
                    @foreach($saved_templates as $temp)
                    <option value="{{$temp->id}}">{{$temp->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="clearfix"></div>
            
            <div class="col-md-12" style="margin-top: 20px;">
                <div class="tab-panel" role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#tab_doctor" aria-controls="tab_doctor" role="tab" data-toggle="tab">
                                Doctor Mapping
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#tab_speciality" aria-controls="tab_speciality" role="tab" data-toggle="tab">
                                Speciality Mapping
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tab_doctor">
                            <div class="col-md-12" id="template_mapping_content" style="margin-top: 10px;">
                    
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab_speciality">
                            <div class="col-md-12" id="speciality_mapping_content" style="margin-top: 10px;">
                    
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- <div class="col-md-12" id="template_mapping_content" style="margin-top: 10px;">
                
            </div> -->
        </div>
    </div>
</div>
@stop
@section('javascript_extra')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{asset("packages/extensionsvalley/master/form_template/js/form_template_mapper.js")}}"></script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
@endsection