<div class="col-md-{{$res->size}} sortable" draggable="true" ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);">
    <i class="fa fa-times-circle" aria-hidden="true"></i>
    <fieldset style="min-height:{{$res->container_min_height}}px;" class="scheduler-border">
        <legend class="scheduler-border"><span class="contenteditable">Patient Allergy</span></legend>
        <div class="col-md-12 no-padding" id="static_component_allergy_form">
            <div class="col-md-6 no-padding"
                style="min-height: 320px; border-right:1px solid #e5e5e5; padding-left:5px;">
                <div class="col-md-12">
                    <strong> Medicines </strong>
                </div>
                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="medicationAllergyData">
                    <div id="" class="col-md-4 padding_xs">
                        <div class="btn-group btn-group-toggle gen_exam_acne" data-toggle="buttons">
                            <label class="btn bg-radio_grp">
                                <input type="radio" name="sel_t" id="type1"  value="0"> Brand
                            </label>
                            <label class="btn bg-radio_grp active">
                                <input type="radio" name="sel_t"  id="type2" value="1" checked="checked"> Generic
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2 pull-right" style="margin-right:-15px;">
                        <button type="button" data-toggle="tooltip" data-placement="top" title="" class="btn btn-success addNewRowItem" data-original-title="Add Medicine" onclick="addRow()"><i class="fa fa-plus fa-lg "></i></button>
                    </div>
                    <div class="clearfix"></div>
                    <div class="h10"></div>
                    <div class="h10"></div>
                    <div class="col-md-12"><hr></div>
                    {!! Form::open(['name'=>'formallergyprescription','id'=>'formallergyprescription','method'=>'post']) !!}
                    <table id="allergymedicTable" class="table table_sm no-border">
                        <tbody id="MedicineAllergyListData" class="AllergyListData">
                            @for ($j = 1; $j <= 3; $j++)
                            <tr style="cursor:pointer">
                                <td width="90%">
                                    <input type="text" class="form-control bottom-border-text  btn-without-border" placeholder="" value="" autocomplete="off" onkeyup="searchMedicine2(this.id,event)" name="allergymedicine[]" id="allergymedicine-{{$j}}">
    
                                    <div class="ajaxSearchBox" id="alergymedicineAjaxList-{{$j}}"
                                        style="width:100%;max-height: 515px; display:none;"></div>
                                        <input type="hidden" value="" id="AllergymedCodeId-{{ $j }}" name="alergylist_medicine_code_hidden[]" >
                                        <input type='hidden' name="alrg_type[]" id="alrg_type-{{ $j }}">
                                </td>
                                <td>
                                <i class='fa fa-times-circle' aria-hidden='true' style="/*color:#FFFF;*/font-size: 23px; margin: 7px;" onclick="deleteNews(this)" id="delete-{{ $j }}"></i>
                                </td>
                            </tr>
                            @endfor
                        </tbody>
                    </table>
                    {!! Form::token() !!}
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-6 no-padding" style="min-height: 320px;">
                <div class="col-md-12">
                    <strong> Others </strong>
                </div>
                <div class="otherAllergyData">
                    <div class="padding_sm">
                        <label for="allergy">Allergy Description:</label>
                        <br>
                        <textarea class="form-control otherAllergies" id="otherAllergies" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
</div>