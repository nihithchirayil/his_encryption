{{-- ########################### STATIC COMPONENT ##########################################3#### --}}
@if ($res->is_static_component == 1)
    @if ($res->name == 'ip_patient_header')
        <div class="col-md-{{ $res->size }} sortable data_id_control_id box-body patient-header" draggable="true"
            ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
            <i class="fa fa-times-circle" aria-hidden="true"
                style="margin-right: -12px !important;margin-top: -10px !important;"></i>
            <div class="col-md-3">
                <label>Uhid</label>
                <div class="clearfix"></div>
                <input id="template_uhid" name="template_uhid" class="form-control text-form-input" type="text"
                    disabled="disabled" />
            </div>

            <div class="col-md-3">
                <label>Patient Name</label>
                <div class="clearfix"></div>
                <input id="template_name" name="template_name" class="form-control text-form-input" type="text"
                    disabled="disabled" />
            </div>
            <div class="col-md-3">
                <label>Age/Gender</label>
                <div class="clearfix"></div>
                <input id="template_age_gender" name="template_age_gender" class="form-control text-form-input"
                    type="text" disabled="disabled" />
            </div>
            <div class="col-md-3">
                <label>Phone</label>
                <div class="clearfix"></div>
                <input id="template_phone" name="template_phone" class="form-control text-form-input" type="text"
                    disabled="disabled" />
            </div>

            <div class="clearfix"></div>

            <div class="col-md-3">
                <label>Admission No</label>
                <div class="clearfix"></div>
                <input id="template_admission_no" name="template_admission_no" class="form-control text-form-input"
                    type="text" disabled="disabled" />
            </div>


            <div class="col-md-3">
                <label>Admission Date</label>
                <div class="clearfix"></div>
                <input id="template_admission_date" name="template_admission_date" class="form-control text-form-input"
                    type="text" disabled="disabled" />
            </div>
            <div class="col-md-3">
                <label>Admitted Doctor</label>
                <div class="clearfix"></div>
                <input id="template_admitting_doctor" name="template_admitting_doctor"
                    class="form-control text-form-input" type="text" disabled="disabled" />
            </div>
            <div class="col-md-3">
                <label>Ward/Room</label>
                <div class="clearfix"></div>
                <input id="template_admitting_ward" name="template_admitting_ward" class="form-control text-form-input"
                    type="text" disabled="disabled" />
            </div>
        </div>
    @elseif($res->name == 'patient_header')
        <div class="col-md-{{ $res->size }} sortable data_id_control_id box-body patient-header" draggable="true"
            ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
            <i class="fa fa-times-circle" aria-hidden="true"
                style="margin-right: -12px !important;margin-top: -10px !important;"></i>

            <div class="col-md-3">
                <label>Uhid</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" disabled="disabled" />
            </div>
            <div class="col-md-3">
                <label>Patient Name</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" disabled="disabled" />
            </div>
            <div class="col-md-3">
                <label>Age/Gender</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" disabled="disabled" />
            </div>
            <div class="col-md-3">
                <label>Phone</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" disabled="disabled" />
            </div>

            <div class="clearfix"></div>

            <div class="col-md-3">
                <label>Address</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" disabled="disabled" />
            </div>
            <div class="col-md-3">
                <label>Area</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" disabled="disabled" />
            </div>
            <div class="col-md-3">
                <label>Visit Date</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" disabled="disabled" />
            </div>
            <div class="col-md-3">
                <label>Consulting Doctor</label>
                <div class="clearfix"></div>
                <input class="form-control text-form-input" type="text" disabled="disabled" />
            </div>

        </div>
    @elseif($res->name == 'investigation')
        <div class="col-md-{{ $res->size }} sortable data_id_control_id box-body patient-header" draggable="true"
            ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);"
            style="min-height:{{ $res->container_min_height }}px;" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
            <i class="fa fa-times-circle" aria-hidden="true"></i>
            <label>{{ $res->label }}</label>
        </div>
    @elseif($res->name == 'medication')
        <div class="col-md-{{ $res->size }} sortable data_id_control_id  box-body patient-header" draggable="true"
            ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);"
            style="min-height:{{ $res->container_min_height }}px;" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
            <i attr_control_id="{{ $res->id }}"
                attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}"
                class="fa fa-times-circle" aria-hidden="true"></i>
            <label>{{ $res->label }}</label>
        </div>
    @elseif($res->name == 'vital_master')
        @include('Master::form_template.vital_master')
    @elseif($res->name == 'patient_allergy_master')
        @include('Master::form_template.allergy_master')
    @elseif($res->name == 'pain_assessment_scale')
        @include('Master::form_template.pain_scale_score_master')
    @elseif($res->name == 'image_editor')
        @include('Master::form_template.image_editor')
    @endif
@endif

{{-- textbox --}}
@if ($res->control_id == 1)

    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}" attr_data_point="{{$res->data_point}}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <label>{{ $res->label }}</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            <input type="text" autocomplete="off" class="form-control text-form-input"
                name="{{ $res->id_name }}" id="{{ $res->id_name }}" placeholder="{{ $res->placeholder }}"
                value="{{ $res->default_value }}"
                @if ($res->is_favourite == '1') data-toggle="popover" data-trigger="focus" data-content=""  data-original-title="Favourites" @endif />
            @if ($res->is_favourite == '1')
                <a class="btn btn-sm btn-default template-fav-button" id="btn_{{ $res->name }}"
                    onclick="addToDynamicBookmark('{{ $res->name }}');" title="Add To Bookmark"
                    style="position: absolute;right: 5px;bottom: -15px;"><i id="i_{{ $res->name }}"
                        class="fa fa-star-o"></i></a>
            @endif
        </span>
    </div>

    {{-- Text Area --}}
@elseif($res->control_id == 2)
    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}" attr_data_point="{{$res->data_point}}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <label>{{ $res->label }}</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            <textarea class="form-control textarea-form-input" autocomplete="off" name="{{ $res->id_name }}"
                id="{{ $res->id_name }}" style="height:{{ $res->container_min_height }}px !important;"
                placeholder="{{ $res->placeholder }}"
                @if ($res->is_favourite == '1') data-toggle="popover" data-trigger="focus" data-content=""  data-original-title="Favourites" @endif>{{ $res->default_value }}</textarea>
            @if ($res->is_favourite == '1')
                <a class="btn btn-sm btn-default template-fav-button" id="btn_{{ $res->name }}"
                    onclick="addToDynamicBookmark('{{ $res->name }}');" title="Add To Bookmark"
                    style="position: absolute;right: 5px;bottom: -15px;"><i id="i_{{ $res->name }}"
                        class="fa fa-star-o"></i></a>
            @endif
        </span>
    </div>

    {{-- Checkbox --}}
@elseif($res->control_id == 3)
    <div class="btn-group col-md-{{ $res->size }} sortable data_id_control_id" data-toggle="buttons" draggable="true"
        ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}" >
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <label>{{ $res->label }}</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            <ul class="select_button no-padding check-form-input">
                @php
                    $json_value = $res->group_value;
                    // dd($json_value);
                    $check_array = json_decode($json_value);
                    $activeclass = '';
                @endphp
                @foreach ($check_array as $check => $check_value)
                    @if ($check == $res->default_value)
                        @php
                            $activeclass = 'active';
                        @endphp
                    @else
                        @php
                            $activeclass = '';
                        @endphp
                    @endif

                    <li data-value="{{ $check_value }}" class="{{ $activeclass }}">
                        <i class="fa fa-check-circle"></i> {{ $check_value }}
                        <input type="hidden" name="{{ $res->name }}"
                            id="{{ $res->id_name }}_{{ $check }}" value="{{ $check_value }}">
                    </li>
                @endforeach
            </ul>
        </span>
    </div>



    {{-- Radio button --}}
@elseif($res->control_id == 4)
    <div class="btn-group col-md-{{ $res->size }} sortable data_id_control_id" data-toggle="buttons" draggable="true"
        ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <label>{{ $res->label }}</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            @php
                $json_value = $res->group_value;
                $radio_array = json_decode($json_value);
                // dd($json_value);
                $activeclass = '';
            @endphp
            @foreach ($radio_array as $radio => $radio_value)
                @if ($radio == $res->default_value)
                    @php
                        $activeclass = 'active';
                    @endphp
                @else
                    @php
                        $activeclass = '';
                    @endphp
                @endif
                <label class="btn bg-blue {{ $activeclass }} radio_label">
                    <input type="radio" class="radio-form-input" value="{{ $radio }}"
                        name="{{ $res->name }}" id="{{ $res->id_name }}"
                        @if ($activeclass != '') checked @endif>{{ $radio_value }}
                </label>
            @endforeach
        </span>

    </div>

    {{-- Select box --}}
@elseif($res->control_id == 5)
    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <label>{{ $res->label }}</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            <select class="form-control select2 select-form-input" name="{{ $res->id_name }}"
                id="{{ $res->id_name }}" placeholder="{{ $res->placeholder }}">
                @php
                    $result_data = \DB::select($res->sql);
                @endphp
                @if (!empty($result_data))
                    @for ($i = 0; $i < sizeof($result_data); $i++)
                        <option @if ($res->default_value == $result_data[$i]->id) selected @endif value="{{ $result_data[$i]->id }}">
                            {{ $result_data[$i]->name }}</option>
                    @endfor
                @endif
            </select>
        </span>
    </div>


    {{-- Multiple Select box --}}
@elseif($res->control_id == 6)
    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <label>{{ $res->label }}</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            <select class="form-control select2 multiselect-form-input" multiple name="{{ $res->id_name }}"
                id="{{ $res->id_name }}" placeholder="{{ $res->placeholder }}">
                @php
                    $result_data = \DB::select($res->sql);
                @endphp
                @if (!empty($result_data))
                    @for ($i = 0; $i < sizeof($result_data); $i++)
                        <option @if ($res->default_value == $result_data[$i]->id) selected @endif value="{{ $result_data[$i]->id }}">
                            {{ $result_data[$i]->name }}</option>
                    @endfor
                @endif
            </select>
        </span>
    </div>

    {{-- Header --}}
@elseif($res->control_id == 7)
    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <h4 class="header-class">{{ $res->label }}</h4>
    </div>
    <div class="clearfix"></div>

    {{-- Datepicker --}}
@elseif($res->control_id == 8)
    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <label>{{ $res->label }}</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            <input type="text" class="form-control datepicker datepicker-form-input" name="{{ $res->id_name }}"
                id="{{ $res->id_name }}" placeholder="{{ $res->placeholder }}"
                value="@if ($res->default_value == 'current_date') {{ date('M-d-Y') }} @endif " />
        </span>
    </div>

    {{-- Timepicker --}}
@elseif($res->control_id == 9)
    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <label>{{ $res->label }}</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            <input type="text" class="form-control timepicker timepicker-form-input" name="{{ $res->id_name }}"
                id="{{ $res->id_name }}" placeholder="{{ $res->placeholder }}"
                value="@if ($res->default_value == 'current_date_time') {{ date('h:i A') }} @endif " />
        </span>
    </div>

    {{-- Datetimepicker --}}
@elseif($res->control_id == 10)
    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <label>{{ $res->label }}</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            <input type="text" class="form-control datetimepicker datetimepicker-form-input"
                name="{{ $res->id_name }}" id="{{ $res->id_name }}" placeholder="{{ $res->placeholder }}"
                value="@if ($res->default_value == 'current_datetime') {{ date('M-d-Y h:i A') }} @endif " />
        </span>
    </div>


    {{-- Progressive search --}}
@elseif($res->control_id == 11)
    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <label>{{ $res->label }}</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            <input type="text" class="form-control progressive-form-input-text" autocomplete="off"
                name="{{ $res->id_name }}" id="{{ $res->id_name }}" placeholder="{{ $res->placeholder }}"
                value="" />
            <div id="{{ $res->name }}AjaxDiv" class="ajaxSearchBox"></div>
            <input class="progressive-form-input-hidden" type="hidden" name="{{ $res->name }}_hidden"
                value="" id="{{ $res->name }}_hidden">
        </span>
    </div>

    {{-- TinyMce --}}
@elseif($res->control_id == 13)
    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <label>{{ $res->label }}</label>
        <div class="clearfix"></div>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            <textarea id="{{ $res->id_name }}" name="{{ $res->id_name }}" class="tiny_class tiny-form-input"></textarea>
        </span>
    </div>

    {{-- Table --}}
@elseif($res->control_id == 14)
    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        @php
            $headline_array = $res->colum_names;
            $headline_array = explode(',', $headline_array);

        @endphp
        <table class="table table-striped table-bordered">
            <thead>
                @if($res->table_header!='')
                <tr style="background: cornflowerblue;color:white;">
                    <td colspan="{{$res->colum_count}}"><h6 style="text-align:center;font-weight:600;">{{$res->table_header}}</h6></td>
                </tr>

                @endif
            </thead>
            <tbody>
                @for ($i = 0; $i <= $res->row_count; $i++)
                    <tr>
                    @for($j=0;$j<$res->colum_count;$j++)
                        @if($i==0)
                            <td style="color:#807e7e !important;">
                                <h6 style="font-weight: 600;">{{$headline_array[$j]}}</h6>
                            </td>
                        @else
                            <td style="padding:0px !important;">
                                <span class="show_print" id="show_print_{{$res->id_name}}_{{$i}}_{{$j}}"></span>
                                <span class="hide_print">
                                    <input name="{{$res->id_name}}_{{$i}}_{{$j}}" id="{{$res->id_name}}_{{$i}}_{{$j}}" style="margin:0px !important;background:none !important;" type="text" class="form-control table-form-input"/>
                                </span>
                            </td>
                        @endif
                    @endfor
                    </tr>
                @endfor
            </tbody>
        </table>
    </div>

    {{-- Html Container --}}
@elseif($res->control_id == 15)
    <div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
        ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <fieldset style="min-height:{{ $res->container_min_height }}px;" class="scheduler-border">
            <legend class="scheduler-border"><span class="contenteditable">{{ $res->label }}</span></legend>
            <div style="min-height:{{ $res->container_min_height }}px;" class="control-group dest_copy sortable"
                ondragover="dragover_handler(event);" draggable="true" ondragstart="dragstart_handler(event);"
                ondragend="dragend_handler(event);">

            </div>
        </fieldset>
    </div>

    {{-- Html Container --}}
@elseif($res->control_id == 17)
    <div class="btn-group col-md-{{ $res->size }} sortable data_id_control_id" data-toggle="buttons" draggable="true"
        ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
        <span class="show_print" id="show_print_{{ $res->id_name }}"></span>
        <span class="hide_print">
            <div id="component_{{ $res->id_name }}_box" class="form_grid_component">
                <div class="col-md-12" style="display:flex;">
                    <div class="collapsible_text_box_header" contenteditable="true"> {{ $res->label }} </div>
                    <button class="btn txt_box_collapsible" type="button" data-toggle="collapse"
                        data-target="#{{ $res->id_name }}_collapse" aria-expanded="false"
                        aria-controls="{{ $res->id_name }}_collapse">
                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="col-md-12">
                    <div style="min-height:{{ $res->container_min_height }}px; border:1px solid #f0f0f0"
                        id="{{ $res->id_name }}_collapse" class="control-group dest_copy sortable collapse"
                        ondragover="dragover_handler(event);" draggable="true"
                        ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);">

                    </div>
                </div>
            </div>
        </span>
    </div>

{{-- Label --}}
@elseif($res->control_id == 18)
<div class="col-md-{{ $res->size }} sortable data_id_control_id" draggable="true" ondragstart="dragstart_handler(event);"
    ondragend="dragend_handler(event);" attr_control_id="{{ $res->id }}" attr_dataset_id="{{ $res->data_set_id ? $res->data_set_id : 0 }}">
    <i class="fa fa-times-circle" aria-hidden="true"></i>
    <label class="label-class" contenteditable="true">{{ $res->label }}</label>
</div>
<div class="clearfix"></div>

@endif
