@if($active_tab == '#tab_doctor')
<div class="theadscroll" style="position: relative; height: 61vh; width: 55vw; border-bottom: 2px solid #01987a;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead style="position: sticky;top: 0;">
            <tr class="table_header_bg">
                <th width="60%">Doctor Name</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        <tbody>
            @if (count($result) > 0)
                @foreach ($result as $row)
                    <tr>
                        <td class="common_td_rules">
                            {{ $row->doctor_name }}
                        </td>
                        <td> 
                            <input type="checkbox" id="templateMapping_{{$row->doctor_id}}" onclick="saveTemplateMaping(this, '{{$active_tab}}');" style="cursor: pointer;height: 18px;  display: block; width: 100%; box-shadow: none;" doctor_id={{$row->doctor_id}} @if($row->status == 1) checked="true" @endif >
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
@elseif($active_tab == '#tab_speciality')
<div class="theadscroll" style="position: relative; height: 61vh; width: 55vw; border-bottom: 2px solid #01987a;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead style="position: sticky;top: 0;">
            <tr class="table_header_bg">
                <th width="60%">Speciality</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        <tbody>
            @if (count($result) > 0)
                @foreach ($result as $row)
                    <tr>
                        <td class="common_td_rules">
                            {{ $row->speciality }}
                        </td>
                        <td> 
                            <input type="checkbox" id="templateMapping_{{$row->speciality_id}}" onclick="saveTemplateMaping(this, '{{$active_tab}}');" style="cursor: pointer;height: 18px;  display: block; width: 100%; box-shadow: none;" speciality_id={{$row->speciality_id}} @if($row->status == 1) checked="true" @endif >
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
@endif