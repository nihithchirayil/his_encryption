<div class="btn-group col-md-{{$res->size}} sortable" data-toggle="buttons" draggable="true" ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);">
    <i class="fa fa-times-circle" aria-hidden="true"></i>
    <label>PAIN ASSESSMENT SCALE</label>
    <div class="clearfix"></div>
    <span class="show_print" id="show_print_radio"></span>
    <span class="hide_print">
        <div id="static_component_pain_assessment_scale" style="display: flex;">
            <div class="pain_scale_radio">
                <input type="radio" id="option_0" name="pain_assessment_scale" value="0">
                <label for="option_0" class="radio-image">
                    <img class="pain_scale_smiley" src="##smily0##">
                    <span>0 <br> No <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_1" name="pain_assessment_scale" value="1">
                <label for="option_1" class="radio-image">
                    <img class="pain_scale_smiley" src="##smily1##">
                    <span>1 <br> Just <br> noticeable</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_2" name="pain_assessment_scale" value="2">
                <label for="option_2" class="radio-image">
                    <img class="pain_scale_smiley" src="##smily2##">
                    <span>2 <br> Mild <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_3" name="pain_assessment_scale" value="3">
                <label for="option_3" class="radio-image">
                    <img class="pain_scale_smiley" src="##smily3##">
                    <span>3 <br> Uncomfortable <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_4" name="pain_assessment_scale" value="4">
                <label for="option_4" class="radio-image">
                    <img class="pain_scale_smiley" src="##smily4##">
                    <span>4 <br> Annoying <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_5" name="pain_assessment_scale" value="5">
                <label for="option_5" class="radio-image">
                    <img class="pain_scale_smiley" src="##smily5##">
                    <span>5 <br> Moderate <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_6" name="pain_assessment_scale" value="6">
                <label for="option_6" class="radio-image">
                    <img class="pain_scale_smiley" src="##smily6##">
                    <span>6 <br> Just <br> bearable</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_7" name="pain_assessment_scale" value="7">
                <label for="option_7" class="radio-image">
                    <img class="pain_scale_smiley"  src="##smily7##">
                    <span>7 <br> Strong <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_8" name="pain_assessment_scale" value="8">
                <label for="option_8" class="radio-image">
                    <img class="pain_scale_smiley" src="##smily8##">
                    <span>8 <br> Severe <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_9" name="pain_assessment_scale" value="9">
                <label for="option_9" class="radio-image">
                    <img class="pain_scale_smiley" src="##smily9##">
                    <span>9 <br> Horrible <br> pain</span>
                </label>
            </div>
            <div class="pain_scale_radio">
                <input type="radio" id="option_10" name="pain_assessment_scale" value="10">
                <label for="option_10" class="radio-image">
                    <img class="pain_scale_smiley" src="##smily10##">
                    <span>10 <br> Worst <br> pain</span>
                </label>
            </div>
        </div>
    </span>
</div>
