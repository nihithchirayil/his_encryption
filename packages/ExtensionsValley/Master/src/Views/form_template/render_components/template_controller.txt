<?php

namespace ExtensionsValley\Master;

use ExtensionsValley\Dashboard\Models\traits\DashboardTraits;
use ExtensionsValley\Dashboard\Models\WebSettings;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use ExtensionsValley\Master\Validators\CommonMasterValidation;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use ExtensionsValley\Emr\EncounterVisits;

class ######TEMPLATE_NAME######Controller extends Controller {
    public function ######FUNCTION_NAME######(Request $request){
        $title = '######TEMPLATE_NAME_FOR_BLADE######';
        $viewData = \View::make('Master::dynamic_templates/######BLADE NAME######',compact('title'));
        return $viewData;
    }

    public function ######FUNCTION_NAME######Save(Request $request){
        $patient_id = $request->has('patient_id') ? $request->get('patient_id'):0;
        $visit_id = $request->has('visit_id') ? $request->get('visit_id'):0;
        $doctor_id = $request->has('doctor_id') ? $request->get('doctor_id'):0;
        $input_data = $request->has('input_data') ? $request->get('input_data'):'';
        $input_array = $request->has('input_array') ? $request->get('input_array'):'';
        $name_array = $request->has('name_array') ? $request->get('name_array'):'';
        $template_data_id = 0;

        $dynamic_template_id = $request->has('dynamic_template_id') ? $request->get('dynamic_template_id'):0;
        $encounter_id = '';
        $user_id =  session()->get('USER_ID') ?? 0;
        if ($encounter_id == "") {
            $encounter_id = EncounterVisits::createOrSelectEncounter($doctor_id, $patient_id, $visit_id, $user_id);
        }

        $primary_key_index_array = [
            'patient_id'=>$patient_id,
            'visit_id'=>$visit_id,
            'doctor_id'=>$doctor_id,
            'encounter_id'=>$encounter_id,
        ];

        $dataParams = [
            'doctor_id'=>$doctor_id,
            'visit_id'=>$visit_id,
            'patient_id'=>$patient_id,
            'encounter_id'=>$encounter_id,
            'input_data'=>$input_data,
            'template_id'=>$dynamic_template_id,
            'updated_by'=>$user_id,
            'updated_at'=>date('Y-m-d H:i:s'),
            'deleted_at'=>null,
            'deleted_by'=>null,
        ];

        if(\DB::table('dynamic_template_data')
            ->where('encounter_id',$encounter_id)->where('template_id',$dynamic_template_id)->exists()){
            $status = \DB::table('dynamic_template_data')
                        ->where('encounter_id',$encounter_id)
                        ->where('template_id',$dynamic_template_id)->update($dataParams);

            $template_data_id = \DB::table('dynamic_template_data')
            ->where('encounter_id',$encounter_id)->where('template_id',$dynamic_template_id)->value('id');
        }else{
            $dataParams['created_by'] = $user_id;
            $dataParams['created_at'] = date('Y-m-d H:i:s');
            $template_data_id =  \DB::table('dynamic_template_data')->insertGetId($dataParams);
            if($template_data_id !=0){
                $status = 1;
            }
        }

        if(!empty($name_array)){
            for($i=0;$i<count($name_array);$i++){

                $data_saving_info = \DB::table('template_control')->where('id_name',$name_array[$i])
                ->select('data_saving_table','data_saving_colum','data_saving_primary_key','compare_key_in_emr','data_point','id_name')->first();

                if(!empty($data_saving_info)){
                    $data_saving_table = $data_saving_info->data_saving_table;
                    $data_saving_colum = $data_saving_info->data_saving_colum;
                    $data_saving_primary_key = $data_saving_info->data_saving_primary_key;
                    $compare_key_in_emr = $data_saving_info->compare_key_in_emr;

                    if(!empty($data_saving_table) && !empty($data_saving_colum) && !empty($data_saving_primary_key) && !empty($compare_key_in_emr)){
                        $input_data_array = json_decode($input_data);
                        foreach($input_data_array as $key=>$value){

                            if($input_array[$i] == $key){
                                if(\DB::table($data_saving_table)->where($data_saving_primary_key,$primary_key_index_array[$compare_key_in_emr])->exists()){
                                    try{

                                        $update_data = \DB::table($data_saving_table)->where($data_saving_primary_key,$primary_key_index_array[$compare_key_in_emr])->update([$data_saving_colum=>$value]);

                                    }catch (\Exception $e){
                                        $update_data = 1;
                                    }

                                }elseif($data_saving_table == 'patient_clinical_data'){
                                    try{
                                        $dataparams = [
                                            'patient_id'=>$patient_id,
                                            'created_by'=>$user_id,
                                            'updated_by'=>$user_id,
                                            'created_at'=>date('Y-m-d H:i:s'),
                                            'updated_at'=>date('Y-m-d H:i:s'),
                                            $data_saving_colum=>$value
                                        ];
                                        $update_data = \DB::table($data_saving_table)->insert($dataparams);
                                    }catch (\Exception $e){
                                        $update_data = 1;
                                    }
                                }
                            }
                        }
                    }

                }


                //------data point save----------
                    $data_point = $data_saving_info->data_point;
                    $input_data_array = json_decode($input_data);

                    foreach($input_data_array as $key=>$value){

                        if(($input_array[$i]) == $key && ($input_array[$i] == $data_saving_info->id_name)){
                            $dataparams = [
                                'patient_id'=>$patient_id,
                                'visit_id'=>$visit_id,
                                'code'=>$data_point,
                                'entry_data'=>$value,
                                'dynamic_data_id'=>$template_data_id,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'),
                                'created_by'=>$user_id,
                                'updated_by'=>$user_id,
                            ];

                            $status = \DB::table('dynamic_template_data_point_data')->insert($dataparams);
                        }
                    }

            }
        }

        if(!empty($input_array)){

            for($i=0;$i<count($input_array);$i++){

                $data_saving_info = \DB::table('template_control')->where('id_name',$input_array[$i])
                ->select('data_saving_table','data_saving_colum','data_saving_primary_key','compare_key_in_emr')->first();

                if(!empty($data_saving_info)){
                    $data_saving_table = $data_saving_info->data_saving_table;
                    $data_saving_colum = $data_saving_info->data_saving_colum;
                    $data_saving_primary_key = $data_saving_info->data_saving_primary_key;
                    $compare_key_in_emr = $data_saving_info->compare_key_in_emr;
                    if(($data_saving_table !='') && ($data_saving_colum !='') && ($data_saving_primary_key !='') && ($compare_key_in_emr !='')){

                        $input_data_array = json_decode($input_data);

                        foreach($input_data_array as $key=>$value){

                            if($input_array[$i] == $key){


                                if(\DB::table($data_saving_table)->where($data_saving_primary_key,$primary_key_index_array[$compare_key_in_emr])->exists()){

                                    try{

                                        $update_data = \DB::table($data_saving_table)->where($data_saving_primary_key,$primary_key_index_array[$compare_key_in_emr])->update([$data_saving_colum=>$value]);

                                    }catch (\Exception $e){
                                        $update_data = 1;
                                    }

                                }elseif($data_saving_table == 'patient_clinical_data'){

                                    try{

                                        $dataparams = [
                                            'patient_id'=>$patient_id,
                                            'created_by'=>$user_id,
                                            'updated_by'=>$user_id,
                                            'created_at'=>date('Y-m-d H:i:s'),
                                            'updated_at'=>date('Y-m-d H:i:s'),
                                            $data_saving_colum=>$value
                                        ];

                                        $update_data = \DB::table($data_saving_table)->insert($dataparams);

                                    }catch (\Exception $e){
                                        $update_data = 1;
                                    }


                                }

                            }
                        }
                    }
                }

            }
        }

        if($status){
            $returnData=[
                'template_data_id'=>$template_data_id,
            ];
        }else{
            $returnData=[
                'template_data_id'=>0,
            ];
        }

        return $returnData;
    }
}
