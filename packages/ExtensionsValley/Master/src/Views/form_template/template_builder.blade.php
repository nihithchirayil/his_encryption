@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/form_template/css/form_template.css")}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.6.95/css/materialdesignicons.css" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<div class="right_col" style="min-height:765px !important;">
    <div class="col-md-12 no-padding">
        <div class="col-md-2" style="position:sticky;top:0;">
            <div class="col-md-12 box-body no-padding control-container" style="height:101vh !important;">
            @php
            $template_controler = collect($template_controler)->groupBy('control_name');
            @endphp
                <h5  style="margin-bottom: 5px !important;">Html Controls</h5>
                <input type="text" class="form-control" id="myInput" onkeyup="myFunction();" placeholder="Search.." title="Type name for search control">

                <button type="button"  style="margin-top:-78px;" onclick="ReloadHtmlControls();" class="btn btn-sm bg-info pull-right"><i id="fa_reset_controls" class="fa fa-refresh"></i></button>

                <div class="col-md-12 no-padding theadscroll" id="control_block" style="position:absolute;height:617px !important;padding-right:13px !important;">
                    <ul class="html-components" id="html_components">
                        @foreach($template_controler as $key => $datas)
                        <li style="color: #c73838;margin: 2px 0;"><b>{{$key}}</b></li>
                            @foreach($datas as $data)
                            <!-- @if($data->is_static_component == 0) -->
                            <li>
                                <div draggable="true" id="{{$data->id}}" dataset_id="{{ @$data->data_set_id?$data->data_set_id:0 }}"  ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);">
                                    {{$data->label}}
                                </div>
                            </li>
                            <!-- @endif -->
                            @endforeach
                        @endforeach
                    </ul>
                    <h5>Static Components</h5>
                    <ul class="static-components">
                        @foreach($static_components as $key => $data)
                            <!-- @if($data->is_static_component == 1) -->
                            <li>
                                <div style="color:brown;" draggable="true" id="{{$data->id}}" ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);">
                                    {{$data->label}}
                                </div>
                            </li>
                            <!-- @endif -->
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-10" style="margin-bottom: 10px;">
            <div class="col-md-12 box-body">
                <div class="col-md-5">
                    <label>Template Name</label>
                    <div class="clearfix"></div>
                    <input type="text" class="form-control" name="template_name" id="template_name"/>
                </div>
                <div class="col-md-4">
                    <label>Saved Templates</label>
                    <div class="clearfix"></div>
                    <select class="form-control select2" id="template_list"  name="template_list">
                        <option value="0">Select</option>
                        @foreach($saved_templates as $temp)
                        <option value="{{$temp->id}}">{{$temp->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <button type="button" class="btn bg-green main-btn-class pull-right" onclick="saveTemplate();">
                        <i class="fa fa-save"></i> Save Template
                    </button>
                    <button type="button" class="btn bg-orange main-btn-class pull-right" onclick="resetTemplate();">
                        <i class="fa fa-repeat"></i> Reset Template
                    </button>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-2">
                    <input type="checkbox" id="template_in_emr_lite" name="template_in_emr_lite" style="cursor: pointer;">
                    <label for="">Is Emr Lite</label>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            {{-- <div class="col-md-12 box-body template-container theadscroll dest_copy" id="" ondrop="drop_handler(event);" ondragover="dragover_handler(event);" style="height:610px;position:relative;"> --}}
            <div class="col-md-12 box-body template-container  dest_copy" id="" ondrop="drop_handler(event);" ondragover="dragover_handler(event);" style="min-height:610px;">

            </div>
        </div>
    </div>
</div>
@stop
@section('javascript_extra')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/flexy.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/form_template/js/form_template.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/form_template/js/jquery.drawr.combined.js")}}"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $('.theadscroll').perfectScrollbar({
            minScrollbarLength: 30
        });
        $('.right_col').css('height', '90vh');
    });

    $(document).on('click', '.select_button li', function () {
        $(this).toggleClass('active');
        let sel_value = $(this).attr('data-value');
        if (sel_value != "" && sel_value != undefined) {
            if ($(this).hasClass('active')) {
                $(this).find('input').val(sel_value);
                $(this).find('input').prop("disabled", false);
            } else {
                $(this).find('input').val("");
                $(this).find('input').prop("disabled", true);
            }
        }
    });
</script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
@endsection

