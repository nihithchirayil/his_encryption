<div class="col-md-{{$res->size}} sortable" draggable="true" ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);">
    <i class="fa fa-times-circle" aria-hidden="true"></i>
    <fieldset style="min-height:{{$res->container_min_height}}px;" class="scheduler-border">
        <legend class="scheduler-border"><span class="contenteditable">Vitals</span></legend>
        
        <div id="static_component_vital_form">
            @php
                $weight =  ExtensionsValley\Emr\VitalMaster::getVitalDetails('Weight');
                $height =  ExtensionsValley\Emr\VitalMaster::getVitalDetails('Height');
                $temperature  =  ExtensionsValley\Emr\VitalMaster::getVitalDetails('Temperature');
            @endphp
            {!! Form::select('weight', $weight->toArray(),'', ['class' => 'form-control hidden','id' => 'weight','onchange' => 'weightSelect(this)']) !!}
            {!! Form::select('temperature', $temperature->toArray(),'', ['class' => 'form-control hidden','id' => 'temperature','onchange' => 'temperatureSelect(this)']) !!}
            {!! Form::select('height', $height->toArray(),'', ['class' => 'form-control hidden','id' => 'height','onchange' => 'heightSelect(this)']) !!}
            <input type="hidden" id="latest_vital_batch" value="">
    
            <table class="table no-margin theadfix_wrapper table-condensed" >
                <tr>
                    <td><label for="">Weight in Kg</label></td>
                    <td>    <input type="text" class="form-control" name="weight_value" id="weight_value" value="" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                    </td>
    
                    <td><label for="">Height/Length in cm</label></td>
                    <td><input type="text" class="form-control" name="height_value" id="height_value" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        value="">
                    </td>
    
                    <td><label for="">BMI Kg/M^2</label></td>
                    <td><input type="text" class="form-control" name="bmi" id="bmi" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        value="">
                    </td>
                    <td><label for="">Oxygen Saturation %</label></td>
                    <td><input type="text" class="form-control" name="oxygen" id="oxygen" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        value="">
                    </td>
                </tr>
    
                <tr>
                    <td><label for="">BP Systolic mmHg</label></td>
                    <td><input type="text"  value="" class="form-control" name="bp_systolic" id="bp_systolic" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                    </td>
    
                    <td><label for="">BP Diastolic mmHg</label></td>
                    <td><input type="text" class="form-control" name="bp_diastolic" id="bp_diastolic" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        value="">
                    </td>
    
                    <td><label for="">Temperature in C</label></td>
                    <td><input type="text" class="form-control int_type " name="temperature_value" id="temperature_value" onkeyup="calculateTemperature(1)" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        value="">
                    </td>
                    <td><label for="">Temperature in F</label></td>
                    <td><input type="text" class="form-control int_type " name="temperature_value_f" id="temperature_value_f" onkeyup="calculateTemperature(2)" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        value="">
                    </td>
    
                </tr>
    
                <tr>
                    <td><label for="">Pulse/Min</label></td>
                    <td><input type="text" class="form-control int_type " name="pulse" id="pulse" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        value="">
                    </td>
                    <td><label for="">Respiration/Min</label></td>
                    <td><input type="text" class="form-control int_type " name="respiration" id="respiration" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        value="">
                    </td>
    
                    <td><label for="">Head Circ-cm</label></td>
                    <td><input type="text" class="form-control" name="head" id="head" 
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="">
                    </td>
                    <td><label>Waist Circumference (Peripheral)</label></td>
                    <td><input type="text" autocomplete="off"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control" name="waist" id="waist"
                        value="">
                    </td>
                </tr>
    
                <tr>
                    <td><label>Hip Circumference (cm)</label></td>
                    <td><input type="text" autocomplete="off"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control" name="hip" id="hip"
                        value=""></td>
    
                        <td><label>Waist/Hip ratio</label></td>
                    <td>
                        <input type="text" autocomplete="off" readonly disabled
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            class="form-control" name="waist_hip_ratio" id="waist_hip_ratio"
                            value="">
                    </td>
    
                    <td>
                            <label>Blood sugar</label></td>
                        <td> <input type="text" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            class="form-control" name="blood_sug" id="blood_sug"
                            value="{{ @$str ? $str : '' }}">
                    </td>
    
                    <td><label>Blood sugar unit</label></td>
                    <td> <div class="col-md-6 no-padding"><input type="radio" name="visibility_status"
                        value="1" />&nbsp;<br> <label>mmol/L</label> </div>
                        <div class="col-md-6 no-padding"><input type="radio" name="visibility_status"  checked
                        value="2" />&nbsp;<br> <label>mg/dL</label> </div></td>
                </tr>
                
                <tr>
                    <td><label for="">Time Taken</label></td>
                    <td colspan="2"><input type="text" class="form-control date_time_picker" name="time_taken" id="time_taken" value="" /></td>
                    <td></td>
    
                    <td><label for="">Vital Remarks</label></td>
                    <td colspan="3">
                        <textarea class="form-control remarks" name="remarks" id="remarks" style="height: 60px !important;"></textarea>
                    </td>
                </tr>
    
            </table>
        </div>
    </fieldset>
</div>