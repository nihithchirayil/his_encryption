 {{-- <div class="btn-group col-md-{{$res->size}} sortable" data-toggle="buttons" draggable="true" ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);"> --}}
    <i class="fa fa-times-circle" aria-hidden="true"></i>
    <label contenteditable="true">Image Editor</label>
    <div class="clearfix"></div>
    <span class="show_print" id="show_print_canvas_img"></span>
    <span class="hide_print">
        <span id="image_canvas_container">

            <div id="drawr-container" style="width:90% !important;height:450px !important;border:2px solid rgb(209, 209, 209);margin:20px;">
                <canvas id="canvas_img" class="demo-canvas drawr-test3"></canvas>
            </div>

        </span>
        <input type="file" id="file-picker" style="display:none;">
        <div class="col-md-10" style="margin-left: 19px;float: inline-start;background: #545454;" id="image_gallery">
        </div>
        <script type="text/javascript">

            $("#drawr-container .demo-canvas").drawr({ "enable_transparency" : true, enable_transparency_image: true });
            $("#drawr-container .demo-canvas").drawr("start");

            //add custom save button.
            var buttoncollection = $("#drawr-container .demo-canvas").drawr("button", {
                "icon":"mdi mdi-folder-open mdi-24px"
            }).on("touchstart mousedown",function(){
                //alert("demo of a custom button with your own functionality!");
                $("#file-picker").click();
            });
            var buttoncollection = $("#drawr-container .demo-canvas").drawr("button", {
                "icon":"mdi mdi-content-save mdi-24px"
            }).on("touchstart mousedown",function(){
                var imagedata = $("#drawr-container .demo-canvas").drawr("export","image/png");
                var element = document.createElement('a');
                element.setAttribute('href', imagedata);// 'data:text/plain;charset=utf-8,' + encodeURIComponent("sillytext"));
                element.setAttribute('download', "test.png");
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
            });
            $("#file-picker")[0].onchange = function(){
                var file = $("#file-picker")[0].files[0];
                if (!file.type.startsWith('image/')){ return }
                var reader = new FileReader();
                reader.onload = function(e) {
                    $("#drawr-container .demo-canvas").drawr("load",e.target.result);
                    var img_html = "<div class='col-md-1 tile-image pull-left' style='text-align:center;margin-bottom:11px;margin-top:10px;background-color:#545454 !important;'><i class='fa fa-times-circle' aria-hidden='true'></i><img class='image_gallery_content' style='width:50px;height:50px;border:1px solid white;border-radius:4px;' src="+e.target.result+"></img></div>";
                    $('#image_gallery').append(img_html);
                };
                reader.readAsDataURL(file);
            };

            $(document).on("click", ".image_gallery_content", function(){
                var img_html = $(this).attr('src');
                $("#drawr-container .demo-canvas").drawr("load",img_html);
            });

        </script>
    </span>
 {{-- </div> --}}

