<div class="col-md-12 no-padding theadscroll" id="control_block" style="position:absolute;height:617px !important;padding-right:13px !important;">
    <ul class="html-components" id="html_components">
        @php
            $control_name ="";
        @endphp
        @foreach($template_controler as $key => $datas)
            @if($datas->control_name !=$control_name)
                <li style="color: #c73838;margin: 2px 0;"><b>{{$datas->control_name}}</b></li>
            @endif
            @if($datas->is_static_component == 0)
                <li style="list-style-type: none;">
                    <div draggable="true" id="{{$datas->id}}" dataset_id="{{ @$datas->data_set_id?$datas->data_set_id:0 }}"  ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);">
                        {{$datas->label}}
                    </div>
                </li>
            @endif
            @php
                $control_name = $datas->control_name;
            @endphp
        @endforeach
    </ul>
    <h5>Static Components</h5>
    <ul class="static-components">
        @foreach($static_components as $key => $data)
            <!-- @if($data->is_static_component == 1) -->
            <li>
                <div style="color:brown;" draggable="true" id="{{$data->id}}" ondragstart="dragstart_handler(event);" ondragend="dragend_handler(event);">
                    {{$data->label}}
                </div>
            </li>
            <!-- @endif -->
        @endforeach
    </ul>
</div>
