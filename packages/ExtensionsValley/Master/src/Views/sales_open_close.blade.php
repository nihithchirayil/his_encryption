{{-- @extends('Dashboard::dashboard.dashboard') --}}
@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">

<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
.heading{
background: aliceblue;
font-size: 21px;
color: #908989;
padding: 6px;
box-shadow: 0px 1px 2px;
}
#datatable{
box-shadow: 0px 1px 4px;
margin-top: 58px;
width: 100%;
}
th{
background: aliceblue;
}
tr{
box-shadow: 0px 0px 1px;
}
td,th{
padding: 7px;
}
.bt{
font-size: 13px;
padding: 3px 6px;
margin-top: 12px;
}
#bottom{
margin-top: 5px;
border-top: 3px solid aliceblue;
padding-top: 8px;
}
</style>

@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <div  id="main" class="col-md-12 padding_sm" style="min-height: 480px;">
                        <div class="col-md-12 heading">
               
                            Day Opening Closing
                            
                        </div>
                        <table id="datatable">
                            <thead>
                                <tr>
                                    <th id="th">S.No</th>
                                    <th id="th">Date</th>
                                    <th id="th">Opening Time</th>
                                    <th id="th">Closing Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                 @php
                                 $var=$res;
                                 $date='';
                                 date('Y-m-d ',strtotime($var[0]->sales_date));
                                 if(strlen($var[0]->closing_time)<=0){
                                    $date= date('Y-m-d ',strtotime($var[0]->opening_time));
                                }else{
                                    
                                 $date= date('Y-m-d ',strtotime($var[0]->closing_time));
                                }
                                
                                if(strlen($var[0]->closing_time)<=0){
                                    $date_time= date('Y-m-d H:i:s ',strtotime($var[0]->opening_time));
                                }else{
                                    $date_time= date('Y-m-d H:i:s ',strtotime($var[0]->closing_time));   
                                }
                                @endphp
                                 @foreach ($sales_opening_closing as $item)
                                <tr id="unique">
                                    <td id="th" style="display: none" >{{$item->id}}</td>
                                    <td>{{($sales_opening_closing->currentPage() - 1) * $sales_opening_closing->perPage() + $loop->iteration}}</td>
                                    <td id="th">{{date('Y-m-d ',strtotime($item->sales_date))}}</td>
                                    <td id="th">{{$item->opening_time}}</td>
                                    <td id="th">{{$item->closing_time}}</td>
                                </tr>
                                 
                                 @endforeach
                            </tbody>                                
                        </table>
                        <div style="margin-top: -12px;">
                            {{ $sales_opening_closing->links() }}
                        </div>

                    
                        <div id="bottom">                           <b>Last Active:</b>{{$date}}&nbsp;<b>Last Opened On:</b>{{$date_time}}</br>
                            <b>Time:</b>{{date('Y-m-d H:i:s')}}
                          <div style="float: right">    
                              <button type="button" id="btn" class="btn btn-success" onclick="adddate()" style="background-color: green"><i class="fa fa-save" id="add_spin"></i> Open Today's Sale</button>
                              <button type="button" id="btn1" class="btn btn-success" onclick="update()"   style="background-color: #b31919" ><i class="fa fa-save" id="update_spin"></i> Close Todays's sale</button>
                          </div>
                        </div>
 

           
        </div>
    </div>
</div>
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>


<script type="text/javascript">


//--------------------------------------------FUNCTION FOR OPEN NEW SALES------------------------------------------------------------------------------------


function adddate(){     
                        
                        
                        document.getElementById("btn").disabled = true;
                       
                        var myBody = document.getElementsByTagName("td")[2].textContent;
                        var myBody1 = document.getElementsByTagName("td")[3].textContent;
                        var myBody4 = document.getElementsByTagName("td")[4].textContent;
                        
                        if((myBody4.length)<=0){
                            toastr.warning("Please Make Sure That The Last Sales Is Closed Successfully !!");
                            document.getElementById("btn1").disabled = false;
                            return;
                        }
                        
                        if(((myBody4.length)>=0)&&((myBody1.length)>=0)&&((myBody.length)>=0)){
                         var MyDate = new Date();
                         MyDate.setDate(MyDate.getDate());
                         var current_date = MyDate.getFullYear()+'-'+('0' + (MyDate.getMonth()+1)).slice(-2)+'-' +('0' + MyDate.getDate()).slice(-2) ;
                         var current_time =  current_date +' '+ ('0' + (MyDate.getHours())).slice(-2)+':'+('0' + MyDate.getMinutes()).slice(-2)+':'+('0' + (MyDate.getSeconds()+1)).slice(-2);
        
                        $("#datatable tbody").prepend('<tr><td><b>Today-</b></td><td>'+current_date+'</td><td>'+current_time+'</td></tr>');
                       
                               
                        var add=1;
                        var unique_id=$('#unique').closest('tr').children('td:first').text();
                        
                        
                        $.ajax({
                            type: "GET",
                            url: "{!! route('extensionsvalley.master.save_date') !!}",
                            data: {data3:unique_id,data4:add},
                            beforeSend: function () {
                                $('#main').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                                $("#add_spin").removeClass('fa fa-save');
                                $("#add_spin").addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                 $("#add_spin").removeClass('fa fa-spinner fa-spin');
                                $("#add_spin").addClass('fa fa-save');
                               
                                if (data== 1) {
                                    $("#main").load(" #main");
                                    $('#main').LoadingOverlay("hide");
                                    toastr.success("sale's opend  Successfully");
                                   
                                   
                                    
                                }else{
                                    toastr.error("somting went wrong");
                                }
                            },
                            complete: function () {
                               
                               
                                


                            }
                        });
                    }
       
}


 //-----------------------------------------FUNCTION TO CLOSE SALES----------------------------------------------------------------------------------------------




function update() {
                            document.getElementById("btn").disabled = false;
                            document.getElementById("btn1").disabled = true;
                            var myBody4 = document.getElementsByTagName("td")[4].textContent;
                            
                            if((myBody4.length)<=0){
                            var add=0;
                            var unique_id=$('#unique').closest('tr').children('td:first').text();
                            $.ajax({
                                type: "GET",
                                url: "{!! route('extensionsvalley.master.save_date') !!}",
                                data:{data3:unique_id,data4:add},
                                beforeSend: function () {
                                    $('#main').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
                                    $("#update_spin").removeClass('fa fa-save');
                                    $("#update_spin").addClass('fa fa-spinner fa-spin');
                                },
                                success: function (data) {

                                    $("#update_spin").removeClass('fa fa-spinner fa-spin');
                                    $("#update_spin").addClass('fa fa-save');
                                    if (data == '2') {
                                        $("#main").load(" #main");
                                        $('#main').LoadingOverlay("hide");
                                        toastr.success("sale's closed successfully");
                                    }
                                },
                                complete: function () {

                                }
                            });
                        
                        }else{ 
                                toastr.warning("Sales Is Closed Already !!");
                               
                                return;
                              }
                    }


//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
</script>
@endsection
