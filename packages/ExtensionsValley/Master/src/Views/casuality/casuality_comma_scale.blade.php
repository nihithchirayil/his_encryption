<style>
    .form-check-input{
        appearance: auto !important;
        display: block !important;
    }
</style>
<div class="col-md-12 no-padding">
    <div class="col-md-8 text-blue">
        <h5><b>COMA SCALE</b></h5>
    </div>
    <div class="col-md-4">
        <input type="text" data-attr="date" autocomplete="off" name="coma_scale_date" autofocus="" value="{{date('M-d-Y')}}" class="form-control datepicker table_text text-blue" placeholder="MM-DD-YYYY" id="coma_scale_date" style="font-size: 14px !important;">
    </div>
</div>
<div class="col-md-12 no-padding" style="margin-top:10px;padding: 10px !important;">
    <table class="table table-striped table-bordered table-sm">
        <tr>

            <td>
                <h6><b>Time :</b></h6>
            </td>
            <td>
                <input type="time" value="{{date('H:i')}}" name="" id="coma_scale_time1" class="form-control timepickerclass_default  timepickerclass">

            </td>
            <td>
                <input type="time" value="{{date('H:i')}}" name="" id="coma_scale_time2" class="form-control timepickerclass_default  timepickerclass">

            </td>
            <td>
                <input type="time" value="{{date('H:i')}}" name="" id="coma_scale_time3" class="form-control timepickerclass_default  timepickerclass">

            </td>
        </tr>
        <tr>
            <td colspan="4">
                <h6 style="margin:0px !important;"><b>EYES OPEN</b></h6>
            </td>
        </tr>
        <tr>
            <td>
                <label>Spontaneously 4</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="eyes_open1" id="" value="4">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="eyes_open2" id="" value="4">
                    <label class="form-check-label" for=""></label>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="eyes_open3" id="" value="4">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>

        <tr>
            <td>
                <label>To Verbal Command 3</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="eyes_open1" id="" value="3">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="eyes_open2" id="" value="3">
                    <label class="form-check-label" for=""></label>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="eyes_open3" id="" value="3">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>

        <tr>
            <td>
                <label>To Pain 2</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="eyes_open1" id="" value="2">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="eyes_open2" id="" value="2">
                    <label class="form-check-label" for=""></label>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="eyes_open3" id="" value="2">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>No Response 1</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="eyes_open1" id="" value="1">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="eyes_open2" id="" value="1">
                    <label class="form-check-label" for=""></label>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="eyes_open3" id="" value="1">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>



        <tr>
            <td colspan="4">
                <h6 style="margin:0px !important;"><b>BEST MOTOR RESPONSE</b></h6>
            </td>
        </tr>
        <tr>
            <td>
                <label>Obey Commands 6</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="best_motor_response1" id="" value="6">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_motor_response2" id="" value="6">
                    <label class="form-check-label" for=""></label>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_motor_response3" id="" value="6">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>
       <tr>
            <td>
                <label>Localizes Pain 5</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="best_motor_response1" id="" value="5">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_motor_response2" id="" value="5">
                    <label class="form-check-label" for=""></label>
            </td>
            <td >
                <input class="form-check-input" type="radio" name="best_motor_response3" id="" value="5">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>
       <tr>
            <td>
                <label>Flexion Withdrawal 4</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="best_motor_response1" id="" value="4">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_motor_response2" id="" value="4">
                    <label class="form-check-label" for=""></label>
            </td>
            <td >
                <input class="form-check-input" type="radio" name="best_motor_response3" id="" value="4">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>
       <tr>
            <td>
                <label>Flexion abnormal(Decorticate regidity)3</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="best_motor_response1" id="" value="3">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_motor_response2" id="" value="3">
                    <label class="form-check-label" for=""></label>
            </td>
            <td >
                <input class="form-check-input" type="radio" name="best_motor_response3" id="" value="3">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>

        <tr>
            <td>
                <label>Extension(Decorticate regidity) 2</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="best_motor_response1" id="" value="2">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_motor_response2" id="" value="2">
                    <label class="form-check-label" for=""></label>
            </td>
            <td >
                <input class="form-check-input" type="radio" name="best_motor_response3" id="" value="2">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>

        <tr>
            <td>
                <label>No Response 1</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="best_motor_response1" id="" value="1">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_motor_response2" id="" value="1">
                    <label class="form-check-label" for=""></label>
            </td>
            <td >
                <input class="form-check-input" type="radio" name="best_motor_response3" id="" value="1">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <h6 style="margin:0px !important;"><b>BEST VERBAL RESPONSE</b></h6>
            </td>
        </tr>
        <tr>
            <td>
                <label>Oriented and Converses 5</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="best_verbel_response1" id="" value="5">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_verbel_response2" id="" value="5">
                    <label class="form-check-label" for=""></label>
            </td>
            <td >
                <input class="form-check-input" type="radio" name="best_verbel_response3" id="" value="5">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Disoriented and Converses 4</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="best_verbel_response1" id="" value="4">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_verbel_response2" id="" value="4">
                    <label class="form-check-label" for=""></label>
            </td>
            <td >
                <input class="form-check-input" type="radio" name="best_verbel_response3" id="" value="4">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>In appropiate words 3</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="best_verbel_response1" id="" value="3">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_verbel_response2" id="" value="3">
                    <label class="form-check-label" for=""></label>
            </td>
            <td >
                <input class="form-check-input" type="radio" name="best_verbel_response3" id="" value="3">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>In Comprehensible Sounds 2</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="best_verbel_response1" id="" value="2">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_verbel_response2" id="" value="2">
                    <label class="form-check-label" for=""></label>
            </td>
            <td >
                <input class="form-check-input" type="radio" name="best_verbel_response3" id="" value="2">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>No Response 1</label>
            </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="best_verbel_response1" id="" value="1">
                    <label class="form-check-label" for=""></label>
                </div>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="best_verbel_response2" id="" value="1">
                    <label class="form-check-label" for=""></label>
            </td>
            <td >
                <input class="form-check-input" type="radio" name="best_verbel_response3" id="" value="1">
                    <label class="form-check-label" for=""></label>
            </td>
        </tr>
    </table>




    <!--------------------pupils-------------------------->
    <table class="table table-striped table-bordered table-sm" style="color:#212529;">
        <tr>
            <td rowspan="4">
                <b>PUPILS</b>
            </td>
            <td rowspan="2">Right</td>
            <td rowspan="2">Size Reaction</td>
            <td>
                <input type="text" autocomplete="off" name="pupils_right_size_reaction11" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_right_size_reaction11" style="">
            </td>
            <td>
                <input type="text" autocomplete="off" name="pupils_right_size_reaction12" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_right_size_reaction12" style="">
            </td>
            <td>
                <input type="text" autocomplete="off" name="pupils_right_size_reaction13" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_right_size_reaction13" style="">
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" autocomplete="off" name="pupils_right_size_reaction21" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_right_size_reaction21" style="">
            </td>
            <td>
                <input type="text" autocomplete="off" name="pupils_right_size_reaction22" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_right_size_reaction22" style="">
            </td>
            <td>
                <input type="text" autocomplete="off" name="pupils_right_size_reaction23" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_right_size_reaction23" style="">
            </td>
        </tr>
        <tr>
            <td rowspan="2">Left</td>
            <td rowspan="2">Size Reaction</td>
            <td>
                <input type="text" autocomplete="off" name="pupils_left_size_reaction11" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_left_size_reaction11" style="">
            </td>
            <td>
                <input type="text" autocomplete="off" name="pupils_left_size_reaction12" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_left_size_reaction12" style="">
            </td>
            <td>
                <input type="text" autocomplete="off" name="pupils_left_size_reaction13" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_left_size_reaction13" style="">
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" autocomplete="off" name="pupils_left_size_reaction21" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_left_size_reaction21" style="">
            </td>
            <td>
                <input type="text" autocomplete="off" name="pupils_left_size_reaction22" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_left_size_reaction22" style="">
            </td>
            <td>
                <input type="text" autocomplete="off" name="pupils_left_size_reaction23" autofocus="" value="" class="form-control no_border_text" placeholder="" id="pupils_left_size_reaction23" style="">
            </td>
        </tr>
    </table>

</div>
