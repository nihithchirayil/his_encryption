@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/default/css/bootstrap-timepicker.min.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/master/default/css/casuality.css")}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<style>
    .header_bg{
        background: rgb(37,184,155) !important;
background: linear-gradient(90deg, rgba(37,184,155,1) 0%, rgba(24,147,141,1) 0%, rgba(23,182,197,1) 100%) !important;
color:white !important;
    }
    .gray_headeer_bg{
        background: rgb(37,184,155) !important;
background: linear-gradient(90deg, rgba(37,184,155,1) 0%, rgba(219,219,219,1) 0%, rgba(174,174,174,1) 100%) !important;
    }
    .mate-input-box label{
        top:-18px !important;
    }
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="patient_id" value="{{$patient_id}}">
<input type="hidden" id="visit_id" value="{{$visit_id}}">
<input type="hidden" id="encounter_id" value="{{$encounter_id}}">
<input type="hidden" id="doctor_id" value="{{$doctor_id}}">
<input type="hidden" id="prescription_head_id" value="0">
<input type="hidden" id="prescription_consumable_head_id" value="0">
<div class="right_col">
    <div class="col-md-12 no-padding text-center box-body header_bg">
        <h5><b>CASUALITY PATIENT RECORD WITH NEURO CHART</b></h5>
   </div>
   <div class="col-md-12 no-padding" style="margin:10px;">
       {{--  header section  --}}
       @include('Master::casuality.casuality_head')
   </div>
   <div class="col-md-12" style="margin:10px">
        <button class="btn bg-default headerbtn " onclick="combinedView()"><i class="fa fa-history" aria-hidden="true"></i> History</button>

        <button class="btn bg-default headerbtn " onclick="specialNotes()" data-toggle="modal" data-target="#speial_note_modal"> <i class="fa fa-book" aria-hidden="true"></i>Special
            Notes
        </button>

        <button class="btn bg-default headerbtn " onclick="referDoctor();"> <i class="fa fa-user-plus" aria-hidden="true"></i> Refer Patient
        </button>

        <button data-toggle="modal" onclick="LabResultsTrend();" data-target="#modal_lab_results" class="btn bg-default headerbtn  btn-nurse-task labResultBtn blink-btn"><i class="fa fa-flask"></i> Lab Results</button>

        <button class="btn bg-default headerbtn " id="btn_advnc_dtail" onclick="manageDocs();"><i class="fa fa-archive" aria-hidden="true"></i> Documents</button>

        <button class="btn bg-default headerbtn " id="btn_private_notes"><i class="fa fa-list" aria-hidden="true"></i> Private Notes</button>

        <button class="btn bg-default headerbtn showDischargeSummary" onclick="showDischargeSummary();"><i class="fa fa-tasks" aria-hidden="true"></i> Discharge
            Summary </button>

        <button type="button" @if(strtoupper($visit_status)=='OP')disabled="disabled"@endif onclick="editDischargeSummary('{{$visit_id}}');" class="btn bg-default headerbtn"><span><i class="fa fa-plus"></i></span> Create Summary
        </button>


    </div>

   <div class="col-md-12 box-body" style="margin-top:10px;">
        <div class="no-border">
                <label class="text-blue"><h5 style="margin-left:10px;"><b>Present Complaints</b></h5></label>
                <div class="" style="padding:10px !important;">
                    <textarea style="min-height:180px !important;padding-left:10px !important;" class=" form-control text_area" id="present_complaints"></textarea>
                </div>
        </div>
   </div>

    <div class="col-md-12 no-padding">
        @include('Emr::emr.includes.patient_investigations')
    </div>

    <div class="col-md-12 box-body text-blue" style="margin-top:10px;">
        <h5><b>Prescription</b></h5>
    </div>
    <div class="col-md-12 no-padding">
            @include('Emr::emr.includes.patient_prescription')
    </div>
   <div class="col-md-12 no-padding" style="margin-top:10px;">
       <div class="col-md-12 no-padding text-center box-body header_bg">
            <h5><b>CASUALITY NURSE'S NOTES</b></h5>
       </div>


        <div class="col-md-12 no-padding" style="min-height:1045px !important;">
            <div class="box-body col-md-7 padding-sm" style="height:1032px;">
                @include('Master::casuality.casuality_nurse_notes_area')
            </div>
            <div class="box-body col-md-5 padding-sm">
                @include('Master::casuality.casuality_comma_scale')
            </div>
        </div>

   </div>
   <div class="col-md-12 box-body no-padding" style="margin-bottom:25px;padding:20px;">
       <button style="height:30px;width:125px;margin-top:10px;margin-bottom:10px;" type="button" class="btn bg-green pull-right" onclick="SaveCasuality(0);"><i class="fa fa-save"></i> Save</button>
       <button type="button" style="height:30px;width:125px;margin-top:10px;margin-bottom:10px;" class="btn bg-green pull-right" onclick="SaveCasuality(1);"><i class="fa fa-print"></i> Save & Print</button>
   </div>
</div>
@include('Emr::emr.includes.custom_modals')
@stop
@section('javascript_extra')

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/casuality.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/js/allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/emr/js/investigation.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">


    <script type="text/javascript">

</script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="{{ asset('packages/extensionsvalley/emr/js/vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/emr/js/manage-document.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
