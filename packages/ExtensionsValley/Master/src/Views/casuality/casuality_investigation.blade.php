@if(sizeof($investigation_details)>0)
<div class="col-md-12 box-body">
    <table border="1" cellspacing="0" cellpadding="3" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important">
        <thead>
            <tr>
                <td>Investigation</td>
                <td>Type</td>
                <td>Sub Department</td>
            </tr>
        </thead>
        <tbody>
            @foreach($investigation_details as $data)
            <tr>
                <td>{{$data->service_desc}}</td>
                <td>{{$data->investigation_type}}</td>
                <td>{{$data->sub_dept_name}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif
