@if(sizeof($medication_details)>0)
<div class="col-md-12 box-body">
    <table border="1" cellspacing="0" cellpadding="3" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important">
        <thead>
            <tr>
                <td>Medicine</td>
                <td>Generic Name</td>
                <td>Dose</td>
                <td>Frequency</td>
                <td>Days</td>
                <td>Quantity</td>
                <td>Route</td>
                <td>Start At</td>
                <td>Instructions</td>
            </tr>
        </thead>
        <tbody>
            @foreach($medication_details as $data)
            <tr>
                <td>{{$data->item_desc}}</td>
                <td>{{$data->generic_name}}</td>
                <td>{{$data->dose}}</td>
                <td>{{$data->frequency}}</td>
                <td>{{$data->duration}}</td>
                <td>{{$data->quantity}}</td>
                <td>{{$data->route}}</td>
                <td>
                    @if($data->start_date!='')
                        {{date('M-d-Y h:i A',strtotime($data->start_date))}}
                    @endif
                </td>
                <td>{{$data->notes}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif
