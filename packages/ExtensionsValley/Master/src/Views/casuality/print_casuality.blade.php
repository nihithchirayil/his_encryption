<div class="col-md-12">
    <div class="col-md-12">
        {!!$hospital_header!!}
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 box-body" style="text-align:center;background-color:rgb(222, 219, 219) !important;">

        <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important">
            <tr>
                <td class="titleclass"><h3>CASUALITY PATIENT RECORD WITH NEURO CHART</h3></td>
            </tr>
        </table>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 box-body">

        <table border="1" cellspacing="0" cellpadding="3" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important;margin-top:5px;">
            <tr>
                <td width="40%"> Name</td>
                <td width="20%"> Age/Gender</td>
                <td width="20%"> UHID</td>
                <td width="20%"> Date&Time</td>
            </tr>
            <tr>
                <td width="40%"> {{$patient_details[0]->patient_name}}</td>
                <td width="20%"> {{$patient_details[0]->age}}/{{$patient_details[0]->gender}}</td>
                <td width="20%"> {{$patient_details[0]->uhid}}</td>
                <td width="20%"> {{date('M-d-Y h:i A',strtotime($visit_data->created_at))}}</td>
            </tr>
        </table>

    </div>

    <div class="clearfix"></div>

    @if($visit_data->present_complaints != '')
    <div class="col-md-12 box-body">
        <table border="1" cellspacing="0" cellpadding="3" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important;margin-top:5px;">
            <tr>
                <td style="text-align:center">
                    <b>Present Complaints</b>
                    <p>{{$visit_data->present_complaints}}</p>
                </td>
            </tr>
        </table>
    </div>
    @endif

    <div class="col-md-12 box-body">
        <table border="1" cellspacing="0" cellpadding="3" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important;margin-top:5px;">
            <tr>
                <td colspan="6" style="text-align:center" style="padding-top:10px !important;">
                    <b>Vital Signs</b>
                </td>
            </tr>
            <tr>
                <td width="16.6%">BP :</td>
                <td width="16.6%">{{$vitals['bp']}}</td>
                <td width="16.6%">Pulse :</td>
                <td width="16.6%">{{$vitals['pulse']}}</td>
                <td width="16.6%">SpO2 :</td>
                <td width="16.6%">{{$vitals['spo2']}}</td>
            </tr>
            <tr>
                <td>Temp :</td>
                <td>{{$vitals['temp']}}</td>
                <td>Respiration :</td>
                <td>{{$vitals['respiration']}}</td>
                <td>GRBS :</td>
                <td>{{$vitals['GRBS']}}</td>
            </tr>
        </table>

        <table border="1" cellspacing="0" cellpadding="3" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important;margin-top:5px;">
            <tr>
                <td style="padding-top:10px !important;text-align:center;">
                    <b>Investigations</b>
                </td>
            </tr>
            <tr>
                <td style="padding-top:10px !important;">
                    {!!$investigation_view!!}
                </td>
            </tr>
        </table>

        <table border="1" cellspacing="0" cellpadding="3" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important;margin-top:5px;">
            <tr>
                <td style="padding-top:10px !important;text-align:center;">
                    <b>Prescriptions</b>
                </td>
            </tr>
            <tr>
                <td style="padding-top:10px !important;">
                    {!!$prescription_view!!}
                </td>
            </tr>
        </table>

        <table border="1" cellspacing="0" cellpadding="3" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important;margin-top:5px;">

            @if(sizeof($medicine_allergy_data) > 0 || sizeof($other_allery_data) > 0)
            <tr>
                <td style="padding-top:10px !important;text-align:center;">
                    <b>Allergies</b>
                </td>
            </tr>
            <tr>
                <td style="padding-top:10px !important;">
                    @if(sizeof($medicine_allergy_data) > 0)
                        <span><h5>Drug Allergies</h5></span>
                        <span>
                            <ul>
                            @foreach($medicine_allergy_data as $key => $value)
                                <li><span>{{$value->description}}</span></li>
                            @endforeach
                            </ul>
                        </span>
                    @endif
                    @if(sizeof($other_allery_data) > 0)
                        <span><h5>Other Allergies</h5></span>
                        <ul>
                            <span>
                                @foreach($other_allery_data as $key => $value)
                                    <li><span>{{$value->allergy}}</span></li>
                                @endforeach
                            </span>
                        </ul>
                    @endif
                </td>
            </tr>
            @endif
        </table>
    </div>

    <div class="col-md-12 box-body" style="text-align:center;background-color:rgb(222, 219, 219) !important;">
        <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important">
            <tr>
                <td class="titleclass"><h3>CASUALITY NURSE'S NOTES</h3></td>
            </tr>
        </table>
    </div>
    <div class="col-md-12 box-body" style="">
            @if($visit_data->nursing_notes != '')
            <table border="1" cellspacing="0" cellpadding="3" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important">
                <tr>
                    <td style="padding-top:0px !important;text-align:center;">
                        <b>Nursing Notes</b>
                        <span style="float:right;">
                            {{date('M-d-Y h:i A', strtotime($visit_data->created_at))}}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="padding-top">

                        <p style="padding-top:10px">{{$visit_data->nursing_notes}}</p>
                    </td>
                </tr>
            </table>
            @endif
            <table border="1" cellspacing="0" cellpadding="3" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important;margin-top:3px;">
                <tr>
                    <td colspan="4"><b>Coma Scale</b></td>
                </tr>
                <tr>
                    <td width="40%"> <h6><b>Time :</b></h6></td>
                    <td width="20%">{{date('h:i A',strtotime($commascale_data[0]->coma_scale_time))}}</td>
                    <td width="20%">{{date('h:i A',strtotime($commascale_data[1]->coma_scale_time))}}</td>
                    <td width="20%">{{date('h:i A',strtotime($commascale_data[2]->coma_scale_time))}}</td>
                </tr>
                <tr>
                    <td colspan="4">
                        <h6 style="margin:0px !important;"><b>EYES OPEN</b></h6>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Spontaneously 4</label>
                    </td>
                    <td> @if($commascale_data[0]->eyes_open ==4) T @endif </td>
                    <td> @if($commascale_data[1]->eyes_open ==4) T @endif </td>
                    <td> @if($commascale_data[2]->eyes_open ==4) T @endif </td>
                </tr>

                <tr>
                    <td>
                        <label>To Verbal Command 3</label>
                    </td>
                    <td> @if($commascale_data[0]->eyes_open ==3) T @endif </td>
                    <td> @if($commascale_data[1]->eyes_open ==3) T @endif </td>
                    <td> @if($commascale_data[2]->eyes_open ==3) T @endif </td>
                </tr>

                <tr>
                    <td>
                        <label>To Pain 2</label>
                    </td>
                    <td> @if($commascale_data[0]->eyes_open ==2) T @endif </td>
                    <td> @if($commascale_data[1]->eyes_open ==2) T @endif </td>
                    <td> @if($commascale_data[2]->eyes_open ==2) T @endif </td>
                </tr>
                <tr>
                    <td>
                        <label>No Response 1</label>
                    </td>
                    <td> @if($commascale_data[0]->eyes_open ==1) T @endif </td>
                    <td> @if($commascale_data[1]->eyes_open ==1) T @endif </td>
                    <td> @if($commascale_data[2]->eyes_open ==1) T @endif </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <h6 style="margin:0px !important;"><b>BEST MOTOR RESPONSE</b></h6>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Obey Commands 6</label>
                    </td>
                    <td> @if($commascale_data[0]->best_motor_response ==6) T @endif </td>
                    <td> @if($commascale_data[1]->best_motor_response ==6) T @endif </td>
                    <td> @if($commascale_data[2]->best_motor_response ==6) T @endif </td>
                </tr>
                <tr>
                        <td>
                            <label>Localizes Pain 5</label>
                        </td>
                        <td> @if($commascale_data[0]->best_motor_response ==5) T @endif </td>
                        <td> @if($commascale_data[1]->best_motor_response ==5) T @endif </td>
                        <td> @if($commascale_data[2]->best_motor_response ==5) T @endif </td>
                </tr>
                <tr>
                        <td>
                            <label>Flexion Withdrawal 4</label>
                        </td>
                        <td> @if($commascale_data[0]->best_motor_response ==4) T @endif </td>
                        <td> @if($commascale_data[1]->best_motor_response ==4) T @endif </td>
                        <td> @if($commascale_data[2]->best_motor_response ==4) T @endif </td>
                </tr>
                <tr>
                        <td>
                            <label>Flexion abnormal(Decorticate regidity)3</label>
                        </td>
                        <td> @if($commascale_data[0]->best_motor_response ==3) T @endif </td>
                        <td> @if($commascale_data[1]->best_motor_response ==3) T @endif </td>
                        <td> @if($commascale_data[2]->best_motor_response ==3) T @endif </td>
                </tr>
                <tr>
                    <td>
                        <label>Extension(Decorticate regidity) 2</label>
                    </td>
                    <td> @if($commascale_data[0]->best_motor_response ==2) T @endif </td>
                    <td> @if($commascale_data[1]->best_motor_response ==2) T @endif </td>
                    <td> @if($commascale_data[2]->best_motor_response ==2) T @endif </td>
                </tr>
                <tr>
                    <td>
                        <label>No Response 1</label>
                    </td>
                    <td> @if($commascale_data[0]->best_motor_response ==1) T @endif </td>
                    <td> @if($commascale_data[1]->best_motor_response ==1) T @endif </td>
                    <td> @if($commascale_data[2]->best_motor_response ==1) T @endif </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <h6 style="margin:0px !important;"><b>BEST VERBAL RESPONSE</b></h6>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Oriented and Converses 5</label>
                    </td>
                    <td> @if($commascale_data[0]->best_verbal_response ==5) T @endif </td>
                    <td> @if($commascale_data[1]->best_verbal_response ==5) T @endif </td>
                    <td> @if($commascale_data[2]->best_verbal_response ==5) T @endif </td>
                </tr>
                <tr>
                    <td>
                        <label>Disoriented and Converses 4</label>
                    </td>
                    <td> @if($commascale_data[0]->best_verbal_response ==4) T @endif </td>
                    <td> @if($commascale_data[1]->best_verbal_response ==4) T @endif </td>
                    <td> @if($commascale_data[2]->best_verbal_response ==4) T @endif </td>
                </tr>
                <tr>
                    <td>
                        <label>In appropiate words 3</label>
                    </td>
                    <td> @if($commascale_data[0]->best_verbal_response ==3) T @endif </td>
                    <td> @if($commascale_data[1]->best_verbal_response ==3) T @endif </td>
                    <td> @if($commascale_data[2]->best_verbal_response ==3) T @endif </td>
                </tr>
                <tr>
                    <td>
                        <label>In Comprehensible Sounds 2</label>
                    </td>
                    <td> @if($commascale_data[0]->best_verbal_response ==2) T @endif </td>
                    <td> @if($commascale_data[1]->best_verbal_response ==2) T @endif </td>
                    <td> @if($commascale_data[2]->best_verbal_response ==2) T @endif </td>
                </tr>
                <tr>
                    <td>
                        <label>No Response 1</label>
                    </td>
                    <td> @if($commascale_data[0]->best_verbal_response ==1) T @endif </td>
                    <td> @if($commascale_data[1]->best_verbal_response ==1) T @endif </td>
                    <td> @if($commascale_data[2]->best_verbal_response ==1) T @endif </td>
                </tr>
        </table>

         <!-----Comma pupils data-------------------------------------->
         <table  border="1" cellspacing="0" cellpadding="3" width="100%" style="border-collapse: collapse; font-size: 12px; border:1px solid #dbdbdb !important;margin-top:5px;">
            <tr>
                <td width="10%" rowspan="4">
                    <b>PUPILS</b>
                </td>
                <td width="15%" rowspan="2">Right</td>
                <td width="15%" rowspan="2">Size Reaction</td>
                <td width="20%">{{$commascale_data[0]->pupils_right_size_reaction1}}</td>
                <td width="20%">{{$commascale_data[1]->pupils_right_size_reaction1}}</td>
                <td width="20%">{{$commascale_data[2]->pupils_right_size_reaction1}}</td>
            </tr>
            <tr>
                <td>{{$commascale_data[0]->pupils_right_size_reaction2}}</td>
                <td>{{$commascale_data[1]->pupils_right_size_reaction2}}</td>
                <td>{{$commascale_data[2]->pupils_right_size_reaction2}}</td>
            </tr>
            <tr>
                <td rowspan="2">Left</td>
                <td rowspan="2">Size Reaction</td>
                <td>{{$commascale_data[0]->pupils_left_size_reaction1}}</td>
                <td>{{$commascale_data[1]->pupils_left_size_reaction1}}</td>
                <td>{{$commascale_data[2]->pupils_left_size_reaction1}}</td>
            </tr>
            <tr>
                <td>{{$commascale_data[0]->pupils_left_size_reaction2}}</td>
                <td>{{$commascale_data[1]->pupils_left_size_reaction2}}</td>
                <td>{{$commascale_data[2]->pupils_left_size_reaction2}}</td>

            </tr>
        </table>

    </div>




</div>
