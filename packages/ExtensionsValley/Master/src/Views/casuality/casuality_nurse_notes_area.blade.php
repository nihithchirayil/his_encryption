<div class="col-md-12 no-padding" style="margin-top:10px;">
    <div class="col-md-4">
        <h5 class="text-blue" style="margin-left:11px !important;"><b>Nursing Notes</b></h5>
    </div>
    <div class="col-md-2 pull-right">
        <input type="text" data-attr="date" autocomplete="off" name="nursing_notes_date" autofocus="" value="{{date('M-d-Y')}}" class="form-control datepicker table_text text-blue" placeholder="YYYY-MM-DD" id="nursing_notes_date" style="font-size: 14px !important;">
    </div>
    <div class="col-md-1 pull-right">:</div>
    <div class="col-md-1 no-padding text-blue pull-right" style="margin:3px;">
        <label><b>Date</b></label>
    </div>
</div>

<div class="col-md-12 no-padding">
    <div class="no-border">
            <div class="" style="padding:10px !important;">
                <textarea style="min-height:935px !important;" class="text_area form-control" id="nursing_notes" placeholder="Enter Notes Here..."></textarea>
            </div>
    </div>
</div>
