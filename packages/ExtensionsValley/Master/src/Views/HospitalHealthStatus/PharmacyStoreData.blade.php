<table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass bg-info">
            <th width="70%" class="common_td_rules ">
                Pharmacy Store
            </th>
            <th width="30%" class="common_td_rules">
                Percentage
            </th>
        </tr>
    </thead>
    <tbody class="tbodycolor">
        <tr>
            <td class="common_td_rules ">
                Stock Out
            </td>
            <td class="td_common_numeric_rules">
                1%
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Expired Stock Value
            </td>
            <td class="td_common_numeric_rules">
                ₹ 116930
            </td>
        </tr>
        <tr>
            <td class="common_td_rules">
                Non Moving Stock Value
            </td>
            <td class="td_common_numeric_rules">
                ₹ 256312
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                POs To Be Reviewed
            </td>
            <td class="td_common_numeric_rules">
                4
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                PO Edits
            </td>
            <td class="td_common_numeric_rules">
                0.90%
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                GRN Edits
            </td>
            <td class="td_common_numeric_rules">
                1.10%
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Stock Shrinkage
            </td>
            <td class="td_common_numeric_rules">
                0.98%
            </td>
        </tr>
    </tbody>
</table>
