<table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass bg-info">
            <th width="70%" class="common_td_rules">
                Types
            </th>
            <th width="30%" class="common_td_rules">
                Percentage
            </th>
        </tr>
    </thead>
    <tbody class="tbodycolor">
        <tr>
            <td class="common_td_rules ">
                Invalid Data Entry
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['invalid_per'] ? number_format($send_array['invalid_per'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                No Shows
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['noshows_per'] ? number_format($send_array['noshows_per'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules">
                Bill Edits
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['bill_edited_per'] ? number_format($send_array['bill_edited_per'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Bill Cancellation
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['canceled_bill_per'] ? number_format($send_array['canceled_bill_per'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Refunds
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['return_bill_per'] ? number_format($send_array['return_bill_per'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
    </tbody>
</table>
