<table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass bg-info">
            <th width="70%" class="common_td_rules">
                Types
            </th>
            <th width="30%" class="common_td_rules">
                Percentage
            </th>
        </tr>
    </thead>
    <tbody class="tbodycolor">
        <tr>
            <td class="common_td_rules ">
                TAT Outliers
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['tat_outlet_per'] ? number_format($send_array['tat_outlet_per'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Machine Accuracy
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['machine_accuracy_per'] ? number_format($send_array['machine_accuracy_per'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Report Edits
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['edited_bill_per'] ? number_format($send_array['edited_bill_per'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules">
                Bill Refunds
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['bill_refund_per'] ? number_format($send_array['bill_refund_per'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Reagent Wastage
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['wasteage_data_per'] ? number_format($send_array['wasteage_data_per'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
    </tbody>
</table>
