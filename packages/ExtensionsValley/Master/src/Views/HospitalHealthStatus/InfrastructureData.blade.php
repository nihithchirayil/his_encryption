<table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead colspan="2">
        <tr class="headerclass head_data">
            <th class="common_td_rules" colspan="2">
                Infrastructure Outages
            </th>
        </tr>
    </thead>
    <tbody class="tbodycolor">
        <tr>
            <td class="common_td_rules ">
                Housekeeping
            </td>
            <td class="td_common_numeric_rules">
                0.20%
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Electrical
            </td>
            <td class="td_common_numeric_rules">
                0.48%
            </td>
        </tr>
        <tr>
            <td class="common_td_rules">
                Toilets
            </td>
            <td class="td_common_numeric_rules">
                1%
            </td>
        </tr>
    </tbody>
</table>
