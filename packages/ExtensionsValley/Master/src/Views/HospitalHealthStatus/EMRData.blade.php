<table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass head_data bg-teal-active">
            <th class="common_td_rules" colspan="2">
                Least EMR Utilization
            </th>
        </tr>
        <tr class="headerclass head_data bg-info">
            <th width='80%' class="common_td_rules">
                Speciality name
            </th>
            <th width='20%' class="common_td_rules">
                Percentage
            </th>
        </tr>
    </thead>
    <tbody class="tbodycolor">
        <?php
        $i=0;
            if(count($send_array['LeastEMRUtilization'])!=0){
                foreach ($send_array['LeastEMRUtilization'] as $key=>$val) {
                    if($i>=3){
                    break;
                }
                    ?>
        <tr>
            <td class="common_td_rules ">
                <?= trim($key) ?>
            </td>
            <td class="td_common_numeric_rules">
                <?= number_format($val, 2, '.', '') ?> %
            </td>
        </tr>
        <?php
        $i++;
                }
            }else{
                ?>
        <tr>
            <td colspan="3" style="text-align: center">
                No Result Found
            </td>
        </tr>
        <?php
            }
        ?>
    </tbody>
</table>


<table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass head_data bg-teal-active">
            <th class="common_td_rules" colspan="2">
                Incomplete Clinical Documentation
            </th>
        </tr>
        <tr class="headerclass head_data bg-info">
            <th width='80%' class="common_td_rules">
                Speciality name
            </th>
            <th width='20%' class="common_td_rules">
                Percentage
            </th>
        </tr>
    </thead>
    <tbody class="tbodycolor">
        <?php
        $i=0;
            if(count($send_array['IncompleteClinicalDocumentation'])!=0){
            foreach ($send_array['IncompleteClinicalDocumentation'] as $key=>$val) {
                if($i>=3){
                    break;
                }
            ?>
        <tr>
            <td class="common_td_rules ">
                <?= trim($key) ?>
            </td>
            <td class="td_common_numeric_rules">
                <?= number_format($val, 2, '.', '') ?> %
            </td>
        </tr>
        <?php
        $i++;
                }
            }else{
                ?>
        <tr>
            <td colspan="3" style="text-align: center">
                No Result Found
            </td>
        </tr>
        <?php
            }
        ?>
    </tbody>
</table>
