<table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass bg-info">
            <th width="70%" class="common_td_rules">
                Discharge Delay
            </th>
            <th width="30%" class="common_td_rules ">
                Percentage/Min
            </th>
        </tr>
    </thead>
    <tbody class="tbodycolor">
        <tr>
            <td class="common_td_rules ">
                Delayed Discharges
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['delay_discharge_per'] ? number_format($send_array['delay_discharge_per'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Average Delay
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['average_delay_min'] ? number_format($send_array['average_delay_min'], 2, '.', '') : 0 ?>  mins
            </td>
        </tr>
    </tbody>
</table>
