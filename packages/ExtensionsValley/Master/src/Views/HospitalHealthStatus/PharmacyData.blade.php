<table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass bg-info">
            <th width="70%" class="common_td_rules ">
                Types
            </th>
            <th width="30%" class="common_td_rules">
                Percentage/Count
            </th>
        </tr>
    </thead>
    <tbody class="tbodycolor">
        <tr>
            <td class="common_td_rules ">
                Prescription Conversion
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['prescription_conversion'] ? number_format($send_array['prescription_conversion'], 2, '.', '') : 0 ?>
                %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Missed Revenue
            </td>
            <td class="td_common_numeric_rules">
                ₹ <?= @$send_array['missed_revenue'] ? number_format($send_array['missed_revenue'], 2, '.', '') : 0 ?>
            </td>
        </tr>
        <tr>
            <td class="common_td_rules">
                Expired Stock Value
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['expired_stock_value'] ? number_format($send_array['expired_stock_value'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Non Moving Stock Value
            </td>
            <td class="td_common_numeric_rules">
                ₹
                <?= @$send_array['non_moving_stock_value'] ? number_format($send_array['non_moving_stock_value'], 2, '.', '') : 0 ?>
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Stock Out
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['stock_out'] ? number_format($send_array['stock_out'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Medicine Substitution
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['medicine_substitution'] ? number_format($send_array['medicine_substitution'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Bill Edits
            </td>
            <td class="td_common_numeric_rules">
                <?= @$send_array['pharmacy_bill_edits'] ? number_format($send_array['pharmacy_bill_edits'], 2, '.', '') : 0 ?> %
            </td>
        </tr>
    </tbody>
</table>
