@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    @include('Dashboard::dashboard.partials.headersidebar')
@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-list.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/HospitalHealthStatus.css') }}" rel="stylesheet">
@endsection
@section('content-area')

    <div class="modal fade" id="customdatapopmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 500px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Custom Date Range Selection</h4>
                </div>
                <div class="modal-body" style="min-height: 80px">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-4 date_filter_div">
                            <div class="mate-input-box">
                                <label class="filter_label">From Date</label>
                                <input type="text" data-attr="date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                    class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="from_date">
                            </div>
                        </div>
                        <div class="col-md-4 date_filter_div">
                            <div class="mate-input-box">
                                <label class="filter_label">To Date</label>
                                <input type="text" data-attr="date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                    class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="to_date">
                            </div>
                        </div>
                        <div class="col-md-4 date_filter_div pull-right" style="padding-top: 10px;">
                            <button id="searchdatabtn" onclick="getCustomDateRange()"
                                class="btn btn-primary btn-block">Search <i id="searchdataspin"
                                    class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="right_col" role="main" style="background-color:#FFFFFF !important;">
        <input type="hidden" id="daterange_hidden">

        <div class="row padding_sm">
            <input type="hidden" id="date_datahidden" value='<?= $date_array ?>'>
            <div class="col-md-12 padding_sm pull-right">
                <div class="col-md-10 padding_sm" style="color:rgb(47, 45, 173);">
                    <div class="col-md-4 padding_sm">
                        Range Type : <strong><span class='blue' id='range_typedata'></span></strong>
                    </div>
                    <div class="col-md-3 padding_sm">
                        From Date : <strong><span class='red' id='from_datadis'></span></strong>
                    </div>
                    <div class="col-md-3 padding_sm">
                        To Date : <strong><span class='red' id='to_datadis'></span></strong>
                    </div>
                </div>
                <div class="col-md-2 padding_sm pull-right" style="color:dodgerblue;">
                    <?= $title ?>
                </div>
            </div>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="col-md-10 padding_sm">
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(1)" title="Today" class="btn btn-primary btn-block">Today <i
                                class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(2)" title="Yesterday" class="btn btn-success btn-block">Yesterday <i
                                class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(3)" title="Week Till Date" class="btn btn-warning btn-block">Week
                            Till Date <i class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(4)" title="Last Week" class="btn btn-danger btn-block">Last
                            Week <i class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(5)" title="Month Till Date" class="btn btn-info btn-block">Month
                            Till Date <i class="fa fa-calendar"></i></button>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <button onclick="getCustomDates(6)" title="Last Month" class="btn bg-purple btn-block">Last
                            Month <i class="fa fa-calendar"></i></button>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">
                        <button onclick="getCustomDates(7)" title="Custom Date Range Selection"
                            class="btn bg-teal-active btn-block">Custom <i class="fa fa-calendar"></i></button>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 padding_sm" style="margin-bottom:3px;">
                    <div class="col-md-4 padding_sm header_main_class" style="background-color:#99cbd3;">
                        <div class="col-md-12 padding_sm header_main_class">
                            <strong>Front Office/Service Billing/Cash</strong>
                        </div>
                        <div class="card" style="background-color:#FFFFFF !important;">
                            <div class="card-body" id="list_data1" style="min-height: 210px;">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 no-padding no-margin header_main_class" style="background-color:#99cbd3;">
                        <div class="col-md-12 padding_sm header_main_class">
                            <strong>Revenue</strong>
                        </div>
                        <div class="card" style="background-color:#FFFFFF !important;">
                            <div class="card-body" id="list_data2" style="min-height: 210px;">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm header_main_class" style="background-color:#99cbd3;">
                        <div class="col-md-12 padding_sm header_main_class">
                            <strong>Pharmacy</strong>
                        </div>
                        <div class="card" style="background-color:#FFFFFF !important;">
                            <div class="card-body" id="list_data3" style="min-height: 210px;">
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-12 padding_sm" style="margin-bottom:3px;">
                    <div class="col-md-4 padding_sm header_main_class" style="background-color:#99cbd3;">
                        <div class="col-md-12 padding_sm header_main_class">
                            <strong>Patient Facilities</strong>
                        </div>
                        <div class="card" style="background-color:#FFFFFF !important;">
                            <div class="card-body" id="list_data4" style="min-height:403px;">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 no-padding no-margin header_main_class" style="background-color:#99cbd3;">
                        <div class="col-md-12 no-padding no-margin">
                            <div class="card">
                                <div class="col-md-12 no-padding no-margin head_data" style="background-color:#99cbd3;">
                                    <strong>Patient Feedback</strong>
                                </div>
                                <div class="card-body tbodycolor text-center" id="list_data6"
                                    style="min-height: 90px;padding-top:30px;background-color:#FFFFFF !important;">
                                    <i class="fa fa-star fa-3x orange"></i>
                                    <i class="fa fa-star fa-3x orange"></i>
                                    <i class="fa fa-star fa-3x orange"></i>
                                    <i class="fa fa-star fa-3x orange"></i>
                                    <i class="fa fa-star fa-3x gray"></i>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 no-padding no-margin">
                            <div class="card" style="margin-top: -13.5px !important;">
                                <div class="col-md-12 no-padding no-margin head_data" style="background-color:#99cbd3;">
                                    <strong>Discharge</strong>
                                </div>
                                <div class="card" style="background-color:#FFFFFF !important;">
                                    <div class="card-body" id="list_data11" style="min-height: 110px;">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding no-margin">
                            <div class="card" style="margin-top: 6px !important;">
                                <div class="col-md-12 no-padding no-margin head_data" style="background-color:#99cbd3;">
                                    <strong>Lab</strong>
                                </div>
                                <div class="card" style="background-color:#FFFFFF !important;">
                                    <div class="card-body" id="list_data8" style="min-height: 140px;">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 padding_sm header_main_class" style="background-color:#99cbd3;">
                        <div class="card" style="background-color:#FFFFFF !important;">
                            <div class="col-md-12 padding_sm head_data" style="background-color:#99cbd3;">
                                <strong>Discharge TAT</strong>
                            </div>
                            <div class="card-body" id="list_data7" style="min-height: 418.5px;">

                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-12 padding_sm" style="margin-bottom:3px;">
                    <div class="col-md-4 padding_sm header_main_class" style="background-color:#99cbd3;">
                        <div class="col-md-12 padding_sm header_main_class">
                            <strong>HR</strong>
                        </div>
                        <div class="card" style="background-color:#FFFFFF !important;">
                            <div class="card-body" id="list_data10" style="min-height: 270px;">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 no-padding no-margin header_main_class" style="background-color:#99cbd3;">
                        <div class="col-md-12 padding_sm header_main_class">
                            <strong>EMR</strong>
                        </div>
                        <div class="card" style="background-color:#FFFFFF !important;">
                            <div class="card-body" id="list_data5" style="min-height: 270px;">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm header_main_class" style="background-color:#99cbd3;">
                        <div class="col-md-12 padding_sm header_main_class">
                            <strong>Pharmacy Store</strong>
                        </div>
                        <div class="card" style="background-color:#FFFFFF !important;">
                            <div class="card-body" id="list_data9" style="min-height: 270px;">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    </div>
    </div>
    </div>

@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/HospitalHealthStatus.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js') }}"></script>
@endsection
