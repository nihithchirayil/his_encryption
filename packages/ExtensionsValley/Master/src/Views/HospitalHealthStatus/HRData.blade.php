<table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead colspan="2">
        <tr class="headerclass bg-info">
            <th width="70%" class="common_td_rules">
                HR
            </th>
            <th width="30%" class="common_td_rules ">
                Percentage
            </th>
        </tr>
    </thead>
    <tbody class="tbodycolor">
        <tr>
            <td class="common_td_rules ">
                Attrition Rate
            </td>
            <td class="td_common_numeric_rules">
                10.70%
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Employee Satisfaction Index
            </td>
            <td class="td_common_numeric_rules">
                30%
            </td>
        </tr>
        <tr>
            <td class="common_td_rules">
                Unauthorized Absence
            </td>
            <td class="td_common_numeric_rules">
                20%
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Vaccination Percentage
            </td>
            <td class="td_common_numeric_rules">
                60%
            </td>
        </tr>
        <tr>
            <td class="common_td_rules ">
                Mandatory Training
            </td>
            <td class="td_common_numeric_rules">
                50%
            </td>
        </tr>
    </tbody>
</table>
