<table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
        <tr class="headerclass head_data bg-teal-active">
            <th class="common_td_rules" colspan="3">
                Low Revenue Specialities
            </th>
        </tr>
        <tr class="headerclass head_data bg-info">
            <th class="common_td_rules">
                Speciality name
            </th>
            <th class="common_td_rules">
                Patient Count
            </th>
            <th class="common_td_rules">
                Revenue
            </th>
        </tr>
    </thead>
    <tbody class="tbodycolor">
        <?php
            if(count($send_array)!=0){
                foreach ($send_array as $each) {
                    ?>
        <tr>
            <td class="common_td_rules ">
                <?= trim($each->name) ?>
            </td>
            <td class="td_common_numeric_rules">
                <?= $each->patient_count ?>
            </td>
            <td class="td_common_numeric_rules">
                ₹ <?= number_format($each->net_amount, 2, '.', ''); ?>
            </td>
        </tr>
        <?php
                }
            }else{
                ?>
        <tr>
            <td colspan="3" style="text-align: center">
                No Result Found
            </td>
        </tr>
        <?php
            }
        ?>
    </tbody>
</table>
