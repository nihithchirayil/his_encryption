{{--*/
    $resultData = $resultData[0]->get_report_data;
    $resultData = json_decode($resultData);
/*--}}    
    
   @if(sizeof($resultData)>0)
   <?php  $headderArray = $resultData[0]; ?>
    <div class="theadscroll" style="position: relative; max-height: 340px;">
      
            <table id="myTable" class="table table_sm no-margin theadfix_wrapper table-striped">
                <thead>
                    <tr style="background-color: #869c8f;color:white">
                    @foreach($headderArray as $key=>$value)
                    <th>{{$key}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($resultData as $data)
                <tr>
                    @foreach($headderArray as $key=>$value)
                       <td>{{$data->$key}}</td>
                    @endforeach
                </tr>
                @endforeach
                </tbody>
            </table>            
    </div>
   @else
   <div class="col-md-12">
       <label>No results Found!</label>
   </div>
   @endif
