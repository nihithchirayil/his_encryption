@if($offset == 0)
<div class="row">
    <div class="col-md-12 theadscroll" id="load_data" style="position: relative; height: 450px;">
        <table class="table table_round_border theadfix_wrapper " id="receipt_table1">
            <thead>
                <tr class="table_header_bg">
                    <th style="text-align:center;width:15%">UHID</th>
                    <th style="text-align:center">Patient name</th>
                    <th style="text-align:center;width:12%">Adv. Amount</th>
                    <th style="text-align:center;width:15%">Refund</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($receivedData))
                @php $pat_adv = $inv_adv =0; @endphp
                @foreach($receivedData as $rec)
                @php
                $inv_adv = !empty($rec->insurance_advance) ? $rec->insurance_advance:0;
                $pat_adv = !empty($rec->patient_advance	) ? $rec->patient_advance:0;
                $total = $inv_adv + $pat_adv;
                @endphp
                <tr>
                    <td class="common_td_rules">{{ !empty($rec->uhid) ? $rec->uhid :'' }}</td>
                    <td class="common_td_rules">{{ !empty($rec->patient_name) ?$rec->patient_name :'' }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($pat_adv, 2, '.', ',')}}</td>
                    <td class=" common_td_rules text-center" style="text-align: center !important">
                        <button class='btn btn-success rfndbtn' type="button" onclick="refundAdvAmountLoad('{{$rec->id}}','{{$rec->patient_name}}','{{$pat_adv}}')">Refund <i class="fa fa-reply" id="refund_{{$rec->id}}"></i></button>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">No Records Found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>

<script>

    $('#load_data').on('scroll', function() {
    var scrollHeight = $('#load_data').height();
    var scrollPosition = $('#load_data').scrollTop() + $('#load_data').innerHeight();
    if (scrollPosition + 3 >= $('#load_data')[0].scrollHeight){
    offset = offset + limit;
    console.log(limit + '##' + offset + '##' + total_rec);
    if (offset < total_rec){
    setTimeout(function(){
    searchList(limit, offset);
    }, 500);
    }
    }
    })
</script>

@else
@if(isset($receivedData))
@php $pat_adv = $inv_adv =0; @endphp
@foreach($receivedData as $rec)
@php
$inv_adv = !empty($rec->insurance_advance) ? $rec->insurance_advance:0;
$pat_adv = !empty($rec->patient_advance	) ? $rec->patient_advance:0;
$total = $inv_adv + $pat_adv;
@endphp
<tr>
    <td class="common_td_rules">{{ !empty($rec->uhid) ? $rec->uhid :'' }}</td>
    <td class="common_td_rules">{{ !empty($rec->patient_name) ?$rec->patient_name :'' }}</td>
    <td class="td_common_numeric_rules">{{ number_format($pat_adv, 2, '.', ',')}}</td>

    <td class=" common_td_rules text-center" style="text-align: center !important">
        <button class='btn btn-success rfndbtn' type="button" onclick="refundAdvAmountLoad('{{$rec->id}}','{{$rec->patient_name}}','{{$pat_adv}}')">Refund <i class="fa fa-reply" id="refund_{{$rec->id}}"></i></button>
    </td>
</tr>
@endforeach
@else
<tr>
    <td colspan="5">No Records Found</td>
</tr>
@endif
@endif
