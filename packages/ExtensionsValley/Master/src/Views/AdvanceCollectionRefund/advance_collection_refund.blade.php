@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
    <style>

    </style>
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <div class="right_col">
        <div class="row" style="text-align: right;font-size: 12px; font-weight: bold;"> <label
                style="font-weight:600">{{ $title }}</label></div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            {!! Form::token() !!}
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Patient Name/UHID</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control hidden_search"
                                        name="patient_name" id="patient_name" value="" autocomplete="false">
                                    <input type="hidden" class="form-control" name="patient_name_hidden"
                                        id="patient_name_hidden" value="">
                                    <div id="AjaxDiv_patient_name" class="ajaxSearchBox"
                                        style="margin: 14px 0px 0px 0px !important;z-index: 9999999;max-height: 250px !important">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg"
                                    onclick="searchList(20,0)"><i id="searchdataspin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                    Clear</a>
                            </div>
                            <div class="col-md-1 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="add_advance_btn" type="button" class="btn btn-block light_purple_bg"
                                    onclick="loadCashCollection()">
                                    <i id="addamntspin" class="fa fa-plus"></i>
                                    Add</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin" id="common_list_div">

                </div>
            </div>

        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="modalCashRecive">
        <div class="modal-dialog" role="document" style="width:85%;">
            <div class="modal-content">
                <div class="modal-header box_header" style="background-color:#01987a !important;color:#fff">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h3 class="modal-title">Advance Collection</h3>
                </div>
                <div class="modal-body" style="min-height:470px;" id="modalCashReciveBody">

                </div>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="modalAdvanceRefund">
        <div class="modal-dialog" role="document" style="width:55%;">
            <div class="modal-content" style="height: 210px">
                <div class="modal-header box_header" style="background-color:#01987a !important;color:#fff">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h3 class="modal-title">Advance Refund -<span id="patient_title"></span></h3>
                </div>
                <div class="modal-body" style="min-height:470px;" id="modalAdvRefundBdy">
                    <input type="hidden" name="advance_refund_amnt_hidden" id="advance_refund_amnt_hidden"
                        value="">
                    <input type="hidden" name="advance_refund_pt_id_hidden" id="advance_refund_pt_id_hidden"
                        value="">
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Amount</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                name="advance_amount_to_pay" id="advance_amount_to_pay" value=""
                                autocomplete="false" onkeyup="checkRefundAmnt(this)">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm ">
                        <div class="mate-input-box">
                            <label>Payment Mode</label>
                            <div class="clearfix"></div>
                            {!! Form::select('payment_mode_refund', $payment_mode, 1, [
                                'class' => 'form-control ',
                                'id' => 'payment_mode_refund',
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-2" style="margin-top:25px">
                        <button style="width:113px;" type="button" class="btn btn-primary"
                            onclick="saveCollectionRefundAmount();" id="save_refund_payment">
                            <i class="fa fa-save" id="save_refund_payment_spn"></i> Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/advance_collection_refund.js') }}"></script>
    <script type="text/javascript"></script>

@endsection
