
<style>
    .ajaxSearchBox{
        display: none;
        width: 250px !important;
        height: 400px !important;
        overflow: scroll;
        padding-top: 0px !important;
        margin-top: 15px !important;
    }
    .liHover{
        background: #ccebbc !important;
    }
    .ajaxSearchBox{
        z-index: 9999 !important;
    }
    .ajaxSearchBox>li{
        font-size:13px !important;
        font-family: Arial !important;
    }
    .payment_mode{
        width:175px !important;
    }
    .amount_table{
        border-spacing:10px !important;
        border:1px solid white !important;
        border-collapse: collapse !important;
    }
    .amount_table >tbody>tr{
        height:30px !important;
    }
   .payment_mode :hover{
        background-color:#02cf99;
        color:white;
   }
   .blink_me {
        animation: blinker 1s linear infinite;
    }
    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

    @media print {
        .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #efefef;
        }


        .theadscroll {max-height: none; overflow: visible;}
        .header_bg {
            background-color: #08898d;
            color: white;
        }
        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #f2fef5;
        }

        #resultDataTable{
            border: 1px solid #cedfe9 !important;
            border-collapse: collapse !important;
        }
        td{
            padding-left: 5px !important;
        }

    }
</style>

<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}"/>
<input type="hidden" id="c_token" value="{{csrf_token()}}"/>
<input type="hidden" id="payment_mode_detail" value="{!!$payment_mode_detail_array!!}"/>

<div class="">
    <div class="col-md-12 box-body text-right" style="padding-right:10px !important;">
        <div class="col-md-2">
            <label class="filter_label ">Logged User : {{$user_name}}</label>
        </div>
        <div class="col-md-2">
            <label class="filter_label ">Counter :</label>
        </div>
        <div class="col-md-2">
            {!! Form::select('counter', $counter_list, 0, ['class' => 'form-control select2 ', 'id' => 'counter', 'style' => '']) !!}
        </div>

    </div>
    <div class="col-md-12 box-body" style="padding-top:8px;">


        <div class="col-md-12" >

            <div class="col-md-2">
                Patient Name :
            </div>
            <div class="col-md-4">
                <input class="form-control hidden_search"  value=""  autocomplete="off" type="text" id="patient_name_add" name="patient_name_add" />
                <input type="hidden" class="form-control" name="patient_name_add_hidden"
                                        id="patient_name_add_hidden" value="">
                                    <div id="AjaxDiv_patient_name_add" class="ajaxSearchBox"
                                        style="margin: -5px 0px 0px 0px !important;z-index: 9999999;max-height: 250px !important;width:100% !important">
                                    </div>
            </div>

        </div>

        <div class="clearfix"></div>

        <div class="col-md-12" style="margin-top:8px;">

            <div class="col-md-2">
                UHID :
            </div>
            <div class="col-md-4">
                <input class="form-control" disabled="disabled" value="" autocomplete="off" type="text" autocomplete="off" id="uhid" name="uhid"/>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="col-md-12" style="margin-top:8px;">

            <div class="col-md-2">
                Advance Amount :
            </div>
            <div class="col-md-4">
                <input class="form-control"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                    value="" autocomplete="off" type="text"  autocomplete="off"  id="advance_amount" name="advance_amount" />
            </div>
            <div class="col-md-2">
                Current Advance Amount :
            </div>
            <div class="col-md-2">
                <input class="form-control"  disabled="disabled" id="current_adv_amnt" value=""/>
            </div>
        </div>


        <div class="clearfix"></div>


    </div>

    <div class="col-md-12 box-body" style="padding-top:8px;">
        <div class="col-md-2">
            <button type="button" onclick="changePaymentMode('1');" class="btn bg-blue payment_mode">
               CASH
            </button>
            <button type="button" onclick="changePaymentMode('4');" class="btn bg-blue payment_mode">
               CHEQUE
            </button>
            <button type="button" onclick="changePaymentMode('2');" class="btn bg-blue payment_mode">
               CREDIT CARD
            </button>
            <button type="button" onclick="changePaymentMode('3');" class="btn bg-blue payment_mode">
               DEBIT CARD
            </button>
            <button type="button" onclick="changePaymentMode('14');" class="btn bg-blue payment_mode">
               UPI
            </button>
        </div>



        <div class="col-md-10 box-body">
            <div class="col-md-12 no-padding">
                <div class="col-md-1 no">
                    <label>Amount</label>
                    <input class="form-control" value=""  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" autocomplete="off" type="text"  autocomplete="off"  id="amount" name="amount"/>
                </div>
                <div class="col-md-1" style="width:120px;">
                    <label>Payment Mode</label>
                    {!! Form::select('payment_mode', $payment_mode, 1, ['class' => 'form-control select2 ', 'id' => 'payment_mode','placeholder'=>'select', 'style' => '']) !!}
                </div>

                <div class="col-md-2">
                    <label>Machine Bank</label>
                    {!! Form::select('machine_bank', $mechine_bank_list, 0, ['class' => 'form-control select2 ', 'id' => 'machine_bank','placeholder'=>'select', 'style' => '']) !!}
                </div>
                <div class="col-md-2">
                    <label>Bank</label>
                    {!! Form::select('bank', $bank_list, 0, ['class' => 'form-control select2 ', 'id' => 'bank','placeholder'=>'select', 'style' => '']) !!}
                </div>
                <div class="col-md-1">
                    <label>Card No</label>
                    <input class="form-control" value="" autocomplete="off" type="text"  autocomplete="off"  id="card_no" name="card_no"/>
                </div>
                <div class="col-md-2">
                    <label>Phone No</label>
                    <input class="form-control" value="" autocomplete="off" type="text"  autocomplete="off"  id="phone_no" name="phone_no"/>
                </div>
                <div class="col-md-1">
                    <label>Exp Month</label>
                    <input class="form-control"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="checkContentLength('exp_month',2);" value="" autocomplete="off" type="text"  autocomplete="off"  id="exp_month" name="exp_month"/>
                </div>
                <div class="col-md-1">
                    <label>Exp Year</label>
                    <input class="form-control"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="checkContentLength('exp_year',4);" value="" autocomplete="off" type="text"  autocomplete="off"  id="exp_year" name="exp_year"/>
                </div>
                <div class="col-md-1" style="width:40px;padding-top:24px;">
                    <button type="button" class="btn btn-primary" value="" id="add_new_mode" name="add_new_mode" onclick="addNewPaymentMode();">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding floatThead-table" id="payment_mode_table">
                    <thead style="background-color: cadetblue;color:white;">
                        <tr>
                            <td>Payment Mode</td>
                            <td>Amount Paid</td>
                            <td>Machine Bank</td>
                            <td>Bank</td>
                            <td>Card No</td>
                            <td>Exp Date</td>
                            <td>Phone No</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody >

                    </tbody>
                </table>
            </div>

            <div class="col-md-12">
               <table style="width:100%;" class="amount_table">
                   <tr>
                       <td width="23%"><label>Balance Amount</label></td>
                       <td width="2%">:</td>
                       <td width="23%"><input class="form-control"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text" name="balance_amount" id="balance_amount" disabled="disabled" value=""/></td>

                       <td width="23%" style="text-align:right;">
                           <button style="width:113px;" type="button" class="btn btn-primary" name="save_bill_payment" onclick="saveCollectionAmount();" id="save_bill_payment">
                            <i class="fa fa-save" id="save_bill_payment_spn"></i> Save</button>
                       </td>
                   </tr>
               </table>
            </div>

        </div>
    </div>
</div>
<script>
    $('.datepicker').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    $(".number_only").keydown(function(event) {
        // Allow only backspace and delete
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190) {
            // let it happen, don't do anything
        } else {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
            }
        }
    });

    $('#edited_advance_amount').keyup(function () {
        var edited_advance_amount = $('#edited_advance_amount').val();
        var advance_collected_hidden = parseInt($('#advance_collected_hidden').val());
        if(edited_advance_amount == ''){
            edited_advance_amount = 0;
        }
        edited_advance_amount = advance_collected_hidden -edited_advance_amount;
        edited_advance_amount = parseInt(edited_advance_amount);
        var remaining_amount = parseInt($('#remaining_amount').val());
        var balance_amount = remaining_amount;
        var new_balance_amount = edited_advance_amount+balance_amount;
        $('#balance_amount').val(new_balance_amount);

        //---amount to pay-------------
        var amount_to_pay = parseInt($('#amount_to_be_pay_hidden').val());
        var new_amount_to_pay = edited_advance_amount+amount_to_pay;
        $('#amount_to_pay').val(new_amount_to_pay);
        $('#amount_to_be_paid').val(new_amount_to_pay);

        //---amount entry colum--------------


    });
    $('.hidden_search').keyup(function (event) {
var input_id = '';
var keycheck =  /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
var value = event.key; //get the charcode and convert to char
input_id = $(this).attr('id');
var url = base_url+'/advance_colrefnd/advanceCollectionRefund';
if (value.match(keycheck)  || event.keyCode == '8' || event.keyCode == 46) {
    if ($('#'+input_id+'_hidden').val() != "") {
        $('#'+input_id+'_hidden').val('');
    }
    var search_key = $(this).val();
    search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
    search_key = search_key.trim();

    var datastring = '';
    if (search_key == "") {
        $("#AjaxDiv_"+input_id).html("");
    } else {
        $.ajax({
            type: "GET",
            url: url,
            data: 'op_no_search=' + search_key+'&input_id='+input_id,
            beforeSend: function () {

                $("#AjaxDiv_"+input_id).html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
            },
            success: function (html) {
                if (html == 0) {
                    $("#AjaxDiv_"+input_id).html("No results found!").show();

                    $("#AjaxDiv_"+input_id).find('li').first().addClass('liHover');

                    return "<span>No result found!!</span>";

                } else {
                    $("#AjaxDiv_"+input_id).html(html).show();
                    $("#AjaxDiv_"+input_id).find('li').first().addClass('liHover');

                }

            },

            complete: function () {
            },
            error: function () {
                Command: toastr["error"]("Network Error!");
                return;
            },

        });
    }
} else {
    ajaxProgressiveKeyUpDown('AjaxDiv_'+input_id, event);
}
});

function fillSearchDetials(id,uhid,name,input_id,current_adv_amnt) {
$('#'+input_id+"_hidden").val(id);
if(input_id == 'patient_name_add'){
    $("#current_adv_amnt").val(current_adv_amnt);
    $('#'+input_id).val(name);
    $("#uhid").val(uhid);
}else{
    $('#'+input_id).val(name+'['+ uhid+']');
}

 $("#AjaxDiv_"+input_id).hide();
}
</script>
