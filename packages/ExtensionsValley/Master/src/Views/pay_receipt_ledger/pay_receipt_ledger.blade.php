@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
/*    .select2-container--default .select2-selection--single {
        border-radius: 0px !important; line-height: 0px !important;
        
    }
    .select2-container {
        width: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 22px !important;
}
ul.pagination {
    margin: 0;
}
.select2-container--default .select2-results>.select2-results__options {
    max-height: 370px !important;
    overflow-y: auto;
}
.select2-container--default .select2-selection--single {
    border: 1px solid #CCC !important;
}*/
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <?php $c_payment_mod = \DB::table('payment_mode')->Where('status', 1)->orderBy('name', 'asc')->pluck('name', 'id'); ?>
        <div class="col-md-12 padding_sm">
            {!!Form::open(array('url' => $saveUrl, 'method' => 'get', 'id'=>'form'))!!}
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-xs-3 padding_sm" style="padding-bottom: 10px;">
                           <div class="date custom-float-label-control">
                                <?php //$payment_receipt = \WebConf::getConfig('PayReceiptLedgerTypes'); ?>
                               <label class="custom_floatlabel">&nbsp;</label>

                            </div>
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="payment_receipt_p" name="payment_receipt" value="1" name="type" checked="" onchange="changePaymentType(this.value)">
                                <label for="payment_receipt_p"> Payment </label>
                            </div>
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="payment_receipt_r" name="payment_receipt" value="2" name="type" onchange="changePaymentType(this.value)">
                                <label for="payment_receipt_r"> Receipt </label>
                            </div>
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="payment_receipt_c" name="payment_receipt" value="3" name="type" onchange="changePaymentType(this.value)">
                                <label for="payment_receipt_c"> Contra </label>
                            </div>
                        </div>
                        <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <div class="mate-input-box">
                                <label class="custom_floatlabel">Bank</label>
                                <?php
                                $bank_details = \DB::table('ledger_master')->Where('is_bank', 1)
                                        ->orderBy('ledger_name', 'asc')
                                        ->pluck('ledger_name', 'id');
                                ?>
                                {!! Form::select('bank_name',$bank_details,'',['class' => 'form-control','placeholder' => 'Bank','title' => 'Bank','id' => 'bank_details','style' => 'color:#555555; padding:2px 12px;','onchange' => 'selectCurrentBalance(this.value);']) !!}
                            </div>
                            </div>
                        </div>
                        <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <div class="mate-input-box" id="current_balance_load">
                                <label class="custom_floatlabel">Current Balance</label>
                                <input type="text" id="current_balance" class="form-control" readonly="" value="0" >                          
                            </div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                <label for="">Vouchr. Type</label>
                                <div class="clearfix"></div>
                                {!! Form::select('v_type', array(""=> " Select") + $v_type->toArray(),'',
                                        ['class'=>"form-control v_type", 'id'=>"v_type_1"]) !!}
                            </div></div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="height: 450px;">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="pay_rec_id">
                            <thead>
                                <tr class="table_header_bg">
                                    <th width="5%">Ledger</th>
                                    <th width="8%">Reference No </th>
<!--                                    <th width="3%">Reff. date</th>-->
                                    <th width="3%" style="display: none">Amount</th>
                                    <th width="3%">Paid amount</th>
                                    <th width="4%" style="display: none">Payment Mode</th>
                                    <th width="7%">Narration</th>
                                    <th width="2%"><i class="fa fa-plus" onclick="fill_new_row()" style="cursor: pointer"></i></th>
                                </tr>
                            </thead>
                            <tbody id="payment_receipt_tbody">
                                <tr>
                                    <td>
                                        <input type="text" value=""  name="payee[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event)" id="ledger_item_desc-1">
                                        <input type="hidden" value="" name="payee_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-1">
                                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-1" 
                                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                                             margin: -2px 0px 0px 0px;overflow-y: auto; width: 20%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                        <input type="text" value="" name="referrence_no[]" class="form-control" id="edit_bill_no_1"> 
                                        <div class="input-group-btn">
                                    <span class="btn btn_green_bg">
                                         <i class="fa fa-search " onclick="search_bill_no('1')" style="cursor:pointer" ></i>
                                    </span></div>
                                        </div>
                                    </td>
<!--                                    <td>
                                        <input type="text" value="" id="edit_bill_date_1" name="bill_date[]" autocomplete="off"  class="form-control datepicker">
                                    </td>-->
                                    <td style="display: none">
                                        <input type="text" value="" name="amount[]" id="edit_bill_amount_1" autocomplete="off"  class="form-control bill_amount"  onkeyup="number_validation(this);calculate_total()">
                                    </td>
                                    <td>
                                        <input type="text" value="" name="paid_amnt[]" class="form-control bill_paid_amount" id="edit_bill_paid_amount_1" onkeyup="number_validation(this);calculate_balance();"> 
                                    </td>
                                    <td style="display: none">
                                        {!! Form::select('payment_mode[]',$c_payment_mod,'',['class' => 'form-control','placeholder' => 'Payment Mode','title' => 'Payment Mode','style' => 'color:#555555; padding:2px 12px;']) !!}
                                    </td>
                                    <td >
                                        <input type="text" value="" name="naration[]"  class="form-control"> 
                                    </td>
                                    <td>
                                        <i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                        <div class="col-xs-2 padding_sm" style="padding-bottom: 10px; display: none">
                            <div class="date custom-float-label-control">
                                <label class="custom_floatlabel">Total Amount</label>
                                <input type="text" id="total_amount_by_bill" class="form-control" readonly="" value="" >                          
                            </div>
                        </div>
                        <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <label class="custom_floatlabel"> <b>Total Paid</b></label>
                                 <label class="custom_floatlabel"  style="color: red"><h4 id="ttl_dif_label"></h4></label>
                                 <input type="hidden" id="paid_total" class="form-control" readonly="" value="" name="total_paid" >                          
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="float: right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block light_purple_bg" onclick="validate();" ><i class="fa fa-save" id="save_spin_icon"></i> Save</span>
                        </div>
                        <div class="col-md-1 padding_sm" style="float: right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span type="button" class="btn btn-warning pull-right" onclick="backToList()" >
                         <i class="fa fa-times"></i>  Cancel</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
            {!! Form::token() !!} {!! Form::close() !!}
        </div>
    </div>
</div>
<div id="search_bill_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 1200px; width: 40%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" >Bill search <span id="modal_itemcodehr"> </span></h4>
            </div>
            <div class="modal-body" style="padding: 7px 15px;" id="itemts_batchwisediv">
                <div class="row" style="padding-bottom: 100px;">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Voucher No</label>
                            <input type="text" value="" autocomplete="off" name="voucher_no"  class="form-control" id="voucher_no" onkeyup="searchVoucherNo(this.id, event)" style="height:33px !important;">
                            <div class="ajaxSearchBox" id="search_voucher_no" style="">
                            </div>
                        </div>
                    </div>
                    <?php $bills = \DB::table('ledger_booking_bill_details')->pluck('bill_no','bill_no'); ?>
                    <div class="col-md-8">
                        <div class="mate-input-box">
                            <label for="" class="header_label">Bill No</label>
                        {!! Form::select('bills',$bills,null,
                            ['class' => 'form-control','title' => 'bills',
                            'id' => 'bills','style' => 'color:#555555;', 'multiple'=>'multiple']) !!}                        
                        </div>
                    </div>
                    <input type="hidden" id="search_bill_modal_id" value="">
                </div>
            </div>
            <div class="modal-footer" style="padding: 2px 7px;">
            <span class="btn btn-warning"  data-dismiss="modal" style="padding: 0px 2px;"><i class="fa fa-times"> Cancel</i></span>
            <span class="btn btn-success" id="go" style="padding: 0px 2px;"><i class="fa fa-thumbs-up"> OK</i></span>
    
        </div>
        </div>

    </div>
    </div>
</div>
<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
<input type="hidden" id="cash_payment_mod_hidden" value="<?php echo htmlspecialchars($cash_payment_mod_json, ENT_COMPAT); ?>">
<input type="hidden" id="bank_hidden" value="<?php echo htmlspecialchars($bank_details_json, ENT_COMPAT); ?>">
<input type="hidden" id="default_payment_bank" value="<?php echo htmlspecialchars($payment_bank_json, ENT_COMPAT); ?>">
<input type="hidden" id="default_rec_bank" value="<?php echo htmlspecialchars($rec_bank_json, ENT_COMPAT); ?>">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/pay_receipt_ledger.js")}}"></script>

<script type="text/javascript">
                                           
</script>
@endsection
