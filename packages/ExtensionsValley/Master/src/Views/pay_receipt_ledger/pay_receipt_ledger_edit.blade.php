@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
/*    .select2-container--default .select2-selection--single {
        border-radius: 0px !important; line-height: 0px !important;

    }
    .select2-container {
        width: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #444;
        line-height: 22px !important;
    }
    ul.pagination {
        margin: 0;
    }
    .select2-container--default .select2-results>.select2-results__options {
        max-height: 370px !important;
        overflow-y: auto;
    }
    .select2-container--default .select2-selection--single {
        border: 1px solid #CCC !important;
    }*/
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <?php $c_payment_mod = \DB::table('payment_mode')->Where('status', 1)->orderBy('name', 'asc')->pluck('name', 'id'); ?>
        <div class="col-md-12 padding_sm">
            {!!Form::open(array('url' => $saveUrl, 'method' => 'get', 'id'=>'form'))!!}
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-xs-3 padding_sm" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <?php
                                //$payment_receipt = \WebConf::getConfig('PayReceiptLedgerTypes');
                                $selected_type_value = $head_details[0]->type;
                                if ($selected_type_value == 2) {
                                    $income_checked = 'checked';
                                    $payment_checked = '';
                                    $contra_checked = '';
                                } else if ($selected_type_value == 1) {
                                    $income_checked = '';
                                    $contra_checked = '';
                                    $payment_checked = 'checked';
                                } else {
                                    $income_checked = '';
                                    $contra_checked = 'checked';
                                    $payment_checked = '';
                                }
                                ?>

                                <div class="radio radio-primary radio-inline">
                                    <input type="radio" id="payment_receipt_p" name="payment_receipt" value="1"  <?= $payment_checked ?> onchange="changePaymentType(this.value)">
                                    <label for="payment_receipt_p"> Payment </label>
                                </div>
                                <div class="radio radio-primary radio-inline">
                                    <input type="radio" id="payment_receipt_r" name="payment_receipt" value="2" <?= $income_checked ?> onchange="changePaymentType(this.value)">
                                    <label for="payment_receipt_r"> Receipt </label>
                                </div>
                                <div class="radio radio-primary radio-inline">
                                    <input type="radio" id="payment_receipt_c" name="payment_receipt" value="3" <?= $contra_checked ?> name="type" onchange="changePaymentType(this.value)">
                                    <label for="payment_receipt_c"> Contra </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <div class="mate-input-box">
                                    <label class="custom_floatlabel">Bank</label>
                                    <?php
                                    $selected_bank_value = $head_details[0]->bank;
                                    $bank_details = \DB::table('ledger_master')->Where('is_bank', 1)
                                            ->orderBy('ledger_name', 'asc')
                                            ->pluck('ledger_name', 'id');
                                    ?>
                                    {!! Form::select('bank_name',$bank_details,$selected_bank_value,['class' => 'form-control','placeholder' => 'Select','title' => 'Bank','id' => 'bank_details','style' => 'color:#555555; padding:2px 12px;','onchange' => 'selectCurrentBalance(this.value);']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                            <div class="date custom-float-label-control">
                                <div class="mate-input-box" id="current_balance_load">
                                    <label class="custom_floatlabel">Current Balance</label>
                                    <input type="text" id="current_balance" class="form-control" readonly="" value="{{$head_details[0]->current_balance}}" >                          
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Vouchr. Type</label>
                                <div class="clearfix"></div>
                                @php $selected_voucher_value = $head_details[0]->voucher_type; 
                                $voucher_no = (isset($head_details[0]->voucher_no))?$head_details[0]->voucher_no:'';
                                @endphp
                                {!! Form::select('v_type', array(""=> " Select") + $v_type->toArray(),$selected_voucher_value??'',
                                ['class'=>"form-control v_type", 'id'=>"v_type_1"]) !!}
                            </div></div>
                        <div class="col-md-3 padding_sm  pull-right" style=" padding-top: 10px !important">
                            <label for=""style="float:right">Voucher No : <span style="color: red"><b><?= $voucher_no ?></b></span></label>
                            <input type="hidden" value="<?= $voucher_no ?>" name="vouchr_no">
                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="height: 450px;">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table" id="pay_rec_id">
                            <thead>
                                <tr class="table_header_bg">
                                    <th width="5%">Ledger</th>
                                    <th width="8%">Reference No </th>
<!--                                    <th width="3%">Reff. date</th>-->
                                    <th width="3%" style="display:none">Amount</th>
                                    <th width="3%" >Paid amount</th>
                                    <th width="4%" style="display:none">Payment Mode</th>
                                    <th width="7%">Narration</th>
                                    <th width="1%"><i class="fa fa-plus " onclick="fill_new_row()"  style="cursor: pointer"></i></th>
                                </tr>
                            </thead>
                            <tbody id="payment_receipt_tbody">
                                <?php
                                if (isset($detail_list) && sizeof($detail_list) > 0) {
                                    $k = 1;
                                    foreach ($detail_list as $dtls) {
                                        $ref_detail = $dtls->ref_detail;
                                     
                                        ?>
                                        <tr id="ledger_row_id_<?= $dtls->id ?>">
                                            <td>
                                                <input type="hidden"  value="{{$dtls->id}}" name="detail_id[]" autocomplete="off"  class="form-control ">
                                                <input type="text"   value="{{$dtls->ledger_name}}"  name="payee[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event)" id="ledger_item_desc-<?= $k ?>">
                                                <input type="hidden" value="{{$dtls->ledger_id}}"  name="payee_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-<?= $k ?>">
                                                <input type="hidden" value="{{$dtls->ref_detail}}" name="ref_detail[]"  autocomplete="off" class="form-control" >
                                                <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-<?= $k ?>" 
                                                     style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                                                     margin: -2px 0px 0px 0px;overflow-y: auto; width: 20%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;
                                                     border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="text"  value="{{$ref_detail}}" name="referrence_no[]" class="form-control" id="edit_bill_no_<?= $k ?>">
                                                    <div class="input-group-btn">

                                                        <span class="btn btn_green_bg">
                                                            <i class="fa fa-search " onclick="search_bill_no('<?= $k ?>')" style="cursor:pointer" ></i>
                                                        </span>
                                                    </div>
                                                </div>

                                            <td style="display:none">
                                                <input type="text"  value="{{$dtls->amount}}" name="amount[]" autocomplete="off"  class="form-control bill_amount"  id="edit_bill_amount_<?= $k ?>" onkeyup="number_validation(this);calculate_total();">
                                            </td>
                                            <td>
                                                <input type="text" readonly="" value="{{$dtls->amount}}" name="paid_amnt[]" class="form-control bill_paid_amount" onkeyup="number_validation(this);calculate_balance();" id="edit_bill_paid_amount_<?= $k ?>"> 
                                            </td>
                                            <td style="display:none">
                                                {!! Form::select('payment_mode[]',$c_payment_mod,'',['class' => 'form-control','placeholder' => 'Payment Mode','title' => 'Payment Mode','style' => 'color:#555555; padding:2px 12px;']) !!}
                                            </td>
                                            <td title="{{$dtls->naration}}">
                                                <input type="text" value="{{$dtls->naration}}" name="naration[]"  class="form-control"> 
                                            </td>
                                            <td>
                                                <i class="fa fa-trash-o " onclick="deleteLedgerDetails('<?= $dtls->id ?>','<?= $dtls->amount ?>')" style="cursor:pointer"></i>
                                            </td>
                                        </tr>
                                        <?php
                                        $k++;
                                    }
                                }
                                ?>
                                <tr>
                                    <td>
                                        <input type="text" value="" name="payee[]"  autocomplete="off" class="form-control ledger_item_desc" onkeyup="select_item_desc(this.id, event)" id="ledger_item_desc-<?= $k ?>">
                                        <input type="hidden" value="" name="payee_id[]"  autocomplete="off" class="form-control " id="ledger_item_desc_hidden-<?= $k ?>">
                                        <div class="ajaxSearchBox search_ledger_item_box" id="search_ledger_item_box-<?= $k ?>" 
                                             style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px;
                                             margin: -2px 0px 0px 0px;overflow-y: auto; width: 20%; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <input type="text" value="" name="referrence_no[]" class="form-control" id="edit_bill_no_<?= $k ?>"> 
                                            <div class="input-group-btn">
                                                <span class="btn btn_green_bg">
                                                    <i class="fa fa-search " onclick="search_bill_no('<?= $k ?>')" style="cursor:pointer" ></i>
                                                </span>
                                            </div>
                                        </div>
                                    </td>
                                    
                                    <td style="display:none">
                                        <input type="text" value="" name="amount[]" autocomplete="off"  class="form-control bill_amount"  onkeyup="number_validation(this);calculate_total();" id="edit_bill_amount_<?= $k ?>">
                                    </td>
                                    <td>
                                        <input type="text" value="" name="paid_amnt[]" class="form-control bill_paid_amount" onkeyup="number_validation(this);calculate_balance();" id="edit_bill_paid_amount_<?= $k ?>"> 
                                    </td>
                                    <td style="display:none">
                                        {!! Form::select('payment_mode[]',$c_payment_mod,'',['class' => 'form-control','placeholder' => 'Payment Mode','title' => 'Payment Mode','style' => 'color:#555555; padding:2px 12px;']) !!}
                                    </td>
                                    <td >
                                        <input type="text" value="" name="naration[]"  class="form-control"> 
                                    </td>
                                    <td>
                                        <i class="fa fa-trash-o delete_return_entry_added" style="cursor:pointer"></i>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>


                    </div>
                    <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;display:none">
                        <div class="date custom-float-label-control">
                            <label class="custom_floatlabel">Total Amount</label>
                            <input type="text" id="total_amount_by_bill" class="form-control" readonly="" value="" >                          
                        </div>
                    </div>
                    <div class="col-xs-2 padding_sm" style="padding-bottom: 10px;">
                        <div class="date custom-float-label-control">
                            <label class="custom_floatlabel"> <b>Total Paid</b></label>
                            <label class="custom_floatlabel"  style="color: red"><h4 id="ttl_dif_label"></h4></label>
                            <input type="hidden" id="paid_total" class="form-control" readonly="" value="" name="total_paid" >                          
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm" style="float: right">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <span class="btn btn-block light_purple_bg"  onclick="validate();"><i class="fa fa-save" id="save_spin_icon"></i> Save</span>
                    </div>
                    <div class="col-md-1 padding_sm" style="float: right">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <span type="button" class="btn btn-warning pull-right" onclick="backToList()" >
                            <i class="fa fa-times" ></i>  Cancel</span>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
            <input type="hidden" name="head_id" value="<?= $head_details[0]->id ?>" id="head_id">
            {!! Form::token() !!} {!! Form::close() !!}
        </div>
    </div>
</div>
<div id="search_bill_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 1200px; width: 40%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" >Bill search <span id="modal_itemcodehr"> </span></h4>
            </div>
            <div class="modal-body" style="padding: 7px 15px;" id="itemts_batchwisediv">
                <div class="row" style="padding-bottom: 100px;">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="mate-input-box">
                                <label for="" class="header_label">Voucher No</label>
                                <input type="text" value="" name="voucher_no"  class="form-control" id="voucher_no" onkeyup="searchVoucherNo(this.id, event)" style="height:33px !important;">
                                <div class="ajaxSearchBox" id="search_voucher_no" style="">
                                </div>
                            </div>
                        </div>
                        <?php $bills = \DB::table('ledger_booking_bill_details')->pluck('bill_no', 'bill_no'); ?>
                        <div class="col-md-8">
                            <div class="mate-input-box">
                                <label for="" class="header_label">Bill No</label>
                                {!! Form::select('bills',$bills,null,
                                ['class' => 'form-control','title' => 'bills',
                                'id' => 'bills','style' => 'color:#555555;', 'multiple'=>'multiple']) !!}                        
                            </div>
                        </div>
                        <input type="hidden" id="search_bill_modal_id" value="">
                    </div>
                </div>
                <div class="modal-footer" style="padding: 2px 7px;">
                    <span class="btn btn-warning"  data-dismiss="modal" style="padding: 0px 2px;"><i class="fa fa-times"> Cancel</i></span>
                    <span class="btn btn-success" id="go" style="padding: 0px 2px;"><i class="fa fa-thumbs-up"> OK</i></span>

                </div>
            </div>

        </div>
    </div>
</div>
<input type="hidden" id="ins_base_url" value="{{URL::to('/')}}">
<input type="hidden" id="cash_payment_mod_hidden" value="<?php echo htmlspecialchars($cash_payment_mod_json, ENT_COMPAT); ?>">
<input type="hidden" id="bank_hidden" value="<?php echo htmlspecialchars($bank_details_json, ENT_COMPAT); ?>">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/pay_receipt_ledger.js")}}"></script>
<script type="text/javascript">


</script>
@endsection
