@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        {!!Form::open(array('url' => $searchUrl, 'method' => 'get', 'id'=>'form'))!!}
                        

<!--                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Bill date from</label>
                            <div class="clearfix"></div>
                            <input type="text" name="from_date" autocomplete="off"
                                   value="@if(old('from_date')){{old('from_date')}}@else{{$searchFields['from_date']}}@endif"
                                   class="form-control datepicker" id="from_date"
                                   placeholder="From Date">
                            </div>
                        </div>-->

<!--                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Bill date to</label>
                            <div class="clearfix"></div>
                            <input type="text" name="to_date" autocomplete="off"
                                   value="@if(old('to_date')){{old('to_date')}}@else{{$searchFields['to_date']}}@endif"
                                   class="form-control datepicker" id="to_date"
                                   placeholder="To Date">
                            </div>
                        </div>-->
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Pay. dt. from</label>
                            <div class="clearfix"></div>
                            <input type="text" name="payment_from_date" autocomplete="off"
                                   value="@if(old('payment_from_date')){{old('payment_from_date')}}@else{{$searchFields['payment_from_date']}}@endif"
                                   class="form-control datepicker" id="payment_from_date"
                                   placeholder="Created from Date">
                            </div>
                        </div>

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Pay. dt. to</label>
                            <div class="clearfix"></div>
                            <input type="text" name="payment_to_date" autocomplete="off"
                                   value="@if(old('payment_to_date')){{old('payment_to_date')}}@else{{$searchFields['payment_to_date']}}@endif"
                                   class="form-control datepicker" id="payment_to_date"
                                   placeholder="Created to Date">
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Type</label>
                            <div class="clearfix"></div>
                            <?php $payment_receipt = array( '1' => 'Payment','2' => 'Receipt',) ?>
                                {!! Form::select('payment_receipt',$payment_receipt,$payment_receipt ?? '',['class' => 'form-control','placeholder' => 'Select','title' => 'Payment/Receipt','id' => 'payment_receipt','style' => 'color:#555555; padding:2px 12px;']) !!}
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Voucr. Type</label>
                            <div class="clearfix"></div>
                                {!! Form::select('vouchertype',array(""=> " Select") + $v_type->toArray(), $searchFields['vouchertype'] ?? '',['class' => 'form-control','title' => 'Voucher Type','id' => 'vouchertype','style' => 'color:#555555; padding:2px 12px;']) !!}
                            </div>
                        </div>
                         <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                            <label for="" class="header_label">Voucher No</label>
                            <div class="clearfix"></div>
                            <input type="text" name="voucherno" value="" class=" form-control">
                            </div>
                        </div>

                        

                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button class="btn btn-block light_purple_bg" onclick="reqSearch();" ><i class="fa fa-search"></i> Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block btn-info" onclick="addItem();" ><i class="fa fa-plus"></i> Add</span>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block btn-warning" onclick="clear_search();" ><i class="fa fa-times"></i> Clear</span>
                        </div>
                        {!! Form::token() !!} {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="height: 450px;">
                <div class="box-body clearfix" style="border-radius:6px;background-color: #fff !important">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table">
                            <thead>
                                <tr class="table_header_bg ">
                                    <th width="2%" >Type</th>
                                    <th width="2%" >Bank</th>
<!--                                    <th width="5%" >Paid Amount </th>-->
                                    <th width="5%">Payment Date </th>
                                    <th width="5%">Voucher Type</th>
                                    <th width="5%">Voucher No</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($ledger_list) && sizeof($ledger_list) > 0)
                                @foreach($ledger_list as $list)
                                <tr onclick="selectLedger({{$list->head_id}})" style="cursor:pointer">
                                    
                                    <td class="common_td_rules">{{ $list->type }}</td>
                                    <td class="common_td_rules">{{ $list->bank }}</td>
<!--                                    <td class="td_common_numeric_rules">{{ $list->paid_amount }}</td>-->
                                    <td class="common_td_rules">{{$list->created_at? date(\WebConf::getConfig('date_format_web'), strtotime($list->created_at)):''}}</td>
                                    <td class="common_td_rules">{{$list->voucher_type }}</td>
                                    <td class="common_td_rules">{{$list->voucher_no }}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 text-right">
                        <ul class="pagination purple_pagination" style="text-align:right !important;">
                            {!! $page_links !!}
                        </ul>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@stop
@section('javascript_extra')
 <script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
 <script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script type="text/javascript">
    
    function reqSearch() {
    var makeSearchUrl = '';
    $('#form').submit();
    }
    function selectLedger(id){
    window.location = "{!! route('extensionsvalley.master.edit_pay_receipt') !!}/" + id;
    }
    function addItem(){
        window.location = "{!! route('extensionsvalley.master.add_pay_receipt') !!}";
    }
  
   
   @if(Session::has('mes') && Session::has('id'))
    var type = "{{ Session::get('type', 'info') }}";
    var id = "{{ Session::get('id') }}";
    if(id !=''){
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('mes') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('mes') }}");
            break;

        case 'success':
            toastr.success("Added Successfully");
            break;

        case 'error':
            toastr.error("{{ Session::get('mes') }}");
            break;
           
    }
     window.location.reload();
    }
  @endif
    
</script>
@endsection
