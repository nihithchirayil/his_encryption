<div class="row">
     <input type="hidden" value="{{$total_rec ?? '0'}}" id="show_total_paid_bills">
    <div class="clearfix"></div>
    <div class="h10"></div>
    <div class="col-md-12">
        <!--                                            <div class="table-responsive theadscroll"  style="height: 165px;">-->
        <table class="table table_round_border styled-table" id="receipt_table1">
            <thead>
                <tr class="table_header_bg">
                    <th style="text-align:center"><input id="issue_search_box1" onkeyup="searchProducts1();" type="text" placeholder="Search.. " style="display: none;width: 90%;color:black" >
                                                            <span id="item_search_btn1" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                                                            <span id="item_search_btn_text1">Party</span></th>
                    <th style="text-align:center">Voucher Type</th>
                    <th style="text-align:center">Voucher No</th>
                    <th style="text-align:center">Date</th>
                    <th style="text-align:center">Amount</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($receivedData))
                @foreach($receivedData as $rec)
                <tr>
                    <td class="common_td_rules">{{ $rec->ledger_name }}</td>
                    <td class="common_td_rules">{{ !empty($rec->voucher_type) ? $rec->voucher_type:'' }}</td>
                    <td class="common_td_rules">{{ !empty($rec->voucher_no) ? $rec->voucher_no:'' }}</td>
                    <td class="common_td_rules">{{ !empty($rec->created_at) ?$rec->created_at :'' }}</td>
                    <td class="td_common_numeric_rules">{{ !empty($rec->amount) ?$rec->amount :'' }}</td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">No Records Found</td>
                </tr>
                @endif                        
            </tbody>
        </table>
        <!--                                            </div>-->
    </div>
    <div class="clearfix"></div>
    <div class="h10"></div>

    <div class="row" style="">
        <div style="padding:10px 30px;">
            <div class="col-md-12" style="text-align:right;">
                <nav id="rec_paging"> <?php echo $paginator_rec->render(); ?>
                </nav>
            </div>
        </div>
    </div>                                            
</div>
<script>
     $("#tab_2 #rec_paging .pagination a").click(function (e) {
        e.preventDefault();
 $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        var param = {from_receive: 1,received:1 ,page: $(this).attr('href').split('page=')[1]};
        $.ajax({
            url: "",
            data: param,
            dataType: 'html',
        }).done(function (data) {
            $('#tab_2').html(data);
            $.LoadingOverlay("hide");
        }).fail(function () {
            $.LoadingOverlay("hide");
            alert('Posts could not be loaded.');
        });
    });
    $("#item_search_btn1").click(function () {
$("#issue_search_box1").toggle();
    document.getElementById("issue_search_box1").focus();
    $("#item_search_btn_text1").toggle();
});
function searchProducts1() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box1");
    filter = input.value.toUpperCase();
    table = document.getElementById("receipt_table1");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];

        label = td.getElementsByTagName("label");

        if (td) {
            if (td.textContent.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
</script>