@extends('Dashboard::dashboard.dashboard')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
{{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
    /*   Change Color for Active Tabs     */
    .nav-pills>li.active>a,
    .nav-pills>li.active>a:focus,
    .nav-pills>li.active>a:hover,
    .nav-pills>li>a:hover,
    .nav>li>a:hover {
        background-color: #F47321 ;
        color: #FFFFFF;
    }
    .nav>li>a {
        padding: 3px 6px !important;
    }

    /*   Change Color for Inactive Tabs     */
    .nav-pills>li>a {
        background-color: #01987a;
        color: #FFFFFF;

    }
    /* toggle button */
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col" >
    <div class="row" style="text-align: right;  font-size: 12px; font-weight: bold;padding-right: 80px"> {{$title}}
        <div class="clearfix"></div></div>

    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div id="product_tab_container">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs sm_nav nav-pills">
                                <li class="active"><a data-toggle="tab" href="#tab_1" id="" onclick="LabelChange()"> Show pending bills<i id="" class=""></i></a></li>
                                <li><a data-toggle="tab" href="#tab_2" id="" onclick="receivedDetails()">Show received bills <i id="" class=""></i></a></li>
                                <li class="pull-right"><button onclick="viewConsolidate()" class="btn btn-block btn-primary" style="height: 26px;"><i class=" fa fa-thumbs-up"></i> Make Received </button></li>
                                 <li class="pull-right" style="padding:4px" id="pending_label"><b>Total Pending&nbsp;&nbsp;-&nbsp;&nbsp;<span style="color:red">{{$total ?? '0'}}</span></b></li>
                                <li class="pull-right" style="padding:4px;display: none" id="payment_label"><b>Total Received&nbsp;&nbsp;-&nbsp;&nbsp;<span style="color:red" id="show_total_paid_bills_span">{{$total_rec ?? '0'}}</span></b></li>

                            </ul>
                            <div class="tab-content" style="min-height: 535px;">
                                <div id="tab_1" class="tab-pane active">
                                    <div class="row">
                                        <div class="clearfix"></div>
                                        <div class="h10"></div>
                                        <div class="col-md-12">
                                            <!--                                            <div class="table-responsive theadscroll"  style="height: 165px;">-->
                                            <table class="table table_round_border styled-table" id="receipt_table">
                                                <thead>
                                                    <tr class="table_header_bg">
                                                        <th style=";width:3%"><input class="checkAll" type="checkbox"></th>
                                                        <th style="text-align:center"><input id="issue_search_box" onkeyup="searchProducts();" type="text" placeholder="Search.. " style="display: none;width: 90%;color:black" >
                                                            <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                                                            <span id="item_search_btn_text">Party</span></th>
                                                        <th style="text-align:center;width:10%">Voucher Type</th>
                                                        <th style="text-align:center;width:10%">Voucher No</th>
<!--                                                        <th style="text-align:center">Reference Detail</th>-->
                                                        <th style="text-align:center;width:10%">Date</th>
                                                        <th style="text-align:center;width:8%">Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(isset($pendingData))
                                                    @foreach($pendingData as $st)
                                                    <tr>
                                                        <td><input name="detail_id[]" class="" type="checkbox" value="{{$st->detail_id}}" ></td>
                                                        <td class="common_td_rules">{{ $st->ledger_name }}</td>
                                                        <td class="common_td_rules">{{ !empty($st->voucher_type) ? $st->voucher_type:'' }}</td>
                                                        <td class="common_td_rules">{{ !empty($st->voucher_no) ? $st->voucher_no:'' }}</td>
<!--                                                        <td class="common_td_rules">{{ !empty($st->ref_detail) ?$st->ref_detail :'' }}</td>-->
                                                        <td class="common_td_rules">{{ !empty($st->created_at) ?$st->created_at :'' }}</td>
                                                        <td class="td_common_numeric_rules">{{ !empty($st->amount) ?$st->amount :'' }}</td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <td colspan="5">No Records Found</td>
                                                    </tr>
                                                    @endif                        
                                                </tbody>
                                            </table>
                                            <!--                                            </div>-->
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="h10"></div>

                                        <div class="row" style="">
                                            <div style="padding:10px 30px;">
                                                <div class="col-md-12" style="text-align:right;">
                                                    <nav id="purchase_paging"> <?php echo $paginator->render(); ?>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>                                            
                                    </div>
                                </div>
                                <div id="tab_2" class="tab-pane">

                                </div>


                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div id="modal_pending_consolidate" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 1200px; width: 40%;">

            <!-- Modal content-->
            <div class="modal-content" style=" height: 500px">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Process Pending Bills</h4>
                </div>
                <div class="modal-body" id="append_pending">

                </div>

            </div>
        </div>
    </div>
    <input type="hidden" value="0" id="receive_ajax">
    <script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
    <script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
    <script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
    @stop
    @section('javascript_extra')
    <script type="text/javascript">
                                                            $(document).ready(function () {

                                                                setTimeout(function () {
                                                                    // $('#menu_toggle').trigger('click');
                                                                    $('.theadscroll').perfectScrollbar("update");
                                                                    $(".theadfix_wrapper").floatThead('reflow');
                                                                }, 300);


                                                                $('.month_picker').datetimepicker({
                                                                    format: 'MM'
                                                                });
                                                                $('.year_picker').datetimepicker({
                                                                    format: 'YYYY'
                                                                });

                                                                $('.datepicker').datetimepicker({
                                                                    format: 'DD-MMM-YYYY'
                                                                });
                                                                $('.date_time_picker').datetimepicker();
                                                                $('.theadscroll').perfectScrollbar({
                                                                    wheelPropagation: true,
                                                                    minScrollbarLength: 30
                                                                });
                                                            });
                                                            $("#tab_1 #purchase_paging .pagination a").click(function (e) {
                                                                e.preventDefault();
                                                                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                                                                var param = {from_pending: 1, page: $(this).attr('href').split('page=')[1]};
                                                                $.ajax({
                                                                    url: "",
                                                                    data: param,
                                                                    dataType: 'html',
                                                                }).done(function (data) {
                                                                    $('#tab_1').html(data);
                                                                    $.LoadingOverlay("hide");
                                                                }).fail(function () {
                                                                    $.LoadingOverlay("hide");
                                                                    alert('Posts could not be loaded.');
                                                                });
                                                            });

                                                            function viewConsolidate() {
                                                                var detailArray = [];
                                                                $('input[name="detail_id[]"]:checked').each(function () {
                                                                    detailArray.push(this.value);
                                                                });
                                                                if (detailArray.length == 0) {
                                                                    toastr.error('Please select atleast one entry !!');
                                                                    return false;
                                                                }
                                                                var param = {show_pending_consolidate: 1, detailArray: detailArray};
                                                                $.ajax({
                                                                    type: "GET",
                                                                    url: '',
                                                                    data: param,
                                                                    beforeSend: function () {
                                                                        $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                                                                    },
                                                                    success: function (html) {
                                                                        $.LoadingOverlay("hide");
                                                                        $("#append_pending").html(html);
                                                                    },
                                                                    complete: function () {
                                                                        $("#modal_pending_consolidate").modal('show');
                                                                    }
                                                                });
                                                            }
                                                            function showVoucherDiv(e) {
                                                                $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                                                                if ($(e).is(':checked')) {
                                                                    $("#voucherWise").show();
                                                                    $("#consolidate").hide();
                                                                } else {
                                                                    $("#voucherWise").hide();
                                                                    $("#consolidate").show();
                                                                }
                                                                $.LoadingOverlay("hide");
                                                            }
                                                            function SaveReceipt() {
                                                                var ledger_id = $("#ledger_id").val();
                                                                var bank_details = $("#bank_details").val();
                                                                if (bank_details == '') {
                                                                    toastr.error('Please select Bank !!');
                                                                    return false;
                                                                }
                                                                var v_type = $("#v_type").val();
                                                                var param = {ledger_id_array: ledger_id, save_receive: 1, bank_id: bank_details, v_type: v_type};
                                                                $.ajax({
                                                                    type: "GET",
                                                                    url: '',
                                                                    data: param,
                                                                    beforeSend: function () {
                                                                        $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                                                                    },
                                                                    success: function (data) {
                                                                        $.LoadingOverlay("hide");
                                                                        if (data == 1) {
                                                                            toastr.success('Added successfully');
                                                                        } else {
                                                                            toastr.error('Error occured !!');
                                                                        }
                                                                    },
                                                                    complete: function () {
                                                                        $("#modal_pending_consolidate").modal('hide');
                                                                        window.location.reload();
                                                                    }
                                                                });
                                                            }
                                                            function receivedDetails() {

                                                                var param = {received: 1};
                                                                if ($("#receive_ajax").val() == 0) {
                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: '',
                                                                        data: param,
                                                                        beforeSend: function () {
                                                                            $("#pending_label").hide();
                                                                            $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
                                                                        },
                                                                        success: function (html) {
                                                                            $.LoadingOverlay("hide");
                                                                            $("#tab_2").html(html);
                                                                        },
                                                                        complete: function () {
                                                                            $("#receive_ajax").val('1');
                                                                            var ttl = $("#show_total_paid_bills").val();
                                                                            $("#show_total_paid_bills_span").html(ttl);
                                                                            $("#payment_label").show();
                                                                        }
                                                                    });
                                                                }else{                                                                
                                                                 $("#pending_label").hide();                                                                
                                                                 $("#payment_label").show();
                                                            }
                                                            }
                                                            function LabelChange(){
                                                                 $("#payment_label").hide();
                                                                 $("#pending_label").show();
                                                            }
                                                            $(".checkAll").click(function () {
                                                                $('input[name="detail_id[]"]').not(this).prop('checked', this.checked);
                                                            });
                                                            $("#item_search_btn").click(function () {
    $("#issue_search_box").toggle();
    document.getElementById("issue_search_box").focus();
    $("#item_search_btn_text").toggle();
});
function searchProducts() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("issue_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("receipt_table");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];

        label = td.getElementsByTagName("label");

        if (td) {
            if (td.textContent.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
 
                                                            @include('Purchase::messagetemplate')
    </script>

    @endsection
