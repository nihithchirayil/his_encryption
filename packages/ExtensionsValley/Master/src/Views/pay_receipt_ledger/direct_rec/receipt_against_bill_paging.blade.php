<div class="row">
    <div class="clearfix"></div>
    <div class="h10"></div>
    <div class="col-md-12">
        <!--                                            <div class="table-responsive theadscroll"  style="height: 165px;">-->
        <table class="table table_round_border styled-table">
            <thead>
                <tr class="table_header_bg">
                    <th><input class="checkAll" type="checkbox" ></th>
                    <th style="text-align:center"><input id="issue_search_box" onkeyup="searchProducts();" type="text" placeholder="Search.. " style="display: none;width: 90%;color:black" >
                                                            <span id="item_search_btn" style=" float: right" class="btn btn-warning"><i class="fa fa-search"></i></span>
                                                            <span id="item_search_btn_text">Party</span></th>
                    <th style="text-align:center">Voucher Type</th>
                    <th style="text-align:center">Voucher No</th>
<!--                    <th style="text-align:center">Reference Detail</th>-->
                    <th style="text-align:center">Date</th>
                    <th style="text-align:center">Amount</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($pendingData))
                @foreach($pendingData as $st)
                <tr>
                    <td><input name="detail_id[]" class="" type="checkbox" value="{{$st->detail_id}}" ></td>
                    <td class="common_td_rules">{{ $st->ledger_name }}</td>
                    <td class="common_td_rules">{{ !empty($st->voucher_type) ? $st->voucher_type:'' }}</td>
                    <td class="common_td_rules">{{ !empty($st->voucher_no) ? $st->voucher_no:'' }}</td>
<!--                    <td class="common_td_rules">{{ !empty($st->ref_detail) ?$st->ref_detail :'' }}</td>-->
                    <td class="common_td_rules">{{ !empty($st->created_at) ?$st->created_at :'' }}</td>
                    <td class="td_common_numeric_rules">{{ !empty($st->amount) ?$st->amount :'' }}</td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">No Records Found</td>
                </tr>
                @endif                        
            </tbody>
        </table>
        <!--                                            </div>-->
    </div>
    <div class="clearfix"></div>
    <div class="h10"></div>

    <div class="row" style="">
        <div style="padding:10px 30px;">
            <div class="col-md-12" style="text-align:right;">
                <nav id="purchase_paging"> <?php echo $paginator->render(); ?>
                </nav>
            </div>
        </div>
    </div>                                            
</div>
<script>
     $("#tab_1 #purchase_paging .pagination a").click(function (e) {
        e.preventDefault();
        $.LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7'});
        var param = {from_pending: 1, page: $(this).attr('href').split('page=')[1]};
        $.ajax({
            url: "",
            data: param,
            dataType: 'html',
        }).done(function (data) {
            $('#tab_1').html(data);
            $.LoadingOverlay("hide");
        }).fail(function () {
            $.LoadingOverlay("hide");
            alert('Posts could not be loaded.');
        });
    });
    $(".checkAll").click(function () {
     $('input[name="detail_id[]"]').not(this).prop('checked', this.checked);
    });
</script>