<div class="modal-body" id="" style="min-height: 400px">
<div class="col-md-12 padding_sm">
       <div class="col-md-3 padding_sm">
        <label class="text-blue">Copy From Date</label>
            <input onblur="getPreviousYellowSheetSevices(2)" type="text" value="<?=$from_date?>" autocomplete="off" id="copyfrom_previous"  class="form-control" placeholder="Copy From Date">
        </div>
       <div class="col-md-3 padding_sm">
            <label class="text-blue">Copy To Date</label>
            <input readonly type="text" value="<?=$to_date?>" autocomplete="off" class="form-control" placeholder="Copy To Date">
        </div>
</div>
    <div class="theadscroll" style="position: relative; height: 350px;">
        <table class="table theadfix_wrapper table-striped table_sm table-condensed styled-table"
        style="border: 1px solid #CCC;">
            <thead>
            <tr class="table_header_bg" style="cursor: pointer;">
                <th width="30%" style="text-align: left;">Procedure Name</th>
                <th width="30%" style="text-align: left;">Doctor</th>
                <th width="10%" style="text-align: left;">Time</th>
                <th width="10%">
                <div class="checkbox checkbox-warning inline no-margin">
                <input title="Check All" id="checkAllPatientProcedures" onclick="checkAllProcedures()" class="chk_dl procedure_check_all" value="" type="checkbox">
                <label class="text-blue" for="checkAllPatientProcedures"> Check All</label>
                </div>
                </th>
            </tr>
            </thead>
                <tbody id="previousYelloeSheetData">
                <?php
                        if(count($yellowsheet)!=0){
                        foreach($yellowsheet as $each){
                        $created_at=date('H:i',strtotime($each->service_datetime));
                        $service_datetime=date('M-d-Y',strtotime($each->service_datetime));
                        ?>
                        <tr id="billingyellowsheetrow<?=$each->id?>">
                        <td style="text-align: left;"><?=$each->service_desc?></td>
                        <td style="text-align: left;"><select class="form-control" name="previousSheetDoctor">
                        <option value=" ">Select Doctor</option>
                        <?php
                        foreach($doctor_data as $key=>$val){
                        $checked_staus='';
                        if($each->doctor_id==$key){
                        $checked_staus="selected=''";
                        }
                        ?>
                        <option <?=$checked_staus?> value="<?=$key?>"><?=$val?></option>
                        <?php
                        }
                        ?>
                        </select>

                        </td>
                        <td>
                            <input type="time" value="<?=$created_at?>" name="privousYellowSheetTimePicker" class="form-control timepickerclass_default timepickerclass">
                            <label for="yellowsheettime_picker<?=$each->id?>"></label>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success inline no-margin">
                                <input id="checkPreviousPatientProcedures<?=$each->id?>" class="chk_dl procedure_check" name="checkPreviousPatientProcedures" value="<?=$each->yellow_sheet_id?>" type="checkbox">
                                <label class="text-blue" for="checkPreviousPatientProcedures<?=$each->id?>"></label>
                            </div>
                        </td>
                        </tr>
                        <?php
                        }
                        }else{
                        ?>
                        <tr><td colspan="6" style="text-align: center;"> No Result Found</td></tr>
                        <?php
                        }
                        ?>

                </tbody>
        </table>
</div>
</div>
<div class="modal-footer">
    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger">Close <i class="fa fa-times"></i></button>
    <?php
        if(count($yellowsheet)!=0){
            ?>
                <button class="btn btn-primary" id="savePreviousServicesBtn" style="padding: 3px 8px;" onclick="addPreviousYellowSheet()" type="button"> Apply <i id="savePreviousServicesSpin" class="fa fa-save"></i></button>
        <?php
        }
    ?>
</div>
