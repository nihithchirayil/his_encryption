                       <div class="col-md-12 padding_sm text-center text-blue" style="padding-top: 10px;"><strong>Total
                               Records : <?= count($yellowsheet) ?></strong></div>
                       <div class="theadscroll" style="position: relative; height: 557px;">
                           <table class="table theadfix_wrapper table-striped table_sm table-condensed styled-table"
                               style="border: 1px solid #CCC;">
                               <thead>
                                   <tr class="table_header_bg" style="cursor: pointer;">
                                       <th width="30%" style="text-align: left;">Procedure Name</th>
                                       <th width="10%" style="text-align: left;">Price</th>
                                       <th width="20%" style="text-align: left;">Doctor</th>
                                       <th width="15%" style="text-align: left;">Time</th>
                                       <th width="10%"><i class="fa fa-repeat"></i></th>
                                       <th width="10%"><i class="fa fa-trash-o"></i></th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <?php
                           if(count($yellowsheet)!=0){
                            foreach($yellowsheet as $each){
                                $disabled_status = '';
                                if ($doctor_id == '0' || $billed_status != '0' || $each->is_includeindischarge=='1' ||  $each->paid_status!='0') {
                                    $disabled_status = 'disabled';
                                }
                                $created_at=date('H:i',strtotime($each->service_datetime));
                                $service_datetime=date('M-d-Y',strtotime($each->service_datetime));
                                $created_date = @$each->created_date ? $each->created_date : '';
                                $created_at_new = '';
                                if ($created_date) {
                                    $created_at_new = date('M-d-Y H:i:s',strtotime($created_date));
                                }
                                ?>
                                   <tr id="billingyellowsheetrow<?= $each->id ?>">
                                       <td style="text-align: left;"><?= $each->service_desc ?><br/><p style="font-size:11px;color: red;"><?= $created_at_new ?></p></td>
                                       <td style="text-align: left;">
                                    @if($each->is_price_editable == 't' && $each->paid_status =='0')
                                        @php $read_only_price = ''; @endphp
                                    @else
                                        @php $read_only_price = 'readonly'; @endphp
                                    @endif
                                    <input type="text" <?= $read_only_price ?> name="update_price_procedure[]" value="<?= $each->total_charge ?>" id="update_price_procedure_<?= $each->id ?>" onchange="changePriceChecked(this.value,'<?= $each->id ?>')">
                                    </td>
                                       <td style="text-align: left;">
                                           <select <?= $disabled_status ?>
                                               onchange="updateYellowSheetBillingServices(<?= $each->id ?>)"
                                               class="form-control" id="yellowsheetdoctor_select<?= $each->id ?>">
                                               <option value=" ">Select Doctor</option>
                                               <?php
                                        foreach($doctor_data as $key=>$val){
                                            $checked_staus='';
                                            if($each->doctor_id==$key){
                                                $checked_staus="selected=''";
                                            }
                                           ?>
                                               <option <?= $checked_staus ?> value="<?= $key ?>"><?= $val ?></option>
                                               <?php
                                        }
                                    ?>
                                           </select>

                                       </td>
                                       <td>
                                           <input onchange="updateYellowSheetBillingServices(<?= $each->id ?>)"
                                               type="time" value="<?= $created_at ?>" <?= $disabled_status ?>
                                               name="yellowsheettime_picker" id="yellowsheettime_picker<?= $each->id ?>"
                                               class="form-control timepickerclass_default timepickerclass">
                                           <label for="yellowsheettime_picker<?= $each->id ?>"></label>
                                       </td>
                                       <td><button class="btn btn-sm btn-info"
                                               id="repeatBillingServicesbtn<?= $each->id ?>"
                                               onclick="repeatYellowSheetBillingServices(<?= $each->id ?>)"
                                               type="button">Repeat <i id="repeatBillingServicesspin<?= $each->id ?>"
                                                   class="fa fa-repeat"></i></button></td>
                                       <td><button class="btn btn-sm btn-danger" <?= $disabled_status ?>
                                               id="removeBillingServicesbtn<?= $each->id ?>"
                                               onclick="removeYellowSheetBillingServices(<?= $each->id ?>,<?= $each->yellow_sheet_id ?>,<?= $each->procedure_log_id ?>)"
                                               type="button">Remove <i id="removeBillingServicesspin<?= $each->id ?>"
                                                   class="fa fa-trash-o"></i></button></td>
                                   </tr>
                                   <?php
                            } ?>
                            <tr class="table_header_bg" style="cursor: pointer;">
                                <td><b>Total</b></td>
                                <td width="10%"  style="text-align: left;" id="all_total_checked"></td>
                                <td colspan="5"></td>
                                <td></td>
                            </tr>
                       <?php }else{
                            ?>
                                   <tr>
                                       <td colspan="6" style="text-align: center;"> No Result Found</td>
                                   </tr>
                                   <?php
                        }
                            ?>

                               </tbody>
                           </table>
                       </div>
