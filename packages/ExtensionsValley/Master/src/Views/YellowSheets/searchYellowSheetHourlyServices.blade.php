<?php
if (count($yellowsheet) != 0) {
    foreach ($yellowsheet as $each) { ?>
<tr id="yellowsheetrow<?= $each->id ?>">
    <td style="text-align: left;"><?= $each->service_desc ?></td>
    <td id="service_rate_<?= $each->id ?>"><?= $each->price ?></td>
    <td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="hourly_service_value[]" class="form-control hourly-service-input" onkeyup="calculateHourlyServiceCharge(this)"     id="hourly_service_<?= $each->id ?>"></td>
    <td id="calculated_service_td_<?= $each->id ?>"></td>
    <td><button class="btn btn-success btn-hourly" onclick="saveHourlyService(this,'<?= $each->id ?>')">
        <i class="fa fa-save" id="btn_save_hourly_spin{{$each->id}}"></i>
    </button></td>
</tr>

<?php }
  } else {  ?>
<tr>
    <td colspan="2" style="text-align: center;"> No Result Found</td>
</tr>
<?php } ?>
