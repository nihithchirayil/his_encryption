
                    @php $i=1; @endphp
                    @if(count($procedureList) > 0)
                    @foreach ($procedureList as $list)
                    @php
                    $ledger_id = @$list->id ? $list->id : 0;
                    $procedure_name = @$list->name ? $list->name : '';
                    $status = @$list->yellow_sheet_id ? $list->yellow_sheet_id : 0;
                    $hourly_status = @$list->is_hourly ? $list->is_hourly : 0;
                    $checked = $hourly_checked = '';
                    if ($status > 0) {
                    $checked = 'checked';
                    }
                    if ($hourly_status == 1) {
                        $hourly_checked = 'checked';
                    }
                    @endphp
                    <tr style="cursor: pointer;" class="tr_mapping_cls tr_mapping_cls{{$i}}" attr_mapping_type={{$list->id}}
                        attr_count={{$i}} data-service-name='{{ $list->name }}' data-yellow-sheet-id='{{ $status }}'>
                        <td class="common_td_rules">{{$i}}</td>
                        <td class="mapping_value common_td_rules">{{ $list->name }}</td>
                        <td class="mapping_value common_td_rules">{{ $list->procedure_name }}</td>
                        <td class="status" style="text-align:center">
                            <input {{ $checked }} style="" type="checkbox" name="procedure_chk[]" class="procedure_chk commonchk_cls"
                                id="procedure_chk{{$i}}">
                        </td>
                        <td class="status" style="text-align:center">
                            <input {{ $hourly_checked }} style="" type="checkbox" name="is_hourly[]" class="is_hourly commonchk_cls"
                                id="is_hourly{{$i}}">
                        </td>


                    </tr>
                    @php $i++; @endphp
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5" class="tax_code">No Records found</td>
                    </tr>
                    @endif
