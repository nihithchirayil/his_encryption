                       <div class="theadscroll" style="position: relative; height: 557px;">
                           <table  class="table table_sm theadfix_wrapper table-striped table-condensed styled-table"
                           style="border: 1px solid #CCC;">
                               <thead>
                                   <tr class="table_header_bg" style="cursor: pointer;">
                                       <th width="30%" style="text-align: left;">Procedure Name</th>
                                       <th width="25%" style="text-align: left;">Doctor</th>
                                       <th width="10%" style="text-align: left;">Quantity</th>
                                       <th width="10%" style="text-align: left;">Price</th>
                                       <th width="10%"  style="text-align: left;">Total</th>
                                       <th width="15%"><i class="fa fa-trash-o"></i></th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <?php
                           if(count($yellowsheet)!=0){
                            foreach($yellowsheet as $each){
                                $disabled_status = '';
                                if ($doctor_id == '0' || $billed_status != '0' || $each->is_includeindischarge=='1' || $each->paid_status!='0') {
                                    $disabled_status = 'disabled';
                                }
                                $created_ats=date('H:i',strtotime($each->service_datetime));
                                $service_datetime=date('M-d-Y',strtotime($each->service_datetime));
                                ?>
                                   <tr id="billingyellowsheetrowhourly<?= $each->id ?>">
                                       <td style="text-align: left;"><?= $each->service_desc ?></td>
                                       <td style="text-align: left;">
                                        <select <?= $disabled_status ?>
                                            onchange="updateYellowSheetBillingServicesHourly(<?= $each->id ?>)"
                                            class="form-control" id="yellowsheetdoctorhourly_select<?= $each->id ?>">
                                            <option value=" ">Select Doctor</option>
                                            <?php
                                     foreach($doctor_data as $key=>$val){
                                         $checked_staus='';
                                         if($each->doctor_id==$key){
                                             $checked_staus="selected=''";
                                         }
                                        ?>
                                            <option <?= $checked_staus ?> value="<?= $key ?>"><?= $val ?></option>
                                            <?php
                                     }
                                 ?>
                                        </select>

                                    </td>
                                       <td style="text-align: left;">
                                        <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                        $disabled_status name="update_qnty[]" value="<?= $each->quantity ?>"
                                         onkeyup="changePrice('<?= $each->id ?>')" id="update_qty_<?= $each->id ?>">
                                         <input type="hidden" id="update_service_time_hourly_<?= $each->id ?>" value="<?= $created_ats ?>">
                                    </td>
                                    <td style="text-align: left;">
                                        @if($each->is_price_editable == 't'  && $each->paid_status =='0')
                                            @php $read_only = ''; @endphp
                                        @else
                                            @php $read_only = 'readonly'; @endphp
                                        @endif

                                        <input type="text" <?= $read_only ?> name="update_price[]" value="<?= $each->price ?>" id="hourly_price_<?= $each->id ?>" onkeyup="changePrice('<?= $each->id ?>')">
                                    </td>
                                       <td style="text-align: left;" id="ttl_charge_updated_<?= $each->id ?>"><?= $each->total_charge ?></td>
                                       <td>
                                        <button class="btn btn-sm btn-success" <?= $disabled_status ?>
                                            id="saveBillingServicesdetailbtn<?= $each->id ?>"
                                            onclick="updateHourlQnty('<?= $each->id ?>')"
                                            type="button"> <i id="saveBillingServicesdetailspin<?= $each->id ?>"
                                                class="fa fa-save"></i></button>

                                        <button class="btn btn-sm btn-danger" <?= $disabled_status ?>
                                               id="removeBillingServicesbtnhourly<?= $each->id ?>"
                                               onclick="removeYellowSheetBillingServicesHourly(<?= $each->id ?>,<?= $each->yellow_sheet_id ?>,<?= $each->procedure_log_id ?>)"
                                               type="button"><i id="removeBillingServicesspinhourly<?= $each->id ?>"
                                                   class="fa fa-trash-o"></i></button></td>
                                   </tr>
                                   <?php
                            } ?>
                            <tr class="table_header_bg" style="cursor: pointer;">
                                <td colspan="4"><b>Total</b></td>
                                <td width="10%"  style="text-align: left;" id="all_total_hourly"></td>
                                <td></td>
                            </tr>
                        <?php }else{
                            ?>
                                   <tr>
                                       <td colspan="6" style="text-align: center;"> No Result Found</td>
                                   </tr>
                                   <?php
                        }
                            ?>

                               </tbody>
                           </table>
                           <table  class="table table_sm theadfix_wrapper table-striped table-condensed styled-table"
                           style="border: 1px solid #CCC;">
                               <thead>

                               </thead>
                               <tbody></tbody>
                       </table>
                       </div>

