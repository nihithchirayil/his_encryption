@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<style>
    .procedure_panel {
        position: relative;
        width: 40%;
        margin-bottom: 10px;
        padding: 10px 17px;
        display: inline-block;
        background: #fff;
        border: 1px solid #01A881;
        -webkit-column-break-inside: avoid;
        -moz-column-break-inside: avoid;
        column-break-inside: avoid;
        opacity: 1;
        transition: all .2s ease;
    }

    .billing_panel {
        position: relative;
        width: 60%;
        margin-bottom: 10px;
        padding: 10px 17px;
        display: inline-block;
        background: #fff;
        border: 1px solid #01A881;
        -webkit-column-break-inside: avoid;
        -moz-column-break-inside: avoid;
        column-break-inside: avoid;
        opacity: 1;
        transition: all .2s ease;
    }

    .doctor_panel {
        position: relative;
        width: 50%;
        margin-bottom: 10px;
        padding: 10px 17px;
        display: inline-block;
        background: #fff;
        border: 1px solid #01A881;
        -webkit-column-break-inside: avoid;
        -moz-column-break-inside: avoid;
        column-break-inside: avoid;
        opacity: 1;
        transition: all .2s ease;
    }

    .doctorbilling_panel {
        position: relative;
        width: 50%;
        margin-bottom: 10px;
        padding: 10px 17px;
        display: inline-block;
        background: #fff;
        border: 1px solid #01A881;
        -webkit-column-break-inside: avoid;
        -moz-column-break-inside: avoid;
        column-break-inside: avoid;
        opacity: 1;
        transition: all .2s ease;
    }

    .nav-tabs-custom>.sm_nav.nav-tabs>li.active {
        border-top-color: #d81b60;
    }

    .sm_nav {
        background: #F5F5F5;
    }

    .sm_nav li a {
        padding: 4px 2px;
        background: #01A881 !important;
        color: #FFF !important;
    }

    .sm_nav li.active a {
        background: #FFF !important;
        color: #444 !important;
        border-bottom: 2px solid #fff;
    }

    .sm_nav li.active a:hover {
        background: #FFF !important;
        color: #444 !important;
        border-bottom: 2px solid #fff;
    }

    .sm_nav li.active a:focus {
        background: #FFF !important;
        color: #444 !important;
        border-bottom: 2px solid #fff;
    }
</style>
@endsection
@section('content-area')

<div class="modal fade" id="addPreviousServicesModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Add Previous Services</h4>
            </div>
            <div id="addPreviousServicesModelDiv">

            </div>
        </div>
    </div>
</div>

<div class="right_col" role="main">
    <div class="row" style="font-size: 12px; font-weight: bold;">
        <div class="col-md-4 padding_sm bg-blue-active">
            Patient Name:
            <?= @$patient_data[0]->patient_name ? strtoupper($patient_data[0]->patient_name) : 'NA' ?>
        </div>
        <div class="col-md-4 padding_sm bg-blue-active">
            UHID:
            <?= @$patient_data[0]->uhid ? strtoupper($patient_data[0]->uhid) : 'NA' ?>
        </div>
        <div class="col-md-4 padding_sm bg-blue-active">
            Doctor Name:
            <?= @$patient_data[0]->doctor_name ? strtoupper($patient_data[0]->doctor_name) : '' ?>
        </div>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="col-md-4 padding_sm">


                    <label> Patient Search</label> <input type="text"
                        value="<?= @$patient_data[0]->patient_name ? strtoupper($patient_data[0]->patient_name) : 'NA' ?>"
                        autocomplete="off" id="patientMasterSearch" id="procedureSearch" class="form-control"
                        placeholder="Patient Search" onkeyup="searchpatientMaster(this.id, event)">
                    <div class="ajaxSearchBox" id="fav_grp_div" style="top:50px !important"></div>
                    <input type="hidden"
                        value="<?= @$patient_data[0]->ip_visit_id ? strtoupper($patient_data[0]->ip_visit_id) : 0 ?>"
                        id="patientMasterID">
                </div>

                <div class="col-md-3 padding_sm">
                    <label for="">Service Date</label>
                    <input onblur="loadTabData()" type="text" value="<?= $current_date ?>" autocomplete="off"
                        id="servieDate" class="form-control datepicker" placeholder="Service Date">

                </div>
            </div>
        </div>
    </div>


    <input type="hidden" id="patient_id"
        value="<?= @$patient_data[0]->patient_id ? strtoupper($patient_data[0]->patient_id) : 0 ?>">
    <input type="hidden" id="visit_id"
        value="<?= @$patient_data[0]->ip_visit_id ? strtoupper($patient_data[0]->ip_visit_id) : 0 ?>">
    <input type="hidden" id="doctor_id"
        value="<?= @$patient_data[0]->consulting_doctor ? strtoupper($patient_data[0]->consulting_doctor) : 0 ?>">
    <input type="hidden" id="patient_admission_date"
        value="<?= @$patient_data[0]->admission_date ? strtoupper($patient_data[0]->admission_date) : date('Y-m-d') ?>">
    <input type="hidden" id="patient_discharge_date"
        value="<?= @$patient_data[0]->discharge_datetime ? strtoupper($patient_data[0]->discharge_datetime) : date('Y-m-d') ?>">

    <div class="row">
        <ul class="nav nav-tabs sm_nav">
            <li onclick="getYelloewSheetData()" class="active"><a data-toggle="tab" href="#yellowSheetBillingDetailsli"
                    style="border-radius: 0px;" aria-expanded="false">Nursing Procedures List</a></li>
            <li onclick="getDoctorSheetData()"><a data-toggle="tab" href="#doctorlistBillingDetailsli"
                    style="border-radius: 0px;" aria-expanded="true">Doctor Encounter List</a></li>
            <li onclick="getYellowSheetReport()"><a data-toggle="tab" href="#getYellowSheetReportli"
                    style="border-radius: 0px;" aria-expanded="true">Report</a></li>
            <li onclick="getYellowSheetHourlyService()"><a data-toggle="tab" href="#getYellowSheetHourlyServiceli"
                    style="border-radius: 0px;" aria-expanded="true">Hourly Service</a></li>
            <li class="pull-right" style="margin-right:20px;margin-top:4px"><button id="updateBillYellowSheetfnBtn"
                    onclick="updateBillYellowSheetfn()" type="button" class="btn btn-primary"><i
                        id="updateBillYellowSheetfnSpin" class="fa fa-save"></i>
                    Save Sheet</button></li>
        </ul>
        <div class="tab-content">
            <div id="yellowSheetBillingDetailsli" class="tab-pane active in">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="row padding_sm">
                            <div class="col-md-12 padding_sm">
                                <div class="col-md-4 padding_sm procedure_panel padding_sm">
                                    <div class="col-md-12 padding_sm">
                                        <div class="col-md-4 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Procedure Type</label>
                                                {!! Form::select('procedure_type',array("-1"=> "All") +
                                                $procedureType->toArray(), $default_procedure_type, ['class' =>
                                                'select2','id' => 'procedure_type_id', 'style' => ' color:#555555;
                                                padding:4px 12px;','onchange' =>'getYellowSheetServices(1)']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-5 padding_sm">
                                            <label> Search Procedure Name</label>
                                            <input type="text" value="" autocomplete="off" id="procedureSearch"
                                                onkeyup="searchProcudureList()" class="form-control"
                                                placeholder="Search Procedure Name">
                                        </div>
                                        <div class="col-md-3 padding_sm">
                                            <div style="padding-top: 23px;">
                                                <button class="btn btn-sm btn-primary btn-block"
                                                    id="addPreviousServicesBtn"
                                                    onclick="getPreviousYellowSheetSevices(1)" type="button">Previous
                                                    Services <i id="addPreviousServicesSpin"
                                                        class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="theadscroll always-visible" style="position: relative; height: 525px;">
                                        <table
                                            class="table table_sm theadfix_wrapper table-striped table-condensed styled-table"
                                            style="border: 1px solid #CCC;">
                                            <thead>
                                                <tr class="table_header_bg" style="cursor: pointer;">
                                                    <th width="65%" style="text-align: left;">Procedure Name</th>
                                                    <th width="20%" style="text-align: left;">Price</th>
                                                    <th width="15%"><i class="fa fa-plus"></i></th>
                                                </tr>
                                            </thead>
                                            <tbody id="yellowSheetServicesDiv">
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="col-md-8 padding_sm billing_panel padding_sm"
                                    id="billingyellowSheetServicesDiv"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div id="doctorlistBillingDetailsli" class="tab-pane">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="row padding_sm">
                            <div class="col-md-12 padding_sm">
                                <div class="col-md-6 padding_sm doctor_panel padding_sm">
                                    <div class="col-md-12 padding_sm">
                                        <label> Search Doctor Name</label> <input type="text" value=""
                                            autocomplete="off" id="doctorSearchName" class="form-control"
                                            placeholder="Search Doctor Name">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="h10"></div>

                                    <div class="theadscroll always-visible" style="position: relative;height: 500px;">
                                        <table
                                            class="table table_sm theadfix_wrapper table-striped table-condensed styled-table"
                                            style="border: 1px solid #CCC;">
                                            <thead>
                                                <tr class="table_header_bg" style="cursor: pointer;">
                                                    <th width="50%" style="text-align: left;">Doctor Name</th>
                                                    <th width="10%" style="text-align: left;">Morning</th>
                                                    <th width="10%" style="text-align: left;">Noon</th>
                                                    <th width="10%" style="text-align: left;">Evening</th>
                                                    <th width="10%" style="text-align: left;">Night</th>
                                                    <th width="10%" style="text-align: left;">Emergency</th>
                                                </tr>
                                            </thead>
                                            <tbody id="doctormasterServicesDiv">
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="col-md-6 padding_sm doctorbilling_panel padding_sm">
                                    <div class="theadscroll always-visible" style="position: relative;height: 550px;">
                                        <table
                                            class="table table_sm theadfix_wrapper table-striped table-condensed styled-table"
                                            style="border: 1px solid #CCC;">
                                            <thead>
                                                <tr class="table_header_bg" style="cursor: pointer;">
                                                    <th width="50%" style="text-align: left;">Doctor Name</th>
                                                    <th width="10%" style="text-align: left;">Morning</th>
                                                    <th width="10%" style="text-align: left;">Noon</th>
                                                    <th width="10%" style="text-align: left;">Evening</th>
                                                    <th width="10%" style="text-align: left;">Night</th>
                                                    <th width="10%" style="text-align: left;">Emergency</th>
                                                </tr>
                                            </thead>
                                            <tbody id="billingdoctormasterServicesDiv">
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div id="getYellowSheetReportli" class="tab-pane">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="row padding_sm">
                            <div class="col-md-12 padding_sm" id="yellowSheetServicesReportDiv">

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div id="getYellowSheetHourlyServiceli" class="tab-pane">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="row padding_sm">
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    {!! Form::select('procedure_type',array("-1"=> "All") + $procedureType->toArray(),
                                    $default_procedure_type, ['class' => 'select2','id' => 'procedure_type_id', 'style'
                                    => ' color:#555555; padding:4px 12px;','onchange' =>'selectProcedureType(this)'])
                                    !!}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-5 padding_sm" id="yellowSheetHourlyServiceDiv" style="margin-top:-25px">
                                <div class="theadscroll always-visible" style="position: relative;height: 550px;">
                                    <table
                                        class="table table_sm theadfix_wrapper table-striped table-condensed styled-table"
                                        style="border: 1px solid #CCC;">
                                        <thead>
                                            <tr class="table_header_bg" style="cursor: pointer;">
                                                <th width="50%" style="text-align: left;">Service</th>
                                                <th width="10%" style="text-align: left;">Rate</th>
                                                <th width="10%" style="text-align: left;"> Value </th>
                                                <th width="10%" style="text-align: left;"> Total</th>
                                                <th width="10%" style="text-align: center;">
                                                    <i class="fa fa-save"></i>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="yellowSheetHourlyServicetbody">
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="col-md-7 padding_sm" id="yellow_sheet_hourly_list_div"
                                style="margin-top:-25px;">

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/mapYellowSheetPatients.js') }}"></script>
@endsection
