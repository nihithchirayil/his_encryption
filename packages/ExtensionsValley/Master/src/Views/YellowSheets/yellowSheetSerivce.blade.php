@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <style>
    </style>
@endsection
@section('content-area')
    @php
    @endphp
    <div class="right_col">
        <div class="row pull-right" style="font-size: 12px; font-weight: bold;"> {{ $title }}</div>
        <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
        <input type="hidden" value='<?= $route_data ?>' id="route_value">
        <div class="row codfox_container">
            <div class="col-md-12 paddimg_sm">
                <div class="col-md-4 paddimg_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 65px;">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Procedure Search</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="" autocomplete="off" id="searchaddedProcedure"
                                        class="form-control" placeholder="Search Procedure Name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 paddimg_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 65px;">
                            <div class="col-md-8 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Procedure Add</label>
                                    <div class="clearfix"></div>
                                    <input type="text" value="" autocomplete="off" id="serviceMasterSearch"
                                        class="form-control hidden_search" placeholder="Name">
                                    <div id="serviceMasterSearch_div" class="ajaxSearchBox"
                                    style="width: 100%;position:absolute"></div>
                                    <input type="hidden" value="" id="serviceMasterID">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="addyellowsheetbtn" type="button" onclick="saveServiceMaster()"
                                    class="btn btn-block light_purple_bg"><i id="addyellowsheetspin"
                                        class="fa fa-plus"></i> Add </button>

                            </div>
                            <div class="col-md-2 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" onclick="clearProceudureSearch()" class="btn btn-block btn-warning"><i
                                        class="fa fa-times"></i> Clear </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="h10"></div>
            <div class="col-md-12 paddimg_sm" id="searchProcedureSearchDiv">

            </div>
        </div>
    </div>

@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/yellowSheetService.js') }}"></script>
@endsection
