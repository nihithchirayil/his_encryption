@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')

<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">

<div class="right_col">
    <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"></div>
    <div class="row codfox_container">
        <div class="col-md-12 no-padding">
            <div class="box no-border">
                <div class="box-body clearfix">
                    {!! Form::token() !!}
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            {!! Form::select('procedure_type',array("0"=> " Choose Procedure Type") + $procedureType->toArray(), '', ['class' => 'select2','id' => 'procedure_type_id', 'style' => ' color:#555555; padding:4px 12px;']) !!}
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <button class="btn btn-block btn-info" id="btn_save_mapping_data" data-toggle="modal" data-target="#procedure_type_modal"><i
                                class="fa fa-plus"></i></button>
                    </div>
                    <div class="col-md-1 padding_sm">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <button class="btn btn-block btn-warning" id="btn_clear_department_data"
                            onclick="clear_department_data();"><i class="fa fa-times"></i>
                            Clear</button>
                    </div>
                            <div class="pull-right" style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 65px;width:32%">
                                <div class="col-md-12 padding_sm">
                                <div class="col-md-5 padding_sm">
                                    <div class="mate-input-box">
                                        {!! Form::select('procedure_type',array("0"=> " Search procedure Type") + $procedureType->toArray(), '', ['class' => 'select2','id' => 'search_procedure_type_id', 'style' => ' color:#555555; padding:4px 12px;','onchange' => 'searchYellowSheetFilter(this)']) !!}
                                    </div>
                                </div>
                                <div class="col-md-5 padding_sm">
                                    Is Nursing Procedure
                                    <input style="" type="checkbox" name="search_by_active" class="search_by_active">
                                </div>
                                <div class="col-md-2 padding_sm">
                                    Hourly
                                    <input style="" type="checkbox" name="search_by_hourly" class="search_by_hourly">
                                </div>
                            </div>
                            </div>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>
        <div class="box no-border no-margin">
            <div class="box-body clearfix" id="procedure_mapping_listDiv">
                <div class="theadscroll" style="position: relative; height: 65vh;" >
                    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                    style="border: 1px solid #CCC;" id="procedure_tbl_id">
                    <thead>
                        <tr class="table_header_bg">
                            <th width='1%'>Sl.No</th>
                            <th width='10%'>
                                <input id="procedure_search_box" onkeyup="searchProcedure();" type="text"
                                autocomplete="off" placeholder="Procedure Search.. "
                                style="color: #000;display: none;width: 90%;">
                            <span id="procedure_search_btn" style=" float: right" class="btn btn-warning"><i
                                    class="fa fa-search"></i></span>
                            <span id="procedure_search_btn_text">Procedure Name</span>

                            </th>
                            <th width='5%'>Procedure Type</th>
                            <th width='5%'>Is Nursing Procedure</th>
                            <th width='5%'>Is hourly</th>
                        </tr>
                    </thead>
                    <tbody id=procedure_type_tbody>
                        @php $i=1; @endphp
                        @if(count($procedureList) > 0)
                        @foreach ($procedureList as $list)
                        @php
                        $ledger_id = @$list->id ? $list->id : 0;
                        $procedure_name = @$list->name ? $list->name : '';
                        $status = @$list->yellow_sheet_id ? $list->yellow_sheet_id : 0;
                        $hourly_status = @$list->is_hourly ? $list->is_hourly : 0;
                        $checked = $hourly_checked = '';
                        if ($status > 0) {
                        $checked = 'checked';
                        }
                        if ($hourly_status == 1) {
                            $hourly_checked = 'checked';
                        }
                        @endphp
                        <tr style="cursor: pointer;" class="tr_mapping_cls tr_mapping_cls{{$i}}" attr_mapping_type={{$list->id}}
                            attr_count={{$i}} data-service-name='{{ $list->name }}' data-yellow-sheet-id='{{ $status }}'>
                            <td class="common_td_rules">{{$i}}</td>
                            <td class="mapping_value common_td_rules">{{ $list->name }}</td>
                            <td class="mapping_value common_td_rules">{{ $list->procedure_name }}</td>
                            <td class="status" style="text-align:center">
                                <input {{ $checked }} style="" type="checkbox" name="procedure_chk[]" class="procedure_chk commonchk_cls"
                                    id="procedure_chk{{$i}}">
                            </td>
                            <td class="status" style="text-align:center">
                                <input {{ $hourly_checked }} style="" type="checkbox" name="is_hourly[]" class="is_hourly commonchk_cls"
                                    id="is_hourly{{$i}}">
                            </td>


                        </tr>
                        @php $i++; @endphp
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="tax_code">No Records found</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="procedure_type_modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" style="max-width: 1300px;width: 40%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
            <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <span class="modal-title" id="audit_log_header"></span>
            </div>
            <div class="modal-body" style="min-height:100px;height:10px;overflow: auto" >
                <div class="col-md-8 padding_sm">
                    <div class="mate-input-box">
                        <label for="">Procedure Type</label>
                        <div class="clearfix"></div>
                        <input type="text" value="" name="new_procedure_type" autocomplete="off" id="new_procedure_type" class="form-control ">
                    </div>
                </div>
                <div class="col-md-2 padding_sm">
                    <label for="">&nbsp;</label>
                    <div class="clearfix"></div>
                    <button class="btn btn-block light_purple_bg" id="save_new_procedure"
                        onclick="saveNewProcedureType();"><i class="fa fa-save" id="new_procedure_type_save_spin"></i>
                        Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{asset("packages/extensionsvalley/accounts/default/javascript/audit_log.js")}}"></script>

<script type="text/javascript">
$(document).ready(function() {
    var $table = $('table.theadfix_wrapper');
    token = $('#hidden_filetoken').val();
    base_url = $('#base_url').val();

    $table.floatThead({
        scrollContainer: function($table) {
            return $table.closest('.theadscroll');
        }
    });

    $('.theadscroll').perfectScrollbar({
        wheelPropagation: true,
        minScrollbarLength: 30
    });


    $('.fixed_header').floatThead({
        position: 'absolute',
        scrollContainer: true
    });
});
$(document).on('click', '.procedure_chk', function () {
    var status = ($(this).prop('checked')) ? 1 : 0;
    var attr_mapping_type_id = $(this).closest("tr").attr('attr_mapping_type');
    var attr_mapping_type_name = $(this).closest("tr").data('service-name');
    var attr_yellow_sheet_id = $(this).closest("tr").data('yellow-sheet-id');
    var procedure_type_id = $("#procedure_type_id").val();
    if(procedure_type_id ==0 && status == 1){
        toastr.error("Please select procedure type");
        return false;
    }
    var url = $('#base_url').val();
    var send_url = url + "/master/saveYellowSheetMaster";
    $.ajax({
        type: "GET",
        url: send_url,
        data: "service_id=" + attr_mapping_type_id+'&status='+status+'&service_name='+attr_mapping_type_name+'&procedure_type_id='+procedure_type_id+'&yellow_sheet_id='+attr_yellow_sheet_id,
        beforeSend: function () {
            $('#procedure_mapping_listDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            if(data==1){
                toastr.success("successfully updated");
            }
            if(data==2){
                toastr.success("successfully deleted");
            }
        },
        complete: function () {
            $('#procedure_mapping_listDiv').LoadingOverlay("hide");
        }
    });
});
$(document).on('click', '.is_hourly', function () {
    var status = ($(this).prop('checked')) ? 1 : 0;
    var attr_yellow_sheet_id = $(this).closest("tr").data('yellow-sheet-id');
    var url = $('#base_url').val();
    var is_active = ($(this).closest('tr').find('input[name="procedure_chk[]"]').prop('checked')) ? 1 : 0;
    if(is_active==0){
        toastr.error("Please make it as active");
        return false;
    }
    var send_url = url + "/master/saveYellowSheetMasterHourly";
    $.ajax({
        type: "GET",
        url: send_url,
        data: 'status='+status+'&yellow_sheet_id='+attr_yellow_sheet_id,
        beforeSend: function () {
            $('#procedure_mapping_listDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            if(data==1){
                toastr.success("successfully updated");
               }
            if(data==2){
                toastr.success("successfully deleted");
            }
        },
        complete: function () {
            $('#procedure_mapping_listDiv').LoadingOverlay("hide");
        }
    });
});
function saveNewProcedureType(){
    var new_procedure_type = $("#new_procedure_type").val();
    if(new_procedure_type==''){
        toastr.error("Please select procedure type");
        return false;
    }
    var url = $('#base_url').val();
    var send_url = url + "/master/saveYellowSheetProcedureType";
    $.ajax({
        type: "GET",
        url: send_url,
        data: "new_procedure_type=" + new_procedure_type,
        beforeSend: function () {
            $("#save_new_procedure").prop('disabled',true);
        },
        success: function (data) {
            if(data > 1){
                toastr.success("successfully updated");
                $('#procedure_type_id').append($('<option>', {
                    value: data,
                    text: new_procedure_type,
                    selected: true
                }));
                $("#procedure_type_modal").modal('hide');
                $("#new_procedure_type").val('');
            }
            $("#save_new_procedure").prop('disabled',false);

        },
        complete: function () { }
    });
}
function searchYellowSheetFilter(e){
    var procedure_id = $(e).val();
    var search_by_hourly = ($('.search_by_hourly').prop('checked')) ? 1 : 0;
    var search_by_active = ($('.search_by_active').prop('checked')) ? 1 : 0;
    var url = $('#base_url').val();
    var send_url = url + "/master/searchByYellowSheetFilter";
    $.ajax({
        type: "GET",
        url: send_url,
        data: "is_hourly_status="+search_by_hourly+'&is_active_status='+search_by_active+'&procedure_id='+procedure_id,
                beforeSend: function () {
            $('#procedure_mapping_listDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            if(data){
                $('#procedure_type_tbody').html(data);
            }
            // $("#save_new_procedure").prop('disabled',false);
        },
        complete: function () { $('#procedure_mapping_listDiv').LoadingOverlay("hide");  }
    });
}
$(document).on('click', '.search_by_active', function () {
    var status = ($(this).prop('checked')) ? 1 : 0;
    var search_by_hourly = ($('.search_by_hourly').prop('checked')) ? 1 : 0;
    var procedure_id = $("#search_procedure_type_id").val();
    var url = $('#base_url').val();
    var send_url = url + "/master/searchByYellowSheetFilter";
    $.ajax({
        type: "GET",
        url: send_url,
        data: "is_hourly_status="+search_by_hourly+'&is_active_status='+status+'&procedure_id='+procedure_id,
        beforeSend: function () {
            $('#procedure_mapping_listDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            if(data){
                $('#procedure_type_tbody').html(data);
            }

        },
        complete: function () {  $('#procedure_mapping_listDiv').LoadingOverlay("hide"); }
    });
});
$(document).on('click', '.search_by_hourly', function () {
    var status = ($(this).prop('checked')) ? 1 : 0;
    var search_by_active = ($('.search_by_active').prop('checked')) ? 1 : 0;
    var procedure_id = $("#search_procedure_type_id").val();
    var url = $('#base_url').val();
    var send_url = url + "/master/searchByYellowSheetFilter";
    $.ajax({
        type: "GET",
        url: send_url,
        data: "is_hourly_status="+status+'&is_active_status='+search_by_active+'&procedure_id='+procedure_id,
        beforeSend: function () {
            $('#procedure_mapping_listDiv').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
        },
        success: function (data) {
            if(data){
                $('#procedure_type_tbody').html(data);
            }
            // $("#save_new_procedure").prop('disabled',false);
        },
        complete: function () {
            $('#procedure_mapping_listDiv').LoadingOverlay("hide");
         }
    });
});
function searchProcedure() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("procedure_search_box");
    filter = input.value.toUpperCase();
    table = document.getElementById("procedure_tbl_id");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
$("#procedure_search_btn").click(function () {
    $("#procedure_search_box").toggle();
    $("#procedure_search_btn_text").toggle();
    $('#procedure_search_box').focus();
});
@include('Purchase::messagetemplate')
</script>

@endsection
