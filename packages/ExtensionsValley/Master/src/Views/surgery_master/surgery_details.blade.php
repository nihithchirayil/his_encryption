<div class="col-md-3 padding_sm">
    <div class="box-footer" style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #0c9e61;height: 400px;">
        <div class="col-md-12 padding_sm">
            <div class="mate-input-box">
                <label for="">Service Name</label>
                <div class="clearfix"></div>
                <input class="form-control hidden_search" value="" autocomplete="off" type="text" id="service_item" />
                <div id="service_itemAjaxDiv" class="ajaxSearchBox"
                    style="width: 100%; position: absolute; margin-top: 21px;"></div>
                <input class="filters" value="" type="hidden" name="service_item_hidden" value=""
                    id="service_item_hidden" />
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm">
            <div class="mate-input-box">
                <label for="">Surgery Name</label>
                <div class="clearfix"></div>
                <input class="form-control" value="" autocomplete="off" id="service_item_surgery_name"
                    name="service_item_surgery_name" type="text" />
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm">
            <div class="mate-input-box">
                <label for="">Approximate Amount</label>
                <div class="clearfix"></div>
                <input class="form-control hidden_search" value="" autocomplete="off" type="text" id="detail_amount"
                    name="detail_amount"
                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
            </div>
        </div>
    </div>
</div>
<div class="col-md-9 padding_sm">
    <div class="box-footer" style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #0c9e61;height: 400px;">
        <table class='table table-condensed table_sm table-col-bordered theadfix_wrapper' style="font-size: 12px;">
            <thead>
                <tr class="common_table_header">
                    <th width="70%">Level Name</th>
                    <th width="20%">
                        Amount
                    </th>
                    <th width="10%"><i class="fa fa-edit"></i></th>
                </tr>
            </thead>
            <tbody id="surgery_levels_table">
                @foreach ($surgery_levels as $each)
                <tr>
                    <td class="common_td_rules">
                        {{ $each->level_name }}
                        <input type="hidden" name="surgery_levels" value="{{ $each->id }}">
                    </td>
                    <td>
                        <input class="form-control hidden_search" autocomplete="off" type="text"
                            id="detail_amount" name="detail_amount" value="{{ $each->surgeon_amount }}"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                    </td>
                    <td>
                        @php
                        $checked="";
                        @endphp
                        @if(intval($each->is_editable)==1)
                        @php
                        $checked="checked";
                        @endphp
                        @endif
                        <div class="checkbox checkbox-primary inline no-margin">
                            <input {{ $checked }} type="checkbox" id="edit_surgery_price{{ $each->id }}"
                                value="{{ $each->id }}" name="edit_surgery_price">
                            <label for="edit_surgery_price{{ $each->id }}"></label>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="col-md-12 padding_sm" style="margin-top: 28px">
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" style="padding: 5px 5px;margin-top: 15px;" onclick="saveSurgeryDeatils()" name="next"
            id="departmentBtn" class="btnNext btn-block btn btn-success pull-right">
            <i id="departmentSpin" class="fa fa-save"></i> Save & Next <i class="fa fa-forward"></i>
        </button>
    </div>
</div>
