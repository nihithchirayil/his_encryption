<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var from_date = $("#from_date").val();
            var to_date = $("#to_date").val();
            var surgery_master = $("#surgery_master").val();

            $.ajax({
                url: url,
                type: "POST",
                data: {
                    _token: token,
                    from_date: from_date,
                    to_date: to_date,
                    surgery_master: surgery_master
                },
                beforeSend: function() {
                    $('#searchSurgeryMasterDiv').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                    $('#searchSurgeryMasterBtn').attr('disabled', true);
                    $('#searchSurgeryMasterSpin').removeClass('fa fa-search');
                    $('#searchSurgeryMasterSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchSurgeryMasterDiv').html(data);
                },
                complete: function() {
                    $('#searchSurgeryMasterDiv').LoadingOverlay("hide");
                    $('#searchSurgeryMasterBtn').attr('disabled', false);
                    $('#searchSurgeryMasterSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchSurgeryMasterSpin').addClass('fa fa-search');
                },
            });
            return false;
        });

    });
</script>
<div class="theadscroll" style="position: relative; height: 550px;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th class="common_td_rules" width="40%">Surgery Name</th>
                <th class="common_td_rules" width="20%">Surgery Code</th>
                <th class="common_td_rules" width="20%">Approximate Amount</th>
                <th class="common_td_rules" width="15%">Created At</th>
                <th width="5%"><i class="fa fa-edit"></i></th>
            </tr>
        </thead>
        <tbody>
            @if (count($request_list) > 0)
                @foreach ($request_list as $data)
                    <tr>
                        <td class="common_td_rules">{{ $data->name }}</td>
                        <td class="common_td_rules">{{ $data->service_code }}</td>
                        <td class="common_td_rules">{{ $data->approximate_amount }}</td>
                        <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->created_at)) }}</td>
                        <td>
                            <button type="button" class="btn btn-sm btn-warning" id="schedule_surgery"
                                onclick="addWindowLoad('getSurgeryMaster','{{ $data->head_id }}');" title="Schedule">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </button>
                        </td>

                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5" class="location_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important;">
        {!! $page_links !!}
    </ul>
</div>
