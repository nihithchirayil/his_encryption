<div class="col-md-12 padding_sm" style="margin-top: 10px;">
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label for="">Medicine Search</label>
            <div class="clearfix"></div>
            <input class="form-control" value="" autocomplete="off" type="text" id="medicine_serach">
            <div id="medicine_serachAjaxDiv" class="ajaxSearchBox"
                style="width: 100%; position: absolute; margin-top: 21px;">
                <button type="button" class="btn btn-dark close-btn">X</button>
            </div>
            <input class="filters" value="" type="hidden" value="" id="medicine_serach_hidden">
        </div>
    </div>
    <div class="col-md-2 padding_sm" style="margin-top: 10px">
        <div class="radio radio-success inline no-margin">
            <input type="radio" checked id="medicine_both_name" name="medicine_name_search" value="both">
            <label for="medicine_both_name">Both</label>
        </div>
    </div>
    <div class="col-md-2 padding_sm" style="margin-top: 10px">
        <div class="radio radio-success inline no-margin">
            <input type="radio" id="medicine_brand_name" name="medicine_name_search" value="brand">
            <label for="medicine_brand_name">Brand Name</label>
        </div>
    </div>
    <div class="col-md-2 padding_sm" style="margin-top: 10px">
        <div class="radio radio-success inline no-margin">
            <input type="radio" id="medicine_generic_name" name="medicine_name_search" value="generic">
            <label for="medicine_generic_name">Generic Name</label>
        </div>
    </div>

</div>
<div class="col-md-12 padding_sm theadscroll" style="height: 370px;" id="medicine_serach">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
        style="font-size: 12px;">
        <thead>
            <tr class="common_table_header">
                <th class="common_td_rules" width="5%">
                    Sl.No.
                </th>
                <th class="common_td_rules" width="30%">
                    Medicine Name
                </th>
                <th class="common_td_rules" width="30%">
                    Generic Name
                </th>
                <th class="common_td_rules" width="10%">
                    Qty
                </th>
                <th class="common_td_rules" width="10%">
                    Price
                </th>
                <th class="common_td_rules" width="10%">
                    Total
                </th>
                <th width="5%"> <i class="fa fa-trash"></i> </th>
            </tr>
        </thead>
        <tbody id="medicinelist">
        </tbody>

    </table>
</div>
<div class="col-md-12 padding_sm">
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" style="padding: 5px 5px;margin-top: 15px;" onclick="saveSurgeryMedicine('Medicine')" name="next"
            id="saveSurgeryMedicineBtn" class="btnNext btn-block btn btn-success pull-right">
            <i id="saveSurgeryMedicineSpin" class="fa fa-save"></i> Save & Next <i class="fa fa-forward"></i></button>
    </div>
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" onclick="getPreviousTab('medicine')" style="padding: 5px 5px;margin-top: 15px;"
            id="personalPreviousBtn" name="Previous" class="btnPrevious btn-block btn btn-warning "><i class="fa fa-backward"></i>
            Previous</button>
    </div>
    <div class="col-md-3 padding_sm pull-right" style="margin-top:20px">
        <strong for="">Approximate Total Charge : <span class="td_common_numeric_rules" id="Medicine_charge">0.0</span></strong>
    </div>
</div>
