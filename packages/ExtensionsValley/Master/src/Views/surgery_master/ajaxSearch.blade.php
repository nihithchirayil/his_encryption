<?php
if(count($resultData)!=0){
    if($input_id == 'service_item'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->code)) }}","{{ htmlentities(ucfirst($item->name)) }}","{{ $input_id }}","{{ $item->price }}","{{ $item->id }}")'>
    {{ $item->name }} ||{{ $item->code }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'instrument_list' || $input_id == 'equipment_list'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->code)) }}","{{ htmlentities(ucfirst($item->name)) }}","{{ $input_id }}","{{ $item->price }}")'>
    {{ $item->name }} ||{{ $item->price }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'medicine_serach'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillMedicineData("{{ htmlentities(ucfirst($item->item_code)) }}","{{ htmlentities(ucfirst($item->item_desc)) }}","{{ $input_id }}","{{ $item->stock }}","{{ $item->price }}","{{ $item->generic_name }}")'>
    {{ $item->item_desc }} ||{{ $item->generic_name }} ||{{ $item->price }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'consumables_search'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillMedicineData("{{ htmlentities(ucfirst($item->item_code)) }}","{{ htmlentities(ucfirst($item->item_desc)) }}","{{ $input_id }}","{{ $item->stock }}","{{ $item->price }}")'>
    {{ $item->item_desc }} ||{{ $item->price }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }
} else {
            echo 'No Results Found';
        }

?>
