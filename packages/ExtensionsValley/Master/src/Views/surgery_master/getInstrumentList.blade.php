<table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
    style="font-size: 12px;">
    <thead>
        <tr class="common_table_header">
            <th width="5%">Sl.No.</th>
            <th width="90%">
                Instrument Name
            </th>
            <th width="5%">Price</th>
        </tr>
    </thead>
    <tbody>
        @php
            $total_price = 0;
        @endphp
        @foreach ($instrument as $row)
            <tr>
                <td class="td_common_numeric_rules">{{ $loop->iteration }}</td>
                <td class="common_td_rules">{{ $row->name }}</td>
                <td class="common_td_rules">{{ $row->price }}</td>
                @php
                    $total_price += $row->price;
                @endphp
            </tr>
        @endforeach
        <input type="hidden" id="total_price" value="{{ $total_price }}">
    </tbody>

</table>
<script></script>
