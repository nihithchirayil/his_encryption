<div class="col-md-12 padding_sm theadscroll" style="height: 432px;">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
        style="font-size: 12px;">
        <thead>
            <tr class="common_table_header">
                <th class="common_td_rules" width="5%">
                    Sl.No.
                </th>
                <th class="common_td_rules" width="90%">
                    Notes
                </th>
                <th width="5%"> <button type="button" onclick="addNewBillChecklist()" class="btn btn-primary"><i class="fa fa-plus"></i></button></th>
            </tr>
        </thead>
        <tbody id="bill_check_list">
        </tbody>

    </table>
</div>
<div class="col-md-12 padding_sm">
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" style="padding: 5px 5px;margin-top: 15px;" onclick="validate('bill_check_list');" name="next"
            id="personalBtn" class="btnNext btn-block btn btn-success pull-right">
            <i id="personalSpin" class="fa fa-save"></i> Save & Next <i class="fa fa-forward"></i></button>
    </div>
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" onclick="getPreviousTab('bill_check_list')" style="padding: 5px 5px;margin-top: 15px;"
            id="personalPreviousBtn" name="Previous" class="btnPrevious btn-block btn btn-warning "><i class="fa fa-backward"></i>
            Previous</button>
    </div>
</div>
