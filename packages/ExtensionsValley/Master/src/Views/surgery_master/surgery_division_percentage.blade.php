<div class="col-md-12 padding_sm" style="margin-top: 10px;">
    <div class="col-md-6 padding_sm">
        <div class="theadscroll" style="height: 425px;" id="consumables_search">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
                style="font-size: 12px;">
                <thead>
                    <tr class="common_main_table_header">
                        <th colspan="3">New Surgery</th>
                    </tr>
                    <tr class="common_table_header">
                        <th class="common_td_rules" width="5%">
                            Sl.No.
                        </th>
                        <th class="common_td_rules" width="65%">
                            Service Name
                        </th>
                        <th class="common_td_rules" width="15%">
                            Percentage
                        </th>
                    </tr>
                </thead>
                <tbody id="new_surgergy_division_per">
                    <?php
                    if(count($new_surgergy)!=0){
                        $i=1;
                        foreach ($new_surgergy as $each) {
                            $percentage=$each->default_percentage;
                            if($each->id==$each->surgery_division_id){
                                $percentage=$each->percentage;
                            }
                            ?>
                    <tr>
                        <td class="common_td_rules"><?= $i ?></td>
                        <td class="common_td_rules"><?= $each->name ?></td>
                        <td class="common_td_rules"><input class="form-control" value="<?= $percentage ?>"
                                autocomplete="off" type="text" name="new_surgergy_percentage"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                            <input type="hidden" name="new_surgergy_percentage_id" value="<?= $each->id ?>">
                        </td>
                    </tr>
                    <?php
                        $i++;
                            }
                        }
                        ?>
                </tbody>

            </table>
        </div>
    </div>
    <div class="col-md-6 padding_sm">
        <div class="theadscroll" style="height: 425px;" id="consumables_search">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
                style="font-size: 12px;">
                <thead>
                    <tr class="common_main_table_header">
                        <th colspan="3">Subsequent Surgery</th>
                    </tr>
                    <tr class="common_table_header">
                        <th class="common_td_rules" width="5%">
                            Sl.No.
                        </th>
                        <th class="common_td_rules" width="65%">
                            Service Name
                        </th>
                        <th class="common_td_rules" width="15%">
                            Percentage
                        </th>
                    </tr>
                </thead>
                <tbody id="sub_sequence_surgergy_division_per">
                    <?php
        if(count($sub_sequence_surgery)!=0){
            $i=1;
            foreach ($sub_sequence_surgery as $each) {
                $percentage=$each->default_percentage;
                if($each->id==$each->surgery_division_id){
                    $percentage=$each->percentage;
                }
                ?>
                    <tr>
                        <td class="common_td_rules"><?= $i ?></td>
                        <td class="common_td_rules"><?= $each->name ?></td>
                        <td class="common_td_rules"><input class="form-control" value="<?= $percentage ?>"
                                autocomplete="off" type="text" name="sub_sequence_surgery_percentage"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                            <input type="hidden" name="sub_sequence_surgery_id" value="<?= $each->id ?>">
                        </td>
                    </tr>
                    <?php
                $i++;
            }
        }
    ?>
                </tbody>

            </table>
        </div>
    </div>
</div>

<div class="col-md-12 padding_sm">
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" style="padding: 5px 5px;margin-top: 15px;" onclick="saveCompleteSurgery()" name="next"
            id="saveCompleteSurgeryBtn" class="btnNext btn-block btn btn-primary pull-right">
            <i id="saveCompleteSurgerySpin" class="fa fa-save"></i> Save & Complete</button>
    </div>
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" onclick="getPreviousTab('surgery_division')" style="padding: 5px 5px;margin-top: 15px;"
            id="personalPreviousBtn" name="Previous" class="btnPrevious btn-block btn btn-warning "><i
                class="fa fa-backward"></i>
            Previous</button>
    </div>
</div>
