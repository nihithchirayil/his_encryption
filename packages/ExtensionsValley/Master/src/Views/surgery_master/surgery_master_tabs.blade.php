@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/wizard.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        <input type="hidden" id="surgery_id" value="<?= $surgery_id ?>">
        <input type="hidden" id="surgery_full_deatils" value="<?= $surgery_full_deatils ?>">

        <div class="row" style="margin-bottom: 10px"> </div>
        <div class="row codfox_container" style="margin-top: -19px;margin-bottom: -36px;">
            <div class="col-md-12 pull-right" style="text-align: right; font-size: 12px; font-weight: bold;">
                {{ $title }}</div>
            <div class="clearfix"></div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-12 padding_sm">

                        <div id="msform">
                            <ul id="progressbar" style="cursor: pointer;margin: 3px;" class="nav nav-tabs" role="tablist">
                                <li class="nav-item" id="surgery_details_li">
                                    <a class="nav-link" id="surgery_details-tab" href="#surgery_details_tab"
                                        role="tab" aria-controls="surgery_details" aria-selected="true"><strong><i
                                                class="fa fa fa-scissors"></i> Surgery
                                            Details</strong></a>
                                </li>
                                <li class="nav-item" id="instrument_list_li">
                                    <a class="nav-link" id="instrument_list-tab" href="#instrument_list_tab"
                                        role="tab" aria-controls="instrument_list" aria-selected="true"><strong><i
                                                class="fa fa-wrench"></i> Instruments </strong></a>
                                </li>
                                <li class="nav-item" id="equipment_list_li">
                                    <a class="nav-link" id="equipment_list-tab" href="#equipment_list_tab" role="tab"
                                        aria-controls="equipment_list" aria-selected="true"><strong><i
                                                class="fa fa-thermometer-full"></i> Equipment </strong></a>
                                </li>

                                <li class="nav-item" id="medicine_pack_list_li">
                                    <a class="nav-link" id="medicine_pack_list-tab" href="#medicine_pack_list_tab"
                                        role="tab" aria-controls="medicine_pack_list" aria-selected="true"><strong><i
                                                class="fa fa-medkit"></i> Medicine </strong></a>
                                </li>
                                <li class="nav-item" id="consumables_and_disposables_list_li">
                                    <a class="nav-link" id="consumables_and_disposables_list-tab"
                                        href="#consumables_and_disposables_list_tab" role="tab"
                                        aria-controls="consumables_and_disposables_list" aria-selected="true"><strong><i
                                                class="fa fa-database"></i> Consumables </strong></a>
                                </li>
                                <li class="nav-item" id="bill_configuration_list_li">
                                    <a class="nav-link" id="bill_configuration_list-tab"
                                        href="#bill_configuration_list_tab" role="tab"
                                        aria-controls="bill_configuration_list" aria-selected="true"><strong><i
                                                class="fa fa-cogs"></i> Bill Configuration </strong></a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane  active" id="surgery_details_tab" role="tabpanel"
                                    aria-labelledby="nav-surgery-tab">
                                    <div class="form-card">
                                        <div class="box no-border no-margin">
                                            <div class="box-footer"
                                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #0c9e61;height: 500px;">
                                                @include('Master::surgery_master.surgery_details')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="instrument_list_tab" role="tabpanel"
                                    aria-labelledby="nav-instrument-tab">
                                    <div class="form-card">
                                        <div class="box no-border no-margin">
                                            <div class="box-footer"
                                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #0c9e61;height: 500px;">
                                                @include('Master::surgery_master.instrumental_details')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="equipment_list_tab" role="tabpanel"
                                    aria-labelledby="nav-equipment-tab">
                                    <div class="form-card">
                                        <div class="box no-border no-margin">
                                            <div class="box-footer"
                                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #0c9e61;height: 500px;">
                                                @include('Master::surgery_master.equipmental_details')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="medicine_pack_list_tab" role="tabpanel"
                                    aria-labelledby="nav-medicine_pack-tab">
                                    <div class="form-card">
                                        <div class="box no-border no-margin">
                                            <div class="box-footer"
                                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #0c9e61;height: 500px;">
                                                @include('Master::surgery_master.medicine_details')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="consumables_and_disposables_list_tab" role="tabpanel"
                                    aria-labelledby="nav-consumables_and_disposables-tab">
                                    <div class="form-card">
                                        <div class="box no-border no-margin">
                                            <div class="box-footer"
                                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #0c9e61;height: 500px;">
                                                @include('Master::surgery_master.consumables_details')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="bill_configuration_list_tab" role="tabpanel"
                                    aria-labelledby="nav-bill_configuration_list-tab">
                                    <div class="form-card">
                                        <div class="box no-border no-margin">
                                            <div class="box-footer"
                                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #0c9e61;height: 500px;">
                                                @include('Master::surgery_master.surgery_division_percentage')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('javascript_extra')
    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/surgerydetailtab.js') }}"></script>
@endsection
