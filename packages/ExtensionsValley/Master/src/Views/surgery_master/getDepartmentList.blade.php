<table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
    style="font-size: 12px;">
    <thead>
        <tr class="common_main_table_header">
            <th colspan="3">Select Speciality</th>
        </tr>
        <tr class="common_table_header">
            <th class="common_td_rules" width="5%">Sl.No.</th>
            <th class="common_td_rules" width="5%">Select</th>
            <th class="common_td_rules" width="90%">
                <input id="issue_search_box" onkeyup="searchbyName();" type="text"
                    placeholder="Department Name Search.. " style="color: #000;display: none;width: 22%;">
                <span id="item_search_btn" style="padding: 0px 5px;" class="btn btn-warning"><i
                        class="fa fa-search"></i></span>
                <span id="item_search_btn_text">Department Name</span>
            </th>

        </tr>
    </thead>
    <tbody>
        @foreach ($department as $row)
            @php $selected=''; @endphp
            @if ($row->code == $row->department_code)
                @php $selected='checked'; @endphp
            @endif
            <tr>
                <td class="common_td_rules">{{ $loop->iteration }}</td>
                <td>
                    <div class="checkbox checkbox-success inline">
                        <input {{ $selected }} value="{{ $row->code }}" class="department_code"
                            id="department_code{{ $row->code }}" type="checkbox" name="checkbox">
                        <label for="department_code{{ $row->code }}" class="text-blue" for="checkbox">
                        </label>
                    </div>
                </td>
                <td class="common_td_rules">{{ $row->name }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<script></script>
