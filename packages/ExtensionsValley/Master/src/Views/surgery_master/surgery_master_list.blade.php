@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/wizard.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/wizard_checklist.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <div class="right_col">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        <div class="col-md-12 pull-right" style="text-align: right; font-size: 12px; font-weight: bold;">
            {{ $title }}</div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <form action="{{ route('extensionsvalley.ot_schedule.OTSchedule') }}" id="categorySearchForm"
                                method="POST">
                                {!! Form::token() !!}
                                <div class="col-md-1 padding_sm">
                                    <div class="mate-input-box">
                                        <label style="top:0px;" for="">From Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" name="from_date" autocomplete="off"
                                            value="" class="form-control datepicker" id="from_date"
                                            placeholder="From Date">
                                    </div>
                                </div>

                                <div class="col-md-1 padding_sm">
                                    <div class="mate-input-box">
                                        <label style="top:0px;" for="">To Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" name="to_date" autocomplete="off" value=""
                                            class="form-control datepicker" id="to_date" placeholder="To Date">
                                    </div>
                                </div>

                                <div class="col-md-5 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Surgery Master</label>
                                        <div class="clearfix"></div>
                                        <select id="surgery_master" class="form-control select2"
                                            style="color:#555555; padding:2px 12px;overflow: auto;">
                                            <option value="All">Select</option>
                                            @foreach ($surgery_master as $each)
                                                <option value="{{ $each->id }}">{{ $each->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-1 padding_sm" style="margin-top: -10px">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button type="button" id="searchSurgeryMasterBtn" onclick="searchSurgeryMaster()" class="btn btn-block btn-primary"><i id="searchSurgeryMasterSpin" class="fa fa-search"></i>
                                        Search</button>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: -10px">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button type="button" onclick="resetSugrgeryMaster()" class="btn btn-block btn-warning"><i class="fa fa-recycle"></i>
                                        Reset</button>
                                </div>
                                <div class="col-md-2 padding_sm pull-right" style="margin-top: -10px">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button type="button"  onclick="addWindowLoad('getSurgeryMaster','')" class="btn btn-block btn-success"><i class="fa fa-plus"></i>
                                        Add New Surgery Master</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix" id="searchSurgeryMasterDiv">

                    </div>
                </div>
            </div>

        </div>
    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/surgery_master_list.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

@endsection
