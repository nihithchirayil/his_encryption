<?php
if(sizeof($resultData)>0){
    if($input_id == 'cash_collected_by'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->name)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'users'){
        if(!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->name)); ?>
                </li>
                <?php
            }
        }else{
            echo 'No Results Found';
        }
    }else if($input_id == 'menu'){
        if(!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->name)); ?>
                </li>
                <?php
            }
        }else{
            echo 'No Results Found';
        }
    }else if($input_id == 'op_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->uhid))}}","{{$input_id}}")'>
                    {{$item->uhid}} | {{$item->patient_name}}
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'bill_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities($item->bill_no)}}","{{ htmlentities(ucfirst($item->bill_no))}}","{{$input_id}}")'>
                        {{$item->bill_no}}
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'transaction_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities($item->transaction_no)}}","{{ htmlentities(ucfirst($item->transaction_no))}}","{{$input_id}}")'>
                        {{$item->transaction_no}}
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'patient'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->patient_name))}}","{{$input_id}}")'>
                        {{$item->uhid}} | {{$item->patient_name}}
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
    else if($input_id == 'patient_uhid'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->uhid))}}","{{ htmlentities(ucfirst($item->patient_name))}}","{{$input_id}}")'>
                        {{$item->uhid}} | {{$item->patient_name}}
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }



    else if($input_id == 'bill_type'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->code))}}","{{ htmlentities(ucfirst($item->name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->name)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'credit_company'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->company_name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->company_name)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'scheme'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->tariff_name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->tariff_name)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'department'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->dept_code))}}","{{ htmlentities(ucfirst($item->dept_name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->dept_name)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'sub_department'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->subdepartment_code))}}","{{ htmlentities(ucfirst($item->sub_name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->sub_name)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'item'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->code))}}","{{ htmlentities(ucfirst($item->name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->name)); ?>[<i><?php echo htmlentities(ucfirst($item->type)); ?></i>]
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'item_name'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->code))}}","{{ htmlentities(ucfirst($item->name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->name)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'doctor_name'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{!!ucfirst($item->doctor_name)!!}","{{$input_id}}")'>
                    {!!ucfirst($item->doctor_name)!!}
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'manufacturer'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->manufacturer_name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->manufacturer_name)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'generic_name'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->generic_name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->generic_name)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'purchase_return_num'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->purchase_return_no))}}","{{ htmlentities(ucfirst($item->purchase_return_no))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->purchase_return_no)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }

    else if($input_id == 'invoice_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->invoice_no))}}","{{ htmlentities(ucfirst($item->invoice_no))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->invoice_no)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }

    else if($input_id == 'service'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick="fillSearchDetials('{{htmlentities(ucfirst($item->service_code))}}','{{ htmlentities(ucfirst($item->service_desc))}}','service')">
                    <?php echo htmlentities(ucfirst($item->service_desc)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'sales_return_num'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->return_no))}}","{{ htmlentities(ucfirst($item->return_no))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->return_no)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
    else if($input_id == 'ip_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->admission_no))}}","{{ htmlentities(ucfirst($item->admission_no))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->admission_no)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
    else if($input_id == 'icd_codes'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick="fillSearchDetials('{{htmlentities(ucfirst($item->icd10_code))}}','{{ htmlentities(ucfirst($item->title))}}','icd_codes')">
                    <?php echo htmlentities(ucfirst($item->title)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
    else if($input_id == 'ledger_master'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->ledger_name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->ledger_name)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
    else if($input_id == 'ledger_groups'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->ledger_name))}}","{{$input_id}}")'>
                        <?php echo htmlentities(ucfirst($item->ledger_name)); ?>
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
}else{
    ?>
    <li>No results found!</li>
    <?php
}
?>
