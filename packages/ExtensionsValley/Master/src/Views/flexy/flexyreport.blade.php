@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<style>
    .header_bg{
        background-color: mediumseagreen;
        color:white;
    }
    .ajaxSearchBox{
        display: block;
        width: 250px !important;
        height: 400px !important;
        overflow: hidden;
        padding-top: 0px !important;
        margin-top: -29px !important;
    }

    ul{padding-left:5px !important;font-weight: 200 !important;}
    li>a:hover{color:black !important;}
    .table_name{color:rgb(114, 4, 4) !important;}
    .gradient_bg{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
        color:white !important;
    }
    .bottom-border-text{
    border:none !important;
    border-bottom:2px solid rgb(160, 160, 160) !important;
    box-shadow:none;
    }
    label{
        color:rgb(92 115 157) !important;
        font-size: 13px !important;
        font-weight: 600 !important;
    }
    .bg-radio_grp{
    background-color:#b9b9b9 !important;
    color:white !important;
    margin-right:6px !important;
    /* box-shadow: 3px 2px 3px #bdbdbd; */
    }

    .bg-radio_grp.active{
        background-color:#36a693 !important;
        color:white !important;
        margin-right:6px !important;
        box-shadow: 3px 2px 3px #55ff79;
    }
    .select2-selection__choice{
        background-color: #e6f1e8 !important;
        border: 1px solid #dbd8d8 !important;
        font-size: 10px !important;
    }

    .select2-container{
        box-shadow: none !important;
        border-bottom:2px solid rgb(160, 160, 160) !important;
    }
</style>

@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<div class="right_col">
        <div class="col-md-12 no-padding text-center box-body gradient_bg">
            <div class="col-md-11">
                <h5><b>FLEXY REPORT</b></h5>
            </div>

            <div class="col-md-1">
                <button type="button" onclick="show_read_me();" style="margin-top:5px !important;color: rgb(114 124 161);background-color:yellow;" class="btn btn-sm" id="read_me"><i class="fa fa-question-circle" aria-hidden="true"></i></button>
                <button type="button" onclick="show_sql();" style="margin-top:5px !important;color: rgb(114 124 161);background-color:yellow;" class="btn btn-sm" id="read_me"><i class="fa fa-database" aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:15px;">
            <div class="col-md-3 pull-right">
                <button onclick="exicuteQuery();" type="button" style="width:110px;" class="btn bg-green pull-right" title="Exicute" id="flexy_report_run">
                    <i class="fa fa-save"></i> Save
                </button>

                <button type="button" style="width:110px;" class="btn btn-warning pull-right" title="Reset" id="flexy_report_reset">
                    <i class="fa fa-repeat"></i> Reset
                </button>
            </div>
            <div class="col-md-4 pull-right">
                <label>Select Report</label>
                <div class="clearfix"></div>
                {!! Form::select('flexy_reports', $flexy_reports, 0, ['class' => 'form-control select2 filters', 'placeholder' => 'Select Report','onChange' => 'selectReport(this.value);', 'id' => 'flexy_reports', 'style' => 'width:100%;color:#555555; padding:2px 12px;']) !!}
            </div>
            <input type="hidden" name="report_id" id="report_id" value="">

            <div class="col-md-3 padding_sm pull-right" id="group_access" style="display:none;">
                <label class=""
                    style="width: 100%;">Select Groups</label>
                <div class="clearfix"></div>
                {!! Form::select('groups', $groups, 0, ['class' => 'form-control select2 filters', 'multiple' => 'multiple-select', 'placeholder' => ' ', 'id' => 'groups', 'style' => 'width:100%;color:#555555; padding:2px 10px;']) !!}
            </div>
            <div class="col-md-3 padding_sm pull-right" id="user_access" style="display:none;">
                    <label class=""
                        style="width: 100%;">Select Users</label>
                    <div class="clearfix"></div>
                    {!! Form::select('users', $user_list, 0, ['class' => 'form-control select2 filters', 'multiple' => 'multiple-select', 'placeholder' => ' ', 'id' => 'users', 'style' => 'width:100%;color:#555555; padding:2px 10px;']) !!}
            </div>

            <div class="col-md-2 pull-left">
                <label>Access Given To</label>
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn bg-radio_grp" onclick="selectAccessType(1);">
                        <input type="radio" name="access_type" id="public_wise" checked autocomplete="off" value='1'> Public
                    </label>
                    <label class="btn bg-radio_grp" onclick="selectAccessType(2);">
                        <input type="radio" name="access_type" id="group_wise" autocomplete="off" value='2'> Group
                    </label>
                    <label class="btn bg-radio_grp" onclick="selectAccessType(3);">
                        <input type="radio" name="access_type" id="user_wise" autocomplete="off" value='3'> Users
                    </label>
                </div>
            </div>

        </div>

        <div class="col-md-12 no-padding" style="margin-top:30px;">
            <div class="col-md-3">
                <label>Report Name</label>
                <input type="text" autocomplete="off" class="form-control bottom-border-text" id="report_name" />
            </div>



            <div class="col-md-6 no-padding">
                    <label class=""
                        style="width: 100%;">Masters</label>
                    <div class="clearfix"></div>
                    {!! Form::select('flexy_masters', $flexy_masters, 0, ['class' => 'form-control select2 filters', 'placeholder' => '',  'id' => 'flexy_masters', 'style' => 'width:100%;color:#555555; padding:2px 12px;','onChange' => 'addClassToQuery(this)']) !!}
            </div>

        </div>


        <div class="col-md-6" style="margin-top:15px">
            <label class=""
            style="width: 100%;">Colum Indexes To Show Totals</label>
        <div class="clearfix"></div>
        <input type="text" autocomplete="off" class="form-control bottom-border-text" id="flexy_report_totals" placeholder="Enter comma seperated values" style="width:100%;color:#555555; padding:2px 12px;">
        </div>
        <div class="col-md-4" style="margin-top:15px">
            <label class=""
            style="width: 100%;">Colum width set</label>
        <div class="clearfix"></div>
        <input type="text" autocomplete="off" class="form-control bottom-border-text" id="custom_column_width" placeholder="Enter comma seperated values in percentage" style="width:100%;color:#555555; padding:2px 12px;" onkeyup="checkSumWidth();">
        </div>
        <div class="col-md-2" style="margin-top:15px">
            <label class=""
            style="width: 100%;">Sum(95*)</label>
        <div class="clearfix"></div>
        <input type="text" readonly="readonly" autocomplete="off" class="form-control bottom-border-text" id="sum_width" placeholder="" style="width:100%;color:#555555; padding:2px 12px;">
        </div>




        <div class="col-md-12 padding-sm" style="margin-top:30px;">
            {{-- <div class="col-md-12" style="height:400px;font-size:15px;
            color: darkslategray;" id="flexy_report_sql" contenteditable="true">
            </div> --}}
            <label for="flexy_report_sql">Sql Query </label>
            <textarea class="form-control" id="flexy_report_sql" style="height:320px !important;font-size:15px !important;color:darkslategray !important;border:2px solid rgb(160, 160, 160) !important;">
            </textarea>

            <div class="col-md-12" id="result_container">

            </div>
        </div>

        <div class="col-md-9" id="" style="margin-top:15px;">
            <label class=""
            style="width: 100%;">Numeric Colums</label>
        <div class="clearfix"></div>
        <input type="text" autocomplete="off" name="numeric_colums" autocomplete="off" class="form-control bottom-border-text" id="numeric_colums" placeholder="Enter comma seperated column names, Other colums are considered as non-numeric colums" style="width:100%;color:#555555; padding:2px 12px;"/>
        </div>


        <div class="col-md-3" style="margin-top:15px">
            <input type="checkbox" id="has_drill_down" name="has_drill_down" value="1" style="margin-top:10px;">
            <label for="has_drill_down" style="margin-top:10px;">Has Drill Down</label>
        </div>
        <div class="col-md-9" id="drilldown_column_container" style="margin-top:15px;display:none;">
            <label class=""
            style="width: 100%;">Drill Down Columns</label>
        <div class="clearfix"></div>
        <input type="text" autocomplete="off" name="drill_down_columns" autocomplete="off" class="form-control bottom-border-text" id="drill_down_columns" placeholder="Enter comma seperated column names" style="width:100%;color:#555555; padding:2px 12px;"/>
        </div>

        <div class="col-md-12 padding-sm" id="drilldown_column_sql_container" style="margin-top:30px;display:none;">

            <label for="flexy_report_sql">Drill Down Query </label>
            <textarea class="form-control" id="drill_down_sql" style="height:170px !important;font-size:15px !important;color:darkslategray !important;border:2px solid rgb(160, 160, 160) !important;">
            </textarea>
        </div>


</div>
<div class="modal" id="read_me_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width:80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:white;">Instructions</h4>
            </div>
            <div class="modal-body" style="min-height:500px;overflow-y:scroll;">
                <div class="col-md-12" style="margin-top:15px;">
                    <h5 style="color:rgb(21, 21, 109);"><b>Hide a Colum From The View Part</b></h5>
                    <p>
                        To hide a column from the view part, you can use the following syntax:
                        <i style="color:rgb(21, 21, 109);"> '@'||id as \"@id\" </i>
                    </p>

                    <h5 style="color:rgb(21, 21, 109);"><b> Where 1=1 </b></h5>
                    <p> Don't forget to add 1=1 condition after <i>Where</i> clause. </p>

                    <h5 style="color:rgb(21, 21, 109);"><b>Filter Conditions</b></h5>
                    <p>
                        filter conditions should be specified in the following syntax:
                        <i style="color:rgb(21, 21, 109)">
                        # and condition = '{filter condition}' #
                        </i>
                    </p>
                    <h5 style="color:rgb(21, 21, 109);"><b>Sql Example</b></h5>
                    <p><i>
                        select  '@'||bh.id as \"@id\",bh.bill_no,bt.name as bill_tag,
                    bh.bill_amount,bh.patient_name from bill_head bh
                    join patient_master pm on pm.uhid = bh.uhid
                    join bill_tag bt on bt.code = bh.bill_tag
                    where 1=1 # and pm.id ={Patient Progressive Search}#
                    </i>
                    </p>
                    <h5 style="color:rgb(21, 21, 109);"><b>Show Total Results</b></h5>
                    <p>
                        To show total results for individual columns,
                        Should be specified column names in <i>Colum Indexes To Show Totals</i>

                    </p>
                    <h5 style="color:rgb(21, 21, 109);"><b>Column width Adjustments</b></h5>
                    <p>
                        To adjust column width,
                        Should be specified width in <i>Colum width set</i>
                        first colum is index numner. it seted as 10 by default.
                        all other colums width should set in percenage and separeate by comma.
                        <br><i>example: 10,30,50</i>
                        total sum of percentage of columns custom width should be 90.
                    </p>


                    <h5 style="color:rgb(21, 21, 109);"><b>Drill down Rerport </b> </h5>
                    <ul>
                        <li>
                            <b>Sql Example</b><br>
                            <p><i>
                                select  '@'||bh.id as \"@id\",bh.bill_no,bt.name as bill_tag,
                                bh.bill_amount,bh.patient_name from bill_head bh
                                join patient_master pm on pm.uhid = bh.uhid
                                join bill_tag bt on bt.code = bh.bill_tag
                                where 1=1 # and pm.id ={Patient Progressive Search}#
                                </i>
                            </p>

                        </li>
                        <li>
                            <b>Drill Down Sql Example</b><br>
                            <p>
                                <i>select bd.item_desc, bd.qty,bd.net_amount from bill_detail bd where bill_head_id = {id}</i><br>
                                In this. <i>{id}</i> is passed from the main report.<br>
                                It should be specified in the Drill Down Columns like below.<br>
                                <i style="color:rgb(21, 21, 109);">id,bill_no</i>
                            </p>
                        </li>
                        <li>
                            <b>Important!!!</b><br>

                            <i>Dont forget to Add COLUM WIDTH FOR ALL COLUMS</i>
                            </p>
                        </li>
                    </ul>


                    <br>




                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="sql_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width:60%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:white;">Menu Sql</h4>
            </div>
            <div class="modal-body" style="min-height:250px;">
                <div class="col-md-12" id="menu_sql">

                </div>

            </div>
        </div>
    </div>
</div>


@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/master/default/javascript/flexy.js")}}"></script>
<script type="text/javascript">

</script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
@endsection
