@if(sizeof($res)>0)
<ul style="list-style-type: none;">
    @foreach ($res as $data)
        <li onclick="fetchDbTable('{{$data->table_name}}',this);"><a>{{$data->table_name}}</a></li>
    @endforeach
</ul>
@else
    <p>No data found</p>
@endif
