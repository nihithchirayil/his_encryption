<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var expense_name = $('#expense_name').val();
            var token = $('#token_hiddendata').val();
            var param = {
                _token: token,
                expense_name:expense_name
               
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $("#searchDataDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });

                    $('#searchBtn').attr('disabled', true);
                    $('#searchSpin').removeClass('fa fa-search');
                    $('#searchSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchDataDiv').html(data);
                },
                complete: function() {
                    $('#searchBtn').attr('disabled', false);
                    $('#searchSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchSpin').addClass('fa fa-search');
                    $("#searchDataDiv").LoadingOverlay("hide");

                },
            });
            return false;
        });

    });
</script>
<?php
$div_height = '515px';
if ($page_links) {
    $div_height = '450px';
}
?>

<div class="theadscroll" style="position: relative; height: <?= $div_height ?>;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table" style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="5%">Sl.No.</th>
                <th width="70%">Name</th>
                <th width="10%">Status</th>
                <th width="15%">Action</th>


            </tr>
        </thead>
        <tbody>
            @if (sizeof($search)>0)
            @foreach ($search as $item)

            <tr style="cursor:pointer;" id="trlist_{{ $item->id }}" class="tr_count">
                <td class="td_common_numeric_rules" >{{($search->currentPage() - 1) * $search->perPage() + $loop->iteration}}</td>
                <td class="common_td_rules">{{strtoupper($item->name)}}</td>
                <td class="common_td_rules">{{$item->status}}</td>
                <td>
                    <button type="button" class="btn btn-sm btn-warning "  onclick="editItem({{$item->id}},'{{$item->name}}',{{$item->status_id}});"><i id = "editButton_{{$item->id}}" class="fa fa-edit editButton padding_sm" title="Edit"></i>Edit </button>
                    <button type="button" class="btn btn-sm btn-danger " onclick="delete_menu({{$item->id}});"><i id = "deleteButton_{{$item->id}}" class="fa fa-trash" title="Delete"></i> </button>
                </td>


            </tr>

            @endforeach
            @else
            <tr>
                <td colspan="4" style="text-align: center">No Records found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>