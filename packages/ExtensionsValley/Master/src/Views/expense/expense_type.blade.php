@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
   
      <!-- Custom Theme Scripts -->
      <link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">

    
       <!-- jQuery -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/jquery.min.js') }}"></script>

    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">



@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="row" style="margin-bottom: 10px"> </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <h4 class="blue"><strong> <?= $title ?> </strong></h4>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-9 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix" style="height:70px">
                        <div class="col-md-12 padding_sm">
                            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
                            <input type="hidden" id="hid_id" value="">

                            <div class="col-md-4 padding_sm" style="margin-top:10px;">
                                <div class="mate-input-box ">
                                    <label for="">Expense Type</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control " id="expense_type" value="">
                                  
                                </div>
                            </div>

                            <div class="col-md-1 pull-right  padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" onclick="searchType()" id="searchBtn"
                                    class="btn btn-block btn-primary"><i id="searchSpin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 padding_sm">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div id="searchDataDiv"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3 padding_sm">
                <div class="box no-border">
                    <input type="hidden" name="hidden_id" id="hidden_id" value="">
                    <div class="box-body clearfix" style="height: 630px">
                        <h5 class="blue text-center"><strong> Add Expense Type </strong></h5>
                        <hr>
                        <div class="col-md-12 padding_sm">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control reset" id="type_name"
                                        name="type_name" value="" required>
                                </div>
                            </div>
                           
                           
                            <div class="col-md-6 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Status</label>
                                    <div class="clearfix"></div>
                                    <select class="form-control reset" name="status" id="status">
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>

                                    </select>
                                </div>
                            </div>
                           

                        </div>
                        <div class="col-md-12 padding_sm" style="margin-top:10px;">
                            <div class="col-md-4 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <button type="button" id="savebtnBtn" class="btn btn-block btn-success"
                                    onclick="saveType()"><i id="savebtnSpin" class="fa fa-save"></i> Save
                                </button>
                            </div>
                            <div class="col-md-4 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-info clearFormButton"
                                    onclick="formReset()"><i class="fa fa-times"></i> Clear </button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>



    </div>
    </div>
    </div>





    <!-- /page content -->

@stop
@section('javascript_extra')
 
    <script src="{{asset("packages/extensionsvalley/emr/js/expensetype.js")}}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version='.env('APP_JS_VERSION', '0.0.1')) }}"> </script>
@endsection
