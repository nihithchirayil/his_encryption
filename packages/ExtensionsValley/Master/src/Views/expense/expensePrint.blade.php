<div class="col-md-12">

    <div class="col-md-12" id="hospitalheader" style="padding: 0px;">
        {!! $hospital_header !!}
    </div>
    <div class="col-md-12 " id="initial_details" style="padding: 0px">
        <tr>
            <th colspan="10">
                <table style="width: 100%;border:1px solid rgb(207, 184, 184)">
                    <tr>
                        <th style="width: 25%">
                            <p style="font-weight: 600">Debit:</p>
                        </th>
                        <th style="width: 25%">
                            <p style="font-weight: 300">{{ @$data[0]->expense_type ? $data[0]->expense_type : '' }}</p>
                        </th>
                        <th style="width: 25%">
                            <p style="font-weight: 600">Voucher No:</p>
                        </th>
                        <th style="width: 25%">
                            <p style="font-weight: 300">{{ @$data[0]->voucher_no ? $data[0]->voucher_no : '' }}</p>
                        </th>
                    </tr>
                    <tr>
                        <th style="width: 25%">
                            <p style="font-weight: 600">Paid to:</p>
                        </th>
                        <th style="width: 25%">
                            <p style="font-weight: 300">{{ @$data[0]->paid_to ? $data[0]->paid_to : '' }}
                            </p>
                        </th>
                        <th style="width: 25%">
                            <p style="font-weight: 600">Date:</p>
                        </th>
                        <th style="width: 25%">
                            <p style="font-weight: 300">
                                {{ @$data[0]->created_at ? date('M-d-Y h:i A', strtotime($data[0]->created_at)) : '' }}
                            </p>
                        </th>
                    </tr>
                </table>
            </th>

        </tr>

    </div>

    <div class="col-md-12 " id="payable_details" style="height: 109px;padding: 0px">

        <table style="width: 100%;height: 103px;">
            <thead>
                <th style="width: 60%;border:1px solid rgb(207, 184, 184);text-align:center;">
                    <span> {{ @$data[0]->narattion ? $data[0]->narattion: '' }}</span>
                </th>
                <th style="width: 20%;border:1px solid  rgb(207, 184, 184">
                    <p style="text-align: center;margin-top:-43px;font-weight:600;border-bottom: 1px solid #d4c4c4;">Rs</p>
                    <p style="text-align: center;padding-top: 0px;">{{ @$data[0]->paid_amount ? round($data[0]->paid_amount) : '' }}</p>
                </th>
                <th style="width: 20%;border:1px solid rgb(207, 184, 184">
                    <p style="text-align: center;margin-top:-43px;font-weight:600;border-bottom: 1px solid #d4c4c4;">Ps</p>
                    <p style="text-align: center;padding-top: 0px;">0.00</span>
                </th>
            </thead>
        </table>
       
    </div>
    <div class="col-md-12" id="footer" style="padding: 0px">

        <table>
            <thead>
                <tr>
                       <th> &nbsp;
                        <?php $number_to_word = \ExtensionsValley\Master\GridController::number_to_word(round(@$data[0]->paid_amount ? $data[0]->paid_amount : 0));
                        echo strtoupper($number_to_word);
                        ?>&nbsp;&nbsp;</th></th>
                </tr>
                <tr> &nbsp;</tr>
               <tr>
                <th style="width: 30%">  <p for="">Prepared by:{{ @$data[0]->prepared_by ? $data[0]->prepared_by : '' }}</p></th>
                <th style="width: 30%">  <p for="">Sanctioned by:{{ @$data[0]->prepared_by ? $data[0]->prepared_by : '' }}</p></th>
                <th style="width: 11%">  <p for="">Signature</p></th>
               </tr>
            </thead>
        </table>
       
    </div>


</div>
