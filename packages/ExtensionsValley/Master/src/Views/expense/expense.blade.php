@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
   
      <!-- Custom Theme Scripts -->
      <link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">

    
       <!-- jQuery -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/jquery.min.js') }}"></script>

    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">



@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="row" style="margin-bottom: 10px"> </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <h4 class="blue"><strong> <?= $title ?> </strong></h4>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-9 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix" style="height:70px">
                        <div class="col-md-12 padding_sm">
                            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
                            <input type="hidden" id="hid_id" value="">
                            <input type="hidden" id="user_id" value="{{ $user_id }}">
                            <input type="hidden" id="user_name" value="{{ $user_name }}">
                            <div class="col-md-2 padding_sm" >
                                <div class="mate-input-box">
                                    <label for="">Bill No.</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" placeholder="Bill no." class="form-control =" id="sch_bill"
                                        name="sch_bill"  >
                                </div>
                        </div>
                        <div class="col-md-2 padding_sm" >
                            <div class="mate-input-box">
                                <label for="">Voucher No.</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" placeholder="Voucher no." class="form-control " id="sch_voucher"
                                    name="sch_voucher"  >
                            </div>
                    </div>
                            <div class="col-md-2 padding_sm" >
                                <div class="mate-input-box ">
                                    <label for="">Expense Type</label>
                                    <select class="form-control select2" name="expense_search" id="expense_search">
                                        <option value="">select</option>
                                        @foreach ($expense_types as $type)
                                        <option value="{{ $type->id }}">{{ $type->name }}</option> 
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm" >
                                    <div class="mate-input-box">
                                        <label for="">Expense From</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" placeholder="MM-DD-YYYY" class="form-control date_picker" id="expense_from"
                                            name="expense_from"  >
                                    </div>
                            </div>
                            <div class="col-md-2 padding_sm" >
                                    <div class="mate-input-box">
                                        <label for="">Expense To</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" placeholder="MM-DD-YYYY" class="form-control date_picker" id="expense_to"
                                            name="expense_to">
                                    </div>
                            </div>

                            <div class="col-md-1 pull-right  padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" onclick="searchExpense()" id="searchBtn"
                                    class="btn btn-block btn-primary"><i id="searchSpin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 padding_sm">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div id="searchDataDiv"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3 padding_sm">
                <div class="box no-border">
                    <input type="hidden" name="hidden_id" id="hidden_id" value="">
                    <div class="box-body clearfix" style="height: 630px">
                        <h5 class="blue text-center"><strong> Add Expense </strong></h5>
                        <hr>
                        <div class="col-md-12 padding_sm">
                           
                            <div class="col-md-12 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Expense Type</label>
                                    <select class="form-control reset select2" name="expense_type" id="expense_type">
                                        <option value="">select</option>
                                        @foreach ($expense_types as $type)
                                        <option value="{{ $type->id }}">{{ $type->name }}</option> 
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Amount</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" placeholder="Expense Amount" class="form-control reset" id="amount" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"


                                        name="amount" value="" >
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control date_picker reset" placeholder="Expense Date" id="expense_date"
                                        name="expense_date" value="" >
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Voucher No.</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control  reset" placeholder="Voucher no." id="voucher_no"
                                        name="voucher_no" value="" disabled>
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Bill No.</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control  reset" placeholder="Bill no." id="bill_no"
                                        name="bill_no" value="" >
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box">
                                        <label for="">Payment Type</label>
                                        <select onchange="changeData(this);" name="" class="form-control select2" id="payment_type">
                                            @foreach ($payment_types as $type)
                                                 <option value="{{ $type->id }}">{{ $type->name }}</option>                                                
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm check_no_div hidden" style="margin-top:10px">
                                <div class="mate-input-box">
                                    <label for="">Check No.</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control  reset" placeholder="Check number" id="check_no"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"   name="check_no" value="" >
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm bank_name_div hidden" style="margin-top:10px">
                                <div class="mate-input-box">
                                    
                                        <select name="" class="form-control select2" id="bank_name">
                                            <option value="">Bank Name</option>
                                            @foreach ($bank_name as $name)
                                                 <option value="{{ $name->id }}">{{ $name->bank_name }}</option>                                                
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                            
                            <div class="col-md-12 padding_sm" style="margin-top:10px">
                                <div class="mate-input-box" style="height: 104px !important;">
                                    <label for="">Narration</label>
                                    <div class="clearfix"></div>
                                    <textarea  class="form-control reset" style="resize: none" placeholder="Narration" id="narration"
                                        name="narration"></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 padding_sm" style="margin-top:10px;">
                            <div class="col-md-5 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                @if($expense_payment == 1)
                                    <button type="button" id="savebtnBtn3" class="btn btn-block btn-success"
                                    onclick="saveExpense(3)"><i id="savebtnSpin3" class="fa fa-save"></i> Approve & Pay
                                    </button>
                                @else
                                @if($app_access_type==1)
                                <button type="button" id="savebtnBtn0" class="btn btn-block btn-success"
                                    onclick="saveExpense(1)"><i id="savebtnSpin0" class="fa fa-save"></i> Save & Approve
                                </button>
                                @else
                                <button type="button" id="savebtnBtn0" class="btn btn-block btn-success"
                                    onclick="saveExpense(0)"><i id="savebtnSpin0" class="fa fa-save"></i> Save
                                </button>
                                @endif
                                @endif
                            </div>
                           @if($app_access_type==1)
                           <div class="col-md-3 padding_sm pull-right hidden" id="approve_this_claim" style="display: none">
                            <label for="">&nbsp;</label>
                            <button type="button" id="savebtnBtn1" class="btn btn-block btn-primary"
                                onclick="saveExpense(1)"><i id="savebtnSpin1" class="fa fa-thumbs-up"></i> Approve
                            </button>
                        </div>
                        <div class="col-md-3 padding_sm pull-right"  id="reject_this_claim" style="display: none">
                            <label for="">&nbsp;</label>
                            <button type="button" id="savebtnBtn2" class="btn btn-block btn-danger"
                                onclick="saveExpense(2)"><i id="savebtnSpin2" class="fa fa-thumbs-down"></i> Reject
                            </button>
                        </div>
                        <div class="col-md-3 padding_sm pull-right"  id="pay_this_claim" style="display: none">
                            <label for="">&nbsp;</label>
                            <button type="button" id="savebtnBtn3" class="btn btn-block btn-default"
                                onclick="saveExpense(3)"><i id="savebtnSpin3" class="fa fa-check"></i> Paid
                            </button>
                        </div>
                           @endif
                            <div class="col-md-3 padding_sm pull-right">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-info clearFormButton"
                                    onclick="formReset()"><i class="fa fa-times"></i> Clear </button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>



    </div>
    </div>
    </div>
    <div id="expenseDetails" class="modal fade" role="dialog" style="z-index:10000;">
        <div class="modal-dialog" style="width: 70%;">
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" id="expenseDetails_content">
    
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="print_generate('expenseDetails_content')"> <i class="fa fa-print"></i> Print</button>
                </div>
            </div>
    
        </div>
    </div>




    <!-- /page content -->

@stop
@section('javascript_extra')
 
    <script src="{{asset("packages/extensionsvalley/emr/js/expense.js")}}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version='.env('APP_JS_VERSION', '0.0.1')) }}"> </script>
    <script
    src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
