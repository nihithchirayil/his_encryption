<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var token = $('#token_hiddendata').val();
            var expense_type = $("#expense_search").val();
            var bill_no = $("#bill_no").val();
            var voucher_no = $("#voucher_no").val();
            var expense_from = $("#expense_from").val();
            var expense_to = $("#expense_to").val();
            var param = { _token: token, expense_type: expense_type,expense_from:expense_from,expense_to:expense_to,bill_no:bill_no,voucher_no:voucher_no };

            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $("#searchDataDiv").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });

                    $('#searchBtn').attr('disabled', true);
                    $('#searchSpin').removeClass('fa fa-search');
                    $('#searchSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchDataDiv').html(data);
                },
                complete: function() {
                    $('#searchBtn').attr('disabled', false);
                    $('#searchSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchSpin').addClass('fa fa-search');
                    $("#searchDataDiv").LoadingOverlay("hide");

                },
            });
            return false;
        });

    });
</script>
<?php
$div_height = '515px';
if ($page_links) {
    $div_height = '450px';
}
?>

<div class="theadscroll" style="position: relative; height: <?= $div_height ?>;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table" style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="5%">Sl.No.</th>
                <th width="10%">Bill No.</th>
                <th width="10%">Voucher No.</th>
                <th width="10%">Check No.</th>
                <th width="10%">Bank Name</th>
                <th width="10%">Expense Type</th>
                <th width="10%" style="white-space: pre">Narration</th>
                <th width="5%">Amount</th>
                <th width="5%">Status</th>
                <th width="5%">Expense Date</th>
                <th width="20%">Action</th>



            </tr>
        </thead>
        <tbody>
            @if (sizeof($search)>0)
            @foreach ($search as $item)

            <tr style="cursor:pointer;" id="trlist_{{ $item->id }}" class="tr_count">
                <td class="td_common_numeric_rules" >{{($search->currentPage() - 1) * $search->perPage() + $loop->iteration}}</td>
                <td class="common_td_rules" title="{{$item->bill_no}}">{{$item->bill_no}}</td>
                <td class="common_td_rules" title="{{$item->voucher_no}}">{{$item->voucher_no}}</td>
                <td class="common_td_rules" title="{{@$item->check_no ? $item->check_no : '-' }}">{{@$item->check_no ? $item->check_no : '-' }}</td>
                <td class="common_td_rules" title="{{@$item->bank_name ? $item->bank_name : '-'}}">{{@$item->bank_name ? $item->bank_name : '-'}}</td>
                <td class="common_td_rules" title="{{strtoupper($item->expense_type)}}">{{strtoupper($item->expense_type)}}</td>
                <td class="common_td_rules" title="{{@$item->narration ? $item->narration : '-'}}">{{@$item->narration ? $item->narration : '-'}}</td>
                <td class="common_td_rules" title="{{@$item->amount ? $item->amount.'/-' : '-'}}">{{@$item->amount ? $item->amount.'/-' : '-'}}</td>
                @if($item->status_id==0)
                 @php
                      $status_txt='Requested';
                 @endphp
                @elseif($item->status_id==1)
                @php
                $status_txt='Approved';
                @endphp
                @elseif($item->status_id==2)
                @php
                $status_txt='Rejected';
                @endphp
                @elseif($item->status_id==3)
                @php
                $status_txt='paid';
                @endphp
                @endif

                <td class="common_td_rules" >{{@$status_txt ? $status_txt : '-'}}</td>
                <td class="common_td_rules" title="{{date('M-d-Y',strtotime( $item->expense_date))}}">{{date('M-d-Y',strtotime( $item->expense_date))}}</td>
                <td>
                    @if($item->status_id!=2 && $item->status_id !=3)
                    <button type="button" class="btn btn-sm btn-warning "  onclick="editItem('{{$item->status_id}}',{{$item->id}},'{{$item->bank_id}}','{{$item->check_no}}','{{$item->voucher_no}}','{{$item->bill_no}}','{{$item->amount}}','{{$item->expense_type_id}}','{{date('M-d-Y',strtotime($item->expense_date))}}','{{ $item->narration }}','{{ $app_access_type }}','{{$item->collection_mode}}');"><i id = "editButton_{{$item->id}}" class="fa fa-edit editButton padding_sm" title="Edit"></i> </button>
                    @endif
                    @if($item->status_id !=3)
                    <button type="button" class="btn btn-sm btn-danger " onclick="deleteExpense({{$item->id}});"><i id = "deleteButton_{{$item->id}}" class="fa fa-trash" title="Delete"></i> </button>
                    @endif
                    @if($item->status_id==1 || $item->status_id==3) 
                    <button type="button" class="btn btn-sm btn-default" onclick="printClaimDetails({{$item->id}})" id="printClaimDetail{{ $item->id }}"printClaimDetails({{$item->id}})"><i  class="fa fa-print" id="printClaimDetailsSpin{{ $item->id }}" title="print"></i> </button>
                    @endif
                </td>

            </tr>

            @endforeach
            @else
            <tr>
                <td colspan="7" style="text-align: center">No Records found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
