@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<style>
    .print_label{
        font-weight: bold;
    }
    .print_labels{
        font-weight: bold;
    }
    @media print {
  .col-md-4 {
    max-width: 33.33333%; } }
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" value='<?= $route_data ?>' id="route_value">
<div class="right_col">
    <div class="container-fluid">


        <div class="col-md-12 padding_sm">
            <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
                <div class="box no-border no-margin">
                    <div class="box-body" style="padding-bottom:15px;">
                        <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                            <thead><tr class="table_header_bg">
                                    <th colspan="11">Filters

                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="row">

                            <div class="col-md-12">
                                {!! Form::open(['name' => 'search_from', 'id' => 'search_from']) !!}
                                <div>

                                    <?php
                                    $i = 1;

                                    foreach ($showSearchFields as $fieldType => $fieldName) {
                                        $fieldTypeArray[$i] = explode('-', $fieldType);

                                        if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                            $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                            ?>

                                            <div class= "col-md-2">
                                                <div class="mate-input-box">
                                                    <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                                    <input class="form-control hidden_search " value="" autocomplete="off" type="text" id="{{$fieldTypeArray[$i][1]}}" name="{{$fieldTypeArray[$i][1]}}" />
                                                    <div id="{{$fieldTypeArray[$i][1]}}AjaxDiv" class="ajaxSearchBox"></div>
                                                    <input class="filters" value="{{$hiddenFields[$hidden_filed_id]}}"  type="hidden" name="{{$fieldTypeArray[$i][1]}}_hidden" value="" id="{{$fieldTypeArray[$i][1]}}_hidden">
                                                </div>
                                            </div>

                                            <?php
                                        } else if ($fieldTypeArray[$i][0] == 'text') {
                                            ?>

                                            <div class="col-md-12">
                                                <label class="filter_label">{{ $fieldTypeArray[$i][2] }}</label>
                                                <input type="text" name="{{ $fieldTypeArray[$i][1] }}" <?php
                                                if (isset($fieldTypeArray[$i][3])) {
                                                    echo $fieldTypeArray[$i][3];
                                                }
                                                ?>
                                                       value="" class="form-control filters" id="{{ $fieldTypeArray[$i][1] }}"
                                                       data="{{ isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : '' }}"
                                                       <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?>>
                                            </div>

                                            <?php
                                        } else if ($fieldTypeArray[$i][0] == 'date') {
                                            ?>
                                            <div class="col-md-2 padding_sm date_filter_div">
                                                <div class="mate-input-box">
                                                    <label class="filter_label">{{ $fieldTypeArray[$i][2] }} From</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" data-attr="date" autocomplete="off"
                                                           name="{{ $fieldTypeArray[$i][1] }}_from" <?php
                                                           if (isset($fieldTypeArray[$i][3])) {
                                                               echo $fieldTypeArray[$i][3];
                                                           }
                                                           ?>
                                                           value="{{ date('M-d-Y') }}" class="form-control datepicker filters"
                                                           placeholder="YYYY-MM-DD" id="{{ $fieldTypeArray[$i][1] }}_from">
                                                </div>
                                            </div>
                                            <div class="col-md-2 padding_sm date_filter_div" style="width:15% !important">
                                                <div class="mate-input-box">
                                                    <label class="filter_label">{{ $fieldTypeArray[$i][2] }} To</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" data-attr="date" autocomplete="off"
                                                           name="{{ $fieldTypeArray[$i][1] }}_to" <?php
                                                           if (isset($fieldTypeArray[$i][3])) {
                                                               echo $fieldTypeArray[$i][3];
                                                           }
                                                           ?>
                                                           value="{{ date('M-d-Y') }}" class="form-control datepicker filters"
                                                           placeholder="YYYY-MM-DD" id="{{ $fieldTypeArray[$i][1] }}_to">

                                                </div>
                                            </div>
                                            <?php
                                        } else if ($fieldTypeArray[$i][0] == 'combo') {
                                            ?>
                                            <div class="col-md-2 date_filter_div">
                                                 <div class="mate-input-box">
                                                <label class="filter_label ">{{ $fieldTypeArray[$i][2] }}</label>
                                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control filters ', 'placeholder' => $fieldTypeArray[$i][2], 'id' => $fieldTypeArray[$i][1], 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                            </div>
                                            </div>
                                            <?php
                                        } else if ($fieldTypeArray[$i][0] == 'multi_combo') {
                                            ?>
                                            <div class="col-md-2 date_filter_div">
                                                <label style="width: 100%;"
                                                       class="filter_label ">{{ $fieldTypeArray[$i][2] }}</label>
                                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control filters multiple_selectbox', 'multiple' => 'multiple', 'placeholder' => $fieldTypeArray[$i][2], 'id' => $fieldTypeArray[$i][1], 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                            </div>
                                            <?php
                                        }

                                        $i++;
                                    }
                                    ?>
                                   <div class="col-md-3 padding_sm" style="margin-top:10px;">
                                            <div class="mate-input-box">
                                                <label class="filter_label ">Item List</label>
                                                <div class="clearfix"></div>
                                                <div id="itemsdiv">
                                                    <select multiple='multiple-select' class='form-control select2 filters'
                                                        name='item_list' id='item_list'>
                                                        <?php
                                                        foreach($item_list as $each){
                                                            ?>
                                                        <option value='<?= $each->service_desc ?>'><?= $each->service_desc ?>
                                                        </option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    <input type="hidden" value="0" id="summary_hid" name="summary_hid">
                                    <div class="col-md-12"
                                         style="text-align:right; padding: 0px; margin-top: 10px;">
                                        <div class="clearfix"></div>

                                        <button onclick="datarst();" type="reset" class="btn light_purple_bg"
                                                name="clear_results" id="clear_results">
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                            Reset
                                        </button>
                                        <a class="btn light_purple_bg disabled" onclick="generate_csv();"
                                           name="csv_results" id="csv_results">
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                            Excel
                                        </a>

                                        <a class="btn light_purple_bg disabled" onclick="printReportData();"
                                           name="print_results" id="print_results">
                                            <i class="fa fa-print" aria-hidden="true"></i>
                                            Print
                                        </a>

                                        <a class="btn light_purple_bg" onclick="getInsuranceReportData();"
                                           name="search_results" id="search_results">
                                            <i id="serachbtnspin" class="fa fa-search" aria-hidden="true"></i>
                                            Search
                                        </a>

                                    </div>
                                    {!! Form::token() !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-12 padding_sm">
            <div id="ResultDataContainer"
                 style="max-height: 650px; padding: 10px; display:none;font-family:poppinsregular">
                <div style="background:#686666;">
                    <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);
                          width: 100%;
                          padding: 30px;" id="ResultsViewArea">

                    </page>
                </div>
            </div>
        </div>
    </div>
</div>

<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                           value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                           value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal_insurance">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                <button type="button" class="close" onclick="modal_ins_hide()"aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height:120px;">
                <div class="col-md-12">
                    <span style="margin-right: 5%;">Print Mode :</span>
                    <input style="margin-right: 1%;" type="radio" name="printModeIns" checked="true" id="printMode"
                           value="1">Portrait
                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printModeIns" id="printMode"
                           value="2">Landscape
                </div>
                <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                    Include Hospital Header
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    <button onclick="print_generate_insuranceReportProcessData('append_ins_discount_modal')" class="btn bg-primary pull-right"
                            style="color:white">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="view_ins_discount_modal">
    <div class="modal-dialog" role="document" style="width: 90%">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgb(91 110 91);">
                <h4 class="modal-title" style="color: white;">Process Settlement</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="min-height:400px;" id="append_ins_discount_modal">
                                
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript_extra')controller

<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>

<script
src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js') }}"></script>
<script type="text/javascript">
                            $(document).ready(function () {
                                setTimeout(function () {
                                    //$(body).addClass('sidebar-collapse');
                                    $('.multiple_selectbox').multiselect();
                                    $("option:selected").prop("selected", false);
                                    $(".multiselect ").css("width", "100%");
                                }, 300);
                                $('.select2').select2();
                                $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });
                                var route_value = $('#route_value').val();
                                var decode_route = atob(route_value);
                                route_json = JSON.parse(decode_route);

                            });
                        function getInsuranceReportData() {
                            ret_item_qty = $('input[name="item_desc[]"]').val();
                            var ret_item_qty = $('input[name="item_desc[]"]').map(function(){ 
                                return this.value; 
                            }).get();

                            var url = route_json.insurancereportresults;
                            //-------filters---------------------------------------------
                            var filters_list = new Object();
                            var filters_value = '';
                            var filters_id = '';
                            $('.filters').each(function () {
                                filters_id = this.id;
                                if (filters_id == 'bill_no_hidden') {
                                    filters_id = 'bill_no';
                                }
                                filters_value = $('#' + filters_id).val();
                                filters_id = filters_id.replace('_hidden', '');

                                if (filters_id == 'bill_date_from') {
                                    filters_id = 'from_date';
                                }
                                if (filters_id == 'bill_date_to') {
                                    filters_id = 'to_date';
                                }
                                

                                if (filters_value !== "" && filters_value !== "0" && filters_value !== null) {
                                    filters_list[filters_id] = filters_value;
                                }
                            });
                            filters_list['item_desc'] = ret_item_qty;
                            $.ajax({
                                type: "GET",
                                url: url,
                                data: filters_list,
                                beforeSend: function () {
                                    $('#ResultsViewArea').LoadingOverlay("show", {background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869'});
                                },
                                success: function (html) {
                                    $('#ResultsViewArea').html(html);
                                    $('#print_results').removeClass('disabled');
                                    $('#csv_results').removeClass('disabled');
                                },
                                complete: function () {
                                    $('#ResultsViewArea').LoadingOverlay("hide");
                                    $('#ResultDataContainer').css('display', 'block');
                                    $('.theadscroll').perfectScrollbar({
                                        wheelPropagation: true,
                                        minScrollbarLength: 30
                                    });
                                },
                                error: function () {
                                    Command: toastr["error"]("Network Error!");
                                    $('#ResultDataContainer').css('display', 'hide');
                                    return;
                                }

                            });
                        }


//----Hidden Filed Search--------------------------------------------------------------------

$('.hidden_search').keyup(function (event) {
    var input_id = '';
    var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
    var value = event.key; //get the charcode and convert to char
    input_id = $(this).attr('id');
    //alert(input_id);return;
    var current;
    if (value.match(keycheck) || event.keyCode == '8') {
        if ($('#' + input_id + '_hidden').val() != "") {
            $('#' + input_id + '_hidden').val('');
        }
        var search_key = $(this).val();
        search_key = search_key.replace('/[^\w\s-_\.]/gi', '');
        search_key = search_key.trim();

        var department_hidden = $('#department_hidden').val();
        var datastring = '';
        if (input_id == 'sub_department') {
            datastring = '&department_id=' + department_hidden;
        }
        if (input_id == 'scheme') {
            datastring = '&company_id=' + company_hidden;
        }


        if (search_key == "") {
            $("#" + input_id + "AjaxDiv").html("");
        } else {
            var url = route_json.insurancereport;
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_key=' + search_key + '&search_key_id=' + input_id + datastring,
                beforeSend: function () {

                    $("#" + input_id + "AjaxDiv").html('<li style="width:300px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                },
               success: function (html) {
                    if (html == 0) {
                        $("#" + input_id + "AjaxDiv").html("No results found!").show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    } else {
                        $("#" + input_id + "AjaxDiv").html(html).show();
                        $("#" + input_id + "AjaxDiv").find('li').first().addClass('liHover');
                    }
                },
                complete: function () {
                    //  $('#loading_image').hide();
                },
                error: function () {
                    Command: toastr["error"]("Network Error!");
                    return;
                }

            });
        }
    } else {
        ajax_list_key_down(input_id + 'AjaxDiv', event);
    }
});
const txtarea=document.getElementById('TextAreaBtn');
function fillSearchDetials(id, name, serach_key_id) {
    $('#' + serach_key_id + '_hidden').val(id);
    if(serach_key_id != 'doctor_search'){
    $('#' + serach_key_id).val(name);
    }
    $('#' + serach_key_id).attr('title', name);
    $("#" + serach_key_id + "AjaxDiv").hide();
    if(serach_key_id=='doctor_search'){
        const txtarea=document.getElementById('TextAreaBtn');
    txtarea.innerHTML+=
   '<div><input type="text" name="item_desc[]" value="'+name+'" class="URLtxt"><button class="rmvbtn">-</button></div>';
    }

}
txtarea.onclick=ev=>ev.target.className==="rmvbtn"&&ev.target.parentNode.remove();
/* setting for enter key press in ajaxDiv listing */
$(".hidden_search").on('keydown', function (event) {
    var input_id = '';
    input_id = $(this).attr('id');
    if (event.keyCode === 13) {
        ajaxlistenter(input_id + 'AjaxDiv');
        return false;
    }
});





function print_generate_insuranceReportProcessData(printData) {
    //$('#print_config_modal').modal('hide');


     var showw = "";
     var printMode = 1;//$('input[name=printModeIns]:checked').val();
     var printhead = "";

     var mywindow = window.open('', 'my div', 'height=3508,width=2480');



    if($('#showTitle').is(":checked")){
        printhead  = $("#hospital_headder").val();
        showw = showw + atob(printhead);
    }

    //console.log(printhead);return;

    var msglist = document.getElementById(printData);
    showw = showw + msglist.innerHTML;

    if (printMode == 1) {
        mywindow.document.write('<style>@page{size:portrait;margin:0;margin-left:15px;text-align:center;}</style>');
    } else {
        mywindow.document.write('<style>@page{size:landscape;margin:0;marginleft:15px;text-align:center;}</style>');
    }
    mywindow.document.write('<style>.headerclass{text-align:left !important;background-color:#00ffe4 !important;color:black !important;font-size:12.5px !important}</style>');
    mywindow.document.write('<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>');
    mywindow.document.write('<style>.common_td_rules{text-align: left !important;overflow: hidden !important;border-right: solid 1px #bbd2bd !important;border-left: solid 1px #bbd2bd !important;max-width: 100px;text-overflow: ellipsis;white-space: nowrap;}</style>');
    mywindow.document.write('<style>.td_common_numeric_rules{border-left: solid 1px #bbd2bd !important;text-align: right !important;}</style>');
    mywindow.document.write('<style>.common_td_rules{text-align: left !important;border-right: solid 1px #bbd2bd !important;border-left: solid 1px #bbd2bd !important;}</style>');
    mywindow.document.write('<style>.generate_sum_btn{display:none !important}</style>');
    mywindow.document.write('<style>.print_labal_text{display:none !important}</style>');
    mywindow.document.write('<style>.hide_td{display:none !important}</style>');
    mywindow.document.write('<style>.print_labels{padding-right:70% !important}</style>');
        mywindow.document.write('<style>.print_label{padding-left:50% !important}</style>');
    mywindow.document.write('<style>.col-md-3{float: left;padding-bottom: 10px;padding-right: 10px;padding-left: 10px;width: 40.33333333%;}</style>');
    mywindow.document.write(showw);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print()
    mywindow.close();
    return true;
}




</script>
@endsection
