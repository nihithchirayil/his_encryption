<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px;padding-right: 20px">
             <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
             <p style="font-size: 12px;" id="total_data">Approved Insurance Bill Settlement:<b> <?= $from ?> To <?= $to ?></b></p>
            <?php
            $collect = collect($results);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Insurance Report</b></h4>
            <a class="btn light_purple_bg pull-right" onclick="exceller_ins();"
               name="print_process_results" id="print_process_results">
                <i class="fa fa-print" aria-hidden="true"></i>
                Excel
            </a>
            <a class="btn light_purple_bg pull-right" onclick="print_generate_insuranceReportProcessData('append_ins_discount_modal');"
               name="print_process_results" id="print_process_results">
                <i class="fa fa-print" aria-hidden="true"></i>
                Print
            </a>
            <table class='table table-condensed table_sm table-col-bordered'
                   style="font-size: 12px;padding-right: 50px !important;width: 100%">
                <tr>
                        <td colspan="12">
                            <?= base64_decode($hospital_headder) ?>
                        </td>
                    </tr>

            </table>
            <table id="result_data_table_exc" class='table table-condensed table_sm table-col-bordered'
                   style="font-size: 12px;padding-right: 50px !important;">
                <thead>

                    <tr>
                        <td colspan="6">
                            <table>
                                <tr  class="headerclass" style="background-color:rgb(105 154 159);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                    <th style="padding: 2px;width:15%;text-align: center;border:2px; ">Bill Number</th>
                                    <th style="padding: 2px;text-align: center;border:2px; ">Admitting Date</th>
                                    <th style="padding: 2px;text-align: center;border:2px; ">Discharge Date</th>
                                    <th style="padding: 2px;width:10%;text-align: center;border:2px; ">Patient Name</th>
                                    <th style="padding: 2px;width:8%;text-align: center;border:2px; ">UHID </th>
                                    <th style="padding: 2px;width:10%;text-align: center;border:2px; ">Doctor Name</th>
                                    <th style="padding: 2px;width:10%;text-align: center;border:2px; ">Insurance Company</th>
                                    <th style="padding: 2px;width:7%;text-align: center;border:2px; ">Patient Paid</th>
                                    <th style="padding: 2px;text-align: center;border:2px; ">Writeoff amount</th>
                                    <th style="padding: 2px;width:8%;text-align: center;border:2px; ">Profit Amount</th>
                                    <th style="padding: 2px;width:8%;text-align: center;border:2px; ">Tds Amount</th>
                                </tr>
                                <tr>
                                    <td style="padding:5px;width:15%;" class="common_td_ruless">{{$bill_no}}</td>
                                    <td style="padding:5px;" class="common_td_ruless">{{$admission_date}}</td>
                                    <td style="padding:5px;" class="common_td_ruless">{{$discharge_date}}</td>
                                    <td style="padding:5px;width:10%;" class="common_td_ruless">{{$pat}}</td>
                                    <td style="padding:5px;width:8%;" class="common_td_ruless">{{$uhid}}</td>
                                    <td style="padding:5px;width:10%;" class="common_td_ruless">{{@$results[0]->doctor ? $results[0]->doctor : ''}}</td>
                                    <td style="padding:5px;width:10%;" class="common_td_ruless">{{$company_name}}</td>
                                    <td style="padding:5px;width:7%;" class="td_common_numeric_ruless">{{@$results[0]->ins_patient_paid ? $results[0]->ins_patient_paid :'' }}</td>
                                    <td style="padding:5px;" class="td_common_numeric_ruless">{{@$results[0]->writeoff_amount ? $results[0]->writeoff_amount: ''}}</td>
                                    <td style="padding:5px;width:8%;" class="td_common_numeric_ruless">{{@$results[0]->r_profit_amount ? $results[0]->r_profit_amount : ''}}</td>
                                    <td style="padding:5px;width:8%;" class="td_common_numeric_ruless">{{@$results[0]->r_tds_amount ? $results[0]->r_tds_amount: ''}}</td>
                                </tr>
                            </table>
                        </td>

                    </tr>



                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th onclick="sortTable('result_data_table', 4, 1);" style="padding: 2px;width:20%;text-align: center;border:2px; ">Item</th>
                        <th onclick="sortTable('result_data_table', 3, 1);" style="padding: 2px;width:10%;text-align: center;border:2px; ">Quantity</th>
                        <th onclick="sortTable('result_data_table', 0, 2);" style="padding: 2px;width:20%;text-align: center;border:2px; ">Net amount</th>
                        <th onclick="sortTable('result_data_table', 1, 2);" style="padding: 2px;width:20%;text-align: center;border:2px; ">Discount Amount</th>
                        <th onclick="sortTable('result_data_table', 1, 2);" style="padding: 2px;width:20%;text-align: center;border:2px; ">Settled Amount</th>
                        <th class="hide_td"></th>
                    </tr>


                </thead>
                <tbody>
                    @php $total_net_amount = $total_settle_amount = $total_rec_amnt = $total_disc_amount = $total_qty = $total_ph = 0;@endphp
                    <?php if (count($results) != 0) { ?>
                        <?php
                        $i = 1;
                        foreach ($results as $list) {
                            ?>
                            <tr>
                                <td style="padding:5px;width:20%;" class="common_td_rules generate_desc">{{ $list->item_desc }}</td>
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules">{{ $list->quantity }}</td>
                                <td style="padding:5px;width:20%;" class="td_common_numeric_rules">{{ $list->net_amount }}</td>
                                <td style="padding:5px;width:20%;" class="td_common_numeric_rules">{{ $list->discount_amount }}</td>
                                <td style="padding:5px;width:20%;" class="td_common_numeric_rules">{{ $list->settled_amount }}</td>

                                @php $total_net_amount += $list->net_amount;
                                $total_qty += $list->quantity;
                                $total_disc_amount += $list->discount_amount;
                                $total_settle_amount += $list->settled_amount;
                                @endphp
                                @if($list->item_desc == 'PHARMACY')
                                @php $total_ph += $list->settled_amount; @endphp
                                @endif
                                <td class="hide_td"><input type="checkbox" onclick="generateCheckBoxSum(this)" data-id = "{{$list->cons_doctor_id}}" class="generate_sum" value="{{$list->settled_amount}}"></td>
                            </tr>
        <?php
        $i++;
    }
    ?>
                        <tr><td><b>Total</b></td><td class="td_common_numeric_rules"><b>{{$total_qty}}</b></td><td class="td_common_numeric_rules"><b>{{$total_net_amount}}</b></td>
                            <td class="td_common_numeric_rules"><b>{{$total_disc_amount}}</b></td><td class="td_common_numeric_rules"><b>{{$total_settle_amount}}</b></td></tr>
                        <?php } else { ?>
                        <tr><td>No Results</td></tr>
                    <?php }
                    ?>
                </tbody>
            </table>
            <div class="row" style="padding-top: 30px;border-top: 1px solid;">
            <form id='process_ins_gen'>
                <input type="hidden" id="div_id" value="1">
                 <button type="button" class="btn btn-sm btn-danger generate_sum_btn" onclick="generate_sum()">
                                <i id="save_fully_ins_payment_spin_btn" class="fa fa-thumbs-up">
                                </i>&nbsp;&nbsp;Next</button>
                <button type="button" class="btn btn-sm btn-success" onclick="generate_ins_report()">
                                <i id="" class="fa fa-reply">
                                </i>&nbsp;&nbsp;Report</button>
                    <div class="col-md-12" id="append_generated_sum">
                        @if($total_ph>0)
                        <div class="col-md-3" id="0" style="min-height: 125px;">
                            <label class="print_labels" style="width:60%">Pharmacy</label>
                            <label class="print_label">{{$total_ph}}</label>
                        </div>
                        @endif
                        <div class="col-md-3" id="1" style="min-height: 125px;">

                        </div>
                    </div>
                </div>
                </form>
        </div>
    </div>
</div>
<input type="hidden" id="ins_process_patient_uhid" value="{{$uhid}}">
<input type="hidden" id="ins_process_patient_company" value="{{$company_ids}}">
<input type="hidden" id="ins_process_patient_discharge_date" value="{{$discharge_date}}">
<input type="hidden" id="ins_process_patient_approve_date" value="{{$settled_date}}">