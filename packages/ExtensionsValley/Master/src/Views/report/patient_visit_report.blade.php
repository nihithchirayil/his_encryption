@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
<style>

.search_header{
    background: #36A693 !important;
    color: #FFF !important;
}
.ajaxSearchBox{
    z-index: 9999999;
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="user_id" value="{{$user_id}}">
<div class="right_col">
    <div class="col-md-12" style="margin:0px">
        <div class="container-fluid" style="padding: 15px;">
            <div class="row codfox_container">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-12 no-padding">
                        <div class="box no-border" >
                            <div class="box-header search_header">
                                <span class="padding_sm">{{$title}}</span>
                            </div>
                            <div class="box-body clearfix">

                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="filter_label">UHID</label>
                                        <div class="clearfix"></div>
                                        <input class="form-control hidden_search filters" value="" autocomplete="off" type="text" id="op_no" name="op_no" />
                                        <div id="op_noAjaxDiv" class="ajaxSearchBox"></div>
                                        <input class="filters" value="" type="hidden" name="op_no_hidden" id="op_no_hidden">
                                    </div>
                                </div>

                                <div class="col-md-1  padding_sm" >
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary getReport"><i class="fa fa-search"></i> Get Report</button>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ht10"></div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 box no-border no-margin " id="printData">


                    </div>


                </div>
            </div>

        </div>
    </div>
</div>


@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/patientvisitreport.js")}}"></script>


<script type="text/javascript">

</script>
@endsection
