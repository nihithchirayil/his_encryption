<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Insurance Report</b></h4>
            <table style="width:100%">
                <thead>
                    <tr>
                        <td colspan="6">
                            <?= base64_decode($hospital_headder) ?>
                        </td>
                    </tr>
                </thead>
            </table>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                   style="font-size: 12px;">
                <thead>

                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th onclick="sortTable('result_data_table', 4, 1);" style="padding: 2px;width:15%;text-align: center;border:2px; ">Item</th>
                        <th onclick="sortTable('result_data_table', 3, 1);" style="padding: 2px;width:15%;text-align: center;border:2px; ">Amount</th>
                        <th onclick="sortTable('result_data_table', 0, 2);" style="padding: 2px;width:15%;text-align: center;border:2px; ">Percentage</th>
                    </tr>


                </thead>
                <tbody>
                    @php $total_net_amount = $total_claim_amount = $total_rec_amnt = $perce=0;@endphp
                    <?php
                    if (count($res) != 0) {
                        $i = 1;
                        foreach ($res as $list) { ?>
                    <tr>
                                <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->item_desc }}</td>
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules">{{ $list->settled_amount }}</td>
                                @if($list->item_desc == 'PHARMACY')
                                @php $perce += (($list->settled_amount)*10)/100; @endphp
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><?= (($list->settled_amount)*10)/100; ; ?></td>
                                @elseif($list->item_desc == 'LAB')
                                @php $perce += (($list->settled_amount)*15)/100; @endphp
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><?= (($list->settled_amount)*15)/100 ; ?></td>
                                @elseif($list->item_desc == 'CT/MRI')
                                 @php $perce += (($list->settled_amount)*5)/100;@endphp
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><?= (($list->settled_amount)*5)/100 ; ?></td>
                                @else
                                @php $perce += (($list->settled_amount)*20)/100; @endphp
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><?= (($list->settled_amount)*20)/100 ; ?></td>
                                @endif
                                @php $total_net_amount += $list->settled_amount;@endphp
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                        <tr style="font-family:robotoregular">
                            <td><b>Total</b></td>
                            <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{$total_net_amount}}</b></td>
                            <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{$perce}}</b></td>
                        </tr>
                        <?php
                    } else {
                        ?>
                        <tr>
                            <td colspan="3" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
