<style>
    @media print {
        .theadscroll {max-height: none; overflow: visible;}
        .header_bg {
            background-color: #08898d;
            color: white;
        }
        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #f2fef5;
        }

        #resultDataTable{
            border: 1px solid #cedfe9 !important;
            border-collapse: collapse !important;
        }
        td{
            padding-left: 5px !important;
        }
        }
    .global-footer {
        position: fixed;
        width: 90.5%;
        bottom: 38px;
    }
</style>

<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Care Context Linking Report</b></h4>
            <div class="theadscroll" style="position:relative">
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
                    <thead>
                        <tr class="headerclass"
                        style="background-color:rgb(54 166 147);color:rgb(255, 255, 255);border-spacing: 0 1em;font-family:sans-serif">
                            <th width='2%'>Sn.No.</th>
                            <th width='7%'>Uhid</th>
                            <th width='7%'>Abha Id</th>
                            <th width='7%'>Phr Id</th>
                            <th width='13%'>Patient</th>
                            <th width='6%'>Dob</th>
                            <th width='4%'>Phone</th>
                            <th width='4%'>Gender</th>
                            <th width='5%'>Encounter Name</th>
                            <th width='5%'>Encounter Date Time</th>
                            <th width='3%'>Practitioner Id</th>
                            <th width='12%'>Practitioner Name</th>
                            <th width='5%'>Document Id</th>
                            <th width='7%'>Document Type</th>
                            <th width='8%'>Document Url</th>
                            <th width='5%'>Organization</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($res) != 0)
                            @php
                                $i=1;
                            @endphp
                            @foreach ($res as $data)
                            @php
                                $filename = '';
                                $html = '';
                                if($data->filename !=''){
                                    $filename = $data->filename;
                                    $base_url = URL::to('/').'/packages/extensionsvalley/emr/uploads/'.$data->pid.'/';
                                    $filename = $base_url.$filename;
                                    $base_filename = public_path('packages/extensionsvalley/emr/uploads/' . $data->pid . '/' . $data->filename);
                                    $html = "<a target='_black' href='".$filename."'>Click Here</a>"; 
                                    if ($data->file_extentions == 'jpg' || $data->file_extentions == 'jpeg' || $data->file_extentions == 'png' || $data->file_extentions == 'docx' || $data->file_extentions == 'pdf') {
                                        if (file_exists($base_filename)) {   
                                           $html = "<a class='document_data_save' style='cursor:pointer' patient_id='".$data->pid."' filename='".$data->filename."'>Click Here</a>";
                                        }
                                    }
                                }
                            @endphp
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{!!$data->uhid!!}</td>
                                    <td>{!!$data->abha_id!!}</td>
                                    <td>{!!$data->phr_id!!}</td>
                                    <td>{!!$data->patient_name!!}</td>
                                    <td>{!!date('M-d-Y',strtotime($data->dob))!!}</td>
                                    <td>{!!$data->phone!!}</td>
                                    <td>{!!$data->gender!!}</td>
                                    <td>{!!$data->encounter_name!!}</td>
                                    <td>{!!date('M-d-Y h:i A',strtotime($data->encounter_date_time))!!}</td>
                                    <td>{!!$data->doctor_id!!}</td>
                                    <td>{!!$data->doctor_name!!}</td>
                                    <td>{!!$data->document_id!!}</td>
                                    <td>{!!$data->document_type!!}</td>
                                    <td>{!!$html!!}</td>
                                    <td>{!!$data->organization!!}</td>
                                </tr>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        @else
                            <tr>
                                <td colspan="11" style="text-align: center">
                                    No Result Found
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>

        </div>
    </div>
