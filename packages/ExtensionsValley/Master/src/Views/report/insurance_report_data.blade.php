<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Insurance Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                   style="font-size: 12px;">
                <thead>
                    <tr>
                        <td colspan="11">
                            <?= base64_decode($hospital_headder) ?>
                        </td>
                    </tr>
                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th onclick="sortTable('result_data_table', 4, 1);" style="padding: 2px;width:15%;text-align: center;border:2px; ">Patient Name</th>
                        <th onclick="sortTable('result_data_table', 3, 1);" style="padding: 2px;width:10%;text-align: center;border:2px; ">UHID</th>
                        <th onclick="sortTable('result_data_table', 0, 2);" style="padding: 2px;width:15%;text-align: center;border:2px; ">Doctor Name</th>
                        <th onclick="sortTable('result_data_table', 1, 2);" style="padding: 2px;width:5%;text-align: center;border:2px; ">Approved Date</th>
                        <th onclick="sortTable('result_data_table', 1, 2);" style="padding: 2px;width:10%;text-align: center;border:2px; ">Insurance Company</th>
                        <th onclick="sortTable('result_data_table', 2, 1);" style="padding: 2px;width:10%;text-align: center;border:2px; ">Settled Amnt.</th>
                        <th onclick="sortTable('result_data_table', 2, 1);" style="padding: 2px;width:10%;text-align: center;border:2px; ">TDS Amnt.</th>
                        <th onclick="sortTable('result_data_table', 2, 1);" style="padding: 2px;width:10%;text-align: center;border:2px; ">Patient Paid</th>
                        <th onclick="sortTable('result_data_table', 2, 1);" style="padding: 2px;width:10%;text-align: center;border:2px; ">Total</th>
                        <th onclick="sortTable('result_data_table', 2, 1);" style="padding: 2px;width:10%;text-align: center;border:2px; ">Write Off</th>
                    </tr>


                </thead>
                <tbody>
                    @php $total_net_amount = $total_claim_amount = $total_rec_amnt = $total_tds_amnt = $total_patpaid_amnt = $totalamnt = $total_wrt_amnt = 0;@endphp
                    <?php
                    if (count($res) != 0) {
                        $i = 1;
                        foreach ($res as $list) {
                            if ($list->settled_status == 2) {
                                $settled = "Partially Settled";
                            } else if ($list->settled_status == 1) {
                                $settled = "Settled";
                            } else {
                                $settled = "Not Paid";
                            }
                            ?>
                    <tr onclick="getInsuranceDiscount('<?= $list->bill_no ?>','<?= $list->bill_tag?>','<?= $list->admission_date?>','<?= $list->discharge_date?>','<?= $list->pat?>','<?= $list->uhid?>','<?= $list->company_name?>','<?= $list->rec_amnt ?>','<?=  $list->company_id?>','<?= $list->settled_date ?>');"  style="cursor: pointer">
                                <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->pat }}</td>
                                <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->uhid }}</td>
                                <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->doctor_name }}</td>
                                <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->settled_date }}</td>
                                <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->company_name }}</td>
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules">{{ $list->settled_amount }}</td>
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules">{{ $list->tds_amt }}</td>
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules">{{ $list->patient_paid }}</td>
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules">{{ $list->tds_amt + $list->settled_amount }}</td>
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules">{{ $list->write_of_amount }}</td>
                                @php $total_net_amount += $list->settled_amount;
                                $total_tds_amnt += $list->tds_amt;
                                $total_patpaid_amnt += $list->patient_paid;
                                $totalamnt += ($list->settled_amount+$list->tds_amt);
                                $total_wrt_amnt += $list->write_of_amount; @endphp
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                        <tr style="font-family:robotoregular">
                            <td colspan="5"><b>Total</b></td>
                            <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{$total_net_amount}}</b></td>
                            <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{$total_tds_amnt}}</b></td>
                            <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{$total_patpaid_amnt}}</b></td>
                            <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{$totalamnt}}</b></td>
                            <td style="padding:5px;width:10%;" class="td_common_numeric_rules"><b>{{$total_wrt_amnt}}</b></td>
                        </tr>
                        <?php
                    } else {
                        ?>
                        <tr>
                            <td colspan="3" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
