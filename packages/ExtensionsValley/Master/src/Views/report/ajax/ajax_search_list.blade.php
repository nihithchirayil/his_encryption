<?php
if(sizeof($resultData)>0){
    if($input_id == 'op_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->patient_name))}}","{{$input_id}}")'>
                        {{$item->uhid}} | {{$item->patient_name}}
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
    if($input_id == 'bill_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->bill_no))}}","{{$input_id}}")'>
                        {{$item->bill_no}} | {{$item->bill_tag}} | {{$item->net_amount_wo_roundoff}}
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
    if($input_id == 'ip_no'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->admission_no))}}","{{$input_id}}")'>
                        {{$item->admission_no}} | {{$item->patient_name}} 
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
    if($input_id == 'item_code'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities($item->item_code)}}","{{ htmlentities($item->item_desc)}}","{{$input_id}}")'>
                        {{$item->item_desc}} 
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
    if($input_id == 'ledger_id'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px;" class="liHovernew"
                    onclick='fillSearchDetials("{{htmlentities($item->ledger_id)}}","{{$item->ledger_name}}","{{$input_id}}")'>
                        {{$item->ledger_name}} 
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
    if($input_id == 'ledger_group_id'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; " class="liHovernew"
                    onclick='fillSearchGroupDetials("{{htmlentities($item->ledger_id)}}","{{$item->ledger_name}}","{{$input_id}}","{{$item->balance_sheet_ident}}")'>
                        {{$item->ledger_name}} 
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
    if($input_id == 'doctor_search'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities($item->id)}}","{{$item->doctor_name}}","{{$input_id}}")'>
                        {{$item->doctor_name}} 
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
   
}