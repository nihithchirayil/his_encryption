<!-- <div class="theadscroll always-visible" style="position: relative; height: 450px;"> -->
    <table class="table table-bordered table-striped no-margin theadfix_wrapper table_sm no-border">
        <thead>
            <tr style="background-color: #36a693; color:white; ">
                <th style="width: 5%;">Sl.No.</th>
                <th>Patient Name</th>
                <th>UHID</th>
                <th>Phone</th>
                <th>Visit Date</th>
                <th>Doctor Name</th>
                <!-- <th>Paid Status</th> -->
                <th>Print</th>
            </tr>
        </thead>
        <tbody>
            @if(count($visit_data) > 0)
            @foreach($visit_data as $data)
                <tr>
                    {{--<td style="white-space:nowrap; ">{{($visit_data->currentPage() - 1) * $visit_data->perPage() + $loop->iteration}}</td>--}}
                    <td style="white-space:nowrap; ">{{$loop->iteration}}</td><td title="{{$data->patient_name}}">{{$data->patient_name}}</td>
                    <td title="{{$data->uhid}}">{{$data->uhid}}</td>
                    <td title="{{$data->phone}}">{{$data->phone}}</td>
                    <td title="{{date(\WebConf::getConfig('date_format_web'),strtotime($data->visit_date))}}" style="white-space:nowrap; ">{{date(\WebConf::getConfig('date_format_web'),strtotime($data->visit_date))}}</td>
                    <td title="{{$data->doctor_name}}">{{$data->doctor_name}}</td>
                    <!-- <td title="{{$data->paid_status}}">{{$data->paid_status}}</td> -->
                    <td><button class="btn btn-lg light_purple_bg" title="Print" onclick="printVisitReport('{{$data->patient_id}}','{{$data->visit_id}}')"><i class="fa fa-print"></i> Print</button></td>
                </tr>
            @endforeach
            @else
            <td colspan="6">No Data Found..!</td>
            @endif
        </tbody>
    </table>
<!-- </div> -->
{{-- @if(count($visit_data) > 0) --}}
{{-- <div id="pagination">{{{ $visit_data->links() }}}</div> --}}
{{-- @endif --}}