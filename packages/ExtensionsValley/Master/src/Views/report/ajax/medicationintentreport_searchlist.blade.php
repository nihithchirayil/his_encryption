
<?php
if($input_id == 'op_no'){
    if (!empty($resultData)) {
        foreach ($resultData as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->patient_name))}}","{{$input_id}}")'>
                    {{$item->uhid}} | {{$item->patient_name}}
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}
if($input_id == 'medicine'){
    if (!empty($resultData)) {
        foreach ($resultData as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->item_desc))}}","{{$input_id}}")'>
                    <?php echo htmlentities(ucfirst($item->item_desc)); ?>
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}
if($input_id == 'generic_name'){
    if (!empty($resultData)) {
        foreach ($resultData as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->generic_name))}}","{{$input_id}}")'>
                    <?php echo htmlentities(ucfirst($item->generic_name)); ?>
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}
if($input_id == 'location'){
    if (!empty($resultData)) {
        foreach ($resultData as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->location_name))}}","{{$input_id}}")'>
                    <?php echo htmlentities(ucfirst($item->location_name)); ?>
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}
if($input_id == 'location_from'){
    if (!empty($resultData)) {
        foreach ($resultData as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->location_name))}}","{{$input_id}}")'>
                    <?php echo htmlentities(ucfirst($item->location_name)); ?>
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}
