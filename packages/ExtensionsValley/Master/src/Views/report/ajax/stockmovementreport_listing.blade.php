<?php

if($input_id == 'doctor_name'){
    if (!empty($resultData)) {
        foreach ($resultData as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->doctor_name))}}","{{$input_id}}")'>
                     {{$item->doctor_name}}|{{$item->id}} 
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}

if($input_id == 'item_name'){
    if (!empty($resultData)) {
        foreach ($resultData as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->item_desc))}}","{{$input_id}}")'>
                     {{$item->item_desc}}|{{$item->id}} 
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}
