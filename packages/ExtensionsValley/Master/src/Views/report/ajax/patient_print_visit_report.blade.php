@php
use ExtensionsValley\Emr\FormBuilderList;
@endphp
<style>

.assesment_history_table{
    margin-top: 25px;
}

.assesment_history_form_name{
    margin-top: 50px;
}

.assesment_history_table > tbody > tr > td {
    font-size: 14px;
    border: 1px solid #ccc;
}


</style>
<table class="table table_sm no-margin no-border table-striped" style="width:100%">
    <thead>
        <tr>
            <td colspan="10" style="padding: 0px;">
                <table style="width: 100%; font-size: 12px;">
                    <tbody>
                        <tr>
                            <td style="margin-top: 50px;padding: 3px;border: 1px solid #FFF; text-align: center;" align="center" valign="top">
                                {!! $hospital_header !!}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" align="center" style="border-right: 1px solid #FFF; border-left: 1px solid #FFF; padding: 5px 0;">
                                <i class='report_date'></i><br>
                                <span>
                                    <strong> Patient Visit Summary </strong>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" align="center" style="border-right: 1px solid #FFF; border-left: 1px solid #FFF; padding: 5px 0;">
                                <i class='report_date'></i><br>
                                <span>
                                    <i> Visit Date : @if($patient_details['visit_date']) {{date(\WebConf::getConfig('date_format_web'), strtotime($patient_details['visit_date']))}} @endif</i>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </thead>
    <tbody style="color: #ccc;">
            <tr class="headerclass" style="border-spacing: 0 1em;font-family:sans-serif">
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="2" class="common_td_rules">Name</th>
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="3" class="common_td_rules">@if($patient_details['patient_name']) {{ $patient_details['patient_name'] }} @endif</th>
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="2" class="common_td_rules">UHID</th>
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="3" class="common_td_rules">@if($patient_details['uhid']) {{ $patient_details['uhid'] }} @endif</th>
            </tr>
            <tr class="headerclass" style="border-spacing: 0 1em;font-family:sans-serif">
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="2" class="common_td_rules">Address</th>
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="3" class="common_td_rules">@if($patient_details['address']) {{ $patient_details['address'] }} @endif @if($patient_details['area']) {{ $patient_details['area'] }} @endif @if($patient_details['state']) {{ $patient_details['state'] }} @endif @if($patient_details['country']) {{ $patient_details['country'] }} @endif @if($patient_details['pincode']) {{ $patient_details['pincode'] }} @endif</th>
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="2" class="common_td_rules">Phone</th>
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="3" class="common_td_rules">@if($patient_details['phone']) {{ $patient_details['phone'] }} @endif</th>
            </tr>
            <tr class="headerclass" style="border-spacing: 0 1em;font-family:sans-serif">
            <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="2" class="common_td_rules">Consulting Doctor</th>
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="3" class="common_td_rules">@if($patient_details['consulting_doctor_name']) {{ $patient_details['consulting_doctor_name'] }} @endif</th>
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="2" class="common_td_rules">Sex</th>
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="3" class="common_td_rules">@if($patient_details['gender']) {{ $patient_details['gender'] }} @endif</th>
            </tr>
            <tr class="headerclass" style="border-spacing: 0 1em;font-family:sans-serif">
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="2" class="common_td_rules">Age</th>
                <th style="border: 1px solid #ccc; font-size: 12px; color: #000; text-align:left;" colspan="8" class="common_td_rules">@if($patient_details['age']) {{ $patient_details['age'] }} @endif</th>
            </tr>
    </tbody>
</table>

@foreach($clincal_assessments as $assessments)
@php
$all_form_fields = FormBuilderList::getFormFields($assessments->form_id);
$already_entered = $assessments->data_text;
@endphp

<table style="border-collapse:collapse;border-color:#000;" class="assesment_history_form_name" width="100%" cellspacing="0" cellpadding="0">
    <tr> <td> <strong> {{$assessments->form_name}} </strong> </td> </tr>
</table>
<table class="table table_sm no-margin no-border table-striped assesment_history_table" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
        @if (!empty($all_form_fields))
            @if (sizeof($all_form_fields) > 0)
                @for ($i = 0; $i < sizeof($all_form_fields); $i++)
                    @php

                        $type_name = '';
                        $type = '';
                        $default_value = '';
                        $entered_value = '';
                        $field_value = '';
                        $is_required = '';
                        $field_name = '';
                        $field_label = '';
                        $progress_table = '';
                        $progress_value = '';
                        $progress_text = '';
                        $class_name = '';
                        $table_structure = '';
                        $table_details = '';
                        $field_width = '';
                        $field_height = '';
                        $form_id = '';
                        $form_field_id = '';

                        $type_name = !empty($all_form_fields[$i]->type_name) ? $all_form_fields[$i]->type_name : '';
                        $type = !empty($all_form_fields[$i]->type) ? $all_form_fields[$i]->type : '';
                        $type = !empty($all_form_fields[$i]->type) ? $all_form_fields[$i]->type : '';
                        $field_name = !empty($all_form_fields[$i]->field_name) ? $all_form_fields[$i]->field_name : '';
                        $field_label = !empty($all_form_fields[$i]->field_label) ? $all_form_fields[$i]->field_label : '';
                        //$default_value = (!empty($all_form_fields[$i]->default_value)) ? $all_form_fields[$i]->default_value : '';
                        $form_field_id = !empty($all_form_fields[$i]->form_field_id) ? $all_form_fields[$i]->form_field_id : '';
                        $form_id = !empty($all_form_fields[$i]->form_id) ? $all_form_fields[$i]->form_id : '';
                        //progress search
                        $progress_table = !empty($all_form_fields[$i]->progress_table) ? $all_form_fields[$i]->progress_table : '';
                        $progress_value = !empty($all_form_fields[$i]->progress_value) ? $all_form_fields[$i]->progress_value : '';
                        $progress_text = !empty($all_form_fields[$i]->progress_text) ? $all_form_fields[$i]->progress_text : '';
                        //table
                        $table_structure = !empty($all_form_fields[$i]->table_structure) ? $all_form_fields[$i]->table_structure : '';
                        $table_details = !empty($all_form_fields[$i]->table_details) ? $all_form_fields[$i]->table_details : '';

                        $radio = [];
                        $gcheckbox = [];
                        $selected_value = [];
                        $optionsListSelected = [];




                    @endphp


                    <tr>
                        @if ($type_name == 'TEXT')

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }
                            @endphp

                                @if ($field_value != '')
                                <td width="25%"><strong>{{ $field_label }}</strong></td>
                                <td>@if ($field_value != '') {!! $field_value !!} @endif</td>
                                @endif


                        @elseif($type_name == "TEXT AREA")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }
                            @endphp

                                @if ($field_value != '')
                                <td width="30%"><strong>{{ $field_label }}</strong></td>
                                <td>@if ($field_value != '') {!! nl2br($field_value) !!} @endif</td>
                                @endif

                        @elseif($type_name == "DATE")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }
                            @endphp

                                @if ($field_value != '')
                                <td width="30%"><strong>{{ $field_label }}</strong></td>
                                <td>@if ($field_value != '') {{ $field_value }} @endif</td>
                                @endif


                        @elseif($type_name == "TINYMCE")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }
                            @endphp

                                @if ($field_value != '')
                                <td width="30%"><strong>{{ $field_label }}</strong></td>
                                <td>@if ($field_value != ''){!! $field_value !!}@endif</td>
                                @endif


                        @elseif($type_name == "CERTIFICATE")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }
                            @endphp

                                @if ($field_value != '')
                                <td width="30%"><strong>{{ $field_label }}</strong></td>
                                <td>@if ($field_value != '') {!! $field_value !!} @endif</td>
                                @endif

                        @elseif($type_name == "CHECK BOX")

                            @php

                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';

                                $checked_box_default_value = '';

                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;

                                    //if check box checked then get default value as name
                                    $checked_box_default_value = !empty($all_form_fields[$i]->default_value) ? $all_form_fields[$i]->default_value : '';
                                } else {
                                    $field_value = $default_value;
                                }

                            @endphp

                                @if ($field_name == $field_value)
                                <td width="30%"><strong>{{ $field_label }}</strong></td>
                                <td>@if ($field_name == $field_value) {{ $checked_box_default_value }} @endif</td>
                                @endif

                        @elseif($type_name == "SELECT BOX")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }

                                $options = '';
                                if (!empty($field_value)):
                                    $options = ExtensionsValley\Emr\FormFieldSelectboxOptions::where('form_field_id', '=', $form_field_id)
                                        ->where('value', '=', $field_value)
                                        ->value('name');
                                endif;
                            @endphp

                                @if (!empty($options))
                                <td width="30%"><strong>{{ $field_label }}</strong></td>
                                <td>@if (!empty($options)) {!! $options !!} @endif</td>
                                @endif


                        @elseif($type_name == "RADIO BUTTON")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }
                            @endphp

                            @if (isset($form_field_id) && !empty($form_field_id))
                                @php
                                    $radio = '';
                                    if (!empty($field_value)):
                                        $radio = ExtensionsValley\Emr\FormFieldRadioOptions::where('form_field_id', '=', $form_field_id)
                                            ->where('value', '=', $field_value)
                                            ->value('name');
                                    endif;
                                @endphp
                            @endif

                                @if (!empty($radio))
                                <td width="30%"><strong>{{ $field_label }}</strong></td>
                                <td>@if (!empty($radio)) {!! $radio !!} @endif</td>
                                @endif


                        @elseif($type_name == "GROUPED CHECKBOX")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }
                            @endphp

                            @if (sizeof((array) $field_value) > 0)

                                @php
                                    $datArr = [];
                                    $datArr = (array) $field_value;
                                @endphp

                                    @if ($field_value != '')
                                    <td width="30%"><strong>{{ $field_label }}</strong></td>
                                    <td>
                                        @if ($field_value != '')
                                        @foreach ($datArr as $ind => $data)
                                            {!! $data !!} <br>
                                        @endforeach
                                        @endif
                                    </td>
                                    @endif

                            @endif

                        @elseif($type_name == "HEADER TEXT")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';

                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }
                            @endphp

                            @if (!empty($default_value))
                            <td colspan="2"><b>{!! $default_value !!}</b></td>
                            @endif

                        @elseif($type_name == "PROGRESSIVE SEARCH")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }
                            @endphp

                            @if (sizeof((array) $field_value) > 0)

                                @php
                                    $datProArr = [];
                                    $datProArr = (array) $field_value;
                                @endphp

                                @if ($field_value != '')
                                    <td width="30%"><strong>{{ $field_label }}</strong></td>
                                    <td>
                                        @foreach ($datProArr as $ind => $pro_data)
                                            {!! $pro_data !!} <br>
                                        @endforeach
                                    </td>
                                @endif

                            @endif

                        @elseif($type_name == "TABLE")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }

                                $tableDatArr = [];
                                $tableDatArr = (array) $field_value;

                                $Tdetails = json_decode($table_details);
                                $table_details = $Tdetails->$field_name;

                                $rowHeader = isset($table_details->rowHeader) ? $table_details->rowHeader : 0;
                                $colHeader = isset($table_details->colHeader) ? $table_details->colHeader : 0;

                                $Tdata = [];
                                $TdataCount = 0;
                                $fixed_headers = 0;
                                $fixed_headers = $rowHeader + $colHeader;

                                if (count($tableDatArr) > 0) {
                                    $Tdata = array_filter($tableDatArr);
                                    if (is_array($Tdata) && count($Tdata) > 0) {
                                        $TdataCount = sizeof($Tdata);
                                    }
                                }
                            @endphp

                            @if (isset($tableDatArr))
                                @if (sizeof($tableDatArr) > 0 && $TdataCount > $fixed_headers)
                                    <td colspan="2" class="no-padding">
                                        <table width="99%"
                                            class="table tabe_sm no-margin table-bordered table-striped">
                                            @foreach ($tableDatArr as $rows => $colsArr)
                                                <tr>
                                                    @foreach ($colsArr as $colsind => $cols)
                                                        <td>{{ $cols }}</td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        </table>
                                    </td>
                                @endif
                            @endif


                        @elseif($type_name == "CUSTOM DATA")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    $field_value = $default_value;
                                }
                            @endphp

                                @if ($field_value != '')
                                <td width="30%"><strong>{{ $field_label }}</strong></td>
                                <td>@if ($field_value != '') {!! $field_value !!} @endif</td>
                                @endif


                        @endif
                    </tr>

                @endfor
            @endif
        @endif
    </tbody>
</table>

@endforeach


@if(count($investigation_details) > 0)
<table style="border-collapse:collapse;border-color:#000;" class="assesment_history_form_name" width="100%" cellspacing="0" cellpadding="0" style="margin-top:40px; ">
    <tr> <td><h4> INVESTIGATION DETAILS </h4> </td> </tr>
</table>

<table class="table table-bordered table-striped no-margin theadfix_wrapper table_sm no-border" style="margin-top:10px; " width="100%">
    <thead>
        <tr style="background-color: #ececec;">
            <th width="15%" style="text-align:left; white-space:nowrap;"><strong> Date </strong></th>
            <th style="text-align:left; white-space:nowrap;"><strong> Test </strong></th>
            <th style="text-align:left; white-space:nowrap;"><strong> Type </strong></th>
            <!-- <th style="text-align:left; white-space:nowrap;"><strong> Doctor </strong></th> -->
            <th style="text-align:left; white-space:nowrap;"><strong> Remark </strong></th>
            <th style="text-align:left; white-space:nowrap;"><strong> Quantity </strong></th>
            <!-- <th style="text-align:left; white-space:nowrap;"><strong> Status </strong></th> -->
        </tr>
    </thead>
    <tbody>

        @foreach($investigation_details as $data)
            <tr>
                <td width="15%" title="@if($data->created_at){{date(\WebConf::getConfig('datetime_format_web'), strtotime($data->created_at))}} @endif">@if($data->created_at) {{date(\WebConf::getConfig('datetime_format_web'), strtotime($data->created_at))}} @endif</td>
                <td title="{{$data->service_desc}}">{{$data->service_desc}}</td>
                <td title="{{$data->investigation_type}}">{{$data->investigation_type}}</td>
                <!-- <td title="{{$data->doctor_name}}">{{$data->doctor_name}}</td> -->
                <td title="{{$data->remarks}}">{{$data->remarks}}</td>
                <td title="{{$data->quantity}}">{{$data->quantity}}</td>
                <!-- <td title="{{$data->bill_converted_status}}">{{$data->bill_converted_status}}</td> -->
            </tr>
        @endforeach

    </tbody>
</table>
@endif



@if(count($lab_details) > 0)
<table style="border-collapse:collapse;border-color:#000;" class="assesment_history_form_name" width="100%" cellspacing="0" cellpadding="0" style="margin-top:40px; ">
    <tr> <td><h4> LAB DETAILS </h4> </td> </tr>
</table>

<table class="table table-bordered table-striped no-margin theadfix_wrapper table_sm no-border" style="margin-top:10px; " width="100%">
    <thead>
        <tr style="background-color: #ececec;">
            <th style="text-align:left; white-space:nowrap;"><strong> Date </strong></th>
            <th style="text-align:left; white-space:nowrap;"><strong> Test </strong></th>
            <th style="text-align:left; white-space:nowrap;"><strong> Description </strong></th>
            <th style="text-align:left; white-space:nowrap;"><strong> Value </strong></th>
            <th style="text-align:left; white-space:nowrap;"><strong> Normal Range </strong></th>
            <th style="text-align:left; white-space:nowrap;"><strong> Remark </strong></th>
            <th style="text-align:left; white-space:nowrap;"><strong> Result Status </strong></th>
        </tr>
    </thead>
    <tbody>

        @foreach($lab_details as $data)
            <tr>
                <td title="{{$data['created_at']}}">{{$data['created_at']}}</td>
                <td title="{{$data['service_desc']}}">{{$data['service_desc']}}</td>
                <td title="{{$data['sub_service_desc']}}">{{$data['sub_service_desc']}}</td>
                @if($data['report_type'] == 'T')
                <td title="{!! $data['result'] !!}">{!! $data['result'] !!}</td>
                @else
                <td title="{{$data['result']}}">{{$data['result']}}</td>
                @endif
                <td title="{{$data['normal_range']}}">{{$data['normal_range']}}</td>
                <td title="{{$data['remark']}}">{{$data['remark']}}</td>
                <td title="{{$data['lab_result_status']}}">{{$data['lab_result_status']}}</td>
            </tr>
        @endforeach

    </tbody>
</table>
@endif


@if(count($radiology_details) > 0)
<table style="border-collapse:collapse;border-color:#000;" class="assesment_history_form_name" width="100%" cellspacing="0" cellpadding="0" style="margin-top:40px; ">
    <tr> <td><h4> RADIOLOGY DETAILS </h4> </td> </tr>
</table>

<table class="table table-bordered table-striped no-margin theadfix_wrapper table_sm no-border" style="margin-top:10px; " width="100%">
    <thead>
        <tr style="background-color: #ececec;">
            <th width="25%" style="text-align:left;"><strong> Date </strong></th>
            <th width="30%" style="text-align:left;"><strong> Service Name </strong></th>
            <th width="30%" style="text-align:left;"><strong> Doctor Name </strong></th>
            <th width="15%" style="text-align:left;"><strong> Billed Status </strong></th>
        </tr>
    </thead>
    <tbody>

        @foreach($radiology_details as $data)
            <tr>
                <td title="{{$data->created_at}}">{{$data->created_at}}</td>
                <td title="{{$data->service_desc}}">{{$data->service_desc}}</td>
                <td title="{{$data->doctor_name}}">{{$data->doctor_name}}</td>
                <td title="{{$data->bill_converted_status}}">{{$data->bill_converted_status}}</td>
            </tr>
        @endforeach

    </tbody>
</table>
@endif

@if(count($prescription_details) > 0)
<table style="border-collapse:collapse;border-color:#000;" class="assesment_history_form_name" width="100%" cellspacing="0" cellpadding="0" style="margin-top:40px; ">
    <tr> <td><h4> PRESCRIPTION DETAILS  </h4> </td> </tr>
</table>

<table class="table table-bordered table-striped no-margin theadfix_wrapper table_sm no-border" style="margin-top:10px; ">
    <thead>
        <tr style="background-color: #ececec;">
            <th width="30%" style="text-align:left;"><strong> Medicine Name </strong> </th>
            <th width="10%" style="text-align:left;"><strong> Dose </strong> </th>
            <th width="10%" style="text-align:left;"><strong> Frequency </strong> </th>
            <th width="10%" style="text-align:left;"><strong> Duration </strong> </th>
            <th width="10%" style="text-align:left;"><strong> Quantity </strong> </th>
            <th width="10%" style="text-align:left;"><strong> Route </strong> </th>
            <th width="20%" style="text-align:left;"><strong> Notes </strong> </th>
        </tr>
    </thead>
    <tbody>
        @foreach($prescription_details as $data)
            <tr>
                <td title="{{$data->item_desc}}">{{$data->item_desc}}</td>
                <td title="{{$data->dose}}">{{$data->dose}}</td>
                <td title="{{$data->frequency}}">{{$data->frequency}}</td>
                <td title="{{$data->duration}}">{{$data->duration}}</td>
                <td title="{{$data->quantity}}">{{$data->quantity}}</td>
                <td title="{{$data->route}}">{{$data->route}}</td>
                <td title="{{$data->notes}}">{{$data->notes}}</td>
            </tr>
        @endforeach

    </tbody>
</table>
@endif

@if(count($vital_details) > 0)
<table style="border-collapse:collapse;border-color:#000;" class="assesment_history_form_name" width="100%" cellspacing="0" cellpadding="0" style="margin-top:40px; ">
    <tr> <td><h4> PATIENT VITAL DETAILS  </h4> </td> </tr>
</table>

<table class="table table-bordered table-striped no-margin theadfix_wrapper table_sm no-border" style="margin-top:10px; ">
    <thead>
        <tr style="background-color: #ececec;">
            <th width="20%" style="text-align:left;"><strong>Date </strong></th>
            <th width="10%" style="text-align:left;"><strong>Weight </strong></th>
            <th width="10%" style="text-align:left;"><strong>Height </strong></th>
            <th width="10%" style="text-align:left;"><strong>BP </strong></th>
            <th width="10%" style="text-align:left;"><strong>Pulse </strong></th>
            <th width="10%" style="text-align:left;"><strong>Respiration </strong></th>
            <th width="10%" style="text-align:left;"><strong>Temperature </strong></th>
            <th width="10%" style="text-align:left;"><strong>Oxygen </strong></th>
            <th width="10%" style="text-align:left;"><strong>BMI </strong></th>
        </tr>
    </thead>
    <tbody>
        @foreach($vital_details as $data)
            <tr>
                <td title="@if($data->time_taken){{ date(\WebConf::getConfig('datetime_format_web'), strtotime($data->time_taken)) }} @endif">@if($data->time_taken) {{ date(\WebConf::getConfig('datetime_format_web'), strtotime($data->time_taken)) }} @endif</td>
                <td title="@if($data->weight) {{$data->weight}} @endif ">@if($data->weight) {{$data->weight}} @endif </td>
                <td title="@if($data->height) {{$data->height}} @endif ">@if($data->height) {{$data->height}} @endif </td>
                <td title="@if($data->bp_systolic) {{$data->bp_systolic}} @endif  @if($data->bp_diastolic) /{{$data->bp_diastolic}} @endif ">@if($data->bp_systolic) {{$data->bp_systolic}} @endif @if($data->bp_diastolic) /{{$data->bp_diastolic}} @endif </td>
                <td title="@if($data->pulse) {{$data->pulse}} @endif ">@if($data->pulse) {{$data->pulse}} @endif </td>
                <td title="@if($data->respiration) {{$data->respiration}} @endif ">@if($data->respiration) {{$data->respiration}} @endif </td>
                <td title="@if($data->temperature) {{$data->temperature}} @endif ">@if($data->temperature) {{$data->temperature}} @endif </td>
                <td title="@if($data->oxygen) {{$data->oxygen}} @endif ">@if($data->oxygen) {{$data->oxygen}} @endif </td>
                <td title="@if($data->bmi) {{$data->bmi}} @endif ">@if($data->bmi) {{$data->bmi}} @endif </td>
            </tr>
        @endforeach

    </tbody>
</table>
@endif

<div style="float:right; margin-top:75px;">
@if($patient_details['consulting_doctor_name']) {{ $patient_details['consulting_doctor_name'] }} @endif
<br/>
@if($patient_details['consulting_doctor_name']) ( {{ $patient_details['speciality'] }} ) @endif
</div>
