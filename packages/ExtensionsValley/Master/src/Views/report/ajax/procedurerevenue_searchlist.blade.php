<?php
if($input_id == 'patient_name'){
    if (!empty($resultData)) {
        foreach ($resultData as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities(ucfirst($item->uhid))}}","{{ htmlentities(ucfirst($item->patient_name))}}","{{$input_id}}")'>
                     {{$item->patient_name}}|{{$item->uhid}} 
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}
if($input_id == 'doctor_name'){
    if (!empty($resultData)) {
        foreach ($resultData as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->doctor_name))}}","{{$input_id}}")'>
                     {{$item->doctor_name}}|{{$item->id}} 
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}