<div class="row">
    <?php if($report_type==1){?>
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Insurance Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                   style="font-size: 12px;">
                <thead>
                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th style="padding: 2px;width:15%;text-align: center;border:2px; ">Doctor Name</th>
                        <th style="padding: 2px;width:5%;text-align: center;border:2px; ">Amount</th>
                    </tr>


                </thead>
                <tbody>
                    @php $total_net_amount = $total_claim_amount = $total_rec_amnt = $total_tds_amnt = $total_patpaid_amnt = $totalamnt = $total_wrt_amnt = 0;@endphp
                    <?php
                    if (count($res) != 0) {
                        $i = 1;
                        $ttl_sum_amnt = 0;
                        foreach ($res as $list) {
                            ?>
                    <tr style="cursor: pointer">
                                <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->doctor_name }}</td>
                                <td style="padding:5px;width:10%;" class="td_common_numeric_rules">{{ $list->amount }}</td>
                                 
                            <?php
                            $i++; 
                        $ttl_sum_amnt += $list->amount;
                        }
                        ?>
                        <tr><td class="common_td_rules"><b>Total</b></td><td class="td_common_numeric_rules"><b>{{$ttl_sum_amnt}}</b></td></tr>
                        <?php
                    } else {
                        ?>
                        <tr>
                            <td colspan="3" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
            </table>
        </div>
        <?php }else{ ?>
        <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Insurance Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                   style="font-size: 12px;">
                <thead>
                    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th style="padding: 2px;width:15%;text-align: center;border:2px; ">UHID</th>
                        <th style="padding: 2px;width:15%;text-align: center;border:2px; ">Patient Name</th>
                        <th style="padding: 2px;width:15%;text-align: center;border:2px; ">Doctor Name</th>
                        <th style="padding: 2px;width:15%;text-align: center;border:2px; ">Company</th>
                        <th style="padding: 2px;width:15%;text-align: center;border:2px; ">Doscharge Date</th>
                        <th style="padding: 2px;width:5%;text-align: center;border:2px; ">Amount</th>
                    </tr>


                </thead>
                <tbody>
                    @php $total_net_amount = $total_claim_amount = $total_rec_amnt = $total_tds_amnt = $total_patpaid_amnt = $totalamnt = $total_wrt_amnt = 0;@endphp
                    <?php
                    if (count($res2) != 0) {
                        $i = 1;
                        foreach ($res2 as $list) {
                            ?>
                    <tr style="cursor: pointer">
                    <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->ins_process_patient_uhid }}</td>
                    <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->patient_name }}</td>
                    <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->doctor_name }}</td>
                    <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->company_name }}</td>
                    <td style="padding:5px;width:10%;" class="common_td_rules">{{ $list->dod }}</td>
                    <td style="padding:5px;width:10%;" class="td_common_numeric_rules">{{ $list->amount }}</td>
                                 
                            <?php
                            $i++;
                            $total_net_amount +=  $list->amount;
                        }
                        ?>
                        <tr><td colspan=5 class="common_td_rules"><b>Total</b></td><td class="td_common_numeric_rules"><b>{{$total_net_amount}}</b></td></tr>
                        <?php
                    } else {
                        ?>
                        <tr>
                            <td colspan="3" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
            </table>
        </div>
        <?php } ?>
    </div>
