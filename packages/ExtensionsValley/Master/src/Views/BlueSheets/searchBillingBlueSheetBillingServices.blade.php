                       <div class="col-md-12 padding_sm text-center text-blue" style="padding-top: 10px;"><strong>Total
                               Records : <?= count($bluesheet) ?></strong>
                               <div style="width:20%;height:20%;background-color:#adf1ad;display:inline-block">Paid</div>
                               <div style="width:25%;height:20%;background-color:#f1c0c0;display:inline-block">Bill Generated</div>
                            </div>
                       <div class="theadscroll" style="position: relative; height: 557px;">
                           <table class="table theadfix_wrapper table-striped table_sm table-condensed styled-table"
                               style="border: 1px solid #CCC;">
                               <thead>
                                   <tr class="table_header_bg" style="cursor: pointer;">
                                       <th width="30%" style="text-align: left;">Procedure Name</th>
                                       <th width="20%" style="text-align: left;">Doctor</th>
                                       <th width="15%" style="text-align: left;">Time</th>
                                       <th width="10%"><i class="fa fa-repeat"></i></th>
                                       <th width="10%"><i class="fa fa-trash-o"></i></th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <?php
                           if(count($bluesheet)!=0){
                            foreach($bluesheet as $each){
                                $disabled_status = '';
                                $back_ground_color='';
                                if ($doctor_id == '0' ) {
                                    $disabled_status = 'disabled';
                                }
                                if($each->billed_status == 1){
                                $back_ground_color='#f1c0c0'; 
                                }
                                if($each->paid_status == 1){
                                $back_ground_color='#adf1ad';
                                $disabled_status = 'disabled';
                                }
                                $created_at=date('H:i',strtotime($each->service_datetime));
                                $service_datetime=date('M-d-Y',strtotime($each->service_datetime));
                                ?>
                                   <tr id="billingyellowsheetrow<?= $each->id ?>" style="background-color:{{$back_ground_color}}">
                                       <td style="text-align: left;"><?= $each->service_desc ?></td>
                                       <td style="text-align: left;">
                                           <select <?= $disabled_status ?>
                                               onchange="updateBlueSheetBillingServices(<?= $each->id ?>)"
                                               class="form-control" id="yellowsheetdoctor_select<?= $each->id ?>">
                                               <option value=" ">Select Doctor</option>
                                               <?php
                                        foreach($doctor_data as $key=>$val){
                                            $checked_staus='';
                                            if($each->doctor_id==$key){
                                                $checked_staus="selected=''";
                                            }
                                           ?>
                                               <option <?= $checked_staus ?> value="<?= $key ?>"><?= $val ?></option>
                                               <?php
                                        }
                                    ?>
                                           </select>

                                       </td>
                                       <td>
                                           <input onchange="updateBlueSheetBillingServices(<?= $each->id ?>)"
                                               type="time" value="<?= $created_at ?>" <?= $disabled_status ?>
                                               name="yellowsheettime_picker" id="yellowsheettime_picker<?= $each->id ?>"
                                               class="form-control timepickerclass_default timepickerclass">
                                           <label for="yellowsheettime_picker<?= $each->id ?>"></label>
                                       </td>
                                       <td><button class="btn btn-sm btn-info" <?= $disabled_status ?>
                                               id="repeatBillingServicesbtn<?= $each->id ?>"
                                               onclick="repeatBlueSheetBillingServices(<?= $each->id ?>)"
                                               type="button">Repeat <i id="repeatBillingServicesspin<?= $each->id ?>"
                                                   class="fa fa-repeat"></i></button></td>
                                       <td><button class="btn btn-sm btn-danger" <?= $disabled_status ?>
                                               id="removeBillingServicesbtn<?= $each->id ?>"
                                               onclick="removeBlueSheetBillingServices(<?= $each->id ?>,<?= $each->blue_sheet_id ?>)"
                                               type="button">Remove <i id="removeBillingServicesspin<?= $each->id ?>"
                                                   class="fa fa-trash-o"></i></button></td>
                                   </tr>
                                   <?php
                            }
                        }else{
                            ?>
                                   <tr>
                                       <td colspan="6" style="text-align: center;"> No Result Found</td>
                                   </tr>
                                   <?php
                        }
                            ?>

                               </tbody>
                           </table>
                       </div>