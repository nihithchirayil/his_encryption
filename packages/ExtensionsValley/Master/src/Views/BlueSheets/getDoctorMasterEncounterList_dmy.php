<?php

if (count($doctor_data) != 0) {
    foreach ($doctor_data as $each) {
        $disabled_status = '';
        if ($patient_id == '0' || $billed_status != '0') {
            $disabled_status = 'disabled';
        }
        echo "<tr>";
        if ($from_type == '1' || count($each['service_details']) != 0) {
?>
            <td style="text-align: left;"><?= $each['doc_name'] ?></td>
            <?php

            for ($i = 1; $i <= 5; $i++) {
                $checked_status = '';
                $service_id = 0;
                if (isset($each['service_details']['service_id'])) {
                    if (in_array($i, $each['service_details']['service_doneon'])) {
                        if (isset($each['service_details']['service_id'][$i])) {
                            $service_id = $each['service_details']['service_id'][$i];
                            $checked_status = "checked=''";
                        }
                    }
                    if ($each['service_details']['is_includeindischarge']=='1') {
                        $disabled_status = 'disabled';
                    }
                }

            ?>
                <td><input <?= $disabled_status ?> <?= $checked_status ?> id="check_doctorservice<?= $each['id'] . $from_type . $i ?>" class="styled fav_group_check_box" type="checkbox" onclick="check_doctorservice(<?= $each['id'] ?>,<?= $from_type ?>,<?= $i ?>,<?= $service_id ?>)"></td>
    <?php
            }
        }
        echo "</tr>";
    }
} else {
    ?>
    <tr>
        <td colspan="6" style="text-align: center;"> No Result Found</td>
    </tr>
<?php
}
