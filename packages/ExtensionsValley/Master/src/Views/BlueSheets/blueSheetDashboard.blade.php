@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')
@php
@endphp
<div class="right_col">
    <div class="row pull-right" style="font-size: 12px; font-weight: bold;"> {{ $title }}</div>
    <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <div class="row codfox_container">
        <div class="col-md-12 paddimg_sm">
            <div class="box no-border no-margin">
                <div class="box-footer"
                    style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 65px;">
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Patient Name</label>
                            <div class="clearfix"></div>
                            <input type="text" value="" autocomplete="off" id="patient_name" class="form-control"
                                disabled>
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Doctor</label>
                            <div class="clearfix"></div>
                            <select
                                class="form-control select2" id="doctor_id">
                                <option value=" ">Select Doctor</option>
                                <?php
                         foreach($doctor_data as $key=>$val){  ?>
                                <option  value="<?= $key ?>"><?= $val ?></option>
                                <?php
                         }
                     ?>
                            </select>
                            <input type="hidden" value="" autocomplete="off" id="doctor_name" class="form-control"
                                disabled>
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Date</label>
                            <div class="clearfix"></div>
                            <input type="text" value="" autocomplete="off" id="visit_date" class="form-control"
                                disabled>
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm" style="top:15px">
                        <button onclick="saveAll();" class="btn btn-block btn-success" name="save_bluesheetData"
                            id="save_bluesheetData">
                            <i class="fa fa-save" aria-hidden="true"></i>
                            Save
                        </button>
                    </div>
                    <div class="col-md-1 padding_sm" style="top:15px">
                        <button onclick="datarst();" type="reset" class="btn btn-block btn-warning" name="clear_results"
                            id="clear_results">
                            <i class="fa fa-refresh" aria-hidden="true"></i>
                            Reset
                        </button>
                    </div>
                </div>
                <input type="hidden" value="" autocomplete="off" id="patient_id">
                <input type="hidden" value="" autocomplete="off" id="doctor_idss">
                <input type="hidden" value="" autocomplete="off" id="op_id">
                <input type="hidden" value="" autocomplete="off" id="visit_id">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="h10"></div>
        <div class="col-md-12 paddimg_sm" id="searchProcedureSearchDiv">

            <div class="col-md-4" style="width: 35.333333%; !important">
                <input type="text" id="patient_name_search_tble" class="form-control" style="width:44%;display:inline" placeholder="Search Patient / Doctor">
                <input type="text" id="patient_date_search_tble" onblur="selectPatientData(this)"
                    value="<?= $service_date ?>" class="form-control datepicker" style="width:44%;display:inline">
                <div class="theadscroll" style="position: relative; height: 557px;top-20px !important">
                    <table class="table theadfix_wrapper table-striped table_sm table-condensed styled-table"
                        style="border: 1px solid #CCC;">
                        <thead>
                            <tr class="table_header_bg" style="cursor: pointer;">
                                <th width="20%" style="text-align: left;">Uhid</th>
                                <th width="30%" style="text-align: left;">Patient Name</th>
                                <th width="20%" style="text-align: left;">Doctor</th>
                                <th width="15%" style="text-align: left;">Date</th>
                            </tr>
                        </thead>
                        <tbody id="blueSheetServicesPatientList">
                            @foreach($bluesheet as $bs)
                            <tr style="cursor:pointer" onclick="selectPatientBlueSheet(this,'{{$bs->patient_name}}','{{$bs->doctor_name}}',
                                    '{{$bs->visit_date}}','{{$bs->patient_id}}','{{$bs->doctor_id}}','{{$bs->visit_id}}'
                                    ,'{{$bs->visit_id}}')">
                                <td class="common_td_rules" title="{{$bs->uhid}}">{{$bs->uhid}}</td>
                                <td class="common_td_rules" title="{{$bs->patient_name}}">{{$bs->patient_name}}</td>
                                <td class="common_td_rules" title="{{$bs->doctor_name}}">{{$bs->doctor_name}}</td>
                                <td class="common_td_rules" title="{{$bs->visit_date}}">{{$bs->visit_date}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-4" style="width: 31.333333% !important">
                <input type="text" id="procedure_name_search_tble" class="form-control" placeholder="Procedure search">
                <div class="theadscroll always-visible" style="position: relative; height: 525px;">
                    <table class="table table_sm theadfix_wrapper table-striped table-condensed styled-table"
                        style="border: 1px solid #CCC;width:80% !important">
                        <thead>
                            <tr class="table_header_bg" style="cursor: pointer;">
                                <th style="text-align: left;">Procedure Name</th>
                                <th><i class="fa fa-plus"></i></th>
                            </tr>
                        </thead>
                        <tbody id="blueSheetServicesDiv">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-4" id="billingblueSheetServicesDiv"></div>

        </div>
    </div>
</div>

@stop

@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/blueSheetServiceDashboard.js') }}"></script>
@endsection
