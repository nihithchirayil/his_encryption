<div class="box no-border no-margin">
    <div class="box-body clearfix">
        <div class="theadscroll" style="position: relative; height: 500px;">
            <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                style="border: 1px solid #CCC;" id="">
                <thead>
                    <tr class="table_header_bg" style="cursor: pointer;">
                        <th width="60%" style="text-align: center;">Procedure Name</th>
                        <!-- <th width="20%" style="text-align: center;">Price</th> -->
                        <th width="20%" style="text-align: center;">Procedure Code</th>
                        <th width="5%"><i class="fa fa-trash-o"></i></th>
                    </tr>
                </thead>
                <tbody id="procedurelistData">
                    <?php
                            if(count($bluesheet)!=0){
                            foreach($bluesheet as $each){
                                ?>
                    <tr id="bluesheetrow<?= $each->id ?>">
                        <td class = "common_td_rules"><?= $each->service_desc ?></td>
                        <!-- <td class = "td_common_numeric_rules"><?php  //$each->price ?></td> -->
                        <td class = "common_td_rules"><?= $each->service_code ?></td>
                        <td><button class="btn btn-danger" id="deletebluesheetbtn<?= $each->id ?>"
                                style="padding: 0px 5px" onclick="deletebluesheet(<?= $each->id ?>)" type="button"><i
                                    id="deletebluesheetspin<?= $each->id ?>" class="fa fa-trash-o"></i></button></td>
                    </tr>
                    <?php
                            }
                        }else{
                            ?>
                    <tr>
                        <td colspan="3" style="text-align: center;"> No Result Found</td>
                    </tr>
                    <?php
                        }
                            ?>

                </tbody>
            </table>
        </div>

    </div>
</div>
