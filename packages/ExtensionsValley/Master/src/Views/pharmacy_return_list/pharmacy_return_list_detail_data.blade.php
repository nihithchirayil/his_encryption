<div class="col-md-12" style="margin-bottom:20px;">
    @if(sizeof($result_data)>0)
    <div class="col-md-12" style="margin-bottom:20px;">
        <table>
            <tr>
                <td width="10%"><b>Bill No</b></td>
                <td width="5%"><b>:</b></td>
                <td width="30%"><b>{{$result_data[0]->bill_no}}</b></td>
            </tr>
        </table>
    </div>

    <div class="theadscroll" id="table_container_div" style="position: relative; max-height:300px;">
        <table id="resultDataTable" border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper" style="border-collapse: collapse;">
            <thead>
                <tr class="table_header_common">
                    <th>Sl.No</th>
                    <th>Item</th>
                    @if($service_return_flag == 0)
                    <th>Batch</th>
                    <th>Exp Date</th>
                    @endif
                    <th>Return Qty</th>
                    <th>Selling Price</th>
                    <th>Return Amt</th>
                </tr>
            </thead>
            <tbody>
                @php $i =1; @endphp
                @foreach($result_data as $data)
                <tr>
                    <td>{{$i}}</td>
                    <td class="common_td_rules">{{$data->item_desc}}</td>
                    @if($service_return_flag == 0)
                    <td class="common_td_rules">{{$data->batch}}</td>
                    <td class="common_td_rules">{{date('M-d-Y',strtotime($data->exp_date))}}</td>
                    @endif
                    <td class="td_common_numeric_rules">{{$data->quantity}}</td>
                    <td class="td_common_numeric_rules">{{$data->selling_price}}</td>
                    <td class="td_common_numeric_rules">{{$data->total_amount}}</td>
                 {{-- <td class="td_common_numeric_rules">{{ floatVal($data->selling_price * $data->quantity ) }}</td> --}}
                </tr>
                @php $i++; @endphp
                @endforeach
            </tbody>
        </table>
    </div>
    @else
        <div class="col-md-12">
            <div>
                <strong>No Return Bills Found</strong>
            </div>
        </div>
    @endif
</div>
