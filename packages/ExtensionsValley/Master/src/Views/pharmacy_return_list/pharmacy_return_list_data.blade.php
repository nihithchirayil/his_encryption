<script>
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var patient_id = $('#patient_uhid_hidden').val();
            var return_no = $('#return_no').val();
            var cash_card = $('#cash_card').is(':checked') ? 1 : 0;
            var credit = $('#credit').is(':checked') ? 1 : 0;
            var insurance = $('#insurance').is(':checked') ? 1 : 0;
            var company_credit = $('#company_credit').is(':checked') ? 1 : 0;
            var paid_bills = $('#paid_bills').is(':checked') ? 1 : 0;
            var unpaid_bills = $('#unpaid_bills').is(':checked') ? 1 : 0;
            var cancelled_bills = $('#cancelled_bills').is(':checked') ? 1 : 0;
            var token = $('#c_token').val();
            var data = {
                _token: token,
                from_date: from_date,
                to_date: to_date,
                patient_id: patient_id,
                cash_card: cash_card,
                credit: credit,
                insurance: insurance,
                company_credit: company_credit,
                paid_bills: paid_bills,
                unpaid_bills: unpaid_bills,
                cancelled_bills: cancelled_bills,
                return_no: return_no
            };
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                beforeSend: function() {
                    $("body").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(data) {
                    $('#bill_return_list_container').html(data);
                },
                complete: function() {
                    $("body").LoadingOverlay("hide");
                    $('.theadscroll').perfectScrollbar({
                        minScrollbarLength: 30
                    });

                    var $table = $('table.theadfix_wrapper');
                    $table.floatThead({
                        scrollContainer: function($table) {
                            return $table.closest('.theadscroll');
                        }
                    });
                }
            });
            return false;
        });

    });
</script>
<div class="col-md-12" style="padding: 0px;">

    <div class="" id="table_container_div" style="position: relative; max-height: 500px;">
        <table id="resultDataTable" border="1" width="100%;"
            class="table table-striped table-bordered table-hover table-condensed table_sm "
            style="border-collapse: collapse;">
            <thead>
                <tr class="table_header_common">
                    <th>Sl.No</th>
                    <th>Return No</th>
                    <th>Return Date</th>
                    <th>UHID</th>
                    <th>Patient Name</th>
                    <th>IP No</th>
                    <th>Bill Tag</th>
                    <th>Location</th>
                    <th>Return Amt</th>
                    <th>Payment Type</th>
                    <th>Advance Return</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if (sizeof($result_data) > 0)

                    @php
                        $i = 1;
                        $clr = '';
                    @endphp
                    @foreach ($result_data as $data)
                        <tr>
                            @php $clr=""; @endphp
                            @if ($data->paid_status == '1')
                                @php $clr="paid-bg"; @endphp
                            @else
                                @php $clr="unpaid-bg"; @endphp
                            @endif

                            @if ($data->cancel_status != '0')
                                @php $clr="cancelled-bg"; @endphp
                            @endif

                            <td style="color:white;" class="{{ $clr }}" id="paid{{ $data->id }}">
                                {{ ($result_data->currentPage() - 1) * $result_data->perPage() + $loop->iteration }}
                            </td>
                            <td class="common_td_rules">{{ $data->return_no }}</td>
                            <td class="common_td_rules">{{ date('M-d-Y h:i A', strtotime($data->return_date)) }}</td>
                            <td class="common_td_rules">{{ $data->uhid }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->admission_no }}</td>
                            <td class="common_td_rules">{{ $data->billtagdesc }}</td>
                            <td class="common_td_rules">{{ $data->location_name }}</td>
                            <td class="td_common_numeric_rules">{{ $data->return_amount }}</td>

                            @php $pclr=""; @endphp
                            @if (trim($data->payment_type) == 'cash/Card')
                                @php $pclr="cash_card_bg"; @endphp
                            @elseif($data->payment_type == 'credit')
                                @php $pclr="credit_bg"; @endphp
                            @elseif($data->payment_type == 'insurance')
                                @php $pclr="insurance_bg"; @endphp
                            @elseif($data->payment_type == 'ipcredit')
                                @php $pclr="ipcredit_bg"; @endphp
                            @endif

                            <td class="common_td_rules {{ $pclr }}">
                                {{ $data->paymenttypedesc }}</td>
                            <td class="td_common_numeric_rules">{{ $data->is_open_return }}</td>
                            <td class="common_td_rules">
                                
                                    <div class="col-md-4">
                                <button onclick="selectDetailData('{{ $data->id }}');" title="view" type="button"
                                    class="btn btn-primary btn-sm">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </button>
                            </div>
                                <div class="col-md-4">

                                @if ($data->paid_status == 0)
                                    <button onclick="loadCashReturn('{{ $data->id }}');" title="Pay Bill" id="pay_button{{ $data->id }}"
                                        type="button" class="btn btn-sm bg-green">
                                        <i class="fa fa-credit-card"></i>
                                    </button>
                                @endif
                                </div>
                                <div class="col-md-4">
                                <button class="btn bg-blue" onclick="PrintCashReturn('{{ $data->id }}','{{$data->return_no}}','{{ $data->return_type }}');" title="Print">
                                    <i class="fa fa-print"></i>
                                </button>
                            </div>
                            </td>
                        </tr>
                        @php $i++; @endphp
                    @endforeach
                @else
                    <tr>
                        <td colspan="12"> <strong>No Return Bills Found</strong></td>

                    </tr>
                @endif
            </tbody>
        </table>
    </div>

</div>
<div class="col-md-12">
    <div class="clearfix"></div>
    <div class="col-md-12 text-right">
        <ul class="pagination purple_pagination" style="text-align:right !important; margin: -24px -4px;">
            {!! $page_links !!}
        </ul>
    </div>
</div>
