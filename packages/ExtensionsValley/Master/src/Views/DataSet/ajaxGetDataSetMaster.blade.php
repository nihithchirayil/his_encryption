<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var search_dataset_id = $('#search_dataset_id').val();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        _token: token,
                        from_date: from_date,
                        to_date: to_date,
                        search_dataset_id: search_dataset_id
                    },
                    beforeSend: function() {
                        $('#searchDatasetListBtn').attr('disabled', true);
                        $('#searchDatasetListSpin').removeClass('fa fa-search');
                        $('#searchDatasetListSpin').addClass('fa fa-spinner fa-spin');
                    },
                    success: function(data) {
                        $('#searchDataDiv').html(data);
                    },
                    complete: function() {
                        $('#searchDatasetListBtn').attr('disabled', false);
                        $('#searchDatasetListSpin').removeClass('fa fa-spinner fa-spin');
                        $('#searchDatasetListSpin').addClass('fa fa-search');
                    },
                    error: function() {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
                return false;
            }
        });
    });
</script>
<table class="table no-margin table_sm table-striped table-col-bordered table-condensed" style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_bg">
            <th class="common_td_rules" width="20%">Dataset Name</th>
            <th class="common_td_rules" width="50%">Data Selection</th>
            <th class="common_td_rules" width="15%">Created At</th>
            <th class="common_td_rules" width="5%">Status</th>
            <th class="common_td_rules" width="3%">Is Master</th>
            <th width="3%"><i class="fa fa-edit"></i></th>
            <th width="3%"><i class="fa fa-trash"></i></th>
        </tr>
    </thead>
    <tbody>
        @if (count($item) > 0)
            @foreach ($item as $each)
                <tr id="datasetDataRow{{ $each->set_id }}">
                    <td class="common_td_rules">{{ $each->dataset_name }}</td>
                    <td class="common_td_rules">{{ $each->dataset_selection_data }}</td>
                    <td class="common_td_rules">{{ $each->created_at }}</td>
                    <td class="common_td_rules">{{ $each->active_status }}</td>
                    <td style="text-align: center;">
                        <i class="{{ $each->is_master }}"></i>
                    </td>
                    <td style="text-align: center;">
                        <button id="updateDataSetBtn{{ $each->set_id }}"
                            onclick="addEditDataSet({{ $each->set_id }},'{{ base64_encode($each->dataset_name) }}')"
                            type="button" style="padding: 1px 6px" class="btn btn-warning"><i
                                id="updateDataSetSpin{{ $each->set_id }}" class="fa fa-edit"></i></button>
                    </td>
                    <td style="text-align: center;">
                        <button id="deleteDataSetBtn{{ $each->set_id }}"
                            onclick="deleteDataset({{ $each->set_id }},'{{ base64_encode($each->dataset_name) }}')"
                            type="button" style="padding: 1px 6px" class="btn btn-danger"><i
                                id="deleteDataSetSpin{{ $each->set_id }}" class="fa fa-trash"></i></button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5" style="text-align: center">No Records found</td>
            </tr>
        @endif
    </tbody>
</table>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination">
        {!! $page_links !!}
    </ul>
</div>
