@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')


    <div class="modal fade" id="dataSetmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="addDataSetHeader">NA</h4>
                </div>
                <div class="modal-body" style="height: 450px">
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Dataset Name</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control"
                                id="dataset_name" name="dataset_name" value="">
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm" style="margin-top:16px">
                        <div class="checkbox checkbox-warning inline no-margin">
                            <input checked type="checkbox" id="dataset_status" value="1">
                            <label style="padding-left: 2px;" for="dataset_status">
                                Status</label><br>
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm" style="margin-top:16px">
                        <div class="checkbox checkbox-success inline no-margin">
                            <input type="checkbox" id="is_master_dataset" value="1">
                            <label style="padding-left: 2px;" for="is_master_dataset">
                                Is Master</label><br>
                        </div>
                    </div>
                    <div class="col-md-7 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Dataset Selection</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control"
                                id="dataset_selection_data" name="dataset_selection_data" value="">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Filter 1</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control dataset_filter" id="dataset_filter1"
                                name="dataset_filter1" value="">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Filter 2</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control dataset_filter" id="dataset_filter2"
                                name="dataset_filter2" value="">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Filter 3</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control dataset_filter" id="dataset_filter3"
                                name="dataset_filter3" value="">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Filter 4</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control dataset_filter" id="dataset_filter4"
                                name="dataset_filter4" value="">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Filter 5</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control dataset_filter" id="dataset_filter5"
                                name="dataset_filter5" value="">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Filter 6</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control dataset_filter" id="dataset_filter6"
                                name="dataset_filter6" value="">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm">
                        <label class="custom_floatlabel"><strong> Dataset Query</strong></label>
                        <textarea cols="10" rows="10" name="dataset_query" id="dataset_query" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button id="verfyDatasetBtn" style="padding: 3px 3px" onclick="verfyDataset(1)" type="button"
                        class="btn btn-primary">Verify
                        <i id="verfyDatasetSpin" class="fa fa-check"></i></button>
                    <button id="saveDatasetBtn" style="padding: 3px 3px" onclick="saveDataset()" type="button"
                        class="btn btn-success">Save <i id="saveDatasetSpin" class="fa fa-save"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addToFiltersModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 700px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Add Filters</h4>
                </div>
                <div class="modal-body" style="height: 300px" id="addToFiltersModelDiv">

                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" onclick="addToFilter()" type="button" class="btn btn-primary">Add
                        To Filter <i class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
    </div>


    <div class="right_col" role="main">
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm"><strong class="pull-right"><?= strtoupper($title) ?></strong></div>
            <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
            <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
            <input type="hidden" id="dataset_id" value="0">
        </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">
                    <div class="row codfox_container">
                        <div class="col-md-10 padding_sm">
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">From date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control datepicker"
                                        id="from_date" name="from_date" value="">
                                </div>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">To date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control datepicker"
                                        id="to_date" name="to_date" value="">
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Data Set Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control"
                                        onkeyup="searchDataset(event);" id="search_dataset_name">
                                    <input type="hidden" name="search_dataset_id" value="0"
                                        id="search_dataset_id">
                                    <div class="ajaxSearchBox" id="ajax_search_dataset"
                                        style="margin-top: -15px;width: 100%;z-index: 9999;display: none;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm" style="margin-top: 17px">
                                <div class="clearfix"></div>
                                <button type="button" onclick="resetFilters()" class="btn btn-block btn-warning"><i
                                        class="fa fa-recycle"></i>
                                    Reset</button>
                            </div>
                            <div class="col-md-1 padding_sm" style="margin-top: 17px">
                                <div class="clearfix"></div>
                                <button type="button" id="searchDatasetListBtn" onclick="searchDatasetList()"
                                    class="btn btn-block btn-primary"><i id="searchDatasetListSpin"
                                        class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="col-md-12 padding_sm" style="margin-top: 17px">
                                <button type="button" id="addDataSetBtn"
                                    onclick="addEditDataSet(0,'{{ base64_encode('Add New Data Set') }}')"
                                    class="btn btn-block btn-success"><i id="addDataSetSpin" class="fa fa-plus"></i>
                                    Add New Data Set</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 padding_sm">
                <div class="x_panel">
                    <div id="searchDataDiv">

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/dataSetManager.js') }}"></script>
@endsection
