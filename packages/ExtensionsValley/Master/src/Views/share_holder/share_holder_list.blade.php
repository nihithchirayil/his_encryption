@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<style>
    .panel_bg {
        background: #0f7375 !important;
        color: #FFF !important;
        padding: 5px !important;
    }
    .btn_green_bg {
        background: #0f7375 !important;
        color: #FFF !important;
    }
    .beneficiary_dropdown {
        float: left;
        width: 100%;
        background: #edf9f9;
        box-shadow: 0 0 6px #AAA;
        padding: 5px;
        margin: 0 0 10px 0;
        border-radius: 3px;
        display: none;
    }
    .benef_edit_drop_box .form-group {
        padding: 5px 10px;
        border-radius: 3px;
    }

    .benef_edit_drop_box .form-group h4 {
        margin: 0;
    }

</style>

@endsection
@section('content-area')
<!-- page content -->
<div class="right_col"  role="main">
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix" style="background: #FFF; padding: 5px;">
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="" class="header_label">Share Holder Name</label>
                                <div class="clearfix"></div>
                                <input type="text" name="name" autocomplete="off" id="share_holder"
                                       class="form-control share_holder_name"
                                       placeholder="Name" onkeyup="searchShareHolder(this.id, event)">
                                <div class="ajaxSearchBox" id="share_holder_div" style="margin-top: 13px !important">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label for="" class="header_label"></label>
                            <div class="clearfix"></div>
                            <button class="btn btn_green_bg" onclick="newShareHolder()">
                                    <i class="fa fa-plus"></i>
                                    Add New Share Holder
                                </button>
                        </div>

                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="" class="header_label">Beneficiery Name</label>
                                <div class="clearfix"></div>
                                <input type="text" name="name" autocomplete="off" id="beneficery"
                                       class="form-control beneficery"
                                       placeholder="Name" onkeyup="searchBeneficery(this.id, event)">

                                <div class="ajaxSearchBox" id="benef_div" style="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm text-right">
                            <label for="" class="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-success" onclick="BulkBlock()"><i class="fa fa-ban" id="bulk_block"></i> Block All</span>
                        </div>
                        <div class="col-md-2 padding_sm pull-right">
                            <label for="" class="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <div style='width:20px;height:20px;display:block;float:left;background-color:#f7e5e5;'></div>
                            <label style="margin: 2px 0 0 10px;float: left;">Blocked</label>
                            <div style='width:20px;height:20px;display:block;float:left;background-color:#d3f5de;margin-left: 10px'></div>
                            <label style="margin: 2px 0 0 10px;float: left;">Active</label>
                        </div>

                    </div>
                </div>
            </div>


            <div class="clearfix"></div>
            <div class="box no-border no-margin" style="height: 450px;">
                <div class="box-body clearfix">
                    <div id="content_form"></div>
                </div>

            </div>


        </div>
    </div>

</div>
<div class="modal bs-example-modal-lg mkModalFloat in" id="shareHolderModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background:#26B99A; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">New Share Holder</h4>
            </div>
            {!!Form::open(array('url' => $addUrl,'id'=>'shareaddForm', 'method' => 'post'))!!}
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="name" id="name" autocomplete="off" placeholder="name">
                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">

                            <input type="checkbox" id="isDirector" name="is_director" /> <label for="">Is Director</label>
                        </div>
                    </div>
                            </div>
                     </div>
                     <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label class="">DOB</label>
                                    <input type="text" data-attr="date"  autocomplete="off" name="date" value="" class="form-control" placeholder="MM-DD-YYYY" id="dob_shareholder">

                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Area</label>
                            <div class="clearfix"></div>
                            {!! Form::select('area', $area,'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'area']) !!}

                        </div></div>
                            </div></div>
                     <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Pincode</label>
                            <input type="text" class="form-control" name="pincode" id="pincode" autocomplete="off" placeholder="Pincode">

                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Mobile No</label>
                            <input type="text" class="form-control" name="mobile_no" id="mobile_no" autocomplete="off" placeholder="Mobile No">

                        </div></div>
                            </div></div>
                     <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Email</label>
                            <input type="text" class="form-control" name="email" id="email" autocomplete="off" placeholder="Email">

                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Pan No</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="pan_no" id="pan_no" autocomplete="off" placeholder="Pan No">

                        </div></div>
                            </div></div>
                     <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Nationality</label>
                            <div class="clearfix"></div>
                            {!! Form::select('nationality', $country,1,['autocomplete'=>'off', 'class'=>'form-control','id' => 'nationality']) !!}

                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Passport No</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="passport_no" id="passport_no" autocomplete="off" placeholder="Passport No">

                        </div></div>
                            </div></div>
                     <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Share Value</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="share_value" id="share_value" autocomplete="off" placeholder="Share Value">

                        </div></div>

                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Number of shares</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="no_of_shares" id="no_of_shares" autocomplete="off" placeholder="Number of shares">

                        </div></div>
                            </div></div>
                    <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-5 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Gender</label>
                                        <div class="clearfix"></div>
                                        @php $gender_array = array("0"=> " Select") + $genders->toArray() @endphp
                                        {!! Form::select('gender', $gender_array,'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'gender']) !!}

                                    </div></div>
                            </div>
                        </div>
                     <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                         <div class="col-md-10 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Address</label>
                            <div class="clearfix"></div>
                            <textarea class="form-control" id="address" name="address" placeholder="Address"></textarea>
                        </div></div>
                            </div></div>

                    <!--                    <div class="col-md-5 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Credit Limit</label>
                                                <div class="clearfix"></div>
                                                <input type="text" class="form-control" name="credit_limit" id="credit_limit" autocomplete="off" placeholder="Credit Limit">
                                            </div></div>
                                        <div class="col-md-5 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Credit Description</label>
                                                <div class="clearfix"></div>
                                                <input type="text" class="form-control" name="credit_desc" id="credit_desc" autocomplete="off" placeholder="Credit Description">
                                            </div></div>-->
                </div>
            </div>
            <input type="hidden" value="0" id="change_share_holder" name="change_share_holder">
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i>  Close</button>
                <button type="button" class="btn btn-success" onclick="saveShareHolder()"><i class="fa fa-save" id="add_share_holder_spin"></i> Save</button>
            </div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script type="text/javascript">


//----------------------------------------progressive search fields---------------------------------------------

                    function searchShareHolder(id, event) {
                        var keycheck = /[a-zA-Z0-9 ]/;
                        var value = event.key;
                        var ajax_div = 'share_holder_div';

                        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
                            var share_holder = $('#' + id).val();
                            if (share_holder == "") {
                                $("#" + ajax_div).html("");
                            } else {
                                $.ajax({
                                    type: "GET",
                                    url: "",
                                    data: 'sh_name=' + share_holder + "&is_benef=0",
                                    beforeSend: function () {
                                        $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                                    },
                                    success: function (html) {
                                        $("#" + ajax_div).html(html).show();
                                        $("#" + ajax_div).find('li').first().addClass('liHover');
                                    },
                                    complete: function () {
                                        $('.theadscroll').perfectScrollbar("update");
                                        $(".theadfix_wrapper").floatThead('reflow');
                                    }
                                });
                            }

                        } else {
                            ajaxProgressiveKeyUpDown(ajax_div, event);
                        }
                    }

                    function fillShareHolderValues(e, id, name, uhid, parent_share_holder) {
                        if (parent_share_holder != 0) {
                            id = parent_share_holder;
                        }
                        $.ajax({
                            type: "GET",
                            url: "",
                            data: 'content_show=1&holder_id=' + id,
                            beforeSend: function () {
                                if (parent_share_holder != 0 || parent_share_holder != '') {
                                    $("#benef_div").hide();
                                } else {
                                    $("#share_holder_div").hide();
                                }
                                $("#content_form").html('<span style="width:300px;text-align: center;margin-left: 50%;"><i class="fa fa-spinner fa-pulse fa-4x fa-fw fa-fade"></i></span>').show();
                            },
                            success: function (html) {
                                $("#content_form").html(html);

                            },
                            complete: function () {
                                $('.theadscroll').perfectScrollbar("update");
                                $(".theadfix_wrapper").floatThead('reflow');
                            }
                        });
                        if (parent_share_holder != 0) {
                            $("#beneficery").val(name);
                        } else {
                            $("#share_holder").val(name);
                        }
//                                    $("#edit_bill_date_" + rowid).val(bill_date);
//                                    $("#edit_bill_paid_amount_" + rowid).val(net_amount);
                    }

                    function searchBeneficery(id, event) {
                        var keycheck = /[a-zA-Z0-9 ]/;
                        var value = event.key;
                        var ajax_div = 'benef_div';

                        if (value.match(keycheck) || event.keyCode == 8 || event.keyCode == 46) {
                            var benef_holder = $('#' + id).val();
                            if (benef_holder == "") {
                                $("#" + ajax_div).html("");
                            } else {
                                $.ajax({
                                    type: "GET",
                                    url: "",
                                    data: 'sh_name=' + benef_holder + "&is_benef=1",
                                    beforeSend: function () {
                                        $("#" + ajax_div).html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                                    },
                                    success: function (html) {
                                        $("#" + ajax_div).html(html).show();
                                        $("#" + ajax_div).find('li').first().addClass('liHover');
                                    },
                                    complete: function () {
                                        $('.theadscroll').perfectScrollbar("update");
                                        $(".theadfix_wrapper").floatThead('reflow');
                                    }
                                });
                            }

                        } else {
                            ajaxProgressiveKeyUpDown(ajax_div, event);
                        }
                    }



  //-----------------------------------------------------------------dropdown benef-sharehaolder---------------------------------------------------------------------------------------

                    $(document).ready(function () {

                        $(document).on('click', '.benef_drop_btn', function () {
                            $(this).closest('.benef_edit_drop_box').find('.beneficiary_dropdown').slideToggle();
                        });
                        $(document).on('click', '.benef_drop_close_btn', function () {
                            $(this).closest('.benef_edit_drop_box').find('.beneficiary_dropdown').slideUp();
                        });


                        $('.month_picker').datetimepicker({
                            format: 'MM'
                        });
                        $('.year_picker').datetimepicker({
                            format: 'YYYY'
                        });


                        var $table = $('table.theadfix_wrapper');

                        $table.floatThead({
                            scrollContainer: function ($table) {
                                return $table.closest('.theadscroll');
                            }

                        });

                        $('.datepicker').datetimepicker({
                            format: 'DD-MM-YYYY'
                        });
                        $('.date_time_picker').datetimepicker();


                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                        $("input[data-attr='date']").datetimepicker({ format: 'MMM-DD-YYYY' });





                    });

//------------------------------------function to add new shareholder-----------------------------------------
                    function saveShareHolder() {

                        var name = $("#name").val();

                        var date = $("#dob_shareholder").val();

                        var gender = $("#gender").val();

                        if(gender == 0){
                            alert("Please select gender");return false;
                        }
                        if(name == ''){
                        alert("Please select name");return false;
                        }
//                        if(date == ''){
//                        alert("Please enter Date of birth");return false;
//                        }


                        $.ajax({
                            type: "GET",
                            url: "{!! route('extensionsvalley.master.add_share_holder') !!}",
                            data: $("#shareaddForm").serialize(),
                            beforeSend: function () {
                                $("#add_share_holder_spin").removeClass('fa fa-save');
                                $("#add_share_holder_spin").addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                var json_data = JSON.parse(data);
                                $("#add_share_holder_spin").removeClass('fa fa-spinner fa-spin');
                                $("#add_share_holder_spin").addClass('fa fa-save');
                                if (json_data.success == 1) {
                                    toastr.success("Created  Successfully");
                                    bootbox.alert("Created Successfully and UHID is: "+json_data.uhid);
                                    refresh_content(json_data.id);
                                    $("#shareHolderModel").modal('hide');
                                } else if (json_data.success == 1) {
                                    toastr.warning("Error !!");
                                }
                            },
                            complete: function () {

                            }
                        });
                    }

 //-----------------------------------------------------------------function to update shareholder data---------------------------------------------------------------------------------------

                    function updateShareHolder() {
                        $.ajax({
                            type: "GET",
                            url: "{!! route('extensionsvalley.master.add_share_holder') !!}",
                            data: $("#shareeditForm").serialize(),
                            beforeSend: function () {
                                $("#update_share_holder_spin").removeClass('fa fa-save');
                                $("#update_share_holder_spin").addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                $("#update_share_holder_spin").removeClass('fa fa-spinner fa-spin');
                                $("#update_share_holder_spin").addClass('fa fa-save');
                                if (data == '1') {
                                    toastr.success("UpdatedSuccessfully");
                                }
                            },
                            complete: function () {

                            }
                        });
                    }


//-----------------------------------------------------------------function to delete shareholder---------------------------------------------------------------------------------------

                    function deleteShareHolder(id) {
                        if (confirm("If sure CLICK OK button")) {
                        $.ajax({
                            type: "GET",
                            url: "{!! route('extensionsvalley.master.delete_share_holder') !!}",
                            data: {id: id},
                            beforeSend: function () {
                                $("#delete_share_holder_spin").removeClass('fa fa-times');
                                $("#delete_share_holder_spin").addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                $("#delete_share_holder_spin").removeClass('fa fa-spinner fa-spin');
                                $("#delete_share_holder_spin").addClass('fa fa-times');
                                if (data == '1') {
                                    toastr.success("DeletedSuccessfully");
                                }
                            },
                            complete: function () {
                                window.location.reload();
                            }
                        });
                    }
                    }
//----------------------------------------function to save new benef----------------------------------------------
                    function saveBenef() {

                        var name = $("#benef_name").val();


                        var date = $("#dob_benef").val();

                       var gender = $("#gender_benef").val();

                        if(gender == 0){
                            alert("Please select gender");return false;
                        }

                        if(name == ''){
                        alert("Please select name");return false;
                        }
//                        if(date == ''){
//                        alert("Please enter Date of birth");return false;
//                        }

                        var id = $("#parent_share_holder").val();
                        $.ajax({
                            type: "GET",
                            url: "{!! route('extensionsvalley.master.save_beneficiary') !!}",
                            data: $("#benefaddForm").serialize(),
                            beforeSend: function () {
                                $("#benif_spin").removeClass('fa fa-save');
                                $("#benif_spin").addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                 var json_data = JSON.parse(data);
                                $("#benefModel").modal('hide');
                                 if (json_data.success == 1) {
                                    bootbox.alert("Created Successfully and UHID is: "+json_data.uhid);
                                    refresh_content(id);
                                    $("#benif_spin").removeClass('fa fa-spinner fa-spin');
                                    $("#benif_spin").addClass('fa fa-save');
                                    toastr.success("Updated Successfully");
                                } else {
                                    toastr.error("Error Occured Plese Check Data");
                                }
                            },
                            complete: function () {

                            }
                        });
                    }


 //-----------------------------------------------------------------function to refresh the content of table---------------------------------------------------------------------------------------


                    function refresh_content(id,detailArray1='') {
                        $.ajax({
                            type: "GET",
                            url: "",
                            data: 'content_show=1&holder_id=' + id,
                            beforeSend: function () {
                                $("#content_form").html('<span style="width:300px;text-align: center;margin-left: 50%;"><i class="fa fa-spinner fa-pulse fa-4x fa-fw fa-fade"></i></span>').show();
                            },
                            success: function (html) {
                                $("#benefModel").modal('hide');
                                $("#content_form").html(html);
                                if(detailArray1.length > 0){
                                    for (var i = 0; i < detailArray1.length; i++) {
                                        $(".benef_check_class_"+detailArray1[i]).trigger('click');
                                    }
                                }
                            },
                            complete: function () {

                            }
                        });
                    }
                    function updateBenef(form_id) {
                        var id = $("#parent_share_holder").val();
                        var name_updated = $("#name_" + form_id).val();
                        var relation_upd = $("#relationship_" + form_id +" option:selected").text();
                        $.ajax({
                            type: "GET",
                            url: "{!! route('extensionsvalley.master.save_beneficiary') !!}",
                            data: $("#benef_edit_" + form_id).serialize() + '&parent_share_holder=' + id,
                            beforeSend: function () {
                                $("#update_benif_spin_" + form_id).removeClass('fa fa-save');
                                $("#update_benif_spin_" + form_id).addClass('fa fa-spinner fa-spin');
                            },
                            success: function (data) {
                                $("#edit_close_button_" + form_id).trigger("click");
                                if (data == 1) {
                                    $("#show_name_update_" + form_id).html(name_updated);
                                    $("#show_rel_update_" + form_id).html('('+ relation_upd +')');
                                    $("#update_benif_spin_" + form_id).removeClass('fa fa-spinner fa-spin');
                                    $("#update_benif_spin_" + form_id).addClass('fa fa-save');
                                    toastr.success("Updated Successfully");
                                }
                            },
                            complete: function () {

                            }
                        });
                    }


 //-----------------------------------------------------------------function to block the benef---------------------------------------------------------------------------------------

                    function blockBenef(form_id, detail_id, type) {
                        var detailArray1 = [];
                        $('.bulk_check:checked').each(function () {
                            detailArray1.push(this.value);
                            });
                        if (confirm("If sure CLICK OK button")) {
                            var id = $("#parent_share_holder").val();
                            $.ajax({
                                type: "GET",
                                url: "{!! route('extensionsvalley.master.block_beneficiary') !!}",
                                data: 'detail_id=' + detail_id + '&type=' + type+'&detailArray1='+detailArray1,
                                beforeSend: function () {
                                    if (type == 1) {
                                        $("#block_benif_spin_" + form_id).removeClass('fa fa-ban');
                                        $("#block_benif_spin_" + form_id).addClass('fa fa-spinner fa-spin');
                                    } else {
                                        $("#unblock_benif_spin_" + form_id).removeClass('fa fa-ban');
                                        $("#unblock_benif_spin_" + form_id).addClass('fa fa-spinner fa-spin');
                                    }
                                },
                                success: function (data) {
                                    $("#edit_close_button_" + form_id).trigger("click");
                                    if (data == 1) {
                                        refresh_content(id,detailArray1);
                                        toastr.success("Updated Successfully");
                                    }
                                },
                                complete: function () {
                                    if (type == 1) {
                                        $("#block_benif_spin_" + form_id).removeClass('fa fa-spinner fa-spin');
                                        $("#block_benif_spin_" + form_id).addClass('fa fa-ban');
                                    } else {
                                        $("#unblock_benif_spin_" + form_id).removeClass('fa fa-spinner fa-spin');
                                        $("#unblock_benif_spin_" + form_id).addClass('fa fa-ban');
                                    }
                                }
                            });
                        }
                    }
                    function expireBenef(form_id, detail_id) {
                        if (confirm("If sure CLICK OK button")) {
                            var id = $("#parent_share_holder").val();
                            $.ajax({
                                type: "GET",
                                url: "{!! route('extensionsvalley.master.delete_beneficiary') !!}",
                                data: 'detail_id=' + detail_id ,
                                beforeSend: function () {

                                        $("#exp_benif_spin_" + form_id).removeClass('fa fa-ban');
                                        $("#exp_benif_spin_" + form_id).addClass('fa fa-spinner fa-spin');

                                },
                                success: function (data) {
                                    $("#edit_close_button_" + form_id).trigger("click");
                                    if (data == 1) {
                                        refresh_content(id);
                                        toastr.success("Updated Successfully");
                                    }
                                },
                                complete: function () {
                                        $("#exp_benif_spin_" + form_id).removeClass('fa fa-spinner fa-spin');
                                        $("#exp_benif_spin_" + form_id).addClass('fa fa-ban');

                                }
                            });
                        }
                    }
function MakeShareHolder(id,parent_id){
     if (confirm("If sure CLICK OK button")) {
                        $.ajax({
                            type: "GET",
                            url: "{!! route('extensionsvalley.master.make_share_holder') !!}",
                            data: {id: id,parent_id:parent_id},
                            beforeSend: function () {

                            },
                            success: function (data) {
                                if (data == '1') {
                                    toastr.success("Share Holder Updated successfully");
                                }
                            },
                            complete: function () {
                                $("#make_share_holder_"+id).hide();
                                window.location.reload();
                            }
                        });
                    }
}
function changeShareHolder(id){
    $("#change_share_holder").val(id);
    $("#shareHolderModel").modal('show');
}
function newShareHolder(){
    $("#change_share_holder").val('0');
    $("#shareHolderModel").modal('show');
    }
function bulUploadCheck(e,id,old_color){
    if($(e).is(':checked')){
        $(".name_list"+id).css('background-color','#7e7ed3');
    }else{
        $(".name_list"+id).css('background-color',old_color);
    }

}
function BulkBlock(){
     var patient_array = new Array();
     $('.bulk_check').each(function(){
         if($(this).is(':checked')){
         patient_array.push({
                 'patient_id': $(this).val()
         });
     }
 });
 var return_data = JSON.stringify(patient_array);
    if (confirm("If sure CLICK OK button")) {
                            var id = $("#parent_share_holder").val();
                            $.ajax({
                                type: "POST",
                                url: "{!! route('extensionsvalley.master.block_bulk_beneficiary') !!}",
                                data: 'patient_array=' + return_data ,
                                beforeSend: function () {
                                        $("#bulk_block").removeClass('fa fa-ban');
                                        $("#bulk_block").addClass('fa fa-spinner fa-spin');
                                },
                                success: function (data) {
                                    if (data == 1) {
                                        refresh_content(id);
                                        toastr.success("Updated Successfully");
                                    }
                                },
                                complete: function () {
                                        $("#bulk_block").removeClass('fa fa-spinner fa-spin');
                                        $("#bulk_block").addClass('fa fa-ban');
                                }
                            });
                        }
}
</script>
@endsection
