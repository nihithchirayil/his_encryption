<?php
$country = ExtensionsValley\Master\Models\Country::where('status', '=', '1')->orderBy('name')->pluck('name', 'id');
$area = ExtensionsValley\Master\Models\Area::Where('status', 1)->OrderBy('name')->pluck('name', 'name');
$genders = \DB::table('gender')->OrderBy('name')->pluck('name', 'id');
if (isset($benef_active_count) && $benef_active_count >= 200) {
    $benef_active_count_span = '<span style="color:#fff"> Count:' . $benef_active_count . '</span>';
} else {
    $benef_active_count_span = '<a class="" data-toggle="modal" href="#benefModel" > <button class="btn btn_green_bg">
                                    <i class="fa fa-plus"></i> </button></a>';
}
?>
<input type="hidden" value="{{$benef_active_count}}" name="benef_active_count" id="benef_active_count">
<div class="container-fluid" style="padding: 15px;">
    <div class="row codfox_container">
        <div class="clearfix"></div>
        <div class="ht10"></div>
        <div class="col-md-12 padding_sm">
            <div class="col-md-6 padding_sm">
                <div class="panel panel-default no-margin">
                    <div class="panel-heading panel_bg clearfix">
                        <div class="col-md-8 padding_sm">
                            Share Holder
                        </div>
                        <div class="col-md-4 text-right">
                            {{$share_holder_data->uhid}}
                        </div>
                    </div>
                    <div class="panel-body form_group_last_no_margin" style="padding: 10px;">
                        {!!Form::open(array('url' => $updateUrl,'id'=>'shareeditForm', 'method' => 'post'))!!}
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Name</label>
                                        <div class="clearfix"></div>
                                        <input  type="text" value="{{$share_holder_data->patient_name}}" class="form-control" name="name" placeholder="First Name"></div></div>
                                <?php
                                if ($share_holder_data->is_director == 1) {
                                    $chkd = 'checked';
                                } else {
                                    $chkd = '';
                                }
                                ?>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <input type="checkbox" <?= $chkd; ?> id="isDirector" name="is_director" /> <label for="" class="header_label">Is Director</label>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label class="">DOB</label>
                                    <input type="text" data-attr="date1"  autocomplete="off" name="date" value="{{date('M-d-Y',strtotime($share_holder_data->dob))}}" class="form-control" placeholder="MM-DD-YYYY" >


                                    </div></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Area</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('area', $area,$share_holder_data->area,['autocomplete'=>'off', 'class'=>'form-control','id' => 'area']) !!}

                                    </div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Pincode</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$share_holder_data->pincode}}"  class="form-control" name="pincode" id="pincode" autocomplete="off" placeholder="Pincode">

                                    </div>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Mobile No</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$share_holder_data->phone}}" class="form-control" name="mobile_no" id="mobile_no" autocomplete="off" placeholder="Mobile No">

                                    </div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Email</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$share_holder_data->email}}" class="form-control" name="email" id="email" autocomplete="off" placeholder="Email">
                                       </div></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Pan No</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$share_holder_data->pan_no}}" class="form-control" name="pan_no" id="pan_no" autocomplete="off" placeholder="Pan No">
                                       </div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Nationality</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('nationality', $country,$share_holder_data->nationality?$share_holder_data->nationality:1,['autocomplete'=>'off', 'class'=>'form-control','id' => 'nationality']) !!}
                                        </div>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Passport No</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$share_holder_data->passport_no}}" class="form-control" name="passport_no" id="passport_no" autocomplete="off" placeholder="Passport No">
                                       </div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Share Value</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$share_holder_data->share_value}}" class="form-control" name="share_value" id="share_value" autocomplete="off" placeholder="Share Value">
                                        </div>
                                </div>
                                <div class="col-md-6 no-padding">
                                    <div class="form-group clearfix">
                                        <div class="col-md-12 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">No Of Shares</label>
                                        <div class="clearfix"></div>
                                        <input type="text"  value="{{$share_holder_data->no_of_shares}}" class="form-control" name="no_of_shares" id="no_of_shares" autocomplete="off" placeholder="Number of shares"> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">
                                <div class="col-md-5 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Gender</label>
                                        <div class="clearfix"></div>
                                        @php $gender_array = array("0"=> " Select") + $genders->toArray() @endphp
                                        {!! Form::select('gender', $gender_array,$share_holder_data->gender,['autocomplete'=>'off', 'class'=>'form-control','id' => 'gender']) !!}

                                    </div></div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group clearfix">


                                        <div class="col-md-12 padding_sm">
                                            <div class="mate-input-box">
                                                <label for="">Address</label>
                                                <div class="clearfix"></div>
                                                <textarea class="form-control" id="address" name="address" placeholder="Address">{{$share_holder_data->address}}</textarea></div>
                                        </div>
                                    </div>
                                </div>

                                <!--                                <div class="col-md-6 padding_sm">
                                                                    <div class="mate-input-box">
                                                                    <label for="">Credit Limit</label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="text"  value="{{$share_holder_data->credit_limit}}" class="form-control" name="credit_limit" id="credit_limit" autocomplete="off" placeholder="Credit Limit"></div>
                                                                </div>-->
                            </div>
                        </div>
                        <!--                        <div class="col-md-12 no-padding">
                                                    <div class="form-group clearfix">
                                                        <div class="col-md-6 padding_sm">
                                                            <div class="mate-input-box">
                                                            <label for="">Credit Description</label>
                                                            <div class="clearfix"></div>
                                                            <input type="text"  value="{{$share_holder_data->credit_desc}}" class="form-control" name="credit_desc" id="credit_desc" autocomplete="off" placeholder="Credit Description"></div>
                                                        </div>
                                                    </div>
                                                </div>-->
                        <input type="hidden" value="{{$share_holder_data->id}}" name="edit_id" id="edit_id">
                        <div class="col-md-12 padding_sm text-right">
                            <span class="btn btn-danger " onclick="deleteShareHolder({{$share_holder_data->id}})"><i class="fa fa-trash-o"></i> Delete</span>
                            <button type="button" class="btn btn-success" onclick="updateShareHolder()" ><i class="fa fa-save" id="update_share_holder_spin"></i> Update</button>
                            <button type="button" class="btn btn-info" onclick="changeShareHolder({{$share_holder_data->id}})" ><i class="fa fa-save" id="update_share_holder_spin"></i> Change Share Holder</button>
                        </div>
                        {!! Form::token() !!}
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
            <div class="col-md-6 padding_sm">
                <div class="panel panel-default no-margin">
                    <div class="panel-heading panel_bg clearfix">
                        <div class="col-md-8 padding_sm">
                            Beneficiaries
                        </div>
                        <div class="col-md-4 text-right">
                            <?= $benef_active_count_span ?>
                            <!--                            <a class="" data-toggle="modal" href="#benefModel" > <button class="btn btn_green_bg">
                                                                <i class="fa fa-plus"></i> </button></a>-->
                        </div>
                    </div>
                    <div class="panel-body form_group_last_no_margin" style="padding: 10px;">
                        <?php
                        if (isset($benef_data) && $benef_data != '') {
                            $i = 0;
                            foreach ($benef_data as $dtls) {
                                if ($dtls->share_holder_status == 0) {
                                    $bg_color = 'style="background-color: #f77a7a;cursor:pointer"';
                                    $bg_color_chk = "background-color: #f77a7a;cursor:pointer";
                                    $bg_color_snd = "#f77a7a";
                                } else {
                                    $bg_color = 'style="background-color: #5fd083;cursor:pointer"';
                                    $bg_color_chk = "background-color: #5fd083;cursor:pointer";
                                    $bg_color_snd = "#5fd083";
                                }
                                ?>
                                <div class="col-md-1 padding_sm name_list{{  $dtls->id }}" style=";cursor: pointer;padding: 7px 16px !important;border-radius: 3px;{{ $bg_color_chk }}">
                                    <div class="checkbox checkbox-success inline no-margin">
                                        <input type="checkbox" class="form-control filters bulk_check benef_check_class_{{$dtls->id}}" value="{{ $dtls->id }}" onclick="bulUploadCheck(this,'{{  $dtls->id }}','{{ $bg_color_snd }}')">
                                        <label class="text-blue " >&nbsp;</label>
                                    </div>
                               </div>
                                <div class="col-md-11 padding_sm benef_edit_drop_box" >
                                    <div class="form-group clearfix name_list{{  $dtls->id }}" <?= $bg_color; ?>>
                                        <label class="benef_drop_btn" id="show_name_<?= $i ?>" style="cursor:pointer;width:100%">
                                        <h4>
                                        <span id="show_name_update_<?= $i ?>"><?= $dtls->patient_name; ?></span>
                                        <span  id="show_rel_update_<?= $i ?>">{{$dtls->relationship_title ? '('.$dtls->relationship_title.')' :''}}</span>
                                        <span class="pull-right">{{$dtls->uhid ? $dtls->uhid :''}}</span>
                                        </h4></label><!--                                        <div class="input-group">
                                                <input type="text" class="form-control" <?= $bg_color ?> value="<?= $dtls->patient_name; ?>" id="show_name_<?= $i ?>">
                                                <div class="input-group-btn">
                                                    <button class="btn btn_green_bg benef_drop_btn"><i class="fa fa-edit"></i></button>
                                                </div>
                                            </div>-->
                                        <div class="beneficiary_dropdown">
                                            <form  id="benef_edit_<?= $i ?>" method="post">
                                                <div class="col-md-12 padding_sm text-right">
                                                    <span class="btn btn-success" onclick="updateBenef('<?= $i ?>')"><i class="fa fa-save" id="update_benif_spin_<?= $i ?>"></i> Save</span>
                                                    <?php if ($dtls->share_holder_status == 1) { ?>
                                                        <span class="btn btn-primary" onclick="blockBenef('<?= $i ?>', '<?= $dtls->id ?>', '1')"><i class="fa fa-ban" id="block_benif_spin_<?= $i ?>"></i> Block</span>
                                                    <?php } else if ($benef_active_count < 5 && $dtls->share_holder_status == 0) { ?>
                                                        <span class="btn btn-danger" onclick="blockBenef('<?= $i ?>', '<?= $dtls->id ?>', '0')"><i class="fa fa-ban" id="unblock_benif_spin_<?= $i ?>"></i> UnBlock</span>
                                                    <?php } ?>
                                                    <span class="btn btn-danger benef_drop_close_btn" id="edit_close_button_<?= $i ?>"><i class="fa fa-times"></i> Close</span>
                                                    <span class="btn btn-info benef_drop_close_btn" id="make_share_holder_<?= $dtls->id ?>" onclick="MakeShareHolder('<?= $dtls->id ?>','<?= $dtls->parent_share_holder ?>')"> Make Share Holder</span>
                                                    <span class="btn btn-warning" onclick="expireBenef('<?= $i ?>', '<?= $dtls->id ?>')"><i class="fa fa-ban" id="exp_benif_spin_<?= $i ?>"></i> Delete</span>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="ht5"></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">name</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" id="name_<?= $i; ?>"value="<?= $dtls->patient_name; ?>"class="form-control" name="benef_name" id="benef_name" autocomplete="off" placeholder="name">
                                                    </div></div>
                                                <?php
                                                $relationship = \DB::table('relationship_master')->where('status', 1)->where('id','<>',1)->orderBy('view_order')->pluck('description', 'id');
                                                ?>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Relationship</label>
                                                        <div class="clearfix"></div>
                                                        {!! Form::select('relationship', $relationship,$dtls->dependent_relation,['autocomplete'=>'off', 'class'=>'form-control','id' => 'relationship_'.$i]) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label class="">DOB</label>
                                                         <input type="text" data-attr="date1"  autocomplete="off" name="benef_date" value="{{date('M-d-Y',strtotime($dtls->dob))}}" class="form-control" placeholder="MM-DD-YYYY">

                                                    </div>
                                                </div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Area</label>
                                                        <div class="clearfix"></div>
                                                        {!! Form::select('benef_area', $area,$dtls->area,['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_area']) !!}
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Pincode</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" class="form-control" value="<?= $dtls->pincode; ?>"  name="benef_pincode" id="benef_pincode" autocomplete="off" placeholder="Pincode">
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Mobile No</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" class="form-control" value="<?= $dtls->phone; ?>" name="benef_mobile_no" id="benef_mobile_no" autocomplete="off" placeholder="Mobile No">
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Email</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" class="form-control" value="<?= $dtls->email; ?>" name="benef_email" id="benef_email" autocomplete="off" placeholder="Email">
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Pan No</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" class="form-control" value="<?= $dtls->pan_no; ?>" name="benef_pan_no" id="benef_pan_no" autocomplete="off" placeholder="Pan No">
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Nationality</label>
                                                        <div class="clearfix"></div>
                                                        {!! Form::select('benef_nationality', $country,$dtls->nationality?$dtls->nationality:1,['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_nationality']) !!}
                                                    </div></div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Passport No</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" class="form-control" value="<?= $dtls->passport_no; ?>"  name="benef_passport_no" id="benef_passport_no" autocomplete="off" placeholder="Passport No">
                                                    </div></div>
                                                <div class="col-md-12 no-padding">
                                                    <div class="form-group clearfix">
                                                        <div class="col-md-5 padding_sm">
                                                            <div class="mate-input-box">
                                                                <label for="">Gender</label>
                                                                <div class="clearfix"></div>
                                                                @php $gender_array = array("0"=> " Select") + $genders->toArray() @endphp
                                                                {!! Form::select('gender_benef', $gender_array,$dtls->gender,['autocomplete'=>'off', 'class'=>'form-control','id' => 'gender_benef']) !!}

                                                            </div></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5 padding_sm">
                                                    <div class="mate-input-box">
                                                        <label for="">Address</label>
                                                        <div class="clearfix"></div>
                                                        <textarea class="form-control" id="benef_address" name="benef_address" placeholder="Address"><?= $dtls->address; ?></textarea>
                                                    </div></div>

                                                <input type="hidden" name="benef_edit_id" id="benef_id" value="{{$dtls->id}}"
                                                       </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                                <?php
                                $i++;
                            }
                        }
                        ?>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="modal" id="benefModel"  role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background:#26B99A; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">New Beneficiarey</h4>
            </div>
            {!!Form::open(array('id'=>'benefaddForm', 'method' => 'post'))!!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">name</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="benef_name" id="benef_name" autocomplete="off" placeholder="name">
                        </div></div>
                    <?php
                    $relationship = \DB::table('relationship_master')->where('status', 1)->where('id','<>',1)->orderBy('view_order')->pluck('description', 'id');
                    ?>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Relationship</label>
                            <div class="clearfix"></div>
                            {!! Form::select('relationship', $relationship,'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'relationship']) !!}
                        </div>
                    </div>


                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label class="">DOB</label>
                            <input type="text" data-attr="date1"  autocomplete="off" name="benef_date" value="" class="form-control" placeholder="MM-DD-YYYY" id="dob_benef">
                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Area</label>
                            <div class="clearfix"></div>
                            {!! Form::select('benef_area', $area,'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_area']) !!}
                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Pincode</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="benef_pincode" id="benef_pincode" autocomplete="off" placeholder="Pincode">
                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Mobile No</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="benef_mobile_no" id="benef_mobile_no" autocomplete="off" placeholder="Mobile No">
                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Email</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="benef_email" id="benef_email" autocomplete="off" placeholder="Email">
                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Pan No</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="benef_pan_no" id="benef_pan_no" autocomplete="off" placeholder="Pan No">
                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Nationality</label>
                            <div class="clearfix"></div>
                            {!! Form::select('benef_nationality', $country,1,['autocomplete'=>'off', 'class'=>'form-control','id' => 'benef_nationality']) !!}
                        </div></div>
                    <div class="col-md-5 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Passport No</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="benef_passport_no" id="benef_passport_no" autocomplete="off" placeholder="Passport No">
                        </div></div>
                   <div class="col-md-12 no-padding">
                    <div class="form-group clearfix">
                        <div class="col-md-5 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Gender</label>
                                <div class="clearfix"></div>
                                @php $gender_array = array("0"=> " Select") + $genders->toArray() @endphp
                                {!! Form::select('gender_benef', $gender_array,'',['autocomplete'=>'off', 'class'=>'form-control','id' => 'gender_benef']) !!}

                            </div></div>
                    </div>
                    </div>
                    <div class="col-md-10 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Address</label>
                            <div class="clearfix"></div>
                            <textarea class="form-control" id="benef_address" name="benef_address" placeholder="Address"></textarea>
                        </div></div>
                    <input type="hidden" name="parent_share_holder" id="parent_share_holder" value="{{$share_holder_data->id}}"
                </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-warning" data-dismiss="modal" id="close_benef_modal"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</button>
                <button type="button" class="btn btn-success" onclick="saveBenef()"><i class="fa fa-save" id="benif_spin"></i>&nbsp;&nbsp;Save</button>

            </div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>
<script> $(document).ready(function () {
    $("input[data-attr='date1']").datetimepicker({ format: 'MMM-DD-YYYY' });

});</script>
