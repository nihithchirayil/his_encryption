<div class="col-md-12 padding_sm theadscroll" style="position: relative;height:450px">

<table id="result_data_table" class='table table-condensed theadfix_wrapper table_sm table-col-bordered'
    style="font-size: 12px;">
    <thead>
        <tr class="headerclass"
            style="background-color:#3498db;color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th width='10%'>Sl. No.</th>
            <th width='40%'>Vendor Name</th>
            <th width='10%'>Net Amount</th>
            <th width='10%'>Cost</th>
            <th width='10%'>Selling Price</th>
            <th width='10%'>Margin %</th>
            <th width='10%'>Purchase %</th>
        </tr>
    </thead>
    <tbody>
        <?php
$net_cototal=0.0;
$net_salestotal=0.0;
$netpurchase_total_cost=0.0;
$net_cost_per=0.0;
$net_amounttotal=0.0;
if (count($res)!=0) {
    $i=1;
    foreach ($res as $each) {
        $cal_per_text='';
        $cal_per_font='';
        $cal_per=(100*(floatval($each->sales_rate)-floatval($each->co))/floatval($each->co));
        if(floatval($cal_per)==0){
            $cal_per_text='Equal';
            $cal_per_font='green fa fa-sort-asc';
        }else if(floatval($cal_per)<0){
            $cal_per_text='Loss';
            $cal_per_font='red fa fa-sort-desc';
        }else if(floatval($cal_per)>0){
            $cal_per_text='Profit';
            $cal_per_font='green fa fa-sort-asc';
        }
        if(floatval($cal_per)>100){
            $cal_per=100;
        }

        $purchase_total_cost=(floatval($each->co/$pharmrcy_tot_cost)*100);
        $netpurchase_total_cost+=$purchase_total_cost;
        $net_cost_per+=$cal_per;



        ?>
        <tr title="<?= $cal_per_text ?>">
            <td class="common_td_rules">
                <?= $i ?>
            </td>
            <td class="common_td_rules">
                <?= $each->vendor_name ?>
            </td>
            <td class='td_common_numeric_rules'>
                <?= number_format($each->net_detail, 2, '.', '') ?>
            </td>
            <td class='td_common_numeric_rules'>
                <?= number_format($each->co, 2, '.', '') ?>
            </td>
            <td class='td_common_numeric_rules'>
                <?= number_format($each->sales_rate, 2, '.', '') ?>
            </td>
            <td class='td_common_numeric_rules'><i class="<?= $cal_per_font ?>"></i>
                <?= number_format($cal_per, 2, '.', '') . ' %' ?>
            </td>
            <td class='td_common_numeric_rules'>
                <?= number_format($purchase_total_cost, 2, '.', '') . ' %' ?>
            </td>
        </tr>
        <?php
    $net_cototal+= floatval($each->co);
    $net_salestotal+= floatval($each->sales_rate);
    $net_amounttotal+= floatval($each->net_detail);
    $i++;
    }

        $cal_per_text='';
        $cal_per=(100*(floatval($net_salestotal)-floatval($net_cototal))/floatval($net_cototal));
        $cal_per_font='';
        if(floatval($cal_per)==0){
            $cal_per_text='Equal';
            $cal_per_font='green fa fa-sort-asc';
        }else if(floatval($cal_per)<0){
            $cal_per_text='Loss';
            $cal_per_font='green fa fa-sort-desc';
        }else if(floatval($cal_per)>0){
            $cal_per_text='Profit';
            $cal_per_font='green fa fa-sort-asc';
        }
        if(floatval($cal_per)>100){
            $cal_per=100;
        }
    ?>
        <tr title="<?= $cal_per_text ?>">
            <th class="common_td_rules" colspan="2">Total</th>
            <th class='td_common_numeric_rules'>
                <?= number_format($net_amounttotal, 2, '.', '') ?>
            </th>
            <th class='td_common_numeric_rules'>
                <?= number_format($net_cototal, 2, '.', '') ?>
            </th>
            <th class='td_common_numeric_rules'>
                <?= number_format($net_salestotal, 2, '.', '') ?>
            </th>
            <th style="text-align: center">-</th>
            <th class='td_common_numeric_rules'>
                <?= number_format($netpurchase_total_cost, 2, '.', '') . ' %' ?>
            </th>
        </tr>
        <?php
}else{
    ?>
        <tr>
            <th colspan="6" style="text-align: center;"> No Result Found</th>
        </tr>
        <?php
}
?>
    </tbody>
</table>

</div>
