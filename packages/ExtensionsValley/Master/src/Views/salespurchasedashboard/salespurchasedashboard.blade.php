@extends('Dashboard::dashboard.dashboard')
@section('content-header')

@include('Dashboard::dashboard.partials.headersidebar')
@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-list.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">


@endsection
@section('content-area')


<div class="modal fade" id="getCountsInDetailModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1200px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #337ab7; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getCountsInDetailModelHeader">NA</h4>
            </div>
            <div class="modal-body" style="min-height:500px">
                <div id="getCountsInDetailModelDiv">

                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>

    </div>
</div>
</div>

<div class="modal fade" id="customdatapopmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 500px;width: 100%">
        <div class="modal-content">
            <div class="modal-header" style="background: #337ab7; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Custom Date Range Selection</h4>
            </div>
            <div class="modal-body" style="min-height: 80px">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-4 date_filter_div">
                        <div class="mate-input-box">
                            <label class="filter_label">From Date</label>
                            <input type="text" data-attr="date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="from_date">
                        </div>
                    </div>
                    <div class="col-md-4 date_filter_div">
                        <div class="mate-input-box">
                            <label class="filter_label">To Date</label>
                            <input type="text" data-attr="date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                class="form-control datepicker filters" placeholder="YYYY-MM-DD" id="to_date">
                        </div>
                    </div>
                    <div class="col-md-4 date_filter_div pull-right" style="padding-top: 10px;">
                        <button id="searchdatabtn" onclick="getCustomDateRange()"
                            class="btn btn-primary btn-block">Search <i id="searchdataspin"
                                class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>



<div class="right_col" role="main">
    <input type="hidden" id="daterange_hidden">
    <div class="row padding_sm">
        <input type="hidden" id="date_datahidden" value='<?= $date_array ?>'>
        <div class="col-md-12 padding_sm pull-right">
            <div class="col-md-10 padding_sm">
                <div class="col-md-4 padding_sm">
                    Range Type :<strong><span class='blue' id='range_typedata'></span></strong>
                </div>
                <div class="col-md-3 padding_sm">
                    From Date : <strong><span class='blue' id='from_datadis'></span></strong>
                </div>
                <div class="col-md-3 padding_sm">
                    To Date :<strong><span class='blue' id='to_datadis'></span></strong>
                </div>
            </div>
            <div class="col-md-2 padding_sm pull-right">
                <?= $title ?>
            </div>
        </div>
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
        <input type="hidden" id="company_code" value="<?= $company_code ?>">
    </div>
    <div class="row padding_sm" style="margin-top: 10px">
        <div class="col-md-12 padding_sm">
            <div class="col-md-10 padding_sm">
                <div class="col-md-2 padding_sm">
                    <button onclick="getCustomDates(1)" title="Today" class="btn bg-blue btn-block">Today <i
                            class="fa fa-calendar"></i></button>
                </div>
                <div class="col-md-2 padding_sm">
                    <button onclick="getCustomDates(2)" title="Yesterday" class="btn bg-blue btn-block">Yesterday
                        <i class="fa fa-calendar"></i></button>
                </div>
                <div class="col-md-2 padding_sm">
                    <button onclick="getCustomDates(3)" title="Week Till Date" class="btn bg-blue btn-block">Week
                        Till Date <i class="fa fa-calendar"></i></button>
                </div>
                <div class="col-md-2 padding_sm">
                    <button onclick="getCustomDates(4)" title="Last Week" class="btn bg-blue btn-block">Last
                        Week <i class="fa fa-calendar"></i></button>
                </div>
                <div class="col-md-2 padding_sm">
                    <button onclick="getCustomDates(5)" title="Month Till Date" class="btn bg-blue btn-block">Month
                        Till
                        Date <i class="fa fa-calendar"></i></button>
                </div>
                <div class="col-md-2 padding_sm">
                    <button onclick="getCustomDates(6)" title="Last Month" class="btn bg-blue btn-block">Last
                        Month <i class="fa fa-calendar"></i></button>
                </div>
            </div>
            <div class="col-md-2">
                <div class="col-md-12">
                    <button onclick="getCustomDates(7)" title="Custom Date Range Selection"
                        class="btn btn-primary btn-block">Custom <i class="fa fa-calendar"></i></button>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 padding_sm" style="margin-top: 10px;min-height: 80vh;">
                <div class="col-md-6 padding_sm" style="margin-top: 10px;">
                    <div class="box no-border no-margin">
                        <div class="box-footer revenue_main_shadow" style="min-height: 18vh;">
                            <div class="tile_count" id="totalCollectionsales">
                                <div class="col-md-6 padding_sm" style="margin-bottom: 5px;">
                                    <span class="count_top"><i id="list_data1" class="fa fa-list blue"></i>
                                        Net Collection</span>
                                </div>
                                <div class="col-md-5 padding_sm" style="margin-bottom: 5px;">
                                    <div class="count td_common_dashboard_count" id="total_collection1">0
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6 padding_sm" style="margin-bottom: 5px;">
                                    <span class="count_top"><i id="list_data2" class="fa fa-list blue"></i>
                                        Cost</span>
                                </div>
                                <div class="col-md-6 padding_sm" style="margin-bottom: 5px;">
                                    <div class="count td_common_dashboard_count" id="total_collection2">0
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6 padding_sm" style="margin-bottom: 5px;">
                                    <span class="count_top"><i id="list_data3" class="fa fa-list blue"></i>
                                        Margin</span>
                                </div>
                                <div class="col-md-6 padding_sm" style="margin-bottom: 5px;">
                                    <div class="count td_common_dashboard_count" id="total_collection3">0
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 padding_sm" style="margin-top: 10px;">
                    <div class="box no-border no-margin">
                        <div class="box-footer revenue_main_shadow" style="min-height: 18vh;">
                            <div class="tile_count" id="totalCollectionpurchase">
                                <div class="col-md-5 padding_sm" style="margin-bottom: 5px;">
                                    <span class="count_top"><i id="list_data4" class="fa fa-list blue"></i>
                                        Credit Amount</span>
                                </div>
                                <div class="col-md-5 padding_sm" style="margin-bottom: 5px;">
                                    <div class="count td_common_dashboard_count" id="total_collection4">0
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-bottom: 5px;margin-left:-10px">
                                    <button title="More Details"
                                        onclick="getSalesFullTotal('Credit Amount','credit_amout')"
                                        class="btn btn-primary btn-block" id="listbtncredit_amout" type="button"><i
                                            id="listspincredit_amout" class="fa fa-bar-chart"></i></button>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-5 padding_sm" style="margin-bottom: 5px;">
                                    <span class="count_top"><i id="list_data5" class="fa fa-list blue"></i>
                                        Discount Amount</span>
                                </div>
                                <div class="col-md-5 padding_sm" style="margin-bottom: 5px;">
                                    <div class="count td_common_dashboard_count" id="total_collection5">0
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-5 padding_sm" style="margin-bottom: 5px;">
                                    <span class="count_top"><i id="list_data6" class="fa fa-list blue"></i>
                                        Purchase Amount</span>
                                </div>
                                <div class="col-md-5 padding_sm" style="margin-bottom: 5px;">
                                    <div class="count td_common_dashboard_count" id="total_collection6">0
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm" style="margin-bottom: 5px;margin-left:-10px">
                                    <button title="More Details"
                                        onclick="getSalesFullTotal('Purchase Amount','purchase_amount')"
                                        class="btn btn-primary btn-block" id="listbtnpurchase_amount" type="button"><i
                                            id="listspinpurchase_amount" class="fa fa-bar-chart"></i></button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                    <div class="box no-border no-margin">
                        <div class="box-footer revenue_main_shadow" style="min-height: 57vh;">
                            <div class="text-center col-md-12 padding_sm">
                                <h2
                                    style=" background-color: #337ab7; color: white; margin-top: 5px; font-size: 14px;padding:3px; ">
                                    Location Wise Collection</h2>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm" style="padding: 5px" id="collection_deatils"></div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 padding_sm" style="margin-top: 10px;min-height: 80vh;">
                <div class="col-md-6 padding_sm" style="margin-top: 10px;">
                    <div class="box no-border no-margin">
                        <div class="box-footer revenue_main_shadow" style="min-height: 37vh;">

                            <div class="text-center col-md-10 padding_sm">
                                <h2
                                    style=" background-color: #337ab7; color: white; margin-top: 5px; font-size: 14px;padding:3px; ">
                                    Top Suppliers by Value</h2>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 6px">
                                <button title="More Details"
                                    onclick="getFullDataDrillDown('Top Suppliers by Value','supplier_deatils_value',0)"
                                    class="btn btn-primary btn-block" title="More" id="listbtnsupplier_deatils_value"
                                    type="button"><i id="listspinsupplier_deatils_value"
                                        class="fa fa-bar-chart"></i></button>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm" style="padding: 5px" id="supplier_deatils_value"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 padding_sm" style="margin-top: 10px;">
                    <div class="box no-border no-margin">
                        <div class="box-footer revenue_main_shadow" id="" style="min-height: 37vh;">
                            <div class="text-center col-md-10 padding_sm">
                                <h2
                                    style=" background-color: #337ab7; color: white; margin-top: 5px; font-size: 14px;padding:3px; ">
                                    Top Suppliers by Number</h2>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 6px">
                                <button title="More Details"
                                    onclick="getFullDataDrillDown('Top Suppliers by Number','supplier_deatils_number',0)"
                                    class="btn btn-primary btn-block" title="More" id="listbtnsupplier_deatils_number"
                                    type="button"><i id="listspinsupplier_deatils_number"
                                        class="fa fa-bar-chart"></i></button>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm" style="padding: 5px" id="supplier_deatils_number"></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-4 padding_sm" style="margin-top: 10px;">
                    <div class="box no-border no-margin">
                        <div class="box-footer revenue_main_shadow" style="min-height: 38vh;">
                            <div class="text-center col-md-8 padding_sm">
                                <h2
                                    style=" background-color: #337ab7; color: white; margin-top: 5px; font-size: 14px;padding:3px; ">
                                    Fast Moving Items</h2>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 4px">
                                <button title="More Details"
                                    onclick="getFullDataDrillDown('Fast Moving Items','fast_moving_items',1)"
                                    class="btn btn-primary btn-block" title="More" id="listbtnfast_moving_items"
                                    type="button"><i id="listspinfast_moving_items"
                                        class="fa fa-bar-chart"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">
                                <input onblur="getPharamacyDeatils('fast_moving_items', 1)"
                                    class="form-control td_common_numeric_rules" id="fast_moving_itemsno"
                                    style="margin-top:3px;margin-left: -1px"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    name='fast_moving_itemsno' type="text" autocomplete="off" value="10">
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm" style="padding: 5px" id="fast_moving_items"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding_sm" style="margin-top: 10px;">
                    <div class="box no-border no-margin">
                        <div class="box-footer revenue_main_shadow" style="min-height: 38vh;">
                            <div class="text-center col-md-10 padding_sm">
                                <h2
                                    style=" background-color: #337ab7; color: white; margin-top: 5px; font-size: 14px;padding:3px; ">
                                    Expired Items</h2>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 4px">
                                <button title="More Details"
                                    onclick="getFullDataDrillDown('Expiry Items','expiry_item_deatils',0)"
                                    class="btn btn-primary btn-block" title="More" id="listbtnexpiry_item_deatils"
                                    type="button"><i id="listspinexpiry_item_deatils"
                                        class="fa fa-bar-chart"></i></button>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm" style="padding: 5px" id="expiry_item_deatils"></div>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding_sm" style="margin-top: 10px;">
                    <div class="box no-border no-margin">
                        <div class="box-footer revenue_main_shadow" style="min-height: 38vh;">

                            <div class="text-center col-md-8 padding_sm">
                                <h2
                                    style=" background-color: #337ab7; color: white; margin-top: 5px; font-size: 14px;padding:3px; ">
                                    Near Expiry Items</h2>
                            </div>
                            <div class="col-md-2 padding_sm" style="margin-top: 4px">
                                <button title="More Details"
                                    onclick="getFullDataDrillDown('Near Expiry Items','nearby_expiry_items',1)"
                                    class="btn btn-primary btn-block" title="More" id="listbtnnearby_expiry_items"
                                    type="button"><i id="listspinnearby_expiry_items"
                                        class="fa fa-bar-chart"></i></button>
                            </div>
                            <div class="col-md-2 padding_sm">

                                <input onblur="getPharamacyDeatils('nearby_expiry_items', 1)"
                                    class="form-control td_common_numeric_rules" id="nearby_expiry_itemsno"
                                    style="margin-top:3px;margin-left: -1px"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    name='nearby_expiry_itemsno' type="text" autocomplete="off" value="10">
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm" style="padding: 5px" id="nearby_expiry_items"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @stop
    @section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/highcharts/highcharts.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/salesPurchaseDashBoard.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js') }}"></script>
    @endsection
