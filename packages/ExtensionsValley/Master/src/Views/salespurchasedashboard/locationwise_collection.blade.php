<table id="result_data_table" class='table table-condensed theadfix_wrapper table_sm table-col-bordered'
    style="font-size: 12px;">
    <thead>
        <tr class="headerclass"
            style="background-color:#3498db;color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th width='5%'>Sl. No.</th>
            <th width='25%'>Location Name</th>
            <th width='10%'>Sales</th>
            <th width='10%'>Cash</th>
            <th width='10%'>Card</th>
            <th width='10%'>UPI</th>
            <th width='10%'>Others</th>
            <th width='10%'>Cost</th>
            <th width='10%'>Margin %</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($res)!=0) {
            $i=1;
            $net_totaclcash=0;
            $totcash = 0;
            $totcard = 0;
            $totupi = 0;
            $totother = 0;
            $totcost = 0;
            $totmargin = 0;
            foreach ($res as $each) {
                $net_totaclcash += $each->tot_collection;
                $totcash += $each->cash;
                $totcard += $each->card;
                $totupi += $each->upi;
                $totother += $each->other;
                $totcost += $each->sales_cost;
                $totmargin += $each->margin;
                $cal_per=$each->margin;
                if(floatval($each->margin)<0){
                $cal_per=0;
                }
                ?>

        <tr>
            <td class="common_td_rules">
                <?=$i?>
            </td>
            <td class="common_td_rules">
                <?=$each->location_name?>
            </td>
            <td class='td_common_numeric_rules'>
                <?=$each->tot_collection?>
            </td>
            <td class='td_common_numeric_rules'>
                <?=$each->cash?>
            </td>
            <td class='td_common_numeric_rules'>
                <?=$each->card?>
            </td>
            <td class='td_common_numeric_rules'>
                <?=$each->upi?>
            </td>
            <td class='td_common_numeric_rules'>
                <?=$each->other?>
            </td>
            <td class='td_common_numeric_rules'>
                <?=$each->sales_cost?>
            </td>
            <td class='td_common_numeric_rules'>
                <?=$cal_per?>
            </td>
        </tr>
        <?php
        $i++;
            }
            ?>
        <tr class="headerclass"
            style="background-color:#3498db;color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th class="common_th_rules" colspan="2">Total</th>
            <th class='th_common_numeric_rules'style="text-align: right">
                {{ $net_totaclcash }}
            </th>
            <th class='th_common_numeric_rules'style="text-align: right">
                {{ $totcash }}
            </th>
            <th class='th_common_numeric_rules'style="text-align: right">
                {{ $totcard }}
            </th>
            <th class='th_common_numeric_rules'style="text-align: right">
                {{ $totupi }}
            </th>
            <th class='th_common_numeric_rules'style="text-align: right">
                {{ $totother }}
            </th>
            <th class='th_common_numeric_rules' style="text-align: right">
                {{ $totcost }}
            </th>
            <th class='th_common_numeric_rules' style="text-align: right">
                -
            </th>
        </tr>
        <?php
        }else{
            ?>
        <tr>
            <th colspan="9" style="text-align: center;"> No Result Found</th>
        </tr>
        <?php
        }
        ?>
    </tbody>
</table>

