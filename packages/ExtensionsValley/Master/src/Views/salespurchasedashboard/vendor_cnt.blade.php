@if(intval($limit)==0)
<div class="col-md-12 padding_sm theadscroll" style="position: relative;height:450px">
    @endif
    <table id="result_data_table" class='table table-condensed theadfix_wrapper table_sm table-col-bordered'
        style="font-size: 12px;">
        <thead>
            <tr class="headerclass"
                style="background-color:#3498db;color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th width='75%'>Supplier</th>
                <th width='30%'>Count</th>
            </tr>
        </thead>
        <tbody>
            <?php
        if (count($res)!=0) {
            $i=1;
            foreach ($res as $each) {
                ?>
            <tr>
                <td class="common_td_rules">
                    <?=$each->vendor_name?>
                </td>
                <td class='td_common_numeric_rules'>
                    <?=$each->cnt?>
                </td>
            </tr>
            <?php
        $i++;
            }
        }else{
            ?>
            <tr>
                <th colspan="2" style="text-align: center;"> No Result Found</th>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    @if(intval($limit)==0)
</div>
@endif
