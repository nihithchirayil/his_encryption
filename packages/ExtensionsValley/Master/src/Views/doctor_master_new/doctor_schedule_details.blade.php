<div class="box no-border no-margin">
    <div class="box-body clearfix">
        <div class="row padding_sm" style="padding:15px !important;">
            <ul class="nav nav-tabs sm_nav" style="margin-top:-13px;font-size:11px;">
                <li class="active">
                    <a data-toggle="tab" href="#doctorTimeSlotSetup" style="border-radius: 0px;" aria-expanded="false">
                        <i class="fa fa-calendar" aria-hidden="true"
                            style="margin-top:0px;float: left;margin-right:4px;"></i>
                        Doctor time slot setup
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#doctorwalkin" style="border-radius: 0px;" aria-expanded="false">
                        <i class="fa fa-walking" aria-hidden="true"></i>
                        Walkin slots
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="doctorTimeSlotSetup" class="tab-pane active">

                    @include('Master::doctor_master_new.doctor_time_slot_setup')

                </div>
                <div id="doctorwalkin" class="tab-pane">

                    @include('Master::doctor_master_new.walkin_slots')

                </div>
            </div>
        </div>
    </div>
</div>
