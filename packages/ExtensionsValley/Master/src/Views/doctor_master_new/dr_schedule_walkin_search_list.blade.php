<div class="theadscroll" style="position: relative; height: 440px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_blue">
                <th class="common_td_rules" width="5%"><i class="fa fa-edit"></i></th>
                <th class="common_td_rules" width="20%">Walk Count</th>
                <th class="common_td_rules" width="75%">Repeat desc</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($resultData) != 0) {
                foreach ($resultData as $list) {
                    ?>
            <tr id="rowWalkingDrSchedule{{$list->id}}">
                <td>
                    <button style="padding: 0px 4px" id="deletedrwakinscheduleBtn{{$list->id}}" type="button"
                        class="btn bg-red" onclick="delete_dr_wakin_schedule({{$list->id}});"><i
                            id="deletedrwakinscheduleSpin{{$list->id}}" class="fa fa-times"></i></button>
                </td>
                <td class="common_td_rules">{{ $list->walkin_count }}</td>
                <td class="common_td_rules">
                    @if($list->repeats_on_sunday == 1)
                    Sun,
                    @endif
                    @if($list->repeats_on_monday == 1)
                    Mon,
                    @endif
                    @if($list->repeats_on_tuesday == 1)
                    Tue,
                    @endif
                    @if($list->repeats_on_wednesday == 1)
                    Wed,
                    @endif
                    @if($list->repeats_on_thursday == 1)
                    Thu,
                    @endif
                    @if($list->repeats_on_friday == 1)
                    Fri,
                    @endif
                    @if($list->repeats_on_saturday == 1)
                    Sat
                    @endif
                </td>
            </tr>
            <?php
                }
            }else{
                ?>
            <tr>
                <td colspan="3" style="text-align: center">No Records Found</td>
            </tr>
            <?php
            }
            ?>

        </tbody>
    </table>
</div>
