@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="token" value="{{ csrf_token() }}">
<input type="hidden" id="doctor_id_hidden" value="0">
<input type="hidden" id="doctoruser_id_hidden" value="0">
<input type="hidden" id="schedule_id_hidden" value='0'>



<div class="modal fade" id="commonListModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="width: 90%">
        <div class="modal-content">
            <div class="modal-header" style="background: #337ab7; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="commonListModelHeader">NA</h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
                <div class="col-md-12 padding_sm" id="commonListModelDiv">

                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="right_col" style="min-height: 700px !important;">
    <div class="container-fluid">
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="col-md-5 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 600px;">
                            <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label class="blue">Doctor</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="doctor_name">
                                    <div id="doctor_name_AjaxDiv" class="ajaxSearchBox"
                                        style="margin-top:13px;z-index: 99999;margin-left: -4px;max-height:250px"></div>
                                    <input type="hidden" autocomplete="off" class="form-control" id="doctor_id">
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm" style="margin-top: 10px">
                                <div class="mate-input-box">
                                    <label class="blue">Speciality</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="speciality_name">
                                    <div id="speciality_name_AjaxDiv" class="ajaxSearchBox"
                                        style="margin-top:13px;z-index: 99999;margin-left: -4px;max-height:250px"></div>
                                    <input type="hidden" autocomplete="off" class="form-control" id="speciality_id">
                                </div>
                            </div>
                            <div class="col-md-2 pull-right padding_sm" style="margin-top: 22px">
                                <button class="btn btn-primary btn-block" id="getDoctrorListBtn"
                                    onclick="getDoctorList();">
                                    <i id="getDoctrorListSpin" class="fa fa-search"></i> Search
                                </button>
                            </div>
                            <div class="col-md-2 pull-right padding_sm" style="margin-top: 22px">
                                <button class="btn btn-warning btn-block" onclick="resetForm();">
                                    <i class="fa fa-recycle"></i> Reset
                                </button>
                            </div>
                            <div class="col-md-12 padding_sm" id="getDoctrorListData">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 padding_sm" id="patientappoinment_div">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 600px;">
                            <div class="col-md-12 padding_sm text-center blue">
                                <strong id="appointment_title">Manage Doctor Profile</strong>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row padding_sm" style="padding:15px !important;min-height: 480px">
                                <ul class="nav nav-tabs sm_nav" style="margin-top:-13px;font-size:11px;">
                                    <li class="active">
                                        <a id="BasicInformationsli" data-toggle="tab" href="#collapseOne"
                                            style="border-radius: 0px;" aria-expanded="false">
                                            <i class="fa fa-info" aria-hidden="true"
                                                style="margin-top:0px;float: left;margin-right:4px;"></i>
                                            Basic Info
                                        </a>
                                    </li>
                                    <li class="">
                                        <a id="DoctorUserProfileli" data-toggle="tab" href="#collapsefour"
                                            style="border-radius: 0px;" aria-expanded="false">
                                            <i class="fa fa-user-md" aria-hidden="true"></i>
                                            Profile
                                        </a>
                                    </li>
                                    <li class="">
                                        <a id="FollowupRuleli" data-toggle="tab" href="#collapseTwo"
                                            style="border-radius: 0px;" aria-expanded="false">
                                            <i class="fa fa-repeat" aria-hidden="true"></i>
                                            Followup Rule
                                        </a>
                                    </li>
                                    <li class="">
                                        <a id="RoomDetailsli" data-toggle="tab" href="#collapseThree"
                                            style="border-radius: 0px;" aria-expanded="false">
                                            <i class="fa fa-home" aria-hidden="true"></i>
                                            Room Details
                                        </a>
                                    </li>
                                    <li class="">
                                        <a id="DischargeSummaryli" data-toggle="tab" href="#collapseFive"
                                            style="border-radius: 0px;" aria-expanded="false">
                                            <i class="fa fa-sticky-note" aria-hidden="true"></i>
                                            Summary Headers
                                        </a>
                                    </li>
                                    <li class="">
                                        <a id="MedicationDetailsli" data-toggle="tab" href="#collapseSix"
                                            style="border-radius: 0px;" aria-expanded="false">
                                            <i class="fa fa-sticky-note" aria-hidden="true"></i>
                                            Medication Headers
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div id="collapseOne" class="tab-pane active">
                                        <div class="card-body">
                                            <div class="col-md-6 padding_sm" style="margin-top: 12px">
                                                <div class="mate-input-box">
                                                    <label class="red" for="">Doctor Name</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" autocomplete="off" class="form-control"
                                                        id="add_doctor_name">
                                                </div>
                                            </div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 10px">
                                                <div class="mate-select-box">
                                                    <label class="red" for="">Gender</label>
                                                    @php
                                                    $gender_list =
                                                    \DB::table('gender')->where('name','!=','')->orderBy('name',
                                                    'ASC')->pluck('name','id');
                                                    @endphp
                                                    <div class="clearfix"></div>
                                                    {!! Form::select('doctor_gender',$gender_list,null, ['class'
                                                    => 'form-control select2',
                                                    'id' =>'doctor_gender', 'placeholder' => 'select']) !!}
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 10px">
                                                <div class="mate-input-box">
                                                    <label class="red" for="">Token prefix</label>

                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control" name="token_prefix"
                                                        id="token_prefix" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 10px">
                                                <div class="mate-input-box">
                                                    <label class="red" for="">Speciality</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" autocomplete="off" class="form-control"
                                                        id="speciality_name_add">
                                                    <div id="speciality_name_add_AjaxDiv" class="ajaxSearchBox"
                                                        style="margin-top:13px;z-index: 99999;margin-left: -4px;max-height:250px">
                                                    </div>
                                                    <input type="hidden" autocomplete="off" class="form-control"
                                                        id="speciality_id_add">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue" for="">Email</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" onblur="isEmail()" autocomplete="off"
                                                        class="form-control" id="add_doctor_email">
                                                </div>
                                            </div>
                                            <div class="col-md-4 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue" for="">Phone</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control" id="add_doctor_phone"
                                                        autocomplete="off"
                                                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                                </div>
                                            </div>
                                            <div class="col-md-4 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue" for="">Qualification</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control"
                                                        id="add_doctor_qualification" />
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-10 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue" for="">Address</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control" id="add_doctor_address" />
                                                </div>
                                            </div>
                                            <div class="col-md-2 padding_sm" style="margin-top:25px">
                                                <div class="checkbox checkbox-info inline no-margin">
                                                    <input title="IS Surgeon" class="chkbox" type="checkbox"
                                                        id="is_surgeon" value="1">
                                                    <label class="blue" for="is_surgeon">Surgeon</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-2 padding_sm">
                                                <div class="checkbox checkbox-info inline no-margin">
                                                    <input title="MLC Doctor" class="chkbox" type="checkbox" id="is_mlc"
                                                        value="1">
                                                    <label class="blue" for="is_mlc">MLC Doctor</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3 padding_sm">
                                                <div class="checkbox checkbox-info inline no-margin">
                                                    <input title="Active" class="chkbox" type="checkbox"
                                                        id="is_casuality" value="1">
                                                    <label class="blue" for="is_casuality">Casuality Doctor</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3 padding_sm">
                                                <div class="checkbox checkbox-info inline no-margin">
                                                    <input title="Hide from Appointments" class="chkbox" type="checkbox"
                                                        id="hide_from_appointments" value="1">
                                                    <label class="blue" for="hide_from_appointments">Hide from
                                                        Appointments</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4 padding_sm">
                                                <div class="checkbox checkbox-info inline no-margin">
                                                    <input title="Show in Appointments only" class="chkbox"
                                                        type="checkbox" id="show_in_appointments_only" value="1">
                                                    <label class="blue" for="show_in_appointments_only">Show in
                                                        Appointments
                                                        only</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>


                                        </div>
                                    </div>
                                    <div id="collapsefour" class="tab-pane">
                                        <div class="card-body">
                                            <div class="col-md-3 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="red" for="">UserName</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" onblur="checkDoctorUserName()" autocomplete="off"
                                                        class="form-control" id="add_doctor_user_name">
                                                </div>
                                            </div>
                                            <div class="col-md-5 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="red" for="">Email</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control" id="add_doctor_login_email"
                                                        autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="col-md-4 padding_sm" style="margin-top: 20px">
                                                <div class="checkbox checkbox-info inline no-margin">
                                                    <input title="Same as above email" onclick="same_as_email()"
                                                        class="chkbox" type="checkbox" id="same_as_email" value="1">
                                                    <label class="blue" for="same_as_email">Same as Basic Info. email</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box" style="height:100px !important">
                                                    <label class="blue">Additional Information</label>
                                                    <div class="clearfix"></div>
                                                    <textarea class="form-control" name="additional_information"
                                                        id="additional_information"></textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                <div class="mate-select-box">
                                                    <label class="blue">Medical licenced</label>
                                                    @php
                                                    $licenced_country_list =
                                                    \DB::table('country')->orderBy('name',
                                                    'ASC')->pluck('name','id');
                                                    @endphp
                                                    <div class="clearfix"></div>
                                                    {!!
                                                    Form::select('medical_licenced',$licenced_country_list,1,
                                                    ['class' => 'form-control
                                                    select2', 'id' =>'medical_licenced', 'placeholder' =>
                                                    'select']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                <div class="mate-select-box">
                                                    <label class="blue">Medical licenced state</label>
                                                    @php
                                                    $licenced_state_list = \DB::table('state')->orderBy('name',
                                                    'ASC')->pluck('name','id');
                                                    @endphp
                                                    <div class="clearfix"></div>
                                                    {!!
                                                    Form::select('medical_licenced_state',$licenced_state_list,1,
                                                    ['class' =>
                                                    'form-control select2', 'id' =>'medical_licenced_state',
                                                    'placeholder' =>
                                                    'select']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue">licence number</label>

                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control" name="license_number"
                                                        id="license_number" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 25px">
                                                <div class="checkbox checkbox-info inline no-margin">
                                                    <input title="Active" class="chkbox" type="checkbox"
                                                        id="login_active" value="1">
                                                    <label class="blue" for="login_active">Active</label>
                                                </div>
                                            </div>
                                            {{-- <div class="col-md-6 padding_sm" style="margin-top: 25px">
                                                <div class="checkbox checkbox-info inline no-margin">
                                                    <button type="button" class="btn btn-block primary">Summary Header And Footer
                                                    </button>
                                                </div>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div id="collapseTwo" class="tab-pane">
                                        <div class="card-body">
                                            <div class="col-md-12 padding_sm" style="margin-top: 25px">
                                                <div class="checkbox checkbox-info inline no-margin">
                                                    <input onclick="checkRoomStatus('speciality_wise_followup',3)"
                                                        title="Speciality Wise Followup" class="chkbox" type="checkbox"
                                                        id="speciality_wise_followup" value="1">
                                                    <label class="blue" for="speciality_wise_followup">Speciality Wise
                                                        Followup</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue">Speciality Wise No. free days</label>
                                                    <div class="clearfix"></div>
                                                    <input readonly style="background: #FFF"
                                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                        type="text" name="sp_wise_no_free_days" autocomplete="off"
                                                        class="form-control" id="sp_wise_no_free_days">
                                                </div>
                                            </div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue">Speciality Wise No. free times</label>
                                                    <div class="clearfix"></div>
                                                    <input readonly style="background: #FFF"
                                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                        type="text" name="sp_wise_no_free_times" autocomplete="off"
                                                        class="form-control" id="sp_wise_no_free_times">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue">No. free days</label>
                                                    <div class="clearfix"></div>
                                                    <input
                                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                        type="text" name="no_free_days" autocomplete="off"
                                                        class="form-control" id="no_free_days">
                                                </div>
                                            </div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue">No. free times</label>
                                                    <div class="clearfix"></div>
                                                    <input
                                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                        type="text" name="no_free_times" autocomplete="off"
                                                        class="form-control" id="no_free_times">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div id="collapseThree" class="tab-pane">
                                        <div class="card-body">
                                            <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                <div class="checkbox checkbox-info inline no-margin">
                                                    <input onclick="checkRoomStatus('has_op_rights',1)"
                                                        title="Has OP Rights" class="chkbox" type="checkbox"
                                                        id="has_op_rights" value="1">
                                                    <label class="blue" for="has_op_rights">Has OP Rights</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                <div class="checkbox checkbox-info inline no-margin">
                                                    <input onclick="checkRoomStatus('has_ip_rights',2)"
                                                        title="Has IP Rights" class="chkbox" type="checkbox"
                                                        id="has_ip_rights" value="1">
                                                    <label class="blue" for="has_ip_rights">Has IP Rights</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue">OP Room Number</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" style="background: #FFF" readonly
                                                        name="op_room_no" autocomplete="off" class="form-control"
                                                        id="op_room_no">
                                                </div>
                                            </div>
                                            <div class="col-md-6 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue">IP Room Number</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" style="background: #FFF" readonly
                                                        name="ip_room_no" autocomplete="off" class="form-control"
                                                        id="ip_room_no">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 padding_sm" style="margin-top: 5px">
                                                <div class="mate-input-box">
                                                    <label class="blue">Screening Room Number</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" name="screening_room_no" autocomplete="off"
                                                        class="form-control" id="screening_room_no">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="collapseFive" class="tab-pane">
                                        <div class="card-body">
                                            <div class="col-md-12 text-center box-body">
                                                    <label class="blue">Doctor Header</label>
                                                    <textarea class="form-control texteditortyne" id="doctor_header" placeholder="Doctor Header"/></textarea>
                                            </div>
                                            <div class="col-md-12 text-center box-body">
                                                    <label class="blue">Doctor Footer</label>
                                                    <textarea class="form-control texteditortyne" id="doctor_footer" placeholder="Doctor Footer"/></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="collapseSix" class="tab-pane">
                                        <div class="card-body">
                                            <div class="col-md-6 padding_sm" style="margin-top: 10px">
                                                <div class="mate-input-box">
                                                    <label class="blue" for="">Prescription Department Head</label>
                                                    <div class="clearfix"></div>
                                                    <input type="text" class="form-control" name="prescription_department_head" id="prescription_department_head">
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-center box-body">
                                                    <label class="blue">Medication Note</label>
                                                    <textarea class="form-control texteditortyne" id="medication_note" placeholder="Doctor Header"/></textarea>
                                            </div>
                                            <div class="col-md-12 text-center box-body">
                                                    <label class="blue">Medication Footer</label>
                                                    <textarea class="form-control texteditortyne" id="medication_footer_text" placeholder="Doctor Footer"/></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="col-md-3 padding_sm pull-right">
                                    <button type="button" id="saveSalaryMasterBtn" onclick="saveSalaryMaster()"
                                        class="btn btn-primary btn-block">Save <i class="fa fa-save"
                                            id="saveSalaryMasterSpin"></i></button>
                                </div>
                                <div class="col-md-3 padding_sm pull-right">
                                    <button type="button" onclick="resetDoctorProfile()"
                                        class="btn btn-warning btn-block">Reset <i class="fa fa-recycle"></i></button>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


@stop
@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/doctor_master_new.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/doctor_master_pricing.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/doctor_master_new_schedule.js') }}"></script>
@endsection
