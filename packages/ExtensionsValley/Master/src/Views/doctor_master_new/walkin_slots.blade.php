<div class="col-md-6 padding_sm" style="margin-top: 5px">
    <div class="box no-border no-margin">
        <div class="box-footer"
            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 450px;">
            <div class="col-md-3 padding_sm" style="margin-top: 5px">
                <div class="mate-input-box">
                    <label for="">Walkin Count</label>
                    <div class="clearfix"></div>
                    <input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        type="text" class="form-control" id="dr_time_sloat_walkin_count" autocomplete="off">
                </div>
            </div>
            <div class="col-md-3 padding_sm pull-right" style="margin-top: 25px">
                <button type="button" id="saveDrWalkinBtn" onclick="saveDrWalkin()"
                    class="btn btn-primary btn-block">Save <i id="saveDrWalkinSpin" class="fa fa-save"></i></button>
            </div>
            <div class="col-md-3 padding_sm pull-right" style="margin-top: 25px">
                <button type="button" onclick="resetDrWalkin()"
                    class="btn btn-warning btn-block">Reset <i class="fa fa-recycle"></i></button>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-12 padding_sm">
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input onclick="checkAllWalkinDays()" type="checkbox" id="dr_time_sloat_walkin_select_all"
                            value="1">
                        <label class="green" for="dr_time_sloat_walkin_select_all">All</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="chk_dr_walkin" type="checkbox" id="dr_time_sloat_walkin_mon" value="1">
                        <label class="green" for="dr_time_sloat_walkin_mon">Monday</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="chk_dr_walkin" type="checkbox" id="dr_time_sloat_walkin_tue" value="1">
                        <label class="green" for="dr_time_sloat_walkin_tue">Tuesday</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="chk_dr_walkin" type="checkbox" id="dr_time_sloat_walkin_wed" value="1">
                        <label class="green" for="dr_time_sloat_walkin_wed">Wednesday</label>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="chk_dr_walkin" type="checkbox" id="dr_time_sloat_walkin_thu" value="1">
                        <label class="green" for="dr_time_sloat_walkin_thu">Thursday</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="chk_dr_walkin" type="checkbox" id="dr_time_sloat_walkin_fri" value="1">
                        <label class="green" for="dr_time_sloat_walkin_fri">Friday</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="chk_dr_walkin" type="checkbox" id="dr_time_sloat_walkin_sat" value="1">
                        <label class="green" for="dr_time_sloat_walkin_sat">Saturday</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="chk_dr_walkin" type="checkbox" id="dr_time_sloat_walkin_sun" value="1">
                        <label class="green" for="dr_time_sloat_walkin_sun">Sunday</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6 padding_sm" style="margin-top: 5px">
    <div class="box no-border no-margin">
        <div class="box-footer"
            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 450px;">
            <div id="walkin_container_data"></div>
        </div>

    </div>
</div>
