<div class="theadscroll" style="position: relative; height: 440px;">
    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_blue">
                <th style="text-align: center" width="3%"><i class="fa fa-edit"></i></th>
                <th style="text-align: center" width="3%"><i class="fa fa-trash"></i></th>
                <th class="common_td_rules" width="8%">From date</th>
                <th class="common_td_rules" width="8%">From time</th>
                <th class="common_td_rules" width="8%">To Time</th>
                <th class="common_td_rules" width="8%">Ends on</th>
                <th class="common_td_rules" width="8%">Grace Token</th>
                <th class="common_td_rules" width="8%">Repeats</th>
                <th class="common_td_rules" width="35%">Repeat desc</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($resultData) != 0) {
                foreach ($resultData as $list) {
                    ?>
            <tr id="rowDrSchedule{{$list->id}}">
                <td style="text-align: center">
                    <button id="editDrScheduleBtn{{$list->id}}" style="padding: 0px 4px" type="button"
                        class="btn btn-warning" onclick="edit_dr_schedule('{{$list->id}}');"><i
                            id="editDrScheduleSpin{{$list->id}}" class="fa fa-edit"></i></button>

                </td>
                <td style="text-align: center">
                    <button style="padding: 0px 4px" id="deletedrschedulebtn{{$list->id}}" type="button"
                        class="btn bg-red" onclick="delete_dr_schedule({{$list->id}});"><i
                            id="deletedrschedulespin{{$list->id}}" class="fa fa-times"></i></button>
                </td>
                <td class="common_td_rules">{{ $list->from_date }}</td>
                <td class="common_td_rules">{{ $list->from_time }}</td>
                <td class="common_td_rules">{{ $list->to_time }}</td>
                <td class="common_td_rules">{{ $list->ends_on_date }}</td>
                <td class="common_td_rules">{{ $list->grace_token }}</td>
                <td class="common_td_rules">{{ $list->repeats_every_string }}</td>
                <td class="common_td_rules">
                    @if($list->repeats_on_sunday == 1)
                    Sun,
                    @endif
                    @if($list->repeats_on_monday == 1)
                    Mon,
                    @endif
                    @if($list->repeats_on_tuesday == 1)
                    Tue,
                    @endif
                    @if($list->repeats_on_wednesday == 1)
                    Wed,
                    @endif
                    @if($list->repeats_on_thursday == 1)
                    Thu,
                    @endif
                    @if($list->repeats_on_friday == 1)
                    Fri,
                    @endif
                    @if($list->repeats_on_saturday == 1)
                    Sat
                    @endif
                </td>
            </tr>
            <?php
                }
            }else{
                ?>
            <tr>
                <td colspan="9" style="text-align: center">No Records Found</td>
            </tr>
            <?php
            }
            ?>

        </tbody>
    </table>
</div>
