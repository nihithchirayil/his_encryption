<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var doctor_id = $('#doctor_id').val();
                var speciality_id = $('#speciality_id').val();
                var param = {
                    _token: token,
                    speciality_id: speciality_id,
                    doctor_id: doctor_id
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $('#getDoctrorListBtn').attr('disabled', true);
                        $('#getDoctrorListSpin').removeClass('fa fa-search');
                        $('#getDoctrorListSpin').addClass('fa fa-spinner fa-spin');
                        $("#getDoctrorListData").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        $('#getDoctrorListData').html(data);
                    },
                    complete: function() {
                        $('#getDoctrorListBtn').attr('disabled', false);
                        $('#getDoctrorListSpin').removeClass('fa fa-spinner fa-spin');
                        $('#getDoctrorListSpin').addClass('fa fa-search');
                        $("#getDoctrorListData").LoadingOverlay("hide");
                    },
                    error: function() {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
            return false;
        });

    });
</script>
<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table_sm theadfix_wrapper no-border" style="">
        <thead>
            <tr class="table_header_blue">
                <th width="50%">Doctor Name</th>
                <th width="41%">Speciality</th>
                <th style="text-align: center" width="3%"><i class="fa fa-user-md"></i></th>
                <th style="text-align: center" width="3%"><i class="fa fa-money"></i></th>
                <th style="text-align: center" width="3%"><i class="fa fa-credit-card"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($doctor_list) != 0) {
                foreach ($doctor_list as $list) {
                    ?>
            <tr style="cursor: pointer">
                <td class="editDoctorMasterRow editDoctorMasterRow{{ $list->doctor_id }}"
                    id="doctorname{{ $list->doctor_id }}" onclick="editDoctorMaster({{ $list->doctor_id }})"
                    class="common_td_rules">{{ $list->doctor_name }}
                </td>
                <td class="editDoctorMasterRow editDoctorMasterRow{{ $list->doctor_id }}"
                    onclick="editDoctorMaster({{ $list->doctor_id }})" class="common_td_rules">{{ $list->speciality }}
                </td>
                <td style="text-align: center"><button id="doctorScheduleBtn{{ $list->doctor_id }}"
                        style="padding: 0px 4px" onclick="doctorSchedule({{ $list->doctor_id }})"
                        title="Doctor Schedule" class="btn btn-primary"><i id="doctorScheduleSpin{{ $list->doctor_id }}"
                            class="fa fa-user-md"></i></button></td>
                <td style="text-align: center"><button id="opPricincingBtn{{ $list->doctor_id }}"
                        style="padding: 0px 4px" title="OP Pricing" onclick="getOpPricing({{ $list->doctor_id }})"
                        class="btn btn-warning"><i id="opPricincingSpin{{ $list->doctor_id }}"
                            class="fa fa-money"></i></button></td>
                <td style="text-align: center"><button onclick="getIpPricing({{ $list->doctor_id }})"
                        id="ipPricincingBtn{{ $list->doctor_id }}" style="padding: 0px 4px" title="IP Pricing"
                        class="btn btn-success"><i id="ipPricincingSpin{{ $list->doctor_id }}"
                            class="fa fa-credit-card"></i></button></td>
            </tr>
            <?php
                }
            }else{
                ?>
            <tr>
                <td colspan="3" class="re-records-found">No Records Found</td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<div class="col-md-12 text-center" style="margin-top: 35px;">
    <ul class="pagination purple_pagination pull-right">
        {!! $page_links !!}
    </ul>
</div>
