<div class="box no-border no-margin">
    <div class="box-footer"
        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 420px;">
        <div class="col-md-3 padding_sm">
            <div class="mate-input-box">
                <label class="">Ip consultation item</label>
                <div class="clearfix"></div>
                <input type="hidden" id="ip_consultation_item_id" value="0">
                <input onkeyup="searchIpConsultationItem(event, 'ip_consultation_itemAjaxDiv', 1)" class=" form-control
                    hidden_search ip_pricing_setup" value="" autocomplete="off" type="text" id="ip_consultation_item"
                    name="ip_consultation_item">
                <div id="ip_consultation_itemAjaxDiv" style="margin-top: 26px; margin-left: -4px;"
                    class="ajaxSearchBox"></div>
                <input class="filters" type="hidden" name="ip_consultation_item_hidden" value=""
                    id="ip_consultation_item_hidden">
            </div>
        </div>
        <div class="col-md-3 padding_sm">
            <div class="mate-input-box">
                <label for="">Ip consultation amount</label>
                <div class="clearfix"></div>
                <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                    class="form-control ip_pricing_setup" name="ip_consultation_amount" id="ip_consultation_amount" />
            </div>
        </div>
        <div class="col-md-3 padding_sm">
            <div class="mate-input-box">
                <label class="">Ip cross consultation item</label>
                <div class="clearfix"></div>
                <input type="hidden" id="ip_cross_consultation_item_id" value="0">
                <input onkeyup="searchIpConsultationItem(event, 'ip_cross_consultation_itemAjaxDiv', 2)"
                    class="form-control hidden_search ip_pricing_setup" value="" autocomplete="off" type="text"
                    id="ip_cross_consultation_item" name="ip_cross_consultation_item">
                <div id="ip_cross_consultation_itemAjaxDiv" style="margin-top: 26px; margin-left: -4px;"
                    class="ajaxSearchBox"></div>
                <input class="filters" type="hidden" name="ip_cross_consultation_item_hidden" value=""
                    id="ip_cross_consultation_item_hidden">
            </div>
        </div>
        <div class="col-md-3 padding_sm">
            <div class="mate-input-box">
                <div class="clearfix"></div>
                <label for="">Ip cross consultation amount</label>
                <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                    class="form-control ip_pricing_setup" name="ip_cross_consultation_amount"
                    id="ip_cross_consultation_amount" />
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-2 padding_sm pull-right" style="margin-top: 25px">
            <button type="button" id="saveIpPricingBtn" onclick="saveIpPricing()" class="btn btn-primary btn-block">Save
                <i id="saveIpPricingSpin" class="fa fa-save"></i></button>
        </div>
    </div>
</div>
