<div class="box-body clearfix" style="padding:15px !important;min-height: 430px">
    <div class="row padding_sm">
        <ul class="nav nav-tabs sm_nav" style="margin-top:-13px;font-size:11px;">
            <li class="active">
                <a data-toggle="tab" href="#collapseOne" style="border-radius: 0px;" aria-expanded="false">
                    <i class="fa fa-info-circle" aria-hidden="true"
                        style="margin-top:0px;float: left;margin-right:4px;"></i>
                    Basic Informations
                </a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#collapseTwo" style="border-radius: 0px;" aria-expanded="false">
                    <i class="fa fa-repeat" aria-hidden="true"></i>
                    Followup Rule
                </a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#collapseThree" style="border-radius: 0px;" aria-expanded="false">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    Room Details
                </a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#collapsefour" style="border-radius: 0px;" aria-expanded="false">
                    <i class="fa fa-user-md" aria-hidden="true"></i>
                    Doctor User Profile
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="collapseOne" class="tab-pane active">

                <div class="card-body">
                    <input type="hidden" name="doctor_id_hidden" id="doctor_id_hidden" value="" />

                    <div class="col-md-6 padding_sm" style="margin-top: 12px">
                        <div class="mate-input-box">
                            <label for="">Doctor Name</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="add_doctor_name">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 10px">
                        <div class="mate-input-box">
                            <label for="">Gender</label>
                            @php
                            $gender_list = \DB::table('gender')->where('name','!=','')->orderBy('name',
                            'ASC')->pluck('name','id');
                            @endphp
                            <div class="clearfix"></div>
                            {!! Form::select('doctor_gender',$gender_list,null, ['class' => 'form-control select2',
                            'id' =>'doctor_gender', 'placeholder' => 'select']) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6 padding_sm" style="margin-top: 10px">
                        <div class="mate-input-box">
                            <label for="">Token prefix</label>

                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="token_prefix" id="token_prefix" />
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 10px">
                        <div class="mate-input-box">
                            <label for="">Speciality</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" id="speciality_name_add">
                            <div id="speciality_name_add_AjaxDiv" class="ajaxSearchBox"
                                style="margin-top:13px;z-index: 99999;margin-left: -4px;max-height:250px">
                            </div>
                            <input type="hidden" autocomplete="off" class="form-control" id="speciality_id_add">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Email</label>
                            <div class="clearfix"></div>
                            <input type="text" onblur="isEmail()" autocomplete="off" class="form-control"
                                id="add_doctor_email">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Phone</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" id="add_doctor_phone" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Address</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" id="add_doctor_address" />
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Qualification</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" id="add_doctor_qualification" />
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Medical licenced</label>
                            @php
                            $licenced_country_list = \DB::table('country')->orderBy('name',
                            'ASC')->pluck('name','id');
                            @endphp
                            <div class="clearfix"></div>
                            {!! Form::select('medical_licenced',$licenced_country_list,1, ['class' => 'form-control
                            select2', 'id' =>'medical_licenced', 'placeholder' => 'select']) !!}
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Medical licenced state</label>
                            @php
                            $licenced_state_list = \DB::table('state')->orderBy('name', 'ASC')->pluck('name','id');
                            @endphp
                            <div class="clearfix"></div>
                            {!! Form::select('medical_licenced_state',$licenced_state_list,1, ['class' =>
                            'form-control select2', 'id' =>'medical_licenced_state', 'placeholder' =>
                            'select']) !!}
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">license number</label>

                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="license_number" id="license_number" />
                        </div>
                    </div>


                </div>

            </div>
            <div id="collapseTwo" class="tab-pane">

                <div class="card-body">
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Hospital wise renewal days</label>
                            <div class="clearfix"></div>
                            <input type="text" name="hospital_wise_renewal_days" autocomplete="off" class="form-control"
                                id="hospital_wise_renewal_days">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <label>
                        <h5>Speciality Wise</h5>
                    </label>
                    <div class="clearfix"></div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Speciality Wise No free days</label>
                            <div class="clearfix"></div>
                            <input type="text" name="sp_wise_no_free_days" autocomplete="off" class="form-control"
                                id="sp_wise_no_free_days">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Speciality Wise No free times</label>
                            <div class="clearfix"></div>
                            <input type="text" name="sp_wise_no_free_times" autocomplete="off" class="form-control"
                                id="sp_wise_no_free_times">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">No free days</label>
                            <div class="clearfix"></div>
                            <input type="text" name="no_free_days" autocomplete="off" class="form-control"
                                id="no_free_days">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">No free times</label>
                            <div class="clearfix"></div>
                            <input type="text" name="no_free_times" autocomplete="off" class="form-control"
                                id="no_free_times">
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <div class="">
                            <label style="font-weight: 600;margin-bottom: 0px !important;" for="">Surgeon</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" name="is_surgeon" autocomplete="off" class="form-control"
                                id="is_surgeon">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" style="margin-top: 5px">
                        <div class="">
                            <label style="font-weight: 600;margin-bottom: 0px !important;" for="">Hide from
                                appointments</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" name="hide_from_appointments" autocomplete="off" class="form-control"
                                id="hide_from_appointments">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm" style="margin-top: 5px">
                        <div class="" title="hide from billing and admission">
                            <label style="font-weight: 600;margin-bottom: 0px !important;" for="">Show in
                                appointments only</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" name="show_in_appointments_only" autocomplete="off"
                                class="form-control" id="show_in_appointments_only">
                        </div>
                    </div>
                </div>

            </div>
            <div id="collapseThree" class="tab-pane">

                <div class="card-body">
                    <div class="clearfix"></div>
                    <div class="col-md-3 padding_sm" style="margin-top: 5px">
                        <div class="">
                            <label style="font-weight: 600;margin-bottom: 0px !important;" for="">Has OP
                                rights</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" name="has_op_rights" autocomplete="off" class="form-control"
                                id="has_op_rights">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm" style="margin-top: 5px">
                        <div class="">
                            <label style="font-weight: 600;margin-bottom: 0px !important;" for="">Has IP
                                rights</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" name="has_ip_rights" autocomplete="off" class="form-control"
                                id="has_ip_rights">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">OP room number</label>
                            <div class="clearfix"></div>
                            <input type="text" name="op_room_no" autocomplete="off" class="form-control"
                                id="op_room_no">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">IP room number</label>
                            <div class="clearfix"></div>
                            <input type="text" name="ip_room_no" autocomplete="off" class="form-control"
                                id="ip_room_no">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Screening room number</label>
                            <div class="clearfix"></div>
                            <input type="text" name="screening_room_no" autocomplete="off" class="form-control"
                                id="screening_room_no">
                        </div>
                    </div>
                </div>

            </div>
            <div id="collapsefour" class="tab-pane">

                <div class="card-body">
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">UserName</label>
                            <div class="clearfix"></div>
                            <input type="text" onblur="checkDoctorUserName()" autocomplete="off" class="form-control"
                                id="add_doctor_user_name">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Email</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" id="add_doctor_login_email" autocomplete="off">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3 padding_sm" style="margin-top: 5px">
                        <div class="">
                            <label style="font-weight: 600;margin-bottom: 0px !important;" for="">Same as above
                                email</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" name="same_as_email" autocomplete="off" class="form-control"
                                id="same_as_email">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm" style="margin-top: 5px">
                        <div class="">
                            <label style="font-weight: 600;margin-bottom: 0px !important;" for="">active</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" name="login_active" autocomplete="off" class="form-control"
                                id="login_active">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Additional Information</label>

                            <div class="clearfix"></div>
                            <textarea class="form-control" name="additional_information"
                                id="additional_information"></textarea>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6 padding_sm" style="margin-top: 5px">
                        <div class="mate-input-box">
                            <label for="">Consultation Duration(In min)</label>
                            <div class="clearfix"></div>
                            <input type="text" name="consultation_duration" autocomplete="off" class="form-control"
                                id="consultation_duration">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <div class="">
                            <label style="font-weight: 600;margin-bottom: 0px !important;" for="">Active</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" name="is_active" autocomplete="off" class="form-control"
                                id="is_active">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <div class="">
                            <label style="font-weight: 600;margin-bottom: 0px !important;" for="">MLC Doctor</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" name="is_mlc" autocomplete="off" class="form-control" id="is_mlc">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <div class="">
                            <label style="font-weight: 600;margin-bottom: 0px !important;" for="">casuality
                                Doctor</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" name="is_casuality" autocomplete="off" class="form-control"
                                id="is_casuality">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="col-md-12 padding_sm">
    <div class="col-md-3 padding_sm pull-right">
        <button type="button" id="saveSalaryMasterBtn" onclick="saveSalaryMaster()"
            class="btn btn-primary btn-block">Save <i class="fa fa-save" id="saveSalaryMasterSpin"></i></button>
    </div>
    <div class="col-md-3 padding_sm pull-right">
        <button type="button" onclick="resetSalaryMaster()" class="btn btn-warning btn-block">Reset <i
                class="fa fa-recycle"></i></button>
    </div>
</div>
