<div class="col-md-6 padding_sm" style="margin-top: 5px">
    <div class="box no-border no-margin">
        <div class="box-footer"
            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 450px;">
            <div class="col-md-3 padding_sm" style="margin-top: 5px">
                <div class="mate-input-box">
                    <label for="">From date</label>
                    <div class="clearfix"></div>
                    <input type="text" class="form-control datepicker" id="dr_time_sloat_from_date" autocomplete="off">
                </div>
            </div>
            <div class="col-md-2 padding_sm" style="margin-top: 25px">
                <div class="checkbox checkbox-info inline no-margin">
                    <input onclick="scheduleEndsOn()" title="Ends on" type="checkbox" id="dr_time_sloat_is_ends_on"
                        value="1">
                    <label class="blue" for="dr_time_sloat_is_ends_on">Ends on</label>
                </div>
            </div>
            <div class="col-md-3 padding_sm" style="margin-top: 5px">
                <div class="mate-input-box">
                    <label for="">Date</label>
                    <div class="clearfix"></div>
                    <input type="text" readonly class="form-control datepicker" style="background: #FFF"
                        id="dr_time_sloat_ends_on" autocomplete="off">
                </div>
            </div>
            <div class="col-md-2 padding_sm" style="margin-top: 25px">
                <button type="button" onclick="resetDoctorSchedule()" class="btn btn-warning btn-block">Reset <i
                        class="fa fa-recycle"></i></button>
            </div>
            <div class="col-md-2 padding_sm pull-right" style="margin-top: 25px">
                <button type="button" id="saveDoctorScheduleBtn" onclick="saveDoctorSchedule()"
                    class="btn btn-primary btn-block">Save <i id="saveDoctorScheduleSpin"
                        class="fa fa-save"></i></button>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-3 padding_sm">
                <div class="mate-input-box">
                    <label for="">Start Time</label>
                    <div class="clearfix"></div>
                    <input type="text" onblur="getAvailableTimeSlots()" class="form-control timepicker "
                        id="dr_time_sloat_start_time" autocomplete="off">
                </div>
            </div>
            <div class="col-md-3 padding_sm">
                <div class="mate-input-box">
                    <label for="">End Time</label>
                    <div class="clearfix"></div>
                    <input type="text" onblur="getAvailableTimeSlots()" class="form-control timepicker "
                        id="dr_time_sloat_end_time" autocomplete="off">
                </div>
            </div>
            <div class="col-md-3 padding_sm">
                <div class="mate-input-box" title="In minutes">
                    <label for="">Duration (min)</label>
                    <div class="clearfix"></div>
                    <input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        type="text" onblur="getAvailableTimeSlots()" class="form-control " id="dr_time_sloat_duration"
                        autocomplete="off">
                </div>
            </div>
            <div class="col-md-3 padding_sm">
                <div class="mate-input-box">
                    <label for="">Available Slot</label>
                    <div class="clearfix"></div>
                    <input type="text" class="form-control" style="background: #FFF" readonly
                        id="dr_time_sloat_available_slot" autocomplete="off">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 padding_sm">
                <div class="col-md-5 padding_sm">
                    <div class="mate-input-box">
                        <label for="">Grace token</label>
                        <div class="clearfix"></div>
                        <input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            type="text" class="form-control" id="dr_time_sloat_grace_token" autocomplete="off">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-warning inline no-margin">
                        <input onclick="checkAllDays()" type="checkbox" id="dr_time_sloat_week_select_all" value="1">
                        <label class="green" for="dr_time_sloat_week_select_all">All</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="time_sloat_weeks" type="checkbox" id="dr_time_sloat_mon" value="1">
                        <label class="green" for="dr_time_sloat_mon">Monday</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="time_sloat_weeks" type="checkbox" id="dr_time_sloat_tue" value="1">
                        <label class="green" for="dr_time_sloat_tue">Tuesday</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="time_sloat_weeks" type="checkbox" id="dr_time_sloat_wed" value="1">
                        <label class="green" for="dr_time_sloat_wed">Wednesday</label>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="time_sloat_weeks" type="checkbox" id="dr_time_sloat_thu" value="1">
                        <label class="green" for="dr_time_sloat_thu">Thursday</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="time_sloat_weeks" type="checkbox" id="dr_time_sloat_fri" value="1">
                        <label class="green" for="dr_time_sloat_fri">Friday</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="time_sloat_weeks" type="checkbox" id="dr_time_sloat_sat" value="1">
                        <label class="green" for="dr_time_sloat_sat">Saturday</label>
                    </div>
                </div>
                <div class="col-md-3 padding_sm">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input class="time_sloat_weeks" type="checkbox" id="dr_time_sloat_sun" value="1">
                        <label class="green" for="dr_time_sloat_sun">Sunday</label>
                    </div>
                </div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 180px;">
                        <div class="theadscroll" style="position: relative; height: 175px;" id="sloat_list_area"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6 padding_sm" style="margin-top: 5px">
    <div class="box no-border no-margin">
        <div class="box-footer"
            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 450px;">
            <div id="dr_time_sloat_list_data"></div>
        </div>

    </div>
</div>
