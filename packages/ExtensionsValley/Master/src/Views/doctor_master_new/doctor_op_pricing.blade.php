<div class="col-md-4 padding_sm" style="margin-top: 5px">
    <div class="box no-border no-margin">
        <div class="box-footer"
            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 450px;">
            <div class="col-md-6 padding_sm">
                <div class="mate-input-box">
                    <label for="">Company</label>
                    <div class="clearfix"></div>
                    <select class="form-control" onchange="changeOpPricing('op_pricing_company','op_pricing_pricing')" id="op_pricing_company">
                        <option value="">All</option>
                        @foreach ($credit_company_list as $key=>$val)
                        <option value="{{ $key }}">{{ $val }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6 padding_sm">
                <div class="mate-input-box">
                    <label for="">Pricing</label>
                    <div class="clearfix"></div>
                    <select class="form-control select2" id="op_pricing_pricing" name="op_pricing_pricing">
                        <option value="">All</option>
                    </select>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px">
                <div class="theadscroll" style="position: relative; height: 160px;">
                    <table class="table no-margin table_sm theadfix_wrapper no-border" style="">
                        <thead>
                            <tr class="table_header_blue">
                                <th width="40%">Service</th>
                                <th width="15%">Registration</th>
                                <th width="15%">Renewal</th>
                                <th width="15%">MLCReg</th>
                                <th width="15%">NewBorn</th>
                            </tr>
                        </thead>
                        <tbody id="opservicedata_table">
                            @php
                            $j = 1;
                            @endphp
                            @foreach ($op_pricing_services as $data)
                            <tr style="height:35px !important;">
                                <td class="common_td_rules">
                                    {{$data->service_desc}}
                                    <input type="hidden" name="op_pricing_row_id" id="op_pricing_row_id"
                                        value="{{$j}}" />
                                    <input type="hidden" name="op_pricing_service_id" id="op_pricing_service_id_hidden"
                                        value="{{$data->service_id}}" />
                                    <input type="hidden" name="op_pricing_service_code"
                                        id="op_pricing_service_code_hidden" value="{{$data->service_code}}" />
                                </td>
                                <td class="common_td_rules">
                                    <input
                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                        type="text" class="form-control op_pricing_service_charges reg" id="reg_"
                                        name="reg" autocomplete="off" data-attr-row=""
                                        data-attr-service_id="{{$data->service_id}}" readonly
                                        data-attr-service_code="{{$data->service_code}}">
                                </td>
                                <td class="common_td_rules"> <input
                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                        type="text" class="form-control op_pricing_service_charges ren" id="ren_"
                                        autocomplete="off" name="ren" data-attr-row=""
                                        data-attr-service_id="{{$data->service_id}}" readonly
                                        data-attr-service_code="{{$data->service_code}}">
                                </td>
                                <td class="common_td_rules"> <input
                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                        type="text" class="form-control op_pricing_service_charges mlc" id="mlc_"
                                        autocomplete="off" name="mlc" data-attr-row=""
                                        data-attr-service_id="{{$data->service_id}}" readonly
                                        data-attr-service_code="{{$data->service_code}}">
                                </td>
                                <td class="common_td_rules"> <input
                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                        type="text" class="form-control op_pricing_service_charges new_born"
                                        id="new_born_" autocomplete="off" name="new_born" data-attr-row=""
                                        data-attr-service_id="{{$data->service_id}}" readonly
                                        data-attr-service_code="{{$data->service_code}}">
                                </td>
                            </tr>
                            @php
                            $j++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4 padding_sm" style="margin-top: 25px">
                <div class="checkbox checkbox-info inline no-margin">
                    <input onclick="setOpPricing()" title="Set OP Pricing" type="checkbox" id="set_op_pricing"
                        value="1">
                    <label class="blue" for="set_op_pricing">Set OP Pricing</label>
                </div>
            </div>
            <div class="col-md-3 padding_sm pull-right" style="margin-top: 25px">
                <button type="button" id="saveOpPricingBtn" onclick="saveOpPricing()"
                    class="btn btn-primary btn-block">Save <i id="saveOpPricingSpin" class="fa fa-save"></i></button>
            </div>
            <div class="col-md-3 padding_sm pull-right" style="margin-top: 25px">
                <button type="button" onclick="resetOPPricing()" class="btn btn-warning btn-block">Reset <i
                        class="fa fa-recycle"></i></button>
            </div>
        </div>
    </div>
</div>
<div class="col-md-8 padding_sm" style="margin-top: 5px">
    <div class="box no-border no-margin">
        <div class="box-footer"
            style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 450px;">
            <div class="col-md-5 padding_sm">
                <div class="mate-input-box">
                    <label for="">Company</label>
                    <div class="clearfix"></div>
                    <select onchange="changeOpPricing('op_pricing_company_search','op_pricing_pricing_search')" class="form-control select2" id="op_pricing_company_search"
                        name="op_pricing_company_search">
                        <option value="">All</option>
                        @foreach ($credit_company_list as $key=>$val)
                        <option value="{{ $key }}">{{ $val }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-5 padding_sm">
                <div class="mate-input-box">
                    <label for="">Pricing</label>
                    <div class="clearfix"></div>
                    <select class="form-control select2" id="op_pricing_pricing_search"
                        name="op_pricing_pricing_search">
                        <option value="">All</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2 padding_sm">
                <button class="btn btn-primary btn-block" onclick="searchOpPricing()" id="searchOpPricingBtn"
                    style="margin-top:20px;" type="button" id="search_op_pricing"><i id="searchOpPricingSpin"
                        class="fa fa-search"></i> Search</button>
            </div>
            <div class="clearfix"></div>
            <div class="theadscroll" style="position: relative; height: 350px;">
                <div id="getOpPricingListData"></div>
            </div>
        </div>

    </div>
</div>
