@if (count($resultData) != 0)
@foreach ($resultData as $key=>$val)
@php
$key_string=explode("::",$key);
@endphp
<div class="col-md-6 padding_sm" style="margin-top: 5px">
    <table class="table no-margin table_sm table-striped">
        <thead>
            <tr class="table_header_blue">
                <th style="text-align: center" colspan="5">{{ @$key_string[2] ? $key_string[2] : '' }}</th>
            </tr>
            <tr class="table_header_blue">
                <th>Service</th>
                <th>Reg.</th>
                <th>Ren.</th>
                <th>MLC</th>
                <th>N-Born</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($op_pricing_services as $each)
            <tr>
                <td class="common_td_rules">
                    {{$each->service_desc}}
                </td>
                @foreach ($val as $data)
                @if($data->service_code==$each->service_code)
                <td class="common_td_rules">
                    {{$data->amount}}
                </td>
                @endif
                @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endforeach
@endif
