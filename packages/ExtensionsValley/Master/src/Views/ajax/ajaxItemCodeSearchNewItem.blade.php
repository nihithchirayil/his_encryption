<?php 
$single_tax_status = \Site::getConfig('single_tax_status');
?>
<?php
if (isset($itemCodeDetails) ) {
if(!empty($itemCodeDetails)){
foreach ($itemCodeDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fill("{{htmlentities($item->item_code).'[ '. htmlentities(ucfirst($item->item_desc)).' ]'}}")'>
<!--            <img class="img-circle" src="<?php //echo web_url() . 'uploads/user_icon/' . $staff['photo']; ?>" width="32" height="32">-->
    <?php echo htmlentities(ucfirst($item->item_code)) . '     [   ' . htmlentities(ucfirst($item->item_desc)) . '   ]'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}


if (isset($itemCodeVendorDetails) ) {
if(!empty($itemCodeVendorDetails)){
foreach ($itemCodeVendorDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fill("{{htmlentities(ucfirst($item->item_desc))}}","{{ htmlentities($item->item_code) }}")'>
<!--            <img class="img-circle" src="<?php //echo web_url() . 'uploads/user_icon/' . $staff['photo']; ?>" width="32" height="32">-->
    <?php echo htmlentities(ucfirst($item->item_desc)) . '     [   ' . htmlentities(ucfirst($item->item_code)) . '   ]'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

if (isset($itemCodeFetch) ) {
if(!empty($itemCodeFetch)){
foreach ($itemCodeFetch as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fill("{{$item->item_code}}","{{htmlentities(ucfirst($item->item_desc)).'[ '. htmlentities($item->item_code).' ]'}}")'>
<!--            <img class="img-circle" src="<?php //echo web_url() . 'uploads/user_icon/' . $staff['photo']; ?>" width="32" height="32">-->
    <?php echo htmlentities(ucfirst($item->item_code)) . '     [   ' . htmlentities(ucfirst($item->item_desc)) . '   ]'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}


if (isset($serviceItemForPackages) ) {
if(!empty($serviceItemForPackages)){
foreach ($serviceItemForPackages as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillServiceItem(this,"{{$item->id}}","{{$item->code}}","{{htmlentities(ucfirst($item->name))}}","{{htmlentities($item->charges)}}","{{htmlentities($item->category)}}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

if (isset($packages) ) {
if(!empty($packages) && !empty($packages[0])){
foreach ($packages as $package) {
?>
<li style="display: block; padding:5px 10px 5px 5px; " onclick='fillPackage("{{$package->id}}")'>
    <?php echo htmlentities(ucfirst($package->name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}
?>

@if(isset($packageDetails))
    @if(!empty($packageDetails))

        <div class="pop_closebtn">X</div>
        <table width="400px">
            <thead>
            <tr>
                <th>Name</th>
                <!--<th>Price</th>-->
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($packageDetails as $item)
                <tr>
                    <td>
                        <input type="hidden" name="package_item_id-{{$package_code}}[]" class="package_id_class"
                               value="{{$item->code}}">
                        <input type="hidden" name="package_amount-{{$package_code}}[]" value="{{$item->amount}}">

                        {{$item->name}}
                    </td>
                    <!--  <td>
                      </td>-->
                    <td style="width:20%;">
                        <input type="number" min="0" maxlength="6" class="form-control package_item_amount"
                               name="package_item_amount-{{$package_code}}[]" value="{{$item->item_amount}}">
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        {{'No Results Found'}}
    @endif
@endif

<?php if (isset($itemCodeLocation) ) {
if(!empty($itemCodeLocation)){
foreach ($itemCodeLocation as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fill("{{htmlentities($item->item_code).'[ '. htmlentities(ucfirst($item->item_desc)).' ]'}}","{{htmlentities(ucfirst($item->item_desc)).'[ '. htmlentities($item->item_code).' ]'}}")'>
<!--            <img class="img-circle" src="<?php //echo web_url() . 'uploads/user_icon/' . $staff['photo']; ?>" width="32" height="32">-->
    <?php echo htmlentities(ucfirst($item->item_code)) . '     [   ' . htmlentities(ucfirst($item->item_desc)) . '   ]'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

/* For Patient details search */

if (isset($managerDetails)) {
if(!empty($managerDetails)){
foreach ($managerDetails as $manager) { ?>

<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillDoctorValues("{{htmlentities($manager->id)}}","{{htmlentities($manager->manager)}}")'>
    <?php echo htmlentities($manager->manager); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

if (isset($itemDetails) ) {
if(!empty($itemDetails)){
foreach ($itemDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillItemValues(this,"{{htmlentities($item->id)}}","{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","{{htmlentities($item->mrp)}}")'>
<!--            <img class="img-circle" src="<?php //echo web_url() . 'uploads/user_icon/' . $staff['photo']; ?>" width="32" height="32">-->
    <?php echo htmlentities(ucfirst($item->item_desc)) . '     [   ' . htmlentities($item->item_code) . '   ]'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

//--------vender name progressive search--------------------
if (isset($vendorDetails) ) {
if(!empty($vendorDetails)){
foreach($vendorDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillVendorDetials("{{htmlentities(ucfirst($item->vendor_code))}}","{{ htmlentities(ucfirst($item->name))}}")'>
    <?php echo htmlentities(ucfirst($item->name)) . '     [   ' . htmlentities(ucfirst($item->vendor_code)) . '   ]'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

//--------vender name td progressive search--------------------
if (isset($vendorDetailstdSearch) ) {
if(!empty($vendorDetailstdSearch)){
foreach($vendorDetailstdSearch as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillVendorDetialstdsearch(this,"{{htmlentities(ucfirst($item->vendor_code))}}","{{ htmlentities(ucfirst($item->name))}}")'>
    <?php echo htmlentities(ucfirst($item->name)) . '     [   ' . htmlentities(ucfirst($item->vendor_code)) . ' ]'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

//--------doctor name progressive search--------------------
if (isset($doctorDetails) ) {
if(!empty($doctorDetails)){
foreach($doctorDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillVendorDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->name)).'('.htmlentities(ucfirst($item->department)).')'}}")'>
    <?php echo htmlentities(ucfirst($item->name)) . '     (' . htmlentities(ucfirst($item->department)) . ')'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

//--------Manufacturer name progressive search--------------------


if (isset($manufactureDetails) ) {
if(!empty($manufactureDetails)){
foreach ($manufactureDetails as $item) {

?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillManufactureDetials("{{$item->code}}","{{htmlentities(ucfirst($item->name)).'[ '. htmlentities($item->code).' ]'}}")'>
    <?php echo htmlentities(ucfirst($item->code)) . '     [   ' . htmlentities(ucfirst($item->name)) . '   ]'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}
//--------Manufacturer name progressive search--------------------



//--------Deparment name progressive search--------------------


if (isset($departmentnewDetails) ) {
if(!empty($departmentnewDetails)){
foreach ($departmentnewDetails as $departmentnew) {

?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillDepartmentnewDetials("{{$departmentnew->department_code}}","{{htmlentities(ucfirst($departmentnew->name)).'[ '. htmlentities($departmentnew->department_code).' ]'}}")'>
    <!-- <?php echo htmlentities(ucfirst($departmentnew->department_code)) . '     [   ' . htmlentities(ucfirst($departmentnew->name)) . '   ]'; ?> -->
    <?php echo htmlentities(ucfirst($departmentnew->name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}
//--------Deparment name progressive search--------------------





//--------vendor name progressive search--------------------


if (isset($vendorDetailsNew) ) {
if(!empty($vendorDetailsNew)){
foreach ($vendorDetailsNew as $vendordata) {

?>
<!-- <li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillVendorDetialsNew("{{$vendordata->vendor_code}}","{{htmlentities(ucfirst($vendordata->vendor_name)).'[ '. htmlentities($vendordata->vendor_code).' ]'}}")'> -->
    <li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillVendorDetialsNew("{{$vendordata->vendor_code}}",atob("<?= base64_encode(ucfirst($vendordata->vendor_name).'[ '.$vendordata->vendor_code.' ]') ?>"),"{{$vendordata->contact_no}}")'>
    <?php echo htmlentities(ucfirst($vendordata->vendor_code)) . '     [   ' . htmlentities(ucfirst($vendordata->vendor_name)) . '   ]'; ?>

</li>
<?php
}
} else {
    echo 'No Results Found';
}
}
//--------vendor name progressive search--------------------




//--------Email Search search--------------------


if (isset($emailData) ) {
if(!empty($emailData)){
foreach ($emailData as $email) {

?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillEmailDetialsTo("{{$email->email}}")'>
   
    <?php echo htmlentities(ucfirst($email->email)); ?>

</li>

<?php
}
}
}
//--------Email search End--------------------
//--------Email Search search--------------------


if (isset($ccEmailData) ) {
if(!empty($ccEmailData)){
foreach ($ccEmailData as $email) {

?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillCcEmailDetialsTo("{{$email->email}}")'>
   
    <?php echo htmlentities(ucfirst($email->email)); ?>

</li>

<?php
}
}
}
//--------Email search End--------------------



//--------vendor name progressive search--------------------


if (isset($vendorDetailsNew1) ) {
if(!empty($vendorDetailsNew1)){
foreach ($vendorDetailsNew1 as $vendordata) {

?>
<!-- <li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillVendorDetialsNew1("{{$vendordata->vendor_code}}","{{htmlentities(ucfirst($vendordata->vendor_name)).'[ '. htmlentities($vendordata->vendor_code).' ]'}}")'> -->
    <li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillVendorDetialsNew1("{{$vendordata->vendor_code}}",atob("<?= base64_encode(ucfirst($vendordata->vendor_name).'[ '.$vendordata->vendor_code.' ]') ?>"),"{{$vendordata->contact_no}}")'>
    <?php echo htmlentities(ucfirst($vendordata->vendor_name)) . '     [   ' . htmlentities(ucfirst($vendordata->vendor_code)) . '   ]'; ?>

</li>
<?php
}
} else {
    echo 'No Results Found';
}
}
//--------vendor name progressive search--------------------



//--------vendor name progressive search--------------------

if (isset($vendorDetailsWithState) ) {
    if(!empty($vendorDetailsWithState)){
        foreach ($vendorDetailsWithState as $vendordata) {

        ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillVendorDetialsNew2("{{$vendordata->vendor_code}}","{{htmlentities(ucfirst($vendordata->vendor_name)).' [ '. htmlentities($vendordata->gst_vendor_code).' ]'}}","0","{{$vendordata->contact_no}}")'>
            <?php echo htmlentities(ucfirst($vendordata->vendor_name))."  [ $vendordata->gst_vendor_code ]"; ?>
        </li>
        <?php
        }
    } else {
        echo 'No Results Found';
    }
}
if (isset($vendorDetailsWithState1) ) {
    if(!empty($vendorDetailsWithState1)){
        foreach ($vendorDetailsWithState1 as $vendordata) {
  
       ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillVendorDetialsNew2("{{$vendordata->vendor_code}}",atob("<?= base64_encode(ucfirst($vendordata->vendor_name).' [ '. ($vendordata->gst_vendor_code).' ]') ?>"),"0","{{$vendordata->contact_no}}")'>
            <?php echo htmlentities(ucfirst($vendordata->vendor_name)); ?>
        </li>  
        <?php
        }
    } else {
        echo 'No Results Found';
    }
}
//--------vendor name progressive search--------------------
//--------vendor name progressive search--------------------

if (isset($vendorDetailsNew2) ) {
if(!empty($vendorDetailsNew2)){
foreach ($vendorDetailsNew2 as $vendordata) {

?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillVendorDetialsNew2("{{$vendordata->vendor_code}}","{{htmlentities(ucfirst($vendordata->vendor_name)).'[ '. htmlentities($vendordata->vendor_code).' ]'}}")'>
   
    <?php echo htmlentities(ucfirst($vendordata->vendor_code)) . '     [   ' . htmlentities(ucfirst($vendordata->vendor_name)) . '   ]'; ?>

</li>
<?php
}
} else {
    echo 'No Results Found';
}
}
//--------vendor name progressive search--------------------




//--------Employee name progressive search Start--------------------


if (isset($managernewDetails) ) {
if(!empty($managernewDetails)){
foreach ($managernewDetails as $managernew) {

?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillManagernewDetials("{{$managernew->employee_code}}","{{htmlentities(ucfirst($managernew->first_name)).'[ '. htmlentities($managernew->employee_code).' ]'}}")'>
    
    <?php echo htmlentities(ucfirst($managernew->first_name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}
//--------Employee name progressive search End--------------------



//--------generic name progressive search Start--------------------

if (isset($genericnewNameDetails) ) {
if(!empty($genericnewNameDetails)){
foreach ($genericnewNameDetails as $genericnamenew) {

?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillgenericnameDetials("{{$genericnamenew->code}}","{{htmlentities(ucfirst($genericnamenew->generic_name)).''}}")'>

<?php echo htmlentities(ucfirst($genericnamenew->generic_name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}
//--------generic name progressive search End--------------------



if (isset($zeroDischargedoctorDetails) ) {
if(!empty($zeroDischargedoctorDetails)){
foreach($zeroDischargedoctorDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillZeroDischargedoctorDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->name)).'('.htmlentities(ucfirst($item->department)).')'}}")'>
    <?php echo htmlentities(ucfirst($item->name)) . '     (' . htmlentities(ucfirst($item->department)) . ')'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}
//--------doctor name in discahrge summary progressive search--------------------
if (isset($summaryDoctorDetails) ) {
if(!empty($summaryDoctorDetails)){
foreach($summaryDoctorDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillDoctorDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->name)).'('.htmlentities(ucfirst($item->department)).')'}}")'>
    <?php echo htmlentities(ucfirst($item->name)) . '     (' . htmlentities(ucfirst($item->department)) . ')'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

if (isset($userDetails) ) {
if(!empty($userDetails)){
foreach($userDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillUserDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->name))}}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

if (isset($userDetailsprepared) ) {
if(!empty($userDetailsprepared)){
foreach($userDetailsprepared as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillUserDetialsprepared("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->name))}}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}



//--------doctor name progressive search for MR list--------------------
if (isset($MrdoctorDetails) ) {
if(!empty($MrdoctorDetails)){
foreach($MrdoctorDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillDoctorDetials("{!!$item->id !!}","{!! $item->name !!}")'>
    {!! $item->name !!}
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

//--------patient name progressive search MR List--------------------
if (isset($MRpatientDetails) ) {
if(!empty($MRpatientDetails)){
foreach($MRpatientDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillPatientDetials("{!! $item->id !!}","{!! $item->patient_name !!}","{!! $item->patient_id !!}")'>
    {!! $item->patient_name . " [".$item->patient_id ."]" !!}
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}


//--------doctor name progressive search--------------------
if (isset($doctorWithDeptDetails) ) {
if(!empty($doctorWithDeptDetails)){
foreach($doctorWithDeptDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='filldoctorWithDeptDetails("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->name)).'('.htmlentities(ucfirst($item->department)).')'}}","{{htmlentities(ucfirst($item->department))}}","{{htmlentities(ucfirst($item->department_id))}}")'>
    <?php echo htmlentities(ucfirst($item->name)) . '     (' . htmlentities(ucfirst($item->department)) . ')'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}


//--------patient name progressive search-------------------------
if (isset($patientDetails) ) {
if(!empty($patientDetails)){
foreach($patientDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillVendorDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->first_name)).' '.htmlentities(ucfirst($item->last_name))}}")'>
    <?php echo htmlentities(ucfirst($item->first_name)) . ' ' . htmlentities(ucfirst($item->last_name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}
//--------op numberprogressive search--------------------
if (isset($OpNoDetails) ) {
if(!empty($OpNoDetails)){
foreach($OpNoDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillOpDetials("{{htmlentities(ucfirst($item->patient_id))}}","{{ htmlentities(ucfirst($item->patient_id))}}","{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->first_name)).' '.htmlentities(ucfirst($item->last_name))}}")'>
    <?php echo htmlentities(ucfirst($item->first_name)) . ' ' . htmlentities(ucfirst($item->last_name)) . ' [' . htmlentities(ucfirst($item->patient_id)) . ']'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}
//--------location name progressive search--------------------

if (isset($locationNameDetails) ) {
if(!empty($locationNameDetails)){
foreach($locationNameDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillLocationDetials("{{htmlentities(ucfirst($item->location_code))}}","{{ htmlentities(ucfirst($item->name))}}")'>
    <?php echo htmlentities(ucfirst($item->name)) . '     [   ' . htmlentities(ucfirst($item->location_code)) . '   ]'; ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}


//--------chemical item name progressive search--------------------

if (isset($itemChemicalDetails) ) {
if(!empty($itemChemicalDetails)){
foreach($itemChemicalDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillItemChemicalDetials("{{htmlentities($item->item_code)}}","{{ htmlentities(ucfirst($item->name))}}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}


//--------chemical composition item name progressive search--------------------

if (isset($itemChemicalCompositionDetails) ) {
if(!empty($itemChemicalCompositionDetails)){
foreach($itemChemicalCompositionDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillItemChemicalDetials("{{htmlentities(ucfirst($item->name))}}","{{ htmlentities(ucfirst($item->name))}}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

//--------Ip services listing progressive search--------------------
if (isset($ipServicesDetails) ) {
if(!empty($ipServicesDetails)){
foreach($ipServicesDetails as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillIpserviceDetials(this,"{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->name))}}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

//--------patient name progressive search--------------------
if (isset($patientDetailsWithOP) ) {
if(!empty($patientDetailsWithOP)){
foreach($patientDetailsWithOP as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillPatientOPDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->first_name)).' '.htmlentities(ucfirst($item->last_name))}}","{{htmlentities(ucfirst($item->patient_id))}}")'>
    <?php echo htmlentities(ucfirst($item->first_name)) . ' ' . htmlentities(ucfirst($item->last_name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}

if (isset($ItemForPharmacyPackages) ) {
if(!empty($ItemForPharmacyPackages)){
foreach ($ItemForPharmacyPackages as $item) {
?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillPharmacyItem(this,"{{$item->id}}","{{$item->code}}","{{htmlentities(ucfirst($item->name))}}","{{htmlentities($item->category)}}")'>
    <?php echo htmlentities(ucfirst($item->name)); ?>
</li>
<?php
}
} else {
    echo 'No Results Found';
}
}


//--------mappinglocation name progressive search Start--------------------
if (isset($mappinglocation) ) {
if(!empty($mappinglocation)){
foreach ($mappinglocation as $mappinglocations) {
?>
<option value="{{$mappinglocations->location_code}}">{{$mappinglocations->location_name}}</option>
<?php
}
} else {
    echo 'No Results Found';
}
}
//--------mappinglocation name progressive search End--------------------






?>