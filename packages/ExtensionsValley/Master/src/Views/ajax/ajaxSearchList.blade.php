<?php
if (isset($ledger_search)) {
    if (!empty($ledger_search)) {

        foreach ($ledger_search as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillLedgerValues(this,"{{htmlentities($desc->ledger_name)}}","{{htmlentities($desc->id)}}","{{$type}}")' class="list_hover">
                {!! $desc->ledger_name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($ledger_bank_search)) {
    if (!empty($ledger_bank_search)) {
        if(isset($is_adv) && $is_adv == 1){
           $fn_name = "fillLedgerValues_adv";
        }else if (isset($is_bank) && $is_bank == 1) {
            $fn_name = 'fillLedgerValues';
        } else {
            $fn_name = 'fillLedgerValuesOthr';
        }
        foreach ($ledger_bank_search as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='<?=$fn_name?>(this,"{{htmlentities($desc->ledger_name)}}","{{htmlentities($desc->id)}}",
                "{{htmlentities($desc->ledger_group_id)}}","{{htmlentities($desc->opening_balence??'')}}","{{htmlentities($desc->is_asset??'')}}")' class="list_hover">
                {!! $desc->ledger_name !!}
            </li>
            <?php
}
    } else {
        if(isset($from_ledger_booking)){ ?>
            <span type="button" class="btn btn-block btn-primary" onclick="saveNewLedger(this)" style="margin-bottom: 0px;">
                       <i class="fa fa-plus" id="add_new_ledger_spin"></i> Add new ledger</span>
       <?php }else{
        echo 'No Result Found';
        }
    }
}
if (isset($bill_no_search)) {
    if (!empty($bill_no_search)) {

        foreach ($bill_no_search as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillBillValues(this,"{{htmlentities($desc->bill_no)}}","{{htmlentities($desc->bill_date)}}","{{htmlentities($desc->net_amount)}}")' class="list_hover">
                {!! $desc->bill_no !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($voucher_no_search)) {
    if (!empty($voucher_no_search)) {

        foreach ($voucher_no_search as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillVoucherValues(this,"{{htmlentities($desc->voucher_no)}}")' class="list_hover">
                {!! $desc->voucher_no !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($shareHolderDetails)) {
    if (!empty($shareHolderDetails)) {

        foreach ($shareHolderDetails as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillShareHolderValues(this,"{{htmlentities($desc->id)}}","{{htmlentities($desc->patient_name)}}","{{htmlentities($desc->uhid)}}","{{htmlentities($desc->parent_share_holder)}}")' class="list_hover">
                {!! $desc->patient_name !!} [  {!! $desc->uhid !!} ]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($familyDetails)) {
    if (!empty($familyDetails)) {

        foreach ($familyDetails as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillCardHolderValues(this,"{{htmlentities($desc->id)}}","{{htmlentities($desc->patient_name)}}","{{htmlentities($desc->uhid)}}","{{htmlentities($desc->parent_family_holder)}}")' class="list_hover">
                {!! $desc->patient_name !!} [  {!! $desc->uhid !!} ]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
} else if (isset($ins_data)) {
    if (!empty($ins_data)) {
        foreach ($ins_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_patient_id(this,"{{htmlentities($desc->id)}}","{{htmlentities($desc->uhid)}}")' class="list_hover">
                {{$desc->patient_name}} [{!! $desc->uhid !!}]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
} else if (isset($infertility_patient_search)) {
    if (!empty($infertility_patient_search)) {
        foreach ($infertility_patient_search as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_patient_id(this,"{{htmlentities($desc->id)}}","{{htmlentities($desc->uhid)}}","{{htmlentities($desc->patient_name)}}","{{htmlentities($desc->age)}}","{{htmlentities($desc->blood_group)}}", "{{htmlentities($desc->last_visit_datetime)}}","{{htmlentities($visit_id)}}", "{{htmlentities($encounter_id)}}", "{{htmlentities($search_type)}}")' class="list_hover">
                {{$desc->patient_name}} [{!! $desc->uhid !!}]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
} else if (isset($ins_ip_data)) {
    if (!empty($ins_ip_data)) {
        foreach ($ins_ip_data as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fill_patient_ip_id(this,"{{htmlentities($desc->admission_no)}}","{{htmlentities($desc->uhid)}}")' class="list_hover">
                {{$desc->admission_no}}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}else if (isset($patient_visit_search)) {
    if (!empty($patient_visit_search)) {
        foreach ($patient_visit_search as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities($desc->id)}}","{{htmlentities($desc->uhid)}}","{{htmlentities($desc->patient_name)}}", "{{$input_id}}")' class="list_hover">
                {{$desc->patient_name}} [{!! $desc->uhid !!}]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }

}else if (isset($search_advance_collection)) {
    if (!empty($search_advance_collection)) {
        foreach ($search_advance_collection as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities($desc->id)}}","{{htmlentities($desc->uhid)}}","{{htmlentities($desc->patient_name)}}", "{{$input_id}}","{{htmlentities($desc->patient_advance)}}")' class="list_hover">
                {{$desc->patient_name}} [{!! $desc->uhid !!}]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }

}else if (isset($patient_od_uhid_search)) {
    if (!empty($patient_od_uhid_search)) {
        foreach ($patient_od_uhid_search as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillOlduhidSearchDetials("{{htmlentities($desc->ext_uhid)}}","{{htmlentities($desc->patient_name)}}", "{{$input_id}}")' class="list_hover">
                {{$desc->patient_name}} [{!! $desc->ext_uhid !!}]
            </li>
            <?php
        }
    } else {
        echo 'No Result Found';
    }
}else if (isset($uhid_pharmacy_bill)) {
    if (!empty($uhid_pharmacy_bill)) {
        foreach ($uhid_pharmacy_bill as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='getPatientDetailsByUHID("{{htmlentities($desc->uhid)}}")' class="list_hover">
                {{$desc->patient_name}} [{!! $desc->uhid !!}]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
} else if (isset($uhid_discharge_bill)) {
    if (!empty($uhid_discharge_bill)) {
        foreach ($uhid_discharge_bill as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='getIPPatientDetailsByUHID("{{htmlentities($desc->uhid)}}")' class="list_hover">
                {{$desc->patient_name}} [{!! $desc->uhid !!}]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
} else if (isset($uhid_service_bill)) {
    if (!empty($uhid_service_bill)) {
        foreach ($uhid_service_bill as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='getPatientDetailsByUHID("{{htmlentities($desc->uhid)}}")' class="list_hover">
                {{$desc->patient_name}} [{!! $desc->uhid !!}]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
} else if (isset($service_list_service_bill)) {
    if (!empty($service_list_service_bill)) {
        foreach ($service_list_service_bill as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='selectServiceItemForReplace("{{htmlentities($desc->service_code)}}", "{{htmlentities($desc->service_desc)}}", "{{htmlentities($desc->investigation_type)}}")' class="list_hover">
                {{$desc->service_desc}}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}

if (isset($favouriteGroupDetails)) {
    if (!empty($favouriteGroupDetails)) {

        foreach ($favouriteGroupDetails as $group) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillGroupDetails(this,"{{htmlentities($group->id)}}","{{htmlentities($group->group_name)}}")' class="list_hover">
                {!! $group->group_name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}

if (isset($yellowsheetPatienSearch)) {
    if (!empty($yellowsheetPatienSearch)) {

        foreach ($yellowsheetPatienSearch as $group) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillGroupDetails(this,"{{htmlentities($group->id)}}","{{htmlentities($group->patient_name)}}")' class="list_hover">
                <?=$group->patient_name . ' (' . $group->uhid . ')'?>
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}

if (isset($productStockProductDetails)) {
    if (!empty($productStockProductDetails)) {

        foreach ($productStockProductDetails as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillProductStockDetails(this,"{{htmlentities($data->item_code)}}","{{htmlentities($data->item_desc)}}")' class="list_hover">
                {!! $data->item_desc !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}

if (isset($productStockBatchDetails)) {
    if (!empty($productStockBatchDetails)) {

        foreach ($productStockBatchDetails as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillBathDetails(this,"{{htmlentities($data->batch_no)}}","{{htmlentities($data->unit_selling_price)}}","{{htmlentities($data->unit_mrp)}}")' class="list_hover">
                {!! $data->batch_no !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}

if (isset($ledgerDetails)) {
    if (!empty($ledgerDetails)) {

        foreach ($ledgerDetails as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillLedgerDetails(this,"{{htmlentities($data->id)}}","{{htmlentities($data->ledger_name)}}")' class="list_hover">
                {!! $data->ledger_name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($MappingValues)) {
    if (!empty($MappingValues)) {

        foreach ($MappingValues as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillMappingValues(this,"{{htmlentities($data->id)}}","{{htmlentities($data->name)}}")' class="list_hover">
                {!! $data->name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($MapValues)) {
    if (!empty($MapValues)) {

        foreach ($MapValues as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillMapValues(this,"{{htmlentities($data->id)}}","{{htmlentities($data->name)}}")' class="list_hover">
                {!! $data->name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($genericNameDetails)) {
    if (!empty($genericNameDetails)) {

        foreach ($genericNameDetails as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillGenericValues("{{htmlentities($data->id)}}","{{htmlentities($data->generic_name)}}","{{htmlentities($val_type)}}")' class="list_hover">
                {!! $data->generic_name !!}
            </li>
            <?php
}
    } else {
        if(isset($from_item_creation)){ ?>
            <span type="button" class="btn btn-block btn-primary" onclick="saveNewGenericName()" style="margin-bottom: 0px;">
                       <i class="fa fa-plus" id="add_new_ledger_spin"></i> Add new generic name</span>
       <?php }else{
        echo 'No Result Found';
        }
    }
}
if(isset($item_for_location_details)){
    if (!empty($item_for_location_details)) {

        foreach ($item_for_location_details as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='itemLocationTable("{{htmlentities($data->item_id)}}","{{htmlentities($data->item_desc)}}")' class="list_hover">
                {!! $data->item_desc !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if(isset($rack_location_details)){
    if (!empty($rack_location_details)) {

        foreach ($rack_location_details as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillLocationData("{{htmlentities($data->location_name)}}","{{htmlentities($data->location_code)}}")' class="list_hover">
                {!! $data->location_name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if(isset($product_location_details)){
    if (!empty($product_location_details)) {

        foreach ($product_location_details as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemLocationData("{{htmlentities($data->location_name)}}","{{htmlentities($data->id)}}")' class="list_hover">
                {!! $data->location_name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if(isset($product_e_location_details)){
    if (!empty($product_e_location_details)) {

        foreach ($product_e_location_details as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillItemEditLocationData("{{htmlentities($data->location_name)}}","{{htmlentities($data->id)}}")' class="list_hover">
                {!! $data->location_name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($search_group_asset)) {
    if(isset($types) && $types==3){
        $fill_fn = "fillAssetGroupValuesMaster";
    }else{
        $fill_fn = "fillAssetGroupValues";
    }
    if (!empty($search_group_asset)) {

        foreach ($search_group_asset as $desc) {
            if(isset($desc->ledger_group_parent_id) && $desc->ledger_group_parent_id != ''){
                $bg_color ="background:#d2e3e2;";
            }else{
                $bg_color ="";
            }
            ?>
            <li style="display: block; padding:5px 10px 5px 5px;<?= $bg_color ?> "
                onclick='<?= $fill_fn ?>(this,"{{htmlentities($desc->ledger_group_name)}}","{{htmlentities($desc->id)}}","{{htmlentities($types)}}","{{htmlentities($desc->is_asset)}}")' class="list_hover">
                {!! $desc->ledger_group_name !!}
<?php }
 } else {
        echo 'No Result Found';
    }
}
if(isset($doctor_name_details)){
    if (!empty($doctor_name_details)) {

        foreach ($doctor_name_details as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillDoctorNameDetails("{{htmlentities($data->doctor_name)}}","{{htmlentities($data->id)}}")' class="list_hover">
                {!! $data->doctor_name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if(isset($manufacturer_name_details)){
    if (!empty($manufacturer_name_details)) {

        foreach ($manufacturer_name_details as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillManufacturerNameDetails("{{htmlentities($data->manufacturer_name)}}","{{htmlentities($data->id)}}")' class="list_hover">
                {!! $data->manufacturer_name !!}
            </li>
            <?php
}
    }  else {
        if(isset($from_item_creation)){ ?>
            <span type="button" class="btn btn-block btn-primary" onclick="addManufacturerName()" style="margin-bottom: 0px;">
                       <i class="fa fa-plus" id="add_new_ledger_spin"></i> Add new manufacturer name</span>
       <?php }else{
        echo 'No Result Found';
        }
    }
}
if(isset($service_desc_search)){
    if (!empty($service_desc_search)) {

        foreach ($service_desc_search as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillServiceDescDetails("{{htmlentities($data->service_desc)}}","{{htmlentities($data->id)}}","{{htmlentities($input_id)}}")' class="list_hover">
                {!! $data->service_desc !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($productsearch)) {
    if (!empty($productsearch)) {

        foreach ($productsearch as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillProductSearch(this,"{{htmlentities($data->item_code)}}","{{htmlentities($data->item_desc)}}","{{htmlentities($data->id)}}")' class="list_hover">
                {!! $data->item_desc !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($productStockProductDetails1)) {
    if (!empty($productStockProductDetails1)) {

        foreach ($productStockProductDetails1 as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillProductStockDetails1(this,"{{htmlentities($data->item_code)}}","{{htmlentities($data->item_desc)}}","{{htmlentities($data->id)}}")' class="list_hover">
                {!! $data->item_desc !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($productStockProductDetails2)) {
    if (!empty($productStockProductDetails2)) {

        foreach ($productStockProductDetails2 as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillProductStockDetails2(this,"{{htmlentities($data->item_code)}}","{{htmlentities($data->item_desc)}}","{{htmlentities($data->id)}}")' class="list_hover">
                {!! $data->item_desc !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}

if (isset($dialysis_patient_data)) {
    if (sizeof($dialysis_patient_data) > 0) {
if(isset($from_type) && $from_type == 'search'){
    $dial_fn = 'filldaialysisDataSearch';
}else{
$dial_fn = 'filldaialysisData';
}
        foreach ($dialysis_patient_data as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='<?= $dial_fn ?>(this,"{{htmlentities($data->id)}}","{{htmlentities($data->patient_name)}}","{{htmlentities($data->uhid)}}","{{htmlentities($data->age)}}","{{htmlentities($data->gender)}}","{{htmlentities($data->address)}}","{{htmlentities(rtrim($data->blood_group))}}")' class="list_hover">
                {!! $data->patient_name !!}  [  {!! $data->uhid !!} ]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}
if (isset($dialysis_patient_enrol_data)) {
    if (sizeof($dialysis_patient_enrol_data) > 0) {
        foreach ($dialysis_patient_enrol_data as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillEnrolPatient(this,"{{htmlentities($data->patient_id)}}","{{htmlentities($data->enrol_id)}}"
                ,"{{htmlentities($data->patient_name)}}","{{htmlentities($data->uhid)}}","{{htmlentities($data->dialysis_serial_no)}}")' class="list_hover">
                {!! $data->patient_name !!}  [  {!! $data->uhid !!} ]
            </li>
            <?php
    }
    } else {
        echo 'No Result Found';
    }
}
    if (isset($dialysis_user_data)) {
        if (sizeof($dialysis_user_data) > 0) {
        foreach ($dialysis_user_data as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='filluserData(this,"{{htmlentities($data->id)}}","{{htmlentities($data->user_name)}}","{{ $from_type }}")' class="list_hover">
                {!! $data->user_name !!}
            </li>
            <?php
    }
    } else {
        echo 'No Result Found';
    }
}


if(isset($modality_desc_search)){
    if (!empty($modality_desc_search)) {

        foreach ($modality_desc_search as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillModalityDetails("{{htmlentities($data->name)}}","{{htmlentities($data->code)}}","{{$input_id}}")' class="list_hover">
                {{ $data->name }}
                <?php }
} else {
        echo 'No Result Found';
    }
}
if (isset($modality_patient_search)) {
    if (!empty($modality_patient_search)) {
        foreach ($modality_patient_search as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillSearchDetials("{{htmlentities($desc->id)}}","{{htmlentities($desc->uhid)}}","{{htmlentities($desc->patient_name)}}", "{{$input_id}}")' class="list_hover">
                {{$desc->patient_name}} [{!! $desc->uhid !!}]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }

}
if (isset($productStockProductDetails3)) {
    if (!empty($productStockProductDetails3)) {

        foreach ($productStockProductDetails3 as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillProductStockDetails3(this,"{{htmlentities($data->item_code)}}","{{htmlentities($data->item_desc)}}","{{htmlentities($data->id)}}")' class="list_hover">
                {!! $data->item_desc !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}

if (isset($ledgerMasterDetails)) {
    if (!empty($ledgerMasterDetails)) {

        foreach ($ledgerMasterDetails as $data) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillLedgerDetails(this,"{{htmlentities($data->id)}}","{{htmlentities($data->ledger_name)}}", {{$count}})' class="list_hover">
                {!! $data->ledger_name !!}
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}

