<?php
if(sizeof($resultData)>0){
    if($input_id == 'doctor'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->doctor_name)) }}","{{ $input_id }}")'>
    {{ $item->id }} | {{ $item->doctor_name }}
</li>

<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'item_name'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->item_desc)) }}","{{ $input_id }}")'>
    {{ $item->id }} | {{ $item->item_desc }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'search_item'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillItemDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->item_desc)) }}","{{ $input_id }}")'>
    {{ $item->id }} | {{ $item->item_desc }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'manufacturer_name'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->manufacturer_name)) }}","{{ $input_id }}")'>
    {{ $item->id }} | {{ $item->manufacturer_name }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'vendor_name'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->vendor_name)) }}","{{ $input_id }}")'>
    {{ $item->id }} | {{ $item->vendor_name }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }else if($input_id == 'search_generic_name'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
<li style="display: block; padding:5px 10px 5px 5px; "
    onclick='fillSearchDetials("{{ htmlentities(ucfirst($item->id)) }}","{{ htmlentities(ucfirst($item->generic_name)) }}","{{ $input_id }}")'>
    {{ $item->generic_name }}
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }
} else {
    echo 'No Results Found';
}
?>
