<?php

if(sizeof($resultData)>0){
    if($input_id == 'group'){
        if (!empty($resultData)) {
            foreach ($resultData as $item) {
                ?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillSearchDetials("{{htmlentities(ucfirst($item->ledger_code))}}","{{ htmlentities(ucfirst($item->ledger_name))}}","{{$input_id}}")'>
                        {{$item->ledger_code}} | {{$item->ledger_name}}
                </li>
                <?php
            }
        } else {
            echo 'No Results Found';
        }
    }
} else {
    echo 'No Results Found';
}
