<!DOCTYPE HTML>
<link href="{{ asset('packages/extensionsvalley/dashboard/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/dashboard/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/dashboard/css/custom.min.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/dashboard/css/core-admin.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/dashboard/css/green.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/default/css/select2.min.css') }}" rel="stylesheet">
<link
				href="{{ asset('packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"
				rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/dashboard/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}"
				rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">

<link href="{{ asset('packages/extensionsvalley/dashboard/css/timepicker.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/bootstrap-select/css/bootstrap-multiselect.css') }}"
				rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/new_fonts/stylesheet.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">


@yield('css_extra')

<!-- jQuery -->
<script src="{{ asset('packages/extensionsvalley/dashboard/js/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('packages/extensionsvalley/dashboard/js/bootstrap.min.js') }}"></script>
<!---overlay loading effect--->
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/bootbox.js') }}"></script>

<script src="{{ asset('packages/extensionsvalley/bootstrap-select/js/bootstrap-multiselect.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/bootstrap-select/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/dashboard/plugins/momentjs/moment.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>

<script
src="{{ asset('packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}">
</script>
<script src="{{ asset('packages/extensionsvalley/default/js/select2.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>
<script
src="{{ asset('packages/extensionsvalley/dashboard/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}">
</script>

<style>
				.box-body {
								background-color: #242736 !important;
				}

				.btn-default {
								background-color: #66d7ac !important;
								border-color: #242736 !important;
				}

				.select2-container {
								width: 100% !important;
				}

				#select2-forms-container,
				.select2-results,
				.select2-search,
				.select2-search__field {
								background-color: black !important;
								color: #66d7ac;
				}

				.select2-selection {
								background-color: black !important;
				}

				.app_bg {
								background-color: #2e3142 !important;
								color: white !important;
				}

				.app_header {
								background-color: #2e3142 !important;
								color: #66d7ac !important;
								margin-bottom: 15px !important;
				}

				.app_control {
								background-color: black !important;
								color: white !important;
								border: black !important;
								border-radius: 3px !important;
				}

				@media only screen and (min-width: 600px) {

								table.mceLayout,
								textarea.richEditor {
												width: 600px !important;
								}
				}

				.select2-selection,
				.select2-selection--multiple,
				.custom-text-box,
				.datepicker {
								height: 40px !important;
								margin-bottom: 12px !important;
				}

				.select_button>li {
								background: rgb(181, 243, 219);
								background-color: #66d7ac;
								color: black;
								padding: 3px !important;
				}

				.select_button {
								padding: 7px !important;
				}

</style>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta http-equiv=" X-UA-Compatible" content="IE=edge" content="chrome=1">

<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="patient_id" value="{{ $patient_id }}">
<input type="hidden" id="user_id" value="{{ $user_id }}">
<input type="hidden" id="visit_id" value="{{ $visit_id }}">
<input type="hidden" id="encounter_id" value="{{ $encounter_id }}">
<input type="hidden" id="doctor_id" value="{{ $doctor_id }}">
<input type="hidden" id="form_id" value="{{ $edit_form_id }}">
<input type="hidden" id="fav_form_id" value="{{ $form_id }}">
<input type="hidden" id="form_list_id" value="{{ $form_list_id }}">
<input type="hidden" id="ca_head_id" value="0">
<input type="hidden" id="ca_edit_status" value="0">
{{-- <input type="hidden" id="fav_form_id" value="{{ $fav_form_id }}"> --}}

<div class="container-fluid" style="width:375px !important;margin:5px; background-color:#242736;min-height:640px !important;">
				<div class="row content" style="margin-top:10px;">
								<div class="col-xs-12 no-padding " style="cursor: pointer;margin-left:5px;">
												<div class=" col-xs-10 no-padding">
																<select name="forms" id="forms" class="btn-block">
																				<option value="">Select Template</option>
																</select>
												</div>

												<div class="col-xs-2 no-padding">
																<button type="button" style="height:38px;width:85%;float:left;clear:right;margin-right:2px;"
																				onclick="saveFavTemplate()" class="btn btn-default btn-block fav-button-forms no-margin"
																				title="Add To Bookmarks"><i class="fa fa-star"></i>
																</button>
												</div>

								</div>
				</div>
				<div class="row content" style="margin-top:10px;">
								<div class="col-xs-12 padding_sm " style="cursor: pointer;">

												<div class="card">
																<!-- Card Body -->
																<form id="ca-data-form">
																				<div class="card_body notes_box app_bg"
																								style="padding:0px;border:none;margin-bottom:30px;padding-bottom:20px;">
																								<div class="theadscrolll" style="position: relative; height: auto;">
																												<div class="" id="template-content-load-div"
																																style="background-image: url({{ asset('packages/extensionsvalley/default/img/medical_bg.jpgs') }}); background-repeat: repeat;margin-bottom:30px;">

																												</div>
																								</div>
																				</div>
																</form>
																<div class="col-xs-12" id="template_save_container" style="margin-bottom:30px;display:none;">
																				<div class="col-xs-6">
																								<button type="button" style="height:38px;background-color:#75f9ed !important"
																												onclick="resetTemplate()"
																												class="btn btn-default btn-block no-margin fetchFavoriteTemplatesBtn" title="Bookmarks">
																												<i class="fa fa-repeat"></i> Reset
																								</button>
																				</div>
																				<div class="col-xs-6">
																								<button type="button" style="height:38px;background-color:#75f9ed !important"
																												onclick="saveClinicalTemplate(1);"
																												class="btn btn-default btn-block no-margin fetchFavoriteTemplatesBtn" title="Bookmarks">
																												<i class="fa fa-save"></i> Save
																								</button>
																				</div>
																</div>
																<!-- Card Body -->
												</div>
								</div>
				</div>
</div>

<script
src="{{ asset('packages/extensionsvalley/emr/js/clinical_notes.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>

<script>

</script>
