
<div class="row content" style="margin-top:10px;">
<div class="col-xs-12 padding_sm " style="cursor: pointer;">

<div class="card">
<!-- Card Body -->
<form id="ca-data-form">
<div class="card_body notes_box app_bg"
    style="padding:0px;border:none;margin-bottom:30px;padding-bottom:20px;">
    <div class="theadscrolll" style="position: relative; height: auto;">
                    <div class="" id="template-content-load-div" style="margin-bottom:30px;">
                    </div>
    </div>
</div>
</form>
<div class="col-xs-12" id="template_save_container" style="margin-bottom:30px;display:none;">
<div class="col-xs-6">
    <button type="button" style="height:38px;background-color:#75f9ed !important"
                    onclick="resetTemplate()"
                    class="btn btn-default btn-block no-margin fetchFavoriteTemplatesBtn" title="Bookmarks">
                    <i class="fa fa-repeat"></i> Reset
    </button>
</div>
<div class="col-xs-6">
    <button type="button" style="height:38px;background-color:#75f9ed !important"
                    onclick="saveClinicalTemplate(1);"
                    class="btn btn-default btn-block no-margin fetchFavoriteTemplatesBtn" title="Bookmarks">
                    <i class="fa fa-save"></i> Save
    </button>
</div>
</div>
<!-- Card Body -->
</div>
</div>
</div>


<script>

</script>
