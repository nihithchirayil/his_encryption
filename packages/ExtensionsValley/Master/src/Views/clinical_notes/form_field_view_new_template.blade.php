<!-- Field Content -->
@php
$tableNamesArr = [];
$tinyNamesArr = [];
$certNamesArr = [];
$customDataArr = [];
$gcheckboxNamesArr = [];
$psearchNamesArr = [];
$is_favorite_option = !empty($form_list->is_favorite_option) ? $form_list->is_favorite_option : 0;
$patientVitalsArr = [];
$patientAllergyArr = [];
if ($has_custom_fields > 0) {
    if (!empty($patient_id)) {
        //vitals list start
        $patientVitalsArr = \DB::select("select ve.vital_value,vm.name
                from vital_entry ve
                join vital_master vm on vm.id = ve.vital_master_id
                where ve.patient_id = $patient_id
                and vm.defaultvalue = 1
                and ve.batch_no =( select batch_no from vital_entry where patient_id =$patient_id
                order by id desc limit 1 )
                and ve.deleted_at is null order by ve.id desc");
        //vitals list end

        //allergy list start
        $other_allergies = \DB::table('patient_other_allergies')
            ->where('patient_id', $patient_id)
            ->whereNull('deleted_at')
            ->select('allergy')
            ->first();

        if (!empty($other_allergies->allergy)) {
            $patientAllergyArr['other_allergies'] = $other_allergies->allergy;
        }

        $sql = "(Select CASE WHEN pa.type = '0' THEN p.item_desc
                ELSE G.generic_name END  as description
                from patient_allergy_medication pa
                left join product p on p.item_code = pa.item_code
                left join generic_name G on G.id = p.generic_name_id
                where pa.patient_id = '{$patient_id}' and pa.deleted_at is null)
                union
                (select gn.generic_name as description
                from generic_patient_allergy gpa
                join generic_name gn on gn.id = gpa.generic_id
                where gpa.patient_id = '{$patient_id}' and gpa.deleted_at is null )";

        $patient_allergy = \DB::select($sql);

        if (sizeof($patient_allergy) > 0) {
            $patientAllergyArr['patient_allergy'] = $patient_allergy;
        }
        //allergy list end
    }
}
@endphp
<div class="col-md-12">
    <div class="col-md-2 theadscroll nav-selection" style="position:relative;height:488px;
    -webkit-box-shadow: 3px 3px 8px -2px rgba(207,207,207,0.49) !important;
    -moz-box-shadow: 3px 3px 8px -2px rgba(207,207,207,0.49) !important;
    box-shadow: 3px 3px 8px -2px rgba(207,207,207,0.49) !important;">
        @if (!empty($all_form_fields))
            @if (sizeof($all_form_fields) > 0)
            <div class="list-group">
                @for ($i = 0; $i < sizeof($all_form_fields); $i++)
                    @php
                        $field_label = !empty($all_form_fields[$i]->field_label) ? $all_form_fields[$i]->field_label : '';
                        $form_field_id = !empty($all_form_fields[$i]->form_field_id) ? $all_form_fields[$i]->form_field_id : '';
                    @endphp
                    @if($field_label !='')
                        <a id="nav_{{$form_field_id}}" href="#{{$form_field_id}}" class="list-group-item list-group-item-action nav_class">{!! $field_label !!}</a>
                    @endif
                @endfor
            </div>
            @endif
        @endif
    </div>


    <div class="col-md-7 box-body theadscroll" id="form-fields-container" style="position:relative;height:488px;padding:15px !important;border-radius: 5px;">

    @if (!empty($all_form_fields))
    @if (sizeof($all_form_fields) > 0)
        @for ($i = 0; $i < sizeof($all_form_fields); $i++)
            @php
                $field_value = '';
                $entered_value = '';
                $form_id = '';
                $type_name = '';
                $type = '';
                $default_value = '';
                $is_required = '';
                $field_name = '';
                $field_label = '';
                $progress_table = '';
                $progress_value = '';
                $progress_text = '';
                $class_name = '';
                $table_structure = '';
                $table_details = '';
                $field_width = '';
                $field_height = '';
                $form_field_id = '';

                $type_name = !empty($all_form_fields[$i]->type_name) ? $all_form_fields[$i]->type_name : '';
                $type = !empty($all_form_fields[$i]->type) ? $all_form_fields[$i]->type : '';
                $field_name = !empty($all_form_fields[$i]->field_name) ? $all_form_fields[$i]->field_name : '';
                $field_label = !empty($all_form_fields[$i]->field_label) ? $all_form_fields[$i]->field_label : '';
                $default_value = !empty($all_form_fields[$i]->default_value) ? $all_form_fields[$i]->default_value : '';
                $form_field_id = !empty($all_form_fields[$i]->form_field_id) ? $all_form_fields[$i]->form_field_id : '';
                $form_id = !empty($all_form_fields[$i]->form_id) ? $all_form_fields[$i]->form_id : '';
                //progress search
                $progress_table = !empty($all_form_fields[$i]->progress_table) ? $all_form_fields[$i]->progress_table : '';
                $progress_value = !empty($all_form_fields[$i]->progress_value) ? $all_form_fields[$i]->progress_value : '';
                $progress_text = !empty($all_form_fields[$i]->progress_text) ? $all_form_fields[$i]->progress_text : '';
                //table
                $table_structure = !empty($all_form_fields[$i]->table_structure) ? $all_form_fields[$i]->table_structure : '';
                $table_details = !empty($all_form_fields[$i]->table_details) ? $all_form_fields[$i]->table_details : '';

                $radio = [];
                $gcheckbox = [];
                $selected_value = [];
                $optionsListSelected = [];
                $favorite_option_enabled = 0;

                //favorite option enabled then adjust width
                if ($is_favorite_option == 1 && ($type_name == 'TEXT' || $type_name == 'TEXT AREA')) {
                    $favorite_option_enabled = 1;
                }

                //field width
                if ($type_name == 'TEXT' || $type_name == 'DATE' || $type_name == 'SELECT BOX' || $type_name == 'PROGRESSIVE SEARCH') {
                    $field_full_width = 'col-xs-12 no-padding';
                    $field_content_width = 'col-xs-12 no-padding';
                    $field_favorite_width = 'col-xs-12 col-xs-offset-4';
                } else {
                    $field_full_width = 'col-xs-12 no-padding';
                    $field_content_width = 'col-xs-12 no-padding';
                    $field_favorite_width = 'col-xs-12 no-padding';
                }

            @endphp

            <div class="{{ $field_full_width }} ca_border_bottom app_bg" id="{{$form_field_id}}">

                @if (!empty($field_label))
                    @if ($type_name != 'HEADER TEXT')
                        <span class="text-blue">
                            <h6 style=""><b>{!! $field_label !!}</b></h6>
                        </span>
                    @endif
                @endif

                <div class="{{ $field_content_width }}">

                    @if ($type_name == 'TEXT')

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        <input type="text" class="form-control custom-text-box" name="{{ $field_name }}"
                            id="{{ $field_name }}" style="width:90%;" placeholder="" value="{{ $field_value }}">

                        @if ($favorite_option_enabled == 1)
                            <a class="btn btn-sm bg-blue fav-button-bottom assess_input_fav" style="float: right;margin-right:-14px !important;float: right;" title="Add To Bookmark" onclick="addToFavFormItem(this,'{{ $form_field_id }}')"><i class="fa fa-star-o"></i></a>
                        @endif

                        <a class="btn btn-sm bg-blue fav-button-bottom assess_input_refresh"
                            style="float: right;margin-right:-14px !important;float: right;"
                            title="Set Last Entered Data"
                            onclick="oldDataPreFetch('{{ $field_name }}','{{ $form_id }}','{{ $patient_id }}')"><i
                                class="fa fa-history"></i></a>

                    @elseif($type_name == "DATE")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        <input type="text" style="margin-bottom:12px;width:90%;" class="form-control datepicker  "
                            name="{{ $field_name }}" id="{{ $field_name }}" placeholder=""
                            value="{{ $field_value }}">

                    @elseif($type_name == "PATIENT DETAILS")

                        @php
                            $field_value = '';
                            if (isset($default_value) && !empty($default_value) && $patient_id != '') {
                                $fetchfields = explode('|', $default_value);
                                foreach ($fetchfields as $field) {
                                    $field = trim($field);
                                    if ($patient_id) {
                                        //field name is gender then fetch gender
                                        if ($field == 'gender') {
                                            $fieldgender = \DB::table('patient_master')
                                                ->where('id', '=', $patient_id)
                                                ->value($field);
                                            $field_value .=
                                                \DB::table('gender')
                                                    ->where('id', '=', $fieldgender)
                                                    ->value('name') . ' ';
                                        } elseif ($field == 'admitting_doctor') {
                                            $fieldadmitting_doctor = \DB::table('ip_visits')
                                                ->where('id', '=', $patient_id)
                                                ->orderBy('id', 'desc')
                                                ->limit(1)
                                                ->value('admitting_doctor');

                                            $field_value .= $fieldipno . ' ';
                                        } elseif ($field == 'admission_no') {
                                            $fieldipno = \DB::table('ip_visits')
                                                ->where('id', '=', $patient_id)
                                                ->orderBy('id', 'desc')
                                                ->limit(1)
                                                ->value($field);
                                            $field_value .= $fieldipno . ' ';
                                        } elseif ($field == 'marital_status') {
                                            $fieldpatient = \DB::table('patient_master')
                                                ->where('id', '=', $patient_id)
                                                ->value($field);
                                            if ($fieldpatient == '2') {
                                                $field_value .= ' Married ';
                                            } elseif ($fieldpatient == '1') {
                                                $field_value .= ' Single ';
                                            } else {
                                                $field_value .= ' Not Specified ';
                                            }
                                        } else {
                                            $field_value .=
                                                \DB::table('patient_master')
                                                    ->where('id', '=', $patient_id)
                                                    ->value($field) . ' ';
                                        }
                                    }
                                }
                            }
                        @endphp

                        <input type="text" class="form-control  " name="{{ $field_name }}"
                            id="{{ $field_name }}" placeholder="" value="{{ $field_value }}">

                    @elseif($type_name == "TINYMCE")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                            if ($include_patient_header == 1 && empty($entered_value)) {
                                $field_value = base64_decode($patient_header) . '<br>' . $field_value;
                            }

                            array_push($tinyNamesArr, $field_name);
                        @endphp

                        <textarea style="height:90px;border:none; !important;padding-left:10px !important;" name="{{ $field_name }}"
                            class="form-control texteditor  " id="{{ $field_name }}" cols="30" rows="10">
                            <div style="word-break: break-word !important;">
                                {!! $field_value !!}
                            </div>
                        </textarea>
                        <input type="hidden" name="include_patient_header" id="include_patient_header"
                            value="{!! $include_patient_header !!}" />

                    @elseif($type_name == "CHECK BOX")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        <ul class="select_button no-padding  ">
                            <li @if ($field_name == $field_value) class="active" @endif data-value="{{ $field_name }}">
                                <i class="fa fa-check-circle"></i>{{ $default_value }}
                                <input type="hidden" name="{{ $field_name }}" id="{{ $field_name }}"
                                    @if ($field_name == $field_value) value="{{ $field_name }}" @else disabled="disabled" value="" @endif />
                            </li>
                        </ul>

                    @elseif($type_name == "SELECT BOX")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }

                            $options = [];
                            $options = ExtensionsValley\Emr\FormFieldSelectboxOptions::where('form_field_id', '=', $form_field_id)->pluck('name', 'value');

                        @endphp

                        {!! Form::select($field_name, $options, !empty($field_value) ? $field_value : [], ['class' => 'form-control', 'placeholder' => 'Select Option', 'title' => $field_label]) !!}

                    @elseif($type_name == "RADIO BUTTON")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        @if (isset($form_field_id) && !empty($form_field_id))
                            @php
                                $radio = ExtensionsValley\Emr\FormFieldRadioOptions::where('form_field_id', '=', $form_field_id)->pluck('name', 'value');
                                $k = 0;
                            @endphp
                        @endif

                        @if (sizeof($radio) > 0)
                            @foreach ($radio as $value => $name)
                                <label>
                                    <input type="radio" @if ($field_value == $value) checked="checked" @endif name="{{ $field_name }}"
                                        id="radio-{{ $k }}" value="{{ $value }}"
                                        style="margin-bottom:15px !important;">
                                    <span>{{ $name }}</span>
                                    @php
                                        $k++;
                                    @endphp
                                </label>
                            @endforeach
                        @endif

                    @elseif($type_name == "GROUPED CHECKBOX")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        @if (isset($form_field_id) && !empty($form_field_id))
                            @php
                                $gcheckbox = ExtensionsValley\Emr\FormFieldGroupedCheckboxOptions::where('form_field_id', '=', $form_field_id)->pluck('name', 'value');
                                $g = 0;
                            @endphp
                        @endif

                        @if (sizeof($gcheckbox) > 0)
                            <ul class="select_button no-padding  ">
                                @foreach ($gcheckbox as $value => $name)

                                    @php
                                        $key_exist = property_exists($field_value, $value);
                                    @endphp
                                    <li @if ($key_exist) class="active" @endif data-value="{{ $value }}">
                                        <i class="fa fa-check-circle"></i>{{ $name }}
                                        <input type="hidden" name="{{ $field_name }}_gbox_{{ $value }}"
                                            id="gcheckbox-{{ $g }}" @if ($key_exist) value="{{ $value }}" @else disabled="disabled" value="" @endif>
                                    </li>
                                    @php
                                        array_push($gcheckboxNamesArr, $field_name . '_gbox_' . $value);
                                        $g++;
                                    @endphp
                                @endforeach
                            </ul>
                        @endif

                    @elseif($type_name == "PROGRESSIVE SEARCH")
                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }

                            if (!empty($progress_table)):
                                if (!empty((array) $field_value)):
                                    $selected_value = array_keys((array) $field_value);
                                    $selected_list = array_values((array) $field_value);
                                endif;

                                $optionsListSelected = \DB::table($progress_table)
                                    ->select([$progress_value, $progress_text])
                                    ->whereIn($progress_value, $selected_value)
                                    ->whereIn($progress_text, $selected_list)
                                    ->whereNull('deleted_at')
                                    ->get();
                            endif;

                            array_push($psearchNamesArr, $field_name);

                        @endphp

                        <select style="margin-bottom:15px;" class="progress-search-multiple form-control  "
                            name="{{ $field_name }}" id="{{ $field_name }}" data-table="{{ $progress_table }}"
                            data-value="{{ $progress_value }}" data-text="{{ $progress_text }}"
                            multiple="multiple">
                            @if (count($optionsListSelected) > 0)
                                @for ($p = 0; $p < count($optionsListSelected); $p++)
                                    <option selected="selected" value="{!! $optionsListSelected[$p]->$progress_value !!}">{!! $optionsListSelected[$p]->$progress_text !!}
                                    </option>
                                @endfor
                            @endif
                        </select>

                    @elseif($type_name == "TABLE")

                        @if (!empty($table_details))

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    if ($head_id == 0):
                                        $field_value = $default_value;
                                    endif;
                                }

                                array_push($tableNamesArr, $field_name);

                                $Tdetails = json_decode($table_details);
                                $Tstructure = json_decode($table_structure);

                                $table_details = $Tdetails->$field_name;
                                $table_structure = $Tstructure->$field_name;
                                //no rows
                                $rows = isset($table_details->rows) ? $table_details->rows : 0;
                                $columns = isset($table_details->columns) ? $table_details->columns : 0;
                                $rowHeader = isset($table_details->rowHeader) ? $table_details->rowHeader : 0;
                                $colHeader = isset($table_details->colHeader) ? $table_details->colHeader : 0;
                                if ($rowHeader == 1) {
                                    ++$columns;
                                }
                                if ($colHeader == 1) {
                                    ++$rows;
                                }

                                $tableColsArr = [];

                            @endphp

                            <div class="clearfix"></div>
                            <div class="theadscroll1 always-visible table_{{ $form_id }}  ">
                                <table class="">
                                    @for ($m = 0; $m < $rows; $m++)
                                        <tr>
                                            @for ($n = 0; $n < $columns; $n++)
                                                @php
                                                    $fieldSelValue = '';

                                                    $rwName = 'rows_' . $m;

                                                    //already enterd data
                                                    $already_entered_row_key = 'rows_' . $m;
                                                    $already_entered_column = !empty($field_value->$already_entered_row_key) ? $field_value->$already_entered_row_key : [];
                                                    if (sizeof($already_entered_column) > 0) {
                                                        $fieldSelValue = !empty($already_entered_column[$n]) ? $already_entered_column[$n] : '';
                                                    } else {
                                                        $row_data = !empty($table_structure->$rwName) ? $table_structure->$rwName : '';
                                                        $fieldSelValue = !empty($row_data[$n]) ? $row_data[$n] : '';
                                                    }

                                                    $fieldId = $field_name . '_' . $m . '_' . $n;
                                                    $fieldNm = $field_name . '_row_' . $m . '_' . $n . '[]';
                                                    array_push($tableColsArr, $fieldNm);
                                                @endphp
                                                <td>
                                                    <input @if ($colHeader == 1 && $m == 0) readonly tabIndex="-1" @endif @if ($rowHeader == 1 && $n == 0) readonly tabIndex="-1" @endif type="text"
                                                        name="{{ $fieldNm }}" id="{{ $fieldId }}"
                                                        value="{{ $fieldSelValue }}"
                                                        class="form-control assessment_table"
                                                        style="">
                                                </td>
                                            @endfor
                                        </tr>
                                    @endfor
                                </table>
                            </div>
                            <!-- all table column names array -->
                            <input type="hidden" name="{{ $field_name . '_#colsCount' }}" disabled
                                @if (isset($n)) value="{{ json_encode($n) }}" @endif>
                            <input type="hidden" name="{{ $field_name . '_#rowsCount' }}" disabled
                                @if (isset($m)) value="{{ json_encode($m) }}" @endif>
                        @endif

                    @elseif($type_name == "HEADER TEXT")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        <div class="col-xs-12 header-text text-center app_header" style="font-weight: 700;color:steelblue !important;padding:0px;margin:0px;">
                            @php
                                $default_value = str_replace('<h3>', '', $default_value);
                                $default_value = str_replace('<b>', '', $default_value);
                                $default_value = str_replace('<u>', '', $default_value);
                                $default_value = str_replace('</h3>', '', $default_value);
                                $default_value = str_replace('</b>', '', $default_value);
                                $default_value = str_replace('</u>', '', $default_value);
                            @endphp
                            <h5>{!! $default_value !!}</h5>
                        </div>

                    @elseif($type_name == "CERTIFICATE")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }

                            array_push($certNamesArr, $field_name);
                        @endphp

                        <textarea name="{{ $field_name }}" style="height:150px !important;"
                            class="form-control texteditor  " id="{{ $field_name }}" cols="30"
                            rows="10">{{ $field_value }}</textarea>

                    @elseif($type_name == "TEXT AREA")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):


                                    //$field_value = $default_value;
                                endif;
                            }
                        @endphp

                        <textarea rows="5" name="{{ $field_name }}" class="form-control custom-text-area  "
                            style="height:90px;border:none; !important;padding-left:10px !important;" id="{{ $field_name }}"
                            placeholder="">{{ $field_value }}</textarea>

                        @if ($favorite_option_enabled == 1)
                            <a class="btn btn-sm fav_textarea_icon bg-blue fav-button-bottom"
                                title="Add To Bookmark" style="float: right;margin-right:-14px !important;float: right;" onclick="addToFavFormItem(this,'{{ $form_field_id }}')"><i class="fa fa-star-o"></i></a>
                        @endif

                        <a class="btn btn-sm refresh_textarea_icon bg-blue prev-button-bottom"
                            title="Set Last Entered Data" style="float: right;margin-right:-14px !important;float: right;" onclick="oldDataPreFetch('{{ $field_name }}','{{ $form_id }}','{{ $patient_id }}')"><i
                                class="fa fa-history"></i></a>

                    @elseif($type_name == "CUSTOM DATA")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            $default_value = !empty($default_value) ? trim($default_value) : '';

                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                //Patient Allergy
                                if ($default_value == 'GET_PATIENT_ALLERGIES') {
                                    if (!empty($patientAllergyArr['other_allergies'])) {
                                        $other_allergy_html =
                                            "<table width='50%' border='1' style='border-collapse: collapse;margin-top:20px;'>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 <tr>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  <th colspan='2' style='height: 25px;'>Other Allergies</th>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </tr>\n
                                                                                                                                                                                                                                                                                                                                                                     <tr>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     <td colspan='2'>" .
                                            $patientAllergyArr['other_allergies'] .
                                            "</td>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 </tr>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </table>";
                                        $field_value .= $other_allergy_html;
                                    }
                                    if (!empty($patientAllergyArr['patient_allergy'])) {
                                        if (sizeof($patientAllergyArr['patient_allergy']) > 0) {
                                            $allergy_html = "<table width='50%' border='1' style='border-collapse: collapse;margin-top:20px;'>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <tr>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <th colspan='2' style='height: 25px;'>Allergies</th>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </tr>";
                                            for ($a = 0; $a < sizeof($patientAllergyArr['patient_allergy']); $a++) {
                                                $rdata = !empty($patientAllergyArr['patient_allergy'][$a]) ? $patientAllergyArr['patient_allergy'][$a] : [];
                                                $allergy_html .= "<tr><td colspan='2'>" . $rdata->description . '</td></tr>';
                                            }
                                            $allergy_html .= '</table>';
                                            $field_value .= $allergy_html;
                                        }
                                    }
                                } elseif ($default_value == 'GET_PATIENT_VITALS') {
                                    //Patient Vitals
                                    if (!empty($patientVitalsArr)) {
                                        if (sizeof($patientVitalsArr) > 0) {
                                            $vital_html = "<table width='50%' border='1' style='border-collapse: collapse;margin-top:20px;'>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <tr>\n
                                                                                                                                                                                                                                                                                                                                                                             <th colspan='2' style='height: 25px;'>Vitals</th>\n
                                                                                                                                                                                                                                                                                                                                                                           </tr>";
                                            for ($a = 0; $a < sizeof($patientVitalsArr); $a++) {
                                                $rdata = !empty($patientVitalsArr[$a]) ? $patientVitalsArr[$a] : [];
                                                $vital_html .=
                                                    "<tr>\n
                                                                                                                                                                                                                                                                                                                                                                      <td>" .
                                                    $rdata->name .
                                                    "</td>\n
                                                                                                                                                                                                                                                                                                                                                                            <td>" .
                                                    $rdata->vital_value .
                                                    "</td>\n</tr>";
                                            }
                                            $vital_html .= '</table>';
                                            $field_value .= $vital_html;
                                        }
                                    }
                                }
                            }

                            array_push($customDataArr, $field_name);
                        @endphp

                        <textarea name="{{ $field_name }}" class="form-control texteditor" id="{{ $field_name }}"
                            cols="30" rows="10">{{ $field_value }}</textarea>

                    @endif

                </div>

            </div>

        @endfor
    @endif
    <input type="hidden" disabled name="tinymce_names" id="tinymce_names" value="{{ json_encode($tinyNamesArr) }}" />
    <input type="hidden" disabled name="cert_names" id="cert_names" value="{{ json_encode($certNamesArr) }}" />
    <input type="hidden" disabled name="custom_field_data" id="custom_field_data"
        value="{{ json_encode($customDataArr) }}" />
    <input type="hidden" disabled name="table_names" id="table_names" value="{{ json_encode($tableNamesArr) }}" />
    <input type="hidden" disabled name="gcheckbox_names" id="gcheckbox_names"
        value="{{ json_encode($gcheckboxNamesArr) }}" />
    <input type="hidden" disabled name="psearch_names" id="psearch_names"
        value="{{ json_encode($psearchNamesArr) }}" />
    @endif

    </div>



    <div class="col-md-3">
        <div class="col-md-12 box-body no-padding theadscroll" style="border-radius:5px;padding:6px !important;position:relative;height:488px;">

              <!-- If Favorite Option Enabled -->
            @for ($i = 0; $i < sizeof($all_form_fields); $i++)
              @php
                $field_favorite_width = 'col-xs-12 no-padding';;
                $field_name = '';
                $form_field_id = '';
                $type_name = '';

                $field_name = !empty($all_form_fields[$i]->field_name) ? $all_form_fields[$i]->field_name : '';
                $form_field_id = !empty($all_form_fields[$i]->form_field_id) ? $all_form_fields[$i]->form_field_id : '';
                $type_name = !empty($all_form_fields[$i]->type_name) ? $all_form_fields[$i]->type_name : '';

              @endphp
              @if ($favorite_option_enabled == 1)
              <div class="{{ $field_favorite_width }}" style="margin-bottom: 15px;">
                  <div class="floating-table-list">
                      <div class="theadscroll" id="fav-{{ $field_name }}"
                          style="position: relative; height: auto; max-height: 400px; padding-right: 5px;">
                          @php
                              $favformitmlistTextBox = [];
                              if (!empty($favorited_items_field_arr[$form_field_id])) {
                                  $favformitmlistTextBox = !empty($favorited_items_field_arr[$form_field_id]) ? $favorited_items_field_arr[$form_field_id] : [];
                              }
                          @endphp

                          @if (isset($favformitmlistTextBox) && !empty($favformitmlistTextBox) && count($favformitmlistTextBox) > 0)
                              <table id="favListTable-{{ $i }}"
                                  class="table_sm no-margin fav-list-table table-striped">
                                  <thead>
                                      <tr style="background-color:#3498db;">
                                          <th colspan="2" style="color:white;">Bookmarks &nbsp;
                                              <i class="fa fa-caret-down"></i>
                                          </th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      @foreach ($favformitmlistTextBox as $dataitms)
                                          @if (isset($form_field_id) && !empty($form_field_id) && isset($dataitms['form_field_id']) && !empty($dataitms['form_field_id']) && $form_field_id == $dataitms['form_field_id'])
                                              @php
                                                  $cur_fav_form_item_id = isset($dataitms['form_field_id']) ? $dataitms['form_field_id'] : '';
                                                  $cur_fav_table_id = isset($dataitms['id']) ? $dataitms['id'] : '';
                                                  $cur_fav_val = isset($dataitms['value']) ? $dataitms['value'] : '';
                                                  $cur_fav_text = isset($dataitms['text']) ? $dataitms['text'] : '';
                                              @endphp

                                              <tr style="">
                                                  <td style="height:30px;cursor: pointer;"
                                                      onclick='addToSelected("{{ $field_name }}","{!! base64_encode($cur_fav_text) !!}","{{ $type_name }}")'>
                                                      {!! $cur_fav_text !!}</td>
                                                  <td width="5%">
                                                      <a style="float:right;color:white;" class="btn bg-red" name="remove"
                                                          onclick='removeFavorite("{{ $cur_fav_table_id }}","{{ base64_encode($cur_fav_text) }}",this)'
                                                          title="remove From List"><i
                                                              class="fa fa-trash-o"></i></a>
                                                  </td>
                                              </tr>
                                          @endif
                                      @endforeach
                                  </tbody>
                              </table>
                          @else
                              <table id="favListTable-{{ $i }}"
                                  class="table table-bordered table_sm no-margin table-striped fav-list-table hidden">
                                  <thead>
                                      <tr class="table_header_bg">
                                          <th colspan="2">Bookmarks &nbsp;
                                              <i class="fa fa-caret-down"></i>
                                          </th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                              </table>
                          @endif
                      </div>
                  </div>
              </div>
          @endif
          @endfor
          <!-- If Favorite Option Enabled -->
        </div>
    </div>






</div>



<!-- Field Content -->
