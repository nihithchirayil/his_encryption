@if(sizeof($res)>0)

    @if(($appointment_date > $current_date)&&($block_future_booking_status))
        <h5 style="color:rgb(237, 130, 100);">Future Booking Disabled!</h5>
    @else
        <h5 style="color:cornflowerblue;">Morning</h5>
        <input type="hidden" id="block_future_booking_status" value="{{$block_future_booking_status}}"/>
        @php
            $value_stringArray = [];
            $value_string = \DB::table('config_detail')->where('name','disable_tokens_in_web_appointments')->value('value_string');
            if(isset($value_string) && !empty($value_string)){
                $value_stringArray = explode(',', $value_string);
            }
           $j=0;
           $k=0;
        @endphp

        @foreach($res as $data)
            @if((strtolower($data->schedule_description) == 'morning' || $data->schedule_description == '') && !in_array($data->token_no, $value_stringArray))
                @if($j==0)
                    <h6 style="color:forestgreen;">Consulting Duration : {{date('h:i A',strtotime($data->consulting_time_from))}} - {{date('h:i A',strtotime($data->consulting_time_to))}}</h6>
                @endif
                <span class="slot_container btn">
                    <input type="radio" data-attr-consulting_time_from ="{{date('h:i A',strtotime($data->consulting_time_from))}}"  data-attr-consulting_time_to ="{{date('h:i A',strtotime($data->consulting_time_to))}}" data-attr-session_id = "{{$data->session_id}}" data-attr-token_no = "{{$data->token_no}}" data-attr-token_no name="slot_div" id="{{date('h:i a',strtotime($data->timeslot))}}"  value="{{date('h:i a',strtotime($data->timeslot))}}" class="slot_div"/>
                    <label for="{{date('h:i a',strtotime($data->timeslot))}}">
                        {{$data->token_no}}
                    </label>
                </span>
                @php
                    $j++;
                @endphp
            @endif

        @endforeach

        <h5 style="color:cornflowerblue;">Evening</h5>
        @foreach($res as $data)
            @if((strtolower($data->schedule_description) != 'morning' && $data->schedule_description != '') && !in_array($data->token_no, $value_stringArray))
                @if($k==0)
                    <h6 style="color:forestgreen;">Consulting Duration : {{date('h:i A',strtotime($data->consulting_time_from))}} - {{date('h:i A',strtotime($data->consulting_time_to))}}</h6>
                @endif
                <span class="slot_container btn">
                    <input type="radio" data-attr-consulting_time_from ="{{date('h:i A',strtotime($data->consulting_time_from))}}"  data-attr-consulting_time_to ="{{date('h:i A',strtotime($data->consulting_time_to))}}" data-attr-session_id = "{{$data->session_id}}" data-attr-token_no = "{{$data->token_no}}" name="slot_div" id="{{date('h:i a',strtotime($data->timeslot))}}"  value="{{date('h:i a',strtotime($data->timeslot))}}" class="slot_div"/>
                    <label for="{{date('h:i a',strtotime($data->timeslot))}}">
                        {{$data->token_no}}
                    </label>
                </span>
                @php
                    $k++;
                @endphp
            @endif
        @endforeach
    @endif
@else
    <label>No slots found</label>
@endif
