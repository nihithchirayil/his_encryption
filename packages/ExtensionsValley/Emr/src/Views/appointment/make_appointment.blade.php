<!DOCTYPE html>
<html lang="en">
<head>
<title>Grandis HIS</title>

     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="Tooplate">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
     <input type="hidden" id="base_url" value="{{ URL::to('/') }}">

     <link href="{{ asset('packages/extensionsvalley/appointment/css/bootstrap.min.css') }}" rel="stylesheet">
     <link href="{{ asset('packages/extensionsvalley/appointment/css/font-awesome.min.css') }}" rel="stylesheet">
     <link
        href="{{ asset('packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"
        rel="stylesheet">
     <link href="{{ asset('packages/extensionsvalley/dashboard/css/timepicker.css') }}" rel="stylesheet">
     <link href="{{ asset('packages/extensionsvalley/appointment/css/animate.css') }}" rel="stylesheet">
     <link href="{{ asset('packages/extensionsvalley/appointment/css/owl.carousel.css') }}" rel="stylesheet">
     <link href="{{ asset('packages/extensionsvalley/appointment/css/owl.theme.default.min.css') }}" rel="stylesheet">

     <link href="{{ asset('packages/extensionsvalley/appointment/css/tooplate-style.css') }}" rel="stylesheet">
     <link href="{{ asset('packages/extensionsvalley/default/css/select2.min.css') }}" rel="stylesheet">
     <link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">

    <style>
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            background: #f9f9f9;
            border: none;
            border-radius: 3px;
            box-shadow: none;
            font-size: 14px;
            font-weight: normal;
            margin-bottom: 15px;
            transition: all ease-in-out 0.4s;
            padding-top:5px !important;
            padding-bottom:10px !important;
        }
        .select2-container--default .select2-selection--single{
            border:none !important;
        }

        #appointment{
            /* min-height:920px !important; */
            margin-top:-30px !important;
        }
        .select2-container{
            height:45px !important;
        }

        .btn-default{
            font-size:11px !important;
            min-width:78px !important;
            margin-top:4px !important;
        }

        .slot_div:focus {
            background: red !important;
        }
        .slot_container{
            width:50px !important;
            border: 1px solid gainsboro !important;
        }
        .slot_container input{
            height: 15px !important;
            margin-top:5px !important;
        }
        #slots_area >.btn{
            padding:0px !important;
        }


        .slot_container input[type='radio']:after {
            width: 15px;
            height: 15px;
            border-radius: 0px;
            top: -2px;
            left: -1px;
            position: relative;
            background-color: #ffffff;
            content: '';
            display: inline-block;
            visibility: visible;
            border: 2px solid #ffffff;
        }

        .slot_container input[type='radio']:checked:after {
            width: 15px;
            height: 15px;
            border-radius: 0px;
            top: -2px;
            left: -1px;
            position: relative;
            background-color: #d9edf7;
            content: '';
            display: inline-block;
            visibility: visible;
            border: 2px solid #d9edf7;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            min-height:40px !important;
        }

        .slot_container:hover{
            background:#d5e0e6;
        }

       .btn-group {
            border: 1px solid lavender;
            width: 165px;
            padding-left: 10px;
            height:50px !important;
            border-radius:10px !important;
            background:aliceblue !important;
        }



    </style>

</head>
<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
    <input type="hidden" id="current_date" value="{{date('M-d-Y')}}"/>
    <input type="hidden" id="current_time" value="{{date('h:i A')}}"/>

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>

          </div>
     </section>



     <!-- MAKE AN APPOINTMENT -->
     <section id="appointment" data-stellar-background-ratio="3">
          <div class="container" style="margin-top:-45px !important;">
               <div class="row">
                <div class="col-md-12" style="margin-bottom:10px;">
                    @php
                        $hospital_header = \DB::table('company')->select('online_appointment_header')->first();
                    @endphp
                    {!!$hospital_header->online_appointment_header!!}
                </div>

                    <div class="col-md-7 col-sm-6">
                        <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                            <h2 style="color:deepskyblue;">Take an appointment</h2>
                        </div>
                        <div class="wow fadeInUp" data-wow-delay="0.8s">
                            <div class="col-md-6 col-sm-6">
                                <label for="select">Select Department</label>
                                {!! Form::select('department',$specialities,null, ['class' => 'form-control select2', 'id' =>'department','placeholder'=>'select','onchange'=>'selectDoctorsList()','style' => 'color:#555555; padding:4px 12px;']) !!}
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <label for="select">Select Doctor</label>
                                {!! Form::select('doctor_id',$doctors_list,null, ['class' => 'form-control select2', 'id' =>'doctor_id','placeholder'=>'select', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                            </div>

                            <div class="col-md-6 col-sm-6" style="margin-top:13px !important;">
                                <label for="date">Select Date</label>
                                <input type="text" name="appointment_date" autocomplete="off" value="{{date('M-d-Y')}}" onblur="getDoctorSlots();" class="form-control datepicker" id="appointment_date" placeholder="">

                            </div>

                            <div class="col-md-12 col-sm-12">
                                <h4>Available Tokens</h4>
                                <div class="col-md-12" id="slots_area" style="padding-bottom:10px;">

                                </div>
                            </div>


                       </div>
                    </div>


                    <div class="col-md-5 col-sm-6" style="padding-top:100px;">
                         <!-- CONTACT FORM HERE -->
                         <form id="appointment-form" role="form" method="post" action="#">

                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                   <div class="col-md-12 no-padding" style="margin-top:24px;">
                                        <div class="col-md-6" style="margin-left:-31px;">

                                             <div class="col-md-12 no-padding">
                                                <div class="btn-group">
                                                <input style="height:10px;" type="radio" checked="checked" name="patient_type" id="new_patient" value="1"/>
                                                  <label style="padding-top:15px;" for="new_patient">New Patient</label>
                                                </div>
                                             </div>
                                        </div>
                                        <div class="col-md-6" style="margin-left:-40px;">

                                             <div class="col-md-12 no-padding">
                                                <div class="btn-group">
                                                    <input style="height:10px;" type="radio" name="patient_type" id="existing_patient" value="2"/>
                                                    <label style="padding-top:15px;" for="existing_patient">ExistingPatient</label>
                                                </div>
                                             </div>
                                        </div>
                                   </div>

                                   <div class="col-md-10 col-sm-10" style="margin-top:18px;">
                                        <label for="telephone">Phone Number</label>
                                        <input type="tel" class="form-control" id="phone" name="phone" placeholder="Phone">
                                   </div>
                                   <div class="col-md-2 col-sm-2" style="padding-top:35px;margin-left:-18px !important;margin-top:18px;">
                                        <button type="button" class="btn bg-info" id="verify_phone" name="verify_phone" onclick="verifyPhone();">Verify</button>
                                   </div>
                                   <div class="col-md-12 col-sm-12 otp">
                                        <div class="col-md-10" style="margin-left:-14px;">
                                            <label for="name">Verify OTP</label>
                                            <input type="text" class="form-control" id="verify_otp" name="verify_otp" placeholder="Verify OTP">
                                        </div>
                                        <div class="col-md-2">
                                            <button style="margin-top:35px;margin-left:-18px;" type="button" class="btn bg-success" id="verify_continue" name="verify_continue" onclick="verifyContinue();">Continue</button>
                                        </div>
                                   </div>
                                   <div class="col-md-12 col-sm-12 name">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Full Name">
                                   </div>
                                   <div class="col-md-12 col-sm-12 uhid">
                                        <label for="uhid">Uhid</label>
                                        <input type="text" class="form-control" id="uhid" name="uhid" placeholder="Uhid">
                                        <input type="hidden" name="patient_id" id="patient_id" value=''/>
                                   </div>

                                   <div class="col-md-12 col-sm-12">
                                        <button style="background:aquamarine;color:darkgreen;" type="button" class="form-control" onclick="saveBooking();" id="save_booking" name="submit">Book Appointment</button>
                                        <button style="background:rgb(208, 255, 127);color:darkgreen;" type="button" class="form-control" onclick="CheckAppointment();" id="check_status" name="check_status">Check Status</button>
                                   </div>
                              </div>
                        </form>
                    </div>

               </div>
          </div>
     </section>


     {{-- patient select modal --}}
     <div id="patient_select_modal" class="modal fade" role="dialog" style="z-index:10000;">
        <div class="modal-dialog" style="width:50%;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm" style="background:orange;color:white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:white;">Select patient</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" id="patient_select_modal_content">

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{-- check status modal --}}
    <div id="modal_check_status" class="modal fade" role="dialog" style="">
        <div class="modal-dialog" style="width:50%;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-sm" style="background:orange;color:white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:white;">Check Status</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" id="modal_check_status_content">

                            <div class="col-md-6 col-sm-6">
                                <label for="select">Select Department</label>
                                <div class="clearfix"></div>
                                {!! Form::select('status_department',$specialities,null, ['class' => 'form-control select2', 'id' =>'status_department','placeholder'=>'select','onchange'=>'selectStatusDoctorsList()','style' => 'color:#555555; padding:4px 12px;width:100%;']) !!}
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <label for="select">Select Doctor</label>
                                <div class="clearfix"></div>
                                {!! Form::select('status_doctor_id',$doctors_list,null, ['class' => 'form-control select2', 'id' =>'status_doctor_id','placeholder'=>'select', 'style' => 'color:#555555; padding:4px 12px;width:100%;']) !!}
                            </div>

                            <div class="col-md-6 col-sm-6" style="margin-top:13px !important;">
                                <label for="date">Select Date</label>
                                <input type="text" name="status_appointment_date" autocomplete="off" value="{{date('M-d-Y')}}" class="form-control datepicker" id="status_appointment_date" placeholder="">
                            </div>
                            <div class="col-md-6 col-sm-6" style="margin-top:12px;">
                                <label for="status_name">Name</label>
                                <input type="text" class="form-control" id="status_name" name="status_name" placeholder="Name">
                            </div>

                            <div class="col-md-6 col-sm-6" style="margin-top:12px;">
                                <label for="telephone">Phone Number</label>
                                <input type="tel" class="form-control" id="status_phone" name="status_phone" placeholder="Phone">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-blue" onclick="CheckAppointmentStatus();">
                    Check Status
                    </button>
                </div>
            </div>

        </div>
    </div>



     <!-- SCRIPTS -->
    <script src="{{ asset('packages/extensionsvalley/appointment/js/jquery.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/plugins/momentjs/moment.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/default/js/timepicker.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/bootbox/bootbox.min.js') }}"></script>

    <script src="{{ asset('packages/extensionsvalley/appointment/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/appointment/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/appointment/js/jquery.stellar.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/appointment/js/wow.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/appointment/js/smoothscroll.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/appointment/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/appointment/js/custom.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/select2.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script src="{{ asset('packages/extensionsvalley/appointment/js/appointment.js') }}"></script>
</body>
</html>
