<div class="col-md-12">
    <div class="col-md-4 no-padding box-body" id="reaction_list_container" style="border-radius:5px;background:white !important;height:575px;">

    </div>
    <!-----Add new reaction---------->
    <div class="col-md-8" style="height:575px;">
        <div class="col-md-12 box-body" style="border-radius:5px;background:white !important;">
            {!!Form::open(['name'=>'add_drug_reaction','id'=>'add_drug_reaction']) !!}
            <div class="col-md-12">
                <h5><b>{{$title}}</b></h5>
            </div>
            <div class="col-md-12" style="margin-top:15px !important;">
                <div class="mate-input-box" style="padding:24px 6px 4px 4px !important;">
                    <label>Suspected Drug</label>
                    {!! Form::select('suspected_drug', $drug_array,0, ['class' => 'form-control select2 bottom-border-text','placeholder'=>'','title' => 'Suspected Drug', 'id' => 'suspected_drug', 'style' => 'color:#555555; padding:2px 12px;margin-top:5px;']) !!}
                </div>
                <input type="hidden" name="drug_reaction_item_id" id="drug_reaction_item_id" value="">
                <input type="hidden" name="drug_reaction_detail_array" id="drug_reaction_detail_array" value="{{$drug_detail_array}}">
                <input type="hidden" value='' name="drug_reaction_edit_id" id="drug_reaction_edit_id"/>
            </div>
            <div class="col-md-6" style="margin-top:5px !important;">
                <div class="mate-input-box" style="padding:14px 6px 4px 4px !important;">
                    <label>Dose</label>
                    <input type="text" name="drug_reaction_dose" readonly="readonly" placeholder="Dose" class="form-control" id="drug_reaction_dose" style="color:#555555; padding:2px 12px;margin-top:5px;">
                </div>
            </div>
            <div class="col-md-6" style="margin-top:5px !important;">
                <div class="mate-input-box" style="padding:14px 6px 4px 4px !important;">
                    <label>Batch No</label>
                    <input type="text" name="drug_reaction_batch_no" placeholder="Batch No" class="form-control" id="drug_reaction_batch_no" style="color:#555555; padding:2px 12px;margin-top:5px;">
                </div>
            </div>
            <div class="col-md-6" style="margin-top:5px !important;">
                <div class="mate-input-box" style="padding:14px 6px 4px 4px !important;">
                    <label>Expiry Date</label>
                    <input type="text" name="drug_reaction_expiry_date" placeholder="Expiry Date"  readonly="readonly" class="form-control" id="drug_reaction_expiry_date" style="color:#555555; padding:2px 12px;margin-top:5px;">
                </div>
            </div>
            <div class="col-md-6" style="margin-top:5px !important;">
                <div class="mate-input-box" style="padding:41px 6px 4px 4px !important;">
                    <label>Date&Time Of Suspected Drug Started</label>

                        <div class="col-md-6" style="margin-top:-20px;">
                        <input type="text" id='drug_reaction_start_date' name='start_date' class="form-control start_date" 
                        value=""
                            data-attr="date" placeholder="Date">
                        </div>
                        <div class="col-md-6" style="margin-top:-20px;">
                        <input type="text" id='drug_reaction_start_time' name='start_time' class="form-control timepicker" value=""
                            data-attr="date" placeholder="Time">
                        </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-top:5px !important;">
                <div class="mate-input-box" style="padding:24px 6px 4px 4px !important;">
                    <label>Brief Description Of Reaction</label>
                    <textarea id="drug_reaction_description" name="drug_reaction_description" style="background:white;"></textarea>
                </div>
            </div>

            <div class="col-md-12" style="margin-top:100px !important;">
                <button type="button" class="btn bg-green pull-right" onclick="saveDrugReaction();" id="" name=""><i class="fa fa-save"></i> Save</button>

                <button type="reset" class="btn btn-primary pull-right" id="" name=""><i class="fa fa-repeat"></i> Reset</button>
            </div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>

