<div class="col-md-12">
    <h5><b>Reaction List</b></h5>
</div>
@if(sizeof($res)>0)
<div class="col-md-12" style="position: relative;max-height:600px;">
    <table id="" border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper" style="border-collapse: collapse;">
        <thead>
            <tr style="background-color:aquamarine;">
                <th style="width:10%" class="header_bg">Sl.No</th>
                <th style="width:60%" class="header_bg">Drug Name</th>
                <th style="width:30%" class="header_bg">Actions</th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
            @endphp
            @foreach($res as $key => $value)
                @php
                    $html = "";
                    $html .= "<div class='col-md-12' style='padding:15px;margin-bottom:15px;background-color:rgb(186, 246, 226);'>";
                        $html .= "<table>";
                            $html .= "<tr>";
                                $html .= "<td width='35%'>Suspected Drug</td>";
                                $html .= "<td width='5%'>:</td>";
                                $html .= "<td width='60%'>".$value->item_desc."</td>";
                            $html .= "</tr>";
                            $html .= "<tr>";
                                $html .= "<td width='35%'>Dose</td>";
                                $html .= "<td width='5%'>:</td>";
                                $html .= "<td width='60%'>".$value->dose."</td>";
                            $html .= "</tr>";
                            $html .= "<tr>";
                                $html .= "<td width='35%'>Batch No</td>";
                                $html .= "<td width='5%'>:</td>";
                                $html .= "<td width='60%'>".$value->batch_no."</td>";
                            $html .= "</tr>";
                            $html .= "<tr>";
                                $html .= "<td width='35%'>Expiry Date</td>";
                                $html .= "<td width='5%'>:</td>";
                                $html .= "<td width='60%'>".$value->expiry_date."</td>";
                            $html .= "</tr>";
                            $html .= "<tr>";
                                $html .= "<td width='35%'>Drug Started At</td>";
                                $html .= "<td width='5%'>:</td>";
                                $html .= "<td width='60%'>".date('M-d-Y h:i A',strtotime($value->drug_started_at))."</td>";
                            $html .= "</tr>";
                            $html .= "<tr>";
                                $html .= "<td width='35%'>Description</td>";
                                $html .= "<td width='5%'>:</td>";
                                $html .= "<td width='60%'>".$value->description."</td>";
                            $html .= "</tr>";
                            $html .= "<tr>";
                                $html .= "<td width='35%'>Reported By</td>";
                                $html .= "<td width='5%'>:</td>";
                                $html .= "<td width='60%'>".$value->reported_by."</td>";
                            $html .= "</tr>";
                            $html .= "<tr>";
                                $html .= "<td width='35%'>Reported At</td>";
                                $html .= "<td width='5%'>:</td>";
                                $html .= "<td width='60%'>".$value->reported_at."</td>";
                            $html .= "</tr>";
                        $html .= "</table>";
                    $html .= "</div>";
                @endphp
                <tr id="drug_reaction_table_row_{{$value->id}}">
                    <td>{{$i}}</td>
                    <td style="text-align:left;">{{$value->item_desc}}</td>
                    <td>
                        <a style="box-shadow:none;" href="javascript:void(0);" class="btn btn-warning btn-xs tooltip-drug-details" data-toggle="popover" title="Suspected Drug Details" data-content="{{$html}}" data-html="true" data-trigger="hover">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a style="box-shadow:none;" title="Edit" href="javascript:void(0);" class="btn btn-primary btn-xs" onclick="edit_drug_reaction_data('{{$value->id}}');">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a style="box-shadow:none;" title="Edit" href="javascript:void(0);" class="btn bg-red btn-xs" onclick="delete_drug_reaction_data('{{$value->id}}');">
                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                        </a>
                    </td>
                </tr>
                @php
                    $i++;
                @endphp
            @endforeach
        </tbody>
    </table>
</div>
@else
<div class="col-md-12">
    <div class="alert alert-info">
        <strong>Info!</strong> No Drug Reaction Data Found.
    </div>
</div>
@endif
