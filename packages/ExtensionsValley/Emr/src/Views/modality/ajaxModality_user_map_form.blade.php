<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            if (url && url != 'undefined') {
                var user_id = $("#search_user_id").val();
                var modality_id = $("#search_modality_id").val();
                var param = {user:user_id,modality_name:modality_id};

                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function() {
                        $("#appendFrm").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#009869'
                        });
                    },
                    success: function(data) {
                        $('#appendFrm').html(data);
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30

                        });
                        setTimeout(function() {
                            $('.theadfix_wrapper').floatThead({
                                position: 'absolute',
                                scrollContainer: true
                            });
                        }, 400);
                    },
                    complete: function() {
                        $("#appendFrm").LoadingOverlay("hide");
                    },
                    error: function() {
                        toastr.error("Error Please Check Your Internet Connection");
                    }
                });
            }
            return false;
        });

    });
</script>
<div class="theadscroll" style="position: relative; height: 350px;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th>Modality Name</th>
                <th>Group Name</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @if (count($user_access_list) > 0)
                @foreach ($user_access_list as $menu)
                    <tr style="cursor: pointer;"
                        onclick="editLoadData(this,
                                            '{{ $menu->id }}',
                                            '{{ $menu->modality }}',
                                            '{{ $menu->user_id }}',
                                            '{{ $menu->status }}');">
                        <td class="acl_key common_td_rules">{{ $menu->modality_name }}</td>
                        <td class="group_id common_td_rules">{{ $menu->user_name }}</td>
                        <td class="status common_td_rules">{{ $menu->status_name }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5" class="vendor_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important;">
        {!! $page_links !!}
    </ul>
</div>
