<div class="col-md-12 theadscroll" style="position: relative; height: 350px;">
    <table class="table table_round_border theadfix_wrapper styled-table">
        <thead>
            <tr class="table_header_bg">
                <th>Sl.No</th>
                <th style="text-align:center;width:50%">Checklist</th>
                <th>Applicable</th>
                <th>Not Applicable</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($receivedData))
            @php $i=1; @endphp
            @foreach($receivedData as $rec)
            <tr style="cursor: pointer">
                <td style="text-align: center;">{{$i}}</td>
                <td style="text-align: left;">{{ !empty($rec->name) ?$rec->name :'' }}</td>
                <td style="text-align: center;padding-right: 20px" >
                    @if(in_array($rec->id,$chklist))
                        @php $chk_prop = 'checked'; @endphp
                    @else
                        @php $chk_prop = ''; @endphp
                    @endif
                    <input type="checkbox" class="chklist" name="check_list[]" {{$chk_prop}} value="{{$rec->id}}">
                </td>
                <td style="text-align: center;padding-right: 20px" >
                    @if(in_array($rec->id,$na_chklist))
                        @php $na_chk_prop = 'checked'; @endphp
                    @else
                        @php $na_chk_prop = ''; @endphp
                    @endif
                    <input type="checkbox" class="nachklist" name="na_check_list[]" {{$na_chk_prop}} value="{{$rec->id}}">
                </td>
            </tr>
            @php $i++; @endphp
            @endforeach
            @else
            <tr>
                <td colspan="5">No Records Found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
<div class=" clearfix"></div>
<div class="col-md-4 padding_sm pull-right">
    <label for="">&nbsp;</label>
    <div class="clearfix"></div>
    <button id="savechkbtn" type="button" class="btn btn-block light_purple_bg chkbtncls" onclick="saveCheckList('{{$row_id}}','{{$status_update}}',0)">
        <i id="savecheckdataspin" class="fa fa-save"></i>
        Save
    </button>
</div>
<div class="col-md-4 padding_sm pull-right">
    <label for="">&nbsp;</label>
    <div class="clearfix"></div>
    <button class="btn btn-block btn-warning chkbtncls" id="revertdatabtn" onclick="saveCheckList('{{$row_id}}','{{$status_update}}',1)">
        <i class="fa fa-times" id="revertcheckspin"></i> 
        Revert
    </button>
</div>

