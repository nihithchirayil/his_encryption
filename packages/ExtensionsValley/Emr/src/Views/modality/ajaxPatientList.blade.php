<div class="col-md-12">
    <div class="col-md-3 padding_sm">
        <div class="mate-input-box">
            <label for="">Patient Name/UHID</label>
            <div class="clearfix"></div>
            <input type="text" autocomplete="off" onkeyup="progressiveSearchFn('new_patient_name')" class="form-control hidden_search" name="new_patient_name" id="new_patient_name" value="" autocomplete="false">
            <input type="hidden" class="form-control" name="new_patient_name_hidden" id="new_patient_name_hidden" value="">
             <input type="hidden" class="form-control" name="new_patient_name_uhid_hidden" id="new_patient_name_uhid_hidden" value="">
        </div>
    </div>
    <div id="new_patient_nameAjaxDiv" class="ajaxSearchBox" style="margin: 14px 0px 0px 0px !important;z-index: 9999999;max-height: 450px !important;width:30% !important"></div>
    <div class="col-md-3 padding_sm">
        <div class="mate-input-box">
            <label for="">Service</label>
            <div class="clearfix"></div>
            <input type="text" autocomplete="off" onkeyup="progressiveSearchFn('new_service_desc')" class="form-control" name="new_service_desc" id="new_service_desc" value="" autocomplete="false">
            <input type="hidden" class="form-control" name="new_service_desc_hidden" id="new_service_desc_hidden" value="">
        </div>
    </div>
    <div id="new_service_descAjaxDiv" class="ajaxSearchBox" style="margin: 14px 0px 0px 260px !important;z-index: 9999999;max-height: 450px !important;width:30% !important"></div>
    <div class="col-md-1 padding_sm pull-right">
        <label for="">&nbsp;</label>
        <div class="clearfix"></div>
        <button class="btn btn-block btn-warning chkbtncls" id="revertdatabtn" onclick="clearAll()">
            <i class="fa fa-times" id="revertcheckspin"></i> 
            Clear All
        </button>
    </div>
    <div class="col-md-1 padding_sm pull-right">
        <label for="">&nbsp;</label>
        <div class="clearfix"></div>
        <button id="savenewpatientbtn" type="button" class="btn btn-block light_purple_bg chkbtncls" onclick="saveNewPatient()">
            <i id="savenewpatientbtnspin" class="fa fa-save"></i>
            Save
        </button>
    </div>
</div>


