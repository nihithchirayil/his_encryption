@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <style>
    </style>
@endsection
@section('content-area')

    <div class="right_col">
        <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;"> {{ $title }}</div>
        <div class="row codfox_container">
            <div class="col-md-8 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <form action="{{ route('extensionsvalley.modality.list_pacs_access') }}" id="categorySearchForm"
                                method="POST">
                                {!! Form::token() !!}
                                <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Pacs Status</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select(
                                            'pacs_name',
                                            ['-1' => ' Select Status'] + $pacs_status_master->toArray(),
                                            $searchFields['pacs_name'] ?? '',
                                            [
                                                'class' => 'form-control select2',
                                            ],
                                        ) !!}
                                    </div>
                                </div>

                                <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Group</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('group', ['-1' => ' Select Group'] + $group->toArray(), $searchFields['group'] ?? '', [
                                            'class' => 'form-control select2',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i>
                                        Search</button>
                                </div>
                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                            class="fa fa-times"></i> Clear</a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="theadscroll" style="position: relative; height: 350px;">
                            <table
                                class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th>Pacs Name</th>
                                        <th>Group Name</th>
                                        <th>Approve</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($menu_list) > 0)
                                        @foreach ($menu_list as $menu)
                                            <tr style="cursor: pointer;"
                                                onclick="editLoadData(this,
                                            '{{ $menu->id }}',
                                            '{{ $menu->pacs_id }}',
                                            '{{ $menu->group_id }}',
                                            '{{ $menu->approve }}',
                                            '{{ $menu->status }}');">
                                                <td class="acl_key common_td_rules">{{ $menu->menu_name }}</td>
                                                <td class="group_id common_td_rules">{{ $menu->group_name }}</td>
                                                <td class="approve common_td_rules">{{ $menu->approve_status }}</td>
                                                <td class="status common_td_rules">{{ $menu->status_name }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" class="vendor_code">No Records found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 text-right">
                            <ul class="pagination purple_pagination" style="text-align:right !important;">
                                {!! $page_links !!}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix" id="paccs_access_frm">
                        <form action="{{ route('extensionsvalley.modality.save_pacs_access') }}" method="POST"
                            id="menuaccessForm">
                            {!! Form::token() !!}
                            <input type="hidden" name="menu_access_id" id="menu_access_id" value="0">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Pacs Status<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('pacs_id', ['-1' => ' Select Status'] + $pacs_status_master->toArray(), null, [
                                        'class' => 'form-control pacs_id select2',
                                        'id' => 'pacs_id',
                                    ]) !!}
                                    <span class="error_red">{{ $errors->first('pacs_id') }}</span>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Group Name<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('group_id', ['-1' => ' Select Group'] + $group->toArray(), '', [
                                        'class' => 'form-control select2',
                                        'id' => 'group_id',
                                    ]) !!}
                                    <span class="error_red">{{ $errors->first('group_id') }}</span>
                                </div>
                            </div>

                            <div class="col-md-12 padding_sm">

                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Approve Status</label>
                                        <div class="clearfix"></div>
                                        <input type="checkbox" class="" name="approve" id="approve">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Status<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1">{{ __('Active') }}</option>
                                        <option value="0">{{ __('Inactive') }}</option>
                                    </select>
                                    <span class="error_red">{{ $errors->first('status') }}</span>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-6 padding_sm">
                                <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                        class="fa fa-times"></i> Cancel</a>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <span class="btn btn-block light_purple_bg" onclick="saveModalityAccess()"><i
                                        class="fa fa-save"></i> Save</span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
    <script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });

        });

        function editLoadData(obj, id, pacs_id, group_id, approve, status) {
            $('#menu_access_id').val(id);
            $('#pacs_id').prop('readonly', true).val(pacs_id).select2().trigger('change');
            $('#group_id').val(group_id).select2().trigger('change');
            $('#status').val(status);
            $('#menuaccessForm').attr('action', '{{ $updateUrl }}');
            if (approve == 1) {
                $("#approve").prop("checked", true);
            } else {
                $("#approve").prop("checked", false);
            }
        }

        function resetFunction() {
            $('#menuaccessForm')[0].reset();
        }

        function saveModalityAccess() {
            var menu_access_id = $('#menu_access_id').val();
            var pacs_id = $('#pacs_id').val();
            var group_id = $('#group_id').val();
            var status = $('#status').val();
            if ($("#approve").prop("checked")) {
                var approve = 1;
            } else {
                var approve = 0;
            }
            var param = {
                menu_access_id: menu_access_id,
                pacs_id: pacs_id,
                group_id: group_id,
                status: status,
                approve:approve
            };
            $.ajax({
                type: "POST",
                url: $('#menuaccessForm').attr('action'),
                data: param,
                beforeSend: function() {
                    $('#paccs_access_frm').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                },
                success: function(msg) {
                    if (msg) {
                        var obj = JSON.parse(msg);
                        var msg = obj.message;
                        var status = obj.status;
                        if (status == 1) {
                            toastr.success(msg);
                            $("#paccs_access_frm").modal('hide');
                            setTimeout(function() {
                               window.location.reload();
                            }, 500);
                        } else {
                            toastr.error(msg);
                        }
                    }
                },
                complete: function() {
                    $('#paccs_access_frm').LoadingOverlay("hide");
                }
            });
        }
    </script>

@endsection
