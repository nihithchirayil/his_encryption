<style>
    table td.highlighted {
        background-color:rgb(182, 255, 87);
    }
    table td.scheduled {
        background-color:rgb(255, 191, 87);
    }
    table td.scheduled_fixed {
        background-color:rgb(255, 191, 87);
    }

    .tooltip {
        position: absolute;
        z-index: 99999;
        color:white !important;
        background-color: rgb(245, 43, 184) !important;
        min-width:250px !important;
        border-radius:5px !important;
        padding:10px !important;
    }

    .tooltip-inner {
        background-color: rgb(245, 43, 184);
    }

    .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td{
        border:1px solid #f4f4f4 !important;
    }

</style>
<div class="row">
    <input type="hidden" name="row_id_hidden" id="row_id_id_hidden" value="{{$row_id}}">
    <input type="hidden" name="status_update_hidden" id="status_update_hidden" value="">
    <!---time allocation------------------>
    <div class="col-md-12">
        <div class="col-md-12" style="margin-top:15px;">
            <button type="button" class="btn bg-green pull-right" onclick="SavePacsBooking();" id="btnSavePacsBooking">
                <i class="fa fa-save"></i> Save
            </button>
            <button type="reset" class="btn bg-orange pull-right" id="reset_pacs_book">
                <i class="fa fa-save"></i> Reset
            </button>
        </div>
    </div>
    <div class="col-md-12 no-padding" id="time_allocation_container">
        <div class=" box-body theadscroll table-responsive text-nowrap" style="position:relative;height:595px;">
            <table id="shedule_time_table" class='table table-condensed table-bordered     theadfix_wrapper' style="font-size: 12px;">
                <thead>

                    <tr class="common_table_header">
                        <td style="width:130px !important;">Time
                        </td>

                        <td style="width:120px!important;"></td>
                    </tr>
                </thead>
                <tbody>
                    @php $k=1; @endphp
                    @for($k=1; $k<= sizeof($slots);$k++)
                    <tr id="time_{{$slots[$k]['slot_start_time']}}">
                        <td style="color: #2e749d;font-weight: 600;">
                            {{$slots[$k]['slot_start_time']}}-{{$slots[$k]['slot_end_time']}}
                        </td>
                        @endfor
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
