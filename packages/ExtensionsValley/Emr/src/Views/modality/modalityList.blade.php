@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop

@section('css_extra')
<style>

</style>
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
@endsection
@section('content-area')
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="pacs_viewer_prefix" value="{{ $pacs_viewer_prefix }}">
<input type="hidden" id="pacs_report_prefix" value="{{ $pacs_report_prefix }}">
<?php $company = \DB::table('company')->pluck('sub_code'); ?>
<input type="hidden" id="table_company_code" value="<?= $company[0] ?>">
<div class="right_col" >
    <div class="row" style="text-align: right;font-size: 12px; font-weight: bold;"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                        {!! Form::token() !!}
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Patient Name/UHID</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" onkeyup="progressiveSearchFn('patient_name')" class="form-control hidden_search" name="patient_name" id="patient_name" value="" autocomplete="false">
                                <input type="hidden" class="form-control" name="patient_name_hidden" id="patient_name_hidden" value="">
                                <input type="hidden" class="form-control" name="patient_name_uhid_hidden" id="patient_name_uhid_hidden" value="">
                            </div>
                        </div>
                        <div id="patient_nameAjaxDiv" class="ajaxSearchBox" style="margin: 14px 0px 0px 0px !important;z-index: 9999999;max-height: 450px !important;width:25% !important"></div>
<!--                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Service</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" onkeyup="progressiveSearchFn('service_desc')" class="form-control" name="service_desc" id="service_desc" value="" autocomplete="false">
                                <input type="hidden" class="form-control" name="service_desc_hidden" id="service_desc_hidden" value="">
                            </div>
                        </div>-->
<!--                        <div id="service_descAjaxDiv" class="ajaxSearchBox" style="margin: 14px 0px 0px 209px !important;z-index: 9999999;max-height: 450px !important;width:25% !important"></div>-->
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Modality</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" onkeyup="progressiveSearchFn('modality_desc')" class="form-control" name="modality_desc" id="modality_desc" value="" autocomplete="false">
                                <input type="hidden" class="form-control" name="modality_desc_hidden" id="modality_desc_hidden" value="">
                            </div>
                        </div>
                        <div id="modality_descAjaxDiv" class="ajaxSearchBox" style="margin: 14px 0px 0px 209px !important;z-index: 9999999;max-height: 450px !important;width:25% !important"></div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">From Date</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control datepicker" name="from_date" id="from_date" value="<?= date('Mon-dd-Y') ?>">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">To Date</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" class="form-control datepicker" name="to_date" id="to_date" value="<?= date('Mon-dd-Y') ?>">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm" style="width:13% !important">
                            <div class="mate-input-box">
                                <label for="">Status</label>
                                <div class="clearfix"></div>
                            {!! Form::select('status_filter',$status_list->toArray() + array("paid"=> "Paid"),'',['class' => 'form-control custom_floatinput select2','placeholder' => 'All','title' => 'Status','id' => 'status_filter','style' => 'color:#555555; padding:2px 12px;']) !!}
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button id="searchdatabtn" type="button" class="btn btn-block light_purple_bg" onclick="searchList(20, 0)"><i id="searchdataspin" class="fa fa-search"></i>
                                Search</button>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Clear</a>
                        </div>
                        <div class="padding_sm pull-right">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <button id="add_new_patient" type="button" class="btn btn-block btn-info" onclick="addNewPatient()">
                                <i id="add_new_patient_spin" class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>              
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin" id="modality_list_div">

            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="check_list_modal" role="dialog" aria-labelledby="check_list_modal" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 45%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
            </div>
            <div class="modal-body" style="min-height:400px;height:500px;">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm' id="append_check_list">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="add_new_pat_modal" role="dialog" aria-labelledby="add_new_pat_modal" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 80%"  id="dialog_modal">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="pat_name_header"></h4>
            </div>
            <div class="modal-body" style="min-height:300px;height:300px;" id="modalbody">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm' id="append_pat_modal">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="radiology_pacs_viewer_iframe_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:97%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title"> Radiology Image Viewer</h4>
            </div>
            <div class="modal-body no-padding" style="min-height:560px;" >
                <div class="col-md-12" style="margin-top:10px">
                    <iframe src="" title="PACS Viewer" id="pacs_viewer_iframe" style="width:100%; height: 550px;"></iframe>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="time_list_modal" role="dialog" aria-labelledby="time_list_modal" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1300px;width: 40%">
        <div class="modal-content">
            <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                <button type="button" class="close close_white"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 30px;">&times;</span></button>
            </div>
            <div class="modal-body" style="min-height:200px;">
                <div class="row padding_sm">
                    <div class='col-md-12 padding_sm' id="append_time_list">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/modality.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>

@endsection
