<div class="col-md-12">
    @if(!empty($prev_date))
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label for="">Previously Added Time</label>
            <div class="clearfix"></div>
            <input type="textt" readonly autocomplete="off"  class="form-control" name="" id="" value="{{@$prev_date ? date('M-d-Y h:i:s',strtotime($prev_date)):''}}" autocomplete="false">
        </div>
    </div>
    @endif
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label for="">Time</label>
            <div class="clearfix"></div>
            <input type="text" autocomplete="off"  class="form-control date_time_picker" name="date_time" id="date_time" value="" autocomplete="false">
        </div>
    </div>
    
    <div class="col-md-2 padding_sm pull-right">
        <label for="">&nbsp;</label>
        <div class="clearfix"></div>
        <button class="btn btn-block btn-warning time_btn" id="revertdatabtn" onclick="saveDateTime('{{$row_id}}','{{$status_update}}',1)">
            <i class="fa fa-times" id="revertcheckspin"></i> 
            Revert
        </button>
    </div>
    <div class="col-md-2 padding_sm pull-right">
        <label for="">&nbsp;</label>
        <div class="clearfix"></div>
        <button id="savenewtimebtn" type="button" class="btn btn-block light_purple_bg time_btn" onclick="saveDateTime('{{$row_id}}','{{$status_update}}',0)">
            <i id="savenewtimebtnspin" class="fa fa-save"></i>
            Save
        </button>
    </div>
</div>
<script>
$('#date_time').datetimepicker({
        format: 'hh:mm:ss A',
    });
    function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ':' + seconds +' '+ ampm;
  return strTime;
}

var current_time = (formatAMPM(new Date));
$("#date_time").val(current_time);
</script>