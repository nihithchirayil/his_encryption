@if($offset == 0)
<div class="row">
    <div class="col-md-12" style="padding-bottom: 10px">
            <span onclick="getStatusFilter('7')" style="width: 2%;background-color:#f3deb8;border-radius: 5px;padding-top: 3px;padding-left: 36px;padding-right: 36px;padding-bottom: 4px;cursor:pointer">
            Image received</span>
        
            <span onclick="getStatusFilter('8')" style="width: 2%;color:#fff;background-color:#8f98bd;border-radius: 5px;padding-top: 3px;padding-left: 36px;padding-right: 36px;padding-bottom: 4px;cursor:pointer">
            Image completed</span>        
        
            <span onclick="getStatusFilter('9')" style="width: 2%;background-color:#62ebc5;border-radius: 5px;padding-top: 3px;padding-left: 36px;padding-right: 36px;padding-bottom: 4px;cursor:pointer">
            InDiction</span>        
        
            <span onclick="getStatusFilter('10')" style="width: 2%;background-color:#f6c1f9;border-radius: 5px;padding-top: 3px;padding-left: 36px;padding-right: 36px;padding-bottom: 4px;cursor:pointer">
            Preliminary</span>        
        
            <span onclick="getStatusFilter('11')" style="width: 2%;background-color:#f9c1c1;border-radius: 5px;padding-top: 3px;padding-left: 36px;padding-right: 36px;padding-bottom: 4px;cursor:pointer">
            Reviewed</span> 
        
            <span onclick="getStatusFilter('12')" style="width: 2%;background-color:#a2c787;border-radius: 5px;padding-top: 3px;padding-left: 36px;padding-right: 36px;padding-bottom: 4px;cursor:pointer">
            Reporting</span> 
        
            <span onclick="getStatusFilter('13')" style="width: 2%;background-color:#89ede8;border-radius: 5px;padding-top: 3px;padding-left: 36px;padding-right: 36px;padding-bottom: 4px;cursor:pointer">
            Reported</span> 
        
            <span onclick="getStatusFilter('14')" style="width: 2%;color:#fff;background-color:#e97373;border-radius: 5px;padding-top: 3px;padding-left: 36px;padding-right: 36px;padding-bottom: 4px;cursor:pointer">
            Failed</span>
        <span onclick="getStatusFilter('paid')" style="width: 2%;color:#fff;background-color:#5bc0de;border-radius: 5px;padding-top: 3px;padding-left: 36px;padding-right: 36px;padding-bottom: 4px;cursor:pointer">
            Paid</span>
    </div>
    <div class="col-md-12 theadscroll" id="load_data" style="position: relative; height: 350px;">
        <table class="table table_round_border theadfix_wrapper" id="receipt_table1" style="font-size: 0.9em;font-family: sans-serif;box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);">
            <thead>
                <tr class="table_header_bg">
                    <th style="text-align:center;width:10%">UHID</th>
                    <th style="text-align:center;width:12%">Patient Name</th>
                    <th style="text-align:center;width:10%">Bill No</th>
                    <th style="text-align:center;width:8%">Bill Date</th>
                    <th style="text-align:center;width:16%">Service Desc</th>
                    <th style="text-align:center;width:15%">Doctor</th>
                    <th style="text-align:center;width:30%">Action</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($receivedData))
                @foreach($receivedData as $rec)
                @if($rec->status == 7)
                        @php $back_ground_color = "#f3deb8"; @endphp
                    @elseif($rec->status == 8)
                        @php $back_ground_color = "#8f98bd"; @endphp
                    @elseif($rec->status == 9)
                        @php $back_ground_color = "#62ebc5"; @endphp
                    @elseif($rec->status == 10)
                        @php $back_ground_color = "#f6c1f9"; @endphp
                    @elseif($rec->status == 11)
                        @php $back_ground_color = "#f9c1c1"; @endphp
                    @elseif($rec->status == 12)
                        @php $back_ground_color = "#a2c787"; @endphp
                    @elseif($rec->status == 13)
                        @php $back_ground_color = "#89ede8"; @endphp
                    @elseif($rec->status == 14)
                        @php $back_ground_color = "#e97373"; @endphp
                    @else
                        @php $back_ground_color = ""; @endphp
                    @endif
                    @if($rec->paid_status == 1)
                        @php $paid_color = "#5bc0de"; @endphp
                    @else
                        @php $paid_color = ""; @endphp
                    @endif
                <tr style="cursor: pointer;background-color:{{$back_ground_color}}">
                    <td class="common_td_rules" style="background-color: {{$paid_color}}">{{ !empty($rec->uhid) ?$rec->uhid :'' }}</td>
                    <td class="common_td_rules">{{ !empty($rec->patient_name) ?$rec->patient_name :'' }}</td>
                    <td class="common_td_rules">{{ $rec->bill_no }}</td>
                    <td class="common_td_rules">{{ !empty($rec->bill_date) ? $rec->bill_date:'' }}</td>
                    <td class="common_td_rules">{{ !empty($rec->item_desc) ? $rec->item_desc:'' }}</td>
                    <td class="common_td_rules">{{ !empty($rec->doctor) ? $rec->doctor :'' }}</td>
                    <td class="">
                        @foreach($pacs_status_master as $pm)
                        @if($pm->display_order <= $rec->status)
                        <button type="button" class="btn btn-sm btn-success btn_disable"
                                id="btn_{{ $pm->display_order }}{{ $rec->id }}"
                                onclick="updatePacsStatus(this,'{{ $rec->id }}','{{ $pm->display_order }}','{{ $rec->status }}','{{ $pm->is_popup }}', '1');"
                                title= '{{ $pm->name }}' data-accession-no="{{$rec->accession_no}}">
                            <i id="pc_master_{{ $pm->display_order }}{{ $rec->id }}"
                               class="{{$pm->i_class}}" aria-hidden="true"></i>
                        </button>
                        @else
                         @if($pm->display_order != $rec->status + 1)
                            @php $disabled = 'disabled'; @endphp
                         @else
                           @php $disabled = ''; @endphp
                         @endif
                        <button type="button" {{$disabled}} class="btn btn-sm btn-default btn_disable"
                                id="btn_{{ $pm->display_order }}{{ $rec->id }}"
                                onclick="updatePacsStatus(this,'{{ $rec->id }}','{{ $pm->display_order }}','{{ $rec->status }}','{{ $pm->is_popup }}', '0');"
                                title= '{{ $pm->name }}' data-accession-no="{{$rec->accession_no}}">
                            <i id="pc_master_{{ $pm->display_order }}{{ $rec->id }}"
                               class="{{$pm->i_class}}" aria-hidden="true"></i>
                        </button>
                        @endif
                        @endforeach
                        @if($image_flg != 0)
                        <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
                                id="btn_101{{ $rec->id }}"
                                onclick="updatePacsStatus(this,'{{ $rec->id }}','101','{{ $rec->status }}','3', '0');"
                                title= 'Show Image' data-accession-no="{{$rec->accession_no}}">
                            <i id="pc_master_101{{ $rec->id }}"
                               class="fa fa-image" aria-hidden="true"></i>
                        </button>
                        @endif
                        @if($report_flg != 0)
                        <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
                                id="btn_102{{ $rec->id }}"
                                onclick="updatePacsStatus(this,'{{ $rec->id }}','101','{{ $rec->status }}','4', '0');"
                                title= 'Show Report' data-accession-no="{{$rec->accession_no}}">
                            <i id="pc_master_102{{ $rec->id }}"
                               class="fa fa-file-o" aria-hidden="true"></i>
                        </button>
                        @endif
                        <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
                                id="btn_status_{{ $rec->id }}"
                                onclick="showPacsStatus(this,'{{ $rec->id }}');"
                                title= 'Show Status'>
                            <i id="statusspin_{{ $rec->id }}"
                               class="fa fa-info" aria-hidden="true"></i>
                        </button>
                        <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
                                id="btn_history_{{ $rec->id }}"
                                onclick="showPacsHistory(this,'{{ $rec->id }}');"
                                title= 'Show History'>
                            <i id="historyspin_{{ $rec->id }}"
                               class="fa fa-history" aria-hidden="true"></i>
                        </button>
                        @if($billing_flg != 0 )
                        <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
                                id="btn_service_{{ $rec->id }}"
                                onclick="serviceBill(this,'{{ $rec->patient_id }}');"
                                title= 'Service Bill'>
                            <i id=""
                               class="fa fa-money" aria-hidden="true" style="color:#1b00ff"></i>
                        </button>
                        @endif
                        @if($revert_flg != 0)
                        <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
                                id="btn_revert_{{ $rec->id }}"
                                onclick="RevertAll(this,'{{ $rec->id }}');"
                                title= 'Revert All'>
                            <i id="revertspin"
                               class="fa fa-trash-o" aria-hidden="true" style="color:red"></i>
                        </button>
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">No Records Found</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<script>

    $('#load_data').on('scroll', function () {
    var scrollHeight = $('#load_data').height();
    var scrollPosition = $('#load_data').scrollTop() + $('#load_data').innerHeight();
    if (scrollPosition + 3 >= $('#load_data')[0].scrollHeight) {
    cur_limit += limit;
    offset = offset + limit;
    console.log(limit + '##' + offset + '##' + total_rec);
    console.log(total_rec + '**' + offset);
    if (offset < total_rec) {
    setTimeout(function () {
    searchList(limit, offset);
    }, 500);
    }
    }
    })
</script>

@else
@if(isset($receivedData))
@foreach($receivedData as $rec)
                @if($rec->status == 7)
                        @php $back_ground_color = "#f3deb8"; @endphp
                    @elseif($rec->status == 8)
                        @php $back_ground_color = "#c7cbdb"; @endphp
                    @elseif($rec->status == 9)
                        @php $back_ground_color = "#62ebc5"; @endphp
                    @elseif($rec->status == 10)
                        @php $back_ground_color = "#f6c1f9"; @endphp
                    @elseif($rec->status == 11)
                        @php $back_ground_color = "#f9c1c1"; @endphp
                    @else
                        @php $back_ground_color = ""; @endphp
                @endif
                @if($rec->paid_status == 1)
                        @php $paid_color = "#5bc0de"; @endphp
                    @else
                        @php $paid_color = ""; @endphp
                    @endif
<tr style="cursor: pointer;background-color:{{$back_ground_color}}">
    <td class="common_td_rules" style="background-color: {{$paid_color}}">{{ !empty($rec->uhid) ?$rec->uhid :'' }}</td>
    <td class="common_td_rules">{{ !empty($rec->patient_name) ?$rec->patient_name :'' }}</td>
    <td class="common_td_rules">{{ $rec->bill_no }}</td>
    <td class="common_td_rules">{{ !empty($rec->bill_date) ? $rec->bill_date:'' }}</td>
    <td class="common_td_rules">{{ !empty($rec->item_desc) ? $rec->item_desc:'' }}</td>
    <td class="common_td_rules">{{ !empty($rec->doctor) ? $rec->doctor :'' }}</td>
    <td class="">
        @foreach($pacs_status_master as $pm)
        @if($pm->display_order <= $rec->status)
        <button type="button" class="btn btn-sm btn-success btn_disable"
                id="btn_{{ $pm->display_order }}{{ $rec->id }}"
                onclick="updatePacsStatus(this,'{{ $rec->id }}','{{ $pm->display_order }}','{{ $rec->status }}','{{ $pm->is_popup }}', '1');"
                title= '{{ $pm->name }}' data-accession-no="{{$rec->accession_no}}">
            <i id="pc_master_{{ $pm->display_order }}{{ $rec->id }}"
               class="{{$pm->i_class}}" aria-hidden="true" ></i>
        </button>
        @else
            @if($pm->display_order != $rec->status + 1)
                @php $disabled = 'disabled'; @endphp
            @else
                @php $disabled = ''; @endphp
            @endif
        <button type="button" {{$disabled}} class="btn btn-sm btn-default btn_disable"
                id="btn_{{ $pm->display_order }}{{ $rec->id }}"
                onclick="updatePacsStatus(this,'{{ $rec->id }}','{{ $pm->display_order }}','{{ $rec->status }}','{{ $pm->is_popup }}', '0');"
                title= '{{ $pm->name }}' data-accession-no="{{$rec->accession_no}}">
            <i id="pc_master_{{ $pm->display_order }}{{ $rec->id }}"
               class="{{$pm->i_class}}" aria-hidden="true"></i>
        </button>
        @endif
        @endforeach
        @if($image_flg != 0)
        <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
            id="btn_101{{ $rec->id }}"
            onclick="updatePacsStatus(this,'{{ $rec->id }}','101','{{ $rec->status }}','3', '0');"
            title= 'Show Image' data-accession-no="{{$rec->accession_no}}">
        <i id="pc_master_101{{ $rec->id }}"
           class="fa fa-image" aria-hidden="true"></i>
        </button>
        @endif
        @if($report_flg != 0)
        <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
                id="btn_102{{ $rec->id }}"
                onclick="updatePacsStatus(this,'{{ $rec->id }}','101','{{ $rec->status }}','4', '0');"
                title= 'Show Report' data-accession-no="{{$rec->accession_no}}">
            <i id="pc_master_102{{ $rec->id }}"
               class="fa fa-file-o" aria-hidden="true"></i>
        </button>
        @endif
        <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
                id="btn_status_{{ $rec->id }}"
                onclick="showPacsStatus(this,'{{ $rec->id }}');"
                title= 'Show Status'>
            <i id="statusspin_{{ $rec->id }}"
               class="fa fa-info" aria-hidden="true"></i>
        </button>
        <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
                id="btn_history_{{ $rec->id }}"
                onclick="showPacsHistory(this,'{{ $rec->id }}');"
                title= 'Show History'>
            <i id="historyspin_{{ $rec->id }}"
               class="fa fa-history" aria-hidden="true"></i>
        </button>
        @if($billing_flg != 0 )
       <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
                id="btn_service_{{ $rec->id }}"
                onclick="serviceBill(this,'{{ $rec->patient_id }}');"
                title= 'Service Bill'>
            <i class="fa fa-money" aria-hidden="true" style="color:#1b00ff"></i>
        </button>
        @endif
        @if($revert_flg != 0)
        <button type="button" class="btn btn-sm btn-default btn_disable" style=" background-color: #e9e9e9"
                id="btn_revert_{{ $rec->id }}"
                onclick="RevertAll(this,'{{ $rec->id }}');"
                title= 'Revert All'>
            <i id="revertspin_{{ $rec->id }}"
               class="fa fa-trash-o" aria-hidden="true" style="color:red"></i>
        </button>
        @endif
    </td>
</tr>
@endforeach
@else
<tr>
    <td colspan="5">No Records Found</td>
</tr>
@endif
@endif
