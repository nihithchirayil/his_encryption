@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <style>
    </style>
@endsection
@section('content-area')

    <div class="right_col">
        <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;padding-right:72px">
           <div> {{ $title }} </div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-8 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <form action="{{ route('extensionsvalley.modality.listModalityUserAccess') }}" id="SearchForm"
                                method="POST">
                                {!! Form::token() !!}
                                <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Modality</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select(
                                            'modality_name',
                                            ['-1' => ' Select Modality'] + $modality_master->toArray(),
                                            $searchFields['modality_name'] ?? '',
                                            [
                                                'class' => 'form-control select2', 'id' => 'search_modality_id'
                                            ],
                                        ) !!}
                                    </div>
                                </div>

                                <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">User</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('user_id', ['-1' => ' Select User'] + $user->toArray(), $searchFields['user_id'] ?? '', [
                                            'class' => 'form-control select2',
                                            'id' => 'search_user_id'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <span class="btn btn-block light_purple_bg" onclick="searchList()"><i class="fa fa-search"></i>
                                        Search</span>
                                </div>
                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                            class="fa fa-times"></i> Clear</a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix" id="appendFrm">

                    </div>
                </div>
            </div>
            <div class="col-md-4 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix" id="paccs_access_frm">
                        <form action="{{ route('extensionsvalley.modality.saveModalityUserMap') }}" method="POST"
                            id="menuaccessForm">
                            {!! Form::token() !!}
                            <input type="hidden" name="menu_access_id" id="menu_access_id" value="0">
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Modality<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('save_modality_id', ['-1' => 'Select Modality'] + $modality_master->toArray(), null, [
                                        'class' => 'form-control pacs_id select2',
                                        'id' => 'save_modality_id',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">User Name<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('save_user_id', ['-1' => 'Select User'] + $user->toArray(), '', [
                                        'class' => 'form-control select2',
                                        'id' => 'save_user_id',
                                    ]) !!}
                                </div>
                            </div>

                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Status<span class="error_red">*</span></label>
                                    <div class="clearfix"></div>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1">{{ __('Active') }}</option>
                                        <option value="0">{{ __('Inactive') }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-6 padding_sm">
                                <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                        class="fa fa-times"></i> Cancel</a>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <span class="btn btn-block light_purple_bg" onclick="saveModalityAccess()"><i
                                        class="fa fa-save"></i> Save</span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
    <script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('.fixed_header').floatThead({
                position: 'absolute',
                scrollContainer: true
            });
            searchList();
        });
        function searchList(){
            var user_id = $("#search_user_id").val();
            var modality_id = $("#search_modality_id").val();
            var param = {user:user_id,modality_name:modality_id};
            $.ajax({
                type: "POST",
                url: $('#SearchForm').attr('action'),
                data: param,
                beforeSend: function() {
                    $('#appendFrm').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                },
                success: function(msg) {
                    $("#appendFrm").html(msg);
                },
                complete: function() {
                    $('#appendFrm').LoadingOverlay("hide");
                }
            });
        }
        function editLoadData(obj, id, modality_id, user_id, status) {
            $('#menu_access_id').val(id);
            $('#save_modality_id').val(modality_id).select2().trigger('change');
            $('#save_user_id').val(user_id).select2().trigger('change');
            $('#status').val(status);
            $('#menuaccessForm').attr('action', '{{ $updateUrl }}');

        }

        function resetFunction() {
            $('#menuaccessForm')[0].reset();
        }

        function saveModalityAccess() {
            var menu_access_id = $('#menu_access_id').val();
            var modality_id = $('#save_modality_id').val();
            var user_id = $('#save_user_id').val();
            var status = $('#status').val();
            if(modality_id == '-1' || user_id == '-1'){
                toastr.error("Please fill Required fields"); return false;
            }
            if ($("#approve").prop("checked")) {
                var approve = 1;
            } else {
                var approve = 0;
            }
            var param = {
                menu_access_id: menu_access_id,
                modality_id: modality_id,
                user_id: user_id,
                status: status,
            };
            $.ajax({
                type: "POST",
                url: $('#menuaccessForm').attr('action'),
                data: param,
                beforeSend: function() {
                    $('#paccs_access_frm').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                },
                success: function(msg) {
                    if (msg) {
                        var obj = JSON.parse(msg);
                        var msg = obj.message;
                        var status = obj.status;
                        if (status == 1) {
                            toastr.success(msg);
                            setTimeout(function() {
                               window.location.reload();
                            }, 500);
                        } else {
                            toastr.error(msg);
                        }
                    }
                },
                complete: function() {
                    $('#paccs_access_frm').LoadingOverlay("hide");
                }
            });
        }
    </script>

@endsection
