<script type="text/javascript">
$(document).ready(function() {
    $(".page-link").click(function() {
        var url = $(this).attr("href");
        var patient_id = $("#search_patient_name_id").val();
        var param = {
            patient_id: patient_id
        };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function() {
                $('#searchdatabtn').attr('disabled', true);
                $('#searchdataspin').removeClass('fa fa-search');
                $('#searchdataspin').addClass('fa fa-spinner');
                $('#fetch_dialysis_data').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function(msg) {
                $('#fetch_dialysis_data').html(msg);
            },
            complete: function() {
                $('#searchdatabtn').attr('disabled', false);
                $('#searchdataspin').removeClass('fa fa-spinner');
                $('#searchdataspin').addClass('fa fa-search');
                $('#fetch_dialysis_data').LoadingOverlay("hide");
            },
            error: function() {
                toastr.error("Please Check Internet Connection");
            }
        });
        return false;
    });

});
</script>
<div class="theadscroll" style="position: relative; max-height: 400px;">
    <table class="table table-bordered no-margin table_sm no-border">
        <thead>
            <tr class="table_header_bg">
                <th>UHID</th>
                <th>Patient Name</th>
                <th>Age</th>
                <th>Dialysis Type</th>
                {{-- <th>Book Dialysis</th> --}}
                <th>Dialysis Log</th>
                <th>Print Log</th>
            </tr>
        </thead>
        <tbody class="dialysis_list_table_body">
            @isset($result)
            @if(count($result) > 0)
            @foreach($result as $item)
            @if($item->log_patient)
            <tr id="tr_{{$item->id}}" style="background-color: #ababab;color: white;">
            @else
            <tr id="tr_{{$item->id}}">
            @endif
                <td>{{$item->uhid}}</td>
                <td>{{$item->patient_name}}</td>
                {{-- <td>{{date('M-d-Y', strtotime($item->dob))}}</td> --}}
                <td>{{$item->age}}</td>
                <td>{{$item->dialysis_type}}</td>
                {{-- <td style="text-align: center;">
                    <a target="_blank" href ="{{ route('extensionsvalley.dialysis.dialysisMachineBooking')}}?patient_id={{$item->pid}}&patient_name={{$item->patient_name}}&patient_enrol_id={{$item->id}}" >
                        <button type="button" class="btn btn-sm bg-blue">
                        <i  class="fa fa-edit"></i>
                    </button></a>
                </td> --}}
                <td style="text-align: center;">
                    <a target="_blank" href ="{{ route('extensionsvalley.dialysis.dialysisForm')}}?patient_id={{$item->pid}}&patient_name={{$item->patient_name}}&patient_enrol_id={{$item->id}}&patient_uhid={{$item->uhid}}" >
                        <button type="button" class="btn btn-sm bg-green">
                        <i  class="fa fa-sign-in"></i>
                    </button></a>
                </td>
                <td style="text-align: center;">
                    <button type="button" id="print_dialysis_btn_{{$item->id}}" class="btn btn-sm bg-orange" onclick="printDialysis('{{$item->id}}','{{$item->pid}}');">
                        <i id="print_dialysis_btn_spin_{{$item->id}}" class="fa fa-print"></i>
                    </button>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="6">No records found..!</td>
            </tr>
            @endif
            @endisset
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<div class="col-md-12 text-center">
    <ul class="pagination purple_pagination pull-right">
        {!! $page_links !!}
    </ul>
</div>
</div>
