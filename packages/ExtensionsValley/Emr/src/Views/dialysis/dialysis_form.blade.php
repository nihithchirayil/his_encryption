@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <style>
        .list_hover:hover {
            background-color: #ccebbc !important;
        }

        .liHovernew:hover {
            background-color: #4c6456 !important;
            color: #ffff;
        }

        .custom_label_class {
            position: absolute !important;
            top: 2px !important;
            left: 6px !important;
            font-size: 12px !important;
            font-weight: 700 !important;
        }

        .custom_chkbox_cls {
            position: relative;
            padding: 15px 4px 4px 4px !important;
            margin-bottom: 10px;
            margin-left: 20px !important;
        }

        .chk_input {
            position: absolute;
            margin-top: 0px !important;
            margin-left: -35px !important;
        }
    </style>
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <div class="right_col">
        <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;padding-right: 80px">
            {{ $title }}</div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Patient Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="search_patient_name"
                                id="search_patient_name" onkeyup="select_booked_patient(this, event,'booked')"
                                value="{{ @$patient_name ? $patient_name: '' }}" autocomplete="off">
                            <input type="hidden" class="form-control" name="search_patient_name_id"
                                id="search_patient_name_id" value="{{ @$patient_id ? $patient_id :'' }}">
                                <input type="hidden" class="form-control" name="enrol_id"
                                id="enrol_id" value="{{ @$patient_enrol_id ? $patient_enrol_id :'' }}">
                            <div class="ajaxSearchBox" id="search_patient_name_ajax" style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                         margin: 6px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                         border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label for="">UHID</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" value="{{ @$patient_uhid ? $patient_uhid :'' }}" name="uhid" id="uhid" readonly style="background-color: #fff">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Date </label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepickers" value="{{ date('M-d-Y') }}"
                                    name="date" id="date">
                            </div>
                        </div>
                        @if(isset($genertedSerial_no) && $genertedSerial_no != '')
                            @php $display_style = ""; @endphp
                        @else
                            @php $display_style = "display:none"; @endphp
                        @endif
                            <div class="col-md-2 padding_sm" id="dsn" style="{{ $display_style }}">
                                <div class="mate-input-box">
                                    <label for="">Dialysis Serial No </label>
                                    <div class="clearfix"></div>
                                    <span id="serail_no_label" style="color:red">{{ @$genertedSerial_no ? $genertedSerial_no :'' }}</span>
                                    <input type="hidden" id="dialysis_serial_no" value="{{ @$genertedSerial_no ? $genertedSerial_no :'' }}" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="box-body clearfix" >
                        <div class="col-md-4 padding_sm" style="border:2px solid #529554 !important;border-radius:20px;width:32% !important">
                            <div class="col-md-6 padding_sm " style="padding-top: 16px !important;">
                                <div class="mate-input-box">
                                <label for="">Dry weight (Kg)</label>
                                    <input type="textbox" class="form-control " name="dry_weight"
                                        id="dry_weight" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm " style="padding-top: 16px !important;">
                                <div class="mate-input-box">
                                <label for="">Machine</label>
                                    <input type="textbox" class="form-control " name="machine"
                                        id="machine">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm " >
                                <div class="mate-input-box">
                                <label for="">A.v Fistula</label>
                                    <input type="textbox" class="form-control " name="av_fistula"
                                        id="av_fistula" >
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm " >
                                <div class="mate-input-box">
                                <label for="">Subclavian/femoral/Jugular</label>
                                    <input type="textbox" class="form-control " name="femoral"
                                        id="femoral" >
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-6 padding_sm " >
                                <div class="mate-input-box">
                                <label for="">Blood Tubing</label>
                                    <input type="textbox" class="form-control " name="blood_tubing"
                                        id="blood_tubing" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm " >
                                <div class="mate-input-box">
                                <label for="">Dializer</label>
                                    <input type="textbox" class="form-control " name="dializer"
                                        id="dializer" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-6 padding_sm " >
                                <div class="mate-input-box">
                                <label for="">No.of use</label>
                                    <input type="textbox" class="form-control " name="no_of_use"
                                        id="no_of_use" onkeyup="number_validation(this)">
                                </div>
                            </div>

                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Condition of start</label>
                                    <div class="clearfix"></div>
                                     <div class="ht10"></div>
                                <textarea id="condition_of_start" class="tiny_text form-control">
                                  </textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Pre B.P</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="pre_bp" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 padding_sm" style="background-color: #f5e7d4;">
                                <div class="mate-input-box">
                                    <label for="">Pre Weight (Kg)</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="pre_weight" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm"  style="background-color: #f5e7d4;">
                                <div class="mate-input-box">
                                    <label for="">Post weight (Kg)</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="post_weight" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Pulse</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="pulse" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Resp</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="resp" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Temperature</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="temperature" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Inter Dialysis weight gain (Kg)</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="inter_dialysis_weight_gain" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Weight gain from dry weight (Kg)</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="weight_gain_from_dry_weight" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm bg-blue" style="color:#fff">
                                <label style="font-weight:700">HEPARIN</label>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Bolus</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="bolus">
                                </div>
                            </div>
                            <div class="col-md-6 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Infusion</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="infusion">
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Duration</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="duration">
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Blood flow</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="blood_flow">
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Dialysis flow</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="dialysis_flow">
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label class="bg-blue" style="width: 100%">Post Dialysis Medication</label>
                                    <div class="clearfix"></div>
                                     <div class="ht10"></div>
                                <textarea id="post_dialysis_medication" class="tiny_text form-control">
                                  </textarea>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label class="bg-blue" style="width: 100%">Doctors Orders</label>
                                    <div class="clearfix"></div>
                                    <div class="ht10"></div>
                                    <textarea id="doctors_order" class="tiny_text form-control">
                                      </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 padding_sm" style="border:2px solid #a3a3de !important;border-radius:20px;margin-left:5px">
                            <div class="col-md-12 padding_sm custom_chkbox_cls">
                                <div class="">
                                    <label style="font-size: 12px;font-weight: 700;">Blood Transfusion</label>
                                    <input type="radio" class="radio-success inline no-margin" name="blood_transfusion"
                                        id="blood_transfusion_y" value="y">Yes
                                    <input type="radio" class="radio-success inline no-margin" name="blood_transfusion"
                                        id="blood_transfusion_n" value="n">No
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="theadscroll" style="position: relative;max-height: 287px; min-height: 287px;height: 287px;">
                                    <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
                                    style="border: 1px solid #CCC;">
                                        <thead>
                                            <tr class="bg-blue">
                                                <th style="width: 8%">Time</th>
                                                <th style="width: 6%">BP sys.</th>
                                                <th style="width: 6%">BP dia.</th>
                                                <th style="width: 6%">Pulse</th>
                                                <th style="width: 6%">Temp</th>
                                                <th style="width: 6%">Hep</th>
                                                <th style="width: 6%">B/F</th>
                                                <th style="width: 6%">V.P</th>
                                                <th style="width: 6%">N.p/TMP</th>
                                                <th style="width: 20%">Complication</th>
                                                <th style="width: 20%">Medication</th>
                                                <th style="width: 3%" onclick="appendTableRaw(this)">
                                                  <button class="btn btn-success"><i class="fa fa-plus"></i>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="dialysis_tblform_body">
                                            <tr>
                                                <td><input type="text" name="time[]" value="{{ date('h:i a') }}" class="form-control time_picker" onkeyup="number_validation(this)"></td>
                                                <td><input type="text" name="bp_sys[]" class="form-control" onkeyup="number_validation(this)" onblur="appendTableRaw(this)"></td>
                                                <td><input type="text" name="bp_dia[]" class="form-control" onkeyup="number_validation(this)"></td>
                                                <td><input type="text" name="pulse_tbl[]" class="form-control" onkeyup="number_validation(this)"></td>
                                                <td><input type="text" name="temp[]" class="form-control" onkeyup="number_validation(this)"></td>
                                                <td><input type="text" name="hep[]" class="form-control" onkeyup="number_validation(this)"></td>
                                                <td><input type="text" name="bf[]" class="form-control" onkeyup="number_validation(this)"></td>
                                                <td><input type="text" name="vp[]" class="form-control" onkeyup="number_validation(this)"></td>
                                                <td><input type="text" name="np[]" class="form-control" onkeyup="number_validation(this)"></td>
                                                <td><input type="text" name="complication[]" class="form-control"></td>
                                                <td><input type="text" name="medication[]" class="form-control"></td>
                                                <td onclick="removeCurrentRaw(this)"><i class = "fa fa-trash" style="color:red;cursor:pointer"></i></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-12">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Closing Time</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control closedatepickers" value="{{ date('M-d-Y h:i:s') }}"
                                            name="" id="closing_time">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm bg-blue" style="color:#fff">
                                <label style="font-weight: 700">CONDITION AT CLOSE</label>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht10"></div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Post B.P</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="close_post_bp" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Post Weight (Kg)</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="close_post_weight" onkeyup="number_validation(this);calculateWeightLoss(this)">
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Weight Loss (Kg)</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="close_weight_loss" onkeyup="number_validation(this)">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Lab Investigation</label>
                                    <div class="clearfix"></div>
                                    <div class="ht10"></div>
                                    <textarea  class="tiny_text form-control" name=""
                                        id="lab_investigation" ></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 padding_sm">
                                <div class="mate-input-box">
                                <label for="" >Medication</label>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <textarea id="close_medication" class="tiny_text form-control">
                                  </textarea>
                                {{-- <div class="mate-input-box">

                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="close_medication">
                                </div> --}}
                            </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Started By</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="started_by" onkeyup="select_user(this, event,'started_by')">

                                <input type="hidden" class="form-control" name="search_started_by"
                                id="search_started_by" value="">
                            <div class="ajaxSearchBox" id="search_started_by_ajax" style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                         margin: 6px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                         border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                         </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Assisted By</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name="" onkeyup="select_user(this, event,'assisted_by')"
                                        id="assisted_by">

                                <input type="hidden" class="form-control" name="search_assisted_by"
                                id="search_assisted_by" value="">
                            <div class="ajaxSearchBox" id="search_assisted_by_ajax" style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                         margin: 6px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                         border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                                         </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Dializer FBV</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control" name=""
                                        id="dializer_fbv">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm pull-right" style="width: 10%;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block btn-success" id="searchdatabtn"onclick="saveDialysisFormData()"><i
                                    class="fa fa-save" id="searchdataspin"></i>
                                Save</span>
                        </div>
                        <div class="col-md-1 padding_sm pull-right" style="width: 10%;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Clear</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @stop
    @section('javascript_extra')
        <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
        <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('packages/extensionsvalley/dialysis/js/dialysis_form.js') }}"></script>
        <script type="text/javascript"></script>
    @endsection
