@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <style>
        .list_hover:hover {
            background-color: #ccebbc !important;
        }

        .liHovernew:hover {
            background-color: #4c6456 !important;
            color: #ffff;
        }

    </style>
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <div class="right_col">
        <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;padding-right: 80px">
            {{ $title }}</div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-md-4 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Patient Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="search_patient_name"
                                id="search_patient_name" onkeyup="select_booked_patient(this, event,'booked')"
                                value="" autocomplete="off">
                            <input type="hidden" class="form-control" name="search_patient_name_id"
                                id="search_patient_name_id" value="">
                                <input type="hidden" class="form-control" name="enrol_id"
                                id="enrol_id" value="">
                            <div class="ajaxSearchBox" id="search_patient_name_ajax" style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                         margin: 6px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                         border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="box-body clearfix" >
                        <div class="col-md-12 padding_sm" id="dialysis_log_div">

                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
    @section('javascript_extra')
        <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
        <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('packages/extensionsvalley/dialysis/js/dialysis_log.js') }}"></script>
        <script type="text/javascript"></script>
    @endsection
