 
  <div style="width:100%">
       <table style="border: 1px solid black;;border-colapse:colapse;margin-top:15px;" width="100%" border="0"
           cellspacing="0" cellpadding="0">

           @php
               // $jsonArray = json_decode($logData->json_data);
               // $jsonArray = collect($logData);
           @endphp
           <tr>
               <td width="13%"><strong style="margin-left: 5px;">Dialysis Serial No</strong></td>
               <td width="3%" align="center">:</td>
               <td width="10%">{{ $logData[0]->dialysis_serial_no }}</td>
               <td width="11%"><strong>Patient Name</strong></td>
               <td width="3%" align="center">:</td>
               <td width="13%">{{ $logData[0]->patient_name }}</td>
               <td width="10%"><strong>Gender</strong></td>
               <td width="3%" align="center">:</td>
               <td width="8%">{{ $logData[0]->gender }}</td>
               <td width="7%"><strong>Age</strong></td>
               <td width="3%" align="center">:</td>
               <td width="8%">{{ $logData[0]->age }}</td>
               
           </tr>
           <tr>
            <td colspan="9">&nbsp;</td>
           </tr>
           <tr>   
               <td width="8%"><strong style="margin-left: 5px;">Phone</strong></td>
               <td width="3%" align="center">:</td>
               <td width="10%" class="break">{{ $logData[0]->phone }}</td>
               <td width="10%"><strong>DOB</strong></td>
               <td width="3%" align="center">:</td>
               <td width="10%">{{ date('M-d-Y',strtotime($logData[0]->dob)) }}</td>
               <td width="8%"><strong>UHID</strong></td>
               <td width="3%" align="center">:</td>
               <td width="13%">{{ $logData[0]->uhid }}</td>
               <td width="9%"><strong>Date</strong></td>
               <td width="3%" align="center">:</td>
               <td width="10%">{{ $logData[0]->date }}</td>
               
           </tr>
       </table>
       <div class="col-md-12">
           <div class="col-md-12" style="margin:20px; text-align:center;">
               <h3>DIALYSIS LOG</h3>
           </div>
           <table style="border: 1px solid black;border-colapse:colapse;margin-top:1px;" width="100%" border="0"
           cellspacing="0" cellpadding="0">
                  
       </div>

       <tbody class="dialysis_tbllog_body">
           @if (isset($logData))

               @if (sizeof($logData) > 0)
                   @php $i=1 @endphp
                   @foreach ($logData as $item)
                       @php
                           $jsonArray = json_decode($item->json_data);
                           $started_at = @$jsonArray->started_at ? $jsonArray->started_at :'';
                           if($started_at == ''){
                            $started_at ='NILL';
                           }
                           $doctorsorder = $jsonArray->doctors_order;
                           if($doctorsorder == ''){
                            $doctorsorder = 'NILL';
                           }
                           $blood_transfusion = $jsonArray->blood_transfusion;
                           if($blood_transfusion == 'y'){
                            $blood_transfusion = 'YES';
                           }elseif($blood_transfusion == 'n'){
                            $blood_transfusion = 'NO';
                           }else{
                            $blood_transfusion ='NILL';
                           }
                       @endphp
                       <tr>  
                        <td width="10%"><strong style="margin-left: 5px;">DRY WT</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->dry_weight }}</td>
                        <td width="8%"><strong>Machine</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->machine }}</td>
                        <td width="8%"><strong>Av Fistula</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="10%" class="break">{{ $jsonArray->av_fistula }}</td>
                        <td width="8%"><strong>Femoral</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="10%" class="break">{{ $jsonArray->femoral }}</td>
                     
                    </tr>
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>
                   
                    <tr>    
                        <td width="10%"><strong style="margin-left: 5px;">Condition Of Start</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->condition_of_start }}</td>
                        <td width="10%"><strong>Pulse</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->pulse }}</td>
                        <td width="8%"><strong>Resp</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->resp }}</td>
                        <td width="8%"><strong>Temperature</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="10%" class="break">{{ $jsonArray->temperature }}</td>
                    </tr>
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>
                    <tr>    
                        
                        <td width="20%"><strong style="margin-left: 5px;">InterDialysisWeightGain</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->inter_dialysis_weight_gain }}</td>
                        <td width="20%"><strong>WeightGainFromDryWeight</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->weight_gain_from_dry_weight }}</td>
                        <td width="20%"><strong style="margin-left: 5px;">PostDialysisMedication</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->post_dialysis_medication }}</td>
                    </tr>
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>
                   
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>
                    <tr>  
                        <td width="8%"><strong style="border-bottom: 2px solid;margin-left: 5px;">No Of Use&nbsp;({{ $jsonArray->no_of_use }})</strong></td>
                    </tr> 
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>   
                    <tr>  
                        <td width="10%"><strong style="margin-left: 5px;">Blood Tubing</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->blood_tubing }}</td>
                        <td width="8%"><strong>Dializer</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->dializer }}</td>
                    </tr> 
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>  
                    
                    <tr>  
                        <td width="10%"><strong style="border-bottom: 2px solid;margin-left: 5px;">PRE HD</strong></td>
                    </tr> 
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>  
                    <tr>    
                        
                        <td width="10%"><strong style="margin-left: 5px;">BP</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->pre_bp }}</td>
                        <td width="8%"><strong>WT</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->pre_weight }}</td>
                        <td width="8%"><strong>WT GAIN</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="10%" class="break">{{ $jsonArray->weight_gain_from_dry_weight }}</td>
                    </tr>
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>
                    <tr>  
                        <td width="10%"><strong style="border-bottom: 2px solid;margin-left: 5px;">POST HD</strong></td>
                    </tr>  
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr> 
                    <tr>    
                        
                        <td width="10%"><strong style="margin-left: 5px;">BP</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->close_post_bp }}</td>
                        <td width="8%"><strong>WT</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->close_post_weight }}</td>
                        <td width="8%"><strong>WT GAIN</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="10%" class="break">{{ $jsonArray->close_weight_loss }}</td>
                    </tr>
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>
                    
                    <tr>  
                        <td width="10%"><strong style="border-bottom: 2px solid;margin-left: 5px;">HEPARIN</strong></td>
                    </tr> 
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>
                    <tr>    
                        
                        <td width="10%"><strong style="margin-left: 5px;">Bolus</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->bolus }}</td>
                        <td width="8%"><strong>Infusion</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->infusion }}</td>
                        <td width="8%"><strong>Duration</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->duration }}</td>
                    </tr>
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>
                    <tr>    
                        
                        <td width="10%"><strong style="margin-left: 5px;">Blood Flow</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->blood_flow }}</td>
                        <td width="8%"><strong>Dialystate Flow</strong></td>
                        <td width="3%" align="center">:</td>
                        <td width="8%">{{ $jsonArray->dialysis_flow }}</td>
                    </tr>
                   

                       @php $i++ @endphp
                   @endforeach
               @else
                   <tr>
                       <td colspan="9">No data !!</td>
                   </tr>
               @endif
           @endif
       </tbody>
       </table>

       <div class="col-md-12" style="margin:20px;">
           <h6>DOCTORS ORDERS &nbsp;&nbsp; : &nbsp; &nbsp; {{ $doctorsorder }}</h6>
       </div>
       <table width="100%" style="">
        <tbody>
          <tr>
             <td width="20%">Blood Transfusion</td>
             <td>:</td>
             <td>{{ $blood_transfusion }}</td>
             <td width="50%"></td>
             <td width="10%">Started At</td>
             <td width="3%">:</td>
             <td width="17%">{{ $started_at }}</td>
          </tr>
        </tbody>
      </table> 
      <h4> PROCESSING </h4>   
       <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed styled-table"
       style="border: 1px solid #CCC;width:100%;">
       {{-- <table class="table table-bordered no-margin table_sm no-borderstyled-table"
           style="border: 1px solid #CCC;width:100%"> --}}
           <thead>
               <tr>
                   <th style="border-bottom: #ffffff !important;width:10%">Time</th>
                   <th style="text-align: center;">BP sys.</th>
                   <th style="text-align: center;">BP dia.</th>
                   <th style="text-align: center ">Pulse</th>
                   <th style="text-align: center">Temp.</th>
                   <th style="text-align: center">Hep.</th>
                   <th style="text-align: center">B/F</th>
                   <th style="text-align: center">V.P</th>
                   <th style="text-align: center">N.P/TMP</th>
                   <th style="text-align: center">Complication</th>
                   <th style="text-align: center">Medication</th>
               </tr>
           </thead>
   </div>

   <tbody class="dialysis_tbllog_body">
       @if (isset($logData))

           @if (sizeof($logData) > 0)
               @php $i=1 @endphp
               @foreach ($logData as $item)
                   @php
                       $jsonArray = json_decode($item->json_data, true);
                       $closing_time = $jsonArray['closing_time'];
                       $close_post_bp = $jsonArray['close_post_bp'];
                       $close_post_weight = $jsonArray['close_post_weight'];
                       $close_weight_loss = $jsonArray['close_weight_loss'];
                       $lab_investigation = $jsonArray['lab_investigation'];
                       $close_medication = $jsonArray['close_medication'];
                       $started_by = $jsonArray['started_by'];
                       $assisted_by = $jsonArray['assisted_by'];
                       $dializer_fbv = $jsonArray['dializer_fbv'];
                       $tabl_data = $jsonArray['tabl_data'];
                   @endphp
                   @foreach ($tabl_data as $data)
                       @foreach ($data as $key => $value)
                           {{-- @php dd($value['time']); @endphp --}}

                           <tr>
                               <td class = "td_common_numeric_rules">{{ $value['time'] }}</td>
                               <td class = "td_common_numeric_rules">{{ $value['bp_sys'] }}</td>
                               <td class = "td_common_numeric_rules">{{ $value['bp_dia'] }}</td>
                               <td class = "td_common_numeric_rules">{{ $value['pulse_tbl'] }}</td>
                               <td class = "td_common_numeric_rules">{{ $value['temp'] }}</td>
                               <td class = "td_common_numeric_rules">{{ $value['hep'] }}</td>
                               <td class = "td_common_numeric_rules">{{ $value['bf'] }}</td>
                               <td class = "td_common_numeric_rules">{{ $value['vp'] }}</td>
                               <td class = "td_common_numeric_rules">{{ $value['np'] }}</td>
                               <td class = "td_common_numeric_rules">{{ $value['complication'] }}</td>
                               <td class = "td_common_numeric_rules">{{ $value['medication'] }}</td>
                           </tr>

                           @php $i++ @endphp
                       @endforeach
                   @endforeach
               @endforeach
           @else
               <tr>
                   <td colspan="9">No data !!</td>
               </tr>
           @endif
       @endif
   </tbody>
   </table>
   <div class="col-md-12" style="margin:20px;">
       <h6>CLOSING TIME &nbsp;&nbsp; : &nbsp; &nbsp; {{ $closing_time }}</h6>
       <h4>CONDITION AT CLOSE </h4>
       <table width="100%" border="0" style="border: 1px solid black;">
           <tbody>
            <tr>
                <td width="15%">Post B.P</td>
                <td width="3%">:</td>
                <td width="15%">{{ $close_post_bp }}</td>
                <td width="15%">Post Weight</td>
                <td width="3%">:</td>
                <td width="15%">{{ $close_post_weight }}</td>
                <td width="15%">Weight Loss</td>
                <td width="3%">:</td>
                <td width="16%">{{ $close_weight_loss }}</td>
            </tr>
            <tr>
                <td colspan="9">&nbsp;</td>
            </tr>
            <tr>
                <td>Lab Investigation</td>
                <td>:</td>
                <td>{{ $lab_investigation }}</td>
                <td>Close Medication</td>
                <td>:</td>
                <td>{{ $close_medication }}</td>
            </tr>
           </tbody>
       </table>
       <table width="100%" border="0" style="">
        <tbody>
           
         <tr>
             <td width="15%">Started By</td>
             <td width="3%">:</td>
             <td width="15%">{{ $started_by }}</td>
             <td width="67%"></td>
         </tr>
         <tr>
             <td colspan="9">&nbsp;</td>
         </tr>
         <tr>
             <td>Assisted By</td>
             <td>:</td>
             <td>{{ $assisted_by }}</td>
             <td></td>
         </tr>
         <tr>
            <td colspan="9">&nbsp;</td>
        </tr>
        </tbody>
        <table width="100%" style="">
            <tbody>
         <tr>
            <td width="80%"></td>
            <td width="20%">Dializer FBV</td>
            <td>:</td>
            <td>{{ $dializer_fbv }}</td>
        </tr>
        </tbody>
    </table>
   </div>
   </div>
