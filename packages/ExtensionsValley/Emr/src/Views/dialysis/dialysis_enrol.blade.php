@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <style>
        .list_hover:hover {
            background-color: #ccebbc !important;
        }

        .liHovernew:hover {
            background-color: #4c6456 !important;
            color: #ffff;
        }
    </style>
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <div class="right_col">
        <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;padding-right: 80px">
            {{ $title }}</div>
        <div class="row codfox_container">

            <div class="col-md-5 padding_sm" id="dialysis_form_whole">
                <form action="{{ route('extensionsvalley.master.save_ledger_master') }}" method="POST"
                    id="ledger_master_form">
                    {!! Form::token() !!}
                    <div class="box no-border">
                        <div class="box-body clearfix" style="border:2px solid #a3a3de !important;border-radius:20px">

                            <input type="hidden" name="dialysis_id" id="dialysis_id" value="0">
                            <div class="col-md-12 padding_sm">
                                <div class="col-md-12 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Patient Name/UHID</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="patient_name" id="patient_name"
                                            onkeyup="select_patient_data(this,event,'add')">
                                        <input type="hidden" class="form-control" name="patient_name_id"
                                            id="patient_name_id" value="">
                                        <div class="ajaxSearchBox" id="patient_name_ajax"
                                            style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                             margin: 6px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 padding_sm"
                                    style="border-right: 2px solid #39ea79;padding:0px !important;height:214px">
                                    <div class="mate-input-box"
                                        style="border-bottom:0px #fff !important;box-shadow:0 0 0px #ffff !important;padding-top:0px !important">
                                        <table
                                            class="table no-margin theadfix_wrapper table-striped table-condensed styled-table">
                                            <tr>
                                                <td style="text-align:left">UHID</td>
                                                <td class=" common_td_rules" id="uhid" style="text-align:left"></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left">Age</td>
                                                <td class=" common_td_rules" id="patient_age" style="text-align:left"></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left">Sex</td>
                                                <td class=" common_td_rules" id="patient_sex" style="text-align:left"></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left">Address</td>
                                                <td class=" common_td_rules">
                                                    <textarea name="patient_address" id="patient_address" readonly class="form-control " style="background-color: #fff;"></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6 padding_sm" style="padding:0px !important;height:214px">
                                    <div class="mate-input-box"
                                        style="border-bottom:0px #fff !important;box-shadow: 0 0 0px #ffff !important;padding-top:0px !important">
                                        <table
                                            class="table no-margin theadfix_wrapper table-striped table-condensed styled-table">
                                            <tr>
                                                <td style="text-align:left">Blood Group</td>
                                                <td>
                                                    <?php $blood_group = [0 => 'select'] + $blood_group->toArray(); ?>
                                                    {!! Form::select('blood_group', $blood_group, '', [
                                                        'autocomplete' => 'off',
                                                        'class' => 'form-control',
                                                        'id' => 'blood_group',
                                                    ]) !!}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left">Diagnosis</td>
                                                <td id="diagnosis">
                                                    <textarea name="patient_diagnosis" id="patient_diagnosis" class="form-control " style="height:138px !important"></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                        <div class="box-body clearfix" style="border:2px solid #a3a3de !important;border-radius:20px"
                            id="savedatadiv">
                            <div class="col-md-12 padding_sm">
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Date Of Intimation Of Dialysis</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control datepickers" value="{{ date('M-d-Y') }}"
                                            name="intimation_date" id="intimation_date">
                                    </div>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Vascular Access</label>
                                        <div class="clearfix"></div>
                                        <?php $dialysis_vascular_access = [0 => ' select'] + $dialysis_vascular_access->toArray(); ?>
                                        {!! Form::select('vascular_access', $dialysis_vascular_access, '', [
                                            'autocomplete' => 'off',
                                            'class' => 'form-control',
                                            'id' => 'vascular_access',
                                        ]) !!}

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Needle Type</label>
                                        <div class="clearfix"></div>
                                        <?php $dialysis_needle_types = [0 => ' select'] + $dialysis_needle_types->toArray(); ?>
                                        {!! Form::select('needle_types', $dialysis_needle_types, '', [
                                            'autocomplete' => 'off',
                                            'class' => 'form-control',
                                            'id' => 'needle_types',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Duration Of HD</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="duration_of_hd"
                                            id="duration_of_hd">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Bolus</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="bolus" id="bolus">
                                    </div>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Infusion</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="infusion" id="infusion">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Type of dialysis</label>
                                        <div class="clearfix"></div>
                                        <?php $dialysis_types = [0 => ' select'] + $dialysis_types->toArray(); ?>
                                        {!! Form::select('dialysis_types', $dialysis_types, '', [
                                            'autocomplete' => 'off',
                                            'class' => 'form-control',
                                            'id' => 'dialysis_types',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Dializer Type</label>
                                        <div class="clearfix"></div>
                                        <?php $dializer_types = [0 => ' select'] + $dializer_types->toArray(); ?>
                                        {!! Form::select('dializer_types', $dializer_types, '', [
                                            'autocomplete' => 'off',
                                            'class' => 'form-control',
                                            'id' => 'dializer_types',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Blood flow rate</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="blood_flow_rate"
                                            id="blood_flow_rate">
                                    </div>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">HIV</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="hiv" id="hiv">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">HBs Ag</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="hbag" id="hbag">
                                    </div>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">HCV</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" name="hcv" id="hcv">
                                    </div>
                                </div>


                                <div class="col-md-6 padding_sm" id="led_cancel_btn">
                                    <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                            class="fa fa-times"></i> Cancel</a>
                                </div>
                                <div class="col-md-6 padding_sm">
                                    <span class="btn btn-block btn-success" onclick="saveDialysisData()"
                                        id="save_btn_id"><i class="fa fa-save" id="add_new_ledger_spin"></i> Save</span>
                                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <div class="col-md-7 padding_sm">
        <div class="col-md-12 no-padding">
            <div class="box no-border">
                <div class="box-body clearfix">
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Patient Name/UHID</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" name="search_patient_name"
                                id="search_patient_name" onkeyup="select_booked_patient(this, event,'booked')"
                                value="" autocomplete="off">
                            <input type="hidden" class="form-control" name="search_patient_name_id"
                                id="search_patient_name_id" value="">
                            <div class="ajaxSearchBox" id="search_patient_name_ajax"
                                style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                         margin: 6px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                         border: 1px solid rgba(0, 0, 0, 0.3);">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Date </label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control datepickers" value=""
                                name="search_date" id="search_date">
                        </div>
                    </div>

                    <div class="col-md-1 padding_sm" style="width: 10%;">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <span class="btn btn-block btn-success" id="searchdatabtn"onclick="fetchDialysisListData()"><i
                                class="fa fa-search" id="searchdataspin"></i>
                            Search</span>
                    </div>
                    <div class="col-md-1 padding_sm" style="width: 10%;">
                        <label for="">&nbsp;</label>
                        <div class="clearfix"></div>
                        <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                            Clear</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="box no-border no-margin">
            <div class="box-body clearfix">
                <div id="fetch_dialysis_data" style="min-height:360px;"></div>

            </div>
        </div>
        <div style="margin-top: 9px;">
             <span> Booked Patient <i class="fa fa-square" style="color: #7ba1c1"></i></span>
        </div>
    </div>
    </div>
    </div>
    </div>

@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dialysis/js/dialysis.js') }}"></script>
    <script type="text/javascript"></script>
@endsection
