@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <style>
        .list_hover:hover {
            background-color: #ccebbc !important;
        }

        .liHovernew:hover {
            background-color: #4c6456 !important;
            color: #ffff;
        }
    </style>
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="hospital_header" value="{{$hospital_header}}">
    <div class="right_col">
        <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;padding-right: 80px">
            {{ $title }}</div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Patient Name/UHID</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="search_patient_name"
                                    id="search_patient_name" onkeyup="select_booked_patient(this, event,'booked')"
                                    value="" autocomplete="off">
                                <input type="hidden" class="form-control" name="search_patient_name_id"
                                    id="search_patient_name_id" value="">
                                <div class="ajaxSearchBox" id="search_patient_name_ajax"
                                    style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                         margin: 6px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                         border: 1px solid rgba(0, 0, 0, 0.3);">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Date </label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepickers" value="" name="search_date"
                                    id="search_date">
                            </div>
                        </div>

                        <div class="col-md-1 padding_sm" style="width: 10%;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block btn-success" id="searchdatabtn"onclick="fetchDialysisListData()"><i
                                    class="fa fa-search" id="searchdataspin"></i>
                                Search</span>
                        </div>
                        <div class="col-md-1 padding_sm" style="width: 10%;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Clear</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div id="fetch_dialysis_data" style="min-height:360px;"></div>

                </div>
            </div>
            <div style="margin-top: 9px;">
                <span> Log Patient <i class="fa fa-square" style="color: #ababab"></i></span>
           </div>
        </div>
    </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printModeIns" checked="true" id="printMode"
                               value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printModeIns" id="printMode"
                               value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate_log_Data('ResultsViewArea')" class="btn bg-primary pull-right"
                                style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 padding_sm" style="display: none">
        <div id="ResultDataContainer"
             style="max-height: 650px; padding: 10px;font-family:poppinsregular">
            <div style="background:#686666;">
                <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);
                      width: 100%;
                      padding: 30px;" id="ResultsViewArea">

                </page>
            </div>
        </div>
    </div>
    
@stop
@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/purchase/default/javascript/purchase_common.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dialysis/js/dialysis_enrol_patient_list.js') }}"></script>
    <script type="text/javascript"></script>
@endsection
