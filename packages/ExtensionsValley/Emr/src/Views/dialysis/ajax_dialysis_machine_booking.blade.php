@foreach ($slots as $sl)
    <div style="background-color: #b1e39c;height: 27px;padding-left: 20px;">
        <h5 style="padding-top: 9px;">{{ $sl->name }}</h5>
    </div>
    <div class="box-body clearfix">
        @php $i = 0 @endphp
        @foreach ($booking_list as $data)
        @if ($data->slot_id == $sl->id)
        @php $i++ @endphp
        <div class="col-md-2 ns_patient_container_main sortable no-padding;" style="width:8% !important" id="img_cls{{ $sl->id }}{{ $data->machine_id }}" data-machine_id="{{ $data->machine_id }}">

                <div class="col-md-12 card-body ns_patient_container sortable no-padding" id="booking_slot_{{ $data->machine_id }}" data-booking_incr = '{{ $i }}'
                    style="font-size: 17px !important;height: 65px">    


                        @if ($data->booking_id == '')
                        <a data-toggle = 'popover' data-trigger="hover" data-container = 'body'  style='cursor:pointer;float: left'
                    data-content="<table>
                        <tr>
                            <th>{{ @$data->machine ? $data->machine :'' }}</th>
                        </tr></table>" class="booking_id_{{ $i }}" data-booking_incr = '{{ $i }}' data-booking_id = '{{ $data->booking_id }}' data-machine-id="{{ $data->machine_id }}" data-patient="">
                            <img src="{{ asset('packages/extensionsvalley/dialysis/img/dia_m_available.png') }}"
                                alt="" class=" thumbnail" style="height:40px;width:40px;margin-left: 14px;" onclick="saveMachineBook(this,'{{ $sl->id }}','{{ $data->machine_id }}')">
                    </a>
                                @else
                    <a  data-toggle = 'popover' data-trigger="hover" data-container = 'body' style='cursor:pointer;float: left'
                    data-content="<table>
                        <tr class='bg-blue'>
                            <th colspan='3'>{{ @$data->machine ? $data->machine :'' }}</th>
                        </tr>
                            <tr class='table_book_{{ $i }}'>
                                <td>Name</td>
                                <td>:</td>
                                <td>{{ @$data->patient_name ? $data->patient_name :'' }}</td>
                            </<tr>
                            <tr class='table_book_{{ $i }}'>
                                <td>UHID</td>
                                <td>:</td>
                                <td>{{ @$data->uhid ? $data->uhid :''}}</td>
                            </<tr>
                            <tr class='table_book_{{ $i }}'>
                                <tr>
                                    <td>Age</td>
                                    <td>:</td>
                                    <td>{{ @$data->age ? $data->age :''}}</td>
                                </<tr>
                                <tr>
                                <td>ADDRESS</td>
                                <td>:</td>
                                <td>{{ @$data->address ? $data->address :'' }}</td>
                            </<tr>
                            <tr class='table_book_{{ $i }}'>
                                <td>Phone</td>
                                <td>:</td>
                                <td>{{ @$data->phone ? $data->phone :'' }}</td>
                            </<tr>
                            <tr class='table_book_{{ $i }}'>
                                <td>Gender</td>
                                <td>:</td>
                                <td>{{ @$data->gender ? $data->gender :'' }}</td>
                            </<tr>
                                <tr class='table_book_{{ $i }}'>
                                    <td>Blood Group</td>
                                    <td>:</td>
                                    <td>{{ @$data->blood_group ? $data->blood_group :'' }}</td>
                                </<tr>
                        </table>"
                    class="booking_id_{{ $i }}" data-patient = "{{ $data->patient_id }}" data-booking_incr = '{{ $i }}' data-booking_id = '{{ $data->booking_id }}' data-machine-id="{{ $data->machine_id }}">
                    <img src="{{ asset('packages/extensionsvalley/dialysis/img/dia_m_booked.png') }}"
                                alt="" class=" thumbnail" style="height:40px;width:40px;margin-left: 14px;" onclick="deleteMachineBook(this,'{{ $data->booking_id }}','{{ $sl->id }}','{{ $data->machine_id }}','{{ $i }}','{{ $data->patient_id }}')">
                </a>
                        @endif


                </div>

        </div>
        @endif
        @endforeach
    </div>
@endforeach
