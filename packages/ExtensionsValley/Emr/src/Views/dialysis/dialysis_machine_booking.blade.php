@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<style>
.list_hover:hover {
    background-color: #ccebbc !important;
}

.liHovernew:hover {
    background-color: #4c6456 !important;
    color: #ffff;
}

</style>
<link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/nursing_new/css/nursing_station.css?version='.env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">

@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<div class="right_col">
    <div class="row" style="text-align: right; font-size: 12px; font-weight: bold;padding-right: 80px"> {{$title}}</div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border">
                    <div class="box-body clearfix">
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Date</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control datepickers" name="booking_date" id="booking_date" value="{{date('M-d-Y')}}"
                                    autocomplete="off" onblur="searchBookingData();">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">
                                <label for="">Patient Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="search_patient_name"
                                    id="search_patient_name" onkeyup="select_booked_patient(this, event,'booked')"
                                    value="{{ @$patient_name ? $patient_name: ''}}" autocomplete="off">
                                <input type="hidden" class="form-control" name="search_patient_name_id"
                                    id="search_patient_name_id" value="{{ @$patient_id ? $patient_id: '' }}">
                                    <input type="hidden" class="form-control" name="enrol_id"
                                    id="enrol_id" value="{{ @$patient_enrol_id ? $patient_enrol_id: '' }}">
                                    <input type="hidden" class="form-control" name="booking_id"
                                    id="booking_id" value="{{ @$patient_booking_id ? $patient_booking_id: '' }}">
                                <div class="ajaxSearchBox" id="search_patient_name_ajax" style="text-align: left; list-style: none;  cursor: pointer; max-height: 430px;
                                             margin: 6px 0px 0px 0px;overflow-y: auto; width: 100%; z-index: 1100;  position:absolute; background: #ffffff;  border-radius: 3px;
                                             border: 1px solid rgba(0, 0, 0, 0.3);"> </div>
                            </div>
                        </div>

                        {{-- <div class="col-md-1 padding_sm" style="width: 10%;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <span class="btn btn-block btn-success" id="searchdatabtn"onclick="searchBookingData()"><i
                                    class="fa fa-search" id="searchdataspin"></i>
                                Search</span>
                        </div> --}}
                        <div class="col-md-1 padding_sm" style="width: 10%;">
                            <label for="">&nbsp;</label>
                            <div class="clearfix"></div>
                            <a href="{{Request::url()}}" class="btn btn-block btn-warning"><i class="fa fa-times"></i>
                                Clear</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="box no-border no-margin" id="machineBookDiv" style="margin-top: -20px !important;">

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

@stop
@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/purchase/default/javascript/purchase_common.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/dialysis/js/dialysis_machine.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript">

</script>
@endsection
