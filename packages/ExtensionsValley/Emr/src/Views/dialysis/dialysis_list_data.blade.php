<script type="text/javascript">
$(document).ready(function() {
    $(".page-link").click(function() {
        var url = $(this).attr("href");
        var patient_id = $("#search_patient_name_id").val();
        var param = {
            patient_id: patient_id
        };
        $.ajax({
            type: "GET",
            url: url,
            data: param,
            beforeSend: function() {
                $('#searchdatabtn').attr('disabled', true);
                $('#searchdataspin').removeClass('fa fa-search');
                $('#searchdataspin').addClass('fa fa-spinner');
                $('#fetch_dialysis_data').LoadingOverlay("show", {
                    background: "rgba(255, 255, 255, 0.7)",
                    imageColor: '#009869'
                });
            },
            success: function(msg) {
                $('#fetch_dialysis_data').html(msg);
            },
            complete: function() {
                $('#searchdatabtn').attr('disabled', false);
                $('#searchdataspin').removeClass('fa fa-spinner');
                $('#searchdataspin').addClass('fa fa-search');
                $('#fetch_dialysis_data').LoadingOverlay("hide");
            },
            error: function() {
                toastr.error("Please Check Internet Connection");
            }
        });
        return false;
    });

});
</script>
<div class="theadscroll" style="position: relative; max-height: 400px;">
    <table class="table table-bordered no-margin table_sm no-border">
        <thead>
            <tr class="table_header_bg">
                <th>UHID</th>
                <th>Patient Name</th>
                <th>Age</th>
                <th>Dialysis Type</th>
                <th>Last Visit Datetime</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody class="dialysis_list_table_body">
            @isset($result)
            @if(count($result) > 0)
            @foreach($result as $item)
            @if($item->booking_count > 0)
             <tr id="tr_{{$item->id}}" style="background-color: #7ba1c1;color: #fff;">
            @else    
             <tr id="tr_{{$item->id}}">
            @endif
                <td>{{$item->uhid}}</td>
                <td>{{$item->patient_name}}</td>
                <td>{{$item->age}}</td>
                <td>{{$item->dialysis_type}}</td>
                <td>{{date('M-d-Y', strtotime($item->last_visit_datetime))}}</td>
                <td>
                    <button type="button" class="btn btn-sm bg-blue" onclick="editDialysis('{{$item->id}}','{{$item->patient_name}}','{{$item->pid}}','{{$item->uhid}}','{{$item->age}}','{{$item->gender}}',
                            '{{$item->address}}','{{$item->blood_group}}','{{$item->intimation_date}}','{{$item->vascular_access}}','{{$item->needle_type}}'
                            ,'{{$item->duration_of_hd}}','{{$item->bolus}}','{{$item->infusion}}','{{$item->dialysis_types}}','{{$item->blood_flow_rate}}','{{$item->hiv}}'
                            ,'{{$item->hbag}}','{{$item->hcv}}','{{$item->dializer_types}}','{{trim($item->blood_group)}}','{{$item->diagnosis}}');">
                        <i  class="fa fa-edit"></i>
                    </button>
                    <button type="button" id="delete_dialysis_btn_{{$item->id}}" class="btn btn-sm bg-red" onclick="deleteDialysis('{{$item->id}}');">
                        <i id="delete_dialysis_btn_spin_{{$item->id}}" class="fa fa-trash"></i>
                    </button>
                    @if($item->booking_count > 0)
                    <a target="_blank" href ="{{ route('extensionsvalley.dialysis.dialysisMachineBooking')}}?patient_id={{$item->pid}}&patient_name={{$item->patient_name}}&patient_enrol_id={{$item->id}}" >
                        <button type="button" class="btn btn-sm bg-blue" style="background-color: orange !important">
                            <i  class="fa fa-calendar"></i>
                        </button></a>
                    @else    
                    <a target="_blank" href ="{{ route('extensionsvalley.dialysis.dialysisMachineBooking')}}?patient_id={{$item->pid}}&patient_name={{$item->patient_name}}&patient_enrol_id={{$item->id}}" >
                        <button type="button" class="btn btn-sm bg-blue">
                            <i  class="fa fa-calendar"></i>
                        </button></a>
                    @endif
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="5">No records found..!</td>
            </tr>
            @endif
            @endisset
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<div class="col-md-12 text-center">
    <ul class="pagination purple_pagination pull-right">
        {!! $page_links !!}
    </ul>
</div>
</div>
