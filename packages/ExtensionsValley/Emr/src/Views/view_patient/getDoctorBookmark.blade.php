<style>
    .bookmarks_list {
        display: none;
    }
</style>
@if (count($result) > 0)

    @foreach ($result as $each)
        <div class="prescription_bookmark_item" id="prescription_bookmark_item{{ $each['group_id'] }}">
            <div class="presc_bookmark_head" style="display:flex;">
                <div class="presc_bookmark_head_left">
                    <div><i class="fa fa-bookmark"></i> {{ ucwords(strtolower($each['group_name'])) }} </div>
                </div>
                <div class="presc_bookmark_head_right">
                    <i class="fa fa-eye viewBtn viewPrescriptionGroupBtn"></i>
                    <i class="fa fa-copy applyBtn applyPrescriptionGroupBtn"></i>
                    <button type="button" class="btn deleteBtn"
                        onclick="deleteGrpName(this,{{ $each['group_id'] }},1)"><i class="fa fa-trash"></i></button>

                </div>
            </div>
            
            <div class="bookmarks_list">
                @foreach ($each['medicine_list'] as $each1)
                   {{-- @php
                       print_r($each['medicine_list']);
                       exit;
                   @endphp --}}
                    @php
                        $item_code = @$each1['item_code'] ? $each1['item_code'] : '';
                        $medicine_name = @$each1['medicine_name'] ? $each1['medicine_name'] : '';
                        $frequency = @$each1['frequency'] ? $each1['frequency'] : '';
                        $frequency_value = @$each1['frequency_value'] ? $each1['frequency_value'] : 1;
                        $frequency_id = @$each1['frequency_id'] ? $each1['frequency_id'] : '';
                        $duration = @$each1['duration'] ? $each1['duration'] : '';
                        $duration_unit = @$each1['duration_unit'] ? $each1['duration_unit'] : 1;
                        $dosage_unit = @$each1['dosage_unit'] ? $each1['dosage_unit'] : 0;
                        $quantity = @$each1['quantity'] ? $each1['quantity'] : '';
                        $notes = @$each1['notes'] ? $each1['notes'] : '';
                        $dose = @$each1['dose'] ? $each1['dose'] : '';
                        $generic_name_id = @$each1['generic_name_id'] ? $each1['generic_name_id'] : 0;
                        
                    @endphp
                   
                    <div class="bookmarks_list_item" data-item-code="{{ base64_encode($item_code) }}"
                        data-medicine-name="{{ base64_encode(ucwords(strtolower($medicine_name))) }}"
                        data-frequency-name="{{ base64_encode($frequency) }}"
                        data-frequency-value="{{ base64_encode($frequency_value) }}"
                        data-frequency-id="{{ base64_encode($frequency_id) }}"
                        data-item-duration="{{ base64_encode($duration) }}"
                        data-item-quantity="{{ base64_encode($quantity) }}"
                        data-item-direction="{{ base64_encode($notes) }}" 
                        data-item-duration_unit="{{ base64_encode($duration_unit) }}"
                        data-item-dose="{{ base64_encode($dose) }}"
                        data-item-dosage_unit="{{ base64_encode($dosage_unit) }}"
                        data-generic-id="{{ base64_encode($generic_name_id) }}">
                        <div class="checkbox checkbox-brown inline no-margin bookmark_item_check"
                            style="margin-top: 5px;">
                            <input type="checkbox"
                                class="item_code_prescription{{ $item_code }} prescription_bookmark_item_checkbox"
                                type="checkbox" style="margin-left: -20px !important;"
                                onclick="addDoctorPrescription('{{ base64_encode(ucwords(strtolower($medicine_name))) }}'
                    ,'{{ base64_encode($frequency) }}','{{ $duration }}',{{ $duration_unit }},'{{ $quantity }}','{{ base64_encode($notes) }}','{{ base64_encode($item_code) }}',this, '{{ base64_encode($dose) }}','{{ base64_encode($dosage_unit) }}', '{{ base64_encode($generic_name_id) }}')">
                            <label style="padding-left: 2px;" for="item_code_prescription{{ $item_code }}">
                                <span class="presc_bookmark_item_desc">{{ ucwords(strtolower($medicine_name)) }}</span>
                                <span class="presc_bookmark_frequency">{{ $frequency }}</span>
                                <span class="presc_bookmark_duration"> x {{ $duration }} days</span></label><br>
                        </div>
                        <div class="clearfix border_dashed_bottom"></div>
                    </div>
                @endforeach
            </div>

        </div>
    @endforeach
@else
    <div class="prescription_bookmark_item">
        <div class="presc_bookmark_head" style="display:flex;">
            <div class="presc_bookmark_head_left">
                No Result Found
            </div>
        </div>
    </div>
@endif
