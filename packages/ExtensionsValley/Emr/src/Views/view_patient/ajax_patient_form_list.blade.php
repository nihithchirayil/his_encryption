<table class="table no-margin theadfix_wrapper table-striped table_sm table-condensed styled-table floatThead-table table-col-bordered" id="lab_result_table" style="border: 1px solid rgb(204, 204, 204); table-layout: fixed;width:100%;">
    <thead class="headergroupbg" style='background: #21a4f1 !important; color: white;'>
        <th style="width: 2%;">Sl.No</th>
        <th style="width: 15%;">Form</th>
        <th style="width: 10%;">Created Date</th>
        <th style="width: 27%;">Created User</th>
        <th style="width: 7%;">Action</th>
    </thead>
    <tbody>
        @php $i = 1; @endphp
       @if(!empty($formdata_list) && count($formdata_list) > 0)
        @foreach($formdata_list as $value)

        <tr>
            <td> {{ $i++ }}</td>
            <td> {{ @$value->name ?  $value->name : '' }}</td>
            <td> {{ @$value->created_at ? date('Y-m-d H:i', strtotime($value->created_at)) : '' }}</td>   
            <td> {{ @$value->created_by ?  $value->created_by : '' }}</td>

            <td class="text-center">
                <a class="btn btn-info btn-default" id="viewFormViewbtn{{$value->id}}" style="margin-right: 2px;" onclick="viewFormData({{$value->id}}, {{$value->form_id}}, 1);" title="View" ><i class="fa fa-eye"></i></a>
                <a class="btn btn-info btn-default" id="printFormViewbtn{{$value->id}}" style="margin-right: 2px;" onclick="viewFormData({{$value->id}}, {{$value->form_id}} , 2);" title="Print" ><i class="fa fa-print"></i></a>
            </td>
        </tr>
       @endforeach
        @else
        <tr>
            <td colspan="7">No data found...</td>
        </tr>
        @endif
    </tbody>
</table>
