@if(!empty($resultData))
    {{-- todays visits --}}
    <div class="text_head">Today's Visit</div>
    <div class="clearfix"></div>
    <div class="todays_invresult_entry_history_container theadscroll" style="position:relative; height:143px;">
        <table class="table todays_invresult_entry_history_container no-border no-margin">
            </tbody>
                @php
                    $i = 0;
                @endphp
            @foreach($resultData as $data)
                @if($data->visit_id == $visit_id)
                    @php
                        $i++;
                    @endphp
                    @if($i==1)
                        <tr class="border_dashed_bottom" id="scrollto-1" style="margin-bottom:5px;">
                            <td colspan="3" class="text-left">
                                <b>{{date('M-d-Y h:i A',strtotime($data->created_at))}}</b>
                            </td>
                        </tr>
                        <tr class="border_dashed_bottom no-padding" style="background-color:ebfaef;">
                            <td width="20%" style="padding-left:0px;"><b>Date</b></td>
                            <td width="40%" style="padding-left:0px;"><b>Investigation</b></td>
                            <td width="40%" style="padding-left:0px;"><b>Result</b></td>
                        </tr>
                    @endif
                        <tr class="no-padding">
                            <td class="no-padding">{{date('M-d-Y',strtotime($data->investiagation_date))}}</td>
                            <td class="no-padding">{{$data->investiagation}}</td>
                            <td class="no-padding">{{$data->investiagation_result}}</td>
                        </tr>
                @endif
            @endforeach
        </table>
    </div>
    <br>

    {{-- history --}}

    <div class="text_head">History</div>
    <div class="clearfix"></div>

    <div class="history_invresult_entry_history_container theadscroll" style="position:relative; height:303px;">
        <table class="table todays_invresult_entry_history_container no-border no-margin">
            </tbody>
                @php
                    $temp_visit_id = '';
                @endphp
            @foreach($resultData as $data)
                @if($data->visit_id != $visit_id)
                    <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                        <td width="20%" style="padding-left:0px;"><b>Date</b></td>
                        <td width="20%" style="padding-left:0px;"><b>Investigation</b></td>
                        <td width="20%" style="padding-left:0px;"><b>Result</b></td>
                    </tr>
                    @if($temp_visit_id != $data->visit_id)
                        <tr class="border_dashed_bottom" id="scrollto-1" style="margin-bottom:5px;">
                            <td colspan="3" class="text-left">
                                <b>{{date('M-d-Y h:i A',strtotime($data->created_at))}}</b>
                            </td>
                        </tr>
                    @endif
                        <tr class="no-padding">
                            <td class="no-padding">{{date('M-d-Y',strtotime($data->investiagation_date))}}</td>
                            <td class="no-padding">{{$data->investiagation}}</td>
                            <td class="no-padding">{{$data->investiagation_result}}</td>
                        </tr>
                        @php
                            $temp_visit_id = $data->visit_id;
                        @endphp
                @endif
            @endforeach
        </table>
    </div>

@else
@endif
