<?php
if (isset($patient_visit_search)) {
    if (!empty($patient_visit_search)) {

        foreach ($patient_visit_search as $desc) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillGlobalPatientData("{{htmlentities($desc->id)}}","{{htmlentities($desc->uhid)}}","{{htmlentities($desc->patient_name)}}", "{{$input_id}}")' class="list_hover">
                {{$desc->patient_name}} [{!! $desc->uhid !!}]
            </li>
            <?php
}
    } else {
        echo 'No Result Found';
    }
}

?>