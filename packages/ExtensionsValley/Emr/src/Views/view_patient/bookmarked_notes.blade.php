@foreach($bookmarked_notes as $key => $notes)
<div class="bookmarked_notes_item">
    <span class=" bookmarks-notes-text bookmarked_notes_text" title="{{ $notes->notes_text }}"> 
    {{ $notes->bookmark_name }}
    </span>
    @if($notes->doctor_id == $doctor_id)
    <i style="margin-left: 8px;" class="fa fa-times red deleteBookmarkedItemBtn" data-bookmarked-id="{{$notes->id}}"> </i>
    @endif
</div>
@endforeach