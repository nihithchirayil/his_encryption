
<!----------Modal box for doctor referral------------------------------------------------>
<div class="modal modal-fullscreen" id="modal_doctor_refferal" tabindex="-1" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Refer Doctor</h4>
            </div>
            <div class="modal-body" id="modal_doctor_refferal_content">
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-save" onclick="SaveDoctorRefferal();"
                        data-dismiss="modal"><i class="fa fa-save" aria-hidden="true"></i> Save</button>
                </div>
        </div>
    </div>
</div>
