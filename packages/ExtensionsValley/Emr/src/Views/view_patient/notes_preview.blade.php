
<table class="smart_table" style="border-collapse:collapse;border-color:rgb(241, 235, 235);" width="100%" cellspacing="0" cellpadding="0">
    <tbody>

        @if(!empty($result))
            @foreach($result as $key => $data)
            <tr style="padding-left:0px;">
                @php
                    $field_label = (!empty($data['field_label'])) ? $data['field_label'] : '';
                    $res = (!empty($data['result'])) ? $data['result'] : '';
                @endphp
                @if(!empty($res))
                    @php
                        $res = nl2br($res);
                    @endphp
                <td colspan="4">
                    <b>{!!$field_label!!}</b>
                    <br> {!!$res!!}
                </td>
                @endif
            </tr>
            @endforeach
        @endif
    </tbody>
</table>

        