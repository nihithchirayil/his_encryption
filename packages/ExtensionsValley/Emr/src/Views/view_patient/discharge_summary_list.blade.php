<div class="theadscroll always-visible" style="position: relative; height: 310px;">
    @if(sizeof($res)>0)
    <table class="table table_sm table-bordered theadfix_wrapper">
        <thead>
            <tr class="table_header_bg">
                <th>Sl No</th>
                <th>Admit Date</th>
                <th>Admit Doctor</th>
                <th>Patient Name</th>
                <th>Uhid</th>
                <th>Discharge Date</th>
                <th>Summary Prepared At</th>
                <th>Finalize</th>
                <th>Summary</th>
            </tr>
        </thead>
        <tbody>
            @php
                $i =1;
            @endphp
            @foreach($res as $data)

                <tr>
                    <td >{{$i}}</td>
                    <td >{{date('M-d-Y',strtotime($data->admit_date))}}</td>
                    <td >{{$data->doctor_name}}</td>
                    <td >{{$data->patient_name}}</td>
                    <td >{{$data->uhid}}</td>
                    <td >
                        @if($data->discharge_date !='')
                            {{date('M-d-Y',strtotime($data->discharge_date))}}
                        @endif
                    </td>
                    <td >{{date('M-d-Y',strtotime($data->last_updated_at))}}</td>
                    @if($data->status !=1 && $data->final_status !=0)
                    <td style="text-align:center">
                            <button title="Mark as Finalized" onclick="finalizeDischargeSummary('{{$data->id}}');" class="btn btn-success">
                                <i class="fa fa-check"></i>
                            </button>

                    </td>
                    @elseif($data->status == 1)
                    <td style="text-align:center"><span style="color:green;">Finalized</span></td>
                    @else
                    <td style="text-align:center"></td>
                    @endif
                    @if($data->final_status !=0)
                    <td style="text-align:center">
                            <button title="View Summary" onclick="showSummary('{{$data->id}}');" class="btn bg-primary">
                                <i class="fa fa-list"></i>
                            </button>

                    </td>
                    @else
                    <td style="text-align:center"><span style="color:red;">Summary Not Generated</span></td>
                    @endif

                </tr>
                @php
                    $i++;
                @endphp
            @endforeach
        </tbody>
    </table>
    @else
    <span>No Summary found </span>
    @endif
</div>
@if(!empty($res))

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
@endif