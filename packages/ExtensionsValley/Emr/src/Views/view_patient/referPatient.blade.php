<div class="row">
    <div class="col-md-12">
        <span>Doctor</span>
        {!! Form::select('doctor_ref', $doctors_list, '', ['class' => 'form-control','placeholder' => 'Select Doctor','id' => 'doctor_ref']) !!}
        <br>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <span>Refferal Notes</span>
        <textarea style="resize: vertical;" class="form-control" name="refer_notes" placeholder=""></textarea>
    </div>
</div>
