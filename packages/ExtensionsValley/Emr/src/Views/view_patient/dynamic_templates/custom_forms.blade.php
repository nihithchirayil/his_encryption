<div class="col-md-12" style="padding: 0 0 6px 8px;">
    <div class="col-md-8 no-padding">
        {!! Form::select('select_dynamic_template',$dynamic_templates,null, ['class' => 'form-control bottom-border-text bottom-border-text borderless_textbox', 'id' =>'select_dynamic_template', 'style' => 'color:#555555; padding:4px 12px;','onchange'=>'loadDynamicTemplate()','placeholder' => 'Select Template']) !!}
    </div>
    <div class="col-md-4 no-padding">
        <button type="button" class="btn bg-primary" onclick="loadDynamicTempateHistory();"  style="
        width: 75px;
        height: 34px;
        margin-left: 8px;
        background:#21a4f1;
    " data-toggle="collapse" href="#dynamic_template_history_section"><i class="fa fa-book"></i> History</button>
    </div>
    <div class="col-md-12 collapse theadscroll box-body" id="dynamic_template_history_section" style="height:150px;padding:8px;">
    </div>
</div>
<div class="col-md-12 theadscroll" id="dynamic_template_data" style="margin-top: 10px; height: 80vh;">

</div>
