
<div class="box no-border no-margin anim" style="min-height:390px;">
    <div class="box-body clearfix">
        <table class="table table-bordered no-margin table_sm no-border theadfix_wrapper">
            <thead>
                <tr class="table_header_bg">
                    <th>Sl.No.</th>
                    <th>Bill Date</th>
                    <th>UHID</th>
                    <th>Patient Name</th>
                    <th>Service Description</th>
                    <th>Modality</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody class="radiology_list_table_body">
            @isset($radiology_list)
            @if(count($radiology_list) > 0)
            @foreach($radiology_list as $radiology)
            <tr class="patient_det_widget_hp" style="cursor:pointer;" >
                <td>{{($radiology_list->currentPage() - 1) * $radiology_list->perPage() + $loop->iteration}}</td>
                <td>{{$radiology->bill_date}}</td>
                <td>{{$radiology->uhid}}</td>
                <td>{{$radiology->patient_name}}</td>
                <td>{{$radiology->service_desc}}</td>
                <td>{{$radiology->modality}}</td>
                <td>
                    <button type="button" class="btn btn-sm btn-primary pacsViewerBtn"  data-accession-no="{{$radiology->accession_no}}" ><i class="fa fa-eye"></i> View Image</button>
                    <button type="button" class="btn btn-sm btn-primary pacsViewReportBtn" data-accession-no="{{$radiology->accession_no}}"><i class="fa fa-file"></i> View Report</button>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="7">No records found..!</td>
            </tr>
            @endif
            @endisset
            </tbody>
        </table>

    </div>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination radilogy_results_pagination_div" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>