<input type="hidden" id="addItemTogrp_grpId" value="{{ $group_id }}">
<div class="col-md-12 theadscroll" style="height: 258px">
    <table id=""
        class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed">
        <thead>
            <tr class="tableRowHead">

                <th width="5%">Sl.No.</th>
                @if($duration_unit_enable == '1')
                <th width="23%">Medicine</th>
                <th width="10%">Dose</th>
                @else
                <th width="33%">Medicine</th>
                @endif
                <th width="14%">Freq</th>
                <th width="14%">Dur</th>
                <th width="10%">Qty</th>
                <th width="10%">Dir</th>

                <!-- <th width="5%">Sl.No.</th>
                <th width="25%">Medicine</th>
                <th width="15%">Freq</th>
                <th width="15%">Dur</th>
                <th width="15%">Qty</th>
                <th width="20%">Dir</th> -->
                <th width="5%"><i onclick="addNewMediceneGrp()" class="fa fa-plus"></i></th>
            </tr>
        </thead>
        <tbody class="prescription_list_table_body_grp">
            @if (count($med_list) > 0)

            @php

            $i=1;
            @endphp
            @foreach ($med_list as $each)
            <tr class='row_class_addGrp'>
                <td style='text-align: center; line-height: 25px;' class='row_count_class_addGrp'>{{ $i }}.</td>
                <td><input type='text' autocomplete='off' name='medicine_name' readonly
                        class='form-control bottom-border-text borderless_textbox item_description'
                        data-item-code='{{ $each->item_code }}' value="{{ $each->medicine_name }}" />
                </td>
                @if($duration_unit_enable == '1')
                <td><input type='text' autocomplete='off'
                        class='form-control bottom-border-text borderless_textbox dosage' value="{{$each->dose}}"
                        style='width: 60%;float: left;' /><select onchange='showTitle(this)' name='dosage_unit'
                        class='form-control bottom-border-text borderless_textbox dosage_unit'
                        style='width: 18px;height: 18px; margin-top: 2px; margin-left: 3px; border:1px solid #21a4f1 !important'>
                        <option value="">Dose</option>
                        @foreach ($dosage_unit as $data)
                        <?php
                        $selected = '';
                        if ($each->dose_unit_id == $data->id)
                            $selected = 'selected';
                        ?>
                        <option <?= $selected ?> value="{{ $data->id }}">{{ $data->name }}
                        </option>
                        @endforeach
                    </select>
                </td>
                @endif
                <td><input type='text' autocomplete='off'
                        class='form-control bottom-border-text borderless_textbox frequency'
                        value="{{$each->freq}}" data-frequency-value="{{$each->frequency_value}}"
                        data-frequency-id="{{$each->frequency_id}}" /></td>
                @if($duration_unit_enable == '1')
                <td><input type='text' autocomplete='off'
                        class='form-control bottom-border-text borderless_textbox duration' value="{{$each->dur}}"
                        style='width: 60%;float: left;' /><select onchange='showTitle(this)' name='duration_unit'
                        class='form-control bottom-border-text borderless_textbox duration_unit'
                        style='width: 18px;height: 18px; margin-top: 2px; margin-left: 3px; border:1px solid #21a4f1 !important'>
                        <option value="">Unit</option>
                        @foreach ($duration_unit as $data)
                        <?php
                        $selected = '';
                        if ($each->duration_unit == $data->id)
                            $selected = 'selected';
                        ?>
                        <option <?= $selected ?> value="{{ $data->id }}">{{ $data->name }}
                        </option>
                        @endforeach
                    </select>
                </td>
                @else
                <td><input type='text' autocomplete='off'
                        class='form-control bottom-border-text borderless_textbox duration' /
                        value="{{$each->dur}}"></td>                
                @endif              
                <td style="display: none"><input type='text' autocomplete='off'
                        class='form-control bottom-border-text borderless_textbox duration_unit' /
                        value="{{$each->duration_unit}}"></td>
                <td><input type='text' autocomplete='off'
                        class='form-control bottom-border-text borderless_textbox quantity'
                        value="{{$each->quantity}}" /></td>
                <td><input type='text' autocomplete='off'
                        class='form-control bottom-border-text borderless_textbox direction'
                        value="{{ $each->notes}}" /></td>
                <td class='text-center'><i onclick='removeAddGrpRow(this)'
                        class='fa fa-times red removeAddGrpRowBtn'></i></td>
            </tr>
            @php
            $i++;
            @endphp
            @endforeach

            @else
            @endif
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<div class="col-md-1 pull-right " style="margin-right: 18px;">
    <button style="padding: 3px 3px" type="button" id="saveNewItemsToGroupBtn" onclick="saveNewItemsToGroup(this)"
        class="btn btn-brown"><i class="fa fa-save padding_sm"></i>Save</button>
</div>