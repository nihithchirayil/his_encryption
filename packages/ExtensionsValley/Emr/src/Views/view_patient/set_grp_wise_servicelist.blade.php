<input type="hidden" id="addItemToInv_grpId" value="{{ $group_id }}">
<div class="patient_details_patient_name text-normal" id="addnewgrp">
    <input type="text" placeholder="Search and add services" autocomplete="off"
        class="form-control bottom-border-text borderless_textbox investigation_item_search_textbox">
</div>

<div class="col-md-12 theadscroll" style="height: 224px;margin-top:10px">
    <table id=""
        class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed">

        <tbody class="service_list_table_body_grp">
            @if (count($service_list) > 0)

                @php
                    
                    $i = 1;
                @endphp
                @foreach ($service_list as $each)
                    <tr data-service-code="{{ $each->service_code }}" data-group-id="{{ $group_id }}">
                        <td class="add_investigation_service_name">{{ $each->service_name }}</td>
                        <td class="add_investigation_delete_btn"><i class="fa fa-times red"></i></td>
                    </tr>

                    @php
                        $i++;
                    @endphp
                @endforeach
            @else
            @endif
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<div class="col-md-1 pull-right " style="margin-right: 45px;">
    <button style="padding: 3px 3px" type="button" id="saveNewItemsToInvBtn" onclick="saveNewInvToGroup(this)"
        class="btn btn-brown"><i class="fa fa-save padding_sm"></i>Save</button>
</div>
