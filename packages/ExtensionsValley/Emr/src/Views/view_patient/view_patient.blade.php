@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/view_patient.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/multi_tab_setup.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/common_controls.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/CombinedHistory.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.6.95/css/materialdesignicons.css" rel="stylesheet">
<style>
    .patient_details_patient_image {
        border: 1px solid #21a4f1;
        width: 60px;
        height: 60px;
    }

    .patient_details_patient_image_container {
        margin-left: 15px;
    }

    .img-circle {
        border-radius: 20% !important;
    }

    .video_consultation_div {
        position: absolute;
        right: 5px;
        bottom: -6px;
    }

    .video_consultation_div button {
        width: 28px;
        height: 28px;
        border-radius: 50%;
        border: 2px solid #FFF;
        background: #2dfd20;
    }

    .video_consultation_div i {
        margin-left: -1px;
        color: #2a0dcb;
    }

    .revenue_main_shadow {
        box-shadow: 1px 1px 1px 1px grey;
        border-radius: 10px;
        border: 3px solid #ebebe1;
    }

    .template-name {
        text-align: center;
        font-weight: 700;
        text-decoration: underline;
    }
</style>

@endsection
@section('content-area')

<!--  modal boxes  -->

@include('Emr::view_patient.custom_modals_lite')

<div class="right_col" role="main">
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="emr_changes" value="{{ @$emr_changes ? $emr_changes : ''}}">
    <input type="hidden" id="doctor_id" value="{{ $doctor_id }}">
    <input type="hidden" id="default_note_form_id" value="{{ $default_note_form_id }}">
    <input type="hidden" id="patient_id" value="">
    <input type="hidden" id="booking_id" value="">
    <input type="hidden" id="visit_id" value="">
    <input type="hidden" id="visit_type" value="">
    <input type="hidden" id="encounter_id" value="">
    <input type="hidden" id="ca_head_id" value="">
    <input type="hidden" id="investigation_head_id" value="">
    <input type="hidden" id="prescription_head_id" value="">
    <input type="hidden" id="weight" value="2">
    <input type="hidden" id="temperature" value="9">
    <input type="hidden" id="height" value="4">
    <input type="hidden" id="vital_batch_no" value="">
    <input type="hidden" id="pacs_viewer_prefix" value="{{ $pacs_viewer_prefix }}">
    <input type="hidden" id="pacs_report_prefix" value="{{ $pacs_report_prefix }}">
    <input type="hidden" id="print_dialog_config" value="{{ $print_dialog_config }}">
    <input type="hidden" id="add_new_vital_batch" value="0">
    <input type="hidden" id="add_new_vital_batch" value="0">
    <input type="hidden" id="emr_lite_allergy_vital_config" value="{{$emr_lite_allergy_vital_config}}">
    <input type="hidden" id="quantity_auto_calculation_config" value="{{$quantity_auto_calculation_config}}">
    <input type="hidden" id="allow_outside_medicine_config" value="{{$allow_outside_medicine_config}}">
    <input type="hidden" id="queue_api_url" value="{{env('QUEUE_API_URL', '')}}">
    <input type="hidden" id="doctor_location" value="{{$doctor_location}}">
    <input type="hidden" id="show_price_prescription" value="{{ @$show_price_prescription ? $show_price_prescription : 0}}">
    <input type="hidden" id="doctor_room" value="{{$doctor_room}}">
    <input type="hidden" id="duration_unit" value="{{$duration_unit}}">
    <input type="hidden" id="duration_unit_enable" value="{{$duration_unit_enable}}">
    <input type="hidden" id="dosage_unit" value="{{$dosage_unit}}">
    <input type="hidden" id="document_save_type" value="{{$document_save_type}}">
    <input type="hidden" id="document_upload_path" value="{{$document_upload_path}}">
    <input type="hidden" id="dynamic_template_enable_emr_lite" value="{{$dynamic_template_enable_emr_lite}}">
    <input type="hidden" id="minimun_searchable_alphabet" value="{{$minimun_searchable_alphabet}}">
    <input type="hidden" id="ShowFarenheit" value="{{$ShowFarenheit}}">

    <input type="hidden" id="font_awsome_css_path"
        value="{{ asset('packages/extensionsvalley/dashboard/css/font-awesome.min.css') }}">
    <input type="hidden" id="bootstrap_min_path"
        value="{{ asset('packages/extensionsvalley/dashboard/css/bootstrap.min.css') }}">
    <input type="hidden" id="fontawsome_min_path"
        value="{{ asset('packages/extensionsvalley/dashboard/css/font-awesome.min.css') }}">
    <input type="hidden" id="bootstrapmin_js_path"
        value="{{ asset('packages/extensionsvalley/dashboard/js/bootstrap.min.js') }}">

    <a class="toggle-ip-op-list-btn toggleIpOpListBtn leftArrow" style="top: 147px; left: 64px;">
        <span class="toggle-ip-op-list-btn-icon">
            <i class="fa fa-arrow-left"></i>
        </span>

    </a>

    @include('Emr::view_patient.ip_op_list')
    @include('Emr::view_patient.patient_visit_master')
    @include('Emr::doctor_notes.doctor_notes_modal')

</div>

@stop
@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/view_patient.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/allergy_vitals_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/multi_tab_setup.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/investigation_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/prescription_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/notes_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/accounts/default/javascript/jquery-ui.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/manage-document.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/inv_result_entry.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/addnewinvestigationpopup_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>

<script src="{{asset("packages/extensionsvalley/master/form_template/js/jquery.drawr.combined.js")}}"></script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<!-- <script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script>
    <script src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template_vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}"></script> -->
<script src="{{ asset('packages/extensionsvalley/emr/js/ezoom.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
src="{{ asset('packages/extensionsvalley/emr/js/doctor_notes.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>


@endsection
