@if(sizeof($resultData)>0)
    @php
        $i=1;
    //dd($resultData);
    @endphp
    @foreach($resultData as $data)
    <tr>
        <td>
            <input type="textbox" value="{{date('M-d-Y',strtotime($data->investiagation_date))}}" name="investiagation_entry_date[]" id="investiagation_entry_date_{{$i}}" class="form-control bottom-border-text borderless_textbox datepicker"/>
        </td>
        <td>
            <input type="textbox" value="{{$data->investiagation}}"  name="investiagation_entry[]" id="investiagation_entry_{{$i}}" class="form-control bottom-border-text borderless_textbox"/>
        </td>
        <td>
            <input type="textbox" value="{{$data->investiagation_result}}" name="investiagation_entry_result[]" id="investiagation_entry_result_{{$i}}" class="form-control bottom-border-text borderless_textbox"/>
        </td>
        <td>
            <i onclick="removeInvResultEntry(this)" class="fa fa-times red"></i>
        </td>
    </tr>
    @php
    $i++;
    @endphp
    @endforeach

@else
<tr>
    <td>
        <input type="textbox" name="investiagation_entry_date[]" id="investiagation_entry_date_1" class="form-control bottom-border-text borderless_textbox datepicker"/>
    </td>
    <td>
        <input type="textbox" name="investiagation_entry[]" id="investiagation_entry_1" class="form-control bottom-border-text borderless_textbox"/>
    </td>
    <td>
        <input type="textbox" name="investiagation_entry_result[]" id="investiagation_entry_result_1" class="form-control bottom-border-text borderless_textbox"/>
    </td>
    <td>
        <i onclick="removeInvResultEntry(this)" class="fa fa-times red"></i>
    </td>
</tr>
@endif
