<div class="row padding_sm vital_form_container">
        @if(count($patient_visits) > 0)
        <div class="col-md-12 padding_sm addNewBatchDiv" >
                <button class="btn btn-sm btn-blue addNewBatchBtn" style="float: right;"> Add new batch</button>
        </div>
        @endif
    <div class="col-md-12 padding_sm">
        <div class="theadscroll" style="position: relative; height: 250px;">
            <div class="col-md-12 padding_sm" style="margin-top: 10px">
                <div class="col-md-3 padding_sm"><label>Weight in Kg</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        class="form-control td_common_numeric_rules"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        name="weight_value" id="weight_value"
                        value="{{ @$patient_visits[2] ? $patient_visits[2] : '' }}">
                </div>
                <div class="col-md-3 padding_sm"><label>Height/Length in cm</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        class="form-control td_common_numeric_rules"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        name="height_value" id="height_value"
                        value="{{ @$patient_visits[4] ? $patient_visits[4] : '' }}">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px">
                <div class="col-md-3 padding_sm"><label>BP Systolic mmHg</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        value="{{ @$patient_visits[5] ? $patient_visits[5] : '' }}"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control td_common_numeric_rules" name="bp_systolic" id="bp_systolic"></div>
                <div class="col-md-3 padding_sm"><label>BP Diastolic mmHg</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control td_common_numeric_rules" name="bp_diastolic" id="bp_diastolic"
                        value="{{ @$patient_visits[6] ? $patient_visits[6] : '' }}">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px">
                <div class="col-md-3 padding_sm"><label>Temperature in C</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        onkeyup="calculateTemperature(1)"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control td_common_numeric_rules int_type " name="temperature_value"
                        id="temperature_value" value="{{ @$patient_visits[10] ? $patient_visits[10] : '' }}"></div>
                <div class="col-md-3 padding_sm"><label>Temperature in F</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        onkeyup="calculateTemperature(2)"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control td_common_numeric_rules int_type " name="temperature_value_f"
                        id="temperature_value_f" value="{{ @$patient_visits[9] ? $patient_visits[9] : '' }}"></div>
                <div class="clearfix"></div>
            </div>

            <div class="col-md-12 padding_sm" style="margin-top: 10px">
                <div class="col-md-3 padding_sm"><label>Pulse/Min</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control td_common_numeric_rules int_type " name="pulse" id="pulse"
                        value="{{ @$patient_visits[7] ? $patient_visits[7] : '' }}">
                </div>
                <div class="col-md-3 padding_sm"><label>Respiration/Min</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control td_common_numeric_rules int_type " name="respiration" id="respiration"
                        value="{{ @$patient_visits[8] ? $patient_visits[8] : '' }}"></div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px">

                <div class="col-md-3 padding_sm"><label>Head Circ-cm</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control td_common_numeric_rules" name="head" id="head"
                        value="{{ @$patient_visits[13] ? $patient_visits[13] : '' }}">
                </div>
                <div class="col-md-3 padding_sm"><label>Oxygen Saturation %</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control td_common_numeric_rules" name="oxygen" id="oxygen"
                        value="{{ @$patient_visits[12] ? $patient_visits[12] : '' }}">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px">

                <div class="col-md-3 padding_sm"><label>BMI Kg/M^2</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control td_common_numeric_rules" name="bmi" id="bmi"
                        value="{{ @$patient_visits[14] ? $patient_visits[14] : '' }}">
                </div>
                <div class="col-md-3 padding_sm"><label>Spirometry</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        class="form-control td_common_numeric_rules" name="spirometry" id="spirometry"
                        value="{{ @$patient_visits[20] ? $patient_visits[20] : '' }}">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px">
                <div class="col-md-3 padding_sm"><label>Time Taken</label></div>
                <div class="col-md-3 padding_sm"><input type="text" autocomplete="off" class="form-control"
                        id="time_taken" value="{{ $time_taken ? $time_taken : date('M-d-Y h:i a') }}"></div>
            </div>
        </div>
    </div>
</div>
