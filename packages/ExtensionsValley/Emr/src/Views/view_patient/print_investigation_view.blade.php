<!-- .css -->
<style>
  #doctorHeadClass p:first-child{
      margin: 0 !important;
      padding: 0 !important;
  }
</style>
<style type="text/css" media="print">
  @page {
  margin: 15px;
  }
  table {
    font-size : 13px;
  }
  @media print {
    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
  }
</style>
<!-- .css -->
<!-- .box-body -->
<div class="box-body">
    @php
    $hospital_header_disable_in_prescription = 0;
    $investigation_types = array();
    foreach($data as $inves){
        array_push($investigation_types,strtoupper($inves->investigation_type));
    }
    @endphp

    <div class="col-md-12 no-padding" @if($hospital_header_disable_in_prescription == 1) style="margin-top:3cm;" @endif>
        @include('Emr::emr.investigation.investigation_hospital_header')
        <!-----------Laboratory Investigations----------------------------------->
        @if (in_array("LAB", $investigation_types))
            <div class="col-md-12">
                <div class="col-md-12" style="margin:20px; text-align:center;">
                    <h3>LABORATORY INVESTIGATIONS</h3>
                </div>
                <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table" style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">

                        <tr>
                            <td style="width:5%;"><strong>SL#</strong></td>
                            <td style="width:35%;"><strong><center>Department</center></strong></td>
                            <td style="width:60%;"><strong><center>Item</center></strong></td>

                        </tr>

                        @foreach($data as $inves)
                            @if(strtoupper($inves->investigation_type) == 'LAB')
                            <tr>
                                <td style="width:5%;"> {{ $loop->iteration }} </td>
                                <td style="width:35%; text-align:left;padding-left:5px;"> {!! $inves->sub_dept_name!!} </td>
                                <td style="width:60%; text-align:left;padding-left:5px;"> {!!$inves->service_desc!!} </td>
                            </tr>
                            @endif
                        @endforeach
                    </table>
            </div>
        @endif

        <!-------------------------------- Radiology Investigation------------------------------------------->
        @if (in_array("RADIOLOGY", $investigation_types))
        <!-- <div class="pagebreak"> </div> -->
        <div class="col-md-12">
            <div class="col-md-12" style="margin:20px; text-align:center;">
                <h3>RADIOLOGY INVESTIGATIONS</h3>
            </div>
            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"  style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">

                    <tr>
                        <td style="width:5%;"><strong>SL#</strong></td>
                        <td style="width:35%;"><strong><center>Department</center></strong></td>
                        <td style="width:60%;"><strong><center>Item</center></strong></td>

                    </tr>

                    @foreach($data as $inves)
                        @if(strtoupper($inves->investigation_type) == 'RADIOLOGY')
                        <tr>
                            <td style="width:5%;"> {{ $loop->iteration }} </td>
                            <td style="width:35%; text-align:left;padding-left:5px;"> {!! $inves->sub_dept_name!!} </td>
                            <td style="width:60%; text-align:left;padding-left:5px;"> {!!$inves->service_desc!!} </td>
                        </tr>
                        @endif
                    @endforeach
                </table>
        </div>
        @endif

        <!-------------------------------- Procedure Investigation------------------------------------------->
        @if (in_array("PROCEDURE", $investigation_types))
        <!-- <div class="pagebreak"> </div> -->
        <div class="col-md-12">
                <div class="col-md-12" style="margin:20px; text-align:center;">
                    <h3>PROCEDURES</h3>
                </div>
                <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"  style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">

                    <tr>
                        <td style="width:5%;"><strong>SL#</strong></td>
                        <td style="width:35%;"><strong><center>Department</center></strong></td>
                        <td style="width:60%;"><strong><center>Item</center></strong></td>

                    </tr>

                    @foreach($data as $inves)
                        @if(strtoupper($inves->investigation_type) == 'PROCEDURE')
                        <tr>
                            <td style="width:5%;"> {{ $loop->iteration }} </td>
                            <td style="width:35%; text-align:left;padding-left:5px;"> {!! $inves->sub_dept_name!!} </td>
                            <td style="width:60%; text-align:left;padding-left:5px;"> {!!$inves->service_desc!!} </td>
                        </tr>
                        @endif
                    @endforeach
                </table>
        </div>
        @endif

        @if(!empty($inv_remarks) || !empty($inv_clinical_history))
        <div class="col-md-12" style="margin-top:40px;">
            <table class="table no-border no-margin theadfix_wrapper table-striped table-condensed styled-table"  style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="20%"><strong>REMARKS</strong></td>
                    <td style="text-align:left;padding-left:5px;"> {!! $inv_remarks !!} </td>
                </tr>
                <tr>
                    <td width="20%"><strong>CLINICAL HISTORY</strong></td>
                    <td style="text-align:left;padding-left:5px;"> {!! $inv_clinical_history !!} </td>
                </tr>
            </table>
        </div>
        @endif
    </div>

</div>
<!-- .box-body -->

