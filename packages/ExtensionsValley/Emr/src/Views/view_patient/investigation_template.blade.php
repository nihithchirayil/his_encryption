<input type="hidden" value="lab" class="investigation_type" />
<div class="col-md-7 padding_sm">
    <div class="col-md-12 padding_sm table_box investigation_entering_area">
        <div class="text_head">Investigation</div>
        <div class="clearfix"></div>
        <div class="col-md-12 no-padding investigation_search_area">
            <div class="col-md-5 padding_sm">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-primary bg-brown brown-border investigation_type_btn active" >
                        <input type="radio" name="options" class="options" value="lab" autocomplete="off" checked> Lab
                    </label>
                    <label class="btn btn-primary bg-brown brown-border investigation_type_btn ">
                        <input type="radio" name="options" class="options" value="radiology" autocomplete="off"> Radiology
                    </label>
                    <label class="btn btn-primary bg-brown brown-border investigation_type_btn ">
                        <input type="radio" name="options" class="options" value="procedure" autocomplete="off"> Procedure
                    </label>
                </div>
            </div>
            <div class="col-md-1">
                <button type="button" class="btn btn-blue" title="Add Investigation" id="addNewInvestigation" onclick="addNewInvestigation(this)"><i class="fa fa-star"></i></button>
            </div>

            <div class="col-md-6 padding_sm">
                <input type="text" placeholder="Search items" autocomplete="off" class="form-control bottom-border-text borderless_textbox investigation_item_search_textbox">
            </div>
            
        </div>
        <div class="col-md-4 padding_sm updateInvestigationBtnDiv">
            <button type="button" class="btn btn-sm btn-blue updateInvestigationBtn"><i class="fa fa-save"></i>  Save</button>
            <button type="button" class="btn btn-sm btn-blue canceInvestigationEditBtn"><i class="fa fa-times"></i>  Cancel</button>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm">
            <div class="theadscroll investigation_tree_area">

            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm">
            <div class="text_head">Selected Investigations</div>
            <div class="clearfix"></div>
            <div class="theadscroll investigation_selected_area">
                <table class="table no-margin no-border theadfix_wrapper table-striped  table_sm table-condensed" style="border: 1px solid #CCC;">
                    <tbody class="selected_investigation_table_body">
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div class="col-md-12 no-padding investigation_remarks_area">
        <div class="col-md-6 padding_sm table_box investigation_remarks">
            <div class="text_head">Remarks</div>
            <div class="clearfix"></div>
            <textarea class="form-control investigation_remarks_textarea" ></textarea>
        </div>
        <div class="col-md-6 padding_sm table_box investigation_clinical_history">
            <div class="text_head">Clinical History</div>
            <div class="clearfix"></div>
            <textarea class="form-control investigation_clinical_history_textarea" ></textarea>
        </div>
    </div>
</div>

<div class="col-md-5 padding_sm">
    <div class="col-md-12 padding_sm table_box investigation_chief_complaint_container">
        <div class="text_head">Chief Complaint</div>
        <div class="clearfix"></div>
        <div class="chief_complaint_div theadscroll">
            <div class="chief_complaint"></div>
        </div>
    </div>
    <div class="col-md-12 padding_sm table_box investigation_history_container">
        <div class="text_head">History</div>
        <div class="clearfix"></div>
        <div class="investigation_history_div theadscroll">
            
        </div>
    </div>
</div>

 <!-- investigation List -->
 <div class="investigation-list-div">
    <a style="float: left;" class="close_btn_inv_search">X</a>
    <div class="inv_theadscroll" style="position: relative;">
        <table id="InvestigationTable" class="table table-bordered no-margin table_sm table-striped inv_theadfix_wrapper">
            <tbody id="ListInvestigationSearchData">
            </tbody>
        </table>
    </div>
</div>
<!-- investigation List -->

