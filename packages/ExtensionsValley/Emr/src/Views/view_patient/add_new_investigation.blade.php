<div class="col-md-6 box-body leaf_border">
    <div  class="patient_details_patient_name text-normal" id="addnewinv" ><label  style="margin-top: 2px" for="">Add New Group:</label>
    <input type="text" id="addNewInv" class="form-control leaf_border">
    <button type="button" class="btn btn-warning leaf_border" id="addInvBtn" style="margin-left:3px;" onclick="saveNewInvGroup($('#addNewInv').val().trim(),this)"><i class="fa fa-plus"></i></button>
    </div>
    <div class="col-md-12 padding_sm theadscroll" style="height: 250px;margin-top:10px" id="itemRelativeInv">
       <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed floatThead-table"style="border-collapse: collapse; border: 0px none rgb(128, 128, 128); display: table; margin: 0px; table-layout: fixed; width: 100%;">
        <thead>
<tr class="tableRowHead">
    <th width='10%'>Sl.No.</th>
    <th width='60%'>Group</th>
    <th width='10%'><i class="fa fa-eye"></i></th>
    <th width='10%'><i class="fa fa-trash"></i></th>
</tr>
        </thead>
        <tbody id="addInvTableBody">
        @if (count($group_name) != 0)

            @php
                $i=1;
            @endphp
            @foreach ($group_name as $each)
            <tr  class="activeTr row_class_newInv">
                <td class="td_common_numeric_rules row_count_class_newInv" onclick="itemListInvWise(this,{{ $each->id }})">{{ $i }}.</td>
                <td class="common_td_rules"><div class="input-group" style="margin:0px">
                    <input type="text" class="form-control editInvName" data-oldInvName="{{ $each->group_name }}" id="editInvName" value="{{ $each->group_name }}">
                      <div class="input-group-append"  >
                    <span class="input-group-text" style="margin-left:9px"   id="basic-addon2"><button type="button" title="Change Group Name" id="invEditBtn"   onclick="saveChangedInvName(this,{{ $each->id }})" class="btn btn-blue grpEditBtn"><i  class="fa fa-save"></i></button></span>
                  </div>
                </div></td>
                <td><button type="button" title="Items In Group" id="itemListInvWise" onclick="itemListInvWise(this,{{ $each->id }})" class="btn btn-blue "><i  class="fa fa-eye"></i></button></td>
                <td><button type="button" title="Delete Group" id="InvdeleteBtn" onclick="deleteInvName(this,{{ $each->id }})" class="btn btn-danger"><i  class="fa fa-trash"></i></button></td>
               
                        
            </tr>
            @php $i++; @endphp  

        @endforeach
        @endif
       
        </tbody>
       </table>
    </div>
    </div>

    
    <div class="col-md-6 padding_sm box-body " style="min-height: 293px" id="prescription_list_table_inv">
        <div  class="patient_details_patient_name text-normal" style="box-shadow: 0px 3px  0px 0px;"> </div>

           
       

       
    </div>
