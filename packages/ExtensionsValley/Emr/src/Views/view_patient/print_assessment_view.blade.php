<!-- .css -->
<style>
    #doctorHeadClass p:first-child {
        margin: 0 !important;
        padding: 0 !important;
    }

</style>
<style type="text/css" media="print">
    @page {
        margin: 15px;
    }

    table {
        font-size: 13px;
    }

</style>
<!-- .css -->
<!-- .box-body -->
<div class="">
@if($include_patient_header == 1)
<style >
    .margintinymce {margin-left: 10px !important;}
    </style>
@else
<style>
    .margintinymce {}
    </style>
    @endif

    @php
        $hospital_header_disable_in_prescription = 0;
        $emr_lite_assessment_head = \DB::table('emr_lite_assessment_head')->whereNull('deleted_at')->pluck('name','class_name');
    @endphp
    <!-- .col-md-12 -->
    <div class="col-md-12 margintinymce" @if ($hospital_header_disable_in_prescription == 1) style="margin-top:3cm;" @endif>
        @if ($include_patient_header == 1)
            @if (!empty($hospitalHeader))
                {!! $hospitalHeader !!}
                <hr style="margin-top:0px; margin-bottom:20px; ">
            @endif

            <table style="border-color:rgb(241, 235, 235);" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="7"><strong><u>CLINICAL ASSESSMENT</u></strong></td>
                </tr>
                <tr>
                    <td colspan="7">&nbsp;</td>
                </tr>
                <tr>
                    <td><strong>Patient Name</strong></td>
                    <td align="center">:</td>
                    <td>{{ $patient_details[0]->patient_name }}</td>
                    <td>&nbsp;</td>
                    <td><strong>Age/Gender</strong></td>
                    <td align="center">:</td>
                    <td>
                        {{ $patient_details[0]->age }}/{{ $patient_details[0]->gender }}
                    </td>
                </tr>
                <tr>
                    <td><strong>UHID</strong></td>
                    <td align="center">:</td>
                    <td>{{ $patient_details[0]->uhid }}</td>
                    <td>&nbsp;</td>
                    <td><strong>Doctor</strong></td>
                    <td align="center">:</td>
                    <td>{{ $doctor_name }}</td>
                </tr>
                <tr>
                    <td><strong>Date</strong></td>
                    <td align="center">:</td>
                    <td>{{ Date('M-d-Y', strtotime($date)) }}</td>
                    <td>&nbsp;</td>
                    <td><strong>Speciality</strong></td>
                    <td align="center">:</td>
                    <td>{{ $speciality }}</td>
                </tr>
            </table>
        @endif
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table class="head_name" style="border-collapse:collapse;border-color:rgb(241, 235, 235);" width="100%"
                    @if ($include_patient_header == 1)border="1" @endif cellspacing="0" cellpadding="0">
                    
                    @if (!empty($data_text))
                    @php
                    @endphp
                        @foreach($data_text as $key => $val)
                        @php
                        $head_name = $emr_lite_assessment_head[$key];
                        @endphp
                        @if ($val!="")
                        <tr class="head_name">
                            <td width="25%"><strong>{{ $head_name }}</strong></td>
                            <td><?php echo nl2br($val); ?></td>
                        </tr>
                            
                        @endif
                 
                        @endforeach
                    @endif
                </table>
            </td>
        </tr>
        </table>
    </div>
</div>
<!-- .box-body -->
