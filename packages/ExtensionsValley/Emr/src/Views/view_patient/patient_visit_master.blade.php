@php
$emr_changes_array=json_decode($emr_changes,true);
$notes_template_type = @$emr_changes_array['notes_template'] ? $emr_changes_array['notes_template'] : 0;
$history_combined_view = @$emr_changes_array['CombinedView'] ? $emr_changes_array['CombinedView'] : 0;
@endphp

<div class="col-md-9 no-padding patient_data_list_div" style="height:98vh;">
    <div class="col-md-12 no-padding" style="height:100%;">
        <div class="col-md-12 padding_sm patient_details_inner_div">
            <div class="col-md-1 padding_sm" style="height:60px;">
                <div class="patient_details_patient_image_container text-normal">
                    <img class="img-circle patient_details_patient_image" id="patient_image_url" src="" alt="Patient" />
                    <div class="video_consultation_div">
                        <button class="" title="Video Consultation">
                            <i class="fa fa-video-camera"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-2 padding_sm dashed_border_box">
                <div class="patient_details_patient_name text-normal"> Patient Name
                    <!-- <i id="patient_data_list_btn"
                        style="margin-top: 10px;display: none;cursor: pointer;" onclick="showHidePatientList(2)"
                        class="pull-right text-normal fa fa-bars hover_btn"></i> -->
                </div>
                <div class="patient_details_patient_uhid"> UHID </div>
                <div class="patient_details_patient_age_gender"> Age/Gender </div>
            </div>
            <div class="col-md-3 padding_sm dashed_border_box">
                <div class="patient_details_patient_address"> Address : -- </div>
                <div class="patient_details_patient_place"> Place : -- </div>
                <span class="patient_details_patient_district">District : -- </span>
            </div>
            <div class="col-md-2 padding_sm dashed_border_box">
                <div class="patient_details_patient_company"> Company Name : -- </div>
                <div class="patient_details_patient_pricing"> Pricing Name : -- </div>
                <span class="patient_details_patient_phone">Mobile : -- </span>
            </div>
            <div class="col-md-2 padding_sm dashed_border_box">
                @if($show_patient_reference == '1')
                <div class="patient_details_co_type">C/O Type : -- </div>
                <div class="patient_details_co_name">C/O Name : -- </div>
                <div class="patient_details_co_mobile">C/O Mobile : -- </div>
                @else
                <div class="patient_details_patient_covid_history">Covid History : -- </div>
                <div class="patient_details_patient_vaccine_type">Type of vaccine : -- </div>
                <div class="">&nbsp;</div>
                @endif
            </div>
            <div class="col-md-2 padding_sm dashed_border_box timer_container">
                <label id="minutes">00</label>:<label id="seconds">00</label>
            </div>



        </div>
        <div class="col-md-3 padding_sm patient_details_div">

            <div class="col-md-12 padding_sm">
                <div class="col-md-2 no-padding left_side_tools_container__parent">
                    <div class="left_side_tools_container1">
                        @if(intval($history_combined_view)==1)
                        <a class="expanding_button" onclick="getPatientCombinedHistory()">
                            <span class="expanding_button_icon"><i id="CombinedHistoryBtn" class="fa fa-history"></i></span>
                            <span class="expanding_button_text">Combined History</span>
                        </a>
                        @else
                        <a class="expanding_button patient_clinical_history_btn">
                            <span class="expanding_button_icon"><i class="fa fa-history"></i></span>
                            <span class="expanding_button_text">History</span>
                        </a>
                        @endif
                        <a class="expanding_button patient_special_notes_btn">
                            <span class="expanding_button_icon"><i class="fa fa-book"></i></span>
                            <span class="expanding_button_text">Special Notes</span>
                        </a>
                        <a class="expanding_button patient_personal_notes_btn">
                            <span class="expanding_button_icon"><i class="fa fa-commenting-o"></i></span>
                            <span class="expanding_button_text">Personal Notes</span>
                        </a>
                       
                        <a class="expanding_button patient_lab_results_btn">
                            <span class="expanding_button_icon"><i class="fa fa-flask"></i></span>
                            <span class="expanding_button_text">Lab Results</span>
                        </a>
                        <a class="expanding_button patient_radiology_results_btn">
                            <span class="expanding_button_icon"><i class="fa fa-camera"></i></span>
                            <span class="expanding_button_text">Radiology Results</span>
                        </a>
                        <a class="expanding_button patient_discharge_summary_list_btn">
                            <span class="expanding_button_icon"><i class="fa fa-list"></i></span>
                            <span class="expanding_button_text">Discharge Summary</span>
                        </a>
                        <a class="expanding_button allergy_vital_show_hide_button">
                            <span class="expanding_button_icon"><i class="fa fa-eye"></i></span>
                            <span class="expanding_button_text">Show/Hide Allergy & Vital</span>
                        </a>
                    </div>
                    <div class="left_side_tools_container2">
                        <a class="expanding_button patient_refer_btn">
                            <span class="expanding_button_icon"><i class="fa fa-user-plus"></i></span>
                            <span class="expanding_button_text">Refer Patient</span>
                        </a>
                        <a class="expanding_button patient_transfer_btn">
                            <span class="expanding_button_icon"><i class="fa fa-exchange"></i></span>
                            <span class="expanding_button_text">Transfer Patient</span>
                        </a>
                        <a class="expanding_button patient_documents_btn">
                            <span class="expanding_button_icon"><i class="fa fa-archive"></i></span>
                            <span class="expanding_button_text">Documents</span>
                        </a>
                        <a class="expanding_button patient_discharge_summary_create_btn">
                            <span class="expanding_button_icon"><i class="fa fa-plus"></i></span>
                            <span class="expanding_button_text">Create Summary</span>
                        </a>
                        <a class="expanding_button patient_visit_history_list_btn">
                            <span class="expanding_button_icon"><i class="fa fa-refresh"></i></span>
                            <span class="expanding_button_text">Patient Visit History</span>
                        </a>
                        <a class="expanding_button patient_visit_form_list_btn">
                            <span class="expanding_button_icon"><i class="fa fa-list-alt"></i></span>
                            <span class="expanding_button_text">Patient Form List</span>
                        </a>
                        <a class="expanding_button getDoctorNotesModelBtn" id="getDoctorNotesModelBtn" onclick="getDoctorNotesModel()">
                            <span class="expanding_button_icon"><i class="fa fa fa-sticky-note-o small_bounce"></i></span>
                            <span class="expanding_button_text">Doctor Notes</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-10 padding_sm patient_demographics_container__parent">
                    <div class="patient_demographics_container">
                        <div class="col-md-12 padding_sm allergy_and_notes_div">
                            <div class="col-md-12 no-padding allergy_div" style="height: 120px;">
                                <div class="text_head">Allergies <i class="fa fa-exclamation-circle blink_me allergy_icon_blink" style="display:none;"></i> <i
                                        class="pull-right fa fa-plus-circle btn-blue-sm addAllergies"></i></div>
                                <div class="clearfix"></div>
                                <div class="allergy_list theadscroll" style="position: relative; height: 115px;">

                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 padding_sm vitals_container">
                            <div class="text_head">Vitals
                                <i class="pull-right fa fa-plus-circle btn-blue-sm_sm editVitals"></i>
                                <i class="pull-right fa fa-history btn-yellow-sm_sm historyVitals"></i>
                            </div>
                            <div class="clearfix"></div>
                            <div class="vital_list theadscroll" style="position: relative; height: 250px;">
                                <table class="table no-margin no-border theadfix_wrapper">
                                    <tbody class="vital_list_table_body">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 padding_sm visit_details_div">
            <div class="col-md-12 padding_sm visit_details_inner_div">

                <div class="tab_wrapper first_tab _side">
                    <ul class="tab_list">
                        <li rel="tab_1" class="active">Notes</li>
                        <li rel="tab_2" class="">Investigation</li>
                        <li rel="tab_3" class="">Investigation Result Entry</li>
                        <li rel="tab_4" class="emr_lite_prescription_tab">Prescription</li>

                        @if($dynamic_template_enable_emr_lite == 1)
                        <li rel="tab_5" class="emr_dynamic_template_tab">Assessment</li>
                        @endif
                    </ul>

                    <div class="content_wrapper">
                        <div class="tab_content tab_1" id="dynamic_notes_template">
                            @if(intval($notes_template_type)==2)
                            @include('Emr::view_patient.separateNotesTemplate')
                            @else
                            @include('Emr::view_patient.notes_template')
                            @endif
                        </div>

                        <div class="tab_content tab_2">
                            @include('Emr::view_patient.investigation_template')
                        </div>
                        <div class="tab_content tab_3">
                            @include('Emr::view_patient.investigation_result_entry')
                        </div>

                        <div class="tab_content tab_4">
                            @include('Emr::view_patient.prescription_template')
                        </div>

                        @if($dynamic_template_enable_emr_lite == 1)
                        <div class="tab_content tab_5">
                            @include('Emr::view_patient.dynamic_templates.custom_forms')
                        </div>
                        @endif
                    </div>
                </div>

                <div class="saveButtonDiv">
                    <button class="btn btn-sm saveClinicalDataButton" type="button"><i class="fa fa-save"></i>
                        Save</button>
                    <button class="btn btn-sm saveAndPrintClinicalDataButton" type="button"><i class="fa fa-save"></i>
                        Save & Print</button>
                </div>
            </div>
        </div>
    </div>
</div>
