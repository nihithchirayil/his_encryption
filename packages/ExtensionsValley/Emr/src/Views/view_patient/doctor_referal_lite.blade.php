
<span>Doctor</span>
 {!! Form::select('doctor_ref', $doctorList, '', ['class' => 'form-control','placeholder' => 'Select Doctor','id' => 'doctor_ref']) !!}
<br>
Notes
<textarea style="resize: vertical;min-height: 250px; max-height: 250px; border-radius: 3px; border:1px solid #dddddd; width:100%;" rows="3" cols="20" class="form-control" name ="refer_notes" placeholder=""></textarea>
<input type="hidden" name="doctor_ref_loaded" id="doctor_ref_loaded" value="1">
<script>
$('select[name="doctor_ref"]').select2({
  dropdownParent: $('#referDoctorForm')
});
</script>
