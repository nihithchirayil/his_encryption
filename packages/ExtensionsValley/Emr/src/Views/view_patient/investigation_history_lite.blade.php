@php
$emr_changes=json_decode($emr_changes,true);
$investigation_future_date = @$emr_changes['investigation_future_date'] ? $emr_changes['investigation_future_date'] : 0;
$current_date = @$current_date ? $current_date : '';

@endphp

@foreach ($investigation_details as $investigation_detail)
    @php
        if (count($investigation_detail['investigation_list']) > 0) {
            $invest_date = date('d-M-Y h:i A', strtotime($investigation_detail['investigation_list'][0]['created_at']));
        } else {
            $invest_date = date('d-M-Y h:i A', strtotime($investigation_detail['created_at']));
        }
        
        $doctor_name = $investigation_detail['doctor_name'] ? ucwords(strtolower($investigation_detail['doctor_name'])) : '';
        $bill_converted_class = '';
        $investigation_class_hidden = '';
        $date1 = $invest_date;
        $date2 = now();
        $days = 0;
        $diff = strtotime($date2) - strtotime($date1);
        $days = abs(round($diff / 86400));
        $created_visit_id = !empty($investigation_detail['visit_id']) ? $investigation_detail['visit_id'] : 0;
        $created_doctor_id = \DB::table('op_visits')->where('visit_id', $created_visit_id)->orderBy('id', 'desc')->limit(1)->value('doctor_id');
        if ($investigation_detail['bill_converted_status'] == 1  || ($investigation_detail['bill_converted_status'] == 0 && $days > $history_editable) ) {    
            $bill_converted_class = ' hidden ';
        }
        if ($created_doctor_id != $doctor_id) {
            $bill_converted_class = ' hidden ';
        } 
        if (intval($investigation_detail['head_id']) == 0) {
            $investigation_class_hidden = ' hidden ';
        }      
        
    @endphp

    <div class="investigation_history_item" data-investigation-head-id="{{ $investigation_detail['head_id'] }}">
        <div class="invest_history_head" style="display:flex;">
            <div class="invest_history_head_left">
                <div title="{{ $invest_date }}"><i class="fa fa-calendar"></i> {{ $invest_date }}</div>
                <div class="overflow_text" title="{{ $doctor_name }}"><i class="fa fa-user-md"></i> {{ $doctor_name }}
                </div>
            </div>
            <div class="invest_history_head_right"><i class="fa fa-print printBtn printInvestigationButton {{ $investigation_class_hidden }}"></i><i
                    class="fa fa-edit editBtn editInvestigationBtn {{ $bill_converted_class }}"></i><i
                    class="fa fa-copy copyBtn copyInvestigationBtn"></i><i
                    class="fa fa-trash deleteBtn deleteInvestigationBtn {{ $bill_converted_class }}"></i></div>
        </div>
        <div class="investigation_list">



            <table class="table patientCombinedHistoryTable no-margin  " style="width:100%;">
                <tbody>
                    <tr style="background-color:ebfaef;">
                        <td><b>Test</b></td>
                        <td><b>Description</b></td>
                        <td><b>Value</b></td>
                        <td><b>Normal Range</b></td>
                        <td><b>Remarks</b></td>
                    </tr>
                    @foreach ($investigation_detail['investigation_list'] as $patient_invsestigation)
                        @php
                            $inv_count = count($patient_invsestigation['sub_test_list']);
                            if($investigation_future_date == 1){
                                $service_date = !empty($patient_invsestigation['service_date']) ? $patient_invsestigation['service_date'] : '';
                            }
                        @endphp
                        @if ($inv_count > 0)
                            <tr>
                                <td colspan="5"><b>{{ $patient_invsestigation['service_desc'] }}</b></td>
                            </tr>
                            @foreach ($patient_invsestigation['sub_test_list'] as $invsestigation)
                                @php
                                    $min = '';
                                    $max = '';
                                    $style = '';
                                    $direction = '';
                                    $html_content = '';
                                    $service_desc = $invsestigation['service_desc'];
                                    
                                    $investigation_type = !empty($invsestigation['investigation_type']) ? $invsestigation['investigation_type'] : 0;
                                    $quantity = !empty($invsestigation['quantity']) ? $invsestigation['quantity'] : 0;
                                    $sub_test_name = !empty($invsestigation['detail_description']) ? $invsestigation['detail_description'] : '-';
                                    $actual_result = !empty($invsestigation['actual_result']) ? $invsestigation['actual_result'] : '-';
                                    $normal_range = !empty($invsestigation['normal_range']) ? $invsestigation['normal_range'] : '-';
                                    $remarks = !empty($invsestigation['remarks']) ? $invsestigation['remarks'] : '-';
                                    
                                    if (!empty($invsestigation['min_value']) && is_numeric($invsestigation['actual_result']) && $invsestigation['max_value'] != '-') {
                                        if (!empty($invsestigation['min_value'])) {
                                            $min = $invsestigation['min_value'];
                                        }
                                    
                                        if (!empty($invsestigation['max_value'])) {
                                            $max = $invsestigation['max_value'];
                                        }
                                    
                                        if ($max < $invsestigation['actual_result'] && !empty($invsestigation['actual_result'])) {
                                            $style = 'style=color:red;';
                                            $direction = '&nbsp;<i class="fa fa-arrow-up"></i>';
                                        }
                                    
                                        if ($min > $invsestigation['actual_result'] && !empty($invsestigation['actual_result'])) {
                                            $style = 'style=color:#FF7701;';
                                            $direction = '&nbsp;<i class="fa fa-arrow-down"></i>';
                                        }
                                    }
                                    
                                    if ($invsestigation['report_type'] == 'T') {
                                        $html_content = "<button class='btn bg-primary' type='button' onclick='getLabResultRtf(" . $invsestigation['labresult_id'] . ")'><i class='fa fa-list'></i></button>";
                                    } elseif ($invsestigation['report_type'] == 'N' || $invsestigation['report_type'] == 'F') {
                                        $html_content = "<div class='text-left'" . $style . '>' . $invsestigation['actual_result'] . $direction . '</div>';
                                    } else {
                                        $html_content = '';
                                    }
                                    
                                    $result_finalized = !empty($invsestigation['result_finalized']) ? $invsestigation['result_finalized'] : 0;
                                    if ($result_finalized == 0) {
                                        $html_content = '';
                                    }
                                    $style = '';
                                    $service_date = '';
                                    if($investigation_future_date == 1 && strtotime($service_date) > strtotime($current_date)){
                                    $service_date1 = '('.$service_date.')';
                                    $style = 'style=color:#7383d1;';
                                }  
                                @endphp
                                <tr class="investigation_list_item" data-detail-id="{{ $invsestigation['detail_id'] }}"
                                    data-service-code="{{ $invsestigation['service_code'] }}" data-service-date="{{ $patient_invsestigation['service_date'] }}">
                                    <td></td>
                                    <td>
                                        <div class="text-left text_limit invest_history_item_desc" {{ $style }}>{{ $sub_test_name }}{{ $service_date1 }}
                                        </div>
                                    </td>
                                    <td>{!! $html_content !!}</td>
                                    <td>{!! $normal_range !!}</td>
                                    <td>{!! $remarks !!}</td>
                                </tr>
                            @endforeach
                        @else
                            @php
                                $min = '';
                                $max = '';
                                $style = '';
                                $direction = '';
                                $html_content = '';
                                $service_desc = $patient_invsestigation['service_desc'];
                                
                                $investigation_type = !empty($patient_invsestigation['investigation_type']) ? $patient_invsestigation['investigation_type'] : 0;
                                $quantity = !empty($patient_invsestigation['quantity']) ? $patient_invsestigation['quantity'] : 0;
                                $sub_test_name = !empty($patient_invsestigation['detail_description']) ? $patient_invsestigation['detail_description'] : '-';
                                $actual_result = !empty($patient_invsestigation['actual_result']) ? $patient_invsestigation['actual_result'] : '-';
                                $normal_range = !empty($patient_invsestigation['normal_range']) ? $patient_invsestigation['normal_range'] : '-';
                                $remarks = !empty($patient_invsestigation['remarks']) ? $patient_invsestigation['remarks'] : '-';
                                
                                if (!empty($patient_invsestigation['min_value']) && is_numeric($patient_invsestigation['actual_result']) && $patient_invsestigation['max_value'] != '-') {
                                    if (!empty($patient_invsestigation['min_value'])) {
                                        $min = $patient_invsestigation['min_value'];
                                    }
                                
                                    if (!empty($patient_invsestigation['max_value'])) {
                                        $max = $patient_invsestigation['max_value'];
                                    }
                                
                                    if ($max < $patient_invsestigation['actual_result'] && !empty($patient_invsestigation['actual_result'])) {
                                        $style = 'style=color:red;';
                                        $direction = '&nbsp;<i class="fa fa-arrow-up"></i>';
                                    }
                                
                                    if ($min > $patient_invsestigation['actual_result'] && !empty($patient_invsestigation['actual_result'])) {
                                        $style = 'style=color:#FF7701;';
                                        $direction = '&nbsp;<i class="fa fa-arrow-down"></i>';
                                    }
                                }
                                
                                if ($patient_invsestigation['report_type'] == 'T') {
                                    $html_content = "<button class='btn bg-primary' type='button' onclick='getLabResultRtf(" . $patient_invsestigation['labresult_id'] . ")'><i class='fa fa-list'></i></button>";
                                } elseif ($patient_invsestigation['report_type'] == 'N' || $patient_invsestigation['report_type'] == 'F') {
                                    $html_content = "<div class='text-left'" . $style . '>' . $patient_invsestigation['actual_result'] . $direction . '</div>';
                                } else {
                                    $html_content = '';
                                }
                                
                                $result_finalized = !empty($patient_invsestigation['result_finalized']) ? $patient_invsestigation['result_finalized'] : 0;
                                if ($result_finalized == 0) {
                                    $html_content = '';
                                }
                                                
                            @endphp
                            @php
                                $style = '';
                                $service_date1 = '';
                                if($investigation_future_date == 1 && strtotime($service_date) > strtotime($current_date)){
                                    $service_date1 = '('.$service_date.')';
                                    $style = 'style=color:#7383d1;';
                                }  
                            @endphp
                            <tr class="investigation_list_item"
                                data-detail-id="{{ $patient_invsestigation['detail_id'] }}"
                                data-service-code="{{ $patient_invsestigation['service_code'] }}" data-service-date="{{ $patient_invsestigation['service_date'] }}">
                                <td colspan="2">
                                    <b class="invest_history_item_desc" {{ $style }}>{{ $sub_test_name }}{{ $service_date1 }}</b>
                                </td>
                                <td>{!! $html_content !!}</td>
                                <td>{!! $normal_range !!}</td>
                                <td>{!! $remarks !!}</td>
                            </tr>
                        @endif
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endforeach
