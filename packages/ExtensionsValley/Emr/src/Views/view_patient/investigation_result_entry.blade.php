<div class="col-md-7 padding_sm">
    <div class="col-md-12 padding_sm table_box investigation_results_entering_area">
        <div class="text_head">Investigation Results</div>

        <div class="theadscroll" style="position: relative; height:500px">
            <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed" id="investigation_result_entry_table">
                <thead>
                    <tr class="tableRowHead">
                        <th style="width:20%">Date</th>
                        <th style="width:35%">Investigation</th>
                        <th style="width:40%">Result</th>
                        <th style="width:5%">
                            <i onclick="addNewInvResultEntry();" class="fa fa-plus"></i>
                        </th>
                    </tr>
                </thead>
                <tbody id="investigation_result_entry_tbody">
                    {{-- <tr>
                        <td>
                            <input type="textbox" name="investiagation_entry_date[]" id="investiagation_entry_date_1" class="form-control bottom-border-text borderless_textbox datepicker"/>
                        </td>
                        <td>
                            <input type="textbox" name="investiagation_entry[]" id="investiagation_entry_1" class="form-control bottom-border-text borderless_textbox"/>
                        </td>
                        <td>
                            <input type="textbox" name="investiagation_entry_result[]" id="investiagation_entry_result_1" class="form-control bottom-border-text borderless_textbox"/>
                        </td>
                        <td>
                            <i onclick="removeInvResultEntry(this)" class="fa fa-times red"></i>
                        </td>
                    </tr> --}}
                </tbody>
            </table>
        </div>

    </div>
</div>

{{-- history area --}}
<div class="col-md-5" id="inv_res_entry_his_container">

</div>
