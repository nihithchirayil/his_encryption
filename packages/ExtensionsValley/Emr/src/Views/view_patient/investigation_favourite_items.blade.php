<ul class="tree investigation_group_tree">
    @foreach($investigation_groups as $key => $investigation_group)
    <li> <input type="checkbox" class="tree_checkbox parent_group_item" value="{{$investigation_group['group_id']}}" /> {{ucfirst($investigation_group['group_name'])}}
        <ul style="list-style: none;">
            @foreach($investigation_group['service_list'] as $key1 => $service_item)
            <li> <input type="checkbox" class="tree_checkbox child_group_item" data-service-name="{{ucfirst($service_item['service_desc'])}}" data-sub-dept-name="{{$service_item['sub_dept_name']}}" value="{{$service_item['service_code']}}" /> {{ucfirst($service_item['service_desc'])}}  </li>
            @endforeach
        </ul>
    </li>
    @endforeach
</ul>