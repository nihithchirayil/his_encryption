<div class="modal fade" id="uploadEyeImagesModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 500px;width: 100%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getPatientVitalModelHeader">Upload Image</h4>
            </div>
            <div class="modal-body" id="uploadEyeImagesModelDiv">
                <form id="uploadEyeImageDocumentData">
                    <div class="col-md-7 padding_sm" style="margin-top:-1px;">
                        <input style="height:25px !important" type="file" class="form-control btn bg-blue"
                            name="upload_eyemarkdocument" id="upload_eyemarkdocument">
                    </div>
                    <div class="col-md-5 padding_sm">
                        <button id="uploadDocumentBtn" class="btn btn-success btn-block" type="submit"
                            title="Upload">Upload <i id="uploadDocumentSpin" class="fa fa-upload"></i></button>
                    </div>
                    <div class="col-md-12 padding_sm" style="margin-top: 15px">
                        <div class="col-md-2 padding_sm pull-right">
                            <button style="padding: 3px 3px" id="saveEyeImageDataBtn" class="btn btn-block btn-blue"
                                type="submit"><i id="saveEyeImageDataSpin" class="fa fa-save"></i>
                                Save</button>
                        </div>
                        <div class="col-md-2 padding_sm pull-right">
                            <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                                class="btn btn-danger btn-block">Close <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="investigationDateTimePickerModel" tabindex="-1" role="dialog" style=" z-index:99999; display:none;">
    <div class="modal-dialog" style="width: 40%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="investigationDateTimePickerModelHeader"></h4>
            </div>
            <div class="modal-body" style="min-height: 75px;">
                <div class="col-md-12">
                    <div class="mate-input-box">
                        <label class="filter_label">Service Date</label>
                        <input type="hidden" value="" id="invest_cnt_hiddden">
                        <input type="text" autocomplete="off" value="" class="form-control datepicker"
                            id="investigationServiceDate">
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px; margin-top: 5px;" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="getPatientVitalModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 600px;width: 100%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getPatientVitalModelHeader"><span class="getPatientVitalModelHeader"></span></h4>
            </div>
            <div class="modal-body" id="getPatientVitalModelDiv">
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" id="savePatientVitalsBtn" class="btn btn-blue" type="button"
                    onclick="savePatientVitals()"><i id="savePatientVitalsSpin" class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="getPatientAllegryModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1100px;width: 100%">
        <div class="modal-content">
            <div class="modal-header modal_box" style="height:37px;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getPatientAllegryModelHeader">NA</h4>
            </div>
            <div class="modal-body" id="getPatientAllegryModelDiv">
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" id="savePatientAllergyBtn" class="btn btn-blue" type="button"
                    onclick="savePatientAllergy()"><i id="savePatientAllergySpin" class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="addItemtoFavouriteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" id="addfovouritesmodal" style="max-width: 1000px;width: 100%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="addItemtoFavouriteModelHeader">NA</h4>
            </div>
            <div class="modal-body" style="min-height: 300px" id="addItemtoFavouriteModelDiv">
            </div>
            <div class="modal-footer" style="margin-top: 10px;">

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="patientClinicalHistoryModal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 85%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="patientClinicalHistoryModalHeader">History</h4>
            </div>
            <div class="modal-body" style="min-height: 400px;" id="patientClinicalHistoryModalBody">
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px; margin-top: 5px;" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div id="vital_graph_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 1300px; width: 100%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg" style="height:37px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Vital History</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box no-border no-margin">
                            <div class="box-body clearfix">
                                <div class="col-md-12">
                                    <div class="form-group control-required col-md-3 padding_sm">
                                        <label for="lstVitalItems">Filter By Vitals </label>
                                        <select class="form-control " name="lstVitalItems" id="lstVitalItems">
                                            <option value="">Fiter By Vitals</option>
                                            <option value="5">BP Systolic/ BP Diastolic</option>
                                            <option value="6">BP Diastolic / BP Systolic</option>
                                            <option value="7">Pulse / Min</option>
                                            <option value="8">Respiration / Min</option>
                                            <option value="9">Temperature F </option>
                                            <option value="12">Oxygen Saturation %</option>
                                            <option value="2">Weight in Kg</option>
                                            <option value="4">Height in CM</option>
                                            <option value="17">BMI Kg/M^2</option>
                                            <option value="20">Spirometry</option>
                                        </select>
                                    </div>
                                    <div class="form-group  control-required col-md-3 padding_sm">
                                        <label for="vital_graph_from_date">From Date</label>
                                        <input id="vital_graph_from_date" value="" class="form-control datepicker"
                                            name="vital_graph_from_date" type="text">
                                    </div>
                                    <div class="form-group  control-required col-md-3 padding_sm">
                                        <label for="vital_graph_to_date">To Date</label>
                                        <input id="vital_graph_to_date" value="" class="form-control datepicker"
                                            name="vital_graph_to_date" type="text">
                                    </div>
                                    <div class="form-group  control-required col-md-2 padding_sm">
                                        <label>&nbsp;</label><br>
                                        <button class="btn btn-block save_btn_bg" onclick="show_vital_history();"><i
                                                class="fa fa-check"></i> Submit</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12">
                        <div class="col-md-3 padding_sm">
                            <div class="theadscroll" style="position: relative; height: 400px;">
                                <table
                                    class="table table-bordered no-margin table_sm table-striped theadfix_wrapper vital_his_table">
                                    <thead>
                                        <tr class="table_header_bg">
                                            <th>Vitals</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>
                                    <tbody id="vital_his_table">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-9 padding_sm">
                            <div class="box no-margin no-border">
                                <div class="box-body">
                                    <div id="vital_graph"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>



<div class="modal fade" id="vitalGraphIndividualModal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 88%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Vital Graph</h4>
            </div>
            <div class="modal-body" style="min-height: 450px;" id="vitalGraphIndividualModalBody">
                <div class="box no-margin no-border">
                    <div class="box-body">
                        <div id="vital_graph_individual"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="patientLabResultsModal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 88%">
        <div class="modal-content">
            <div class="modal-header modal_box" style="height:37px;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" style="height: 37px;">Lab Results</h4>
            </div>
            <div class="modal-body" style="min-height: 450px;" id="patientLabResultsModalBody">
                <div class="row" id="lab_restuls_data">

                </div>
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="patientPersonalNotesModal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog modal-sm" style="width: 60%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Personal Notes</h4>
            </div>
            <div class="modal-body" style="min-height: 250px;" id="patientPersonalNotesModalBody">
                <div class="col-md-12 no-padding notes_div" style="height: 100px;">
                    <div class="clearfix"></div>
                    <div class="private_notes_list">
                        <textarea class="form-control private_notes_textarea" rows="5"
                            style="height: 200px !important; width: 100%; resize: none;"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button onclick="savePatientPersonalNotes();" class="btn btn-sm btn-blue savePatientPersonalNotesBtn">
                    <i class="fa fa-save" aria-hidden="true"></i> Save
                </button>
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="discharge_summary_list_modal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Discharge Summary List</h4>
            </div>
            <div class="modal-body" style="min-height: 250px;" id="discharge_summary_list_modal_body">

            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="discharge_summary_view_modal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Discharge Summary View</h4>
            </div>
            <div class="modal-body" style="min-height: 250px; max-height: 450px;"
                id="discharge_summary_view_modal_body">

            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="special_notes_modal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Special Notes</h4>
            </div>
            <div class="modal-body" style="min-height: 250px; max-height: 450px;" id="special_notes_modal_body">

            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="radiology_results_modal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Radiology Results</h4>
            </div>
            <div class="modal-body" style="min-height: 250px; max-height: 450px;" id="radiology_results_modal_body">
                <div class="col-md-12">
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Service Description</label>
                            <div class="clearfix"></div>
                            <select id="search_service_id" class="form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" id="radiology_results_data">

                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div id="radiology_pacs_viewer_iframe_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:97%">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="modal-title"> Radiology Image Viewer</h4>
            </div>
            <div class="modal-body no-padding" style="min-height:560px;">
                <div class="col-md-12" style="margin-top:10px">
                    <iframe src="" title="PACS Viewer" id="pacs_viewer_iframe"
                        style="width:100%; height: 550px;"></iframe>
                </div>
            </div>
        </div>

    </div>
</div>



<!----Referring Doctor---------->

<div class="modal bs-example-modal-md mkModalFloat" id="referDoctor" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Refer Patient</h4>
            </div>
            <form id="referDoctorForm" name="referDoctorForm" method="POST">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn save_btn_bg save_class" id="refer_save_btn"
                        onclick="saveReferDoctor()" data-dismiss="modal">Refer</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!----Referring Doctor Ends---------->


<div class="modal bs-example-modal-md mkModalFloat" id="transferDoctor" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Transfer Patient</h4>
            </div>
            <form id="transferDoctorForm" name="transferDoctorForm" method="POST">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn save_btn_bg save_class" id="transfer_save_btn"
                        onclick="saveTransferPatient()" data-dismiss="modal">Transfer</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!----manage Docs---------->

<div class="modal bs-example-modal-md mkModalFloat" id="manageDocs" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Manage Document</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    @php
                    $document_types = \DB::table('document_types')
                    ->WhereNull('deleted_at')
                    ->Where('status', 1)
                    ->OrderBy('name', 'DESC')
                    ->pluck('name', 'id');
                    @endphp

                    {!! Form::open(['id' => 'manageDocsForm', 'name' => 'manageDocsForm', 'method' => 'post', 'file' =>
                    'true', 'enctype' => 'multipart/form-data']) !!}
                    @php $i = 1; @endphp

                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <div class="doc-type-hidden" style="display: none;">
                        {!! Form::select('document_type_hidden', $document_types, null, ['class' => 'form-control',
                        'placeholder' => 'Select Type']) !!}
                    </div>

                    <div class="remove_00 col-sm-12 no-padding visit-action-button">
                        <div class="col-md-3 padding_sm">
                            {!! Form::select('document_type[]', $document_types, old('document_type'), ['class' =>
                            'form-control document_type_upload', 'placeholder' => 'Select Type', 'required' =>
                            'required']) !!}
                        </div>

                        <div class="col-md-3 padding_sm">
                            <input type="text" name="description[]" class="form-control description_upload"
                                id="description" placeholder="Description">
                        </div>

                        <div class="col-md-3 padding_sm">
                            @php $f = $i - 1; @endphp
                            <input name="file_upload[]" type="file" id="f_upload"
                                class="inputfile inputfile-1 hidden document_upload" data-show-upload="false"
                                data-multiple-caption="{count} files selected" />
                            <label class="no-margin btn btn-block custom-uploadfile_label" for="f_upload"
                                style="background: #21a4f1; color: white;">
                                <i class="fa fa-folder"></i>
                                <span id="uploadspan_documentsdocumenttab">
                                    Choose a file ...
                                </span>
                            </label>
                            <a class="uploadedImg" style="display:none; font-size: 11px;" href="" target="_blank"> </a>
                            <span style="color: #d14;">@if ($errors->first('f_upload.' . $f) != '') {{ 'Upload only
                                doc,docx,pdf,jpeg,jpg,png,gif,bmp and File size < 10MB' }} @endif</span>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <button class="btn btn-block btn-lite" id="doc_file_upload" type="submit">
                                <i class="fa fa-upload"></i> Upload
                            </button>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <button type="button" class="btn btn-block btn-lite" onclick="clear_upload_file();"><i
                                    class="fa fa-trash-o"></i> Clear</button>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="ht5"></div>

                    <div class="col-md-12 no-padding">
                        <div class="theadscroll sm_margin_bottom" id="roww" style="max-height: 160px; height: auto;">
                        </div>
                    </div>

                    @php $i++; @endphp

                    {!! Form::token() !!}
                    {!! Form::close() !!}

                </div>

                <hr>

                <div class="row" id="document-data-list">
                    @include('Emr::emr.document.ajax_document_pagination')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>



<div id="view_document_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:93%">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="document_data" style="height:550px">

                <iframe src="" width="100%" height="100%" id="myframe"></iframe>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

</div>

<!----manage Docs Ends---------->


<!-- Prescription Print Config Modal -->
<div id="prescription_print_config_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 30%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Print Config</h4>
            </div>
            <div class="modal-body" style="height:200px;">
                <div class="col-md-12" id="prescription_print_config_list_data">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                    </div>
                    @php
                    $print_vital_dialog_config = @$print_vital_dialog_config ? $print_vital_dialog_config : 0;
                    @endphp
                    @if (intval($print_vital_dialog_config) == 1)
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="print_vital_check"
                            id="print_vital_check">
                        Vitals
                    </div>
                    @endif
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="print_assessment_check"
                            id="print_assessment_check">
                        Clinical Assessment
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="print_investigation_check"
                            id="print_investigation_check">
                        Investigation
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="print_prescription_check"
                            id="print_prescription_check" checked="checked">
                        Prescription
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="printPrescriptionWithInvestigation();"
                            id="print_prescription_and_investigation_btn" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Prescription Print Config Modal -->


<!-- Other Allergy save Modal -->
<div id="other_allergy_save_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 40%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title other_allergy_description">Allergy</h4>
            </div>
            <div class="modal-body" style="height:150px;">
                <div class="col-md-12">
                    @php
                    $allergy_type = \DB::table('allergy_type')
                    ->WhereNull('deleted_at')
                    ->OrderBy('name', 'ASC')
                    ->pluck('name', 'id');
                    @endphp

                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12 padding_sm">
                        <label for="other_allergy_type">Allergy Type</label>
                        <div class="clearfix"></div>
                        {!! Form::select('other_allergy_type', $allergy_type, null, ['class' => 'form-control',
                        'placeholder' => 'Select Type']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" class="btn btn-blue saveOtherAlleryMasterBtn" type="button"
                    onclick="saveOtherAlleryMaster()"><i class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
</div>
<!-- Other Allergy save Modal -->



<!-- presenting complaints fav -->
<div id="presenting_complaints_fav_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 40%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title other_allergy_description">Favorite List</h4>
            </div>
            <div class="modal-body theadscroll" style="position: relative;height:489px;"
                id="presenting_complaints_modal_data">

            </div>
        </div>
    </div>
</div>
<!--------------------------------------------------->
<div id="lab_results_rtf_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:73%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Lab Result</h4>
            </div>
            <div class="modal-body" id="lab_results_rtf_data" style="height:550px;overflow-y:scroll;">

            </div>

        </div>
    </div>
</div>


<div id="dynamicTemplatePrintPreviewModal" class="modal fade" role="dialog" data-backdrop="static"
    data-keyboard="false">
    <div class="modal-dialog" style="width:75%;margin-left:23%;">
        <div class="modal-content">
            <div class="modal-header" style="background:#267cb9; color:#ffffff;">
                <button type="button" class="close" onclick="closeDynamicTemplateModal();"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Preview</h4>
            </div>
            <div class="modal-body" style="height:650px;">
                <div class="col-md-12" style="margin-bottom:10px;">
                    <button id="btn_printDynamicTemplate" type="button" onclick="printDynamicTemplate();"
                        class="btn btn-sm btn-primary pull-right"><i class="fa fa-print"></i> Print</button>
                    <input type="checkbox" id="chk_show_hospital_header" /> Show Hospital Header
                </div>

                <div class="row theadscroll" id="dynamicTemplatePrintPreviewModalData"
                    style="position:relative;height:620px;">

                </div>
            </div>
        </div>
    </div>
</div>
<!-----------take appointment--------------------------------------->

<div id="appointment_modal" class="modal fade " role="dialog" style="z-index:9999">
    <div class="modal-dialog" style="width:63%">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Take Appointment</h4>
            </div>
            <div class="modal-body" id="next_appointment_list_data">

            </div>
        </div>

    </div>
</div>
<!-----------take appointment ends---------------------------------->
<!-----------lab results trends modal------------------------------>
<div id="modal_lab_result_trends" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 92%;margin-left: 95px;">

        <div class="modal-content">
            <div class="modal-header" style="background:#267cb9; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Lab Result Graph View</h4>
            </div>
            <div class="modal-body" style="height:580px;">
                <div class="row" id="lab_result_trends_data">

                </div>
            </div>
        </div>
    </div>
</div>
<!-----------lab results trends modal ends------------------------->
<div class="modal fade" id="patient_visit_history_modal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal">&times;</button>
     <h4 class="modal-title" style="padding-bottom: 10px;"><i class="fa fa fa-list"></i> Patient Visit History List </h4>
     </div>
      <div class="modal-body" style="height:520px;">

    <div class="col-md-12" style="border: 1px solid #0000003b; border-radius: 10px 2px; padding:10px !important">
    <div class="col-md-3">
     <label class="name-label">Visit Date</label>
     <div class="clearfix"></div>
     <input type="text" class="form-control datepicker" name="visit_date" value="" placeholder="Visit Date" id="visit_date_emr_history" style="height: 30px !important;  margin-top: 3px;" onblur="fetchPatientVisitHistory()">
    </div>
    <div class="col-md-3">
      <label class="name-label">Doctor</label>
      <div class="clearfix"></div>
      @php
      $doctor_arr = \DB::table('doctor_master')->whereNull('deleted_at')->pluck('doctor_name', 'id');
      @endphp
      {!! Form::select('visit_history_doctor_id', array("0"=>"Select Doctor")+$doctor_arr->toArray(), 0, ['class' => 'form-control  select2', 'id' =>'visit_history_doctor_id', 'style' => 'color:#555555; padding:4px 12px;', 'onchange'=> 'fetchPatientVisitHistory();']) !!}
     </div>
     </div>

     <div class="col-md-12 no-padding" id="patient_visit_history_modal_content" style="margin-top: 15px" >
    </div>
     </div>
     </div>
    </div>
    </div>

    <!-- Modal for form list -->
    <div class="modal fade" id="patient_visit_form_modal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="padding-bottom: 10px;"><i class="fa fa fa-list"></i> Patient Form List </h4>
            </div>
            <div class="modal-body" style="height:520px;">
                <div class="col-md-12 theadscroll" id="patient_visit_form_modal_content" style="position: relative; height: 501px;">

                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="patient_visit_form_view_modal" role="dialog">
    <div class="modal-dialog" role="document" style="width: 92%;margin-left: 95px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="padding-bottom: 10px;"><i class="fa fa fa-list"></i> Preview</h4>
            </div>
            <div class="modal-body" style="height:520px;">
                <div class="col-md-12 theadscroll" id="patient_visit_form_view_modal_content" style="position: relative; height: 501px;">

                </div>
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px; margin-top: 5px;" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px; margin-top: 5px;" type="button"  id="preview_form_print_btn" class="btn btn-info">Print <i class="fa fa-print"></i></button>
            </div>

        </div>
    </div>
</div>


<div class="modal fade " id="presc_comman_validate_modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header modal_box">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title" id='presc_comman_validate_modal_header'></h4>
            </div>        
            <div class="modal-body" id="presc_comman_validate_modal_data">
                <div class="theadscroll" style="position: relative; height: 40vh; padding-right:10px;">
                    <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed">
                    <thead>
                    <tr style="background-color: #21a4f1; color:white; ">
                        <th style="width: 5%;">
                            <div class="checkbox checkbox-info inline no-margin" style="margin-top:4px !important">
                                <input type="checkbox" id="check_all_medicine">
                                <label style="padding-left: 2px;" for="check_all_medicine">
                                </label>
                            </div>
                        <th style="width: 30%;">Medicine</th>
                        <th style="width: 20%;">Allergic for</th>
                    </tr>
                    </thead>
                    <tbody id='presc_comman_validate_modal_body'>

                    </tbody>
                    </table>
                </div>            
            </div>
            <div class="modal-footer" style="border-top: 1px solid #21a4f1;">
                <button type="button" onclick="addAllergy_medicine_save()" class="btn btn-info" style="background:#21a4f1 !important; width:100px;">Yes</button>
            </div>
        </div>
    </div>
</div>