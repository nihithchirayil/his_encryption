<style>
    .tr_payment_notdone {
    background: #f2dac5 !important;
}
</style>
<div class="theadscroll" style="position:relative; height:600px;">
    <table class="table no-margin no-border theadfix_wrapper">
        <tbody class="op_patients_list_table_body">
            <?php
                if(count($resultData)!=0){
                    foreach ($resultData as $each) {
                            $class="list_item_tokenunseen_div";
                            // $td_class="tr_token_unseen";
                        if(intval($each->dr_seen_status)==1){
                            $class="list_item_tokenseen_div";
                            // $td_class="tr_token_seen";
                        }
                        $td_class = "";
                        $td_class1 = "";
                        if($each->visit_type == "New"){
                            $td_class="tr_new_patient";
                        }
                    $ShowPatientPaymentNotDoneSpursh = @$ShowPatientPaymentNotDoneSpursh ? $ShowPatientPaymentNotDoneSpursh : 0;
                    if($ShowPatientPaymentNotDoneSpursh == 1){
                        if(($each->paid_status != 1 && $each->net_amount_wo_roundoff != 0)){
                            $td_class1="tr_payment_notdone";
                        }
                    }    
                        
                        $qms_auto_call_enable = 0;
                    if(\DB::table('config_head')->where('name','auto_qms_call_while_patient_select')->exists()){
                        $qms_auto_call_enable = \DB::table('config_head')->where('name','auto_qms_call_while_patient_select')->value('value');
                    }

                        ?>
            <tr title="<?= $each->uhid ?>" class="ip_op_list_item col-md-12 no-padding {{$td_class}} {{$td_class1}}" data-seen-status="{{$each->dr_seen_status}}" data-patient-id="{{$each->patient_id}}" data-booking-id="{{$each->booking_id}}" data-qms-appointment-id="{{$each->qms_appointment_id}}" data-qms-token-no="{{$each->qms_token_no}}" data-qms_auto_call_enable="{{$qms_auto_call_enable}}" data-qms-queue_enable='{{env('QUEUE_ENABLED', '')}}' data-video-consultation="{{$each->is_video_consultation}}">
                @if(env('QUEUE_ENABLED', '') == 1)
                <td class="">
                    <div class=" no-padding" style="margin-left:5px;">
                        <button type="button" class="btn btn-sm btn-success qms_call_button" ><i class="fa fa-phone" style="font-size:10px;"></i></button>
                    </div>
                </td>
                @endif

                <td class="">
                    <div class=" no-padding" style="@if(env('QUEUE_ENABLED', 0) == 0) margin-left: 10px; @endif">
                        <div class="list_item_name" ><?= $each->patient_name ?></div>
                    </div>
                </td>
                <td class="">
                    <div class=" no-padding {{ $class }}">
                        <div class="list_item_token"><?= $each->token_no ?></div>
                    </div>
                </td>

            </tr>
            <?php
                    }
                }else {
                    ?>
            <tr>
                <td colspan="2" style="text-align: center">
                   No result found
                </td>
            </tr>
            <?php
                }
            ?>
        </tbody>
    </table>

</div>
