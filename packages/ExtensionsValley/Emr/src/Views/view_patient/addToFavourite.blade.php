<div class="col-md-4 box-body leaf_border">
    <div  class="patient_details_patient_name text-normal" id="addnewgrp" ><label  style="margin-top: 2px" for="">Add New Group:</label>
    <input type="text" id="addNewGrp" class="form-control leaf_border">
    <button type="button" class="btn btn-warning leaf_border" id="addGrpBtn" style="margin-left:3px;" onclick="saveNewGroup($('#addNewGrp').val().trim(),this)"><i class="fa fa-plus"></i></button>
    </div>
    <div class="col-md-12 padding_sm theadscroll" style="height: 250px;margin-top:10px" id="itemRelativeGrps">
       <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed floatThead-table"style="border-collapse: collapse; border: 0px none rgb(128, 128, 128); display: table; margin: 0px; table-layout: fixed; width: 100%;">
        <thead>
<tr class="tableRowHead">
    <th width='10%'>Sl.No.</th>
    <th width='60%'>Group</th>
    @php $has_margin ="margin-left:7px";@endphp
    @if($from_type !=1)
     @php $has_margin =" " @endphp
    <th width='10%'><i class="fa fa-check"></i></th>
    @endif
    <th width='10%'><i class="fa fa-eye"></i></th>
    <th width='10%'><i class="fa fa-trash"></i></th>
</tr>
        </thead>
        <tbody id="addGrpTableBody">
            @if (count($group_name) != 0)

            @php
                $i=1;
            @endphp
            @foreach ($group_name as $each)
            @if (in_array($each->id, $group_id))
            <tr  class="activeTr row_class_newGrp">
                <td class="td_common_numeric_rules row_count_class_newGrp" onclick="itemListGrpWise(this,{{ $each->id }})">{{ $i }}.</td>
                <td class="common_td_rules"><div class="input-group" style="margin:0px">
                    <input type="text" class="form-control editGrpName" data-oldGrpName="{{ $each->group_name }}" id="editGrpName" value="{{ $each->group_name }}">
                      <div class="input-group-append"  >
                    <span class="input-group-text"  style="{{ $has_margin }}"  id="basic-addon2"><button type="button" title="Change Group Name" id="grpEditBtn"   onclick="saveChangedGrpName(this,{{ $each->id }})" class="btn btn-blue"><i  class="fa fa-save"></i></button></span>
                  </div>
                </div></td>
               @if($from_type !=1)
               <td class="common_td_rules"><div class="checkbox checkbox-brown inline no-margin" >
                <input type="checkbox" checked id="prescription_favgroup{{ $each->id }}" onchange="addRemoveItemFrmGrp(this,'{{ $item_code }}',{{ $each->id }})"
                    type="checkbox" style="margin:0px;">
                    <label style="padding-left: 2px;" for="prescription_favgroup{{ $each->id }}"></label></td>
           
               @endif
                <td><button type="button" title="Items In Group" id="itemListGrpWise" onclick="itemListGrpWise(this,{{ $each->id }})" class="btn btn-blue"><i  class="fa fa-eye"></i></button></td>
                <td><button type="button" title="Delete Group" id="grpdeleteBtn" onclick="deleteGrpName(this,{{ $each->id }})" class="btn btn-danger"><i  class="fa fa-trash"></i></button></td>
               
                        
            </tr>
            @php $i++; @endphp  
            @endif
           
          
        
        @endforeach
        @foreach ($group_name as $each)
        @if (!in_array($each->id, $group_id))
        <tr  class="activeTr row_class_newGrp">
            <td class="td_common_numeric_rules row_count_class_newGrp" onclick="itemListGrpWise(this,{{ $each->id }})">{{ $i }}.</td>
            <td class="common_td_rules" ><div class="input-group " style="margin:0px">
                <input type="text" class="form-control editGrpName" data-oldGrpName="{{ $each->group_name }}" id="editGrpName" value="{{ $each->group_name }}">
                  <div class="input-group-append" >
                <span class="input-group-text" style="{{ $has_margin }}" id="basic-addon2"><button type="button"  title="Change Group Name" id="grpEditBtn"   onclick="saveChangedGrpName(this,{{ $each->id }})" class="btn btn-blue grpEditBtn"><i  class="fa fa-save"></i></button></span>
              </div>
            </div></td>
            @if($from_type !=1)
            <td class="common_td_rules"><div class="checkbox checkbox-brown inline no-margin" >
             <input type="checkbox"  id="prescription_favgroup{{ $each->id }}" onchange="addRemoveItemFrmGrp(this,'{{ $item_code }}',{{ $each->id }})"
                 type="checkbox" style="margin:0px;">
                 <label style="padding-left: 2px;" for="prescription_favgroup{{ $each->id }}"></label></td>
        
            @endif
            <td><button type="button" title="Items In Group" id="itemListGrpWise" onclick="itemListGrpWise(this,{{ $each->id }})" class="btn btn-blue"><i  class="fa fa-eye"></i></button></td>
            <td><button type="button" title="Delete Group" id="" onclick="deleteGrpName(this,{{ $each->id }})" class="btn btn-danger grpdeleteBtn"><i  class="fa fa-trash"></i></button></td>
           
                    
        </tr>
        @php $i++; @endphp 
        @endif
       
      
     
    @endforeach
    @endif
        </tbody>
       </table>
    </div>
    </div>

    
    <div class="col-md-8 padding_sm box-body " style="min-height: 293px" id="prescription_list_table_grp">
        <div  class="patient_details_patient_name text-normal" style="box-shadow: 0px 3px  0px 0px;"> </div>

            <table id="prescriptiondata_table_grp"
            class="table no-margin  table-border-radius-top table-striped  table_sm table-condensed">
            <thead>
                <tr class="tableRowHead">
                    <th width="5%">Sl.No.</th>
                    <th width="25%">Medicine</th>
                    <th width="15%">Freq</th>
                    <th width="15%">Dur</th>
                    <th width="15%">Qty</th>
                    <th width="20%">Dir</th>
                    <th width="5%"><i  onclick="addNewMediceneGrp()" class="fa fa-plus"></i></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
       

       
    </div>

