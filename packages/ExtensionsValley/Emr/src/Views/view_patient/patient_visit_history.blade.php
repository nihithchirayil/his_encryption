{{-- @php
    dd($visit_data);
@endphp --}}
<button class="btn btn-lg btn-warning print_all_visit_history_btn" title="Print" style="float:right;" disabled><i
        class="fa fa-print"></i> Print Selected</button>
<div class="col-md-12 no-padding theadscroll" id="load_data" style="height: 50vh;">
    <table
        class="table table-bordered table-striped no-margin theadfix_wrapper table_sm no-border visit_history_data_table">
        <thead>
            <tr style="background-color: #21a4f1; color:white; ">
                <th style="width: 5%;">Sl.No.</th>
                <th style="width: 5%;"><input type="checkbox" name="visit_check_all"
                        class=" visit_check_all" /></th>
                <th>Visit Date</th>
                <th>Doctor Name</th>
                <th>Print</th>
                </tr>
            </thead>
        <tbody>
            @if (count($visit_data) > 0)
                @foreach ($visit_data as $data)
                    <tr style="cursor: pointer;" class="visit_history_parent_content"
                        data-visit-tr="{{ $data->visit_id }}">
                        <td style="white-space:nowrap; ">{{ $loop->iteration }}</td>
                        <td style="white-space:nowrap; "><input type="checkbox"
                                name="visit_check_item" class="visit_check_item" /></td>
                        <td
                            title="{{ date(\WebConf::getConfig('date_format_web'), strtotime($data->visit_date)) }}"
                            style="white-space:nowrap; ">
                            {{ date(\WebConf::getConfig('date_format_web'), strtotime($data->visit_date)) }}</td>
                        <td title="{{ $data->doctor_name }}">{{ $data->doctor_name }}</td>
                        <td><button class="btn btn-lg btn-primary print_visit_history_btn"
                                data-patient-id="{{ $data->patient_id }}" data-visit-id="{{ $data->visit_id }}"
                                title="Print"><i class="fa fa-print"></i> Print</button></td>
                        </tr>
                    <tr style="cursor: pointer; display:none;" class="visit_history_child_content"
                        id="visit_history_child_content_{{ $data->visit_id }}">
                        <td colspan="5">
                            <div class="col-md-12 padding_sm">
                                <div class="col-md-3 padding_sm visit_history_child_div "
                                    style="display:flex; border-right: 1px solid #e3e3e3;">
                                    <div class="col-md-4"><input type="checkbox"
                                            name="visit_history_child_div_check" class="visit_item_vital_check"
                                            checked="checked" /></div>
                                    <div class="col-md-8" style="margin-top:4px;">Vitals
                                    </div>
                                    </div>
                                <div class="col-md-3 padding_sm visit_history_child_div "
                                    style="display:flex; border-right: 1px solid #e3e3e3;">
                                    <div class="col-md-4"><input type="checkbox"
                                            name="visit_history_child_div_check" class="visit_item_clinical_check"
                                            checked="checked" /></div>
                                    <div class="col-md-8" style="margin-top:4px;">
                                        Clinical Notes</div>
                                    
                                    </div>
                                <div class="col-md-3 padding_sm visit_history_child_div "
                                    style="display:flex; border-right: 1px solid #e3e3e3;">
                                    <div class="col-md-4"><input type="checkbox"
                                            name="visit_history_child_div_check" class="visit_item_investigation_check"
                                            checked="checked" /></div>
                                    <div class="col-md-8" style="margin-top:4px;">
                                        Investigation</div>
                                    
                                    </div>
                                <div class="col-md-3 padding_sm visit_history_child_div "
                                    style="display:flex; ">
                                    <div class="col-md-4"><input type="checkbox"
                                            name="visit_history_child_div_check" class="visit_item_prescription_check"
                                            checked="checked" /></div>
                                    <div class="col-md-8" style="margin-top:4px;">
                                        Prescription</div>
                                    
                                    </div>
                                </div>
                            </td>
                        </tr>
                @endforeach
            @else
                <td colspan="6">No Data Found..!</td>
                
            @endif
            </tbody>
        </table>
</div>
