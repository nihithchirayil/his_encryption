
<div class="col-md-6 padding_sm">
    <div class="col-md-12 padding_sm table_box notes_entering_area">
         <div class="col-md-11 no-padding">
            <div class="text_head dynamic_single_head_text">Chief Complaint</div>
        </div>
        <div class="clearfix"></div>
        <form id="ca-data-form">
            <div class="col-md-12 no-padding notes_chief_complaint_container theadscroll" style="height : 235px !important">
                <!-- <textarea class="form-control notes_chief_complaint_textarea" ></textarea> -->
                @if(isset($dynamic_notes_fields) && count($dynamic_notes_fields) > 0)
                <div class="dynamic_notes tab">
                    @foreach($dynamic_notes_fields as $fields)
                    <button type="button" class="tablinks" data-head-name="{{$fields->class_name}}" data-head-id="{{$fields->head_id}}" onclick="showDynamicAssessmentTab(event, '{{$fields->class_name}}')">{{ $fields->name }}</button>
                    @endforeach
                </div>

                @foreach($dynamic_notes_fields as $fields)
                @if ($fields->type=='dyn_textarea')
                <div id="{{$fields->class_name}}" class="tabcontent" data-element-type="{{$fields->type}}">
                    <textarea class="form-control {{$fields->class_name}}_textarea" ></textarea>
                </div>
                @elseif ($fields->type=='dyn_textbox')
                <div id="{{$fields->class_name}}" class="tabcontent" data-element-type="{{$fields->type}}">
                    <input type="text" class="form-control {{$fields->class_name}}_textarea" style="border-radius: 8px 0px 8px 0px;">

                </div>
                @endif

                @endforeach

                @endif
            </div>
            <div class="favouriteBtnDiv">
                <i class="fa fa-star assessment-fav-button assessmentFavButton"></i>
            </div>
            <div class="updateNotesDiv">
                <button type="button" class="btn btn-sm updateNotesBtn"><i class="fa fa-save"></i> Save</button>
                <button type="button" class="btn btn-sm btn-blue canceAssessmentEditBtn"><i class="fa fa-times"></i> Cancel</button>
            </div>
        </form>

    </div>
    <div class="col-md-12 padding_sm table_box bookmarked_notes_area">
        <div class="text_head">Bookmarks</div>
        <div class="clearfix"></div>
        <div class="notes_bookmarks_area theadscroll" style="height : 210px !important" >
        </div>
    </div>
</div>

<div class="col-md-6 padding_sm">

    <div class="col-md-12 padding_sm table_box patient_todays_visit_container">
        <div class="text_head">Today's Visit</div>
        <div class="clearfix"></div>
        <div class="todays_visit_details_div theadscroll" style="position:relative; height:143px;">

        </div>
    </div>
    <div class="col-md-12 padding_sm table_box patient_history_container">
        <div class="text_head">History</div>
        <div class="clearfix"></div>
        <div class="patient_combined_history_div theadscroll" style="position:relative; height:320px;">

        </div>
    </div>
</div>
