
@if(sizeof($prescription_array)>0)
    @foreach ($prescription_array as $ph)
        @php
        $prescription_no = $ph['prescription_no'];
        $prescription_date = (!empty($ph['prescription_date'])) ? date('d-M-Y h:i A', strtotime($ph['prescription_date'])): '';
        $doctor_name = $ph['doctor_name'];
        @endphp
        <div class="prescription_history_item">
            <div class="presc_history_head" style="display:flex;">
                <div class="presc_history_head_left">
                    <div><i class="fa fa-calendar"></i> {{$prescription_date}}
                </div>
                <div class="overflow_text">
                    <i class="fa fa-user-md"></i> {{$doctor_name}}
                </div> 
            </div>
            <div class="presc_history_head_right">
            </div>
        </div> 
        <div class="prescription_list">
            @foreach ($ph['medicine_list'] as $detail)
                @php
                $item_desc = $detail['item_desc'];
                $frequency = $detail['frequency'];
                $quantity = $detail['quantity'];
                $no_of_days = $detail['no_of_days'];
                $duration_unit_id=''
                @endphp
                
                  @if ($duration_unit_id == 1)
                  @php
                      $duration_unit = 'Days';
                  @endphp
              @elseif($duration_unit_id == 2)
                  @php
                      $duration_unit = 'Week';
                  @endphp
              @elseif($duration_unit_id == 3)
                  @php
                      $duration_unit = 'Month';
                  @endphp
              @else
                  @php
                      $duration_unit = 'Days';
                  @endphp
              @endif
                <span class="presc_history_item_desc">{{$item_desc}}</span>
                <span class="presc_history_frequency"> | {{$frequency}} | </span>
                <span class="presc_history_duration"> x {{$no_of_days}} days</span>
                <div class="clearfix border_dashed_bottom"></div>
            @endforeach
            </div> 
        </div>
    @endforeach
@endif