
<!-- .css -->
<style>
  #doctorHeadClass p:first-child{
      margin: 0 !important;
      padding: 0 !important;
  }
</style>
<style type="text/css" media="print">
  @page {
  margin: 15px;
  }
  table {
    font-size : 13px;
  }
  @media print {
    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
  }
@media print {
  .page-break {
    page-break-before: always;
  }
}
</style>
<!-- .css -->
<!-- .box-body -->
<div class="box-body">
    @if(!empty($res_vitals))
    <div class="col-md-12 no-padding">
        @include('Emr::emr.investigation.investigation_hospital_header')
        
        <div class="col-md-12" style="margin:30px; text-align:center;">
            <h3>VITALS</h3>
        </div>
        <table border="1" style="border-collapse:collapse;border-color:rgb(241, 235, 235);" width="100%" cellspacing="0" cellpadding="0">
        <thead>            
        <tr>
            <td style="width:5%;"><strong></strong></td>
            <td style="width:60%; text-align:left;"><strong>Vitals</strong></td>
            <td style="width:40%; text-align:left;"><strong>Value</strong></td>
        </tr>
        </thead>
        @if(sizeof($res_vitals) > 0)
            @php
                $i =1;
                $time_taken = '';
            @endphp
            @foreach($res_vitals as $data)
                @if($data->time_taken != $time_taken)
                <tr class="head_name">
                    <td colspan="3"><strong> Take at : {{date('M-d-Y h:i A',strtotime($data->time_taken))}}</strong></td>
                </tr>
                    @php
                        $vital_remarks = \DB::table('vital_remarks')
                        ->where('batch_no',$data->batch_no)
                        ->whereNull('deleted_at')
                        ->value('remarks');
                    @endphp
                    @if(!empty($vital_remarks))
                    <tr>
                        <td>
                            Remarks
                        </td>
                        <td>{{$vital_remarks}}</td>
                    </tr>
                    @endif
                @endif
                <tr>
                    <td ></td>
                    <td >{{$data->name}}</td>
                    <td >{{$data->vital_value}}</td>
                </tr>
                @php
                    $time_taken = $data->time_taken;
                @endphp
            @endforeach
            @endif
                </table>
        </div>
        @endif

    @if(!empty($already_entered))
    <div class="col-md-12 no-padding">
        @if(empty($res_vitals))
        @include('Emr::emr.investigation.investigation_hospital_header')
        @endif
        
        <div class="col-md-12" style="margin:30px; text-align:center;">
            <h3>CLINICAL ASSESSMENT</h3>
        </div>
        <table border="1" style="border-collapse:collapse;border-color:rgb(241, 235, 235);" width="100%" cellspacing="0" cellpadding="0">
            @if (!empty($data_text))
            @php
            @endphp
                @foreach($data_text as $key => $val)
                @php
                $head_name = @$already_entered[$key] ? $already_entered[$key] : '';
                @endphp
                @if ($val!="")
                <tr class="head_name">
                    <td width="40%"><strong>{{ $head_name }}</strong></td>
                    <td width="60%"><?php echo nl2br($val); ?></td>
                </tr>                
                @endif
                @endforeach
            @endif
        </table>
    </div>
    @endif

   
    @if(count($investigation_data) > 0)
        @php
        $hospital_header_disable_in_prescription = 0;
        $investigation_types = array();
        foreach($investigation_data as $inves){
            array_push($investigation_types,strtoupper($inves->investigation_type));
        }
        @endphp

        <div class="col-md-12 no-padding" @if($hospital_header_disable_in_prescription == 1) style="margin-top:3cm;" @endif>
            @if(empty($already_entered) && empty($res_vitals))
            @include('Emr::emr.investigation.investigation_hospital_header')
            @endif
            <!-----------Laboratory Investigations----------------------------------->
            @if (in_array("LAB", $investigation_types))
                <div class="col-md-12">
                    <div class="col-md-12" style="margin:20px; text-align:center;">
                        <h3>LABORATORY INVESTIGATIONS</h3>
                    </div>
                    <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table" style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">

                            <tr>
                                <td style="width:5%;"><strong>SL#</strong></td>
                                <td style="width:35%; text-align:left;"><strong>Department</strong></td>
                                <td style="width:60%; text-align:left;"><strong>Item</strong></td>

                            </tr>

                            @foreach($investigation_data as $inves)
                                @if(strtoupper($inves->investigation_type) == 'LAB')
                                <tr>
                                    <td style="width:5%;"> {{ $loop->iteration }} </td>
                                    <td style="width:35%; text-align:left;padding-left:5px;"> {!! $inves->sub_dept_name!!} </td>
                                    <td style="width:60%; text-align:left;padding-left:5px;"> {!!$inves->service_desc!!} </td>
                                </tr>
                                @endif
                            @endforeach
                        </table>
                </div>
            @endif

            <!-------------------------------- Radiology Investigation------------------------------------------->
            @if (in_array("RADIOLOGY", $investigation_types))
            <!-- <div class="pagebreak"> </div> -->
            <div class="col-md-12">
                <div class="col-md-12" style="margin:20px; text-align:center;">
                    <h3>RADIOLOGY INVESTIGATIONS</h3>
                </div>
                <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"  style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">

                        <tr>
                            <td style="width:5%;"><strong>SL#</strong></td>
                            <td style="width:35%; text-align:left;"><strong>Department</strong></td>
                            <td style="width:60%; text-align:left;"><strong>Item</strong></td>

                        </tr>

                        @foreach($investigation_data as $inves)
                            @if(strtoupper($inves->investigation_type) == 'RADIOLOGY')
                            <tr>
                                <td style="width:5%;"> {{ $loop->iteration }} </td>
                                <td style="width:35%; text-align:left;padding-left:5px;"> {!! $inves->sub_dept_name!!} </td>
                                <td style="width:60%; text-align:left;padding-left:5px;"> {!!$inves->service_desc!!} </td>
                            </tr>
                            @endif
                        @endforeach
                    </table>
            </div>
            @endif

            <!-------------------------------- Procedure Investigation------------------------------------------->
            @if (in_array("PROCEDURE", $investigation_types))
            <!-- <div class="pagebreak"> </div> -->
            <div class="col-md-12">
                    <div class="col-md-12" style="margin:20px; text-align:center;">
                        <h3>PROCEDURES</h3>
                    </div>
                    <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"  style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">

                        <tr>
                            <td style="width:5%;"><strong>SL#</strong></td>
                            <td style="width:35%; text-align:left;"><strong>Department</strong></td>
                            <td style="width:60%; text-align:left;"><strong>Item</strong></td>

                        </tr>

                        @foreach($investigation_data as $inves)
                            @if(strtoupper($inves->investigation_type) == 'PROCEDURE')
                            <tr>
                                <td style="width:5%;"> {{ $loop->iteration }} </td>
                                <td style="width:35%; text-align:left;padding-left:5px;"> {!! $inves->sub_dept_name!!} </td>
                                <td style="width:60%; text-align:left;padding-left:5px;"> {!!$inves->service_desc!!} </td>
                            </tr>
                            @endif
                        @endforeach
                    </table>
            </div>
            @endif

            @if(!empty($inv_remarks) || !empty($inv_clinical_history))
            <div class="col-md-12" style="margin-top:40px;">
                <table class="table no-border no-margin theadfix_wrapper table-striped table-condensed styled-table"  style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="20%"><strong>REMARKS</strong></td>
                        <td style="text-align:left;padding-left:5px;"> {!! $inv_remarks !!} </td>
                    </tr>
                    <tr>
                        <td width="20%"><strong>CLINICAL HISTORY</strong></td>
                        <td style="text-align:left;padding-left:5px;"> {!! $inv_clinical_history !!} </td>
                    </tr>
                </table>
            </div>
            @endif
        </div>
    @endif

    @if(count($prescription_data) > 0)
    @if(!empty($res_vitals))
        <div class="page-break"></div>
    @endif
    <div class="col-md-12 no-padding">
        @if(count($investigation_data) == 0 && empty($already_entered) && empty($res_vitals))
        @include('Emr::emr.investigation.investigation_hospital_header')
        @endif
        <div class="col-md-12" style="margin:30px; text-align:center;">
            <h3>PRESCRIPTION DETAILS</h3>
        </div>

        <table border="1" cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; font-size: 12px; margin-top:15px;">
            <tbody>
            <tr>
                <td style="padding: 0px; border-top: none; font-size: 11px;" width="15%" align="left" colspan="2">
                <table width="100%">
                    <tbody>
                    @if($encounterDetails->diagnosis)
                    <tr>
                        <td colspan="5" style="font-size:11px;">
                        <strong> Diagnosis : </strong> <br/>
                        {{$encounterDetails->diagnosis}}
                        </td>
                    </tr>
                    @endif
                    </tbody>
                </table>
                </td>
            </tr>
            <tr>
                @if(!empty($medication_note))
                    <td width="25%" valign="top" style="padding: 20px 5px 5px 5px; font-size: 11px;">
                    <span>
                        {!!$medication_note!!}
                    </span>
                    </td>
                @endif
                <td width="75%" valign="top" style="padding: 20px 5px 5px 5px;">
                    <span style="float:right">
                    @if(!empty($intend_type) && $intend_type == 1)
                        Regular
                    @elseif(!empty($intend_type) && $intend_type == 2)
                        New Admission
                    @elseif(!empty($intend_type) && $intend_type == 3)
                        Emergency
                    @else
                        Discharge
                    @endif
                    </span>
                    <br><br>
                    <table border="1" cellspacing="0" cellpadding="2" width="100%" style="border-collapse: collapse; font-size: 13px;">
                    @if($is_iv_fluid)
                        <tr>
                        <td><strong>SL#</strong></td>
                        <td><strong>Generic Name</strong></td>
                        <td><strong>Drug Name</strong></td>
                        <td><strong>Quantity</strong></td>
                        <td><strong>Rate</strong></td>
                        <td><strong>Remarks</strong></td>
                        </tr>
                    @elseif($consumable_ind == 1)
                        <tr>
                        <td><strong>SL#</strong></td>
                        <td><strong>Drug Name</strong></td>
                        <td><strong>Quantity</strong></td>
                        <td><strong>Remarks</strong></td>
                        </tr>
                    @else
                        <tr>
                        <td><strong>SL#</strong></td>
                        <!-- <td><strong>Generic Name</strong></td> -->
                        <td width="40%"><strong>Drug Name</strong></td>
                        <td><strong>Dose</strong></td>
                        <td><strong>Frequency</strong></td>
                        <td><strong>Route</strong></td>
                        <td><strong>Days</strong></td>
                        <td><strong>Qty</strong></td>
                        <td><strong>Remarks</strong></td>
                        </tr>
                    @endif
                    <?php $i = 1;?>
                    @foreach($prescription_data as $pres)
                    <?php

                    /********* Medication Period **********/
                    $start_date = "";
                    if(!empty($pres->start_date)){
                        $start_date = \Carbon::parse($pres->start_date)->format('d-m-y');
                    }

                    $stop_date = "";
                    if(!empty($pres->stop_date)){
                        $stop_date = \Carbon::parse($pres->stop_date)->format('d-m-y');
                    }
                    /********* Medication Period **********/

                    $medicine = "";
                    if(!empty($pres->medicine_code)){
                        $medicine = $pres->item_desc;
                    }else{
                        $medicine = $pres->medicine_name;
                    }

                    ?>
                    @if($is_iv_fluid)
                    <tr>
                        <td> {{$i}} </td>
                        <td> {{ $pres->generic_name }}</td>
                        <td> {{ $medicine }}</td>
                        <td valign="top"> {!! $pres->quantity!!} </td>
                        <td><center>{!! $pres->iv_rate!!}</center> </td>
                        <td><center>{!! $pres->notes!!} </center></td>
                    </tr>
                    @elseif($consumable_ind == 1)
                    <tr>
                        <td> {{$i}} </td>
                        <td> {{ $medicine }}</td>
                        <td valign="top"> {!! $pres->quantity!!} </td>
                        <td><center>{!! $pres->notes!!} </center></td>
                    </tr>
                    @else
                    <tr>
                        <td> {{$i}} </td>
                        <!-- <td> {{ $pres->generic_name }}</td> -->
                        <td width="40%"> <div>{{ $medicine }}</div><div style="font-size:11px;">Generic : {{ $pres->generic_name }} </div> </td>
                        <td><center> {!! $pres->dose!!} </center></td>
                        <td><center>{!! $pres->frequency!!}</center> </td>
                        <td><center> {!!$pres->route!!}</center> </td>
                        <td><center> {!! $pres->duration!!} </center> </td>
                        <td><center> {!! $pres->quantity!!}</center> </td>
                        <td><center>{!! $pres->notes!!} </center></td>
                    </tr>
                    @endif
                    <?php $i++;?>
                    @endforeach
                    </table>
                    <br>
                    <table width="100%" style="font-size:12px;">
                    <tr>
                        <td width="49%">
                        @if($encounterDetails->reason_for_visit)
                            <strong style="vertical-align:top; float: left;">Reason For Visit</strong>
                            <br/><br/>
                            @if($encounterDetails->reason_for_visit)
                                {!!trim(nl2br($encounterDetails->reason_for_visit))!!}
                            @endif
                            <br/>
                        @endif
                        </td>
                        <td  width="49%">
                        @if(!empty($encounterDetails->special_notes))
                            <strong style="vertical-align:top; float: left;">Special Notes</strong>
                                <br /><br />
                                @if(!empty($encounterDetails->special_notes))
                                {!!trim(nl2br($encounterDetails->special_notes))!!}
                                @endif
                                <br/>
                        @endif
                        </td>
                    </tr>
                </table>
                    <table width="100%" style="font-size:12px;">
                    <tr>
                        <td width="49%">
                        @if(!empty($next_review_date))
                            <strong style="font-size: 11px;">REVIEW ON: </strong> &nbsp;
                            <input value="{{$next_review_date}}" style="border-left: none; border-top: none; border-right: none; border-bottom: none;" type="text">
                        @endif
                        </td>
                        <td  width="49%">
                        @if(!empty($doctor['doctor_name']))
                        <span style="float: right; margin-right: 10px; font-size: 11px;">
                            @if (intval($doctor_name_visible_flag) == 0) 
                            {{$doctor['doctor_name']}}<br/>
                            {{$doctor['qualification']}} ( {{$doctor['doctor_speciality']}} )
                            @endif
                            <br/>
                            <br/>
                            <strong>SIGNATURE:</strong>
                        </span>
                        @endif
                        </td>
                    </tr>
                    </table>
                    <br><br>
                </td>
            </tr>
            @if(!empty($medication_footer_text))
            <tr>
                <td style='padding: 3px; vertical-align: middle;' width='15%' align='center' colspan="2">
                <span style="float: right; margin-right: 180px; font-size: 11px;"><strong>{!! $medication_footer_text !!}</strong></span>
                </td>
            </tr>
            @endif
            </tbody>
        </table>
    </div>
    @endif

</div>
<!-- .box-body -->

