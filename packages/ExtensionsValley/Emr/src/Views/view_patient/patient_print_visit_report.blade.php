@php
require_once(base_path() . "/packages/ExtensionsValley/Master/src/libraries/rtf.php");
@endphp
<style>
  .print-content {
    border: 2px solid #000;
    /* display: inline-block; */
    padding: 10px 5px 50px 5px;
    width: 740px;
  }
  .light_green_bg {
    margin-bottom: 5px;
  }

</style>
<style type="text/css" media="print">
  @media print {
    .page-break {
      page-break-before: always;
    }
    @page {
      border: 2px solid #000;
    }
  }
</style>
<!-- <div class="print-body"> -->
@php
$visit_count = 1;
@endphp
@foreach($visit_data as $id => $data)
  @php
  $patient_details = isset($data['patient_details']) ? $data['patient_details'] : [] ;
  $hospital_header = $data['hospital_header'];
  $clincal_assessments = $data['clincal_assessments'];
  $prescription_details = $data['prescription_details'];
  $vital_details = $data['vital_details'];
  $investigation_details = $data['investigation_details'];
  $assesment_heads = $data['assesment_heads'];
  $direct_billed_labs = $data['lab_results'];
  $page_break = $data['page_break'];

  $gynecology_lite_template_entry = !empty($data['gynecology_lite_template_entry']) ? $data['gynecology_lite_template_entry'] : '';
  $gynecology_obstretics_history = !empty($data['gynecology_obstretics_history']) ? $data['gynecology_obstretics_history'] : '';
  $gynecology_usg_result_entry = !empty($data['gynecology_usg_result_entry']) ? $data['gynecology_usg_result_entry'] : '';

  $antinatal_clinical_data = !empty($data['antinatal_clinical_data']) ? $data['antinatal_clinical_data'] : '' ;
  $antinatal_visit_data = !empty($data['antinatal_visit_data']) ? $data['antinatal_visit_data'] : '';
  @endphp

  <div class="print-content">
    <table class="table table_sm no-margin no-border table-striped" style="width:100%">
      <thead>
        @if($visit_count == 1)
        <tr>
          <td colspan="10" style="padding: 0px;">
            <table style="width: 100%; font-size: 12px;">
              <tbody>
                <tr>
                  <td style="margin-top: 50px;padding: 3px;border: 1px solid #FFF; text-align: center;" align="center" valign="top">
                    {!! $hospital_header !!}
                  </td>
                </tr>
                <tr> <td  style="border-top: 1px solid #000; height: 1px;"></td> </tr>
                <!-- <tr> <td>&nbsp;</td> </tr> -->
              </tbody>
            </table>
          </td>
        </tr>
        @endif
      </thead>
      <tbody style="color: #ccc;">
        @if($visit_count == 1)
        <tr>
          <td colspan="10" style="padding:0 5px;">
            <table width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td width="45%">
                  <table width="100%" cellspacing="0" cellpadding="2">
                    <tr>
                      <td width="10%"><strong>Name</strong></td>
                      <td width="5%" align="center"><strong>:</strong></td>
                      <td width="23%">@if($patient_details['patient_name']) {{ $patient_details['patient_name'] }} @endif</td>
                    </tr>
                    <tr>
                      <td><strong>Age/Gender</strong></td>
                      <td align="center"><strong>:</strong></td>
                      <td>@if($patient_details['age']) {{ $patient_details['age'] }} @endif / @if($patient_details['gender']) {{ $patient_details['gender'] }} @endif</td>
                    </tr>
                    <tr>
                      <td><strong>Phone</strong></td>
                      <td align="center"><strong>:</strong></td>
                      <td>@if($patient_details['phone']) {{ $patient_details['phone'] }} @endif</td>
                    </tr>
                  </table>
                </td>
                <td width="45%">
                  <table width="100%" cellspacing="0" cellpadding="2">
                    <tr>
                      <td width="8%" align="right"><strong>UHID</strong></td>
                      <td width="5%" align="center"><strong>:</strong></td>
                      <td width="26%">@if($patient_details['uhid']) {{ $patient_details['uhid'] }} @endif</td>
                    </tr>
                    <tr>
                      <td align="right"><strong>Allergy</strong></td>
                      <td align="center"><strong>:</strong></td>
                      <td  style="text-align:left;">{{ $patient_details['allergic_history'] }}</td>
                    </tr>
                  </table>
                </td>
                <td width="10%">
                  <!-- <td width="18%" rowspan="3" align="center" valign="middle"><img width="80px" src="qr.jpg"></td> -->
                </td>
              </tr>
            </table>
          </td>
        </tr>
        @endif

        <!-- <tr> <td>&nbsp;</td> </tr> -->
        <tr> <td colspan=12  style="border-top: 1px solid #000; height: 1px;"></td> </tr>
        <!-- <tr> <td>&nbsp;</td> </tr> -->
        <tr style="color: #000;">
          <td width="25%"><strong>Assessment by</strong></td>
          <td width="3%" align="center"><strong>:</strong></td>
          <td width="72%">@if($patient_details['consulting_doctor_name']) {{ $patient_details['consulting_doctor_name'] }} @endif</td>
        </tr>
        <tr style="color: #000;">
          <td width="25%"><strong>Assessment Date & Time</strong></td>
          <td align="center"><strong>:</strong></td>
          <td>@if($patient_details['visit_date']) {{date(\WebConf::getConfig('date_format_web'), strtotime($patient_details['visit_date']))}} @endif {{ $patient_details['visit_time'] }}</td>
        </tr>
        <!-- <tr> <td>&nbsp;</td> </tr> -->
        <tr> <td colspan=12  style="border-top: 1px solid #000; height: 1px;"></td> </tr>
      </tbody>
    </table>

    @if(count($vital_details) > 0)
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
      <tr> <td>&nbsp;</td> </tr>
      <tr>
        <td colspan="7">
          <h4 class="light_green_bg">VITALS</h4>
        </td>
      </tr>
      @foreach($vital_details as $vitals)
          <tr>
            <td colspan=3><strong> @if($vitals->time_taken) {{date(\WebConf::getConfig('datetime_format_web'), strtotime($vitals->time_taken))}} @endif </strong></td>
          </tr>
        @if(isset($vitals->weight) && !empty($vitals->weight))
          <tr>
            <td width="23%">Weight(Kg)</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->weight}}</td>
          </tr>
        @endif
        @if(isset($vitals->height) && !empty($vitals->height))
          <tr>
            <td width="23%">Height(Cm)</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->height}}</td>
          </tr>
        @endif
        @if(isset($vitals->bmi) && !empty($vitals->bmi))
          <tr>
            <td width="23%">BMI</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->bmi}}</td>
          </tr>
        @endif
        @if(isset($vitals->oxygen) && !empty($vitals->oxygen))
          <tr>
            <td width="23%">Oxygen</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->oxygen}}</td>
          </tr>
        @endif
        @if(!empty($vitals->bp_systolic) || !empty($vitals->bp_diastolic))
          <tr>
            <td width="23%">BP</td>
            <td width="5%" align="center">:</td>
            <td width="72%">@if($vitals->bp_systolic) {{$vitals->bp_systolic}} @endif @if($vitals->bp_diastolic) /{{$vitals->bp_diastolic}} @endif</td>
          </tr>
        @endif
        @if(isset($vitals->temperature) && !empty($vitals->temperature))
          <tr>
            <td width="23%">Temperature(F)</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->temperature}}</td>
          </tr>
        @endif
        @if(isset($vitals->pulse) && !empty($vitals->pulse))
          <tr>
            <td width="23%">Pulse</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->pulse}}</td>
          </tr>
        @endif
        @if(isset($vitals->respiration) && !empty($vitals->respiration))
          <tr>
            <td width="23%">Respiration</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->respiration}}</td>
          </tr>
        @endif
        @if(isset($vitals->head_circ) && !empty($vitals->head_circ))
          <tr>
            <td width="23%">Head Circumference</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->head_circ}}</td>
          </tr>
        @endif
        @if(isset($vitals->waist_circ) && !empty($vitals->waist_circ))
          <tr>
            <td width="23%">Waist Circumference</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->waist_circ}}</td>
          </tr>
        @endif
        @if(isset($vitals->hip_circ) && !empty($vitals->hip_circ))
          <tr>
            <td width="23%">Hip Circumference</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->hip_circ}}</td>
          </tr>
        @endif
        @if(isset($vitals->blood_mm) && !empty($vitals->blood_mm))
          <tr>
            <td width="23%">Blood Sugar(mmol/L)</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->blood_mm}}</td>
          </tr>
        @elseif(isset($vitals->blood_mg) && !empty($vitals->blood_mg))
          <tr>
            <td width="23%">Blood Sugar(mg/dL)</td>
            <td width="5%" align="center">:</td>
            <td width="72%">{{$vitals->blood_mg}}</td>
          </tr>
        @else
        @endif

      @endforeach
      <tr> <td>&nbsp;</td> </tr>

    </table>
    @endif
    <!-- @if(isset($clincal_assessments['dynamic_chief_complaints']) && count($clincal_assessments['dynamic_chief_complaints']) > 0)
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
      @php     $chief_complaints = array_unique($clincal_assessments['dynamic_chief_complaints']); @endphp
      @foreach($chief_complaints as $data)
      @if($data != "")
        <tr>
          <td width="90%"><strong> CHIEF COMPLAINTS </strong> : {{$data}}</td>
        </tr>
      @endif
      @endforeach
    </table>
    @endif

    @if(isset($clincal_assessments['dynamic_past_history']) && count($clincal_assessments['dynamic_past_history']) > 0)
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
      @php  $past_history = array_unique($clincal_assessments['dynamic_past_history']); @endphp
      @foreach($past_history as $data)
      @if($data != "")
        <tr>
          <td width="90%"><strong> PAST HISTORY </strong> : {{$data}}</td>
        </tr>
      @endif
      @endforeach
    </table>
    @endif

    @if(isset($clincal_assessments['dynamic_allergic_history']) && count($clincal_assessments['dynamic_allergic_history']) > 0)
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
      @php     $allergic_history = array_unique($clincal_assessments['dynamic_allergic_history']); @endphp
      @foreach($allergic_history as $data)
      @if($data != "")
        <tr>
          <td width="90%"><strong> ALLERGIC HISTORY </strong> : {{$data}}</td>
        </tr>
        @endif
      @endforeach
    </table>
    @endif

    @if(isset($clincal_assessments['dynamic_diagnosis']) && count($clincal_assessments['dynamic_diagnosis']) > 0)
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
      @php     $diagnosis = array_unique($clincal_assessments['dynamic_diagnosis']); @endphp
      @foreach($diagnosis as $data)
      @if($data != "")
        <tr>
          <td width="90%"><strong> DIAGNOSIS </strong> : {{$data}}</td>
        </tr>
      @endif
      @endforeach
    </table>
    @endif

    @if(isset($clincal_assessments['dynamic_physical_examination']) && count($clincal_assessments['dynamic_physical_examination']) > 0)
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
      @php     $physical_examination = array_unique($clincal_assessments['dynamic_physical_examination']); @endphp
      @foreach($physical_examination as $data)
      @if($data != "")
        <tr>
          <td width="90%"><strong> PHYSICAL EXAMINATION </strong> : {{$data}}</td>
        </tr>
      @endif
      @endforeach
    </table>
    @endif -->

    @if(isset($clincal_assessments['all_assesments']) && count($clincal_assessments['all_assesments']) > 0)
      @foreach($clincal_assessments['all_assesments'] as $key => $assessment)
      @if (!empty($assessment))
        @php
          $assesment_arr = [];
          foreach ($assesment_heads as $k => $value) {
            if (isset($assessment->$k) && !empty($assessment->$k)) {
              $assesment_arr[$k] = $assessment->$k;
            }
          }
        @endphp
        @if (!empty($assesment_arr))
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
          <tr>
            <td width="90%">
              <h5 class="light_green_bg" style="text-decoration: underline;">{{date('M-d-Y h:i A', strtotime($key))}}</h5>
            </td>
          </tr>
          @foreach($assesment_arr as $k => $data)
            @if($data != "")
            @php
            $assesment_head = !empty($assesment_heads[$k]) ? $assesment_heads[$k] : 'Test';
            @endphp
              <tr>
                <td width="90%" @if($loop->last) style="padding-bottom: 15px !important;" @endif ><strong> {{$assesment_head}} </strong> : {{$data}}</td>
              </tr>

            @endif
          @endforeach
        </table>
        @endif
      @endif
      @endforeach
    @endif


    @if(isset($gynecology_lite_template_entry) && !empty($gynecology_lite_template_entry))
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tbody>
                <tr>
                    <td colspan="4"><b>Gynecology Entry</b></td>
                </tr>
                @for ($m = 0; $m < sizeof($gynecology_lite_template_entry); $m++)
                    @php
                    $created_at=!empty($gynecology_lite_template_entry[$m]->created_at) ?
                    $gynecology_lite_template_entry[$m]->created_at : '';
                    $doctor_name = !empty($gynecology_lite_template_entry[$m]->doctor_name) ?
                    $gynecology_lite_template_entry[$m]->doctor_name : '';
                    $gynec_data = !empty($gynecology_lite_template_entry[$m]) ?
                    $gynecology_lite_template_entry[$m]: '';
                    @endphp

                    @if ($gynec_data != '')
                    <tr>
                        <td>Doctor:</td>
                        <td>{{ $doctor_name }}</td>
                    </tr>
                    <tr>
                        <td>Lmp:</td>
                        <td>
                            @if ($gynec_data->lmp != '')
                            {!! $gynec_data->lmp !!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Pmp:</td>
                        <td>
                            @if ($gynec_data->pmp != '')
                            {!! $gynec_data->pmp !!}
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td>Presenting Complaints:</td>
                        <td>
                            {!! $gynec_data->presenting_complaints !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Local Examinations:</td>
                        <td>
                            {!! $gynec_data->local_examinations !!}
                        </td>
                    </tr>
                    <tr>
                        <td>USG:</td>
                        <td>
                            {!! $gynec_data->usg !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Provisional Diagnosis:</td>
                        <td>
                            {!! $gynec_data->provisional_diagnosis !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Plan Of Care:</td>
                        <td>
                            {!! $gynec_data->plan_of_care !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Follow Up Care:</td>
                        <td>
                            {!! $gynec_data->follow_up_care !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Special Care:</td>
                        <td>
                            {!! $gynec_data->special_care !!}
                        </td>
                    </tr>
                    @endif
                @endfor
            </tbody>
        </table>
    @endif

    @if(isset($gynecology_obstretics_history) && !empty($gynecology_obstretics_history))
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tbody>
                <tr>
                    <td colspan="4"><b>Gynecology Obstretics</b></td>
                </tr>



                @for ($m = 0; $m < sizeof($gynecology_obstretics_history); $m++)
                @php
                    $created_at=!empty($gynecology_obstretics_history[$m]->created_at) ?
                    $gynecology_obstretics_history[$m]->created_at : '';
                    $doctor_name = !empty($gynecology_obstretics_history[$m]->doctor_name) ?
                    $gynecology_obstretics_history[$m]->doctor_name : '';
                @endphp

                @if($gynecology_obstretics_history[$m]->delevery_type1 !='')
                    <tr>
                        <td colspan="2"><b>Child-1</b></td>
                    </tr>
                    <tr>
                        <td>Delivery Type : </td>
                        <td>
                            @if($gynecology_obstretics_history[$m]->delevery_type1 == 1)
                            FTND
                            @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 2)
                            LSCS
                            @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 3)
                            AB
                            @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 4)
                            ECT
                            @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 5)
                            MTP
                            @endif
                        </td>
                    </tr>
                    @if(isset($gynecology_obstretics_history[$m]->gender1))
                    <tr>
                        <td>Gender : </td>
                        <td>
                            @if($gynecology_obstretics_history[$m]->gender1 == 1)
                            Girl
                            @else
                            Boy
                            @endif
                        </td>
                    </tr>
                    @endif
                    @if(isset($gynecology_obstretics_history[$m]->birth_weight1))
                    <tr>
                        <td>Birth Weight : </td>
                        <td>
                            {{$gynecology_obstretics_history[$m]->birth_weight1 }}
                        </td>
                    </tr>
                    @endif
                    @if(isset($gynecology_obstretics_history[$m]->age1))
                    <tr>
                        <td>Age : </td>
                        <td>
                            {{$gynecology_obstretics_history[$m]->age1 }}
                        </td>
                    </tr>
                    @endif
                    @if(isset($gynecology_obstretics_history[$m]->place_of_birth1))
                    <tr>
                        <td>Place of Birth : </td>
                        <td>
                            {{$gynecology_obstretics_history[$m]->place_of_birth1 }}
                        </td>
                    </tr>
                    @endif
                    @if(isset($gynecology_obstretics_history[$m]->any_complications1))
                    <tr>
                        <td>Any Complications : </td>
                        <td>
                            {{$gynecology_obstretics_history[$m]->any_complications1 }}
                        </td>
                    </tr>
                    @endif
                @endif
                @if($gynecology_obstretics_history[$m]->delevery_type2 !='')
                    <tr>
                        <td colspan="2"><b>Child-2</b></td>
                    </tr>
                    <tr>
                        <td>Delivery Type : </td>
                        <td>
                            @if($gynecology_obstretics_history[$m]->delevery_type2 == 1)
                            FTND
                            @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 2)
                            LSCS
                            @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 3)
                            AB
                            @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 4)
                            ECT
                            @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 5)
                            MTP
                            @endif
                        </td>
                    </tr>
                    @if(isset($gynecology_obstretics_history[$m]->gender2))
                    <tr>
                        <td>Gender : </td>
                        <td>
                            @if($gynecology_obstretics_history[$m]->gender2 == 1)
                            Girl
                            @else
                            Boy
                            @endif
                        </td>
                    </tr>
                    @endif
                    @if(isset($gynecology_obstretics_history[$m]->birth_weight2))
                    <tr>
                        <td>Birth Weight : </td>
                        <td>
                            {{$gynecology_obstretics_history[$m]->birth_weight2 }}
                        </td>
                    </tr>
                    @endif
                    @if(isset($gynecology_obstretics_history[$m]->age2))
                    <tr>
                        <td>Age : </td>
                        <td>
                            {{$gynecology_obstretics_history[$m]->age2 }}
                        </td>
                    </tr>
                    @endif
                    @if(isset($gynecology_obstretics_history[$m]->place_of_birth2))
                    <tr>
                        <td>Place of Birth : </td>
                        <td>
                            {{$gynecology_obstretics_history[$m]->place_of_birth2 }}
                        </td>
                    </tr>
                    @endif
                    @if(isset($gynecology_obstretics_history[$m]->any_complications2))
                    <tr>
                        <td>Any Complications : </td>
                        <td>
                            {{$gynecology_obstretics_history[$m]->any_complications2 }}
                        </td>
                    </tr>
                    @endif
                @endif
                @if($gynecology_obstretics_history[$m]->additional_child_information !='')
                    @php
                        $additional_child_information = json_decode($gynecology_obstretics_history[$m]->additional_child_information);
                        // dd($additional_child_information[1]);
                    @endphp

                    @if(sizeof($additional_child_information) > 0)
                        @php
                            $additional_child_count = 3;
                        @endphp
                        @for($n = 0; $n < sizeof($additional_child_information); $n++)

                        <tr>
                            <td colspan="2"><b>Child-{{$additional_child_count}}</b></td>
                        </tr>
                        @if(isset($additional_child_information[$n]->delevery_type))
                        <tr>
                            <td>Delivery Type : </td>
                            <td>
                                @if($additional_child_information[$n]->delevery_type == 1)
                                FTND
                                @elseif($additional_child_information[$n]->delevery_type == 2)
                                LSCS
                                @elseif($additional_child_information[$n]->delevery_type == 3)
                                AB
                                @elseif($additional_child_information[$n]->delevery_type == 4)
                                ECT
                                @elseif($additional_child_information[$n]->delevery_type == 5)
                                MTP
                                @endif
                            </td>
                        </tr>
                        @endif
                        @if(isset($additional_child_information[$n]->gender))
                        <tr>
                            <td>Gender : </td>
                            <td>
                                @if($additional_child_information[$n]->gender == 1)
                                Girl
                                @else
                                Boy
                                @endif
                            </td>
                        </tr>
                        @endif
                        <tr>
                            <td>Birth Weight : </td>
                            <td>
                                {{$additional_child_information[$n]->birth_weight }}
                            </td>
                        </tr>
                        <tr>
                            <td>Age : </td>
                            <td>
                                {{$additional_child_information[$n]->age }}
                            </td>
                        </tr>
                        <tr>
                            <td>Place of Birth : </td>
                            <td>
                                {{$additional_child_information[$n]->place_of_birth }}
                            </td>
                        </tr>
                        <tr>
                            <td>Any Complications : </td>
                            <td>
                                {{$additional_child_information[$n]->any_complications }}
                            </td>
                        </tr>
                        @php
                            $additional_child_count++;
                        @endphp

                        @endfor
                    @endif
                @endif
            @endfor







            </tbody>
        </table>
    @endif

    @if(isset($gynecology_usg_result_entry) && !empty($gynecology_usg_result_entry))
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tbody>
                <tr>
                    <td colspan="4"><b>USG Result Entry</b></td>
                </tr>

                    @for ($m = 0; $m < sizeof($gynecology_usg_result_entry); $m++)
                        @php
                            $created_at=!empty($gynecology_usg_result_entry[$m]->created_at) ?
                            $gynecology_usg_result_entry[$m]->created_at : '';
                            $doctor_name = !empty($gynecology_usg_result_entry[$m]->doctor_name) ?
                            $gynecology_usg_result_entry[$m]->doctor_name : '';
                        @endphp


                        <tr>
                            <td>Usg taken at : </td>
                            <td>
                                @if($gynecology_usg_result_entry[$m]->usg_results_entry_date !='')
                                    {{date('M-d-Y',strtotime($gynecology_usg_result_entry[$m]->usg_results_entry_date))}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>POG : </td>
                            <td>
                                @if($gynecology_usg_result_entry[$m]->pog_wk !='')
                                    {{$gynecology_usg_result_entry[$m]->pog_wk}} Weeks
                                @endif
                            </td>
                        </tr>
                        @if($gynecology_usg_result_entry[$m]->siugs !='')
                            <tr>
                                <td>SIUGS : </td>
                                <td>
                                    {{$gynecology_usg_result_entry[$m]->siugs}} Weeks
                                </td>
                            </tr>
                        @endif

                        @if($gynecology_usg_result_entry[$m]->sliug !='')
                        <tr>
                            <td>SLIUG : </td>
                            <td>
                                {{$gynecology_usg_result_entry[$m]->sliug}} Weeks
                            </td>
                        </tr>
                        @endif

                        @if($gynecology_usg_result_entry[$m]->slf !='')
                        <tr>
                            <td>SLF : </td>
                            <td>
                                {{$gynecology_usg_result_entry[$m]->slf}} Weeks
                            </td>
                        </tr>
                        @endif

                        @if($gynecology_usg_result_entry[$m]->ceph !='')
                        <tr>
                            <td  colspan="2">CEPH </td>

                        </tr>
                        @endif
                        @if($gynecology_usg_result_entry[$m]->breech !='')
                        <tr>
                            <td  colspan="2">BREECH </td>

                        </tr>
                        @endif
                        @if($gynecology_usg_result_entry[$m]->transverse_lie !='')
                        <tr>
                            <td colspan="2">Transverse lie</td>

                        </tr>
                        @endif
                        <tr>
                            <td>AFI : </td>
                            <td>{{$gynecology_usg_result_entry[$m]->afi}}</td>
                        </tr>
                        <tr>
                            <td>EFW : </td>
                            <td>{{$gynecology_usg_result_entry[$m]->efw}}</td>
                        </tr>
                        <tr>
                            <td>EDD </td>
                            <td>{{$gynecology_usg_result_entry[$m]->edd}}</td>
                        </tr>
                        <tr>
                            <td>Placenta </td>
                            <td>{{$gynecology_usg_result_entry[$m]->placenta}}</td>
                        </tr>
                        <tr>
                            <td>NT </td>
                            <td>{{$gynecology_usg_result_entry[$m]->nt}}</td>
                        </tr>
                        <tr>
                            <td>FHR </td>
                            <td>{{$gynecology_usg_result_entry[$m]->fhr}}</td>
                        </tr>
                        <tr>
                            <td>NB </td>
                            <td>{{$gynecology_usg_result_entry[$m]->nb}}</td>
                        </tr>
                    @endfor
            </tbody>
        </table>
    @endif


    @if(isset($antinatal_clinical_data) && !empty($antinatal_clinical_data))
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tbody>
                <tr>
                    <td valign="top">
                        <table class="table patientCombinedHistoryTable no-margin  antinatal_clinical_data_table  " style="margin-bottom:4px;">
                            <tbody>
                                <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                    <td colspan="9"><b>Antenatal Clinical Data</b></td>
                                </tr>
                                <tr>
                                    <td class="label_class">
                                        <span >Parity :</span>
                                    </td>
                                    <td colspan="8" class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->parity))
                                        {{$antinatal_clinical_data[0]->parity}}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label_class">
                                        <span >TT :</span>
                                    </td>
                                    <td  class="label_class">
                                        <label>1st :</label>
                                        @if(isset($antinatal_clinical_data[0]->tt_1) && $antinatal_clinical_data[0]->tt_1 == 1)
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        @endif
                                    </td>
                                    <td  class="label_class">
                                        <label>2nd :</label>
                                        @if(isset($antinatal_clinical_data[0]->tt_2) && $antinatal_clinical_data[0]->tt_2 == 1)
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        @endif
                                    </td>
                                    <td class="label_class">
                                        <span >HB :</span>
                                    </td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->hb))
                                        {{$antinatal_clinical_data[0]->hb}}
                                        @endif
                                    </td>
                                    <td class="label_class">
                                        <span>Ht :</span>
                                    </td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->ht))
                                        {{$antinatal_clinical_data[0]->ht}}
                                        @endif
                                    </td>
                                    <td class="label_class">
                                        <span>H/o Allergy :</span>
                                    </td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->ho_allergy))
                                        {{$antinatal_clinical_data[0]->ho_allergy}}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label_class">
                                        <span >BG</span>
                                        <span  class="value_class">
                                            @if(isset($antinatal_clinical_data[0]->blood_group))
                                            {{$antinatal_clinical_data[0]->blood_group}}
                                            @endif
                                        </span>
                                    </td>
                                    <td class="label_class" colspan="2">
                                        <span >Rh Type</span>
                                        <span  class="value_class">
                                            @if(isset($antinatal_clinical_data[0]->rh_type))
                                            {{$antinatal_clinical_data[0]->rh_type}}
                                            @endif
                                        </span>
                                    </td>
                                    <td class="label_class">
                                        <span >PCV</span>
                                    </td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->pcv))
                                        {{$antinatal_clinical_data[0]->pcv}}
                                        @endif

                                    </td>
                                    <td class="label_class">
                                        <span >MH</span>
                                    </td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->mh))
                                        {{$antinatal_clinical_data[0]->mh}}
                                        @endif

                                    </td>
                                    <td class="label_class" rowspan="5" colspan="2">
                                        <label ><b>Usg Report</b></label><br>
                                        <span  class="value_class">
                                            @if(isset($antinatal_clinical_data[0]->usg_date) && $antinatal_clinical_data[0]->usg_date !='')
                                                {{date('M-d-Y',strtotime($antinatal_clinical_data[0]->usg_date))}}<br>
                                                {{$antinatal_clinical_data[0]->usg}}
                                            @endif
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label_class">
                                    bloodgroup
                                    </td>
                                    <td colspan="2"  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->blood_group))
                                        {{$antinatal_clinical_data[0]->blood_group}}
                                        @endif
                                        @if(isset($antinatal_clinical_data[0]->rh_type))
                                        {{$antinatal_clinical_data[0]->rh_type }}
                                        @endif
                                    </td>
                                    <td class="label_class">
                                        <span >Plt</span>
                                    </td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->plt))
                                        {{$antinatal_clinical_data[0]->plt}}
                                        @endif
                                    </td>
                                    <td class="label_class">
                                        <span >FH</span>
                                    </td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->fh))
                                        {{$antinatal_clinical_data[0]->fh}}
                                        @endif
                                    </td>

                                </tr>
                                <tr>
                                    <td class="label_class">
                                        <span >HIV</span>
                                    </td>
                                    <td colspan="2"  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->hiv) && $antinatal_clinical_data[0]->hiv !='')
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        @endif
                                    </td>
                                    <td class="label_class">
                                        <span >RBS</span>
                                    </td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->rbs))
                                        {{$antinatal_clinical_data[0]->rbs}}
                                        @endif
                                    </td>
                                    <td class="label_class">
                                        <span >CVS</span>
                                    </td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->cvs))
                                        {{$antinatal_clinical_data[0]->cvs}}
                                        @endif
                                    </td>

                                </tr>
                                <tr>
                                    <td class="label_class">
                                        <span >HCV</span>
                                    </td>
                                    <td colspan="2"  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->hcv))
                                        {{$antinatal_clinical_data[0]->hcv}}
                                        @endif
                                    </td>
                                    <td class="label_class">
                                        <span >UrineRIE</span>
                                    </td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->urine_rie))
                                        {{$antinatal_clinical_data[0]->urine_rie}}
                                        @endif
                                    </td>
                                    <td  class="label_class">
                                        <span>Rs</span>
                                    </td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->rs))
                                        {{$antinatal_clinical_data[0]->rs}}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label_class"><span >HBsAg</span></td>
                                    <td colspan="2"  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->hbsag))
                                        {{$antinatal_clinical_data[0]->hbsag}}
                                        @endif
                                    </td>

                                    <td class="label_class"><span >VDRL</span></td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->vdrl))
                                        {{$antinatal_clinical_data[0]->vdrl}}
                                        @endif
                                    </td>

                                    <td class="label_class"><span >TSH</span></td>
                                    <td  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->tsh))
                                        {{$antinatal_clinical_data[0]->tsh}}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label_class" rowspan="2">
                                        <span>Personal History</span>
                                    </td>
                                    <td colspan='2' rowspan="2"  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->vdrl))
                                        {{$antinatal_clinical_data[0]->vdrl}}
                                        @endif
                                    </td>
                                    <td class="label_class" rowspan="2">
                                        <span>Professional Details</span>
                                    </td>
                                    <td colspan='3'>
                                        <span><i>Husband</i></span><br>
                                    </td>
                                    <td colspan='2'>
                                        <span><i>Wife</i></span><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan='3'><span  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->profession_husband))
                                        {{$antinatal_clinical_data[0]->profession_husband}}
                                        <br>
                                        @endif
                                    </span></td>
                                    <td colspan='2'><span  class="value_class">
                                        @if(isset($antinatal_clinical_data[0]->profession_wife))
                                        {{$antinatal_clinical_data[0]->profession_wife}}
                                        @endif
                                    </span></td>
                                </tr>
                                <tr></tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    @endif

    @if(isset($antinatal_visit_data) && !empty($antinatal_visit_data))
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tbody>
                <tr>
                    <td  valign="top">

                        <table class="table patientCombinedHistoryTable no-margin   " style="margin-bottom:4px;">
                            <tbody>


                                <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                    <td colspan="8"><b>Antenatal</b></td>
                                </tr>
                                <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                    <td><b>Date</b></td>
                                    <td><b>GA</b></td>
                                    <td><b>BP</b></td>
                                    <td><b>Wt</b></td>
                                    <td><b>F.Ht</b></td>
                                    <td><b>FHS</b></td>
                                    <td><b>Present</b></td>
                                    <td><b>Remarks</b></td>
                                </tr>
                                @php

                                @endphp

                                @for ($m = 0; $m < sizeof($antinatal_visit_data); $m++)
                                    <tr>
                                        <td>{{ date('M-d-Y', strtotime($antinatal_visit_data[$m]->antinatal_date)) }}
                                        </td>
                                        <td>{{ $antinatal_visit_data[$m]->ga }}</td>
                                        <td>{{ $antinatal_visit_data[$m]->bp }}</td>
                                        <td>{{ $antinatal_visit_data[$m]->wt }}</td>
                                        <td>{{ $antinatal_visit_data[$m]->fht }}</td>
                                        <td>{{ $antinatal_visit_data[$m]->fhs }}</td>
                                        <td>{{ $antinatal_visit_data[$m]->present }}</td>
                                        <td>{{ $antinatal_visit_data[$m]->antinatal_note }}</td>
                                    </tr>
                                @endfor

                            </tbody>
                        </table>

                    </td>
                </tr>
            </tbody>
        </table>
    @endif


    @if(count($investigation_details) > 0 || count($direct_billed_labs) > 0)
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
      <tr> <td>&nbsp;</td> </tr>
      <tr>
        <td width="90%">
          <h4 class="light_green_bg">INVESTIGATION DETAILS</h4>
        </td>
      </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
      <thead>
        <tr>
          <th style="text-align:left; white-space:nowrap; width:5%;"><strong> SL </strong></th>
          <!-- <th style="text-align:left; white-space:nowrap; width:35%;"><strong> Department </strong></th> -->
          <th style="text-align:left; white-space:nowrap; width:40%;"><strong> Item </strong></th>
          <th style="text-align:left; white-space:nowrap; width:30%;"><strong> Result </strong></th>
          <th style="text-align:left; white-space:nowrap; width:25%;"><strong> Normal Value </strong></th>
        </tr>
      </thead>
      <tbody>
        @php
          $index = 0;
          $direct_billed_labs = collect($direct_billed_labs)->groupBy('bill_detail_id');
        @endphp
        @foreach($investigation_details as $k => $data)
          @php
            $lab_results = \DB::table('lab_result_entry')->where('intend_id', $data->id)->whereNull('deleted_at')->select(['sub_test_id', 'actual_result', 'normal_range', 'detail_description', 'report_type'])->get();
            $index += 1;
          @endphp
          <tr>
            <td>{{$index}}</td>
            <!-- <td>{{$data->sub_dept_name}}</td> -->
            <td>{{$data->service_desc}}</td>
            @if(count($lab_results) == 1)
              @php
                if(!empty($lab_results[0]->report_type) && strtoupper($lab_results[0]->report_type) == 'T') {
                  $reader = new \RtfReader();
                  $reader->Parse($lab_results[0]->actual_result);
                  $formatter = new \RtfHtml();
                  if(empty($reader->root)){
                    $actual_result = 'No Data Available';
                  } else{
                    $html_content = $formatter->Format($reader->root);
                    $html_content = iconv(mb_detect_encoding($html_content, mb_detect_order(), true), "utf-8//IGNORE", $html_content);
                    $actual_result = $html_content;
                  }
                } else {
                  $actual_result = $lab_results[0]->actual_result;
                }
              @endphp
            <td style="line-break: anywhere;">{!! $actual_result !!}</td>
            <td>{{$lab_results[0]->normal_range}}</td>
            @endif
          </tr>
          @if(count($lab_results) > 1)
          <tr>
            <td>&nbsp;</td>
            <td colspan="3" style="padding: 0;">
              <table style="border-collapse: collapse; margin-left:25px;" width="90%" cellspacing="0" cellpadding="6" border="0">
                <thead>
                  <tr>
                    <td style="width:5%; text-align:left; white-space:nowrap;"><strong> SL </strong></td>
                    <td style="width:40%; text-align:left; white-space:nowrap;"><strong>Test Name</strong></td>
                    <td style="width:30%; text-align:left; white-space:nowrap;"><strong>Result</strong></td>
                    <td style="width:25%; text-align:left; white-space:nowrap;"><strong>Normal Value</strong></td>
                  </tr>
                </thead>
                @foreach($lab_results as $result )
                  @php
                    if(!empty($result->report_type) && strtoupper($result->report_type) == 'T') {
                      $reader = new \RtfReader();
                      $reader->Parse($result->actual_result);
                      $formatter = new \RtfHtml();
                      if(empty($reader->root)){
                        $actual_result = 'No Data Available';
                      } else{
                        $html_content = $formatter->Format($reader->root);
                        $html_content = iconv(mb_detect_encoding($html_content, mb_detect_order(), true), "utf-8//IGNORE", $html_content);
                        $actual_result = $html_content;
                      }
                    } else {
                      $actual_result = $result->actual_result;
                    }
                  @endphp
                  <tr>
                    <td> {{ $loop->iteration }} </td>
                    <td> {{ $result->detail_description }} </td>
                    <td> {!! $actual_result !!} </td>
                    <td> {{ $result->normal_range }} </td>
                  </tr>
                @endforeach
              </table>
            </td>
          </tr>
          @endif
        @endforeach

        @foreach($direct_billed_labs as $k1 => $res)
          @php
            $index += 1;
          @endphp
          @if(count($res) == 1)
            <tr>
              <td>{{$index}}</td>
              <td>{{$res[0]->service_desc}}</td>
              @php
                if(!empty($res[0]->report_type) && strtoupper($res[0]->report_type) == 'T') {
                  $reader = new \RtfReader();
                  $reader->Parse($res[0]->actual_result);
                  $formatter = new \RtfHtml();
                  if(empty($reader->root)){
                    $actual_result = 'No Data Available';
                  } else{
                    $html_content = $formatter->Format($reader->root);
                    $html_content = iconv(mb_detect_encoding($html_content, mb_detect_order(), true), "utf-8//IGNORE", $html_content);
                    $actual_result = $html_content;
                  }
                } else {
                  $actual_result = $res[0]->actual_result;
                }
              @endphp
              <td style="line-break: anywhere;">{!! $actual_result !!}</td>
              <td>{{$res[0]->normal_range}}</td>
            </tr>
          @endif
          @if(count($res) > 1)
            <tr>
              <td>{{$index}}</td>
              <td>{{$res[0]->service_desc}}</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td colspan="3" style="padding: 0;">
                <table style="border-collapse: collapse; margin-left:25px;" width="90%" cellspacing="0" cellpadding="6" border="0">
                  <thead>
                    <tr>
                      <td style="width:5%; text-align:left; white-space:nowrap;"><strong> SL </strong></td>
                      <td style="width:40%; text-align:left; white-space:nowrap;"><strong>Test Name</strong></td>
                      <td style="width:30%; text-align:left; white-space:nowrap;"><strong>Result</strong></td>
                      <td style="width:25%; text-align:left; white-space:nowrap;"><strong>Normal Value</strong></td>
                    </tr>
                  </thead>
                  @foreach($res as $lab )
                    @php
                      if(!empty($lab->report_type) && strtoupper($lab->report_type) == 'T') {
                        $reader = new \RtfReader();
                        $reader->Parse($lab->actual_result);
                        $formatter = new \RtfHtml();
                        if(empty($reader->root)){
                          $actual_result = 'No Data Available';
                        } else{
                          $html_content = $formatter->Format($reader->root);
                          $html_content = iconv(mb_detect_encoding($html_content, mb_detect_order(), true), "utf-8//IGNORE", $html_content);
                          $actual_result = $html_content;
                        }
                      } else {
                        $actual_result = $lab->actual_result;
                      }
                    @endphp
                    <tr>
                      <td> {{ $loop->iteration }} </td>
                      <td> {{ $lab->detail_description }} </td>
                      <td> {!! $actual_result !!} </td>
                      <td> {{ $lab->normal_range }} </td>
                    </tr>
                  @endforeach
                </table>
              </td>
            </tr>
          @endif
        @endforeach
      </tbody>
    </table>
    @endif

    @if(count($prescription_details) > 0)
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
      <tr> <td>&nbsp;</td> </tr>
      <tr>
        <td width="90%">
          <h4 class="light_green_bg">PRESCRIPTION</h4>
        </td>
      </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
      <tbody>
        @foreach($prescription_details as $k => $data)
        <tr>
            @php $duration = @$data->duration ? $data->duration : ''; @endphp
            <td>{{$k+1}}. {{$data->item_desc}} - {{$data->frequency}} x {{$duration}} {{ucfirst(strtolower($data->duration_name))}}({{ ucfirst(strtolower(@$data->notes ? $data->notes : '')) }})</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    @endif

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr> <td>&nbsp;</td> </tr>
      <tr> <td colspan=12  style="border-top: 1px solid #000; height: 2px;"></td> </tr>
      <tr> <td>&nbsp;</td> </tr>
      <tr>
        <td width="76%">&nbsp;</td>
        <td width="24%"><strong>{{ucwords(strtolower($patient_details['consulting_doctor_name']))}}</strong><br>
        {{ucwords(strtolower($patient_details['speciality']))}} <br>
        </td>
      </tr>
    </table>

  </div>

  @if($page_break == '1' && $visit_count == 1)
  <div class="page-break"></div>
  @endif

  @php
  $visit_count++;
  @endphp
@endforeach
<!-- </div> -->
