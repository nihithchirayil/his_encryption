@php
$emr_lite_assessment_head = \DB::table('emr_lite_assessment_head')
->whereNull('deleted_at')
->pluck('name', 'class_name');
$from_type=@$from_type ? $from_type : 0;
$current_date = @$current_date ? $current_date : '';
$emr_changes = @$emr_changes ? $emr_changes : '';
$patient_clinical_data = @$patient_clinical_data ? $patient_clinical_data : array();

@endphp

<div class="col-md-12 padding_sm" id="history_data_container">
    <!-- @if (intval($from_type) == 2 || intval($from_type) == 7 || intval($from_type)== 3 )

    <div style="" class="pull-right historyPullUpDown" sort-order="desc"><button type="button" class="btn btn-warning"> <i class="fa fa-angle-double-up"></i></button>
    </div>
    @endif -->
    <div class="theadscroll combined_theadscroll always-visible" id="historydatadiv{{ $from_type }}"
    style="{{ $table_inline_style }}">
        @if (intval($from_type) == 7 && !empty($patient_clinical_data))
        <table class="table patientCombinedHistoryTable" style="margin-bottom:4px;">
        @php 
            $allergic_history = @$patient_clinical_data->allergic_history ? $patient_clinical_data->allergic_history : '';
            $obstretic_history = @$patient_clinical_data->obstretic_history ? $patient_clinical_data->obstretic_history : '';
            $family_history = @$patient_clinical_data->family_history ? $patient_clinical_data->family_history : '';
            $medical_and_surgical_history = @$patient_clinical_data->medical_and_surgical_history ? $patient_clinical_data->medical_and_surgical_history : '';
            $nurtitional_and_screening = @@$patient_clinical_data->nurtitional_and_screening ? $patient_clinical_data->nurtitional_and_screening : '';
        @endphp
            <tbody>
                @if (!empty($allergic_history))                    
                    <tr class="border_dashed_bottom" style="padding-left:0px;">
                        <td colspan="4">
                            <b>Allergic History</b>
                            <br><span style="line-height: 0px;"> {{ $allergic_history }}
                             </span>
                        </td>
                    </tr>
                @endif
                @if (!empty($obstretic_history))                 
                <tr class="border_dashed_bottom" style="padding-left:0px;">
                    <td colspan="4">
                        <b>Obstretic History</b>
                        <br><span style="line-height: 0px;">
                        {{ $obstretic_history }} </span>
                    </td>
                </tr>
                @endif
                @if (!empty($family_history))     
                <tr class="border_dashed_bottom" style="padding-left:0px;">
                    <td colspan="4">
                        <b>Family History</b>
                        <br><span style="line-height: 0px;">
                        {{ $family_history }} </span>
                    </td>
                </tr>
                @endif
                @if (!empty($medical_and_surgical_history))     
                <tr class="border_dashed_bottom" style="padding-left:0px;">
                    <td colspan="4">
                        <b>Medical and Surgical History</b>
                        <br><span style="line-height: 0px;">
                            {{ $medical_and_surgical_history }} </span>
                    </td>
                </tr>
                @endif

                @if (!empty($nurtitional_and_screening))  
                <tr class="border_dashed_bottom" style="padding-left:0px;">
                    <td colspan="4">
                        <b>Nurtitional and Screening</b>
                        <br><span style="line-height: 0px;">
                             {{ $nurtitional_and_screening }} </span>
                    </td>
                </tr>
                @endif

            </tbody>
        </table>
        @endif

                @if ($total_count > 0)
                @php
                $i = 1;
                // dd($data);
                @endphp
                @foreach ($data as $datas)

                @php

                $visit_id = $datas->visit_id;
                $class_highlight = '';
                if ($i == 1):
                $class_highlight = 'tr_highlight';
                else:
                $class_highlight = '';
                endif;
                $scroll_to_tab = 'scrollto-' . $i;
                $selected_tab = 'selectedtab-' . $i;

                @endphp
                {{-- hiding ip visit details --}}
                {{-- @if (trim($datas->visit_status) != 'OP')
                @continue
                @endif --}}
                @if (count($datas->patient_vitals) == 0 &&
                count($datas->patient_medication) == 0 &&
                count($datas->patient_invsestigation) == 0 &&
                count($datas->patient_formdata) == 0 &&
                count($datas->emr_lite_doctor_assessment) == 0)
                @continue
                @endif


                @if ($view_type == 2 && date('Y-m-d', strtotime($datas->encounter_date_time)) == date('Y-m-d'))
                @continue
                @endif

                <div class="patient_visit_historyDiv" visit_id= "{{$datas->visit_id}}">
                <table class="table patientCombinedHistoryTable no-border no-margin">
                <tbody>

                @if (
                $datas->is_casuality_visit == 1 &&
                \DB::table('patient_casuality_visit')->where('visit_id', $visit_id)->whereNull('deleted_at')->exists())
                @php
                $visit_id = $datas->visit_id;
                $visit_data = \DB::table('patient_casuality_visit')
                ->where('visit_id', $visit_id)
                ->whereNull('deleted_at')
                ->select('template_print_view', 'created_at', 'doctor_id')
                ->get();
                $encounter_date_time = date('M-d-Y h:i A', strtotime($visit_data[0]->created_at));
                $doctor_name = \DB::table('doctor_master')
                ->where('id', $visit_data[0]->doctor_id)
                ->value('doctor_name');
                @endphp

                <tr id="{{ $scroll_to_tab }}"
                    class="table patientCombinedHistoryTable_header_bg_grey border_dashed_bottom {{ $class_highlight }} {{ $selected_tab }}"
                    style="margin-bottom:5px;">
                    <td class="text-left">
                        <b>{{ $encounter_date_time }}</b>
                    </td>
                </tr>
                @if ($login_doctor_name != $doctor_name)
                <tr class="border_dashed_bottom" style="margin-bottom:5px;">
                    <td class="text-left">
                        <b>{{ $doctor_name }} </b> <span class="badge bg-purple">{{ $visit_status }}</span>
                    </td>
                </tr>
                @endif

                <tr class="border_dashed_bottom" style="margin-bottom:5px;">
                    <td>{!! $visit_data[0]->template_print_view !!}</td>
                </tr>
                @else
                @php
                $doctor_name = !empty($datas->doctor_name) ? $datas->doctor_name : '';
                $visit_status = !empty($datas->visit_status) ? $datas->visit_status : '';
                $encounter_date_time = !empty($datas->encounter_date_time) ?
                ExtensionsValley\Emr\CommonController::getDateFormat($datas->encounter_date_time, 'd-M-Y h:i A') : '';
                $patient_medication = !empty($datas->patient_medication) ? (array) $datas->patient_medication : '';
                $patient_invsestigation = !empty($datas->patient_invsestigation) ? (array)
                $datas->patient_invsestigation : '';
                $patient_formdata = !empty($datas->patient_formdata) ? (array) $datas->patient_formdata : '';
                $patient_vitals = !empty($datas->patient_vitals) ? (array) $datas->patient_vitals : '';
                $gynecology_template = !empty($datas->gynecology_template) ? (array) $datas->gynecology_template : '';
                $gynecology_usg_result = !empty($datas->gynecology_usg_result_entry) ? (array) $datas->gynecology_usg_result_entry : '';
                $gynecology_lite_template_entry = !empty($datas->gynecology_lite_template_entry) ? (array) $datas->gynecology_lite_template_entry : '';

                $gynecology_obstretics_history =  !empty($datas->gynecology_obstretics_history) ? (array) $datas->gynecology_obstretics_history : '';

                $antinatal_template = !empty($datas->antinatal_visit_data) ? (array) $datas->antinatal_visit_data : '';

                $antinatal_clinical_data = !empty($datas->antinatal_clinical_data) ? (array) $datas->antinatal_clinical_data : '';



                $doctor_assessment = !empty($datas->emr_lite_doctor_assessment) ? (array)
                $datas->emr_lite_doctor_assessment : [];

                $ca_count = [];
                $inv_count = [];
                $med_count = [];
                $vitals_count = [];
                $gynec_count = [];
                $gynec_usg_count = [];
                $gynec_lite_template_count = [];
                $gynec_obstretics_count = [];
                $antinatal_count = [];
                $antinatal_clinical_count = [];

                $med_count = !empty($patient_medication) ? sizeof($patient_medication) : 0;
                $inv_count = !empty($patient_invsestigation) ? sizeof($patient_invsestigation) : 0;
                $ca_count = !empty($patient_formdata) ? sizeof($patient_formdata) : 0;
                $assessment_count = !empty($doctor_assessment) ? sizeof($doctor_assessment) : 0;
                $vitals_count = !empty($patient_vitals) ? sizeof($patient_vitals) : 0;
                $gynec_count = !empty($gynecology_template) ? sizeof($gynecology_template) : 0;
                $gynec_usg_count = !empty($gynecology_usg_result) ? sizeof($gynecology_usg_result) : 0;
                $gynec_lite_template_count = !empty($gynecology_lite_template_entry) ? sizeof($gynecology_lite_template_entry) : 0;

                $gynec_obstretics_count = !empty($gynecology_obstretics_history) ? sizeof($gynecology_obstretics_history) : 0;
                $antinatal_count = !empty($antinatal_template) ? sizeof($antinatal_template) : 0;
                $antinatal_clinical_count = !empty($antinatal_clinical_data) ? sizeof($antinatal_clinical_data) : 0;

                @endphp
                <tr class="border_dashed_bottom" id="{{ $scroll_to_tab }}"
                    class="table patientCombinedHistoryTable_header_bg_grey {{ $class_highlight }} {{ $selected_tab }}"
                    style="margin-bottom:5px;">
                    <td class="text-left">
                        <b>{{ $encounter_date_time }}</b>
                    </td>
                </tr>

                @if ($login_doctor_name != $doctor_name)
                <tr class="border_dashed_bottom" style="margin-bottom:5px;">
                    <td class="text-left">
                        <b>{{ $doctor_name }} </b> {{-- <span class="badge bg-purple">{{$visit_status}}</span> --}}
                    </td>
                </tr>
                @endif
                {{-- <tr class="border_dashed_bottom" style="margin-bottom:5px;">
                    <td class="text-left">
                        <span class="badge bg-purple">{{$visit_status}}</span>
                    </td>
                </tr> --}}
                @if ($vitals_count > 0)
                <tr>
                    <td valign="top">
                        <table class="table patientCombinedHistoryTable no-margin   ">
                            <tbody>
                                <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                    <td><b>Vital</b></td>
                                    <td><b>Value</b></td>
                                </tr>

                                @for ($v = 0; $v < $vitals_count; $v++) @php $name=!empty($patient_vitals[$v]->
                                    vital_name) ? $patient_vitals[$v]->vital_name : 0;
                                    $value = !empty($patient_vitals[$v]->vital_value) ? $patient_vitals[$v]->vital_value
                                    : 0;
                                    $min_value = !empty($patient_vitals[$v]->min_value) ? $patient_vitals[$v]->min_value
                                    : 0;
                                    $max_value = !empty($patient_vitals[$v]->max_value) ? $patient_vitals[$v]->max_value
                                    : 0;
                                    @endphp
                                    @if ($patient_vitals[$v]->vital_master_id == 5)
                                    @php $bp_sis = $value @endphp
                                    @elseif($patient_vitals[$v]->vital_master_id == 6)
                                    @php $bp_dia = $value @endphp
                                    @else
                                    <tr>
                                        <td>{{ $name }}</td>
                                        <td>{{ $value }}</td>
                                    </tr>
                                    @endif
                                    @endfor
                                    <tr>
                                        <td>BP</td>
                                        <td>
                                            @if (isset($bp_sis))
                                            {{ $bp_sis }}
                                            @endif / @if (isset($bp_dia))
                                            {{ $bp_dia }}
                                            @endif


                                        </td>
                                    </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
                @endif
                {{-- <tr>
                    <td>
                        <table class="table patientCombinedHistoryTable no-margin    ">
                            <tbody>
                                @if ($ca_count > 0)
                                @for ($n = 0; $n < $ca_count; $n++) @php $name=(!empty($patient_formdata[$n]->name)) ?
                                    $patient_formdata[$n]->name : "";
                                    $form_id = (!empty($patient_formdata[$n]->form_id)) ? $patient_formdata[$n]->form_id
                                    : "";
                                    $data_text = (!empty($patient_formdata[$n]->data_text)) ?
                                    json_decode($patient_formdata[$n]->data_text) : [];
                                    $ca_head_id = (!empty($patient_formdata[$n]->id)) ? $patient_formdata[$n]->id : 0;
                                    $created_at = (!empty($patient_formdata[$n]->created_at)) ? date('d-M-Y h:i A',
                                    strtotime($patient_formdata[$n]->created_at)): '';
                                    $is_doctor = (!empty($patient_formdata[$n]->is_doctor)) ?
                                    $patient_formdata[$n]->is_doctor : 1;
                                    $created_by = (!empty($patient_formdata[$n]->created_by)) ?
                                    $patient_formdata[$n]->created_by : 2167;
                                    @endphp
                                    @php
                                    $result = '';
                                    if(!empty($form_id)){
                                    $result = ExtensionsValley\Emr\FormDataList::getFormData($form_id, $data_text);
                                    }
                                    @endphp
                                    @if (!empty($result))
                                    <tr class="border_dashed_bottom" data-is-doctor="{{$is_doctor }}"
                                        data-created-by="{{$created_by}}" data-user-id="{{$user_id}}"
                                        data-assessment-id="{{$assessment_id}}" data-form-id="{{$form_id}}"
                                        data-head-id="{{$ca_head_id}}" style="background-color:ebfaef;">
                                        <td><i class="fa fa-calendar"></i> {{$created_at}}</b></td>
                                        <td class="text_right"> @if ($view_type != 3) <i
                                                class="fa fa-print printBtn printAssessmentBtn  "></i> @if ($is_doctor
                                            == 1 && $created_by == $user_id && $assessment_id == $form_id)<i
                                                class="fa fa-edit editBtn editAssessmentBtn  "></i><i
                                                class="fa fa-copy copyBtn copyAssessmentBtn"></i><i
                                                class="fa fa-trash deleteBtn deleteAssessmentBtn "></i> @endif @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table class="table patientCombinedHistoryTable  "
                                                style="margin-bottom:4px;">
                                                <tbody>
                                                    @foreach ($result as $key => $data)
                                                    <tr class="border_dashed_bottom" style="padding-left:0px;">
                                                        @php
                                                        $field_label = (!empty($data['field_label'])) ?
                                                        $data['field_label'] : '';
                                                        $res = (!empty($data['result'])) ? $data['result'] : '';
                                                        @endphp
                                                        @if (!empty($res))
                                                        @php
                                                        $res = nl2br($res);
                                                        @endphp
                                                        <td colspan="4">
                                                            <b>{!!$field_label!!}</b>
                                                            <br><span class="form_value_{{$ca_head_id}}"
                                                                style="line-height: 0px;"> {!!$res!!} </span>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    @endif
                                    @endfor
                                    @endif
                            </tbody>
                        </table>
                    </td>

                </tr> --}}


                <tr>
                    <td>
                        <table class="table patientCombinedHistoryTable no-margin    ">
                            <tbody>
                                @if ($assessment_count > 0)
                                @for ($n = 0; $n < $assessment_count; $n++) @php $id=!empty($doctor_assessment[$n]->id)
                                    ? $doctor_assessment[$n]->id : '';
                                    $name = !empty($doctor_assessment[$n]->doctor_name) ?
                                    $doctor_assessment[$n]->doctor_name : '';
                                    $data_text = !empty($doctor_assessment[$n]->data_text) ?
                                    json_decode($doctor_assessment[$n]->data_text) : [];
                                    $created_at = !empty($doctor_assessment[$n]->created_at) ? date('d-M-Y h:i A',
                                    strtotime($doctor_assessment[$n]->created_at)) : '';
                                    $created_by = !empty($doctor_assessment[$n]->created_by) ?
                                    $doctor_assessment[$n]->created_by : 0;
                                    $created_visit_id = !empty($doctor_assessment[$n]->visit_id) ?
                                    $doctor_assessment[$n]->visit_id : 0;

                                    // $created_doctor_id = \DB::table('op_visits')->where('visit_id',
                                    // $created_visit_id)->orderBy('id', 'desc')->limit(1)->value('doctor_id');

                                    $created_doctor_id = \DB::table('emr_lite_doctor_assessment')->where('visit_id',$created_visit_id)->whereNull('deleted_at')->orderBy('id', 'desc')
                                    ->limit(1)->value('doctor_id');

                                    $array_check = array_values((array) $data_text);
                                    $array_check = array_filter($array_check);
                                    @endphp
                                    @if (!empty($data_text) && count($array_check) > 0)
                                    <tr class="border_dashed_bottom" data-head-id="{{ $id }}"
                                        style="background-color:ebfaef;">
                                        <td><i class="fa fa-calendar"></i> {{ $created_at }}</b>
                                            @php
                                            $date1 = $created_at;
                                            $date2=now();
                                            $days=0;
                                            $diff = strtotime($date2) - strtotime($date1);
                                            $days= abs(round($diff / 86400));

                                            @endphp

                                        </td>
                                        <td class="text_right">
                                            @if ($view_type != 0 && $view_type != 3 )
                                            @if ($created_doctor_id == $doctor_id && $days < $history_editable )
                                                    <i class="fa fa-print printBtn printAssessmentBtn"></i>
                                                    <i class="fa fa-edit editBtn editAssessmentBtn"></i>
                                                    <i class="fa fa-copy copyBtn copyAssessmentBtn"></i>
                                                    <i class="fa fa-trash deleteBtn deleteAssessmentBtn"></i>
                                                @endif
                                                @endif
                                        </td>
                                    </tr>
                                    <tr class="border_dashed_bottom" style="padding-left:0px;">
                                        @if ($created_by != $user_id)
                                        <td class="text-left"><i class="fa fa-user-md"></i><b>
                                                {{ $name }} </b></td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td colspan="2">
                                            <table class="table patientCombinedHistoryTable"
                                                style="margin-bottom:4px;">
                                                <tbody>
                                                    @foreach ($data_text as $key => $data)
                                                    @php
                                                    $head_name = $emr_lite_assessment_head[$key];
                                                    @endphp
                                                    @if (!empty($data))
                                                    <tr class="border_dashed_bottom" style="padding-left:0px;">
                                                        <td colspan="4">
                                                            <b>{!! $head_name !!}</b>
                                                            <br><span style="line-height: 0px;">
                                                                <?php echo nl2br($data); ?>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    @endif

                                    @endfor
                                    @endif
                            </tbody>
                        </table>
                    </td>
                </tr>

                @php
                $template_data_id =
                \DB::table('dynamic_template_data')->where('visit_id',$visit_id)->whereNull('deleted_at')->value('id');
                // echo '#####'.$visit_id;
                if(!empty($template_data_id)){
                $template_data=\ExtensionsValley\Emr\EmrController::getDynamicTemplateData($template_data_id);
                }else{
                $template_data = '';
                }
                @endphp
                @if($template_data !='')
                <tr>
                    <td>{!!$template_data!!}</td>
                </tr>
                @endif




        @if ($gynec_count > 0)
        <tr>
            <td valign="top">
                <table class="table patientCombinedHistoryTable no-margin   " style="margin-bottom:4px;">
                    <tbody>

                        <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                            <td colspan="2"><b>Gynecology Details</b></td>
                        </tr>


                        @for ($m = 0; $m < $gynec_count; $m++) @php
                            $created_at=!empty($gynecology_template[$m]->created_at) ?
                            $gynecology_template[$m]->created_at : '';
                            $doctor_name = !empty($gynecology_template[$m]->doctor_name) ?
                            $gynecology_template[$m]->doctor_name : '';
                            $gynec_data = !empty($gynecology_template[$m]->gynec_data) ?
                            $gynecology_template[$m]->gynec_data : '';

                            @endphp
                            @if ($gynec_data != '')
                            <tr>
                                <td>Doctor:</td>
                                <td>{{ $doctor_name }}</td>
                            </tr>
                            <tr>
                                <td>Lmp:</td>
                                <td>
                                    @if ($gynec_data[0]->lmp != '')
                                    {!! $gynec_data[0]->lmp !!}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Pmp:</td>
                                <td>
                                    @if ($gynec_data[0]->pmp != '')
                                    {!! $gynec_data[0]->pmp !!}
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <td>Presenting Complaints:</td>
                                <td>
                                    {!! $gynec_data[0]->presenting_complaints !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Local Examinations:</td>
                                <td>
                                    {!! $gynec_data[0]->local_examinations !!}
                                </td>
                            </tr>
                            <tr>
                                <td>USG:</td>
                                <td>
                                    {!! $gynec_data[0]->usg !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Provisional Diagnosis:</td>
                                <td>
                                    {!! $gynec_data[0]->provisional_diagnosis !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Plan Of Care:</td>
                                <td>
                                    {!! $gynec_data[0]->plan_of_care !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Follow Up Care:</td>
                                <td>
                                    {!! $gynec_data[0]->follow_up_care !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Special Care:</td>
                                <td>
                                    {!! $gynec_data[0]->special_care !!}
                                </td>
                            </tr>
                            @endif
                            @endfor
                    </tbody>
                </table>

            </td>
        </tr>
        @endif

        @if($gynec_lite_template_count > 0)
        <tr>
            <td valign="top">
                <table class="table patientCombinedHistoryTable no-margin   " style="margin-bottom:4px;">
                    <tbody>

                        <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                            <td colspan="2"><b>Gynecology Details</b></td>
                        </tr>

                        @for ($m = 0; $m < $gynec_lite_template_count; $m++)
                            @php
                            $created_at=!empty($gynecology_lite_template_entry[$m]->created_at) ?
                            $gynecology_lite_template_entry[$m]->created_at : '';
                            $doctor_name = !empty($gynecology_lite_template_entry[$m]->doctor_name) ?
                            $gynecology_lite_template_entry[$m]->doctor_name : '';
                            $gynec_data = !empty($gynecology_lite_template_entry[$m]) ?
                            $gynecology_lite_template_entry[$m]: '';
                            @endphp

                            @if ($gynec_data != '')
                            <tr>
                                <td>Doctor:</td>
                                <td>{{ $doctor_name }}</td>
                            </tr>
                            <tr>
                                <td>Lmp:</td>
                                <td>
                                    @if ($gynec_data->lmp != '')
                                    {!! $gynec_data->lmp !!}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Pmp:</td>
                                <td>
                                    @if ($gynec_data->pmp != '')
                                    {!! $gynec_data->pmp !!}
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <td>Presenting Complaints:</td>
                                <td>
                                    {!! $gynec_data->presenting_complaints !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Local Examinations:</td>
                                <td>
                                    {!! $gynec_data->local_examinations !!}
                                </td>
                            </tr>
                            <tr>
                                <td>USG:</td>
                                <td>
                                    {!! $gynec_data->usg !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Provisional Diagnosis:</td>
                                <td>
                                    {!! $gynec_data->provisional_diagnosis !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Plan Of Care:</td>
                                <td>
                                    {!! $gynec_data->plan_of_care !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Follow Up Care:</td>
                                <td>
                                    {!! $gynec_data->follow_up_care !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Special Care:</td>
                                <td>
                                    {!! $gynec_data->special_care !!}
                                </td>
                            </tr>
                            @endif
                        @endfor

                    </tbody>
                </table>
            </td>
        @endif


        @if($gynec_usg_count > 0)
        <tr>
            <td valign="top">
                <table class="table patientCombinedHistoryTable no-margin" style="margin-bottom:4px;width:76%;">
                    <tbody>
                        <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                            <td colspan="2"><b>Gynecology USG Result Entry</b></td>
                        </tr>
                        @for ($m = 0; $m < $gynec_usg_count; $m++)
                            @php
                                $created_at=!empty($gynecology_usg_result[$m]->created_at) ?
                                $gynecology_usg_result[$m]->created_at : '';
                                $doctor_name = !empty($gynecology_usg_result[$m]->doctor_name) ?
                                $gynecology_usg_result[$m]->doctor_name : '';
                            @endphp


                            <tr>
                                <td>Usg taken at : </td>
                                <td>
                                    @if($gynecology_usg_result[$m]->usg_results_entry_date !='')
                                        {{date('M-d-Y',strtotime($gynecology_usg_result[$m]->usg_results_entry_date))}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>POG : </td>
                                <td>
                                    @if($gynecology_usg_result[$m]->pog_wk !='')
                                        {{$gynecology_usg_result[$m]->pog_wk}} Weeks
                                    @endif
                                </td>
                            </tr>
                            @if($gynecology_usg_result[$m]->siugs !='')
                                <tr>
                                    <td>SIUGS : </td>
                                    <td>
                                        {{$gynecology_usg_result[$m]->siugs}} Weeks
                                    </td>
                                </tr>
                            @endif

                            @if($gynecology_usg_result[$m]->sliug !='')
                            <tr>
                                <td>SLIUG : </td>
                                <td>
                                    {{$gynecology_usg_result[$m]->sliug}} Weeks
                                </td>
                            </tr>
                            @endif

                            @if($gynecology_usg_result[$m]->slf !='')
                            <tr>
                                <td>SLF : </td>
                                <td>
                                    {{$gynecology_usg_result[$m]->slf}} Weeks
                                </td>
                            </tr>
                            @endif

                            @if($gynecology_usg_result[$m]->ceph !='')
                            <tr>
                                <td  colspan="2">CEPH </td>

                            </tr>
                            @endif
                            @if($gynecology_usg_result[$m]->breech !='')
                            <tr>
                                <td  colspan="2">BREECH </td>

                            </tr>
                            @endif
                            @if($gynecology_usg_result[$m]->transverse_lie !='')
                            <tr>
                                <td colspan="2">Transverse lie</td>

                            </tr>
                            @endif
                            <tr>
                                <td>AFI : </td>
                                <td>{{$gynecology_usg_result[$m]->afi}}</td>
                            </tr>
                            <tr>
                                <td>EFW : </td>
                                <td>{{$gynecology_usg_result[$m]->efw}}</td>
                            </tr>
                            <tr>
                                <td>EDD </td>
                                <td>{{$gynecology_usg_result[$m]->edd}}</td>
                            </tr>
                            <tr>
                                <td>Placenta </td>
                                <td>{{$gynecology_usg_result[$m]->placenta}}</td>
                            </tr>
                            <tr>
                                <td>NT </td>
                                <td>{{$gynecology_usg_result[$m]->nt}}</td>
                            </tr>
                            <tr>
                                <td>FHR </td>
                                <td>{{$gynecology_usg_result[$m]->fhr}}</td>
                            </tr>
                            <tr>
                                <td>NB </td>
                                <td>{{$gynecology_usg_result[$m]->nb}}</td>
                            </tr>
                        @endfor
                    </tbody>
                </table>
            </td>
        </tr>
        @endif



        @if ($antinatal_count > 0 && !empty($antinatal_template))
            <tr>
                <td valign="top">
                    <table class="table patientCombinedHistoryTable no-margin   " style="margin-bottom:4px;">
                        <tbody>


                            <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                <td colspan="8"><b>Antenatal</b></td>
                            </tr>
                            <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                <td><b>Date</b></td>
                                <td><b>GA</b></td>
                                <td><b>BP</b></td>
                                <td><b>Wt</b></td>
                                <td><b>F.Ht</b></td>
                                <td><b>FHS</b></td>
                                <td><b>Present</b></td>
                                <td><b>Remarks</b></td>
                            </tr>
                            @php

                            @endphp

                            @for ($m = 0; $m < $antinatal_count; $m++)
                                <tr>
                                    <td>{{ date('M-d-Y', strtotime($antinatal_template[$m]->antinatal_data[0]->antinatal_date)) }}
                                    </td>
                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->ga }}</td>
                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->bp }}</td>
                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->wt }}</td>
                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->fht }}</td>
                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->fhs }}</td>
                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->present }}</td>
                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->antinatal_note }}</td>
                                </tr>
                            @endfor

                        </tbody>
                    </table>

                </td>
            </tr>
        @endif


        @if ($antinatal_clinical_count > 0 && !empty($antinatal_clinical_data))
        @php
            $antinatal_clinical_data = $antinatal_clinical_data[0];
        @endphp
        <tr>
            <td valign="top">
                <table class="table patientCombinedHistoryTable no-margin  antinatal_clinical_data_table  " style="margin-bottom:4px;">
                    <tbody>
                        <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                            <td colspan="9"><b>Antenatal Clinical Data</b></td>
                        </tr>


                        <tr>
                            <td class="label_class">
                                <span >Parity :</span>
                            </td>
                            <td colspan="8" class="value_class">
                                {{$antinatal_clinical_data->parity}}
                            </td>
                        </tr>
                        <tr>
                            <td class="label_class">
                                <span >TT :</span>
                            </td>
                            <td  class="label_class">
                                <label>1st :</label>
                                @if($antinatal_clinical_data->tt_1 == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td  class="label_class">
                                <label>2nd :</label>
                                @if($antinatal_clinical_data->tt_2 == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="label_class">
                                <span >HB :</span>
                            </td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->hb}}
                            </td>
                            <td class="label_class">
                                <span>Ht :</span>
                            </td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->ht}}
                            </td>
                            <td class="label_class">
                                <span>H/o Allergy :</span>
                            </td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->ho_allergy}}
                            </td>
                        </tr>
                        <tr>
                            <td class="label_class">
                                <span >BG</span>
                                <span  class="value_class">{{$antinatal_clinical_data->blood_group}}</span>
                            </td>
                            <td class="label_class" colspan="2">
                                <span >Rh Type</span>
                                <span  class="value_class">{{$antinatal_clinical_data->rh_type}}</span>
                            </td>
                            <td class="label_class">
                                <span >PCV</span>
                            </td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->rh_type}}

                            </td>
                            <td class="label_class">
                                <span >MH</span>
                            </td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->mh}}

                            </td>
                            <td class="label_class" rowspan="5" colspan="2">
                                <label ><b>Usg Report</b></label><br>
                                <span  class="value_class">
                                    @if($antinatal_clinical_data->usg_date !='')
                                        {{date('M-d-Y',strtotime($antinatal_clinical_data->usg_date))}}<br>
                                    @endif
                                    {{$antinatal_clinical_data->usg}}
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label_class">
                            bloodgroup
                            </td>
                            <td colspan="2"  class="value_class">
                                {{$antinatal_clinical_data->blood_group}} {{$antinatal_clinical_data->rh_type }}
                            </td>
                            <td class="label_class">
                                <span >Plt</span>
                            </td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->plt}}
                            </td>
                            <td class="label_class">
                                <span >FH</span>
                            </td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->fh}}
                            </td>

                        </tr>
                        <tr>
                            <td class="label_class">
                                <span >HIV</span>
                            </td>
                            <td colspan="2"  class="value_class">
                                @if($antinatal_clinical_data->hiv !='')
                                <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="label_class">
                                <span >RBS</span>
                            </td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->rbs}}
                            </td>
                            <td class="label_class">
                                <span >CVS</span>
                            </td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->cvs}}
                            </td>

                        </tr>
                        <tr>
                            <td class="label_class">
                                <span >HCV</span>
                            </td>
                            <td colspan="2"  class="value_class">
                                {{$antinatal_clinical_data->hcv}}
                            </td>
                            <td class="label_class">
                                <span >UrineRIE</span>
                            </td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->urine_rie}}
                            </td>
                            <td  class="label_class">
                                <span>Rs</span>
                            </td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->rs}}
                            </td>
                        </tr>
                        <tr>
                            <td class="label_class"><span >HBsAg</span></td>
                            <td colspan="2"  class="value_class">
                                {{$antinatal_clinical_data->hbsag}}
                            </td>

                            <td class="label_class"><span >VDRL</span></td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->vdrl}}
                            </td>

                            <td class="label_class"><span >TSH</span></td>
                            <td  class="value_class">
                                {{$antinatal_clinical_data->tsh}}
                            </td>
                        </tr>
                        <tr>
                            <td class="label_class" rowspan="2">
                                <span>Personal History</span>
                            </td>
                            <td colspan='2' rowspan="2"  class="value_class">
                                {{$antinatal_clinical_data->vdrl}}
                            </td>
                            <td class="label_class" rowspan="2">
                                <span>Professional Details</span>
                            </td>
                            <td colspan='3'>
                                <span><i>Husband</i></span><br>
                            </td>
                            <td colspan='2'>
                                <span><i>Wife</i></span><br>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='3'><span  class="value_class">{{$antinatal_clinical_data->profession_husband}}<br></span></td>
                            <td colspan='2'><span  class="value_class">{{$antinatal_clinical_data->profession_wife}}</span></td>
                        </tr>
                        <tr></tr>

                    </tbody>
                </table>
            </td>
        </tr>
        @endif

        @if($gynec_obstretics_count > 0)
                <tr>
                    <td valign="top">
                        <table class="table patientCombinedHistoryTable no-margin" style="margin-bottom:4px;width:50%;">

                            <tbody>
                                <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                    <td colspan="2"><b>Antinatal Obstretics History</b></td>
                                </tr>

                                @for ($m = 0; $m < $gynec_obstretics_count; $m++)
                                    @php
                                        $created_at=!empty($gynec_obstretics_count[$m]->created_at) ?
                                        $gynec_obstretics_count[$m]->created_at : '';
                                        $doctor_name = !empty($gynec_obstretics_count[$m]->doctor_name) ?
                                        $gynec_obstretics_count[$m]->doctor_name : '';
                                    @endphp

                                    @if($gynecology_obstretics_history[$m]->delevery_type1 !='')
                                        <tr>
                                            <td colspan="2"><b>Child-1</b></td>
                                        </tr>
                                        <tr>
                                            <td>Delivery Type : </td>
                                            <td>
                                                @if($gynecology_obstretics_history[$m]->delevery_type1 == 1)
                                                FTND
                                                @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 2)
                                                LSCS
                                                @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 3)
                                                AB
                                                @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 4)
                                                ECT
                                                @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 5)
                                                MTP
                                                @endif
                                            </td>
                                        </tr>
                                        @if(isset($gynecology_obstretics_history[$m]->gender1))
                                        <tr>
                                            <td>Gender : </td>
                                            <td>
                                                @if($gynecology_obstretics_history[$m]->gender1 == 1)
                                                Girl
                                                @else
                                                Boy
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                        @if(isset($gynecology_obstretics_history[$m]->birth_weight1))
                                        <tr>
                                            <td>Birth Weight : </td>
                                            <td>
                                                {{$gynecology_obstretics_history[$m]->birth_weight1 }}
                                            </td>
                                        </tr>
                                        @endif
                                        @if(isset($gynecology_obstretics_history[$m]->age1))
                                        <tr>
                                            <td>Age : </td>
                                            <td>
                                                {{$gynecology_obstretics_history[$m]->age1 }}
                                            </td>
                                        </tr>
                                        @endif
                                        @if(isset($gynecology_obstretics_history[$m]->place_of_birth1))
                                        <tr>
                                            <td>Place of Birth : </td>
                                            <td>
                                                {{$gynecology_obstretics_history[$m]->place_of_birth1 }}
                                            </td>
                                        </tr>
                                        @endif
                                        @if(isset($gynecology_obstretics_history[$m]->any_complications1))
                                        <tr>
                                            <td>Any Complications : </td>
                                            <td>
                                                {{$gynecology_obstretics_history[$m]->any_complications1 }}
                                            </td>
                                        </tr>
                                        @endif

                                    @endif

                                    @if($gynecology_obstretics_history[$m]->delevery_type2 !='')
                                        <tr>
                                            <td colspan="2"><b>Child-2</b></td>
                                        </tr>
                                        <tr>
                                            <td>Delivery Type : </td>
                                            <td>
                                                @if($gynecology_obstretics_history[$m]->delevery_type2 == 1)
                                                FTND
                                                @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 2)
                                                LSCS
                                                @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 3)
                                                AB
                                                @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 4)
                                                ECT
                                                @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 5)
                                                MTP
                                                @endif
                                            </td>
                                        </tr>
                                        @if(isset($gynecology_obstretics_history[$m]->gender2))
                                        <tr>
                                            <td>Gender : </td>
                                            <td>
                                                @if($gynecology_obstretics_history[$m]->gender2 == 1)
                                                Girl
                                                @else
                                                Boy
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                        @if(isset($gynecology_obstretics_history[$m]->birth_weight2))
                                        <tr>
                                            <td>Birth Weight : </td>
                                            <td>
                                                {{$gynecology_obstretics_history[$m]->birth_weight2 }}
                                            </td>
                                        </tr>
                                        @endif
                                        @if(isset($gynecology_obstretics_history[$m]->age2))
                                        <tr>
                                            <td>Age : </td>
                                            <td>
                                                {{$gynecology_obstretics_history[$m]->age2 }}
                                            </td>
                                        </tr>
                                        @endif
                                        @if(isset($gynecology_obstretics_history[$m]->place_of_birth2))
                                        <tr>
                                            <td>Place of Birth : </td>
                                            <td>
                                                {{$gynecology_obstretics_history[$m]->place_of_birth2 }}
                                            </td>
                                        </tr>
                                        @endif
                                        @if(isset($gynecology_obstretics_history[$m]->any_complications2))
                                        <tr>
                                            <td>Any Complications : </td>
                                            <td>
                                                {{$gynecology_obstretics_history[$m]->any_complications2 }}
                                            </td>
                                        </tr>
                                        @endif
                                    @endif
                                    @if($gynecology_obstretics_history[$m]->additional_child_information !='')
                                        @php
                                            $additional_child_information = json_decode($gynecology_obstretics_history[$m]->additional_child_information);
                                            // dd($additional_child_information[1]);
                                        @endphp

                                        @if(sizeof($additional_child_information) > 0)
                                            @php
                                                $additional_child_count = 3;
                                            @endphp
                                            @for($n = 0; $n < sizeof($additional_child_information); $n++)

                                            <tr>
                                                <td colspan="2"><b>Child-{{$additional_child_count}}</b></td>
                                            </tr>
                                            @if(isset($additional_child_information[$n]->delevery_type))
                                            <tr>
                                                <td>Delivery Type : </td>
                                                <td>
                                                    @if($additional_child_information[$n]->delevery_type == 1)
                                                    FTND
                                                    @elseif($additional_child_information[$n]->delevery_type == 2)
                                                    LSCS
                                                    @elseif($additional_child_information[$n]->delevery_type == 3)
                                                    AB
                                                    @elseif($additional_child_information[$n]->delevery_type == 4)
                                                    ECT
                                                    @elseif($additional_child_information[$n]->delevery_type == 5)
                                                    MTP
                                                    @endif
                                                </td>
                                            </tr>
                                            @endif
                                            @if(isset($additional_child_information[$n]->gender))
                                            <tr>
                                                <td>Gender : </td>
                                                <td>
                                                    @if($additional_child_information[$n]->gender == 1)
                                                    Girl
                                                    @else
                                                    Boy
                                                    @endif
                                                </td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td>Birth Weight : </td>
                                                <td>
                                                    {{$additional_child_information[$n]->birth_weight }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Age : </td>
                                                <td>
                                                    {{$additional_child_information[$n]->age }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Place of Birth : </td>
                                                <td>
                                                    {{$additional_child_information[$n]->place_of_birth }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Any Complications : </td>
                                                <td>
                                                    {{$additional_child_information[$n]->any_complications }}
                                                </td>
                                            </tr>
                                            @php
                                                $additional_child_count++;
                                            @endphp

                                            @endfor
                                        @endif

                                    @endif


                                @endfor
                            </tbody>
                        </table>
                    </td>
                </tr>
        @endif



        @if ($med_count > 0)

            <tr>
                <td valign="top">
                    <table class="table patientCombinedHistoryTable no-margin   " style="margin-bottom:4px;">
                        <tbody>


                            <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                <td colspan="2"><b>Medicine</b></td>
                            </tr>
                            @php
                                $next_review_date = '';
                                $next_review_remarks = '';
                                $advice = '';
                            @endphp

                            @for ($m = 0; $m < $med_count; $m++)
                                @php
                                    $head_id = !empty($patient_medication[$m]->head_id) ? $patient_medication[$m]->head_id : 0;
                                    $medicine_code = !empty($patient_medication[$m]->medicine_code) ? $patient_medication[$m]->medicine_code : '';
                                    $item_desc = !empty($patient_medication[$m]->item_desc) ? $patient_medication[$m]->item_desc : '';
                                    $duration = !empty($patient_medication[$m]->duration) ? $patient_medication[$m]->duration : 0;
                                    $duration_unit_id = !empty($patient_medication[$m]->duration_unit_id) ? $patient_medication[$m]->duration_unit_id : 0;
                                    $frequency = !empty($patient_medication[$m]->frequency) ? $patient_medication[$m]->frequency : '';
                                    $quantity = !empty($patient_medication[$m]->quantity) ? $patient_medication[$m]->quantity : 0;
                                    $notes = !empty($patient_medication[$m]->notes) ? $patient_medication[$m]->notes : '';
                                    $next_review_date = !empty($patient_medication[$m]->next_review_date) ? $patient_medication[$m]->next_review_date : '';
                                    $next_review_remarks = !empty($patient_medication[$m]->next_review_remarks) ? $patient_medication[$m]->next_review_remarks : '';
                                    $advice = !empty($patient_medication[$m]->advice_given) ? $patient_medication[$m]->advice_given : '';
                                @endphp
                                @if ($duration_unit_id == 1)
                                    @php
                                        $duration_unit = 'Days';
                                    @endphp
                                @elseif($duration_unit_id == 2)
                                    @php
                                        $duration_unit = 'Week';
                                    @endphp
                                @elseif($duration_unit_id == 3)
                                    @php
                                        $duration_unit = 'Month';
                                    @endphp
                                @else
                                    @php
                                        $duration_unit = 'Days';
                                    @endphp
                                @endif
                                <tr>
                                    <td colspan="2">
                                        {!! $item_desc !!} ({!! $duration !!} {{ $duration_unit }} /
                                        {!! $frequency !!}) - {!! $notes !!}
                                        @if (!empty($notes))
                                            <br>
                                        @endif
                                    </td>
                                </tr>
                            @endfor
                            @if ($next_review_date)
                            <tr>
                                <td style="width:50%"> <b>Next Review Date:</b> </td>
                                <td> {{ $next_review_date }} </td>
                            </tr>
                            @endif
                            @if ($next_review_remarks)
                            <tr>
                                <td style="width:50%"> <b>Next Review Remarks:</b> </td>
                                <td> {{ $next_review_remarks }} </td>
                            </tr>
                            @endif
                            @if ($advice)
                            <tr>
                                <td style="width:50%"> <b>Advice Given:</b> </td>
                                <td> {{ $advice }} </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>



                </td>
            </tr>
        @endif

        @if ($inv_count > 0)
            <tr>
                <td colspan="2">
                    <table class="table patientCombinedHistoryTable no-margin  " style="width:100%;">
                        <tbody>
                            <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                <td colspan="5"><b>Investigation</b></td>
                            </tr>
                            <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                <td><b>Test</b></td>
                                <td><b>Description</b></td>
                                <td><b>Value</b></td>
                                <td><b>Normal Range</b></td>
                                <td><b>Remarks</b></td>
                            </tr>

                            @php $service_desc = "";
                            if(is_string($emr_changes)){
                                $emr_changes= json_decode($emr_changes,true);
                            }
                            $investigation_future_date = @$emr_changes['investigation_future_date'] ? $emr_changes['investigation_future_date'] : 0;
                            @endphp
                            @for ($c = 0; $c < $inv_count; $c++)
                                @php
                                    $head_id = !empty($patient_invsestigation[$c]->head_id) ? $patient_invsestigation[$c]->head_id : 0;
                                    $min = '';
                                    $max = '';
                                    $style = '';
                                    $direction = '';
                                    $html_content = '';
                                    if ($service_desc != $patient_invsestigation[$c]->service_desc) {
                                        $service_desc = $patient_invsestigation[$c]->service_desc;
                                    } else {
                                        $service_desc = '';
                                    }
                                    if($investigation_future_date == 1){
                                        $service_date = !empty($patient_invsestigation[$c]->service_date) ? $patient_invsestigation[$c]->service_date : '';
                                    }
                                    $investigation_type = !empty($patient_invsestigation[$c]->investigation_type) ? $patient_invsestigation[$c]->investigation_type : 0;

                                    $quantity = !empty($patient_invsestigation[$c]->quantity) ? $patient_invsestigation[$c]->quantity : 0;

                                    $sub_test_name = !empty($patient_invsestigation[$c]->detail_description) ? $patient_invsestigation[$c]->detail_description : '-';

                                    $actual_result = !empty($patient_invsestigation[$c]->actual_result) ? $patient_invsestigation[$c]->actual_result : '-';

                                    $normal_range = !empty($patient_invsestigation[$c]->normal_range) ? $patient_invsestigation[$c]->normal_range : '-';

                                    $remarks = !empty($patient_invsestigation[$c]->remarks) ? $patient_invsestigation[$c]->remarks : '-';

                                    if (!empty($patient_invsestigation[$c]->min_value) && is_numeric($patient_invsestigation[$c]->actual_result) && $patient_invsestigation[$c]->max_value != '-') {
                                        if (!empty($patient_invsestigation[$c]->min_value)) {
                                            $min = $patient_invsestigation[$c]->min_value;
                                        }

                                        if (!empty($patient_invsestigation[$c]->max_value)) {
                                            $max = $patient_invsestigation[$c]->max_value;
                                        }

                                        if ($max < $patient_invsestigation[$c]->actual_result && !empty($patient_invsestigation[$c]->actual_result)) {
                                            $style = 'style=color:red;';
                                            $direction = '&nbsp;<i class="fa fa-arrow-up"></i>';
                                        }

                                        if ($min > $patient_invsestigation[$c]->actual_result && !empty($patient_invsestigation[$c]->actual_result)) {
                                            $style = 'style=color:#FF7701;';
                                            $direction = '&nbsp;<i class="fa fa-arrow-down"></i>';
                                        }
                                    }

                                    if ($patient_invsestigation[$c]->report_type == 'T') {
                                        $html_content = "<button class='btn bg-primary' type='button' onclick='getLabResultRtf(" . $patient_invsestigation[$c]->labresult_id . ")'><i class='fa fa-list'></i></button>";
                                    } elseif ($patient_invsestigation[$c]->report_type == 'N' || $patient_invsestigation[$c]->report_type == 'F') {
                                        $html_content = "<div class='text-left'" . $style . '>' . $patient_invsestigation[$c]->actual_result . $direction . '</div>';
                                    } else {
                                        $html_content = '';
                                    }

                                    $result_finalized = !empty($patient_invsestigation[$c]->result_finalized) ? $patient_invsestigation[$c]->result_finalized : 0;
                                    if ($result_finalized == 0) {
                                        $html_content = '';
                                    }

                                @endphp
                                @php
                                $style = '';
                                $service_date1 = '';
                                if($investigation_future_date == 1 && strtotime($service_date) > strtotime($current_date)){
                                    $service_date1 = '('.$service_date.')';
                                        $style = 'style=color:#7383d1;';
                                }
                               @endphp
                                <tr>
                                    <td {{ $style }}><b>{!! $service_desc !!}{{ $service_date1 }}</b></td>
                                    <td>
                                        <div class="text-left text_limit">{{ $sub_test_name }}</div>
                                    </td>
                                    <td>{!! $html_content !!}</td>
                                    <td>{!! $normal_range !!}</td>
                                    <td>{!! $remarks !!}</td>
                                </tr>
                                @php
                                    $service_desc = $patient_invsestigation[$c]->service_desc;
                                @endphp
                            @endfor


                        </tbody>
                    </table>
                </td>
            </tr>
        @endif
        @php
            $i++;
        @endphp
        @endif
        </tbody>
        </table>
    </div>
        @endforeach
        @endif



        @if (isset($clinical_history_old) && count($clinical_history_old) > 0 && $view_type != 1)
            <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                <td colspan="5"><b>Clinical Notes History</b></td>
            </tr>
            <tr>
                <td>
                    <table class="table patientCombinedHistoryTable no-margin    ">
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <table class="table patientCombinedHistoryTable  " style="margin-bottom:4px;">
                                        <tbody>
                                            @foreach ($clinical_history_old as $key => $data)
                                                @php
                                                    $clinical_date = $data->mr_date ? date('d-M-Y h:i A', strtotime($data->mr_date)) : '';
                                                    $complaints = $data->complaints;
                                                @endphp
                                                <tr class="border_dashed_bottom" style="padding-left:0px;">
                                                    <td class="text-left">
                                                        <i class="fa fa-calendar"></i> {{ $clinical_date }}
                                                    </td>
                                                </tr>
                                                <tr class="border_dashed_bottom" style="padding-left:0px;">
                                                    <td class="text-left">
                                                        <i class="fa fa-user-md"></i>
                                                        <b>{{ $data->doctor_code }} </b>
                                                    </td>
                                                </tr>
                                                <tr class="border_dashed_bottom" style="padding-left:0px;">
                                                    <td colspan="4">
                                                        <span style="line-height: 0px;"> {!! $complaints !!}
                                                        </span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        @endif
        @if (!empty($patient_medications_old) && $view_type != 1)
            <div style="background-color:ebfaef;"><span><b>Medicine History</b></span></div>
            {!! $patient_medications_old !!}
        @endif
    </div>
</div>
