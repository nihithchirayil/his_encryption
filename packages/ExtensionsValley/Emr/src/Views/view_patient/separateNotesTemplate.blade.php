<style>

    .assessmentFavButtonNew{
        position: absolute;
        top: 2px;
        right:10px;
        z-index: 999;
        color: #21a4f1;
        cursor: pointer
    }
    .assessmentBookmarkButtonNew{
        position: absolute;
        top: 3px;
        right: 30px;
        z-index: 999;
        color: #21a4f1;
        cursor: pointer;
    }
    .popover-title .close {
        margin-top: -3px;
        margin-right: -6px;
    }

    .bookmark-list-div-row-listing tbody tr:hover {
        background: #21a4f1 !important;
        color: white;
        cursor: pointer;
    }

    .bookmark-list-div-row-listing {
        top: 15%;
        right: 1%;
        z-index: 9999;
        position: absolute;
        background: #FFF;
        box-shadow: 0 0 6px #ccc;
        width: 200px;
        height: 140px;
        border: 1px solid #5ca5f0;
        padding: 8px;
    }
    .close_btn_book_mark_search {
        position: absolute;
        z-index: 99;
        color: #FFF !important;
        background: #000;
        right: -11px;
        top: -1px;
        border-radius: 100%;
        text-align: center;
        width: 20px;
        height: 20px;
        line-height: 20px;
        cursor: pointer;
    }

    .bookmark-list-div-row-listing tbody tr:hover {
        background: #21a4f1 !important;
        color: white;
        cursor: pointer;
    }

    .bookmark-list-div-row-listing{
        right:-200px !important;
    }
</style>
<div class="col-md-6 padding_sm">
    <div class="col-md-12 padding_sm table_box notes_entering_area " style="">
        <div class="col-md-11 no-padding">
            <div class="text_head dynamic_single_head_text">Chief Complaint</div>
        </div>
        <div class="clearfix"></div>
        <form id="ca-data-form">
            <div class="col-md-12 no-padding notes_chief_complaint_container" style="">
                @if (isset($dynamic_notes_fields) && count($dynamic_notes_fields) > 0)
                @foreach ($dynamic_notes_fields as $fields)
                @if ($fields->type == 'dyn_textarea')
                <div class="col-md-12 padding_sm" style="margin-top: 5px">
                    <i class="fa fa-star assessmentFavButtonNew"></i>
                    <i class="fa fa-bookmark assessmentBookmarkButtonNew" asss_book_mark_data-head-id ='{{$fields->head_id}}'></i>
                    <div class="bookmark-list-div-row-listing bookmark_list_div_{{$fields->head_id}}" style="display: none;">
                    <a style="float: left;" class="close_btn_book_mark_search">X</a>
                        <div class=" theadscroll" style="position: relative; height : 100px !important">
                            <table class="table table-bordered no-margin table_sm table-striped theadfix_wrapper">
                                <tbody class="list-book-mark-search-data-row-listing theadscroll">
                                    @if(!empty($bookmarked_notes[$fields->head_id]))
                                    @php
                                    $notes = $bookmarked_notes[$fields->head_id];
                                    @endphp
                                    @foreach($notes as $data2)
                                    <tr title='{{$data2->notes_text}}'>
                                    <td onclick=fetchNotesToTemplate('{!! base64_encode($data2->notes_text)!!}','{!!$data2->emr_dynamic_head_id!!}'); style='width:90%;'>{{$data2->bookmark_name}}</td>
                                    <td style='width:10%;'><i class='fa fa-times red deleteBookmarkedItemBtn' data-bookmarked-head-id='{!!$data2->emr_dynamic_head_id!!}' data-bookmarked-id='{{$data2->id}}'> </i></td>
                                    </<tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="mate-input-box">
                        <label class="filter_label">{{ $fields->name }}</label>
                        @if ($fields->type == 'dyn_textarea')
                        <textarea id="{{$fields->class_name}}" data-head-id="{{$fields->head_id}}" data-element-classname="{{$fields->class_name}}" data-element-type="{{$fields->type}}"
                            class="form-control notes_data_template {{$fields->class_name}}_textarea"
                            ></textarea>
                        @elseif ($fields->type=='dyn_textbox')
                        <input id="{{$fields->class_name}}" data-head-id="{{$fields->head_id}}" data-element-classname="{{$fields->class_name}}" data-element-type="{{$fields->type}}" type="text"
                            class="form-control notes_data_template {{$fields->class_name}}_textarea"
                            style="border-radius: 8px 0px 8px 0px;">
                        @endif
                    </div>

                </div>
                <div class="clearfix"></div>
                @endif
                @endforeach
                @endif

            </div>
            <div class="updateNotesDiv">
                <button type="button" class="btn btn-sm updateNotesBtn"><i class="fa fa-save"></i> Save</button>
                <button type="button" class="btn btn-sm btn-blue canceAssessmentEditBtn"><i class="fa fa-times"></i>
                    Cancel</button>
            </div>
        </form>

    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 padding_sm table_box bookmarked_notes_area" style="height : 210px !important; display:none;">
        <div class="text_head">Bookmarks</div>
        <div class="clearfix"></div>
        <div class="notes_bookmarks_area theadscroll" style="">
        </div>
    </div>
</div>

<div class="col-md-6 padding_sm">

    <div class="col-md-12 padding_sm table_box patient_todays_visit_container">
        <div class="text_head">Today's Visit</div>
        <div class="clearfix"></div>
        <div class="todays_visit_details_div theadscroll" style="position:relative; height:143px;">

        </div>
    </div>
    <div class="col-md-12 padding_sm table_box patient_history_container">
        <div class="text_head">History
        <div style="" class="pull-right"><button style=" background: white;
            color: #21a4f1;" type="button" class="btn btn-info historyPullUpDown" sort_order="asc" title="Past History"> <i class="fa fa-angle-double-down"></i></button>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="patient_combined_history_div" id="showPatientFullHistoryData" style="min-height:300px;">

        </div>
    </div>
</div>
<script>
    $(document).on("click", '.assessmentFavButtonNew', function (ev) {
        // var notes = $("#chief_com").val();
        var class_name = $(this).nextAll('div').find('textarea').data('element-classname');
        var head_id =  $(this).nextAll('div').find('textarea').data('head-id');
        var notes ="";
        notes = $("#"+class_name).val();

        if (notes != '' && notes != undefined) {
            bootbox.prompt({
                title: "Enter name for bookmark.",
                centerVertical: true,
                callback: function (result) {
                    if (result) {
                        let url = $('#base_url').val();
                        let patient_id = $("#patient_id").val();
                        var doctor_id = $('#doctor_id').val();
                        $.ajax({
                            type: "POST",
                            url: url + "/emr_lite/bookmarkAssessment",
                            data: {
                                notes: notes,
                                bookmark_name: result,
                                head_id: head_id,
                                doctor_id: doctor_id,
                            },
                            beforeSend: function () {
                                $(".assessmentFavButton").removeClass('fa-star').addClass('fa-spinner fa-spin');
                            },
                            success: function (data) {

                                if (data.status == 1) {
                                    $(".notes_bookmarks_area").prepend('<div class="bookmarked_notes_item"><span class=" bookmarks-notes-text bookmarked_notes_text" title="' + notes + '"> ' + result + '</span> <i style="margin-left: 8px;" class="fa fa-times red deleteBookmarkedItemBtn" data-bookmarked-head-id="'+head_id+'"  data-bookmarked-id="' + data.bookmarked_id + '"> </i></div>')
                                } else {
                                    Command: toastr["error"]("Error please check your internet connection");
                                }

                                fetchBookmarkedNotes(head_id);

                            },
                            complete: function () {
                                $(".assessmentFavButton").removeClass('fa-spinner fa-spin').addClass('fa-star');
                            }
                        });
                    }
                }
            });
        }
    });

    function fetchNotesToTemplate(notes,head_id){
        var current_text =  $('[data-head-id="'+head_id+'"]').val();
        $('[data-head-id="'+head_id+'"]').val(current_text +' '+ atob(notes));
    }

    function fetchBookmarkedNotes(head_id){
        console.log(head_id);
        var doctor_id = $('#doctor_id').val();
        let url = $('#base_url').val();
        $.ajax({
            type: "POST",
            url: url + "/emr_lite/fetchBookmarkedNotes",
            data: {
                head_id: head_id,
                doctor_id: doctor_id,
            },
            beforeSend: function(){
            },
            success: function (data) {
                if(data !=0){
                    $('[data-head-id="'+head_id+'"]').attr("data-content", data);
                }
            },
            complete: function () {
            }
        });
    }

//close Book mark search
$(document).on('click', '.close_btn_book_mark_search', function (event) {
    event.preventDefault();
    $(".bookmark-list-div-row-listing").hide();
});
$(document).on('click', '.assessmentBookmarkButtonNew', function (event) {
    $(".bookmark-list-div-row-listing").hide();
    var data_head_id = $(this).attr('asss_book_mark_data-head-id');
    $('.bookmark_list_div_' + data_head_id).show();
});

</script>
