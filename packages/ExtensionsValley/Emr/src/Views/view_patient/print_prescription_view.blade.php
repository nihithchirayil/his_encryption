@php
  $hospital_header_disable_in_prescription = 0;
  $department_head = (isset($doctor->prescription_department_head)) ? $doctor->prescription_department_head : '';
  $medication_footer_text = (isset($doctor->medication_footer_text)) ? $doctor->medication_footer_text : '';
  $medication_note = (isset($doctor->medication_note)) ? $doctor->medication_note : '';
  $next_review_date = (!empty($encounterDetails->next_review_date)) ? date('d-M-Y', strtotime($encounterDetails->next_review_date)) : '';
  $advice_given = (!empty($advice_given)) ? $advice_given : '';
  $diagnosis = (!empty($encounterDetails->diagnosis)) ? $encounterDetails->diagnosis : '';
  $next_review_remarks = (!empty($encounterDetails->next_review_remarks)) ? $encounterDetails->next_review_remarks : '';
  $chief_complaint = (!empty($encounterDetails->chief_complaint)) ? $encounterDetails->chief_complaint : '';
  $doctor_name_visible_flag = @$doctor_name_visible_flag ? $doctor_name_visible_flag : 0; 
  $license_no_flag = @$license_no_flag ? $license_no_flag : 0; 
  $prescription_changes = @$prescription_changes ? $prescription_changes : 0;
  $style = ''; 
@endphp

<style>
@page {
    margin: 15px 15px;
    size: A4 portrait;
    /* size: A5 landscape; */
}
  </style>
<div class="col-md-12" @if($hospital_header_disable_in_prescription == 1) style="margin-top:3cm;" @endif>
  @php
    if($prescription_changes == 0){
      $style = "border-top: 1px solid;";
    }
  @endphp

  <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; font-size: 12px;{{ $style }} border-bottom: 1px solid; margin-top:2px;">
    <thead>
      <!-- Hospital Header -->
      <tr>
        <th>
          @if(!empty($hospitalHeader))
            {!!$hospitalHeader!!}
          @endif

        </th>
      </tr>
      <!-- Hospital Header -->
      <tr style="margin-top:2px; ">
        <th>
          <table width="100%" style="border-top:1px solid; border-bottom:1px solid; ">
              <tbody>
                <tr>
                  <td style="font-size: 12px;">{{$patient[0]->patient_name}} ({{$patient[0]->gender}}/{{$patient[0]->age}})</td>
                  <td style="text-align:right; font-size: 12px;">{{$date}}</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">{{$patient[0]->uhid}}</td>
                  <td style="text-align:right; font-size: 12px;">
                      @if(!empty($intend_type) && $intend_type == 1)
                        Regular
                      @elseif(!empty($intend_type) && $intend_type == 2)
                          New Admission
                      @elseif(!empty($intend_type) && $intend_type == 3)
                          Emergency
                      @else
                          Discharge
                      @endif
                  </td>
                </tr>
              </tbody>
            </table>
        </th>
      </tr>
    </thead>
    <tbody>
      {{--<tr style="border-bottom: none">
          <td style="padding: 0px; vertical-align: middle; border-bottom: none" width="15%" align="center" colspan="2">
          <h3>{!! $department_head !!}</h3>
          </td>
      </tr> --}}
      <tr>
        <td width="100%" valign="top" style="padding: 5px 5px 5px 5px;">
            <table cellspacing="0" cellpadding="2" width="100%" style="border-collapse: collapse; font-size: 13px; ">
              <tr style=" border-bottom: 1px solid;">
                <td><strong>SL#</strong></td>
                <td ><strong>Medicine Name</strong></td>
                <td><strong>Frequency</strong></td>
                <td><strong>Duration</strong></td>
              </tr>
              <?php $i = 1;?>
              @foreach($data as $pres)
              <?php

              $medicine = "";
              if(!empty($pres->medicine_code)){
                  $medicine = $pres->item_desc;
              }else{
                  $medicine = $pres->medicine_name;
              }

              ?>
              <tr style="border-bottom: 1px dotted #efefef;">
                <td> {{$i}} </td>
                <td ><div>{{ $medicine }}</div><div style="font-size:11px;">Generic : {{ $pres->generic_name }} </div> </td>
                <td> {!! $pres->frequency!!} <br/> @if(!empty($pres->notes)) ( {!! $pres->notes !!} ) @endif </td>
                @php
                    $duration_unit_id=$pres->duration_unit_id;
                @endphp
                @if ($duration_unit_id == 1)
                @php
                    $duration_unit = 'Days';
                @endphp
            @elseif($duration_unit_id == 2)
                @php
                    $duration_unit = 'Week';
                @endphp
            @elseif($duration_unit_id == 3)
                @php
                    $duration_unit = 'Month';
                @endphp
            @else @php
            $duration_unit = 'Days';
            @endphp
            @endif
                <td> {!! $pres->duration!!} {{ $duration_unit }} <br/> @if(!empty($pres->quantity)) ( Total: {!! $pres->quantity!!} Nos @endif ) </td>
              </tr>
              <?php $i++;?>
              @endforeach
              <tr>
                <td colspan="4" width="100%" style="border-bottom: 1px solid;"></td>
            </tr>
            </table>
            <br>
            <table width="100%" style="font-size:12px;">
              <tr>
                <td>
                    @if(!empty($chief_complaint))
                    <strong style="vertical-align:top; float: left;">Chief Complaint : </strong>
                    {!!trim(nl2br($chief_complaint))!!}
                    @endisset
                </td>
              </tr>
           </table>
            <table width="100%" style="font-size:12px;">
            <tr>
                <td colspan="2">
                  @if(!empty($diagnosis))
                    <strong style="font-size: 11px;">Diagnosis : </strong> &nbsp;
                    <input value="{{$diagnosis}}" style="border-left: none; border-top: none; border-right: none; border-bottom: none;" type="text">
                  @endif
                </td>
            </tr>
            <tr>
                <td colspan="2">
                  @if(!empty($advice_given))
                    <strong style="font-size: 11px;">Advice Given : </strong> &nbsp;
                    <input value="{{$advice_given}}" style="border-left: none; border-top: none; border-right: none; border-bottom: none;" type="text">
                  @endif
                </td>
            </tr>
              <tr>
                <td width="49%">
                  @if(!empty($next_review_date))
                    <strong style="font-size: 11px;">Review On : </strong> &nbsp;
                    <input value="{{$next_review_date}}" style="border-left: none; border-top: none; border-right: none; border-bottom: none;" type="text">
                  @endif
                </td>
              </tr>
              <tr>  
                <td width="49%">
                  @if(!empty($next_review_remarks))
                    <strong style="font-size: 11px;">Next Review Remarks : </strong> &nbsp;
                    <input value="{{$next_review_remarks}}" style="border-left: none; border-top: none; border-right: none; border-bottom: none;" type="text">
                  @endif
                </td>
              </tr>
                <td  width="49%">
                  @if(!empty($doctor['doctor_name']))
                  <span style="float: right; margin-right: 10px; font-size: 11px;">
                    @if (intval($doctor_name_visible_flag) == 0) 
                    {{$doctor['doctor_name']}}<br/>
                    {{$doctor['qualification']}} ( {{$doctor['doctor_speciality']}} )<br/>
                      @if (intval($license_no_flag) == 1) 
                      <strong style="font-size: 11px;">License No : </strong> &nbsp; {{$doctor['license_no']}}
                      @endif
                    @endif
                    <br/>
                    <br/>
                    <strong>SIGNATURE:</strong>
                  </span>
                  @endif
                </td>
              </tr>
            </table>
            @if($prescription_changes == 1)
           
              @if(isset($clincal_assessments['dynamic_chief_complaints']) && count($clincal_assessments['dynamic_chief_complaints']) > 0 )
              <table width="100%" border="0" cellspacing="0" cellpadding="4">
                @php  $chief_complaints = array_unique($clincal_assessments['dynamic_chief_complaints']); @endphp
                @foreach($chief_complaints as $data)
                @if($data != "")
                  <tr>
                    <td width="90%"><strong> CHIEF COMPLAINTS </strong> : {{$data}}</td>
                  </tr>
                @endif
                @endforeach
              </table>
              @endif
              @if(isset($clincal_assessments['dynamic_diagnosis']) && count($clincal_assessments['dynamic_diagnosis']) > 0 )
              <table width="100%" border="0" cellspacing="0" cellpadding="4">
                @php $diagnosis = array_unique($clincal_assessments['dynamic_diagnosis']); @endphp
                @foreach($diagnosis as $data)
                @if($data != "")
                  <tr>
                    <td width="90%"><strong> DIAGNOSIS </strong> : {{$data}}</td>
                  </tr>
                @endif
                @endforeach
              </table>
              @endif
              @if(isset($clincal_assessments['dynamic_eye_treatment']) && count($clincal_assessments['dynamic_eye_treatment']) > 0 )
              <table width="100%" border="0" cellspacing="0" cellpadding="4">
                @php $eye_treatment = array_unique($clincal_assessments['dynamic_eye_treatment']); @endphp
                @foreach($eye_treatment as $data)
                @if($data != "")
                  <tr>
                    <td width="90%"><strong> TREATMENT PLAN </strong> : {{$data}}</td>
                  </tr>
                @endif  
                @endforeach
              </table>
              @endif
            @endif
            <br><br>
          </td>
      </tr>
      @if(!empty($medication_footer_text))
      {{--<tr style="border-top: 1px solid;">
        <td style='padding: 3px; vertical-align: middle;' width='15%' align='center' colspan="2">
          <span style="float: right; margin-right: 180px; font-size: 11px;"><strong>{!! $medication_footer_text !!}</strong></span>
        </td>
      </tr> --}}
      @endif
    </tbody>
  </table>
</div>
