<div class="wrapper theadscroll" style="position: relative; height: 800px;" id="resltData">
    <table class="table no-margin table_sm theadfix_wrapper table-striped  table-condensed"
        id="result_data_table">
        <thead>
            <tr class="table_header_bg"
                style="background-color: rgb(227 227 227);color: #0c0c0c;border-spacing: 0 1em;font-family:sans-serif">
                <th style="width: 7%;">GRN NO</th>
                <th style="width: 7%;">BILL NO</th>
                <th style="width:8%;">BILL DATE</th>
                <th style="width: 13%;">VENDOR NAME</th>
                <th style="width: 9%">BATCH</th>
                <th style="width: 8%;">PURCH.RATE</th>
                <th style="width: 5%;">STOCK VALUE</th>
                <th style="width: 8%;">QTY</th>
                <th style="width: 8%;">UOM</th>
                <th style="width: 8%;">FREE QTY</th>
                <th style="width: 8%;">ORG.MRP</th>
            </tr>
        </thead>
        <tbody>
            @if(sizeof($res) > 0)
            @foreach ($res as $data)
            <tr>
                <td class=" common_td_rules" title="{{ $data->grn_no }}">{{ $data->grn_no }}</td>
                <td class=" common_td_rules"  title="{{ $data->bill_no }}">{{ $data->bill_no }}</td>
                <td class=" common_td_rules"  title="{{ $data->bill_date }}">{{ $data->bill_date }}</td>
                <td class=" common_td_rules"  title="{{ $data->vendor_name }}">{{ $data->vendor_name }}</td>
                <td class=" common_td_rules"  title="{{ $data->batch }}">{{ $data->batch }}</td>
                <td class=" common_td_rules"  title="{{ $data->grn_rate }}">{{ $data->grn_rate }}</td>
                <td class=" common_td_rules"  title="{{ $data->stock_value }}">{{ $data->stock_value }}</td>
                <td class=" common_td_rules"  title="{{ $data->grn_qty}}">{{ $data->grn_qty}}</td>
                <td class=" common_td_rules"  title="{{ $data->uom_name }}">{{ $data->uom_name }}</td>
                <td class=" common_td_rules"  title="{{ $data->grn_free_qty }}">{{ $data->grn_free_qty }}</td>
                <td class=" common_td_rules"  title="{{ $data->grn_mrp }}">{{ $data->grn_mrp }}</td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>
