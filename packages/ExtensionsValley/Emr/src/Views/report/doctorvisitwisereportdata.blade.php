<div class="row">

   <div class="col-md-12" id="result_container_div">

    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">

      <thead>
          <tr class="headerclass" style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
              <th width="3%;">Sl.No</th>
              <th width="13%;">Doctor Name</th>
              <th width="14%;">Speciality Name</th>
              <th width="5%">New Count</th>
              <th width="5%">Renew Count</th>
              <th width="6%">Followup Count</th>
              <th width="5%">Tot. Count</th>
              <th width="5%">Refund Count</th>
              <th width="5%">Cons. Fee</th>
              <th width="5%">Hosp. Fee</th>
              <th width="5%">MLC Charge</th>
              <th width="5%">Conv. Fee</th>
              <th width="5%">Tot. Amount</th>
              <th width="6%">Cons. Refund Amt.</th>
              <th width="6%">Hosp. Refund Amt.</th>
              <th width="6%">Conv. Refund Amt.</th>
              <th width="6%">Tot. Refund Amt.</th>
              <th width="6%">Net Amt.</th>
          </tr>
      </thead>
      <?php
        $tot_new_count=0.0;
        $tot_renew_count=0.0;
        $tot_followup_count=0.0;
        $tot_tot_count=0.0;
        $tot_refund_count=0.0;
        $tot_consultation_fee=0.0;
        $tot_hospital_fee=0.0;
        $tot_mlc_charge=0.0;
        $tot_convenience_fee=0.0;
        $tot_tot_amount=0.0;
        $tot_consultation_refund_amt=0.0;
        $tot_hosp_refund_amt=0.0;
        $tot_conv_refund_amt=0.0;
        $tot_tot_refund_amount=0.0;
        $net_amount_tot=0.0;

      if(count($res)!=0){
      ?>

      <tbody>
          <?php
          $i = 1;
          foreach ($res as $data){
            $net_amt = floatval($data->tot_amount)-floatval($data->tot_refund_amount);
                ?>
              <tr>
                  <td class="common_td_rules">{{$i}}</td>
                  <td class="common_td_rules">{{$data->doctor_name}}</td>
                  <td class="common_td_rules">{{$data->speciality_name}}</td>
                  <td class="td_common_numeric_rules">{{$data->new_count}}</td>
                  <td class="td_common_numeric_rules">{{$data->renew_count}}</td>
                  <td class="td_common_numeric_rules">{{$data->followup_count}}</td>
                  <td class="td_common_numeric_rules">{{$data->tot_count}}</td>
                  <td class="td_common_numeric_rules">{{$data->refund_count}}</td>
                  <td class="td_common_numeric_rules">{{$data->consultation_fee}}</td>
                  <td class="td_common_numeric_rules">{{$data->hospital_fee}}</td>
                  <td class="td_common_numeric_rules">{{$data->mlc_charge}}</td>
                  <td class="td_common_numeric_rules">{{$data->convenience_fee}}</td>
                  <td class="td_common_numeric_rules">{{$data->tot_amount}}</td>
                  <td class="td_common_numeric_rules">{{$data->consultation_refund_amt}}</td>
                  <td class="td_common_numeric_rules">{{$data->hosp_refund_amt}}</td>
                  <td class="td_common_numeric_rules">{{$data->conv_refund_amt}}</td>
                  <td class="td_common_numeric_rules">{{$data->tot_refund_amount}}</td>
                  <td class="td_common_numeric_rules">{{$net_amt}}</td>
              </tr>
                  <?php

                  $tot_new_count += floatval($data->new_count);
                  $tot_renew_count+= floatval($data->renew_count);
                  $tot_followup_count+= floatval($data->followup_count);
                  $tot_tot_count+= floatval($data->tot_count);
                  $tot_refund_count+= floatval($data->refund_count);
                  $tot_consultation_fee+= floatval($data->consultation_fee);
                  $tot_hospital_fee+= floatval($data->hospital_fee);
                  $tot_mlc_charge+= floatval($data->mlc_charge);
                  $tot_convenience_fee+= floatval($data->convenience_fee);
                  $tot_tot_amount+= floatval($data->tot_amount);
                  $tot_consultation_refund_amt+= floatval($data->consultation_refund_amt);
                  $tot_hosp_refund_amt+= floatval($data->hosp_refund_amt);
                  $tot_conv_refund_amt+= floatval($data->conv_refund_amt);
                  $tot_tot_refund_amount+= floatval($data->tot_refund_amount);
                  $net_amount_tot+=$net_amt;
                  $i++;
              }
              ?>
              <tr class="" style="height: 30px;">
                  <td class="common_td_rules" colspan="3" style="text-align: left;"> <strong>Total</strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_new_count?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_renew_count?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_followup_count?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_tot_count?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_refund_count?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_consultation_fee?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_hospital_fee?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_mlc_charge?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_convenience_fee?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_tot_amount?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_consultation_refund_amt?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_hosp_refund_amt?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_conv_refund_amt?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_tot_refund_amount?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$net_amount_tot?></strong></td>
              </tr>
          <?php
           } else{ ?>
      <tr>
          <td colspan="16" style="text-align: center;">No Results Found!</td>
      </tr>
      <?php } ?>
      </tbody>

</table>

</div>
</div>
