<div class="row">

    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Department Wise Income Detail Report </b></h4>
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="14%;">Doctor Name</th>
                        <th width="15%;">Bill No</th>
                        <th width="8%;">Bill Date</th>
                        <th width="12%;">UHID</th>
                        <th width="14%;">Patient Name</th>
                        <th width="14%;">Item Desc</th>
                        <th width="5%;">Hos.Revenue</th>
                        <th width="5%;">Doc. Revenue</th>
                        <th width="5%;">Bill Amount</th>
                        <th width="4%;">Refund</th>
                        <th width="5%;">Net Amount</th>
                    </tr>
                </thead>
                <?php
                $nettotal_bill_net_amount = 0;
                $nettotal_billamount_total=0;
                $nettotal_refundamount_total=0;
                $nettotal_hosrevenue_total=0;
                $nettotal_docrevenue_total=0;
                ?>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
            $i=0;
            $department_name = '';
            $total_bill_net_amount = 0;
            $net_amount_total=0;
            $net_billamount_total=0;
            $net_refundamount_total=0;
            $net_hosrevenue_total=0;
            $net_docrevenue_total=0;

         foreach ($res as $data){
             $net_amount=floatval($data->bill_amount)-floatval($data->refund);
             if($department_name != $data->dept_name){
                 $i=0;
                 $department_name = $data->dept_name;
                 $net_amount_total=0;
                 $net_billamount_total=0;
                 $net_refundamount_total=0;
                 $net_hosrevenue_total=0;
                 $net_docrevenue_total=0;
               ?>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="11" class="common_td_rules">{{ $department_name }}</th>
                        </tr>
                        <tr>
                            <td class="common_td_rules">{{ $data->doctor_name }}</td>
                            <td class="common_td_rules">{{ $data->bill_no }}</td>
                            <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->bill_date)) }}</td>
                            <td class="common_td_rules">{{ $data->uhid }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->item_desc }}</td>
                            <td class="td_common_numeric_rules">{{ $data->hos_revenue }}</td>
                            <td class="td_common_numeric_rules">{{ $data->doc_revenue }}</td>
                            <td class="td_common_numeric_rules">{{ $data->bill_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $data->refund }}</td>
                            <td class="td_common_numeric_rules">{{ $net_amount }}</td>
                        </tr>
                        <?php
             }else{
                 ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->doctor_name }}</td>
                            <td class="common_td_rules">{{ $data->bill_no }}</td>
                            <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->bill_date)) }}</td>
                            <td class="common_td_rules">{{ $data->uhid }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->item_desc }}</td>
                            <td class="td_common_numeric_rules">{{ $data->hos_revenue }}</td>
                            <td class="td_common_numeric_rules">{{ $data->doc_revenue }}</td>
                            <td class="td_common_numeric_rules">{{ $data->bill_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $data->refund }}</td>
                            <td class="td_common_numeric_rules">{{ $net_amount }}</td>
                        </tr>
                        <?php

             }
             $net_amount_total+= floatval($net_amount);
             $net_billamount_total+= floatval($data->bill_amount);
             $net_refundamount_total+= floatval($data->refund);
             $net_hosrevenue_total+= floatval($data->hos_revenue);
             $net_docrevenue_total+= floatval( $data->doc_revenue);
             if($i==$department_cnt[$department_name]){
             ?>
                        <tr style="height: 30px;">
                            <th class="common_td_rules" colspan="6" style="text-align: left;">Department Total</th>
                            <th class="td_common_numeric_rules"><?= $net_hosrevenue_total ?></th>
                            <th class="td_common_numeric_rules"><?= $net_docrevenue_total ?></th>
                            <th class="td_common_numeric_rules"><?= $net_billamount_total ?></th>
                            <th class="td_common_numeric_rules"><?= $net_refundamount_total ?></th>
                            <th class="td_common_numeric_rules"><?= $net_amount_total ?></th>

                        </tr>
                        <?php
                    $nettotal_bill_net_amount+=$net_amount_total;
                    $nettotal_billamount_total+=$net_billamount_total;
                    $nettotal_refundamount_total+=$net_refundamount_total;
                    $nettotal_hosrevenue_total+=$net_hosrevenue_total;
                    $nettotal_docrevenue_total+=$net_docrevenue_total;
             }
             $i++;

         }
         ?>
                        <tr style="height: 30px;">
                            <th class="common_td_rules" colspan="6" style="text-align: left;">Net Total</th>
                            <th class="td_common_numeric_rules"><?= $nettotal_hosrevenue_total ?></th>
                            <th class="td_common_numeric_rules"><?= $nettotal_docrevenue_total ?></th>
                            <th class="td_common_numeric_rules"><?= $nettotal_billamount_total ?></th>
                            <th class="td_common_numeric_rules"><?= $nettotal_refundamount_total ?></th>
                            <th class="td_common_numeric_rules"><?= $nettotal_bill_net_amount ?></th>
                        </tr>
                    @else
                        <tr>
                            <td colspan="11" style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
        </font>
    </div>
</div>
