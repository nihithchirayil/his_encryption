@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<style>
@import url('https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap');


.wrapper{
  width: 100%;
  background: #fff;
  padding: 30px;
  box-shadow: 0px 5px 3px rgb(0 0 0 / 10%);
}
.wrapper .input-data{
  height: 40px;
  width: 90%;
  position: relative;
  margin-left: -20px;
}
.wrapper .input-data input{
  height: 100%;
  width: 100%;
  border: none;
  font-size: 14px;
  border-bottom: 2px solid silver;
  font-family:sans-serif;
}
.input-data input:focus ~ label,
.input-data input:valid ~ label{
  transform: translateY(-20px);
  font-size: 14px;
  color: grey;
}
.wrapper .input-data label{
  position: absolute;
  bottom: 10px;
  left: 0;
  color: grey;
  pointer-events: none;
  transition: all 0.4s ease;
}
input[type=checkbox], input[type=radio] {
    margin: 6px 1px 10px;
    margin-top: 1px\9;
    line-height: normal;
}
.input-data .underline{
  position: absolute;
  height: 2px;
  width: 100%;
  bottom: 0;
}
.input-data .underline:before{
  position: absolute;
  content: "";
  height: 100%;
  width: 100%;
  background: #hsl(0deg 1% 3%);
  transform: scaleX(0);
  transform-origin: center;
  transition: transform 0.3s ease;
}
.input-data input:focus ~ .underline:before,
.input-data input:valid ~ .underline:before{
  transform: scaleX(1);
}

  .liHover{
     background: #2a3f54 !important;
  }
  .box_border {
      padding-top: 25px;
      padding-bottom: 10px;
      border: 1px solid #ccc;
      background: #f3f3f3;
  }
  .tinymce-content p {
      padding: 0;
      margin: 2px 0;
  }
  .content_add:hover{
   color: red;
   background-color: yellow;
  }
  .content_remove:hover{
   color: red;
   background-color: yellow;
  }
  .modal-header{
      background-color:orange;
  }

    #MTable > tbody > tr > td{
        word-wrap:break-word
      }
      table#posts {
        word-wrap: break-word;
    }
    .page{
        padding: 65px;
        min-height: 1134px;
        width: 812px;
    }



    .avlailable_check{
        margin-top: 2px !important;
        margin-right: 5px !important;
    }
    .order_by_select{
        float:right !important;
    }
    .filters_title {
        margin-top:0px !important;
        margin-bottom:0px !important;
        margin-left:2px !important;
    }

    .filter_label{
        font-weight: 50 !important;
        font-size: 11px !important;
        color: #266c94 !important;
    }

    .col-xs-2{
        padding-right:7px !important;
        padding-left:7px !important;
    }

    .btn_add_to_order_by{
        float: right;
        width: 18px;
        height: 16px;
        padding: 0px;
    }
    .ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 350px;
        margin: 3px 0px 0px 0px;
        /* overflow-y: auto; */
        /* width: 250px; */
        z-index: 599;
        position:relative;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);
        border-top: 0px;

    }
    .ajaxSearchBox>li{
        padding:2px 2px 5px 5px;
        font-size: 12px !important;
        font-weight: 400 !important;
        font-family: "sans-serif";
        border-bottom: 1px solid grey;
    }

    .liHover{
        /* background: #4c6456 !important; */
        color:white;
        padding:2px 2px 5px 5px;
        font-size: 10px !important;
        font-weight: 400;
        font-family: "sans-serif";
        border-bottom: 1px solid white;
    }
    .bg-blue-active {
        background-color: #E1D5F4  !important;
    }
    .filter_label {
        color: #1c3525 !important;
    }
    .bg-primary {
        color: black;
        background-color: #768a7e;
    }
    .box.box-primary {
        border-top:1px solid #E1D5F4 ;
    }
    .btn-info{
        color: black !important;
        background-color: #E1D5F4  !important;
        border-color: #768a7e !important;
        background-image: linear-gradient(to bottom,#bedecb 0,#2aabd2 100%);
    }
    .liHover {
        background: #8e948f;
        color: white;
    }
    .filter_label {
       margin-bottom: 0px;
    }
    .btn-group {
        width:100% !important;
    }
    .col-xs-12{
        margin-bottom:8px;
    }
    .col-xs-6{
        margin-bottom:8px;
    }
    .filter_label {
        color: #294236 !important;
        font-weight: 600 !important;
    }
    /* tr:hover{
        font-size: 12.5px;
        color:blue !important;
        background-color: #f2f3cc !important;
    } */
    @media print{
        .headerclass{
            background:#999;
        }
        hr {
            display: block;
            height: 1px;
            background: transparent;
            width: 100%;
            border: none;
            border-top: solid 1px #aaa;
        }
    }
    .litag {
        /* border: 1px solid grey; */
        padding: 6px;
    padding-right: 10px;
    box-shadow: 0px 2px 3px rgb(0 0 0 / 15%);
     list-style-type: none;

    }
    #listing{
        width: 100%;
    background: #fff;
    /* padding: 30px; */
    
    cursor: pointer;
    padding-top: 6px !important;

    }
    .litag:hover {
        font-size: 17px !important;
}


#myButton {
	
    padding: 4px 8px;
  font-size: 15px;
  text-align: center;
  cursor: pointer;
  outline: none;
  color: #fff;
  background-color: #36A693;
  border: none;
  border-radius: 16px;
  box-shadow: 0 1px #999;
    float: right;
    
    margin-right: -105px;
   
}

#myButton:hover {background-color: #36A693}

#myButton:active {
  background-color: #36A693;
  box-shadow: 0 2px #666;
  transform: translateY(2px);
}
.drag{
    margin-left: -39px;
}

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" value='<?= $route_data ?>' id="route_value">

<div class="right_col" >
    {!!Form::open(['name'=>'search_from','id'=>'search_from']) !!}
    @csrf
    


    
    <div id="container" style="position:relative;">
            <div class="container-fluid">


            
                            <div class="row">
                                <div class="col-md-12" id="main">
                                  
                                    <div>
                                        <?php
                                        $i = 1;

                                        foreach ($showSearchFields as $fieldType => $fieldName) {
                                            $fieldTypeArray[$i] = explode('-', $fieldType);

                                            if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                                $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                                ?>

                                            <div class="wrapper">
                                                <div class="input-data">
                                                    <input class="hidden_search" value="" autocomplete="off" type="text"  id="{{$fieldTypeArray[$i][1]}}" name="{{$fieldTypeArray[$i][1]}}" required/>
                                                    
                                                    <div class="col-md-13"><button type="" onclick="checkboxfunc();" id="myButton" style="margin-top:-41px;">Save</button></div>
                                                    <div class="underline">
                                                    <div id="{{$fieldTypeArray[$i][1]}}AjaxDiv" class="ajaxSearchBox"></div>
                                                    <input class="filters" value="{{$hiddenFields[$hidden_filed_id]}}"  type="hidden" name="{{$fieldTypeArray[$i][1]}}_hidden" value="" id="{{$fieldTypeArray[$i][1]}}_hidden">
                                        </div>
                                        
                                        <label>{{$fieldTypeArray[$i][2]}}</label>
                                                </div>
                                                
                                        </div>
                                                <?php
                                            }
                                        } 
                                            $i++;

                                        
                                        ?>
                                        
                                    </div>
                                    
                                </div>
                            </div> 
                            
                    <div id="listing">
                        <div id="note"style="display: block;margin-bottom: -18px;color: black;height: 36px;text-decoration: none;padding-top: 15px;background: #f7f7f7;padding-left: 0px;font-size: 10px;padding-bottom: -1px;"><b>Note:</b> Drag the checkbox and arrange the fields as per your required order.</div>

                                                
                        <ul style=" margin-top: 27px;">
                                                
                                                                                            
                            <div class="drag">
                                @foreach($discharge_summary_list as $key=>$value)
                                    <li class="litag"><input  id="{{$key}}" name="li" type="checkbox" value="{{$key}}">
                                        <label style="width: 98%;" for="{{$key}}">{{$value}}</label></li>
                                @endforeach
                                                            
                                                            
                                                     
                                                            
                            </div>
                            
                        </ul>
                        
                    </div>
                    
                    
            
            
            </div>

    </div>
    
{!! Form::token() !!}
{!! Form::close() !!}


</div>
            
  
 




                               
  


@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>


<script src="{{asset("packages/extensionsvalley/emr/js/summarydisplayorder.js")}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.10.2/Sortable.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



<script>
    const dragArea = document.querySelector(".drag");
    new Sortable(dragArea, {
      animation: 350
    });


    function check_boxes() {
   var tmp = [];
   $("input").each(function() {
    if ($(this).is(':checked')) {
      var checked = ($(this).val());
      tmp.push(checked);
    }
  });
  return tmp;
}






    function checkboxfunc(){
           var tmp = check_boxes();
            
            var x = document.getElementById('doctor_department_hidden').value;
            
            
            
           
            // alert(tmp);
            // alert(x);


           
            
            if(((tmp.length)>0)&&((x.length)>0)){
            
                var url = route_json.summarydisplayorderreport;
                var filters_list = new Object();

                    var check_box = tmp;
                    var data = x;


                filters_list['checkbox'] = check_box; //pushing the value into the list;
                filters_list['department'] = data; //pushing the value into the list;
                   
                   

                    $.ajax({
                        type: "GET",
                        url: url,
                        data: filters_list,
                        
                        success: function (html) {  
                            if(html == 0){

                                Command: toastr["error"]("Something Went Wrong!");
                                
                                
                                return;
                            }else{
                                
                                Command:toastr["success"](" Data Added Successfully!");
                                
                               return;

                            }

                        }
                        
                        
                    

                    });
                    }else{ 
                        if(((tmp.length)<=0)&&((x.length)<=0)){
                            
                            toastr.warning("Please select a depatment and atleast one checkbox !!");
                            event.preventDefault();
                            return;

                        }else if((x.length)<=0){
                           
                            toastr.warning("Please select a department !!");
                            event.preventDefault();
                            return;

                           

                        }else{
                           
                            toastr.warning("'Please select atleast one checkbox!!");
                            event.preventDefault();
                            return;
                            
                            
                        }
                        


                    }
                        
        
    }
    var x= document.getElementById('doctor_department_hidden').value = '';



</script>


@endsection
