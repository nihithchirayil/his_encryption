<div class="row">
    <button class="pull-right btn-primary summary_btn" onclick="showSummary()">Show Summary</button>
    <div class="col-md-12 theadscroll  always-visible" style="padding:0px;display:block" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
        <p style="font-size: 12px;">Total Count<b> : {{ $count }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"> <b>Department wise Cash Collection Report</b>
        </h4>
        <div class="col-md-12 theadscroll" id="input_table" style="padding:0px;">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
                style="font-size: 12px;">
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif;position:sticky;top:0px;">
                    <th width="5%;">Sl. No.</th>
                    <th width="10%;">UHID</th>
                    <th width="10%;">Patient Name</th>
                    <th width="10%;">Doctor Name</th>
                    <th width="10%;">Receipt No.</th>
                    <th width="10%;">Bill No.</th>
                    <th width="10%;">Bill Tag</th>
                    <th width="12%;">Bill Date Time</th>
                    <th width="12%;">Payment Date Time</th>
                    {{-- <th width="5%;">Bill Amt.</th> --}}
                    <th width="5%;">Amt. Adj.</th>
                    <th width="10%;">Tran.No./Card No.</th>
                    <th width="5%;">Payment Mode</th>
                    <th width="5%;">Amt. Coll.</th>
                </tr>
                </thead>
                <?php
                ?>
                <tbody>

                </tbody>
                @if (sizeof($res) > 0)
                    <tbody>
                        @php
                            $i = 0;
                            $bill_total = 0;
                            $coll_total = 0;
                        @endphp

                        @foreach ($res as $key => $value)
                            <tr>
                                <td colspan="13" style="background: rgb(187, 187, 241)">{{ $key }}</td>
                            </tr>
                            @php
                                $j = 1;
                                $bill_amt = 0;
                                $amt_collected = 0;
                            @endphp
                            @foreach ($value as $key1 => $row)
                                <tr>
                                    <td class="td_common_numeric_rules">{{ $key1 + 1 }}</td>
                                    <td class="common_td_rules">{{ $row['uhid'] }}</td>
                                    <td class="common_td_rules">{{ $row['patient_name'] }}</td>
                                    <td class="common_td_rules">{{ $row['doctor_name'] }}</td>
                                    <td class="common_td_rules">{{ $row['recipt_no'] }}</td>
                                    <td class="common_td_rules">{{ $row['bill_no'] }}</td>
                                    <td class="common_td_rules">{{ $row['bill_tag'] }}</td>
                                    <td class="common_td_rules"
                                        title="{{ date('d-M-Y h:i A', strtotime($row['collected_date'])) }}">
                                        {{ date('d-M-Y h:i A', strtotime($row['collected_date'])) }} </td>
                                    <td class="common_td_rules"
                                        title="{{ date('d-M-Y h:i A', strtotime($row['paymentdate'])) }}">
                                        {{ date('d-M-Y h:i A', strtotime($row['paymentdate'])) }} </td>
                                    {{-- <td class="td_common_numeric_rules">{{ $row['bill_net_amount'] }}</td> --}}
                                    <td class="common_td_rules">{{ $row['advance_adjusted'] }}</td>
                                    <td class="common_td_rules">{{ $row['tran_id'] }}</td>
                                    <td class="common_td_rules">{{ $row['payment_mode'] }}</td>
                                    <td class="td_common_numeric_rules">{{ $row['collected_amount'] }}</td>
                                </tr>
                                @php
                                    $bill_amt += $row['bill_net_amount'];
                                    $amt_collected += $row['collected_amount'];
                                @endphp
                            @endforeach
                            <tr>
                                <td colspan="10"></td>
                                {{-- <td class="td_common_numeric_rules"><b>{{ number_format($bill_amt, 2) }}</b></td> --}}
                                <td colspan="2"></td>
                                <td class="td_common_numeric_rules"><b>{{ number_format($amt_collected, 2) }}</b></td>
                            </tr>
                            @php
                                $bill_total += $bill_amt;
                                $coll_total += $amt_collected;
                            @endphp
                        @endforeach
                        <tr style="border-top: 2px solid ">
                            <td colspan="10"></td>
                            {{-- <td class="td_common_numeric_rules"><b>{{ number_format(round($bill_total), 2) }}</b></td> --}}
                            <td class="td_common_numeric_rules" colspan="2"></td>
                            <td><b>{{ number_format(round($coll_total), 2) }}</b></td>
                        </tr>
                @endif
            </table>
        </div>
        <div class="clearfix"></div>

    </div>
    <div class="col-md-4 summary_display" style="display: none">
        <h3 align="center">Payment type wise collection</h3>
        @php
            $grand_ttl=0;
        @endphp
        @foreach ($collect_payment_type as $key => $rows)
            <table id="result_data_table" class="table table-condensed table_sm table-col-bordered theadfix_wrapper"
                   style="font-size: 12px;">
                <thead>
                    <tr class="headerclass" style="background-color: rgb(91, 110, 91); color: white; border-spacing: 0 1em; font-family: sans-serif; position: sticky; top: 0px;">
                        <th width="50%">{{ $key }}</th>
                        <th width="50%">Total Collected Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $total=0;
                    @endphp
                    @foreach ($rows as $set => $row)
                        <tr>
                            <td class="">{{ $set }}</td>
                            <td class="common_td_rules">{{ $row }}</td>
                        </tr>
                        @php
                            $total+=$row ;
                        @endphp
                    @endforeach
                     <tr><td><b>Total</b></td><td class="common_td_rules"><b>{{  $total }}</b></td></tr>
                </tbody>
                @php
                $grand_ttl+=$total;
            @endphp
        @endforeach
        <tr><tr><td><b>Grand Total</b></td><td class="common_td_rules"><b>{{  number_format(round($grand_ttl),2) }}</b></td></tr></tr>
            </table>
           
    </div>
</div>





