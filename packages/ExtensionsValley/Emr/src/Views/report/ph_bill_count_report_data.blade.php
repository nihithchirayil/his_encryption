<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From: <?= date("M-d-Y", strtotime($from))?> To <?= date("M-d-Y", strtotime($to_date))?></p>
            <?php
                $op_bill_count_total = $ip_bill_count_total = $cal_bill_count_total = 0;
                $collect = collect($date_array);
                $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Pharmacy Bill Count Report </b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered table-striped' style="font-size: 12px; width: 100%;">
                <thead>
                    <tr class="headerclass"
                    style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">                   
                        <th style="width: 20%">Date</th>
                        <th style="width: 20%">IP</th>
                        <th style="width: 20%">OP</th>
                        <th style="width: 20%">Casualty</th>                       
                                       
                    </tr>
                    </tr>
                </thead>
                <tbody>
                @if(sizeof($collect) > 0)
                    @foreach ($collect as $key => $value)
                    @php
                        $ph_data = @$res[$value] ? $res[$value] : array();                         
                        $ph_ip_bill_count = @$ph_data[0]->ip_bill_count ? $ph_data[0]->ip_bill_count : 0;                         
                        $ph_op_bill_count = @$ph_data[0]->op_bill_count ? $ph_data[0]->op_bill_count : 0;                         
                        $ph_casuality_bill_count = @$ph_data[0]->casuality_bill_count ? $ph_data[0]->casuality_bill_count : 0;       

                    @endphp
                    <tr style="height: 30px;" style="background-color: rgb(200 223 200);">                    
                        <td class="td_common_numeric_rules" style="text-align:left">{{ date("M-d-Y", strtotime($value)) }}</td>
                        <td class="common_td_rules"> {{ $ph_ip_bill_count }} </td>
                        <td class="common_td_rules">{{ $ph_op_bill_count }} </td>
                        <td class="common_td_rules"> {{ $ph_casuality_bill_count }} </td>                       
                    </tr>

                    <?php
                        $ip_bill_count_total     += $ph_ip_bill_count;
                        $op_bill_count_total     += $ph_op_bill_count;                       
                        $cal_bill_count_total    += $ph_casuality_bill_count;                       
                    ?>

                    @endforeach
                    <tr style="height: 30px; background-color:#d9edf7">
                        <th class="common_td_rules" style="text-align:left;">Total</th>

                        <th class="td_common_numeric_rules">
                            {{ $ip_bill_count_total }}
                        </th>
                        <th class="td_common_numeric_rules">
                            {{ $op_bill_count_total }}
                        </th>
                        <th class="td_common_numeric_rules">
                            {{ $cal_bill_count_total }}
                        </th>                                           

                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
