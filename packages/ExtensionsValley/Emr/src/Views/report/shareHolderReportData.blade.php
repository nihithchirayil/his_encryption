<div class="row">
 
   <div class="col-md-12" id="result_container_div">
     <p style="font-size: 12px;" id="total_data">Report Print Date: <?=date("M-d-Y h:i: A")?> </p>
    <h4 style="text-align: center;margin-top: -29px;" id="heading"> Share Holder Report</h4>
   <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead><tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th>Name</th>
            <th>No of shares</th>
            <th>Status</th>
        </tr>
    </thead>
   <tbody>
    <?php
    if (count($res)!=0) {
        $patient_name='';
        foreach ($res as $data) {
            $holder_status="Active";
            $holder_status_color = " ";
            if ($data->share_holder_status=='0') {
                $holder_status="Inactive";
                $holder_status_color = "background-color:#ea8282";
            }
            if(isset($only_share_holder) && $only_share_holder == 1){
                $bg_style= "border-bottom: 1px solid;";
            }else{
               $bg_style="background-color:rgb(75 113 127);color:white;"; 
            }
            if ($data->patient_name!=$patient_name) {
                $patient_name=$data->patient_name; ?>
                    <tr class="headerclass" style="<?= $bg_style ?> border-spacing: 0 1em;font-family:sans-serif">
                        <th colspan="" class="common_td_rules"  width="80%">{{$data->patient_name}}</th>
                        <th class="common_td_rules">{{$data->no_of_shares}}</th>
                        <th width="10%"></th>
                    </tr>
                    @if(isset($data->beneficiary))
                    <tr style="<?= $holder_status_color ?>">
                        <td width="80%" class="common_td_rules">{{$data->beneficiary}}</td>
                        <td></td>
                        <td width="10%" class="common_td_rules">{{$holder_status}}</td>
                    </tr>
                    @endif
                <?php
            } else {
                ?>
                    @if(isset($data->beneficiary))
                    <tr style="<?= $holder_status_color ?>">
                        <td width="80%" class="common_td_rules">{{$data->beneficiary}}</td>
                        <td></td>
                        <td width="10%" class="common_td_rules">{{$holder_status}}</td>
                    </tr>
                    @endif
                <?php
            }
        }
    }else{
        ?>
        <tr>
            <td colspan="2" style="text-align: center;">No Results Found!</td>
        </tr>
        <?php
    }
        ?>
    
     </tbody>
</table>
</div>
</div>