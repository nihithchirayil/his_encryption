<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Patient Diagonosis Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th style="padding: 2px;" width='10%'>Date</th>
                        <th style="padding: 2px;" width='10%'>Patient Name</th>
                        <th style="padding: 2px;" width='8%'>UHID</th>
                        <th style="padding: 2px;" width='40%'>title name</th>
                    </tr>

                </thead>
                <tbody>
                    @if (count($res) != 0)
                        @foreach ($res as $data)
                            <tr>
                                <td class="td_common_nummeric_rules">{{ date('M-d-Y', strtotime($data->visit_date)) }}
                                </td>
                                <td class="td_common_rules">{{ $data->patient_name }}</td>
                                <td class="td_common_rules">{{ $data->uhid }}</td>
                                <td class="td_common_rules">{{ $data->title_name }}</td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
