<div class="row">
    <div class="col-md-12" id="result_container_popdiv">
        @if(sizeof($res)>0)
        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?=date('M-d-Y h:i A')?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?=$from?> To <?=$to?></p>
        @foreach($res as $data)
        @if ($data == reset($res )) Item Name:
        {{ $data->item_desc }}@endif
    @endforeach
              <?php
                //   $collect = collect($res); $total_records=count($collect);
               ?>
              {{-- <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p> --}}
              <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> </b></h4>
              <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;"><br>
  <thead>
      <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
          <th width="7%" style="padding: 2px; text-align: center; ">Quantity</th>
          <th width="9%" style="padding: 2px; text-align: center; ">Batch no</th>
          <th width="11%" style="padding: 2px; text-align: center; ">Location name</th>
          <th width="10%" style="padding: 2px; text-align: center; ">Selling Price</th>
          <th width="5%" style="padding: 2px; text-align: center; ">Purchase Cost</th>
          <th width="5%" style="padding: 2px; text-align: center; ">Mrp</th>
          <th width="5%" style="padding: 2px; text-align: center; ">Remarks</th>
          <th width="9%" style="padding: 2px; text-align: center; ">Expiry date</th>
          <th width="5%" style="padding: 2px; text-align: center; ">Date</th>
      </tr>

      </thead>
      <tbody>




         @foreach ($res as $data)

        <tr>

              <td class="td_common_numeric_rules">{{$data->total_quantity}}</td>
              <td class="common_td_rules">{{$data->batch}}</td>
              <td class="common_td_rules">{{$data->location_name}}</td>
              <td class="td_common_numeric_rules">{{$data->selling_price}}</td>
              <td class="td_common_numeric_rules">{{$data->unit_purchase_rate}}</td>
              <td class="td_common_numeric_rules">{{$data->unit_mrp}}</td>
              <td class="td_common_numeric_rules">{{$data->return_narration}}</td>
              <td class="common_td_rules">{{ date('M-d-Y',strtotime($data->exp_date))}}</td>
              <td class="common_td_rules">{{$data->created_at}}</td>

        </tr>

         @endforeach

        @else
         <tr>
            <td colspan="8" style="text-align: center;">No Results Found!</td>
        </tr>
         @endif
      </tbody>
  </table>
</div>
<input type="hidden" id="is_print" value="{{$is_print}}">
<input type="hidden" id="is_excel" value="{{$is_excel}}">
</div>
