<style>table, th, td {
    border: 1px solid #dad9d954;
  
  }
  td {
    
    padding-left: 2px;

  }</style>
 
<div class="row">
  @php
  // echo"<pre>";
  //     print_r($res[1]->service_datetime);
  // echo"</pre>";
  //     exit();
  @endphp
  
  <div class="theadscroll always-visible" id="result_container_div" style="position: relative;height:510px;" >
   
     @if(sizeof($res)>0)
     <p style="font-size: 12px;">Report Date: <span>{{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}}</span> </p><br>
     @php
      $collect = collect($res); $total_records=count($collect);   
     @endphp 
     <p style="font-size: 12px;">Total Records<b> : {{$total_records}}</b></p>
     <h2 style="text-align: center;margin-top: -29px;"> Procedure Revenue Report  </h2>
     <table id="result_data_table" class="theadfix_wrapper" style="font-size: 14px !important;font-family: 'robotoregular';cursor: pointer;">

      <thead class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:robotoregular">
        <th  style="padding: 2px;width:10%;text-align: center; ">Sn. No.</th>
        <th  style="padding: 2px;width:10%;text-align: center; ">visit Date</th>
        <th  style="padding: 2px;width:25%;text-align: center; ">Bill No.</th>
        <th  style="padding: 2px;width:25%;text-align: center; ">Doctor Name</th>
         <th  style="padding: 2px;width:25%;text-align: center">Service</th>
         <th  style="padding: 2px;width:15%;text-align: center">Price</th>
         
       
         
   </thead>
      <tbody style="font-size: 12px;">
        @php
            $grand_total=0;
        @endphp
      
          @foreach ($res as $data)
              <tr >
                <td>{{ $loop->iteration}}</td>
                  <td >{{date('M-d-Y',strtotime($data->visit_datetime))}}</td>
                  <td >{{$data->bill_no}}<b>({{$data->patient_name}})</b></td>
                  @php
                  $dog=json_decode($data->doc_name);
                  @endphp
                 
                  <td>
                    <table style="width:100%;border:none;margin-top:-16px;">
                    <tr style="border:none">
                     @foreach ($dog as $item)
                      <td style="border:none"><tr style="border:none"><td style="border:none">{{$item}}</td></tr></td>
                      
                      @endforeach
                      
                     </tr>
                   </table>
                 </td>
                   @php
                   $dug=json_decode($data->detail);
                   @endphp
                  
                   <td>
                     <table style="width:100%;border:none">
                        
                   
                      @foreach ($dug as $item)
                       <td style="border:none"><tr style="border:none"><td style="border:none">{{$item}}</td></tr></td>
                       
                       @endforeach
                       <td style="border:none"><tr style="border: 1px solid#c1b8b8;border-right: none;"><td style="border:none"><b>Total</b></td></tr></td>
                      
                    </table>
                  </td>
                 
                  @php
                  $dig=json_decode($data->pay);
                  $total_pay=0;
                  @endphp
                  <td>
                    <table style="width:100%;border:none">
                       
                  
                    <tr style="border:none">
                     @foreach ($dig as $item)
                      <td style="border:none"><tr style="border:none"><td style="border:none">{{number_format($item,$report_decimal_separator)}}</td></tr></td>
                      @php
                        $total_pay+=$item;
                      @endphp
                      @endforeach
                      <td style="border:none"><tr style="border: 1px solid #c1b8b8;border-left: none;"><td style="border:none"> {{number_format($total_pay,$report_decimal_separator)}}</td></tr></td>
                     </tr>
                   </table>
                 </td>
                   
                </tr>
              @php
                  $grand_total+=$total_pay;
              @endphp
            
          @endforeach
          <tr><td><b>Grand Total:</b></td><td><td><td><td><b>{{number_format($grand_total,$report_decimal_separator)}}</b></td></td></td></td></tr>
         
      </tbody>
          @else 
          <h1 style="text-align: center; color: antiquewhite;">No Record Found</h1>
            @endif   
 </table>
    </div>
 </div>
 