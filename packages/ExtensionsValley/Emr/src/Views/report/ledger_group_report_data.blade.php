<div class="row">
    <div class="col-md-12" id="result_container_div">
<div class="col-md-12 " id="result_container_div" style="width: 100%;">
    <div style="position:relative;">

    <table id="result_data_table" class="table theadfix_wrapper" style="font-size: 12px;">
    @if(sizeof($res)>0)
    
    <p style="font-size: smaller;padding-left: 10px" id="d1">Report Date: 
        <span>
            {{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}}
        </span> 
    </p>
     <br>
   
    <h3 align="center" style="padding_right:3px;"> Ledger Group Report</h4>
   
      <thead>

     
    <tr class="headerclass"
    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
        {{-- <th style="padding: 2px;width: 1%">No</th> --}}
      
        <th style="padding: 2px;width: 35%;">Group</th>
        <th style="padding: 2px;width: 20%;">Ledger</th>
     
    </tr>
</thead>
    @php 
        $sort_array = array();
        $i = 1;
        $sort_change   = 0;
        $user_name='';
    @endphp
      @php $i= 1; @endphp
    @foreach ($res as $data)
         

        @if(!in_array($data->ledger,$sort_array)) 
              @php 
                  $sort_array[] = $data->ledger;
                  $sort_order = true;
              @endphp
        @endif
        @if(!in_array($data->ledger_code,$sort_array)) 
        @php 
            $sort_array[] = $data->ledger_code;
            $sort_order = true;
        @endphp
  @endif

        {{-- @if($user_name != $data->location_name)
            @php $user_name = $data->location_name; @endphp
            <th colspan="2">{{$data->location_name}}</th>
        @endif --}}
     
        @if( isset($sort_order) && ($sort_order==1) )

        <tr>
          <th colspan="1">{{$data->ledger}}</th>
          <th colspan="1">{{$data->ledger_code}}</th>
        </tr>
     
              @php 
                $sort_order = false; 
              @endphp
        @endif           
      
      
        <tr>
            {{-- <td>{{$i++}}</td> --}}
            <td>{{$data->group}}</td>
            <td>{{$data->group_code}}</td>

        
        </tr>
       
    @endforeach

    @else
          No Result Found !!
    @endif
    
    </table>
</div>
</div>
{{-- <div class="clearfix"></div>
<div class="row">
<div class="col-md-12 text-right"  style="padding-top:10px">
        <ul class="outlet_stock_list_pages pagination purple_pagination" style="text-align:right !important;">
                    {!! $paginator->render() !!}
        </ul>
</div>  
</div>  --}}
    <input type="hidden" id="is_print" value="{{$is_print}}">
    <input type="hidden" id="is_excel" value="{{$is_excel}}">
  