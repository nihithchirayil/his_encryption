<div class="row">


    <div class="col-md-12" id="result_container_div" style="min-width:1300px">
        <style>
            .responsive_class {
                max-height: 500px;
                width: 100%;
                overflow: auto;
            }
            #result_data_table {
               /* white-space: nowrap;*/
            }
                    @media print {
                        #result_data_table {
                            white-space: normal;
                            font-size: 9px;
                        }
                        .responsive_class {
                            max-height: none !important; overflow: visible !important;
                        }
                        .bill_date_class{
                            width:100px !important;
                        }
                    }
                </style>
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('Y-m-d h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"> Department Wise Procedure Income Report</h4>

            {{--  <div class="responsive_class">  --}}
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="20px;">#</th>
                        <th class="bill_date_class" style="width:100px !important;">BillDate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</th>
                        <th style="width:10%;">Bill No</th>
                        <th style="width:10%;">Payment Type</th>
                        <th style="width:10%;">UHID</th>
                        <th style="width:10%;" >Patient Name</th>
                        <th style="width:10%;">Type</th>
                        <th style="width:10%;">Pricing</th>
                        <th style="width:10%;" >Bill Tag</th>
                        <th style="width:10%;" >Department</th>
                        <th style="width:10%;" >Sub Department</th>
                        <th style="width:10%;">Item</th>
                        @if($filter_type !=1)
                        <th width="120px;10%">Doctor</th>
                        @else
                        {{--  <th width="120px;">Speciality</th>  --}}
                        @endif
                        <th style="width:10%;">Service Qty</th>
                        <th style="width:10%;">Return Qty</th>
                        <th style="width:10%;">Gross Amount</th>
                        <th style="width:10%;">Discount</th>
                        <th style="width:10%;">Return Amount</th>
                        <th style="width:10%;">Net Amount</th>
                        <th style="width:10%;">Status</th>
                    </tr>
                </thead>
                        <?php
                            $doctor_name = '';
                            $speciality_name = '';
                            $dept_name = '';
                            $i=1;

                            $total_qty = 0;
                            $total_return_qty=0;
                            $total_gross_amount=0.0;
                            $total_discount=0.0;
                            $total_return_amount=0.0;
                            $total_net_amount=0.0;

                            $sub_total_qty = 0;
                            $sub_total_return_qty=0;
                            $sub_total_gross_amount=0.0;
                            $sub_total_discount=0.0;
                            $sub_total_return_amount=0.0;
                            $sub_total_net_amount=0.0;
                        ?>
                <tbody>
                    @if (sizeof($res) > 0)
                        @foreach($res as $data)

                        <!----------doctor wise--------------------------->

                        @if($filter_type ==1)



                            @if($doctor_name != $data->doctor_name)

                                @if($i != 1)
                                    <tr>
                                        <td></td>
                                        <td colspan="11" style="text-align: left;"><b><i>Sub Total</i></b></td>
                                        <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_qty, 2, '.', '')?></i></b></td>
                                        <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_return_qty, 2, '.', '')?></i></b></td>
                                        <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_gross_amount, 2, '.', '')?></i></b></td>
                                        <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_discount, 2, '.', '')?></i></b></td>
                                        <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_return_amount, 2, '.', '')?></i></b></td>
                                        <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_net_amount, 2, '.', '')?></i></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="18">&nbsp;</td>
                                    </tr>
                                @endif
                                <tr><td></td>
                                    <td colspan="7" style='text-align:left;'><b>{{$data->doctor_name}}</b></td>
                                </tr>
                                @php
                                    $sub_total_qty = 0;
                                    $sub_total_return_qty=0;
                                    $sub_total_gross_amount=0.0;
                                    $sub_total_discount=0.0;
                                    $sub_total_return_amount=0.0;
                                    $sub_total_net_amount=0.0;
                                @endphp
                            @endif
                                <tr>
                                    <td class="">{{$i}}</td>
                                    <td class="">{{date('M-d-Y h:i:s',strtotime($data->bill_date))}}</td>
                                    <td class="">{{$data->bill_no}}</td>
                                    <td class="">{{$data->payment_type}}</td>
                                    <td class="">{{$data->uhid}}</td>
                                    <td style="text-align:left;" class="">{{$data->patient_name}}</td>
                                    <td class="">{{$data->visit_status}}</td>
                                    <td class="">{{$data->pricing_name}}</td>
                                    <td style="text-align:left;" class="">{{$data->bill_tag}}</td>
                                    <td style="text-align:left;" class="">{{$data->dept_name}}</td>
                                    <td style="text-align:left;" class="">{{$data->sub_dept_name}}</td>
                                    <td style="text-align:left;" class="">{{$data->item_desc}}</td>

                                    {{--  <td style="text-align:left;" style="text-align:left;" class="">{{$data->speciality}}</td>  --}}
                                    <td class="td_common_numeric_rules">{{$data->qty}}</td>
                                    <td class="td_common_numeric_rules">{{$data->return_qty}}</td>
                                    <td class="td_common_numeric_rules">{{$data->gross_amount}}</td>
                                    <td class="td_common_numeric_rules">{{$data->discount}}</td>
                                    <td class="td_common_numeric_rules">{{$data->return_amount}}</td>
                                    <td class="td_common_numeric_rules">{{$data->net_amount}}</td>
                                    <td class="common_td_rules">{{$data->status}}</td>
                                </tr>
                            @php
                                $i++;
                                $doctor_name = $data->doctor_name;
                                $total_qty += $data->qty;
                                $total_return_qty += $data->return_qty;
                                $total_gross_amount += $data->gross_amount;
                                $total_discount += $data->discount;
                                $total_return_amount += $data->return_amount;
                                $total_net_amount += $data->net_amount;

                                $sub_total_qty += $data->qty;
                                $sub_total_return_qty += $data->return_qty;
                                $sub_total_gross_amount += $data->gross_amount;
                                $sub_total_discount += $data->discount;
                                $sub_total_return_amount += $data->return_amount;
                                $sub_total_net_amount += $data->net_amount;

                            @endphp

                       <!----------Department wise--------------------------->

                       @elseif($filter_type == 3)
                        @if($dept_name != $data->dept_name)

                            @if($i != 1)
                                <tr>
                                    <td></td>
                                    <td colspan="11" style="text-align: left;"><b><i>Sub Total</i></b></td>
                                    <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_qty, 2, '.', '')?></i></b></td>
                                    <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_return_qty, 2, '.', '')?></i></b></td>
                                    <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_gross_amount, 2, '.', '')?></i></b></td>
                                    <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_discount, 2, '.', '')?></i></b></td>
                                    <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_return_amount, 2, '.', '')?></i></b></td>
                                    <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_net_amount, 2, '.', '')?></i></b></td>
                                </tr>
                                <tr>
                                    <td colspan="18">&nbsp;</td>
                                </tr>
                            @endif
                            <tr><td></td>
                                <td colspan="7" style='text-align:left;'><b>{{$data->dept_name}}</b></td>
                            </tr>
                            @php
                                $sub_total_qty = 0;
                                $sub_total_return_qty=0;
                                $sub_total_gross_amount=0.0;
                                $sub_total_discount=0.0;
                                $sub_total_return_amount=0.0;
                                $sub_total_net_amount=0.0;
                            @endphp
                        @endif
                            <tr>
                                <td class="">{{$i}}</td>
                                <td class="">{{date('M-d-Y',strtotime($data->bill_date))}}</td>
                                <td class="">{{$data->bill_no}}</td>
                                <td class="">{{$data->payment_type}}</td>
                                <td class="">{{$data->uhid}}</td>
                                <td style="text-align:left;" class="">{{$data->patient_name}}</td>
                                <td class="">{{$data->visit_status}}</td>
                                <td class="">{{$data->pricing_name}}</td>
                                <td style="text-align:left;" class="">{{$data->bill_tag}}</td>
                                <td style="text-align:left;" class="">{{$data->dept_name}}</td>
                                <td style="text-align:left;" class="">{{$data->sub_dept_name}}</td>
                                <td style="text-align:left;" class="">{{$data->item_desc}}</td>

                                <td style="text-align:left;" style="text-align:left;" class="">{{$data->speciality}}</td>
                                <td class="td_common_numeric_rules">{{$data->qty}}</td>
                                <td class="td_common_numeric_rules">{{$data->return_qty}}</td>
                                <td class="td_common_numeric_rules">{{$data->gross_amount}}</td>
                                <td class="td_common_numeric_rules">{{$data->discount}}</td>
                                <td class="td_common_numeric_rules">{{$data->return_amount}}</td>
                                <td class="td_common_numeric_rules">{{$data->net_amount}}</td>
                                <td class="common_td_rules">{{$data->status}}</td>
                            </tr>
                        @php
                            $i++;
                            $doctor_name = $data->doctor_name;
                            $dept_name = $data->dept_name;
                            $total_qty += $data->qty;
                            $total_return_qty += $data->return_qty;
                            $total_gross_amount += $data->gross_amount;
                            $total_discount += $data->discount;
                            $total_return_amount += $data->return_amount;
                            $total_net_amount += $data->net_amount;

                            $sub_total_qty += $data->qty;
                            $sub_total_return_qty += $data->return_qty;
                            $sub_total_gross_amount += $data->gross_amount;
                            $sub_total_discount += $data->discount;
                            $sub_total_return_amount += $data->return_amount;
                            $sub_total_net_amount += $data->net_amount;

                        @endphp

                        <!----------speciality wise--------------------------->
                        @else

                        @if($speciality_name != $data->speciality)

                        @if($i != 1)
                            <tr>
                                <td></td>
                                <td colspan="12" style="text-align: left;"><b><i>Sub Total</i></b></td>
                                <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_qty, 2, '.', '')?></i></b></td>
                                <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_return_qty, 2, '.', '')?></i></b></td>
                                <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_gross_amount, 2, '.', '')?></i></b></td>
                                <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_discount, 2, '.', '')?></i></b></td>
                                <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_return_amount, 2, '.', '')?></i></b></td>
                                <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_net_amount, 2, '.', '')?></i></b></td>
                            </tr>
                            <tr>
                                <td colspan="20">&nbsp;</td>
                            </tr>
                        @endif
                        <tr><td></td>
                            <td colspan="7" style='text-align:left;'><b>{{$data->speciality}}</b></td>
                        </tr>
                        @php
                            $sub_total_qty = 0;
                            $sub_total_return_qty=0;
                            $sub_total_gross_amount=0.0;
                            $sub_total_discount=0.0;
                            $sub_total_return_amount=0.0;
                            $sub_total_net_amount=0.0;
                        @endphp
                    @endif
                        <tr>
                            <td class="">{{$i}}</td>
                            <td class="">{{date('M-d-Y',strtotime($data->bill_date))}}</td>
                            <td class="">{{$data->bill_no}}</td>
                            <td class="">{{$data->payment_type}}</td>
                            <td class="">{{$data->uhid}}</td>
                            <td style="text-align:left;" class="">{{$data->patient_name}}</td>
                            <td class="">{{$data->visit_status}}</td>
                            <td style="text-align:left;" class="">{{$data->pricing_name}}</td>
                            <td style="text-align:left;" class="">{{$data->bill_tag}}</td>
                            <td style="text-align:left;" class="">{{$data->dept_name}}</td>
                            <td style="text-align:left;" class="">{{$data->sub_dept_name}}</td>
                            <td style="text-align:left;" class="">{{$data->item_desc}}</td>
                            <td style="text-align:left;" class="">{{$data->doctor_name}}</td>
                            <td class="td_common_numeric_rules">{{$data->qty}}</td>
                            <td class="td_common_numeric_rules">{{$data->return_qty}}</td>
                            <td class="td_common_numeric_rules">{{$data->gross_amount}}</td>
                            <td class="td_common_numeric_rules">{{$data->discount}}</td>
                            <td class="td_common_numeric_rules">{{$data->return_amount}}</td>
                            <td class="td_common_numeric_rules">{{$data->net_amount}}</td>
                            <td class="common_td_rules">{{$data->status}}</td>
                        </tr>
                    @php
                        $i++;
                        $speciality_name = $data->speciality;
                        $total_qty += $data->qty;
                        $total_return_qty += $data->return_qty;
                        $total_gross_amount += $data->gross_amount;
                        $total_discount += $data->discount;
                        $total_return_amount += $data->return_amount;
                        $total_net_amount += $data->net_amount;

                        $sub_total_qty += $data->qty;
                        $sub_total_return_qty += $data->return_qty;
                        $sub_total_gross_amount += $data->gross_amount;
                        $sub_total_discount += $data->discount;
                        $sub_total_return_amount += $data->return_amount;
                        $sub_total_net_amount += $data->net_amount;
                    @endphp





                        @endif
                        @endforeach
                        <tr class="bg-info" style="height: 30px;">
                            <th @if($filter_type !=1) colspan="13" @else colspan="12" @endif style="text-align: left;"  class="common_td_rules">Total</th>
                            <th class="td_common_numeric_rules">{{$total_qty}}</th>
                            <th class="td_common_numeric_rules">{{$total_return_qty}}</th>
                            <th class="td_common_numeric_rules">{{number_format((float)$total_gross_amount, 2, '.', '')}}</th>
                            <th class="td_common_numeric_rules">{{number_format((float)$total_discount, 2, '.', '')}}</th>
                            <th class="td_common_numeric_rules">{{number_format((float)$total_return_amount, 2, '.', '')}}</th>
                            <th class="td_common_numeric_rules">{{number_format((float)$total_net_amount, 2, '.', '')}}</th>
                            <th></th>
                        </tr>
                    @else
                    <tr>
                        <td  colspan="20" style="text-align: center;">No Results Found!</td>
                    </tr>
                    @endif
                </tbody>

            </table>
            {{--  </div>  --}}

    </div>
</div>
