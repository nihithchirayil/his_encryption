@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
<input type="hidden" id="hospital_address" value="{{ $hospital_address }}">
<input type="hidden" value='<?= $route_data ?>' id="route_value">
<input type="hidden" value='<?= date('M-d-Y') ?>' id="current_date">
<div class="right_col">
<div class="container-fluid">
 <div class="col-md-12 padding_sm">
    <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
        <div class="box no-border no-margin">
            <div class="box-body" style="padding-bottom:15px;">
                <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                    <thead><tr class="table_header_bg">
                        <th colspan="11"><b>Department Wise Collection Summary</b>
                        </th>
                        </tr>
                    </thead>
                </table>
                <div class="row padding_sm">
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">From Date</label>
                            <input type="text" data-attr="date" autocomplete="off" id="summary_date_from"
                                autofocus="" value="{{date('M-d-Y')}}" class="form-control date-picker"
                                placeholder="YYYY-MM-DD">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">To Date</label>
                            <input type="text" data-attr="date" autocomplete="off" id="summary_date_to"
                                autofocus="" value="{{date('M-d-Y')}}" class="form-control date-picker"
                                placeholder="YYYY-MM-DD">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Payment Type</label>
                            <div class="clearfix"></div>
                            <select class='form-control select2' id="payment_type" name="payment_type">
                                <option value="">All</option>
                                <option value="cash/Card">Cash/Card</option>
                                <option value="insurance">Insurance</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Paid Status</label>
                            <div class="clearfix"></div>
                            <select class='form-control select2' id="paid_status" name="paid_status">
                                <option value="">All</option>
                                <option value="1">Paid</option>
                                <option value="0">Not Paid</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Doctor</label>
                            <div class="clearfix"></div>
                            {!! Form::select('doctor', $doctors, 0, ['class' => 'form-control select2','placeholder'=>'', 
                            'placeholder'=>'All', 'id' =>'doctor', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 padding_sm date_filter_div" style="margin-top: 10px;">
                        <div class="col-md-1 pull-right padding_sm">
                            <button type="button" class="btn light_purple_bg btn-block"
                                onclick="getSummaryData();" name="search_results" id="search_results">
                                <i id="searchresultspin" class="fa fa-search" aria-hidden="true"></i>
                                Search
                            </button>
                        </div>
                        <div class="col-md-1 pull-right padding_sm">
                            <a class="btn light_purple_bg btn-block disabled"
                                onclick="exceller_template('DEPARTMENT WISE COLLECTION SUMMARY',5);" name="csv_results"
                                id="csv_results">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                Excel
                            </a>
                        </div>
                        <div class="col-md-1 pull-right padding_sm">
                            <a class="btn light_purple_bg btn-block disabled" onclick="printReportData();"
                                name="print_results" id="print_results">
                                <i class="fa fa-print" aria-hidden="true"></i>
                                Print
                            </a>
                        </div>
                        <div class="col-md-1 pull-right padding_sm">
                            <button type="reset" class="btn light_purple_bg btn-block"
                                onclick="resetFilter();" name="clear_results" id="clear_results">
                                <i class="fa fa-refresh" aria-hidden="true"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
 </div>





    <div class="col-md-12 padding_sm">
        <div id="ResultDataContainer" style="max-height: 650px; padding: 10px 10px 10px 0px; display:none;font-family:poppinsregular">
            <div style="background:#686666;">
            <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);
            width: 100%;
            padding: 30px;" id="ResultsViewArea">

            </page>
            </div>
        </div>
    </div>
  </div>
</div>

<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:rgb(91 110 91);">
          <h4 class="modal-title" style="color: white;">Print Configuration</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="height:120px;">
            <div class="col-md-12">
                <span style="margin-right: 5%;">Print Mode :</span>
                <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
            </div>
            <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                Include Hospital Header
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                    <i class="fa fa-print" aria-hidden="true"></i> Print
                </button>
            </div>
        </div>
      </div>
    </div>
  </div>

@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/js/department_wise_collection_summary.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/report_summary_csvprint.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js') }}"></script>
</script>
@endsection
