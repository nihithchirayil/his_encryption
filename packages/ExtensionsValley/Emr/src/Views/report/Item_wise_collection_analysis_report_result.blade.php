<style>table, th, td {
    border: 1px solid #dad9d954;
  
  }
  td {
    
    padding-left: 2px;

  }</style>
 
<div class="row">
    
   
  
  <div class="theadscroll always-visible" id="result_container_div" style="position: relative;height:510px;" >
   
     @if(sizeof($res)>0)
     <p style="font-size: 12px;" id="total_data">Report Date: {{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}} </p><br>
     @endif
     @php
     $collect = collect($res); $total_records=count($collect);   
    @endphp 
    <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
    <h2 style="text-align: center;margin-top: -29px;" id="heading"> Item Wise Collection Analysis Report  </h2>
     
            @if(sizeof($res)>0)
            <table id="result_data_table" class="theadfix_wrapper" style=" width:100%;font-size: 14px !important;font-family: 'robotoregular';cursor: pointer;">
            
                <thead class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:robotoregular">
                    <th  style="padding: 2px;width:5%;text-align: center; ">Sn. No.</th>
                    <th  style="padding: 2px;width:30%;text-align: center;border:2px; ">Doctor Name	</th>
                     <th  style="padding: 2px;width:50%;text-align: center">Item name</th>
                     <th  style="padding: 2px;width:15%;text-align: center">Quantity</th>
                </thead>
                  <tbody style="font-size: 12px;">
                    @php
                        $total=0
                    @endphp
                   
                      @foreach ($res as $data)
                        <tr >
                            <td>{{ $loop->iteration}}.</td>
                            <td >{{$data->consulting_doctor_name}}</td>
                            <td >{{$data->item_desc}}</td>
                            <td > {{number_format($data->qty,$report_decimal_separator)}}</td>
                        </tr>
                        @php
                            $total+=$data->qty;

                        @endphp
                    @endforeach
                    <tr><td colspan="3"><b>Total Quantity</b></td><td><b>{{number_format($total,$report_decimal_separator)}}</b></td></tr>
                    </tbody>
                    @else 
                      <h1 style="text-align: center; color: antiquewhite;">No Record Found</h1>
                    @endif   
             </table>
     
    </div>
 </div>
 