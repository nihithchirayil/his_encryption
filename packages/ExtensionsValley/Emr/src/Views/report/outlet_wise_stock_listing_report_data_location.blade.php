<div class="row">
    @php
        $locationDataArray = [];
        $locationStock = [];
        $locationNameArray = [];
        $locationNameArrayD = [];

        $jj = 0;
    @endphp
    @if (count($res) != 0)
        @foreach ($res as $data)
            @php $locationArray[$data->loc_id][$data->item_code] = $data->stock; @endphp

            @php $locationNameArray[$data->loc_id] = $data->location_name; @endphp
        @endforeach
        @php
            $locationNameArray = array_unique($locationNameArray);
            $locationNameArrayD = array_unique(array_keys($locationNameArray));
        @endphp
        @if ($batch_wise == 1)
        @php $locArray =[]; @endphp
        @php $item_b = $item_c=''; @endphp
                @foreach ($res as $rs)

                @foreach ($locationNameArray as $key => $val)
                    @if ($rs->loc_id == $key )
                     @if($rs->batch_no != $item_b )
                     @php $item_b = $rs->batch_no;
                          $item_c = $rs->item_code @endphp
                        @php
                            $batchArray[] = array('category' => $rs->category,
                            'subcategory_name' => $rs->subcategory_name,
                            'item_desc' => $rs->item_desc,
                            'hsn_code'=> $rs->hsn_code,
                            'vendor_name'=> $rs->vendor_name,
                            'doctor_name' => $rs->doctor_name,
                            'manufacturer' => $rs->manufacturer,
                            'rack' => $rs->rack,
                            'batch_no' => $rs->batch_no,
                            'expiry_date' => $rs->expiry_date,
                            'intransit_qty' => $rs->intransit_qty,
                            'unit_purchase_cost' => $rs->unit_purchase_cost,
                            'unit_mrp' => $rs->unit_mrp,
                            'unit_purchase_cost' => $rs->unit_purchase_cost,
                            'unit_selling_price' => $rs->unit_selling_price,
                             'loc_id' => $key,
                             'item_code'=> $rs->item_code,
                            );

                        @endphp
                        @endif
                        {{-- @php  $locArray[$key][$rs->item_code][$rs->batch_no] = $rs->stock; @endphp --}}
                    @else
                    {{-- @php $locArray[$key][$rs->item_code][$rs->batch_no] = 0; @endphp --}}
                    @endif
                @endforeach
            @endforeach
            {{-- @foreach ($locationNameArray as $key) --}}
            @foreach ($res as $rs)
                @if (array_key_exists($rs->loc_id,$locationNameArray) )
                    @php  $locArray[$rs->loc_id][$rs->item_code][$rs->batch_no] = $rs->stock; @endphp
                @else
                    @php $locArray[$rs->loc_id][$rs->item_code][$rs->batch_no] = 0; @endphp
                @endif
            @endforeach
        {{-- @endforeach --}}





        @endif
    @endif
{{-- @php dd($locArray) @endphp --}}
    <div class="col-md-12 padding_sm" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <h4 style="text-align: center" id="heading"> <b>Outlet Wise Stock List</b></h4>
        <div class="wrapper theadscroll" style="position: relative; height: 800px;" id="resltData">
            <table class="table no-margin table_sm theadfix_wrapper table-striped no-border table-condensed"
                id="result_data_table">
                <thead>
                    <tr class="table_header_bg"
                        style="background-color: rgb(227 227 227);color: #0c0c0c;border-spacing: 0 1em;font-family:sans-serif">
                        <th style="width: 3%">SI.No.</th>


                        <th style="width: 7%;">Category</th>
                        <th style="width: 7%;">Sub Category</th>
                        <th style="width:15%;">Item Description</th>
                        <th style="width: 7%;">HSN Code</th>
                        <th style="width: 9%">Vendor Name</th>
                        <th style="width: 8%;">Doctor</th>
                        <th style="width: 5%;">Rack</th>
                        <th style="width: 8%;">Manufacturer</th>
                        @if ($batch_wise == 1)
                            <th style="width: 10%;">Batch No</th>
                            <th style="width: 10%;">Expiry Date</th>
                        @endif
                        <th style="width: 3%;">Intransit Qty</th>
                        {{-- <th style="width: 3%;">Stock</th> --}}
                        <th style="width: 10%;">Unit Cost</th>
                        <th style="width: 7%;">MRP</th>
                        <th style="width: 7%;">Price</th>
                        @if (sizeof($locationNameArray) > 0)
                            @foreach ($locationNameArray as $loc)
                                <th style="width: 3%">{{ $loc }}</th>
                            @endforeach
                        @endif
                        @php
                            $span = 15;
                        @endphp
                </thead>
                </tr>

                @php
                    $sort_array = [];
                    $i = 0;
                    $sort_change = 0;
                    $user_name = $item_c = $item_b = '';
                    $intransit_qty = 0;
                    $stock = 0;
                    $unit_purchase_cost = 0;
                    $unit_mrp = 0;
                    $unit_selling_price = 0;
                    $cols_span = 9;
                    $locationStock = [];
                    $sort_order = false;
                @endphp
                @if ($batch_wise != 1)
                    @if (count($res) != 0)
                        @foreach ($res as $data)


                            @if ($data->item_code != $item_c)
                                @php
                                    $item_c = $data->item_code;
                                    $intransit_qty += floatval($data->intransit_qty);
                                    $stock += floatval($data->stock);
                                    $unit_purchase_cost += floatval($data->unit_purchase_cost);
                                    $unit_mrp += floatval($data->unit_mrp);
                                    $unit_selling_price += floatval($data->unit_selling_price);
                                @endphp
                                <tr onclick="getGrnDetails('{{ $data->item_code }}')" style="cursor:pointer"
                                    class="tr_hover">
                                    <td class="td_common_numeric_rules">{{ ++$i }}</td>
                                    <td class="common_td_rules" title="{{ $data->category }}">{{ $data->category }}</td>
                                    <td class="common_td_rules" title="{{ $data->subcategory_name }}">
                                        {{ $data->subcategory_name }}</td>
                                    <td class="common_td_rules" title="{{ $data->item_desc }}">{{ $data->item_desc }}
                                    </td>
                                    <td class="common_td_rules" title="{{ $data->hsn_code }}">{{ $data->hsn_code }}
                                    </td>
                                    <td class="common_td_rules" title="{{ $data->vendor_name }}">
                                        {{ $data->vendor_name }}
                                    </td>
                                    <td class="common_td_rules"
                                        title="{{ $data->doctor_name ? $data->doctor_name : '-' }}">
                                        {{ $data->doctor_name ? $data->doctor_name : '-' }}</td>
                                    <td class="td_common_numeric_rules">{{ $data->rack ? $data->rack : '-' }}</td>
                                    <td class="common_td_rules"
                                        title="{{ $data->manufacturer ? $data->manufacturer : '-' }}">
                                        {{ $data->manufacturer ? $data->manufacturer : '-' }}</td>
                                    @if ($batch_wise == 1)
                                        @php $cols_span=11; @endphp
                                        <td class="common_td_rules">{{ $data->batch_no }}</td>
                                        <td class="common_td_rules">{{ $data->expiry_date }}</td>
                                    @endif
                                    <td class="td_common_numeric_rules">{{ $data->intransit_qty }}</td>
                                    {{-- <td class="td_common_numeric_rules">{{ $data->stock }}</td> --}}
                                    <td class="td_common_numeric_rules">
                                        {{ number_format($data->unit_purchase_cost, 2, '.', '') }}
                                    <td class="td_common_numeric_rules">
                                        {{ number_format($data->unit_mrp, 2, '.', '') }}
                                    </td>
                                    <td class="td_common_numeric_rules">
                                        {{ number_format($data->unit_selling_price, 2, '.', '') }}</td>
                                    @if (sizeof($locationNameArray) > 0)
                                        @php $j = 0; @endphp
                                        @foreach ($locationNameArray as $key => $value)
                                            @if (isset($location_exsit) && $locationNameArrayD[$j] == $location_exsit)
                                                @php $bg_color = '#c6c6ff'; @endphp
                                            @else
                                                @php $bg_color = ''; @endphp
                                            @endif

                                            @if (array_key_exists($data->item_code, $locationArray[$key]))
                                                <td style="background-color: {{ $bg_color }}">
                                                    {{ $locationArray[$key][$data->item_code] }}</td>
                                                @php $locationStock[$locationNameArrayD[$j]] = @$locationStock[$locationNameArrayD[$j]] ? $locationStock[$locationNameArrayD[$j]] +$locationArray[$key][$data->item_code] : $locationArray[$key][$data->item_code]; @endphp
                                            @else
                                                <td style="background-color: {{ $bg_color }}"> 0 </td>
                                                @php $locationStock[$locationNameArrayD[$j]] = @$locationStock[$locationNameArrayD[$j]] ? $locationStock[$locationNameArrayD[$j]] + 0 : 0 ; @endphp
                                            @endif
                                            @php $j++; @endphp
                                        @endforeach
                                    @endif
                                </tr>
                            @endif

                        @endforeach
                    @endif
                @else
                @php $j = 0; $itm_b = '';$m=1; $cols_span = 11;@endphp
                    @foreach ($batchArray as $ba=>$s)
                    @php
                    $item_c = $data->item_code;
                    $intransit_qty += floatval($s['intransit_qty']);
                    $stock += floatval($data->stock);
                    $unit_purchase_cost += floatval($s['unit_purchase_cost']);
                    $unit_mrp += floatval($s['unit_mrp']);
                    $unit_selling_price += floatval($s['unit_selling_price']);
                    $s_item_code = $s['item_code'];
                    @endphp
                       <tr onclick="getGrnDetails('{{ $s_item_code }}')" style="cursor:pointer"
                        class="tr_hover">
                            <td>{{ $m }}</td>
                            <td class="common_td_rules">{{ $s['category'] }}</td>
                            <td class="common_td_rules">{{ $s['subcategory_name'] }}</td>
                            <td class="common_td_rules" title="{{ $s['item_desc'] }}">{{ $s['item_desc'] }}</td>
                            <td class="common_td_rules">{{ $s['hsn_code'] }}</td>
                            <td class="common_td_rules">{{ $s['vendor_name'] }}</td>
                            <td class="common_td_rules">{{ $s['doctor_name'] }}</td>
                            <td class="common_td_rules">{{ $s['rack'] }}</td>
                            <td class="common_td_rules">{{ $s['manufacturer'] }}</td>
                            <td class="common_td_rules">{{ $s['batch_no'] }}</td>
                            <td class="common_td_rules">{{ $s['expiry_date'] }}</td>
                            <td class="td_common_numeric_rules">{{ $s['intransit_qty'] }}</td>
                            <td class="td_common_numeric_rules">{{ $s['unit_purchase_cost'] }}</td>
                            <td class="td_common_numeric_rules">{{ $s['unit_mrp'] }}</td>
                            <td class="td_common_numeric_rules">{{ $s['unit_selling_price'] }}</td>
                            @php $m = 0 ;@endphp
                            @foreach($locArray as $k => $vl)
                            @if (isset($location_exsit) && $locationNameArrayD[$m] == $location_exsit)
                            @php $bg_color = '#c6c6ff'; @endphp
                        @else
                            @php $bg_color = ''; @endphp
                        @endif
                                @if(isset($vl[$s['item_code']][$s['batch_no']]))
                            <td class="td_common_numeric_rules" style="background-color: {{ $bg_color }}">{{ $vl[$s['item_code']][$s['batch_no']] }}</td>
                            @php $locationStock[$locationNameArrayD[$m]] = @$locationStock[$locationNameArrayD[$m]] ? $locationStock[$locationNameArrayD[$m]] +$vl[$s['item_code']][$s['batch_no']] : $vl[$s['item_code']][$s['batch_no']]; @endphp
                            @else
                            <td class="td_common_numeric_rules" style="background-color: {{ $bg_color }}">0</td>
                            @php $locationStock[$locationNameArrayD[$m]] = @$locationStock[$locationNameArrayD[$m]] ? $locationStock[$locationNameArrayD[$m]] + 0 : 0; @endphp
                            @endif
                            @php $m++; @endphp
                            @endforeach
                        </tr>
                        @php $j++;@endphp
                        {{-- @endif --}}
                    @endforeach
                @endif
                <tr>
                    <th colspan="{{ $cols_span }}">Total</th>
                    <td class="td_common_numeric_rules">{{ number_format($intransit_qty, 2, '.', '') }}</td>
                    {{-- <td class="td_common_numeric_rules">{{ number_format($stock, 2, '.', '') }}</td> --}}
                    <td class="td_common_numeric_rules">{{ number_format($unit_purchase_cost, 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($unit_mrp, 2, '.', '') }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($unit_selling_price, 2, '.', '') }}</td>
                    @foreach ($locationStock as $key => $value)
                        <td class="td_common_numeric_rules">{{ number_format($value, 2, '.', '') }}</td>
                    @endforeach
                </tr>

            </table>
        </div>
    </div>
</div>
