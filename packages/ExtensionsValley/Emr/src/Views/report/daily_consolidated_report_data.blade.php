<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From: <?= date("M-d-Y", strtotime($from))?> To
                <?= date("M-d-Y", strtotime($to_date))?></p>
            <?php
                $op_total = $admitting_total = $discharge_total = $occupied_total = $op_bill_count_total = $op_bill_sum_total=$ip_bill_count_total = 0;
                $ip_bill_sum_total = $ext_bill_count_total =$ext_bill_sum_total = $lab_bill_count_total = $lab_ext_bill_count_total = $xray_bill_count_total = $usg_bill_count_total =0;
                $ecg_bill_count_total = 0;
                
                $collect = collect($date_array);
                $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Daily Consolidated Report </b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered table-striped'
                style="font-size: 12px; width: 100%">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th colspan='5'>Reception</th>
                        <th colspan='3'>Pharmacy</th>
                        <th colspan='2'>Radiology</th>
                        <th colspan='1'>Others</th>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='2%'>Date</th>
                        <th width='2%'>OP Patients</th>
                        <th width='2%'>Admissions</th>
                        <th width='2%'>Discharges</th>
                        <th width='2%'>Current Occupancy</th>
                        <th width='3%'>OP </th>
                        <!-- <th width='2%'>OP Amount</th> -->
                        <th width='3%'>IP</th>
                        <!-- <th width='2%'>IP Amount</th> -->
                        <th width='3%'>Ext. Patient</th>
                        <!-- <th width='5%'>Ext. Amount</th> -->
                        <th width='7%'>X-Ray</th>
                        <th width='7%'>USG</th>
                        <th width='6%'>ECG</th>
                    </tr>
                </thead>
                <tbody>
                    @if(sizeof($collect) > 0)
                    @foreach ($collect as $key => $value)
                    @php
                    $result_data_array = @$res[$value] ? $res[$value] : array();
                    $ip_visit_count = $op_visit_count = $discharge_visit_count = $occupied_visit_count = 0;
                    if (!empty($result_data_array)) {
                        foreach($result_data_array as $val) {
                            if ($val->visit_type == 'IP') {
                                $ip_visit_count = @$val->total_visit_count ? $val->total_visit_count : 0;
                            }else if ($val->visit_type == 'OP') {
                                $op_visit_count = @$val->total_visit_count ? $val->total_visit_count : 0;
                            }else if ($val->visit_type == 'Discharge') {
                                $discharge_visit_count = @$val->total_visit_count ? $val->total_visit_count : 0;
                            }else if ($val->visit_type == 'Occupied') {
                                $occupied_visit_count = @$val->total_visit_count ? $val->total_visit_count : 0;
                            }
                        }
                    } 

                    $res_ph_billArray = @$res_ph_bill_count[$value] ? $res_ph_bill_count[$value] : array();
                    $ip_bill_sum      = @$res_ph_billArray[0]->ip_bill_sum ? $res_ph_billArray[0]->ip_bill_sum : 0;
                    $op_bill_sum      = @$res_ph_billArray[0]->op_bill_sum ? $res_ph_billArray[0]->op_bill_sum : 0;
                    $ext_bill_sum     = @$res_ph_billArray[0]->ext_bill_sum ? $res_ph_billArray[0]->ext_bill_sum : 0;
                    $ip_bill_count    = @$res_ph_billArray[0]->ip_bill_count ? $res_ph_billArray[0]->ip_bill_count : 0;
                    $op_bill_count    = @$res_ph_billArray[0]->op_bill_count ? $res_ph_billArray[0]->op_bill_count : 0;
                    $ext_bill_count   = @$res_ph_billArray[0]->ext_bill_count ? $res_ph_billArray[0]->ext_bill_count : 0;
                    $lab_bill_count   = @$res_ph_billArray[0]->lab_sum ? $res_ph_billArray[0]->lab_sum : 0;
                    $lab_ext_bill_count   = @$res_ph_billArray[0]->lab_ext ? $res_ph_billArray[0]->lab_ext : 0;
                    $xray_bill_count   = @$res_ph_billArray[0]->xray ? $res_ph_billArray[0]->xray : 0;
                    $usg_bill_count   = @$res_ph_billArray[0]->usg ? $res_ph_billArray[0]->usg : 0;
                    $ecg_bill_count   = @$res_ph_billArray[0]->ecg ? $res_ph_billArray[0]->ecg : 0;
                    

                    @endphp
                    <tr style="height: 30px;" style="background-color: rgb(200 223 200);">
                        <td class="td_common_numeric_rules" style="text-align:left; width:10%;"> {{ date("M-d-Y", strtotime($value)) }} </td>
                        <td class="common_td_rules drill_down_data" style="cursor: pointer; width: 10%;"> {{ $op_visit_count }}
                            @if($op_visit_count > 0)
                            <table class='table table-condensed table_sm table-col-bordered hidden' style="font-size: 12px; display: none;">
                                <thead>
                                    <tr class="headerclass"
                                        style="background-color:rgb(185 237 227);color:black;border-spacing: 0 1em;font-family:sans-serif">
                                        <th class="common_td_rules" style="width: 2%"> Doctor </th>
                                        <th class="common_td_rules" style="width: 1%">Count</th>
                                        <th class="common_td_rules" style="width: 1%">Registration</th>
                                        <th class="common_td_rules" style="width: 1%">Renewal</th>
                                        <th class="common_td_rules" style="width: 1%">Free Visit</th>
                                    </tr>
                                </thead>
                                <tbody style="border: 1px solid #CCC;">
                                    @if (!empty($result_data_array)) 
                                    @foreach ($result_data_array as $key => $val)
                                    @if($val->visit_type == 'OP') 
                                    <tr style="height: 30px;" style="background-color: rgb(200 223 200);">
                                        <td class="td_common_numeric_rules" style="text-align:left">
                                            {{  $val->doctor_name }} </td>
                                        <td class="td_common_numeric_rules"> {{ $val->visit_count }} </td>
                                        <td class="td_common_numeric_rules"> {{ @$val->registration_count ? $val->registration_count : 0}} </td>
                                        <td class="td_common_numeric_rules"> {{ @$val->renewal_count ? $val->renewal_count : 0 }} </td>
                                        <td class="td_common_numeric_rules"> {{ @$val->free_visit_count ? $val->free_visit_count : 0}} </td>
                                    <tr>
                                    @endif
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            @endif
                        </td>

                        <td class="common_td_rules drill_down_data" style="cursor: pointer; width: 10%;"> {{ $ip_visit_count }}
                            @if($ip_visit_count > 0)
                            <table class='table table-condensed table_sm table-col-bordered hidden' style="font-size: 12px; display: none;">
                                <thead>
                                    <tr class="headerclass"
                                        style="background-color:rgb(185 237 227);color:black;border-spacing: 0 1em;font-family:sans-serif">
                                        <th class="common_td_rules" style="width: 6%"> Doctor </th>
                                        <th class="common_td_rules" style="width: 3%">Count</th>
                                    </tr>
                                </thead>
                                <tbody style="border: 1px solid #CCC;">
                                    @if (!empty($result_data_array)) 
                                    @foreach ($result_data_array as $key => $val)
                                    @if($val->visit_type == 'IP') 
                                    <tr style="height: 30px;" style="background-color: rgb(200 223 200);">
                                        <td class="td_common_numeric_rules" style="text-align:left">
                                            {{  $val->doctor_name }} </td>
                                        <td class="td_common_numeric_rules"> {{ $val->visit_count }} </td>
                                    <tr>
                                    @endif
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            @endif
                        </td>

                        <td class="common_td_rules drill_down_data" style="cursor: pointer; width: 10%;"> {{ $discharge_visit_count }}
                            @if(intval($discharge_visit_count) > 0)
                            <table class='table table-condensed table_sm table-col-bordered hidden' style="font-size: 12px; display: none;">
                                <thead>
                                    <tr class="headerclass"
                                        style="background-color:rgb(185 237 227);color:black;border-spacing: 0 1em;font-family:sans-serif">
                                        <th class="common_td_rules" style="width: 6%"> Doctor </th>
                                        <th class="common_td_rules" style="width: 3%">Count</th>
                                    </tr>
                                </thead>
                                <tbody style="border: 1px solid #CCC;">
                                    @if (!empty($result_data_array)) 
                                    @foreach ($result_data_array as $key => $val)
                                    @if($val->visit_type == 'Discharge') 
                                    <tr style="height: 30px;" style="background-color: rgb(200 223 200);">
                                        <td class="td_common_numeric_rules" style="text-align:left">
                                            {{  $val->doctor_name }} </td>
                                        <td class="td_common_numeric_rules"> {{ $val->visit_count }} </td>
                                    <tr>
                                    @endif
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            @endif
                        </td>

                        <td class="common_td_rules drill_down_data" style="cursor: pointer; width: 10%;"> {{ $occupied_visit_count }}
                            @if(intval($occupied_visit_count) > 0)
                            <table class='table table-condensed table_sm table-col-bordered hidden' style="font-size: 12px; display: none;">
                                <thead>
                                    <tr class="headerclass"
                                        style="background-color:rgb(185 237 227);color:black;border-spacing: 0 1em;font-family:sans-serif">
                                        <th class="common_td_rules" style="width: 6%"> Doctor </th>
                                        <th class="common_td_rules" style="width: 3%">Count</th>
                                    </tr>
                                </thead>
                                <tbody style="border: 1px solid #CCC;">
                                    @if (!empty($result_data_array)) 
                                    @foreach ($result_data_array as $key => $val)
                                    @if($val->visit_type == 'Occupied') 
                                    <tr style="height: 30px;" style="background-color: rgb(200 223 200);">
                                        <td class="td_common_numeric_rules" style="text-align:left">
                                            {{  $val->doctor_name }} </td>
                                        <td class="td_common_numeric_rules"> {{ $val->visit_count }} </td>
                                    <tr>
                                    @endif
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            @endif
                        </td>                       
                       
                        <td class="td_common_numeric_rules" style="width: 8%;"> {{ $op_bill_count }} </td>
                        <!-- <td class="td_common_numeric_rules" style="width: 8%;"> {{ $op_bill_sum }} </td> -->
                        <td class="td_common_numeric_rules" style="width: 8%;"> {{ $ip_bill_count }} </td>
                        <!-- <td class="td_common_numeric_rules" style="width: 8%;"> {{ $ip_bill_sum }} </td> -->
                        <td class="td_common_numeric_rules" style="width: 8%;"> {{ $ext_bill_count }} </td>
                        <!-- <td class="td_common_numeric_rules" style="width: 8%;"> {{ $ext_bill_sum }} </td>        -->
                        <td class="td_common_numeric_rules" style="width: 8%;"> {{ $xray_bill_count }} </td>
                        <td class="td_common_numeric_rules" style="width: 8%;"> {{ $usg_bill_count }} </td>     
                        <td class="td_common_numeric_rules" style="width: 8%;"> {{ $ecg_bill_count }} </td> 
                        

                    </tr>
                    <?php
                        $op_total         += $op_visit_count;        
                        $admitting_total  += $ip_visit_count;        
                        $discharge_total  += $discharge_visit_count;        
                        $occupied_total   += $occupied_visit_count;

                        $op_bill_count_total += $op_bill_count; 
                        $op_bill_sum_total   += $op_bill_sum; 
                        $ip_bill_count_total += $ip_bill_count; 
                        $ip_bill_sum_total   += $ip_bill_sum; 
                        $ext_bill_count_total+= $ext_bill_count; 
                        $ext_bill_sum_total  += $ext_bill_sum; 
                        $lab_bill_count_total+= $lab_bill_count; 
                        $lab_ext_bill_count_total  += $lab_ext_bill_count; 
                        $xray_bill_count_total+= $xray_bill_count; 
                        $usg_bill_count_total  += $usg_bill_count; 
                        $ecg_bill_count_total  += $ecg_bill_count; 

                    ?>
                    @endforeach
                    <tr style="height: 30px; background-color:#d9edf7">
                        <th class="common_td_rules" style="text-align:left;">Total</th>
                        <th class="td_common_numeric_rules"> {{ $op_total }} </th>
                        <th class="td_common_numeric_rules"> {{ $admitting_total }} </th>
                        <th class="td_common_numeric_rules"> {{ $discharge_total }} </th>
                        <th class="td_common_numeric_rules"> {{ $occupied_total }} </th>
                        <th class="td_common_numeric_rules"> {{ $op_bill_count_total }} </th>
                        <!-- <th class="td_common_numeric_rules"> {{ number_format($op_bill_sum_total, 2) }} </th> -->
                        <th class="td_common_numeric_rules"> {{ $ip_bill_count_total }} </th>
                        <!-- <th class="td_common_numeric_rules"> {{ number_format($ip_bill_sum_total, 2) }} </th>                         -->
                        <th class="td_common_numeric_rules"> {{ $ext_bill_count_total }} </th>
                        <!-- <th class="td_common_numeric_rules"> {{ number_format($ext_bill_sum_total, 2) }} </th>   -->
                        <th class="td_common_numeric_rules"> {{ $xray_bill_count_total }} </th>
                        <th class="td_common_numeric_rules"> {{ $usg_bill_count_total}} </th>   
                        <th class="td_common_numeric_rules"> {{ $ecg_bill_count_total}} </th>   
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    