

<div class="row">
    <div class="theadscroll always-visible" id="result_container_div" style="position: relative;" >

              <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?=date('M-d-Y h:i A')?> </b></p>
              <p style="font-size: 12px;" id="total_data">Report Date: <?=$from?> To <?=$to?></p>
              <?php
                  $collect = collect($res); $total_records=count($collect);
              ?>
              <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
              <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> New  Product List </b></h4>
              <style>
                   table tr:hover td{
        text-overflow: initial;
        white-space: normal;
    }
</style>
        <table id="result_data_table" class='table table-condensed table_sm  theadfix_wrapper' style="font-size: 12px;cursor: pointer;">
            <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th width="5%">#</th>
                 <th width="17">Product Name</th>
                 <th width="11%">Created At</th>
                 <th width="12%">Category</th>
                 <th width="11%">Sub Category</th>
                 <th  width='15%'>Generic Name</th>
                 <th  width='15%'>Doctor Name</th>

                </tr>
            <tbody>


             @if(count($res)!=0)
                 @foreach ($res as $data)
                 <tr>
                     <td class="td_common_numeric_rules">{{$loop->iteration}}.</td>
                     <td class="common_td_rules">{{$data->item_desc}}</td>
                     <td class="common_td_rules">{{ date('M-d-Y',strtotime($data->created_at))}}</td>
                     <td class="common_td_rules">{{$data->category}}</td>
                     <td class="common_td_rules">{{$data->subcategory}}</td>
                     <td class="common_td_rules">{{$data->generic_name}}</td>
                     <td class="common_td_rules">{{$data->doctor_name}}</td>


                 </tr>
                 @endforeach
             @else

                 <tr>
                    <td colspan="8" style="text-align: center;">No Results Found!</td>
                </tr>
           @endif
            </tbody>
        </table>
    </div>
</div>
