<div class="row">
  <div class="col-md-12" id="result_container_div">

  <p style="font-size: 12px;" id="total_data">Report Print Date: <?=date('Y-m-d h:i A')?> </p>
            <p style="font-size: 12px;" id="total_data">Report Date:<b> <?=$from_date?> To <?=$to_date?> </b></p>
            <?php
                $collect = collect($res); $total_records=count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> {{$report_header}}</b></h4>
            <font size="16px" face="verdana" >



<table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="width:60%; font-size: 12px;">
    <thead>
        <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th rowspan="2" width='15%'>Si No.</th>
            <th rowspan="2" width='60%'>Doctor Name</th>
            <th rowspan="2" width='25%'>Amount</th>
        </tr>

    </thead>

    <tbody>
    @if(count($res)!=0)
        @php
            $i=1;
            $total_amount = 0.0;
            $sub_total_amount = 0.0;
            $pack_name = '';
        @endphp

        @foreach ($res as $data)
            @php
                $total_amount+= floatval($data->amount);
                $sub_total_amount+= floatval($data->amount);
            @endphp
            @if($pack_name != $data->pack_name)
                @if($i != 1)
                    <tr>
                        <td></td>
                        <td style="text-align: left;"><b><i>Sub Total</i></b></td>
                        <td class="td_common_numeric_rules"><b><i><?=number_format($sub_total_amount)?></i></b></td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>&nbsp;</b></td>
                    </tr>
                @endif
                <tr>
                    <td></td>
                    <td colspan="2" style="text-align:left;"><b>{{$data->pack_name}}</b></td>
                    @php
                        $sub_total_amount = 0;
                    @endphp
                </tr>

            @endif
            <tr>
                <td class="td_common_numeric_rules"><?=$i?></td>
                <td class="common_td_rules"><?=$data->doctor_name?></td>
                <td class="td_common_numeric_rules"><?=number_format($data->amount)?></td>
            </tr>
            @php
                $i++;
                $pack_name = $data->pack_name;
            @endphp
        @endforeach
        <tr class="bg-info" style="height: 30px;">
            <th colspan="2" style="text-align: left;"  class="common_td_rules">Total</th>
            <th class="td_common_numeric_rules"><?=number_format((float)$total_amount, 2, '.', '')?></th>
        </tr>
        @else
            <tr>
                <td  colspan="12" style="text-align: center;">No Results Found!</td>
            </tr>
        @endif
    </tbody>
</table>
