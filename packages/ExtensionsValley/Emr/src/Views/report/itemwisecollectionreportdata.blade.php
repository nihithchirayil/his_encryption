<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
              <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?=date('M-d-Y h:i A')?> </b></p>
              <p style="font-size: 12px;" id="total_data">Report Date: <?=$from?> To <?=$to_date?></p>
              <?php
                  $collect = collect($res); $total_records=count($collect);
              ?>
              <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
              <h4 style="text-align: center;margin-top: -29px;" id="heading"><b><?=$report_header?></b></h4>
              <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
              <th style="padding: 2px;width: 70%;">Serivce Description</th>
              <th style="padding: 2px;width: 10%;">Gross Amount</th>
              <th style="padding: 2px;width: 10%">Discount</th>
              <th style="padding: 2px;width: 10%;">Net Amount</th>
          </tr>
      </thead>
      <?php
        $tot_gross_amount=0.0;
        $tot_discount=0.0;
        $tot_netamt=0.0;

      if(count($res)!=0){
      ?>

      <tbody>
          <?php
          foreach ($res as $data){
            $net_amt = floatval($data->gross_amount)-floatval($data->discount);
                ?>
              <tr>
                  <td class="common_td_rules">{{$data->service_desc}}</td>
                  <td class="td_common_numeric_rules">{{$data->gross_amount}}</td>
                  <td class="td_common_numeric_rules">{{$data->discount}}</td>
                  <td class="td_common_numeric_rules">{{$net_amt}}</td>
              </tr>
                  <?php

                  $tot_gross_amount += floatval($data->gross_amount);
                  $tot_discount+= floatval($data->discount);
                  $tot_netamt+=$net_amt;
              }
              ?>
              <tr class="" style="height: 30px;">
                  <th class="common_td_rules" style="text-align: left;" >Total</th>
                  <th class="td_common_numeric_rules"><?=$tot_gross_amount?></th>
                  <th class="td_common_numeric_rules"><?=$tot_discount?></th>
                  <th class="td_common_numeric_rules">{{$tot_netamt}}</th>
              </tr>
          <?php
           } else{ ?>
      <tr>
          <td colspan="4" style="text-align: center;">No Results Found!</td>
      </tr>
      <?php } ?>
      </tbody>

</table>

</div>
</div>
</div>
