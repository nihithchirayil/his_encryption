@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/custom_input.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/select2/css/select2-bootstrap.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="current_date" value="{{date('M-d-Y')}}">
<input type="hidden" id="base_url" value="{{URL::to('/')}}">

<div class="right_col">
    <div class="container-fluid">

        <div class="col-md-12 padding_sm">
            <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
                <div class="box no-border no-margin">
                    <div class="box-body" style="padding-bottom:15px;">
                        <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                            <thead><tr class="table_header_bg">
                                    <th colspan="11"><?=strtoupper($title)?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="row padding_sm">

                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">

                            <label for="">From Date</label>
                            <input type="text" data-attr="date" autocomplete="off" id="discharge_date_from" autofocus="" value="<?=$current_date?>" class="form-control filters" placeholder="YYYY-MM-DD">
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <div class="mate-input-box">

                            <label for="">To Date</label>
                            <input type="text" data-attr="date" autocomplete="off" id="discharge_date_to" autofocus="" value="<?=$current_date?>" class="form-control filters" placeholder="YYYY-MM-DD">
                            </div>
                        </div>

                        <div class="col-md-3 padding_sm date_filter_div">
                            <div class="mate-input-box">
                                @php
                                    $bill_tags = \DB::table('bill_tag')->where('status',1)
                                    ->orderBy('name')
                                    ->pluck('name','code');
                                @endphp
                            <label class="filter_label ">Bill Tag</label>
                            <div class="clearfix"></div>
                            {!! Form::select('bill_tag', $bill_tags, 0, ['class' => 'form-control select2 filters datapicker','placeholder' => 'Bill Tag','id' => 'bill_tag','style' => 'width:100%;color:#555555; padding:2px 12px;']) !!}
                            </div>
                        </div>

                        <div class="col-md-4 padding_sm ">
                            <label class="filter_label ">Paid Status</label>
                            <div class="clearfix"></div>

                                <input checked="checked" class="form-check-input" type="radio" name="paid_status" id="not_paid" value="0"/>
                                <label style="margin-right: 20px;" class="form-check-label" for="not_paid"> Not Paid</label>

                                <input class="form-check-input" type="radio" name="paid_status" id="paid" value="1"/>
                                <label class="form-check-label" for="paid"> Paid</label>

                        </div>


                        <div class="clearfix"></div>
                        <div class="col-md-12 padding_sm date_filter_div" style="margin-top: 10px;">
                            <div class="col-md-1 pull-right padding_sm">
                                <button type="button" class="btn light_purple_bg btn-block" onclick="getReportData();"  name="search_results" id="search_results">
                                    <i id="searchresultspin" class="fa fa-search" aria-hidden="true"></i>
                                    Search
                                </button>
                            </div>

                            <div class="col-md-1 pull-right padding_sm">
                                <a class="btn light_purple_bg btn-block disabled" onclick="generate_csv();"  name="csv_results" id="csv_results">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                    Excel
                                </a>
                            </div>
                            <div class="col-md-1 pull-right padding_sm">
                                <a class="btn light_purple_bg btn-block disabled" onclick="printReportData();"  name="print_results" id="print_results">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    Print
                                </a>
                            </div>
                            <div class="col-md-1 pull-right padding_sm">
                                <button type="reset" class="btn light_purple_bg btn-block" onclick="search_clear();"  name="clear_results" id="clear_results">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                    Reset
                                </button>
                            </div>
                        </div>

                        </div>


                        </div>
                    </div>
                </div>

            </div>


 <div class="col-md-12 padding_sm">
    <div class="col-md-12 padding_sm">
    <div id="ResultDataContainer" style="max-height: 650px; padding: 10px; display:none;font-family:poppinsregular">
    <div style="background:#686666;">
    <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);
    width: 100%;
    padding: 30px;" id="ResultsViewArea">

    </page>
    </div>
    </div>
    </div>
    </div>
    </div>
</div>

<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:rgb(91 110 91);">
          <h4 class="modal-title" style="color: white;">Print Configuration</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="height:120px;">
            <div class="col-md-12">
                <span style="margin-right: 5%;">Print Mode :</span>
                <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
            </div>
            <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                Include Hospital Header
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                    <i class="fa fa-print" aria-hidden="true"></i> Print
                </button>
            </div>
        </div>
      </div>
    </div>
  </div>

@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/patientpendingbillsreport.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/select2/select2.js")}}"></script>


<script type="text/javascript">

</script>
@endsection
