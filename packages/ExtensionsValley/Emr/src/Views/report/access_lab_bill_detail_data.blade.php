<div class="row">
    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;"><b> Access Lab report </b> </h4>
        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
            style="font-size: 12px;">
            <thead>
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th style="text-align:right;" width="2%">SL.No.</th>
                    <th style="text-align:left;" width="18%">Bill No.</th>
                    <th style="text-align:left;" width="11%">Bill Date</th>
                    <th style="text-align:left;" width="18%">UHID</th>
                    <th style="text-align:left;" width="20%">Patient Name</th>
                    <th style="text-align:left;" width:="5%">Paid Status</th>
                    <th style="text-align:right;" width="10%">Bill Amount</th>
                    <th style="text-align:right;" width="10%">Net Amount</th>
                    <th style="text-align:right;" width:="10%">Pricing Discount Amount</th>
                </tr>
            </thead>
            <?php
            $i = 1;

            $bill_amount = 0.0;
            $net_amount_wo_roundoff = 0.0;
            $pricing_discount_amount = 0.0;
            if(count($res)!=0){
            foreach ($res as $data){
                $bill_amount += floatval($data->bill_amount);
                    $net_amount_wo_roundoff += floatval($data->net_amount_wo_roundoff);
                    $pricing_discount_amount += floatval($data->pricing_discount_amount);
                ?>
            <tr>
                <td class="common_td_rules">{{ $i++ }}</td>
                <td class="common_td_rules">{{ $data->bill_no }}</td>
                <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->bill_date)) }}</td>
                <td class="common_td_rules">{{ $data->uhid }}</td>
                <td class="common_td_rules">{{ $data->patient_name }}</td>
                <td class="common_td_rules">{{ $data->paid_status }}</td>

                <td class="td_common_numeric_rules">
                    {{ number_format($data->bill_amount, $report_decimal_separator) }}
                </td>
                <td class="td_common_numeric_rules">
                    {{ number_format($data->net_amount_wo_roundoff, $report_decimal_separator) }}</td>
                <td class="td_common_numeric_rules">
                    {{ number_format($data->pricing_discount_amount, $report_decimal_separator) }}</td>
            </tr>

            <?php }
            ?>
            <tr class="bg-info">
                <th class="common_td_rules" colspan="6" style="text-align: left;">Total</th>
                <th class="td_common_numeric_rules">{{ number_format($bill_amount, 2, '.', '') }}</th>
                <th class="td_common_numeric_rules">{{ number_format($net_amount_wo_roundoff, 2, '.', '') }}</th>
                <th class="td_common_numeric_rules">{{ number_format($pricing_discount_amount, 2, '.', '') }}</th>
            </tr>
            <?php }else{
                ?>
            <tr>
                <td colspan="9">No Result Found</td>
            </tr>
            <?php
        }
        ?>


        </table>
    </div>
    <input type="hidden" id="is_print" value="{{ $is_print }}">
    <input type="hidden" id="is_excel" value="{{ $is_excel }}">
</div>
