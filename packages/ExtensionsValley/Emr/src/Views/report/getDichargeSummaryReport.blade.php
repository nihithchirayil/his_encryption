<div class="row">
  <div class="col-md-12" id="result_container_div">

  <p style="font-size: 12px;" id="total_data">Report Print Date: <?=date('M-d-Y h:i A')?> </p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?=$from_date?> To <?=$to_date?> </p>
            <?php
                $collect = collect($res); $total_records=count($collect);
            ?>
            <p style="font-size: 12px;">Total Count : {{$total_records}}</p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Discharge Bill Report </b></h4>
            <font size="16px" face="verdana" >
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
            <thead>
                <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th width='2%'>SL.No.</th>
                    <th width='4%'>Discharge Date</th>
                    <th width='20%'>UHID</th>
                    <th width='20%'>Patient Name</th>
                    <th width='4%'>Patient Type</th>
                    <th width='8%'>Doctor</th>
                    <th width='13%'>Bill No.</th>
                    <th width='4%'>Bill Tag</th>
                    <th width='4%'>Paid Status</th>
                    <th width='4%'>Bill Amt.</th>
                    <th width='4%'>Discount Amt.</th>
                    <th width='4%'>Advance Amt.</th>
                    <th width='4%'>Total Amt.</th>
                    <th width='4%'>Paid Cash</th>
                </tr>
        </thead>


    <tbody>
        <?php
    if(count($res)!=0){
        $i=1;
        $total_amt=0.0;
        $paid_amt=0.0;
    foreach ($res as $data){
        $total_amt+= floatval($data->net_amount);
        $paid_amt+= floatval($data->cash_collection);
      ?>
        <tr>
            <td class="td_common_numeric_rules"><?=$i?></td>
            <td class="common_td_rules"><?=date('M-d-Y',strtotime($data->discharge_date))?></td>
            <td class="common_td_rules"><?=$data->uhid?></td>
            <td class="common_td_rules"><?=$data->patient_name?></td>
            <td class="common_td_rules"><?=$data->patient_type?></td>
            <td class="common_td_rules"><?=$data->doctor?></td>
            <td class="common_td_rules"><?=$data->bill_no?></td>
            <td class="common_td_rules"><?=$data->bill_tag?></td>
            <td class="common_td_rules"><?=$data->paid_status?></td>
            <td class="td_common_numeric_rules"><?=number_format($data->bill_amount, 2, '.', '');?></td>
            <td class="td_common_numeric_rules"><?=number_format($data->total_discount, 2, '.', '');?></td>
            <td class="td_common_numeric_rules"><?=number_format($data->advance_collected, 2, '.', '');?></td>
            <td class="td_common_numeric_rules"><?=number_format($data->net_amount, 2, '.', '');?></td>
            <td class="td_common_numeric_rules"><?=number_format($data->cash_collection, 2, '.', '');?></td>
        </tr>
       <?php
       $i++;
    }
    ?>
    <tr class="bg-info" style="height: 30px;">
        <th colspan="12" style="text-align: left;"  class="common_td_rules">Total</th>
        <th class="td_common_numeric_rules"><?=number_format($total_amt, 2, '.', '');?></th>
        <th class="td_common_numeric_rules"><?=number_format($paid_amt, 2, '.', '');?></th>
    </tr>
    <?php
    }else{
        ?>
        <tr>
            <td  colspan="12" style="text-align: center;">No Results Found!</td>
        </tr>
        <?php
    }
        ?>

</table>
            </font>
</div>
</div>
