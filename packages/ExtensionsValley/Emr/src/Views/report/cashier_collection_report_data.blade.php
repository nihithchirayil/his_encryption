<div class="row">

   
 
   <div class="col-md-12" id="result_container_div">

       <div class="theadscroll always-visible" style="position: relative; height: 510px;">

    <table id="result_data_table" class="theadfix_wrapper" style="font-size: 12px;">
    @if(sizeof($res)>0)
    <thead>
        <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th onclick="sortTable('result_data_table',0,2);" style="padding: 2px;width: 120px;">Date</th>
            <th onclick="sortTable('result_data_table',1,2);" style="padding: 2px;width: 180px;">Bill Tag</th>
            <th onclick="sortTable('result_data_table',2,1);" style="padding: 2px;width: 75px;">Bill Amount</th>
            <th onclick="sortTable('result_data_table',3,1);" style="padding: 2px;width: 75px;">Net Amount</th>
        </tr>
    </thead>


    @php $user_name = ''; $total_bill_amount = $total_net_amount = 0;@endphp
    <tbody>
    @foreach ($res as $data)

        @if($user_name != $data->user_name)
        @php $user_name = $data->user_name; @endphp

       <tr class="headerclass" style="height: 30px;">
           <td colspan="4">{{$user_name}}</td>

       </tr>

       @endif
        <tr>
            <td>{{ date(\WebConf::getConfig('date_format_web'),strtotime($data->collected_date))}}</td>
            <td>{{$data->bill_tag}}</td>
            <td>{{$data->bill_amount}}</td>
            <td>{{$data->net_amount}}</td>

        </tr>
        @php
            $total_bill_amount+= $data->bill_amount;
            $total_net_amount+= $data->net_amount;
        @endphp
    @endforeach
        <tr>
            <td colspan="4">
                <hr style="height:1px;border-width:0;color:gray;background-color:gray">
            </td>
        </tr>
        <tr style="font-family:robotoregular">
            <td colspan="2"><b>Total</b></td>
            <td><b>{{$total_bill_amount}}</b></td>
            <td><b>{{$total_net_amount}}</b></td>
        </tr>
    </tbody>
    @else
    <tr>
        <td>No Results Found!</td>
    </tr>
    @endif

</table>
</div>
   </div>
</div>
