<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= date('M-d-Y', strtotime($from)) ?> To
                <?= date('M-d-Y', strtotime($to)) ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b>Lab Revenue Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                @if (count($res) > 0)
                    @foreach ($res as $key => $val)
                        <?php
                        $str = explode('-', $key);
                        $month = @$str[0] ? date('F', strtotime($str[0])) : date('M');
                        $first_day = date('jS', strtotime('first day of this month', strtotime($month)));
                        $last_day = date('jS', strtotime('last day of this month', strtotime($month)));

                        ?>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th style="text-align: center" colspan="15">
                                {{ $first_day . ' ' . $month }} {{ @$str[1] ? $str[1] : '-' }} To
                                {{ $last_day . ' ' . $month }}
                                {{ @$str[1] ? $str[1] : '-' }}</th>
                        </tr>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th width='10%'>Op Cash</th>
                            <th width='10%'>Op Insur.</th>
                            <th width='10%'>Ip Cash</th>
                            <th width='10%'>Ip Credit</th>
                            <th width='10%'>Ip Insur.</th>
                            <th width='10%'>Patho.Cash</th>
                            <th width='10%'>Patho.Credit</th>
                            <th width='10%'>Jeeva Lab</th>
                            <th width='10%'>Thyrocare</th>
                            <th width='10%'>Life Cell</th>
                            <th width='10%'>Shar.Holr.Dis.</th>
                            <th width='10%'>Oth.Dis.</th>
                            <th width='10%'>Acce.Lab</th>
                            <th width='10%'>Consumables</th>
                            <th width='10%'>Total</th>
                        </tr>
                        <tr>
                            @php
                                $op_cash = 0;
                                $op_insurance = 0;
                                $ip_cash = 0;
                                $ip_credit = 0;
                                $ip_insurance = 0;
                                $patho_cash = 0;
                                $patho_credit = 0;
                                $share_holder_discount = 0;
                                $other_discount = 0;
                                $access_lab = 0;
                                $consumer_lab = 0;
                                $total = 0;
                                $thyrocare = 0;
                                $jeeva = 0;
                                $life_cell = 0;
                                $access_lab = 0;
                                $tr_total = 0;

                                foreach ($val as $item => $each) {
                                    if ($item == 'op_cash') {
                                        $op_cash = $each;
                                    } elseif ($item == 'op_insurance') {
                                        $op_insurance = $each;
                                    } elseif ($item == 'ip_cash') {
                                        $ip_cash = $each;
                                    } elseif ($item == 'ip_credit') {
                                        $ip_credit = $each;
                                    } elseif ($item == 'ip_insurance') {
                                        $ip_insurance = $each;
                                    } elseif ($item == 'patho_cash') {
                                        $patho_cash = $each;
                                    } elseif ($item == 'patho_credit') {
                                        $patho_credit = $each;
                                    } elseif ($item == 'share_holder_discount') {
                                        $share_holder_discount = $each;
                                    } elseif ($item == 'other_discount') {
                                        $other_discount = $each;
                                    } elseif ($item == 'access_lab') {
                                        $access_lab = $each;
                                    } elseif ($item == 'consumable') {
                                        $consumer_lab = $each;
                                    } elseif ($item == 'total') {
                                        $total = $each;
                                    } elseif ($item == 'thyrocare') {
                                        $thyrocare = $each;
                                    } elseif ($item == 'jeeva_lab') {
                                        $jeeva = $each;
                                    } elseif ($item == 'life_cell') {
                                        $life_cell = $each;
                                    }
                                }
                                $outsource_total = ((floatval($thyrocare) + floatval($jeeva) + floatval($life_cell)) * 40) / 100;
                                $access_lab_per = (floatval($access_lab) * 60) / 100;
                                $tr_total = floatval($op_cash) + floatval($op_insurance) + floatval($ip_cash) + floatval($ip_credit) + floatval($ip_insurance) + floatval($patho_cash) + floatval($patho_credit) + floatval($consumer_lab) + floatval($access_lab_per) + floatval($outsource_total);
                            @endphp
                            <td class="td_common_numeric_rules" width='5%'>{{ number_format($op_cash, 2) }}</td>
                            <td class="td_common_numeric_rules" width='5%'>{{ number_format($op_insurance, 2) }}</td>
                            <td class="td_common_numeric_rules" width='5%'>{{ number_format($ip_cash, 2) }}</td>
                            <td class="td_common_numeric_rules" width='5%'>{{ number_format($ip_credit, 2) }}</td>
                            <td class="td_common_numeric_rules" width='5%'>{{ number_format($ip_insurance, 2) }}</td>
                            <td class="td_common_numeric_rules" width='5%'>{{ number_format($patho_cash, 2) }}</td>
                            <td class="td_common_numeric_rules" width='5%'>{{ number_format($patho_credit, 2) }}</td>
                            <td class="td_common_numeric_rules" width='10%'>{{ number_format($jeeva, 2) }}</td>
                            <td class="td_common_numeric_rules" width='10%'>{{ number_format($thyrocare, 2) }}</td>
                            <td class="td_common_numeric_rules" width='10%'>{{ number_format($life_cell, 2) }}</td>
                            <td class="td_common_numeric_rules" width='10%'>
                                {{ number_format($share_holder_discount, 2) }}</td>
                            <td class="td_common_numeric_rules" width='10%'>{{ number_format($other_discount, 2) }}
                            </td>
                            <td class="td_common_numeric_rules" width='10%'>{{ number_format($access_lab, 2) }}</td>
                            <td class="td_common_numeric_rules" width='5%'>{{ number_format($consumer_lab, 2) }}</td>
                            <td class="td_common_numeric_rules" width='5%'>{{ number_format($tr_total, 2) }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="13" style="text-align: center;">No Results Found!</td>
                    </tr>
                @endif
                </tbody>

            </table>

        </div>
    </div>
</div>
