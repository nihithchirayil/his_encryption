<div class="row">

    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('Y-m-d h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"> Department Wise Total Income Report</h4>
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="11%;">Department Name</th>
                        <th width="13%;">Gross Amount</th>
                        <th width="10%;">Discount</th>
                        <th width="15%;">Net Amount</th>
                    </tr>
                </thead>
                <?php
                $netgross_amount = 0;
                $netdiscount_amount = 0;
                $netnet_amount = 0;
                ?>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
         $i=0;
         $specialty_name = '';
         $totalgross_amount = 0;
         $totaldiscount_amount = 0;
         $totalnet_amount = 0;

         foreach ($res as $data){
             if($specialty_name != $data->speciality){
                 $i=0;
                 $specialty_name = $data->speciality;
                 $totalgross_amount = 0.0;
                 $totaldiscount_amount = 0.0;
                 $totalnet_amount = 0.0;
               ?>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="4" class="common_td_rules">{{ $specialty_name }}</th>
                        </tr>
                        <tr>
                            <td class="common_td_rules">{{ $data->dept_name }}</td>
                            <td class="td_common_numeric_rules">{{ $data->gross_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $data->discount }}</td>
                            <td class="td_common_numeric_rules">{{ $data->net_amount }}</td>
                        </tr>
                        <?php
             }else{
                 ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->dept_name }}</td>
                            <td class="td_common_numeric_rules">{{ $data->gross_amount }}</td>
                            <td class="td_common_numeric_rules">{{ $data->discount }}</td>
                            <td class="td_common_numeric_rules">{{ $data->net_amount }}</td>
                        </tr>
                        <?php

             }
             $totalgross_amount+= floatval($data->gross_amount);
             $totaldiscount_amount+= floatval($data->discount);
             $totalnet_amount+= floatval($data->net_amount);
             if($i==$specialty_namecnt[$specialty_name]){
             ?>
                        <tr class="" style="height: 30px;">
                            <th class="common_td_rules" style="text-align: left;">User Total</th>
                            <th class="td_common_numeric_rules"><?= $totalgross_amount ?></th>
                            <th class="td_common_numeric_rules"><?= $totaldiscount_amount ?></th>
                            <th class="td_common_numeric_rules"><?= $totalnet_amount ?></th>
                        </tr>
                        <?php
                $netgross_amount+=$totalgross_amount;
                $netdiscount_amount+=$totaldiscount_amount;
                $netnet_amount+=$totalnet_amount;
             }
             $i++;

         }
         ?>
                        <tr class="" style="height: 30px;">
                            <th class="common_td_rules" style="text-align: left;">Total</th>
                            <th class="td_common_numeric_rules"><?= $netgross_amount ?></th>
                            <th class="td_common_numeric_rules"><?= $netdiscount_amount ?></th>
                            <th class="td_common_numeric_rules"><?= $netnet_amount ?></th>
                        </tr>
                    @else
                        <tr>
                            <td colspan="4" style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
        </font>
    </div>
</div>
