<div class="row">
    <div class="col-md-12" id="result_container_div" style="">

        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper' style="font-size: 12px;">
            @if(sizeof($res)>0)
     <p style="font-size: smaller;padding-left: 10px">Report Date: <span>{{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}}</span> </p>

     <h3 align="center" style="padding_right:3px"> Product Details Report</h3>

     <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
         <th width="11%"  style="padding: 2px;">Item Type</th>
         <th width="12%" style="padding: 2px;">Category</th>
         <th width="11%" style="padding: 2px;">Sub Category</th>
         <th width="10%" style="padding: 2px;">Product Code</th>
         <th width="10%" style="padding: 2px;">Product Name</th>
         {{-- <th style="padding: 2px;width: 10%;">Product By</th> --}}
         <th  width='7%' style="padding: 2px;">HSN Code</th>
         <th  width='10%' style="padding: 2px;">Generic Name</th>
         <th  width='7%' style="padding: 2px;">Status</th>
     </tr>
 

     @foreach ($res as $data)
 
         <tr>
             {{-- <td class="td_common_nummeric_rules">{{ date('M-d-Y',strtotime($data->visit_date))}}</td> --}}
             <td class="td_common_rules">{{$data->item_type}}</td>
             <td class="td_common_rules">{{$data->category}}</td>
             <td class="td_common_rules">{{$data->subcategory}}</td>
             <td class="td_common_rules">{{$data->product_code}}</td>
             <td class="td_common_rules">{{$data->product_name}}</td>
             {{-- <td class="td_common_nummeric_rules">{{$data->product_by}}</td> --}}
             <td class="td_common_rules">{{$data->hsn_code}}</td>
             <td class="td_common_rules">{{$data->generic_name}}</td>
             <td class="td_common_rules">{{$data->status}}</td>
 
         </tr>
         @endforeach
       @else
       <script type='text/javascript'>
        toastr.warning('No Result Found..!');
     </script>
     @endif
 
 </table>
    </div>
    <input type="hidden" id="is_print" value="{{$is_print}}">
    <input type="hidden" id="is_excel" value="{{$is_excel}}">
 </div>
 