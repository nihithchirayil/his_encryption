<div class="row">

    <div class="theadscroll always-visible" id="result_container_div" style="position: relative;height:510px;">

        @if (sizeof($res) > 0)
            <p style="font-size: 12px;" id="total_data">Report Date:
                {{ date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s'))) }} </p><br>
            @php
                $collect = collect($res);
                $total_records = count($collect);
            @endphp
            <p style="font-size: 12px;">Total Records<b> : {{ $total_records }}</b></p>
            <h2 style="text-align: center;margin-top: -29px;" id="heading"> <b>IP Services Report</b> </h2>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
                style="font-size: 12px;">

                <thead class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:robotoregular">
                    <th width='5%;' style="padding: 2px;text-align: center; ">Sn. No.</th>
                    <th width='35%' style="padding: 2px;text-align: center">Service Name</th>
                    <th width='30%' style="padding: 2px;text-align: center">Service Datetime</th>
                    <th width='30%' style="padding: 2px;text-align: center">Doctor Name</th>
                </thead>
                <tbody style="font-size: 12px;">
                    @php
                        $total = 0;
                    @endphp
                    @foreach ($res as $data)
                        <tr>
                            <td class="td_common_nummeric_rules">{{ $loop->iteration }}.</td>
                            <td class="td_common_rules">{{ $data->service_desc }}</td>
                            <td class="td_common_nummeric_rules">
                                {{ date('M-d-Y h:i:a', strtotime($data->service_datetime)) }}</td>
                            <td class="td_common_rules">{{ $data->doctor_name }}</td>
                        </tr>


                    @endforeach

                </tbody>
            @else
                <h1 style="text-align: center; color: antiquewhite;">No Record Found</h1>
        @endif
        </table>
    </div>
</div>
