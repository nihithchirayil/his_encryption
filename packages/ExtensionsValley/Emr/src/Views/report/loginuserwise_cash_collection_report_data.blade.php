<div class="row">
    <div class="col-md-12 wrapper" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
        @if (sizeof($res) > 0)
            @php
                $collect = collect($res);
                $total_records = count($collect);
            @endphp
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"> <b> Cash Collection Report-Login UserWise </b></h4>
            <font size="16px" face="verdana">
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                    style="font-size: 12px;">
                    <thead>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th width='5%'>SL.No</th>
                            <th width='10%'>Cashier Name</th>
                            @if ($checkbox == 1)
                                <th width='10%'> Bill Detail </th>
                            @else
                                <th width='10%'>Total Collected Amt. </th>
                            @endif
                            <th width='10%'>Refund Cash</th>
                            <th width='10%'>Net Amount</th>
                            <th width='10%'>Cash Collection</th>
                            <th width='10%'>Card/Oth. Collection</th>
                            <th width='10%'>Advance Adjust</th>
                            <th width='10%'>Cash In Hand</th>
                        </tr>
                    </thead>
                    <tbody style="font-size: 12px;">
                        @php
                            $cash_collection_total = 0;
                            $card_other_collection = 0;
                            $bill_collection = 0;
                            $advance_adjust = 0;
                            $cash_refund = 0;
                            $cash_in_hand = 0;
                            $net_amount = 0;
                            $net_amounttot = 0;
                            $totalrefund = 0;

                        @endphp
                        @foreach ($res as $data)

                            <tr>
                                <td class="common_td_rules">{{ $loop->iteration }}.</td>
                                <td rowspan="" class="common_td_rules" style="padding:5px;width:10%;">
                                    {{ $data->user_name }}</td>
                                @if ($checkbox == '1')

                                    <td style="padding:3px;border-bottom:1px solid;width:10%">
                                        <table id="inner_table" style="width:100%;">
                                            <tr class="headerclass"
                                                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                                <th width='60%'>Bill Tag</th>
                                                <th width='20%'>Bill Amount</th>
                                                <th width='20%'>Refund Amount</th>
                                            </tr>
                                            <tbody>
                                                @php
                                                    $total = 0;
                                                    $totalrefund = 0;
                                                @endphp
                                                @foreach ($res1 as $item)
                                                    @if ($data->id == $item->id)
                                                        <tr>
                                                            <td class="inner-td">{{ $item->bill_type }}</td>
                                                            <td class="inner-td common_td_rules">
                                                                {{ number_format($item->bill_amount, $report_decimal_separator) }}
                                                            </td>
                                                            <td class="inner-td common_td_rules">
                                                                {{ number_format($item->refund, $report_decimal_separator) }}
                                                            </td>
                                                            @php $total+=$item->bill_amount; @endphp
                                                            @php $totalrefund+=$item->refund; @endphp
                                                        </tr>

                                                    @endif
                                                @endforeach

                                                <tr>
                                                    <td><b>Total Bill Amount</b></td>
                                                    <td class='td_common_numeric_rules'>
                                                        <b>{{ number_format($total, $report_decimal_separator) }}</b>
                                                    </td>
                                                    <td class='td_common_numeric_rules'>
                                                        <b>{{ number_format($totalrefund, $report_decimal_separator) }}</b>
                                                    </td>
                                                </tr>
                                            </tbody>


                                        </table>
                                    </td>
                                    @php

                                        $net_amount = $total - $data->refund_cash;
                                        $net_amounttot += $net_amount;

                                    @endphp
                                @else

                                    <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                        {{ number_format($data->bill_collecction, $report_decimal_separator) }}</td>
                                    @php

                                        $net_amount = $data->bill_collecction - $data->refund_cash;
                                        $net_amounttot += $net_amount;

                                    @endphp
                                @endif
                                @php

                                    $cash_collection_total += $data->cash_collection;
                                    $bill_collection += $data->bill_collecction;
                                    $card_other_collection += $data->card_other_collection;
                                    $advance_adjust += $data->advance_adjust;
                                    $cash_refund += $data->refund_cash;
                                    $cash_in_hand += $data->cash_in_hand;
                                @endphp
                                <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($data->refund_cash, $report_decimal_separator) }}</td>
                                <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($net_amount, $report_decimal_separator) }}</td>
                                <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($data->cash_collection, $report_decimal_separator) }}</td>
                                <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($data->card_other_collection, $report_decimal_separator) }}</td>
                                <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($data->advance_adjust, $report_decimal_separator) }}</td>
                                <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($data->cash_in_hand, $report_decimal_separator) }}</td>


                            </tr>


                        @endforeach

                        <tr class="bg-info">
                            <th class="common_td_rules" colspan='2' style="padding:5px;width:10%;"><b>Total</b></th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($bill_collection, $report_decimal_separator) }}</th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($cash_refund, $report_decimal_separator) }}</th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($net_amounttot, $report_decimal_separator) }}</th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($cash_collection_total, $report_decimal_separator) }}</th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($card_other_collection, $report_decimal_separator) }}</th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($advance_adjust, $report_decimal_separator) }}</th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($cash_in_hand, $report_decimal_separator) }}</th>
                        </tr>



                    </tbody>
                @else
                    <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
        @endif
        </table>
        </font>
    </div>
</div>
