@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
{!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
<link href="{{asset("packages/extensionsvalley/dashboard/css/style.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<style>

.header_bg{
        background-color: #02967f;
        color:white;
    }
    a{color:black !important;}
    li>a{
      color:#E7E7E7 !important;
    }
    ul{padding-left:5px !important;font-weight: 200 !important;}
    li>a:hover{color:black !important;}
    .table_name{color:rgb(114, 4, 4) !important;}
    .gradient_bg{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
        color:white !important;
    }
    .bottom-border-text{
    border:none !important;
    border-bottom:1px solid lightgrey !important;
    box-shadow:none;
    }

    .select2-selection__choice{
        background-color: #e6f1e8 !important;
        border: 1px solid #dbd8d8 !important;
        font-size: 10px !important;
    }
    .mate-input-box{
        height:51px !important;
    }

    .mate-input-box>text{
        margin-top:3px !important;
    }
    .select2-selection--multiple{
        padding-bottom:0px !important;
    }
    .table_header_bg{
        height: 30px !important;
        text-align:right !important;
        padding-right :40px !important;
        margin-bottom : 6px !important;
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }
    .table_header_bg>h5{
        margin-top:7px;
        font-weight: 700;
    }
    .ajaxSearchBox{
        z-index: 9999 !important;
    }
    .ajaxSearchBox>li{
        font-size:13px !important;
        font-family: Arial !important;

    }
    .liHover{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
        color:white;
    }
    .hospital_header>table{
        .border-bottom:1px solid #a5a4a4;
    }

    .td_common_numeric_rules{
        border-left: solid 1px #bbd2bd !important;
        text-align: right !important;
        font-size:11px !important;
    }
    .common_td_rules{
        text-align: left !important;
        overflow: hidden !important;
        border-right: solid 1px #bbd2bd !important;
        border-left: solid 1px #bbd2bd !important;
        max-width: 100px;
        text-overflow: ellipsis;
        white-space: nowrap;
        font-size:11px !important;
    }
    .ajaxSearchBox{
        display: none;
        width: 250px !important;
        height: 400px !important;
        overflow: hidden;
        padding-top: 0px !important;
        margin-top: 15px !important;
        z-index: 9999 !important;
    }

    .table_sm th, .table_sm td{
        font-size:11px !important;
    }

    .hospital_header_excel{
        background-color:#07ad8c; color: white;
    }

    @media print {

        .theadscroll {max-height: none; overflow: visible;}
        #ResultsViewArea{width:50% !important;}
        table{
            display: none !important;
        }

      }

    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #efefef;
    }

    .Blink {
        animation: blinker 1.5s cubic-bezier(.5, 0, 1, 1) infinite alternate;
    }
    @keyframes blinker {
        from { opacity: 1; }
        to { opacity: 0; }
    }
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" value='<?= $route_data ?>' id="route_value">
<input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
<input type="hidden" id="exceller_data"
    value="<?= base64_encode('<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>') ?>">

<div class="right_col">
<div class="container-fluid">


 <div class="12 no-padding">
    <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
        <div class="box no-border no-margin">
            <div class="box-body" style="padding-bottom:15px;">
                <table border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;">
                    <thead><tr class="table_header_bg">
                            <th colspan="11" style="text-align: right;">Patient Lab Results
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="row">
                    <div class="col-md-12"  style="margin-bottom:5px;">
                        {!!Form::open(['name'=>'search_from','id'=>'search_from']) !!}
                        <div>
                            <?php
                            $i = 1;

                            foreach ($showSearchFields as $fieldType => $fieldName) {
                                $fieldTypeArray[$i] = explode('-', $fieldType);

                                if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                    $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                    ?>

                                    <div class= "col-xs-3">
                                        <div class="mate-input-box">
                                        <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                        <input class="form-control hidden_search" value="" autocomplete="off" type="text"  id="{{$fieldTypeArray[$i][1]}}" name="{{$fieldTypeArray[$i][1]}}" />
                                        <div id="{{$fieldTypeArray[$i][1]}}AjaxDiv" class="ajaxSearchBox"></div>
                                        <input class="filters" value="{{$hiddenFields[$hidden_filed_id]}}"  type="hidden" name="{{$fieldTypeArray[$i][1]}}_hidden" value="" id="{{$fieldTypeArray[$i][1]}}_hidden">
                                    </div>
                                    </div>

                                    <?php
                                } else if ($fieldTypeArray[$i][0] == 'text') {
                                    ?>
                                    <div class="col-xs-3">
                                        <label class="filter_label">{{$fieldTypeArray[$i][2]}}</label>
                                        <input type="text"  name="{{$fieldTypeArray[$i][1]}}" <?php if (isset($fieldTypeArray[$i][3])) {
                                echo $fieldTypeArray[$i][3];
                            } ?>  value="" class="form-control filters" id="{{$fieldTypeArray[$i][1]}}" data="{{isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : ''}}" <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?> >
                                    </div>
                                    <?php
                                } else if ($fieldTypeArray[$i][0] == 'date') {

                                    ?>
                                    <div class="col-xs-2 date_filter_div">
                                        <div class="mate-input-box">
                                        <label class="filter_label">{{$fieldTypeArray[$i][2]}} From</label>
                                        <input type="text" value="{{date('M-d-Y')}}" data-attr="date" autocomplete="off" name="{{$fieldTypeArray[$i][1]}}_from" <?php
                            if (isset($fieldTypeArray[$i][3])) {
                                echo $fieldTypeArray[$i][3];
                            }
                            ?>  value="" class="form-control filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_from">

                                    </div>
                                    </div>
                                    <div class="col-xs-2 date_filter_div">
                                        <div class="mate-input-box">
                                        <label class="filter_label">{{$fieldTypeArray[$i][2]}} To</label>
                                        <input type="text" value="{{date('M-d-Y')}}" data-attr="date" autocomplete="off" name="{{$fieldTypeArray[$i][1]}}_to" <?php
                            if (isset($fieldTypeArray[$i][3])) {
                                echo $fieldTypeArray[$i][3];
                            }
                                    ?>  value="" class="form-control filters" placeholder="YYYY-MM-DD" id="{{$fieldTypeArray[$i][1]}}_to">

                                    </div>
                                    </div>
                                    <?php

                                } else if ($fieldTypeArray[$i][0] == 'combo') {
                                    ?>
                                    <div class="col-xs-12 date_filter_div">
                                        <label class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                        {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control filters ','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1], 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                    </div>
                                    <?php
                                } else if ($fieldTypeArray[$i][0] == 'multi_combo') {
                                    ?>
                                    <div class="col-xs-12 date_filter_div">
                                        <label style="width: 100%;"class="filter_label ">{{$fieldTypeArray[$i][2]}}</label>
                                        {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control filters multiple_selectbox','multiple'=>'multiple','placeholder' => $fieldTypeArray[$i][2],'id' => $fieldTypeArray[$i][1], 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                    </div>
                                    <?php
                                }
                                $i++;

                            }
                            ?>

                            <div class="col-md-1">
                                <input class="" type="checkbox" name="is_finalize" id="is_finalize"/>
                                <label for="is_finalize"><b>Inprogress</b></label>
                            </div>
                            <div class="col-md-2">
                                <input class="" type="checkbox" name="is_pdf" id="is_pdf"/>
                                <label for="is_pdf"><b>Pdf results only</b></label>
                            </div>

                            <div class="col-xs-4 pull-right" style="text-align:right; padding: 0px;margin-top:5px;">
                                <div class="clearfix"></div>

                                <button type="reset" class="btn light_purple_bg" onclick="search_clear();"  name="clear_results" id="clear_results">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                    Reset
                                </button>
                                <a class="btn light_purple_bg disabled"  onclick="exceller()"  name="csv_results" id="csv_results">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                    Excel
                                </a>
                                <a class="btn light_purple_bg disabled" onclick="printReportData();"  name="print_results" id="print_results">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    Print
                                </a>
                                <a class="btn light_purple_bg" onclick="getLabReportData();"  name="search_results" id="search_lab_report_results">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    Search
                                </a>

                            </div>
                            {!! Form::token() !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
 </div>

    <div class="col-md-12 no-padding">
        <div id="ResultDataContainer"
            style=" padding: 10px; display:none;font-family:poppinsregular;">
            <style>
                @media print {
                        .theadscroll {max-height: none; overflow: visible;}
                    }
            </style>
            <div style="float: left; width:100% !important;padding:10px;" id="ResultsViewArea">


            </div>
        </div>
    </div>
  </div>
</div>

<!-------print modal---------------->
<div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:rgb(91 110 91);">
          <h4 class="modal-title" style="color: white;">Print Configuration</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="height:120px;">
            <div class="col-md-12">
                <span style="margin-right: 5%;">Print Mode :</span>
                <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode" value="1">Portrait
                <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode" value="2">Landscape
            </div>
            <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                Include Hospital Header
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right" style="color:white">
                    <i class="fa fa-print" aria-hidden="true"></i> Print
                </button>
            </div>
        </div>
      </div>
    </div>
  </div>

  <div id="lab_results_rtf_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:73%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Lab Result</h4>
            </div>
            <div class="modal-body" id="lab_results_rtf_data" style="height:550px;overflow-y:scroll;">

            </div>

        </div>
    </div>

</div>

@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>

<script src="{{asset("packages/extensionsvalley/emr/js/lab_result_report.js")}}"></script>

<script type="text/javascript">

</script>
@endsection
