<div class="row">
    <div class="col-md-12" id="result_container_div" style="overflow-y: hidden;
    overflow-x: auto;">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date:<b> <?= $from_date ?> To <?= $to_date ?> </b></p>
        <?php
        $collect = collect($res);
        $total_records = count($collect);
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> {{ $report_header }}</b></h4>
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="width:100%; font-size: 12px;">
                <thead>

                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='1%'>SL. No.</th>
                        <th width='10%'>UHID</th>
                        <th width='10%'>Patient Name</th>
                        <th width='10%'>Adv. as on Date</th>
                        <th width='8%'>Visit Date</th>
                        <th width='10%'>Payment Date</th>
                        <th width='5%'>Amount Received</th>
                        <th width='5%'>OT Adj.</th>
                        <th width='5%'>Pharmacy Adj.</th>
                        <th width='5%'>Service Adj.</th>
                        <th width='4%'>Discharge Adj.</th>
                        <th width='5%'>Total Adj.</th>
                        <th width='4%'>Advance Refund</th>
                        <th width='4%'>Insurance Adj.</th>
                        <th width='4%'>Insurance Refund</th>
                        <th width='5%'>Mode</th>
                        <th width='5%'>Company</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($res) != 0)
                        @php
                            $i = 1;
                            $total_advance = 0.0;
                            $total_adjust = 0.0;
                            $total_refund = 0.0;
                            $total_ot_adjust = 0.0;
                            $total_pharmacy_adjust = 0.0;
                            $total_service_adjust = 0.0;
                            $total_discharge_adjust = 0.0;
                            $total_ins_adj = 0.0;
                            $total_ins_refund = 0.0;
                            $total_adv_as_on_date = 0.0;
                            $adv_as_on_date_totoal = 0.0;
                            $patient_uhid='';
                        @endphp

                        @foreach ($res as $key => $data)
                        
                            @php
                                $total_advance += floatval($data->total_advance);
                                $total_adjust += floatval($data->adjust_amount);
                                $total_refund += floatval($data->refund_amount);
                                $total_ot_adjust += floatval($data->adju_ot);
                                $total_pharmacy_adjust += floatval($data->adju_phar);
                                $total_service_adjust += floatval($data->adju_serv);
                                $total_discharge_adjust += floatval($data->adju_discharge);
                                $total_ins_adj += floatval($data->insurance_adj_amount);
                                $total_ins_refund += floatval($data->ins_refund_amount);
                                
                            @endphp

                            @if($patient_uhid!=$data->uhid)
                            @php
                            $patient_uhid=$data->uhid;
                            $adv_as_on_date_totoal += floatval($data->adv_as_on_date);
                            @endphp
                            @endif


                            {{-- @if ($key > 0 && $data->adv_as_on_date == $res[$key - 1]->adv_as_on_date)
                                
                            @else
                                @php
                                    $adv_as_on_date_totoal += floatval($data->adv_as_on_date);
                                @endphp
                            @endif --}}

                            <tr>
                                <td class="td_common_numeric_rules">{{ $i }}</td>
                                
                                @if (
                                    $key > 0 &&
                                        $data->uhid == $res[$key - 1]->uhid &&
                                        $data->patient_name == $res[$key - 1]->patient_name &&
                                        $data->adv_as_on_date == $res[$key - 1]->adv_as_on_date)
                                    <td class="common_td_rules"
                                        >
                                       
                                    </td>
                                    <td class="common_td_rules"
                                       >
                                       
                                    </td>
                                    <td class="common_td_rules"
                                       >
                                      
                                    </td>
                                @else
                                    <td class="common_td_rules" style="border-top: 2px solid">
                                        {{ $data->uhid }}
                                    </td>
                                    <td class="common_td_rules" style="border-top: 2px solid">
                                        {{ $data->patient_name }}
                                    </td>
                                    <td class="common_td_rules" style="border-top: 2px solid">
                                        {{ $data->adv_as_on_date }}
                                    </td>
                                @endif

                                <td class="common_td_rules" style="border-bottom:1px solid black">
                                    {{ date('M-d-Y', strtotime(trim($data->visit_date))) }}</td>
                                <td class="common_td_rules" style="border-bottom:1px solid black">
                                    {{ date('M-d-Y', strtotime(trim($data->payment_date))) }}
                                </td>
                                <td class="td_common_numeric_rules" style="border-bottom:1px solid black">
                                    {{ number_format($data->total_advance, 2, '.', '') }}</td>
                                <td class="td_common_numeric_rules" style="border-bottom:1px solid black">
                                    {{ number_format($data->adju_ot, 2, '.', '') }}</td>
                                <td class="td_common_numeric_rules" style="border-bottom:1px solid black">
                                    {{ number_format($data->adju_phar, 2, '.', '') }}
                                </td>
                                <td class="td_common_numeric_rules" style="border-bottom:1px solid black">
                                    {{ number_format($data->adju_serv, 2, '.', '') }}
                                </td>
                                <td class="td_common_numeric_rules" style="border-bottom:1px solid black">
                                    {{ number_format($data->adju_discharge, 2, '.', '') }}</td>
                                <td class="td_common_numeric_rules" style="border-bottom:1px solid black">
                                    {{ number_format($data->adjust_amount, 2, '.', '') }}</td>
                                <td class="td_common_numeric_rules" style="border-bottom:1px solid black">
                                    {{ number_format($data->refund_amount, 2, '.', '') }}</td>
                                <td class="td_common_numeric_rules" style="border-bottom:1px solid black">
                                    {{ number_format($data->insurance_adj_amount, 2, '.', '') }}</td>
                                <td class="td_common_numeric_rules" style="border-bottom:1px solid black">
                                    {{ number_format($data->ins_refund_amount, 2, '.', '') }}</td>
                                <td class="common_td_rules" style="border-bottom:1px solid black">{{ $data->mode }}
                                <td class="common_td_rules" style="border-bottom:1px solid black">{{ $data->company_name }}
                                </td>
                            </tr>
                            @php
                                $i++;
                            @endphp
                        @endforeach

                        <tr class="bg-info" style="height: 30px;">
                            <th colspan="3" style="text-align: left;" class="common_td_rules">Total</th>
                            <th class="td_common_numeric_rules">
                               
                                <?= number_format((float) $adv_as_on_date_totoal, 2, '.', '') ?>
                            </th>
                            <th colspan="3" class="td_common_numeric_rules">
                                <?= number_format((float) $total_advance, 2, '.', '') ?>
                            </th>
                            <th class="td_common_numeric_rules">
                                <?= number_format((float) $total_ot_adjust, 2, '.', '') ?></th>
                            <th class="td_common_numeric_rules">
                                <?= number_format((float) $total_pharmacy_adjust, 2, '.', '') ?></th>
                            <th class="td_common_numeric_rules">
                                <?= number_format((float) $total_service_adjust, 2, '.', '') ?></th>
                            <th class="td_common_numeric_rules">
                                <?= number_format((float) $total_discharge_adjust, 2, '.', '') ?></th>
                            <th class="td_common_numeric_rules"><?= number_format((float) $total_adjust, 2, '.', '') ?>
                            </th>
                            <th class="td_common_numeric_rules"><?= number_format((float) $total_refund, 2, '.', '') ?>
                            </th>
                            <th class="td_common_numeric_rules"><?= number_format((float) $total_ins_adj, 2, '.', '') ?>
                            </th>
                            <th class="td_common_numeric_rules">
                                <?= number_format((float) $total_ins_refund, 2, '.', '') ?>
                            </th>
                        </tr>
                    @else
                        <tr>
                            <td colspan="12" style="text-align: center;">No Results Found!</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </font>
    </div>
</div>
