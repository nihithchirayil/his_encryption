<div class="row" id="print_data">

    <div class="col-md-12" id="result_container_div">
    <p style="font-size: 12px;" id="total_data">Report Print Date: <?=date('M-d-Y h:i A')?> </p>
      <p style="font-size: 12px;" id="total_data">Report Date: <?=$from?> To <?=$to?></p>
      <?php
      $total_records=0;
      if (count($res)!=0) {
          $collect = collect($res);
          $total_records=count($collect);
      }
      ?>
     <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
     <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Doctor Wise OP Collection Report</b></h4>
     <font size="16px" face="verdana" >
     <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
      <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
              <th width="2%;">SL.No.</th>
              <th width="20%;">Doctor Name</th>
              <th width="6%">Patient Count</th>
          </tr>
      </thead>
      <?php
      $patient_count=0;
      if(count($res)!=0){
        $res = collect($res)->groupBy('speciality');
      ?>

      <tbody>
        <?php
          foreach ($res as $key => $values){

          ?>
          <tr style="background-color: paleturquoise;"><td style="text-align:left;" colspan="4"><b>{{$key}}</b></td></tr>
          <?php
          $i=1;
          foreach ($values as $k => $data){
      
          ?>
          <tr>
                  <td class="common_td_rules">{{$i}}</td>
                  <td class="common_td_rules">{{$data->doctor_name}}</td>
                  <td class="td_common_numeric_rules">{{$data->patient_count}}</td>
              </tr>

          <?php 
          $patient_count += $data->patient_count;
          $i++;
        }?>

          <?php } ?>
<tr class="" style="background-color: paleturquoise;">
          <td colspan="2" class="common_td_rules" style="text-align: left;"> <strong>Total</strong></td>
          <td class="td_common_numeric_rules"><strong><?=number_format($patient_count, 2, '.', '');?></strong></td>
        </tr>
          <?php
           } else{ ?>
          <tr>
              <td colspan="12" style="text-align: center;">No Results Found!</td>
          </tr>
          <?php } ?>
      </tbody>

</table>

</div>
</div>
