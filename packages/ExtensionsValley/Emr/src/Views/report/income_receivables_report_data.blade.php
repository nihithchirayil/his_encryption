<div class="row">
    <div class="col-md-12" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
        <?php
        $collect = collect($res);
        $total_records = count($collect);
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Income Receivables Report
            </b></h4>
        <table id="result_data_table"
            class='table table-striped table-bordered table-hover table-condensed table_sm no-padding'
            style="font-size: 12px;">
            <thead>
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th width="5%" style="padding: 2px; text-align: center; ">Sl No.</th>
                    <th width="10%" style="padding: 2px; text-align: center; ">Patient name</th>
                    <th width="9%" style="padding: 2px; text-align: center; ">UHID</th>
                    <th width="15%" style="padding: 2px;text-align: center; ">Consulting Doctor</th>
                    <th width="12%" style="padding: 2px;text-align: center; ">Company Name</th>
                    <th width="11%" style="padding: 2px; text-align: center; ">Bill No.</th>
                    <th width="10%" style="padding: 2px; text-align: center; ">Bill Tag</th>
                    <th width="10%" style="padding: 2px;text-align: center; ">Paid Status</th>
                    <th width="10%" style="padding: 2px;text-align: center; ">Payment Date</th>
                    <th width="12%" style="padding: 2px;text-align: center; ">Collected By</th>
                    <th width="10%" style="padding: 2px;text-align: center; ">Net Amt</th>
                </tr>

            </thead>
            <tbody>

                <?php
   $total_qty = 0;
   $total_net_amount = 0;
   $j=1;
   if (count($res)!=0) {
       foreach ($res as $data) {
           ?>
                <tr onclick="showRelatedDetailTable({{ $data['head_id'] }})" class="common_tr" id="common_tr{{ $data['head_id'] }}"style="cursor: pointer;">

                    <td class="common_td_rules">{{ $j }}</td>
                    <td class="common_td_rules">{{ $data['patient_name'] }}</td>
                    <td class="common_td_rules">{{ $data['uhid'] }}</td>
                    <td class="common_td_rules" title="{{ $data['consulting_doctor_name'] }}">{{ $data['consulting_doctor_name'] }}</td>
                    <td class="common_td_rules">{{ $data['company_name'] }}</td>
                    <td class="common_td_rules">{{ $data['bill_no'] }}</td>
                    <td class="common_td_rules" title="{{ $data['bill_tag'] }}">{{ $data['bill_tag'] }}</td>
                    <td class="common_td_rules">{{ $data['paidstatus'] }}</td>
                    <td class="common_td_rules">
                        {{ $data['payment_accounting_date'] ? $data['payment_accounting_date'] : '-' }}</td>
                    <td class="common_td_rules">{{ $data['cash_collected_by'] }}</td>
                    <td class="td_common_numeric_rules">{{ number_format($data['amt'],2) }}</td>

                </tr>
                <tr id="detail_table{{ $data['head_id'] }}" class="detail_table" style="display:none;cursor: pointer;">
                    <td colspan="11">
                        <table class='table table-striped table-bordered table-hover table-condensed table_sm no-padding'
                        style="font-size: 12px;margin:0px;border:2px solid">
                           <thead>
                               <tr class="headerclass"
                               style="background-color:rgb(132 166 132);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                   <th width="2%" style="padding: 2px; text-align: center; ">#</th>
                                   <th width="48%" style="padding: 2px; text-align: center; ">Item</th>
                                   <th width="20%" style="padding: 2px; text-align: center; ">Qty</th>
                                   <th width="20%" style="padding: 2px;text-align: center; ">Net Amount</th>
                                   <th width="10%" style="padding: 2px;text-align: center; ">Paid Status</th>
                               </tr>
                           </thead>
                           <tbody>
                               <tr>
                                   @php
                                       $i=1;
                                       $de_sum=0;
                                       $de_qty=0;
                                   @endphp
                                   @foreach ($data['details'] as $row)
                                   <tr>
                                       <td class="td_common_numeric_rules" >{{ $i }}</td>
                                       <td class="common_td_rules" >{{ $row['item_desc'] }}</td>
                                       <td class="td_common_numeric_rules" >{{ $row['qty'] }}</td>
                                       <td class="td_common_numeric_rules" >{{ $row['net_amount'] }}</td>
                                       <td class="td_common_numeric_rules" >{{ $row['paidstatus'] }}</td>
                                   </tr>
                                   
                                   @php
                                       $i++;
                                       $de_sum+=$row['net_amount'];
                                       $de_qty+=$row['qty'];
                                   @endphp   
                                   @endforeach
                                   <tr><td colspan="1"><b>Total</b></td><td></td><td class="td_common_numeric_rules"><b>{{ number_format($de_qty,2) }}</b></td><td class="td_common_numeric_rules"><b>{{ number_format($de_sum,2) }}</b></td></tr>
                                   
                               </tr>
                           </tbody>
                        </table>
                    </td>
                </tr>
             
                <?php
       $total_net_amount+= $data['amt'];
       $j++;
       }
       ?>

                <tr style="font-family:robotoregular">
                    <th class="common_td_rules" colspan="5"><b>Total</b></th>
                    <th class="td_common_numeric_rules"></th>
                    <th class="td_common_numeric_rules" colspan="5">{{ $total_net_amount }}</th>

                </tr>
                <?php
   }else{
       ?>
                <tr>
                    <td colspan="11" style="text-align: center;">No Results Found!</td>
                </tr>
                <?php
   }
   ?>
            </tbody>
        </table>
    </div>
</div>
