<div class="row">
	<div class="col-md-12 wrapper" id="result_container_div">
		<p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
		<p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
		@if (sizeof($res) > 0)
		@php
		$collect = collect($res);
		$total_records = count($collect);
		@endphp
		<p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
		<h4 style="text-align: center;margin-top: -29px;" id="heading"> <b> User Wise Cash Close Report </b></h4>
		<font size="16px" face="verdana">
			<table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
				<thead>
					<tr class="headerclass" style="background-color:rgb(216 216 216);color:black;border-spacing: 0 1em;font-family:sans-serif">
						<th width='5%'>SL.No</th>
						<th width='10%'>Cashier Name</th>
						@if ($checkbox == 1)
						<th width='15%'> Bill Detail </th>
						@else
						<th width='10%'>Total Collected Amt. </th>
						@endif
						<th width='10%'>Refund Cash</th>
						<th width='10%'>Net Amount</th>
						@if ($checkbox == 0)
						<th width='12%'>Cash Close Amt.</th>
						<th width='8%'>Difference Amt.</th>
						@endif
					</tr>
				</thead>
				<tbody style="font-size: 12px;">
					@php
					$cash_collection_total = 0;
					$card_other_collection = 0;
					$bill_collection = 0;
					$advance_adjust = 0;
					$cash_refund = 0;
					$cash_in_hand = 0;
					$net_amount = 0;
					$net_amounttot = 0;
					$totalrefund = 0;
					$ini_refund = 0;
					$cash_close_amnt = 0;
					$excess_amnt = 0;
					$short_amount = 0;
					$diff_amount = 0;
					$diff_amount_total = 0;
					$amount_total = 0;
					@endphp
					@foreach ($res as $data)
					<tr>
						<td class="common_td_rules">{{ $loop->iteration }}.</td>
						<td rowspan="" class="common_td_rules" style="padding:5px;width:10%;">
							{{ $data->user_name }}
						</td>
						@if ($checkbox == '1')
						<td style="padding:3px;border-bottom:1px solid;width:10%">
							<table id="inner_table" style="width:100%;">
								<tr class="headerclass" style="background-color:rgb(216 216 216);color:black;border-spacing: 0 1em;font-family:sans-serif">
									<th width='50%' style="black-space: pre;">Bill Tag</th>
									<th width='20%' style="black-space: pre;">Bill Amount</th>
									<th width='30%' style="black-space: pre;">Refund Amount</th>
								</tr>
								<tbody>
									@php
									$total = 0;
									$totalrefund = 0;
									$final_refund = 0;

									@endphp
									@foreach ($res1 as $item)
									@php
									$bill_tag =@$item->bill_type ? $item->bill_type : '' ;
									$type=@$item->type ? $item->type : '' ;

									@endphp
									@if ($data->id == $item->id)
									<tr class="adv_refund_color" style="cursor: pointer">
										<td class="inner-td">{{ $item->bill_type }}</td>
										<td class="inner-td common_td_rules">
											{{ number_format($item->bill_amount, $report_decimal_separator) }}
										</td>
										<td class="inner-td common_td_rules">
											{{ number_format($item->refund, $report_decimal_separator) }}
										</td>
										@php
										$total+=$item->bill_amount;
										$totalrefund+=$item->refund;

										@endphp
									</tr>
									@endif
									@endforeach

									<tr>
										<td><b>Total Bill Amount</b></td>
										<td class='td_common_numeric_rules'>
											<b>{{ number_format($total, $report_decimal_separator) }}</b>
										</td>
										<td class='td_common_numeric_rules'>
											<b>{{ number_format($totalrefund, $report_decimal_separator) }}</b>
										</td>
									</tr>
									@php
									$final_refund=$total-$totalrefund;
									@endphp
								</tbody>


							</table>
						</td>
						@php

						$net_amount = $total - $data->refund_cash;
						$net_amounttot += $net_amount;

						@endphp
						@else
						<td class='td_common_numeric_rules total_collection' rowspan="" style="padding:5px;width:10%;">
							{{ number_format($data->bill_collecction, $report_decimal_separator) }}
						</td>
						@php

						$net_amount = $data->bill_collecction - $data->refund_cash;
						$net_amounttot += $net_amount;

						@endphp
						@endif
						@php

						$cash_collection_total += $data->cash_collection;
						$bill_collection += $data->bill_collecction;
						$card_other_collection += $data->card_other_collection;
						$advance_adjust += $data->advance_adjust;
						$cash_refund += $data->refund_cash;
						$cash_in_hand += $data->cash_in_hand;
						$cash_close_amnt += $data->amount;
						$diff_amount_total += $data->diff_amount;
						$amount_total += $data->amount;
						@endphp
						<td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
							{{ number_format($data->refund_cash, $report_decimal_separator) }}
						</td>
						<td class='td_common_numeric_rules net_amount' rowspan="" style="padding:5px;width:10%;">
							{{ number_format($net_amount, $report_decimal_separator) }}
						</td>
						@if ($checkbox == 0)
						@php
						$html = '';
						$readonly = '';
						$btn_i_class = 'fa fa-save';
						$btn_class = 'cash_close_save';
						if ($data->amount) {
							$readonly = 'readonly';
							$btn_i_class = 'fa fa-pencil';
							$btn_class = 'cash_close_edit';
							$differenceAmt = number_format(($net_amount - $data->amount), 2, '.', '');
							if ($differenceAmt > 0) {
								$short_amount += abs($differenceAmt);
								$html = '<i class="fa fa-arrow-down" style="color: red;"></i> ' . abs($differenceAmt) . ' (Short)';
							} else {
								if ($differenceAmt != 0) {
									$excess_amnt += intval(abs($differenceAmt));
									$html = '<i class="fa fa-arrow-up" style="color: green;"></i> ' . abs($differenceAmt). ' (Excess)';
								} else {
									$html = intval($differenceAmt);
								}
							}
						}
						@endphp
						<td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:12%;">
							@php 
							$amount = '';
							if ($data->amount) {
								$amount = number_format($data->amount, $report_decimal_separator);
							}
							@endphp
							{{ $amount }}
						</td>
						@php 
							$diff_amount = '';
							if ($data->diff_amount) {
								$diff_amount = number_format($data->diff_amount, $report_decimal_separator);
							}
							@endphp
						<td class='td_common_numeric_rules cash_difference_amt' rowspan="" data-difference-amt="{{$diff_amount}}" style="padding:5px;width:8%;">{!! $diff_amount !!}
						</td>
						@endif
					</tr>
					@if ($checkbox=='1')
					@php
					$ini_refund+=$final_refund;
					@endphp
					@endif

					@endforeach

					<tr class="bg-info">
						<th class="common_td_rules" colspan='2' style="padding:5px;width:10%;"><b>Total</b></th>
						<th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
							@if ($checkbox=='1')
							@php
							$set_amt=$ini_refund;
							@endphp
							@else
							@php
							$set_amt=$bill_collection;
							@endphp
							@endif
							{{ number_format($set_amt, $report_decimal_separator) }}
						</th>
						<th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
							{{ number_format($cash_refund, $report_decimal_separator) }}
						</th>
						<th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
							{{ number_format($net_amounttot, $report_decimal_separator) }}
						</th>
						<th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
							{{ number_format($amount_total, $report_decimal_separator) }}
						</th>
						<th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
							{{ number_format($diff_amount_total, $report_decimal_separator) }}
						</th>
						
					</tr>
				</tbody>
				@else
				<h1 style="text-align: center; color: antiqueblack;">No Data Found</h1>
				@endif
			</table>
		</font>
	</div>
</div>