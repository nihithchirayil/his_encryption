<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Admit History Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                    style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='3%'>Sn. No.</th>
                        <th width='13%'>Uhid</th>
                        <th width='10%'>Ip No</th>
                        <th width='10%'>Patient</th>
                        <th width='3%'>Age</th>
                        <th width='8%'>Admitted on</th>
                        <th width='14%'>Doctor Name</th>
                        <th width='8%'>Bed</th>
                        <th width='10%'>Room Type</th>
                        <th width='15%'>Nursing Station</th>
                        <th width='15%'>Discharge Date</th>
                        @if ($checkbox == 1)
                            <th style="padding: 2px;width: 15%;text-align: center">Company Name</th>
                        @endif
                    </tr>


                </thead>
                <tbody>
                    @if (count($res) != 0)
                        @foreach ($res as $data)
                            <tr>
                                <td class="td_common_numeric_rules">{{ $loop->iteration }}.</td>
                                <td class="common_td_rules">{{ $data->op_number }}</td>
                                <td class="common_td_rules">{{ $data->ip_no }}</td>
                                <td class="common_td_rules">{{ $data->patient_name }}</td>
                                @if ($data->gender == 'Female')
                                    <td class="common_td_rules">{{ $data->age }}/F</td>
                                @else
                                    <td class="common_td_rules">{{ $data->age }}/M</td>
                                @endif

                                <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->actual_admission_date)) }}</td>
                                <td class="common_td_rules">{{ $data->doctor_name }}</td>
                                <td class="common_td_rules">{{ $data->bed_name }}</td>
                                <td class="common_td_rules">{{ $data->room_type_name }}</td>
                                <td class="common_td_rules">{{ $data->ward_name }}</td>
                                <td class="common_td_rules">
                                    {{ ($data->discharge_datetime!='') ? date('M-d-Y', strtotime($data->discharge_datetime)) : '-' }}</td>
                                @if ($checkbox == 1)
                                    <td class="common_td_rules">{{ $data->company_name }}</td>
                                @endif



                            </tr>


                        @endforeach
                    @else
                        <tr>
                            <td colspan="11" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
