<div class="row">

    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"> Doctor Collection Percentage Report</h4>
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr>
                        <th colspan="12">
                            <?= base64_decode($hospital_headder) ?>
                        </th>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="3%;">SL.No.</th>
                        <th width="27%;">Item</th>
                        <th width="7%">Amount</th>
                        <th width="7%">Item Percentage</th>
                        <th width="7%">Department Percentage</th>
                        <th width="7%">Doctor Amount</th>
                    </tr>
                </thead>
                <?php
                    $tot_amount=0.0;
                    $tot_doctor_amount=0.0;

                if(count($res)!=0){
                ?>

                <tbody>
                    <tr>
                        <td></td>
                        <td colspan="5" style="
                            float: left;
                            font-weight: 700;">{{ $doctor_name }}</td>
                    </tr>
                    <?php
          $i=1;
          foreach ($res as $data){
                ?>
                    <tr>
                        <td class="common_td_rules">{{ $i }}</td>
                        <td class="common_td_rules">{{ $data->item_dept }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->amount, 2) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->it_percentage, 2) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->d_percentage, 2) }}</td>
                        <td class="td_common_numeric_rules">{{ number_format($data->doc, 2) }}</td>
                    </tr>
                    <?php

                    $tot_amount+= floatval($data->amount);
                    $tot_doctor_amount+= floatval($data->doc);
                    $i++;
              }
              ?>
                    <tr class="" style="height: 30px;">
                        <td colspan="2" class="common_td_rules" style="text-align: left;"> <strong>Total</strong></td>
                        <td class="td_common_numeric_rules"><strong><?= number_format($tot_amount, 2) ?></strong></td>
                        <td></td>
                        <td></td>
                        <td class="td_common_numeric_rules"><strong><?= number_format($tot_doctor_amount, 2) ?></strong>
                        </td>

                    </tr>
                    <?php
           } else{ ?>
                    <tr>
                        <td colspan="12" style="text-align: center;">No Results Found!</td>
                    </tr>
                    <?php } ?>
                </tbody>

            </table>

    </div>
</div>
