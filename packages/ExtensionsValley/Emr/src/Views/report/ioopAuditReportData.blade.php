<style>
    table tr:hover td {
        text-overflow: initial;
        white-space: normal;
    }

</style>
<div class="row">

    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>

        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> IP OP Payment Audit Report </b></h4>
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="3%;">SL. No.</th>
                        <th width="33%;">Bill No.</th>
                        <th width="33%;">Bill Tag.</th>
                        <th width="10%;">Bill Date</th>
                        <th width="10%;">Payment Date</th>
                        <th width="20%;">Patient Name</th>
                        <th width="24%;">UHID</th>
                        <th width="2%;">Type</th>
                        <th width="2%;">Payment Mode</th>
                        <th width="2%;">Paid Status</th>
                        <th width="6%;">Doctor Name</th>
                        <th width="2%;">Bill Amount</th>
                        <th width="2%;">Discount Amount</th>
                        <th width="2%;">Net Amount</th>
                        <th width="1%;">Cash</th>
                        @if ($company_code=='VATHI')
                        <th width="10%;">Counter</th>
                        @endif
                        <th width="5%;">Created By</th>
                    </tr>
                </thead>
                <?php
                $nettotal_bill_amt = 0;
                $nettotal_refundamt = 0;
                $nettotal_netamt = 0;
                $nettotal_cashamt = 0;
                ?>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
            $i=0;
            $j=1;
            $billtype_name = '';
            $net_bill_amt=0;
            $net_discount_amt=0;
            $net_net_amt=0;
            $net_cash_amt=0;
         foreach ($res as $data){
            $net_amount=floatval($data->total_amount)-floatval($data->discount_amount);
            $paid_status="Paid";
                 $cash_amt=$net_amount;
                 if($data->paid_status==0){
                     $paid_status="UnPaid";
                     $cash_amt=0.0;
                 }else if($data->payment_type=='Insurance'){
                    $cash_amt=0.0;
                 }
             if($billtype_name != $data->bill_type){
                 $i=0;
                 $j=1;
                 $billtype_name = $data->bill_type;
                 $net_bill_amt=0;
                 $net_net_amt=0;
                 $net_discount_amt=0;
                 $net_cash_amt=0;
               ?>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th @if($company_code=='VATHI')  colspan="16" @else colspan="16" @endif class="common_td_rules">{{ $billtype_name }}</th>
                        </tr>
                        <tr>
                            <td class="common_td_rules">{{ $j }}</td>
                            <td class="common_td_rules">{{ $data->bill_no }}</td>
                            <td class="common_td_rules">{{ $data->bill_type }}</td>
                            <td class="common_td_rules">{{ $data->voucher_date }}</td>
                            <td class="common_td_rules">{{ $data->payment_accounting_date }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->uhid }}</td>
                            <td class="common_td_rules">{{ $data->current_visit_type }}</td>
                            <td class="common_td_rules">{{ $data->payment_type }}</td>
                            <td class="common_td_rules">{{ $paid_status }}</td>
                            <td class="common_td_rules">{{ $data->doctor_name }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($data->total_amount, 2, '.', '') }}
                            </td>
                            <td class="td_common_numeric_rules">
                                {{ number_format($data->discount_amount, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($net_amount, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($cash_amt, 2, '.', '') }}</td>
                            @if ($company_code=='VATHI')
                            <td class="common_td_rules">{{ @$data->counter_name ? $data->counter_name : '--' }}</td>
                            @endif
                            <td class="common_td_rules">{{ $data->name }}</td>

                        </tr>
                        <?php
             }else{
                 ?>
                        <tr>
                            <td class="common_td_rules">{{ $j }}</td>
                            <td class="common_td_rules">{{ $data->bill_no }}</td>
                            <td class="common_td_rules">{{ $data->bill_type }}</td>
                            <td class="common_td_rules">{{ $data->voucher_date }}</td>
                            <td class="common_td_rules">{{ $data->payment_accounting_date }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->uhid }}</td>
                            <td class="common_td_rules">{{ $data->current_visit_type }}</td>
                            <td class="common_td_rules">{{ $data->payment_type }}</td>
                            <td class="common_td_rules">{{ $paid_status }}</td>
                            <td class="common_td_rules">{{ $data->doctor_name }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($data->total_amount, 2, '.', '') }}
                            </td>
                            <td class="td_common_numeric_rules">
                                {{ number_format($data->discount_amount, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($net_amount, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($cash_amt, 2, '.', '') }}</td>
                            @if ($company_code=='VATHI')
                            <td class="common_td_rules">{{ @$data->counter_name ? $data->counter_name : '--' }}</td>
                            @endif
                            <td class="common_td_rules">{{ $data->name }}</td>

                        </tr>
                        <?php

             }
             $net_net_amt+= floatval($net_amount);
             $net_cash_amt+= floatval($cash_amt);
             $net_discount_amt+= floatval($data->discount_amount);
             $net_bill_amt+= floatval($data->total_amount);
             if($i==$department_cnt[$billtype_name]){
             ?>
                        <tr style="height: 30px;">
                            <th class="common_td_rules"  colspan="10"  style="text-align: left;">Bill Type Total</th>
                            <td class="td_common_numeric_rules">{{ number_format($net_bill_amt, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($net_discount_amt, 2, '.', '') }}
                            </td>
                            <td class="td_common_numeric_rules">{{ number_format($net_net_amt, 2, '.', '') }}</td>
                            <td class="td_common_numeric_rules">{{ number_format($net_cash_amt, 2, '.', '') }}</td>
                        </tr>
                        <?php
                    $nettotal_bill_amt +=$net_bill_amt;
                    $nettotal_refundamt +=$net_discount_amt;
                    $nettotal_netamt +=$net_net_amt;
                    $nettotal_cashamt +=$net_cash_amt;
             }
             $i++;
             $j++;

         }
         ?>
                        <tr style="height: 30px;" class="bg-info">
                            <th class="common_td_rules" colspan="10"  style="text-align: left;">Net Total</th>
                            <th class="td_common_numeric_rules">{{ number_format($nettotal_bill_amt, 2, '.', '') }}
                            </th>
                            <th class="td_common_numeric_rules">{{ number_format($nettotal_refundamt, 2, '.', '') }}
                            </th>
                            <th class="td_common_numeric_rules">{{ number_format($nettotal_netamt, 2, '.', '') }}
                            </th>
                            <th class="td_common_numeric_rules">{{ number_format($nettotal_cashamt, 2, '.', '') }}
                            </th>
                        </tr>
                    @else
                        <tr>
                            <td @if($company_code=='VATHI')  colspan="16" @else colspan="14" @endif style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
        </font>
    </div>
