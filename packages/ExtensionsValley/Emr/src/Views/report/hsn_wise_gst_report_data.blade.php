<div class="row">
    <div class="col-md-12" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b>{{ date('M-d-Y h:i A') }} </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <b><?= $from ?> To <?= $to_date ?></b></p>

        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> GST Report </b></h4>
        <div class="col-md-12" id="result_container_div">

            @php
                extract($headData);
                $result1 = $top;
                $result2 = $table1;
                $report_decimal_separator;
                $result3 = $table2;
                $result4 = $table3;
                $result5 = $table4;
                $result11 = $table5;
            @endphp

            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">

                <thead class="headerclass">
                    <tr>
                        <th class="common_td_rules" colspan="12"><strong>
                                Sales
                            </strong>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules" colspan="12"><strong>
                                {{ @$result1[0]->name ? $result1[0]->name : ''  }} &nbsp;&nbsp;&nbsp;&nbspStart From:{{ @$result1[0]->bill_no ? $result1[0]->bill_no : '' }}
                                To:{{ @$result11[0]->bill_no ? $result11[0]->bill_no : '' }}
                            </strong>
                        </th>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th>Date</th>
                        <th>Bill No</th>
                        <th>Visit Type</th>
                        <th>Item Description</th>
                        <th>HSN Code</th>
                        <th>Quantity</th>
                        <th>GST %</th>
                        <th>Sales Amount</th>
                        <th>CGST %</th>
                        <th>CGST Amount</th>
                        <th>SGST %</th>
                        <th>SGST Amount</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
            if(count($result2)!=0){

                $total_sales_amount = 0.00;
                $total_cgst_amount = 0.00;
                $total_sgst_amount = 0.00;
                $net_total = 0.00;
              foreach($result2 as $each){

                $sales_percentage=floatval($each->tax_percent);
                $sales_amount=floatval($each->salesamount);
                $cgst_percentage=floatval($each->tax_percent)/2;
                $cgst_amount=floatval($each->taxamount)/2;
                $sgst_percentage=floatval($each->tax_percent)/2;
                $sgst_amount=floatval($each->taxamount)/2;
                $total=floatval($each->net_amount);
                
                $total_sales_amount += $sales_amount;
                $total_cgst_amount += $cgst_amount;
                $total_sgst_amount += $sgst_amount;
                $net_total += $total;
                ?>
                    <tr>
                        <td class="common_td_rules"><?= date('M-d-Y', strtotime($each->payment_date)) ?></td>
                        <td class="td_common_numeric_rules"><?= $each->bill_no ?></td>
                        <td class="td_common_numeric_rules"><?= $each->visit_status ?></td>
                        <td style="text-align:left"><?= $each->item_desc ?></td>
                        <td class="td_common_numeric_rules"><?= $each->hsn_code ?></td>
                        <td class="td_common_numeric_rules"><?= $each->quantity ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_percentage ?></td>
                        <td class="td_common_numeric_rules"><?= $sales_amount ?></td>
                        <td class="td_common_numeric_rules"><?= $cgst_percentage ?></td>
                        <td class="td_common_numeric_rules"><?= $cgst_amount ?></td>
                        <td class="td_common_numeric_rules"><?= $sgst_percentage ?></td>
                        <td class="td_common_numeric_rules"><?= $sgst_amount ?></td>
                        <td class="td_common_numeric_rules"><?= round($total,2) ?></td>
                    </tr>
                    <?php
                        }
                    ?>
                    <tr>
                        <th class="common_td_rules">Total</th>
                        <th class="td_common_numeric_rules"></th>
                        <th class="td_common_numeric_rules"></th>
                        <th class="td_common_numeric_rules"></th>
                        <th class="td_common_numeric_rules"></th>
                        <th class="td_common_numeric_rules"></th>
                        <th class="td_common_numeric_rules"></th>
                        <th class="td_common_numeric_rules"><?= $total_sales_amount ?></th>
                        <th class="td_common_numeric_rules"></th>
                        <th class="td_common_numeric_rules"><?= $total_cgst_amount ?></th>
                        <th class="td_common_numeric_rules"></th>
                        <th class="td_common_numeric_rules"><?= $total_sgst_amount ?></th>
                        <th class="td_common_numeric_rules"><?= round($net_total,2) ?></th>

                    </tr>
                    <?php
            }else{
                ?>
                    <tr>
                        <td colspan="15" style="text-align: center">No Result Found</td>
                    </tr>
                    <?php
            }
            ?>

                </tbody>

                <thead class="headerclass">
                <th class="common_td_rules" colspan="12"><strong>
                                Sales Return
                            </strong>
                        </th>
                <tr>
                        <th class="common_td_rules" colspan="12"><strong>
                                {{ @$result4[0]->name ? $result4[0]->name : '' }} &nbsp;&nbsp;&nbsp;&nbspStart
                                From:{{ @$result4[0]->return_no ? $result4[0]->return_no : '' }}
                                To:{{ @$result5[0]->return_no ? $result5[0]->return_no : '' }}
                            </strong>
                        </th>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th>Date</th>
                        <th>Bill No</th>
                        <th>Visit Type</th>
                        <th>Item Description</th>
                        <th>HSN Code</th>
                        <th>Quantity</th>
                        <th>GST %</th>
                        <th>Sales Amount</th>
                        <th>CGST %</th>
                        <th>CGST Amount</th>
                        <th>SGST %</th>
                        <th>SGST Amount</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if(count($result3)!=0){

                        $total_sales_amount = 0.00;
                        $total_cgst_amount = 0.00;
                        $total_sgst_amount = 0.00;
                        $net_total = 0.00;
                      foreach($result3 as $each){
        
                        $sales_percentage=floatval($each->tax_percent);
                        $sales_amount=floatval($each->salesamount);
                        $cgst_percentage=floatval($each->tax_percent)/2;
                        $cgst_amount=floatval($each->taxamount)/2;
                        $sgst_percentage=floatval($each->tax_percent)/2;
                        $sgst_amount=floatval($each->taxamount)/2;
                        $total=floatval($each->net_amount);
                        
                        $total_sales_amount += $sales_amount;
                        $total_cgst_amount += $cgst_amount;
                        $total_sgst_amount += $sgst_amount;
                        $net_total += $total;
                        ?>
                            <tr>
                                <td class="common_td_rules"><?= date('M-d-Y', strtotime($each->payment_date)) ?></td>
                                <td class="td_common_numeric_rules"><?= $each->bill_no ?></td>
                                <td class="td_common_numeric_rules"><?= $each->visit_status ?></td>
                                <td style="text-align:left"><?= $each->item_desc ?></td>
                                <td class="td_common_numeric_rules"><?= $each->hsn_code ?></td>
                                <td class="td_common_numeric_rules"><?= $each->quantity ?></td>
                                <td class="td_common_numeric_rules"><?= $sales_percentage ?></td>
                                <td class="td_common_numeric_rules"><?= $sales_amount ?></td>
                                <td class="td_common_numeric_rules"><?= $cgst_percentage ?></td>
                                <td class="td_common_numeric_rules"><?= $cgst_amount ?></td>
                                <td class="td_common_numeric_rules"><?= $sgst_percentage ?></td>
                                <td class="td_common_numeric_rules"><?= $sgst_amount ?></td>
                                <td class="td_common_numeric_rules"><?= round($total,2) ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                            <tr>
                                <th class="common_td_rules">Total</th>
                                <th class="td_common_numeric_rules"></th>
                                <th class="td_common_numeric_rules"></th>
                                <th class="td_common_numeric_rules"></th>
                                <th class="td_common_numeric_rules"></th>
                                <th class="td_common_numeric_rules"></th>
                                <th class="td_common_numeric_rules"></th>
                                <th class="td_common_numeric_rules"><?= $total_sales_amount ?></th>
                                <th class="td_common_numeric_rules"></th>
                                <th class="td_common_numeric_rules"><?= $total_cgst_amount ?></th>
                                <th class="td_common_numeric_rules"></th>
                                <th class="td_common_numeric_rules"><?= $total_sgst_amount ?></th>
                                <th class="td_common_numeric_rules"><?= round($net_total,2) ?></th>
        
                            </tr>
                            <?php
                    }else{
                        ?>
                            <tr>
                                <td colspan="15" style="text-align: center">No Result Found</td>
                            </tr>
                            <?php
                    }

                    ?>

                </tbody>

            </table>

        </div>

    </div>
