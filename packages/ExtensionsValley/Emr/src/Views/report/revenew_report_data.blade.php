<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div id="print_data" style="margin-top: 10px">
            <?php
                $collect = collect($res);
                $total_records=count($collect);
                // $collect = $collect->groupBy('dept_name');
            ?>
            <table id="result_data_table" class="table table-condensed table_sm table-col-bordered" style="font-size: 12px;">
                <thead>
                    <tr class="thead_headerclass" style="color:black;border-spacing: 0 1em;font-family:sans-serif;">
                        <th style="text-align: left;border:none;" colspan="28">Report Print Date: <?=date('M-d-Y h:i A')?></th>
                    </tr>
                    <tr class="thead_headerclass" style="color:black;border-spacing: 0 1em;font-family:sans-serif;">
                        <th style="text-align: left;border:none;" colspan="28">Report Date: <?=$from?> To <?=$to_date?></th>
                    </tr>
                    <tr class="thead_headerclass" style="color:black;border-spacing: 0 1em;font-family:sans-serif;">
                        <th style="text-align: left;border:none;" colspan="28">Total Count: {{ $total_records }}</th>
                    </tr>
                    <tr class="thead_headerclass" style="color:black;border-spacing: 0 1em;font-family:sans-serif;">
                        <th colspan="28">Revenue Report</th>
                    </tr>
                </thead>
                <tbody style="border: 1px solid #CCC;">

                        <tr class="" style="background-color:rgb(170 203 170);color:black;border-spacing: 0 1em;font-family:sans-serif;">
                            {{-- <th colspan="3" width="35%" style="text-align:left;">{{$key}}</th> --}}
                            <th style="background-color: rgb(145 197 145);">Date</th>
                            <th colspan="5" style="background-color: rgb(145 197 145);">Consultation</th>
                            <th colspan="5" style="background-color: rgb(145 197 145);">Pharmacy</th>
                            {{-- <th colspan="5" style="background-color: rgb(145 197 145);">Discharge</th> --}}
                            <th colspan="5" style="background-color: rgb(145 197 145);">Radiology</th>
                            <th colspan="5" style="background-color: rgb(145 197 145);">Lab</th>
                            <th colspan="5" style="background-color: rgb(145 197 145);">Other Services</th>
                            <th colspan="2" style="background-color: rgb(145 197 145);"></th>
                        </tr>
                        <tr style="background-color: rgb(200 223 200);">
                            <td class="common_td_rules" style="text-align:left"></td>
                            <th class="common_td_rules" style="text-align:center !important;">General</th>
                            <th class="common_td_rules" style="text-align:center !important;">Company</th>
                            <th class="common_td_rules" style="text-align:center !important;">Refund/Return</th>
                            <th class="common_td_rules" style="text-align:center !important;color:rgb(197, 0, 0)">Cancelled</th>
                            <th class="common_td_rules" style="text-align:center !important;">Net Amount</th>
                            <th class="common_td_rules" style="text-align:center !important;">General</th>
                            <th class="common_td_rules" style="text-align:center !important;">Company</th>
                            <th class="common_td_rules" style="text-align:center !important;">Refund/Return</th>
                            <th class="common_td_rules" style="text-align:center !important;color:rgb(197, 0, 0)">Cancelled</th>

                            <th class="common_td_rules" style="text-align:center !important;">Net Amount</th>
                            <th class="common_td_rules" style="text-align:center !important;">General</th>
                            <th class="common_td_rules" style="text-align:center !important;">Company</th>
                            <th class="common_td_rules" style="text-align:center !important;">Refund/Return</th>
                            <th class="common_td_rules" style="text-align:center !important;color:rgb(197, 0, 0)">Cancelled</th>
                            <th class="common_td_rules" style="text-align:center !important;">Net Amount</th>
                            <th class="common_td_rules" style="text-align:center !important;">General</th>
                            <th class="common_td_rules" style="text-align:center !important;">Company</th>
                            <th class="common_td_rules" style="text-align:center !important;">Refund/Return</th>
                            <th class="common_td_rules" style="text-align:center !important;color:rgb(197, 0, 0)">Cancelled</th>
                            <th class="common_td_rules" style="text-align:center !important;">Net Amount</th>
                            <th class="common_td_rules" style="text-align:center !important;">General</th>
                            <th class="common_td_rules" style="text-align:center !important;">Company</th>
                            <th class="common_td_rules" style="text-align:center !important;">Refund/Return</th>
                            <th class="common_td_rules" style="text-align:center !important;color:rgb(197, 0, 0)">Cancelled</th>
                            <th class="common_td_rules" style="text-align:center !important;">Net Amount</th>
                            <th class="common_td_rules" style="text-align:center !important;">Total Discount</th>
                            <th class="common_td_rules" style="text-align:center !important;">Net Total</th>
                        </tr>
                        @if(sizeof($collect) > 0)
                        <?php
                         $consultation_general = $consultation_company = $consultation_refund = 0;
                         $consultation_cancelled = $pharmacy_cancelled = $radiology_cancelled = $lab_cancelled = $others_cancelled = 0;
                         $consultation_net     = 0;
                         $pharmacy_general     = 0;
                         $pharmacy_company     = 0;
                         $pharmacy_refund      = 0;
                         $pharmacy_net         = 0;

                         $radiology_general    = 0;
                         $radiology_company    = 0;
                         $radiology_refund     = 0;
                         $radiology_net        = 0;
                         $lab_general          = 0;
                         $lab_company          = 0;
                         $lab_refund           = 0;
                         $lab_net              = 0;
                         $others_general       = 0;
                         $others_company       = 0;
                         $others_refund        = 0;
                         $others_net           = 0;
                         $tot_discount         = 0;
                         $net_total            = 0;
                        foreach ($collect as $key => $value) {
                        ?>
                    <?php
                            $consultation_general    += floatval($value->consultation_general);
                            $consultation_company    += floatval($value->consultation_company);
                            $consultation_refund     += floatval($value->consultation_refund);
                            $consultation_net        += floatval($value->consultation_net);
                            $pharmacy_general        += floatval($value->pharmacy_general);
                            $pharmacy_company        += floatval($value->pharmacy_company);
                            $pharmacy_refund         += floatval($value->pharmacy_refund);
                            $pharmacy_net            += floatval($value->pharmacy_net);

                            $radiology_general       += floatval($value->radiology_general);
                            $radiology_company       += floatval($value->radiology_company);
                            $radiology_refund        += floatval($value->radiology_refund);
                            $radiology_net           += floatval($value->radiology_net);
                            $lab_general             += floatval($value->lab_general);
                            $lab_company             += floatval($value->lab_company);
                            $lab_refund              += floatval($value->lab_refund);
                            $lab_net                 += floatval($value->lab_net);
                            $others_general          += floatval($value->others_general);
                            $others_company          += floatval($value->others_company);
                            $others_refund           += floatval($value->others_refund);
                            $others_net              += floatval($value->others_net);
                            $tot_discount            += floatval($value->discount);
                            $net_total               += floatval($value->tot_net);
                            $consultation_cancelled  += floatval($value->consultation_cancelled);
                            $pharmacy_cancelled      += floatval($value->pharmacy_cancelled);
                            $radiology_cancelled     += floatval($value->radiology_cancelled);
                            $lab_cancelled           += floatval($value->lab_cancelled);
                            $others_cancelled        += floatval($value->others_cancelled);

                    ?>
                        <tr style="height: 30px;">
                            <td class="common_td_rules" style="white-space: nowrap;">{{$value->date}}</td>
                            <td class="common_td_rules" style="text-align:left">{{$value->consultation_general}}</td>
                            <td class="td_common_numeric_rules">{{$value->consultation_company}}</td>
                            <td class="td_common_numeric_rules">{{$value->consultation_refund}}</td>
                            <td class="td_common_numeric_rules" style="color:rgb(197, 0, 0);">{{$value->consultation_cancelled}}</td>
                            <td class="td_common_numeric_rules">{{$value->consultation_net}}</td>
                            <td class="common_td_rules" style="text-align:left">{{$value->pharmacy_general}}</td>
                            <td class="td_common_numeric_rules">{{$value->pharmacy_company}}</td>
                            <td class="td_common_numeric_rules">{{$value->pharmacy_refund}}</td>
                            <td class="td_common_numeric_rules"  style="color:rgb(197, 0, 0);">{{$value->pharmacy_cancelled}}</td>
                            <td class="td_common_numeric_rules">{{$value->pharmacy_net}}</td>

                            <td class="common_td_rules" style="text-align:left">{{$value->radiology_general}}</td>
                            <td class="td_common_numeric_rules">{{$value->radiology_company}}</td>
                            <td class="td_common_numeric_rules">{{$value->radiology_refund}}</td>
                            <td class="td_common_numeric_rules"  style="color:rgb(197, 0, 0);">{{$value->radiology_cancelled}}</td>
                            <td class="td_common_numeric_rules">{{$value->radiology_net}}</td>
                            <td class="common_td_rules" style="text-align:left">{{$value->lab_general}}</td>
                            <td class="td_common_numeric_rules">{{$value->lab_company}}</td>
                            <td class="td_common_numeric_rules">{{$value->lab_refund}}</td>
                            <td class="td_common_numeric_rules"  style="color:rgb(197, 0, 0);">{{$value->lab_cancelled}}</td>
                            <td class="td_common_numeric_rules">{{$value->lab_net}}</td>
                            <td class="common_td_rules" style="text-align:left">{{$value->others_general}}</td>
                            <td class="td_common_numeric_rules">{{$value->others_company}}</td>
                            <td class="td_common_numeric_rules">{{$value->others_refund}}</td>
                            <td class="td_common_numeric_rules"  style="color:rgb(197, 0, 0);">{{$value->others_cancelled}}</td>
                            <td class="td_common_numeric_rules">{{$value->others_net}}</td>
                            <td class="td_common_numeric_rules">{{$value->discount}}</td>
                            <td class="td_common_numeric_rules">{{$value->tot_net}}</td>
                        </tr>
                    <?php

                        // }
                        $cons_gen_total     = 0;
                        $cons_comp_total    = 0;
                        $cons_refund_total  = 0;
                        $cons_net_total     = 0;
                        $phar_total         = 0;
                        $phar_comp_total    = 0;
                        $phar_refund_total  = 0;
                        $phar_net_total     = 0;

                        $consultation_cancelled_total = 0;
                        $consultation_cancelled_total = 0;
                        $pharmacy_cancelled_total = 0;
                        $radiology_cancelled_total = 0;
                        $lab_cancelled_total = 0;
                        $others_cancelled_total = 0;

                        $rad_total = $rad_comp_total = $rad_refund_total = $rad_net_total = 0;
                        $lab_total = $lab_comp_total = $lab_refund_total = $lab_net_total = 0;
                        $others_total = $others_comp_total = $others_refund_total = $others_net_total = 0;
                        $total_discount = $total_net = 0;

                        $cons_gen_total      += $consultation_general;
                        $cons_comp_total     += $consultation_company;
                        $cons_refund_total   += $consultation_refund;
                        $cons_net_total      += $consultation_net;
                        $phar_total          += $pharmacy_general;
                        $phar_comp_total     += $pharmacy_company;
                        $phar_refund_total   += $pharmacy_refund;
                        $phar_net_total      += $pharmacy_net;

                        $rad_total           += $radiology_general;
                        $rad_comp_total      += $radiology_company;
                        $rad_refund_total    += $radiology_refund;
                        $rad_net_total       += $radiology_net;
                        $lab_total           += $lab_general;
                        $lab_comp_total      += $lab_company;
                        $lab_refund_total    += $lab_refund;
                        $lab_net_total       += $lab_net;
                        $others_total        += $others_general;
                        $others_comp_total   += $others_company;
                        $others_refund_total += $others_refund;
                        $others_net_total    += $others_net;
                        $total_discount      += $tot_discount;
                        $total_net           += $net_total;

                        $consultation_cancelled_total += $consultation_cancelled;
                        $pharmacy_cancelled_total     += $pharmacy_cancelled;
                        $radiology_cancelled_total    += $radiology_cancelled;
                        $lab_cancelled_total          += $lab_cancelled;
                        $others_cancelled_total       += $others_cancelled;
                    ?>

                    <?php
                    }
                    ?>
                    <tr style="height: 30px; background-color:#d9edf7">
                        <th colspan="1" class="common_td_rules" style="text-align:left;">Total</th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$cons_gen_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$cons_comp_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$cons_refund_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"  style="color:rgb(197, 0, 0);"><?=number_format((float)$consultation_cancelled_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$cons_net_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$phar_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$phar_comp_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$phar_refund_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"  style="color:rgb(197, 0, 0);"><?=number_format((float)$pharmacy_cancelled_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$phar_net_total, 2, '.', '')?></th>

                        <th class="td_common_numeric_rules"><?=number_format((float)$rad_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$rad_comp_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$rad_refund_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"  style="color:rgb(197, 0, 0);"><?=number_format((float)$radiology_cancelled_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$rad_net_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$lab_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$lab_comp_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$lab_refund_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"  style="color:rgb(197, 0, 0);"><?=number_format((float)$lab_cancelled_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$lab_net_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$others_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$others_comp_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$others_refund_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"  style="color:rgb(197, 0, 0);"><?=number_format((float)$others_cancelled_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$others_net_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$total_discount, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$total_net, 2, '.', '')?></th>
                    </tr>
                @else
                    <tr>
                        <td colspan="20" style="text-align: center;">No Results Found!</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
