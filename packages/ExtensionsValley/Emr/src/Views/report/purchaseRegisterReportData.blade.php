<div class="row">
 
   <div class="col-md-12" id="result_container_div">

    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">

      <thead>
          <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
              <th width='15%'>Created Date</th>
              <th width='30%'>Ledger Name</th>
              <th width='20%'>Voucher Type</th>
              <th width='20%'>Voucher No</th>
              <th width='15%'>Credit Amt.</th>
          </tr>
      </thead>
      <?php
        $tot_credit_amt=0.0;
      if(count($res)!=0){
      ?>
     
      <tbody>
          <?php 
          foreach ($res as $data){
                ?>
              <tr>
                  <td style='text-align:left' class="common_td_rules">{{$data->created_date}}</td>
                  <td style='text-align:left' class="common_td_rules">{{$data->ledger_name}}</td>
                  <td style='text-align:left' class="common_td_rules">{{$voucher_type}}</td> 
                  <td style='text-align:left' class="common_td_rules">{{$data->voucher_no}}</td>
                  <td style='text-align:right' class="td_common_numeric_rules">{{$data->credit_amt}}</td>
              </tr>
                  <?php
                  												
                $tot_credit_amt += floatval($data->credit_amt);
              }
              ?>
              <tr class="" style="height: 30px;">
                  <th  colspan="4" style="text-align: left;" >Total</th>
                  <th style='text-align:right'><?=$tot_credit_amt?></th>
              </tr>
          <?php
           } else{ ?>
      <tr>
          <td colspan="5" style="text-align: center;">No Results Found!</td>
      </tr>
      <?php } ?>
      </tbody>
     
</table>

</div>
</div>
