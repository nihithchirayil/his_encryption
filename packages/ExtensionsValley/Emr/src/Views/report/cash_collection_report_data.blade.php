
<div class="row">
    <div class="col-md-12 wrapper" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
        @if (sizeof($res) > 0)
            @php
                $collect = collect($res);
                $total_records = count($collect);
            @endphp
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"> <b> Cash Collection Report </b></h4>
            <font size="16px" face="verdana">
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                    style="font-size: 12px;">
                    <thead>
                        <tr class="headerclass"
                            style="background-color:rgb(216 216 216);color:black;border-spacing: 0 1em;font-family:sans-serif">
                            <th width='5%'>SL.No</th>
                            <th width='10%'>Cashier Name</th>
                            @if ($checkbox == 1)
                                <th width='15%'> Bill Detail </th>
                            @else
                                <th width='10%'>Total Collected Amt. </th>
                            @endif
                            <th width='10%'>Refund Cash</th>
                            <th width='10%'>Net Amount</th>
                            <th width='10%'>Cash Collection</th>
                            <th width='15%'>Card/Oth. Collection</th>
                            @if ($company_code!='ALMA')
                                <th width='10%'>Advance Adjust</th>
                            @endif
                            <th width='10%'>Cash In Hand</th>
                        </tr>
                    </thead>
                    <tbody style="font-size: 12px;">
                        @php
                            $cash_collection_total = 0;
                            $card_other_collection = 0;
                            $bill_collection = 0;
                            $advance_adjust = 0;
                            $cash_refund = 0;
                            $cash_in_hand = 0;
                            $net_amount = 0;
                            $net_amounttot = 0;
                            $totalrefund = 0;
                            $ini_refund = 0;

                        @endphp
                        @foreach ($res as $data)
                            <tr>
                                <td class="common_td_rules">{{ $loop->iteration }}.</td>
                                <td rowspan="" class="common_td_rules" style="padding:5px;width:10%;">
                                    {{ $data->user_name }}</td>
                                @if ($checkbox == '1')
                                    <td style="padding:3px;border-bottom:1px solid;width:10%">
                                        <table id="inner_table" style="width:100%;">
                                            <tr class="headerclass"
                                                style="background-color:rgb(216 216 216);color:black;border-spacing: 0 1em;font-family:sans-serif">
                                                <th width='50%'style="black-space: pre;">Bill Tag</th>
                                                <th width='20%' style="black-space: pre;">Bill Amount</th>
                                                <th width='30%'  style="black-space: pre;">Refund Amount</th>
                                            </tr>
                                            <tbody>
                                                @php
                                                    $total = 0;
                                                    $totalrefund = 0;
                                                    $final_refund = 0;

                                                @endphp
                                                @foreach ($res1 as $item)
                                                @php
                                                $bill_tag =@$item->bill_type ? $item->bill_type : '' ;
                                              $type=@$item->type ? $item->type : '' ;

                                                @endphp
                                                    @if ($data->id == $item->id)
                                                        <tr class="adv_refund_color" style="cursor: pointer" @if($company_code=='ALMA') onclick="showAdvDetails(this,'{{ $item->bill_type }}','{{ $item->type }}',{{ $item->id }}) @endif ">
                                                            <td class="inner-td">{{ $item->bill_type }}</td>
                                                            <td class="inner-td common_td_rules">
                                                                {{ number_format($item->bill_amount, $report_decimal_separator) }}
                                                            </td>
                                                            <td class="inner-td common_td_rules">
                                                                {{ number_format($item->refund, $report_decimal_separator) }}
                                                            </td>
                                                            @php
                                                                 $total+=$item->bill_amount;
                                                                 $totalrefund+=$item->refund;

                                                            @endphp
                                                        </tr>

                                                        @if ($bill_tag == 'ADVANCE' && $type=='refund' && $company_code=='ALMA')
                                                            <tr id="adv_refund_{{ $item->id }}" style="display: none" class="adv_refund" >
                                                                <td colspan="3">
                                                                    <table style="width: 100%;border: 1px solid;">
                                                                        <thead>
                                                                            <tr class="headerclass"
                                                                                style="background-color:rgb(216 216 216);color:black;border-spacing: 0 1em;font-family:sans-serif">
                                                                                <th colspan="3">Advance Refund Details</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @if (count($advance_res) > 0)
                                                                                @foreach ($advance_res as $row)
                                                                                    @if ($data->id == $row->id)
                                                                                        <tr>
                                                                                            <td class="inner-td"
                                                                                                colspan="2">
                                                                                                {{ $row->uhid }}</td>
                                                                                            <td class="inner-td common_td_rules"
                                                                                                colspan="2">
                                                                                                {{ number_format($row->refund, $report_decimal_separator) }}
                                                                                            </td>
                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                <tr>
                                                                                    <td class="inner-td" colspan="5">No
                                                                                        Record Found</td>
                                                                                </tr>
                                                                            @endif
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if($bill_tag == 'ADVANCE' && $type=='collection' && $company_code=='ALMA')
                                                        <tr id="adv_collection_{{ $item->id }}" style="display: none" class="adv_refund">
                                                            <td colspan="3">
                                                                <table style="width: 100%;border: 1px solid;">
                                                                    <thead>
                                                                        <tr class="headerclass"
                                                                            style="background-color:rgb(216 216 216);color:black;border-spacing: 0 1em;font-family:sans-serif">
                                                                            <th colspan="3">Advance Collection Details</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @if (count($collection) > 0)
                                                                            @foreach ($collection as $row)
                                                                                @if ($data->id == $row->id)
                                                                                    <tr>
                                                                                        <td class="inner-td"
                                                                                            colspan="2">
                                                                                            {{ $row->uhid }}</td>
                                                                                        <td class="inner-td common_td_rules"
                                                                                            colspan="2">
                                                                                            {{ number_format($row->collection, $report_decimal_separator) }}
                                                                                        </td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            <tr>
                                                                                <td class="inner-td" colspan="5">No
                                                                                    Record Found</td>
                                                                            </tr>
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        @endif
                                                        @if($bill_tag == 'ADVANCE ADJUST' && $type=='adjust' && $company_code=='ALMA')
                                                        <tr id="adv_adjust_{{ $item->id }}" style="display: none" class="adv_refund">
                                                            <td colspan="3">
                                                                <table style="width: 100%;border: 1px solid;">
                                                                    <thead>
                                                                        <tr class="headerclass"
                                                                            style="background-color:rgb(216 216 216);color:black;border-spacing: 0 1em;font-family:sans-serif">
                                                                            <th colspan="3">Advance Adjust Details</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @if (count($adv_adjust) > 0)
                                                                            @foreach ($adv_adjust as $row)
                                                                                @if ($data->id == $row->id)
                                                                                    <tr>
                                                                                        <td class="inner-td"
                                                                                            colspan="2">
                                                                                            {{ $row->uhid }}</td>
                                                                                        <td class="inner-td common_td_rules"
                                                                                            colspan="2">
                                                                                            {{ number_format($row->adjust, $report_decimal_separator) }}
                                                                                        </td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            <tr>
                                                                                <td class="inner-td" colspan="5">No
                                                                                    Record Found</td>
                                                                            </tr>
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        @endif
                                                    @endif
                                                @endforeach

                                                <tr>
                                                    <td><b>Total Bill Amount</b></td>
                                                    <td class='td_common_numeric_rules'>
                                                        <b>{{ number_format($total, $report_decimal_separator) }}</b>
                                                    </td>
                                                    <td class='td_common_numeric_rules'>
                                                        <b>{{ number_format($totalrefund, $report_decimal_separator) }}</b>
                                                    </td>
                                                </tr>
                                                @php
                                                    $final_refund=$total-$totalrefund;
                                                @endphp
                                            </tbody>


                                        </table>
                                    </td>
                                    @php

                                        $net_amount = $total - $data->refund_cash;
                                        $net_amounttot += $net_amount;

                                    @endphp
                                @else
                                    <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                        {{ number_format($data->bill_collecction, $report_decimal_separator) }}</td>
                                    @php

                                        $net_amount = $data->bill_collecction - $data->refund_cash;
                                        $net_amounttot += $net_amount;

                                    @endphp
                                @endif
                                @php

                                    $cash_collection_total += $data->cash_collection;
                                    $bill_collection += $data->bill_collecction;
                                    $card_other_collection += $data->card_other_collection;
                                    $advance_adjust += $data->advance_adjust;
                                    $cash_refund += $data->refund_cash;
                                    $cash_in_hand += $data->cash_in_hand;
                                @endphp
                                <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($data->refund_cash, $report_decimal_separator) }}</td>
                                <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($net_amount, $report_decimal_separator) }}</td>
                                <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($data->cash_collection, $report_decimal_separator) }}</td>
                                <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($data->card_other_collection, $report_decimal_separator) }}</td>
                                    @if ($company_code!='ALMA')
                                      <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($data->advance_adjust, $report_decimal_separator) }}</td>
                                    @endif

                                <td class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                    {{ number_format($data->cash_in_hand, $report_decimal_separator) }}</td>


                            </tr>
                            @if ($checkbox=='1')
                            @php
                            $ini_refund+=$final_refund;
                            @endphp
                            @endif

                        @endforeach

                        <tr class="bg-info">
                            <th class="common_td_rules" colspan='2' style="padding:5px;width:10%;"><b>Total</b></th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                @if ($checkbox=='1')
                                    @php
                                        $set_amt=$ini_refund;
                                    @endphp
                                @else
                                    @php
                                        $set_amt=$bill_collection;
                                    @endphp
                                @endif
                                {{ number_format($set_amt, $report_decimal_separator) }}</th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($cash_refund, $report_decimal_separator) }}</th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($net_amounttot, $report_decimal_separator) }}</th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($cash_collection_total, $report_decimal_separator) }}</th>
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($card_other_collection, $report_decimal_separator) }}</th>
                           @if ($company_code!='ALMA')
                                <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($advance_adjust, $report_decimal_separator) }}</th>
                           @endif
                            <th class='td_common_numeric_rules' rowspan="" style="padding:5px;width:10%;">
                                {{ number_format($cash_in_hand, $report_decimal_separator) }}</th>
                        </tr>



                    </tbody>
                @else
                    <h1 style="text-align: center; color: antiqueblack;">No Data Found</h1>
        @endif
        </table>
        </font>
    </div>
</div>
