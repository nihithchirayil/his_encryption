<div class="row print_data" id="result_data_table">

    <div class="col-md-12 theadscroll  always-visible" style="padding:0px;" id="result_container_div" style="overflow-x: hidden;
    overflow-y: scroll;">
        <p style="font-size: 12px;" id="total_data">Report Print Date:
            <?= date('M-d-Y h:i A') ?>
        </p>
        <p style="font-size: 12px;" id="total_data">Report Date:
            <?= $from_date ?> To
            <?= $to_date ?>
        </p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"> <b>Cash Collection Report Detail</b></h4>
        <div class="col-md-12 theadscroll" id="input_table" style="height:400px;padding:0px;">
        <div id="print_data" >
            <table class='table table-condensed table_sm table-col-bordered theadfix_wrapper' style="font-size: 12px;">
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif;position:sticky;top:0px;">
                    <th width="4%;">Sl. No.</th>
                    <th width="8%;">Receipt No.</th>
                    <th width="9%;">Bill No.</th>
                    <th width="10%;">Bill Tag</th>
                    <th width="10%;">Bill Date Time</th>
                    @if ($company_code == 'VATHI')
                    <th width="12%;">Payment Date Time</th>
                    @endif

                    <th width="10%;">UHID</th>
                    <th width="10%;">Patient Name</th>
                    <th width="9%;">Doctor Name</th>

                    <th width="5%;">Bill Amt.</th>
                    <th width="4%;">Amt. Adj.</th>
                    <th width="5%;">Amt. Coll.</th>
                    <th width="4%;">Payment Mode</th>
                    @if ($company_code == 'VATHI')
                    <th width="10%;">Tran.No./Card No.</th>
                    @else
                        @if($enable_card_num == 1)
                        <th width="10%;">Card No / Name</th>
                        @endif
                    @endif
                    <th width="9%;">PC Name</th>

                </tr>
                </thead>
                <?php
                    $enable_card_num = @$enable_card_num ? $enable_card_num : 0;
                    $nettotal_bill_net_amount = 0;
                    $nettotal_collected_amount = 0;
                    $nettotal_adj_amount = 0;
                    $bill_date_sort = @$bill_date_sort ? $bill_date_sort : 0;
                    ?>
                @if(sizeof($res) > 0 && $bill_date_sort == 1)
                <?php
                       $i=0;
                       $user_name = '';
                       $total_bill_net_amount = 0;
                       $total_collected_amount = 0;
                       $total_adj_amount = 0;
                       $slno=1;
                       $make_me_vanish='';
                       $bill_no_init='';
                       $make_border='';
                       foreach ($res as $data){
                               $i=0;
                               $user_name = $data->user_name;
                               $total_bill_net_amount = 0.0;
                               $total_collected_amount = 0.0;
                               $total_adj_amount = 0.0;
                               // $slno=1;
                             ?>

                    @if ($bill_no_init != $data->bill_no)
                    @php
                    $bill_no_init = $data->bill_no;
                    $make_me_vanish = '';
                    $make_border = '';
                    @endphp
                    @else
                    @php
                    $make_me_vanish = 'color:white';
                    // $make_border = 'border-top:1px solid black;border-bottom:1px solid black';
                    $total_bill_net_amount -= $data->bill_net_amount;
                    $slno -= 1;

                    @endphp
                    @endif
                    <tr>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules">{{ $slno }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->recipt_no }}">
                            {{ $data->recipt_no }}</td>
                        <td class="common_td_rules" title="{{ $data->bill_no }}">{{ $data->bill_no }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->bill_tag }}">
                            {{ $data->bill_tag }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules"
                            title="{{ date('d-M-Y h:i A', strtotime($data->collected_date)) }}">
                            {{ date('d-M-Y h:i A', strtotime($data->collected_date)) }}</td>
                        @if ($company_code == 'VATHI')
                        <td style="{{ $make_me_vanish }}" class="common_td_rules"
                            title="{{ date('d-M-Y h:i A', strtotime($data->paymentdate)) }}">
                            {{ date('d-M-Y h:i A', strtotime($data->paymentdate)) }}</td>
                        @endif
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->uhid }}">
                            {{ $data->uhid }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->patient_name }}">
                            {{ $data->patient_name }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->doctor_name }}">
                            {{ $data->doctor_name }}
                        </td>

                        <td style="{{ $make_me_vanish }}" class="td_common_numeric_rules">
                            {{ $data->bill_net_amount }}
                        </td>


                        <td style="{{ $make_me_vanish }}" yy class="td_common_numeric_rules">
                            {{ $data->advance_adjusted }}</td>
                       
                        <td style='{{ $make_border }}' class="td_common_numeric_rules">
                            {{ number_format($data->collected_amount, 2) }}</td>
                        <td style='{{ $make_border }}' class="common_td_rules" title="{{ $data->payment_mode }}">
                            {{ $data->payment_mode }}</td>
                        @if ($company_code == 'VATHI')
                        <td style='{{ $make_border }}' class="common_td_rules" title="{{ $data->tran_id }}">{{
                            @$data->tran_id ? $data->tran_id : '--' }}
                        </td>
                        @else
                            @if($enable_card_num == 1)
                            <td style='{{ $make_border }}' class="common_td_rules" title="{{ $data->tran_id }}">{{
                                @$data->tran_id ? $data->tran_id : '--' }}
                            </td>
                            @endif
                        @endif
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ @$data->pc_name ? $data->pc_name : '' }}">
                            {{ @$data->pc_name ? $data->pc_name : '' }}
                        </td>
                    </tr>
                <?php 
                $slno++;
                $total_bill_net_amount+= floatval($data->bill_net_amount);
                $total_collected_amount+= floatval($data->collected_amount);
                $total_adj_amount+= floatval($data->advance_adjusted);
                $nettotal_bill_net_amount+=$total_bill_net_amount;
                $nettotal_collected_amount+=$total_collected_amount;
                $nettotal_adj_amount+=$total_adj_amount;
                } ?>
                <tr class="bg-info" style="height: 30px;">
                        <th class="common_td_rules" @if ($company_code=='VATHI' ) colspan="9" @else colspan="8" @endif
                            style="text-align: left;">Total</th>
                        <th class="td_common_numeric_rules">
                            @if ($company_code != 'KEYHOLE')
                            {{ number_format(round($nettotal_bill_net_amount), 2, '.', '') }}
                            @endif
                        </th>
                        <th class="td_common_numeric_rules">
                            @if ($company_code != 'KEYHOLE')
                            {{ number_format($nettotal_adj_amount, 2, '.', '') }}
                            @endif
                        </th>

                        <th class="td_common_numeric_rules" colspan="1">
                            <?= number_format(round($nettotal_collected_amount), 2, '.', '') ?>
                        </th>
                        <th></th>
                        <th></th>
                        @if($enable_card_num == 1)
                        <th></th>
                        <th></th>
                        @endif
                    </tr>
                @else
                @if (sizeof($res) > 0)
                <tbody>
                    <?php
                       $i=0;
                       $user_name = '';
                       $total_bill_net_amount = 0;
                       $total_collected_amount = 0;
                       $total_adj_amount = 0;
                       $slno=1;
                       $make_me_vanish='';
                       $bill_no_init='';
                       $make_border='';
                       foreach ($res as $data){

                           if($user_name != $data->user_name){
                               $i=0;
                               $user_name = $data->user_name;
                               $total_bill_net_amount = 0.0;
                               $total_collected_amount = 0.0;
                               $total_adj_amount = 0.0;
                               $slno=1;
                             ?>

                    @if ($bill_no_init != $data->bill_no)
                    @php
                    $bill_no_init = $data->bill_no;
                    $make_me_vanish = '';
                    $make_border = '';
                    @endphp
                    @else
                    @php
                    $make_me_vanish = 'color:white';
                    // $make_border = 'border-top:1px solid black;border-bottom:1px solid black';
                    $slno -= 1;

                    @endphp
                    @endif


                    <tr class="headerclass"
                        style="background-color:rgb(171 163 220);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th @if ($company_code=='VATHI' ) colspan="14" @else colspan="13" @endif
                            class="common_td_rules">{{ $user_name }}</th>
                            @if($enable_card_num == 1)
                            <th></th>
                            @endif
                    </tr>
                    <tr>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules">{{ $slno }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->recipt_no }}">
                            {{ $data->recipt_no }}</td>
                        <td class="common_td_rules" title="{{ $data->bill_no }}">{{ $data->bill_no }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->bill_tag }}">
                            {{ $data->bill_tag }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules"
                            title="{{ date('d-M-Y h:i A', strtotime($data->collected_date)) }}">
                            {{ date('d-M-Y h:i A', strtotime($data->collected_date)) }}</td>
                        @if ($company_code == 'VATHI')
                        <td style="{{ $make_me_vanish }}" class="common_td_rules"
                            title="{{ date('d-M-Y h:i A', strtotime($data->paymentdate)) }}">
                            {{ date('d-M-Y h:i A', strtotime($data->paymentdate)) }}</td>
                        @endif
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->uhid }}">
                            {{ $data->uhid }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->patient_name }}">
                            {{ $data->patient_name }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->doctor_name }}">
                            {{ $data->doctor_name }}
                        </td>
                       
                        <td style="{{ $make_me_vanish }}" class="td_common_numeric_rules">
                            {{ $data->bill_net_amount }}
                        </td>


                        <td style="{{ $make_me_vanish }}" yy class="td_common_numeric_rules">
                            {{ $data->advance_adjusted }}</td>
                       
                        <td style='{{ $make_border }}' class="td_common_numeric_rules">
                            {{ number_format($data->collected_amount, 2) }}</td>
                        <td style='{{ $make_border }}' class="common_td_rules" title="{{ $data->payment_mode }}">
                            {{ $data->payment_mode }}</td>
                        @if ($company_code == 'VATHI')
                        <td style='{{ $make_border }}' class="common_td_rules" title="{{ $data->tran_id }}">{{
                            @$data->tran_id ? $data->tran_id : '--' }}
                        </td>
                        @else
                            @if($enable_card_num == 1)
                            <td style='{{ $make_border }}' class="common_td_rules" title="{{ $data->tran_id }}">{{
                                @$data->tran_id ? $data->tran_id : '--' }}
                            </td>
                            @endif
                        @endif
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ @$data->pc_name ? $data->pc_name : ''}}">
                            {{ @$data->pc_name ? $data->pc_name : ''}}
                        </td>
                       
                    </tr>
                    <?php
                            }else{
                                ?>

                    @if ($bill_no_init != $data->bill_no)
                    @php
                    $bill_no_init = $data->bill_no;
                    $make_me_vanish = '';
                    $make_border = '';

                    @endphp
                    @else
                    @php
                    $make_me_vanish = 'color:white;';
                    // $make_border = 'border-top:1px solid black;border-bottom:1px solid black';
                    $total_bill_net_amount -= $data->bill_net_amount;
                    $slno -= 1;

                    @endphp
                    @endif
                    <tr>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules">{{ $slno }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->recipt_no }}">
                            {{ $data->recipt_no }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->bill_no }}">
                            {{ $data->bill_no }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->bill_tag }}">
                            {{ $data->bill_tag }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules"
                            title="{{ date('d-M-Y h:i A', strtotime($data->collected_date)) }}">
                            {{ date('d-M-Y h:i A', strtotime($data->collected_date)) }} </td>
                        @if ($company_code == 'VATHI')
                        <td style="{{ $make_me_vanish }}" class="common_td_rules"
                            title="{{ date('d-M-Y h:i A', strtotime($data->paymentdate)) }}">
                            {{ date('d-M-Y h:i A', strtotime($data->paymentdate)) }} </td>
                        @endif

                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->uhid }}">
                            {{ $data->uhid }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->patient_name }}">
                            {{ $data->patient_name }}</td>
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ $data->doctor_name }}">
                            {{ $data->doctor_name }}
                        </td>


                        <td style="{{ $make_me_vanish }}" class="td_common_numeric_rules"
                            title="{{ $data->bill_net_amount }}">
                            {{ $data->bill_net_amount }}</td>
                        <td style="{{ $make_me_vanish }}" class="td_common_numeric_rules"
                            title="{{ $data->advance_adjusted }}">
                            {{ $data->advance_adjusted }}</td>
                       
                        <td style='{{ $make_border }}' class="td_common_numeric_rules"
                            title="{{ round($data->collected_amount) }}">
                            {{ number_format($data->collected_amount, 2) }}</td>
                        <td style='{{ $make_border }}' class="common_td_rules" title="{{ $data->payment_mode }}">
                            {{ $data->payment_mode }}</td>
                        @if ($company_code == 'VATHI')
                        <td style='{{ $make_border }}' class="common_td_rules" title="{{ $data->tran_id }}">{{
                            @$data->tran_id ? $data->tran_id : '--' }}
                        </td>
                        @else
                            @if($enable_card_num == 1)
                            <td style='{{ $make_border }}' class="common_td_rules" title="{{ $data->tran_id }}">{{
                                @$data->tran_id ? $data->tran_id : '--' }}
                            </td>
                            @endif
                        @endif
                        <td style="{{ $make_me_vanish }}" class="common_td_rules" title="{{ @$data->pc_name ? $data->pc_name : '' }}">
                            {{ @$data->pc_name ? $data->pc_name : '' }}
                        </td>
                       
                    </tr>   
                    <?php

                             }
                             $slno++;
                             $total_bill_net_amount+= floatval($data->bill_net_amount);
                             $total_collected_amount+= floatval($data->collected_amount);
                             $total_adj_amount+= floatval($data->advance_adjusted);
                             if($i==$user_namecnt[$user_name]){
                             ?>
                    <tr class="" style="height: 30px;">
                        <th class="common_td_rules" @if ($company_code=='VATHI' ) colspan="9" @else colspan="8" @endif
                            style="text-align: left;">User Total</th>

                        <th class="td_common_numeric_rules" colspan="">
                            @if ($company_code != 'KEYHOLE')
                            {{ number_format(round($total_bill_net_amount), 2, '.', '') }}
                            @endif
                        </th>
                        <th class="td_common_numeric_rules">
                            @if ($company_code != 'KEYHOLE')
                            {{ number_format($total_adj_amount, 2, '.', '') }}
                            @endif
                        </th>
                        <th class="td_common_numeric_rules" colspan="1">
                            <?= number_format(round($total_collected_amount), 2, '.', '') ?>
                        </th>
                        <th></th>

                    </tr>
                    <?php
                                     $nettotal_bill_net_amount+=$total_bill_net_amount;
                                     $nettotal_collected_amount+=$total_collected_amount;
                                     $nettotal_adj_amount+=$total_adj_amount;
                                }
                                $i++;

                            }
                            ?>
                    <tr class="bg-info" style="height: 30px;">
                        <th class="common_td_rules" @if ($company_code=='VATHI' ) colspan="9" @else colspan="8" @endif
                            style="text-align: left;">Total</th>
                        <th class="td_common_numeric_rules">
                            @if ($company_code != 'KEYHOLE')
                            {{ number_format(round($nettotal_bill_net_amount), 2, '.', '') }}
                            @endif
                        </th>
                        <th class="td_common_numeric_rules">
                            @if ($company_code != 'KEYHOLE')
                            {{ number_format($nettotal_adj_amount, 2, '.', '') }}
                            @endif
                        </th>

                        <th class="td_common_numeric_rules" colspan="1">
                            <?= number_format(round($nettotal_collected_amount), 2, '.', '') ?>
                        </th>
                        <th></th>
                        <th></th>
                        @if($enable_card_num == 1)
                        <th></th>
                        <th></th>
                        @endif
                    </tr>
                    @else
                    <tr>
                        <td @if ($company_code=='VATHI' ) colspan="15" @else colspan="13" @endif
                            style="text-align: center;">No Results Found!</td>
                    </tr>
                    @endif

                </tbody>
                @endif
            </table>
        </div>
        </div>
        <div class="clearfix"></div>

    </div>
    @if (sizeof($res) > 0 && ($company_code == 'VATHI' || $company_code == 'KEYHOLE'))
    <div class="col-md-12">
        <div class="col-md-12 box-body">

            <div class="col-md-6 box-body theadscroll" id="summary_data" style="height: 500px">
                <div class="col-md-12" style="padding: 0px">
                    <p class="bg-info" style="padding-left: 8px !important;"><strong> Payment mode Summary By
                            Date</strong></p>

                    <table class='table table-condensed table_sm table-col-bordered ' style="font-size: 12px;">
                        <thead>
                            <tr class="headerclass"
                                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                <th width="5%;">Sl. No.</th>
                                <th width="10%;">Collection Date</th>
                                <th width="10%;">Payment Mode</th>
                                <th width="10%;">Amount</th>

                            </tr>
                        </thead>
                        <tbody>
                            @if (count($payment_mode_array) > 0)

                            @foreach ($payment_mode_array as $key => $datas)
                            @php
                            $total = 0;
                            @endphp
                            @foreach ($datas as $index => $data)
                            <tr>
                                <td class="td_common_numeric_rules">{{ $loop->iteration }}</td>
                                <td class="common_td_rules">{{ $key }}</td>
                                <td class="common_td_rules">{{ $index }}</td>
                                <td class="td_common_numeric_rules">
                                    {{ number_format(round($data['amount'], 2)) }}</td>
                                @php
                                $total += $data['amount'];
                                @endphp
                            </tr>
                            @endforeach
                            <tr style="border: 2px solid lightblue;">
                                <td colspan="3"><strong>GTotal</strong></td>
                                <td class="td_common_numeric_rules">
                                    <strong>{{ number_format(round($total), 2) }}</strong>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4">No Record Found.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="col-md-6 box-body theadscroll" id="mode_summary" style="height: 500px">
                <div class="col-md-12" style="padding: 0px">
                    <p class="bg-info" style="padding-left: 8px !important;"><strong>Payment mode Summary</strong>
                    </p>
                    <table class='table table-condensed table_sm table-col-bordered ' style="font-size: 12px;">
                        <thead>
                            <tr class="headerclass"
                                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                <th width="5%;">Sl. No.</th>
                                <th width="10%;">User Name</th>
                                <th width="10%;">Payment Mode</th>
                                <th width="10%;">Amount</th>
                                <th width="10%;">Transcation Count</th>

                            </tr>
                        </thead>
                        <tbody>
                            @if (count($user_wise_array) > 0)

                            @foreach ($user_wise_array as $key => $data)
                            @php
                            $total = 0;
                            $count = 1;
                            @endphp
                            @foreach ($data as $index => $row)
                            <tr>
                                <td class="td_common_numeric_rules">{{ $loop->iteration }}</td>
                                <td class="common_td_rules">{{ $key }}</td>
                                <td class="common_td_rules">{{ $index }}</td>
                                <td class="td_common_numeric_rules">
                                    {{ number_format(round($row['amount']), 2) }}</td>

                                <td class="td_common_numeric_rules">{{ $row['t_count'] }}</td>
                            </tr>
                            @php
                            $total += $row['amount'];
                            $count++;
                            @endphp
                            @endforeach
                            <tr style="border: 2px solid lightblue;">
                                <td colspan="4"><strong>Collection Amount OF {{ $key }}
                                    </strong></td>
                                <td class="td_common_numeric_rules">
                                    <strong>{{ number_format(round($total), 2) }}</strong>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="5">No Record Found.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12 box-body">
            <p class="bg-info" style="padding-left: 8px !important;text-align:center"><strong>Return Summary</strong>
            </p>
            <div class="col-md-6 box-body theadscroll" id="summary_data_retrun" style="height: 500px">
                <div class="col-md-12" style="padding: 0px">
                    <p class="bg-info" style="padding-left: 8px !important;"><strong>Return Payment mode Summary By
                            Date</strong></p>

                    <table class='table table-condensed table_sm table-col-bordered ' style="font-size: 12px;">
                        <thead>
                            <tr class="headerclass"
                                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                <th width="5%;">Sl. No.</th>
                                <th width="10%;">Return Date</th>
                                <th width="10%;">Payment Mode</th>
                                <th width="10%;">Amount</th>

                            </tr>
                        </thead>
                        <tbody>
                            @if (count($return_payment_mode_array) > 0)

                            @foreach ($return_payment_mode_array as $key => $datas)
                            @php
                            $total = 0;
                            @endphp
                            @foreach ($datas as $index => $data)
                            <tr>
                                <td class="td_common_numeric_rules">{{ $loop->iteration }}</td>
                                <td class="common_td_rules">{{ $key }}</td>
                                <td class="common_td_rules">{{ $index }}</td>
                                <td class="td_common_numeric_rules">
                                    {{ number_format(round($data['amount'], 2)) }}</td>
                                @php
                                $total += $data['amount'];
                                @endphp
                            </tr>
                            @endforeach
                            <tr style="border: 2px solid lightblue;">
                                <td colspan="3"><strong>GTotal</strong></td>
                                <td class="td_common_numeric_rules">
                                    <strong>{{ number_format(round($total), 2) }}</strong>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4">No Record Found.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="col-md-6 box-body theadscroll" id="mode_summary_return" style="height: 500px">
                <div class="col-md-12" style="padding: 0px">
                    <p class="bg-info" style="padding-left: 8px !important;"><strong>Return Payment mode
                            Summary</strong>
                    </p>
                    <table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
                        <thead>
                            <tr class="headerclass"
                                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                <th width="5%;">Sl. No.</th>
                                <th width="10%;">User Name</th>
                                <th width="10%;">Payment Mode</th>
                                <th width="10%;">Amount</th>
                                <th width="10%;">Transcation Count</th>

                            </tr>
                        </thead>
                        <tbody>
                            @if (count($return_user_wise_array) > 0)

                            @foreach ($return_user_wise_array as $key => $data)
                            @php
                            $total = 0;
                            $count = 1;
                            @endphp
                            @foreach ($data as $index => $row)
                            <tr>
                                <td class="td_common_numeric_rules">{{ $loop->iteration }}</td>
                                <td class="common_td_rules">{{ $key }}</td>
                                <td class="common_td_rules">{{ $index }}</td>
                                <td class="td_common_numeric_rules">
                                    {{ number_format(round($row['amount']), 2) }}</td>

                                <td class="td_common_numeric_rules">{{ $row['t_count'] }}</td>
                            </tr>
                            @php
                            $total += $row['amount'];
                            $count++;
                            @endphp
                            @endforeach
                            <tr style="border: 2px solid lightblue;">
                                <td colspan="4"><strong>Collection Amount OF {{ $key }}
                                    </strong></td>
                                <td class="td_common_numeric_rules">
                                    <strong>{{ number_format(round($total), 2) }}</strong>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="5">No Record Found.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif

</div>
