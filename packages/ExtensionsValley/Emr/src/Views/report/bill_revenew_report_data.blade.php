<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div id="print_data" style="margin-top: 10px">
            <?php
                $bill_amount_total = $net_amount_total = $paid_amount_total = $advance_adjusted_total = $bill_net_amoumt = $refund_amoumt_total = 0;
                $collect = collect($res);
                $total_records = count($collect);
            ?>
            <table id="result_data_table" class="table table-condensed table_sm table-col-bordered "
                style="font-size: 12px;">
                <thead>
                    <tr class="thead_headerclass" style="color:black;border-spacing: 0 1em;font-family:sans-serif;">
                        <th style="text-align: left;border:none;" colspan="28">Report Print Date:
                            <?=date('M-d-Y h:i A')?></th>
                    </tr>
                    <tr class="thead_headerclass" style="color:black;border-spacing: 0 1em;font-family:sans-serif;">
                        <th style="text-align: left;border:none;" colspan="28">Report Date: <?=$from?> To <?=$to_date?>
                        </th>
                    </tr>
                    <tr class="thead_headerclass" style="color:black;border-spacing: 0 1em;font-family:sans-serif;">
                        <th style="text-align: left;border:none;" colspan="28">Total Count: {{ $total_records }}</th>
                    </tr>
                </thead>
            </table>

            <table id="result_data_table_data"
                class="table table-condensed table_sm table-col-bordered theadfix_wrapper" style="font-size: 12px;">
                <thead>
                    <tr class="thead_headerclass"
                        style="color: white;border-spacing: 0 1em;font-family:sans-serif; background-color: #07ad8c;">
                        <th colspan="15"> Bill Wise Revenue Report </th>
                    </tr>
                    <tr style="background-color: #07ad8c; color: white;">
                        <td class="common_td_rules" style="text-align:left; width:1%;">Sl.No</td>
                        <th class="common_td_rules" style="text-align:center !important;  width:1%;"> Type
                        </th>
                        <th class="common_td_rules" style="width:2%;">UHID</th>
                        <th class="common_td_rules" style="width:2%;">Patient Name</th>
                        <th class="common_td_rules" style="width:3%;">Bill No</th>
                        <th class="common_td_rules" style="width:2%;">Bill Tag</th>
                        <th class="common_td_rules" style="width:7%;">Bill Date</th>
                        <th class="common_td_rules" style="width:7%;">Payment Date</th>
                        <th class="common_td_rules" style="width:3%;">Company Name</th>

                        <th class="common_td_rules" style="width:3%;">Pricing Name</th>
                        <th class="common_td_rules" style="width:2%;">Paid Status</th>
                        <th class="common_td_rules" style="width:2%;">Bill Amount</th>
                        <th class="common_td_rules" style="width:2%;">Paid Amount</th>
                        <th class="common_td_rules" style="width:2%;">Net Amount</th>
                        <th class="common_td_rules" style="width:1%;">Advance Adjusted
                        </th>
                    </tr>
                </thead>
                <tbody style="border: 1px solid #CCC;">
                    @if(sizeof($collect) > 0)
                    <?php   
                        $i = 0;                     
                        foreach ($collect as $key => $value) {
                            $i++;
                        ?>
                    <tr style="height: 30px;" style="background-color: rgb(200 223 200);">
                        <td class="common_td_rules" style="white-space: nowrap;">{{$i}}</td>
                        <td class="common_td_rules" style="text-align:left">{{$value->transaction_type}}</td>
                        <td class="common_td_rules">{{$value->uhid}}</td>
                        <td class="common_td_rules">{{$value->patient_name}}</td>
                        <td class="common_td_rules">{{$value->bill_no}}</td>
                        <td class="common_td_rules" style="text-align:left">{{$value->bill_tag}}</td>
                        <td class="common_td_rules">{{$value->bill_date}}</td>
                        <td class="common_td_rules">{{$value->payment_date}}</td>
                        <td class="common_td_rules">{{$value->company_name}}</td>
                        <td class="common_td_rules" style="text-align:left">{{$value->pricing_name}}</td>
                        <td class="common_td_rules">{{$value->paid_status}}</td>
                        <td class="td_common_numeric_rules">{{$value->bill_amount}}</td>
                        <td class="td_common_numeric_rules" style="text-align:left">{{$value->paid_amount}}</td>
                        <td class="td_common_numeric_rules"> @if ($value->transaction_type == 'Return') - @endif
                            {{$value->net_amount}}</td>
                        <td class="td_common_numeric_rules">{{$value->advance_adjusted}}</td>
                    </tr>

                    <?php
                        $bill_amount_total     += $value->bill_amount;
                        $paid_amount_total     += $value->paid_amount;
                        $advance_adjusted_total+= $value->advance_adjusted;
                        if ($value->transaction_type == 'Return') {
                            $net_amount_total -= $value->net_amount;
                        } else {
                            $net_amount_total += $value->net_amount;
                        }
                    ?>

                    <?php
                    }
                    ?>

                    <tr style="height: 30px; background-color:#d9edf7">
                        <th colspan="11" class="common_td_rules" style="text-align:left;">Total</th>

                        <th class="td_common_numeric_rules">
                            <?=number_format((float)$bill_amount_total, 2, '.', '')?>
                        </th>
                        <th class="td_common_numeric_rules">
                            <?=number_format((float)$paid_amount_total, 2, '.', '')?>
                        </th>
                        <th class="td_common_numeric_rules">
                            <?=number_format((float)round($net_amount_total), 2, '.', '')?>
                        </th>
                        <th class="td_common_numeric_rules">
                            <?=number_format((float)$advance_adjusted_total, 2, '.', '')?></th>

                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
