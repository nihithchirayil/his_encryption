<div class="row">
    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"> <b>Medication Indent Report</b></h4>
        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
            style="font-size: 12px;">
            <thead>
                <tr class="headerclass"
                    style="background-color:#36A693;color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th width='1%'>Sn. No.</th>
                    <th width='11%'>Uhid</th>
                    <th width='12%'>Patient Name</th>
                    <th width='11%'>Doctor Name</th>
                    <th width='11%'>Medicine</th>
                    <th width='9%'>Generic Name</th>
                    @if($indent == 1)
                    <th width='5%'>Intend Date</th>
                    @else
                    <th width='5%'>Visit Date</th>
                    @endif
                    <th width='9%'>Location From</th>
                    <th width='7%'>Location Name</th>
                    <th width='5%'>Converted Status</th>
                    <th width='10%'>Created User</th>
                    <th width='3%'>Indent Quantity</th>
                    <th width='3%'>Billed Quantity</th>
                    <th width='4%'>Billed Amount</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($res)!=0){
                    $total=0.0;
                    $total1=0.0;
                    $total_amt=0.0;
                    $i=1;
                foreach ($res as $data){
                    $total += $data->billed_qty;
                    $total1 += $data->indent_quantity;
                    $total_amt += $data->billed_amount;
                    if ($data->out_ind=='1'){
                      $bgcolor="background-color:#a4e4a5";
                    }
                    else{
                        $bgcolor="";
                    }
                    ?>
                <tr style= "{{ $bgcolor }}">
                    <td class="td_common_numeric_rules">{{ $i }}.</td>
                    <td class="common_td_rules">{{ $data->uhid }}</td>
                    <td class="common_td_rules">{{ $data->patient_name }}</td>
                    <td class="common_td_rules">{{ $data->doctor_name }}</td>
                    <td class="common_td_rules">{{ $data->medicine }}</td>
                    <td class="common_td_rules">{{ $data->generic_name }}</td>
                    @if($indent == 1)
                    <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->intend_date)) }}</td>
                    @else
                    <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->visit_datetime)) }}
                    </td>
                    @endif
                    <td class="common_td_rules">{{ $data->location_from }}</td>
                    <td class="common_td_rules">{{ $data->location_name }}</td>
                    <td class="common_td_rules">{{ $data->billconverted_status }}</td>
                    <td class="common_td_rules">{{ $data->user_name }}</td>
                    <td class="td_common_numeric_rules">{{ $data->indent_quantity }}</td>
                    <td class="td_common_numeric_rules">{{ $data->billed_qty }}</td>
                    <td class="td_common_numeric_rules">{{ $data->billed_amount }}</td>
                </tr>
                <?php
                    $i++;
                    }
                    ?>
                <tr class="" style="background-color:#c9c9c9;">
                    <td colspan="11" class="common_td_rules">Total</td>
                    <td class="td_common_numeric_rules"><?= $total1 ?></td>
                    <td class="td_common_numeric_rules"><?= $total ?></td>
                    <td class="td_common_numeric_rules"><?= $total_amt ?></td>
                </tr>
                <?php
                 }else{
                     ?>
                <tr>
                    <td colspan="12" style="text-align: center">No Match Found</td>
                </tr>
                <?php
                 }
                 ?>
            </tbody>
        </table>
    </div>
</div>
