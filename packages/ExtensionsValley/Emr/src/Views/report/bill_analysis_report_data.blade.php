<div class="row">

    <div class="col-md-12" id="result_container_div">
    <p style="font-size: 12px;" id="total_data">Report Print Date: <?=date('Y-m-d h:i A')?> </p>
      <p style="font-size: 12px;" id="total_data">Report Date: <?=$from?> To <?=$to?></p>
      <?php
      $total_records=0;
      if (count($res)!=0) {
          $collect = collect($res);
          $total_records=count($collect);
      }
      ?>
     <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
     <h4 style="text-align: center;margin-top: -29px;" id="heading"> Bill Analysis Report</h4>
     <font size="16px" face="verdana" >
     <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
     <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
              <th width="30%;">Doctor Name</th>
              <th width="7%">Cons. Fee</th>
              <th width="7%">Hosp. Fee</th>
              <th width="7%">MLC Charge</th>
              <th width="7%">Conv. Fee</th>
              <th width="7%">Tot. Amount</th>
              <th width="7%">Cons. Refund Amt.</th>
              <th width="7%">Hosp. Refund Amt.</th>
              <th width="7%">Conv. Refund Amt.</th>
              <th width="7%">Tot. Refund Amt.</th>
              <th width="7%">Net Amt.</th>
          </tr>
      </thead>
      <?php
        $tot_consultation_fee=0.0;
        $tot_hospital_fee=0.0;
        $tot_mlc_charge=0.0;
        $tot_convenience_fee=0.0;
        $tot_tot_amount=0.0;
        $tot_consultation_refund_amt=0.0;
        $tot_hosp_refund_amt=0.0;
        $tot_conv_refund_amt=0.0;
        $tot_tot_refund_amount=0.0;
        $net_amount_tot=0.0;

      if(count($res)!=0){
      ?>

      <tbody>
          <?php
          foreach ($res as $data){
            $net_amt = floatval($data->tot_amount)-floatval($data->tot_refund_amount);
                ?>
              <tr>
                  <td class="common_td_rules">{{$data->doctor_name}}</td>
                  <td class="td_common_numeric_rules">{{$data->consultation_fee}}</td>
                  <td class="td_common_numeric_rules">{{$data->hospital_fee}}</td>
                  <td class="td_common_numeric_rules">{{$data->mlc_charge}}</td>
                  <td class="td_common_numeric_rules">{{$data->convenience_fee}}</td>
                  <td class="td_common_numeric_rules">{{$data->tot_amount}}</td>
                  <td class="td_common_numeric_rules">{{$data->consultation_refund_amt}}</td>
                  <td class="td_common_numeric_rules">{{$data->hosp_refund_amt}}</td>
                  <td class="td_common_numeric_rules">{{$data->conv_refund_amt}}</td>
                  <td class="td_common_numeric_rules">{{$data->tot_refund_amount}}</td>
                  <td class="td_common_numeric_rules">{{$net_amt}}</td>
              </tr>
                  <?php

                  $tot_consultation_fee+= floatval($data->consultation_fee);
                  $tot_hospital_fee+= floatval($data->hospital_fee);
                  $tot_mlc_charge+= floatval($data->mlc_charge);
                  $tot_convenience_fee+= floatval($data->convenience_fee);
                  $tot_tot_amount+= floatval($data->tot_amount);
                  $tot_consultation_refund_amt+= floatval($data->consultation_refund_amt);
                  $tot_hosp_refund_amt+= floatval($data->hosp_refund_amt);
                  $tot_conv_refund_amt+= floatval($data->conv_refund_amt);
                  $tot_tot_refund_amount+= floatval($data->tot_refund_amount);
                  $net_amount_tot+=$net_amt;
              }
              ?>
              <tr class="" style="height: 30px;">
                  <td class="common_td_rules" style="text-align: left;"> <strong>Total</strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_consultation_fee?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_hospital_fee?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_mlc_charge?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_convenience_fee?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_tot_amount?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_consultation_refund_amt?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_hosp_refund_amt?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_conv_refund_amt?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$tot_tot_refund_amount?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=$net_amount_tot?></strong></td>
              </tr>
          <?php
           } else{ ?>
      <tr>
          <td colspan="11" style="text-align: center;">No Results Found!</td>
      </tr>
      <?php } ?>
      </tbody>

</table>

</div>
</div>
