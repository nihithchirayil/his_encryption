<div class="row">
    <div class="theadscroll always-visible" id="result_container_div" style="position: relative;height:510px;">
        <div class="col-md-12" id="result_container_div">
            <div id="print_data" style="margin-top: 10px">
                <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
                <?php
                $collect = collect($res);
                $total_records = count($collect);
                ?>
                <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
                <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Stock Movement Report </b></h4>

                @if ($transaction_type == 4)
                    @if (sizeof($res) > 0)
                        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                            style="font-size: 12px;">
                            <thead>
                                <tr class="headerclass"
                                    style="background-color:rgb(54 166 147);color:#000;border-spacing: 0 1em;font-family:sans-serif">
                                    <th width="5%">Sn. No.</th>
                                    <th width="10%">Item Name </th>
                                    <th width="10%">Sold qty</th>
                                    <th width="10%">Item Sold Value</th>
                                    <th width="5%">Consumed qty</th>
                                    <th width="15%">Total Quantity</th>
                                    <th width="5%">Avg.Stock</th>
                                    <th width="10%">Item Stock Value</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 12px;">
                                @php
                                    $total1 = $total2 = $total3 = $total4 = $total5 = $total6 = 0;

                                @endphp
                                @foreach ($res as $data)
                                    <tr>
                                        <td class="td_common_numeric_rules">{{ $loop->iteration }}.</td>
                                        <td class="common_td_rules">{{ $data->item_desc }}</td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->bill_quantity, $report_decimal_separator) }}</td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->item_sold_value, $report_decimal_separator) }}</td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->consumed_quantity, $report_decimal_separator) }}
                                        </td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->total_qty, $report_decimal_separator) }}</td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->stock, $report_decimal_separator) }}</td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->item_stock_value, $report_decimal_separator) }}</td>
                                    </tr>
                                    @php
                                        $total1 += $data->bill_quantity;
                                        $total2 += $data->item_sold_value;
                                        $total3 += $data->consumed_quantity;
                                        $total4 += $data->total_qty;
                                        $total5 += $data->stock;
                                        $total6 += $data->item_stock_value;
                                    @endphp
                                @endforeach
                                <tr>
                                    <td><b>Total</b>
                                    <td></td>
                                    <td><b>{{ number_format($total1, $report_decimal_separator) }}</b></td>
                                    <td><b>{{ number_format($total2, $report_decimal_separator) }}</b>
                                    </td>
                                    <td><b>{{ number_format($total3, $report_decimal_separator) }}</b></td>
                                    <td><b>{{ number_format($total4, $report_decimal_separator) }}</b></td>
                                    <td><b>{{ number_format($total5, $report_decimal_separator) }}</b></td>
                                    <td><b>{{ number_format($total6, $report_decimal_separator) }}</b></td>
                                </tr>
                            </tbody>
                        @else
                            <h1 style="text-align: center; color: antiquewhite;">No Record Found</h1>
                    @endif
                    </table>
                @endif
                @if ($transaction_type == 3)
                    @if (sizeof($res) > 0)
                        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                            style="font-size: 12px;">
                            <thead>
                                <tr class="headerclass"
                                    style="background-color:rgb(54 166 147);color:#000;border-spacing: 0 1em;font-family:sans-serif">
                                    <th width="5%">Sn. No.</th>
                                    <th width="35%">Item Name</th>
                                    <th width="25%">Category</th>
                                    <th width="25%">Sub Category</th>
                                    <th width="10%">Quantity</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 12px;">
                                @php
                                    $total = 0;
                                @endphp
                                @foreach ($res as $data)
                                    <tr>
                                        <td class="td_common_numeric_rules">{{ $loop->iteration }}.</td>
                                        <td class="common_td_rules">{{ $data->item_desc }}</td>
                                        <td class="common_td_rules">{{ $data->category_name }}</td>
                                        <td class="common_td_rules">{{ $data->group_name }}</td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->issued_qty, $report_decimal_separator) }}</td>

                                        @php
                                            $total += $data->issued_qty;
                                        @endphp
                                    </tr>
                                @endforeach
                                <tr>
                                    <td><b>Total</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>{{ number_format($total, $report_decimal_separator) }}</b></td>
                                </tr>
                            </tbody>
                        @else
                            <h1 style="text-align: center; color: antiquewhite;">No Record Found</h1>
                    @endif
                    </table>



                @endif
                @if ($transaction_type == 2)
                    @if (sizeof($res) > 0)
                            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                            style="font-size: 12px;">
                            <thead>
                                <tr class="headerclass"
                                    style="background-color:rgb(54 166 147);color:#000;border-spacing: 0 1em;font-family:sans-serif">
                                    <th width="5%;">Sn. No.</th>
                                    <th width="35%">Item Name </th>
                                    <th width="25%">Category</th>
                                    <th width="25%">Sub Category</th>
                                    <th width="10%">Quantity</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 12px;">
                                @php
                                    $total = 0;
                                @endphp
                                @foreach ($res as $data)
                                    <tr>
                                        <td class="td_common_numeric_rules">{{ $loop->iteration }}.</td>
                                        <td class="common_td_rules">{{ $data->item_desc }}</td>
                                        <td class="common_td_rules">{{ $data->category_name }}</td>
                                        <td class="common_td_rules">{{ $data->group_name }}</td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->issued_qty, $report_decimal_separator) }}</td>
                                        @php
                                            $total += $data->issued_qty;
                                        @endphp
                                    </tr>
                                @endforeach
                                <tr>
                                    <td><b>Total</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>{{ number_format($total, $report_decimal_separator) }}</b></td>
                                </tr>
                            </tbody>
                        @else
                            <h1 style="text-align: center; color: antiquewhite;">No Record Found</h1>
                    @endif
                    </table>



                @endif
                @if ($transaction_type == 1)

                    @if (sizeof($res) > 0)
                            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                            style="font-size: 12px;">
                            <thead>
                                <tr class="headerclass"
                                    style="background-color:rgb(54 166 147);color:#000;border-spacing: 0 1em;font-family:sans-serif">
                                    <th width="5%">Sn. No.</th>
                                    <th width="20%">Item Name </th>
                                    <th width="15%">Category</th>
                                    <th width="10%">Sub Category</th>
                                    <th width="10%">Doctor Name</th>
                                    <th width="10%">Quantity Sold </th>
                                    <th width="10%">Item Sold Value</th>
                                    <th width="10%">Avg.Stock</th>
                                    <th width="10%">Item Stock Value</th>

                                </tr>
                            </thead>

                            @php
                                $total1 = $total2 = $total3 = $total4 = 0;

                            @endphp
                            <tbody style="font-size: 12px;">
                                @foreach ($res as $data)
                                    <tr>
                                        <td class="td_common_numeric_rules">{{ $loop->iteration }}.</td>
                                        <td class="common_td_rules">{{ $data->item_desc }}</td>
                                        <td class="common_td_rules">{{ $data->category_name }}</td>
                                        <td class="common_td_rules">{{ $data->sub_name }}</td>
                                        <td class="common_td_rules">{{ $data->name }}</td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->qnty_sold, $report_decimal_separator) }}</td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->item_sold_value, $report_decimal_separator) }}</td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->stock, $report_decimal_separator) }}</td>
                                        <td class="td_common_numeric_rules">
                                            {{ number_format($data->item_stock_value, $report_decimal_separator) }}
                                        </td>

                                    </tr>
                                    @php
                                        $total1 += $data->qnty_sold;
                                        $total2 += $data->item_sold_value;
                                        $total3 += $data->stock;
                                        $total4 += $data->item_stock_value;
                                    @endphp
                                @endforeach
                                <tr>
                                    <td class="common_td_rules"><b>Total</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="td_common_numeric_rules"><b>{{ number_format($total1, $report_decimal_separator) }}</b></td>
                                    <td class="td_common_numeric_rules"><b>{{ number_format($total2, $report_decimal_separator) }}</b>
                                    </td>
                                    <td class="td_common_numeric_rules"><b>{{ number_format($total3, $report_decimal_separator) }}</b></td>
                                    <td class="td_common_numeric_rules"><b>{{ number_format($total4, $report_decimal_separator) }}</b></td>
                                </tr>
                            </tbody>
                        @else
                            <h1 style="text-align: center; color: antiquewhite;">No Record Found</h1>
                    @endif
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
