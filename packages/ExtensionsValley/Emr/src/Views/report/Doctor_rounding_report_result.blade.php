<style>table, th, td {
    border-bottom: 1px solid #dad9d954;
  
  }
  td {
    
    padding-left: 2px;

  }</style>
 
<div class="row">
  @php
  // echo"<pre>";
  //     print_r($res[1]->service_datetime);
  // echo"</pre>";
  //     exit();
  @endphp
  
  <div class="theadscroll always-visible" id="result_container_div" style="position: relative;height:510px;" >
   
     @if(sizeof($res)>0)
     <p style="font-size: 12px;" id="total_data">Report Date: {{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}} </p><br>
     @php
      $collect = collect($res); $total_records=count($collect);   
     @endphp 
     <p style="font-size: 12px;">Total Records<b> : {{$total_records}}</b></p>
     <h2 style="text-align: center;margin-top: -29px;" id="heading"> Doctor Rounding Report  </h2>
     <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper' style="font-size: 12px;">

      <thead class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:robotoregular">
        <th  width="5%"  style="padding: 2px;text-align: center; ">Sn. No.</th>
        <th  width="35%" style="padding: 2px;text-align: center;border:2px; ">Doctor Name</th>
         <th width="30%"  style="padding: 2px;text-align: center">Service Date</th>
         <th width="30%"  style="padding: 2px;text-align: center">Service Done On</th>
         
       
         
   </thead>
      <tbody style="font-size: 12px;">
          
          @foreach ($res as $data)
              <tr >
                  <td class="td_common_nummeric_rules">{{ $loop->iteration}}.</td>
                  <td class="td_common_rules" >{{$data->doctor_name}}</td>
                   @php
                   $dug=json_decode($data->service_datetime);
                   @endphp
                  
                   <td>
                     <table style="width:100%;border:none">
                        
                   
                     <tr style="border:none">
                      @foreach ($dug as $item)
                       <td  style="border:none"><tr style="border:none"><td class="td_common_nummeric_rules" style="border:none">{{date('M-d-Y ',strtotime($item))}}</td></tr></td>
                       @endforeach
                      </tr>
                    </table>
                  </td>
                  
                  
                   <td class="td_common_rules" >{{$data->service_doneon}}</td>
                   
                </tr>
            
            
          @endforeach
         
      </tbody>
          @else 
          <h1 style="text-align: center; color: antiquewhite;">No Record Found</h1>
            @endif   
 </table>
    </div>
 </div>
 