<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div id="print_data" style="margin-top: 10px">
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>

            @if ($filter_type == 1)
                <table id="" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
                    <thead>
                        <tr class="headerclass"
                            style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                            <th style="text-align: left" colspan="5">Report Print Date: <?= date('M-d-Y h:i A') ?>
                            </th>
                        </tr>
                        <tr class="headerclass"
                            style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                            <th style="text-align: left" colspan="5">Report Date: <?= $from_date ?> To
                                <?= $to_date ?></th>
                        </tr>
                        <tr class="headerclass"
                            style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                            <th style="text-align: left" colspan="5">Total Count: {{ $total_records }}</th>
                        </tr>
                        <tr class="headerclass"
                            style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="5">{{ $report_header }}</th>
                        </tr>
                        <tr class="headerclass"
                            style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                            <th width='5%'>Si No.</th>
                            <th width='80%'>Department Name</th>
                            <th width='5%'>Total Amount</th>
                            <th width='5%'>Refund Amount</th>
                            <th width='5%'>Final</th>
                        </tr>
                    </thead>

                    <tbody>
                    @php
                        $total_amt = 0.0;
                        $refund_amt = 0.0;
                    @endphp
                        @if (count($res) != 0)
                            @php
                                $doctor_name = '';
                                $i = 1;
                                $dr_first = 0;
                                $dr_total_amt = 0.0;
                                $dr_refund_amt = 0.0;
                            @endphp

                            @foreach ($res as $data) 
                            @php
                            $total_amt += floatval($data->amount);
                            $refund_amt += floatval($data->sum);
                            @endphp
                                @if ($doctor_name != $data->doctor_name)
                                    @php
                                        $doctor_name = $data->doctor_name;
                                        $i = 1;
                                        $dr_first = 0;
                                        $dr_total_amt = 0.0;
                                        $dr_refund_amt = 0.0;
                                    @endphp
                                    <tr
                                        style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                                        <td></td>
                                        <td style='float:left;padding-left:10px'>
                                            <b>{{ $data->doctor_name }}</b>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    @php
                                        $dr_first = 1;
                                        $dr_refund_amt = 0.0;
                                        $dr_total_amt = 0.0;
                                    @endphp
                                @endif
                                <tr>
                                    <td class="td_common_numeric_rules"><?= $i ?></td>
                                    <td class="common_td_rules"><?= $data->dept_name ?></td>
                                    <td class="td_common_numeric_rules"><?= $data->amount ?></td>
                                    <td class="td_common_numeric_rules"><?= $data->sum ?></td>
                                    <td class="td_common_numeric_rules"><?= $data->amount - $data->sum ?>
                                    </td>
                                </tr>
                                @php
                                $dr_total_amt += floatval($data->amount);
                                $dr_refund_amt += floatval($data->sum);
                                @endphp
                                @if($i==$doctor_namecnt[$doctor_name])
                                <tr>
                                    <td></td>
                                    <td style='float:left;padding-left:10px'><b>Sub Total</i></b></td>
                                    <td class="td_common_numeric_rules">
                                        <b><?= $dr_total_amt ?></b>
                                    </td>
                                    <td class="td_common_numeric_rules">
                                        <b><?= $dr_refund_amt ?></b>
                                    </td>
                                    <td class="td_common_numeric_rules">
                                        <b><?= $dr_total_amt - $dr_refund_amt ?></b>
                                    </td>
                                </tr>
                               
                            @endif
                                @php
                                    $i++;                                  
                                @endphp
                            @endforeach
                            <tr class="bg-blue" style="height: 30px;">
                                <th colspan="2" class="common_td_rules">Total</th>
                                <th class="td_common_numeric_rules"><?= number_format((float) $total_amt, 2, '.', '') ?>
                                </th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format((float) $refund_amt, 2, '.', '') ?></th>
                                <th class="td_common_numeric_rules">
                                    <?= number_format((float) $total_amt - $refund_amt, 2, '.', '') ?></th>
                            </tr>
                        @else
                            <tr>
                                <td colspan="12" style="text-align: center;">No Results Found!</td>
                            </tr>
                        @endif
                    </tbody>
                </table>





        <!--------------departmentwise----------------------------->
    @elseif($filter_type == 2)
        <table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
            <thead>
                <tr class="headerclass"
                    style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                    <th style="text-align: left" colspan="5">Report Print Date: <?= date('M-d-Y h:i A') ?></th>
                </tr>
                <tr class="headerclass"
                    style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                    <th style="text-align: left" colspan="5">Report Date: <?= $from_date ?> To <?= $to_date ?></th>
                </tr>
                <tr class="headerclass"
                    style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                    <th style="text-align: left" colspan="5">Total Count: {{ $total_records }}</th>
                </tr>
                <tr class="headerclass"
                    style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                    <th colspan="5">{{ $report_header }}</th>
                </tr>
                <tr class="headerclass"
                    style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                    <th width='5%'>Si No.</th>
                    <th width='80%'>Doctor Name</th>
                    <th width='5%'>Total Amount</th>
                    <th width='5%'>Refund Amount</th>
                    <th width='5%'>Final</th>
                </tr>
            </thead>

            <tbody>
            @php
                $total_amt = 0.0;
                $refund_amt = 0.0;
            @endphp
                @if (count($res) != 0)
                    @php
                        $dept_name = '';
                        $i = 1;
                        $dr_first = 0;
                        $dr_total_amt = 0.0;
                        $dr_refund_amt = 0.0;

                    @endphp

                    @foreach ($res as $data)
                    @php
                    $total_amt += floatval($data->amount);
                    $refund_amt += floatval($data->sum);
                   @endphp
                        @if ($dept_name != $data->dept_name)
                        @php
                        $dept_name = $data->dept_name;
                        $i = 1;
                        $dr_first = 0;
                        $dr_total_amt = 0.0;
                        $dr_refund_amt = 0.0;
                    @endphp
                            <tr
                                style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                                <td></td>
                                <td style='float:left;padding-left:10px'>
                                    <b>{{ $data->dept_name }}</b>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            @php
                                $dr_first = 1;
                                $dr_refund_amt = 0.0;
                                $dr_total_amt = 0.0;
                            @endphp
                        @endif
                        <tr>
                            <td class="td_common_numeric_rules"><?= $i ?></td>
                            <td class="common_td_rules"><?= $data->doctor_name ?></td>
                            <td class="td_common_numeric_rules"><?= $data->amount ?></td>
                            <td class="td_common_numeric_rules"><?= $data->sum ?></td>
                            <td class="td_common_numeric_rules"><?= $data->amount - $data->sum ?>
                            </td>
                        </tr>
                        @php
                            $dr_total_amt += floatval($data->amount);
                            $dr_refund_amt += floatval($data->sum);
                        @endphp
                        @if($i == $dept_namecnt[$dept_name])
                                <tr>
                             <td></td>
                             <td style='float:left;padding-left:10px'><b>Sub Total</i></b></td>
                             <td class="td_common_numeric_rules">
                                 <b><?= $dr_total_amt ?></b>
                             </td>
                             <td class="td_common_numeric_rules">
                                 <b><?= $dr_refund_amt ?></b>
                             </td>
                             <td class="td_common_numeric_rules">
                                 <b><?= $dr_total_amt - $dr_refund_amt ?></b>
                             </td>
                         </tr>
                         
                     @endif
                     @php
                            $i++;
                           
                        @endphp
                    @endforeach
                        
                    <tr class="bg-blue" style="height: 30px;">
                        <th colspan="2" style="text-align: left;" class="common_td_rules">Total</th>
                        <th class="td_common_numeric_rules"><?= number_format((float) $total_amt, 2, '.', '') ?>
                        </th>
                        <th class="td_common_numeric_rules">
                            <?= number_format((float) $refund_amt, 2, '.', '') ?></th>
                        <th class="td_common_numeric_rules">
                            <?= number_format((float) $total_amt - $refund_amt, 2, '.', '') ?></th>
                    </tr>
                @else
                    <tr>
                        <td colspan="12" style="text-align: center;">No Results Found!</td>
                    </tr>
                @endif
            </tbody>
        </table>


        <!--------------departmentwise----------------------------->
    @else($filter_type == 3)
        <table class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
            <thead>
                <tr class="headerclass"
                    style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                    <th style="text-align: left" colspan="5">Report Print Date: <?= date('M-d-Y h:i A') ?></th>
                </tr>
                <tr class="headerclass"
                    style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                    <th style="text-align: left" colspan="5">Report Date: <?= $from_date ?> To <?= $to_date ?></th>
                </tr>
                <tr class="headerclass"
                    style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                    <th style="text-align: left" colspan="5">Total Count: {{ $total_records }}</th>
                </tr>
                <tr class="headerclass"
                    style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                    <th colspan="5">{{ $report_header }}</th>
                </tr>
                <tr class="headerclass"
                    style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                    <th width='5%'>Si No.</th>
                    <th width='80%'>Doctor Name</th>
                    <th width='5%'>Total Amount</th>
                    <th width='5%'>Refund Amount</th>
                    <th width='5%'>Final</th>
                </tr>
            </thead>

            <tbody>
                @php
                $total_amt = 0.0;
                $refund_amt = 0.0;
            @endphp
                @if (count($res) != 0)
                    @php
                        $name = '';
                        $i = 1;
                        $dr_first = 0;
                        $dr_total_amt = 0.0;
                        $dr_refund_amt = 0.0;

                    @endphp

                    @foreach ($res as $data)
                    @php
                    $total_amt += floatval($data->amount);
                    $refund_amt += floatval($data->sum);
                    @endphp
                        @if ($name != $data->name)
                        @php
                            $name = $data->name;
                            $i = 1;
                            $dr_first = 0;
                            $dr_total_amt = 0.0;
                            $dr_refund_amt = 0.0;
                        @endphp

                            <tr style="background-color:#d5d0d0;">
                                <td></td>
                                <td style='float:left;padding-left:10px'>
                                    <b>{{ $data->name }}</b>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            @php
                                $dr_first = 1;
                                $dr_refund_amt = 0.0;
                                $dr_total_amt = 0.0;
                            @endphp
                        @endif
                        <tr>
                            <td class="td_common_numeric_rules"><?= $i ?></td>
                            <td class="common_td_rules"><?= $data->doctor_name ?></td>
                            <td class="td_common_numeric_rules"><?= $data->amount ?></td>
                            <td class="td_common_numeric_rules"><?= $data->sum ?></td>
                            <td class="td_common_numeric_rules"><?= $data->amount - $data->sum ?>
                            </td>
                        </tr>
                        @php
                            $dr_total_amt += floatval($data->amount);
                            $dr_refund_amt += floatval($data->sum);
                        @endphp
                        @if($i==$speccnt[$name])
                                <tr>
                            <td></td>
                            <td style='float:left;padding-left:10px'><b>Sub Total</i></b></td>
                            <td class="td_common_numeric_rules">
                                <b><?= $dr_total_amt ?></b>
                            </td>
                            <td class="td_common_numeric_rules">
                                <b><?= $dr_refund_amt ?></b>
                            </td>
                            <td class="td_common_numeric_rules">
                                <b><?= $dr_total_amt - $dr_refund_amt ?></b>
                            </td>
                        </tr>
                        
                     @endif
                            @php
                                $i++;                                  
                            @endphp                    
                    @endforeach
                    <tr class="bg-blue" style="height: 30px;">
                        <th colspan="2" style="text-align: left;" class="common_td_rules">Total</th>
                        <th class="td_common_numeric_rules"><?= number_format((float) $total_amt, 2, '.', '') ?>
                        </th>
                        <th class="td_common_numeric_rules">
                            <?= number_format((float) $refund_amt, 2, '.', '') ?></th>
                        <th class="td_common_numeric_rules">
                            <?= number_format((float) $total_amt - $refund_amt, 2, '.', '') ?></th>
                    </tr>
                @else
                    <tr>
                        <td colspan="12" style="text-align: center;">No Results Found!</td>
                    </tr>
                @endif
            </tbody>
        </table>
   @endif
        </div>
    </div>
</div>

