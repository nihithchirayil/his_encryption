<div class="row" id="print_data">
    <div class="col-md-12" id="result_container_div">
        <input type="hidden" id="printdata_spanspan" value="{{ count($ledger) }}">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('Y-m-d h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from_date ?> To <?= $to_date ?></p>
        <?php
        $total_records = 0;
        if (count($sql_spec) != 0) {
            $collect = collect($sql_spec);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <font size="16px" face="verdana">
            @if ($insurance == 0)
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                    style="font-size: 12px;white-space: nowrap;display: inline-block;overflow-x: auto;">
                    <thead>
                        <tr class="headerclass"
                            style="background-color: rgb(36 163 142);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th width="5%" style="padding: 2px; text-align: center; ">Sl No.</th>
                            <th>Speciality</th>
                            @foreach ($ledger as $spec)
                                <th>{{ $spec->ledger_name }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <?php
                    $net_amount = 0;
                    $i = 1;
                    ?>
                    @if (sizeof($sql_spec) > 0)
                        <tbody>
                            @foreach ($sql_spec as $key => $data)
                                <tr
                                    style="color:black;border-spacing: 0 1em;font-family:sans-serif;border-bottom: 1pt solid #d4cbcb;">
                                    <td class='common_td_rules'>
                                        {{ $i++ }}
                                    </td>
                                    <td class='common_td_rules' width="50%" style=''>
                                        <b>{{ $data->name }}</b>
                                    </td>

                                    @foreach ($ledger as $val)
                                        @php  $res_spec_amount = @$res_spec[$data->id][$val->id] ? $res_spec[$data->id][$val->id] : 0; @endphp
                                        <td class="td_common_numeric_rules"> {{ $res_spec_amount }} </td>
                                    @endforeach
                                </tr>
                            @endforeach
                            <tr class="" style="height: 30px;background-color: #64c8b4;color:white;">
                                <th colspan="2" class="common_td_rules">Total</th>
                                @foreach ($ledger as $val)
                                    @php
                                        $res_ledger_amount = @$ledger_amount_sum_array[$val->id] ? $ledger_amount_sum_array[$val->id] : 0;
                                    @endphp
                                    <th class="td_common_numeric_rules"> {{ $res_ledger_amount }} </th>
                                @endforeach


                            </tr>
                        </tbody>
                    @endif
                </table>
            @else
                <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                    style="font-size: 12px;white-space: nowrap;display: inline-block;overflow-x: auto;">
                    <thead>
                        <tr class="headerclass"
                            style="background-color: rgb(36 163 142);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th width="5%" style="padding: 2px; text-align: center; ">Sl No.</th>
                            <th width="30%">Speciality</th>
                            @foreach ($ledger as $spec)
                                <th width="70%">{{ $spec->ledger_name }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <?php
                    $net_amount = 0;
                    $i = 1;
                    ?>
                    @if (sizeof($sql_spec) > 0)
                        <tbody>
                            @foreach ($sql_spec as $key => $data)
                                <tr
                                    style="color:black;border-spacing: 0 1em;font-family:sans-serif;border-bottom: 1pt solid #d4cbcb;">
                                    <td class='common_td_rules'>
                                        {{ $i++ }}
                                    </td>
                                    <td class='common_td_rules' width="10%" style=''>
                                        <b>{{ $data->name }}</b>
                                    </td>

                                    @foreach ($ledger as $val)
                                        @php  $res_spec_amount = @$res_spec[$data->id][$val->id] ? $res_spec[$data->id][$val->id] : 0; @endphp
                                        <td class="td_common_numeric_rules"> {{ $res_spec_amount }} </td>
                                    @endforeach
                                </tr>
                            @endforeach
                            <tr class="" style="height: 30px;background-color: #64c8b4;color:white;">
                                <th colspan="2" class="common_td_rules">Total</th>
                                @foreach ($ledger as $val)
                                    @php
                                        $res_ledger_amount = @$ledger_amount_sum_array[$val->id] ? $ledger_amount_sum_array[$val->id] : 0;
                                    @endphp
                                    <th class="td_common_numeric_rules"> {{ $res_ledger_amount }} </th>
                                @endforeach


                            </tr>
                        </tbody>
                    @endif
                </table>
            @endif
        </font>
    </div>
</div>
