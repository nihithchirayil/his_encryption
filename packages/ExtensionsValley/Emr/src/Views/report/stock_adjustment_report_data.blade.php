<style>table, th, td {
    border: 1px solid #00000054;
    font-size: 10px;

}</style>
<div class="row">
        <div class="col-md-12" id="result_container_div" border='2px !important'; >
     <table id="result_data_table" class="" style="font-size:xx-small !important;font-family: 'robotoregular';">

     @if(sizeof($res)>0)
     <p style="font-size: smaller;padding-left: 10px">Report Date: <span>{{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}}</span> </p><br>
     <h2 style="text-align: center;margin-top: -29px;"> Discharge Summary Report  </h2>

     <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
         <th  style="padding: 2px;width:10%;text-align: center;border:2px; ">Patient Name</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Age/Gender</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Address</th>
         <th  style="padding: 2px;width:10%;text-align: center;">UHID</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Ip No</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Doctor Name</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Admit Date</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Discharge Date</th>
         {{-- <th  style="padding: 2px;width:10%;text-align: center;">summary Prepared By</th> --}}
         <th  style="padding: 2px;width:10%;text-align: center;">Created At</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Last Updated At</th>
         {{-- <th  style="padding: 2px;width:10%;text-align: center;">Discharge Summary</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Edit</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Doctor Approval Status</th> --}}
     </tr>

     @foreach ($res as $data)
        <tr>
            <td rowspan="" style="padding:5px;width:10%;">{{$data->patient_name}}</td>
            @if ($data->gender == 'Female')
            <td rowspan="" style="padding:5px;width:10%;" >{{$data->age}}/F</td>
            @else
            <td rowspan="" style="padding:5px;width:10%;" >{{$data->age}}/M</td>
            @endif
            <td rowspan="" style="padding:5px;width:10%;">{{($data->address)}}</td>
            <td rowspan="" style="padding:5px;width:10%;">{{($data->op_no)}}</td>
            <td rowspan="" style="padding:5px;width:10%;">{{($data->ip_no)}}</td>
            <td rowspan="" style="padding:5px;width:10%;">{{($data->doctor_name)}}</td>
            <td rowspan="" style="padding:5px;width:10%;"> {{date('M-d-Y h:i:a',strtotime($data->admit_date))}}</td>
            <td rowspan="" style="padding:5px;width:10%;"> {{date('M-d-Y h:i:a',strtotime($data->discharge_date))}}</td>
            {{-- <td rowspan="" style="padding:5px;width:10%;">{{($data->summary_prepared_by)}}</td> --}}
            <td rowspan="" style="padding:5px;width:10%;"> {{date('M-d-Y h:i:a',strtotime($data->created_at))}}</td>
            <td rowspan="" style="padding:5px;width:10%;">{{date('M-d-Y h:i:a',strtotime($data->last_updated_at))}}</td>
            {{-- <td rowspan="" style="padding:5px;width:10%;">{{($data->discharge_summery)}}</td>
            <td rowspan="" style="padding:5px;width:10%;">{{($data->Edit)}}</td>
            <td rowspan="" style="padding:5px;width:10%;">{{($data->Doctor_aprovel)}}</td> --}}
        </tr>

     @endforeach

    @else
    <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
    @endif
        </table>
    </div>
</div>
