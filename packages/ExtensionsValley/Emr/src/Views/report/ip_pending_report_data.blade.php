<div class="row" id="print_data">
    <div class="col-md-12 " id="result_container_div">
        <div class="" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b>
                    <?= date('M-d-Y h:i A') ?>
                </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From:
                <?= $from ?> To
                <?= $to ?>
            </p>
            
            <?php
        $collect = collect($res);
        $total_records = count($collect);
        ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b>IP Pending Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
               
            <thead>
                
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th width='3%'>Si NO.</th>
                    <th width='7%'>AdmitDate</th>
                    <th width='17%'>Uhid</th>
                    <th width='15%'>Patient Name</th>
                    <th width='5%'>Bed Name</th>
                    <th width='5%'>Room Type</th>
                    <th width='13%'>Ward Name</th>
                    <th width='10%'>Doctor Name</th>
                    <th width='15%'>Company Name</th>
                    <th width='5%'>Net Amount</th>
                    <th width='5%'>Advance</th>
                    <th width='5%'>Insurance Advance</th>
                    <th width='5%'>Advance Adjusted</th>
                    <th width='5%'>Balance Amount</th>
                </tr>
            </thead>
            <tbody>
                <?php
        $total_bill_net_amount=0.0;
        $total_patient_advance = 0.0;
        $total_insurance_advance = 0.0;
        $total_balance_amount = 0.0;
        $uhid = 0;
        $patient_advance = 0;
        $patient_insurance_advance = 0;
        $total_advance_adjusted = 0;
        $i= 1;

        if(count($res)!=0){
          foreach ($res as $data) {
             if($uhid == $data->op_number && $i !=1){
               $patient_advance = 0;
               $patient_insurance_advance = 0;
             }else{
              $patient_advance = $data->patient_advance;
              $patient_insurance_advance = $data->insurance_advance;
             }

             $balance_amount = $data->total_amt - $patient_advance + $patient_insurance_advance;
             ?>
                <tr>
                    <td class="common_td_rules">{{ $i }}</td>
                    <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->admit_date)) }}</td>
                    <td class="common_td_rules">{{ $data->op_number }}</td>
                    <td class="common_td_rules">{{ $data->patient_name }}</td>
                    <td class="common_td_rules">{{ $data->bed_name }}</td>
                    <td class="common_td_rules">{{ $data->room_type_name }}</td>
                    <td class="common_td_rules">{{ $data->ward_name }}</td>
                    <td class="common_td_rules">{{ $data->doctor_name }}</td>
                    <td class="common_td_rules">{{ $data->company_name }}</td>
                    <td class="td_common_numeric_rules">{{ $data->total_amt }}</td>
                    <td class="td_common_numeric_rules">{{ $patient_advance }}</td>
                    <td class="td_common_numeric_rules">{{ $patient_insurance_advance }}</td>
                    <td class="td_common_numeric_rules">{{ $data->adv_adjusted }}</td>
                    <td class="td_common_numeric_rules">{{ $balance_amount }}</td>
                </tr>
                <?php
                $total_bill_net_amount+= floatval($data->total_amt);
                $total_patient_advance +=floatval($patient_advance);
                $total_insurance_advance +=floatval($patient_insurance_advance);
                $total_balance_amount +=floatval($balance_amount);
                $total_advance_adjusted += floatval($data->adv_adjusted);
                $uhid = $data->op_number;
                $i++;
          }
          ?>
                <tr>
                    <th class="common_td_rules" colspan="9" style="text-align:center">Total</th>
                    <th class="td_common_numeric_rules" style="text-align:center"><?= number_format($total_bill_net_amount,2) ?></th>
                    <th class="td_common_numeric_rules" style="text-align:center"><?= number_format($total_patient_advance,2) ?></th>
                    <th class="td_common_numeric_rules" style="text-align:center"><?= number_format($total_insurance_advance,2) ?></th>
                    <th class="td_common_numeric_rules" style="text-align:center"><?= number_format($total_advance_adjusted,2) ?></th>
                    <th class="td_common_numeric_rules" style="text-align:center"><?= number_format($total_balance_amount,2) ?></th>
                </tr>
                <?php
        }else{
          ?>
                <tr>
                    <th colspan="13" style="text-align:center">No Record Found</th>
                </tr>
                <?php
        }
    ?>
            </tbody>
        </table>

        </div>
    </div>
</div>
