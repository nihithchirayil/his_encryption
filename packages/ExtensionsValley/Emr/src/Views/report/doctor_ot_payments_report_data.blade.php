<div class="row">
    <div class="col-md-12" style="padding-left: 50px; padding-right: 50px;" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date:
            <b>{{ date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i A'))) }} </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
        <?php
        $collect = collect($res);
        $total_records = count($collect);
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Doctor OT Payments Report </b></h4>
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr>
                        <td colspan="5">
                            <?= base64_decode($hospital_headder) ?>
                        </td>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="30%;">Doctor Name</th>
                        <th width="10%;">OT</th>
                        <th width="10%;">SUR_SH</th>
                        <th width="10%;">ANAST</th>
                        <th width="10%;">Total Amount</th>
                    </tr>
                </thead>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
        $total_ot = 0.0;
        $total_sur = 0.0;
        $total_anast = 0.0;
        $total_amount = 0.0;
        foreach ($res as $data){
                $total_ot+= floatval($data->ot);
                $total_sur+= floatval($data->sur_sh);
                $total_anast+= floatval($data->anast);
                $total_amount+= floatval($data->total_amnt);
              ?>

                        <tr>
                            <td class="common_td_rules">{{ $data->doctor_name }}</td>
                            <td class="td_common_numeric_rules">{{ $data->ot }}</td>
                            <td class="td_common_numeric_rules">{{ $data->sur_sh }}</td>
                            <td class="td_common_numeric_rules">{{ $data->anast }}</td>
                            <td class="td_common_numeric_rules">{{ $data->total_amnt }}</td>
                        </tr>
                        <?php
        }

        ?>
                        <tr style="height: 30px;">
                            <th class="common_td_rules" style="text-align: left;">Total</th>
                            <th class="td_common_numeric_rules" style="text-align: left;">
                                <?= number_format((float) $total_ot, 2, '.', '') ?></th>
                            <th class="td_common_numeric_rules" style="text-align: left;">
                                <?= number_format((float) $total_sur, 2, '.', '') ?></th>
                            <th class="td_common_numeric_rules" style="text-align: left;">
                                <?= number_format((float) $total_anast, 2, '.', '') ?></th>
                            <th class="td_common_numeric_rules" style="text-align: left;">
                                <?= number_format((float) $total_amount, 2, '.', '') ?></th>
                        </tr>
                    </tbody>
                @else
                    <tr>
                        <td colspan="10" style="text-align: center;">No Results Found!</td>
                    </tr>
                @endif

            </table>
        </font>
    </div>
</div>
