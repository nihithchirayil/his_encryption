<div class="row">
    <div class="col-md-12" id="result_container_div">
        
  
    <p style="font-size: 12px;" id="total_data">Report Print Date: <?=date('Y-m-d h:i A')?> </p>
              <p style="font-size: 12px;" id="total_data">Report Date:<b> <?=$from_date?> To <?=$to_date?> </b></p>
              <?php
                  $collect = collect($res); $total_records=count($collect);
              ?>
              <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
              <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> {{$report_header}}</b></h4>
              <font size="16px" face="verdana" >
  
  
                
  <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="width:100%; font-size: 12px;">
      <thead>
        
          <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
              <th width='5%'>Si No.</th>
              <th width='10%'>UHID</th>
              <th width='15%'>Patient Name</th>
              <th width='10%'>Bill No</th>
              <th width='10%'>Bill Date</th>
              <th width='5%'>Bill Tag</th>
              <th width='8%'>Canceled Date</th>
              <th width='8%'>canceled By</th>
              <th width='8%'>Canceled Naration</th>
      </thead>
  
      <tbody>
      @if(count($res)!=0)
          @php
              $i=1;
  
          @endphp
  
          @foreach ($res as $data)
             
  
              <tr>
                  <td class="td_common_numeric_rules"><?=$i?></td>
                  <td class="common_td_rules"><?=$data->uhid?></td>
                  <td class="common_td_rules"><?=$data->patient_name?></td>
                  <td class="common_td_rules"><?=$data->bill_no?></td>
                  <td class="common_td_rules">{{date('M-d-Y',strtotime($data->bill_date))}}</td>
                  <td class="common_td_rules"><?=$data->bill_tag?></td>
                  <td class="common_td_rules">{{date('M-d-Y',strtotime($data->cancelled_date))}}</td>
                  <td class="common_td_rules"><?=$data->cancel_requested_by?></td>
                  <td class="common_td_rules"><?=$data->cancel_reason?></td>

                  
              </tr>
              @php
                  $i++;
  
              @endphp
          @endforeach
          
          @else
              <tr>
                  <td  colspan="12" style="text-align: center;">No Results Found!</td>
              </tr>
          @endif
      </tbody>
  </table>
  