<div class="row">

    <div class="col-md-12" id="result_container_div">
    <p style="font-size: 12px;" id="total_data">Report Print Date: <?=date('M-d-Y h:i A')?> </p>
      <p style="font-size: 12px;" id="total_data">Report Date: <?=$from?> To <?=$to?></p>
      <?php
      $total_records=0;
      if (count($res)!=0) {
          $collect = collect($res);
          $total_records=count($collect);
      }
      ?>
     <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
     <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Doctor Collection Report Alma</b></h4>
     <font size="16px" face="verdana" >
     <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
    <thead>
      <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
              <th width="2%;">SL.No.</th>
              <th width="20%;">Doctor Name</th>
              <th width="6%">Cons. Fee</th>
              <th width="6%">Hosp. Fee</th>
              <th width="6%">MLC Charge</th>
              <th width="6%">Conv. Fee</th>
              <th width="6%">Tot. Amount</th>
              <th width="6%">Cons. Refund Amt.</th>
              <th width="6%">Cons. Net Amt.</th>
              <th width="6%">Hosp. Refund Amt.</th>
              <th width="6%">Hosp. Net Amt.</th>
              <th width="6%">Conv. Refund Amt.</th>
              <th width="6%">Conv. Net Amt.</th>
              <th width="6%">Tot. Refund Amt.</th>
              <th width="6%">Net Amt.</th>
          </tr>
      </thead>
      <?php
        $tot_consultation_fee=0.0;
        $tot_hospital_fee=0.0;
        $tot_mlc_charge=0.0;
        $tot_convenience_fee=0.0;
        $tot_tot_amount=0.0;
        $tot_consultation_refund_amt=0.0;
        $tot_hosp_refund_amt=0.0;
        $tot_conv_refund_amt=0.0;
        $tot_tot_refund_amount=0.0;
        $net_con_amt=0.0;
        $net_hos_amt=0.0;
        $net_conv_amt=0.0;
        $net_amount_tot=0.0;

      if(count($res)!=0){
      ?>

      <tbody>
          <?php
          $i=1;
          foreach ($res as $data){
            $net_amt = floatval($data->tot_amount)-floatval($data->tot_refund_amount);
            $con_net_amt=  floatval($data->consultation_fee)-floatval($data->consultation_refund_amt);
            $hos_net_amt=  floatval($data->hospital_fee)-floatval($data->hosp_refund_amt);
            $conv_net_amt=  floatval($data->convenience_fee)-floatval($data->conv_refund_amt);
                ?>
              <tr>
                  <td class="common_td_rules">{{$i}}</td>
                  <td class="common_td_rules">{{$data->doctor_name}}</td>
                  <td class="td_common_numeric_rules"><?=number_format($data->consultation_fee, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($data->hospital_fee, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($data->mlc_charge, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($data->convenience_fee, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($data->tot_amount, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($data->consultation_refund_amt, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($con_net_amt, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($data->hosp_refund_amt, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($hos_net_amt, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($data->conv_refund_amt, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($conv_net_amt, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($data->tot_refund_amount, 2, '.', '')?></td>
                  <td class="td_common_numeric_rules"><?=number_format($net_amt, 2, '.', '')?></td>
              </tr>
                  <?php

                  $tot_consultation_fee+= floatval($data->consultation_fee);
                  $tot_hospital_fee+= floatval($data->hospital_fee);
                  $tot_mlc_charge+= floatval($data->mlc_charge);
                  $tot_convenience_fee+= floatval($data->convenience_fee);
                  $tot_tot_amount+= floatval($data->tot_amount);
                  $tot_consultation_refund_amt+= floatval($data->consultation_refund_amt);
                  $tot_hosp_refund_amt+= floatval($data->hosp_refund_amt);
                  $tot_conv_refund_amt+= floatval($data->conv_refund_amt);
                  $net_con_amt+= floatval($con_net_amt);
                  $net_hos_amt+= floatval($hos_net_amt);
                  $net_conv_amt+= floatval($conv_net_amt);
                  $tot_tot_refund_amount+= floatval($data->tot_refund_amount);
                  $net_amount_tot+=$net_amt;
                  $i++;
              }
              ?>
              <tr class="" style="height: 30px;">
                  <td colspan="2" class="common_td_rules" style="text-align: left;"> <strong>Total</strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($tot_consultation_fee, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($tot_hospital_fee, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($tot_mlc_charge, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($tot_convenience_fee, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($tot_tot_amount, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($tot_consultation_refund_amt, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($net_con_amt, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($tot_hosp_refund_amt, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($net_hos_amt, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($tot_conv_refund_amt, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($net_conv_amt, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($tot_tot_refund_amount, 2, '.', '');?></strong></td>
                  <td class="td_common_numeric_rules"><strong><?=number_format($net_amount_tot, 2, '.', '');?></strong></td>
              </tr>
          <?php
           } else{ ?>
      <tr>
          <td colspan="12" style="text-align: center;">No Results Found!</td>
      </tr>
      <?php } ?>
      </tbody>

</table>

</div>
</div>
