<div class="row">
    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('M-d-Y h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"> <b><?= $report_head ?> </b></h4>
        <?php if ($summary == 'true') { ?>
        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
            style="font-size: 12px;">
            <thead>
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th colspan="3" width="3%;">Date</th>
                    <th colspan="7" width="7%;">Amount</th>
                </tr>
            </thead>
            <?php
            $nettotal_bill_net_amount = 0;
            $total_bill_net_amount = 0;
            ?>
            @if (sizeof($res) > 0)
                <tbody>
                    <?php
                        $i = 0;
                        $date = '';
                        foreach ($res as $data) {
                            if ($date != $data->date) {
                                $i = 0;
                                $date = $data->date;
                                $total_bill_net_amount = 0.0;
                        ?>
                    <tr>
                        <td colspan="3" class="common_td_rules">{{ $data->date }}</td>
                        <td colspan="7" class="td_common_numeric_rules">{{ $data->amount }}</td>
                    </tr>
                    <?php
                            } else {
                            ?>
                    <tr>
                        <td colspan="3" class="common_td_rules">{{ $data->date }}</td>
                        <td colspan="7" class="td_common_numeric_rules">{{ $data->amount }}</td>
                    </tr>
                    <?php

                            }
                            $total_bill_net_amount += floatval($data->amount);
                            if ($i == $date_cnt[$date]) {
                            ?>
                    <?php
                                $nettotal_bill_net_amount += $total_bill_net_amount;
                            }
                            $i++;
                        }
                        ?>
                    <tr class="bg-info" style="height: 30px;">
                        <th class="common_td_rules" colspan="3" style="text-align: left;">Net Total</th>
                        <th class="td_common_numeric_rules" style="text-align: left;" colspan="7">
                            <?= $nettotal_bill_net_amount ?></th>
                    </tr>
                @else
                    <tr>
                        <td colspan="10" style="text-align: center;">No Results Found!</td>
                    </tr>
            @endif

            </tbody>
        </table>
        <?php
            } else { ?>
        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
            style="font-size: 12px;">
            <thead>
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th width="25%;">Bill No</th>
                    <th width="25%;">UHID</th>
                    <th width="40%;">Patient Name</th>
                    <th width="10%;">Amount</th>
                </tr>
            </thead>
            <?php
            $nettotal_bill_net_amount = 0;
            $total_bill_net_amount = 0;
            ?>
            @if (sizeof($res) > 0)
                <tbody>
                    <?php
                        $i = 0;
                        $date = '';
                        foreach ($res as $data) {
                            if ($date != $data->date) {
                                $i = 0;
                                $date = $data->date;
                                $total_bill_net_amount = 0.0;
                        ?>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th colspan="10" class="common_td_rules">{{ $date }}</th>
                    </tr>
                    <tr>
                        <td class="common_td_rules">{{ $data->bill_no }}</td>
                        <td class="common_td_rules">{{ $data->uhid }}</td>
                        <td class="common_td_rules">{{ $data->patient_name }}</td>
                        <td class="td_common_numeric_rules">{{ $data->amount }}</td>
                    </tr>
                    <?php
                            } else {
                            ?>
                    <tr>
                        <td class="common_td_rules">{{ $data->bill_no }}</td>
                        <td class="common_td_rules">{{ $data->uhid }}</td>
                        <td class="common_td_rules">{{ $data->patient_name }}</td>
                        <td class="td_common_numeric_rules">{{ $data->amount }}</td>
                    </tr>
                    <?php

                            }
                            $total_bill_net_amount += floatval($data->amount);
                            if ($i == $date_cnt[$date]) {
                            ?>
                    <tr class="bg-info" style="height: 30px;">
                        <th class="common_td_rules" colspan="3" style="text-align: left;">Total</th>
                        <th class="td_common_numeric_rules"><?= $total_bill_net_amount ?></th>
                    </tr>
                    <?php
                                $nettotal_bill_net_amount += $total_bill_net_amount;
                            }
                            $i++;
                        }
                        ?>
                    <tr class="" style=" height: 30px;">
                        <th class="common_td_rules" colspan="3" style="text-align: left;">Net Total</th>
                        <th class="td_common_numeric_rules"><?= $nettotal_bill_net_amount ?></th>
                    </tr>
                @else
                    <tr>
                        <td colspan="10" style="text-align: center;">No Results Found!</td>
                    </tr>
            @endif

            </tbody>
        </table>
        <?php } ?>
    </div>
</div>
