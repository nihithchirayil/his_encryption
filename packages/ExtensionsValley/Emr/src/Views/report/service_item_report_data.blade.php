<div class="row">
    <div class="theadscroll always-visible" id="result_container_div" style="position: relative;height:510px;" >

        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper' style="font-size: 12px;">
            @if(sizeof($res)>0)
     <p style="font-size: smaller;padding-left: 10px">Report Date: <span>{{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}}</span> </p>

     <h2 style="text-align: center;">  Service Item Details</h3>

     <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
         <th width="13%"" style="padding: 2px;">Service Desc</th>
         <th width="13%"" style="padding: 2px;">Room Type</th>
         <th width="13%"" style="padding: 2px;">Price</th>
         <th width="10%"style="padding: 2px;">Department</th>
         <th width="5%"  style="padding: 2px;">Sub Department</th>
  
     </tr>
    

     @foreach ($res as $data)
 
         <tr>
            
         <tr onclick="pricepopup({{$data->id}});">
             <td class="td_common_rules" style="text-align: left;">{{$data->service_desc}}</td>
             <td class="td_common_rules" style="text-align: left;">{{$data->room_type}}</td>
             <td class="td_common_rules" style="text-align: right;">{{$data->price	}}</td> 	
             <td class="td_common_rules" style="text-align: left;">{{$data->department}}</td>
             <td class="td_common_rules" style="text-align: left;">{{$data->sub_department}}</td>
            
 
         </tr>
         @endforeach
       @else
       <script type='text/javascript'>
        toastr.warning('No Result Found..!');
     </script>
     @endif
 
 </table>
    </div>
    <input type="hidden" id="is_print" value="{{$is_print}}">
    <input type="hidden" id="is_excel" value="{{$is_excel}}">
 </div>
 