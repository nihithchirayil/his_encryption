<div class="row">
    <div class="col-md-12" style="padding-left: 100px;
    padding-right: 100px;" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date:
            <b>{{ date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i A'))) }} </b>
        </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
        <?php
        $collect = collect($res);
        $total_records = count($collect);
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Rest of bills collection report </b></h4>
        <font size="16px" face="verdana">
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <?php
                     if($summary == 1){
                         ?>
                <thead>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="55%;">Name</th>
                        <th width="10%;">Amount</th>
                        <th width="10%;">Refund Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(sizeof($res)>0){
                    $total_amount = 0.0;
                    $total_refundamount = 0.0;
                    foreach ($res as $data){
                    $total_amount+= floatval($data->total_amount);
                    $total_refundamount+= floatval($data->total_refundamount);
                    ?>

                    <tr>
                        <td class="common_td_rules">{{ $data->name }}</td>
                        <td class="td_common_numeric_rules">{{ $data->total_amount }}</td>
                        <td class="td_common_numeric_rules">{{ $data->total_refundamount }}</td>
                    </tr>
                    <?php
        }

        ?>
                    <tr style="height: 30px;">
                        <th class="common_td_rules">Total</th>
                        <th class="td_common_numeric_rules"><?= number_format((float) $total_amount, 2, '.', '') ?></th>
                        <th class="td_common_numeric_rules"><?= number_format((float) $total_refundamount, 2, '.', '') ?>
                        </th>
                    </tr>
                    <?php } else{
        ?>
                    <tr>
                        <td colspan="4" style="text-align: center;">No Results Found!</td>
                    </tr>
                    <?php
    }
    ?></tbody>
    <?php

} else {
    ?>
    <thead>
        <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th width="55%;">Name</th>
            <th width="15%;">Bill Date</th>
            <th width="10%;">Amount</th>
            <th width="10%;">Refund Amount</th>
            <th width="10%;">Net Amount</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $total_amount = 0.0;
        $total_refundamount = 0.0;
        $total_netamt = 0.0;
        if(sizeof($res)>0){
        foreach ($res as $data){
                $total_amount+= floatval($data->total_amount);
                $total_refundamount+= floatval($data->total_refundamount);
                $net_amount=floatval($data->total_amount)-floatval($data->total_refundamount);
                $total_netamt+=$net_amount;
              ?>

                    <tr>

                        <td class="common_td_rules">{{ $data->name }}</td>
                        <td class="common_td_rules">{{ date('M-d-Y', strtotime($data->date)) }}</td>
                        <td class="td_common_numeric_rules">{{ $data->total_amount }}</td>
                        <td class="td_common_numeric_rules">{{ $data->total_refundamount }}</td>
                        <td class="td_common_numeric_rules">{{ $net_amount }}</td>

                    </tr>
                    <?php
        }

        ?>
                    <tr style="height: 30px;">
                        <th colspan="2" class="common_td_rules">Total</th>
                        <th class="td_common_numeric_rules"><?= number_format((float) $total_amount, 2, '.', '') ?>
                        </th>
                        <th class="td_common_numeric_rules">
                            <?= number_format((float) $total_refundamount, 2, '.', '') ?></th>
                        <th class="td_common_numeric_rules"><?= number_format((float) $total_netamt, 2, '.', '') ?>
                        </th>
                    </tr>
                <?php } else{
                        ?>
                <tr>
                    <td colspan="4" style="text-align: center;">No Results Found!</td>
                </tr>
                <?php
                    }
                }
?>
                </tbody>

            </table>
        </font>
    </div>
</div>
