<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div id="print_data" style="margin-top: 10px">
            <?php
                $collect = collect($res); $total_records=count($collect);
                $collection_res = $collect->groupBy('dept_name');
            ?>
            <table id="result_data_table" class="table table-condensed table_sm table-col-bordered" style="font-size: 12px;">
                <thead>
                    <tr class="thead_headerclass" style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th style="text-align: left" colspan="7">Report Print Date: <?=date('M-d-Y h:i A')?></th>
                    </tr>
                    <tr class="thead_headerclass" style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th style="text-align: left" colspan="7">Report Date: <?=$from?> To <?=$to_date?></th>
                    </tr>
                    <tr class="thead_headerclass" style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th style="text-align: left" colspan="7">Total Count: {{ $total_records }}</th>
                    </tr>
                    <tr class="thead_headerclass" style="background-color:#b9b2b2ab;color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th colspan="7">Department Wise Collection Summary</th>
                    </tr>
                </thead>
                <tbody style="border: 1px solid #CCC;">
                @if(sizeof($collection_res) > 0)
                    <?php
                    $cons_total = $lab_total = $other_total = $pharmacy_total = $ip_total = 0;
                    foreach ($collection_res as $key => $value) {
                        $cons_charge = $lab_charge = $other_charge = $pharmacy = $ip_charge = 0;
                    ?>
                        <tr class="headerclass" style="background-color:rgb(170 203 170);color:black;border-spacing: 0 1em;font-family:sans-serif;">
                            <th colspan="2" width="35%" style="text-align:left;">{{$key}}</th>
                            <th colspan="4" width="52%" style="background-color: rgb(145 197 145);">OP</th>
                            <th width="13%">IP</th>
                        </tr>
                        <tr>
                            <th width="5%" class="common_td_rules" style="text-align:center !important;">Sl No</th>
                            <th class="common_td_rules" style="text-align:center !important;">Doctor Name</th>
                            <th width="13%" class="common_td_rules" style="text-align:center !important;">Consultation</th>
                            <th width="13%" class="common_td_rules" style="text-align:center !important;">Lab</th>
                            <th width="13%" class="common_td_rules" style="text-align:center !important;">Other Services</th>
                            <th width="13%" class="common_td_rules" style="text-align:center !important;">Pharmacy</th>
                            <th class="common_td_rules" style="text-align:center !important;">Ip Charges</th>
                        </tr>     
                    <?php
                        foreach ($value as $k => $val) {
                            $cons_charge += floatval($val->consultation_gross_amount_op);
                            $lab_charge += floatval($val->lab_gross_amount_op);
                            $other_charge += floatval($val->other_service_gross_amount_op);
                            $pharmacy += floatval($val->pharmacy_gross_amount_op);
                            $ip_charge += floatval($val->ip_charges);
                    ?>
                        <tr style="height: 30px;">
                            <td class="common_td_rules">{{$k+1}}</td>
                            <td class="common_td_rules" style="text-align:left">{{$val->doctor_name}}</td>
                            <td class="td_common_numeric_rules">{{$val->consultation_gross_amount_op}}</td>
                            <td class="td_common_numeric_rules">{{$val->lab_gross_amount_op}}</td>
                            <td class="td_common_numeric_rules">{{$val->other_service_gross_amount_op}}</td>
                            <td class="td_common_numeric_rules">{{$val->pharmacy_gross_amount_op}}</td>
                            <td class="td_common_numeric_rules">{{$val->ip_charges}}</td>
                        </tr>
                    <?php
                        }
                        $cons_total += $cons_charge; 
                        $lab_total += $lab_charge; 
                        $other_total += $other_charge; 
                        $pharmacy_total += $pharmacy; 
                        $ip_total += $ip_charge;
                    ?>
                        <tr style="height: 30px;">
                            <th colspan="2" class="common_td_rules" style="text-align:left;">Department Total</th>
                            <th class="td_common_numeric_rules"><?=number_format((float)$cons_charge, 2, '.', '')?></th>
                            <th class="td_common_numeric_rules"><?=number_format((float)$lab_charge, 2, '.', '')?></th>
                            <th class="td_common_numeric_rules"><?=number_format((float)$other_charge, 2, '.', '')?></th>
                            <th class="td_common_numeric_rules"><?=number_format((float)$pharmacy, 2, '.', '')?></th>
                            <th class="td_common_numeric_rules"><?=number_format((float)$ip_charge, 2, '.', '')?></th>
                        </tr>
                    <?php
                    }
                    ?>
                    <tr style="height: 30px; background-color:#d9edf7">
                        <th colspan="2" class="common_td_rules" style="text-align:left;">Total</th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$cons_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$lab_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$other_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$pharmacy_total, 2, '.', '')?></th>
                        <th class="td_common_numeric_rules"><?=number_format((float)$ip_total, 2, '.', '')?></th>
                    </tr>
                @else
                    <tr>
                        <td colspan="5" style="text-align: center;">No Results Found!</td>
                    </tr>
                @endif
                </tbody>
            </table> 
        </div>
    </div>
</div>
