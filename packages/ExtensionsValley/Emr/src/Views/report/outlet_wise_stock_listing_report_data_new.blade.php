<div class="row">
   
    <div class="col-md-12 padding_sm" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <h4 style="text-align: center" id="heading"> <b>Outlet Wise Stock List New</b></h4>
        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
            <tr class="headerclass"
                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th style="width: 3%">SI.No.</th>
                <th style="width:15%;">Item Description</th>
                <th style="width: 7%;">HSN Code</th>
                @if ($batch_wise == 1)
                    <th style="width: 10%;">Batch No</th>
                    <th style="width: 10%;">Expiry Date</th>
                @endif
                @if($stock_zero !=1 || $batch_wise ==1 )
                  @php
                      $span=15;
                  @endphp
                <th style="width: 7%;">Stock</th>
                @else
                @php
                $span=14;
                @endphp
                @endif
                <th style="width: 7%;">Category</th>
                <th style="width: 7%;">Sub Category</th>
                <th style="width: 8%">Vendor Name</th>
                <th style="width: 8%;">Doctor</th>
                <th style="width: 5%;">Rack</th>
                <th style="width: 8%;">Manufacturer</th>
                @if ($without_expiry == 1 && $stock_zero != 1  || $batch_wise ==1 )
                    <th style="width: 10%;">Unit Cost</th>
                @endif
                @if($stock_zero !=1  || $batch_wise ==1)
                <th style="width: 7%;">MRP</th>
                <th style="width: 7%;">Price</th>
                @endif
                

            </tr>

            @php
                $sort_array = [];
                $i = 0;
                $sort_change = 0;
                $user_name = '';
            @endphp
            @if (count($res) != 0)
                @foreach ($res as $data)
                    @if (!in_array($data->group, $sort_array))
                        @php
                            $sort_array[] = $data->group;
                            $sort_order = true;
                        @endphp
                    @endif

                    @if ($user_name != $data->location_name)
                        @php $user_name = $data->location_name; @endphp
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th class="common_td_rules" colspan="{{ $span }}">{{ $data->location_name }}</th>
                        </tr>
                    @endif

                    <tr>
                        <td class="td_common_numeric_rules">{{ ++$i }}</td>
                        <td class="common_td_rules">{{ $data->item_desc }}</td>
                        <td class="common_td_rules">{{ $data->hsn_code }}</td>
                        @if ($batch_wise == 1)
                            <td class="common_td_rules">{{ $data->batch_no }}</td>
                            <td class="common_td_rules">{{ $data->expiry_date }}</td>
                        @endif
                        @if($stock_zero !=1 || $batch_wise ==1 )
                        <td class="td_common_numeric_rules">{{ $data->stock }}</td>
                        @endif
                        
                        <td class="common_td_rules">{{ $data->category }}</td>
                        <td class="common_td_rules">{{ $data->subcategory_name }}</td>
                        <td class="common_td_rules">{{ $data->vendor_name }}</td>
                        <td class="common_td_rules">{{ $data->doctor_name ? $data->doctor_name : '-' }}</td>
                        <td class="td_common_numeric_rules">{{ $data->rack ? $data->rack : '-' }}</td>
                        <td class="common_td_rules">{{ $data->manufacturer ? $data->manufacturer : '-' }}</td>
                        @if ($without_expiry == 1 && $stock_zero !=1  ||  $batch_wise ==1)
                            <td class="common_td_rules">{{ number_format($data->unit_purchase_cost, 2, '.', '') }}
                            </td>
                        @endif
                        @if($stock_zero !=1  || $batch_wise ==1)
                        <td class="common_td_rules">{{ number_format($data->unit_mrp, 2, '.', '') }}</td>
                        <td class="common_td_rules">{{ number_format($data->unit_selling_price, 2, '.', '') }}</td>
                        @endif
                       
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="14" style="text-align: center;">No Data Found</td>
                </tr>
            @endif
        </table>
    </div>
</div>
