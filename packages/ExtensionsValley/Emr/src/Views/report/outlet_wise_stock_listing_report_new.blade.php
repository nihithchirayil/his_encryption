@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
    <input type="hidden" value='<?= $route_data ?>' id="route_value">
    <div class="right_col">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 padding_sm">
                    <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
                        <div class="box no-border no-margin">
                            <div class="box-body" style="padding-bottom:15px;">
                                <table class="table table-contensed table_sm" style="margin-bottom:10px;">
                                    <thead>
                                        <tr class="table_header_bg">
                                            <th>Outlet Wise Stock List New
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="row">
                                    <div class="col-md-12 padding_sm" style="height:180px;">

                                        <?php
                            $i = 1;
                            $k = 0;

                            foreach ($showSearchFields as $fieldType => $fieldName) {
                                $fieldTypeArray[$i] = explode('-', $fieldType);

                                if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                    $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                    ?>

                                        <div class="col-md-2 padding_sm" style="margin-top: 10px">
                                            <div class="mate-input-box">
                                                <label>{{ $fieldTypeArray[$i][2] }}</label>
                                                <div class="clearfix"></div>
                                                <input class="form-control hidden_search" value="" autocomplete="off"
                                                    type="text" placeholder="Enter Name"
                                                    id="{{ $fieldTypeArray[$i][1] }}"
                                                    name="{{ $fieldTypeArray[$i][1] }}" />
                                                <div id="{{ $fieldTypeArray[$i][1] }}AjaxDiv" style="margin-top: -13px;"
                                                    class="ajaxSearchBox">
                                                </div>
                                                <input class="filters" type="hidden"
                                                    name="{{ $fieldTypeArray[$i][1] }}_hidden" value=""
                                                    id="{{ $fieldTypeArray[$i][1] }}_hidden">
                                            </div>
                                        </div>

                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'text') {
                                    ?>
                                        <div class="col-md-2 padding_sm">
                                            <div class="mate-input-box">
                                                <label>{{ $fieldTypeArray[$i][2] }}</label>
                                                <div class="clearfix"></div>
                                                <input type="text" name="{{ $fieldTypeArray[$i][1] }}"
                                                    <?php if (isset($fieldTypeArray[$i][3])) {
                                                        echo $fieldTypeArray[$i][3];
                                                    } ?> value="" class="form-control filters"
                                                    id="{{ $fieldTypeArray[$i][1] }}"
                                                    data="{{ isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : '' }}"
                                                    <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?>>
                                            </div>
                                        </div>
                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'date') {

                                    ?>

                                        <div class="col-md-2 padding_sm date_filter_div">
                                            <div class="mate-input-box">
                                                <label style="display: none;">{{ $fieldTypeArray[$i][2] }}
                                                    From</label>
                                                <div class="clearfix"></div>
                                                <input style="display: none;" type="text" data-attr="date"
                                                    autocomplete="off" name="{{ $fieldTypeArray[$i][1] }}_from"
                                                    <?php
                                                    if (isset($fieldTypeArray[$i][3])) {
                                                        echo $fieldTypeArray[$i][3];
                                                    }
                                                    ?> value="{{ date('M-d-Y') }}"
                                                    class="form-control filters" placeholder="YYYY-MM-DD"
                                                    id="{{ $fieldTypeArray[$i][1] }}_from">

                                            </div>
                                        </div>

                                        <div class="col-md-3 padding_sm date_filter_div">
                                            <div class="mate-input-box">
                                                <label style="display: none;">{{ $fieldTypeArray[$i][2] }} To</label>

                                                <div class="clearfix"></div>
                                                <input style="display: none;" type="text" data-attr="date"
                                                    autocomplete="off" name="{{ $fieldTypeArray[$i][1] }}_to"
                                                    <?php
                                                    if (isset($fieldTypeArray[$i][3])) {
                                                        echo $fieldTypeArray[$i][3];
                                                    }
                                                    ?> value="{{ date('M-d-Y') }}"
                                                    class="form-control filters" placeholder="YYYY-MM-DD"
                                                    id="{{ $fieldTypeArray[$i][1] }}_to">

                                            </div>
                                        </div>
                                        <?php

                                } else if ($fieldTypeArray[$i][0] == 'combo') {
                                    ?>
                                        <div class="col-md-2 padding_sm date_filter_div" style="margin-top: 10px;">
                                            <div class="mate-input-box">
                                                <label>{{ $fieldTypeArray[$i][2] }}</label>
                                                <select class="form-control select2 filters"
                                                    id="{{ $fieldTypeArray[$i][1] }}">
                                                    <option value="All">Select {{ $fieldTypeArray[$i][2] }}</option>
                                                    <?php
                                                foreach ($fieldName as $key => $value) {
                                                    ?>
                                                    <option value="{{ $key }}">{{ $value }}</option>
                                                    <?php
                                                }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'multi_combo') {
                                    ?>
                                        <div class="clearfix"></div>
                                        <div class="col-md-6 padding_sm" style="margin-top: 13px;z-index:900!important">
                                            <div class="mate-input-box" style="height: 89px !important;">
                                                <label>{{ $fieldTypeArray[$i][2] }}</label>
                                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, [
                                                    'class' => 'form-control select2 filters',
                                                    'multiple' => 'multiple-select',
                                                    'placeholder' => $fieldTypeArray[$i][2],
                                                    'id' => $fieldTypeArray[$i][1],
                                                    'style' => 'width:0%;color:#555555; padding:2px 12px;',
                                                ]) !!}
                                            </div>
                                        </div>

                                        <?php
                                }

                                else if ($fieldTypeArray[$i][0] == 'checkbox') {
                                    ?>
                                        @php
                                            $div__cls = '';
                                        @endphp
                                        @if ($fieldTypeArray[$i][1] == 'pur_cost')
                                            @php $div__cls = 'pur_cost_div'; @endphp
                                        @endif
                                        @if ($fieldTypeArray[$i][1] == 'reorder_level')
                                            @php $div__cls = 'reorder_level_div'; @endphp
                                        @endif
                                        @if ($k == 0 || $k == 6)
                                            {{-- <div class="clearfix"></div> --}}
                                        @endif
                                        @php
                                            $k++;
                                        @endphp

                                        <div id="{{ $div__cls }}"
                                            class="col-md-2 padding_sm date_filter_div {{ $div__cls }}"
                                            style="margin-top: 32px;padding-left:5px !important;">
                                            <div class="checkbox checkbox-success inline no-margin">
                                                <input type="checkbox" name="{{ $fieldTypeArray[$i][1] }}"
                                                    <?php if (isset($fieldTypeArray[$i][3])) {
                                                        echo $fieldTypeArray[$i][3];
                                                    } ?> value=""
                                                    class="check {{ $fieldTypeArray[$i][1] }}"
                                                    id="{{ $fieldTypeArray[$i][1] }}"
                                                    data="{{ isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : '' }}"
                                                    <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'checkbox')) { ?> readonly <?php } ?>>
                                                <label class="text-blue "
                                                    for="{{ $fieldTypeArray[$i][1] }}">{{ $fieldTypeArray[$i][2] }}</label>
                                            </div>
                                        </div>

                                        <?php
                                }

                                $i++;

                            }
                            ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 padding_sm" style="text-align:right; padding: 0px; min-width: 0px;margin-top: -30px;margin-left:-15px">
                    <button onclick="datarst2();" style="padding-inline: 3px;" style="padding-inline: 0px;"
                        type="reset" class="btn light_purple_bg" name="clear_results" id="clear_results">
                        <i class="fa fa-refresh" aria-hidden="true"></i>
                        Reset
                    </button>
                    <button class="btn light_purple_bg disabled" onclick="exceller();" name="csv_results"
                        id="csv_results">
                        <i class="fa fa-file-excel-o" aria-hidden="true" id="add_spin"></i>
                        Excel
                    </button>
                    <button class="btn light_purple_bg disabled" onclick="printReportData();" name="print_results"
                        id="print_results">
                        <i class="fa fa-print" aria-hidden="true"></i>
                        Print
                    </button>
                    <button class="btn light_purple_bg" style="padding-inline: 3px;" onclick="getStockListingData(0);"
                        name="search_results" id="search_results">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        Search
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-12 padding_sm">
            <div id="ResultDataContainer"
                style="max-height: 650px; padding: 10px; display:none;font-family:poppinsregular">
                <div style="background:#686666;">
                    <page size="A4"
                        style="background: white; display: block; margin: 0 auto; margin-bottom: 1cm; box-shadow: 0 0 1cm rgb(113 113 113 / 50%);
                                width: 100%; padding: 50px;"
                        id="ResultsViewArea">
                    </page>
                </div>
            </div>
        </div>
    </div>
    </div>

    <!-------print modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Html::script('packages/extensionsvalley/default/js/bootbox.js') !!}
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/stock_listing_new.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

@endsection
