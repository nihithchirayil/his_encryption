
<div class="col-md-12 " id="result_container_div_excel" style="width: 100%;">
    <div style="position:relative;">

    <table id="result_data_table_excel" class="table theadfix_wrapper" style="font-size: 12px;">
    @if(sizeof($res)>0)
    
    <p style="font-size: smaller;padding-left: 10px" id="d1">Report Date: 
        <span>
            {{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}}
        </span> 
    </p>
     
    <p style="font-size: smaller;padding-left: 10px" id="d2">Location : 
        <span>
        @foreach ($new_location as $item)  
            {{$item->location_name}} , 
        @endforeach
        </span>
    </p>

    <h3 align="center" style="padding_right:3px;"> Outlet Wise Stock Listing</h3>
    
     
    <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
        <th style="padding: 2px;width: 3%">No</th>
        <th style="padding: 2px;width:22%;">Item Description</th>
        <th style="padding: 2px;width: 10%;">HSN Code</th>

        @if($batch_wise == 1)
        <th style="padding: 2px;width: 13%;">Batch No</th>
        @endif
        <th style="padding: 2px;width: 10%;">Stock</th>

        {{-- <th style="padding: 2px;width: 11%;">Generic Name</th> --}}
        {{--<th style="padding: 2px;width: 11%">Vendor Name</th>--}}
        {{-- <th style="padding: 2px;width:8%;">Group</th> --}}
        <th style="padding: 2px;width: 5%;">Doctor</th>
      
        {{-- <th style="padding: 2px;width: 8%;">Location</th> --}}
        <th style="padding: 2px;width: 5%;">Rack</th>
        <th style="padding: 2px;width: 10%;">Manufacturer</th>
        <th style="padding: 2px;width: 10%;">Exp Date</th>
         @if($batch_wise == 1)
        <th style="padding: 2px;width: 8%;">MRP</th>
        @endif
    </tr>

    @php 
        $sort_array = array();
        $i = 1;
        $sort_change   = 0;
        $user_name='';
    @endphp
    @foreach ($res as $data)
            {{--     
                      @if(!in_array($data->location_name,$sort_array)) 
                      @php 
                      $sort_array[] = $data->location_name;
                      $sort_order = true;
                      @endphp
                      @endif 
            --}}

        @if(!in_array($data->group,$sort_array)) 
              @php 
                  $sort_array[] = $data->group;
                  $sort_order = true;
              @endphp
        @endif
        
        @if($user_name != $data->location_name)
            @php $user_name = $data->location_name; @endphp
            <th colspan="2">{{$data->location_name}}</th>
        @endif
     
        @if( isset($sort_order) && ($sort_order==1) )

        <tr>
          <th colspan="2">{{$data->group}}</th>
        </tr>
     
              @php 
                $sort_order = false; 
              @endphp
        @endif           
                     
        <tr>
            <td>{{$i++}}</td>
            <td>{{$data->item_desc}}</td>
            <td>{{$data->hsn_code}}</td>

            @if($batch_wise == 1)
            <td>{{$data->batch_no}}</td>
            @endif
            <td>{{$data->stock}}</td>

            {{-- <td>{{$data->generic_name}}</td> --}}
            {{-- <td>{{$data->vendor_name}}</td>--}}
            {{-- <td>{{$data->group}}</td> --}}
            <td>{{$data->doctor_name}}</td>
           
            {{-- <td>{{$data->location_name}}</td> --}}
            <td>{{$data->rack}}</td>
            <td>{{$data->manufacturer}}</td>
            <td>{{$data->expiry_date}}</td>
            @if($batch_wise == 1)
            <td>{{$data->unit_mrp}}</td>
            @endif
        </tr>
        
    @endforeach

    @else
          No Result Found !!
    @endif
    
    </table>
</div>
</div>

   