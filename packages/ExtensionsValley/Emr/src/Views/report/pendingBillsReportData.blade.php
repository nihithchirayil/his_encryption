<div class="row">
  <div class="col-md-12" id="result_container_div">

  <p style="font-size: 12px;" id="total_data">Report Print Date: <?=date('Y-m-d h:i A')?> </p>
            <p style="font-size: 12px;" id="total_data">Report Date:<b> <?=$from_date?> To <?=$to_date?> </b></p>
            <?php
                $collect = collect($res); $total_records=count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> {{$report_header}}</b></h4>
            <font size="16px" face="verdana" >



<table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="width:100%; font-size: 12px;">
    <thead>
        <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th width='5%'>Si No.</th>
            <th width='10%'>UHID</th>
            <th width='20%'>Patient Name</th>
            <th width='10%'>Bill No</th>
            <th width='10%'>Bill Date</th>
            <th width='10%'>Bill Tag</th>
            <th width='8%'>Bill Amount</th>
            <th width='8%'>Ip Credit</th>
            <th width='8%'>Esi Insurance</th>
            <th width='8%'>Bill Discount</th>
            <th width='8%'>Balance Amount</th>
            <th width='8%'>Paid Amount</th>
            <th width='10%'>Paid Status</th>
        </tr>
    </thead>

    <tbody>
    @if(count($res)!=0)
        @php
            $i=1;
            $total_bill_amt = 0.0;
            $toatal_ip_credit = 0.0;
            $toatal_esi_insurance = 0.0;
            $toatal_bill_discount = 0.0;
            $toatal_balance_amount = 0.0;
            $toatal_paid_amount = 0.0;


        @endphp

        @foreach ($res as $data)
            @php
                $total_bill_amt+= floatval($data->bill_amount);
                $toatal_ip_credit+= floatval($data->ip_credit);
                $toatal_esi_insurance+= floatval($data->esi_insurance);
                $toatal_bill_discount+= floatval($data->bill_discount);
                $toatal_balance_amount+= floatval($data->balance_amount);
                $toatal_paid_amount+= floatval($data->paid_amount);
            @endphp

            <tr>
                <td class="td_common_numeric_rules"><?=$i?></td>
                <td class="common_td_rules"><?=$data->uhid?></td>
                <td class="common_td_rules"><?=$data->patient_name?></td>
                <td class="common_td_rules"><?=$data->bill_no?></td>
                <td class="common_td_rules">{{date('M-d-Y',strtotime($data->bill_date))}}</td>
                <td class="common_td_rules"><?=$data->bill_tag?></td>
                <td class="td_common_numeric_rules"><?=number_format($data->bill_amount)?></td>
                <td class="td_common_numeric_rules"><?=number_format($data->ip_credit)?></td>
                <td class="td_common_numeric_rules"><?=number_format($data->esi_insurance)?></td>
                <td class="td_common_numeric_rules"><?=number_format($data->bill_discount)?></td>
                <td class="td_common_numeric_rules"><?=number_format($data->balance_amount)?></td>
                <td class="td_common_numeric_rules"><?=number_format($data->paid_amount)?></td>
                <td class="common_td_rules"><?=$data->bill_status?></td>
            </tr>
            @php
                $i++;

            @endphp
        @endforeach
        <tr class="bg-info" style="height: 30px;">
            <th colspan="6" style="text-align: left;"  class="common_td_rules">Total</th>
            <th class="td_common_numeric_rules"><?=number_format((float)$total_bill_amt, 2, '.', '')?></th>
            <th class="td_common_numeric_rules"><?=number_format((float)$toatal_ip_credit, 2, '.', '')?></th>
            <th class="td_common_numeric_rules"><?=number_format((float)$toatal_esi_insurance, 2, '.', '')?></th>
            <th class="td_common_numeric_rules"><?=number_format((float)$toatal_bill_discount, 2, '.', '')?></th>
            <th class="td_common_numeric_rules"><?=number_format((float)$toatal_balance_amount, 2, '.', '')?></th>
            <th class="td_common_numeric_rules"><?=number_format((float)$toatal_paid_amount, 2, '.', '')?></th>
            <th></th>
        </tr>
        @else
            <tr>
                <td  colspan="12" style="text-align: center;">No Results Found!</td>
            </tr>
        @endif
    </tbody>
</table>
