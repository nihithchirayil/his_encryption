
<div class="row">
    
   
  
  <div class="theadscroll always-visible" id="result_container_div" style="position: relative;height:510px;" >
   
     @if(sizeof($res)>0)
     <p style="font-size: 12px;" id="total_data">Report Date: {{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}} </p><br>
     @endif
     @php
     $collect = collect($res); $total_records=count($collect);   
    @endphp 
    <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
    <h2 style="text-align: center;margin-top: -29px;" id="heading"> Vendor Wise Purchase Report  </h2>
     
            @if(sizeof($res)>0)
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper' style="font-size: 12px;">
            
                <thead class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:robotoregular">
                    <th  width='5%'  style="padding: 2px;text-align: center; ">Sn. No.</th>
                    <th  width='80%' style="padding: 2px;text-align: center;border:2px; ">Vendor Name	</th>
                     <th width='15%'  style="padding:2px;text-align: center">Purchase Amount</th>
                </thead>
                  <tbody style="font-size: 12px;">
                    @php
                        $total=0
                    @endphp
                   
                      @foreach ($res as $data)
                        <tr >
                            <td  class="td_common_nummeric_rules">{{ $loop->iteration}}.</td>
                            <td  class="td_common_rules" >{{$data->vendor_name}}</td>
                            <td  class="td_common_nummeric_rules"> {{number_format($data->purchase_amount,$report_decimal_separator)}}</td>
                        </tr>
                        @php
                            $total+=$data->purchase_amount;

                        @endphp
                    @endforeach
                    <tr><td colspan="2"><b>Total</b></td><td><b>{{number_format($total,$report_decimal_separator)}}</b></td></tr>
                    </tbody>
                    @else 
                      <h1 style="text-align: center; color: antiquewhite;">No Record Found</h1>
                    @endif   
             </table>
     
    </div>
 </div>
 