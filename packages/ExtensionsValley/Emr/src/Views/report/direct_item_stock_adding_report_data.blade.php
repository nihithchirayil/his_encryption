

<div class="row">
    <div class="col-md-12" id="result_container_div">
        @if(sizeof($res)>0)
              <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?=date('M-d-Y h:i A')?> </b></p>
              <p style="font-size: 12px;" id="total_data">Report Date: <?=$from?> To <?=$to?></p>

              <?php
                //   $collect = collect($res); $total_records=count($collect);
               ?>
              {{-- <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p> --}}
              <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Direct Item Stock Adding Report </b></h4>
              <font size="16px" face="verdana" >
              <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
  <thead>
    <tr>
        <td colspan="10">
            <?= base64_decode($hospital_headder) ?>
        </td>

   </tr>
      <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
          <th width="10%">Item name</th>
          <th width="3%" >Quantity</th>
          <th width="7%" >Location name</th>
          <th width="7%">Selling Price</th>
          @if($checkbox1==1)
          
          <th width="9%">Total Selling Cost</th>
          <th width="5%">Purchase Cost</th>
          <th width="5%">Total Purchase Cost</th>
          @else
          <th width="5%">Batch no</th>
          <th width="7%">Expiry date</th>
          
          @endif
          <th width="5%" >Date</th>
      </tr>

  </thead>
      <tbody>
     @php
        $total_sel=0;
        $total_pur=0;
        $total_sel_cost=0;
     @endphp

         @foreach ($res as $data)
         @if($checkbox1==1)
         <tr style="cursor: pointer;" onclick="stckaddngpopup('<?=$data->id?>')">
         @else
            <tr>
                @endif
             <td class="common_td_rules">{{$data->item_desc}}</td>
             <td class="td_common_numeric_rules">{{$data->total_quantity}}</td>
             <td class="common_td_rules">{{$data->location_name}}</td>
             <td class="td_common_numeric_rules">{{$data->selling_price}}</td>

             @php
             $total_sel+= $data->selling_price;
             $total_sel_cost+= $data->selling_price * $data->total_quantity;
             @endphp
             @if($checkbox1==1)
             <td class="td_common_numeric_rules">{{$data->selling_price*$data->total_quantity}}</td>
             <td class="td_common_numeric_rules">{{$data->unit_purchase_rate}}</td>
             <td class="td_common_numeric_rules">{{$data->unit_purchase_rate*$data->total_quantity}}</td>
             {{-- {{$data->selling_price}}<br>
             {{$data->unit_purchase_rate}} --}}
             @php
             $total_pur+= $data->unit_purchase_rate * $data->total_quantity;
             @endphp
             @else
             <td class="common_td_rules">{{$data->batch}}</td>
             <td class="common_td_rules">{{ date('M-d-Y',strtotime($data->exp_date))}}</td>
             @endif
             <td class="common_td_rules">{{ $data->created_date }}</td>


         </tr>

         @endforeach
         @if($checkbox1==1)
         <tr class="bg-info">
            <th colspan="3" class="common_td_rules">Grand Total</th>
            <th class="td_common_numeric_rules">{{$total_sel}}</th>
            <th class="td_common_numeric_rules">{{$total_sel_cost}}</th>
            <th colspan="2" class="td_common_numeric_rules">{{$total_pur}}</th>
            <th colspan="2" class="td_common_numeric_rules"></th>
        </tr>
        @else 
        <tr class="bg-info">
            <th colspan="3" class="common_td_rules">Grand Total</th>
            <th class="td_common_numeric_rules">{{$total_sel}}</th>
            <th colspan="5" class="td_common_numeric_rules"></th>
        </tr>
        @endif
        @else
         <tr>
            <td colspan="9" style="text-align: center;">No Results Found!</td>
        </tr>
         @endif
      </tbody>
  </table>
    </div>
  </div>
