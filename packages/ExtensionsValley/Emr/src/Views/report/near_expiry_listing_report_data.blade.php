
<div class="row">
    {{-- @php
    print_r("hdfjhefrgfhrfgjrgf");
    exit;
    @endphp --}}
  
    <div class="theadscroll always-visible" id="result_container_div" style="position: relative;height:510px;" >
      @if(sizeof($res)>0)
     <p style="font-size: 12px;" id="total_data">Report Date: {{date(\WebConf::getConfig('datetime_format_web'), strtotime(date('Y-m-d h:i:s')))}} </p><br>
     @php
     $collect = collect($res); $total_records=count($collect);   
    @endphp 
     <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
     <p style="font-size: smaller;padding-left: 10px">Location : <span>{{$new_location[0]->location_name}}</span> </p>
     <h4 style="text-align: center;margin-top: -29px;" id="heading"> Near Expiry Listing  </h4>
     <font size="16px" face="verdana" >
          <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
      <thead>
          <tr>
               <td colspan="10">
                   <?= base64_decode($hospital_headder) ?>
               </td>
       
          </tr>
           <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">

           <th width="2%">No.</th>
           <th width="27%">Item Description</th>
      {{-- <th  width=" 7%>Location</th> --}}
           <th  width="20%">Vendor Name</th>
           {{-- <th  width="15%">Manufacturer</th> --}}
           <th  width="10%">Batch</th>
           <th  width="7%">Quantity</th>
           <th  width="10%">Expiry Date</th>
           <th  width=" 10%">Purchase Cost</th>
           <th  width=" 10%">Selling Price</th>
           <th  width="3%">MRP</th>
           </tr>
      </thead>
    @php $i= 1; 
    $total_purchase_cost=0;
    $total_selling_price=0;
    $total_mrp=0;
    @endphp
     @foreach ($res as $data)
 
         <tr>
            <td class="td_common_nummeric_rules">{{$i++}}</td>
            <td class="common_td_rules">{{$data->item_desc}}</td>
            <td class="common_td_rules">{{$data->vendor_name}}</td>
            {{-- <th class="common_td_rules">{{$data->manufacturer_name}}</td> --}}
            <td class="td_common_numeric_rules">{{$data->batch_no}}</td>
            <td class="td_common_numeric_rules">{{$data->stock}}</td>
            <td class="common_td_rules">{{date('M-d-Y',strtotime($data->expiry_date))}}</td>
            <td class="td_common_numeric_rules">{{number_format($data->unit_purchase_cost,$report_decimal_separator)}}</td>
            <td class="td_common_numeric_rules">{{number_format($data->unit_selling_price,$report_decimal_separator)}}</td>
            <td class="td_common_numeric_rules">{{number_format($data->unit_mrp,$report_decimal_separator)}}</td>
           
         </tr>
        
         
       @php
         $total_purchase_cost+= $data->unit_purchase_cost;
         $total_selling_price+= $data->unit_selling_price;
         $total_mrp+= $data->unit_mrp;
         
       @endphp
    @endforeach

       <br>
       <tr class="bg-info">
        <th colspan="6" class="common_td_rules">Grand Total</th>
            <th class="td_common_numeric_rules">{{$total_purchase_cost}}</th>
            <th class="td_common_numeric_rules">{{$total_selling_price}}</th>
            <th class="td_common_numeric_rules">{{$total_mrp}}</th>
        </tr>
       @else
       <h1 style="text-align: center; color: antiquewhite;">No Record Found</h1>
     @endif
    
 </table>
    </div>
    <input type="hidden" id="is_print" value="{{$is_print}}">
    <input type="hidden" id="is_excel" value="{{$is_excel}}">
 </div>
 