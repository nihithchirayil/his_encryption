@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <style>
        body {
            background-color: white;
        }

        @media print {
            #result_data_table {
                white-space: normal;
                word-break: break-all;
            }

            .responsive_class {
                max-height: none !important;
                overflow: visible !important;
            }
        }

    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" value='<?= $route_data ?>' id="route_value">
    <input type="hidden" value='<?= $current_date ?>' id="current_date">
    <input type="hidden" value='<?= $hospital_headder ?>' id="hospital_header">
    <div class="right_col">
        <div class="container-fluid" style="margin-left:-30px;">

            <div class="col-md-12 padding_sm">
                <div id="filter_area" class="col-md-12" style="padding-left:0px !important;">
                    <div class="box no-border no-margin">
                        <div class="box-body" style="padding-bottom:15px;">
                            <table class="table table-contensed table_sm" style="margin-bottom:10px;">

                                <thead>
                                    <tr class="table_header_bg">
                                        <th colspan="18">{{ $filterheader }}

                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="row">
                                <div class="col-md-12">

                                    {!! Form::open(['name' => 'search_from', 'id' => 'search_from']) !!}
                                    <div>
                                        <?php
                            $i = 1;

                            foreach ($showSearchFields as $fieldType => $fieldName) {
                                $fieldTypeArray[$i] = explode('-', $fieldType);

                                if ($fieldTypeArray[$i][0] == 'progressive_search') {
                                    $hidden_filed_id = $fieldTypeArray[$i][1] . '_hidden';
                                    ?>

                                        <div class="col-md-2 padding_sm" style="margin-top:10px;">

                                            <div class="mate-input-box">
                                                <label>{{ $fieldTypeArray[$i][2] }}</label>
                                                <div class="clearfix"></div>
                                                <input class="form-control hidden_search" value="" autocomplete="off"
                                                    type="text" id="{{ $fieldTypeArray[$i][1] }}"
                                                    name="{{ $fieldTypeArray[$i][1] }}" />
                                                <div id="{{ $fieldTypeArray[$i][1] }}AjaxDiv" class="ajaxSearchBox"
                                                    style="width: 100%" ;></div>
                                                <input class="filters"
                                                    value="{{ $hiddenFields[$hidden_filed_id] }}" type="hidden"
                                                    name="{{ $fieldTypeArray[$i][1] }}_hidden" value=""
                                                    id="{{ $fieldTypeArray[$i][1] }}_hidden" />
                                            </div>
                                        </div>
                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'text') {
                                    ?>
                                        <div class="col-md-2 padding_sm" style="margin-top:10px;">
                                            <div class="mate-input-box">
                                                <label>{{ $fieldTypeArray[$i][2] }}</label>
                                                <div class="clearfix"></div>
                                                <input type="text" name="{{ $fieldTypeArray[$i][1] }}"
                                                    <?php if (isset($fieldTypeArray[$i][3])) {
                                                        echo $fieldTypeArray[$i][3];
                                                    } ?> value="" class="form-control filters"
                                                    id="{{ $fieldTypeArray[$i][1] }}"
                                                    data="{{ isset($fieldTypeArray[$i][3]) ? $fieldTypeArray[$i][3] : '' }}"
                                                    <?php if (isset($fieldTypeArray[$i][3]) && ($fieldTypeArray[$i][3] == 'date')) { ?> readonly <?php } ?>>
                                            </div>
                                        </div>
                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'date') {

                                    ?>
                                        <div class="col-md-2 padding_sm" style="margin-top:10px;">
                                            <div class="mate-input-box">
                                                <label>{{ $fieldTypeArray[$i][2] }} From</label>
                                                <div class="clearfix"></div>
                                                <input type="text" data-attr="date" autocomplete="off"
                                                    name="{{ $fieldTypeArray[$i][1] }}_from" <?php
if (isset($fieldTypeArray[$i][3])) {
    echo $fieldTypeArray[$i][3];
}
?>
                                                    value="{{ date('M-d-Y') }}" class="form-control datepicker filters"
                                                    placeholder="YYYY-MM-DD" id="{{ $fieldTypeArray[$i][1] }}_from">
                                            </div>
                                        </div>
                                        <div class="col-md-2 padding_sm" style="margin-top:10px;">
                                            <div class="mate-input-box">
                                                <label>{{ $fieldTypeArray[$i][2] }} To</label>
                                                <div class="clearfix"></div>
                                                <input type="text" data-attr="date" autocomplete="off"
                                                    name="{{ $fieldTypeArray[$i][1] }}_to" <?php
if (isset($fieldTypeArray[$i][3])) {
    echo $fieldTypeArray[$i][3];
}
?>
                                                    value="{{ date('M-d-Y') }}" class="form-control datepicker filters"
                                                    placeholder="YYYY-MM-DD" id="{{ $fieldTypeArray[$i][1] }}_to">

                                            </div>
                                        </div>
                                        <?php

                                } else if ($fieldTypeArray[$i][0] == 'combo') {
                                    ?>
                                        <div class="col-md-2 padding_sm date_filter_div" style="margin-top:10px;">
                                            <div class="mate-input-box">
                                                <label class="filter_label ">{{ $fieldTypeArray[$i][2] }}</label>
                                                <div class="clearfix"></div>
                                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, $fieldTypeArray[$i][3], ['class' => 'form-control select2 filters datapicker', 'placeholder' => $fieldTypeArray[$i][2], 'id' => $fieldTypeArray[$i][1], 'style' => 'width:50%;color:#555555; padding:2px 12px;']) !!}
                                            </div>
                                        </div>
                                        <?php
                                } else if ($fieldTypeArray[$i][0] == 'multi_combo') {
                                            ?>
                                        <div class="col-md-3 padding_sm" style="margin-top:10px;">
                                            <div class="mate-input-box">
                                                <label class="filter_label"
                                                    style="width: 100%;">{{ $fieldTypeArray[$i][2] }}</label>
                                                <div class="clearfix"></div>
                                                {!! Form::select($fieldTypeArray[$i][1], $fieldName, 0, ['class' => 'form-control select2 filters', 'multiple' => 'multiple-select', 'placeholder' => ' ', 'id' => $fieldTypeArray[$i][1], 'style' => 'width:50%;color:#555555; padding:2px 12px;']) !!}
                                            </div>

                                        </div>
                                        <?php
                                        }
                                        $i++;

                                    }
                                    ?>
                                        <div class="clearfix"></div>


                                        <div id='nondepartment_data_div' class="col-md-3 padding_sm date_filter_div" style="margin-top:10px;">
                                            <div class="mate-input-box">
                                                <label class="filter_label ">Department List</label>
                                                <div class="clearfix"></div>
                                                <select multiple='multiple-select' onchange="getsubdepartments();"
                                                    class='form-control select2 filters' name='department_id'
                                                    id='department_id'>

                                                    <?php
                                                foreach($depatment_list as $each){
                                                    ?>
                                                    <option value='<?= $each->id ?>'><?= $each->dept_name ?></option>
                                                    <?php
                                                }
                                                ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3 padding_sm" style="margin-top:10px;">
                                            <div class="mate-input-box">
                                                <label class="filter_label ">Sub Department List</label>
                                                <div class="clearfix"></div>
                                                <div id="subdepatmentdiv">
                                                    <select multiple='multiple-select'
                                                        onchange="getServiceMasterItemsList();"
                                                        class='form-control select2 filters' name='subdepartment_id'
                                                        id='subdepartment_id'>
                                                        <?php
                                                        foreach($sub_depatment_list as $each){
                                                            ?>
                                                        <option value='<?= $each->id ?>'><?= $each->sub_dept_name ?>
                                                        </option>
                                                        <?php
                                                        }
                                                        ?>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 padding_sm" style="margin-top:10px;">
                                            <div class="mate-input-box">
                                                <label class="filter_label ">Item List</label>
                                                <div class="clearfix"></div>
                                                <div id="itemsdiv">
                                                    <select multiple='multiple-select' class='form-control select2 filters'
                                                        name='item_list' id='item_list'>
                                                        <?php
                                                        foreach($item_list as $each){
                                                            ?>
                                                        <option value='<?= $each->id ?>'><?= $each->service_desc ?>
                                                        </option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="col-md-12 padding_sm" style="text-align:right;margin-top: 10px">
                                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                                <button class="btn light_purple_bg btn-block" type="button"
                                                    onclick="getReportData();" name="search_results" id="search_results">
                                                    <i id="search_results_spin" class="fa fa-search"
                                                        aria-hidden="true"></i>
                                                    Search
                                                </button>
                                            </div>
                                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                                <button class="btn light_purple_bg disabled btn-block" type="button"
                                                    onclick="exceller();" name="csv_results" id="csv_results">
                                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                    Excel
                                                </button>
                                            </div>
                                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                                <button class="btn light_purple_bg disabled btn-block" type="button"
                                                    onclick="printReportData();" name="print_results" id="print_results">
                                                    <i class="fa fa-print" aria-hidden="true"></i>
                                                    Print
                                                </button>
                                            </div>
                                            <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                                                <button class="btn light_purple_bg btn-block" type="button"
                                                    onclick="search_clear();" name="clear_results" id="clear_results">
                                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                                    Reset
                                                </button>
                                            </div>





                                        </div>
                                        {!! Form::token() !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>





            <div class="col-md-12 padding_sm">
                <div class="col-md-12 padding_sm">
                    <div id="ResultDataContainer"
                        style="max-height: 650px; padding: 10px; display:none;font-family:poppinsregular;width:1630px;">
                        <div style="background:#686666;">
                            <page size="A4" style="background: white; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgb(113 113 113 / 50%);
                                                    width: 100%;
                                                    padding: 30px;" id="ResultsViewArea">

                            </page>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------print modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="print_generate('result_container_div')" class="btn bg-primary pull-right"
                            style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/dept_wise_procedure_income_report.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js') }}"></script>
@endsection
