<style>
    table tr:hover td{
        text-overflow: initial;
        white-space: normal;
    }
</style>
<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date From: <?= $from_date ?> To <?= $to_date ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b>Reoder Level Settings Report New</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr class="headerclass"
                    style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='1%'>Sn. No.</th>
                        <th width='15%'>Item Desc</th>
                        <th width='10%'>Location Name</th>
                        <th width='10%'>Re Order Level Qty</th>
                        <th width='11%'>No Sales Consider	</th>
                        <th width='8%'>Re Order Days	</th>
                        <th width='9%'>Ordering Days	</th>
                        <th width='5%'>Sale Qty</th>
                        <th width='13%'>Avg Sales Day</th>
                        <th width='7%'>Ordering Qty</th>
                        <th width='17%'>Update Status</th>
                       
                    </tr>


                </thead>
                <tbody>
                    @if (count($res) != 0)
                        @foreach ($res as $data)
                            <tr style="cursor: pointer;">
                                <td class="td_common_numeric_rules">{{ $loop->iteration }}.</td>
                                <td class="common_td_rules">{{ $data->item_desc }}</td>
                                <td class="common_td_rules">{{ $data->location_name }}</td>
                                <td class="common_td_rules">{{ $data->re_order_level_qty }}</td>
                                <td class="common_td_rules">{{ $data->no_sales_consider }}</td>
                                <td class="common_td_rules">{{ $data->re_order_days }}</td>
                                <td class="common_td_rules">{{ $data->ordering_days }}</td>
                                <td class="common_td_rules">{{ $data->sale_qty }}</td>
                                <td class="common_td_rules">{{ $data->avg_sales_day }}</td>
                                <td class="common_td_rules">{{ $data->ordering_qty }}</td>
                                <td class="common_td_rules">{{ $data->update_status }}</td>
                               



                            </tr>


                        @endforeach
                    @else
                        <tr>
                            <td colspan="11" style="text-align: center">
                                No Result Found
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
