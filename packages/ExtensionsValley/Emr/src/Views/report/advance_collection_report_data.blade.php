<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b>Advance Collection Details Report</b></h4>

            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
    <?php if($patient_type=='1'){
        ?>
    <thead>
        <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th style="padding: 2px;width: 18%;">UHID</th>
            <th style="padding: 2px; width: 18%;">Patient Name</th>
            <th style="padding: 2px; width: 8%;">Admission Date</th>
            <th style="padding: 2px; width: 22%;">Room Type Name</th>
            <th style="padding: 2px; width: 22%;">Ward Name</th>
            <th style="padding: 2px; width: 22%;">Location Name</th>
            <th style="padding: 2px; width: 6%;">Advance Collection</th>
            <th style="padding: 2px;width: 6%;">Advance Adjust</th>
            <th style="padding: 2px;width: 6%;">Advance Refund</th>
            <th style="padding: 2px;width: 6%;">Remaining Advance</th>
            <th style="padding: 2px;width: 6%;">Patient Type</th>
            <th style="padding: 2px;width: 6%;">Remarks</th>
        </tr>
    </thead>
    <?php
    $adv_coll_tot = 0.0;
    $adv_adj_tot = 0.0;
    $adv_ref_tot = 0.0;
    $rem_adv_tot = 0.0;
    if(count($res)!=0){
    ?>

    <tbody>
        <?php
        foreach ($res as $data){
              ?>
            <tr style="cursor: pointer;" onclick="advpopup('<?=$data->uhid?>')">
                <td class="common_td_rules">{{$data->uhid}}</td>
                <td class="common_td_rules">{{$data->patient_name}}</td>
                <td class="common_td_rules">{{date('M-d-Y',strtotime($data->admission_date))}}</td>
                <td class="common_td_rules">{{$data->room_type_name}}</td>
                <td class="common_td_rules">{{$data->ward_name}}</td>
                <td class="common_td_rules">{{$data->location_name}}</td>
                <td class="td_common_numeric_rules">{{ number_format($data->advance_collection, 2, '.', '')}}</td>
                <td class="td_common_numeric_rules">{{ number_format($data->advance_adjust, 2, '.', '')}}</td>
                <td class="td_common_numeric_rules">{{ number_format($data->advance_refund, 2, '.', '')}}</td>
                <td class="td_common_numeric_rules">{{ number_format($data->remaining_advance, 2, '.', '')}}</td>
                <td class="common_td_rules">{{$data->patient_type}}</td>
                <td class="common_td_rules">{{$data->remarks}}</td>
            </tr>
                <?php
                $adv_coll_tot += floatval($data->advance_collection);
                $adv_adj_tot  += floatval($data->advance_adjust);
                $adv_ref_tot  += floatval($data->advance_refund);
                $rem_adv_tot  += floatval($data->remaining_advance);
            }
            ?>
            <tr class="" style="height: 30px;">
                <th class="common_td_rules" colspan="6" style="text-align: left;" >Total</th>
                <th class="td_common_numeric_rules"><?=number_format($adv_coll_tot, 2, '.', '')?></th>
                <th class="td_common_numeric_rules"><?=number_format($adv_adj_tot, 2, '.', '')?></th>
                <th class="td_common_numeric_rules"><?=number_format($adv_ref_tot, 2, '.', '')?></th>
                <th class="td_common_numeric_rules"><?=number_format($rem_adv_tot, 2, '.', '')?></th>
            </tr>
        <?php
         } else{ ?>
    <tr>
        <td colspan="10" style="text-align: center;">No Results Found!</td>
    </tr>
    <?php } ?>
    </tbody>
    <?php }elseif($patient_type=='2'){
      ?>
      <thead>
          <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
              <th style="padding: 2px;width: 10%;">UHID</th>
              <th style="padding: 2px; width: 20%;">Patient Name</th>
              <th style="padding: 2px; width: 10%;">Advance Collection</th>
              <th style="padding: 2px;width: 10%;">Advance Adjust</th>
              <th style="padding: 2px;width: 10%;">Advance Refund</th>
              <th style="padding: 2px;width: 10%;">Remaining_advance</th>
              <th style="padding: 2px;width: 6%;">Patient Type</th>

          </tr>
      </thead>
      <?php
      $adv_coll_tot = 0.0;
      $adv_adj_tot = 0.0;
      $adv_ref_tot = 0.0;
      $rem_adv_tot = 0.0;
      if(count($res)!=0){
      ?>

      <tbody>
          <?php
          foreach ($res as $data){
                ?>


          <tr style="cursor: pointer;" onclick="advpopup('<?=$data->uhid?>')">
                  <td class="common_td_rules">{{$data->uhid}}</td>
                  <td class="common_td_rules">{{$data->patient_name}}</td>
                  <td class="td_common_numeric_rules">{{$data->advance_collection}}</td>
                  <td class="td_common_numeric_rules">{{$data->advance_adjust}}</td>
                  <td class="td_common_numeric_rules">{{$data->advance_refund}}</td>
                  <td class="td_common_numeric_rules">{{$data->remaining_advance}}</td>
                  <td class="common_td_rules">{{$data->patient_type}}</td>
              </tr>
                  <?php
                  $adv_coll_tot += floatval($data->advance_collection);
                  $adv_adj_tot  += floatval($data->advance_adjust);
                  $adv_ref_tot  += floatval($data->advance_refund);
                  $rem_adv_tot  += floatval($data->remaining_advance);
              }
              ?>
              <tr class="" style="height: 30px;">
                  <th class="common_td_rules" colspan="2" style="text-align: left;" >Total</th>
                  <th class="td_common_numeric_rules"><?=$adv_coll_tot?></th>
                  <th class="td_common_numeric_rules"><?=$adv_adj_tot?></th>
                  <th class="td_common_numeric_rules"><?=$adv_ref_tot?></th>
                  <th class="td_common_numeric_rules"><?=$rem_adv_tot?></th>
              </tr>
          <?php
           } else{ ?>
      <tr>
          <td colspan="6" style="text-align: center;">No Results Found!</td>
      </tr>
      <?php } ?>
      </tbody>
      <?php } elseif($patient_type=='3'){
        ?>
     <thead>
        <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th style="padding: 2px;width: 10%;">UHID</th>
            <th style="padding: 2px; width: 20%;">Patient Name</th>
            <th style="padding: 2px; width: 10%;">Advance Collection</th>
            <th style="padding: 2px;width: 10%;">Advance Adjust</th>
            <th style="padding: 2px;width: 10%;">Advance Refund</th>
            <th style="padding: 2px;width: 10%;">Remaining_advance</th>
            <th style="padding: 2px;width: 6%;">Patient Type</th>

        </tr>
    </thead>
    <?php
    $adv_coll_tot = 0.0;
    $adv_adj_tot = 0.0;
    $adv_ref_tot = 0.0;
    $rem_adv_tot = 0.0;
    if(count($res)!=0){
    ?>

    <tbody>
        <?php
        foreach ($res as $data){
              ?>


        <tr style="cursor: pointer;" onclick="advpopup('<?=$data->uhid?>')">
                <td class="common_td_rules">{{$data->uhid}}</td>
                <td class="common_td_rules">{{$data->patient_name}}</td>
                <td class="td_common_numeric_rules">{{$data->advance_collection}}</td>
                <td class="td_common_numeric_rules">{{$data->advance_adjust}}</td>
                <td class="td_common_numeric_rules">{{$data->advance_refund}}</td>
                <td class="td_common_numeric_rules">{{$data->remaining_advance}}</td>
                <td class="common_td_rules">{{$data->patient_type}}</td>
            </tr>
                <?php
                $adv_coll_tot += floatval($data->advance_collection);
                $adv_adj_tot  += floatval($data->advance_adjust);
                $adv_ref_tot  += floatval($data->advance_refund);
                $rem_adv_tot  += floatval($data->remaining_advance);
            }
            ?>
            <tr class="" style="height: 30px;">
                <th class="common_td_rules" colspan="2" style="text-align: left;" >Total</th>
                <th class="td_common_numeric_rules"><?=$adv_coll_tot?></th>
                <th class="td_common_numeric_rules"><?=$adv_adj_tot?></th>
                <th class="td_common_numeric_rules"><?=$adv_ref_tot?></th>
                <th class="td_common_numeric_rules"><?=$rem_adv_tot?></th>
            </tr>
            <?php
        } else{ ?>
   <tr>
       <td colspan="6" style="text-align: center;">No Results Found!</td>
   </tr>
   <?php } ?>
   </tbody>
   <?php }
     ?>
</table>

</div>
</div>
