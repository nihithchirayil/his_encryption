<div class="row">
    <div class="col-md-12" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
        <?php
        $collect = collect($res);
        $total_records = count($collect);
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Patient Bill Report </b></h4>
        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
            style="font-size: 12px;">
            <thead>
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    <th width="15%" style="padding: 2px; text-align: center; ">Patient name</th>
                    <th width="15%" style="padding: 2px; text-align: center; ">UHID</th>
                    <th width="15%" style="padding: 2px; text-align: center; ">Item</th>
                    <th width="15%" style="padding: 2px; text-align: center; ">Bill No</th>
                    <th width="15%" style="padding: 2px; text-align: center; ">Bill Tag</th>
                    <th width="5%" style="padding: 2px; text-align: center; ">Qty</th>
                    <th width="10%" style="padding: 2px;text-align: center; ">Paid Status</th>
                    <th width="10%" style="padding: 2px;text-align: center; ">Payment Date</th>
                    <th width="10%" style="padding: 2px;text-align: center; ">Company Name</th>
                    <th width="10%" style="padding: 2px;text-align: center; ">Consulting Doctor</th>
                    <th width="10%" style="padding: 2px;text-align: center; ">Net Amount</th>
                </tr>

            </thead>
            <tbody>

                <?php
   $total_qty = 0;
   $total_net_amount = 0;
   if (count($res)!=0) {
       foreach ($res as $data) {
           ?>
                <tr>

                    <td class="common_td_rules">{{ $data->patient_name }}</td>
                    <td class="common_td_rules">{{ $data->uhid }}</td>
                    <td class="common_td_rules">{{ $data->item_desc }}</td>
                    <td class="common_td_rules">{{ $data->bill_no }}</td>
                    <td class="common_td_rules">{{ $data->bill_tag }}</td>
                    <td class="td_common_numeric_rules">{{ $data->qty }}</td>
                    <td class="common_td_rules">{{ $data->paidstatus }}</td>
                    <td class="common_td_rules">
                        {{ $data->payment_accounting_date ? $data->payment_accounting_date : '-' }}</td>
                    <td class="common_td_rules">{{ $data->company_name }}</td>
                    <td class="common_td_rules">{{ $data->consulting_doctor_name }}</td>
                    <td class="td_common_numeric_rules">{{ $data->net_amount }}</td>

                </tr>
                <?php
       $total_qty+= $data->qty;
       $total_net_amount+= $data->net_amount;
       }
       ?>

                <tr style="font-family:robotoregular">
                    <th class="common_td_rules" colspan="5"><b>Total</b></th>
                    <th class="td_common_numeric_rules">{{ $total_qty }}</th>
                    <th class="td_common_numeric_rules" colspan="5">{{ $total_net_amount }}</th>

                </tr>
                <?php
   }else{
       ?>
                <tr>
                    <td colspan="11" style="text-align: center;">No Results Found!</td>
                </tr>
                <?php
   }
   ?>
            </tbody>
        </table>
    </div>
</div>
