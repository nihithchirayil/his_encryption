<div class="row">
 
   <div class="col-md-12" id="result_containerpopdiv">

    <table id="result_data_table" style="font-size: 12px;">
    <thead>
        <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th style="padding: 2px;width: 10%;">UHID</th>
            <th style="padding: 2px;width: 25%;">Patient Name</th>
            <th style="padding: 2px;width: 20%;">Payment Date</th>
            <th style="padding: 2px; width: 10%;">Bill NO</th>
            <th style="padding: 2px; width: 15%;">Name</th>
            <th style="padding: 2px; width: 10%;">Net Amount</th>
            <th style="padding: 2px; width: 15%;">Advance Adjusted</th>
        </tr>
    </thead>
    <?php
    $tot_netamt = 0.0;
    $tot_advance_adj = 0.0;
    if(count($res)!=0){
    ?>
    <tbody>
        <?php 
        foreach ($res as $data){
              ?>
        
            
            <tr>
                <td>{{$data->uhid}}</td>
                <td>{{$data->patient_name}}</td>
                <td>{{$data->payment_date}}</td>
                <td>{{$data->bill_no}}</td>
                <td>{{$data->name}}</td>
                <td>{{$data->net_amount_wo_roundoff}}</td>
                <td>{{$data->advance_adjusted}}</td>
            </tr>
                <?php
                $tot_netamt += floatval($data->net_amount_wo_roundoff);
                $tot_advance_adj  += floatval($data->advance_adjusted);
            }
            ?>
            <tr class="" style="height: 30px;">
                <th colspan="3" style="text-align: left;" >Total</th>
                <th><?=$tot_netamt?></th>
                <th><?=$tot_advance_adj?></th>
            </tr>
        <?php
         } else{ ?>
    <tr>
        <td colspan="10" style="text-align: center;">No Results Found!</td>
    </tr>
    <?php } ?>
    </tbody>
   
</table>

</div>
</div>
