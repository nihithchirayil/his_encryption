    <style>table, th, td {
    border: 1px solid #dad9d954;


}</style>
<div class="row">
    <div class="theadscroll always-visible" id="result_container_div" style="position: relative;height:510px;" >
     @if(sizeof($res)>0)
     <p style="font-size: 12px;" id="total_data">Report Date: <?=date('Y-m-d h:i A')?> </p><br>
     @php
     $collect = collect($res); $total_records=count($collect);
    @endphp
    <p style="font-size: 12px;">Total Records<b> : {{$total_records}}</b></p>
    <h2 style="text-align: center;margin-top: -29px;" id="heading"> Discharge Summary Report  </h2>
     <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper' style="font-size: 12px;">


        <thead class="headerclass" style="background-color:#36A693 !important;color:white;border-spacing: 0 1em;font-family:robotoregular">
        <tr>
        <th width="2%" style="padding: 2px;text-align: center; ">SlNo.</th>
         <th width="10%" style="padding: 2px;text-align: center;border:2px; ">Patient Name</th>
         <th width="10%" style="padding: 2px;text-align: center;">Age/ Gender</th>
         {{-- <th width="10%" style="padding: 2px;text-align: center;">Address</th> --}}
         <th width="10%" style="padding: 2px;text-align: center;">UHID</th>
         <th width="10%" style="padding: 2px;text-align: center;">Ip No</th>
         <th width="10%" style="padding: 2px;text-align: center;">Doctor Name</th>
         <th width="10%" style="padding: 2px;text-align: center;">Admit Date</th>
         <th width="10%" style="padding: 2px;text-align: center;">Discharge Date</th>
         {{-- <th  style="padding: 2px;width:10%;text-align: center;">summary Prepared By</th> --}}
         <th  style="padding: 2px;width:10%;text-align: center;">Created At</th>
         <th  style="padding: 2px;width:10%;text-align: center;">View</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Print</th>
         {{-- <th  style="padding: 2px;width:8%;text-align: center;">Last Updated At</th> --}}
         {{-- <th  style="padding: 2px;width:10%;text-align: center;">Discharge Summary</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Edit</th>
         <th  style="padding: 2px;width:10%;text-align: center;">Doctor Approval Status</th> --}}
         </tr>
        </thead>
    <tbody >
     @foreach ($res as $data)
        <tr>
            <td class="td_common_numeric_rules" class="td_common_numeric_rules" style="padding:5px;width:2%;">{{ $loop->iteration}}.</td>
            <td class="td_common_rules" class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;">{{$data->patient_name}}</td>
            @if ($data->gender == 'Female')
            <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;" >{{$data->age}}/F</td>
            @else
            <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;" >{{$data->age}}/M</td>
            @endif
            {{-- <td class="td_common_rules" rowspan="" style="padding:5px;width:10%;">{{($data->address)}}</td> --}}
            <td class="td_common_rules" rowspan="" style="padding:5px;width:10%;">{{($data->op_no)}}</td>
            <td class="td_common_rules" rowspan="" style="padding:5px;width:10%;">{{($data->ip_no)}}</td>
            <td class="td_common_rules" rowspan="" style="padding:5px;width:10%;">{{($data->doctor_name)}}</td>
            <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;"> {{date('M-d-Y h:i:a',strtotime($data->admit_date))}}</td>
            <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;"> {{date('M-d-Y h:i:a',strtotime($data->discharge_date))}}</td>
            {{-- <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;">{{($data->summary_prepared_by)}}</td> --}}
            <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;"> {{date('M-d-Y h:i:a',strtotime($data->created_at))}}</td>
            <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;text-align: center !important;">
                <button type="button" class="btn btn-info" onclick="createSummaryPrintFile('{{$data->id,'view'}}');">
                    <i class="fa fa-eye"></i>
                </button>
            </td>
            <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;text-align: center !important;">
                <button type="button" class="btn btn-primary" onclick="createSummaryPrintFile('{{$data->id}}','print');">
                    <i class="fa fa-book"></i>
                </button>
            </td>
            {{-- <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;">{{date('M-d-Y h:i:a',strtotime($data->last_updated_at))}}</td> --}}
            {{-- <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;">{{($data->discharge_summery)}}</td>
            <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;">{{($data->Edit)}}</td>
            <td class="td_common_numeric_rules" rowspan="" style="padding:5px;width:10%;">{{($data->Doctor_aprovel)}}</td> --}}
        </tr>

     @endforeach
    </tbody>
    @else
    <h1 style="text-align: center; color: antiquewhite;">No Data Found</h1>
    @endif
        </table>
    </div>
</div>
