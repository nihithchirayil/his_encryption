<div class="row">
    <div class="col-md-12" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b>{{ date('M-d-Y h:i A') }} </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <b><?= $from ?> To <?= $to_date ?></b></p>

        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> GST Report </b></h4>
        <div class="col-md-12" id="result_container_div">

            @php
                extract($headData);
                $result1 = $top;
                $result2 = $table1;
                $report_decimal_separator;
                $result3 = $table2;
                $result4 = $table3;
                $result5 = $table4;
                $result11 = $table5;
            @endphp

            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">

                <thead class="headerclass">
                    <tr>
                        <th class="common_td_rules" colspan="15"><strong>
                                Sales
                            </strong>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules" colspan="15"><strong>
                                {{ @$result1[0]->name ? $result1[0]->name : ''  }} &nbsp;&nbsp;&nbsp;&nbspStart From:{{ @$result1[0]->bill_no ? $result1[0]->bill_no : '' }}
                                To:{{ @$result11[0]->bill_no ? $result11[0]->bill_no : '' }}
                            </strong>
                        </th>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='7%'>Date</th>
                        <th width='6%'>Sales</br>(0%)</th>
                        <th width='6%'>Sales</br>(5%)</th>
                        <th width='7%'>CGST</br>(2.5%)</th>
                        <th width='7%'>SGST</br>(2.5%)</th>
                        <th width='6%'>Sales</br>(12%)</th>
                        <th width='6%'>CGST</br>(6%)</th>
                        <th width='6%'>SGST<br>(6%)</th>
                        <th width='7%'>Sales</br>(18%)</th>
                        <th width='7%'>CGST</br>(9%)</th>
                        <th width='7%'>SGST</br>(9%)</th>
                        <th width='7%'>Sales</br>(28%)</th>
                        <th width='7%'>CGST</br>(14%)</th>
                        <th width='7%'>SGST</br>(14%)</th>
                        <th width='7%'>Total</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
            if(count($result2)!=0){

                $total_netsale=0.0;
                $netsales_five=0.0;
                $netsales_fivetax=0.0;

                $netsales_twelve=0.0;
                $netsales_twelvetax=0.0;

                $netsaleseighteen=0.0;
                $netsaleseighteentax=0.0;

                $netsalestwentyeight=0.0;
                $netsalestwentyeighttax=0.0;
                $netexemptedsales=0.0;

                $net_total=0.0;
                $total_tax=0.0;
              foreach($result2 as $each){

                $netexemptedsales+=floatval($each->exemptedsales);

                $salesfivetax= floatval($each->salesfivetax)/2;
                $netsales_five+=floatval($each->salesfive);
                $netsales_fivetax+=floatval($each->salesfivetax);


                $salestwelvetax= floatval($each->salestwelvetax)/2;
                $netsales_twelve+=floatval($each->salestwelve);
                $netsales_twelvetax+=floatval($each->salestwelvetax);

                $saleseighteen= floatval($each->saleseighteentax)/2;
                $netsaleseighteen+=floatval($each->saleseighteen);
                $netsaleseighteentax+=floatval($each->saleseighteentax);

                $salestwentyeight= floatval($each->salestwentyeighttax);
                $netsalestwentyeight+=floatval($each->salestwentyeight);
                $netsalestwentyeighttax+=floatval($each->salestwentyeighttax);

                $total_tax=floatval($each->exemptedsales)+floatval($each->salesfive)+ floatval($each->salesfivetax)+floatval($each->salestwelve)+floatval($each->salestwelvetax)+ floatval($each->saleseighteen)+floatval($each->saleseighteentax)+floatval($each->salestwentyeight)+floatval($each->salestwentyeighttax);
                $net_total+=$total_tax;
                ?>
                    <tr>
                        <td class="common_td_rules"><?= date('M-d-Y', strtotime($each->payment_date)) ?></td>
                        <td class="td_common_numeric_rules"><?= $each->exemptedsales ?></td>

                        <td class="td_common_numeric_rules"><?= $each->salesfive ?></td>
                        <td class="td_common_numeric_rules"><?= $salesfivetax ?></td>
                        <td class="td_common_numeric_rules"><?= $salesfivetax ?></td>

                        <td class="td_common_numeric_rules"><?= $each->salestwelve ?></td>
                        <td class="td_common_numeric_rules"><?= $salestwelvetax ?></td>
                        <td class="td_common_numeric_rules"><?= $salestwelvetax ?></td>

                        <td class="td_common_numeric_rules"><?= $each->saleseighteen ?></td>
                        <td class="td_common_numeric_rules"><?= $saleseighteen ?></td>
                        <td class="td_common_numeric_rules"><?= $saleseighteen ?></td>

                        <td class="td_common_numeric_rules"><?= $each->salestwentyeight ?></td>
                        <td class="td_common_numeric_rules"><?= $salestwentyeight ?></td>
                        <td class="td_common_numeric_rules"><?= $salestwentyeight ?></td>

                        <td class="td_common_numeric_rules"><?= $total_tax ?></td>
                    </tr>
                    <?php
                    }
                    $total_netsale=$net_total;
                    ?>
                    <tr>
                        <th class="common_td_rules">Total</th>
                        <th class="td_common_numeric_rules"><?= $netexemptedsales ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_five ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_fivetax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_fivetax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_twelve ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_twelvetax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_twelvetax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsaleseighteen ?></th>
                        <th class="td_common_numeric_rules"><?= $netsaleseighteentax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsaleseighteentax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsalestwentyeight ?></th>
                        <th class="td_common_numeric_rules"><?= $netsalestwentyeighttax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsalestwentyeighttax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $net_total ?></th>

                    </tr>
                    <?php
            }else{
                ?>
                    <tr>
                        <td colspan="15" style="text-align: center">No Result Found</td>
                    </tr>
                    <?php
            }
            ?>

                </tbody>

                <thead class="headerclass">
                <th class="common_td_rules" colspan="15"><strong>
                                Sales Return
                            </strong>
                        </th>
                <tr>
                        <th class="common_td_rules" colspan="15"><strong>
                                {{ @$result4[0]->name ? $result4[0]->name : '' }} &nbsp;&nbsp;&nbsp;&nbspStart
                                From:{{ @$result4[0]->return_no ? $result4[0]->return_no : '' }}
                                To:{{ @$result5[0]->return_no ? $result5[0]->return_no : '' }}
                            </strong>
                        </th>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='7%'>Date</th>
                        <th width='6%'>Sales</br>(0%)</th>
                        <th width='6%'>Sales</br>(5%)</th>
                        <th width='7%'>CGST</br>(2.5%)</th>
                        <th width='7%'>SGST</br>(2.5%)</th>
                        <th width='6%'>Sales</br>(12%)</th>
                        <th width='6%'>CGST</br>(6%)</th>
                        <th width='6%'>SGST</br>(6%)</th>
                        <th width='7%'>Sales</br>(18%)</th>
                        <th width='7%'>CGST</br>(9%)</th>
                        <th width='7%'>SGST</br>(9%)</th>
                        <th width='7%'>Sales</br>(28%)</th>
                        <th width='7%'>CGST</br>(14%)</th>
                        <th width='7%'>SGST</br>(14%)</th>
                        <th width='7%'>Total</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
            if(count($result3)!=0){

                $netsales_five=0.0;
                $netsales_fivetax=0.0;

                $netsales_twelve=0.0;
                $netsales_twelvetax=0.0;

                $netsaleseighteen=0.0;
                $netsaleseighteentax=0.0;

                $netsalestwentyeight=0.0;
                $netsalestwentyeighttax=0.0;
                $netexemptedsales=0.0;

                $net_total=0.0;
                $total_tax=0.0;
              foreach($result3 as $each){

                $netexemptedsales+=floatval($each->exemptedsales);

                $salesfivetax= floatval($each->salesfivetax)/2;
                $netsales_five+=floatval($each->salesfive);
                $netsales_fivetax+=floatval($each->salesfivetax);


                $salestwelvetax= floatval($each->salestwelvetax)/2;
                $netsales_twelve+=floatval($each->salestwelve);
                $netsales_twelvetax+=floatval($each->salestwelvetax);

                $saleseighteen= floatval($each->saleseighteentax)/2;
                $netsaleseighteen+=floatval($each->saleseighteen);
                $netsaleseighteentax+=floatval($each->saleseighteentax);

                $salestwentyeight= floatval($each->salestwentyeighttax);
                $netsalestwentyeight+=floatval($each->salestwentyeight);
                $netsalestwentyeighttax+=floatval($each->salestwentyeighttax);

                $total_tax=floatval($each->exemptedsales)+floatval($each->salesfive)+ floatval($each->salesfivetax)+floatval($each->salestwelve)+floatval($each->salestwelvetax)+ floatval($each->saleseighteen)+floatval($each->saleseighteentax)+floatval($each->salestwentyeight)+floatval($each->salestwentyeighttax);
                $net_total+=$total_tax;
                ?>
                    <tr>
                        <td class="common_td_rules"><?= date('M-d-Y', strtotime($each->payment_date)) ?></td>
                        <td class="td_common_numeric_rules"><?= $each->exemptedsales ?></td>

                        <td class="td_common_numeric_rules"><?= $each->salesfive ?></td>
                        <td class="td_common_numeric_rules"><?= $salesfivetax ?></td>
                        <td class="td_common_numeric_rules"><?= $salesfivetax ?></td>

                        <td class="td_common_numeric_rules"><?= $each->salestwelve ?></td>
                        <td class="td_common_numeric_rules"><?= $salestwelvetax ?></td>
                        <td class="td_common_numeric_rules"><?= $salestwelvetax ?></td>

                        <td class="td_common_numeric_rules"><?= $each->saleseighteen ?></td>
                        <td class="td_common_numeric_rules"><?= $saleseighteen ?></td>
                        <td class="td_common_numeric_rules"><?= $saleseighteen ?></td>

                        <td class="td_common_numeric_rules"><?= $each->salestwentyeight ?></td>
                        <td class="td_common_numeric_rules"><?= $salestwentyeight ?></td>
                        <td class="td_common_numeric_rules"><?= $salestwentyeight ?></td>

                        <td class="td_common_numeric_rules"><?= $total_tax ?></td>
                    </tr>
                    <?php
                    }
                    $total_netsale=$total_netsale-$net_total;
                    ?>
                    <tr>
                        <th class="common_td_rules">Total</th>
                        <th class="td_common_numeric_rules"><?= $netexemptedsales ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_five ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_fivetax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_fivetax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_twelve ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_twelvetax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsales_twelvetax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsaleseighteen ?></th>
                        <th class="td_common_numeric_rules"><?= $netsaleseighteentax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsaleseighteentax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsalestwentyeight ?></th>
                        <th class="td_common_numeric_rules"><?= $netsalestwentyeighttax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $netsalestwentyeighttax / 2 ?></th>
                        <th class="td_common_numeric_rules"><?= $net_total ?></th>

                    </tr>
                    <tr>
                        <th class="common_td_rules bg-info" colspan="14">Net Sales(Sales-Sales Return)</th>
                        <th class="td_common_numeric_rules bg-info"><?= $total_netsale ?></th>
                    </tr>
                    <?php
            }else{
                ?>
                    <tr>
                        <td colspan="15" style="text-align: center">No Result Found</td>
                    </tr>
                    <?php
            }
            ?>

                </tbody>

            </table>

        </div>

    </div>
