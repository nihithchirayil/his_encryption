<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
            <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
            <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
            <?php
            $collect = collect($res);
            $total_records = count($collect);
            ?>
            <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
            <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Director Report(Selected items) Report</b></h4>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr>
                        <td colspan="3">
                            <?= base64_decode($hospital_headder) ?>
                        </td>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width='5%'>Sn. No.</th>
                        <th width='80%'>Item Description</th>
                        <th width='15%'>Net Amount</th>
                    </tr>


                </thead>
                <tbody>
                    <?php
                    if (count($res) != 0){
                        $i=1;
                        $net_total=0.0;
                        foreach ($res as $data){
                            $net_total+= floatval($data->net_amount);
                            ?>
                    <tr>
                        <td class="td_common_numeric_rules">{{ $i }}.</td>
                        <td class="common_td_rules">{{ $data->item_desc }}</td>
                        <td class="td_common_numeric_rules">{{ $data->net_amount }}</td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
                    <tr>
                        <th class="common_td_rules" colspan="2">Total</th>
                        <th class="td_common_numeric_rules">{{ $net_total }}</th>
                    </tr>
                    <?php

                    }else{
                            ?>
                    <tr>
                        <td colspan="3" style="text-align: center">
                            No Result Found
                        </td>
                    </tr>
                    <?php
                        } ?>
                </tbody>
            </table>
        </div>
    </div>
