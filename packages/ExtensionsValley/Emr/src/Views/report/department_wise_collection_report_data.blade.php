<div class="row">
    <div class="col-md-12" id="result_container_div">
        <div class="print_data" style="margin-top: 10px">
              <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?=date('M-d-Y h:i A')?> </b></p>
              <p style="font-size: 12px;" id="total_data">Report Date: <?=$from?> To <?=$to_date?></p>
              <?php
                  $collect = collect($res); $total_records=count($collect);
              ?>
              <p style="font-size: 12px;">Total Count<b> : {{$total_records}}</b></p>
              <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Department wise Income - General </b></h4>
              <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <tr class="headerclass" style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
            <th width="5%;">SL.No.</th>
            <th width="55%;">Department Name</th>
                <th width="15%;">Collected Amount</th>
                <th width="15%;">Refund amount</th>
                <th width="10%;">Net Amount</th>
            </tr>
    </thead>
    <?php
        $gross_amount = 0.0;
        $refund = 0.0;
        $netamounttot=0.0;
    ?>
    @if(sizeof($res)>0)
    <tbody>
        <?php
        $i=1;
        foreach ($res as $data){
                $net_amt = floatval($data->gross_amount)-floatval($data->refund);

              ?>

            <tr>
            <td class="common_td_rules">{{$i}}.</td>
                <td class="common_td_rules">{{$data->dept_name}}</td>
                <td class="td_common_numeric_rules">{{$data->gross_amount}}</td>
                <td class="td_common_numeric_rules">{{$data->refund}}</td>
                <td class="td_common_numeric_rules">{{$net_amt}}</td>
            </tr>
                <?php
           $gross_amount += floatval($data->gross_amount);
           $refund+= floatval($data->refund);
           $netamounttot+=$net_amt;
           $i++;

        }

        ?>


    <tr style="height: 30px;">
        <th colspan='2' class="common_td_rules" >Total</th>
        <th class="td_common_numeric_rules"><?=number_format((float)$gross_amount, 2, '.', '')?></th>
        <th class="td_common_numeric_rules"><?=number_format((float)$refund, 2, '.', '')?></th>
        <th class="td_common_numeric_rules"><?=number_format((float)$netamounttot, 2, '.', '')?></th>
     </tr>
     <tr style="height: 30px;" class="bg-info">
        <th colspan='4' class="common_td_rules" ><?=@$res1[0]->dept ? $res1[0]->dept : ''?></th>
        <th class="td_common_numeric_rules"><?=@$res1[0]->sum ? $res1[0]->sum : ''?></th>
     </tr>

     @else
    <tr>
        <td colspan="5" style="text-align: center;">No Results Found!</td>
    </tr>
    @endif
     </tbody>
</table>
</div>
</div>
</div>
