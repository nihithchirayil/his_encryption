<div class="row" id="print_data">
    <div class="col-md-12" id="result_container_div">

        <p style="font-size: 12px;" id="total_data">Report Print Date: <b> <?= date('M-d-Y h:i A') ?> </b></p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
        <p style="font-size: 12px;">Total Count<b> : {{ count($res) }}</b></p>
        <h4 style="text-align: center;margin-top: -29px;" id="heading"><b> Doctor Op Ip count Report </b></h4>
        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
            style="font-size: 12px;">
            <thead>
                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                    @if($group_by_date != 1)
                    <th width='10%'>Date</th>
                    @endif
                    <th width='35%'>Doctor name</th>
                    @if($group_by_date != 1)
                    <th width='35%'>Department</th>
                    @endif
                    <th width='10%'>OP</th>
                    <th width='10%'>IP</th>
                </tr>
            </thead>

            <?php
            $total_op=0;
            $total_ip=0;
            $style = "";
            $res = collect($res)->groupBy('department');
            // dd($res);
     if(count($res)!=0){
        foreach ($res as $key => $data){
            ?>
            @if($group_by_date == 1)
             <tr>
                <td colspan="" style='text-align:left;background-color: #d8e4e4;'><b>{{$key}}</b></td>
                <td  style='text-align:center;background-color: #d8e4e4;'><b>{{$data[0]->dept_op}}</b></td>
                <td  style='text-align:center;background-color: #d8e4e4;'><b>{{$data[0]->dept_ip}}</b></td>
            </tr>
            @endif
            @foreach ($data as $val )
            <tr>
                @if($group_by_date != 1)
                <td class="common_td_rules">{{ date('M-d-Y', strtotime($val->visit_date)) }}</td>
                @endif
                <td class="common_td_rules">{{ $val->doctor_name }}</td>
                @if($group_by_date != 1)
                <td class="common_td_rules">{{ $val->department }}</td>
                @endif
                @if($group_by_date == 1)
                @php
                $style = "text-align:center!important";
                @endphp
                @endif
                <td class="td_common_numeric_rules" style={{ $style }}>{{ $val->op }}</td>
                <td class="td_common_numeric_rules" style={{ $style }}>{{ $val->ip }}</td>
            </tr>
            @php
            $total_op+= floatval($val->op);
            $total_ip+= floatval($val->ip);
            @endphp
            @endforeach
            <?php
            
           
        }
        ?>
            <tr class="bg-info">
                @if($group_by_date != 1)
                <th colspan="3" class="common_td_rules">Total</th>
                @else
                <th colspan="1" class="common_td_rules">Total</th>
                @php
                $style = "text-align:center!important";
                @endphp
                @endif
                <th class="td_common_numeric_rules" style={{ $style }}><?=$total_op?></th>
                <th class="td_common_numeric_rules" style={{ $style }}><?=$total_ip?></th>
            </tr>
            <?php
     }else{
         ?>
            <tr>
                <th colspan="5" style="text-align: center">No Result Found</th>
            </tr>
            <?php
     }
     ?>
        </table>
    </div>
    <input type="hidden" id="is_print" value="{{ $is_print }}">
    <input type="hidden" id="is_excel" value="{{ $is_excel }}">
</div>
