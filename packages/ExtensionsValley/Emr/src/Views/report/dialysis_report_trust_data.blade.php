<div class="row">
    <div class="col-md-12" id="result_container_div">
        <p style="font-size: 12px;" id="total_data">Report Print Date: <?= date('Y-m-d h:i A') ?> </p>
        <p style="font-size: 12px;" id="total_data">Report Date: <?= $from ?> To <?= $to ?></p>
        <?php
        $total_records = 0;
        if (count($res) != 0) {
            $collect = collect($res);
            $total_records = count($collect);
        }
        ?>
        <p style="font-size: 12px;">Total Count<b> : {{ $total_records }}</b></p>
        {{-- <h4 style="text-align: center;margin-top: -29px;" id="heading"> Dialysis Report(Daya Charitable Trust)</h4> --}}
        <font size="16px" face="verdana">
            <?php if ($summary =='1') { ?>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    <tr>
                        <td colspan="4">
                            <?= base64_decode($hospital_headder) ?>
                        </td>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="40%;">Department Name</th>
                        <th width="20%;">Total Amount</th>
                        <th width="20%;">Total Refund Amount</th>
                        <th width="20%;">Net Amount</th>
                    </tr>
                </thead>
                <?php
                $fulltotal_bill_net_amount = 0;
                $nettotal_bill_net_amount = 0;
                $total_bill_net_amount = 0;
                ?>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
                        $i = 0;
                        $date = '';
                        foreach ($res as $data) {
                            $nettotal_bill_net_amount=floatval($data->total_amount)-$data->total_refundamount;
                            if ($date != $data->date) {
                                $i = 0;
                                $date = $data->date;
                                $total_bill_net_amount = 0.0;
                        ?>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="7" class="common_td_rules">{{ date('M-d-Y', strtotime($date)) }}</th>
                        </tr>
                        <tr>
                            <td class="common_td_rules">{{ $data->name }}</td>
                            <td class="common_td_rules">{{ $data->total_amount }}</td>
                            <td class="common_td_rules">{{ $data->total_refundamount }}</td>
                            <td class="common_td_rules">{{ $nettotal_bill_net_amount }}</td>
                        </tr>
                        <?php
                            } else {
                            ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->name }}</td>
                            <td class="common_td_rules">{{ $data->total_amount }}</td>
                            <td class="common_td_rules">{{ $data->total_refundamount }}</td>
                            <td class="common_td_rules">{{ $nettotal_bill_net_amount }}</td>
                        </tr>
                        <?php

                            }

                            $total_bill_net_amount += floatval($nettotal_bill_net_amount);
                            if ($i == $date_cnt[$date]) {
                            ?>
                        <tr class="bg-info" style="height: 30px;">
                            <th class="common_td_rules" colspan="3" style="text-align: left;">Total</th>
                            <th class="td_common_numeric_rules"><?= $total_bill_net_amount ?></th>
                        </tr>
                        <?php
                                $fulltotal_bill_net_amount += $total_bill_net_amount;
                            }
                            $i++;
                        }
                        ?>
                        <tr class="" style=" height: 30px;">
                            <th class="common_td_rules" colspan="3" style="text-align: left;">Net Total</th>
                            <th class="td_common_numeric_rules"><?= $fulltotal_bill_net_amount ?></th>
                        </tr>
                    @else
                        <tr>
                            <td colspan="4" style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
            <?php
            } else { ?>
            <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
                style="font-size: 12px;">
                <thead>
                    @php
                        if ($use_bill_tag_header == 1) {
                            $hospital_header = \DB::table('bill_tag')
                                ->where('code', $bill_tag)
                                ->value('bill_tag_header');
                        } else {
                            $hospital_header = base64_decode($hospital_headder);
                        }
                    @endphp
                    <tr>
                        <td colspan="7">
                            {!! $hospital_header !!}
                        </td>
                    </tr>
                    <tr class="headerclass"
                        style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="20%;">UHID</th>
                        <th width="20%;">Patient Name</th>
                        <th width="15%;">Bill No</th>
                        <th width="15%;">Department Name</th>
                        <th width="10%;">Total Amount</th>
                        <th width="10%;">Total Refund Amount</th>
                        <th width="10%;">Net Amount</th>
                    </tr>
                </thead>
                <?php
                $fulltotal_bill_net_amount = 0;
                $nettotal_bill_net_amount = 0;
                $total_bill_net_amount = 0;
                ?>
                @if (sizeof($res) > 0)
                    <tbody>
                        <?php
                    $i = 0;
                    $date = '';
                    foreach ($res as $data) {
                        $nettotal_bill_net_amount=floatval($data->total_amount)-$data->total_refundamount;
                        if ($date != $data->date) {
                            $i = 0;
                            $date = $data->date;
                            $total_bill_net_amount = 0.0;
                    ?>
                        <tr class="headerclass"
                            style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                            <th colspan="7" class="common_td_rules">{{ date('M-d-Y', strtotime($date)) }}</th>
                        </tr>
                        <tr>
                            <td class="common_td_rules">{{ $data->uhid }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->bill_no }}</td>
                            <td class="common_td_rules">{{ $data->name }}</td>
                            <td class="common_td_rules">{{ $data->total_amount }}</td>
                            <td class="common_td_rules">{{ $data->total_refundamount }}</td>
                            <td class="common_td_rules">{{ $nettotal_bill_net_amount }}</td>
                        </tr>
                        <?php
                        } else {
                        ?>
                        <tr>
                            <td class="common_td_rules">{{ $data->uhid }}</td>
                            <td class="common_td_rules">{{ $data->patient_name }}</td>
                            <td class="common_td_rules">{{ $data->bill_no }}</td>
                            <td class="common_td_rules">{{ $data->name }}</td>
                            <td class="common_td_rules">{{ $data->total_amount }}</td>
                            <td class="common_td_rules">{{ $data->total_refundamount }}</td>
                            <td class="common_td_rules">{{ $nettotal_bill_net_amount }}</td>
                        </tr>
                        <?php

                        }

                        $total_bill_net_amount += floatval($nettotal_bill_net_amount);
                        if ($i == $date_cnt[$date]) {
                        ?>
                        <tr class="bg-info" style="height: 30px;">
                            <th class="common_td_rules" colspan="6" style="text-align: left;">Total</th>
                            <th class="td_common_numeric_rules"><?= $total_bill_net_amount ?></th>
                        </tr>
                        <?php
                            $fulltotal_bill_net_amount += $total_bill_net_amount;
                        }
                        $i++;
                    }
                    ?>
                        <tr class="" style=" height: 30px;">
                            <th class="common_td_rules" colspan="6" style="text-align: left;">Net Total</th>
                            <th class="td_common_numeric_rules"><?= $fulltotal_bill_net_amount ?></th>
                        </tr>
                    @else
                        <tr>
                            <td colspan="7" style="text-align: center;">No Results Found!</td>
                        </tr>
                @endif

                </tbody>
            </table>
            <?php } ?>
        </font>
    </div>
</div>
