
    <div class="row" style="padding: 10px;">
        @if(sizeof($res)>0)

        <div class="col-md-12" style="border-bottom: 1px solid beige;
        margin-bottom: 10px;
        height: 30px;">
            <h4 class="text-center no-margin"><b>{{$title}}</b></h4>
        </div>
        <div class="col-md-12" style="height: 44px;
        text-align: right;">

            <a class="btn btn-lg bg-primary" onclick="approve_medications();"><i id="approve_spin" class="fa fa-check" aria-hidden="true"></i> Approve</a>
            <a class="btn btn-lg bg-green"><i class="fa fa-repeat" aria-hidden="true"></i> Reorder</a>
            {{--  <a class="btn btn-lg bg-info"><i class="fa fa-calendar" aria-hidden="true"></i> Reschedule</a>  --}}
            {{--  <a class="btn btn-lg bg-orange" onclick="stop_medications(1);"><i class="fa fa-stop" aria-hidden="true"></i> Stop</a>  --}}
            <a class="btn btn-lg bg-red" onclick="stop_medications(2);"><i class="fa fa-window-close" aria-hidden="true"></i> Discontinue</a>
            <a class="btn btn-lg" style="background-color: green;color: white;" onclick="add_new_medications();"><i id="add_new_medications" class="fa fa-plus" aria-hidden="true"></i> Add New Medicine</a>
            {{--  <a class="btn btn-lg" style="background-color: rgb(110, 27, 82);color: white;" onclick="add_outside_medications();"><i class="fa fa-external-link" aria-hidden="true"></i>Outside Medicine</a>  --}}
        </div>

        <div class="col-md-12" style="height: 30px;
        border-top: 1px solid beige;padding-top:10px;margin-bottom:10px;">
            <div class="col-md-10"><b>Medicine</b></div>
            <div class="col-md-2">
                <div class="checkbox checkbox-primary chekbox_table_lab">
                    <input id="select_all" class="styled 6" type="checkbox" data-id="select_all" value="select_all">
                    <label for="select_all" class="select_all">Select All</label>
                </div>
            </div>
        </div>


            @php
            $i = 1;
            @endphp
            @foreach($res as $data)
                <div class="col-md-12">
                    <div class="col-md-7">
                        <table style="width: 100%">
                            <tr>
                                <td style="margin:0">
                                    <table class="table no-margin no-border table-bordered table_sm" style="">

                                            <tr data-toggle="collapse" data-target=".demo_{{$i}}" style="height: 30px;background-color:#f6f4eb">
                                                <td colspan="4" class="columcontain" style="text-align: left;padding:5px !important">
                                                    <span>Medicine : <b>{{$data->item_desc}}</b></span>&nbsp;&nbsp;
                                                    <span>Dose : <b>{{$data->dose}}</b></span>&nbsp;&nbsp;
                                                    <span>Frequency : <b>{{$data->frequency}}</b></span>&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr class="demo_{{$i}} collapse">
                                                <td>Route</td>
                                                <td colspan="3">Directions</td>
                                            </tr>
                                            <tr class="demo_{{$i}} collapse">
                                                <td class=""><b>{{$data->route}}</b></td>
                                                <td colspan="3"><b>{{$data->notes}}</b></td>
                                            </tr>
                                            <tr class="demo_{{$i}} collapse">
                                                <td>Start At</td>
                                                <td colspan="2">Doctor Name</td>
                                                <td>Nurse Name</td>
                                            </tr>
                                            <tr class="demo_{{$i}} collapse">
                                                <td class=""><b>{{date(\WebConf::getConfig('date_format_web'),strtotime($data->start_date))}}</b></td>
                                                <td colspan="2"><b>
                                                    {{$data->doctor_name}}
                                                </b></td>
                                                <td class=""><b></b></td>
                                            </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-5">
                        <div class="col-md-4">
                            @if($data->order_status ==1)
                            <span style="background-color: blue;" class="badge badge-primary">Approved</span>
                            @endif
                        </div>
                        <div class="col-md-4">
                            @if($data->stop_flag ==1)
                            <span style="background-color: rgb(255, 136, 0);" class="badge badge-primary">Stoped</span>
                            @elseif($data->stop_flag ==2)
                            <span style="background-color: rgb(255, 17, 0);" class="badge badge-primary">Discontinue</span>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <ul class="select_button no-padding" >
                                <li data-value="{{$data->id}}">
                                    <i class="fa fa-check-circle"></i><span>select</span>
                                    <input type="hidden" name="daily_approve[]" data-frequency="{{$data->frequency_value}}" id="daily_approve_{{$data->id}}" value="{{$data->id}}"/>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @php
                    $i++;
                @endphp
            @endforeach

        @else
        <div class="col-md-12"><label>No Medications Found!</label></div>
        @endif
    </div>
