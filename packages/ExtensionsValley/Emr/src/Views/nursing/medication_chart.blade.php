<style>
    .timepickerclass{
        font-weight: 600;
}
.timepickerclass_default{
color: #a8a8a8 !important;
  }

  .blink {
  border: none;
  color: #FFFFFF;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-animation: glowing 1500ms infinite;
  -moz-animation: glowing 1500ms infinite;
  -o-animation: glowing 1500ms infinite;
  animation: glowing 1500ms infinite;
}
@-webkit-keyframes glowing {
  0% { background-color: #f18842; -webkit-box-shadow: 0 0 3px #f18842; }
  50% { background-color: #f5f128; -webkit-box-shadow: 0 0 10px #f5f128; }
  100% { background-color: #f18842; -webkit-box-shadow: 0 0 3px #f18842; }
}

@-moz-keyframes glowing {
  0% { background-color: #f18842; -moz-box-shadow: 0 0 3px #f18842; }
  50% { background-color: #f5f128; -moz-box-shadow: 0 0 10px #f5f128; }
  100% { background-color: #f18842; -moz-box-shadow: 0 0 3px #f18842; }
}

@-o-keyframes glowing {
  0% { background-color: #f18842; box-shadow: 0 0 3px #f18842; }
  50% { background-color: #f5f128; box-shadow: 0 0 10px #f5f128; }
  100% { background-color: #f18842; box-shadow: 0 0 3px #f18842; }
}

@keyframes glowing {
  0% { background-color: #f18842; box-shadow: 0 0 3px #f18842; }
  50% { background-color: #f5f128; box-shadow: 0 0 20px #f5f128; }
  100% { background-color: #f18842; box-shadow: 0 0 3px #f18842; }
}

.popover {
    background: rgb(255, 241, 195);
}
.popover-content{text-align: center;}

.badge{
   padding: 0px ​5px !important;
}

</style>

<?php
    $hide_ns_medication_chart = sizeof($res)>0 ? 0 : 1;
?>
<input type="hidden" id="hide_ns_medication_chart" value="{{$hide_ns_medication_chart}}">
@if(sizeof($res)>0)

    <button class="btn bg-blue" style="float:right;margin-top: -10px; margin-right: -2px;" type="button" onclick="showMedicationChart();">
        <i class="fa fa-refresh"></i>
    </button>

@foreach($res as $data)
<div class="col-md-12 box no-border no-margin" style="margin:4px !important;">
    <div class="col-md-12 box-body clearfix">
        <table class="table no-margin no-border table_sm" style="border:1px solid;font-size:12px;">
            <tbody>
                <tr data-toggle="collapse" data-target=".demo" style="border:1px solid #f1eaea">

                    <td style="text-align: left !important; width:35%;padding-top:5px !important;" class="">

                        <button style="width: 17px;height: 17px;text-align: center;padding: 0px;float: left;clear:right;" title="Detail View" type="button" data-toggle="collapse" data-target=".demo{{$data->medication_detail_id}}" class="btn btn-sm bg-green" >
                            <i class="fa fa-arrow-down" aria-hidden="true"></i>
                        </button> &nbsp;
                        @php
                         $current_date = '';
                         $datediff = 0;
                         $date_string = 'day';
                         $current_date = strtotime(date('Y-m-d H:i:s'));
                         $start_at = strtotime($data->start_at);
                         $datediff = $current_date - $start_at;
                         $datediff =  abs(round($datediff / 86400));
                         if($datediff > 1){
                            $date_string = 'days';
                         }
                        @endphp

                        <span style="font-size:13px;">{{trim($data->medicine)}} ({{$data->generic_name}})</span>
                        <span class="badge bg-purple" style="padding: 1px !important;float:right">{{$datediff}} {{$date_string}}</span>

                    </td>
                    <td style="width:25%">
                        <h6> &nbsp;|&nbsp; <div class="badge bg-blue" style="padding: 1px !important;">{{$data->quantity}}</div>
                             &nbsp;|&nbsp; <div class="badge bg-orange" style="padding: 1px !important;">{{$data->frequency}}</div>
                            <b> &nbsp;|&nbsp; <b>{{$data->notes}}</b>
                             &nbsp;|&nbsp; <b>{{$data->route}}</b>
                        </h6>
                    </td>
                    <td style="text-align:right;width:40%;padding-top:5px !important;">

                            @php
                                $current_datetime = date('Y-m-d H:i:s');
                                $next_medication_time = \DB::table('patient_medication_chart')
                                                        ->whereNull('deleted_at')
                                                        ->where('medication_detail_id',$data->medication_detail_id)->max('next_expected_time');
                                if(empty($next_medication_time)){
                                    $next_medication_time = $current_datetime;
                                }

                                $next_med_time = date('H:i',strtotime($next_medication_time));
                                $next_med_date = date('M-d-Y',strtotime($next_medication_time));

                                $from_time = strtotime($next_medication_time);
                                $to_time = strtotime($current_datetime);

                                $minutes_difference = round(abs($from_time - $to_time) / 60,2);
                                $minutes_difference = round($minutes_difference);

                                $medication_alert_duration = 0;
                                $medication_alert_duration = \WebConf::getConfig('medication_alert_duration');

                                $warning_msg = '';
                                $warning_flag = 0;
                                $warning_class = "";
                                $warning_title = "";

                                if($minutes_difference < $medication_alert_duration){
                                    $warning_title = "<b><i class='fa fa-lightbulb-o' aria-hidden='true'></i><span style='margin-left:45px;'>Its Medication Time!</span></b>";
                                    $warning_msg = "<span style='text-align: center;'>The time has come to give the medication.<br>Medication Time<br>".date('M-d-Y h:i A',strtotime($next_medication_time))."</span>";
                                    $warning_class = 'bg-warning';
                                    $warning_flag = 1;

                                }elseif($minutes_difference< 0){
                                    $warning_title = "<b><i class='fa fa-lightbulb-o' aria-hidden='true'></i> <span style='margin-left:45px;'>Medication Time Exceeded!</span></b>";
                                    $warning_msg = "<span style='text-align: center;'> Medication given time has been exceeded.<br>Medication Time<br>".date('M-d-Y h:i A',strtotime($next_medication_time))."</span>";
                                    $warning_flag = 1;
                                    $warning_class = 'bg-red';
                                }

                            @endphp

                            <div class="md-form col-md-3 no-padding" style="width:155px;">

                                @if($warning_flag == 1 && $data->stop_flag !=1)
                                    <button data-toggle="popover" title="{{$warning_title}}" data-content="{{$warning_msg}}" data-container="body" data-html="true" class="blink btn btn-sm {{$warning_class}}" style="float:left;clear:right;margin-right:5px;font-size:12px !important;margin-top:-9px;border-radius:40px;">
                                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                                    </button>
                                @endif


                                <input type="text" @if($data->stop_flag ==1) disabled="disabled"@endif value="{{$next_med_date}}" id="given_date_{{$data->medication_detail_id}}" name="given_date[]" class="form-control datepicker timepickerclass" data-attr="date" placeholder="Date" style="width:70%; @if($warning_flag == 0)margin-left: 30px !important; @endif">
                            </div>

                            {{-- <div class="md-form col-md-4" style="width:110px;">
                                <input type="time"  @if($data->stop_flag ==1) disabled="disabled"@endif name="given_time[]" id="given_time_{{$data->medication_detail_id}}"
                                onchange="addMedicineGiven('{{$data->medication_detail_id}}','{{$data->frequency_value}}');" value="{{$next_med_time}}"
                                 class="form-control timepickerclass_default timepickerclass">
                            </div> --}}

                            <div class="md-form col-md-4" style="width:110px;">
                                <input type="text"
                                @if($data->stop_flag ==1) disabled="disabled"@endif
                                name="given_time[]" id="given_time_{{$data->medication_detail_id}}"
                                onchange="addMedicineGiven('{{$data->medication_detail_id}}','{{$data->frequency_value}}','{{$data->frequency}}');" value="{{$next_med_time}}"
                                class="form-control intake timepicker timepickerclass" data-attr="date" placeholder="Time">
                            </div>

                            <div class="md-form col-md-1 no-padding">
                                <button class="btn bg-primary" type="button" name="btn_medicine_given[]" id="btn_medicine_given_{{$data->medication_detail_id}}" style="margin-top:0px;" @if($data->stop_flag ==1) disabled @endif
                                    onclick="addMedicineGiven('{{$data->medication_detail_id}}','{{$data->frequency_value}}','{{$data->frequency}}');">
                                    <i class="fa fa-medkit" id="fa-medicine_given_{{$data->medication_detail_id}}" aria-hidden="true"></i>
                                </button>
                            </div>



                            <div class="col-md-4" style="width:130px;">

                                    @if($data->stop_flag ==1)
                                    <span title="Stopped at: {{date('M-d-Y h:i A',strtotime($data->stop_date))}}" style="color:red"><h7><b>Stopped</b></h7></span>
                                    @else
                                        @if($data->frequency != 'STAT')
                                            <button title="Stop Medicine" type="button" class="btn btn-sm bg-orange " onclick="stopMedicine('{{$data->medication_detail_id}}');">
                                            <i class="fa fa-pause-circle" aria-hidden="true"></i>
                                            </button>
                                        @endif
                                    @endif

                                        {{--  <button title="Discontinue Medicine" type="button" class="btn btn-sm bg-red " onclick="disContinueMedicine('{{$data->medication_detail_id}}');" >
                                            <i class="fa fa-close" aria-hidden="true"></i>
                                        </button>  --}}

                            </div>
                    </td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td><b>Doctor Name</b></td>
                    <td><b>Start At</b></td>
                    <td><b>Stop At</b></td>

                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}" style="border-bottom:1px solid #f1eaea;height:30px;">
                    <td class="columcontain">{{$data->approved_dr}}</td>
                    <td class="columcontain">
                        {{date(\WebConf::getConfig('datetime_format_web'),strtotime($data->start_at))}}
                    </td>
                    <td class="columcontain">
                        @if($data->stop_flag == 1)
                            {{date(\WebConf::getConfig('datetime_format_web'),strtotime($data->stop_date))}}
                        @endif
                    </td>

                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}"><td colspan='3'>&nbsp;</td></tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td colspan="3" style="padding-top:10px ​!important;"><b>Dosage given at</b></td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td colspan="3">
                    <div class="col-md-12" id="medication_given_container_{{$data->medication_detail_id}}" style="padding-top:5px;">
                        @php
                            // echo '<pre>';
                            // print_r($resdata);
                            $i =1;
                            $medicine_change_status = '';
                        @endphp
                        @foreach($resdata as $presdata)
                            @if($medicine_change_status != $presdata->medication_detail_id)
                                @php $i = 1; @endphp
                            @endif
                            @if($presdata->medication_detail_id == $data->medication_detail_id)

                                @php
                                    if($i==1){
                                        $append_str = 'st';
                                    }elseif($i==2){
                                        $append_str = 'nd';
                                    }elseif($i==3){
                                        $append_str = 'rd';
                                    }else{
                                        $append_str = 'th';
                                    }
                                @endphp

                                @php
                                    $given_date = date('M-d-Y', strtotime($presdata->created_at));
                                    $given_time = date('h:i A',strtotime($presdata->created_at));
                                    $given_time_picker = date('H:i',strtotime($presdata->created_at));
                                @endphp

                                <div id="chart_id_{{$presdata->id}}" class="col-md-2 nopadding chart_class">
                                    <div class="col-md-10">
                                        <div class="md-form" style=" background-color:#d2fff4;color:#3a3a42 !important;text-align:left;font-weight:600;padding:5px;">
                                            {{$given_date}} </br>
                                            {{$given_time}}
                                        </div>
                                        <label style="margin-top: 2px;">{{$i}}{{$append_str}} Course</label>
                                    </div>
                                    <div class="col-md-2" style="text-align:left;">

                                        <button type="button" onclick="editGivenMedication('{{$presdata->id}}','{{$given_date}}','{{$given_time_picker}}');" class="btn-sm bg-primary medication_stop" style="height: 20px;width:25px;margin-left: -10px;"><i class="fa fa-edit"></i></button>

                                        <button type="button" onclick="deleteGivenMedication('{{$presdata->id}}');" class="btn-sm bg-red medication_stop" style="height: 20px;width:25px;margin-left: -10px;"><i class="fa fa-trash"></i></button>

                                    </div>
                                </div>

                            @endif

                            @php $i++; $medicine_change_status = $presdata->medication_detail_id; @endphp
                        @endforeach

                    </div>
                    <td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td colspan="3">&nbsp;</td>
                </tr>


            </tbody>
        </table>
    </div>

</div>
@endforeach
@else
<div class="col-md-12" style="margin-top: 10px;">
    <label>No Approved Medications Found</label>
</div>
@endif
