<div class="tab-pane fade active" style="margin-top: 5px;" id="BO_{{$patient_id}}_{{ $baby_count }}_tab" role="tabpanel" aria-labelledby="nav-BO_{{$patient_id}}_{{ $baby_count }}-tab">
    <div class="box no-border no-margin" id="BO_{{$patient_id}}_{{ $baby_count }}_div">
        <div class="col-md-6">
            <div class="mate-input-box">
                <label for="" class="red">Baby Name*</label>
                <div class="clearfix"></div>
                <input disabled autocomplete="off" type="text" class="form-control reset_baby_details add_baby_name " id="add_baby_name{{ $baby_count }}" value="B/O {{$mother_name}} {{ $baby_count }}"
                    >
            </div>
        </div>
        <div class="col-md-6">
            <div class="mate-input-box">
                <label for="" class="red">Gender</label>
                <div class="clearfix"></div>
                <select name="" class="form-control reset_baby_details add_baby_gender " id="add_baby_gender{{ $baby_count }}" >
                    <option value="">Select</option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mate-input-box">
                <label for="" class="red">Birth Date</label>
                <div class="clearfix"></div>
                <input autocomplete="off" type="text" value="{{ date('M-d-Y') }}"
                    class="form-control baby_datepicker reset_baby_details add_baby_birth_date " id="add_baby_birth_date{{ $baby_count }}" >
            </div>
        </div>
        <div class="col-md-6">
            <div class="mate-input-box">
                <label for="" class="red">Birth Time</label>
                <div class="clearfix"></div>
                <input autocomplete="off" type="text" value="{{ date('H:i') }}"
                    class="form-control baby_timepicker reset_baby_details add_baby_birth_time " id="add_baby_birth_time{{ $baby_count }}">
            </div>
        </div>

        <div class="col-md-6">
            <div class="mate-input-box">
                <label for="" class="red">Birth Weight(in Kg)</label>
                <div class="clearfix"></div>
                <input autocomplete="off" type="text"
                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                    class="form-control reset_baby_details add_baby_birth_weight_kg " id="add_baby_birth_weight_kg{{ $baby_count }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="mate-input-box">
                <label for="" class="red">Birth Weight(in Pound)</label>
                <div class="clearfix"></div>
                <input  autocomplete="off" type="text"
                    class="form-control reset_baby_details add_baby_birth_weight_pound " id="add_baby_birth_weight_pound{{ $baby_count }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="mate-input-box">
                <label for="" class="red">Birth Length(in cm)</label>
                <div class="clearfix"></div>
                <input autocomplete="off"
                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                    type="text" class="form-control reset_baby_details add_baby_birth_length_cm " id="add_baby_birth_length_cm{{ $baby_count }}"
                    >
            </div>
        </div>
        <div class="col-md-6">
            <div class="mate-input-box">
                <label for="" class="red">Birth Length(in inch)</label>
                <div class="clearfix"></div>
                <input  autocomplete="off" type="text"
                    class="form-control reset_baby_details add_baby_birth_length_inch " id="add_baby_birth_length_inch{{ $baby_count }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="mate-input-box">
                <label for="" class="red">Head Circum.(in cm)</label>
                <div class="clearfix"></div>
                <input autocomplete="off"
                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                    type="text" class="form-control reset_baby_details add_baby_head_cer_cm " id="add_baby_head_cer_cm{{ $baby_count }}"
                    >
            </div>
        </div>
        <div class="col-md-6">
            <div class="mate-input-box">
                <label for="" class="red">Head Circum.(in inch)</label>
                <div class="clearfix"></div>
                <input  autocomplete="off" type="text"
                    class="form-control reset_baby_details add_baby_head_cer_inc " id="add_baby_head_cer_inc{{ $baby_count }}">
            </div>
        </div>
        <div class="col-md-6">
            @php
                $modes = \DB::table('mode_of_delivery')
                    ->where('status', 1)
                    ->select('name', 'id')
                    ->get();
            @endphp
            <div class="mate-input-box">
                <label for="" class="red">Mode Of Delivery</label>
                <div class="clearfix"></div>
                <select name="" class="form-control reset_baby_details mode_of_delivery " id="mode_of_delivery{{ $baby_count }}">
                    <option value="">Select</option>
                    @foreach ($modes as $mode)
                        <option value="{{ $mode->id }}">{{ $mode->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mate-input-box">
                <label for="" class="red">Bed Preference</label>
                <div class="clearfix"></div>
                <select name="" class="form-control reset_baby_details bed_preference" onchange="setBedList(this)" id="bed_preference{{ $baby_count }}">
                    <option value="">Select</option>
                    <option value="1">NICU</option>
                    <option value="2">Cradle Bed</option>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mate-input-box" id="bed_selection_box">
                <label for="" class="red">Bed Selection</label>
                <div class="clearfix"></div>
                <select name="" class="form-control reset_baby_details bed_selection" id="bed_selection{{ $baby_count }}">
                    <option value="">Select</option>

                </select>
            </div>
        </div>
        <div class="col-md-6">
            @php
                $docs = \DB::table('doctor_master')
                    ->where('status', 1)
                    ->select('doctor_name as name', 'id')
                    ->orderby('name')->get();
            @endphp
            <div class="mate-input-box">
                <label for="" class="red">Consulting Doctor</label>
                <div class="clearfix"></div>
                <select name="" class="form-control reset_baby_details baby_consulting_doc" id="baby_consulting_doc{{ $baby_count }}">
                    <option value="">Select</option>
                    @foreach ($docs as $doc)
                        <option value="{{ $doc->id }}">{{ $doc->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>