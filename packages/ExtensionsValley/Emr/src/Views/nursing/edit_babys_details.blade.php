@php
    $baby_count=1;
@endphp
@foreach ($mother_data as $data)
<div class="col-md-12" style="margin-top: 35px;">
    <div class="col-md-6">
        <div class="mate-input-box">
            <label for="" class="red">Baby Name*</label>
            <div class="clearfix"></div>
            <input disabled autocomplete="off" type="text" class="form-control reset_baby_details add_baby_name"
                id="add_baby_name{{ $baby_count }}" value="{{$data->baby_name}}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="mate-input-box">
            <label for="" class="red">Gender</label>
            <div class="clearfix"></div>
            <select name="" class="form-control reset_baby_details add_baby_gender"
                id="add_baby_gender{{ $baby_count }}">
                <option @if($data->gender=='') selected @endif value="">Select</option>
                <option @if($data->gender==1) selected @endif value="1">Male</option>
                <option @if($data->gender==2) selected @endif value="2">Female</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="mate-input-box">
            <label for="" class="red">Birth Date</label>
            <div class="clearfix"></div>
            <input autocomplete="off" type="text" value="{{ $data->birth_date }}"
                class="form-control baby_datepicker reset_baby_details add_baby_birth_date"
                id="add_baby_birth_date{{ $baby_count }}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="mate-input-box">
            <label for="" class="red">Birth Time</label>
            <div class="clearfix"></div>
            <input autocomplete="off" type="text" value="{{ $data->birth_time }}"
                class="form-control baby_timepicker reset_baby_details add_baby_birth_time"
                id="add_baby_birth_time{{ $baby_count }}">
        </div>
    </div>

    <div class="col-md-6">
        <div class="mate-input-box">
            <label for="" class="red">Birth Weight(in Kg)</label>
            <div class="clearfix"></div>
            <input autocomplete="off" type="text"
                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                class="form-control reset_baby_details add_baby_birth_weight_kg" value="{{ number_format($data->weight_kg,2) }}"
                id="add_baby_birth_weight_kg{{ $baby_count }}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="mate-input-box">
            <label for="" class="red">Birth Weight(in Pound)</label>
            <div class="clearfix"></div>
            <input  autocomplete="off" type="text"
                class="form-control reset_baby_details add_baby_birth_weight_pound"  value="{{ number_format($data->weight_pound,2) }}"
                id="add_baby_birth_weight_pound{{ $baby_count }}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="mate-input-box">
            <label for="" class="red">Birth Length(in cm)</label>
            <div class="clearfix"></div>
            <input autocomplete="off"
                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                type="text" class="form-control reset_baby_details add_baby_birth_length_cm " value="{{ number_format($data->length_cm,2) }}"
                id="add_baby_birth_length_cm{{ $baby_count }}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="mate-input-box">
            <label for="" class="red">Birth Length(in inch)</label>
            <div class="clearfix"></div>
            <input  autocomplete="off" type="text"
                class="form-control reset_baby_details add_baby_birth_length_inch " value="{{ number_format($data->length_in,2) }}"
                id="add_baby_birth_length_inch{{ $baby_count }}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="mate-input-box">
            <label for="" class="red">Head Circum.(in cm)</label>
            <div class="clearfix"></div>
            <input autocomplete="off"
                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                type="text" class="form-control reset_baby_details add_baby_head_cer_cm " value="{{ number_format($data->hd_cr_cm,2) }}" id="add_baby_head_cer_cm{{ $baby_count }}"
                >
        </div>
    </div>
    <div class="col-md-6">
        <div class="mate-input-box">
            <label for="" class="red">Head Circum.(in inch)</label>
            <div class="clearfix"></div>
            <input  autocomplete="off" type="text"
                class="form-control reset_baby_details add_baby_head_cer_inc " value="{{ number_format($data->hd_cr_inc,2) }}" id="add_baby_head_cer_inc{{ $baby_count }}">
        </div>
    </div>
    <div class="col-md-6">
        @php
            $modes = \DB::table('mode_of_delivery')
                ->where('status', 1)
                ->select('name', 'id')
                ->get();
        @endphp
        <div class="mate-input-box">
            <label for="" class="red">Mode Of Delivery</label>
            <div class="clearfix"></div>
            <select name="" class="form-control reset_baby_details mode_of_delivery"
                id="mode_of_delivery{{ $baby_count }}">
                <option value="">Select</option>
                @foreach ($modes as $mode)
                    <option @if($data->type_of_delivery==$mode->id) selected @endif value="{{ $mode->id }}">{{ $mode->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="mate-input-box">
            <input type="hidden" id="bed_preference_hidden" value="{{ $data->is_nicu_dummy }}">

            <label for="" class="red">Bed Preference</label>
            <div class="clearfix"></div>
            <select name="" class="form-control reset_baby_details bed_preference " disabled
                onchange="setBedList(this)" id="bed_preference{{ $baby_count }}">
                <option @if($data->is_nicu_dummy=='') selected @endif value="">Select</option>
                <option @if($data->is_nicu_dummy==1) selected @endif value="1">NICU</option>
                <option @if($data->is_nicu_dummy==2) selected @endif value="2">Cradle Bed</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        @php
        $bed_master = \DB::table('bed_master')
            ->select('bed_name', 'id')
            ->get();
    @endphp
        <div class="mate-input-box" id="bed_selection_box">
            <input type="hidden" id="bed_id_hidden" value="{{ $data->bed_id }}">
            <label for="" class="red">Bed Selection</label>
            <div class="clearfix"></div>
            <select name="" class="form-control reset_baby_details bed_selection" disabled
                id="bed_selection{{ $baby_count }}">
                <option value="">Select</option>
                @foreach ($bed_master as $mode)
                <option value="{{ $mode->id }}">{{ $mode->bed_name }}</option>
            @endforeach
              
            </select>
        </div>
    </div>
    <div class="col-md-6">
        @php
            $docs = \DB::table('doctor_master')
                ->where('status', 1)
                ->select('doctor_name as name', 'id')
                ->get();
        @endphp
        <div class="mate-input-box">
            <label for="" class="red">Consulting Doctor</label>
            <div class="clearfix"></div>
            <select name="" class="form-control reset_baby_details baby_consulting_doc" disabled
                id="baby_consulting_doc{{ $baby_count }}">
                <option value="">Select</option>
                @foreach ($docs as $doc)
                    <option @if($data->consulting_doctor==$doc->id) selected @endif value="{{ $doc->id }}">{{ $doc->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

   </div> 
@endforeach
