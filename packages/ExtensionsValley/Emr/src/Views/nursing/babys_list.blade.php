<div>
    <table class='table table-condensed table_sm table-col-bordered' style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;font-size: 12px;;  " width="100%" border="1" cellspacing="0" cellpadding="0">
        <thead>
            <tr class="headerclass"
                style="background:#5d9b7b;color:white;border-spacing: 0 1em;font-family:sans-serif">
                <th width='3%' style="white-space:pre;text-align: center;">#</th>
                <th width='13%' style="white-space:pre;text-align: center;">Mother UHID</th>
                <th width='10%'style="white-space:pre;text-align: center;">Mother Name</th>
                <th width='10%' style="white-space:pre;text-align: center;">Baby UHID</th>
                <th width='10%' style="white-space:pre;text-align: center;">Baby Name</th>
                <th width='8%' style="white-space:pre;text-align: center;">Gender</th>
                <th width='14%' style="white-space:pre;text-align: center;">Birth date time</th>
                <th width='8%' style="white-space:pre;text-align: center;">Weight</th>
                <th width='10%' style="white-space:pre;text-align: center;">Length</th>
                <th width='15%'style="white-space:pre;text-align: center;">Head Circumference</th>
            </tr>


        </thead>
        
        <tbody>
            @if (count($mother_data) != 0)
            @foreach ($mother_data as $data)
                <tr onclick="updateBbyDetails({{ $data->table_id }})" style="cursor: pointer">
                    <td class="td_common_numeric_rules" style="text-align:right">{{ $loop->iteration }}.</td>
                    <td class="common_td_rules" style="text-align: left;white-space: pre;">{{ @$data->mother_uhid ? $data->mother_uhid : '' }}</td>
                    <td class="common_td_rules" style="text-align: left;white-space: pre;">{{ @$data->mother_name ? strtoupper($data->mother_name) : '' }}</td>
                    <td class="common_td_rules" style="text-align: left;white-space: pre;">{{ @$data->baby_uhid ? $data->baby_uhid : '' }}</td>
                    <td class="common_td_rules" style="text-align: left;white-space: pre;">{{ @$data->baby_name ? $data->baby_name : '' }}</td>
                    <td class="common_td_rules" style="text-align: left;white-space: pre;">{{ @$data->gender ? $data->gender : '' }}</td>
                    <td class="common_td_rules" style="text-align: left;white-space: pre;">{{ @$data->birth_date_time ? date('M-d-Y H:i A',strtotime($data->birth_date_time)) : '' }}</td>
                    <td class="common_td_rules" style="text-align: left;white-space: pre;" title="{{ @$data->baby_weight ? number_format($data->baby_weight,2).' lb' : '' }}">{{  @$data->baby_weight ? number_format($data->baby_weight / 2.2,2).' kg' : '' }}</td>
                    <td class="common_td_rules" style="text-align: left;white-space: pre;" title="{{ @$data->baby_length ? number_format($data->baby_length,2).' in' : '' }}">{{  @$data->baby_length ? number_format($data->baby_length / 0.4,2).' cm' : '' }}</td>
                    <td class="common_td_rules" style="text-align: left;white-space: pre;"title="{{ @$data->baby_head_circumference ? $data->baby_head_circumference.'in' : '' }}">{{  @$data->baby_head_circumference ? number_format($data->baby_head_circumference / 0.4,2).' cm'  : '' }}</td>
                  
                 
                </tr>


            @endforeach
        @else
            <tr>
                <td colspan="11" style="text-align: center">
                    No Result Found
                </td>
            </tr>
        @endif
        </tbody>
    </table> 
</div>