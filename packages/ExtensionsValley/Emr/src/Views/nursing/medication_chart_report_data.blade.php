 <hr/>
<style>
    .custom_floatlabel{
        font-size:13px;
        color:rgb(5, 5, 109);
    }
    .course_container {
    padding: 2px;
    color: #FFF;
    border-radius: 3px;
    float: left;
    margin: 2px;
}
.tooltip {
    position: absolute;
    z-index: 99999;
}
.btn.focus,.btn:hover{
    color:white;
}
.tooltip-inner {background-color: rgb(245, 43, 184);}


.table-container{ width: 100%;
                     /*overflow: scroll;*/
                     }
                    table th, table td {
                    white-space: nowrap;
                    padding: 10px 20px;
                    font-family: Arial;
                    }
                    .table-container table tr th:first-child, .table-container table td:first-child{
                    position: sticky;
                    width: 100px;
                    left: 0;
                    z-index: 10;
                    background: #fff;
                    }
                    .table-container table tr th:first-child{
                    z-index: 11;
                    }
                    .table-container table tr th{
                    position: sticky;
                    top: 0;
                    z-index: 9;
                    background: #fff;
                    }
label{
    text-overflow:ellipsis;
    white-space :nowrap;
    overflow:hidden;
}
</style>
<div class="col-md-12 box no-border no-margin">
          <body class="">
            <div class="container-fluid">
             <div class="row">
               <div class="col-md-12 bg-primary" style="margin-bottom:5px;background-color:#b9d4ea;color:black"">
                 <h4 class="text-center"><b>MEDICATION SHEET</b></h4>
               </div>
               <div class="clearfix"></div>
               <div class="h10"></div>
               <div class="col-md-12">
                <div class="col-md-6">
                  <div class="col-md-4">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">Name</label>
                      <label class="medication_content">{{$patient_details[0]->patient_name}}</label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">Age/Gender</label>
                      <label class="medication_content">
                          {{$patient_details[0]->age}}/{{$patient_details[0]->gender}}
                      </label>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">UHID</label>
                      <label class="medication_content">
                        {{$patient_details[0]->uhid}}
                    </label>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">Admission No.</label>
                      <label class="medication_content">
                        {{$patient_details[0]->admission_no}}
                    </label>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                  <div class="h10"></div>
                  <div class="col-md-4">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">Blood Group</label>
                        @php
                            $blood_group = ($patient_details[0]->blood_group != '') ? $patient_details[0]->blood_group:' ';
                        @endphp
                      <label class="medication_content">
                           {{$blood_group}}
                      </label>

                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">Allergies</label>
                      <label class="medication_content"></label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">Weight</label>
                      <label class="medication_content"> {{$patient_details[0]->vital_value}} Kg</label>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="h10"></div>

                </div>

                <div class="col-md-6">
                  <div class="col-md-4">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">Doctor's Name</label>
                      <label class="medication_content"> {{$patient_details[0]->doctor_name}}</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">Department</label>
                      <label class="medication_content"> {{$patient_details[0]->speciality}}</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">OP/WARD/FLOOR</label>
                      <label title="{{$patient_details[0]->ward}}/{{$patient_details[0]->location}}" class="medication_content">
                        @php
                            $wardandroom = $patient_details[0]->ward.'/'.$patient_details[0]->location;
                        @endphp
                         {{substr($wardandroom,18)}}
                    </label>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="h10"></div>
                  <div class="col-md-4">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">Bed / Room No.</label>
                      <label class="medication_content"> {{$patient_details[0]->bed_name}}</label>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel"> Date</label>
                      <label class="medication_content">
                        {{date(\WebConf::getConfig('date_format_web'),strtotime($patient_details[0]->visit_datetime))}}
                      </label>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="custom-float-label-control">
                      <label class="custom_floatlabel">Time</label>
                      <label class="medication_content">
                        {{date('h:i A',strtotime($patient_details[0]->visit_datetime))}}
                      </label>
                    </div>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>
              <div class="h10"></div>

              <div class="clearfix"></div>
              <div class="h10"></div>
              <div class="col-md-10">&nbsp;</div>
            <div class="clearfix"></div>
            <div class="col-md-10">&nbsp;</div>
            <div class="clearfix"></div>
            @if(sizeof($static_medicine_details)>0)
              <div class="col-md-12">
                <div class="theadscroll">
                  <table class="table table-condensed table-striped theadfix_wrapper table_sm" style="padding: 15px;">
                    <thead>
                      <tr class="headergroupbg bg-primary" style="background-color:#b9d4ea;color:black">
                        <th colspan="7" style="padding-bottom:10px ​!important;">
                          <h4 class="text-center no-margin"><b>ONCE ONLY DRUGS</b></h4>
                        </th>
                      </tr>

                      <tr class="headergroupbg bg-info" style="padding-top: 5px;padding-bottom:15px;">
                        <th width="10%">Date</th>
                        <th width="10%">Time</th>
                        <th width="30%">Drug</th>
                        <th width="10%">Dose</th>
                        <th width="10%">Route</th>
                        <th width="15%" title="Name & Sign of Doctor">Doctor</th>
                        <th width="15%" title="Name & Sign of Nurse">Nurse</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach($static_medicine_details as $data)
                      <tr>
                        <td>{{date(\WebConf::getConfig('date_format_web'),strtotime($data->given_at))}}</td>
                        <td>{{date('h:i A',strtotime($data->given_at))}}</td>
                        <td>{{$data->medicine}}</td>
                        <td>{{$data->dose}}</td>
                        <td>{{$data->route}}</td>
                        <td>{{$data->approved_dr}}</td>
                        <td>{{$data->given_by}}</td>
                      </tr>
                      @endforeach

                    </tbody>
                </table>
               </div>
            </div>
            @endif
              <div class="clearfix"></div>

            <div class="clearfix"></div>

            <div class="clearfix"></div>

            <!-----  IV fluid data  ------------------------------------------------------->

            @if(sizeof($iv_fluid_details)>0)
            <div class="col-md-12">
              <table class="table table-condensed table-striped pos_rel_table table_sm" >
                <thead>
                  <tr class="bg-primary" style="background-color:#b9d4ea;color:black">
                    <th colspan="8">
                      <h4 class="text-center no-margin"><b>IV FLUIDS</b></h4>
                    </th>
                  </tr>
                  <tr class="headergroupbg bg-info">
                    <th>Date</th>
                    <th width="35%">IV Fluid</th>
                    <th>Volume</th>
                    <th>Rate/Duration</th>
                    <th>Ordered by</th>
                    <th>Time Started</th>
                    <th>Time Ended</th>
                    <th>Nurse</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($iv_fluid_details as $data)
                        <tr>
                            <td>{{date(\WebConf::getConfig('date_format_web'),strtotime($data->start_at))}}</td>
                            <td>{{$data->medicine}}</td>
                            <td>{{$data->volume}}</td>
                            <td>{{$data->rate_and_duration}}</td>
                            <td>{{$data->approved_dr}}</td>
                            <td>{{date('h:i A',strtotime($data->start_at))}}</td>
                            <td>{{date('h:i A',strtotime($data->stop_at))}}</td>
                            <td>{{$data->given_by}}</td>
                        </tr>
                    @endforeach

                </tbody>
              </table>
            </div>
            @endif

             <!-----  IV fluid data  ends--------------------------------------------------->


            <div class="col-md-12">
              <div class="clearfix"></div>
              <div class="col-md-12 bg-primary" style="margin-bottom: 5px;background-color:#b9d4ea;color:black"">
                <h4 class="text-center no-margin"><b>MEDICATION CHART</b></h4>
              </div>

               <div class="table-container theadscroll" style="position: relative;">
                @if(sizeof($saved_medication_details)>0)
                <table class="table table-condensed table-bordered theadfix_wrapper table_sm main-table">
                    <thead>
                        <tr class="headergroupbg bg-info">
                            <th class="fixed-side" scope="col" style="background-color:#d9edf7;"><b>Medicine</b></th>
                            @php
                                $actual_admission_date = $admission_date;
                                $admission_date = date("M-d-Y",strtotime($admission_date));
                            @endphp
                            <th style="width:150px !important;background-color:#d9edf7;"><b>{{$admission_date}}</b></th>
                            @while (strtotime($admission_date) <= strtotime(date('M-d-Y')))
                                @php
                                    $admission_date = date ("M-d-Y", strtotime("+1 day", strtotime($admission_date)));
                                @endphp
                                <th style="width:150px !important;background-color:#d9edf7;"><b>{{$admission_date}}</b></th>
                            @endwhile
                        </tr>
                    </thead>
                    <tbody>
                @foreach($saved_medication_details as $data)
                    <tr>
                        <td class="text-center " width="35%" scope="col">
                            <table class="table no-margin no-border  table_sm fixed-side" style="border:1px solid;font-size:12px;margin-top:3px !important">
                                <tbody>
                                    <tr data-toggle="collapse" data-target=".demo">
                                        <td colspan="4" class="columcontain" style="text-align: left">

                                            <span><b>{{$data->medicine}}</b></span>&nbsp; | &nbsp;
                                            <span><div class="badge bg-blue">{{$data->dose}}</div></span>&nbsp; | &nbsp;
                                            <span><div class="badge bg-orange">{{$data->frequency}}</div></span>&nbsp; | &nbsp;
                                            <span>{{$data->notes}}</span>
                                        </td>
                                    </tr>
                                    <span>
                                    <tr>
                                        <td style="text-align: left" class="bg-info">
                                            {{$data->approved_dr}}
                                        </td>
                                        <td style="text-align: left">
                                            Start At: {{date(\WebConf::getConfig('datetime_format_web'),strtotime($data->medication_starts_at))}}
                                            @if($data->stop_flag != '')
                                            <span style="padding:3px;float:right;border-radius:3px;background-color:rgb(240, 190, 190)">
                                                Stoped At: {{date(\WebConf::getConfig('datetime_format_web'),strtotime($data->stop_date))}}
                                            </span>

                                            @endif
                                        </td>
                                    </tr>
                                    </span>
                                </tbody>
                            </table>
                        </td>

                            @php
                                $admission_date = $actual_admission_date;
                                $admission_date = date("M-d-Y",strtotime($admission_date));
                                $last_date =  date ("M-d-Y", strtotime("+1 day", strtotime(date('M-d-Y'))));
                            @endphp


                            @while (strtotime($admission_date) <= strtotime($last_date))



                            <td style="width:150px !important;padding:4px ​!important">
                                @foreach($saved_medications_data as $loop_data)
                                    @if($data->medication_detail_id == $loop_data->medication_detail_id)
                                        @if(date('M-d-Y',strtotime($loop_data->created_at)) == $admission_date)
                                            <div class="course_container btn" style="background-color:#a2ffbe;color:black;font-size:10px" data-toggle="tooltip" title="Given by: {{$loop_data->created_by}}" data-placement="right">
                                                <b>{{date('h:i A',strtotime($loop_data->created_at))}}</b>
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            </td>

                               @php
                                   $admission_date = date ("M-d-Y", strtotime("+1 day", strtotime($admission_date)));
                               @endphp
                            @endwhile
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                <label>No medications found!</label>
                @endif
             </div>


            </div>
          </div>
        </div>
</body>
</html>


</div>
