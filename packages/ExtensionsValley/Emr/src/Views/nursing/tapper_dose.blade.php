<div class="col-md-12" style="text-align: center">
    <h4 id="tapper_medicine_name"></h4>
    <input type="hidden" name="tapper_medicine_code" id="tapper_medicine_code"/>
</div>
<div class="col-md-12">
    <button style="float:right" type="button" onclick="add_new_row_tapper_dose();" class="btn btn-sm bg-primary"><i class="fa fa-plus"></i></button>
</div>
<div class="col-md-12 theadscroll" style="">
    <table style="width:100%;" id="tapper_dose_table">
        <tr>
            <td>
                <label style="margin-top: 3px;margin-left:5px;" for="dosage_1"><b>Dose</b></label>
                <input type="text"   name="dosage[]"  class="form-control">
            </td>
            <td>
                <div class="col-md-12 padding_xs" style="text-align:center">
                     <label style="margin-top: 3px;margin-left:5px;" for="start_at_1"><b>Start</b></label>
                    <input type="text" id='start_at_1' name="start_at[]" class="form-control datepicker" data-attr="date" placeholder="Date">
                </div>

            </td>
            <td>
                <div class="col-md-12 padding_xs" style="text-align:center">
                     <label style="margin-top: 3px;margin-left:5px;" for="stop_at_1"><b>End</b></label>
                    <input type="text" id='stop_at_1' name="stop_at[]" class="form-control datepicker" data-attr="date" placeholder="Date">
                </div>

            </td>

            <td style="padding-top: 30px;">
                <button type="button"  class="btn btn-sm bg-red del-tapperdose-list-row"><i class="fa fa-trash"></i></button>
            </td>
        </tr>
    </table>
</div>
<div class="col-md-12" style="position: absolute;bottom:0px;left:0px;">
    <div class="col-md-2 pull-right">
        <button type="button" onclick="save_taaper_dose();" class="btn btn-block bg-primary">
            <i class="fa fa-save"></i> Save
        </button>
    </div>
</div>
