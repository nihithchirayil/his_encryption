<li class="nav-item" id="BO_{{$patient_id}}_{{ $baby_count }}">
    <a class="nav-link" id="BO_{{$patient_id}}_{{ $baby_count }}-tab" href="#BO_{{$patient_id}}_{{ $baby_count }}_tab"
     role="tab" aria-controls="BO_{{$patient_id}}_{{ $baby_count }}" style="padding:10px !important;"
        aria-selected="true"><strong>B/O {{$mother_name}} {{ $baby_count }} @if( $baby_count != 1) <i class="fa fa-times removeTab" 
            onclick="removeThisDViv('BO_{{$patient_id}}_{{ $baby_count }}',{{ $baby_count }})"></i> @endif</strong></a>
</li>