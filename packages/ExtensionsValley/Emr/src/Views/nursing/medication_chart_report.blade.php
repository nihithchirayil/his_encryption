@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/default/css/nurse-patient.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<style>
.block_list button {
    padding: 2px;
    font-size: 13px;
}
.block_list h5 {
    font-size: 13px;
}
.position_fixed_header{
    border-radius:0px;
}
.ajaxSearchBox {
    display:none;
    text-align: left;
    list-style: none;
    cursor: pointer;
    max-height: 350px;
    margin: 20px 0px 0px 0px;
    overflow-y: auto;
    width: auto;
    z-index: 599;
    position:absolute;
    background: #ffffff;
    border-radius: 3px;
    border: 1px solid rgba(0, 0, 0, 0.3);

}
.popover{
    max-height: 100%;
    max-width: 90%;
}
.custom_floatlabel{
    top: -26px;
}
.liHover{
         background: #cde3eb;
}
.medication_content{
    font-weight: 550 !important;
    display: block;
    border: 1px solid lightgray;
    padding: 3px 0px 5px 2px;
    border-radius: 3px;
    margin-left: -5px;
    min-height: 28.5px;
}
.medication_content_hidden{
    font-weight: 550 !important;
    display: block;
}
.col-md-4{
    padding-right: 3px;
    padding-left: 3px;
}
.custom_floatlabel {
    color: #686868;
}

.select_button > li.active {
    background: #5f89e4;
    color: #FFF;
}
.select_button > li{
    background: #aed8e7;
    color: rgb(0, 0, 0);
}
.btn-lg{
    width: 120px;
    height: 30px;
    padding-top: 6px;
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="user_id" value="{{$user_id}}">
<input type="hidden" id="is_from_op_list" value="{{@$from_op_list ?? 0}}">
<div class="right_col">
    <div class="col-md-12" style="margin:0px">
        <div class="nav-tabs-custom blue_theme_tab no-margin">
            <ul class="nav nav-tabs sm_nav text-center no-border">
                <li class="active"> <a style="padding: 4px 8px;" href="#tab1" data-toggle="tab" aria-expanded="true">Medication Chart Report</a></li>
                {{-- <li class=""> <a style="padding: 4px 8px;" href="#tab2" data-toggle="tab" aria-expanded="false">Medication Daily Approval</a></li> --}}
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in" id="tab2">

                    @include('Emr::nursing.medication_daily_approval')

                </div>
                <div class="tab-pane fade active in" id="tab1">
                    <div class="container-fluid" style="padding: 15px;">
                        <div class="row codfox_container">
                            <div class="col-md-12 box no-border no-margin">
                                <div class="col-md-3" style="padding-top:10px;">
                                    <div class="input-group custom-float-label-control">
                                        <label class="custom_floatlabel">UHID</label>
                                        <input class="form-control" value="{{$search_values['op_no']}}"  autocomplete="off" type="text" placeholder="Op.No" id="op_no" name="op_no" />
                                        <div id="OpAjaxDiv" class="ajaxSearchBox" style="z-index:999"></div>
                                    <input value="{{$search_values['op_no_hidden']}}"  type="hidden" name="op_no_hidden" value="" id="op_no_hidden">

                                </div>
                                </div>
                                <div class="col-md-4" style="padding-top:10px;">
                                    <label class="custom_floatlabel">Patient Name</label></br>
                                    <label class="medication_content_hidden" id="label_patient_name">{{$search_values['patient_name']}}</label>
                                    <input type="hidden" id="current_visit_id" name="current_visit_id" value="{{$search_values['current_visit_id']}}"/>
                                </div>
                                <div class="col-md-2 pull-right" style="padding-top:10px;">
                                    <label></label>
                                    <button class="btn bg-primary btn-block" type="button" onclick="getMedicationReport();" name="get_medication_report" id="get_medication_report">
                                        <i class='fa fa-search'></i> Search
                                    </button>
                                </div>
                            </div>

                            <div class="col-md-12 box no-border no-margin" id="medication_report_data">
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>

<!----modal popup--------------------------------------------->
<div class="modal"  tabindex="-1" role="dialog" id="modal_dialogue">
    <div class="modal-dialog modal-lg" role="document" style="width:90%">
      <div class="modal-content">
        <div class="modal-header" style="background-color:#337ab7">
          <h5 class="modal-title" id="modal_title" style="color:white">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="modal_data">

        </div>

      </div>
    </div>
  </div>

  <div class="modal"  tabindex="-1" role="dialog" id="modal_tapper_dose">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:orange">
          <h5 class="modal-title" id="modal_title" style="color:white">Tapper Dose</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="modal_data" style="height: 350px">
            @include('Emr::nursing.tapper_dose')
        </div>

      </div>
    </div>
  </div>
<!----modal popup ends---------------------------------------->


@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/medication_chart.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/medication_daily_approval.js")}}"></script>


<script type="text/javascript">

</script>
@endsection
