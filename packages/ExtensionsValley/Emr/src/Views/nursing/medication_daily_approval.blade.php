<div class="container-fluid" style="padding: 15px;">
    <div class="row codfox_container">
        <div class="box no-border">
            <div class="box-body clearfix">
                <div class="col-md-12 box no-border no-margin">
                    <div class="col-md-3" style="padding-top:10px;">
                        <div class="input-group custom-float-label-control">
                            <label class="custom_floatlabel">UHID</label>
                            <input class="form-control" value="{{$search_values['op_no']}}"  autocomplete="off" type="text" placeholder="Op.No" id="op_no_approval" name="op_no_approval" />
                            <div id="OpAjaxDiv_approval" class="ajaxSearchBox" style="z-index:999"></div>
                        <input value="{{$search_values['op_no_hidden']}}"  type="hidden" name="op_no_approval_hidden" value="" id="op_no_approval_hidden">
                    </div>
                    </div>
                    <div class="col-md-4" style="padding-top:10px;">
                        <label class="custom_floatlabel">Patient Name</label></br>
                        <label class="medication_content_hidden" id="label_patient_name_approval"></label>
                        <input type="hidden" id="current_visit_id_approval" name="current_visit_id_approval" value=""/>
                        <input type="hidden" id="encounter_id" name="encounter_id" value="0"/>
                        <input type="hidden" id="patient_id_approval" name="patient_id_approval" value=""/>
                    </div>
                    <div class="col-md-2 pull-right" style="padding-top:10px;">
                        <label></label>
                        <button class="btn bg-primary btn-block" type="button" onclick="getDailyApprovalReport();" name="get_daily_approval_report" id="get_daily_approval_report">
                            <i class='fa fa-search'></i> Search
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 box no-border no-margin" id="medication_daily_approval_data">
        </div>
    </div>
</div>
