<input type="hidden" id="prescription_head_id" value="0">
<form id="presc-data-form" action="">
    <div class="box no-border no-margin prescription_wrapper anim" id="prescription_wrapper">
        <div class=" clearfix">

            <div class="col-md-12" style="padding: 2px;">

                <div class="">
                    {{-- <h4 class="card_title">
                        Prescription---
                    </h4> --}}
                    <div class="" style="background: #FFF;" id="prescription_wrapper_card_body">
                        <table class="table no-margin table_sm">
                            <tbody>
                                <tr>
                                    <td>Search Medicine by :</td>
                                    <td>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" id="m_s_type_brand" value="brand" name="m_search_type"
                                                checked="">
                                            <label for="m_s_type_brand"> Brand </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" id="m_s_type_generic" value="generic"
                                                name="m_search_type">
                                            <label for="m_s_type_generic"> Generic </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" id="m_s_type_both" value="both" name="m_search_type">
                                            <label for="m_s_type_both"> Both </label>
                                        </div>
                                    </td>
                                    <td width="25%">&nbsp;</td>
                                    <td>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" id="p_type_regular" value="1" name="p_search_type"
                                                checked="">
                                            <label for="p_type_regular"> Regular </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" id="p_type_new_adm" value="2" name="p_search_type">
                                            <label for="p_type_new_adm"> New Admission </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" id="p_type_emergency" value="3" name="p_search_type">
                                            <label for="p_type_emergency"> Emergency </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" id="p_type_discharge" value="4" name="p_search_type">
                                            <label for="p_type_discharge"> Discharge </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" id="p_type_ward_stock" value="5" name="p_search_type">
                                            <label for="p_type_ward_stock"> Ward Stock </label>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <div class="ht5"></div>
                        <table class="table no-margin no-border">
                            <thead>
                                <tr class="prescription_entry_head">
                                    <th>Medicine</th>
                                    <th>Dose</th>
                                    <th>Duration</th>
                                    <th>Frequency</th>
                                    <th></th>
                                    <th>Quantity</th>
                                    <th>Route</th>
                                    <th colspan="2">Instructions</th>
                                </tr>
                            </thead>
                            <tbody class="sortable">
                                <tr>
                                    <td height="30px" width="30%">
                                        <input type="text" name="search_medicine" autocomplete="off"
                                            class="form-control">

                                        <input type="hidden" id="search_item_code_hidden" name="search_item_code_hidden"
                                            value="">
                                        <input type="hidden" id="search_item_name_hidden" name="search_item_name_hidden"
                                            value="">
                                        <input type="hidden" id="search_item_price_hidden"
                                            name="search_item_price_hidden" value="">

                                        <!-- Medicine List -->
                                        <div class="medicine-list-div" style="display: none;">
                                            <a style="float: left;" class="close_btn_med_search">X</a>
                                            <div class=" presc_theadscroll" style="position: relative;">
                                                <table id="MedicationTable"
                                                    class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                    <thead>
                                                        <tr class="light_purple_bg">
                                                            <th>Medicine</th>
                                                            <th>Generic Name</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="ListMedicineSearchData">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- Medicine List -->

                                    </td>
                                    <td width="10%">
                                        <input type="text" name="search_dose" autocomplete="off" id="search_dose"
                                            class="form-control">
                                    </td>
                                    <td width="10%">
                                        <input type="text" name="search_duration" autocomplete="off"
                                            id="search_duration" class="form-control">
                                    </td>
                                    <td width="10%">
                                        <input type="text" name="search_frequency" autocomplete="off"
                                            id="search_frequency" class="form-control">

                                        <input type="hidden" name="search_freq_value_hidden" value="">
                                        <input type="hidden" name="search_freq_name_hidden" value="">

                                        <!-- Frequency List -->
                                        <div class="frequency-list-div" style="display: none;">
                                            <a style="float: left;" class="close_btn_freq_search">X</a>
                                            <div class="freq_theadscroll" style="position: relative;">
                                                <table id="FrequencyTable"
                                                    class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                    {{-- <thead>
                                                    <tr class="light_purple_bg">
                                                    <th>Frequency</th>
                                                    </tr>
                                                </thead> --}}
                                                    <tbody id="ListFrequencySearchData">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- Frequency List -->

                                    </td>
                                    <td width="5%">
                                        <button onclick="addTapperDose();" type="button" class="btn bg-primary">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </td>
                                    <td width="7%">
                                        <input type="text" name="search_quantity" autocomplete="off"
                                            id="search_quantity" class="form-control">
                                    </td>
                                    <td width="15%">
                                        <input type="text" name="search_route" autocomplete="off" id="search_route"
                                            class="form-control">

                                        <!-- Route List -->
                                        <div class="route-list-div" style="display: none;">
                                            <a style="float: left;" class="close_btn_route_search">X</a>
                                            <div class="route_theadscroll" style="position: relative;">
                                                <table id="RouteTable"
                                                    class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                    {{-- <thead>
                                                    <tr class="light_purple_bg">
                                                    <th>Route</th>
                                                    </tr>
                                                </thead> --}}
                                                    <tbody id="ListRouteSearchData">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- Route List -->

                                    </td>
                                    <td width="15%">
                                        <input type="text" name="search_instructions" id="search_instructions"
                                            class="form-control">
                                    </td>
                                    <td width="3%" align="center"><button type="button" class="btn light_purple_bg"
                                            onclick="addNewMedicine();">Insert</button></td>
                                </tr>
                                <tr>
                                    <td colspan="14" class="partition_line"></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <div class="ht5"></div>
                        <form id="presc-data-form" action="">
                            <div class="theadscroll" style="position: relative; height: 244px;">
                                <table border="1"
                                    style="width: 100%;border:1px solid;border-collapse: collapse !important"
                                    class="table  table_sm  table-bordered" id="medicine-listing-table">
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </form>

                        <div class="col-md-12">
                            <div class="col-md-3 no-padding"><label>Approx Amount</label></div>
                            <div class="col-md-1 padding_sm">
                                <label id="med_total_amnt"></label>
                                <input type="hidden" value="0" name="approx_amnt" id="approx_amnt">
                            </div>
                            <div class="col-md-1 no-padding"><label>Next Review </label></div>
                            <div class="col-md-3 padding_sm">
                                <input type="text" class="form-control" name="next_review_date" id="next_review_date"
                                    onblur="update_prescription_footer_fields()" value="" data="date">
                            </div>
                            <div class="col-md-2 no-padding">
                                &nbsp;<i class="fa fa-square" style="color: #f4c4a6;"></i>&nbsp; Allergy Medicine
                            </div>
                            <div class="col-md-2 no-padding">
                                &nbsp;<i class="fa fa-square" style="color: #C9D5F3;"></i>&nbsp; Outside Medicine
                            </div>
                            <div class="clearfix"></div>
                            <div class="h10"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="clearfix"></div>
                        <div class="h10"></div>
                        <div class="col-md-2 pull-right">
                            <button onclick="saveNewPrescriptions();" style="float:right" type="button"
                                class="btn bg-primary btn-block "><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
