<table id="patients_list_data" class="table table-bordered table_sm theadfix_wrapper patients_list_data">
    <thead>
        <tr class="bg-green">
            <th style="width:16%">Patient Name </th>
            <th style="width:15%">UHID </th>
            <th style="width:10%">Visit Date </th>
            <th style="width:10%">Visit Time </th>
            <th style="width:5%">Age/Gender </th>
            <th style="width:10%">Place </th>
            <th style="width:10%">Phone</th>
        </tr>
    </thead>
    <tbody id='patients_list_data_body' style="font-weight: 500;">

        @if(sizeof($res)>0)
        @php $i=1;
        //$background_color = '';
        //$color = '';
        @endphp
        @foreach($res as $data)


        @if($data->patient_id !='')
        {{-- @php
                                    $occupy_status = 'occupied';
                                @endphp --}}
        @else
        {{-- @php
                                    $occupy_status = 'vacent';
                                @endphp --}}
        @endif

        <tr class="patient_dashboard" style="cursor:pointer;">
            <td onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');">{!!$data->patient_name!!}</td>
            <td onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');">{!!$data->uhid!!}</td>

            <td onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');">{!!date('M-d-Y',
                strtotime($data->visit_date))!!}</td>
            <td onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');">{{ $data->visit_date_time }}</td>
            <td onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');">
                @if($data->age != '')
                {!!$data->age!!}/{!!$data->gender!!}
                @endif
            </td>
            <td onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');">{!!$data->area!!}</td>
            <td onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');">{!!$data->phone!!}</td>
            {{-- <td title="{!!date(\WebConf::getConfig('datetime_format_web'),strtotime($data->admission_date))!!}" @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0) onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"  @endif>
                                        @if($data->admission_date !='')
                                        {!!date(\WebConf::getConfig('datetime_format_web'),strtotime($data->admission_date))!!}
                                        @endif
                                    </td> --}}
            {{-- <td @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0) onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"  @endif>{!!$data->doctor_name!!}</td>
                                    <td title="{{$data->room_type_name}}" @if($data->bed_status!=2 &&
            $transfer_requested_status == 0 && $data->bed_retain_status == 0)
            onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"@else title="{{$data->room_type_name}}"
            @endif>
            @if($transfer_requested_status == 0)
            {!!$data->bed!!}
            @endif

            </td>
            <td>
                @if($data->patient_ip_status ==2 && $transfer_requested_status == 0)
                <a style="margin-top:3px;" href=""
                    onclick="patient_occupi('{{$data->bed_id}}','{{$data->visit_id}}',this);" class="btn bg-primary"><i
                        class="fa fa-bed"></i></a>
                @endif

                @if($transfer_requested_status ==1)
                <button title="Accept room transfer request!" type="button" class="btn bg-red"
                    name="choose_bed_room_request[]" id="choose_bed_room_request['1']"
                    onclick="selectRoomRequestBed('{{$data->room_transfer_request_id}}',{{$data->patient_id}},'{{$data->visit_id}}','{{$data->transfer_requested_station}}','{{$data->transfer_request_retain_status}}')"
                    ;><i class="fa fa-bed"></i></button>
                @endif

            </td> --}}
        </tr>
        @php $i++; @endphp
        @endforeach
        @else
        <tr>
            <td colspan="10">No patients found!</td>
        </tr>
        @endif
    </tbody>
</table>