
    {{-- <div class="col-md-12 padding_sm">

            {!! Form::select('RoomTypeSelect',$room_type_list,'', ['class' => 'form-control','placeholder' =>' Room Type','id' => 'RoomTypeSelect','onchange'=>'SearchRoomListIpList(this.value);']) !!}
    </div> --}}
    <div class="clearfix"></div>
    <div class="ht10"></div>
    <div class="col-md-12 padding_sm">
        <div class="box no-margin no-border">
            <div class="box-body bg-white clearfix" style="padding: 5px 2px;">
            <div class="theadscroll" style="position: relative; height: 486px !important;">
                <div class="roomtype_list">
                    @if(sizeof($res)>0)
                    {{-- <ul class="list-unstyled no-margin no-padding ">
                        <li class="btn-default" title="{{$data->bed_status_name}}">@php
                            $bg_color = \WebConf::getConfig('BedStatusColors.'.$data->bed_status_name);
                            @endphp
                            <div class="bed_icon">
                                <i class="fa fa-bed" style='color:{{$bg_color}}!important;'></i>
                            </div>
                            <div class="patient_room_det">
                                @php
                                $patient_name = ($data->patient_name != '')? $data->patient_name:'&nbsp;';
                                $patient_name  = substr($patient_name,0,10);
                                $room_type_name  = substr($data->room_type_name,0,5);
                                $room_name  = substr($data->room_no,0,5);
                                @endphp
                                <h4 title="{{$data->patient_name}}" class="no-margin">{!!$patient_name!!}</h4>
                                <div class="clearfix"></div>
                                <div class="ht5"></div>
                                <div class="room_det" title="{{$data->room_type_name}}/{{$data->room_no}}">
                                    {{$room_type_name}}/{{$room_name}}
                                </div>
                            </div>
                        </li>

                    </ul> --}}
                    @php
                        $room_type = "";
                    @endphp

                    @foreach($res as $data)
                    @if($data->room_type_name !=$room_type)
                    <div class="col-md-12 padding_sm card-body bg-info">
                        <h5>{{$data->room_type_name}}</h5>
                    </div>
                    @endif
                    @php
                       $room_type =  $data->room_type_name;
                    @endphp
                    <div data-uhid="{{str_replace(' ','',$data->uhid)}}" data-phone="{{str_replace(' ','',$data->mobile_no)}}" data-patientname="{{str_replace(' ','',$data->patient_name)}}" onclick="gotoNsPatientDashboard('{{$data->patient_id}}');" class=" box-body  patient_det_widget card" style="margin:2px;cursor: pointer;">

                        <div class="card-body">
                            <table class="table no-margin no-border table_sm" style="font-size: 10px !important;">
                                <tbody>

                                      <tr>

                                        <td valign="top">
                                            @php
                                                $patient_name = ($data->patient_name != '')? $data->patient_name:'&nbsp;';
                                                $patient_name  = substr($patient_name,0,10);
                                                $room_type_name  = substr($data->room_type_name,0,5);
                                                $room_name  = substr($data->room_no,0,5);
                                            @endphp
                                                <table class="table no-margin table_sm" style="font-size:10px;">
                                                    <tbody >
                                                        <tr>
                                                            <td>
                                                                <div style="" class="waiting_time btn bg-primary">{{$room_name}}
                                                                </div>
                                                            </td>

                                                            <td style="font-size: 10px !important">
                                                                <b>{{$data->patient_name}}({{$data->age}} / {{$data->gender}})</b>
                                                            </td>
                                                            <td style="font-size: 12px !important">
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="font-size: 10px !important">

                                                            </td>
                                                            <td style="font-size: 10px !important">
                                                                <b>{{$data->uhid}}</b>
                                                            </td>
                                                            <td>
                                                                <div class="flag_list">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                        </td>
                                        <td width="45px">
                                            @if($data->patient_image !='')

                                            <img class="patient_image_contianer" src="data:image/jpg;base64,{!!$data->patient_image!!}"/>

                                            @else
                                                <div class="pat_det_img" style="color: darkgray;">
                                                    <i class="fa fa-user" style ="font-size: 30px" aria-hidden="true"></i>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    @endforeach








                    @else
                    <label>No Rooms Found!</label>
                    @endif
                </div>
            </div>
            </div>
        </div>
    </div>


