<div class="col-md-12" style="margin-top: 15px;">
    <div class="col-md-4">
       <button type="button" class="btn btn-success btn-lg btn-block">Remaining Advance {{ $advnce_amount }} Rs</button>
    </div>
    <div class="col-md-4">
      <button type="button" class="btn btn-warning btn-lg btn-block">Total Pending Bills {{ $total_pending_bill }} Rs</button>
    </div>
      <div class="col-md-4">
       <button type="button" class="btn btn-danger btn-lg btn-block">Balance {{ $balance }} Rs</button>
    </div>
</div>
<div class="col-md-12" id="pending_bills">
    <div class="row">
        <div class="col-md-12"> <label><h4>Patient Pending Bills</h4></label></div>
    </div>
    <div class="theadscroll" style="position: relative; height: 300px;">
        @if(sizeof($pending_bills)>0)
        <table class="table table-striped theadfix_wrapper table-bordered">
            <thead>
                <tr class="headergroupbg_blue" style="background-color:white">
                      <th>Bill No</th>
                    <th>Bill Date</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>

                @foreach($pending_bills as $res)
                <tr>

                    <td>{{$res->bill_no}} - {{$res->bill_tag_name}}</td>
                     <td> {{ date('M-d-Y', strtotime($res->bill_date))}}</td>
                    <td>{{$res->net_amount}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <label>No Data!</label>
        @endif
    </div>
</div>
<div class="col-md-12" style="text-align:right">
    <input type="button" class="btn btn-secondary" data-dismiss="modal" value="Close" />

</div>




