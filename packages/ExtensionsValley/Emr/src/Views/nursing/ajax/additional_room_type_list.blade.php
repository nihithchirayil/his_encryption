<div class="col-md-12" style="margin-top: 15px;">
    <div class="col-md-8">
        <label class="filter_label ">Room Types List</label>
        {!! Form::select("additional_bed_trasnfer_room_type", $room_types, 0, ['class' => 'form-control ','placeholder' => "Select Room Type",'id' => 'additional_bed_trasnfer_room_type', 'style' => 'color:#555555; padding:4px 12px;']) !!}
    </div>
    <div class="col-md-8">
        <label class="filter_label ">Choose Station</label>
        {!! Form::select("additional_bed_trasnfer_nursing_station", $list_nursing_station, 0, ['class' => 'form-control ','placeholder' => "Select Nursing Station",'id' => 'additional_bed_trasnfer_nursing_station', 'style' => 'color:#555555; padding:4px 12px;']) !!}
    </div>
   
</div>
<div class="col-md-12" style="text-align:right">
    <input type="button" class="btn btn-secondary" data-dismiss="modal" value="Close" />
    <input type="button"  class="btn btn-primary" onclick="saveAdditionalBedTransferRequest();" value="Save"/>
</div>
