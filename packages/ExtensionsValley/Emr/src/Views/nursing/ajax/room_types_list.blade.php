@if($is_retain != 1)
<div class="col-md-12" style="margin-top: 15px;">
    <div class="col-md-8" style="display:none;">
        <label class="filter_label ">Room Types List</label>
        {!! Form::select("bed_trasnfer_room_type", $room_types, 0, ['class' => 'form-control ','placeholder' => "Select Room Type",'id' => 'bed_trasnfer_room_type', 'style' => 'color:#555555; padding:4px 12px;']) !!}
    </div>
    <div class="col-md-8">
        <label class="filter_label ">Choose Station</label>
        {!! Form::select("bed_trasnfer_nursing_station", $list_nursing_station, 0, ['class' => 'form-control ','placeholder' => "Select Room Type",'id' => 'bed_trasnfer_nursing_station', 'style' => 'color:#555555; padding:4px 12px;']) !!}
    </div>
    <div class="col-md-4">
        @php
            $allow_retain_status = \DB::table('bed_master')
            ->where('id',$current_bed_id)->value('allow_retain');
        @endphp
        @if($allow_retain_status == 't')
            <label class="filter_label ">Retain</label><br>
            <label class="switch">
                <input type="checkbox" id="retain_status">
                <span class="slider round"></span>
            </label>
        @endif
    </div>
</div>
<div class="col-md-12" style="text-align:right">
    <input type="button" class="btn btn-secondary" data-dismiss="modal" value="Close" />
    <input type="button"  class="btn btn-primary" onclick="saveBedTransferRequest();" value="Save"/>
</div>
@else
<div class="col-md-12">
    <label style="color:rgb(4, 4, 90)"><h4>The patient has retained a room!!</h4></label></br>
</div>
<div class="col-md-12" style="padding:15px 5px; border-bottom: 1px solid #3bbb9c; border-top: 1px solid #3bbb9c;">
    <div class="col-md-2" >
    <label class="filter_label ">&nbsp;</label>
        <div class="clearfix">&nbsp;</div>
        <button type="button" class="btn btn-primary" onclick="backToRetain('{{$visit_id}}','{{$current_bed_id}}','{{$user_id}}');" ><i class="fa fa-revert"> Back to Room </i></button>
    </div>
    <div class="col-md-4" >
        <div class="clearfix">&nbsp;</div>
        <label class="filter_label ">Choose Station</label>
        {!! Form::select("bed_trasnfer_nursing_station", $list_retain_not_allowed_ns, 0, ['class' => 'form-control ','placeholder' => "Select Room Type",'id' => 'bed_trasnfer_nursing_station', 'style' => 'color:#555555; padding:4px 12px;']) !!}
    </div>
    <div class="col-md-2 col-md-offset-4" >
        <label class="filter_label ">&nbsp;</label>
        <div class="clearfix">&nbsp;</div>
        <input type="button" class="btn btn-secondary" data-dismiss="modal" value="Close" />
        <input type="button"  class="btn btn-primary" onclick="saveBedTransferRequest();" value="Save"/>
    </div>
</div>
@endif




