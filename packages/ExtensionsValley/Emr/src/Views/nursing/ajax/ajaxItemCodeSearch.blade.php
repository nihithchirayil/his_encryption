<?php


//--------op numberprogressive search--------------------
if (isset($OpNoDetails) ) {
    if(!empty($OpNoDetails)){
        foreach($OpNoDetails as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillOpDetials("{{htmlentities(ucfirst($item->uhid))}}","{{ htmlentities(ucfirst($item->uhid))}}","{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->title)).' '.htmlentities(ucfirst($item->patient_name))}}","{{$item->current_visit_id}}")'>
                <?php echo htmlentities(ucfirst($item->title)). ' '.htmlentities(ucfirst($item->patient_name)) . ' [' . htmlentities(ucfirst($item->uhid)) . ']'; ?>
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}


if (isset($SummaryOpNoDetails) ) {
    if(!empty($SummaryOpNoDetails)){
        foreach($SummaryOpNoDetails as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='selectPatientValues("{{htmlentities(ucfirst($item->id))}}")'>
                <?php echo htmlentities(ucfirst($item->title)). ' '.htmlentities(ucfirst($item->patient_name)) . ' [' . htmlentities(ucfirst($item->uhid)) . ']'; ?>
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}


//--------op numberprogressive search--------------------
if (isset($OpNoDetails_approval) ) {
    if(!empty($OpNoDetails_approval)){
        foreach($OpNoDetails_approval as $item) {
            ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillOpDetials_approve("{{htmlentities(ucfirst($item->uhid))}}","{{ htmlentities(ucfirst($item->uhid))}}","{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->title)).' '.htmlentities(ucfirst($item->patient_name))}}","{{$item->current_visit_id}}")'>
                <?php echo htmlentities(ucfirst($item->title)). ' '.htmlentities(ucfirst($item->patient_name)) . ' [' . htmlentities(ucfirst($item->uhid)) . ']'; ?>
            </li>
            <?php
        }
    } else {
        echo 'No Results Found';
    }
}

if(isset($PatientBookingOpNoDetails)){
    if(!empty($PatientBookingOpNoDetails)){
        foreach($PatientBookingOpNoDetails as $item) {
        ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillOpDetials(this,"{{htmlentities(ucfirst($item->uhid))}}","{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->patient_name))}}","{{htmlentities(ucfirst($item->gender))}}","{{htmlentities(ucfirst($item->phone))}}","{{htmlentities(ucfirst($item->address))}}","{{htmlentities(ucfirst($item->age))}}")'>
            <?php echo htmlentities(ucfirst($item->patient_name)) . ' [' . htmlentities(ucfirst($item->uhid)) . ']'; ?>
        </li>
        <?php
        }
    } else {
        ?>
           <span onclick="setAsNewPatient();">No Results Found</span>
        <?php
    }
}
if (isset($doctorDetails) ) {
    if(!empty($doctorDetails)){
        foreach($doctorDetails as $item) {
        ?>
        <li style="display: block; padding:5px 10px 5px 5px; "
            onclick='fillDoctorDetials("{{htmlentities(ucfirst($item->id))}}","{{ htmlentities(ucfirst($item->name)).'('.htmlentities(ucfirst($item->department)).')'}}")'>
            <?php echo htmlentities(ucfirst($item->name)) . '     (' . htmlentities(ucfirst($item->department)) . ')'; ?>
        </li>
        <?php
        }
    } else {
        echo 'No Results Found';
    }
}

?>
