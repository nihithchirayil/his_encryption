<div class="theadscroll" style="position: relative;">
    @if(sizeof($data)>0)
    <table class="table table-bordered theadfix_wrapper table-condensed">
        <thead>
            <tr class="headergroupbg_blue">
                <th><b>Total Beds </b>
                </th>
                <th>
                    <span style="float:right" class="badge badge-info">{{$data[0]->total_beds}}</span>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr onclick="filterAsRoomChart('ready','{{$station_id}}');">
                <td style="text-align:left">Ready</td>
                <td><span style="float:right;background-color:#00a160" class="badge ">{{$data[0]->ready}}</span></td>
            </tr>
            <tr onclick="filterAsRoomChart('reserved','{{$station_id}}');">
                <td style="text-align:left">Reserved</td>
                <td><span style="float:right;background-color:#8d6911" class="badge ">{{$data[0]->reserved}}</span></td>
            </tr>
            <tr onclick="filterAsRoomChart('occupied','{{$station_id}}');">
                <td style="text-align:left">Occupied</td>
                <td><span style="float:right;background-color:#c76000" class="badge ">{{$data[0]->occupied}}</span></td>
            </tr>
            <tr onclick="filterAsRoomChart('vacant','{{$station_id}}');">
                <td style="text-align:left">Vacant</td>
                <td><span style="float:right;background-color:#8c8c8c" class="badge ">{{$data[0]->vacant}}</span></td>
            </tr>
            <tr onclick="filterAsRoomChart('blocked','{{$station_id}}');">
                <td style="text-align:left">Blocked</td>
                <td><span style="float:right;background-color:#ae3a3a" class="badge ">{{$data[0]->blocked}}</span></td>
            </tr>
        </tbody>
    </table>
    @else
    <label>No Rooms Found!</label>
    @endif
</div>
