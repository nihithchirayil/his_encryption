<!----Bed status modal-------------->
<div id="modal_bed_status" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:68%">

      <div class="modal-content modal-sm">
        <div class="modal-header" style="background:#26b99a; color:#ffffff;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
          <h4 class="modal-title">Room Status</h4>
        </div>
        <div class="modal-body">
            <table class="table table-bordered no-margin table_sm no-border table-striped theadfix_wrapper">
                <tbody>
                    <tr>
                        <td>
                            <i class="fa fa-bed" style="color:#02075d"></i>
                        </td>
                        <td>
                            Admission Requested
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-bed" style="color:#bb85b4"></i>
                        </td>
                        <td>
                            Admitted But Not Occupied
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-bed" style="color:#c76000"></i>
                        </td>
                        <td>
                            Occupied
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-bed" style="color:#dad044"></i>
                        </td>
                        <td>
                            Transfer Requested
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-bed" style="color:#8d6911"></i>
                        </td>
                        <td>
                            Retain
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-bed" style="color:#ff851b"></i>
                        </td>
                        <td>
                            Marked for discharge
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-bed" style="color:#83943e"></i>
                        </td>
                        <td>
                            Sent for billing
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-bed" style="color:#A52A2A"></i>
                        </td>
                        <td>
                            Discharged but not vacated
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-bed" style="color:#81c042"></i>
                        </td>
                        <td>
                            House Keeping
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-bed" style="color:#01ff70"></i>
                        </td>
                        <td>
                            Vacant
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-bed" style="color:#ae3a3a"></i>
                        </td>
                        <td>
                            Blocked
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>

      </div>

    </div>
</div>
<!----Bed status modal Ends--------->
