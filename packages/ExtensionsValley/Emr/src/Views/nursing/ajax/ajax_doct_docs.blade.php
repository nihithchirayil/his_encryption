<div class="col-md-12" style="margin-top: 15px;">
    <table style="align-content: center;" class="table table-striped table-bordered table-condensed">
    @if(sizeof($notes)>0)
    <thead>
        <tr class="headergroupbg_blue">
            <th style="width:20%">Doctor Name</th>
           <th style="width:60%">Notes</th>
           <th style="width:10%">Created on</th>
           <th style="width:10%;text-align: center">Activities</th>
        </tr>
    </thead>
    <tbody>

    @foreach($notes as $result)
     <?php
        if($user_id == $result->created_by){
            $disable_status = 0;
        }else{
            $disable_status = 1;
        }
     ?>
    <tr id="row_id_{!!$result->note_id!!}">
        <td>{!!$result->doctor_name!!}</td>
        <td id="notes_data_{!!$result->note_id!!}">{!!$result->notes!!}</td>
        <td>{!! date(\Site::getConfig('datetime_format_ehr_web'), strtotime($result->created_at))  !!}</td>
        <td style="text-align: center">
            @if($disable_status == 0)
                <button type="button" onclick="private_notes_edit('{{$result->note_id}}');" class="btn bg-blue btn-block"><span class="fa fa-pencil"> Edit</span></button>
                <button type="button" onclick="private_notes_delete('{{$result->note_id}}');"class="btn bg-red btn-block"><span class="fa fa-trash"> Delete</span></button>
            @endif
        </td>
       <?php /* $result->created_at */?>
    </tr>
    @endforeach
@else
<tr><td colspan="4">No Records Found!</td></tr>
@endif
    </tbody>
</table>
</div>
