<div class="col-md-12" id="additional_room_request_history">
    <div class="row">
        <div class="col-md-12"> <label><h4>Request History</h4></label></div>
    </div>
    <div class="theadscroll" style="position: relative; height: 300px;">
        @if(sizeof($data)>0)
        <table class="table table-striped theadfix_wrapper table-bordered table-condensed">
            <thead>
                <tr class="headergroupbg_blue">
                    <th>Si No</th>
                    <th>Requested To</th>
                    <th>Requested By</th>
                    <th>Requested At</th>
                    <th>Processing Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                $i = 1;
                @endphp
                @foreach($data as $res)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$res->location}}</td>
                    <td>{{$res->username}}</td>
                    <td>{{date(\WebConf::getConfig('datetime_format_web'),strtotime($res->created_at))}}</td>                    
                    <td>
                        @if($res->status ==1)
                            <i title="Completed" class="btn fa fa-check bg-green" aria-hidden="true"></i>
                        @else
                        <i title="Pending" class="btn fa fa-hourglass bg-red" aria-hidden="true"></i>
                        @endif
                    </td>
                    <td>
                        @if($res->status == 0)
                            <a title="Delete Bed Transfer Request"  onclick="deleteAdditionalRoomTransferReq('{{$res->id}}');">
                              <i class="btn fa fa-trash bg-red" aria-hidden="true"></i>
                            </a>
                        @endif
                    </td>
                    @php
                        $i++;
                    @endphp

                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <label>No Room Additional Requests Found!</label>
        @endif
    </div>
</div>
