<div class="col-md-12 box-body">
    <button type="button" onclick="saveMedicationReturn();" class="btn bg-green pull-right"><i class="fa fa-save"></i> save</button>
</div>
<div class="col-md-12 box-body">
    <div class="theadscroll always-visible" style="position: relative; height: 300px;">
        {!!Form::open(['name'=>'medication_return_form','id'=>'medication_return_form']) !!}
        <table class="table theadfix_wrapper no-margin table_sm table-bordered ">
            <thead class="headerclass" style="background-color:#26b99a;color:white;border-spacing: 0 1em;font-family:sans-serif">
                <tr>
                    <td>Si No</td>
                    <td>Medicine</td>
                    <td>Quantity</td>
                    <td>Ret Qty</td>
                    <td>Available</td>
                    <td>Created On</td>
                    <td>Remarks</td>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                    $qty = 0;
                @endphp
                @foreach ($res as $data)
                @php
                    $qty = round($data->qty);
                @endphp
                <tr>
                    <td>{{$i}}
                    <input type="hidden" name="medicine_id[]" value="{{$data->item_id}}"/>
                    <input type="hidden" name="medication_detail_id[]" value="{{$data->medication_detail_id}}"/>
                    <input type="hidden" name="bill_detail_id[]" value="{{$data->id}}"/>
                    </td>
                    <td>{{$data->item_desc}}</td>
                    <td>{{$qty}}</td>
                    <td><input type="number" onblur="checkReturnQty(this,'{{$data->available_quantity}}')" name="return_qty[]" class="form-control"/></td>
                    <td>{{$data->available_quantity}}</td>
                    <td>{{date('M-d-Y h:i A',strtotime($data->created_at))}}</td>
                    <td><input type="text" name="remarks[]" placeholder="Remarks" class="form-control"/></td>
                </tr>
                @php
                    $i++;
                @endphp
                @endforeach
            </tbody>
        </table>
        {!! Form::token() !!}
        {!! Form::close() !!}
    </div>
</div>
