

    <div class="theadscroll" style="position: relative;width:100%;">
        @if(sizeof($resultData)>0)
        <table class="table table-striped theadfix_wrapper table-condensed">
            <thead>
                <tr class="headergroupbg_blue">
                    <th>Si No</th>
                    <th style="text-align:left">Room Type</th>
                    <th style="text-align:left">Patient</th>
                    <th>Created On</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                $i = 1;
                @endphp
                @foreach($resultData as $res)
                <tr>
                    <td>{{$i}}</td>
                    <td style="text-align:left">{{$res->room_type_name}}</td>
                    <td style="text-align:left">{{$res->patient_name}}</td>
                    <td>{{date('M-d-Y h:i A',strtotime($res->created_at))}}</td>
                    <td style="text-align:center">
                        <button type="button" class="btn bg-green"name="choose_bed_room_request[]" id="choose_bed_room_request['{{$i}}']"
                        onclick="selectRoomRequestBed('{{$res->id}}','{{$res->patient_id}}','{{$res->visit_id}}','{{$res->room_type}}','{{$res->retain_status}}')";>Admit</button>
                        <button type="button" class="btn bg-red" onclick="rejectRoomRequest('{{$res->id}}');">
                            <i class="fa fa-trash"></i>
                        </button>
                    </td>
                </tr>
                @php
                $i++;
                @endphp
                @endforeach
            </tbody>
        </table>
        @else
        <label>No Request Found!</label>
        @endif
    </div>

