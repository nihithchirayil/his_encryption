@if(sizeof($resultData))
<div class="btn-group" data-toggle="buttons">

    <input type="hidden" name="room_request_request_id" id="room_request_request_id" value=''/>
    <input type="hidden" name="room_request_visit_id" id="room_request_visit_id" value=''/>
    <input type="hidden" name="room_request_patient_id" id="room_request_patient_id" value=''/>
    <input type="hidden" name="room_request_room_type" id="room_request_room_type" value=''/>
    <input type="hidden" name="room_request_retain_status" id="room_request_retain_status" value=''/>

    @foreach($resultData as $res)
    @if($res->status == 1)
    <label class="btn" style="margin:10px; width:110px;color:white; background: {{$res->value_string}}; " title="{{$res->bed_status_name}}">
    <input type="radio" name="room_transfer_bed" value="{{$res->id}}"  checked=""/>

    <i class="fa fa-bed" style="float: left;
    margin-left: 5px;
    margin-top: 2px;"></i>
    {{$res->bed_name}} &nbsp;&nbsp;&nbsp;
    </label>
    @else
    <label class="btn " style="margin:10px; width:110px; background: {{$res->value_string}};" disabled title="{{$res->bed_status_name}}">
    <i class="fa fa-bed" style="float: left; margin-left: 5px; margin-top: 2px;"></i> {{$res->bed_name}} &nbsp;&nbsp;&nbsp;
    </label>
    @endif
    @endforeach
</div>
@else
 <label> No vacant beds found </label>
@endif
