<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            // var user_name = $('#search_username').val();
            // var status = $('#search_status').val();
            var param = {
                // user_name: user_name,
                // status: status
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    // $('#searchUserBtn').attr('disabled', true);
                    // $('#searchUserSpin').removeClass('fa fa-search');
                    // $('#searchUserSpin').addClass('fa fa-spinner');
                    $('#new_nursing_note_modal_body').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(data) {
                    $("#new_nursing_note_modal_body").html(data);
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                    setTimeout(function() {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                },
                complete: function() {
                    // $('#searchUserBtn').attr('disabled', false);
                    // $('#searchUserSpin').removeClass('fa fa-spinner');
                    // $('#searchUserSpin').addClass('fa fa-search');
                    $('#new_nursing_note_modal_body').LoadingOverlay("hide");
                }
            });
            return false;
        });

    });
</script>
<div class="col-md-12" style="margin-top: 15px;">
    <table style="align-content: center;" class="table table-striped table-bordered table-condensed">
    @if(sizeof($notes)>0)
    <thead>
        <tr class="headergroupbg_blue">
            <th style="width:20%">Doctor Name</th>
           <th style="width:60%">Notes</th>
           <th style="width:10%">Created on</th>
        </tr>
    </thead>
    <tbody>

    @foreach($notes as $result)
     <?php
        if($user_id == $result->created_by){
            $disable_status = 0;
        }else{
            $disable_status = 1;
        }
        $encoded_string = json_decode($result->notes,true);
            $decoded_string = str_replace("<br />", " ", $encoded_string);
            $decoded_string = html_entity_decode(strip_tags($decoded_string));
     ?>
    <tr id="row_id_{!!$result->note_id!!}">
        <td>{!!$result->doctor_name!!}</td>
        <td id="notes_data_{!!$result->note_id!!}">{!!$decoded_string!!}</td>
        <td>{!! date('M-d-Y h:i A',strtotime($result->created_at))  !!}</td>
       <?php /* $result->created_at */?>
    </tr>
    @endforeach
@else
<tr><td colspan="4">No Records Found!</td></tr>
@endif
    </tbody>
</table>
</div>
<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>
