<div class="theadscroll" style="position: relative;width:400px;">
    @if(sizeof($resultData)>0)
    <table class="table table-striped theadfix_wrapper table-condensed">
        <thead>
            <tr class="headergroupbg_blue">
                <th>Room</th>
                <th>Patient</th>
            </tr>
        </thead>
        <tbody>

            @foreach($resultData as $res)
            <tr>
                <td>{{$res->bed_name}}</td>
                <td>{{$res->patient_name}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <label>No Rooms Found!</label>
    @endif
</div>
