        @php
            if($course_id == 1){
                $append_str = 'st';
            }elseif($course_id == 2){
                $append_str = 'nd';
            }elseif($course_id == 3){
                $append_str = 'rd';
            }else{
                $append_str = 'th';
            }
        @endphp

        @php
            $given_date = date('M-d-Y', strtotime($medicine_given_time));
            $given_time = date('h:i A',strtotime($medicine_given_time));
            $given_time_picker = date('H:i',strtotime($medicine_given_time));
        @endphp

        <div id="chart_id_{{$medication_chart_id}}" class="col-md-2 nopadding chart_class">
            <div class="col-md-10">
                <div class="md-form" style=" background-color:#d2fff4;color:#3a3a42 !important;text-align:left;font-weight:600;padding:5px;">
                    {{$given_date}} </br>
                    {{$given_time}}
                </div>
                <label style="margin-top: 2px;">{{$course_id}}{{$append_str}} Course</label>
            </div>
            <div class="col-md-2" style="text-align:left;">
                <button type="button" class="btn-sm bg-primary medication_stop" onclick="editGivenMedication('{{$medication_chart_id}}','{{$given_date}}','{{$given_time_picker}}');" style="height: 20px;width:25px;    margin-left: -10px;"><i class="fa fa-edit"></i></button>
                <button type="button" onclick="deleteGivenMedication('{{$medication_chart_id}}');" class="btn-sm bg-red medication_stop" style="height: 20px;width:25px;margin-left: -10px;"><i class="fa fa-trash"></i></button>
            </div>
        </div>
