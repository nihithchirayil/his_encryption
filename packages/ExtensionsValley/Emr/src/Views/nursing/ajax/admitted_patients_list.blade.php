
        @if(sizeof($res)>0)
        @php $i=1;
          //$background_color = '';
          //$color = '';
        @endphp
            @foreach($res as $data)
                @php
                    $transfer_requested_status = 0;
                @endphp

            @if($nursing_station_id == $data->transfer_requested_station)
                @php
                    $transfer_requested_status = 1;
                @endphp
            @endif

            @if($data->bed_retain_status == 1)
                @php
                    $data->value_string = '#8d6911';
                    $data->font_color = 'white';
                @endphp
            @endif
            @if($data->patient_id !='')
                @php
                    $occupy_status = 'occupied';
                @endphp
            @else
                @php
                    $occupy_status = 'vacent';
                @endphp
            @endif

                <tr class="{{$data->patient_ip_status}} admitpt {{$data->room_type_name}} {{$data->bed_status_name}} {{$occupy_status}}" style="cursor:pointer;background-color:{{$data->value_string}};color:{{$data->font_color}}" title="@if($data->retain_bed_name !='') Additional Room : {{trim($data->retain_bed_name)}} ({{trim($data->retain_ns)}}) @endif">

                    <td @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0) onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');" @endif>
                        @if($transfer_requested_status == 0)
                            {!!$data->room_type_name!!}
                        @endif

                    </td>
                    <td @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0) onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"  @endif>{!!$data->patient_name!!}</td>
                    <td @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0) onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"  @endif>{!!$data->ip_no!!}</td>
                    <td @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0) onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"  @endif>{!!$data->uhid!!}</td>
                    <td @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0) onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"  @endif>
                        @if($data->age != '')
                        {!!$data->age!!}/{!!$data->gender!!}
                        @endif
                    </td>
                    <td @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0)onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"  @endif>{!!$data->area!!}</td>
                    <td @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0) onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"  @endif>{!!$data->phone!!}</td>
                    <td title="{!!date(\WebConf::getConfig('datetime_format_web'),strtotime($data->admission_date))!!}" @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0) onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"  @endif>
                        @if($data->admission_date !='')
                        {!!date(\WebConf::getConfig('datetime_format_web'),strtotime($data->admission_date))!!}
                        @endif
                    </td>
                    <td @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0) onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"  @endif>{!!$data->doctor_name!!}</td>
                    <td title="{{$data->room_type_name}}" @if($data->bed_status!=2 && $transfer_requested_status == 0 && $data->bed_retain_status == 0) onclick="gotoNsPatientDashboard('{!!$data->patient_id!!}');"@else title="{{$data->room_type_name}}" @endif>
                        @if($transfer_requested_status == 0)
                            {!!$data->bed!!}
                        @endif

                    </td>
                    <td >
                        @if($data->patient_ip_status ==2 && $transfer_requested_status == 0)
                            <a style="margin-top:3px;" href="" onclick="patient_occupi('{{$data->bed_id}}','{{$data->visit_id}}',this);" class="btn bg-primary"><i class="fa fa-bed"></i></a>
                        @endif

                        @if($transfer_requested_status ==1)
                            <button title="Accept room transfer request!" type="button" class="btn bg-red" name="choose_bed_room_request[]" id="choose_bed_room_request['1']" onclick="selectRoomRequestBed('{{$data->room_transfer_request_id}}',{{$data->patient_id}},'{{$data->visit_id}}','{{$data->transfer_requested_station}}','{{$data->transfer_request_retain_status}}')";><i class="fa fa-bed"></i></button>
                        @endif

                    </td>
                </tr>
            @php $i++; @endphp
            @endforeach
        @else
            <tr>
                <td colspan="10">No patients found!</td>
            </tr>
        @endif


