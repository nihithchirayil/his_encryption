
<div class="row">
    <div class="col-md-12">
        <div class="form-group control">
        {!! Form::label('doctor_id', 'Doctor') !!}
        {!! Form::select('doctor_id', $doctor_list, isset($selected_doctor[0]->admitting_doctor) ? $selected_doctor[0]->admitting_doctor :null, [
            'class'       => 'form-control select2',
                'placeholder' => 'Select Doctor',
                'id'          => 'doctor_id'
        ]) !!}

    </div>
</div>




