<style>
    .timepickerclass{
        font-weight: 600;
}
.timepickerclass_default{
color: #a8a8a8 !important;
  }
</style>
@if(sizeof($res)>0)
@foreach($res as $data)
<div class="col-md-12 box no-border no-margin">
    <div class="col-md-6 box-body clearfix">
        <table class="table no-margin no-border table-striped table_sm" style="border:1px solid;font-size:12px;">
            <tbody>
                <tr data-toggle="collapse" data-target=".demo">
                    <td colspan="1">
                        <button style="width: 17px;height: 17px;text-align: center;padding: 0px;" title="Detail View" type="button" data-toggle="collapse" data-target=".demo{{$data->medication_detail_id}}" class="btn btn-sm bg-green" >
                            <i class="fa fa-arrow-down" aria-hidden="true"></i>
                        </button>
                        <b>Medicine123 :</b>
                    </td>
                    <td colspan="3" class="columcontain"><b>{{$data->medicine}}</b></td>
                    <td style="text-align:right;">
                        <div class="col-md-12" style="display: table">
                            <span style="table-row">
                                <span style="table-cell">
                                    <div class="checkbox checkbox-info" style="float: right;clear:left">
                                        <input type="checkbox" title="Check to reorder medicine" id="chk_reorder_medicine_{{$data->medication_detail_id}}" name="chk_reorder_medicine" class="checkbox_reorder" onclick="check_reorder_count();" value="7725151">
                                        <label for="chk_reorder_medicine_{{$data->medication_detail_id}}"></label>
                                    </div>
                                </span>
                                <span style="table-cell">
                                    <button style="margin-right: 7px;" title="Stop Medicine" type="button" class="btn btn-sm bg-red medication_stop" onclick="stopMedicine('{{$data->medication_detail_id}}');">
                                        <i class="fa fa-stop-circle" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </span>
                        </div>

                    </td>
                </tr>
                <span>
                <tr id="" class="collapse demo{{$data->medication_detail_id}}">
                    <td colspan="1"><b>GenericName :</b></td>
                    <td colspan="3" class="columcontain">--------</td>
                    <td></td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td style="width:25%"><b>Dose</b></td>
                    <td style="width:25%"><b>Route</b></td>
                    <td style="width:25%"><b>Frequency</b></td>
                    <td style="width:25%"><b>Directions</b></td>
                    <td></td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td class="columcontain">{{$data->dose}}</td>
                    <td class="columcontain">{{$data->route}}</td>
                    <td class="columcontain">{{$data->frequency}}</td>
                    <td class="columcontain">{{$data->notes}}</td>
                    <td></td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td><b>Start At</b></td>
                    <td><b>Stop At</b></td>
                    <td><b>Doctor Name</b></td>
                    <td rowspan="2">
                        <div class="col-md-12">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6" style="padding-top: 5px;">

                            </div>
                        </div>
                        <div class="col-md-12">

                        </div>
                    </td>
                    <td></td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td class="columcontain">
                        {{date(\WebConf::getConfig('datetime_format_web'),strtotime($data->start_at))}}
                    </td>
                    <td class="columcontain"></td>
                    <td class="columcontain">{{$data->approved_dr}}</td>
                    <td></td>
                </tr>
                </span>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <div class="col-md-12" style="padding-top:5px;">
        @for($i=1;$i<=$data->frequency_value;$i++)
            @php
             if($i==1){
                 $append_str = 'st';
             }elseif($i==2){
                 $append_str = 'nd';
             }elseif($i==3){
                 $append_str = 'rd';
             }else{
                $append_str = 'th';
             }
             $start_at = date('H:i', strtotime($data->start_at));
             $sloat_value = '';
             if($i == 1){
                 $actual_time = $start_at;
             }
            @endphp
                <div class="col-md-4 nopadding" style="height: 60px;">
                    <table>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-info" style=" margin-bottom: 15px;">
                                    <input type="checkbox" title="Check to given sign" id="{{$data->id}}_{{$i}}" name="chk_medicine_given" class="checkbox_reorder" onchange="updateMedicineGiven('{{$data->id}}_{{$i}}','{{$data->frequency_value
                                    }}');" value="{{$data->medication_detail_id}}_{{$i}}">
                                    <label for="{{$data->id}}_{{$i}}"></label>
                                </div>
                            </td>
                            <td>
                                <div class="md-form" >
                                    <input type="time" @if($i==1)value="{{$actual_time}}"@endif name="timepicker_{{$data->id}}_{{$i}}" id="timepicker_{{$data->id}}_{{$i}}" class="form-control timepickerclass_default timepickerclass">
                                    <label style="margin-top: 3px;" for="timepicker_{{$data->id}}_{{$i}}"><b>{{$i}}{{$append_str}} Course</b></label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
        @endfor
        </div>
    </div>
</div>
@endforeach
@else
<div class="col-md-12">
    <label>No Approved Medications Found</label>
</div>
@endif
