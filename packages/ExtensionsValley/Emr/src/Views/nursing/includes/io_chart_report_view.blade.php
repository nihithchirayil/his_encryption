
@php
//  echo '<pre>';
//  print_r($intake_list);
//  exit;
//  print_r($output_list);
@endphp
<div class="col-md-12" style="margin-bottom:30px;">
    <div class="col-md-6">
        <h4 style="text-align:center;margin-bottom:20px;">Intake</h4>
        <table class="table no-margin table-striped" style="width:100%;">
            <thead>
                <tr style="">
                    <th style="width:50%"><b>Date</b></th>
                    <th style="width:25%"><b>Intake Total</b></th>
                    <th style="width:25%"><b>Cumulative Total</b></th>
                </tr>
            </thead>
            <tbody>

                @php
                    $current_date = '';
                    $cumulative_intake_total = 0;
                @endphp
                @foreach($intake_list as $intake)
                    @php
                        $cumulative_intake_total = $cumulative_intake_total + $intake->intake_total;
                    @endphp
                        <tr>
                            <td>{{ date('M-d-Y',strtotime($intake->date_and_time)) }}</td>
                            <td style="text-align:left;">{{$intake->intake_total}}</td>
                            <td style="text-align:left;">{{$cumulative_intake_total}}</td>
                        </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h4 style="text-align:center;margin-bottom:20px;">Output</h4>
        <table class="table no-margin table-striped" style="width:100%;">
            <thead>
                <tr style="">
                    <th style="width:50%"><b>Date</b></th>
                    <th style="width:25%"><b>Output Total</b></th>
                    <th style="width:25%"><b>Cumulative Total</b></th>
                </tr>
            </thead>
            <tbody>
                @php
                    $current_date = '';
                    $cumulative_output_total = 0;
                @endphp
                @foreach($output_list as $output)
                    @php
                        $cumulative_output_total = $cumulative_output_total + $output->output_total;
                    @endphp
                        <tr>
                            <td>{{ date('M-d-Y',strtotime($output->date_and_time)) }}</td>
                            <td style="text-align:left;">{{$output->output_total}}</td>
                            <td style="text-align:left;">{{$cumulative_output_total}}</td>
                        </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>

