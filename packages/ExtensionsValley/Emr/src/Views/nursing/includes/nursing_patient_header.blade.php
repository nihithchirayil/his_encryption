<div class="box no-border no-margin">
    <div class="box-body" style="border-radius:5px;">
        <table class="table table_sm  no-margin no-border">
            <tbody>
                <tr>
                    <td rowspan="3" style="width:14%;align:left">
                        <div class="user_patient_img pos_rel pop-div">


                            @if(isset($patient_info[0]->patient_image) && $patient_info[0]->patient_image !='')

                                <img class="img-circle user_img" alt="" id='poplink' src="data:image/jpg;base64,{!!$patient_info[0]->patient_image!!}" style="height:59px"/>

                            @else
                                <div class="img-circle user_img" id='poplink' style="color: darkgray;">
                                    <i class="fa fa-user" style ="font-size: 54px;
                                    padding-left: 10px;
                                    padding-bottom: 4px;" aria-hidden="true"></i>
                                </div>
                            @endif


                            <div class="smiley_status">
                                <img src="{{asset("packages/extensionsvalley/dashboard/images/smiley.png")}}" alt="">
                                <img class="hidden" src="{{asset("packages/extensionsvalley/dashboard/images/sad_smiley.png")}}" alt="">
                            </div>
                            <div class="pop-content">
                                @include('Emr::emr.includes.patient_info')
                            </div>


                        </div>

                    </td>

                    <td title="{{$patient_info[0]->patient_name}}"><b>{{$patient_info[0]->patient_name}} ({{$patient_info[0]->uhid}})</b>
                        @if($patient_info[0]->is_expired == 1)
                            <span class="badge bg-red">Expired</span>
                        @endif
                    </td>


                </tr>
                <tr>
                    <td><b>IPNO :</b>{{$patient_info[0]->admission_no}}</td>

                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td title="Admission date"><b>DOA :</b>@if(strtoupper(trim($current_visit_type)) == 'IP' ){{date(\WebConf::getConfig('datetime_format_web'),strtotime($patient_info[0]->last_visit_datetime))}} @endif</td>
                </tr>
                <tr>
                    <td><b>({{$patient_info[0]->age}}/{{$patient_info[0]->gender}})</b></td>
                    <td><b>Room No:</b>{{$patient_info[0]->bed_name}}</td>
                </tr>


            </tbody>
        </table>
    </div>
</div>
