


	  <h5 style="margin-left:10px;">Doctor Consultation</h5>

            <table class="table table-striped  no-border no-margin" style="width:100%;">
                <tbody>
                @if($total_count1 > 0)
                    @php
                    $i=1;
                     $previous = NULL;
                    @endphp
                    @foreach($data1 as $datas1)

                     <?php $cls = $text_val ='';?>
                     @if($datas1->service_doneon == 1)
                        <?php

                            $text_val = "Morning";
                         ?>

                         @elseif($datas1->service_doneon == 2)
                        <?php

                            $text_val = "Noon";
                         ?>

                        @elseif($datas1->service_doneon == 3)
                        <?php

                            $text_val = "Evening";
                         ?>

                         @elseif($datas1->service_doneon == 4)
                        <?php

                            $text_val = "Night";
                         ?>

                        @else
                        <?php

                            $text_val = "Emergency";
                         ?>
                         @endif
                    @php
                        $doctor_name = (!empty($datas1->doctor_name)) ? $datas1->doctor_name : "";
                         $service_date = (!empty($datas1->service_datetime)) ? ExtensionsValley\Emr\CommonController::getDateFormat($datas1->service_datetime,'d-M-Y') : "";
                         $service_time = $intake_time = ($datas1->service_datetime !='') ? date('h:i A', strtotime($datas1->service_datetime)) : "";



                        $class_highlight = "";
                        if($i==1):
                            $class_highlight = "tr_highlight";
                        else:
                            $class_highlight = "";
                        endif;
                        $scroll_to_tab = "scrollto-".$i;
                        $selected_tab = "selectedtab-".$i;



                    @endphp
                     @if($service_date != $previous)
                    	 @php
                       $previous = $service_date;
                        @endphp
                    <tr id="{{$scroll_to_tab}}" class=" ">
                        <td colspan="3" class="text-left">
                            <b>{{$service_date}} &nbsp; </b>
                        </td>
                        <td class="text-right">&nbsp;</td>
                    </tr>
                     @endif
                    <tr>

                        <td width="100%" valign="top">

                            <table class="table no-margin table-striped" style="width:100%;">
                                <tbody>

                                    <tr style="">
                                        <td style="width:25%"><b>Consultation Type</b></td>
                                        <td style="width:25%"><b>Time</b></td>
                                        <td style="width:50%"><b>Doctor</b></td>


                                    </tr>

                                    <tr>
                                        <td>{{$text_val}}</td>
                                            <td>{{$service_time}}</td>
                                        <td>{{$doctor_name}} <div class="text-left text_limit"></div></td>

                                    </tr>


                                </tbody>
                            </table>
                        </td>


                    </tr>
                    @php
                        $i++;
                    @endphp
                    @endforeach
                @else
                    <tr>
                      <td colspan="2">No Data Found..</td>
                    </tr>
                  @endif
                </tbody>
            </table>


