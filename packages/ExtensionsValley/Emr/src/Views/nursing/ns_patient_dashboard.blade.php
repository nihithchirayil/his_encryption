@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    @include('Emr::emr.includes.custom_modals')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/default/css/nurse-patient.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <style>
        .block_list button {
            padding: 2px;
            font-size: 13px;
        }

        .block_list h5 {
            font-size: 13px;
        }

        .position_fixed_header {
            border-radius: 0px;
        }

        input-searchicon:focus {
            outline: none;
        }

        .cursor {
            transition: 150ms;
        }

        .appointment_invalid {
            background-color: #ccc0c0;
        }

        .appointment_booked {
            background-color: #cfecfc;
        }

        .appointment_checked_in {
            background-color: #92f0b6;
        }

        .headerbtn {
            border: 1px solid green;
            min-width: 112px;
        }

        .headerbtn:hover {
            background-color: green;
            color: white;
        }

        .pop-history-btn:hover .pos_stat_hover {
            position: static;
        }

        #inv_remarks,
        #inv_clinical_history {
            height: 110px !important;
        }

        #investigation_clipboard {
            height: 110px;
        }

        .btn-block {
            border: none !important;
        }

        .badge {
            font-size: 10px !important;
        }
    </style>
    <link
        href="{{ asset('packages/extensionsvalley/master/default/css/new_emr.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
        rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <div class="modal fade" id="investigationDateTimePickerModel" tabindex="-1" role="dialog" style="display:none;">
        <div class="modal-dialog" style="width: 40%">
            <div class="modal-content">
                <div class="modal-header modal_box">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="investigationDateTimePickerModelHeader"></h4>
                </div>
                <div class="modal-body" style="min-height: 75px;">
                    <div class="col-md-12">
                        <div class="mate-input-box">
                            <label class="filter_label">Service Date</label>
                            <input type="hidden" value="" id="invest_cnt_hiddden">
                            <input type="text" autocomplete="off" value="" class="form-control"
                                id="investigationServiceDate">
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 10px;">
                    <button style="padding: 3px 3px; margin-top: 5px;" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="user_id" value="{{ $user_id }}">
    <input type="hidden" id="patient_id" value="{{ $patient_id }}">
    <input type="hidden" id="visit_id" value="{{ $visit_id }}">
    <input type="hidden" id="encounter_id" value="{{ $encounter_id }}">
    <input type="hidden" id="location_id" value="{{ $location_id }}">
    <input type="hidden" id="patient_ip_status" value="{{ $patient_ip_status }}">
    <input type="hidden" id="ca_head_id" value="0">
    <input type="hidden" id="fav_form_id" value="{{ $form_id }}">
    <input type="hidden" id="investigation_head_id" value="{{ $investition_drafted_head_id }}"
        name="investigation_head_id">
    <input type="hidden" id="age" value="{{ $age }}">
    <input type="hidden" id="search_date_web" value="{{ $search_date_web }}">
    <input type="hidden" id="search_time_web" value="{{ $search_time_web }}">
    <input type="hidden" id="prescription_head_id" value="0">
    <input type="hidden" id="emr_changes" value="{{ @$emr_changes ? $emr_changes : ''}}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="investigation_draft_mode" value="0">
    <input type="hidden" id="assesment_draft_mode" value="0">
    <input type="hidden" id="prescription_changes_draft_mode" value="0">
    <input type="hidden" id="iv_fluid_draft_mode" value="0">
    <input type="hidden" id="intake_output_draft_mode" value="0">
    <input type="hidden" id="nursing_station" value="{{ $nursing_station }}">
    <input type="hidden" id="current_visit_type" value="{{ $current_visit_type }}">
    <input type="hidden" id="hidden_prescription_warning_flag" value="{{ $prescription_warning_flag }}">
    
    <input type="hidden" id="pacs_viewer_prefix" value="@isset($pacs_viewer_prefix){{ $pacs_viewer_prefix  }}@endisset">
    <input type="hidden" id="pacs_report_prefix" value="@isset($pacs_report_prefix){{ $pacs_report_prefix }}@endisset">

    
    <div class="right_col">

        <div class="container-fluid" style="padding: 15px;">
            <div class="row codfox_container">


                <div class="position_fixed_header" style="position:relative;">

                    <div class="col-md-4 padding_sm">
                        @include('Emr::nursing.includes.nursing_patient_header')
                    </div>

                    <div class="col-md-4 padding_sm">
                        @include('Emr::emr.includes.patient_allergy')
                    </div>

                    <div class="col-md-4 padding_sm">
                        @include('Emr::emr.includes.patient_vitals')
                    </div>

                    <div class="clearfix"></div>
                    <div class="ht10"></div>


                </div>

                <div class="ip_op_btn_group position-fixed"
                    style="position: fixed; z-index: 9999; right: 2.5%; top: 124px; z-index: 9999;" data-spy="affix"
                    data-offset-top="50">
                    {{-- <button type="button" class="btn bg-blue pull-right btn-block" style="float:right !important" onclick="saveNurseEncounterData()"><i class="fa fa-save"></i> Save</button> --}}

                </div>

                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12 padding_sm">
                    <div class="col-md-6 box-body" style="background-color:#ffe6c0 !important;">
                        <div class="col-md-2 padding_sm">
                            <button title="History" class="btn bg-default headerbtn btn-nurse-task"
                                onclick="combinedView();"><i class="fa fa-history" aria-hidden="true"></i> History</button>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <button title="Private Notes" class="btn bg-default headerbtn  btn-nurse-task"
                                id="btn_private_notes"><i class="fa fa-book"></i> Notes</button>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <button title="Lab Results" data-toggle="modal" onclick="LabResultsTrend();"
                                data-target="#modal_lab_results" class="btn bg-default headerbtn  btn-nurse-task"><i
                                    class="fa fa-flask"></i> Lab</button>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <button title="Discharge Summary" class="btn bg-default headerbtn  btn-nurse-task"
                                id="btn_dis_summary"><i class="fa fa-list"></i> D. Summary</button>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <button data-toggle="modal" title="Death Marking" data-target="#mark_death_modal"
                                class="btn bg-default headerbtn  btn-nurse-task"><i class="fa fa-plus"></i> Death</button>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <button type="button" title="Adverse Drug Reaction" onclick="drugReaction();"
                                class="btn bg-default headerbtn" style="color:brown;"><i class="fa fa-medkit"></i>Drug
                                Reaction
                            </button>
                        </div>
                    </div>

                    <div class="col-md-6 box-body" style="background-color:#c1d6d9 !important;">
                        <div class="col-md-2 padding_sm">
                            <button title="Radiology Results" class="btn bg-default headerbtn radiologyResultsBtn" type="button">
                                <i class="fa fa-flask"></i> Radiology Results
                            </button>
                        </div>
                        <div class="col-md-2 padding_sm pull-right">
                            <button title="Change Admitting Doctor"
                                class="btn bg-default headerbtn  btn-nurse-task ip_action_button" data-toggle="modal"
                                data-target="#chnge_admit_dr" type="button"> Change Doctor
                            </button>
                        </div>
                        <div class="col-md-2 padding_sm pull-right">
                            <button class="btn bg-default headerbtn  btn-nurse-task" id="btn_advnc_dtail"><i
                                    class="fa fa-rupee"></i> Advance Details</button>
                        </div>
                        <div class="col-md-2 padding_sm pull-right">
                            <button class="btn bg-default headerbtn  btn-nurse-task ip_action_button"
                                id="getYellowSheetServicesbtn" type="button" onclick="getYellowSheetServices(1)"><i
                                    id="getYellowSheetServicesspin" class="fa fa-list"></i> Billing</button>
                        </div>

                        <div class="col-md-2 padding_sm pull-right">
                            <button title="Room Management"
                                class="btn bg-default headerbtn  btn-nurse-task ip_action_button" id="btn_room_mgmt"><i
                                    class="fa fa-bed"></i>Room</button>
                        </div>
                        @php
                        $additional_room_request_status = \DB::table('config_head')
                        ->where('name','additional_room_request_status')->value('value');
                        @endphp
                        @if (!empty($additional_room_request_status) && $additional_room_request_status == 1)
                        <div class="col-md-2 padding_sm pull-right">
                            <button title="Additional Room Management"
                                class="btn bg-default headerbtn  btn-nurse-task ip_action_button" id="btn_additional_room_mgmt"><i
                                    class="fa fa-bed"></i>Additional Room</button>
                        </div>
                        @endif
                        @if($baby_btn_visibility)
                        <div class="col-md-2 padding_sm pull-right">
                            <button title="Baby Register" onclick="addBornBabyDetails({{ $patient_id }})"
                                class="btn bg-default headerbtn  btn-nurse-task ip_action_button" id="baby_reg_mag">+<i
                                 id="baby_reg_mag_spin"   class="fa fa-child"></i> Baby</button>
                        </div>

                        @endif


                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 pull-right" style="padding-top:5px;">
                        <div class="btn-group pull-right" id="btn_discharge_initiate_container">
                            <button type="button" id="btn_discharge_initiate"
                                class="btn bg-orange btn-nurse-task">Discharge
                                Initiate</button>
                            <button type="button"
                                class="btn bg-orange btn-nurse-task dropdown-toggle dropdown-toggle-split"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="height: 22px;">
                                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu">
                                <textarea style="margin-left:-46px;" id="discharge_remarks" placeholder="Remarks"></textarea>

                            </div>
                        </div>
                        <button style="display: none" class="btn bg-orange btn-nurse-task ip_action_button"
                            id="btn_sent_for_biling"><i class="fa fa-file-medical"></i> Send For Billing</button>
                        <button style="display: none" class="btn bg-orange btn-nurse-task ip_action_button"
                            id="btn_revert_discharge"><i class="fa fa-file-medical"></i> Revert Discharge</button>
                        <button style="display: none" class="btn bg-orange btn-nurse-task ip_action_button"
                            id="btn_revert_sent_for_biling"><i class="fa fa-file-medical"></i> Revert Sent For
                            Billing</button>
                    </div>

                </div>

                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="col-md-12 no-padding">
                    <div class="col-md-12 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-body clearfix">
                                <div class="col-md-12" style="padding: 2px;">
                                    <div class="card">
                                        <div class="nav-tabs-custom blue_theme_tab vertical_tab_wrapper no-margin"
                                            style="display: flex;">
                                            <ul
                                                class="nav nav-tabs vertical_tab  text-center col-md-2 no-padding no-border">
                                                <li class="active" style="border: 1px solid bisque !important;">
                                                    <a href="#tab1" data-toggle="tab" aria-expanded="true">
                                                        <div class="vertical_tab_icon"><i class="icon-notes"></i></div>
                                                        <span>
                                                            <h5><i class="fa fa-sticky-note-o" aria-hidden="true"></i>
                                                                Notes
                                                            </h5>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="" style="border: 1px solid bisque !important;">
                                                    <a href="#tab2" data-toggle="tab" aria-expanded="false">
                                                        <div class="vertical_tab_icon"><i class="icon-investigation"></i>
                                                        </div>
                                                        <span>
                                                            <h5><i class="fa fa-search" aria-hidden="true"></i>
                                                                Investigation</h5>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="" style="border: 1px solid bisque !important;">
                                                    <a href="#tab3" data-toggle="tab" aria-expanded="false">
                                                        <div class="vertical_tab_icon"><i class="icon-prescription"></i>
                                                        </div>
                                                        <span>
                                                            <h5><i class="fa fa-medkit" aria-hidden="true"></i>
                                                                Prescription</h5>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="ip_action_button" style="border: 1px solid bisque !important;">
                                                    <a href="#tab4" onclick="showMedicationChart();" data-toggle="tab"
                                                        aria-expanded="false">
                                                        <div class="vertical_tab_icon"><i
                                                                class="icon-medication-chart"></i>
                                                        </div>
                                                        <span>
                                                            <h5>
                                                                <i class="fa fa-medkit" aria-hidden="true"></i>
                                                                Administration
                                                            </h5>
                                                        </span>
                                                        <button onclick="gotoMedicationChart();" class="btn bg-primary"
                                                            title="Medication Chart" style="margin-left:50px;">
                                                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                                        </button>

                                                    </a>
                                                </li>
                                                <li class="ip_action_button" style="border: 1px solid bisque !important;">
                                                    <a href="#tab5" onclick="showIvFluidChart();" data-toggle="tab"
                                                        aria-expanded="false">
                                                        <div class="vertical_tab_icon"><i
                                                                class="icon-medication-chart"></i>
                                                        </div>
                                                        <span>
                                                            <h5><i class="fa fa-flask" aria-hidden="true"></i> I.V Fluid
                                                                Chart</h5>
                                                        </span>
                                                    </a>
                                                </li>

                                                <li class="ip_action_button" style="border: 1px solid bisque !important;">
                                                    <a href="#tab7" onclick="LoadMedicineReturnResults();"
                                                        data-toggle="tab" aria-expanded="false">
                                                        <div class="vertical_tab_icon"><i
                                                                class="icon-medicine-return"></i>
                                                        </div>
                                                        <span>
                                                            <h5><i class="fa fa-undo" aria-hidden="true"></i> Medicine
                                                                Return</h5>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="ip_action_button" style="border: 1px solid bisque !important;">
                                                    <a href="#tab8" data-toggle="tab" aria-expanded="false">
                                                        <div class="vertical_tab_icon"><i
                                                                class="icon-medicine-intake"></i>
                                                        </div>
                                                        <span>
                                                            <h5><i class="fa fa-arrows-v"></i> Intake/Output</h5>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="ip_action_button" style="border: 1px solid bisque !important;">
                                                    <a href="#tab6" data-toggle="tab" aria-expanded="false">
                                                        <div class="vertical_tab_icon"><i class="icon-diet"></i></div>
                                                        <span>
                                                            <h5><i class="fa fa-cutlery" aria-hidden="true"></i> Diet
                                                                Request</h5>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content col-md-10 no-padding">
                                                <div class="tab-pane fade active in" id="tab1">
                                                    <div class="box no-border no-margin ">
                                                        <div class=" bg-white clearfix nurse_patient_content_box">
                                                            <div class="col-md-12 padding_sm">
                                                                <input style="float:right; width:150px;" type="button"
                                                                    name="save_assesment" id="save_assesment"
                                                                    class="btn bg-primary"
                                                                    onclick="saveClinicalTemplate();" value="Save" />
                                                            </div>
                                                            <div class="col-md-12 padding_sm">
                                                                @include('Emr::emr.includes.patient_assessments')
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="tab2">
                                                    <div class="box no-border no-margin">
                                                        <div class=" bg-white clearfix nurse_patient_content_box">
                                                            <input style="float:right; width:150px;" type="button"
                                                                name="save_assesment" id="save_assesment"
                                                                class="btn bg-primary" onclick="saveInvestigation();"
                                                                value="Save" />
                                                            <div class="col-md-12 padding_sm">
                                                                @include('Emr::emr.includes.patient_investigations')
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="tab-pane fade" id="tab3">
                                                    <div class="box no-border no-margin">
                                                        <div class=" bg-white clearfix nurse_patient_content_box">
                                                            <input
                                                                style="float:right;
                                                                width: 150px;
                                                                margin-top: 10px;
                                                                position: absolute;
                                                                z-index: 99;
                                                                float: right !important;
                                                                margin-left: 84%;
                                                            "
                                                                type="button" name="save_prescription"
                                                                id="save_prescription" class="btn bg-primary"
                                                                onclick="savePrescription();" value="Save" />
                                                            <div class="col-md-12 padding_sm">
                                                                @include('Emr::emr.includes.patient_prescription')
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="tab-pane fade" id="tab4">
                                                    <div class="no-border no-margin">
                                                        <div class=" clearfix nurse_patient_content_box">
                                                            <div class="col-md-12 padding_sm"
                                                                id="medication_chart_content">
                                                                {!! $medication_chart !!}
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="tab-pane fade" id="tab5">
                                                    <div class="no-border no-margin">
                                                        <div class="clearfix ">
                                                            <div class="col-md-12 padding_sm" id="iv_chart_content">
                                                                {!! $iv_chart !!}
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="tab-pane fade" id="tab7">
                                                    <div class="box no-border no-margin">
                                                        <div class=" bg-white clearfix nurse_patient_content_box">
                                                            <div class="col-md-12 padding_sm">
                                                                @include('Emr::emr.includes.medicine_return')
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="tab-pane fade" id="tab8">
                                                    <div class="box no-border no-margin">
                                                        <div class="bg-white clearfix nurse_patient_content_box">
                                                            <div class="col-md-12 padding_sm">
                                                                @include('Emr::emr.includes.intake_output')
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="tab-pane fade" id="tab6">
                                                    <div class="box no-border no-margin">
                                                        <div class="bg-white clearfix nurse_patient_content_box">

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="pop-content-history" style="display:none;"></div>

    <div class="modal bs-example-modal-lg mkModalFloat in" tabindex="-1" role="dialog" id="nursing_modal"
        data-backdrop="static" aria-hidden="false">
        <div class="modal-dialog modal-lg" style="">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="nursing_modal_title">Nursing Modal</h4>
                </div>

                <!-- Modal body -->
                <div id="nursing_modal_body">

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">

                </div>

            </div>
        </div>
    </div>

    <div class="modal bs-example-modal-lg mkModalFloat in" tabindex="-1" role="dialog" id="advance_detail"
        data-backdrop="static" aria-hidden="false">
        <div class="modal-dialog modal-lg" style="">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg-green">
                    <h4 class="modal-title" id="advance_detail_title">Patient Pending Bills</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div id="advance_detail_body">

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">

                </div>

            </div>
        </div>
    </div>

    <div id="chnge_admit_dr" class="modal fade" role="dialog" style="z-index:1052 !important;">
        <div class="modal-dialog modal-lg" style="width:63%;">
            <!-- Modal content-->
            <div class="modal-content" style="height:260px;">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Admit Doctor</h4>
                </div>
                <div class="modal-body" id="chnge_dctr_body">
                    @include('Emr::nursing.ajax.change_admit')
                </div>
                <div class="modal-footer">
                    <div class="col-md-12" style="text-align:right">
                        <input type="button" class="btn btn-secondary" data-dismiss="modal" value="Close" />
                        <input type="button" class="btn btn-primary" onclick="saveAdmitDctr();" value="Save" />
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Death marking modal start -->
    <div id="mark_death_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Death Marking</h4>
                </div>
                <div class="modal-body" id="death_mark_details">
                    {!! Form::open(['death_mark_form', 'id' => 'death_mark_form']) !!}
                    {!! Form::label('confirmed_doctor', 'Confirmed Doctor') !!}
                    {!! Form::select(
                        'confirmed_doctor_id_hidden',
                        $doctor_list,
                        isset($selected_doctor[0]->admitting_doctor) ? $selected_doctor[0]->admitting_doctor : null,
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Confirmed Doctor',
                            'id' => 'confirmed_doctor_id_hidden',
                        ],
                    ) !!}


                    {!! Form::label('death_date_time', 'Death Date / Time *', []) !!}
                    {!! Form::text('death_date_time', null, ['class' => 'form-control']) !!}

                    {!! Form::label('death_reason', 'Death Reason *', []) !!}
                    {!! Form::text('death_reason', null, ['class' => 'form-control']) !!}

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-green" onclick="markDeath()"> <i class="fa fa-check"></i>
                        Mark Death</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!---------- Intake output chart----------------------->
    <div class="modal bs-example-modal-lg in" tabindex="-1" role="dialog" id="io_report_modal" data-backdrop="static"
        aria-hidden="false">
        <div class="modal-dialog modal-lg" style="width:80%;">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg-green">
                    <h4 class="modal-title" id="nursing_modal_title">Intake Output Report</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div id="io_report_modal_body" style="max-height:560px;overflow-y:scroll;position:relative;">

                </div>

            </div>
        </div>
    </div>

    <!-- Death marking modal end -->


@stop
@section('javascript_extra')

    {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}

    <script
        src="{{ asset('packages/extensionsvalley/emr/js/ns_patient_dashboard.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/clinical.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/investigation.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    {{-- <script src="{{ asset('packages/extensionsvalley/emr/js/prescription.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script> --}}
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/medication_chart.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/drug_reaction.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/new_born_baby.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>


    <script src="{{ asset('packages/extensionsvalley/emr/js/jquery-ui.min.js') }}" ></script>

    <script type="text/javascript">
        function goToSelectedPatient(patient_id) {
            window.location.href = $('#base_url').val() + "/emr/patient-view/" + patient_id;
        }
        @if ($iv_subcategory)
            window.iv_sub_cat_id = '{{ $iv_subcategory }}';
        @endif
    </script>
@endsection
