@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/nurse-module.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/progressive_search.css') }}" rel="stylesheet">

<style>
.block_list button {
    padding: 2px;
    font-size: 13px;
}

.block_list h5 {
    font-size: 13px;
}

.patient_image_contianer {
    width: 52px;
    height: 52px;
}

.block_list button {
    padding: 2px;
    font-size: 13px;
}

.block_list h5 {
    font-size: 13px;
}

.position_fixed_header {
    border-radius: 0px;
}

.ajaxSearchBox {
    display: none;
    text-align: left;
    list-style: none;
    cursor: pointer;
    max-height: 350px;
    margin: 20px 0px 0px 0px;
    DischargeSummaryReport.blade.php overflow-y: auto;
    width: auto;
    z-index: 599;
    position: absolute;
    background: #ffffff;
    border-radius: 3px;
    border: 1px solid rgba(0, 0, 0, 0.3);

}

.searched_items a {
    color: #555;
}

.custom_floatlabel {
    color: #686868;
}

.liHover {
    background: #cde3eb;
}

.custom_floatlabel {
    color: #686868;
}

.select_button>li.active {
    background: #5f89e4;
    color: #FFF;
}

.select_button>li {
    background: #aed8e7;
    color: rgb(0, 0, 0);
}

.popover {
    max-width: 450px;
}

.active {
    background-color: aqua;
}

.treeview.active {
    background-color: unset !important;
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="user_id" value="{{$user_id}}">
<input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">

<div class="right_col">
    <div class="container-fluid" style="padding: 15px;">
        <div class="row codfox_container">
            <div class="col-md-12 no-padding">

                <div class="col-md-3 padding_sm" style=" position: relative;">
                    <div class="card">
                        <div class="card_body  bg-white">

                            <div class="col-md-12 padding_sm btn-group dropleft pull-right">
                                <button class="btn purple_bg btn-lg btn-block btn-secondary dropdown-toggle"
                                    id="default_nursing_station" value="{{trim($default_nursing_station)}}"
                                    style="padding: 10px; font-size: 12px; font-weight: bold;" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-home float-left"></i>
                                    {{trim($default_nursing_station_name)}}
                                </button>
                                <div class="dropdown-menu theadscroll" id="NursingStationPopup"
                                    style="height: 542px;width: 100%;z-index:99999;position: absolute;">
                                </div>
                            </div>
                            <br>
                            <div id="roomTypeChart_graph-container">
                                <canvas id="roomTypeChart" width="230" height="230"
                                    style="margin: 10px 10px 10px 0"></canvas>
                            </div>


                            <div id="roomChart_graph-container" class="poplink" style="margin-top:-15px;">
                                <canvas id="roomChart" width="130" height="130"
                                    style="margin: 10px 10px 10px 50px !important"></canvas>
                            </div>
                            <div id="room_status_legend_container"></div>

                            <div class="pop-content" id="popRoomchart" style="width:250px; !important">
                            </div>

                            <br>


                        </div>
                    </div>

                </div>

                <div class="col-md-9 no-padding">
                    <div class="col-md-12 padding_sm">
                        <div class="card">
                            <h4 style="" class="">
                                <b style="color:darkblue">Admitted Patients</b>
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm" style="margin-bottom: 10px;">
                        <div class="card">
                            <div class="card_body"
                                style="background-color:#ededed !important;border-radius:4px !important;border:none;">
                                <div class="col-md-2" style="width:102px;">
                                    <span style="color:rgb(80, 80, 80);">Global search</span>
                                    <label class="switch">
                                        <input type="checkbox" id="global_search_flag">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="col-md-6" id="table_search">
                                    <div class="col-md-7 padding_sm">

                                        <span style="color:rgb(80, 80, 80);">Search</span>
                                        <div class="clearfix"></div>
                                        <div class="input-group">
                                            <input type="text" name="patient_search_txt" id="myInput" autocomplete="off"
                                                onkeyup="myFunction('searchby','myInput','admitted_patients_data');"
                                                class="form-control">
                                            <div class="input-group-btn">
                                                <button style="height: 23px;
                                            width: 30px;" class="btn light_purple_bg"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 padding_sm">
                                        <span style="color:rgb(80, 80, 80);">Search By</span>
                                        <div class="clearfix"></div>
                                        <div class="btn-group" data-toggle="buttons">

                                            <input checked="checked" type="radio" id="ip_name_check" value="1"
                                                name="searchby"> Name

                                            <input style="margin-left:10px" type="radio" id="opnumber_checkbox"
                                                value="3" name="searchby" checked=""> UHID

                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-2" style="width:152px;">
                                    <span style="color:rgb(80, 80, 80);">Show vacant beds</span>
                                    <label class="switch">
                                        <input type="checkbox" id="show_vacent_beds">
                                        <span class="slider round"></span>
                                    </label>
                                </div>


                                <div class="col-md-6" id="global_search" style="display: none;">

                                    <div class="input-group custom-float-label-control">
                                        <span style="color:rgb(80, 80, 80);">General Patient Search</span>
                                        <input class="form-control" value="" autocomplete="off" type="text"
                                            placeholder="" id="op_no" name="op_no" />
                                        <div id="OpAjaxDiv" class="ajaxSearchBox"
                                            style="z-index:9999;width:286px;min-height: 410px;"></div>
                                        <input value="" type="hidden" name="op_no_hidden" value="" id="op_no_hidden">
                                    </div>
                                </div>
                                @if($op_patient_list == 1)
                                <div class="col-md-1 padding_sm" style="margin-left: 89px;">
                                    <button type="button" onclick="op_patient_list()" id="op_patients"
                                        style="height: 31px;" class="btn btn-primary btn-block">OP Patients</button>
                                </div>
                                @endif
                                <div class="col-md-1 pull-right">
                                    <div class="dropdown" style="float: right;">
                                        {{-- <button class="btn bg-primary" style="
                                    width: 150px;
                                    height: 30px;font-size:12px;" onclick="openRoomRequestList();">
                                    <i class="fa fa-bed"></i> Room requests
                                    <span class="badge bg-orange pull-right" id="room_request_count">0</span>
                                   </button> --}}
                                        <button class="btn bg-blue" style="
                                    width: 30px;
                                    height: 30px;font-size:12px;margin-right:-7px;"
                                            onclick="$('#refresh_dashboard').addClass('fa-spin');window.location.reload();">
                                            <i id='refresh_dashboard' class="fa fa-refresh"></i>

                                        </button>
                                    </div>
                                    {{-- <div class="dropdown" style="float:right">
                                    <button style="
                                        width: 150px;
                                        height: 30px;font-size:12px;" class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-bed"> Room Types</i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                    </div>
                                </div> --}}
                                </div>

                                <br>
                                <br>
                                <br>
                                @php
                                $bed_status_bg_array = [];
                                $bed_status_font_color_array = [];
                                foreach($patient_bed_status as $bed_status){
                                $bed_status_bg_array[$bed_status->value] = $bed_status->value_string;
                                $bed_status_font_color_array[$bed_status->value] = $bed_status->font_color;
                                }
                                @endphp
                                <div class="col-sm-12 label_list">
                                    <div class="col-sm-1 padding_xs" style="width:12%;" onclick="applyFilter(0);">
                                        <span class="btn bg-teal-active btn-block">
                                            <div class="row no-margin">
                                                <span class="col-md-9 no-padding">All</span>
                                                <span id="all_count" style="top: 1px !important;"
                                                    class="badge col-sm-3 no-padding pull-right">0</span>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-sm-1 padding_xs" style="width:14.5%;" onclick="applyFilter(2);">
                                        <span class="btn btn-block"
                                            style="background-color:{{$bed_status_bg_array[2]}};color:{{$bed_status_font_color_array[2]}};">
                                            <div class="row no-margin">
                                                <span class="col-md-9 no-padding">Awaiting Checkin</span>
                                                <span id="admitted_but_not_occupied_count" style="top: 1px !important;"
                                                    class="badge col-sm-2 no-padding pull-right">0</span>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-sm-1 padding_xs" style="width:12%;" onclick="applyFilter(3);">
                                        <span class="btn btn-block"
                                            style="background-color:{{$bed_status_bg_array[3]}};color:{{$bed_status_font_color_array[3]}};">
                                            <div class="row no-margin">
                                                <span class="col-md-9 no-padding">Checked In</span>
                                                <span id="checked_in_count" style="top: 1px !important;"
                                                    class="badge col-sm-2 no-padding pull-right">0</span>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-sm-1 padding_xs" style="width:15.5%;" onclick="applyFilter(4);">
                                        <span class="btn btn-block"
                                            style="background-color:{{$bed_status_bg_array[4]}};color:{{$bed_status_font_color_array[4]}};">
                                            <div class="row no-margin">
                                                <span class="col-md-9 no-padding">Transfer Requested</span>
                                                <span id="transfer_requested_count" style="top: 1px !important;"
                                                    class="badge col-sm-2 no-padding pull-right">0</span>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-sm-1 padding_xs" style="width:15.5%;" onclick="applyFilter(5);">
                                        <span class="btn btn-block"
                                            style="background-color:{{$bed_status_bg_array[5]}};color:{{$bed_status_font_color_array[5]}};">
                                            <div class="row no-margin">
                                                <span class="col-md-9 no-padding">Discharge Intimated</span>
                                                <span id="discharge_intimated_count" style="top: 1px !important;"
                                                    class="badge col-sm-2 no-padding pull-right">0</span>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-sm-1 padding_xs" style="width:14.5%;" onclick="applyFilter(6);">
                                        <span class="btn btn-block"
                                            style="background-color:{{$bed_status_bg_array[6]}};color:{{$bed_status_font_color_array[6]}};">
                                            <div class="row no-margin">
                                                <span class="col-md-9 no-padding">Sent for Billing</span>
                                                <span id="sent_for_billing_count" style="top: 1px !important;"
                                                    class="badge col-sm-2 no-padding pull-right">0</span>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-sm-1 padding_xs" style="width:14.5%;" onclick="applyFilter(7);">
                                        <span class="btn btn-block"
                                            style="background-color:{{$bed_status_bg_array[7]}};color:{{$bed_status_font_color_array[7]}};">
                                            <div class="row no-margin">
                                                <span class="col-md-9 no-padding">Bill Ready</span>
                                                <span id="bill_generated_count" style="top: 1px !important;"
                                                    class="badge col-sm-2 no-padding pull-right">0</span>
                                            </div>
                                        </span>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 padding_sm">
                        <div class="card">

                            <div class="card_body  bg-white">
                                <div class="theadscroll theadfix_wrapper always-visible"
                                    style="position:relative; height: 540px;">
                                    <table id="admitted_patients_data"
                                        class="table table-bordered table_sm  theadfix_wrapper admitted_patients_data">
                                        <thead>
                                            <tr class="bg-green">

                                                <th onclick="sortTable('admitted_patients_data',0,2)" style="width:16%">
                                                    Room Type</th>
                                                <th onclick="sortTable('admitted_patients_data',1,2)" style="width:16%">
                                                    Patient Name</th>
                                                <th onclick="sortTable('admitted_patients_data',2,2)" style="width:16%">
                                                    IP Number</th>
                                                <th onclick="sortTable('admitted_patients_data',3,2)" style="width:8%">
                                                    UHID</th>
                                                <th onclick="sortTable('admitted_patients_data',4,2)" style="width:5%">
                                                    Ag/Gen</th>
                                                <th onclick="sortTable('admitted_patients_data',5,2)" style="width:15%">
                                                    Place</th>
                                                <th onclick="sortTable('admitted_patients_data',6,1)" style="width:10%">
                                                    Phone</th>
                                                <th onclick="sortTable('admitted_patients_data',7,2)" style="width:13%">
                                                    Admitted on</th>
                                                <th onclick="sortTable('admitted_patients_data',8,2)" style="width:15%">
                                                    Admitted By</th>
                                                <th onclick="sortTable('admitted_patients_data',9,2)" style="width:8%">
                                                    Room</th>
                                                <th style="width:5%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id='admitted_patients_list_data' style="font-weight: 500;">

                                        </tbody>
                                    </table>
                                </div>
                                <span style="float:right;margin-top:10px;">
                                    <i class="fa fa-square" style="color: #8d6911;"></i> Retain Bed
                                </span>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>
                    </div>

                </div>


                {{--  <div class="col-md-3 padding_sm" style=" position: relative;">
                <div class="col-md-12 padding_sm btn-group dropleft">
                    <button class="btn purple_bg btn-lg btn-block btn-secondary dropdown-toggle" id="default_nursing_station" value="{{trim($default_nursing_station[0]->station_id)}}"
                style="padding: 10px; font-size: 14px; font-weight: bold;" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">{{trim($default_nursing_station[0]->location_name)}}</button>
                <div class="dropdown-menu theadscroll" id="NursingStationPopup"
                    style="height: 542px;width: 100%;z-index:99999;position: absolute;">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="ht10"></div>
            <span id="rooms_list_data">

            </span>
            <i class="btn fa fa-info-circle bg-primary" onclick="showBedStatusModal();" style="float:right;"
                aria-hidden="true"></i>
        </div> --}}

    </div>

</div>
</div>
</div>

<div id="room_transfer_request_list_modal" class="modal fade " role="dialog">
    <div class="modal-dialog">

        <div class="modal-content modal-sm" style="width:750px;">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Room Tranfer Request
                </h4>
            </div>
            <div class="modal-body" id="room_transfer_request_data">

            </div>
        </div>
    </div>
</div>

<div id="room_transfer_request_choose_bed_modal" class="modal fade " role="dialog">
    <div class="modal-dialog">

        <div class="modal-content modal-sm" style="width:100%;">
            <div class="modal-header" style="background:#f0ae63; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Select Bed
                </h4>
            </div>
            <div class="modal-body theadscroll" id="room_transfer_request_choose_bed"
                style="position: relative;height:350px;">

            </div>
            <div class="modal-footer">
                <button style="float:right" type="button" onclick="admitRoomRequest();" class="btn bg-primary"><i
                        class="fa fa-save"></i> Admit Patient</button>
            </div>
        </div>
    </div>
</div>
<div id="op_patient_list" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width: fit-content;">

        <div class="modal-content modal-sm" style="width: 1200px;">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">OP Patients List
                </h4>
            </div>
            <div class="modal-body" id="" style="height: 555px;">
                <div class="col-md-2 padding_sm">
                    <div class="mate-input-box">
                        <label>From Date</label>
                        <div class="clearfix"></div>
                        <input type="text" value="<?= date('M-d-Y'); ?>" id="from_date" name="from_date"
                            autocomplete="off" class="form-control datepicker">
                    </div>
                </div>
                <div class="col-md-2 padding_sm">
                    <div class="mate-input-box">
                        <label>To Date</label>
                        <div class="clearfix"></div>
                        <input type="text" value="<?= date('M-d-Y'); ?>" id="to_date" name="to_date" autocomplete="off"
                            class="form-control datepicker">
                    </div>
                </div>
                <div class="col-md-2" style="width:102px;">
                    <span style="color:rgb(0 0 0); font-size: 12px; font-weight: 700;">Global search</span>
                    <label class="switch">
                        <input type="checkbox" id="global_search_flag_op_search">
                        <span class="slider round"></span>
                    </label>
                </div>
                <div class="col-md-3 padding_sm patient_global_search hidden">
                    <div class="mate-input-box">
                        <label for="">Patient Name / UHID</label>
                        <div class="clearfix"></div>
                        <input type="text" autocomplete="off" class="form-control hidden_search" name="patient_name"
                            id="patient_name" value="" autocomplete="false">
                        <input type="hidden" class="form-control" name="patient_name_hidden" id="patient_name_hidden"
                            value="">
                        <div id="patient_nameAjaxDiv" class="ajaxSearchBox"
                            style="margin: 14px 0px 0px 0px !important;z-index: 9999999;max-height: 250px !important">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding_sm patient_local_search ">                    
                    <div class="col-md-7 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Search</label>
                            <div class="clearfix"></div>
                            <input type="text" name="op_patient_search_txt" id="op_patient_search_txt"
                                autocomplete="off"
                                onkeyup="myFunction('searchop_by','op_patient_search_txt','patients_list_data');"
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-md-5 padding_sm">
                        <span style="color:rgb(0 0 0); font-size: 12px; font-weight: 700;">Search By</span>
                        <div class="clearfix"></div>
                        <div class="btn-group" data-toggle="buttons">
                            <input checked="checked" type="radio" id="ip_name_check" value="0" name="searchop_by"> Name
                            <input style="margin-left:10px" type="radio" id="opnumber_checkbox" value="1"
                                name="searchop_by"> UHID
                        </div>
                    </div>
                </div>
                <div class="col-md-2 pull-right" style="margin-top: 12px;">
                    <button type="button" id="btn_search_op_patient_list" onclick="search_patient_list();" class="btn btn-success btn-block">
                    <i class="fa fa-search"></i> Search
                    </button>
                </div>

                <!-- SEARCH RESULT DIV  -->
                <div id="op_patient_list_data" class="col-md-12 padding_sm theadscroll" style="position:relative; height:65vh;">

                </div>
            </div>
        </div>
    </div>
</div>

@include('Emr::nursing.ajax.bed_status_list_modal')

@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>

<script src="{{asset("packages/extensionsvalley/emr/js/ns_dashboard.js")}}"></script>

<script src="{{asset("packages/extensionsvalley/dashboard/js/chart/Chart.min.js")}}"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-piechart-outlabels"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript">
function goToSelectedPatient(patient_id) {
    window.location.href = $('#base_url').val() + "/emr/patient-view/" + patient_id;
}
</script>
@endsection