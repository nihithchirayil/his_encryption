<style>
    .timepickerclass{
        font-weight: 600;
}
.timepickerclass_default{
color: #a8a8a8 !important;
  }

  .bootstrap-datetimepicker-widget .form-control, .bootstrap-datetimepicker-widget .btn{
        box-shadow: none !important;
    }
    .bootstrap-datetimepicker-widget .timepicker-hour, .bootstrap-datetimepicker-widget .timepicker-minute, .bootstrap-datetimepicker-widget .timepicker-second {
        width: 70% !important;
    }

    .bootstrap-datetimepicker-widget td span {
        height: 34px !important;
        line-height: 34px !important;
    }
</style>

@if(sizeof($res)>0)
{!!Form::open(['name'=>'frm-ivchart','id'=>'frm-ivchart','method'=>'post']) !!}

@php
    $saved_id_array = array();
    $saved_data = array();
    $stop_flag = 0;
    foreach($res_saved as $data){
        array_push($saved_id_array,$data->id);
        $saved_data[$data->id] = [
        'id' => $data->id,
        'medication_approval_id' => $data->medication_approval_id,
        'rate_and_duration' => $data->rate_and_duration,
        'volume' => $data->volume,
        'start_at' => $data->start_at,
        'stop_at' => $data->stop_at,
    ];
    }
    // print_r($res);
    // echo '<pre>';
    // print_r($saved_id_array);
    // print_r($saved_data);
    // print_r($res);

    // echo $data->id;

    $iv_rate = '';
    $start_at = '';
    $stop_at = '';
    $stop_at_time = '';
    $start_at_time = '';
    $iv_volume = '';
    $i = 1;
@endphp




@foreach($res as $data)

    @if(in_array($data->iv_chart_id,$saved_id_array))
    @php

        // echo '<pre>';
        // print_r($data);
        // exit;

        $iv_volume =$saved_data[$data->iv_chart_id]['volume'];
        $iv_rate = ($saved_data[$data->iv_chart_id]['rate_and_duration'] != '')? $saved_data[$data->iv_chart_id]['rate_and_duration'] :$data->iv_rate;
        $start_at =$saved_data[$data->iv_chart_id]['start_at'];
        $stop_at =$saved_data[$data->iv_chart_id]['stop_at'];


        $stop_flag = $data->stop_flag;


        if($start_at != ''){

            $start_at = date(\WebConf::getConfig('date_format_web'),strtotime($start_at));
            $start_at_time = date('h:i A',strtotime($saved_data[$data->iv_chart_id]['start_at']));

        }elseif($data->start_at != ''){

            $start_at = date(\WebConf::getConfig('date_format_web'),strtotime($data->start_at));
            $start_at_time = date('h:i A',strtotime($data->start_at));
        }else{
            $start_at = '';
            $stop_at_time = '';
        }

        if($stop_at != ''){
            $stop_at = date(\WebConf::getConfig('date_format_web'),strtotime($stop_at));
            $stop_at_time = date('h:i A',strtotime($saved_data[$data->iv_chart_id]['stop_at']));

        }elseif($data->stop_at != ''){
            $stop_at = date(\WebConf::getConfig('date_format_web'),strtotime($data->stop_at));
            $stop_at_time = date('h:i A',strtotime($data->stop_at));
        }else{
            $stop_at = '';
            $stop_at_time = '';
        }




    @endphp
    @endif

<div class="col-md-12 padding_sm no-border no-margin">
    <div class="col-md-4 padding_sm box-body clearfix" style="margin-top: 6px;">
        <table class="table no-margin no-border table-striped table_sm" style="border:1px solid;font-size:12px;">
            <tbody>
                <tr data-toggle="collapse" data-target=".demo">
                    <td colspan="1" style="width: 11%">
                        <button style="width: 17px;height: 17px;text-align: center;padding: 0px;" title="Detail View" type="button" data-toggle="collapse" data-target=".demo{{$data->medication_detail_id}}" class="btn btn-sm bg-green" >
                            <i class="fa fa-arrow-down" aria-hidden="true"></i>
                        </button>
                    </td>
                    <td colspan="3" style="text-align: left !important;width: 74%" class=""><b>{{$data->medicine}}</b></td>
                    <td style="text-align:right;width: 15%">
                        <div class="col-md-12 padding_sm" style="display: table">
                            <span style="table-row">

                                <span style="table-cell">
                                    @if($data->stop_flag != '')
                                        <b style="color:red;">Stopped</b>
                                    @else
                                        <button style="margin-right: 7px;" title="Stop Medicine" type="button" class="btn btn-sm bg-red medication_stop" onclick="stopMedicine('{{$data->medication_detail_id}}');">
                                            <i class="fa fa-stop-circle" aria-hidden="true"></i>
                                        </button>
                                    @endif
                                </span>
                            </span>
                        </div>

                    </td>
                </tr>
                <span>
                <tr id="" class="collapse demo{{$data->medication_detail_id}}">
                    <td colspan="1"><b>GenericName :</b></td>
                    <td colspan="3" class="columcontain">--------</td>
                    <td></td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td style="width:25%"><b>Dose</b></td>
                    <td style="width:25%"><b>Route</b></td>
                    <td style="width:25%"><b>Frequency</b></td>
                    <td style="width:25%"><b>Directions </b></td>
                    <td></td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td class="columcontain">{{$data->dose}}</td>
                    <td class="columcontain">{{$data->route}}</td>
                    <td class="columcontain">{{$data->frequency}}</td>
                    <td class="columcontain">{{$data->notes}}</td>
                    <td></td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td><b>Start At</b></td>
                    <td><b>Stopped At</b></td>
                    <td><b>Doctor Name</b></td>
                    <td rowspan="2"></td>
                    <td></td>
                </tr>
                <tr class="collapse demo{{$data->medication_detail_id}}">
                    <td class="columcontain">
                        {{date(\WebConf::getConfig('datetime_format_web'),strtotime($data->start_at))}}
                    </td>
                    <td class="columcontain">
                        @if($data->stop_flag != '')
                            {{date(\WebConf::getConfig('datetime_format_web'),strtotime($data->stop_date))}}
                        @endif
                    </td>
                    <td class="columcontain">{{$data->approved_dr}}</td>
                    <td></td>
                </tr>
                </span>
            </tbody>
        </table>
    </div>


    <div class="col-md-8 bg-white padding_sm">


            <input type="hidden" name="mda_id[]" id='{{$data->id}}_mda_id' value="{{$data->id}}"/>
            <input type="hidden" name="medication_detail_id[]" id='{{$data->medication_detail_id}}_medication_detail_id' value="{{$data->medication_detail_id}}"/>
            <input type="hidden" name="iv_chart_id[]" id='{{$data->iv_chart_id}}_iv_chart_id' value="{{$data->iv_chart_id}}"/>
            <div style="padding-top:5px;">

                <div class="col-md-1 padding_sm">
                    <input @if($data->stop_flag != '')disabled="disabled"@endif type="text" value="{{$iv_volume}}"  name="iv_volume[]" id="{{$i}}_iv_volume" class="form-control timepickerclass" @if($stop_flag == 1) readonly="readonly"@endif>
                    <label style="margin-top: 3px;margin-left:5px;" for="{{$i}}_iv_volume"><b>Volume</b></label>
                </div>

                <div class="col-md-2 padding_sm">
                    <input style="font-weight:600px;" @if($data->stop_flag != '')disabled="disabled"@endif  type="text" value="{{$iv_rate}}" name="iv_search_rate[]" autocomplete="off" id="{{$i}}_iv_search_rate" class="form-control iv_search_rate timepickerclass"  @if($stop_flag == 1) readonly="readonly"@endif>

                    <!-- iv rate List -->
                    <div class="iv-rate-list-div" id="{{$i}}_iv-rate-list-div" style="display: none;width:150px;">
                        <a style="float: left;" class="close_btn_iv_rate_search">X</a>
                        <div class="iv_rate_theadscroll" id="{{$i}}_iv_rate_theadscroll" style="position: relative;">
                            <table id="{{$i}}_IvRateTable"  class="table table-bordered-none no-margin table_sm table-striped presc_theadfix_wrapper">
                                <tbody id="{{$i}}_ListIvRateSearchData">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- iv rate List -->
                    <label style="margin-top: 3px;margin-left:5px;" for="{{$i}}_iv_rate"><b>Rate/Duration</b></label>




                </div>
                <div class="col-md-4 padding_sm" style="text-align:center;">


                        <div class="col-md-7 padding_sm" style="text-align:center">
                            <input
                             id="{{$i}}_start_at"
                             @if($data->stop_flag != '')disabled="disabled"@endif
                            value="{{$start_at}}"
                            name="start_at[]"
                            type="text"  @if($stop_flag == 1) readonly="readonly"@endif
                            autocomplete="off"
                            class="form-control timepickerclass datepicker output"
                            data-attr="date"
                            placeholder="Date">
                        </div>
                        <div class="col-md-5 padding_sm" style="text-align:center">
                            <input
                            type="text"
                            @if($data->stop_flag != '')disabled="disabled"@endif
                            autocomplete="off"
                            id="{{$i}}_start_at_time"
                            name="start_at_time[]"
                            class="form-control timepickerclass output timepicker"
                            data-attr="date"
                            value="{{$start_at_time}}"
                            placeholder="Time">
                        </div>
                        <label style="margin-top: 3px;margin-left:5px;" for="{{$i}}_start_at"><b>Time Started</b></label>
                </div>
                <div class="col-md-4 padding_sm" style="text-align:center;">
                        <div class="col-md-7 padding_sm" style="text-align:center">
                            <input
                            id="{{$i}}_stop_at"
                              @if($data->stop_flag != '')disabled="disabled"@endif
                              value="{{$stop_at}}"
                              name="stop_at[]"
                              type="text"
                              @if($stop_flag == 1) readonly="readonly"@endif
                              autocomplete="off"
                              class="form-control timepickerclass datepicker output"
                              data-attr="date"

                              placeholder="Date">
                        </div>
                        <div class="col-md-5 padding_sm" style="text-align:center;">
                            <input
                                type="text"
                                @if($data->stop_flag != '')disabled="disabled"@endif
                                autocomplete="off"
                                id="{{$i}}_stop_at_time"
                                name="stop_at_time[]"
                                class="form-control timepickerclass output timepicker"
                                data-attr="date"
                                placeholder="Time"
                                value="{{$stop_at_time}}"
                            >
                        </div>
                        <label style="margin-top: 3px;margin-left:5px;" for="{{$i}}_stop_at"><b>Time Ended</b></label>
                </div>

                <div class="col-md-1 padding_sm">
                    <button
                        onclick="toggleChangeVolume('{{$i}}','{{$data->id}}','{{$data->iv_chart_id}}');" title="Change volume"
                        @if($data->stop_flag != '') disabled @endif
                        type="button"
                        class="btn bg-info btn-block"
                        name="IvChangevolume[]"
                        id="IvChangevolume">
                        <i class="fa fa-reply" aria-hidden="true"></i>
                    </button>
                    <label style="margin-top: 3px;margin-left:5px;" for="t"><b>Change rate/hr</b></label>
                </div>
            </div>


    </div>

</div>
@php
    $i++;
@endphp
@endforeach
<div class="col-md-12" style="padding-top: 10px;">
    <div class="col-md-2 pull-right">
        <button type="button" onclick="saveIvChart();" class="btn bg-primary btn-block" name="SaveIvFluid" id="SaveIvFluid">
            <i class="fa fa-save"></i> Save
        </button>
    </div>
    <div class="col-md-2 pull-right">

    </div>
</div>
{!! Form::token() !!}
{!! Form::close() !!}
@else
<div class="col-md-12">
    <label>No Approved Medications Found</label>
</div>
@endif


