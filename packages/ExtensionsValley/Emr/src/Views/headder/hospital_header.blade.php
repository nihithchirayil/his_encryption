<?php
            $company_name = ''; $company_address = ''; $company_city = '';$company_pincode = '';
            $company_details = \DB::select("select hospital_header,name,address,city,pincode,phone,booking_no from company LIMIT 1 OFFSET 0");
            $company_name = $company_details[0]->name;
            $company_address = $company_details[0]->address;
            $company_city = $company_details[0]->city;
            $company_pincode = $company_details[0]->pincode;
            $phone = $company_details[0]->phone;
            $booking_no = $company_details[0]->booking_no;
            $query = "select encode(hospital_logo,'base64') hospital_logo,encode(nabh_logo,'base64') nabh_logo from company";
            $result = \DB::select($query);
            $hospital_logo =(isset($result) && isset($result[0])) ? ($result[0]->hospital_logo) : '';
            $nabh_logo =(isset($result) && isset($result[0])) ? ($result[0]->nabh_logo) : '';
    ?>
<!--     <td colspan="3" style="margin-top: 50px;padding: 3px;border: 1px solid #FFF;" width="100%" align="center" valign="top;">
               <b style="font-size: bold; font-size:15px;">DAYA GENERAL HOSPITAL LTD &amp; SPECIALITY SURGICAL CENTER</b><br>
               Shoranur Road, Near Viyyur Bridge, Thrissur 22 <br>
               Phone : 0487 - 2330543, 2334690, 2321725 <br>
               Booking No: 0487-2325537
     </td>-->


    <td width="10%" style="padding: 3px; vertical-align: middle;" align="center" valign="middle">
        <img alt="DAYA GENERAL HOSPITAL" src='data:image/jpg;base64,{{$hospital_logo}}'>
    </td>

     <td style="margin-top: 50px;padding: 3px;border: 1px solid #FFF; text-align: center;" align="center" valign="top" >

        {!! $company_details[0]->hospital_header !!}
 </td>
 @if (Site::getConfig('show_nabh_logo') == '1')
      <td width="10%" style="padding: 3px; vertical-align: middle;" align="center" valign="middle">
          <?php if($nabh_logo!=''){ ?>
          <img style="width:40%" alt="{!!strtolower(env('APP_NAME'))!!}" src='data:image/jpg;base64,{{$nabh_logo}}'>
          <?php }else if(file_exists(public_path().\Site::getConfig("hospital_logo_nabh"))){ ?>
          <img style="width:40%" alt="{!!strtolower(env('APP_NAME'))!!}" src='data:image/jpg;base64,{!! base64_encode(file_get_contents(public_path().\Site::getConfig("hospital_logo_nabh"))) !!}'>
          <?php } ?>
      </td>
           @endif
    <?php } else{ ?>
        <td width="10%" style="padding: 3px; vertical-align: middle;" align="center" valign="middle">
              <img alt="{!!strtolower(env('APP_NAME'))!!}" src='data:image/jpg;base64,{{$hospital_logo}}'>
           </td>
     <td style="margin-top: 50px;padding: 3px;border: 1px solid #FFF; text-align: center;" align="center" valign="top" >

               <b style="font-size: bold; font-size:15px;">{{$company_name}}</b><br>
               {{$company_address}},{{$company_city}} Pin : {{$company_pincode}}<br>
               Phone : {{$phone}} <br>
               Booking No: {{$booking_no}}
 </td>

