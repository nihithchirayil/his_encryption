@if(sizeof($gynec_details)>0)
    <style>
        b{
            font-size:12px !important;
        }
    </style>
    @foreach($gynec_details as $data)
        <div class="col-md-12" style="border:1px solid:gainsboro;border-radius:3px;">
            <i class="fa fa-calendar"></i> {{date('M-d-Y h:i A',strtotime($data->created_at))}}<br>
            @if($data->lmp!='')
                <b>LMP:@if($data->pmp != ''){{date('M-d-Y',strtotime($data->lmp))}}@endif</b><br>
                <b>PMP:@if($data->pmp != ''){{date('M-d-Y',strtotime($data->pmp))}}@endif</b><br>
            @endif
            @if($data->presenting_complaints!='')
                <b>Presenting Complaints:</b><br>
                {{$data->presenting_complaints}}<br>
            @endif
            @if($data->local_examinations!='')
                <b>Local Examinations:</b><br>
                {{$data->local_examinations}}<br>
            @endif
            @if($data->usg!='')
                <b>USG:</b><br>
                {{$data->usg}}<br>
            @endif
            @if($data->provisional_diagnosis!='')
                <b>Provisional Diagnosis:</b><br>
                {{$data->provisional_diagnosis}}<br>
            @endif
            @if($data->plan_of_care!='')
                <b>Plan of Care:</b><br>
                {{$data->plan_of_care}}<br>
            @endif
            @if($data->follow_up_care!='')
                <b>Follow up Care:</b><br>
                {{$data->follow_up_care}}<br>
            @endif
            @if($data->special_care!='')
                <b>Special Care:</b><br>
                {{$data->special_care}}<br>
            @endif
            <br>
        </div>
    @endforeach
@endif
