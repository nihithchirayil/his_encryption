@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/view_patient.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/multi_tab_setup.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/common_controls.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/antinatal/css/antinatal.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">


@endsection
@section('content-area')

<!--  modal boxes  -->
@include('Emr::view_patient.custom_modals_lite')

<div class="right_col" role="main">
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="emr_changes" value="{{ @$emr_changes ? $emr_changes : ''}}">
    <input type="hidden" id="doctor_id" value="{{ $doctor_id }}">
    <input type="hidden" id="default_note_form_id" value="{{ $default_note_form_id }}">
    <input type="hidden" id="patient_id" value="">
    <input type="hidden" id="booking_id" value="">
    <input type="hidden" id="visit_id" value="">
    <input type="hidden" id="visit_type" value="">
    <input type="hidden" id="encounter_id" value="">
    <input type="hidden" id="ca_head_id" value="">
    <input type="hidden" id="investigation_head_id" value="">
    <input type="hidden" id="prescription_head_id" value="">
    <input type="hidden" id="weight" value="2">
    <input type="hidden" id="temperature" value="9">
    <input type="hidden" id="height" value="4">
    <input type="hidden" id="vital_batch_no" value="">
    <input type="hidden" id="pacs_viewer_prefix" value="{{ $pacs_viewer_prefix }}">
    <input type="hidden" id="pacs_report_prefix" value="{{ $pacs_report_prefix }}">
    <input type="hidden" id="print_dialog_config" value="{{ $print_dialog_config }}">
    <input type="hidden" id="antinatal_id" value="">
    <input type="hidden" id="quantity_auto_calculation_config" value="{{$quantity_auto_calculation_config}}">

    <a class="toggle-ip-op-list-btn toggleIpOpListBtn leftArrow">
        <span class="toggle-ip-op-list-btn-icon">
            <i class="fa fa-arrow-left"></i>
        </span>
    </a>
    @include('Emr::antinatal.ip_op_list')
    @include('Emr::antinatal.patient_visit_master')
    @include('Emr::doctor_notes.doctor_notes_modal')

</div>

<div id="antinatal_history_modal" class="modal fade " role="dialog" style="z-index:1052;">
    <div class="modal-dialog" style="width:52%;">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">History</h4>
            </div>
            <div class="modal-body" style="min-height:560px;">
                <div class="row" id="antinatal_history_modal_data">

                </div>
            </div>
        </div>

    </div>
</div>


@stop
@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/accounts/default/javascript/jquery-ui.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/antinatal/js/antinatal.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>


<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/allergy_vitals_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/multi_tab_setup.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/investigation_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/prescription_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/notes_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/manage-document.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/inv_result_entry.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/addnewinvestigationpopup_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
src="{{ asset('packages/extensionsvalley/emr/js/doctor_notes.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
