<div class="theadscroll" style="position:absolute; height:554px;width:94%;">
    <table class="table no-margin no-border theadfix_wrapper" style="
        width:92%;margin-left: 2% !important;">
        <tbody class="op_patients_list_table_body">
            <?php
                if(count($resultData)!=0){
                    foreach ($resultData as $each) {
                        $class="list_item_tokenunseen_div";
                            // $td_class="tr_token_unseen";
                        if(intval($each->dr_seen_status)==1){
                            $class="list_item_tokenseen_div";
                            // $td_class="tr_token_seen";
                        }
                        $td_class = "";
                        if($each->visit_type == "New"){
                            $td_class="tr_new_patient";
                        }
                        ?>
            <tr title="<?= $each->uhid ?>" class="ip_op_list_item col-md-12 no-padding {{$td_class}}" data-patient-id="{{$each->patient_id}}" data-booking-id="{{$each->booking_id}}">
                <td style="width:172px !important;">
                        <div class="dashboard-text" ><?= $each->patient_name ?></div>
                </td>
                <td>
                    <div class="col-md-2 padding_sm {{ $class }} pull-right">
                        <div class="list_item_token pull-right"><?= $each->token_no ?></div>
                    </div>
                </td>
            </tr>
            <?php
                    }
                }else {
                    ?>
            <tr>
                <td colspan="2" style="text-align: center">
                   No result found
                </td>
            </tr>
            <?php
                }
            ?>
        </tbody>
    </table>

</div>
