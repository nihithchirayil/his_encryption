<div class="col-md-12" style="margin-top:10px;">
    <label class="text-normal"> History </label>
</div>
<style>
    #antinatal_clinical_data_history_table tr td{
        border:1px solid #e1e1e1 !important;
    }
    .label_class{
    }
    .value_class{
        color:#21a4f1;
    }
</style>
<div class="col-md-12 no-padding no-margin theadscroll" style="position: relative;height:450px;">
    @if(isset($antinatal_clinical_data) && sizeof($antinatal_clinical_data)>0)
        @foreach($antinatal_clinical_data as $data)
        <div class="col-md-12" style="border-bottom: 1px dashed #cccccc;margin-bottom:10px !important;">

            <div class="assessment_history_head" style="display:flex;">
                <div class="assessment_history_head_left">
                    <div title=" {{date('M-d-Y h:i A',strtotime($data->created_at))}} "><i class="fa fa-calendar"></i>
                        {{date('M-d-Y h:i A',strtotime($data->created_at))}} </div>
                    </div>
                    <div class="assessment_history_head_right">

                    </div>
                </div>

                <table width="80%" id="antinatal_clinical_data_history_table" style="border-spacing:10px;margin-top:10px !important;" class="">
                    <tr>
                        <td class="label_class">
                            <span >Parity :</span>
                        </td>
                        <td colspan="8" class="value_class">
                            {{$data->parity}}
                        </td>
                    </tr>
                    <tr>
                        <td class="label_class">
                            <span >TT :</span>
                        </td>
                        <td  class="label_class">
                            <label>1st :</label>
                            @if($data->tt_1 == 1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </td>
                        <td  class="label_class">
                            <label>2nd :</label>
                            @if($data->tt_2 == 1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </td>
                        <td class="label_class">
                            <span >HB :</span>
                        </td>
                        <td  class="value_class">
                            {{$data->hb}}
                        </td>
                        <td class="label_class">
                            <span>Ht :</span>
                        </td>
                        <td  class="value_class">
                            {{$data->ht}}
                        </td>
                        <td class="label_class">
                            <span>H/o Allergy :</span>
                        </td>
                        <td  class="value_class">
                            {{$data->ho_allergy}}
                        </td>
                    </tr>
                    <tr>
                        <td class="label_class">
                            <span >BG</span>
                            <span  class="value_class">{{$data->blood_group}}</span>
                        </td>
                        <td class="label_class" colspan="2">
                            <span >Rh Type</span>
                            <span  class="value_class">{{$data->rh_type}}</span>
                        </td>
                        <td class="label_class">
                            <span >PCV</span>
                        </td>
                        <td  class="value_class">
                            {{$data->rh_type}}

                        </td>
                        <td class="label_class">
                            <span >MH</span>
                        </td>
                        <td  class="value_class">
                            {{$data->mh}}

                        </td>
                        <td class="label_class" rowspan="5" colspan="2">
                            <label ><b>Usg Report</b></label><br>
                            <span  class="value_class">
                                @if($data->usg_date !='')
                                    {{date('M-d-Y',strtotime($data->usg_date))}}<br>
                                @endif
                                {{$data->usg}}
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label_class">
                        bloodgroup
                        </td>
                        <td colspan="2"  class="value_class">
                            {{$data->blood_group}} {{$data->rh_type }}
                        </td>
                        <td class="label_class">
                            <span >Plt</span>
                        </td>
                        <td  class="value_class">
                            {{$data->plt}}
                        </td>
                        <td class="label_class">
                            <span >FH</span>
                        </td>
                        <td  class="value_class">
                            {{$data->fh}}
                        </td>

                    </tr>
                    <tr>
                        <td class="label_class">
                            <span >HIV</span>
                        </td>
                        <td colspan="2"  class="value_class">
                            @if($data->hiv !='')
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </td>
                        <td class="label_class">
                            <span >RBS</span>
                        </td>
                        <td  class="value_class">
                            {{$data->rbs}}
                        </td>
                        <td class="label_class">
                            <span >CVS</span>
                        </td>
                        <td  class="value_class">
                            {{$data->cvs}}
                        </td>

                    </tr>
                    <tr>
                        <td class="label_class">
                            <span >HCV</span>
                        </td>
                        <td colspan="2"  class="value_class">
                            {{$data->hcv}}
                        </td>
                        <td class="label_class">
                            <span >UrineRIE</span>
                        </td>
                        <td  class="value_class">
                            {{$data->urine_rie}}
                        </td>
                        <td  class="label_class">
                            <span>Rs</span>
                        </td>
                        <td  class="value_class">
                            {{$data->rs}}
                        </td>
                    </tr>
                    <tr>
                        <td class="label_class"><span >HBsAg</span></td>
                        <td colspan="2"  class="value_class">
                            {{$data->hbsag}}
                        </td>

                        <td class="label_class"><span >VDRL</span></td>
                        <td  class="value_class">
                            {{$data->vdrl}}
                        </td>

                        <td class="label_class"><span >TSH</span></td>
                        <td  class="value_class">
                            {{$data->tsh}}
                        </td>
                    </tr>
                    <tr>
                        <td class="label_class" rowspan="2">
                            <span>Personal History</span>
                        </td>
                        <td colspan='2' rowspan="2"  class="value_class">
                            {{$data->vdrl}}
                        </td>
                        <td class="label_class" rowspan="2">
                            <span>Professional Details</span>
                        </td>
                        <td colspan='3'>
                            <span><i>Husband</i></span><br>
                        </td>
                        <td colspan='2'>
                            <span><i>Wife</i></span><br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='3'><span  class="value_class">{{$data->profession_husband}}<br></span></td>
                        <td colspan='2'><span  class="value_class">{{$data->profession_wife}}</span></td>
                    </tr>
                    <tr>

                    </tr>
                </table>
        </div>
        @endforeach
    @endif

    @if(isset($antinatal_visit_data) && sizeof($antinatal_visit_data)>0)
        <div class="col-md-12" style="margin-top:10px;">
            <label class="text-normal"> Antinatal Vitals</label>
        </div>
        <div class="col-md-12">
            <table width="100%" id="antinatal_vitals_history_table" class="table-sm no-padding" style="margin-top:10px !important;">
                <thead>
                    <tr style="background:#21a4f1;color:white !important;">
                        <td style="width:10% !important;">Date</td>
                        <td>GA</td>
                        <td>BP</td>
                        <td>Wt.</td>
                        <td>F.Ht</td>
                        <td>FHS</td>
                        <td>Present</td>
                        <td style="width:50% !important;">Remarks</td>
                    </tr>
                </thead>
                <tbody id="antinatal_entry_body">
                    @foreach($antinatal_visit_data as $data)
                    <tr>
                        <td>
                            @if($data->antinatal_date !='')
                                {{date('Y-m-d',strtotime($data->antinatal_date))}}
                            @endif
                        </td>
                        <td>
                            {{$data->ga}}
                        </td>
                        <td>
                            {{$data->bp}}
                        </td>
                        <td>
                            {{$data->wt}}
                        </td>
                        <td>
                            {{$data->fht}}
                        </td>
                        <td>
                            {{$data->fhs}}
                        </td>
                        <td>
                            {{$data->present}}
                        </td>
                        <td>
                            {{$data->antinatal_note}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>





