<div class="col-md-12 antinatal_assesment_block theadscroll" style="position: relative;height:489px;">
    <table width="100%" id="antinatal_clinical_data_table" style="border-spacing:10px;" class="" style="margin-top:10px !important;">
        <tr>
            <td>
                <span class=" ">Parity</span>
            </td>
            <td colspan="2">
                <div class="col-md-12 no-padding">
                    <input type="text" class="form-control" id="parity" name="parity"/>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <span class=" ">TT</span>
            </td>
            <td>
                <label>1st:</label><input type="checkbox" id="tt_1" name="tt_1" value="1">
            </td>
            <td>
                <label>2nd:</label>
                <input type="checkbox" id="tt_2" name="tt_2" value="2">
            </td>
            <td>
                <span class=" ">HB</span>
            </td>
            <td>
                <input type="text" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " value="" id="hb" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('hb');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>
            <td>
                <span class=" ">Ht</span>
            </td>
            <td>
                <input type="text" style="
                width: 80%;
                float: left;
                clear: right;
                margin-right: 5px;
            " value="" id="ht" class="form-control"/>
            <button class="btn bg-blue" onclick="gethistory('ht');">
                <i class="fa fa-book" aria-hidden="true"></i>
            </button>
            </td>
            <td>
                <span class=" ">H/o Allergy</span>
            </td>
            <td>
                <input type="text" value="" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " id="ho_allergy" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('ho_allergy');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>
        </tr>
        <tr>
            <td>
                <span class=" ">BG</span>
            </td>
            <td colspan="2">
                <span class=" ">Rh Type</span>
            </td>
            <td>
                <span class=" ">PCV</span>
            </td>
            <td>
                <input type="text" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " value="" id="pcv" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('pcv');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>

            </td>
            <td>
                <span class=" ">MH</span>
            </td>
            <td>
                <input type="text" value="" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " id="mh" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('mh');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>
            <td rowspan="5" colspan="2">
                <label><b>Usg Report</b></label><br>
                <label style="margin-top:10px;float:left;clear:right;margin-right:10px;">Date:</label>
                   <input style=" width:72%;
                   float: left;
                   clear: right;
                   margin-right: 5px;" type="text" name="usg_date" id="usg_date" class="form-control datepicker usg_date"/>
                <button class="btn bg-blue" onclick="gethistory('usg');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
                <textarea name="usg" id="usg" class="form-control usg_data" rows="10" style="height:72px !important;"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                @php
                    $blood_grups = \DB::table('blood_group_master')
                    ->where('is_active',1)->orderBy('blood_group','ASC')->pluck('blood_group','blood_group');
                @endphp
                {!! Form::select('name', $blood_grups,null, ['class' => 'form-control', 'id' =>'blood_group', 'style' => 'color:#555555;']) !!}
            </td>
            <td colspan="2">
                @php
                    $rh_array = ['+'=>'+','-'=>'-'];
                @endphp
                {!! Form::select('name', $rh_array,null, ['class' => 'form-control', 'id' =>'rh_type', 'style' => 'color:#555555;']) !!}
            </td>
            <td>
                <span class=" ">Plt</span>
            </td>
            <td>
                <input type="text" value="" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " id="plt" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('plt');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>
            <td>
                <span class=" ">FH</span>
            </td>
            <td>
                <input type="text" value="" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " id="fh" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('fh');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>
            {{-- <td>
                {{-- <input type="text" name="usg_date" id="usg_date" class="form-control datepicker "/> --}}
                {{-- <span class=" ">Usg Report</span> --}}
            {{-- </td> --}}
            <td>
                {{-- <input type="text" name="usg" id="usg" class="form-control"/> --}}
            </td>
        </tr>
        <tr>
            <td>
                <span class=" ">HIV</span>
            </td>
            <td colspan="2">
                <input type="checkbox" id="hiv" name="hiv" value="1">
                <input type="textbox" id="hiv_text" name="hiv_text" value="" class="form-control"/>
            </td>
            <td>
                <span class=" ">RBS</span>
            </td>
            <td>
                <input type="text" value="" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " id="rbs" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('rbs');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>
            <td>
                <span class=" ">CVS</span>
            </td>
            <td>
                <input type="text" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                "  value="" id="cvs" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('cvs');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>
            {{-- <td rowspan="3" colspan="2">
                <textarea name="usg" id="usg" class="form-control" rows="10" style="height:77px !important;"></textarea>
            </td> --}}
        </tr>
        <tr>
            <td>
                <span class=" ">HCV</span>
            </td>
            <td colspan="2">
                <input type="text" value="" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " id="hcv" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('hcv');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>
            <td>
                <span class=" ">UrineRIE</span>
            </td>
            <td>
                <input type="text" value="" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " id="urine_rie" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('urine_rie');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>
            <td>
                <span class=" ">Rs</span>
            </td>
            <td>
                <input type="text" value="" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " id="rs" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('rs');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>
        </tr>
        <tr>
            <td><span class=" ">HBsAg</span></td>
            <td colspan="2"><input type="text" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " value="" id="hbsag" class="form-control"/>
                <button class="btn bg-blue" onclick="gethistory('hbsag');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>

            <td><span class=" ">VDRL</span></td>
            <td><input type="text" value="" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " id="vdrl" class="form-control"/>
            <button class="btn bg-blue" onclick="gethistory('vdrl');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>

            <td><span class=" ">TSH</span></td>
            <td><input type="text" style="
                    width: 80%;
                    float: left;
                    clear: right;
                    margin-right: 5px;
                " value="" id="tsh" class="form-control"/>
            <button class="btn bg-blue" onclick="gethistory('tsh');">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </button>
            </td>
        </tr>
        <tr>
            <td>
                <span class=" ">Personal History</span>
            </td>
            <td colspan='2'>
                <textarea name="personal_history" id="personal_history" class="form-control" rows="10"></textarea>
            </td>
            <td>
                <span class=" ">Professional Details</span>
            </td>
            <td colspan='2'>
                <span class=" ">Husband</span>
                <input tpye="text" name="profession_husband" id="profession_husband" class="form-control" rows="10"/>

                <span class="">Wife</span>
                <input tpye="text" name="profession_wife" id="profession_wife" class="form-control" rows="10"/>
            </td>
            <td>
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr>

        </tr>
    </table>


    <table width="100%" id="antinatal_entry_table" class="table-sm no-padding" style="margin-top:10px !important;">
        <thead>
            <tr style="background:#21a4f1;color:white !important;">
                <td style="width:10% !important;">Date</td>
                <td>GA</td>
                <td>BP</td>
                <td>Wt.</td>
                <td>F.Ht</td>
                <td>FHS</td>
                <td>Present</td>
                <td style="width:50% !important;">Remarks</td>
                <td style="padding-top:7px;">
                    <button type="button" class="btn bg-orange pull-right" onclick="add_new_antinatal_entry();"><i class="fa fa-plus"></i></button>
                </td>
            </tr>
        </thead>
        <tbody id="antinatal_entry_body">
            <tr>
                <td>
                    <div class="form-group" style="position: relative;margin-top:10px;">
                        <input type="text" name="antinatal_date[]" id="antinatal_date_1" class="form-control datepicker "/>
                    </div>
                    <input type="hidden" name="antinatal_entry_id[]" id="antinatal_entry_id_1"/>
                </td>
                <td>
                    <input type="text" name="ga[]" id="ga_1" class="form-control"/>
                </td>
                <td>
                    <input type="text" name="bp[]" id="bp_1" class="form-control"/>
                </td>
                <td>
                    <input type="text" name="wt[]" id="wt_1" class="form-control"/>
                </td>
                <td>
                    <input type="text" name="fht[]" id="fht_1" class="form-control"/>
                </td>
                <td>
                    <input type="text" name="fhs[]" id="fhs_1" class="form-control"/>
                </td>
                <td>
                    <input type="text" name="present[]" id="present_1" class="form-control"/>
                </td>
                <td>
                    <input type="text" name="antinatal_note[]" id="antinatal_note_1" class="form-control"/>
                </td>
                <td>
                    <button type="button" name="delete_antinatal[]" id="delete_antinatal_1" class="btn bg-red pull-right del-antinatal-list-row" onclick="delete_antinatal_entry();"><i class="fa fa-times"></i></button>
                </td>

            </tr>
        </tbody>
    </table>

    <div class="col-md-12">
        <div class="col-md-7 padding_sm">
            <form id="obstetrics_history">
            <div class="col-md-12 padding_sm table_box obstetrics_history_entry_block">
                <div style="margin-top: 5px;
                    padding-bottom: 10px;" class="text_head">Obstetrics</div>
                <style>
                    tr{
                        height:35px !important;
                    }
                </style>
                <div class="col-md-12 no-padding">
                    <span>Notes</span>
                    <textarea class="form-control" name="obstretics_notes" id="obstretics_notes" rows="10"></textarea>
                </div>
                <div class="col-md-12">
                    <button type="button" style="margin-top:10px;" onclick="addNewChild();" class="btn bg-blue pull-right"><i class="fa fa-plus" ></i> Add New Child</button>
                </div>
                <span id="obstretics_child_container">
                    <table class="obstretics_child" data-child-id='1'>
                        <tr>
                            <td colspan="2"><b>Child-1</b></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="radio" name="delevery_type1" id="ftnd1" value="1"/> <label style="margin-right:10px;" for="ftnd1">FTND</label>
                                <input type="radio" name="delevery_type1" id="lscs1" value="2"/> <label style="margin-right:10px;" for="lscs1">LSCS</label>
                                <input type="radio" name="delevery_type1" id="ab1" value="3"/> <label style="margin-right:10px;" for="ab1">AB</label>
                                <input type="radio" name="delevery_type1" id="ect1" value="4"/> <label style="margin-right:10px;" for="ect1">ECT</label>
                                <input type="radio" name="delevery_type1" id="mtp1" value="5"/> <label style="margin-right:10px;" for="mtp1">MTP</label>
                            </td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>
                                <input type="radio" name="gender1" id="girl1" value="1"/>
                                    <label style="margin-right:10px;" for="girl1">Girl
                                        <i class="fa fa-venus"></i>
                                    </label>

                                    <input type="radio" name="gender1" id="boy1" value="2"/>
                                    <label style="margin-right:10px;" for="boy1">Boy
                                        <i class="fa fa-mars"></i>
                                    </label>
                            </td>
                        </tr>
                        <tr>
                            <td>Birth Weight</td>
                            <td>
                                <input type="textbox" style="width:250px;margin-top:-7px !important;" name="birth_weight1" id="birth_weight1" class="form-control bottom-border-text borderless_textbox "/>
                            </td>
                        </tr>
                        <tr>
                            <td>Age</td>
                            <td>
                                <input type="textbox" style="width:250px;margin-top:-7px !important;" name="age1" id="age1" class="form-control bottom-border-text borderless_textbox" oninput="this.value = this.value.replace(/[^0-9-.]/g, '').replace(/(\..*)\./g, '$1');"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Place of birth</td>
                            <td>
                                <input type="textbox" style="width:250px;margin-top:-7px !important;" name="place_of_birth1" id="place_of_birth1" class="form-control bottom-border-text borderless_textbox "/>
                            </td>
                        </tr>
                        <tr>
                            <td>Any complications</td>
                            <td>
                                <input type="textbox" style="width:250px;margin-top:-7px !important;" name="any_complications1" id="any_complications1" class="form-control bottom-border-text borderless_textbox "/>
                            </td>
                        </tr>
                        <tr>
                            <td>Indication</td>
                            <td>
                                <input type="textbox" style="width:250px;margin-top:-7px !important;" name="indication1" id="indication1" class="form-control bottom-border-text borderless_textbox "/>
                            </td>
                        </tr>

                    </table>
                    <table class="obstretics_child" data-child-id='2'>

                        <tr>
                            <td colspan="2"><b>Child-2</b></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="radio" name="delevery_type2" id="ftnd2" value="1"/> <label style="margin-right:10px;" for="ftnd2">FTND</label>
                                <input type="radio" name="delevery_type2" id="lscs2" value="2"/> <label style="margin-right:10px;" for="lscs2">LSCS</label>
                                <input type="radio" name="delevery_type2" id="ab2" value="3"/> <label style="margin-right:10px;" for="ab2">AB</label>
                                <input type="radio" name="delevery_type2" id="ect2" value="4"/> <label style="margin-right:10px;" for="ect2">ECT</label>
                                <input type="radio" name="delevery_type2" id="mtp2" value="5"/> <label style="margin-right:10px;" for="mtp2">MTP</label>
                            </td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>
                                <input type="radio" name="gender2" id="girl2" value="1"/>
                                <label style="margin-right:10px;" for="girl2">Girl
                                    <i class="fa fa-venus"></i>
                                </label>

                                <input type="radio" name="gender2" id="boy2" value="2"/>
                                <label style="margin-right:10px;" for="boy2">Boy
                                    <i class="fa fa-mars"></i>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>Birth Weight</td>
                            <td>
                                <input type="textbox" style="width:250px;margin-top:-7px !important;" name="birth_weight2" id="birth_weight2" class="form-control bottom-border-text borderless_textbox "/>
                            </td>
                        </tr>
                        <tr>
                            <td>Age</td>
                            <td>
                                <input type="textbox" style="width:250px;margin-top:-7px !important;" name="age2" id="age2" class="form-control bottom-border-text borderless_textbox" oninput="this.value = this.value.replace(/[^0-9-.]/g, '').replace(/(\..*)\./g, '$1');"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Place of birth</td>
                            <td>
                                <input type="textbox" style="width:250px;margin-top:-7px !important;" name="place_of_birth2" id="place_of_birth2" class="form-control bottom-border-text borderless_textbox "/>
                            </td>
                        </tr>
                        <tr>
                            <td>Any complications</td>
                            <td>
                                <input type="textbox" style="width:250px;margin-top:-7px !important;" name="any_complications2" id="any_complications2" class="form-control bottom-border-text borderless_textbox "/>
                            </td>
                        </tr>
                        <tr>
                            <td>Indication</td>
                            <td>
                                <input type="textbox" style="width:250px;margin-top:-7px !important;" name="indication2" id="indication2" class="form-control bottom-border-text borderless_textbox "/>
                            </td>
                        </tr>
                    </table>
                </span>
            </div>
            <input type="reset" id="obstetrics_history_reset" style="display:none;"/>
            </form>
        </div>
        <div class="col-md-5">
            <div class="text_head">Obstetrics History</div>
            <div id="obstetrics_history_history_container"></div>
        </div>

    </div>

</div>



