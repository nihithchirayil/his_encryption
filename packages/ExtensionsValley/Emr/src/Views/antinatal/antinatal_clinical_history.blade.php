<table width="80%"  style=" border-spacing: 10px;
    width: 96%;
    text-align: center;
    margin: auto;" class="table table-sm table-bordered" style="margin-top:10px !important;">
    <thead>
        @if($h_type == 'usg')
            <tr style="background:#21a4f1;color:white !important;">
                <td style="width:20%"> Usg Date </td>
                <td style="width:20%"> Entered at </td>
                <td style="width:60%"> Value</td>
            </tr>
        @else
            <tr style="background:#21a4f1;color:white !important;">
                <td style="width:20%"> Date </td>
                <td style="width:20%"> Entered at </td>
                <td style="width:60%"> Value </td>
            </tr>
        @endif
    </thead>
    <tbody>

        @foreach($clinical_data as $data)
        <tr>
            <td>
                @if($h_type == 'usg')
                    {!!date('M-d-Y',strtotime($data->usg_date))!!}
                @else
                    {!!date('M-d-Y',strtotime($data->created_at))!!}
                @endif
            </td>
            <td>{!!date('M-d-Y',strtotime($data->entered_at))!!}</td>
            <td style="text-align: left;">{!!$data->$h_type!!}</td>
        </tr>
        @endforeach
    </tbody>
</table>
