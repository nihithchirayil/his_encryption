<div class="theadscroll" style="position: relative; max-height: 400px;">
    @if ((string) $global_search == '0')
        <table class="table table-bordered no-margin table_sm no-border">
            <thead>
                <tr class="table_header_bg">
                    <th width="3%">SL.No.</th>
                    <th width="10%">UHID</th>
                    <th width="14%">Patient Name</th>
                    <th width="8%">Age/Gender</th>
                    <th width="8%">Dob</th>
                    <th width="30%">Address</th>
                    <th width="7%">Phone</th>
                    <th width="7%">Token No</th>
                    <th width="7%">Shift</th>
                    <th title="Refraction" style="text-align: center" width="3%"><i class="fa fa fa-ravelry"></i>
                    </th>
                    <th title="Dilation" style="text-align: center; display: none;" width="3%"><i
                            class="fa fa-eyedropper"></th>
                </tr>
            </thead>
            <tbody class="screening_list_table_body">
                @isset($screening_list)
                    @if (count($screening_list) > 0)
                        @foreach ($screening_list as $item)
                            <tr class="patient_det_widget_screening @if ($item->screening_status == 1) btn-info @else screening_not_completed @endif "
                                data-patientname="{{ $item->patient_name_whout_title }}" data-uhid="{{ $item->uhid }}"
                                data-phone="{{ $item->phone }}" style="cursor:pointer;">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->uhid }}</td>
                                <td id="patientNameString{{ $item->patient_id }}">{{ $item->patient_name }}</td>
                                <td>{{ $item->age }}/{{ $item->gender }}</td>
                                <td>{{ date('M-d-Y', strtotime($item->dob)) }}</td>
                                <td>{{ $item->address }}</td>
                                <td>{{ $item->phone }}</td>
                                <td>{{ $item->token_no }}</td>
                                <td>{{ $item->schedule_description }}</td>
                                <td style="text-align: center;">
                                    <button id="getRefractionDilationBtn{{ $item->patient_id }}" type="button"
                                        title="Refraction"
                                        onclick="getRefractionDilation({{ $item->patient_id }},{{ $item->doctor_id }},{{ $item->appointment_id }},{{ $item->visit_id }})"
                                        class="btn btn-primary getRefractionDilation"><i
                                            id="getRefractionDilationSpin{{ $item->patient_id }}"
                                            class="fa fa fa-ravelry"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td style="text-align: center" colspan="10">No records found..!</td>
                        </tr>
                    @endif
                @endisset
            </tbody>
        </table>
</div>
@else
<div class="theadscroll" style="position: relative; max-height: 400px;">
    <table class="table table-bordered no-margin table_sm no-border">
        <thead>
            <tr class="table_header_bg">
                <th>SL.No.</th>
                <th>UHID</th>
                <th>Patient Name</th>
                <th>Age/Gender</th>
                <th>Dob</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Action</th>
                <th title="Refraction" style="text-align: center" width="3%"><i class="fa fa fa-ravelry"></i>
                </th>
                <th title="Dilation" style="text-align: center; display: none;" width="3%"><i
                        class="fa fa-eyedropper"></th>
            </tr>
        </thead>
        <tbody class="screening_list_table_body">
            @isset($screening_list)
                @if (count($screening_list) > 0)
                    @foreach ($screening_list as $item)
                        @php
                            $appoinment_id = $item->appointment_id ? $item->appointment_id : 0;
                        @endphp
                        <tr class="patient_det_widget_screening @if ($item->screening_status == 1) btn-info @else screening_not_completed @endif "
                            data-patientname="{{ $item->patient_name_whout_title }}" data-uhid="{{ $item->uhid }}"
                            data-phone="{{ $item->phone }}" style="cursor:pointer;">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->uhid }}</td>
                            <td>{{ $item->patient_name }}</td>
                            <td>{{ $item->age }}/{{ $item->gender }}</td>
                            <td>{{ date('M-d-Y', strtotime($item->dob)) }}</td>
                            <td>{{ $item->address }}</td>
                            <td>{{ $item->phone }}</td>
                            <td>
                                @if (date('Y-m-d', strtotime($item->booking_date)) == date('Y-m-d'))
                                    <button type="button" title="Manage Patient Vital" class="btn btn-sm bg-blue"
                                        onclick="showpatientVital('{{ $item->patient_id }}', '{{ $item->visit_id }}');">
                                        <i id="add_vital_modal_btn" class="fa fa-line-chart"></i>
                                    </button>
                                @endif
                            </td>
                            <td style="text-align: center;">
                                <button id="getRefractionDilationBtn1{{ $item->patient_id }}" type="button"
                                    title="Refraction"
                                    onclick="getRefractionDilation({{ $item->patient_id }},{{ $appoinment_id }},1, {{ $item->visit_id }})"
                                    class="btn btn-primary getRefractionDilation"><i
                                        id="getRefractionDilationSpin1{{ $item->patient_id }}"
                                        class="fa fa fa-ravelry"></i></button>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td style="text-align: center" colspan="9">No records found..!</td>
                    </tr>
                @endif
            @endisset
        </tbody>
    </table>
    @endif
</div>
