<div class="col-md-7 padding_sm">
    <div class="col-md-12 padding_sm table_box prescription_entering_area">
        <div class="col-md-10 no-padding">
            <div class="text_head">Prescription</div>
        </div>
        <div class="col-md-4 no-padding updatePrescriptionBtnDiv">
            <button type="button" class="btn btn-sm btn-blue updatePrescriptionBtn"><i class="fa fa-save"></i>
                Save</button>
            <button type="button" class="btn btn-sm btn-blue cancePrescriptionEditBtn"><i class="fa fa-times"></i>
                Cancel</button>
        </div>
        <div class="clearfix"></div>
        <div class="theadscroll" style="position: relative; height: 350px;">
            <table id="prescriptiondata_table"
                class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed">
                <thead>
                    <tr class="tableRowHead">
                        <th width="5%" title="Sl.No">Sl.No</th>
                        <th width="25%" title="Medicine">Medicine</th>
                        <th width="15%" title="Frequency">Freq</th>
                        <th width="15%" title="Duration">Dur</th>
                        <th width="15%" title="Quantity">Qty</th>
                        <th width="15%" title="Direction">Dir</th>
                        <th width="5%"><i class="fa fa-star"></i></th>
                        <th width="5%"><i onclick="addNewPrescriptionRowWithFocus()"
                                class="fa fa-plus addNewPrescriptionItemBtn"></i></th>
                    </tr>
                </thead>
                <tbody class="prescription_list_table_body">
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-12 padding_sm table_box advice_on_prescription_area">
        <div class="clearfix"></div>
        <div class="col-md-3 text_head">Next Review </div>
        <div class="col-md-6 text_head">
            <input type="text" class="form-control bottom-border-text" name="next_review_date" id="next_review_date"
                value="" autocomplete="off" style="">
        </div>
        <div class="clearfix"></div>
        <div class="col-md-3 text_head">Advice</div>
        <div class="clearfix"></div>
        <textarea class="form-control prescription_advice_textarea"></textarea>
    </div>
</div>

<div class="col-md-5 padding_sm">

    <div class="col-md-12 padding_sm table_box prescription_chief_complaint_container">
        <div class="text_head">Chief Complaint</div>
        <div class="clearfix"></div>
        <div class="chief_complaint_div theadscroll">
            <div class="chief_complaint"></div>
        </div>
    </div>
    <div class="col-md-12 padding_sm table_box prescription_history_container">

        <div class="tab_wrapper first_tab _side">
            <ul class="tab_list">
                <li rel="tab_11" class="active">History</li>
                <li rel="tab_22" class="">Bookmarks</li>
            </ul>

            <div class="content_wrapper">
                <div class="tab_content tab_11">
                    <div class="prescription_history_div theadscroll">

                    </div>
                </div>
                <div class="tab_content tab_22">
                    <div class="col-md-1 padding_sm pull-right" style="margin-top: -12px"><button type="button"
                            class="btn btn-blue" title="Add Favourites" id="favMedOrGrpWithoutItem"
                            onclick="addtoFavorite(this,1)"><i class="fa fa-star"></i></button></div>
                    <div class=" prescription_bookmark_div theadscroll" style="position:relative; height:345px;">

                    </div>

                </div>
            </div>

        </div>


    </div>
</div>

<!-- medicine search list div start -->
<div class="medicine-list-div-row-listing" style="display: none;">
    <a style="float: left;" class="close_btn_dialog close_btn_med_search">X</a>
    <div class="theadscroll" style="position: relative;">
        <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped table_sm">
            <thead>
                <tr class="table_header_common">
                    <th width="26%">Medicine</th>
                    <th width="18%">Generic Name</th>
                    <th width="23%">Therapeutic Category</th>
                    <th width="23
                    %">Therapeutic Subcategory</th>
                    <th width="10%">Stock</th>
                    {{-- <th width="10%">Price</th> --}}
                </tr>
            </thead>
            <tbody class="list-medicine-search-data-row-listing">
            </tbody>
        </table>
    </div>
</div>
<!-- medicine search list div end -->

<!-- frequency list div start -->
<div class="frequency-list-div-row-listing" style="display: none;">
    <a style="float: left;" class="close_btn_dialog close_btn_frequency_search">X</a>
    <div class="theadscroll" style="position: relative; height: 300px">
        <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped table_sm">
            <tbody class="list-frequency-search-data-row-listing">
            </tbody>
        </table>
    </div>
</div>
<!-- frequency search list div end -->

<!-- direction list div start -->
<div class="direction-list-div-row-listing" style="display: none;">
    <a style="float: left;" class="close_btn_dialog close_btn_direction_search">X</a>
    <div class="theadscroll" style="position: relative; height: 300px" id="directionSearchDiv">
        <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped table_sm">
            <tbody class="list-direction-search-data-row-listing">
            </tbody>
        </table>
    </div>
</div>
<!-- direction search list div end -->
