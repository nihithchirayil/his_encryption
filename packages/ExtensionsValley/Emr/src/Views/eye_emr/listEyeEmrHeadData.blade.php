<table class="table no-margin table-striped table_sm table-col-bordered table-condensed"
    style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_bg">
            <th width="70%">Created Date</th>
            <th width="10%"><i class="fa fa-edit"></i></th>
            <th width="10%"><i class="fa fa-print"></i></th>
            <th width="10%"><i class="fa fa-trash"></i></th>
        </tr>
    </thead>
    <tbody>
        @if(count($res)!=0)
        @foreach ($res as $each)
        @if($each->head_id==$head_id)
        <tr id="listEmrHistoryData{{ $each->detail_id }}">
            <td class="common_td_rules">{{ date('M-d-Y h:i A',strtotime($each->created_at)) }}</td>
            <td style="text-align: center"><button
                    onclick="editEyeEmrListData('{{ $each->data_text }}',{{ $each->head_id }},{{ $each->detail_id }})"
                    style="padding: 0px 4px" class="btn btn-warning" type="button"><i class="fa fa-edit"></i></button>
            </td>
            <td style="text-align: center"><button class="btn btn-primary"
                    id="printEyeEmrListDataBtn{{ $each->head_id}}{{ $each->detail_id }}"
                    onclick="printEyeEmrListData('{{ $each->data_text }}',{{ $each->head_id}},{{ $each->detail_id }})"
                    style="padding: 0px 4px" type="button"><i
                        id="printEyeEmrListDataSpin{{ $each->head_id}}{{ $each->detail_id }}"
                        class="fa fa-print"></i></button>
            </td>
            <td style="text-align: center"><button id="deleteEyeEmrListDataBtn{{ $each->detail_id }}"
                    onclick="deleteEyeEmrListData({{ $each->detail_id }})" class="btn btn-danger"
                    style="padding: 0px 4px" type="button"><i id="deleteEyeEmrListDataSpin{{ $each->detail_id }}"
                        class="fa fa-trash"></i></button>
            </td>
        </tr>
        @endif
        @endforeach
        @endif
    </tbody>
</table>
