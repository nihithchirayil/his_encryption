@extends('Emr::emr.page')
@section('content-header')
    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link
        href="{{ asset('packages/extensionsvalley/view_patient/css/view_patient.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
        rel="stylesheet">
    <link
        href="{{ asset('packages/extensionsvalley/view_patient/css/multi_tab_setup.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
        rel="stylesheet">
    <link
        href="{{ asset('packages/extensionsvalley/view_patient/css/common_controls.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
        rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">

    <link
        href="{{ asset('packages/extensionsvalley/view_patient/css/eye_emr.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
        rel="stylesheet">

@endsection
@section('content-area')
    @include('Emr::eye_emr.eye_modals_lite')
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="patient_refraction_id" value="0">
    <input type="hidden" id="patient_id" value="0">
    <input type="hidden" id="doctor_id" value="0">
    <input type="hidden" id="visit_id" value="0">
    <input type="hidden" id="from_list" value="1">

    <div class="right_col" role="main">
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm pull-right">
                <span class="padding_sm">{{ $title }}</span>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px">
                <div class="box no-border no-margin anim">
                    <div class="box-body clearfix">
                        <div class="col-md-2 padding_sm">
                            <div class="col-md-4 padding_sm" style="margin-top: 15px;">
                                <div class="radio radio-success inline no-margin">
                                    <input type="radio" id="patient_name_search_radio" name="search_type">
                                    <label for="patient_name_search_radio">
                                        Name</label><br>
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm" style="margin-top: 15px;">
                                <div class="radio radio-success inline no-margin">
                                    <input type="radio" id="uhid_search_radio" name="search_type">
                                    <label for="uhid_search_radio">
                                        UHID</label><br>
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm" style="margin-top: 15px;">
                                <div class="radio radio-success inline no-margin">
                                    <input type="radio" id="patient_phone_search_radio" name="search_type">
                                    <label for="patient_phone_search_radio">
                                        Phone</label><br>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <label for="">Search Patients</label>
                            <div class="clearfix"></div>
                            <div class="input-group">
                                <input type="text" id="patient_search" onkeyup="search_patient();" class="form-control">
                                <div class="input-group-btn">
                                    <button type="button" style="height: 22px;width: 30px;" class="btn btn-primary"
                                        onclick="search_patient();"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 padding_sm">
                            <label for="">Search By Date</label>
                            <div class="clearfix"></div>
                            <input type="text" id="search_date" onblur="fetchScreeningListData();"
                                class="form-control datepicker" value="{{ date('M-d-Y') }}" data-attr="date"
                                placeholder="Date">

                        </div>


                        @php
                            $doctor_list = \DB::table('doctor_master')
                                ->whereNull('doctor_master.deleted_at')
                                ->where('doctor_master.status', 1)
                                ->orderBy('doctor_master.doctor_name')
                                ->pluck('doctor_master.doctor_name', 'doctor_master.id');
                        @endphp
                        @if (count($doctor_list) > 0)
                            <div class="col-md-3 padding_sm" style="margin-top: 13px;">
                                <select id="group_doctor" name="group_doctor" class="form-control select2">
                                    <option value="All">Select Doctor</option>
                                    @foreach ($doctor_list as $key => $val)
                                        <option value="{{ $key }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="col-md-1 padding_sm" style="margin-top: 20px">
                            <button onclick="resetFilters()" type="button" class="btn btn-warning btn-block">
                                Reset <i class="fa fa-recycle"></i>
                            </button>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 20px">
                            <button onclick="fetchScreeningListData();" id="fetchScreeningListDataBtn" type="button"
                                class="btn btn-primary btn-block">
                                Search <i class="fa fa-search" id="fetchScreeningListDataSpin"></i>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 10px">
                <div class="box no-border no-margin anim">
                    <div class="box-body clearfix screening_list_table_container">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/view_patient/js/refraction_list.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/view_patient/js/refraction.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
@endsection
