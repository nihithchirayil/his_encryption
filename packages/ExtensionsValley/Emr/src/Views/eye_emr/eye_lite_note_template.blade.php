<div class="col-md-6 padding_sm">
    <div class="col-md-12 padding_sm table_box notes_entering_area" style="height:550px !important">
        <form id="ca-data-form">
            <div class="col-md-12 no-padding notes_chief_complaint_container theadscroll"
                style="height : 530px !important;margin-top:17px;">
                @if (isset($dynamic_notes_fields) && count($dynamic_notes_fields) > 0)
                    @foreach ($dynamic_notes_fields as $fields)
                        @if ($fields->type == 'dyn_textarea')
                            <div class="col-md-6 padding_sm" style="margin-top: 10px">
                                <label><strong>{{ $fields->name }}</strong></label>
                                <div id="{{ $fields->class_name }}" class="tabcontents"
                                    data-element-type="{{ $fields->type }}">
                                    <textarea attr-dataid="{{ $fields->class_name }}" class="form-control notes_templates {{ $fields->class_name }}_textarea" style="resize: none">{{ @$patient_assessment_data[$fields->class_name] ? $patient_assessment_data[$fields->class_name] : '' }}</textarea>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </form>
    </div>
</div>
<div class="col-md-6 padding_sm">
    <div class="col-md-12 padding_sm" style="margin-top: 10px;min-height: 500px;">
        <ul class="nav nav-tabs sm_nav">
            <li class="active"><a data-toggle="tab" href="#eye_refraction_present" style="border-radius: 0px;"
                    aria-expanded="false">Today's Visit</a></li>
            <li class=""><a data-toggle="tab" href="#notes_history" style="border-radius: 0px;"
                    aria-expanded="false">Notes History</a></li>
            <li class=""><a data-toggle="tab" href="#eye_refraction_history" style="border-radius: 0px;"
                    aria-expanded="false">Refraction History</a></li>

        </ul>
        <div class="tab-content eye_history_list">
                <div id="eye_refraction_present" class="col-md-12 padding_sm theadscroll tab-pane active"
                style="position: relative;height:550px">
            </div>
            <div id="notes_history" class="col-md-12 padding_sm theadscroll tab-pane"
                style="position: relative;height:550px">
            </div>
            <div id="eye_refraction_history" class="col-md-12 padding_sm theadscroll tab-pane"
                style="position: relative;height:550px">

            </div>
        </div>
    </div>

</div>
