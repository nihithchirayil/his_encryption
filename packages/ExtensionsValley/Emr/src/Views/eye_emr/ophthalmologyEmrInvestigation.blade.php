<div class="row padding_sm">
    <div class="col-md-8 padding_sm">
        <input class="form-control investigation_data" value="0" type="hidden" autocomplete="off" id="ophthalmologyEmrhead5" name="ophthalmologyEmrhead5">
        <div class="col-md-12 padding_sm">
            <table class="table no-margin table-striped table_sm table-col-bordered table-condensed"
                style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th width="20%">Type</th>
                        <th colspan="2" width="40%">Right Eye</th>
                        <th colspan="2" width="40%">Left Eye</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="td_common_numeric_rules">Corneal Topography</td>
                        <td colspan="2" class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="corneal_topography_re"
                                name="corneal_topography_re">
                        </td>
                        <td colspan="2" class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="corneal_topography_le"
                                name="corneal_topography_le">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">Visual Fields</td>
                        <td colspan="2" class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="visual_fields_re"
                                name="visual_fields_re">
                        </td>
                        <td colspan="2" class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="visual_fields_le"
                                name="visual_fields_le">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">Colour Vision</td>
                        <td colspan="2" class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="colour_vision_re"
                                name="colour_vision_re">
                        </td>
                        <td colspan="2" class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="colour_vision_le"
                                name="colour_vision_le">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">Pachymetry</td>
                        <td class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="invest_pachymetry_re"
                                name="invest_pachymetry_re">
                        </td>
                        <td>microns</td>
                        <td class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="pachymetry_le"
                                name="pachymetry_le">
                        </td>
                        <td>microns</td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">Specular Microscopy</td>
                        <td class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="specular_microscopy_re"
                                name="specular_microscopy_re">
                        </td>
                        <td>cells/mm<sup>2</sup></td>
                        <td class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="specular_microscopy_le"
                                name="specular_microscopy_le">
                        </td>
                        <td>cells/mm<sup>2</sup></td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">ACID</td>
                        <td class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="acid_re" name="acid_re">
                        </td>
                        <td>mm</td>
                        <td class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="acid_le" name="acid_le">
                        </td>
                        <td>mm</td>
                    </tr>

                    <tr>
                        <td class="td_common_numeric_rules">ASOCT</td>
                        <td colspan="2" class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="asoct_re" name="asoct_re">
                        </td>
                        <td colspan="2" class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="asoct_le" name="asoct_le">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">OCT</td>
                        <td colspan="2" class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="oct_re" name="oct_re">
                        </td>
                        <td colspan="2" class="common_td_rules">
                            <input class="form-control investigation_data" type="text" autocomplete="off" id="oct_le" name="oct_le">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">USG B Scan</label>
                <div class="clearfix"></div>
                <input class="form-control investigation_data" type="text" autocomplete="off" id="usg_b_scan" name="usg_b_scan">
            </div>
        </div>
    </div>
    <div class="col-md-4 padding_sm">
        <div class="box no-border no-margin">
            <div class="box-footer revenue_main_shadow" style="min-height: 640px;">
                <div class="col-md-8 blue padding_sm" style="font-weight: 600;">History</div>
                <div class="col-md-4 padding_sm" id="investigation_data_history" style="display: none">
                    <button onclick="resetEmrData(5, 2);" style="padding: 0px 4px" class="btn btn-success" type="button">Add
                        New <i class="fa fa-plus"></i></button>
                </div>
                <div class="clearfix"></div>
                <div class="theadscroll always-visible" style="position: relative; height: 630px;" id="eyeEmrInvestigationList">

                </div>
            </div>
        </div>
    </div>
</div>
