<div class="col-md-3 padding_sm ip_op_list_div">
    <div class="tabs">
        @if (intval($check_op_assistant) != 0)
        <select onchange="changeDoctor()" id="change_doctor_id" class="form-control select2">
            @foreach ($doctor_master as $each)
            <option value="{{ $each->id }}">{{ $each->doctor_name }}</option>
            @endforeach
        </select>
        <div class="col-md-12 padding_sm" style="margin-top: 15px">
            @else
            <div class="col-md-12 padding_sm">
                @endif

                <div style="margin-right:7px;margin-top: 10px;cursor: pointer;" class="pull-right hover_btn"><i
                        onclick="getOpPatients()" class="text-normal fa fa-refresh getPatientRefreshIcon"></i></div>
                <div style="margin-right:10px;margin-top: 10px;cursor: pointer;" class="pull-right hover_btn"><i
                        onclick="showHidePatientFilters(1)" class="text-normal fa fa-filter"></i></div>
                <ul id="tabs-nav">
                    <li class="hover_btn" onclick="getOpPatients(1)"><a href="#tab1">Unseen</a> <span id="unseen_cnt"
                            class="span_count">--</span></li>
                    <li class="hover_btn" onclick="getOpPatients(2)"><a href="#tab2">Seen</a> <span id="seen_cnt"
                            class="span_count">--</span></li>
                </ul> <!-- END tabs-nav -->
                <div id="showHidePatientFiltersAjaxDiv" class="table_box"
                    style="width: 95%;z-index: 99999;display: none;position: absolute;background:rgb(229 229 229);min-height: 150px">
                    @php
                    $user_id = \Auth::user()->id;
                    $grouped_doctor_list = [];
                    if (env('DOCTOR_WISE_GROUP_LOGIN', 0) == 0) {
                    $chk_dr_exisits = \DB::table('login_speciality_map')
                    ->where('doctor_id', $doctor_id)
                    ->first();
                    if (!empty($chk_dr_exisits)) {
                    $grouped_doctor_list = \DB::table('doctor_master')
                    ->join('login_speciality_map', 'login_speciality_map.doctor_id', '=', 'doctor_master.id')
                    ->where('login_speciality_map.speciality_id', $speciality_id)
                    ->where('login_speciality_map.status', 1)
                    ->distinct()
                    ->orderBy('doctor_master.id')
                    ->pluck('doctor_master.doctor_name', 'doctor_master.id');
                    }
                    } else {
                    $chk_dr_exisits = \DB::table('doctor_wise_group_login')
                    ->where('doctor_id', $doctor_id)
                    ->whereNull('deleted_at')
                    ->first();
                    if (!empty($chk_dr_exisits)) {
                    $group_parent_doctor_id = \DB::table('doctor_wise_group_login')
                    ->where('doctor_id', $doctor_id)
                    ->whereNull('deleted_at')
                    ->value('parent_doctor_id');
                    $grouped_doctor_list = \DB::table('doctor_master')
                    ->join('doctor_wise_group_login', 'doctor_wise_group_login.doctor_id', '=', 'doctor_master.id')
                    ->where('doctor_wise_group_login.parent_doctor_id', $group_parent_doctor_id)
                    ->orderBy('doctor_master.id')
                    ->pluck('doctor_master.doctor_name', 'doctor_master.id');
                    }
                    }

                    @endphp

                    <div class="col-md-12 padding_sm" style="margin-top: 10px">
                        <label class="text-normal">Search By Date</label>
                        <div class="clearfix"></div>
                        <input type="text" onblur="getOpPatients()" class="form-control datepicker"
                            id="op_patient_search_date" value="{{ date('M-d-Y') }}">

                    </div>
                    @if (count($grouped_doctor_list) > 0)
                    <div class="col-md-12 padding_sm" style="margin-top: 10px">
                        <label class="text-normal">Select Doctor</label>
                        <div class="clearfix"></div>
                        {!! Form::select('group_doctor', $grouped_doctor_list, $doctor_id, [
                        'class' => 'form-control',
                        'id' => 'group_doctor',
                        'onchange' => 'getOpPatients()',
                        'style' => ' color:#555555; padding:4px 12px;',
                        ]) !!}
                    </div>
                    @endif
                    <div class="col-md-12 padding_sm" style="margin-top: 10px">
                        <label class="text-normal">Global Patient Search</label>
                        <div class="clearfix"></div>
                        <input type="text" autocomplete="off"
                            class="form-control hidden_search bottom-border-text borderless_textbox global_patient_search_textbox"
                            name="patient_name" value="" autocomplete="false">
                        <input type="hidden" class="form-control" name="patient_name_hidden" id="patient_name_hidden"
                            value="">
                        <div id="AjaxDiv" class="ajaxSearchBox"
                            style="z-index: 9999;max-height: 400px !important; position: absolute;"></div>
                    </div>
                    <div class="col-md-4 pull-right padding_sm" style="margin-top: 10px">
                        <button type="button" class="btn btn-blue btn-block"
                            onclick="showHidePatientFilters(2)">Close</button>
                    </div>
                </div>
                <div id="tabs-content">
                    <div id="tab1" class="tab-content">

                    </div>
                    <div id="tab2" class="tab-content">

                    </div>
                </div>
            </div>
        </div>
    </div>
