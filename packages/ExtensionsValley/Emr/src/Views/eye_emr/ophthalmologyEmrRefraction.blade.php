<div class="row">
    <div class="col-md-8 padding_sm" id="ophthalmologyEmrRefraction">
        <input class="form-control refraction_data" value="0" type="hidden" autocomplete="off"
            id="ophthalmologyEmrhead2" name="ophthalmologyEmrhead2">
        <div class="col-md-12 padding_sm">
            <div class="col-md-6 padding_sm">
                <div class="mate-input-box" style="height:50px">
                    <label style="margin-top:-4px">Refractive Aid <button style="padding: 0px 5px;"
                            onclick="addNewTypes(5,0,'Add Refractive Aid')" title="Add New Diagnosis"
                            class="btn btn-primary" type="button">
                            <i class="fa fa-plus-circle"></i>
                        </button>
                    </label>
                    <div class="clearfix"></div>
                    <select id="refraction_types" name="refraction_types" class="form-control refraction_data"
                        style="margin-top:-4px">
                        <option value="">Select</option>
                        @foreach ($refraction_types as $each)
                        <option value="{{ $each->id }}">{{ $each->refraction_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6 padding_sm">
                <div class="mate-input-box" style="height:50px">
                    <label style="margin-top:-4px">Using Since</label>
                    <div class="clearfix"></div>
                    <input class="form-control refraction_data" type="text" autocomplete="off"
                        id="refraction_usingsince" name="refraction_usingsince">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm">
            <table class="table no-margin table-striped table_sm table-col-bordered table-condensed"
                style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th width="20%">Type</th>
                        <th width="40%">Right Eye</th>
                        <th width="40%">Left Eye</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="td_common_numeric_rules">Reason</td>
                        <td class="common_td_rules">
                            <input class="form-control refraction_data" type="text" autocomplete="off"
                                id="refraction_right_eye" name="refraction_right_eye">
                        </td>
                        <td class="common_td_rules">
                            <input class="form-control refraction_data" type="text" autocomplete="off"
                                id="refraction_left_eye" name="refraction_left_eye">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">Vision
                            <button onclick="addNewTypes(3,0,'Add Vision')" title="Add New Diagnosis"
                                class="btn btn-primary" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                        </td>
                        <td class="common_td_rules">
                            <select id="revision_types" class="form-control refraction_data" style="margin-top:-4px">
                                <option value="">Select</option>
                                @foreach ($vision_types as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_type }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td class="common_td_rules">
                            <select id="levision_types" class="form-control refraction_data" style="margin-top:-4px">
                                <option value="">Select</option>
                                @foreach ($vision_types as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_type }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">Near Vision
                            <button onclick="addNewTypes(4,0,'Add Near Vision')" title="Add New Diagnosis"
                                class="btn btn-primary" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                        </td>
                        <td class="common_td_rules">
                            <select id="renear_vision" class="form-control refraction_data" style="margin-top:-4px">
                                <option value="">Select</option>
                                @foreach ($near_vision as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_code }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td class="common_td_rules">
                            <select id="lenear_vision" class="form-control refraction_data" style="margin-top:-4px">
                                <option value="">Select</option>
                                @foreach ($near_vision as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_code }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">UCVA
                            <button onclick="addNewTypes(3,0,'Add UCVA')" title="Add New Diagnosis"
                                class="btn btn-primary" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                        </td>
                        <td class="common_td_rules">
                            <select id="ucva_re" class="form-control refraction_data" style="margin-top:-4px">
                                <option value="">Select</option>
                                @foreach ($vision_types as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_type }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td class="common_td_rules">
                            <select id="ucva_le" class="form-control refraction_data" style="margin-top:-4px">
                                <option value="">Select</option>
                                @foreach ($vision_types as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_type }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">Pin Hole
                            <button onclick="addNewTypes(3,0,'Add Pin Hole')" title="Add New Diagnosis"
                                class="btn btn-primary" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                        </td>
                        <td class="common_td_rules">
                            <select id="pinhoe_re" class="form-control refraction_data" style="margin-top:-4px">
                                <option value="">Select</option>
                                @foreach ($vision_types as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_type }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td class="common_td_rules">
                            <select id="pinhoe_le" class="form-control refraction_data" style="margin-top:-4px">
                                <option value="">Select</option>
                                @foreach ($vision_types as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_type }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">BSCVA <button onclick="addNewTypes(3,0,'Add BSCVA')"
                                title="Add New Diagnosis" class="btn btn-primary" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                        </td>
                        <td class="common_td_rules">
                            <select id="bscva_re" class="form-control refraction_data">
                                <option value="">Select</option>
                                @foreach ($vision_types as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_type }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td class="common_td_rules">
                            <select id="bscva_le" class="form-control refraction_data">
                                <option value="">Select</option>
                                @foreach ($vision_types as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_type }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">Near <button onclick="addNewTypes(4,0,'Add Near Vision')"
                                title="Add New Diagnosis" class="btn btn-primary" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button></td>
                        <td class="common_td_rules">
                            <select id="near_re" class="form-control refraction_data">
                                <option value="">Select</option>
                                @foreach ($near_vision as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_code }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td class="common_td_rules">
                            <select id="near_le" class="form-control refraction_data">
                                <option value="">Select</option>
                                @foreach ($near_vision as $each)
                                <option value="{{ $each->id }}">{{ $each->vision_code }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:40px">
                <label style="margin-top:-4px">Type of Glasses</label>
                <div class="clearfix"></div>
                <input class="form-control refraction_data" type="text" autocomplete="off" id="type_of_glasses"
                    name="type_of_glasses">
            </div>
        </div>
    </div>
    <div class="col-md-4 padding_sm">
        <div class="box no-border no-margin">
            <div class="box-footer revenue_main_shadow" style="min-height: 640px;">
                <div class="col-md-8 blue padding_sm" style="font-weight: 600;">History</div>
                <div class="col-md-4 padding_sm" id="refraction_data_history" style="display: none">
                    <button onclick="resetEmrData(2, 2);" style="padding: 0px 4px" class="btn btn-success"
                        type="button">Add
                        New <i class="fa fa-plus"></i></button>
                </div>
                <div class="clearfix"></div>
                <div class="theadscroll always-visible" style="position: relative; height: 630px;"
                    id="eyeEmrRefractionList">

                </div>
            </div>
        </div>
    </div>
</div>
