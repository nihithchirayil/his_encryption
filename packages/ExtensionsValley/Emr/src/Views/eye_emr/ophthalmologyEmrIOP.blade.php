<div class="row">
    <div class="col-md-8 padding_sm">
        <div class="col-md-12 padding_sm">
            <input class="form-control iop_data" value="0" type="hidden" autocomplete="off" id="ophthalmologyEmrhead3" name="ophthalmologyEmrhead3">
            <table class="table no-margin table-striped table_sm table-col-bordered table-condensed"
                style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th width="20%">Type</th>
                        <th width="40%">Right Eye</th>
                        <th width="40%">Left Eye</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="td_common_numeric_rules">IOP</td>
                        <td class="common_td_rules">
                            <input class="form-control iop_data" type="text" autocomplete="off" id="iop_re" name="iop_re">
                        </td>
                        <td class="common_td_rules">
                            <input class="form-control iop_data" type="text" autocomplete="off" id="iop_le" name="iop_le">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">mm HG</td>
                        <td class="common_td_rules">
                            <input class="form-control iop_data" type="text" autocomplete="off" id="mm_hg_re" name="mm_hg_re">
                        </td>
                        <td class="common_td_rules">
                            <input class="form-control iop_data" type="text" autocomplete="off" id="mm_hg_le" name="mm_hg_le">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">Finger Tension</td>
                        <td class="common_td_rules">
                            <input class="form-control iop_data" type="text" autocomplete="off" id="finger_tension_re" name="finger_tension_re">
                        </td>
                        <td class="common_td_rules">
                            <input class="form-control iop_data" type="text" autocomplete="off" id="finger_tension_le" name="finger_tension_le">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">Pachymetry</td>
                        <td class="common_td_rules">
                            <input class="form-control iop_data" type="text" autocomplete="off" id="iop_pachymetry_re" name="iop_pachymetry_re">
                        </td>
                        <td class="common_td_rules">
                            <input class="form-control iop_data" type="text" autocomplete="off" id="pachymetry_le" name="pachymetry_le">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">Gonioscopy</td>
                        <td class="common_td_rules">
                            <input class="form-control iop_data" type="text" autocomplete="off" id="gonioscopy_re" name="gonioscopy_re">
                        </td>
                        <td class="common_td_rules">
                            <input class="form-control iop_data" type="text" autocomplete="off" id="gonioscopy_le" name="gonioscopy_le">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">Glaucoma Assessment Comments</label>
                <div class="clearfix"></div>
                <input class="form-control iop_data" type="text" autocomplete="off" id="glaucoma_assessment_comments"
                    name="glaucoma_assessment_comments">
            </div>
        </div>
    </div>
    <div class="col-md-4 padding_sm">
        <div class="box no-border no-margin">
            <div class="box-footer revenue_main_shadow" style="min-height: 640px;">
                <div class="col-md-8 blue padding_sm" style="font-weight: 600;">History</div>
                <div class="col-md-4 padding_sm" id="iop_data_history" style="display: none">
                    <button onclick="resetEmrData(3, 2);" style="padding: 0px 4px" class="btn btn-success" type="button">Add
                        New <i class="fa fa-plus"></i></button>
                </div>
                <div class="clearfix"></div>
                <div class="theadscroll always-visible" style="position: relative; height: 630px;" id="eyeEmrIOPList">

                </div>
            </div>
        </div>
    </div>
</div>
