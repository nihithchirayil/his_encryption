<div class="row padding_sm">
    <div class="col-md-8 padding_sm">
        <input class="form-control examination_data" value="0" type="hidden" autocomplete="off" id="ophthalmologyEmrhead4" name="ophthalmologyEmrhead4">
        <table class="table no-margin table-striped table_sm table-col-bordered table-condensed"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th width="20%">Type</th>
                    <th width="40%">Right Eye</th>
                    <th width="40%">Left Eye</th>
                </tr>
            </thead>
            <tbody>
                @if(count($examination_types)!=0)
                @foreach ($examination_types as $each)
                @php
                $diagnosis_name = strtolower(str_replace(" ", "",$each->examination_name));
                @endphp
                <tr>
                    <td class="td_common_numeric_rules">{{ $each->examination_name }} <button
                            onclick="addNewTypes(1,{{ $each->id }},'{{ $each->examination_name }}')" type="button"
                            id="addNewTypesBtn1{{ $each->id }}" class="btn btn-primary"><i
                                id="addNewTypesSpin1{{ $each->id }}" class="fa fa-plus"></i></button></td>
                    <td class="common_td_rules">
                        <select id="right{{ $diagnosis_name }}" class="form-control examination_data">
                            <option value="">Select</option>
                            @foreach ($eye_diagnosis as $data)
                            @if($data->examination_type==$each->id)
                            <option value="{{ $data->id }}">{{ $data->diagnosis_name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </td>
                    <td class="common_td_rules">
                        <select id="left{{ $diagnosis_name }}" class="form-control examination_data">
                            <option value="">Select</option>
                            @foreach ($eye_diagnosis as $data)
                            @if($data->examination_type==$each->id)
                            <option value="{{ $data->id }}">{{ $data->diagnosis_name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:40px">
                <label style="margin-top:-4px">EOM</label>
                <div class="clearfix"></div>
                <input class="form-control examination_data" type="text" autocomplete="off" id="eom" name="eom">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:40px">
                <label style="margin-top:-4px">Cover Test</label>
                <div class="clearfix"></div>
                <input class="form-control examination_data" type="text" autocomplete="off" id="cover_test" name="cover_test">
            </div>
        </div>
    </div>
    <div class="col-md-4 padding_sm">
        <div class="box no-border no-margin">
            <div class="box-footer revenue_main_shadow" style="min-height: 640px;">
                <div class="col-md-8 blue padding_sm" style="font-weight: 600;">History</div>
                <div class="col-md-4 padding_sm" id="examination_data_history" style="display: none">
                    <button onclick="resetEmrData(4, 2);" style="padding: 0px 4px" class="btn btn-success" type="button">Add
                        New <i class="fa fa-plus"></i></button>
                </div>
                <div class="clearfix"></div>
                <div class="theadscroll always-visible" style="position: relative; height: 630px;" id="eyeEmrExaminationList">

                </div>
            </div>
        </div>
    </div>
</div>
