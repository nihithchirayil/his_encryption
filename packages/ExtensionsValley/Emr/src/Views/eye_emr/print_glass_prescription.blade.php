<!-- .css -->
<style>
    #doctorHeadClass p:first-child {
        margin: 0 !important;
        padding: 0 !important;
    }
</style>
<style type="text/css" media="print">
    @page {
        margin: 15px;
    }

    table {
        font-size: 13px;
    }

    @media print {
        .pagebreak {
            page-break-before: always;
        }
    }
</style>
<div class="box-body">
    <div class="col-md-12 no-padding">
        @include('Emr::emr.investigation.investigation_hospital_header')
        @if (count($glass_prescription) > 0)
        @foreach ($glass_prescription as $each)
        @php
        $prescription_data = @$each->prescription_json ? json_decode($each->prescription_json, true) : [];
        $prescription_date = $each->prescription_date ? $each->prescription_date : '';
        @endphp
        <table class="table no-margin table_sm no-border" style="width: 100%">
            <tbody>
                <tr>
                    <td>
                        <table class="table no-margin table_sm no-border">
                            <tr class="table_header_bg">
                                <th style="text-align: center" colspan="4">Right Eye (RE)</th>
                            </tr>
                            <tr class="table_header_bg">
                                <th>Sphere</th>
                                <th>Cylinder</th>
                                <th>AXIS</th>
                                <th><i class="fa fa-list"></i></th>
                            </tr>
                            <tr>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_right']['DvSPH'] ?
                                        $prescription_data['glass_prescription_right']['DvSPH'] :
                                        0}}</label>
                                </td>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_right']['DvCyl'] ?
                                        $prescription_data['glass_prescription_right']['DvCyl'] :
                                        0}}</label>
                                </td>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_right']['DvAxix'] ?
                                        $prescription_data['glass_prescription_right']['DvAxix'] :
                                        0}}</label>
                                </td>
                                <td>
                                    DV
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_right']['NvSPH'] ?
                                        $prescription_data['glass_prescription_right']['NvSPH'] :
                                        0}}</label>
                                </td>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_right']['NvCyl'] ?
                                        $prescription_data['glass_prescription_right']['NvCyl'] :
                                        0}}</label>
                                </td>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_right']['NvAxix'] ?
                                        $prescription_data['glass_prescription_right']['NvAxix'] :
                                        0}}</label>
                                </td>
                                <td>
                                    NV
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="table no-margin table_sm no-border">

                            <tr class="table_header_bg">
                                <th style="text-align: center" colspan="4">Left Eye (LE)</th>
                            </tr>
                            <tr class="table_header_bg">
                                <th>Sphere</th>
                                <th>Cylinder</th>
                                <th>AXIS</th>
                                <th><i class="fa fa-list"></i></th>
                            </tr>
                            <tr>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_left']['DvSPH'] ?
                                        $prescription_data['glass_prescription_left']['DvSPH'] : 0}}</label>
                                </td>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_left']['DvCyl'] ?
                                        $prescription_data['glass_prescription_left']['DvCyl'] : 0}}</label>
                                </td>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_left']['DvAxix'] ?
                                        $prescription_data['glass_prescription_left']['DvAxix'] : 0}}</label>
                                </td>
                                <td>
                                    DV
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_left']['NvSPH'] ?
                                        $prescription_data['glass_prescription_left']['NvSPH'] : 0}}</label>
                                </td>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_left']['NvCyl'] ?
                                        $prescription_data['glass_prescription_left']['NvCyl'] : 0}}</label>
                                </td>
                                <td>
                                    <label>{{ @$prescription_data['glass_prescription_left']['NvAxix'] ?
                                        $prescription_data['glass_prescription_left']['NvAxix'] : 0}}</label>
                                </td>
                                <td>
                                    NV
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        @endforeach
        @endif
    </div>
