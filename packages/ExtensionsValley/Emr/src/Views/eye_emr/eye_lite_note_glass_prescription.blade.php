<div class="row padding_sm">
    <div class="col-md-6 padding_sm" id="glass_prescription_data_list">
        <div class="col-md-6 padding_sm">
            <table class="table no-margin table_sm no-border">
                <thead>
                    <tr class="table_header_bg">
                        <th style="text-align: center" colspan="4">Right Eye (RE)</th>
                    </tr>
                    <tr class="table_header_bg">
                        <th>Sphere</th>
                        <th>Cylinder</th>
                        <th>AXIS</th>
                        <th><i class="fa fa-list"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input type="text" attr-code="DvSPH" class="form-control glass_prescription glass_prescription_right "
                                value="" id="glass_prescription_right_dvsphere">
                        </td>
                        <td>
                            <input type="text" attr-code="DvCyl" class="form-control glass_prescription glass_prescription_right "
                                value="" id="glass_prescription_right_dvcylinder">
                        </td>
                        <td>
                            <input type="text" attr-code="DvAxix" class="form-control glass_prescription glass_prescription_right "
                                value="" id="glass_prescription_right_dvaxis">
                        </td>
                        <td>
                            DV
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" attr-code="NvSPH" class="form-control glass_prescription glass_prescription_right "
                                value="" id="glass_prescription_right_nvsphere">
                        </td>
                        <td>
                            <input type="text" attr-code="NvCyl" class="form-control glass_prescription glass_prescription_right "
                                value="" id="glass_prescription_right_nvcylinder">
                        </td>
                        <td>
                            <input type="text" attr-code="NvAxix" class="form-control glass_prescription glass_prescription_right "
                                value="" id="glass_prescription_right_nvaxis">
                        </td>
                        <td>
                            NV
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6 padding_sm">
            <table class="table no-margin table_sm no-border">
                <thead>
                    <tr class="table_header_bg">
                        <th style="text-align: center" colspan="4">Left Eye (LE)</th>
                    </tr>
                    <tr class="table_header_bg">
                        <th>Sphere</th>
                        <th>Cylinder</th>
                        <th>AXIS</th>
                        <th><i class="fa fa-list"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input type="text" attr-code="DvSPH" class="form-control glass_prescription glass_prescription_left " value=""
                                id="glass_prescription_left_dvsphere">
                        </td>
                        <td>
                            <input type="text" attr-code="DvCyl" class="form-control glass_prescription glass_prescription_left " value=""
                                id="glass_prescription_left_dvcylinder">
                        </td>
                        <td>
                            <input type="text" attr-code="DvAxix" class="form-control glass_prescription glass_prescription_left "
                                value="" id="glass_prescription_left_dvaxis">
                        </td>
                        <td>
                            DV
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" attr-code="NvSPH" class="form-control glass_prescription glass_prescription_left " value=""
                                id="glass_prescription_left_nvsphere">
                        </td>
                        <td>
                            <input type="text" attr-code="NvCyl" class="form-control glass_prescription glass_prescription_left " value=""
                                id="glass_prescription_left_nvcylinder">
                        </td>
                        <td>
                            <input type="text" attr-code="NvAxix" class="form-control glass_prescription glass_prescription_left "
                                value="" id="glass_prescription_left_nvaxis">
                        </td>
                        <td>
                            NV
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 padding_sm">
        <div class="text_head col-md-12 padding_sm"> Glass Prescription History <i onclick="editGlassPrescription(0)"
                id="GlassPrescriptionHistoryBtn" class="fa fa-plus green" style="cursor: pointer;display: none"></i>
        </div>
        <div class="row padding_sm" id="glass_prescription_history"></div>
    </div>
</div>
