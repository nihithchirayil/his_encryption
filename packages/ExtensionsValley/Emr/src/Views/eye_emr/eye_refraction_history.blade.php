<div class="row padding_sm">
    <div class="col-md-6 padding_sm table_box" style="margin-top: 5px;height:200px !important">
        <div class="col-md-12 padding_sm eye_refraction_header" style="margin-top: 5px;text-align: center">
            Right Eye (RE)
        </div>
        <div class="col-md-12 padding_sm" style="margin-top: 5px">
            <strong> Snellen Chart</strong>
        </div>
        <div class="col-md-12 padding_sm" style="margin-top: 5px;">
            <div class="col-md-3 padding_sm">
                <label class="blue">DV</label>
                <div class="clearfix"></div>
                <label>{{ @$patient_refraction_data['snellen_right']['SRDV'] ? $patient_refraction_data['snellen_right']['SRDV'] : '-' }}</label>
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">NV</label>
                <div class="clearfix"></div>
                <label>{{ @$patient_refraction_data['snellen_right']['SRNV'] ? $patient_refraction_data['snellen_right']['SRNV'] : '-' }}</label>
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">DV PG</label>
                <div class="clearfix"></div>
                <label>{{ @$patient_refraction_data['snellen_right']['SRDVPG'] ? $patient_refraction_data['snellen_right']['SRDVPG'] : '-' }}</label>
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">NV PG</label>
                <div class="clearfix"></div>
                <label>{{ @$patient_refraction_data['snellen_right']['SRNVPG'] ? $patient_refraction_data['snellen_right']['SRNVPG'] : '-' }}</label>
            </div>
        </div>
        <div class="col-md-12 padding_sm" style="margin-top: 5px;">
            <table class="table no-margin table_sm no-border" style="margin-left:-10px !important;">
                <thead>
                    <tr class="table_header_bg">
                        <th>Sphere</th>
                        <th>Cylinder</th>
                        <th>AXIS</th>
                        <th title="Corrected Vision">Cor.Vn.</th>
                        <th><i class="fa fa-list"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <label>{{ @$patient_refraction_data['right']['ArSPH'] ? $patient_refraction_data['right']['ArSPH'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['right']['ArCyl'] ? $patient_refraction_data['right']['ArCyl'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['right']['ArAxix'] ? $patient_refraction_data['right']['ArAxix'] : '-' }}</label>
                        </td>
                        <td>
                            -
                        </td>
                        <th>
                            AR
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <label>{{ @$patient_refraction_data['right']['DvSPH'] ? $patient_refraction_data['right']['DvSPH'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['right']['DvCyl'] ? $patient_refraction_data['right']['DvCyl'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['right']['DvAxix'] ? $patient_refraction_data['right']['DvAxix'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['right']['DvCorrVn'] ? $patient_refraction_data['right']['DvCorrVn'] : '-' }}</label>
                        </td>
                        <th>
                            DV
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <label>{{ @$patient_refraction_data['right']['NvSPH'] ? $patient_refraction_data['right']['NvSPH'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['right']['NvCyl'] ? $patient_refraction_data['right']['NvCyl'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['right']['NvAxix'] ? $patient_refraction_data['right']['NvAxix'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['right']['NvCorrVn'] ? $patient_refraction_data['right']['NvCorrVn'] : '-' }}</label>
                        </td>
                        <th>
                            NV
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 padding_sm table_box" style="margin-top: 5px;height:200px !important">
        <div class="col-md-12 padding_sm eye_refraction_header" style="margin-top: 5px;text-align: center">
            Left Eye (LE)
        </div>
        <div class="col-md-12 padding_sm" style="margin-top: 5px">
            <strong> Snellen Chart</strong>
        </div>
        <div class="col-md-12 padding_sm" style="margin-top: 5px;">
            <div class="col-md-3 padding_sm">
                <label class="blue">DV</label>
                <div class="clearfix"></div>
                <label>{{ @$patient_refraction_data['snellen_left']['SLDV'] ? $patient_refraction_data['snellen_left']['SLDV'] : '-' }}</label>
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">NV</label>
                <div class="clearfix"></div>
                <label>{{ @$patient_refraction_data['snellen_left']['SLNV'] ? $patient_refraction_data['snellen_left']['SLNV'] : '-' }}</label>
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">DV PG</label>
                <div class="clearfix"></div>
                <label>{{ @$patient_refraction_data['snellen_left']['SLDVPG'] ? $patient_refraction_data['snellen_left']['SLDVPG'] : '-' }}</label>
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">NV PG</label>
                <div class="clearfix"></div>
                <label>{{ @$patient_refraction_data['snellen_left']['SLNVPG'] ? $patient_refraction_data['snellen_left']['SLNVPG'] : '-' }}</label>
            </div>
        </div>
        <div class="col-md-12 padding_sm" style="margin-top: 5px;">
            <table class="table no-margin table_sm no-border" style="margin-left:-10px !important;">
                <thead>
                    <tr class="table_header_bg">
                        <th>Sphere</th>
                        <th>Cylinder</th>
                        <th>AXIS</th>
                        <th title="Corrected Vision">Cor.Vn.</th>
                        <th><i class="fa fa-list"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <label>{{ @$patient_refraction_data['left']['ArSPH'] ? $patient_refraction_data['left']['ArSPH'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['left']['ArCyl'] ? $patient_refraction_data['left']['ArCyl'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['left']['ArAxix'] ? $patient_refraction_data['left']['ArAxix'] : '-' }}</label>
                        </td>
                        <td>
                            -
                        </td>
                        <th>
                            AR
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <label>{{ @$patient_refraction_data['left']['DvSPH'] ? $patient_refraction_data['left']['DvSPH'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['left']['DvCyl'] ? $patient_refraction_data['left']['DvCyl'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['left']['DvAxix'] ? $patient_refraction_data['left']['DvAxix'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['left']['DvCorrVn'] ? $patient_refraction_data['left']['DvCorrVn'] : '-' }}</label>
                        </td>
                        <th>
                            DV
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <label>{{ @$patient_refraction_data['left']['NvSPH'] ? $patient_refraction_data['left']['NvSPH'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['left']['NvCyl'] ? $patient_refraction_data['left']['NvCyl'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['left']['NvAxix'] ? $patient_refraction_data['left']['NvAxix'] : '-' }}</label>
                        </td>
                        <td>
                            <label>{{ @$patient_refraction_data['left']['NvCorrVn'] ? $patient_refraction_data['left']['NvCorrVn'] : '-' }}</label>
                        </td>
                        <th>
                            NV
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    @if (count($patient_pg_power) != 0)
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm eye_refraction_header" style="margin-top: 10px;text-align: center">PG Power
        </div>
        @foreach ($patient_pg_power as $each)
            <div class="col-md-12 padding_sm table_box" style="margin-top: 5px;height:90px !important">
                <div class="col-md-6 padding_sm" style="margin-top: 10px;text-align: center">
                    <table class="table no-margin table_sm no-border">
                        <thead>
                            <tr class="table_header_bg">
                                <th title="Right Eye">RE</th>
                                <th colspan="3">
                                    {{ @$each['right']['RGUF'] ? $each['right']['RGUF'] : '-' }}
                                </th>
                            </tr>
                            <tr class="table_header_bg">
                                <th>Sphere</th>
                                <th>Cylinder</th>
                                <th>AXIS</th>
                                <th><i class="fa fa-list"></i></th>
                            </tr>
                        </thead>
                        <tbody class="right_power">
                            <tr>
                                <td>
                                    {{ @$each['right']['RDvSPH'] ? $each['right']['RDvSPH'] : '-' }}
                                </td>
                                <td>
                                    {{ @$each['right']['RDvCyl'] ? $each['right']['RDvCyl'] : '-' }}
                                </td>
                                <td>
                                    {{ @$each['right']['RDvAxix'] ? $each['right']['RDvAxix'] : '-' }}
                                </td>
                                <th>
                                    DV
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    {{ @$each['right']['RNvSPH'] ? $each['right']['RNvSPH'] : '-' }}
                                </td>
                                <td>
                                    {{ @$each['right']['RNvCyl'] ? $each['right']['RNvCyl'] : '-' }}
                                </td>
                                <td>
                                    {{ @$each['right']['RNvAxix'] ? $each['right']['RNvAxix'] : '-' }}
                                </td>
                                <th>
                                    NV
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6 padding_sm" style="margin-top: 10px;text-align: center">
                    <table class="table no-margin table_sm no-border">
                        <thead>
                            <tr class="table_header_bg">
                                <th title="Left Eye">LE</th>
                                <th colspan="3">
                                    {{ @$each['left']['LGUF'] ? $each['left']['LGUF'] : '-' }}
                                </th>
                            </tr>
                            <tr class="table_header_bg">
                                <th>Sphere</th>
                                <th>Cylinder</th>
                                <th>AXIS</th>
                                <th><i class="fa fa-list"></i></th>
                            </tr>
                        </thead>
                        <tbody class="left_power">
                            <tr>
                                <td>
                                    {{ @$each['left']['LDvSPH'] ? $each['left']['LDvSPH'] : "-" }}
                                </td>
                                <td>
                                    {{ @$each['left']['LDvCyl'] ? $each['left']['LDvCyl'] : "-" }}
                                </td>
                                <td>
                                    {{ @$each['left']['LDvAxix'] ? $each['left']['LDvAxix'] : "-" }}
                                </td>
                                <th>
                                    DV
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    {{ @$each['left']['LNvSPH'] ? $each['left']['LNvSPH'] : "-" }}
                                </td>
                                <td>
                                    {{ @$each['left']['LNvCyl'] ? $each['left']['LNvCyl'] : "-" }}
                                </td>
                                <td>
                                    {{ @$each['left']['LNvAxix'] ? $each['left']['LNvAxix'] : "-" }}
                                </td>
                                <th>
                                    NV
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        @endforeach
    @endif
</div>
