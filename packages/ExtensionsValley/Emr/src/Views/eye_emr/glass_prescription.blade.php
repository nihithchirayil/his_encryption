@if (count($glass_prescription) > 0)
@foreach ($glass_prescription as $each)
@php
$prescription_data = @$each->prescription_json ? json_decode($each->prescription_json, true) : [];
$prescription_date = $each->prescription_date ? $each->prescription_date : '';
@endphp
<div class="col-md-12 padding_sm table_box" style="margin-top: 10px">
    <div class="col-md-7 padding_sm table_box" style="margin-top: 10px">
        {{ @$each->doctor_name ? $each->doctor_name : '-' }}
    </div>
    <div class="col-md-5 padding_sm table_box" style="margin-top: 10px">
        @if($prescription_date==date('Y-m-d'))
        <div class="col-md-10 padding_sm">
            {{ @$each->created_at ? $each->created_at : '-' }}
        </div>
        <div class="col-md-2 padding_sm"><button type="button" id="glassPrescBtn{{ $each->glass_presc }}"
                onclick="editGlassPrescription({{ $each->glass_presc }})" class="btn btn-warning">
                <i id="glassPrescSpin{{ $each->glass_presc }}" class="fa fa-edit"></i>
            </button>
        </div>
        @else
        <div class="col-md-12 padding_sm" style="text-align: center">
            {{ @$each->created_at ? $each->created_at : '-' }}
        </div>
        @endif
    </div>
    <div class="col-md-6 padding_sm" style="margin-top: 10px">
        <table class="table no-margin table_sm no-border">
            <thead>
                <tr class="table_header_bg">
                    <th style="text-align: center" colspan="4">Right Eye (RE)</th>
                </tr>
                <tr class="table_header_bg">
                    <th>Sphere</th>
                    <th>Cylinder</th>
                    <th>AXIS</th>
                    <th><i class="fa fa-list"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_right']['DvSPH'] ?
                            $prescription_data['glass_prescription_right']['DvSPH'] : ''}}</label>
                    </td>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_right']['DvCyl'] ?
                            $prescription_data['glass_prescription_right']['DvCyl'] : ''}}</label>
                    </td>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_right']['DvAxix'] ?
                            $prescription_data['glass_prescription_right']['DvAxix'] : ''}}</label>
                    </td>
                    <td>
                        DV
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_right']['NvSPH'] ?
                            $prescription_data['glass_prescription_right']['NvSPH'] : ''}}</label>
                    </td>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_right']['NvCyl'] ?
                            $prescription_data['glass_prescription_right']['NvCyl'] : ''}}</label>
                    </td>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_right']['NvAxix'] ?
                            $prescription_data['glass_prescription_right']['NvAxix'] : ''}}</label>
                    </td>
                    <td>
                        NV
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-6 padding_sm" style="margin-top: 10px">
        <table class="table no-margin table_sm no-border">
            <thead>
                <tr class="table_header_bg">
                    <th style="text-align: center" colspan="4">Left Eye (LE)</th>
                </tr>
                <tr class="table_header_bg">
                    <th>Sphere</th>
                    <th>Cylinder</th>
                    <th>AXIS</th>
                    <th><i class="fa fa-list"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_left']['DvSPH'] ?
                            $prescription_data['glass_prescription_left']['DvSPH'] : ''}}</label>
                    </td>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_left']['DvCyl'] ?
                            $prescription_data['glass_prescription_left']['DvCyl'] : ''}}</label>
                    </td>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_left']['DvAxix'] ?
                            $prescription_data['glass_prescription_left']['DvAxix'] : ''}}</label>
                    </td>
                    <td>
                        DV
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_left']['NvSPH'] ?
                            $prescription_data['glass_prescription_left']['NvSPH'] : ''}}</label>
                    </td>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_left']['NvCyl'] ?
                            $prescription_data['glass_prescription_left']['NvCyl'] : ''}}</label>
                    </td>
                    <td>
                        <label>{{ @$prescription_data['glass_prescription_left']['NvAxix'] ?
                            $prescription_data['glass_prescription_left']['NvAxix'] : ''}}</label>
                    </td>
                    <td>
                        NV
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endforeach
@endif
