@if (count($patient_full_notes) > 0)
@foreach ($patient_full_notes as $each)
@php
$patient_assessment_data = @$each->data_text ? json_decode($each->data_text, true) : [];
$assessment_data = @$patient_assessment_data[$fields->class_name] ? $patient_assessment_data[$fields->class_name] : '';
@endphp
@if (count($patient_assessment_data) != 0)
<div class="col-md-12 padding_sm theadscroll table_box notes_entering_area" style="height:200px !important">
    <div class="col-md-6 padding_sm eye_refraction_header" style="margin-top: 10px;text-align: center">
        {{ @$each->doctor_name ? $each->doctor_name : '-' }}
    </div>
    <div class="col-md-6 padding_sm eye_refraction_header" style="margin-top: 10px;text-align: center">
        {{ @$each->created_at ? $each->created_at : '-' }}
    </div>
    @if (isset($dynamic_notes_fields) && count($dynamic_notes_fields) > 0)
    @foreach ($dynamic_notes_fields as $fields)
    @php
    $assessment_data = @$patient_assessment_data[$fields->class_name] ? $patient_assessment_data[$fields->class_name]
    :'';
    @endphp
    @if($assessment_data)
    <div class="col-md-6 padding_sm" style="margin-top: 10px">
        <label><strong>{{ $fields->name }}</strong></label>
        <div class="clearfix"></div>
        <label>{{ @$patient_assessment_data[$fields->class_name] ? $patient_assessment_data[$fields->class_name] : ''
            }}</label>
    </div>
    @endif
    @endforeach
    @endif
</div>
@endif
@endforeach
@endif
