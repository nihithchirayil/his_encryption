<div class="modal fade" id="getEyeRefractionModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1250px;width: 100%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getEyeRefractionModelHeader">Refraction</h4>
            </div>
            <div class="modal-body" style="height: 650px;">
                <div class="row padding_sm">
                    <div class="col-md-2 padding_sm table_box" id="refractionDataList"
                        style="margin-top: 10px;height:600px !important">
                    </div>
                    <div id="getEyeRefractionModelDiv" class="col-md-10 padding_sm table_box"
                        style="margin-top: 10px;height:90px !important"></div>

                </div>

            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" id="AddNewRefractionBtn" onclick="addNewRefraction()" type="button"
                    class="btn btn-success">Add New
                    Refraction <i id="AddNewRefractionSpin" class="fa fa-plus"></i></button>
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" id="savePatientRefractionBtn" class="btn btn-blue" type="button"
                    onclick="savePatientRefraction()"><i id="savePatientRefractionSpin" class="fa fa-save"></i>
                    Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="investigationDateTimePickerModel" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 40%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="investigationDateTimePickerModelHeader"></h4>
            </div>
            <div class="modal-body" style="min-height: 75px;">
                <div class="col-md-12">
                    <div class="mate-input-box">
                        <label class="filter_label">Service Date</label>
                        <input type="hidden" value="" id="invest_cnt_hiddden">
                        <input type="text" autocomplete="off" value="" class="form-control datepicker"
                            id="investigationServiceDate">
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px; margin-top: 5px;" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addNewItemsModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 600px;width: 100%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="addNewItemsModelHeader">-</h4>
            </div>
            <div class="modal-body">
                <div class="row padding_sm">
                    <div class="col-md-12 padding_sm">
                        <input type="hidden" id="addnewitemfromtype" value="0">
                        <input type="hidden" id="addnewitemheadid" value="0">
                        <div class="mate-input-box" style="height:60px">
                            <label style="margin-top:-4px">Add New</label>
                            <div class="clearfix"></div>
                            <input class="form-control" type="text" autocomplete="off" id="add_new_item" name="add_new_item">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" id="addNewItemsBtn" class="btn btn-blue" type="button"
                    onclick="addNewItems()"><i id="addNewItemsSpin" class="fa fa-save"></i>
                    Save</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="uploadEyeImagesModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 500px;width: 100%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getPatientVitalModelHeader">Upload Image</h4>
            </div>
            <div class="modal-body" id="uploadEyeImagesModelDiv">
                <form id="uploadEyeImageDocumentData">
                    <div class="col-md-7 padding_sm" style="margin-top:-1px;">
                        <input style="height:25px !important" type="file" class="form-control btn bg-blue"
                            name="upload_eyemarkdocument" id="upload_eyemarkdocument">
                    </div>
                    <div class="col-md-5 padding_sm">
                        <button id="uploadDocumentBtn" class="btn btn-success btn-block" type="submit"
                            title="Upload">Upload <i id="uploadDocumentSpin" class="fa fa-upload"></i></button>
                    </div>
                    <div class="col-md-12 padding_sm" style="margin-top: 15px">
                        <div class="col-md-2 padding_sm pull-right">
                            <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                                class="btn btn-danger btn-block">Close <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


