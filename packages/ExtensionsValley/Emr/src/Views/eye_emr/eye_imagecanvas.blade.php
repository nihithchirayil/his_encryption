<div class="row">
    <div class="col-md-10 padding_sm" id="canvas-container">
        <canvas id="image_canvas"></canvas>
    </div>
    <div class="col-md-1 padding_sm showEyeMarkerDetails" style="display: none;">
        <div class="theadscroll always-visible" style="position: relative; height: 500px;">
            <div id="editedPatientImageData"></div>
        </div>
    </div>
    <div class="col-md-1 padding_sm showEyeMarkerDetails" style="display: none;">
        <button class="btn btn-info btn-block" type="button" title="ZoomIn" id="zoomInBtn"><i
                class="fa fa-search-plus"></i></button>
        <div class="clearfix"></div>
        <button class="btn bg-teal-active btn-block" title="Zoom Out" type="button" id="zoomOutBtn"><i
                class="fa fa-search-minus"></i></button>
        <div class="clearfix"></div>
        <button class="btn btn-primary btn-block" title="Add Comments" type="button" id="addTextBtn"><i
                class="fa fa-comment-o"></i></button>
        <div class="clearfix"></div>
        <button class="btn bg-purple btn-block" title="Line Tool" type="button" id="pencilBtn"><i
                class="fa fa-edit"></i></button>
        <div class="clearfix"></div>
        <button class="btn btn-warning btn-block" title="Upload Image" type="button" onclick="uploadImage()"
            id="uploadImageBtn"><i id="uploadImageSpin" class="fa fa-cloud-upload"></i></button>
        <div class="clearfix"></div>
        <button class="btn btn-danger btn-block" title="Reset" type="button" onclick="resetImageMarker()"><i
                class="fa fa-recycle"></i></button>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 padding_sm showEyeMarkerDetails" style="margin-top: 10px;display: none;">
        <div id="image-slider" class="slider">

        </div>
    </div>
</div>
