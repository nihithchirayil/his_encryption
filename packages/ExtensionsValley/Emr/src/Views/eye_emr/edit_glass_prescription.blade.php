@php
$prescription_data = @$glass_prescription[0]->prescription_json ? json_decode($glass_prescription[0]->prescription_json,
true) : [];
@endphp
<div class="col-md-6 padding_sm">
    <table class="table no-margin table_sm no-border">
        <thead>
            <tr class="table_header_bg">
                <th style="text-align: center" colspan="4">Right Eye (RE)</th>
            </tr>
            <tr class="table_header_bg">
                <th>Sphere</th>
                <th>Cylinder</th>
                <th>AXIS</th>
                <th><i class="fa fa-list"></i></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <input type="text" attr-code="DvSPH"
                        class="form-control glass_prescription_right  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_right']['DvSPH'] ?
                        $prescription_data['glass_prescription_right']['DvSPH'] : ''}}"
                        id="glass_prescription_right_dvsphere">
                </td>
                <td>
                    <input type="text" attr-code="DvCyl"
                        class="form-control glass_prescription_right  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_right']['DvCyl'] ?
                        $prescription_data['glass_prescription_right']['DvCyl'] : ''}}"
                        id="glass_prescription_right_dvcylinder">
                </td>
                <td>
                    <input type="text" attr-code="DvAxix"
                        class="form-control glass_prescription_right  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_right']['DvAxix'] ?
                        $prescription_data['glass_prescription_right']['DvAxix'] : ''}}"
                        id="glass_prescription_right_dvaxis">
                </td>
                <td>
                    DV
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" attr-code="NvSPH"
                        class="form-control glass_prescription_right  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_right']['NvSPH'] ?
                        $prescription_data['glass_prescription_right']['NvSPH'] : ''}}"
                        id="glass_prescription_right_nvsphere">
                </td>
                <td>
                    <input type="text" attr-code="NvCyl"
                        class="form-control glass_prescription_right  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_right']['NvCyl'] ?
                        $prescription_data['glass_prescription_right']['NvCyl'] : ''}}"
                        id="glass_prescription_right_nvcylinder">
                </td>
                <td>
                    <input type="text" attr-code="NvAxix"
                        class="form-control glass_prescription_right  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_right']['NvAxix'] ?
                        $prescription_data['glass_prescription_right']['NvAxix'] : ''}}"
                        id="glass_prescription_right_nvaxis">
                </td>
                <td>
                    NV
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="col-md-6 padding_sm">
    <table class="table no-margin table_sm no-border">
        <thead>
            <tr class="table_header_bg">
                <th style="text-align: center" colspan="4">Left Eye (LE)</th>
            </tr>
            <tr class="table_header_bg">
                <th>Sphere</th>
                <th>Cylinder</th>
                <th>AXIS</th>
                <th><i class="fa fa-list"></i></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <input type="text" attr-code="DvSPH"
                        class="form-control glass_prescription_left  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_left']['DvSPH'] ?
                        $prescription_data['glass_prescription_left']['DvSPH'] : ''}}"
                        id="glass_prescription_left_dvsphere">
                </td>
                <td>
                    <input type="text" attr-code="DvCyl"
                        class="form-control glass_prescription_left  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_left']['DvCyl'] ?
                        $prescription_data['glass_prescription_left']['DvCyl'] : ''}}"
                        id="glass_prescription_left_dvcylinder">
                </td>
                <td>
                    <input type="text" attr-code="DvAxix"
                        class="form-control glass_prescription_left  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_left']['DvAxix'] ?
                        $prescription_data['glass_prescription_left']['DvAxix'] : ''}}"
                        id="glass_prescription_left_dvaxis">
                </td>
                <td>
                    DV
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" attr-code="NvSPH"
                        class="form-control glass_prescription_left  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_left']['NvSPH'] ?
                        $prescription_data['glass_prescription_left']['NvSPH'] : ''}}"
                        id="glass_prescription_left_nvsphere">
                </td>
                <td>
                    <input type="text" attr-code="NvCyl"
                        class="form-control glass_prescription_left  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_left']['NvCyl'] ?
                        $prescription_data['glass_prescription_left']['NvCyl'] : ''}}"
                        id="glass_prescription_left_nvcylinder">
                </td>
                <td>
                    <input type="text" attr-code="NvAxix"
                        class="form-control glass_prescription_left  td_common_numeric_rules" value="{{ @$prescription_data['glass_prescription_left']['NvAxix'] ?
                        $prescription_data['glass_prescription_left']['NvAxix'] : ''}}"
                        id="glass_prescription_left_nvaxis">
                </td>
                <td>
                    NV
                </td>
            </tr>
        </tbody>
    </table>
</div>
