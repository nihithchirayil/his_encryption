<div class="col-md-8 padding_sm" style="margin-top: 10px">
    <div class="col-md-12 padding_sm" style="margin-top: 10px">
        <input class="form-control corneal_data" type="hidden" autocomplete="off" id="ophthalmologyEmrhead11"
            name="ophthalmologyEmrhead11">
        <div class="col-md-6 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">Donor Details</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="donor_details"
                    name="donor_details">
            </div>
        </div>
        <div class="col-md-6 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">Primary Indication</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="primary_indication"
                    name="primary_indication">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">Host Diameter</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="host_diameter"
                    name="host_diameter">
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">Donor Diameter</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="donor_diameter"
                    name="donor_diameter">
            </div>
        </div>
        <div class="col-md-4 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">Surfing Technique</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="surfing_technique"
                    name="surfing_technique">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">DALK Technique</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="dalk_technique"
                    name="dalk_technique">
            </div>
        </div>
        <div class="col-md-6 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">EK Donor Thickness</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="ek_donor_thickness"
                    name="ek_donor_thickness">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">Type of Bubble</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="type_of_bubble"
                    name="type_of_bubble">
            </div>
        </div>
        <div class="col-md-6 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">EK Tamponade</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="ek_tamponade"
                    name="ek_tamponade">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">Donar Thickness DALK</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="donar_thickness_dalk"
                    name="donar_thickness_dalk">
            </div>
        </div>
        <div class="col-md-6 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">EK Complications</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="ek_complications"
                    name="ek_complications">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">Comments</label>
                <div class="clearfix"></div>
                <input class="form-control corneal_data" type="text" autocomplete="off" id="corneal_comments"
                    name="corneal_comments">
            </div>
        </div>
    </div>
</div>
<div class="col-md-4 padding_sm">
    <div class="box no-border no-margin">
        <div class="box-footer revenue_main_shadow" style="min-height: 640px;">
            <div class="col-md-8 blue padding_sm" style="font-weight: 600;">History</div>
            <div class="col-md-4 padding_sm" id="corneal_data_history" style="display: none">
                <button onclick="resetEmrData(11, 2);" style="padding: 0px 4px" class="btn btn-success"
                    type="button">Add
                    New <i class="fa fa-plus"></i></button>
            </div>
            <div class="clearfix"></div>
            <div class="theadscroll always-visible" style="position: relative; height: 630px;"
                id="eyeEmrCornealSurgery">

            </div>
        </div>
    </div>
</div>
