@if (count($patient_refraction_pg_power) != 0)
@foreach ($patient_refraction_pg_power as $each)
<div class="clearfix"></div>
<div class="col-md-12 padding_sm pg_power_data">
    <div class="col-md-6 padding_sm" style="margin-top: 10px;text-align: center">
        <table class="table no-margin table_sm no-border">
            <thead>
                <tr class="table_header_bg">
                    <th title="Right Eye">RE</th>
                    <th colspan="3">
                        <input type="text" class="form-control"
                            value="{{ @$each['right']['RGUF'] ? $each['right']['RGUF'] : '' }}"
                            name="right_glass_used_for" placeholder="Glass Duration">
                    </th>
                </tr>
                <tr class="table_header_bg">
                    <th>Sphere</th>
                    <th>Cylinder</th>
                    <th>AXIS</th>
                    <th><i class="fa fa-list"></i></th>
                </tr>
            </thead>
            <tbody class="right_power">
                <tr>
                    <td>
                        <input type="text" name="RDvSPH"
                            value="{{ @$each['right']['RDvSPH'] ? $each['right']['RDvSPH'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftDvSPH">
                    </td>
                    <td>
                        <input type="text" name="RDvCyl"
                            value="{{ @$each['right']['RDvCyl'] ? $each['right']['RDvCyl'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftDvCyl">
                    </td>
                    <td>
                        <input type="text" name="RDvAxix"
                            value="{{ @$each['right']['RDvAxix'] ? $each['right']['RDvAxix'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftDvAxix">
                    </td>
                    <th>
                        DV
                    </th>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="RNvSPH"
                            value="{{ @$each['right']['RNvSPH'] ? $each['right']['RNvSPH'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftNvSPH">
                    </td>
                    <td>
                        <input type="text" name="RNvCyl"
                            value="{{ @$each['right']['RNvCyl'] ? $each['right']['RNvCyl'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftNvCyl">
                    </td>
                    <td>
                        <input type="text" name="RNvAxix"
                            value="{{ @$each['right']['RNvAxix'] ? $each['right']['RNvAxix'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftNvCyl">
                    </td>
                    <th>
                        NV
                    </th>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-6 padding_sm" style="margin-top: 10px;text-align: center">
        <table class="table no-margin table_sm no-border">
            <thead>
                <tr class="table_header_bg">
                    <th title="Left Eye" colspan="3">LE</th>
                    <th onclick="deletePGPower(this)"><i class="btn btn-danger fa fa-times"></i></th>
                </tr>
                <tr class="table_header_bg">
                    <th>Sphere</th>
                    <th>Cylinder</th>
                    <th>AXIS</th>
                    <th><i class="fa fa-list"></i></th>
                </tr>
            </thead>
            <tbody class="left_power">
                <tr>
                    <td>
                        <input type="text" name="LDvSPH"
                            value="{{ @$each['left']['LDvSPH'] ? $each['left']['LDvSPH'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftDvSPH">
                    </td>
                    <td>
                        <input type="text" name="LDvCyl"
                            value="{{ @$each['left']['LDvCyl'] ? $each['left']['LDvCyl'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftDvCyl">
                    </td>
                    <td>
                        <input type="text" name="LDvAxix"
                            value="{{ @$each['left']['LDvAxix'] ? $each['left']['LDvAxix'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftDvAxix">
                    </td>
                    <th>
                        DV
                    </th>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="LNvSPH"
                            value="{{ @$each['left']['LNvSPH'] ? $each['left']['LNvSPH'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftNvSPH">
                    </td>
                    <td>
                        <input type="text" name="LNvCyl"
                            value="{{ @$each['left']['LNvCyl'] ? $each['left']['LNvCyl'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftNvCyl">
                    </td>
                    <td>
                        <input type="text" name="LNvAxix"
                            value="{{ @$each['left']['LNvAxix'] ? $each['left']['LNvAxix'] : '' }}"
                            class="form-control td_common_numeric_rules" id="leftNvCyl">
                    </td>
                    <th>
                        NV
                    </th>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endforeach
@else
<div>
    <div class="col-md-12 padding_sm pg_power_data">
        <div class="col-md-6 padding_sm" style="margin-top: 10px;text-align: center">
            <table class="table no-margin table_sm no-border">
                <thead>
                    <tr class="table_header_bg">
                        <th title="Right Eye">RE</th>
                        <th colspan="3">
                            <input type="text" class="form-control" value="" name="right_glass_used_for"
                                placeholder="Glass Duration">
                        </th>
                    </tr>
                    <tr class="table_header_bg">
                        <th>Sphere</th>
                        <th>Cylinder</th>
                        <th>AXIS</th>
                        <th><i class="fa fa-list"></i></th>
                    </tr>
                </thead>
                <tbody class="right_power">
                    <tr>
                        <td>
                            <input type="text" name="RDvSPH" value="" class="form-control td_common_numeric_rules"
                                id="leftDvSPH">
                        </td>
                        <td>
                            <input type="text" name="RDvCyl" value="" class="form-control td_common_numeric_rules"
                                id="leftDvCyl">
                        </td>
                        <td>
                            <input type="text" name="RDvAxix" value="" class="form-control td_common_numeric_rules"
                                id="leftDvAxix">
                        </td>
                        <th>
                            DV
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="RNvSPH" value="" class="form-control td_common_numeric_rules"
                                id="leftNvSPH">
                        </td>
                        <td>
                            <input type="text" name="RNvCyl" value="" class="form-control td_common_numeric_rules"
                                id="leftNvCyl">
                        </td>
                        <td>
                            <input type="text" name="RNvAxix" value="" class="form-control td_common_numeric_rules"
                                id="leftNvCyl">
                        </td>
                        <th>
                            NV
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6 padding_sm" style="margin-top: 10px;text-align: center">
            <table class="table no-margin table_sm no-border">
                <thead>
                    <tr class="table_header_bg">
                        <th colspan="3" title="Left Eye">LE</th>
                        <th onclick="deletePGPower(this)"><i class="btn btn-danger fa fa-times"></i></th>
                    </tr>
                    <tr class="table_header_bg">
                        <th>Sphere</th>
                        <th>Cylinder</th>
                        <th>AXIS</th>
                        <th><i class="fa fa-list"></i></th>
                    </tr>
                </thead>
                <tbody class="left_power">
                    <tr>
                        <td>
                            <input type="text" name="LDvSPH" value="" class="form-control td_common_numeric_rules"
                                id="leftDvSPH">
                        </td>
                        <td>
                            <input type="text" name="LDvCyl" value="" class="form-control td_common_numeric_rules"
                                id="leftDvCyl">
                        </td>
                        <td>
                            <input type="text" name="LDvAxix" value="" class="form-control td_common_numeric_rules"
                                id="leftDvAxix">
                        </td>
                        <th>
                            DV
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="LNvSPH" value="" class="form-control td_common_numeric_rules"
                                id="leftNvSPH">
                        </td>
                        <td>
                            <input type="text" name="LNvCyl" value="" class="form-control td_common_numeric_rules"
                                id="leftNvCyl">
                        </td>
                        <td>
                            <input type="text" name="LNvAxix" value="" class="form-control td_common_numeric_rules"
                                id="leftNvCyl">
                        </td>
                        <th>
                            NV
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif
