<div class="row">
    <div class="col-md-8 padding_sm">
        <div class="col-md-12 padding_sm">
            <input class="form-control diagosis_data" value="0" type="hidden" autocomplete="off"
                id="ophthalmologyEmrhead6" name="ophthalmologyEmrhead6">
            <div class="box no-border no-margin">
                <div class="box-footer revenue_main_shadow" style="min-height: 340px;">
                    <div class="theadscroll always-visible" style="position: relative; height: 330px;">
                        <table
                            class="table no-margin table-striped table_sm theadfix_wrapper table-col-bordered table-condensed"
                            style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg">
                                    <th width="5%"> <button onclick="addNewTypes(2,0,'Add New Diagnosis')"
                                            title="Add New Diagnosis" class="btn btn-primary" type="button">
                                            <i class="fa fa-plus-circle"></i></button>
                                    </th>
                                    <th width="70%">Diagnosis</th>
                                    <th width="20%">Eye Side</th>
                                    <th width="5%"> <button id="addNewDiagnosisBtn" title="Add New Row"
                                            class="btn btn-warning" onclick="addNewDiagnosis()" type="button"><i
                                                id="addNewDiagnosisSpin" class="fa fa-plus"></i></button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="diagnosisNewRow">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">Advice</label>
                <div class="clearfix"></div>
                <input class="form-control diagosis_data" type="text" autocomplete="off" id="diagosisAdvice"
                    name="diagosisAdvice">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px">
                <label style="margin-top:-4px">Next Review</label>
                <div class="clearfix"></div>
                <input class="form-control diagosis_data" type="text" autocomplete="off" id="diagosisNextReview"
                    name="diagosisNextReview">
            </div>
        </div>
    </div>
    <div class="col-md-4 padding_sm">
        <div class="box no-border no-margin">
            <div class="box-footer revenue_main_shadow" style="min-height: 640px;">
                <div class="col-md-8 blue padding_sm" style="font-weight: 600;">History</div>
                <div class="col-md-4 padding_sm" id="diagosis_data_history" style="display: none">
                    <button onclick="resetEmrData(6, 2);" style="padding: 0px 4px" class="btn btn-success"
                        type="button">Add
                        New <i class="fa fa-plus"></i></button>
                </div>
                <div class="clearfix"></div>
                <div class="theadscroll always-visible" style="position: relative; height: 630px;"
                    id="eyeEmrDiagnosisList">

                </div>
            </div>
        </div>
    </div>
</div>
