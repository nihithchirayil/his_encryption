<div class="col-md-8 padding_sm" style="margin-top: 10px">
    <div class="col-md-12 padding_sm" style="margin-top: 10px">
        <input class="form-control cataract_data" value="0" type="hidden" autocomplete="off" id="ophthalmologyEmrhead10" name="ophthalmologyEmrhead10">
        <table class="table no-margin table-striped table_sm table-col-bordered table-condensed"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th width="20%">Type</th>
                    <th colspan="3" width="40%">Right Eye</th>
                    <th colspan="3" width="40%">Left Eye</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="td_common_numeric_rules">Axial Length</td>
                    <td colspan="3" class="common_td_rules">
                        <input class="form-control cataract_data" type="text" autocomplete="off" id="axial_length_re"
                            name="axial_length_re">
                    </td>
                    <td colspan="3" class="common_td_rules">
                        <input class="form-control cataract_data" type="text" autocomplete="off" id="axial_length_le"
                            name="axial_length_le">
                    </td>
                </tr>
                <tr>
                    <td class="td_common_numeric_rules">Keratometry</td>
                    <td colspan="3" class="common_td_rules">
                        <input class="form-control cataract_data" type="text" autocomplete="off" id="keratometry_re"
                            name="keratometry_re">
                    </td>
                    <td colspan="3" class="common_td_rules">
                        <input class="form-control cataract_data" type="text" autocomplete="off" id="keratometry_le"
                            name="keratometry_le">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-4 padding_sm" style="margin-top: 10px">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">A Constant</label>
            <div class="clearfix"></div>
            <input class="form-control cataract_data" type="text" autocomplete="off" id="a_constant" name="a_constant">
        </div>
    </div>
    <div class="col-md-8 padding_sm" style="margin-top: 10px">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">IOL Formula</label>
            <div class="clearfix"></div>
            <input class="form-control cataract_data" type="text" autocomplete="off" id="iol_formula" name="iol_formula">
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-4 padding_sm" style="margin-top: 10px">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">IOL Emm</label>
            <div class="clearfix"></div>
            <input class="form-control cataract_data" type="text" autocomplete="off" id="iol_emm" name="iol_emm">
        </div>
    </div>
    <div class="col-md-8 padding_sm" style="margin-top: 10px">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">Type of IOL</label>
            <div class="clearfix"></div>
            <input class="form-control cataract_data" type="text" autocomplete="off" id="type_of_iol" name="type_of_iol">
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-4 padding_sm" style="margin-top: 10px">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">IOL Power Used</label>
            <div class="clearfix"></div>
            <input class="form-control cataract_data" type="text" autocomplete="off" id="iol_power_used" name="iol_power_used">
        </div>
    </div>
    <div class="col-md-8 padding_sm" style="margin-top: 10px">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">IOL Details</label>
            <div class="clearfix"></div>
            <input class="form-control cataract_data" type="text" autocomplete="off" id="iol_details" name="iol_details">
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 padding_sm" style="margin-top: 10px">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">IOL Comments</label>
            <div class="clearfix"></div>
            <input class="form-control cataract_data" type="text" autocomplete="off" id="iol_comments" name="iol_comments">
        </div>
    </div>
</div>
<div class="col-md-4 padding_sm">
    <div class="box no-border no-margin">
        <div class="box-footer revenue_main_shadow" style="min-height: 640px;">
            <div class="col-md-8 blue padding_sm" style="font-weight: 600;">History</div>
            <div class="col-md-4 padding_sm"  id="cataract_data_history" style="display: none">
                <button onclick="resetEmrData(10, 2);" style="padding: 0px 4px" class="btn btn-success" type="button">Add
                    New <i class="fa fa-plus"></i></button>
            </div>
            <div class="clearfix"></div>
            <div class="theadscroll always-visible" style="position: relative; height: 630px;"
                id="eyeEmrCataractSurgery">

            </div>
        </div>
    </div>
</div>
