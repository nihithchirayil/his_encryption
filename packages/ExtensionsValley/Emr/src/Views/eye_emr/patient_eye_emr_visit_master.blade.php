<div class="col-md-9 no-padding patient_data_list_div" style="height:98vh;">
    <div class="col-md-12 no-padding" style="height:100%;">
        <div class="col-md-12 padding_sm patient_details_inner_div">
            <div class="col-md-1 padding_sm" style="height:60px;">
                <div class="patient_details_patient_image_container text-normal">
                    <img class="img-circle patient_details_patient_image" id="patient_image_url" src="" alt="Patient" />
                    <div class="video_consultation_div">
                        <button class="" title="Video Consultation">
                            <i class="fa fa-video-camera"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-2 padding_sm dashed_border_box">
                <div class="patient_details_patient_name text-normal"> Patient Name
                </div>
                <div class="patient_details_patient_uhid"> UHID </div>
                <div class="patient_details_patient_age_gender"> Age/Gender </div>
            </div>
            <div class="col-md-3 padding_sm dashed_border_box">
                <div class="patient_details_patient_address"> Address : -- </div>
                <div class="patient_details_patient_place"> Place : -- </div>
                <span class="patient_details_patient_district">District : -- </span>
            </div>
            <div class="col-md-2 padding_sm dashed_border_box">
                <div class="patient_details_patient_company"> Company Name : -- </div>
                <div class="patient_details_patient_pricing"> Pricing Name : -- </div>
                <span class="patient_details_patient_phone">Mobile : -- </span>
            </div>
            <div class="col-md-2 padding_sm dashed_border_box">
                @if ($show_patient_reference == '1')
                <div class="patient_details_co_type">C/O Type : -- </div>
                <div class="patient_details_co_name">C/O Name : -- </div>
                <div class="patient_details_co_mobile">C/O Mobile : -- </div>
                @else
                <div class="patient_details_patient_covid_history">Covid History : -- </div>
                <div class="patient_details_patient_vaccine_type">Type of vaccine : -- </div>
                <div class="">&nbsp;</div>
                @endif
            </div>
            <div class="col-md-2 padding_sm dashed_border_box timer_container">
                <label id="minutes">00</label>:<label id="seconds">00</label>
            </div>



        </div>
        <div class="col-md-3 padding_sm patient_details_div">

            <div class="col-md-12 padding_sm">
                <div class="col-md-2 no-padding left_side_tools_container__parent">
                    <div class="">
                        <a class="expanding_button patient_refer_btn">
                            <span class="expanding_button_icon"><i class="fa fa-user-plus"></i></span>
                            <span class="expanding_button_text">Refer Patient</span>
                        </a>
                        <a class="expanding_button patient_transfer_btn">
                            <span class="expanding_button_icon"><i class="fa fa-exchange"></i></span>
                            <span class="expanding_button_text">Transfer Patient</span>
                        </a>
                        <a class="expanding_button patient_documents_btn">
                            <span class="expanding_button_icon"><i class="fa fa-archive"></i></span>
                            <span class="expanding_button_text">Documents</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-10 padding_sm patient_demographics_container__parent">
                    <div class="col-md-12 padding_sm refraction_and_notes_div">
                        <div class="col-md-12 no-padding" style="height: 45px;margin-top: 15px;">
                            <div class="text_head blue"> Refraction <i onclick="addRefraction()" id="addRefractionSpin"
                                    class="pull-right fa fa-plus-circle btn-blue-sm"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm refraction_and_notes_div">
                        <div class="col-md-12 padding_sm" style="height: 30px;">
                            <div class="text_head">Refraction Notes</div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="refraction_notes_div" class="show_presenting_complaints_history theadscroll"
                            style="height: 70px;">
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm refraction_and_notes_div">
                        <div class="col-md-12 padding_sm" style="height: 30px;">
                            <div class="text_head">Presenting Complaints</div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="show_presenting_complaints_div" class="show_presenting_complaints_history theadscroll"
                            style="height: 70px;">
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm refraction_and_notes_div">
                        <div class="col-md-12 padding_sm" style="height: 30px;">
                            <div class="text_head">Past History</div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="show_eye_ast_history_div" class="show_presenting_complaints_history theadscroll"
                            style="height: 70px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 padding_sm visit_details_div">
            <div class="col-md-12 padding_sm visit_details_inner_div">

                <div class="tab_wrapper first_tab _side">
                    <ul class="tab_list">
                        <li rel="tab_1" class="active">Notes</li>
                        <li rel="tab_2" class="">Investigation</li>
                        <li rel="tab_3" class="">Prescription</li>
                        <li rel="tab_4" class="">Glass Prescription</li>
                        <li rel="tab_5" class="">Image Marking</li>
                    </ul>

                    <div class="content_wrapper">
                        <div class="tab_content tab_1" id="dynamic_notes_template">
                            @include('Emr::eye_emr.eye_lite_note_template')
                        </div>
                        <div class="tab_content tab_2">
                            @include('Emr::view_patient.investigation_template')
                        </div>
                        <div class="tab_content tab_3">
                            @include('Emr::view_patient.prescription_template')
                        </div>
                        <div class="tab_content tab_4">
                            @include('Emr::eye_emr.eye_lite_note_glass_prescription')
                        </div>
                        <div class="tab_content tab_5">
                            @include('Emr::eye_emr.eye_imagecanvas')
                        </div>
                    </div>
                </div>

                <div class="saveButtonDiv">
                    <button class="btn btn-sm saveClinicalDataButton" type="button"><i class="fa fa-save"></i>
                        Save</button>
                    <button class="btn btn-sm saveAndPrintClinicalDataButton" type="button"><i class="fa fa-save"></i>
                        Save & Print</button>
                </div>
            </div>
        </div>
    </div>
</div>
