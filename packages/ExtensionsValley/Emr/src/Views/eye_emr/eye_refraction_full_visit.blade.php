<div class="row padding_sm">
    @if (count($eye_refraction_full_details) != 0)
    @foreach ($eye_refraction_full_details as $each)
    @php
    $refraction_head_full = @$each->refraction_head_json ? json_decode($each->refraction_head_json, true) : [];
    $presenting_complaints = @$refraction_head_full['presenting_complaints'] ?
    $refraction_head_full['presenting_complaints'] : '';
    $past_history = @$refraction_head_full['past_history'] ? $refraction_head_full['past_history'] : '';
    $patient_refraction_data_full = @$each->refraction_json ? json_decode($each->refraction_json, true) : [];
    $patient_pg_power_full = @$patient_refraction_data_full['pg_power'] ? $patient_refraction_data_full['pg_power'] :
    [];
    @endphp
    <div class="col-md-12 padding_sm table_box">
        <div class="col-md-6 padding_sm eye_refraction_header" style="margin-top: 10px;text-align: center">
            {{ @$each->doctor_name ? $each->doctor_name : '-' }}
        </div>
        <div class="col-md-6 padding_sm eye_refraction_header" style="margin-top: 10px;text-align: center">
            {{ @$each->created_at ? $each->created_at : '-' }}
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 padding_sm table_box" style="margin-top: 5px;height:200px !important">
            <div class="col-md-12 padding_sm eye_refraction_header" style="margin-top: 5px;text-align: center">
                Right Eye (RE)
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 5px">
                <strong> Snellen Chart</strong>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 5px;">
                <div class="col-md-3 padding_sm">
                    <label class="blue">DV</label>
                    <div class="clearfix"></div>
                    <label>{{ @$patient_refraction_data_full['snellen_right']['SRDV'] ?
                        $patient_refraction_data_full['snellen_right']['SRDV'] : 0 }}</label>
                </div>
                <div class="col-md-3 padding_sm">
                    <label class="blue">NV</label>
                    <div class="clearfix"></div>
                    <label>{{ @$patient_refraction_data_full['snellen_right']['SRNV'] ?
                        $patient_refraction_data_full['snellen_right']['SRNV'] : 0 }}</label>
                </div>
                <div class="col-md-3 padding_sm">
                    <label class="blue">DV PG</label>
                    <div class="clearfix"></div>
                    <label>{{ @$patient_refraction_data_full['snellen_right']['SRDVPG'] ?
                        $patient_refraction_data_full['snellen_right']['SRDVPG'] : 0 }}</label>
                </div>
                <div class="col-md-3 padding_sm">
                    <label class="blue">NV PG</label>
                    <div class="clearfix"></div>
                    <label>{{ @$patient_refraction_data_full['snellen_right']['SRNVPG'] ?
                        $patient_refraction_data_full['snellen_right']['SRNVPG'] : 0 }}</label>
                </div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 5px;">
                <table class="table no-margin table_sm no-border">
                    <thead>
                        <tr class="table_header_bg">
                            <th>Sphere</th>
                            <th>Cylinder</th>
                            <th>AXIS</th>
                            <th title="Corrected Vision">Cor.Vn.</th>
                            <th><i class="fa fa-list"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <label>{{ @$patient_refraction_data_full['right']['ArSPH'] ?
                                    $patient_refraction_data_full['right']['ArSPH'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['right']['ArCyl'] ?
                                    $patient_refraction_data_full['right']['ArCyl'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['right']['ArAxix'] ?
                                    $patient_refraction_data_full['right']['ArAxix'] : 0 }}</label>
                            </td>
                            <td>
                                -
                            </td>
                            <th>
                                AR
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <label>{{ @$patient_refraction_data_full['right']['DvSPH'] ?
                                    $patient_refraction_data_full['right']['DvSPH'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['right']['DvCyl'] ?
                                    $patient_refraction_data_full['right']['DvCyl'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['right']['DvAxix'] ?
                                    $patient_refraction_data_full['right']['DvAxix'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['right']['DvCorrVn'] ?
                                    $patient_refraction_data_full['right']['DvCorrVn'] : 0 }}</label>
                            </td>
                            <th>
                                DV
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <label>{{ @$patient_refraction_data_full['right']['NvSPH'] ?
                                    $patient_refraction_data_full['right']['NvSPH'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['right']['NvCyl'] ?
                                    $patient_refraction_data_full['right']['NvCyl'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['right']['NvAxix'] ?
                                    $patient_refraction_data_full['right']['NvAxix'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['right']['NvCorrVn'] ?
                                    $patient_refraction_data_full['right']['NvCorrVn'] : 0 }}</label>
                            </td>
                            <th>
                                NV
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6 padding_sm table_box" style="margin-top: 5px;height:200px !important">
            <div class="col-md-12 padding_sm eye_refraction_header" style="margin-top: 5px;text-align: center">
                Left Eye (LE)
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 5px">
                <strong> Snellen Chart</strong>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 5px;">
                <div class="col-md-3 padding_sm">
                    <label class="blue">DV</label>
                    <div class="clearfix"></div>
                    <label>{{ @$patient_refraction_data_full['snellen_left']['SLDV'] ?
                        $patient_refraction_data_full['snellen_left']['SLDV'] : 0 }}</label>
                </div>
                <div class="col-md-3 padding_sm">
                    <label class="blue">NV</label>
                    <div class="clearfix"></div>
                    <label>{{ @$patient_refraction_data_full['snellen_left']['SLNV'] ?
                        $patient_refraction_data_full['snellen_left']['SLNV'] : 0 }}</label>
                </div>
                <div class="col-md-3 padding_sm">
                    <label class="blue">DV PG</label>
                    <div class="clearfix"></div>
                    <label>{{ @$patient_refraction_data_full['snellen_left']['SLDVPG'] ?
                        $patient_refraction_data_full['snellen_left']['SLDVPG'] : 0 }}</label>
                </div>
                <div class="col-md-3 padding_sm">
                    <label class="blue">NV PG</label>
                    <div class="clearfix"></div>
                    <label>{{ @$patient_refraction_data_full['snellen_left']['SLNVPG'] ?
                        $patient_refraction_data_full['snellen_left']['SLNVPG'] : 0 }}</label>
                </div>
            </div>
            <div class="col-md-12 padding_sm" style="margin-top: 5px;">
                <table class="table no-margin table_sm no-border">
                    <thead>
                        <tr class="table_header_bg">
                            <th>Sphere</th>
                            <th>Cylinder</th>
                            <th>AXIS</th>
                            <th title="Corrected Vision">Cor.Vn.</th>
                            <th><i class="fa fa-list"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <label>{{ @$patient_refraction_data_full['left']['ArSPH'] ?
                                    $patient_refraction_data_full['left']['ArSPH'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['left']['ArCyl'] ?
                                    $patient_refraction_data_full['left']['ArCyl'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['left']['ArAxix'] ?
                                    $patient_refraction_data_full['left']['ArAxix'] : 0 }}</label>
                            </td>
                            <td>
                                -
                            </td>
                            <th>
                                AR
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <label>{{ @$patient_refraction_data_full['left']['DvSPH'] ?
                                    $patient_refraction_data_full['left']['DvSPH'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['left']['DvCyl'] ?
                                    $patient_refraction_data_full['left']['DvCyl'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['left']['DvAxix'] ?
                                    $patient_refraction_data_full['left']['DvAxix'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['left']['DvCorrVn'] ?
                                    $patient_refraction_data_full['left']['DvCorrVn'] : 0 }}</label>
                            </td>
                            <th>
                                DV
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <label>{{ @$patient_refraction_data_full['left']['NvSPH'] ?
                                    $patient_refraction_data_full['left']['NvSPH'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['left']['NvCyl'] ?
                                    $patient_refraction_data_full['left']['NvCyl'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['left']['NvAxix'] ?
                                    $patient_refraction_data_full['left']['NvAxix'] : 0 }}</label>
                            </td>
                            <td>
                                <label>{{ @$patient_refraction_data_full['left']['NvCorrVn'] ?
                                    $patient_refraction_data_full['left']['NvCorrVn'] : 0 }}</label>
                            </td>
                            <th>
                                NV
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @if (count($patient_pg_power_full) != 0)
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm eye_refraction_header" style="margin-top: 10px;text-align: center">
            PG
            Power
        </div>
        @foreach ($patient_pg_power_full as $each)
        <div class="col-md-12 padding_sm table_box" style="margin-top: 5px;height:90px !important">
            <div class="col-md-6 padding_sm" style="margin-top: 10px;text-align: center">
                <table class="table no-margin table_sm no-border">
                    <thead>
                        <tr class="table_header_bg">
                            <th title="Right Eye">RE</th>
                            <th colspan="3">
                                {{ @$each['right']['RGUF'] ? $each['right']['RGUF'] : '' }}
                            </th>
                        </tr>
                        <tr class="table_header_bg">
                            <th>Sphere</th>
                            <th>Cylinder</th>
                            <th>AXIS</th>
                            <th><i class="fa fa-list"></i></th>
                        </tr>
                    </thead>
                    <tbody class="right_power">
                        <tr>
                            <td>
                                {{ @$each['right']['RDvSPH'] ? $each['right']['RDvSPH'] : 0 }}
                            </td>
                            <td>
                                {{ @$each['right']['RDvCyl'] ? $each['right']['RDvCyl'] : 0 }}
                            </td>
                            <td>
                                {{ @$each['right']['RDvAxix'] ? $each['right']['RDvAxix'] : 0 }}
                            </td>
                            <th>
                                DV
                            </th>
                        </tr>
                        <tr>
                            <td>
                                {{ @$each['right']['RNvSPH'] ? $each['right']['RNvSPH'] : 0 }}
                            </td>
                            <td>
                                {{ @$each['right']['RNvCyl'] ? $each['right']['RNvCyl'] : 0 }}
                            </td>
                            <td>
                                {{ @$each['right']['RNvAxix'] ? $each['right']['RNvAxix'] : 0 }}
                            </td>
                            <th>
                                NV
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6 padding_sm" style="margin-top: 10px;text-align: center">
                <table class="table no-margin table_sm no-border">
                    <thead>
                        <tr class="table_header_bg">
                            <th title="Left Eye">LE</th>
                            <th colspan="3">
                                {{ @$each['left']['LGUF'] ? $each['left']['LGUF'] : '' }}
                            </th>
                        </tr>
                        <tr class="table_header_bg">
                            <th>Sphere</th>
                            <th>Cylinder</th>
                            <th>AXIS</th>
                            <th><i class="fa fa-list"></i></th>
                        </tr>
                    </thead>
                    <tbody class="left_power">
                        <tr>
                            <td>
                                {{ @$each['left']['LDvSPH'] ? $each['left']['LDvSPH'] : 0 }}
                            </td>
                            <td>
                                {{ @$each['left']['LDvCyl'] ? $each['left']['LDvCyl'] : 0 }}
                            </td>
                            <td>
                                {{ @$each['left']['LDvAxix'] ? $each['left']['LDvAxix'] : 0 }}
                            </td>
                            <th>
                                DV
                            </th>
                        </tr>
                        <tr>
                            <td>
                                {{ @$each['left']['LNvSPH'] ? $each['left']['LNvSPH'] : 0 }}
                            </td>
                            <td>
                                {{ @$each['left']['LNvCyl'] ? $each['left']['LNvCyl'] : 0 }}
                            </td>
                            <td>
                                {{ @$each['left']['LNvAxix'] ? $each['left']['LNvAxix'] : 0 }}
                            </td>
                            <th>
                                NV
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
        @endforeach
        @endif
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm eye_refraction_header" style="margin-top: 10px;text-align: center">
            Presenting Complaints
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm theadscroll" style="position: relative;height:50px">
            {{ $presenting_complaints }}
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm eye_refraction_header" style="margin-top: 10px;text-align: center">
            Past History
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 padding_sm theadscroll" style="position: relative;height:50px">
            {{ $past_history }}
        </div>
        <div class="clearfix"></div>
    </div>
    @endforeach
    @endif
</div>
