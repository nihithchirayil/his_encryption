<!-- .css -->
<style>
    #doctorHeadClass p:first-child {
        margin: 0 !important;
        padding: 0 !important;
    }
</style>
<style type="text/css" media="print">
    @page {
        margin: 15px;
    }

    table {
        font-size: 13px;
    }

    @media print {
        .pagebreak {
            page-break-before: always;
        }

        /* page-break-after works, as well */
    }
</style>
<!-- .css -->
<!-- .box-body -->
<div class="box-body">
    @php
    $hospital_header_disable_in_prescription = 0;
    $investigation_types = array();
    @endphp

    <div class="col-md-12 no-padding" @if($hospital_header_disable_in_prescription==1) style="margin-top:3cm;" @endif>
        @include('Emr::emr.investigation.investigation_hospital_header')
        <!-----------Notes Print Data----------------------------------->
        @php
        $data_text = $data_text ? json_decode($data_text, true) : array();
        $i=1;
        @endphp
        @if(intval($head_id)==1)
        <div class="col-md-12">
            <div class="col-md-12" style="margin:20px; text-align:center;">
                <h3>Notes</h3>
            </div>
            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"
                style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%"
                border="1" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:5%;"><strong>SL#</strong></td>
                    <td style="width:35%;"><strong>
                            <center>Reason</center>
                        </strong></td>
                    <td style="width:60%;"><strong>
                            <center>Notes</center>
                        </strong></td>
                </tr>
                @if(count($data_text)!=0)
                @foreach($data_text as $key=>$val)
                <tr>
                    <td style="width:5%;"> {{ $i }} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!! @$notes_head[$key] ? $notes_head[$key]
                        : '' !!} </td>
                    <td style="width:60%; text-align:left;padding-left:5px;"> {!! $val !!} </td>
                </tr>
                @php
                $i++;
                @endphp
                @endforeach
                @endif

            </table>
        </div>

        @elseif(intval($head_id)==2)
        <!-----------Refraction Print Data----------------------------------->

        <div class="col-md-12">
            <div class="col-md-12" style="margin:20px; text-align:center;">
                <h3>Refraction</h3>
            </div>
            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"
                style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%"
                border="1" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:50%;"><strong>
                            <center>Refractive Aid</center>
                        </strong></td>
                    <td style="width:50%;"><strong>
                            <center>Using Since</center>
                        </strong></td>

                </tr>
                <tr>
                    <td style="width:50%; text-align:left;padding-left:5px;">
                        @php
                        $refraction_types_id= @$data_text['refraction_types'] ? $data_text['refraction_types'] : ''
                        @endphp
                        @foreach ($refraction_types as $each)
                        @if($refraction_types_id==$each->id)
                        {{ $each->refraction_name }}
                        @endif
                        @endforeach
                    </td>
                    <td style="width:50%; text-align:left;padding-left:5px;"> {!! @$data_text['refraction_usingsince'] ?
                        $data_text['refraction_usingsince'] : '' !!} </td>
                </tr>
            </table>

            <div style="margin:10px; text-align:center;">

                <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"
                    style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;"
                    width="100%" border="1" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width:30%;"><strong>
                                <center>Type</center>
                            </strong></td>
                        <td style="width:35%;"><strong>
                                <center>Right Eye</center>
                            </strong></td>
                        <td style="width:35%;"><strong>
                                <center>Left Eye</center>
                            </strong></td>

                    </tr>
                    <tr>
                        <td style="width:30%; text-align:left;padding-left:5px;">
                            Reason
                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                            @$data_text['right']['refraction_right_eye'] ? $data_text['right']['refraction_right_eye'] :
                            '' !!} </td>
                        <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                            @$data_text['left']['refraction_left_eye'] ? $data_text['left']['refraction_left_eye'] : ''
                            !!} </td>
                    </tr>
                    <tr>
                        <td style="width:30%; text-align:left;padding-left:5px;">
                            Vision
                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">

                            @php
                            $checkData= @$data_text['right']['revision_types'] ? $data_text['right']['revision_types'] :
                            ''
                            @endphp
                            @foreach ($vision_types as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_type }}
                            @endif
                            @endforeach

                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">
                            @php
                            $checkData= @$data_text['left']['levision_types'] ? $data_text['left']['levision_types'] :
                            ''
                            @endphp
                            @foreach ($vision_types as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_type }}
                            @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%; text-align:left;padding-left:5px;">
                            Near Vision
                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">

                            @php
                            $checkData= @$data_text['right']['renear_vision'] ? $data_text['right']['renear_vision'] :
                            ''
                            @endphp
                            @foreach ($near_vision as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_code }}
                            @endif
                            @endforeach

                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">
                            @php
                            $checkData= @$data_text['left']['lenear_vision'] ? $data_text['left']['lenear_vision'] : ''
                            @endphp
                            @foreach ($near_vision as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_code }}
                            @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%; text-align:left;padding-left:5px;">
                            UCVA
                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">
                            @php
                            $checkData= @$data_text['right']['ucva_re'] ? $data_text['right']['ucva_re'] : ''
                            @endphp
                            @foreach ($vision_types as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_type }}
                            @endif
                            @endforeach

                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">
                            @php
                            $checkData= @$data_text['left']['ucva_le'] ? $data_text['left']['ucva_le'] : ''
                            @endphp
                            @foreach ($vision_types as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_type }}
                            @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%; text-align:left;padding-left:5px;">
                            Pin Hole
                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">
                            @php
                            $checkData= @$data_text['right']['pinhoe_re'] ? $data_text['right']['pinhoe_re'] : ''
                            @endphp
                            @foreach ($vision_types as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_type }}
                            @endif
                            @endforeach

                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">
                            @php
                            $checkData= @$data_text['left']['pinhoe_le'] ? $data_text['left']['pinhoe_le'] : ''
                            @endphp
                            @foreach ($vision_types as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_type }}
                            @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%; text-align:left;padding-left:5px;">
                            BSCVA
                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">
                            @php
                            $checkData= @$data_text['right']['bscva_re'] ? $data_text['right']['bscva_re'] : ''
                            @endphp
                            @foreach ($vision_types as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_type }}
                            @endif
                            @endforeach

                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">
                            @php
                            $checkData= @$data_text['left']['bscva_le'] ? $data_text['left']['bscva_le'] : ''
                            @endphp
                            @foreach ($vision_types as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_type }}
                            @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%; text-align:left;padding-left:5px;">
                            Near
                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">
                            @php
                            $checkData= @$data_text['right']['near_re'] ? $data_text['right']['near_re'] : ''
                            @endphp
                            @foreach ($near_vision as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_code }}
                            @endif
                            @endforeach

                        </td>
                        <td style="width:35%; text-align:left;padding-left:5px;">
                            @php
                            $checkData= @$data_text['left']['near_le'] ? $data_text['left']['near_le'] : ''
                            @endphp
                            @foreach ($near_vision as $each)
                            @if($checkData==$each->id)
                            {{ $each->vision_code }}
                            @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%; text-align:left;padding-left:5px;">
                            Type of Glasses
                        </td>
                        <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                            @$data_text['type_of_glasses'] ? $data_text['type_of_glasses']: ''
                            !!}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        @elseif(intval($head_id)==3)
        <div class="col-md-12">
            <div class="col-md-12" style="margin:20px; text-align:center;">
                <h3>IOP</h3>
            </div>
            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"
                style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%"
                border="1" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:30%;"><strong>
                            <center>Type</center>
                        </strong></td>
                    <td style="width:35%;"><strong>
                            <center>Right Eye</center>
                        </strong></td>
                    <td style="width:35%;"><strong>
                            <center>Left Eye</center>
                        </strong></td>

                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        IOP
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['iop_re'] ? $data_text['right']['iop_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['iop_le'] ? $data_text['left']['iop_le'] : ''
                        !!} </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        mm HG
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['mm_hg_re'] ? $data_text['right']['mm_hg_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['mm_hg_le'] ? $data_text['left']['mm_hg_le'] : ''
                        !!} </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Finger Tension
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['finger_tension_re'] ? $data_text['right']['finger_tension_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['finger_tension_le'] ? $data_text['left']['finger_tension_le'] : ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Pachymetry
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['pachymetry_re'] ? $data_text['right']['pachymetry_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['pachymetry_le'] ? $data_text['left']['pachymetry_le'] : ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Gonioscopy
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['gonioscopy_re'] ? $data_text['right']['gonioscopy_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['gonioscopy_le'] ? $data_text['left']['gonioscopy_le'] : ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Glaucoma Assessment Comments
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['glaucoma_assessment_comments'] ? $data_text['glaucoma_assessment_comments']: ''
                        !!}
                    </td>
                </tr>
            </table>
        </div>


        @elseif(intval($head_id)==4)
        <div class="col-md-12">
            <div class="col-md-12" style="margin:20px; text-align:center;">
                <h3>Examination</h3>
            </div>
            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"
                style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%"
                border="1" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:30%;"><strong>
                            <center>Type</center>
                        </strong></td>
                    <td style="width:35%;"><strong>
                            <center>Right Eye</center>
                        </strong></td>
                    <td style="width:35%;"><strong>
                            <center>Left Eye</center>
                        </strong></td>

                </tr>

                @if(count($examination_types)!=0)
                @foreach ($examination_types as $each)
                @php
                $diagnosis_name = strtolower(str_replace(" ", "",$each->examination_name));
                @endphp
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">{{ $each->examination_name }}</td>
                    <td style="width:35%; text-align:left;padding-left:5px;">
                        @php
                        $checkData= @$data_text['right']['right'.$diagnosis_name]
                        ?$data_text['right']['right'.$diagnosis_name] : ''
                        @endphp
                        @foreach ($eye_diagnosis as $data)
                        @if($data->examination_type==$each->id)
                        @if($checkData==$data->id)
                        {{ $data->diagnosis_name }}
                        @endif
                        @endif
                        @endforeach
                    </td>

                    <td style="width:35%; text-align:left;padding-left:5px;">
                        @php
                        $checkData= @$data_text['left']['left'.$diagnosis_name]
                        ?$data_text['left']['left'.$diagnosis_name] : ''
                        @endphp
                        @foreach ($eye_diagnosis as $data)
                        @if($data->examination_type==$each->id)
                        @if($checkData==$data->id)
                        {{ $data->diagnosis_name }}
                        @endif
                        @endif
                        @endforeach
                    </td>
                </tr>
                @endforeach
                @endif
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        EOM
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['eom'] ? $data_text['eom']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Cover Test
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['cover_test'] ? $data_text['cover_test']: ''
                        !!}
                    </td>
                </tr>
            </table>
        </div>

        @elseif(intval($head_id)==5)
        <div class="col-md-12">
            <div class="col-md-12" style="margin:20px; text-align:center;">
                <h3>Investigation</h3>
            </div>
            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"
                style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%"
                border="1" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:30%;"><strong>
                            <center>Type</center>
                        </strong></td>
                    <td style="width:35%;"><strong>
                            <center>Right Eye</center>
                        </strong></td>
                    <td style="width:35%;"><strong>
                            <center>Left Eye</center>
                        </strong></td>

                </tr>

                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Corneal Topography
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['corneal_topography_re'] ? $data_text['right']['corneal_topography_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['corneal_topography_le'] ? $data_text['left']['corneal_topography_le'] :
                        '' !!} </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Visual Fields
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['visual_fields_re'] ? $data_text['right']['visual_fields_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['visual_fields_le'] ? $data_text['left']['visual_fields_le'] :
                        '' !!} </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Pachymetry
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['pachymetry_re'] ? $data_text['right']['pachymetry_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['pachymetry_le'] ? $data_text['left']['pachymetry_le'] :
                        '' !!} </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Specular Microscopy
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['specular_microscopy_re'] ? $data_text['right']['specular_microscopy_re'] :
                        '' !!} cells/mm<sup>2</sup> </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['specular_microscopy_le'] ? $data_text['left']['specular_microscopy_le'] :
                        '' !!} cells/mm<sup>2</sup> </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        ACID
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['acid_re'] ? $data_text['right']['acid_re'] :
                        '' !!} mm</td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['acid_le'] ? $data_text['left']['acid_le'] :
                        '' !!} mm</td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        ASOCT
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['asoct_re'] ? $data_text['right']['asoct_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['asoct_le'] ? $data_text['left']['asoct_le'] :
                        '' !!} </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        OCT
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['right']['oct_re'] ? $data_text['right']['oct_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['left']['oct_le'] ? $data_text['left']['oct_le'] :
                        '' !!} </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        USG B Scan
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['usg_b_scan'] ? $data_text['usg_b_scan'] :
                        '' !!} </td>
                </tr>
            </table>
        </div>


        @elseif(intval($head_id)==6)
        <div class="col-md-12">
            <div class="col-md-12" style="margin:20px; text-align:center;">
                <h3>Diagnosis</h3>
            </div>
            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"
                style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%"
                border="1" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:15%;"><strong>
                            <center>Si.No.</center>
                        </strong></td>
                    <td style="width:50%;"><strong>
                            <center>Diagnosis</center>
                        </strong></td>
                    <td style="width:35%;"><strong>
                            <center>Eye Side</center>
                        </strong></td>

                </tr>
                @php
                $diagosis= $data_text['diagosis'] ? $data_text['diagosis'] : array();
                $i=1;
                @endphp
                @if(count($diagosis)!=0)
                @foreach ($diagosis as $each)
                <tr>
                    <td style="width:15%; text-align:left;padding-left:5px;">{{ $i }}</td>
                    <td style="width:50%; text-align:left;padding-left:5px;">
                        @php
                        $checkData = $each['diagnosis_name'] ? $each['diagnosis_name'] : '';
                        @endphp
                        @foreach ($eye_diagnosis as $data)
                        @if($checkData==$data->id)
                        {{ $data->diagnosis_name }}
                        @endif
                        @endforeach
                    </td>

                    <td style="width:35%; text-align:left;padding-left:5px;">
                        @php
                        $checkData= $each['eye_side'] ? $each['eye_side'] : '';
                        @endphp
                        @foreach ($eye_side as $key=>$val)
                        @if($checkData==$key)
                        {{ $val }}
                        @endif
                        @endforeach
                    </td>
                </tr>
                @php
                $i++;
                @endphp
                @endforeach
                @endif
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Advice
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['diagosisAdvice'] ? $data_text['diagosisAdvice']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Next Review
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['diagosisNextReview'] ? $data_text['diagosisNextReview']: ''
                        !!}
                    </td>
                </tr>
            </table>
        </div>

        @elseif(intval($head_id)==8)
        <div class="col-md-12">
            <div class="col-md-12" style="margin:20px; text-align:center;">
                <h3>Procedure</h3>
            </div>
            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"
                style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%"
                border="1" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Procedure
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['procedure_name'] ? $data_text['procedure_name']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Eye
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;">
                        @php
                        $checkData= $data_text['procedure_eye'] ? $data_text['procedure_eye']: '';
                        @endphp
                        @foreach ($eye_side as $key=>$val)
                        @if($checkData==$key)
                        {{ $val }}
                        @endif
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Procedure Date
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['procedure_date'] ? $data_text['procedure_date']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Procedure Details
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['procedure_details'] ? $data_text['procedure_details']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Procedure Comments
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['procedure_comments'] ? $data_text['procedure_comments']: ''
                        !!}
                    </td>
                </tr>
            </table>
        </div>


        @elseif(intval($head_id)==10)
        <div class="col-md-12">
            <div class="col-md-12" style="margin:20px; text-align:center;">
                <h3>Cataract Surgery</h3>
            </div>
            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"
                style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%"
                border="1" cellspacing="0" cellpadding="0">


                <tr>
                    <td style="width:30%;"><strong>
                            <center>Type</center>
                        </strong></td>
                    <td style="width:35%;"><strong>
                            <center>Right Eye</center>
                        </strong></td>
                    <td style="width:35%;"><strong>
                            <center>Left Eye</center>
                        </strong></td>

                </tr>

                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Axial Length
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['axial_length_re'] ? $data_text['axial_length_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['axial_length_le'] ? $data_text['axial_length_le'] :
                        '' !!} </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Keratometry
                    </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['keratometry_re'] ? $data_text['keratometry_re'] :
                        '' !!} </td>
                    <td style="width:35%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['keratometry_le'] ? $data_text['keratometry_le'] :
                        '' !!} </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        A Constant
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['a_constant'] ? $data_text['a_constant']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        IOL Formula
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['iol_formula'] ? $data_text['iol_formula']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        IOL Emm
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['iol_emm'] ? $data_text['iol_emm']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Type of IOL
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['type_of_iol'] ? $data_text['type_of_iol']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        IOL Power Used
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['iol_power_used'] ? $data_text['iol_power_used']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        IOL Details
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['iol_details'] ? $data_text['iol_details']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        IOL Comments
                    </td>
                    <td colspan="2" style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['iol_comments'] ? $data_text['iol_comments']: ''
                        !!}
                    </td>
                </tr>
            </table>
        </div>


        @elseif(intval($head_id)==11)
        <div class="col-md-12">
            <div class="col-md-12" style="margin:20px; text-align:center;">
                <h3>Corneal Surgery</h3>
            </div>
            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"
                style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%"
                border="1" cellspacing="0" cellpadding="0">

                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Donor Details
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['donor_details'] ? $data_text['donor_details']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Primary Indication
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['primary_indication'] ? $data_text['primary_indication']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Host Diameter
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['host_diameter'] ? $data_text['host_diameter']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Donor Diameter
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['donor_diameter'] ? $data_text['donor_diameter']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Surfing Technique
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['surfing_technique'] ? $data_text['surfing_technique']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        DALK Technique
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['dalk_technique'] ? $data_text['dalk_technique']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        EK Donor Thickness
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['ek_donor_thickness'] ? $data_text['ek_donor_thickness']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Type of Bubble
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['type_of_bubble'] ? $data_text['type_of_bubble']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        EK Tamponade
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['ek_tamponade'] ? $data_text['ek_tamponade']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Donar Thickness DALK
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['donar_thickness_dalk'] ? $data_text['donar_thickness_dalk']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        EK Complications
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['ek_complications'] ? $data_text['ek_complications']: ''
                        !!}
                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left;padding-left:5px;">
                        Comments
                    </td>
                    <td style="width:70%; text-align:left;padding-left:5px;"> {!!
                        @$data_text['corneal_comments'] ? $data_text['corneal_comments']: ''
                        !!}
                    </td>
                </tr>
            </table>
        </div>
        @endif
    </div>
</div>
<!-- .box-body -->
