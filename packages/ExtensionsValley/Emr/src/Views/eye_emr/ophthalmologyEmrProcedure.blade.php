<div class="row padding_sm">
    <div class="col-md-12 padding_sm">
        <input class="form-control" type="hidden" autocomplete="off" id="procedure_id" name="procedure_id">
        <ul class="nav nav-tabs sm_nav">
            <li class="active"><a data-toggle="tab" href="#procedureDetailsli" style="border-radius: 0px;"
                    aria-expanded="false">Procedures</a></li>
            <li class=""><a data-toggle="tab" href="#refractiveSurgeryli" style="border-radius: 0px;"
                    aria-expanded="false">Refractive Surgery</a></li>
            <li class=""><a data-toggle="tab" href="#cataractSurgeryli" style="border-radius: 0px;"
                    aria-expanded="true">Cataract Surgery</a></li>
            <li class=""><a data-toggle="tab" href="#cornealSurgeryli" style="border-radius: 0px;"
                    aria-expanded="true">Corneal Surgery</a></li>
            <li class=""><a data-toggle="tab" href="#procedureListViewli" style="border-radius: 0px;"
                    aria-expanded="true">Procedure List</a></li>
        </ul>
        <div class="tab-content">
            <div id="procedureDetailsli" class="tab-pane active">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix " style="min-height: 640px">
                        <div class="row padding_sm">
                            @include('Emr::eye_emr.procedureDetails')
                        </div>
                    </div>
                </div>
            </div>
            <div id="refractiveSurgeryli" class="tab-pane">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix " style="min-height: 640px">
                        <div class="row padding_sm">
                            @include('Emr::eye_emr.refrectiveSurgeryDetails')
                        </div>
                    </div>
                </div>

            </div>
            <div id="cataractSurgeryli" class="tab-pane">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix " style="min-height: 640px">
                        <div class="row padding_sm">
                            @include('Emr::eye_emr.cataractSurgeryDetails')
                        </div>
                    </div>
                </div>
            </div>
            <div id="cornealSurgeryli" class="tab-pane">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix " style="min-height: 640px">
                        <div class="row padding_sm">
                            @include('Emr::eye_emr.cornealSurgeryDetails')
                        </div>
                    </div>
                </div>
            </div>
            <div id="procedureListViewli" class="tab-pane">
                <div class="box no-border no-margin">
                    <div class="box-body clearfix " style="min-height: 640px">
                        <div class="row padding_sm">
                            @include('Emr::eye_emr.procedureListViewDetails')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
