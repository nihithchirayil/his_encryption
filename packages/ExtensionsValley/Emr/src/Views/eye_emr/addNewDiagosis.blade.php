@if(count($diagosis_data)!=0)
@foreach ($diagosis_data as $item)
<tr style="background: #FFF;" class="row_class_diagosis" id="rowDiagnosis{{ $newrow_id }}">
    <td class='row_count_diagosis'></td>
    <td>
        <select name="diagnosis_name" id="addDiagnosis{{ $newrow_id }}" class="form-control">
            <option value="">Select</option>
            @foreach ($eye_diagnosis as $each)
            @php
            $selected="";
            @endphp
            @if($item['diagnosis_name']==$each->id)
            @php
            $selected="selected";
            @endphp
            @endif
            <option {{ $selected }} value="{{ $each->id }}">{{ $each->diagnosis_name }}</option>
            @endforeach
        </select>
    </td>
    <td>
        <select name="eye_side" id="eyeSide{{ $newrow_id }}" class="form-control">
            <option value="">Select</option>
            @foreach ($eye_side as $key=>$val)
            @php
            $selected="";
            @endphp
            @if($item['eye_side']==$key)
            @php
            $selected="selected";
            @endphp
            @endif
            <option {{ $selected }} value="{{ $key }}">{{ $val }}</option>
            @endforeach
        </select>
    </td>
    <td style="text-align: center">
        <button id="deleteDiagnosisBtn{{ $newrow_id }}" title="Delete Diagosis" class="btn btn-danger"
            onclick="deleteDiagnosis({{ $newrow_id }})" type="button"><i id="deleteDiagnosisSpin{{ $newrow_id }}"
                class="fa fa-trash"></i></button>
    </td>
</tr>
@php $newrow_id++ @endphp
@endforeach
@else
<tr style="background: #FFF;" class="row_class_diagosis" id="rowDiagnosis{{ $newrow_id }}">
    <td class='row_count_diagosis'></td>
    <td>
        <select name="diagnosis_name" id="addDiagnosis{{ $newrow_id }}" class="form-control">
            <option value="">Select</option>
            @foreach ($eye_diagnosis as $each)
            <option value="{{ $each->id }}">{{ $each->diagnosis_name }}</option>
            @endforeach
        </select>
    </td>
    <td>
        <select name="eye_side" id="eyeSide{{ $newrow_id }}" class="form-control">
            <option value="">Select</option>
            @foreach ($eye_side as $key=>$val)
            <option value="{{ $key }}">{{ $val }}</option>
            @endforeach
        </select>
    </td>
    <td style="text-align: center">
        <button id="deleteDiagnosisBtn{{ $newrow_id }}" title="Delete Diagosis" class="btn btn-danger"
            onclick="deleteDiagnosis({{ $newrow_id }})" type="button"><i id="deleteDiagnosisSpin{{ $newrow_id }}"
                class="fa fa-trash"></i></button>
    </td>
</tr>
@endif
