<div class="col-md-8 padding_sm" style="margin-top: 10px">
    <input class="form-control refractive_data" value="0" type="hidden" autocomplete="off" id="ophthalmologyEmrhead9"
        name="ophthalmologyEmrhead9">
    <div class="col-md-12 padding_sm">
        <input class="form-control refractive_data" type="hidden" autocomplete="off" id="refractivesurgery_id"
            name="refractivesurgery_id">
        <table class="table no-margin table-striped table_sm table-col-bordered table-condensed"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg">
                    <th width="20%">Type</th>
                    <th width="40%">Right Eye</th>
                    <th width="40%">Left Eye</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="td_common_numeric_rules">Refractive Error</td>
                    <td class="common_td_rules">
                        <input class="form-control refractive_data" type="text" autocomplete="off"
                            id="refractive_errors_re" name="refractive_errors_re">
                    </td>
                    <td class="common_td_rules">
                        <input class="form-control refractive_data" type="text" autocomplete="off"
                            id="refractive_errors_le" name="refractive_errors_le">
                    </td>
                </tr>
                <tr>
                    <td class="td_common_numeric_rules">Keratometry</td>
                    <td class="common_td_rules">
                        <input class="form-control refractive_data" type="text" autocomplete="off" id="keratometry_re"
                            name="keratometry_re">
                    </td>
                    <td class="common_td_rules">
                        <input class="form-control refractive_data" type="text" autocomplete="off" id="keratometry_le"
                            name="keratometry_le">
                    </td>
                </tr>
                <tr>
                    <td class="td_common_numeric_rules">Pachymetry</td>
                    <td class="common_td_rules">
                        <input class="form-control refractive_data" type="text" autocomplete="off" id="pachymetry_re"
                            name="pachymetry_re">
                    </td>
                    <td class="common_td_rules">
                        <input class="form-control refractive_data" type="text" autocomplete="off" id="pachymetry_le"
                            name="pachymetry_le">
                    </td>
                </tr>
                <tr>
                    <td class="td_common_numeric_rules">Acid</td>
                    <td class="common_td_rules">
                        <input class="form-control refractive_data" type="text" autocomplete="off" id="acid_re"
                            name="acid_re">
                    </td>
                    <td class="common_td_rules">
                        <input class="form-control refractive_data" type="text" autocomplete="off" id="acid_le"
                            name="acid_le">
                    </td>
                </tr>
                <tr>
                    <td class="td_common_numeric_rules">WTW</td>
                    <td class="common_td_rules">
                        <input class="form-control refractive_data" type="text" autocomplete="off" id="wtw_re"
                            name="wtw_re">
                    </td>
                    <td class="common_td_rules">
                        <input class="form-control refractive_data" type="text" autocomplete="off" id="wtw_le"
                            name="wtw_le">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-3 padding_sm" style="margin-top: 10px">
        <div class="blue" style="font-weight: 600;"> PPK</div>
    </div>
    <div class="col-md-3 padding_sm" style="margin-top: 10px">
        <div class="blue" style="font-weight: 600;"> LASIK</div>
    </div>
    <div class="col-md-2 padding_sm" style="margin-top: 10px">
        <div class="blue" style="font-weight: 600;"> ICL</div>
    </div>
    <div class="col-md-2 padding_sm" style="margin-top: 10px">
        <div class="blue" style="font-weight: 600;"> CXL</div>
    </div>
    <div class="col-md-2 padding_sm" style="margin-top: 10px">
        <div class="blue" style="font-weight: 600;"> INTACS</div>
    </div>
    <div class="clearfix"></div>

    <div class="theadscroll always-visible" style="position: relative; height: 400px;">
        <div class="col-md-6 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Epithelial Removal</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="epithelial_removal"
                    name="epithelial_removal">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">RE</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="icl_re" name="icl_re">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Riboflavin</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="cxl_riboflavin"
                    name="cxl_riboflavin">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Incision</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="incision"
                    name="incision">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Optic Zone RE</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="optic_zone_re"
                    name="optic_zone_re">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">LE</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="icl_re" name="icl_re">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Power</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="cxl_power"
                    name="cxl_power">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Size 1</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="intacs_1_size"
                    name="intacs_1_size">
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="col-md-6 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Optic Zone LE</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="optic_zone_le"
                    name="optic_zone_le">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Vault RE</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="vault_re"
                    name="vault_re">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Duration</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="cxl_duration"
                    name="cxl_duration">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Depth 1</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="intacs_1_depth"
                    name="intacs_1_depth">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Ablation Depth RE</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="ablation_depth_re"
                    name="ablation_depth_re">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Vault LE</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="vault_le"
                    name="vault_le">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">

        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Size 2</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="intacs_2_size"
                    name="intacs_2_size">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Ablation Depth LE</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="ablation_depth_le"
                    name="ablation_depth_le">
            </div>
        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">

        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">

        </div>
        <div class="col-md-2 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:45px;">
                <label style="margin-top:-4px">Depth 2</label>
                <div class="clearfix"></div>
                <input class="form-control refractive_data" type="text" autocomplete="off" id="intacs_2_depth"
                    name="intacs_2_depth">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 padding_sm" style="margin-top: 10px">
            <div class="mate-input-box" style="height:65px">
                <label style="margin-top:-4px">MMC</label>
                <div class="col-md-6 padding_sm">
                    <div class="mate-input-box" style="height:45px;">
                        <div class="clearfix"></div>
                        <input class="form-control refractive_data" type="text" autocomplete="off" id="mmc1"
                            name="mmc1">
                    </div>
                </div>
                <div class="col-md-6 padding_sm">
                    <div class="mate-input-box" style="height:45px;">
                        <div class="clearfix"></div>
                        <input class="form-control refractive_data" type="text" autocomplete="off" id="mmc2"
                            name="mmc2">
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 padding_sm" style="margin-top: 10px">
            <table class="table no-margin table-striped table_sm table-col-bordered table-condensed"
                style="border: 1px solid #CCC;">
                <thead>
                    <tr class="table_header_bg">
                        <th width="20%">Type</th>
                        <th width="40%">Right Eye</th>
                        <th width="40%">Left Eye</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="td_common_numeric_rules">Flap</td>
                        <td class="common_td_rules">
                            <input class="form-control refractive_data" type="text" autocomplete="off"
                                id="flap_thickness_re" name="flap_thickness_re">
                        </td>
                        <td class="common_td_rules">
                            <input class="form-control refractive_data" type="text" autocomplete="off"
                                id="flap_thickness_le" name="flap_thickness_le">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_common_numeric_rules">RSB</td>
                        <td class="common_td_rules">
                            <input class="form-control refractive_data" type="text" autocomplete="off" id="rsb_re"
                                name="rsb_re">
                        </td>
                        <td class="common_td_rules">
                            <input class="form-control refractive_data" type="text" autocomplete="off" id="rsb_le"
                                name="rsb_le">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-md-4 padding_sm">
    <div class="box no-border no-margin">
        <div class="box-footer revenue_main_shadow" style="min-height: 640px;">
            <div class="col-md-8 blue padding_sm" style="font-weight: 600;">History</div>
            <div class="col-md-4 padding_sm" id="refractive_data_history" style="display: none">
                <button onclick="resetEmrData(9, 2);" style="padding: 0px 4px" class="btn btn-success" type="button">Add
                    New <i class="fa fa-plus"></i></button>
            </div>
            <div class="clearfix"></div>
            <div class="theadscroll always-visible" style="position: relative; height: 630px;"
                id="eyeEmrRefractiveSurgery">

            </div>
        </div>
    </div>
</div>
