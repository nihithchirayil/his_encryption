<div class="col-md-8 padding_sm" style="margin-top: 10px">
    <input class="form-control procedure_data" value="0" type="hidden" autocomplete="off" id="ophthalmologyEmrhead8" name="ophthalmologyEmrhead8">
    <div class="col-md-12 padding_sm">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">Procedure</label>
            <div class="clearfix"></div>
            <input class="form-control procedure_data" type="text" autocomplete="off" id="procedure_name"
                name="procedure_name">
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-6 padding_sm" style="margin-top: 10px">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">Eye</label>
            <div class="clearfix"></div>
            <select id="procedure_eye" class="form-control procedure_data">
                <option value="">Select</option>
                <option value="rs">Right Side</option>
                <option value="ls">Left Side</option>
                <option value="bs">Both Side</option>
            </select>
        </div>
    </div>
    <div class="col-md-6 padding_sm" style="margin-top: 10px">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">Date</label>
            <div class="clearfix"></div>
            <input class="form-control datepicker" type="text" autocomplete="off"
                id="procedure_date" name="procedure_date">
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 padding_sm" style="margin-top: 10px">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">Procedure Details</label>
            <div class="clearfix"></div>
            <input class="form-control procedure_data" type="text" autocomplete="off"
                id="procedure_details" name="procedure_details">
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 padding_sm" style="margin-top: 10px">
        <div class="mate-input-box" style="height:40px">
            <label style="margin-top:-4px">Comments</label>
            <div class="clearfix"></div>
            <input class="form-control procedure_data" type="text" autocomplete="off"
            id="procedure_comments" name="procedure_comments">
        </div>
    </div>
</div>
<div class="col-md-4 padding_sm">
    <div class="box no-border no-margin">
        <div class="box-footer revenue_main_shadow" style="min-height: 640px;">
            <div class="col-md-8 blue padding_sm" style="font-weight: 600;">History</div>
            <div class="col-md-4 padding_sm" id="procedure_data_history" style="display: none">
                <button onclick="resetEmrData(8, 2);" style="padding: 0px 4px" class="btn btn-success" type="button">Add
                    New <i class="fa fa-plus"></i></button>
            </div>
            <div class="clearfix"></div>
            <div class="theadscroll always-visible" style="position: relative; height: 630px;" id="eyeEmrProcedureDetails">

            </div>
        </div>
    </div>
</div>
