@php
$i=0;
$j=0;
@endphp
<div class="theadscroll" style="position: relative;height:500px">
    <table class="table no-margin table_sm no-border">
        <thead>
            <tr class="table_header_bg">
                <th>Refraction List</th>
            </tr>
        </thead>
        <tbody>
            @if (count($eye_refraction_details) != 0)
            @foreach ($eye_refraction_details as $each)
            @if($each->visit_id == $visit_id)
            @php $i++;@endphp
            @if($i==1)
            <tr style="background: #1e790c; color: #fff;">
                <th>Current Visit</th>
            </tr>
            @endif
            <tr class="eye_refraction_data" id="refraction_datarow{{ $each->id }}">
                <td style="cursor: pointer;" onclick="getRefractionData({{ $each->id }})">
                    {{ $each->refraction_datetime }}
                </td>
            </tr>
            @else
            @php $j++;@endphp
            @if($j==1)
            <tr style="background: #e5ad06; color: #fff;">
                <th>Previous Visit</th>
            </tr>
            @endif
            <tr class="eye_refraction_data" id="refraction_datarow{{ $each->id }}">
                <td style="cursor: pointer;" onclick="getRefractionData({{ $each->id }})">
                    {{ $each->refraction_datetime }}
                </td>
            </tr>
            @endif
            @endforeach
            @else
            <tr style="text-align: center">
                <th>No result found</th>
            </tr>
            @endif
        </tbody>
    </table>
</div>
