@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/canvasImages.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/slick.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/multi_tab_setup.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/common_controls.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/eye_emr.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">

@endsection
@section('content-area')

<!--  modal boxes  -->

@include('Emr::eye_emr.eye_modals_lite')
@include('Emr::view_patient.custom_modals_lite')

<div class="right_col" role="main">
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="doctor_id" value="{{ $doctor_id }}">
    <input type="hidden" id="default_note_form_id" value="{{ $default_note_form_id }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="emr_changes" value="{{ @$emr_changes ? $emr_changes : ''}}">
    <input type="hidden" id="patient_id" value="">
    <input type="hidden" id="booking_id" value="">
    <input type="hidden" id="visit_id" value="">
    <input type="hidden" id="visit_type" value="">
    <input type="hidden" id="encounter_id" value="">
    <input type="hidden" id="ca_head_id" value="">
    <input type="hidden" id="investigation_head_id" value="">
    <input type="hidden" id="prescription_head_id" value="">
    <input type="hidden" id="weight" value="2">
    <input type="hidden" id="temperature" value="10">
    <input type="hidden" id="height" value="4">
    <input type="hidden" id="vital_batch_no" value="">
    <input type="hidden" id="pacs_viewer_prefix" value="{{ $pacs_viewer_prefix }}">
    <input type="hidden" id="pacs_report_prefix" value="{{ $pacs_report_prefix }}">
    <input type="hidden" id="print_dialog_config" value="{{ $print_dialog_config }}">
    <input type="hidden" id="add_new_vital_batch" value="0">
    <input type="hidden" id="emr_lite_allergy_vital_config" value="{{ $emr_lite_allergy_vital_config }}">
    <input type="hidden" id="quantity_auto_calculation_config" value="{{ $quantity_auto_calculation_config }}">
    <input type="hidden" id="queue_api_url" value="{{ env('QUEUE_API_URL', '') }}">
    <input type="hidden" id="doctor_location" value="{{ $doctor_location }}">
    <input type="hidden" id="doctor_room" value="{{ $doctor_room }}">
    <input type="hidden" id="duration_unit" value="{{ $duration_unit }}">
    <input type="hidden" id="duration_unit_enable" value="{{ $duration_unit_enable }}">
    <input type="hidden" id="document_save_type" value="{{ $document_save_type }}">
    <input type="hidden" id="document_upload_path" value="{{ $document_upload_path }}">
    <input type="hidden" id="patient_refraction_id" value="0">
    <input type="hidden" id="glass_prescription_id" value="0">
    <input type="hidden" id="from_list" value="2">

    <a class="toggle-ip-op-list-btn toggleIpOpListBtn leftArrow" style="top: 147px; left: 64px;">
        <span class="toggle-ip-op-list-btn-icon">
            <i class="fa fa-arrow-left"></i>
        </span>

    </a>

    @include('Emr::eye_emr.eyeIpOpList')
    @include('Emr::eye_emr.viewOphthalmologyEmrVisitMaster')

</div>

@stop
@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/js/fabric.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/ophthalmology_emr.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/refraction.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/allergy_vitals_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/multi_tab_setup.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/investigation_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/prescription_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/emrnotes_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/accounts/default/javascript/jquery-ui.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/manage-document.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/eye_canvas_image.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/inv_result_entry.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/addnewinvestigationpopup_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/emr/js/ezoom.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
