<div class="col-md-12 padding_sm" style="margin-top: 5px;">
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label class="blue">Refraction Notes</label>
            <div class="clearfix"></div>
            <textarea class="form-control" style="height:50px !important;"
                id="eye_refraction_notes">{{ @$refraction_head['refraction_notes'] ? $refraction_head['refraction_notes'] : '' }}</textarea>
        </div>
    </div>
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label class="blue">Presenting Complaints</label>
            <div class="clearfix"></div>
            <textarea class="form-control" style="height:50px !important;"
                id="eye_presenting_complaints">{{ @$refraction_head['presenting_complaints'] ? $refraction_head['presenting_complaints'] : '' }}</textarea>
        </div>
    </div>
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label class="blue">Past History</label>
            <div class="clearfix"></div>
            <textarea class="form-control" style="height:50px !important;"
                id="eye_presenting_history">{{ @$refraction_head['past_history'] ? $refraction_head['past_history'] : '' }}</textarea>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-7 padding_sm table_box" style="margin-top: 5px;height:500px !important">
    <div class=" col-md-12 padding_sm" style="margin-top: 5px;text-align: center">
        <div class=" col-md-6 padding_sm table_box"
            style="background: #21a4f1; color: #FFF; font-weight: 600; font-size: 12px;">
            Right Eye (RE)
        </div>
        <div class=" col-md-6 padding_sm table_box"
            style="background: #21a4f1; color: #FFF; font-weight: 600; font-size: 12px;">
            Left Eye (LE)
        </div>
    </div>
    <div class=" col-md-12 padding_sm eye_refraction_header" style="margin-top: 5px;text-align: center">
        Snellen Chart
    </div>
    <div class="col-md-6 padding_sm">
        <div class="col-md-12 padding_sm" style="margin-top: 5px;">
            <div class="col-md-3 padding_sm">
                <label class="blue">DV</label>
                <div class="clearfix"></div>
                <input type="text" attr-code="SRDV" style="text-align: right"
                    value="{{ @$patient_refraction_data['snellen_right']['SRDV'] ? $patient_refraction_data['snellen_right']['SRDV'] : '' }}"
                    id="rightDVValue1" class="snellen_right form-control right">
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">NV</label>
                <div class="clearfix"></div>
                <input type="text" attr-code="SRNV" style="text-align: right"
                    value="{{ @$patient_refraction_data['snellen_right']['SRNV'] ? $patient_refraction_data['snellen_right']['SRNV'] : '' }}"
                    id="rightNVValue1" class="snellen_right form-control right">
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">DV PG</label>
                <div class="clearfix"></div>
                <input type="text" attr-code="SRDVPG" style="text-align: right"
                    value="{{ @$patient_refraction_data['snellen_right']['SRDVPG'] ? $patient_refraction_data['snellen_right']['SRDVPG'] : '' }}"
                    id="rightDVtPGValue1" class="snellen_right form-control right">
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">NV PG</label>
                <div class="clearfix"></div>
                <input type="text" attr-code="SRNVPG" style="text-align: right"
                    value="{{ @$patient_refraction_data['snellen_right']['SRNVPG'] ? $patient_refraction_data['snellen_right']['SRNVPG'] : '' }}"
                    id="rightNVtPGValue1" class="snellen_right form-control right">
            </div>
        </div>
    </div>
    <div class="col-md-6 padding_sm">
        <div class="col-md-12 padding_sm" style="margin-top: 5px;">
            <div class="col-md-3 padding_sm">
                <label class="blue">DV</label>
                <div class="clearfix"></div>
                <input type="text" attr-code="SLDV" style="text-align: right"
                    value="{{ @$patient_refraction_data['snellen_left']['SLDV'] ? $patient_refraction_data['snellen_left']['SLDV'] : '' }}"
                    id="rightDVValue1" class="snellen_left form-control right">
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">NV</label>
                <div class="clearfix"></div>
                <input type="text" attr-code="SLNV" style="text-align: right"
                    value="{{ @$patient_refraction_data['snellen_left']['SLNV'] ? $patient_refraction_data['snellen_left']['SLNV'] : '' }}"
                    id="rightNVValue1" class="snellen_left form-control right">
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">DV PG</label>
                <div class="clearfix"></div>
                <input type="text" attr-code="SLDVPG" style="text-align: right"
                    value="{{ @$patient_refraction_data['snellen_left']['SLDVPG'] ? $patient_refraction_data['snellen_left']['SLDVPG'] : '' }}"
                    id="rightDVtPGValue1" class="snellen_left form-control right">
            </div>
            <div class="col-md-3 padding_sm">
                <label class="blue">NV PG</label>
                <div class="clearfix"></div>
                <input type="text" attr-code="SLNVPG" style="text-align: right"
                    value="{{ @$patient_refraction_data['snellen_left']['SLNVPG'] ? $patient_refraction_data['snellen_left']['SLNVPG'] : '' }}"
                    id="rightNVtPGValue1" class="snellen_left form-control right">
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class=" col-md-6 padding_sm eye_refraction_header" style="margin-top: 5px;">
        Refraction Details
    </div>
    <div class="col-md-6 padding_sm" style="margin-top: 5px;">
        @php
        $dilation_status=@$dilation_status ? $dilation_status : 0;
        $checked_status='';
        $nvread_status='';
        @endphp
        @if(intval($dilation_status)==1)
        @php
        $checked_status='checked';
        $nvread_status='readonly';
        @endphp
        @endif

        <div class="checkbox checkbox-info inline no-margin pull-right">
            <input {{ $checked_status }} onclick="changeDilationData()" type="checkbox" id="refraction_dilation"
                name="refraction_dilation">
            <label class="blue" for="refraction_dilation">
                Dilation</label>
        </div>
    </div>
    <div class="col-md-6 padding_sm" style="margin-top: 5px">
        <table class="table no-margin table_sm no-border">
            <thead>
                <tr class="table_header_bg">
                    <th>Sphere</th>
                    <th>Cylinder</th>
                    <th>AXIS°</th>
                    <th title="Corrected Vision">Corr.Vn</th>
                    <th><i class="fa fa-list"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <input type="text" attr-code="ArSPH" class="ref_right form-control "
                            value="{{ @$patient_refraction_data['right']['ArSPH'] ? $patient_refraction_data['right']['ArSPH'] : '' }}"
                            id="rightArSPH">
                    </td>
                    <td>
                        <input type="text" attr-code="ArCyl" class="ref_right form-control "
                            value="{{ @$patient_refraction_data['right']['ArCyl'] ? $patient_refraction_data['right']['ArCyl'] : '' }}"
                            id="rightArCyl">
                    </td>
                    <td>
                        <input type="text" attr-code="ArAxix" class="ref_right form-control "
                            value="{{ @$patient_refraction_data['right']['ArAxix'] ? $patient_refraction_data['right']['ArAxix'] : '' }}"
                            id="rightArAxix">
                    </td>
                    <td>
                        -
                    </td>
                    <th>
                        AR
                    </th>
                </tr>
                <tr>
                    <td>
                        <input type="text" attr-code="DvSPH" class="ref_right form-control "
                            value="{{ @$patient_refraction_data['right']['DvSPH'] ? $patient_refraction_data['right']['DvSPH'] : '' }}"
                            id="rightDvSPH">
                    </td>
                    <td>
                        <input type="text" attr-code="DvCyl" class="ref_right form-control "
                            value="{{ @$patient_refraction_data['right']['DvCyl'] ? $patient_refraction_data['right']['DvCyl'] : '' }}"
                            id="rightDvCyl">
                    </td>
                    <td>
                        <input type="text" attr-code="DvAxix" class="ref_right form-control "
                            value="{{ @$patient_refraction_data['right']['DvAxix'] ? $patient_refraction_data['right']['DvAxix'] : '' }}"
                            id="rightDvAxix">
                    </td>
                    <td>
                        <input type="text" attr-code="DvCorrVn" class="ref_right form-control "
                            value="{{ @$patient_refraction_data['right']['DvCorrVn'] ? $patient_refraction_data['right']['DvCorrVn'] : '' }}"
                            id="rightDvCorrVn">
                    </td>
                    <th>
                        DV
                    </th>
                </tr>
                <tr>
                    <td>
                        <input {{ $nvread_status }} type="text" attr-code="NvSPH"
                            class="ref_right near_vision form-control "
                            value="{{ @$patient_refraction_data['right']['NvSPH'] ? $patient_refraction_data['right']['NvSPH'] : '' }}"
                            id="rightNvSPH">
                    </td>
                    <td>
                        <input {{ $nvread_status }} type="text" attr-code="NvCyl"
                            class="ref_right near_vision form-control "
                            value="{{ @$patient_refraction_data['right']['NvCyl'] ? $patient_refraction_data['right']['NvCyl'] : '' }}"
                            id="rightNvCyl">
                    </td>
                    <td>
                        <input {{ $nvread_status }} type="text" attr-code="NvAxix"
                            class="ref_right near_vision form-control "
                            value="{{ @$patient_refraction_data['right']['NvAxix'] ? $patient_refraction_data['right']['NvAxix'] : '' }}"
                            id="rightNvCyl">
                    </td>
                    <td>
                        <input {{ $nvread_status }} type="text" attr-code="NvCorrVn"
                            class="ref_right near_vision form-control "
                            value="{{ @$patient_refraction_data['right']['NvCorrVn'] ? $patient_refraction_data['right']['NvCorrVn'] : '' }}"
                            id="rightNvCorrVn">
                    </td>
                    <th>
                        NV
                    </th>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-6 padding_sm" style="margin-top: 5px">
        <table class="table no-margin table_sm no-border">
            <thead>
                <tr class="table_header_bg">
                    <th>Sphere</th>
                    <th>Cylinder</th>
                    <th>AXIS°</th>
                    <th title="Corrected Vision">Corr.Vn</th>
                    <th><i class="fa fa-list"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <input type="text" attr-code="ArSPH" class="ref_left form-control "
                            value="{{ @$patient_refraction_data['left']['ArSPH'] ? $patient_refraction_data['left']['ArSPH'] : '' }}"
                            id="leftArSPH">
                    </td>
                    <td>
                        <input type="text" attr-code="ArCyl" class="ref_left form-control "
                            value="{{ @$patient_refraction_data['left']['ArCyl'] ? $patient_refraction_data['left']['ArCyl'] : '' }}"
                            id="leftArCyl">
                    </td>
                    <td>
                        <input type="text" attr-code="ArAxix" class="ref_left form-control "
                            value="{{ @$patient_refraction_data['left']['ArAxix'] ? $patient_refraction_data['left']['ArAxix'] : '' }}"
                            id="leftArAxix">
                    </td>
                    <td>
                        -
                    </td>
                    <th>
                        AR
                    </th>
                </tr>
                <tr>

                    <td>
                        <input type="text" attr-code="DvSPH" class="ref_left form-control "
                            value="{{ @$patient_refraction_data['left']['DvSPH'] ? $patient_refraction_data['left']['DvSPH'] : '' }}"
                            id="leftDvSPH">
                    </td>
                    <td>
                        <input type="text" attr-code="DvCyl" class="ref_left form-control "
                            value="{{ @$patient_refraction_data['left']['DvCyl'] ? $patient_refraction_data['left']['DvCyl'] : '' }}"
                            id="leftDvCyl">
                    </td>
                    <td>
                        <input type="text" attr-code="DvAxix" class="ref_left form-control "
                            value="{{ @$patient_refraction_data['left']['DvAxix'] ? $patient_refraction_data['left']['DvAxix'] : '' }}"
                            id="leftDvAxix">
                    </td>
                    <td>
                        <input type="text" attr-code="DvCorrVn" class="ref_left form-control "
                            value="{{ @$patient_refraction_data['left']['DvCorrVn'] ? $patient_refraction_data['left']['DvCorrVn'] : '' }}"
                            id="leftDvCorrVn">
                    </td>
                    <th>
                        DV
                    </th>
                </tr>
                <tr>
                    <td>
                        <input {{ $nvread_status }} type="text" attr-code="NvSPH"
                            class="ref_left form-control near_vision "
                            value="{{ @$patient_refraction_data['left']['NvSPH'] ? $patient_refraction_data['left']['NvSPH'] : '' }}"
                            id="leftNvSPH">
                    </td>
                    <td>
                        <input {{ $nvread_status }} type="text" attr-code="NvCyl"
                            class="ref_left form-control near_vision "
                            value="{{ @$patient_refraction_data['left']['NvCyl'] ? $patient_refraction_data['left']['NvCyl'] : '' }}"
                            id="leftNvCyl">
                    </td>
                    <td>
                        <input {{ $nvread_status }} type="text" attr-code="NvAxix"
                            class="ref_left form-control near_vision "
                            value="{{ @$patient_refraction_data['left']['NvAxix'] ? $patient_refraction_data['left']['NvAxix'] : '' }}"
                            id="leftNvCyl">
                    </td>
                    <td>
                        <input {{ $nvread_status }} type="text" attr-code="NvCorrVn"
                            class="ref_left form-control near_vision "
                            value="{{ @$patient_refraction_data['left']['NvCorrVn'] ? $patient_refraction_data['left']['NvCorrVn'] : '' }}"
                            id="leftNvCorrVn">
                    </td>
                    <th>
                        NV
                    </th>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>


    <div class="clearfix"></div>

    <div class="col-md-12 padding_sm" style="margin-top: 5px;">
        <div class="col-md-6 padding_sm" style="margin-top: 5px;">
            <table class="table no-margin table_sm no-border">
                <tbody>
                    <tr>
                        <th>
                            Colour Vision
                        </th>
                        <td>
                            <input type="text" attr-code="colour_vision" class="sub_rightrefraction form-control"
                                value="{{ @$patient_refraction_data['subrefraction_right']['colour_vision'] ? $patient_refraction_data['subrefraction_right']['colour_vision'] : '' }}"
                                id="rightcolour_vision11">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Confrontation
                        </th>
                        <td>
                            <input type="text" attr-code="ponconfrontation" class="sub_rightrefraction form-control"
                                value="{{ @$patient_refraction_data['subrefraction_right']['ponconfrontation'] ? $patient_refraction_data['subrefraction_right']['ponconfrontation'] : '' }}"
                                id="rightponconfrontation12">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            PinHole
                        </th>
                        <td>
                            <input type="text" attr-code="pinHole" class="sub_rightrefraction form-control"
                                value="{{ @$patient_refraction_data['subrefraction_right']['pinHole'] ? $patient_refraction_data['subrefraction_right']['pinHole'] : '' }}"
                                id="rightpinHole13">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6 padding_sm" style="margin-top: 5px;">
            <table class="table no-margin table_sm no-border">
                <tbody>
                    <tr>
                        <td>
                            <input type="text" attr-code="colour_vision" class="sub_leftrefraction form-control"
                                value="{{ @$patient_refraction_data['subrefraction_left']['colour_vision'] ? $patient_refraction_data['subrefraction_left']['colour_vision'] : '' }}"
                                id="leftcolour_vision21">
                        </td>
                        <th>
                            Colour Vision
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" attr-code="ponconfrontation" class="sub_leftrefraction form-control"
                                value="{{ @$patient_refraction_data['subrefraction_left']['ponconfrontation'] ? $patient_refraction_data['subrefraction_left']['ponconfrontation'] : '' }}"
                                id="leftponconfrontation22">
                        </td>
                        <th>
                            Confrontation
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" attr-code="pinHole" class="sub_leftrefraction form-control"
                                value="{{ @$patient_refraction_data['subrefraction_left']['pinHole'] ? $patient_refraction_data['subrefraction_left']['pinHole'] : '' }}"
                                id="leftpinHole23">
                        </td>
                        <th>
                            PinHole
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-12 padding_sm">
        <div class=" col-md-12 padding_sm eye_refraction_header" style="margin-top: 5px;text-align: center">
            Keratometry
        </div>
        <div class="col-md-6 padding_sm" style="margin-top: 5px;">
            <table class="table no-margin table_sm no-border">
                <tbody>
                    <tr>
                        <th>
                            K1
                        </th>
                        <td>
                            <input type="text" attr-code="Kerameter11" class="ketometer_right form-control"
                                value="{{ @$patient_refraction_data['ketometer_right']['Kerameter11'] ? $patient_refraction_data['ketometer_right']['Kerameter11'] : '' }}"
                                id="rightKerameter11">
                        </td>
                        <td>
                            <input type="text" attr-code="Kerameter12" class="ketometer_right form-control"
                                value="{{ @$patient_refraction_data['ketometer_right']['Kerameter12'] ? $patient_refraction_data['ketometer_right']['Kerameter12'] : '' }}"
                                id="rightKerameter12">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            K2
                        </th>
                        <td>
                            <input type="text" attr-code="Kerameter11" class="ketometer_right form-control"
                                value="{{ @$patient_refraction_data['ketometer_right']['Kerameter11'] ? $patient_refraction_data['ketometer_right']['Kerameter11'] : '' }}"
                                id="rightKerameter21">
                        </td>
                        <td>
                            <input type="text" attr-code="Kerameter22" class="ketometer_right form-control"
                                value="{{ @$patient_refraction_data['ketometer_right']['Kerameter22'] ? $patient_refraction_data['ketometer_right']['Kerameter22'] : '' }}"
                                id="rightKerameter22">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6 padding_sm" style="margin-top: 5px;">
            <table class="table no-margin table_sm no-border">
                <tbody>
                    <tr>
                        <td>
                            <input type="text" attr-code="Kerameter11" class="ketometer_left form-control"
                                value="{{ @$patient_refraction_data['ketometer_left']['Kerameter11'] ? $patient_refraction_data['ketometer_left']['Kerameter11'] : '' }}"
                                id="leftKerameter11">
                        </td>
                        <td>
                            <input type="text" attr-code="Kerameter12" class="ketometer_left form-control"
                                value="{{ @$patient_refraction_data['ketometer_left']['Kerameter12'] ? $patient_refraction_data['ketometer_left']['Kerameter12'] : '' }}"
                                id="leftKerameter12">
                        </td>
                        <th>
                            K1
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" attr-code="Kerameter11" class="ketometer_left form-control"
                                value="{{ @$patient_refraction_data['ketometer_left']['Kerameter11'] ? $patient_refraction_data['ketometer_left']['Kerameter11'] : '' }}"
                                id="leftKerameter21">
                        </td>
                        <td>
                            <input type="text" attr-code="Kerameter22" class="ketometer_left form-control"
                                value="{{ @$patient_refraction_data['ketometer_left']['Kerameter22'] ? $patient_refraction_data['ketometer_left']['Kerameter22'] : '' }}"
                                id="leftKerameter22">
                        </td>
                        <th>
                            K2
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12 padding_sm">
        <div class=" col-md-12 padding_sm eye_refraction_header" style="margin-top: 5px;text-align: center">
            Prism
        </div>
        <div class="col-md-6 padding_sm" style="margin-top: 5px;">
            <table class="table no-margin table_sm no-border">
                <tbody>
                    <tr>
                        <th>
                            IOPST
                        </th>
                        <td>
                            <input type="text" attr-code="iopst" class="prism_right form-control"
                                value="{{ @$patient_refraction_data['prism_right']['iopst'] ? $patient_refraction_data['prism_right']['iopst'] : '' }}"
                                id="rightiopst">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            IOPAT
                        </th>
                        <td>
                            <input type="text" attr-code="iopat" class="prism_right form-control"
                                value="{{ @$patient_refraction_data['prism_right']['iopat'] ? $patient_refraction_data['prism_right']['iopat'] : '' }}"
                                id="rightiopat">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6 padding_sm" style="margin-top: 5px;">
            <table class="table no-margin table_sm no-border">
                <tbody>
                    <tr>
                        <td>
                            <input type="text" attr-code="iopst" class="prism_left form-control"
                                value="{{ @$patient_refraction_data['prism_left']['iopst'] ? $patient_refraction_data['prism_left']['iopst'] : '' }}"
                                id="leftiopst">
                        </td>
                        <th>
                            IOPST
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" attr-code="iopat" class="prism_left form-control"
                                value="{{ @$patient_refraction_data['prism_left']['iopat'] ? $patient_refraction_data['prism_left']['iopat'] : '' }}"
                                id="leftiopat">
                        </td>
                        <th>
                            IOPAT
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-md-5 padding_sm table_box" style="margin-top: 5px;height:500px !important">
    <div class="col-md-8 padding_sm eye_refraction_header" style="margin-top: 5px;text-align: center">PG Power
    </div>
    <div class="col-md-4 padding_sm" style="margin-top: 5px;"><button type="button" id="addNewPgBtn"
            onclick="addNewPgPower('')" style="padding:1px 2px;" class="btn btn-primary btn-block">Add New PG
            <i id="addNewPgSpin" class="fa fa-plus-circle"></i></button></div>
    <div class="col-md-12 padding_sm theadscroll" id="addNewPgBtnDiv" style="position: relative;height:320px">
    </div>
</div>
