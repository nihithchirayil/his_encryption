<ul class="nav nav-tabs" id="refraction_dilationnav" role="tablist">
    <li class="nav-item">
        <a class="nav-link" id="ref1_right_list_tab" data-toggle="tab" href="#ref1_right_tab" role="tab"
            aria-controls="ref1_right" aria-selected="false">Ref 1</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="ref2_right_list_tab" data-toggle="tab" href="#ref2_right_tab" role="tab"
            aria-controls="ref2_right" aria-selected="false">Ref 2</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="iol-tab" data-toggle="tab" href="#iol" role="tab" aria-controls="iol"
            aria-selected="false">IOL</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="clava-tab" data-toggle="tab" href="#clava" role="tab" aria-controls="clava"
            aria-selected="false">Clava</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="squint-tab" data-toggle="tab" href="#squint" role="tab" aria-controls="squint"
            aria-selected="false">Squint</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="lasik-tab" data-toggle="tab" href="#lasik" role="tab" aria-controls="lasik"
            aria-selected="false">LASIK</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history"
            aria-selected="false">History</a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane" id="ref1_right_tab" role="tabpanel">
        <div class="row padding_sm" style="margin-top: 5px">
            <div class="box-footer"
                style="padding: 5px; box-shadow: 0px 1px #FFF;border-radius: 5px; border: 1px solid #0aebc566;min-height: 250px;">
                <div class="col-md-5 padding_sm" style="margin-top: 5px">
                    <div class="col-md-5 padding_sm">
                        <label>Snellen Chart</label>
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcDVValue1" id="ref1_rightUcDVValue1"
                                class="form-control ref1_right  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-5 padding_sm">
                        <label>Snellen Chart</label>
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcDVValue2" id="ref1_rightUcDVValue2"
                                class="form-control ref1_right  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <label class="blue"><strong>Uc.DV</strong></label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-5 padding_sm">
                        <label>Snellen Chart</label>
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcDnValue1" id="ref1_rightUcDnValue1"
                                class="form-control ref1_right  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-5 padding_sm">
                        <label>Snellen Chart</label>
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcNVValue2" id="ref1_rightUcNVValue2"
                                class="form-control ref1_right  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <label class="blue"><strong>Uc.NV</strong></label>
                    </div>
                    <div class="clearfix"></div>
                    <table class="table no-margin table_sm no-border">
                        <thead>
                            <tr class="table_header_bg">
                                <th style="text-align: center" colspan="6">Right Eye (RE)</th>
                            </tr>
                            <tr class="table_header_bg">
                                <th>Sphere</th>
                                <th>Cylinder</th>
                                <th>AXIS</th>
                                <th>Prism</th>
                                <th><i class="fa fa-list"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input type="text" attr-code="ArSPH"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightArSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArCyl"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightArCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArAxix"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightArAxix">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArPrism"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightArPrism">
                                </td>
                                <td>
                                    AR
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" attr-code="DvSPH"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightDvSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvCyl"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightDvCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvAxix"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightDvAxix">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvPrism"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightDvPrism">
                                </td>
                                <td>
                                    DV
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" attr-code="NvSPH"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightNvSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvCyl"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightNvCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvAxix"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightNvCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvPrism"
                                        class="form-control ref1_right  td_common_numeric_rules" value="0"
                                        id="ref1_rightNvPrism">
                                </td>
                                <td>
                                    NV
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-2 padding_sm" style="margin-top: 5px">

                    <div class="col-md-12 padding_sm">
                        <button type="button" style="padding: 13px 0px;font-size:18px;"
                            class="btn btn-primary btn-block">Old Glass <i class="fa fa-user-secret"></i></button>
                    </div>

                    <div class="col-md-12 padding_sm" style="margin-top: 2px">
                        <label>Taken Date Time</label>
                        <div class="mate-input-box">
                            <input type="text" id="refractiondatetime_ref1" value="{{ date('M-d-Y h:i a') }}"
                                class="form-control td_common_numeric_rules refractiondatetime"
                                onfocusout="getLatestRefractionData('ref1')">
                        </div>
                    </div>

                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label>Vertex Distance</label>
                        <div class="mate-input-box">
                            <input type="text" id="vertex_distance_ref1" class="form-control td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label title="Interpupillary Distance">IPD</label>
                        <div class="mate-input-box">
                            <input type="text" id="ipd_ref1" class="form-control td_common_numeric_rules">
                        </div>
                    </div>
                </div>
                <div class="col-md-5 padding_sm" style="margin-top: 5px">
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <label class="blue"><strong>Uc.DV</strong></label>
                    </div>
                    <div class="col-md-5 padding_sm">
                        <label>Snellen Chart</label>
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcDVValue1" id="ref1_leftUcDVValue1"
                                class="form-control ref1_left  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-5 padding_sm">
                        <label>Snellen Chart</label>
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcDVValue2" id="ref1_leftUcDVValue2"
                                class="form-control ref1_left  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <label class="blue"><strong>Uc.NV</strong></label>
                    </div>
                    <div class="col-md-5 padding_sm">
                        <label>Snellen Chart</label>
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcNVValue1" attr-side="left" id="ref1_leftUcNVValue1"
                                class="form-control ref1_left  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-5 padding_sm">
                        <label>Snellen Chart</label>
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcNVValue2" attr-side="left" id="ref1_leftUcNVValue2"
                                class="form-control ref1_left  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <table class="table no-margin table_sm no-border">
                        <thead>
                            <tr class="table_header_bg">
                                <th style="text-align: center" colspan="6">Left Eye (LE)</th>
                            </tr>
                            <tr class="table_header_bg">
                                <th><i class="fa fa-list"></i></th>
                                <th>Sphere</th>
                                <th>Cylinder</th>
                                <th>AXIS</th>
                                <th>Prism</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    AR
                                </td>
                                <td>
                                    <input type="text" attr-code="ArSPH" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftArSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArCyl" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftArCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArAxix" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftArAxix">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArPrism" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftArPrism">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    DV
                                </td>
                                <td>
                                    <input type="text" attr-code="DvSPH" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftDvSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvCyl" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftDvCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvAxix" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftDvAxix">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvPrism" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftDvPrism">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    NV
                                </td>
                                <td>
                                    <input type="text" attr-code="NvSPH" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftNvSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvCyl" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftNvCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvAxix" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftNvAxix">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvPrism" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value="0"
                                        id="ref1_leftNvPrism">
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="clearfix"></div>
            <div class="box-footer"
                style="padding: 5px;margin-top: 5px; box-shadow: 0px 1px #FFF;border-radius: 5px; border: 1px solid #0aebc566;min-height: 65px;">
                <div class="col-md-5 padding_sm" style="margin-top: 5px">

                    <table class="table no-margin table_sm no-border">
                        <tbody>
                            <tr>
                                <td>
                                    <input type="text" attr-code="k1value1"
                                        class="form-control ref1_right td_common_numeric_rules" value=""
                                        id="ref1_rightk1value1">
                                </td>
                                <td>
                                    <input type="text" attr-code="k1value2"
                                        class="form-control ref1_right  td_common_numeric_rules" value=""
                                        id="ref1_rightk1value2">
                                </td>
                                <td>
                                    K1
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" attr-code="k2value1"
                                        class="form-control ref1_right  td_common_numeric_rules" value=""
                                        id="ref1_rightk2value1">
                                </td>
                                <td>
                                    <input type="text" attr-code="k2value2"
                                        class="form-control ref1_right  td_common_numeric_rules" value=""
                                        id="ref1_rightk2value2">
                                </td>
                                <td>
                                    K2
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-2 padding_sm" style="margin-top: 5px">
                    <div class="col-md-12 padding_sm text-center" style="margin-top: 11px">
                        <label style="font-size: 18px;" class="blue">Keratometer</label>
                    </div>
                </div>
                <div class="col-md-5 padding_sm" style="margin-top: 5px">
                    <table class="table no-margin table_sm no-border">
                        <tbody>
                            <tr>
                                <td>
                                    <input type="text" attr-code="k1value1" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value=""
                                        id="ref1_leftk1value1">
                                </td>
                                <td>
                                    <input type="text" attr-code="k1value2" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value=""
                                        id="ref1_leftk1value2">
                                </td>
                                <td>
                                    K1
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" attr-code="k2value1" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value=""
                                        id="ref1_leftk2value1">
                                </td>
                                <td>
                                    <input type="text" attr-code="k2value2" attr-side="left"
                                        class="form-control ref1_left  td_common_numeric_rules" value=""
                                        id="ref1_leftk2value2">
                                </td>
                                <td>
                                    K2
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="clearfix"></div>
            <div class="box-footer"
                style="padding: 5px;margin-top: 5px; box-shadow: 0px 1px #FFF;border-radius: 5px; border: 1px solid #0aebc566;min-height: 92px;">
                <div class="col-md-5 padding_sm" style="margin-top: 5px">
                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label>IOP-ST</label>
                        <div class="mate-input-box">
                            <input type="text" attr-code="iop_st"
                                class="form-control ref1_right  td_common_numeric_rules" id="ref1_rightiop_st">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label>IOP-AT</label>
                        <div class="mate-input-box">
                            <input type="text" attr-code="iop_at"
                                class="form-control ref1_right  td_common_numeric_rules" id="ref1_rightiop_at">
                        </div>
                    </div>
                </div>
                <div class="col-md-2 padding_sm" style="margin-top: -5px">
                    <div class="col-md-12 padding_sm text-center">
                        <label style="font-size: 18px;margin-top: 12px" class="blue">Intraocular Pressure</label>
                    </div>
                </div>
                <div class="col-md-5 padding_sm" style="margin-top: 5px">
                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label>IOP-ST</label>
                        <div class="mate-input-box">
                            <input type="text" attr-code="iop_st" attr-side="left"
                                class="form-control ref1_left  td_common_numeric_rules" id="ref1_leftiop_st">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label>IOP-AT</label>
                        <div class="mate-input-box">
                            <input type="text" attr-code="iop_at" attr-side="left"
                                class="form-control ref1_left  td_common_numeric_rules" id="ref1_leftiop_at">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="ref2_right_tab" role="tabpanel">
        <div class="row padding_sm" style="margin-top: 5px">
            <div class="box-footer"
                style="padding: 5px; box-shadow: 0px 1px #FFF;border-radius: 5px; border: 1px solid #0aebc566;min-height: 250px;">
                <div class="col-md-5 padding_sm" style="margin-top: 5px">
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <select class="form-control ref2_right " attr-code="UcDVChart">
                                <option value="">Snellen Chart</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcDVValue1" id="ref2_rightUcDVValue1"
                                class="form-control ref2_right  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcDVValue2" id="ref2_rightUcDVValue2"
                                class="form-control ref2_right  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <label class="blue"><strong>Uc.DV</strong></label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <select class="form-control ref2_right " attr-code="UcNVChart" id="ref2_rightsnellen_chart">
                                <option value="">Snellen Chart</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcDnValue1" id="ref2_rightUcDnValue1"
                                class="form-control ref2_right  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcNVValue2" id="ref2_rightUcNVValue2"
                                class="form-control ref2_right  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <label class="blue"><strong>Uc.NV</strong></label>
                    </div>
                    <div class="clearfix"></div>
                    <table class="table no-margin table_sm no-border">
                        <thead>
                            <tr class="table_header_bg">
                                <th style="text-align: center" colspan="6">Right Eye (RE)</th>
                            </tr>
                            <tr class="table_header_bg">
                                <th>Sphere</th>
                                <th>Cylinder</th>
                                <th>AXIS</th>
                                <th>Prism</th>
                                <th><i class="fa fa-list"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input type="text" attr-code="ArSPH"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightArSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArCyl"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightArCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArAxix"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightArAxix">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArPrism"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightArPrism">
                                </td>
                                <td>
                                    AR
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" attr-code="DvSPH"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightDvSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvCyl"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightDvCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvAxix"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightDvAxix">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvPrism"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightDvPrism">
                                </td>
                                <td>
                                    DV
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" attr-code="NvSPH"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightNvSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvCyl"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightNvCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvAxix"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightNvAxix">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvPrism"
                                        class="form-control ref2_right  td_common_numeric_rules" value="0"
                                        id="ref2_rightNvPrism">
                                </td>
                                <td>
                                    NV
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-2 padding_sm" style="margin-top: 5px">

                    <div class="col-md-12 padding_sm">
                        <button type="button" style="padding: 13px 0px;font-size:18px;"
                            class="btn btn-primary btn-block">Old Glass <i class="fa fa-user-secret"></i></button>
                    </div>

                    <div class="col-md-12 padding_sm" style="margin-top: 2px">
                        <label>Taken Date Time</label>
                        <div class="mate-input-box">
                            <input type="text" id="refractiondatetime_ref2" value="{{ date('M-d-Y h:i a') }}"
                                class="form-control td_common_numeric_rules refractiondatetime"
                                onfocusout="getLatestRefractionData('ref2')">
                        </div>
                    </div>

                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label>Vertex Distance</label>
                        <div class="mate-input-box">
                            <input type="text" id="vertex_distance_ref2" class="form-control td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label title="Interpupillary Distance">IPD</label>
                        <div class="mate-input-box">
                            <input type="text" id="ipd_ref2" class="form-control td_common_numeric_rules">
                        </div>
                    </div>
                </div>
                <div class="col-md-5 padding_sm" style="margin-top: 5px">
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <label class="blue"><strong>Uc.DV</strong></label>
                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <select class="form-control ref2_left " attr-code="UcDVChart">
                                <option value="">Snellen Chart</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcDVValue1" id="ref2_rightUcDVValue1"
                                class="form-control ref2_left  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcDVValue2" id="ref2_rightUcDVValue2"
                                class="form-control ref2_left  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-2 padding_sm" style="margin-top: 5px">
                        <label class="blue"><strong>Uc.NV</strong></label>
                    </div>
                    <div class="col-md-4 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <select class="form-control ref2_left" attr-code="UcNVChart">
                                <option value="">Snellen Chart</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcNVValue1" attr-side="left" id="ref2_leftUcNVValue1"
                                class="form-control ref2_left  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="mate-input-box">
                            <div class="clearfix"></div>
                            <input type="text" attr-code="UcNVValue2" attr-side="left" id="ref2_leftUcNVValue2"
                                class="form-control ref2_left  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <table class="table no-margin table_sm no-border">
                        <thead>
                            <tr class="table_header_bg">
                                <th style="text-align: center" colspan="6">Right Eye (RE)</th>
                            </tr>
                            <tr class="table_header_bg">
                                <th><i class="fa fa-list"></i></th>
                                <th>Sphere</th>
                                <th>Cylinder</th>
                                <th>AXIS</th>
                                <th>Prism</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    AR
                                </td>
                                <td>
                                    <input type="text" attr-code="ArSPH" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftArSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArCyl" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftArCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArAxix" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftArAxix">
                                </td>
                                <td>
                                    <input type="text" attr-code="ArPrism" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftArPrism">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    DV
                                </td>
                                <td>
                                    <input type="text" attr-code="DvSPH" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftDvSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvCyl" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftDvCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvAxix" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftDvAxix">
                                </td>
                                <td>
                                    <input type="text" attr-code="DvPrism" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftDvPrism">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    NV
                                </td>
                                <td>
                                    <input type="text" attr-code="NvSPH" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftNvSPH">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvCyl" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftNvCyl">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvAxix" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftNvAxix">
                                </td>
                                <td>
                                    <input type="text" attr-code="NvPrism" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_leftNvPrism">
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="clearfix"></div>
            <div class="box-footer"
                style="padding: 5px;margin-top: 5px; box-shadow: 0px 1px #FFF;border-radius: 5px; border: 1px solid #0aebc566;min-height: 65px;">
                <div class="col-md-5 padding_sm" style="margin-top: 5px">

                    <table class="table no-margin table_sm no-border">
                        <tbody>
                            <tr>
                                <td>
                                    <input type="text" attr-code="k1value1"
                                        class="form-control ref2_right  td_common_numeric_rules" value=""
                                        id="ref2_rightk1value1">
                                </td>
                                <td>
                                    <input type="text" attr-code="k1value2"
                                        class="form-control ref2_right  td_common_numeric_rules" value=""
                                        id="ref2_rightk1value2">
                                </td>
                                <td>
                                    K1
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" attr-code="k2value1"
                                        class="form-control ref2_right  td_common_numeric_rules" value=""
                                        id="ref2_rightk2value1">
                                </td>
                                <td>
                                    <input type="text" attr-code="k2value2"
                                        class="form-control ref2_right  td_common_numeric_rules" value=""
                                        id="ref2_rightk2value2">
                                </td>
                                <td>
                                    K2
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-2 padding_sm" style="margin-top: 5px">
                    <div class="col-md-12 padding_sm text-center" style="margin-top: 11px">
                        <label style="font-size: 18px;" class="blue">Keratometer</label>
                    </div>
                </div>
                <div class="col-md-5 padding_sm" style="margin-top: 5px">
                    <table class="table no-margin table_sm no-border">
                        <tbody>
                            <tr>
                                <td>
                                    <input type="text" attr-code="k1value1" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_rightk1value1">
                                </td>
                                <td>
                                    <input type="text" attr-code="k1value2" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_rightk1value1">
                                </td>
                                <td>
                                    K1
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" attr-code="k2value1" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_rightk2value1">
                                </td>
                                <td>
                                    <input type="text" attr-code="k2value2" attr-side="left"
                                        class="form-control ref2_left  td_common_numeric_rules" value=""
                                        id="ref2_rightk2value2">
                                </td>
                                <td>
                                    K2
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="clearfix"></div>
            <div class="box-footer"
                style="padding: 5px;margin-top: 5px; box-shadow: 0px 1px #FFF;border-radius: 5px; border: 1px solid #0aebc566;min-height: 92px;">
                <div class="col-md-5 padding_sm" style="margin-top: 5px">
                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label>IOP-ST</label>
                        <div class="mate-input-box">
                            <input type="text" attr-code="iop_st" id="ref2_rightiop_st"
                                class="form-control ref2_right  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label>IOP-AT</label>
                        <div class="mate-input-box">
                            <input type="text" attr-code="iop_at" id="ref2_rightiop_at"
                                class="form-control ref2_right  td_common_numeric_rules">
                        </div>
                    </div>
                </div>
                <div class="col-md-2 padding_sm" style="margin-top: -5px">
                    <div class="col-md-12 padding_sm text-center">
                        <label style="font-size: 18px;margin-top: 12px" class="blue">Intraocular Pressure</label>
                    </div>
                </div>
                <div class="col-md-5 padding_sm" style="margin-top: 5px">
                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label>IOP-ST</label>
                        <div class="mate-input-box">
                            <input type="text" attr-code="iop_st" attr-side="left" id="ref2_leftiop_st"
                                class="form-control ref2_left  td_common_numeric_rules">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm" style="margin-top: 2px">
                        <label>IOP-AT</label>
                        <div class="mate-input-box">
                            <input type="text" attr-code="iop_at" attr-side="left" id="ref2_leftiop_at"
                                class="form-control ref2_left  td_common_numeric_rules">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="iol" role="tabpanel"></div>
    <div class="tab-pane" id="clava" role="tabpanel"></div>
    <div class="tab-pane" id="squint" role="tabpanel"></div>
    <div class="tab-pane" id="lasik" role="tabpanel"></div>
    <div class="tab-pane" id="history" role="tabpanel"></div>
</div>
