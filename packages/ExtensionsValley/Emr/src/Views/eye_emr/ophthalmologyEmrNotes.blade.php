<div class="col-md-8 padding_sm">
    <div class="col-md-12 padding_sm table_box notes_entering_area" style="height:550px !important">
        <input class="form-control" value="0" type="hidden" autocomplete="off" id="ophthalmologyEmrhead1"
            name="ophthalmologyEmrhead1">
        <form id="ca-data-form">
            <div class="col-md-12 no-padding notes_chief_complaint_container theadscroll"
                style="height : 530px !important;margin-top:17px;">
                @if (isset($dynamic_notes_fields) && count($dynamic_notes_fields) > 0)
                @foreach ($dynamic_notes_fields as $fields)
                @if ($fields->type == 'dyn_textarea')
                <div class="col-md-6 padding_sm" style="margin-top: 10px">
                    <label><strong>{{ $fields->name }}</strong></label>
                    <div id="{{ $fields->class_name }}" class="tabcontents" data-element-type="{{ $fields->type }}">
                        <textarea attr-dataid="{{ $fields->class_name }}"
                            class="form-control notes_templates {{ $fields->class_name }}_textarea"
                            style="resize: none">{{ @$patient_assessment_data[$fields->class_name] ? $patient_assessment_data[$fields->class_name] : '' }}</textarea>
                    </div>
                </div>
                @endif
                @endforeach
                @endif
            </div>
        </form>
    </div>
</div>
<div class="col-md-4 padding_sm">
    <div class="box no-border no-margin">
        <div class="box-footer revenue_main_shadow" style="min-height: 640px;">
            <div class="col-md-8 blue padding_sm" style="font-weight: 600;">History</div>
            <div class="col-md-4 padding_sm" id="notes_templates_history" style="display: none">
                <button onclick="resetEmrData(1, 2);" style="padding: 0px 4px" class="btn btn-success" type="button">Add
                    New <i class="fa fa-plus"></i></button>
            </div>
            <div class="clearfix"></div>
            <div class="theadscroll always-visible" style="position: relative; height: 630px;" id="eyeEmrNotesList">

            </div>
        </div>
    </div>
</div>
