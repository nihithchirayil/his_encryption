<?php


//--------op numberprogressive search--------------------
if (isset($bill_list) ) {
    if(!empty($bill_list)){
        ?>
        <table cellpadding="5" cellspacing="5" class="table table-striped table-bordered table-condensed theadfix_wrapper sorted_table table_sm" style="margin-bottom: 10px;">
            <thead>
            <tr class="table_header_bg">
                <th>Bill No.</th>
                <th >Bill Tag</th>
                <th>Patient Name</th>
                <th>UHID</th>
                <th>Admitting Doctor</th>
                <th width="8%">Total Bill Amount</th>
                <th width="8%">Total Net Amount</th>
                <th>Bill Date</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @if(count($bill_list) > 0)
                @foreach($bill_list as $item)
                    <tr style="cursor:pointer; @if((int)$item->ot_update_status > 0) background-color:#ccd814; @endif" data-bill-tag-id="{{$item->bill_tag_id}}" data-bill-head-id="{{$item->id}}">
                        <td class="bill_no">{{ $item->bill_no }}</td>
                        <td class="bill_tag">{{ $item->bill_tag_name }}</td>
                        <td class="patient_name">{{ $item->patient_name }}</td>
                        <td class="uhid">{{ $item->uhid }}</td>
                        <td class="admitting_doctor">{{ $item->doctor_name }}</td>
                        <td class="bill_amount">{{ $item->bill_amount }}</td>
                        <td class="total_net_amount">{{ $item->net_amount_wo_roundoff }}</td>
                        <td class="bill_date">{{ date( 'd-M-Y', strtotime($item->bill_date)) }}</td>
                        <td class="created_date">{{ date( 'd-M-Y H:i:s', strtotime($item->created_at)) }}</td>
                        <td >@if((int)$item->ot_update_status == 0)<button class="btn btn-sm btn-default btn-primary changeOTBillTagBtn" title="Change Bill Tag" ><i class="fa fa-exchange"></i> Change</button>@endif</td>
                    </tr>
                @endforeach
                @else
                <tr style="cursor:pointer;" >
                    <td colspan="11">No bills found..!</td>
                </tr>
                @endif

            </tbody>
        </table>
        <div id="pagination">{{{ $bill_list->links() }}}</div>
        <?php
    } else {
        echo 'No Results Found';
    }
}

?>

        