<?php


//--------op numberprogressive search--------------------
if (isset($service_item_details) ) {
    if(!empty($service_item_details)){
        ?>
        <div class="pop_closebtn">X</div>
        <table width="400px">
            <thead>
            <tr class="table_header_bg">
                <th>Name</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>

        <?php
        foreach($service_item_details as $item) {
            ?>
            
                <tr>
                    <td>
                        <?php echo htmlentities(ucfirst($item->service_desc)) ?>
                    </td>
                    <td style="width:20%;">
                        <input data-detail-id="<?php echo htmlentities(ucfirst($item->id)) ?>" data-head-id="<?php echo htmlentities(ucfirst($item->head_id)) ?>" type="number" min="0" maxlength="6" class="form-control package_item_amount" name="" data-service-code="<?php echo htmlentities(ucfirst($item->service_code)) ?>" value="<?php echo htmlentities(ucfirst($item->price)) ?>">
                    </td>
                </tr>
            
            <?php
        }
        ?>
        </tbody>
        </table>
        <?php
    } else {
        echo 'No Results Found';
    }
}

?>

        