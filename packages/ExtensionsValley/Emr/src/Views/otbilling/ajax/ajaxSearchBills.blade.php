<?php


//--------op numberprogressive search--------------------
if (isset($bill_list) ) {
    if(!empty($bill_list)){
        ?>
        <table cellpadding="5" cellspacing="5" class="table table-striped table-bordered table-condensed theadfix_wrapper sorted_table table_sm" style="margin-bottom: 10px;">
            <thead>
            <tr class="table_header_bg">
                <th>Bill No.</th>
                <th >Bill Tag</th>
                <th>Patient Name</th>
                <th>UHID</th>
                <th>Admitting Doctor</th>
                <th width="8%">Total Bill Amount</th>
                <th width="8%">Total Net Amount</th>
                <th>Bill Date</th>
                <th>Created Date</th>
                <th>Payment Type</th>
                <th>Paid Status</th>
            </tr>
            </thead>
            <tbody>
                @foreach($bill_list as $item)
                    <tr onclick="fetchBillDetails({{$item->id}})" style="cursor:pointer;" >
                        <td>{{ $item->bill_no }}</td>
                        <td>{{ $item->bill_tag_name }}</td>
                        <td>{{ $item->patient_name }}</td>
                        <td>{{ $item->uhid }}</td>
                        <td>{{ $item->doctor_name }}</td>
                        <td>{{ $item->bill_amount }}</td>
                        <td>{{ $item->net_amount_wo_roundoff }}</td>
                        <td>{{ date( 'd-M-Y', strtotime($item->bill_date)) }}</td>
                        <td>{{ date( 'd-M-Y H:i:s', strtotime($item->created_at)) }}</td>
                        <td>{{ $item->payment_type }}</td>
                        <td>@if($item->paid_status == 1) Paid @else Unpaid @endif </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div id="pagination">{{{ $bill_list->links() }}}</div>
        <?php
    } else {
        echo 'No Results Found';
    }
}

?>

