<div id="doctornotes_scroll" class="theadscroll" style="position: relative;height:520px">
    @php
    $user_id = (!empty(\Auth::user()->id)) ? \Auth::user()->id : 0;
    $current_date =date('M-d-Y');
    @endphp
    @if(count($doctor_notes)!=0)
    @foreach ($doctor_notes as $each)

    <div class="col-md-12 padding_sm"
        style="@if($loop->iteration % 2 == 0) background: #E3E3E3; @else background: #d9e4dfa3;  @endif  padding: 10px !important; border-radius: 11px; @if(!$loop->first) margin-top: 5px @endif">
        <div class="col-md-4 padding_sm blue">
            <strong> Created At : {{ $each->created_at }}</strong>
        </div>
        <div class="col-md-6 padding_sm blue">
            <strong> Created By : {{ $each->created_by_name }} ({{ $each->speciality }}) </strong>
        </div>
        <div class="col-md-2 padding_sm">
            @if($each->created_at==$current_date && $each->created_by==$user_id)
            <button class="btn btn-default" id="editNusingNotesBtn{{ $each->notes_id }}" type="button"
                onclick="editDoctorNotes({{ $each->notes_id }})">
                <i id="editNusingNotesSpin{{ $each->notes_id }}" class="fa fa-edit orange"></i>
            </button>
            @endif
            @if($each->created_at == $current_date && $each->created_by == $user_id)
            <button class="btn btn-default" title="Delete" id="deleteDoctorNotesBtn{{ $each->notes_id }}" type="button"
                onclick="deleteDoctorNotes({{ $each->notes_id }})">
                <i id="deleteDoctorNotesSpin{{ $each->notes_id }}" class="fa fa-trash red"></i>
            </button>
            @endif
        </div>
        <div class="col-md-12 padding_sm" style="margin-top: 5px">
            @php
            $encoded_string = json_decode($each->doctor_notes,true);
            $decoded_string = str_replace("<br />", " ", $encoded_string);
            $decoded_string = html_entity_decode(strip_tags($decoded_string));
            @endphp
            {{ $decoded_string }}
        </div>
    </div>
    @endforeach
    @endif
</div>

