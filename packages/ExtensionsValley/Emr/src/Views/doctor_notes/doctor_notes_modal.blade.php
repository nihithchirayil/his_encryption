<link href="{{ asset('packages/extensionsvalley/purchase/default/css/doctor_notes.css') }}" rel="stylesheet">
<input type="hidden" id="doctor_master_hidden" value='{{ @$doctormaster  ? json_encode($doctormaster) : '' }}'>
<div class="modal fade" id="ipop_procedurelogsmodel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="width: 90vw;">
        <div class="modal-content">
            <div class="modal-header" style="background: #21a4f1; color: #FFFFFF;">
                <button type="button" class="close close_white" onclick="closeIpOPmodel()"><span aria-hidden="true"
                        style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="ipop_procedurelogsmodelHeader">NA</h4>
            </div>
            <div class="modal-body" style="min-height: 600px">
                <div id="ipop_procedurelogsmodelDiv"></div>
            </div>
            <div class="modal-footer">
                <button onclick="closeIpOPmodel()" style="padding: 3px 3px" type="button" class="btn btn-danger">Close
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="doctornotesmodel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="display:none;">
    <div class="modal-dialog" style="width: 90vw;">
        <div class="modal-content">
            <div class="modal-header" style="background: #21a4f1; color: #FFFFFF;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="doctornotesmodelHeader">NA</h4>
            </div>
            <div class="modal-body">
                <div class="row padding_sm">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-5 padding_sm">
                            <div class="box no-border no-margin">
                                <div class="box-footer revenue_main_shadow" style="min-height: 70vh;">
                                    <input type="hidden" id="nursingnotes_patientid" value="0">
                                    <input type="hidden" id="nursingnotes_doctorid" value="0">
                                    <input type="hidden" id="nursingnotes_visitid" value="0">
                                    <input type="hidden" id="doctornotes_id" value="0">
                                    <div class="col-md-12 padding_sm">
                                        <div class="custom-float-label-control mate-input-box">
                                            <label style="margin-top: -5px" class="custom_floatlabel ">Doctor_notes
                                                Notes</label>
                                            <textarea style="margin-top: 10px;" class="form-control" value=""
                                                type="text" autocomplete="off" id="patientDoctorNotes"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 padding_sm" style="margin-top: 20px">
                                        <div class="col-md-3 padding_sm pull-right">
                                            <button type="button" id="saveIpOpDoctorNotesBtn"
                                                onclick="saveIpOpDoctorNotes()" class="btn btn-primary btn-block">Save
                                                <i id="saveIpOpDoctorNotesSpin" class="fa fa-save"></i></button>
                                        </div>
                                        <div class="col-md-3 padding_sm pull-right">
                                            <button onclick="resetDoctorNotes()" type="button"
                                                class="btn btn-warning btn-block">Reset <i
                                                    class="fa fa-recycle"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 padding_sm">
                            <div class="box no-border no-margin">
                                <div class="box-footer revenue_main_shadow" style="min-height: 70vh;">
                                    <div id="doctornotesmodelDiv"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>

<script
    src="{{ asset('packages/extensionsvalley/emr/js/doctor_notes.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
