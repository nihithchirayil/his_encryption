@if ($type == 'count')
<div style="position: relative;">
    <table class="table table_sm" width="100%" border="1">
        <thead>
            <tr style="height: 30px;">
                <th width="50%">Intend Type</th>
                <th width="30%">Pending Intends</th>
                <th width="20%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr style="height: 30px;">
                <td>Investigation</td>
                <td @if(intval($inv_count)==0) style="color: green;" @else style="color:red;" @endif>{{$inv_count}}</td>
                <td>
                    @if(intval($inv_count) != 0)
                    <button type="button" onclick="viewPendingIntendData('{{$visit_id}}', 'inv');" class="btn btn-primary">View</button>
                    @endif
                </td>
            </tr>
            <tr style="height: 30px;">
                <td>Prescription</td>
                <td @if(intval($med_count)==0) style="color: green;" @else style="color:red;" @endif>{{$med_count}}</td>
                <td>
                    @if(intval($med_count) != 0)
                    <button type="button" onclick="viewPendingIntendData('{{$visit_id}}', 'med');" class="btn btn-primary">View</button>
                    @endif
                </td>
            </tr>
            <!-- <tr style="height: 30px;">
                <td>Procedure</td>
                <td @if(intval($yellow_count)==0) style="color: green;" @else style="color:red;" @endif>{{$yellow_count}}</td>
                <td>
                    @if(intval($yellow_count) != 0)
                    <button type="button" onclick="viewPendingIntendData('{{$visit_id}}', 'procedure');" class="btn btn-primary">View</button>
                    @endif
                </td>
            </tr> -->
            <tr style="height: 30px;">
                <td>Medicine Return</td>
                <td @if(intval($med_rtn_count)==0) style="color: green;" @else style="color:red;" @endif>{{$med_rtn_count}}</td>
                <td>
                    @if(intval($med_rtn_count) != 0)
                    <button type="button" onclick="viewPendingIntendData('{{$visit_id}}', 'med_rtn');" class="btn btn-primary">View</button>
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
</div>
@elseif ($type == 'med')
<div class="theadscroll" style="position: relative;margin-bottom:30px;height: 400px;">
    <table class="table table-striped theadfix_wrapper table-bordered table-condensed">
        <thead>
            <tr class="headergroupbg_blue">
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Intend Id</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Type</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Created On</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Created By</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Doctor</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Visit Status</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($res_data))
            @foreach($res_data as $res)
            <tr onclick="toggle_indent_pending_list_tr('res_detail', {{$res->id}})" style="cursor: pointer;">
                <td>{{$res->id}}</td>
                <td>Prescription</td>
                <td>{{date('M-d-Y h:i A',strtotime($res->created_at))}}</td>
                <td>{{$res->name}}</td>
                <td>{{$res->doctor_name}}</td>
                <td>{{$res->visit_status}}</td>
            </tr>
            @php
            $res_array = @$res_details[$res->id] ? $res_details[$res->id] : array();
            @endphp
            @if (!empty($res_array) && count($res_array) != 0)
            <tr class="res_detail_{{ $res->id }}">
                <th></th>
                <th style="font-weight: 100;background: #21a4f1;color: #fff; width:20px;">#</th>
                <th style="font-weight: 100;background: #21a4f1;color: #fff;">Item Name</th>
                <th colspan="3"></th>
            </tr>
            @foreach($res_array as $val)
            <tr class="res_detail_{{ $res->id }} ">
                <td></td>
                <td>{{$loop->iteration}}</td>
                <td style="text-align: left;">{{ $val->item_desc }}</td>
                <td colspan="3"></td>
            </tr>
            @endforeach
            @endif

            @endforeach
            @endif
        </tbody>
    </table>
</div>
@elseif ($type == 'inv')
<div class="theadscroll" style="position: relative;margin-bottom:30px;height: 400px;">
    <table class="table table-striped theadfix_wrapper table-bordered table-condensed">
        <thead>
            <tr class="headergroupbg_blue">
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Intend Id</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Type</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Created On</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Created By</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Doctor</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Visit Status</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($res_data))
            @foreach($res_data as $res)
            <tr onclick="toggle_indent_pending_list_tr('res_detail', {{$res->id}})" style="cursor: pointer;">
                <td>{{$res->id}}</td>
                <td>Investigation</td>
                <td>{{date('M-d-Y h:i A',strtotime($res->created_at))}}</td>
                <td>{{$res->created_user}}</td>
                <td>{{$res->doctor_name}}</td>
                <td>{{$res->visit_type}}</td>
            </tr>
            @php
            $res_array = @$res_details[$res->id] ? $res_details[$res->id] : array();
            @endphp
            @if (!empty($res_array) && count($res_array) != 0)
            <tr class="res_detail_{{ $res->id }}">
                <th></th>
                <th style="font-weight: 100;background: #21a4f1;color: #fff; width:20px;">#</th>
                <th style="font-weight: 100;background: #21a4f1;color: #fff;">Item Name</th>
                <th colspan="3"></th>
            </tr>
            @foreach($res_array as $val)
            <tr class="res_detail_{{ $res->id }} ">
                <td></td>
                <td>{{ $loop->iteration }}</td>
                <td style="text-align: left;">{{ $val->service_desc }}</td>
                <td colspan="3"></td>
            </tr>
            @endforeach
            @endif

            @endforeach
            @endif
        </tbody>
    </table>
</div>
@elseif ($type == 'procedure')
<div class="theadscroll" style="position: relative;margin-bottom:30px;height: 400px;">
    <table class="table table-striped theadfix_wrapper table-bordered table-condensed">
        <thead>
            <tr class="headergroupbg_blue">
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Intend Id</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Type</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Created On</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Created By</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Doctor</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1;">Visit Status</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($res_details))
            @foreach($res_details as $key => $res)
            <tr onclick="toggle_indent_pending_list_tr('res_detail', {{strtotime($key)}})" style="cursor: pointer;">
                <td colspan="2">Procedure</td>
                <td>{{date('M-d-Y',strtotime($key))}}</td>
                <th colspan="3"></th>
            </tr>
            @php
            $res_array = @$res_details[$key] ? $res_details[$key] : array();
            @endphp
            @if (!empty($res_array) && count($res_array) != 0)
            <tr class="res_detail_{{ strtotime($key) }}">
                <th></th>
                <th style="font-weight: 100;background: #21a4f1;color: #fff; width:20px;">#</th>
                <th style="font-weight: 100;background: #21a4f1;color: #fff;">Item Name</th>
                <th colspan="3"></th>
            </tr>
            @foreach($res_array as $val)
            <tr class="res_detail_{{ strtotime($key) }} ">
                <td>{{ $val->id }}</td>
                <td>{{ $loop->iteration }}</td>
                <td style="text-align: left;">{{ $val->service_desc }}</td>
                <td>{{ $val->user_name }}</td>
                <td>{{ $val->doctor_name }}</td>
                <td>{{ $val->visit_status }}</td>
            </tr>
            @endforeach
            @endif

            @endforeach
            @endif
        </tbody>
    </table>
</div>
@elseif ($type == 'med_rtn')
<div class="theadscroll" style="position: relative;margin-bottom:30px;height: 400px;">
    <table class="table table-striped theadfix_wrapper table-bordered table-condensed">
        <thead>
            <tr class="headergroupbg_blue">
                <th style="font-weight: 100;color:white;background-color:#21a4f1; width: 7%;">Intend Id</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1; width: 10%;">Type</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1; width: 22%;">Created On</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1; width: 10%;">Created By</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1; width: 25%;">Remarks</th>
                <th style="font-weight: 100;color:white;background-color:#21a4f1; width: 10%;">Visit Status</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($res_data))
            @foreach($res_data as $res)
            <tr onclick="toggle_indent_pending_list_tr('res_detail', {{$res->head_id}})" style="cursor: pointer;">
                <td>{{$res->head_id}}</td>
                <td>Medicine Return</td>
                <td>{{date('M-d-Y h:i A',strtotime($res->requested_at))}}</td>
                <td>{{$res->created_by}}</td>
                <td>{{$res->remarks}}</td>
                <td>{{$res->visit_status}}</td>
            </tr>
            @php
            $res_array = @$res_details[$res->head_id] ? $res_details[$res->head_id] : array();
            @endphp
            @if (!empty($res_array) && count($res_array) != 0)
            <tr class="res_detail_{{ $res->head_id }}">
                <th></th>
                <th style="font-weight: 100;background: #21a4f1;color: #fff; width:20px;">#</th>
                <th style="font-weight: 100;background: #21a4f1;color: #fff;">Item Name</th>
                <th colspan="3"></th>
            </tr>
            @foreach($res_array as $val)
            <tr class="res_detail_{{ $res->head_id }} ">
                <td></td>
                <td>{{ $loop->iteration }}</td>
                <td style="text-align: left;">{{ $val->item_desc }}</td>
                <td colspan="3"></td>
            </tr>
            @endforeach
            @endif

            @endforeach
            @endif
        </tbody>
    </table>
</div>
@endif