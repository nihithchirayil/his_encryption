<style>
    .container-fluid .form-control{
         height: 36px !important;
         border-radius: 5px !important;
         font-size: 15px !important;
         padding-left: 7px !important;
     }
     .container-fluid label{
         font-size: 13px !important;
         color: gray !important;
     }
     .reg_title{
         margin-top: 15px;
         margin-bottom:15px;
     }

     .container-fluid .btn{
         height: 35px !important;
         width: 90px !important;
     }
     .datepicker{
         border:1px solid #d9d4d4 !important;
     }

     .container-datepicker .form-control{
        padding-left:0px !important;
     }

 </style>
 <form class="container-fluid">
    <div class="col-md-12 reg_title">
        <h5><b>New Patient Registration</b></h5>
    </div>
    @php
        $gender = \DB::table('gender')->pluck('name','id');
        $title = \DB::table('title_master')->where('status',1)->pluck('title','title');
        $specialities = \DB::table('speciality')->where('status','1')->orderBy('name','ASC')->pluck('name','id');
        $doctors_list = \DB::table('doctor_master')->where('status','1')->orderBy('doctor_name','ASC')->pluck('doctor_name','id');
    @endphp
    <div class="col-md-12">
        <div class="col-md-1">

            <label>Patient Name</label>
            {!! Form::select('new_reg_title',$title,'Mr.', ['class' => 'form-control', 'id' =>'new_reg_title']) !!}
        </div>
        <div class="col-md-3">
            <label>&nbsp;</label>
            <input type="text" name="new_reg_patient_name" id="new_reg_patient_name" class="form-control"/>
        </div>
        <div class="col-md-2">
            <label>Age</label>
            <div class="col-md-12 no-margin">
                <div class="col-md-4 no-margin">
                    <input type="text" placeholder="y" title="Years" name="new_reg_age_y" id="new_reg_age_y" class="form-control"/>
                </div>
                <div class="col-md-4 no-margin">
                    <input type="text" placeholder="m" title="Months" name="new_reg_age_m" id="new_reg_age_m" class="form-control"/>
                </div>
                <div class="col-md-4 no-margin">
                    <input type="text" placeholder="d" title="Days" name="new_reg_age_d" id="new_reg_age_d" class="form-control"/>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <label>DOB</label>
            <input type="text" name="new_reg_dob" id="new_reg_dob" class="form-control datepicker"/>
            <input type="hidden" name="dobnotknown" id="dobnotknown" value="0"/>
        </div>
    </div>
    <div class="col-md-12" style="margin-top:10px;">
        <div class="col-md-2">

            <label>Gender</label>
            {!! Form::select('new_reg_gender',$gender,null, ['class' => 'form-control','placeholder'=>'Select','id' =>'new_reg_gender']) !!}
        </div>
        <div class="col-md-2">
            <label>Phone</label>
            <input type="text" name="new_reg_phone" id="new_reg_phone" class="form-control"/>
        </div>
    </div>

    <div class="col-md-12 container-datepicker" style="margin-top:10px;">
        <div class="col-md-4">
            <label for="select">Select Department</label>
            {!! Form::select('new_reg_department',$specialities,null, ['class' => 'form-control select2', 'id' =>'new_reg_department','placeholder'=>'select','onchange'=>'selectDoctorsList()','style' => 'color:#555555; padding:4px 12px;']) !!}
        </div>
        <div class="col-md-4">
            <label for="select">Select Doctor</label>
            {!! Form::select('new_reg_doctor_id',$doctors_list,null, ['class' => 'form-control select2', 'id' =>'new_reg_doctor_id','placeholder'=>'select', 'style' => 'color:#555555; padding:4px 12px;']) !!}
        </div>
    </div>


     <div class="col-md-12" style="margin-top:10px;">
         <div class="col-md-6 pull-right">
            <button type="reset" id="reset_patient_registration" class="btn bg-orange">
                <i class="fa fa-repeat"></i> Reset
            </button>
            <button type="button" class="btn bg-blue" onclick="nsRegisterNewPatient();">
                <i class="fa fa-save"></i> Save
            </button>
         </div>
         <div class="col-md-6"></div>
     </div>
 </form>
