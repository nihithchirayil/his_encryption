<div class="col-md-12 padding_sm table_box investigation_results_entering_area">
    <div class="theadscroll" style="position: relative; height:45vh;">
        <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed" id="investigation_result_entry_table">
            <thead>
                <tr class="tableRowHead">
                    <th style="width:20%">Date</th>
                    <th style="width:35%">Investigation</th>
                    <th style="width:40%">Result</th>
                    <th style="width:5%">
                        <i onclick="addNewInvResultEntryNs();" class="fa fa-plus"></i>
                    </th>
                </tr>
            </thead>
            <tbody id="investigation_result_entry_tbody">
            @if(sizeof($resultData)>0)
            @php
                $i=1;
            @endphp
            @foreach($resultData as $data)
            <tr>
                <td>
                    <input type="textbox" value="{{date('M-d-Y',strtotime($data->investiagation_date))}}" name="investiagation_entry_date[]" id="investiagation_entry_date_{{$i}}" class="form-control bottom-border-text borderless_textbox datepicker_poc"/>
                </td>
                <td>
                    <input type="textbox" value="{{$data->investiagation}}"  name="investiagation_entry[]" id="investiagation_entry_{{$i}}" class="form-control bottom-border-text borderless_textbox" autocomplete="off"/>
                </td>
                <td>
                    <input type="textbox" value="{{$data->investiagation_result}}" name="investiagation_entry_result[]" id="investiagation_entry_result_{{$i}}" class="form-control bottom-border-text borderless_textbox" autocomplete="off"/>
                </td>
                <td>
                    <i onclick="removeInvResultEntry(this)" class="fa fa-times red"></i>
                </td>
            </tr>
            @php
            $i++;
            @endphp
            @endforeach
            @else
            <tr>
                <td>
                    <input type="textbox" name="investiagation_entry_date[]" id="investiagation_entry_date_1" class="form-control bottom-border-text borderless_textbox datepicker_poc" />
                </td>
                <td>
                    <input type="textbox" name="investiagation_entry[]" id="investiagation_entry_1" class="form-control bottom-border-text borderless_textbox" autocomplete="off" />
                </td>
                <td>
                    <input type="textbox" name="investiagation_entry_result[]" id="investiagation_entry_result_1" class="form-control bottom-border-text borderless_textbox" autocomplete="off" />
                </td>
                <td>
                    <i onclick="removeInvResultEntry(this)" class="fa fa-times red"></i>
                </td>
            </tr>
            @endif                
            </tbody>
        </table>
    </div>
</div>
