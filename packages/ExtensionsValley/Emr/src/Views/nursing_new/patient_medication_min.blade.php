<link href="{{ asset('packages/extensionsvalley/master/default/css/new_emr.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
rel="stylesheet">
<style>
    .form-control {
    border: none !important;
    /* border-bottom: 1px solid lightgrey !important; */
    box-shadow: none !important;
}
.presc_theadscroll{
    padding:6px !important;
}
#doctor_prescription{
    font-weight: 600;
}
.med_view_clos{
      float: right;
      font-size: 11px;
      font-weight: 700;
      line-height: 1;
      color: #000;
      text-shadow: 0 1px 0 #fff;
      filter: alpha(opacity=20);
      border: white;
   }
   .med_his_clos{
      float: right;
      font-size: 11px;
      font-weight: 700;
      line-height: 1;
      color: #000;
      text-shadow: 0 1px 0 #fff;
      filter: alpha(opacity=20);
      border: white;
   }
</style>
<style>
    .med_close{
      float: right;
      font-size: 11px;
      font-weight: 700;
      line-height: 1;
      color: #000;
      text-shadow: 0 1px 0 #fff;
      filter: alpha(opacity=20);
      border: white;
   }
   .table_header_bg.header_normal_padding>th {
        padding: 8px !important;
    }
    .timepickerclass {
        font-weight: 600;
    }
    .timepickerclass_default {
        color: #a8a8a8 !important;
    }
    .bootstrap-datetimepicker-widget .form-control,
    .bootstrap-datetimepicker-widget .btn {
        box-shadow: none !important;
    }
    .bootstrap-datetimepicker-widget .timepicker-hour,
    .bootstrap-datetimepicker-widget .timepicker-minute,
    .bootstrap-datetimepicker-widget .timepicker-second {
        width: 70% !important;
    }
    .bootstrap-datetimepicker-widget td span {
        height: 34px !important;
        line-height: 34px !important;
    }
    .timepickerclass {
        border-bottom: 1px solid lightgrey !important;
    }
</style>
@php
$enable_ward_stock_data =  \DB::table('config_detail')->where('name', 'is_ward_stock')->get();
$enable_ward_stock = 0;
        if (count($enable_ward_stock_data) > 0) {
            $enable_ward_stock = (int)$enable_ward_stock_data[0]->value;
        } else {
            $enable_ward_stock = 0;
        }
@endphp
<input type="hidden" value="{{ $enable_ward_stock }}" id='enable_ward_stock'>
<input type="hidden" id="pres_patient_id" value="{{$patient_id}}"/>
<input type="hidden" id="pres_visit_id" value="{{$visit_id}}"/>
<input type="hidden" id="pres_encounter_id" value="{{$encounter_id}}"/>
<input type="hidden" id="pres_admitting_doctor_id" value="{{$admitting_doctor_id}}"/>
<input type="hidden" id="prescription_head_id" value=""/>
<input type="hidden" id="administration_in_nurse_dashboard" value="{{$administration_in_nurse_dashboard}}"/>
@if($allergy_medcation_list !='')
@php
$allergy_medcation_list = base64_encode(json_encode($allergy_medcation_list));

@endphp
@endif
<input type="hidden" id="hiddenAllergy_medcation_list" value="{{$allergy_medcation_list}}"/>

<div class="col-md-12 no-padding">

    <div class="col-md-12" style="padding: 2px;">
        <div class="card">




            <input type="hidden" value="{{ $enable_ward_stock }}" id='enable_ward_stock'>
            <div class="col-md-12" style="padding: 5px;">
                <div class="col-md-5 padding_sm pull-right" style="padding-right: 15px !important;">
                    <button style="width: 110px !important;float: right;height: 25px;" class="btn bg-info" onclick="loadCompletePrescriptionHistory(1);"><i class="fa fa-history"
                            aria-hidden="true"></i> Complete History</button>
                    <button style="width: 110px !important;float: right;height: 25px;" class="btn bg-info presc_history_btn" onclick="loadPrescriptionHistory(1, 1);"
                        data-toggle="collapse" data-target="#prescriptionCollapse"><i class="fa fa-history"></i>
                        History</button>
                    @if ($new_bookmark_in_ns == '1')
                    <button style="width: 110px !important;float: right;height: 25px; color: black !important; font-size: 11px;" class="btn  new_medication_bookmarks_btn bg-info">
                        <i class="fa fa-star"></i> Bookmarks
                    </button>
                    @else
                    <button style="width: 110px !important;float: right;height: 25px; color: black !important; font-size: 11px;" class="btn  fav_dropdown_btn bg-info"><i class="fa fa-star"></i>
                        Bookmarks</button>
                    @endif
                    @php
                    try {
                        $company_code=\DB::table('company')->where('id','1')->value('code');
                    } catch (\Throwable $th) {
                        $company_code='';
                    }
                    @endphp
                    @if ($company_code=='ROHINI')
                    <div class="col-md-2 padding_sm" style="float:right;">
                        <button style="width: 110px !important;float: right;height: 25px;" class="btn bg-info save_class"  onclick="saveDoctorPrescriptions(0);"
                        name="save_and_print_prescription1"    id="save_and_print_prescription1"
                        data-toggle="collapse" data-target="#prescriptionCollapse"><i class="fa fa-save"></i>
                        Save</button>
                        {{-- <button type="button" onclick="saveDoctorPrescriptions(0);"
                            class="btn btn-primary save_class" name="save_and_print_prescription1"
                            id="save_and_print_prescription1" style="margin-left:106px;"><i
                                class="fa fa-save"></i> Save</button> --}}
                    </div>
                    @endif
                    {{-- <span style="margin-left: 10px;">
                        <input type="checkbox" name="print_prescription" checked="checked" id="print_prescription" value="1">
                        <label for="print_prescription">Print Prescription</label>
                    </span> --}}

                </div>
                <div class="col-md-3 padding_sm">
                    @if (session()->has('nursing_station'))
                        @php
                            $doctor_list = \DB::table('doctor_master')->where('status',1)->orderBy('doctor_name', 'ASC')->pluck('doctor_name','id');
                        @endphp
                        {!! Form::select('doctor_prescription', $doctor_list, isset($admitting_doctor_id) ? $admitting_doctor_id : null, ['class' => 'form-control select2', 'title' => 'Doctor', 'id' => 'doctor_prescription', 'style' => 'color:#555555; padding:2px 12px; ']) !!}
                    @else
                        {!! Form::select('doctor_prescription', $doctor_list, isset($admitting_doctor_id) ? $admitting_doctor_id : null, ['class' => 'form-control select2', 'title' => 'Doctor', 'id' => 'doctor_prescription', 'style' => 'color:#555555; padding:2px 12px; ', 'disabled' => 'disabled']) !!}
                    @endif
                </div>
                <div class="col-md-3 padding_sm">
                    @if (session()->has('nursing_station'))
                        @php
                            $default_station = session()->get('nursing_station');
                            $list_nursing_station = \DB::table('location')
                                ->where('is_nursing_station', '1')
                                ->orderBy('location_name')
                                ->pluck('location_name', 'id');
                        @endphp
                        {!! Form::select('prescription_nursing_station', $list_nursing_station, isset($default_station) ? $default_station : 0, ['class' => 'form-control select2', 'title' => 'Nursing station', 'id' => 'prescription_nursing_station', 'style' => 'color:#555555; padding:2px 12px;font-weight:600;']) !!}
                    @endif
                </div>

                <div class="col-md-1 padding_sm visit_wise_filter_presc_div " style="display:none;">
                    @php
                        $visit_type_arr = [
                            'OP' => 'OP',
                            'IP' => 'IP',
                            'ALL' => 'ALL',
                        ];
                    @endphp
                    {!! Form::select('visit_wise_filter_presc', $visit_type_arr, 'ALL', ['class' => 'form-control select2', 'title' => 'Visit Wise Filter', 'onchange' => 'loadPrescriptionHistory(1);', 'id' => 'visit_wise_filter_presc', 'style' => 'color:#555555; padding:2px 12px;font-weight:600;']) !!}
                </div>

                <!-- <div class="col-md-2 padding_sm">
                    <button type="button" onclick="saveDoctorPrescriptions(2);" class="btn btn-primary" name="save_and_print_prescription" id="save_and_print_prescription" style="margin-left: 45px;"><i class="fa fa-save"></i> Save and print</button>
                </div> -->

                <!-- Prescription History View -->
                <div class="col-md-12 no-padding">
                    <div class="card collapse" id="prescriptionCollapse">
                        <div class="card_body" id="prescription-history-data-list">
                            @include('Emr::emr.prescription.prescription_history_view')
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht5"></div>
                        <div class="col-md-12 no-padding ">

                        </div>
                    </div>
                </div>
                <!-- Prescription History View -->
                @include('Emr::emr.prescription.prescription_favorite')
            </div>













            <div id="drug" class="tab-pane fade in active">
                <div class="col-md-12 no-padding expand_prescription_btn" style="cursor: pointer;">
                    {{-- <div class="col-md-12" style="padding:0 15px 0 0;">
                        <h4 class="card_title">
                            Prescription <span class="draft_mode_indication_p_consumable"></span>
                        </h4>
                    </div> --}}
                </div>
                <div class="col-md-12" style="padding: 2px;">

                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <div class="card">
                        <div class="" style="background: #FFF;border-top:3px solid #dadaff;" id="prescription_wrapper_card_body">
                            <div class="col-md-12 no-padding" style="margin-top:10px;">
                                <div class="col-md-4">
                                    <span style="display: inline-block;">
                                        <label>Search Medicine by :</label>
                                        <div class="radio_container" style="width:170px !important;">
                                            <input type="radio" id="m_s_type_brand" value="brand"
                                            name="m_search_type" checked >
                                            <label for="m_s_type_brand">Brand</label>

                                            <input type="radio" id="m_s_type_generic" value="generic"
                                            name="m_search_type">
                                            <label for="m_s_type_generic">Generic</label>

                                            <input type="radio" id="m_s_type_both" value="both" name="m_search_type"
                                            checked="">
                                            <label for="m_s_type_both">Both</label>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-md-8">
                                    <span style="display: inline-block;">
                                        <label> Intend Type:</label>
                                        <div class="radio_container"  style="width:560px !important;">
                                            <input type="radio" id="p_type_regular" value="1" name="p_search_type"
                                            checked="">
                                            <label for="p_type_regular">Regular</label>

                                            <input type="radio" id="p_type_new_adm" value="2" name="p_search_type">
                                            <label for="p_type_new_adm" style="width: 127px !important;">New Admission</label>

                                            <input type="radio" id="p_type_emergency" value="3"
                                            name="p_search_type">
                                            <label for="p_type_emergency">Emergency</label>

                                            <input type="radio" id="p_type_discharge" value="4"
                                            name="p_search_type">
                                            <label for="p_type_discharge">Discharge</label>

                                            <input type="radio" id="p_type_outside" value="5" name="p_search_type">
                                            <label for="p_type_outside" style="width: 103px !important;"> Own Medicine</label>
                                        </div>
                                    </span>
                                </div>
                            </div>


                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <table class="table no-margin no-border" style="margin-top:7px !important;">
                                <thead>
                                    @if($administration_in_nurse_dashboard == '1')
                                    <tr class="bg-default" style="background-color:#e6e6e6;">
                                        <th width="5%" style="padding-bottom:5px !important;">Sl.No</th>
                                        <th width="22%" style="padding-bottom:5px !important;">Medicine</th>
                                        <th width="18%" style="padding-bottom:5px !important;">Generic Name</th>
                                        <th width="7%" style="padding-bottom:5px !important;">Dose</th>
                                        <th width="10%" style="padding-bottom:5px !important;">Frequency <i class="fa fa-plus" style="cursor: pointer;" onclick="addFrequency();"></i></th>
                                        <th width="5%" style="padding-bottom:5px !important;">Days</th>
                                        <th width="10%" style="padding-bottom:5px !important;">Quantity</th>
                                        <th width="13%" style="padding-bottom:5px !important;">Instructions</th>
                                        @if($enable_ward_stock != 0)
                                        <th style="width:5%;padding-bottom:5px !important;">Ward Stock</th>
                                        <th  width="5%" align="center" style="padding:0;"><button class="btn light_purple_bg" onclick="addNewMedicine();"><i
                                                    class="fa fa-plus"></i>
                                            </button>
                                        </th>
                                        @else
                                        <th  width="10%" align="center" style="padding:0;">
                                            <button class="btn light_purple_bg" onclick="addNewMedicine();"><i
                                            class="fa fa-plus"></i>
                                            </button>
                                        </th>
                                        @endif
                                    </tr>
                                    @else
                                    <tr class="bg-default" style="background-color:#e6e6e6;">
                                        <th style="width: 5%;padding-bottom:5px !important;">Sl.No</th>
                                        <th style="width: 30%;padding-bottom:5px !important;">Medicine</th>
                                        <th style="width: 25%;padding-bottom:5px !important;">Generic Name</th>
                                        {{-- <th style="width: 7%;">Dose</th> --}}
                                        {{-- <th style="width: 10%;">Frequency <i class="fa fa-plus" style="cursor: pointer;" onclick="addFrequency();"></i></th> --}}
                                        {{-- <th style="width: 5%;">Days</th> --}}
                                        <th style="width: 10%;padding-bottom:5px !important;">Quantity</th>
                                        {{-- <th style="width: 10%;">Start Time</th> --}}
                                        {{-- <th style="width: 10%;">Route <i id="addDoctorRoutesBtn" class="fa fa-plus" style="cursor: pointer;" onclick="addRoute(0);"></i></th> --}}
                                        <th style="width: 20%;padding-bottom:5px !important;">Instructions</th>
                                        @if($enable_ward_stock != 0)
                                        <th style="width: 5%;padding-bottom:5px !important;">Ward Stock</th>
                                        <th width="5%" align="center" style="padding:0;margin-right:44px;float: right;"><button class="btn light_purple_bg" onclick="addNewMedicine();"><i
                                                    class="fa fa-plus"></i>
                                            </button>
                                        </th>
                                        @else
                                        <th width="5%" align="center" style="padding:0; text-align:center; /* float:right; *//* margin-right:68px; */"><button class="btn light_purple_bg" onclick="addNewMedicine();"><i
                                            class="fa fa-plus"></i>
                                            </button>
                                        </th>
                                        @endif
                                    </tr>
                                    @endif
                                </thead>

                            </table>

                            <div class="frequency-list-div" style="display: none;">
                                <a style="float: left;" class="close_btn_freq_search">X</a>
                                <div class="freq_theadscroll" style="position: relative;">
                                    <table id="FrequencyTable"
                                        class="table table-bordered-none no-margin table_sm table-striped presc_theadfix_wrapper">
                                        {{-- <thead>
                                        <tr class="light_purple_bg">
                                            <th>Frequency</th>
                                        </tr>
                                        </thead> --}}
                                        <tbody id="ListFrequencySearchData">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>


                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <div id="presc-data-form" action="" style="min-height:200px;">
                                <div class="" style="">
                                    <table border="1"
                                        style="width: 100%;height: 15%;border:1px solid;border-collapse: collapse !important"
                                        class="table  table_sm  table-bordered" id="medicine-listing-table">
                                        <tbody>

                                        </tbody>
                                    </table>
                                    <div class="col-md-2 padding_sm" style="float:right;">
                                        <button type="button" onclick="saveDoctorPrescriptions(1);"
                                            class="btn btn-primary save_class" name="save_and_print_prescription1"
                                            id="save_and_print_prescription1" style="/*margin-left:180px;*/float:right;"><i
                                                class="fa fa-save"></i> Save</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>








        </div>
    </div>
</div>
<div id="med_det_list_modal1" class="modal fade " role="dialog" style="margin-top: 60px;height: 424px;">
    <div class="modal-dialog" style="width:60%">

        <div class="modal-content" style="background: #e4e3e3;">
            <div class="modal-header" style="background: #07a5ef; color:#ffffff;">
                <button type="button" class="med_view_clos" data-dismiss=""  id="med_view_clos" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    <h4 class="modal-title">Prescription Details</h4>
                </div>
            <div class="modal-body" id="" style="height:232px">

                <div class="row">
                    <div class="col-md-12">
                        <div class="theadscroll always-visible" style="position: relative;height: 300px;">
                            <table class="table no-margin table_sm table-borderd table-striped">
                                <thead id="med_det_list_modal_head1">
                                    <tr class="table_header_bg" style="background: #96babd;">
                                        <th>#</th>
                                        <th width="22%">Medicine</th>
                                        <th width="18%">Generic Name</th>
                                        <th>Dose</th>
                                        <th>Frequency</th>
                                        <th>Days</th>
                                        <th>Quantity</th>
                                        <th>Route</th>
                                        <th>Instructions</th>
                                    </tr>
                                </thead>
                                <tbody id="med_det_list_modal_content1">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm applyPatientMed">Apply</button>
                <button type="button" class="btn btn-default med_view_clos" data-dismiss="" aria-label="" id="med_view_clos">Close</button>
            </div>
        </div>
    </div>

</div>

<div id="complete_medication_history_modal1" class="modal fade " role="dialog" style="margin-top: 60px;height:540px;">
    <div class="modal-dialog" style="width:60%">

        <div class="modal-content" style="background: #e4e3e3;">
            <div class="modal-header" style="background: #07a5ef; color:#ffffff;">
                <button type="button" class="med_his_clos" data-dismiss=""  id="med_his_clos" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    <h4 class="modal-title">Complete Medication History</h4>
                </div>
            <div class="modal-body" id="" style="height:232px">
                <div class='col-md-12' style="margin-bottom:15px;">
                    <div class='col-md-4 pull-left'>
                        <input type="text" class="form-control" onkeyup="loadCompletePrescriptionHistory(1);"
                            id="serchCompletePrescription" placeholder="Search Medicine" />
                    </div>
                    </br>
                </div>
                <div class="col-md-12" id="complete_medication_history_data1"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default med_his_clos" data-dismiss="" aria-label="" id="med_his_clos">Close</button>
            </div>
        </div>
    </div>

</div>
