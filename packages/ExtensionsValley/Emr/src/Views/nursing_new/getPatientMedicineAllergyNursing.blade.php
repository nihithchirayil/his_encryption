<input type="hidden" id="patient_id" value="{{ $patient_id }}">
<div class="row padding_sm">
    <div class="col-md-12 padding_sm">
        <div class="col-md-6 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer emr_box" style="min-height: 450px;">
                    <div class="theadscroll" style="position: relative; height: 400px;">
                        <table id="patientMedicneAllergyTable"
                            class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed">
                            <thead>
                                <tr class="" style="background: #21a4f1; color: white; ">
                                    <th colspan="4">Medicine Allergy</th>
                                </tr>
                                <tr class="" style="background: #21a4f1; color: white; ">
                                    <th width="5%">Sl.No</th>
                                    <th width="20%">Search By</th>
                                    <th width="70%">Generic/Brand Name </th>
                                    <th width="5%"><i onclick="addMedicneAllergy()" class="fa fa-plus"></i></th>
                                </tr>
                            </thead>
                            <tbody class="patientMedicneAllergytddata">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 padding_sm">
            <div class="box no-border no-margin">
                <div class="box-footer emr_box" style="min-height: 450px;">
                    <div class="theadscroll" style="position: relative; height: 400px;">
                        <table id="patientOtherAllergyTable"
                            class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed">
                            <thead>
                                <tr class="" style="background: #21a4f1; color: white; ">
                                    <th colspan="3">Other Allergy</th>
                                </tr>
                                <tr class="" style="background: #21a4f1; color: white; ">
                                    <th width="5%">Sl.No</th>
                                    <th width="90%">Allergy Description </th>
                                    <th width="5%"><i onclick="addOtherAllergy()" class="fa fa-plus"></i></th>
                                </tr>
                            </thead>
                            <tbody class="patientOtherAllergytddata">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
