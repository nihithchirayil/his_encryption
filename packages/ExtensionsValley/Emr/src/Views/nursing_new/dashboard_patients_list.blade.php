<style>
    /* .zoom-effect {
        overflow: hidden;

    }

    .zoom-effect:hover {
        transform: scale(1.3);
        z-index: 99999;
        transition: all .3s ease-out .25s;
         transform: translate(0px,0px) scale(2);
        background-color: white;

    } */


    .small_bounce:hover {
        scale: 1.5 !important;
    }


    .btn-bounce {
        animation: bounce .3s infinite alternate;
    }

    @keyframes bounce {
        to {
            transform: scale(1.2);
        }
    }
</style>
<div class="col-md-12" style="text-align: center;">
    <h4 style="color:black;">IP Patients List</h4>
</div>
@php
$i = 0;
@endphp
@if(sizeof($dashboard_data)>0)
@foreach ($dashboard_data as $data)
@php
$transfer_requested_status = 0;
$i++;
@endphp
@if($nursing_station_id == $data->transfer_requested_station)
@php
$transfer_requested_status = 1;
@endphp
@endif

<div class="col-md-3 ns_patient_container_main no-padding"
    data-attr-patient="{{strtolower(str_replace(' ', '',$data->patient_name))}}"
    data-attr-uhid="{{strtolower(str_replace(' ', '',$data->uhid))}}"
    data-attr-room_type="{{$data->room_type_id}}">
    <div class="col-md-12 card-body ns_patient_container no-padding zoom-effect"
        onclick='gotoNsPatientDashboard("{{ htmlentities($data->patient_id) }}")'>
        <div class="col-md-12 no-padding">
            <div class="col-md-9 no-padding">
                <label class="name-label">{{$data->title}} {{ucfirst(strtolower($data->patient_name))}}</label>
            </div>
            <div class="col-md-3 no-padding pull-right">
                <label>{{$data->age}}/{{$data->gender}}</label>
            </div>
        </div>
        <div class="col-md-12 no-padding ">
            <div class="col-md-3 no-padding">
                <label>Uhid : </label>
            </div>
            <div class="col-md-9 no-padding pull-right">
                <label>{{$data->uhid}}</label>
            </div>
        </div>
        <div class="col-md-12 no-padding">
            <div class="col-md-3 no-padding">
                <label>Doctor : </label>
            </div>
            <div class="col-md-9 no-padding pull-right doctor-label" title="{{$data->doctor_name}}">
                <label>{{$data->doctor_name}}</label>
            </div>
        </div>
        <div class="col-md-12 no-padding" data-uhid="{{$data->uhid}}"
            data-patient="{{ucfirst(strtolower($data->patient_name))}}">
            <i class="fa fa fa-sticky-note-o small_bounce" style="color:rgb(97, 99, 233);
            font-size: 16px !important;" title="Doctor Notes" id="btn_private_notes"
                   onclick="btn_private_notes('{{$data->patient_id}}','{{$data->visit_id}}',this);" data-uhid="{{$data->uhid}}" data-patient="{{ucfirst(strtolower($data->patient_name))}}"></i>
                <i class="fa fa-hospital-o" style="color: teal;
                    font-size: 16px !important;" title="POC"
                    onclick="ViewPatientInvestigationResult('{{$data->patient_id}}','{{$data->visit_id}}','{{$data->doctor_id}}');"></i>

            @if($data->patient_ip_status == 2 && $transfer_requested_status == 0)
                <i class="fa fa fa-bed small_bounce btn-bounce bed_remove" style="color:green;
             font-size: 16px !important;" title="Occupied"
                    onclick="patient_occupi('{{$data->bed_id}}','{{$data->visit_id}}',this);"></i>
            @endif

            @if($data->bed_id != '' && $data->bed_retain_status == 0 )
                @if($data->patient_ip_status != 6)
                <i class="fa fa-plus small_bounce" title="Add vitals" aria-hidden="true"
                        onclick="clear_vital_vlues(this,'{{$data->patient_id}}','{{$data->visit_id}}','{{$data->doctor_id}}');"
                        data-toggle="modal" id="add_vital_modal_btn"
                        style="color:rgb(0, 255, 68);font-size: 16px !important;">
                </i>
                @if($data->is_retain == 0)
                    <i class="fa fa fa-bed small_bounce" style="color:rgb(128, 98, 0);
                    font-size: 16px !important;" title="Bed transfer" onclick="BedTranferRequest('{{$data->patient_id}}','{{$data->visit_id}}','{{ucfirst(strtolower($data->patient_name))}}','{{$data->uhid}}','{{$data->bed}}');"></i>
                @else
                    <i class="fa fa fa-bed small_bounce" style="color:rgb(68, 109, 255);
                    font-size: 16px !important;" title="Back to Retain"
                    onclick="backToRetain('{{$data->visit_id}}','{{$data->bed_id}}','{{$user_id}}');"
                    ></i>
                @endif

                    <i class="fa fa fa-medkit" style="color: teal;
                        font-size: 16px !important;" title="Prescription"
                        onclick="ViewPatientMedicationMin(this,'{{$data->patient_id}}','{{$data->visit_id}}');"></i>
                @endif
                    
                    <i class="fa fa-undo small_bounce" aria-hidden="true" style="font-size: 17px !important;margin-top: 4px !important;color:teal;" data-patient_id="{{ $data->patient_id }}" title="Medication Return" onclick="viewPatientMedicationReturn(this, '{{$data->patient_id}}','{{$data->visit_id}}')"></i>

                @if($data->patient_ip_status != 6)
                    <i class="fa fa fa-search" style="color:rgb(64, 0, 128);
                        font-size: 16px !important;" title="Investigation"
                        onclick="ViewPatientInvestigation(this,'{{$data->patient_id}}','{{$data->visit_id}}','{{$data->doctor_id}}');"></i>

                    <i class="fa fa fa-list" style="color:purple;
                        font-size: 16px !important;" title="Clinical Assesment"
                        onclick="ViewPatientAssesment(this,'{{$data->patient_id}}','{{$data->visit_id}}','{{$data->doctor_id}}');"></i>
                    @if(isset($enable_yellow_sheet) && ($enable_yellow_sheet == 1))
                    <i class="fa fa-th-list small_bounce" title="Nursing Procedures List" aria-hidden="true"
                        onclick="getYellowSheetServices('{{$data->visit_id}}');" data-toggle="modal"
                        id="add_vital_modal_btn" style="color:rgb(255, 255, 0);font-size: 16px !important;">
                    </i>
                    @endif
                @endif

            @endif

                <i class="fa fa fa-book pull-left small_bounce" style="color:purple;
                    font-size: 16px !important;" title="Patient History"
                    onclick="ViewPatientHistory(this,'{{$data->patient_id}}','{{$data->visit_id}}','{{$data->doctor_id}}');">
                </i>
                <i class="fa fa-line-chart small_bounce" title="Show Vitals" aria-hidden="true"
                    onclick="show_vital_history(this,'{{$data->patient_id}}','{{$data->visit_id}}','{{$data->doctor_id}}');"
                    style="color:rgb(0, 255, 68);font-size: 16px !important;" data-toggle="modal"
                    data-target="#vital_graph_modal">
                </i>

                <i class="fa fa-flask small_bounce" aria-hidden="true" style="color:blue;font-size: 16px !important;" title="View Lab Result" onclick="LabResultsTrendView('{{$data->patient_id}}','{{$data->visit_id}}');"></i>

                <i class="fa fa-plus-circle btn-blue-sm addAllergies"
                    style="font-size: 17px !important;margin-top: 4px !important;"
                    data-patient_id="{{ $data->patient_id }}" title="Allergies"
                    onclick="getPatientMedicineAlergy(this,'{{$data->patient_id}}','{{$data->visit_id}}')">
                </i>

                <i class="fa fa-list small_bounce" aria-hidden="true" style="font-size: 17px !important;margin-top: 4px !important;color:orange;" data-patient_id="{{$data->patient_id}}" title="Patient Intend History" onclick="getPatientIntendHistory('{{$data->patient_id}}')"></i>




            @php
            $DischargeInitiateNotDone="display: none";
            $DischargeInitiateSendForBilling="display: none";
            $DischargeInitiateRevertSendForBilling="display: none";
            $DischargeInitiateDone="display: none";
            @endphp
            @if(intval($data->patient_ip_status)==3)
            @php
            $DischargeInitiateDone="";
            @endphp
            @elseif(intval($data->patient_ip_status)==5)
            @php
            $DischargeInitiateSendForBilling="";
            $DischargeInitiateNotDone="";
            @endphp
            @elseif(intval($data->patient_ip_status)==6)
            @php
            $DischargeInitiateRevertSendForBilling='';
            @endphp
            @endif

            <div class="col-md-2 padding_sm pull-right dischargeInitiateDiv{{$data->visit_id}}"
                id="DischargeInitiateDiv1{{$data->visit_id}}" style="{{ $DischargeInitiateDone }}">
                <input type="hidden" id="dischargeIntimationRemarks{{$data->visit_id}}"
                    value="{{ trim($data->discharge_remarks) }}">
                <i class="fa fa-dashcube small_bounce" title="Discharge Intimation" aria-hidden="true"
                    onclick="dischargeIntimation('{{$data->visit_id}}',1,'fa fa-dashcube');"
                    id="DischargeInitiateData1{{$data->visit_id}}"
                    style="color:rgb(248, 166, 13);font-size: 16px !important;">
                </i>
            </div>
            <div class="col-md-2 padding_sm pull-right dischargeInitiateDiv{{$data->visit_id}}"
                id="DischargeInitiateDiv2{{$data->visit_id}}" style="{{ $DischargeInitiateNotDone }}">
                <i class="fa fa-retweet small_bounce" title="Revert Discharge Intimation" aria-hidden="true"
                    onclick="dischargeIntimation('{{$data->visit_id}}',2,'fa fa-retweet');"
                    id="DischargeInitiateData2{{$data->visit_id}}"
                    style="color:rgb(248, 67, 6);font-size: 16px !important;">
                </i>
            </div>
            <div class="col-md-2 padding_sm pull-right dischargeInitiateDiv{{$data->visit_id}}"
                id="DischargeInitiateDiv3{{$data->visit_id}}" style="{{ $DischargeInitiateSendForBilling }}">
                <i class="fa fa-share-square small_bounce" title="Send For Billing" aria-hidden="true"
                    onclick="dischargeIntimation('{{$data->visit_id}}',3,'fa fa-share-square');"
                    id="DischargeInitiateData3{{$data->visit_id}}"
                    style="color:rgb(5, 206, 251);font-size: 16px !important;">
                </i>
            </div>
            <div class="col-md-2 padding_sm pull-right dischargeInitiateDiv{{$data->visit_id}}"
                id="DischargeInitiateDiv4{{$data->visit_id}}" style="{{ $DischargeInitiateRevertSendForBilling }}">
                <i class="fa fa-recycle small_bounce" title="Revert Send For Billing" aria-hidden="true"
                    onclick="dischargeIntimation('{{$data->visit_id}}',4,'fa fa-recycle');"
                    id="DischargeInitiateData4{{$data->visit_id}}"
                    style="color:rgb(251, 9, 5);font-size: 16px !important;">
                </i>
            </div>

        </div>

    </div>

    <div class="col-md-12 ns_patient_container_sub no-padding"  @if($data->patient_id =='')style="margin-top:10px !important;"@endif>
        @if($data->bed !='')
            @php
                $bg_color = $data->bed_status_color;
                $font_color = $data->bed_status_font_color;
            @endphp
        {{-- <div class="col-md-8 room_label no-padding" style="background-color:{{$bg_color}};color:{{$font_color}}" data-attr-bed-status='{{$data->bed_status}}'>
            <label>{{$data->bed}}</label>
        </div> --}}
        <div title="{{$data->bed_status_name}}" class="col-md-8 room_label no-padding" data-attr-bed-status='{{$data->bed_status}}'>
            <label><i style="color:{{$bg_color}};font-size: 18px;" class="fa fa-bed"></i> {{$data->bed}}</label>
        </div>
        @endif
        @if($data->patient_id !='')
            <div class="col-md-6 no-padding">
                @if($data->patient_id !='')
                <a tabindex="0" class="btn btn-sm bg-gold color-white small_bounce"
                    data-toggle="popover"
                    data-trigger="focus"
                    role="button"
                    title="More Information"
                    style="margin:2px 0px 0px 3px !important;"
                    data-content="<table>
                            <tr>
                                <td>Admission no</td>
                                <td>:</td>
                                <td>{{$data->ip_no}}</td>
                            </<tr>
                            <tr>
                                <td>Room Type</td>
                                <td>:</td>
                                <td>{{ucfirst(strtolower($data->room_type_name))}}</td>
                            </<tr>
                            <tr>
                                <td>Admitted on</td>
                                <td>:</td>
                                <td>{{date('M-d-Y h:i A',strtotime($data->admission_date))}}</td>
                            </<tr>
                            <tr>
                                <td>Area</td>
                                <td>:</td>
                                <td>{{ucfirst(strtolower($data->area))}}</td>
                            </<tr>
                            <tr>
                                <td>Phone</td>
                                <td>:</td>
                                <td>{{$data->phone}}</td>
                            </<tr>
                        </table>"
                    >
                    <i class="fa fa-info-circle" title="More Information" aria-hidden="true"></i>
                </a>
                @endif
                @if($data->bed_id == '')
                    <button class="btn btn-sm bg-red color-white pull-right btn-bounce"  onclick="selectRoomRequestBed('{{$data->room_transfer_request_id}}',{{$data->patient_id}},'{{$data->visit_id}}','{{$data->transfer_requested_station}}','{{$data->transfer_request_retain_status}}');">
                        <i class="fa fa-bed" title="Accept Admission" aria-hidden="true" style="padding:4px 6px;"></i>
                    </button>
                @endif
            </div>
        @endif
    </div>

</div>
@if($i==4)
 @php
     $i=0;
     echo "<div class='clearfix'></div>";
 @endphp
@endif
@endforeach
@else
<div class="col-md-12">
    <h5>No Patients Found</h5>
</div>
@endif