@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/nursing_new/css/view_patient.css?version='.env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/nursing_new/css/multi_tab_setup.css?version='.env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/common_controls.css?version='.env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">

<link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">

<style>
 .small_bounce:hover {
        scale: 1.5 !important;
    }
</style>

@endsection
@section('content-area')

<div class="modal fade" id="addItemtoFavouriteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 800px;width: 100%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="addItemtoFavouriteModelHeader">NA</h4>
            </div>
            <div class="modal-body" style="min-height: 300px" id="addItemtoFavouriteModelDiv">
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" type="button" onclick="savePrescriptionFavorites()"
                    class="btn btn-brown">Save <i class="fa fa-save"></i></button>
            </div>
        </div>
    </div>
</div>
<div class="right_col" role="main">
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="emr_changes" value="{{ @$emr_changes ? $emr_changes : ''}}">
    <input type="hidden" id="doctor_id" value="{{ $doctor_id }}">
    <input type="hidden" id="patient_id" value="{{$patient_id}}">
    <input type="hidden" id="visit_id" value="{{$visit_id}}">
    <input type="hidden" id="encounter_id" value="{{$encounter_id}}">
    <input type="hidden" id="visit_type" value="{{$visit_type}}">

    @include('Emr::nursing_new.patient_visit_master')

</div>
{{-- modal boxes --}}
@include('Emr::nursing_new.patient_master_modals')

@stop
@section('javascript_extra')

<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/nursing_new/js/view_patient_dashboard.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/allergy_vitals_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/multi_tab_setup.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/investigation_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/accounts/default/javascript/jquery-ui.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
