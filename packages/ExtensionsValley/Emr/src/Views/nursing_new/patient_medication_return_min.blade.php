<link href="{{ asset('packages/extensionsvalley/master/default/css/new_emr.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}" rel="stylesheet">

<style>
    #medicine_list tbody tr:nth-child(even) {
        background-color: #f2f2f2;
        cursor: pointer;
        font-size: 12px;
    }

    #medicine_list tbody tr:nth-child(odd) {
        background-color: #ffffff;
        cursor: pointer;
        font-size: 12px;
    }

    #medicine_list tbody tr:hover {
        transform: translateY(-5px);
    }

    #medicine_list tbody tr {
        transition: transform 0.2s ease;
    }

    .med_clip {
        font-size: 12px;
        border-bottom: 1px solid #ccc;
        padding-bottom: 3px !important;
        margin-bottom: 2px;
    }

    .med_clip .medicine_name {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: 100%;
        margin-top: 5px;
    }

    #medication_item_list tr.selected {
        /* border: 2px solid green !important; */
        background: #b5efb5 !important;
    }

    #return_medicine_items .remove_icon {
        font-size: 16px;
        margin-top: 3px;
        cursor: pointer;
        color: red;
        font-weight: 700;
    }

    .return_history_pages .pagination {
        margin: 0px !important;
    }
</style>

<input type="hidden" id="ret_patient_id" value="{{$patient_id}}" />
<input type="hidden" id="ret_visit_id" value="{{$visit_id}}" />
<input type="hidden" id="ret_encounter_id" value="{{$encounter_id}}" />
<input type="hidden" id="ret_admitting_doctor_id" value="{{$admitting_doctor_id}}" />

<div class="col-md-12 no-padding">
    <div class="col-md-12" style="padding: 2px;">
        <div class="card">
            <div class="col-md-12" style="padding: 5px;">
                <div class="col-md-12 padding_sm pull-right" style="padding-right: 15px !important;">
                    <button style="width: 110px !important;float: right;height: 25px;" class="btn btn-success" onclick="saveMedicationReturn(this);"><i class="fa fa-save" aria-hidden="true"></i> Save</button>
                    <button style="width: 110px !important;float: right;height: 25px;" class="btn bg-info presc_history_btn" onclick="loadMedicationReturnHistory(this);" data-toggle="collapse" data-target="#returnCollapse"><i class="fa fa-history"></i>
                        History</button>
                    <button style="width: 40px !important;float: right;height: 25px;font-size: 14px;" class="btn bg-yellow presc_history_btn" onclick="patientMedicationReturnDetails();" title="Reload Medicines"><i class="fa fa-refresh"></i></button>
                </div>

                <!-- Return History View -->
                <div class="col-md-12 no-padding">
                    <div class="card collapse" id="returnCollapse">
                        <div class="card_body" id="return-history-data-list" style="height: 200px;">

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 no-padding" style="padding: 5px;">
                <div class="col-md-8" style="padding: 5px;">
                    <div class="col-md-12 box-body" style="padding: 6px !important;">
                        <div class="col-md-12 no-padding theadscroll" id="medication_item_list_div" style="height: 73vh;">
                            <table class="table no-margin no-border" id="medicine_list">
                                <thead style="top: 0; position: sticky;">
                                    <tr class="bg-default" style="background-color:#21a4f1;">
                                        <th width="5%" style="padding-bottom:5px !important;">Sl.No</th>
                                        <th width="30%" style="padding-bottom:5px !important;">
                                            <div class="col-md-12 no-padding">
                                                <span class="col-md-4 padding_sm" style="margin-top: 10px;font-size: 13px;">Medicine</span>
                                                <span class="col-md-6 no-padding"> <input type="text" class="form-control search-input-box" style="display: none; width: 100%; margin-top: 5px; font-weight: 400;"> </span>
                                                <span class="col-md-2 no-padding"> <i class="fa fa-search btn btn-success search-medicine-btn" style="height: 20px; margin: 6px 0 0 3px !important; font-size: 13px;"></i> </span>
                                            </div>
                                        </th>
                                        <th width="10%" style="padding-bottom:5px !important;">Batch</th>
                                        <th width="18%" style="padding-bottom:5px !important;">Bill Number</th>
                                        <th width="8%" style="padding-bottom:5px !important;">Quantity</th>
                                        <th width="8%" style="padding-bottom:5px !important;">Remaining Quantity</th>
                                        <th width="11%" style="padding-bottom:5px !important;">Created On</th>
                                    </tr>

                                </thead>
                                <tbody id="medication_item_list">

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="padding: 5px;">
                    <div class="col-md-12 box-body" style="height: 75vh;" id="medication_return_items">
                        <label><b>Selected Medicines</b></label>
                        <div class="clearfix"></div>
                        <div class="col-md-12 no-padding theadscroll" style="height: 60vh; margin-top: 10px;" id="return_medicine_items">

                        </div>
                        <div class="col-md-12" style="height: 10vh;">
                            <input type="hidden" id="edit_return_id">
                            <textarea id="return_remarks" class="form-control" placeholder="Remarks" style="height: 50px !important;"></textarea>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>