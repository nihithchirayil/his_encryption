    <div class="col-md-3 padding_sm patient_details_div">
        <div class="col-md-12 padding_sm patient_details_inner_div css-normal-box-shadow">
            <div class="patient_details_patient_name css-normal-label">{{$visit_data[0]->patient_name}}
                    <i id="poplink" class="fa fa-info-circle patient_details_info_btn color-yellow" aria-hidden="true"></i>
            </div>
            <div class="patient_details_patient_uhid css-normal-text">
                {{$visit_data[0]->uhid}}
            </div>
            <div class="patient_details_patient_age_gender css-normal-text">
                {{$visit_data[0]->age}}/{{$visit_data[0]->gender}}
            </div>
        </div>

        <div class="col-md-12 padding_sm">
            <div class="col-md-2 no-padding">
                <div class="left_side_tools_container1 css-normal-box-shadow">
                    <a class="expanding_button patient_clinical_history_btn">
                        <span class="expanding_button_icon"><i class="fa fa-history"></i></span>
                        <span class="expanding_button_text">History</span>
                    </a>
                    <a href="#" class="expanding_button btn-bg-orange patient_special_notes_btn">
                        <span class="expanding_button_icon"><i class="fa fa-book"></i></span>
                        <span class="expanding_button_text">Special Notes</span>
                    </a>
                    <a class="expanding_button patient_lab_results_btn btn-bg-green">
                        <span class="expanding_button_icon"><i class="fa fa-flask"></i></span>
                        <span class="expanding_button_text">Lab Results</span>
                    </a>
                    <a href="#" class="expanding_button btn-bg-red patient_radiology_results_btn">
                        <span class="expanding_button_icon"><i class="fa fa-camera"></i></span>
                        <span class="expanding_button_text">Radiology Results</span>
                    </a>
                    <a href="#" class="expanding_button btn-bg-teal patient_discharge_summary_list_btn">
                        <span class="expanding_button_icon"><i class="fa fa-list"></i></span>
                        <span class="expanding_button_text">Discharge Summary</span>
                    </a>
                </div>
                <div class="left_side_tools_container2 css-normal-box-shadow">

                    <a href="#" class="expanding_button">
                        <span class="expanding_button_icon"><i class="fa fa-archive"></i></span>
                        <span class="expanding_button_text">Documents</span>
                    </a>
                    <a href="#" class="expanding_button btn-bg-orange patient_discharge_summary_create_btn">
                        <span class="expanding_button_icon"><i class="fa fa-plus"></i></span>
                        <span class="expanding_button_text">Create Summary</span>
                    </a>

                    <a href="#" class="expanding_button btn-bg-green">
                        <span class="expanding_button_icon"><i class="fa fa-medkit"></i></span>
                        <span class="expanding_button_text">Drug Reaction</span>
                    </a>
                    <a href="#" class="expanding_button btn-bg-red">
                        <span class="expanding_button_icon"><i class="fa fa-bed"></i></span>
                        <span class="expanding_button_text">Room Management</span>
                    </a>
                    <a href="#" class="expanding_button btn-bg-red">
                        <span class="expanding_button_icon"><i class="fa fa-user"></i></span>
                        <span class="expanding_button_text">Death Marking</span>
                    </a>
                </div>
            </div>
            <div class="col-md-10 no-padding">
                <div class="patient_demographics_container css-normal-box-shadow">
                    <div class="col-md-12 padding_sm allergy_and_notes_div">
                        <div class="col-md-12 no-padding allergy_div" style="height: 100px;">
                            <div class="text_head">Allergies <i class="pull-right fa fa-plus-circle btn-common-sm addAllergies"></i></div>
                            <div class="clearfix"></div>
                            <div class="allergy_list theadscroll">

                            </div>

                        </div>
                        <div class="col-md-12 no-padding notes_div" style="height: 100px;">
                            <div class="text_head">Personal Notes</div>
                            <div class="clearfix"></div>
                            <div class="private_notes_area theadscroll lined" contenteditable="true" style="height:100px;position:absolute;">{{$patient_personal_notes}}

                                {{-- <textarea class="form-control lined" rows="5" style="height: 90px !important; width: 180px;">
                                </textarea> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm vitals_container">
                        <div class="text_head">Vitals <i class="pull-right fa fa-plus-circle btn-common-sm addVitals"></i></div>
                        <div class="clearfix"></div>
                        <div class="vital_list">
                            <table class="table no-margin no-border theadfix_wrapper">
                                <tbody class="vital_list_table_body">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 pop-content">
        @include('Emr::nursing_new.patient_info')
    </div>
    <div class="col-md-9 padding_sm visit_details_div css-normal-box-shadow">
        <div class="col-md-12 padding_sm visit_details_inner_div">

            <div class="tab_wrapper first_tab _side">
                <ul class="tab_list">
                    <li rel="tab_1" class="active">
                        <i class="fa fa-book" aria-hidden="true"></i>
                        Notes
                    </li>
                    <li rel="tab_2" class="">
                        <i class="fa fa-flask" aria-hidden="true"></i>
                        Investigation
                    </li>
                    <li rel="tab_3" class="">
                        <i class="fa fa-medkit"></i>
                        Prescription
                    </li>
                    <li rel="tab_3" class="">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        Drug Administration
                    </li>
                    <li rel="tab_3" class="">
                        <i class="fa fa-flask" aria-hidden="true"></i>
                        I.V fluid Chart
                    </li>
                    <li rel="tab_3" class="">
                        <i class="fa fa-undo" aria-hidden="true"></i>
                        Medication Return
                    </li>
                    <li rel="tab_3" class="">
                        <i class="fa fa-arrows-v" aria-hidden="true"></i>
                        Intake-Output
                    </li>
                </ul>

                <div class="content_wrapper">
                    <div class="tab_content tab_1">
                        @include('Emr::view_patient.notes_template')
                    </div>

                    <div class="tab_content tab_2">
                        @include('Emr::view_patient.investigation_template')
                    </div>

                    <div class="tab_content tab_3">
                        @include('Emr::view_patient.prescription_template')
                    </div>
                </div>
            </div>

            <div class="saveButtonDiv">
                <button class="btn btn-sm saveClinicalDataButton" type="button" ><i class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
