@extends('Emr::emr.page')
@section('content-header')
    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@endsection
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/view_patient/css/view_patient.css?version='.env('APP_JS_VERSION', '0.0.1')) }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/view_patient/css/multi_tab_setup.css?version='.env('APP_JS_VERSION', '0.0.1')) }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/view_patient/css/common_controls.css?version='.env('APP_JS_VERSION', '0.0.1')) }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/nursing_new/css/nursing_station.css?version='.env('APP_JS_VERSION', '0.0.1')) }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/nursing_new/css/color_new.css?version='.env('APP_JS_VERSION', '0.0.1')) }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">

    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">

    <link
    href="{{ asset('packages/extensionsvalley/master/default/css/new_emr.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
@endsection
@section('content-area')
<style>
    .close_btn_instruction_search {
    position: absolute;
    z-index: 99;
    color: #FFF !important;
    background: #000;
    right: -11px;
    top: -1px;
    border-radius: 100%;
    text-align: center;
    width: 20px;
    height: 20px;
    line-height: 20px;
    cursor: pointer;
}

    .btn_refresh_dashboard.selected_btn {
        background: green !important;
    }

    .leaf_border {
        border-radius: 7px 0px 7px 0px;
    }

    .medicine-list-div-row-listing-new {
        top: 22%;
        right: 10%;
        z-index: 9999;
        position: absolute;
        background: #FFF;
        box-shadow: 0 0 6px #ccc;
        padding: 1px;
        width: 1000px;
    }
    .list-medicine-search-data-row-listing-new {
        text-align: left;
    }

    .clone_new_group input.bootbox-input-text {
        border: 1px solid #ccc !important;
        height: 30px !important;
        margin-top: 10px !important;
    }
    #lab_result_view_content{
    border: 1px solid #21a4f15e;
    min-height: 58vh;
    padding: 0px !important; 
    }
    .lab-resutl-width .modal-dialog{
        width: 65%;
    height:80%;
    margin: 0;
    padding: 0;
    margin-left:20%;
    margin-top:2%;
    }
.lab-resutl-width .modal-content {
    height: auto;
    min-height: 100%;
    border: 0 none;
    border-radius: 0;
    box-shadow: none;
}
.select2-container{
    margin-top:-11px !important;
}
.give-border-td {
        border: 1px solid #ddd;
        padding: 5px;
    }

    /* Style for the "Not Uploaded" cell */
    .not-uploaded-td {
        text-align: center;
        background-color: #f2f2f2;
        color: #777;
    }
    .result-tr:hover {
        background-color: #f0f0f0; /* Change this to your desired hover color */
    }
</style>
<input type="hidden" id="base_url" value="{{ URL::to('/') }}">
<input type="hidden" id="c_token" value="{{ csrf_token() }}">
<input type="hidden" id="default_nursing_station" value="{{$default_nursing_station}}">
<input type="hidden" id="casuality_station_location" value="{{$casuality_station_location}}">
<input type="hidden" id="nursing_op_list_enabled" value="{{$op_list_enabled}}">
<input type="hidden" id="is_casuality_user" value="{{$is_casuality_user}}">
<input type="hidden" id="casuality_investigation_indent_type" value="{{$casuality_investigation_indent_type}}">
<input type="hidden" id="casuality_prescription_indent_type" value="{{$casuality_prescription_indent_type}}">
<input type="hidden" id="patient_id"/>
<input type="hidden" id="lab_result_patient" value="">
<input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">



</div>
    <div class="right_col" role="main" style="height: 100vh;">
        <div class="col-md-12">
            {{-- left side bar --}}
            <div class="col-md-3">
                <div class="col-md-12 no-padding ns_left_container card-body bg-custom-blue container1">
                    @include('Emr::nursing_new.ns_dashboard_left_side')
                </div>
                <div class="col-md-12 no-padding ns_left_container card-body bg-custom-blue container2">
                    @include('Emr::nursing_new.ns_dashboard_to_do_list')
                </div>
            </div>
            {{-- Main content area --}}
            <div class="col-md-9">
                @include('Emr::nursing_new.ns_dashboard_main_content')
            </div>
        </div>
    </div>
    {{-- modal boxes --}}
    @include('Emr::nursing_new.view_patient_modals')
    <div class="modal fade" id="patientLabResults" tabindex="-1" role="dialog" style="display:none;">
        <div class="modal-dialog" style="width: 88%">
            <div class="modal-content">
                <div class="modal-header modal_box" style="height:37px;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Lab Results</h4>
                </div>
                <div class="modal-body" style="min-height: 450px;" id="patientLabResultsBody">
                    <div class="row" id="lab_restuls_data">
                        <div class='tab_wrapper first_tab _side'>
                            <ul class='tab_list'>
                                <li rel='tab_1' class='remove_active active'>
                                    <a class="nav-link" data-toggle="tab" href="#showResultviewtab1" role="tab"
                                    aria-controls="showResultviewtab1" aria-selected="false" aria-expanded="true">
                                    Lab Results</a>
                                    
                                </li>
                                @if ($outside_lab_result_view_btn==1)
                                <li rel='tab_2' class='remove_active'>
                                    <a class="nav-link" data-toggle="tab" href="#showResultviewtab2" role="tab"
                                    aria-controls="showResultviewtab2" aria-selected="false" aria-expanded="true">
                                    Uploaded Results</a>                              
                                </li>
                                @endif
                            </ul>
                        </div>  
                        <div class="content_wrapper">
                            <div class="tab_content tab-content1 tab_1" id="showResultviewtab1" style="display: block;">
                                
                                {{-- @include('Emr::emr.lab.ajax_lab_results') --}}
                            </div>
                            <div class="tab_content tab-content1 tab_2" id="showResultviewtab2" style="display: none;">
                                <div class="col-md-12" style="margin-bottom: 5px;">
    
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="" style="font-size: 12px !important;">From date</label>
                                            <div class="clearfix"></div>
                                            <input type="text" onblur="showResult();" value="{{ date('M-d-Y') }}" id='lab_result_fromdate_t' name="lab_result_fromdate" class="form-control datepicker" data-attr="date" placeholder="Date">
                                        </div>
                                    </div>
                                
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box">
                                            <label for="" style="font-size: 12px !important;">To date</label>
                                            <div class="clearfix"></div>
                                            <input type="text" onblur="showResult();" value="{{ date('M-d-Y') }}" id='lab_result_todate_t' name="lab_result_todate" class="form-control datepicker" data-attr="date" placeholder="Date">
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <div class="mate-input-box" style="height: 43px">
                                           <select name="" onchange="showResult()" id="report_type" class="form-control select2">
                                            <option value="">Select Type</option>
                                            @foreach ($report_type as $data)
                                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                            @endforeach
                                           </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top: 10px;">
                                        <input type="checkbox"  id="based_on_uploaded_date" value="" onchange="showResult()">
                                        <label for="based_on_uploaded_date" style="font-weight: 700">Based On Uploaded Date</label>
                                    </div>
                                    <div id="showResultviewtab3">
                                    
                                    </div>
                                
                                  </div>
                                  
                                {{-- @include('Emr::nursing_new.ajax_patient_resluts_view') --}}
                            </div>
        
                           
        
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 10px;">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal bs-example-modal-lg mkModalFloat in" tabindex="-1" role="dialog" id="nursing_note_modal"
    data-backdrop="static" aria-hidden="false">
    <div class="modal-dialog modal-lg" style="">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-green">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="nursing_note_modal_title">Nursing Modal</h4>
            </div>

            <!-- Modal body -->
            <div id="nursing_note_modal_body">

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">

            </div>

        </div>
    </div>
</div>
<div class="modal bs-example-modal-lg mkModalFloat in" tabindex="-1" role="dialog" id="new_nursing_note_modal"
    data-backdrop="static" aria-hidden="false">
    <div class="modal-dialog modal-lg" style="">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-blue">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="new_nursing_note_modal_title"></h4>
            </div>

            <!-- Modal body -->
            <div id="new_nursing_note_modal_body">

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">

            </div>

        </div>
    </div>
</div>
@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/nursing_new/js/nursing_new.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/nursing_new/js/prescription_min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

    <script src="{{ asset('packages/extensionsvalley/nursing_new/js/clinical_min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

<script src="{{ asset('packages/extensionsvalley/nursing_new/js/vitals_min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>

    <script
        src="{{ asset('packages/extensionsvalley/accounts/default/javascript/jquery-ui.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

    <script src="{{ asset('packages/extensionsvalley/nursing_new/js/ns_patient_registration.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/nursing_new/js/ns_externel_patient_registration.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
    src="{{ asset('packages/extensionsvalley/emr/js/inv_result_entry.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

    <script
        src="{{ asset('packages/extensionsvalley/nursing_new/js/prescription_return_min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>


@endsection
