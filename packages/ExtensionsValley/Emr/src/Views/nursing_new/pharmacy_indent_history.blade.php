<div class="col-md-12" id="request_history" style="margin-bottom:35px !important;">
    <div class="row">
        <div class="col-md-12"> <label><h4>Pharmacy Indent History</h4></label></div>
    </div>
    <div class="theadscroll" style="position: relative;margin-bottom:30px;">
        @if(sizeof($data)>0)
        <table class="table table-striped theadfix_wrapper table-bordered table-condensed">
            <thead>
                <tr class="headergroupbg_blue">
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Intend Id</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Created On</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Created By</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Doctor</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">visit_status</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Billconverted Status</th>
                </tr>
            </thead>
            <tbody>
                @php
                $i = 1;
                @endphp
                @foreach($data as $res)
                <tr>
                    <td>{{$res->id}}</td>
                    <td>{{date('M-d-Y h:i A',strtotime($res->created_at))}}</td>
                    <td>{{$res->name}}</td>
                    <td>{{$res->doctor_name}}</td>
                    <td>{{$res->visit_status}}</td>
                    <td>@if($res->billconverted_status ==1)
                            Converted
                        @else
                        Pending
                        @endif
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <label>No Result Found!</label>
        @endif
    </div>
</div>
