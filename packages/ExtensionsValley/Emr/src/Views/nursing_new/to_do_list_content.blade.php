@if(!empty($task_list))
    @foreach($task_list as $data)
    <div class="col-md-12 to_do_content_list" id="to_do_content_list_{{$data->id}}">
        <div class="col-md-6 pull-right to-do-left-top no-padding">
            @if(date("Y-m-d")== date("Y-m-d",strtotime($data->created_at)))
                {{date('H:i A',strtotime($data->created_at))}}
            @else
                {{date('M-d-Y H:i A',strtotime($data->created_at))}}
            @endif
        </div>
        <div class="col-md-6 pull-left to-do-left-top text-left no-padding">
            @if($data->patient_name!='')
            Patient: {{ucfirst(strtolower($data->patient_name))}}
            @endif
        </div>
        <div class="col-md-12 no-padding to-do-description">
                {{$data->description}}
                <button type="button" onclick="updateToDoStatus('{{$data->id}}')" class="btn btn-sm bg-green btn-update-to-do pull-right">
                    <i class="fa fa-check" aria-hidden="true"></i>
                </button>
        </div>
    </div>
    @endforeach
@else
<label>No task pending!</label>
@endif
