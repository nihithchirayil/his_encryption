<!----------Modal box for add to do list------------------------------------->
<div class="modal modal-fullscreen" id="modal_add_to_do" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add task</h4>
            </div>
            <div class="modal-body" id="modal_add_to_do_content">
                <div class="col-md-12 to-do-input">

                    <label class="to_do_label">Nursing Station</label>
                    {!! Form::select('to_do_assigned_location',$ns_station_list,$default_nursing_station,['class' =>
                    'form-control select2', 'id' =>'to_do_assigned_location']) !!}
                </div>
                <div class="col-md-12 to-do-input">
                    <label class="to_do_label">Assigned to</label>
                    {!! Form::select('to_do_assigned_to',$nurses_list,null,['class' => 'form-control select2', 'id'
                    =>'to_do_assigned_to','placeholder'=>'Select Nurse']) !!}
                </div>
                <div class="col-md-12" style="height:120px;">
                    <label class="to_do_label">Description</label>
                    <textarea class="form-control" rows="5" id="to_do_description" name="to_do_description"></textarea>
                </div>
                <div class="col-md-12 to-do-input to-do-select-patient-container">
                    <label class="to_do_label">Select Patient</label>
                    <input class="form-control hidden_search" value="" autocomplete="off" type="text"
                        id="to_do_patient_uhid" name="to_do_patient_uhid">
                    <div id="to_do_patient_uhidAjaxDiv" class="ajaxSearchBox"></div>
                    <input class="filters" type="hidden" name="to_do_patient_uhid_hidden" value=""
                        id="to_do_patient_uhid_hidden">
                </div>
                <div class="col-md-12 to-do-input">
                    <input type="hidden" name="to_do_task_id" id="to_do_task_id" value="" />
                    <button type="button" class="btn bg-green pull-right" onclick="SaveToDo();">
                        <i class="fa fa-save" aria-hidden="true"></i> Save
                    </button>
                    <button type="button" class="btn bg-blue pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-----Modal box for history to do list---------------------------------------------->
<div class="modal fade modal-fullscreen-xs-down" id="modal_history_to_do" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Task list History</h4>
            </div>
            <div class="modal-body" id="modal_history_to_do_content">


            </div>
        </div>
    </div>
</div>

<!-----Modal box for intetnd history---------------------------------------------->
<div class="modal fade modal-fullscreen-xs-down" id="modal_indend_history" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Patient Indent History</h4>
            </div>
            <div class="modal-body" id="modal_indent_history_content">
                <input type="hidden" id="indent_history_patient_id"/>
                <div class="col-md-12">
                    <div class="col-md-3 padding_sm">
                    <div class="col-md-6 padding_sm">
                        <div class="mate-input-box">
                            <label for="">From Date</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" onblur="" class="form-control datepicker"
                                id="indent_history_from_date" value="{{date('M-d-Y')}}" name="indent_history_from_date"
                                value="" style="margin-top: 7px;">
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm ">
                        <div class="mate-input-box">
                            <label for="">To Date</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" onblur="" class="form-control datepicker"
                                id="indent_history_to_date" value="{{date('M-d-Y')}}" name="indent_history_to_date" value="" style="margin-top: 7px;">
                        </div>
                    </div>
                    </div>
                    <div class="col-md-2 padding_sm ">
                        @php
                               $location_list = \DB::table('location')->where('status',1)->orderBy('location_name')->pluck('location_name','id');
                        @endphp
                        <div class="mate-input-box">
                            <label for="">Location</label>
                            <div class="clearfix"></div>
                            {!! Form::select('indent_history_location', $location_list,null, ['class' => 'form-control', 'id' =>'indent_history_location','style'=>'margin-top:7px;','placeholder'=>'Location']) !!}
                        </div>
                    </div>
                    <div class="col-md-3 padding_sm ">
                        <div class="mate-input-box">
                            <label for="">Indent Type</label>
                            <div class="clearfix"></div>
                           <select class="form-control select2" multiple id="indent_history_type">
                            <option selected value="1">Prescription</option>
                            <option selected value="2">Investigation</option>
                            <option selected value="3">Procedure</option>
                           </select>
                        </div>
                    </div>
                    <div class="col-md-3 padding-sm">
                        <!-- <span class="badge" style="background:#fcec74;">&nbsp;&nbsp;&nbsp;</span> Pending -->
                        <div class="mate-input-box">
                            <label for="">Vist Date</label>
                            <div class="clearfix"></div>
                            <select class="form-control" id="indent_history_visit_id">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1 padding_sm pull-right" style="margin-top: 20px;">
                        <a class="btn btn-primary pull-right" onclick="fetchIndentHistory();">
                            Search
                        </a>
                    </div>
                </div>
                <div class="col-md-12" id="indent_history_result">

                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="dischargeintimationMessageModel" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel">
    <input type="hidden" id="dischargeintimationvisit_id" value="0">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Discharge Intimation</h4>
            </div>
            <div class="modal-body" id="dischargeintimationMessageModelDiv" style="min-height: 175px">
                <div class="col-md-12 padding_sm">
                    <textarea id="discharge_remarks" cols="5" rows="5" class="form-control"></textarea>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 padding_sm" style="margin-top: 15px;">
                    <button style="width:90px;" class="btn btn-success col-md-1  pull-right "
                        id="saveDischargeintimationBtn" onclick="saveDischargeintimation()"><i class="fa fa-save"
                            id="saveDischargeintimationSpin"></i>
                        Save</button>
                    <button style="width:90px;" type="button" class="btn btn-default col-md-1  pull-right"
                        data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- patient medication minified modal --}}
<div class="modal fade modal-fullscreen-xs-down" id="patient_medication_min" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="height:1093px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa fa-medkit"></i> Medication/Administration </h4>
            </div>
            <div class="modal-body" id="patient_medication_min_content_old">
                <div class="nav-tabs-custom blue_theme_tab no-margin box-body"
                    style="margin-left: -7px !important;width: 93vw;">
                    <ul class="nav nav-tabs sm_nav text-center no-border" style="background-color:white"
                        id="patient_medication_min_tab">
                        <li class="active"> <a style="padding: 4px 8px;" href="#tab_medication" data-toggle="tab"
                                aria-expanded="true" onclick="loadMedicationData();">Medication</a></li>
                        @if($administration_in_nurse_dashboard == '1')
                        <li class=""> <a style="padding: 4px 8px;" href="#tab_administration" data-toggle="tab"
                                aria-expanded="false" onclick="showMedicationChart();">Administration</a></li>
                        @endif
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab_medication">
                            <div class="bg-white clearfix" id="patient_medication_min_content"
                                style="border-top: 1px solid #ddd !important;">

                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_administration">
                            <div class="bg-white clearfix" style="border-top: 1px solid #ddd !important;">
                                <div class="row" style="margin-top: 20px;">
                                    <button class="btn btn-success" id="ns_medication_chart"
                                        onclick="gotoMedicationChart();"
                                        style="margin: 0px 20px 5px; width:120px; margin-left: 87%;"> <i
                                            class="fa fa-bar-chart" aria-hidden="true"></i> Medication Chart</button>
                                </div>
                                <div class="clearfix" id="medication_chart_content"
                                    style="min-height: 250px;margin-top: -16px;">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- patient assesment minified modal --}}
<div class="modal fade modal-fullscreen-xs-down" id="patient_assesment_min" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa fa-list"></i> Assesment </h4>
            </div>
            <div class="modal-body" id="patient_assesment_min_content">


            </div>
        </div>
    </div>
</div>
{{-- patient investigation minified modal --}}
<div class="modal fade modal-fullscreen-xs-down" id="patient_investigation_min" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="height:614px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa fa-search"></i> Investigation</h4>
            </div>
            <div class="modal-body" id="patient_investigation_min_content">


            </div>
        </div>
    </div>
</div>

<!----Frequency popup---------->

<div class="modal bs-example-modal-md mkModalFloat" id="frequencypopup" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header bg-green">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">FREQUENCY DETAILS</h4>
            </div>

            <div class="modal-body">
                <div class="row">

                    <div class="col-md-2 padding_sm">
                        <input type="text" name="frequency_name" class="form-control" id="frequency_name"
                            placeholder="Frequency">
                    </div>

                    <div class="col-md-2 padding_sm">
                        <input type="text" name="frequency_value" class="form-control" id="frequency_value"
                            placeholder="Value/day">
                    </div>

                    <div class="col-md-2 padding_sm">
                        <button type="button" class="btn btn-success btn-block"
                            onclick="save_frequency_details();">Save</button>
                    </div>

                </div>
                <hr>
                <div class="row">

                    <!---- Common frequency table ----->
                    <div class="col-md-6">
                        <h5 style="margin-left:10px;">Available List</h5>
                        <div class="col-md-12" style="margin-bottom: 15px;">
                            <input type="text" name="searchFreq" class="form-control" id="searchFreq"
                                placeholder="search..">
                        </div>
                        <div class=" theadscroll padding_sm" style="position: relative; height: 350px;">
                            <table id="common_freq_table" class="table table-striped theadfix_wrapper table-sm">
                                <thead>
                                    <tr style="background-color: #0f996b; color:white;padding:0px;">
                                        <th>Frequency Name</th>
                                        <th style="text-align:center;">Value</th>
                                    </tr>
                                </thead>
                                <tbody class="globalFreqList">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!---- Common frequency table end ----->



                    <!---- user frequency table ----->
                    <div class="col-md-6">
                        <h5 style="margin-left:10px;">Selected List</h5>

                        <div class="col-md-12" style="margin-bottom: 15px;">
                            <input type="text" name="searchSelectedFreq" class="form-control" id="searchSelectedFreq"
                                placeholder="search..">
                        </div>
                        <div class=" theadscroll padding_sm" style="position: relative; height: 350px;">
                            <table id="selected_freq_table" class="table table-striped theadfix_wrapper table-sm">
                                <thead>
                                    <tr style="background-color: #0f996b; color:white;padding:0px;">
                                        <th style="text-align: left;">Frequency Name</th>
                                        <th style="text-align: center;">Value</th>
                                        <th style="text-align: center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="globalSelectedFreqList">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!---- user frequency table ends ----->




                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<!----Frequency popup ends---------->

<div id="modalChangeMedicationTime" class="modal fade " role="dialog" style="z-index:9999">
    <div class="modal-dialog" style="width:35%">
        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Change Medication Time</h4>
            </div>
            <div class="modal-body" style="height:55px;">
                <input type="hidden" name="change_med_chart_id" id="change_med_chart_id" value='' />
                <div class="md-form col-md-5 no-padding">
                    <input type="text" value="" id="med_chart_change_date" name="med_chart_change_date"
                        class="form-control datepicker  timepickerclass" data-attr="date" placeholder="Date"
                        style="border-bottom: 1px solid lightgrey !important;">
                </div>

                <div class="md-form col-md-5">
                    <input type="time" name="med_chart_change_time" id="med_chart_change_time" value=""
                        class="form-control timepickerclass" style="border: 1px solid lightgrey !important;">
                    <label style="margin-top: 3px;" for="timepicker_"><b> </b></label>
                </div>
                <div class="col-md-2">
                    <button type="button" onclick="editGivenMedicationSave();" class="btn bg-primary"
                        id="btn_change_md_chart_time">
                        <i class="fa fa-save"></i> Save
                    </button>
                </div>

            </div>
        </div>

    </div>
</div>



<div id="modalInvIntendsStatus" class="modal fade " role="dialog" style="z-index:9999">
    <div class="modal-dialog" style="width:85%">
        <div class="modal-content" style="height:610px;">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;height:37px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Investigation intent status</h4>
            </div>
            <div class="modal-body" style="height:55px;">
                <div class="col-md-12">
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">From Date</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" onblur="" class="form-control datepicker"
                                id="inv_status_from_date" value="{{date('M-d-Y')}}" name="inv_status_from_date"
                                value="" style="    height: 30px !important;margin-top: 7px;">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm ">
                        <div class="mate-input-box">
                            <label for="">To Date</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" onblur="" class="form-control datepicker"
                                id="inv_status_to_date" value="{{date('M-d-Y')}}" name="inv_status_to_date" value="" style="    height: 30px !important;margin-top: 7px;">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm pull-right">
                        <button class="btn btn-primary pull-right" type="buttin" onclick="fetchInvStatus(1);">
                            Search
                        </button>
                    </div>
                </div>
                <div id="InvIntendsStatus" class="col-md-12">

                </div>

            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="editvitalModal" tabindex="-1" role="dialog" aria-labelledby="vitalModal" aria-hidden="true">
    <div class="modal-dialog" style="width:43%;">
        <!-- Modal content------>
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <h4 class="modal-title">Patient Vitals</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 vital_form_container">
                        <input type="hidden" value="" id="vital_batch">
                        <input type="hidden" id="vital_values" value="{{$vital_values}}">
                        <input type="hidden" value="" id="selected_patient_id">
                        <input type="hidden" value="" id="patient_visit_id">
                        <input type="hidden" value="" id="patient_encounter_id">
                        <input type="hidden" value="9" id="temperature">
                        <div>
                            @php
                            $weight = ExtensionsValley\Emr\VitalMaster::getVitalDetails('Weight');
                            $height = ExtensionsValley\Emr\VitalMaster::getVitalDetails('Height');
                            $temperature = ExtensionsValley\Emr\VitalMaster::getVitalDetails('Temperature');
                            @endphp
                            {!! Form::select('weight', $weight->toArray(),'', ['class' => 'form-control hidden','id' =>
                            'weight','onchange' => 'weightSelect(this)']) !!}
                            {{-- {!! Form::select('temperature', $temperature->toArray(),'', ['class' => 'form-control
                            hidden','id' => 'temperature','onchange' => 'temperatureSelect(this)']) !!} --}}
                            {!! Form::select('height', $height->toArray(),'', ['class' => 'form-control hidden','id' =>
                            'height','onchange' => 'heightSelect(this)']) !!}

                            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table">
                                <tr>
                                    <td><label for="">Weight in Kg</label></td>
                                    <td> <input type="text" class="form-control" name="weight_value" id="weight_value"
                                            value=""
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </td>

                                    <td><label for="">Height/Length in cm</label></td>
                                    <td><input type="text" class="form-control" name="height_value" id="height_value"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            value="">
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="">BMI Kg/M^2</label></td>
                                    <td><input type="text" class="form-control" name="bmi" id="bmi"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            value="">
                                    </td>
                                    <td><label for="">Oxygen Saturation %</label></td>
                                    <td><input type="text" class="form-control" name="oxygen" id="oxygen"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            value="">
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="">BP Systolic mmHg</label></td>
                                    <td><input type="text" value="" class="form-control" name="bp_systolic"
                                            id="bp_systolic"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </td>

                                    <td><label for="">BP Diastolic mmHg</label></td>
                                    <td><input type="text" class="form-control" name="bp_diastolic" id="bp_diastolic"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            value="">
                                    </td>

                                </tr>

                                <tr>

                                    <td><label for="">Temperature in C</label></td>
                                    <td><input type="text" class="form-control int_type " name="temperature_value"
                                            id="temperature_value"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            value="">
                                    </td>
                                    <td><label for="">Temperature in F</label></td>
                                    <td><input type="text" class="form-control int_type " name="temperature_value_f"
                                            id="temperature_value_f"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            value="">
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="">Pulse/Min</label></td>
                                    <td><input type="text" class="form-control int_type " name="pulse" id="pulse"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            value="">
                                    </td>
                                    <td><label for="">Respiration/Min</label></td>
                                    <td><input type="text" class="form-control int_type " name="respiration"
                                            id="respiration"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            value="">
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="">Head Circ-cm</label></td>
                                    <td><input type="text" class="form-control" name="head" id="head" value="">
                                    </td>
                                    <td><label>Waist Circumference (Peripheral)</label></td>
                                    <td><input type="text" autocomplete="off"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            class="form-control" name="waist" id="waist" value="">
                                    </td>

                                </tr>
                                <tr>
                                    <td><label>Hip Circumference (cm)</label></td>
                                    <td><input type="text" autocomplete="off"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            class="form-control" name="hip" id="hip" value=""></td>

                                    <td><label>Waist/Hip ratio</label></td>
                                    <td>
                                        <input type="text" autocomplete="off" readonly disabled
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            class="form-control" name="waist_hip_ratio" id="waist_hip_ratio" value="">
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <label>Blood sugar</label>
                                    </td>
                                    <td> <input type="text" autocomplete="off"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            class="form-control" name="blood_sug" id="blood_sug"
                                            value="{{ @$str ? $str : '' }}">
                                    </td>

                                    <td><label>Blood sugar unit</label></td>
                                    <td>
                                        <div class="col-md-6 no-padding"><input type="radio" name="visibility_status"
                                                value="1" />&nbsp;<br> mmol/L</div>
                                        <div class="col-md-6 no-padding"><input type="radio" name="visibility_status"
                                                checked value="2" />&nbsp;<br>mg/dL</div>
                                    </td>


                                </tr>
                                <tr>
                                    <td><label for="">Time Taken</label></td>
                                    <td colspan="2"><input type="text" class="form-control date_time_picker"
                                            name="time_taken" id="time_taken" value="" /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><label for="">Vital Remarks</label></td>
                                    <td colspan="3">
                                        <textarea class="form-control remarks" name="remarks" id="remarks"></textarea>
                                    </td>
                                </tr>
                                <td>
                                    <label>Spirometry</label>
                                </td>
                                <td> <input type="text" autocomplete="off"
                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                        class="form-control" name="spirometry" id="spirometry"
                                        value="">
                                </td>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <button style="width:90px;" class="btn btn-success col-md-1  pull-right " id="update_vital"
                        onclick="saveVitals()"> Save </button>
                    <button style="width:90px;" type="button" class="btn btn-default col-md-1  pull-right"
                        data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>

    </div>
</div>
<div id="vital_graph_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 1300px; width: 100%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg" style="height:37px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Vital and Progress</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box no-border no-margin">
                            <div class="box-body clearfix">
                                <div class="col-md-12">
                                    <div class="form-group control-required col-md-3 padding_sm">
                                        <label for="lstVitalItems">Filter By Vitals </label>
                                        <select class="form-control " name="lstVitalItems" id="lstVitalItems">
                                            <option value="">Fiter By Vitals</option>
                                            <option value="5">BP Systolic/ BP Diastolic</option>
                                            <option value="6">BP Diastolic / BP Systolic</option>
                                            <option value="7">Pulse / Min</option>
                                            <option value="8">Respiration / Min</option>
                                            <option value="9">Temperature F </option>
                                            <option value="12">Oxygen Saturation %</option>
                                            <option value="2">Weight in Kg</option>
                                            <option value="4">Height in CM</option>
                                            <option value="17">BMI Kg/M^2</option>
                                            <option value="20">Spirometry</option>
                                        </select>
                                    </div>
                                    <div class="form-group  control-required col-md-3 padding_sm">
                                        <label for="vital_graph_from_date">From Date</label>
                                        <input id="vital_graph_from_date" value="" class="form-control datepicker"
                                            name="vital_graph_from_date" type="text">
                                    </div>
                                    <div class="form-group  control-required col-md-3 padding_sm">
                                        <label for="vital_graph_to_date">To Date</label>
                                        <input id="vital_graph_to_date" value="" class="form-control datepicker"
                                            name="vital_graph_to_date" type="text">
                                    </div>
                                    <div class="form-group  control-required col-md-2 padding_sm">
                                        <label>&nbsp;</label><br>
                                        <button class="btn btn-block save_btn_bg" onclick="show_vital_history();"><i
                                                class="fa fa-check"></i> Submit</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12">
                        <div class="col-md-3 padding_sm">
                            <div class="theadscroll" style="position: relative; height: 400px;">
                                <table
                                    class="table table-bordered no-margin table_sm table-striped theadfix_wrapper vital_his_table">
                                    <thead>
                                        <tr class="table_header_bg">
                                            <th>Vitals</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>
                                    <tbody id="vital_his_table">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-9 padding_sm">
                            <div class="box no-margin no-border">
                                <div class="box-body">
                                    <div id="vital_graph"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade modal-fullscreen-xs-down" id="reg_or_renew_patient_modal" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="height: 100vh;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-user" aria-hidden="true"></i> <i class="fa fa-plus"
                        aria-hidden="true"></i> Registration/Renewal Patient</h4>
            </div>
            <div class="modal-body" id="reg_or_renew_patient_modal_content">
                @include('Emr::nursing_new.ns_registration_or_renewal_patient')
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="patient_investigation_result_min" role="dialog" aria-labelledby="myModalLabel"
    data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <input type="hidden" id="poc_patient_id" value="">
        <input type="hidden" id="poc_visit_id" value="">
        <input type="hidden" id="poc_doctor_id" value="">
        <div class="modal-content" style="height: 60vh;">
            <div class="modal-header" style="height:37px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa fa-search"></i> POC</h4>
            </div>
            <div class="modal-body" id="patient_investigation_result_min_content">

            </div>
            <div class="modal-footer">
                <div class="col-md-2 padding_sm" style="float:right;">
                    <button type="button" onclick="saveInvestigationResultEntryNs();" class="btn btn-primary save_class"
                        name="save_invs_result_btn" id="save_invs_result_btn"
                        style="/*margin-left:180px;*/float:right;"><i class="fa fa-save"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="patientClinicalHistoryModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 85%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="patientClinicalHistoryModalHeader">History</h4>
            </div>
            <div class="modal-body" style="min-height: 400px;" id="patientClinicalHistoryModalBody">
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px; margin-top: 5px;" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="getPatientAllegryModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1100px;width: 100%">
        <div class="modal-content">
            <div class="modal-header modal_box" style="height:37px;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getPatientAllegryModelHeader">NA</h4>
            </div>
            <div class="modal-body" id="getPatientAllegryModelDiv">
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" id="savePatientAllergyBtn" class="btn btn-blue" type="button"
                    onclick="savePatientAllergy()"><i id="savePatientAllergySpin" class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
</div>

<!-----------lab results modal------------------------------>

<div class="modal fade" id="patientLabResultsModal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 88%">
        <div class="modal-content">
            <div class="modal-header modal_box" style="height:37px;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Lab Results</h4>
            </div>
            <div class="modal-body" style="min-height: 450px;" id="patientLabResultsModalBody">
                <div class="row" id="lab_restuls_data">

                </div>
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>
<div id="lab_results_rtf_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:73%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Lab Result</h4>
            </div>
            <div class="modal-body" id="lab_results_rtf_data" style="height:550px;overflow-y:scroll;">

            </div>

        </div>
    </div>

</div>
<div id="modal_lab_result_trends" class="modal fade" role="dialog" style="z-index: 19999;">
    <div class="modal-dialog" style="width: 92%;margin-left: 95px;">

        <div class="modal-content">
            <div class="modal-header" style="background:#267cb9; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Lab Result Graph View</h4>
            </div>
            <div class="modal-body" style="height:580px;">
                <div class="row" id="lab_result_trends_data">

                </div>
            </div>
        </div>
    </div>
</div>



{{-- admit req modal --}}
<div class="modal fade" id="admit_modal" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa fa-list"></i> Admission Request </h4>
            </div>
            <div class="modal-body" id="admit_modal_data" style="height:300px;">

            </div>
        </div>
    </div>
</div>
{{-- bed tranfer req modal --}}
<div class="modal fade bd-example-modal-lg" id="bedTransferRequestModal" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="height: 25px;"><i class="fa fa fa-list"></i> Bed Transfer Request </h4>
            </div>
            <div class="modal-body theadscroll" id="bedTransferequestModal_data" style="min-height:360px;position: relative;">

            </div>
        </div>
    </div>
</div>

{{--choose admitting bed--}}
<div id="room_transfer_request_choose_bed_modal" class="modal fade " role="dialog">
    <div class="modal-dialog">

        <div class="modal-content modal-sm" style="width:100%;">
            <div class="modal-header" style="background:#f0ae63; color:#ffffff;">
            <button type="button" onclick="reloadIpList();" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
            <h4 class="modal-title">Select Bed
            </h4>
            </div>
            <div class="modal-body theadscroll"  id="room_transfer_request_choose_bed"  style="position: relative;height:350px;">

            </div>
            <div class="modal-footer">
                <button style="float:right;height:30px;" type="button" onclick="admitRoomRequest();" class="btn bg-primary"><i class="fa fa-save"></i> Admit Patient</button>
            </div>
        </div>
    </div>
</div>


{{-- new medication bookmarks modal for orchid --}}
<div class="modal fade" id="addItemToFavouriteNursingModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;" data-backdrop="static">
    <div class="modal-dialog" id="addfovouritesmodal" style="width: 80vw;">
        <div class="modal-content">
            <div class="modal-header modal_box" style="height: 40px;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="addItemToFavouriteNursingHeader">NA</h4>
            </div>
            <div class="modal-body" style="height: 65vh" id="addItemToFavouriteNursingDiv">
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
            </div>
        </div>
    </div>
</div>

{{-- Medication Return modal --}}
<div class="modal fade modal-fullscreen-xs-down" id="patient_medication_return_modal" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="height: 100vh; width: 95vw;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa fa-search"></i> Medication Return</h4>
            </div>
            <div class="modal-body" id="patient_medication_return_modal_content">
                
            </div>
        </div>
    </div>
</div>

{{-- Medication Return List Modal --}}
<div id="med_return_list_modal" class="modal fade " role="dialog" style="margin-top: 60px;height: 424px;" data-backdrop="static">
    <div class="modal-dialog" style="width:60%" >
        <div class="modal-content" style="background: #e4e3e3;">
            <div class="modal-header" style="background: #07a5ef; color:#ffffff;">
                <button type="button" class="btn btn-danger" data-dismiss="modal"  id="med_return_view_clos" aria-label="Close" style="float: right;">
                    <span aria-hidden="true">&times;</span>
                </button>
                    <h4 class="modal-title" style="font-size: 17px !important; font-weight: 700;">Return Details</h4>
                </div>
            <div class="modal-body" id="" style="height:340px">
                <div class="row">
                    <div class="col-md-12">
                        <div class="theadscroll always-visible" style="position: relative;height: 300px;">
                            <table class="table no-margin table_sm table-borderd table-striped">
                                <thead id="med_return_list_modal_head">
                                    <tr class="table_header_bg" style="background: #96babd;">
                                        <th>#</th>
                                        <th width="35%">Medicine</th>
                                        <th>Quantity</th>
                                        <th>Return Quantity</th>
                                        <th>Created At</th>
                                    </tr>
                                </thead>
                                <tbody id="med_return_list_modal_content">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

{{-- Pending Indent Count Modal --}}
<div class="modal fade" id="patientPendingIntendCountModal" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 40%;">
        <div class="modal-content" >
            <div class="modal-header" style="height: 40px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pending Intends</h4>
            </div>
            <div class="modal-body" style="min-height: 175px">
            <div id="patientPendingIntendCountModalDiv"></div>
            </div>
        </div>
    </div>
</div>

{{-- Pending Indent List Modal --}}
<div class="modal fade" id="patientPendingIntendListModal" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 80%;">
        <div class="modal-content" >
            <div class="modal-header" style="height: 40px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="patientPendingIntendListModalHeader">Pending Intends</h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
            <div id="patientPendingIntendListModalDiv"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="presc_allergy_comman_validate_modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header modal_box">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title" id='presc_comman_validate_modal_header'></h4>
            </div>        
            <div class="modal-body">
                <div class="theadscroll" style="position: relative; height: 40vh; padding-right:10px;">
                    <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed">
                    <thead>
                    <tr style="background-color: #21a4f1;   color:white; ">
                        <th style="width: 5%;">
                            <div class="checkbox checkbox-info inline no-margin" style="margin-top:4px !important">
                                <input type="checkbox" id="check_all_medicine">
                                <label style="padding-left: 2px;" for="check_all_medicine">
                                </label>
                            </div>
                        <th style="width: 30%;">Medicine</th>
                        <th style="width: 20%;">Allergic for</th>
                    </tr>
                    </thead>
                    <tbody id='presc_comman_allergy_validate_modal_body'>

                    </tbody>
                    </table>
                </div>            
            </div>
            <div class="modal-footer" style="border-top: 1px solid #21a4f1;">
                <button type="button" onclick="addAllergy_medicine_save()" class="btn btn-info" style="background:#21a4f1 !important; width:100px;">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-----------------------Lab Result Vie---------------------------------->
<div class="modal fade lab-resutl-width" id="lab_result_view_modal" data-backdrop="static" role="dialog"
    aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="height:614px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa fa-flask"></i> View Reports :- <span id="report_patient_name"></span></h4>
            </div>
            <div class="modal-body" >
               <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 5px;">

                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="" style="font-size: 12px !important;">From date</label>
                                <div class="clearfix"></div>
                                <input type="text" onblur="showResult();" value="{{ date('M-d-Y') }}" id='lab_result_fromdate' name="lab_result_fromdate" class="form-control datepicker" data-attr="date" placeholder="Date">
                            </div>
                        </div>
                
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box">
                                <label for="" style="font-size: 12px !important;">To date</label>
                                <div class="clearfix"></div>
                                <input type="text" onblur="showResult();" value="{{ date('M-d-Y') }}" id='lab_result_todate' name="lab_result_todate" class="form-control datepicker" data-attr="date" placeholder="Date">
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <div class="mate-input-box" style="height: 43px">
                               <select name="" onchange="showResult()" id="report_type" class="form-control select2">
                                <option value="">Select Type</option>
                                @foreach ($report_type as $data)
                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                @endforeach
                               </select>
                            </div>
                        </div>
              
                       
                
                      </div>
                </div>
               </div>
               <div class="col-md-12" id="lab_result_view_content">
                  
               </div>

            </div>
        </div>
    </div>
</div>
<!------------------------------------------------------------------------>
