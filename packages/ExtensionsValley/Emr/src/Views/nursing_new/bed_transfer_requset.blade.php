<div class="col-md-12" style="border: 0px solid #21a4f1; box-shadow: 0px 1px 4px 0 rgb(206 229 243); padding: 5px;">
    <div class="col-md-4">
        <i class="fa fa-user" title="Patient" style='font-size: 15px; color: #337ab7;'></i> 
        <label> Patient : <span style="color: #337ab7;" id="bed_transfer_patient_name"></span></label>
    </div>
    <div class="col-md-4">
        <i style='font-size: 15px; color: #337ab7;' class="fa fa-id-badge" title="UHID"></i> 
        <label> UHID : <span style="color: #337ab7;" id="bed_transfer_uhid"></span></label>
    </div>
    <div class="col-md-4">
        <i style=" font-size: 15px; color: #337ab7;" class="fa fa-bed" title="Current Bed"></i>
        <label> Current Bed : <span style="color: #337ab7;" id="bed_transfer_current_bed"></span></label>
    </div>
</div>


@if (intval($bed_transfer_icu_bed_flag) == 1)
    <div class="col-md-12">
        <hr>
    </div>    
    <div class="col-md-12">
    <fieldset style="display: inline; vertical-align: top; padding: 10px; width: 100% !important; border: 1px solid #b4dcf3;">
        <legend style="font-size: 14px; color: inherit; margin-bottom: 0px; border-bottom: 0px !important; width: 10%;">Transfer To:</legend> 
        <div class="btn-group btn-group-toggle gen_exam_acne" data-toggle="buttons">
            <label class="btn bg-radio_grp active" title="Transfer to ICU">
                <input type="radio" checked="" id="bed_transferRadio_icu" onchange="changeRadioBedTransfer(this);" name="bed_transferRadio" autocomplete="off" value="icu"> ICU
            </label>
            @if (intval($patient_gender) == 2) 
            <label class="btn bg-radio_grp" title="Transfer to Labour Room">
                <input type="radio" id="bed_transferRadio_labour" onchange="changeRadioBedTransfer(this);" name="bed_transferRadio" autocomplete="off" value="labour"> Labour Room
            </label>
            @endif
            <label class="btn bg-radio_grp" title="Transfer to OT">
                <input type="radio" id="bed_transferRadio_ot" onchange="changeRadioBedTransfer(this);" name="bed_transferRadio" autocomplete="off" value="ot"> OT
            </label>
            <label class="btn bg-radio_grp" title="Transfer to Room">
                <input type="radio" id="bed_transferRadio_room" onchange="changeRadioBedTransfer(this);" name="bed_transferRadio" autocomplete="off" value="room"> Room
            </label>
        </div>
    </fieldset>

    </div>
@endif

<div class="col-md-12">
    <hr>
</div>

<div class="col-md-12">
    <input type="hidden" id="bed_tranfer_patient_id"/>
    <input type="hidden" id="bed_tranfer_visit_id"/>

    <div class="col-md-6">
        <label class="filter_label ">Choose Nursing Station</label>
        {!! Form::select("bed_trasnfer_nursing_station", $locations_list, 0, ['class' => 'form-control select2','placeholder' => "Select",'onclick'=>'selectRoomTypeUsingLocation();','id' => 'bed_trasnfer_nursing_station', 'style' => 'color:#555555; padding:4px 12px;']) !!}
    </div>
    <div class="col-md-6">
        <label class="filter_label ">Room Types List</label>
        {!! Form::select("bed_trasnfer_room_type", $room_type_list, 0, ['class' => 'form-control select2 ','placeholder' => "Select Room Type",'id' => 'bed_trasnfer_room_type', 'style' => 'color:#555555; padding:4px 12px;']) !!}
    </div>
</div>

<div class="col-md-12" style="margin-top:25px;">
    <div class="col-md-4">
        @php
            $allow_retain_status = \DB::table('bed_master')->where('id',$current_bed_id)->value('allow_retain');
        @endphp
        @if($allow_retain_status == 't')
            <label class="filter_label ">Retain</label><br>
            <label class="switch">
                <input type="checkbox" id="retain_status">
                <span class="slider round"></span>
            </label>
        @endif
    </div>
</div>

<div class="col-md-12" style="text-align:right">
    <div class="col-md-12">
    <input style="width:85px;height:30px;" type="button" class="btn btn-secondary" data-dismiss="modal" value="Close" />
    <input style="width:85px;height:30px;" type="button"  class="btn btn-primary" onclick="saveBedTransferRequest();" value="Save"/>
    </div>
</div>

<div class="col-md-12">
    <hr>
</div>

<div class="col-md-12" style="margin-top:10px;" id="request_history">
</div>
