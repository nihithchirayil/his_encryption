<table class="table" style="margin:0px;font-size:12px !important;">
    <tbody id="PatientDetailsList">
    <tr class="table_subheading">
        <td colspan="6" style="text-align:center; font-size: 14px !important; color: #4e4e4e; height:25px !important; vertical-align: middle;background-color:#efefef;"><b>Patient Demographics</b></td>
    </tr>
    <tr>
        <td class="col-md-3" st><b>Patient UHID/MRN</b></td>
        <td>:</td>
        <td class="col-md-3">{{$visit_data[0]->uhid}}</td>
        <td class="col-md-3"><b>Registration Date</b></td>
        <td>:</td>
        <td class="col-md-3">{{date(\WebConf::getConfig('datetime_format_web_updated'),strtotime($visit_data[0]->admitted_on))}}</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>VIP Status</b></td>
        <td>:</td>
        <td class="col-md-3">@if($visit_data[0]->is_vip == 1)VIP Patient @else @endif</td>
        <td class="col-md-3"><b>Name</b></td>
        <td>:</td>
        <td class="col-md-3">{{$visit_data[0]->patient_name}}</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>Phone</b></td>
        <td>:</td>

        <td class="col-md-3">{{$visit_data[0]->phone}}</td>
        <td class="col-md-3"><b>Gender</b></td>
        <td>:</td>
        <td class="col-md-3">{{$visit_data[0]->gender}}</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>DOB</b></td>
        <td>:</td>
        <td class="col-md-3">{{date(\WebConf::getConfig('datetime_format_web_updated'),strtotime($visit_data[0]->dob))}}</td>
        <td class="col-md-3"><b>Blood Group</b></td>
        <td>:</td>
        <td class="col-md-3">{{$visit_data[0]->blood_group}}</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>Age</b></td>
        <td>:</td>

        <td class="col-md-3">{{$visit_data[0]->age}}</td>
        <td class="col-md-3"><b>Email</b></td>
        <td>:</td>
        <td class="col-md-3">{{$visit_data[0]->email}}</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>Marital Status</b></td>
        <td>:</td>
        <td class="col-md-3">@if($visit_data[0]->marital_status == 1)Single @else Married @endif</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr class="table_subheading">
        <td colspan="6" style="text-align:center;  font-size: 14px !important;color: #4e4e4e; height: 30px !important; vertical-align: middle;background-color:#efefef;"><b>Contact Details</b></td>
    </tr>
    <tr>
        <td class="col-md-3"><b>Address</b></td>
        <td>:</td>
        <td colspan ='4' class="col-md-3">{{$visit_data[0]->address}}</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>Area</b></td>
        <td>:</td>
        <td class="col-md-3">{{$visit_data[0]->area}}</td>
        <td class="col-md-3"><b>Town</b></td>
        <td>:</td>
        <td class="col-md-3">{{$visit_data[0]->city}}</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>District</b></td>
        <td>:</td>
        <td class="col-md-3">{{$visit_data[0]->city}}</td>
        <td class="col-md-3"><b>Country</b></td>
        <td>:</td>
        <td class="col-md-3">{{$visit_data[0]->country}}</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>Postal Code</b></td>
        <td>:</td>
        <td class="col-md-3">{{$visit_data[0]->pincode}}</td>
        <td class="col-md-3"><b>State</b></td>
        <td>:</td>
        <td class="col-md-3">{{$visit_data[0]->state}}</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>Relation</b></td>
        <td>:</td>
        <td class="col-md-3"></td>
        <td class="col-md-3"><b>Home Phone</b></td>
        <td>:</td>
        <td class="col-md-3"></td>
    </tr>
    <tr class="table_subheading">
        <td colspan="6" style="text-align:center;  font-size: 14px !important;color: #4e4e4e; height: 30px !important; vertical-align: middle;background-color:#efefef;"><b>Insurance Details</b></td>
    </tr>
    <tr>
        <td class="col-md-3"><b>Company Name</b></td>
        <td>:</td>
        <td class="col-md-3">{!! !empty($insurance_details[0]->company_name) ? $insurance_details[0]->company_name : ""!!}</td>
        <td class="col-md-3"><b>Pricing Name</b></td>
        <td>:</td>
        <td class="col-md-3">{!! !empty($insurance_details[0]->pricing_name) ? $insurance_details[0]->pricing_name : ""!!}</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>Authorisation Letter No</b></td>
        <td>:</td>
        <td class="col-md-3">{!! !empty($insurance_details[0]->authorisation_letter_no) ? $insurance_details[0]->authorisation_letter_no : ""!!}</td>
        <td class="col-md-3"><b>Insurance Id No</b></td>
        <td>:</td>
        <td class="col-md-3">{!! !empty($insurance_details[0]->insurance_id_no) ? $insurance_details[0]->insurance_id_no : ""!!}</td>
    </tr>
    @if($is_new_born == 1)
        <tr class="table_subheading">
            <td colspan="6"><b>Mother Details</b></td>
        </tr>
        <tr>
            <td><b>Mother Name</b></td>
            <td>:</td>
            <td>{{$mother_name}}</td>
            <td><b>Mother Uhid</b></td>
            <td>:</td>
            <td>{{$mother_uhid}}</td>
        </tr>
        <tr>

            <td><b>Mother Bed</b></td>
            <td>:</td>
            <td>{{$mother_bed_name}}</td>
            <td><b>Mother Nursing Station</b></td>
            <td>:</td>
            <td>{{$mother_location}}</td>
        </tr>
    @endif

    @if(isset($covid_info) && count($covid_info) > 0 )
    <tr class="table_subheading">
        <td colspan="6" style="text-align:center;  font-size: 14px !important;color: #4e4e4e; height: 30px !important; vertical-align: middle;background-color:#efefef;"><b>Covid-19 Details</b></td>
    </tr>

    <tr>
        <td class="col-md-3"><b>Type of Vaccine</b></td>
        <td>:</td>
        <td colspan ='4' class="col-md-3">{{$covid_info[0]->vaccine_name}}</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>Vaccine 1st dose date</b></td>
        <td>:</td>
        <td class="col-md-3">@if($covid_info[0]->first_dose){{date(\WebConf::getConfig('datetime_format_web_updated'),strtotime($covid_info[0]->first_dose))}}@endif</td>
        <td class="col-md-3"><b>Vaccine 2nd dose date</b></td>
        <td>:</td>
        <td class="col-md-3">@if($covid_info[0]->second_dose){{date(\WebConf::getConfig('datetime_format_web_updated'),strtotime($covid_info[0]->second_dose))}}@endif</td>
    </tr>
    <tr>
        <td class="col-md-3"><b>Covid History</b></td>
        <td>:</td>
        <td colspan ='4' class="col-md-3">{{$covid_info[0]->remarks}}</td>
    </tr>

    @endif

    </tbody>
    </table>
