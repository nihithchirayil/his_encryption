<style>
    textarea{
        height: 140px !important;
    }
    .assessmnt_clos{
      float: right;
      font-size: 11px;
      font-weight: 700;
      line-height: 1;
      color: #000;
      text-shadow: 0 1px 0 #fff;
      filter: alpha(opacity=20);
      border: white;
   }
   .nursing_notes_clos{
    float: right;
      font-size: 11px;
      font-weight: 700;
      line-height: 1;
      color: #000;
      text-shadow: 0 1px 0 #fff;
      filter: alpha(opacity=20);
      border: white;
   }
   .form_fav_list_clos{
    float: right;
      font-size: 11px;
      font-weight: 700;
      line-height: 1;
      color: #000;
      text-shadow: 0 1px 0 #fff;
      filter: alpha(opacity=20);
      border: white;
   }
</style>
<input type="hidden" id="assesment_patient_id" value="{{$patient_id}}"/>
<input type="hidden" id="assesment_visit_id" value="{{$visit_id}}"/>
<input type="hidden" id="assesment_encounter_id" value="{{$encounter_id}}"/>
<input type="hidden" id="assesment_admitting_doctor_id" value="{{$admitting_doctor_id}}"/>
<input type="hidden" id="fav_form_id" value="{{$fav_form_id}}"/>
<input type="hidden" id="fav_form_name" value="{{$fav_form_name}}"/>
<input type="hidden" id="drafted_form_name" value="{{$drafted_form_name}}"/>
<input type="hidden" id="drafted_form_id" value="{{$drafted_form_id}}"/>
<input type="hidden" id="formdata_drafted_head_id" value="{{$formdata_drafted_head_id}}"/>
<input type="hidden" id="ca_head_id" value="0">
<input type="hidden" id="assesment_draft_mode" value="0">
<div class="box no-border no-margin preview_container_wrapper anim">
    <div class="box-body clearfix">
        <div class="col-md-12 no-padding " style="cursor: pointer;padding:0 15px 0 0;padding-top:5px !important;">

                <div class="col-md-6 padding_sm pull-right">

                    <button type="button" style="width: 110px !important;float: right;height: 25px;background-color:#36A693;color:white;margin-right:13px;" class="btn bg-primary save_class" onclick="saveClinicalTemplate(2);">
                        <i class="fa fa-save"></i> Save and print
                    </button>
                    <button type="button" style="width: 110px !important;float: right;height: 25px;background-color:#36A693;color:white;margin-right:13px;" class="btn bg-primary save_class" onclick="saveClinicalassessment();">
                        <i class="fa fa-save"></i> Save
                    </button>
                    <button class="btn bg-primary" onclick="fetchAssessmentHistory(1);" data-toggle="collapse"
                    data-target="#assesmentCollapse" style="width: 110px !important;float: right;height: 25px;">
                        <i class="fa fa-history"></i> History
                    </button>
                    <button type="button" onclick="fetchFavoriteTemplates()"
                    class="btn bg-primary fetchFavoriteTemplatesBtn" title="Bookmarks" style="width: 110px !important;float: right;height: 25px;"><i
                        class="fa fa-list"></i> Bookmarks
                    </button>

                </div>
                <div class="col-md-2 ">
                    <h4 class="card_title">Clinical Notes</h4><span class="draft_mode_indication_ca"></span>
                </div>
                <div class="col-md-4 pull-right" id="doctorandnurse">
                    @php
                    $nursing_station = session()->get('nursing_station');
                    @endphp
                    <input type="hidden" name="ca_edit_status" id="ca_edit_status" value="0" />
                    @if ($nursing_station != '')
                        <button type="button" class="btn bg-green" onclick="viewNursesNotes();"><i
                                class="fa fa-list"></i> View Nurses Notes</button>


                        <input type="checkbox" name="chk_show_doctor_notes" id="chk_show_doctor_notes" value=""
                            onclick="fetchAssessmentHistory(1);" data-target="#assesmentCollapse"
                            aria-expanded="false" /> <label for="chk_show_doctor_notes">Show Doctor Assessment</label>

                    @else

                        <label class="switch">
                            <input type="checkbox" name="chk_show_nursing_notes" id="chk_show_nursing_notes" value="" onclick="fetchAssessmentHistory(1);"  data-target="#assesmentCollapse"
                            aria-expanded="false" >
                            <span class="slider round"></span>
                        </label>
                        <label for="chk_show_nursing_notes">Show Nursing Notes</label>
                    @endif
                </div>

                {{-- <span style="margin-left: 10px;">
                    <input type="checkbox" name="print_clinical_notes" id="print_clinical_notes">
                    <label for="print_clinical_notes">Print Clinical Notes</label>
                </span> --}}

        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 preview_left_box" style="padding: 2px;">
            <div class="card collapse" id="assesmentCollapse">

                <div class="card_body" id="ca-history-data-list">
                    @include('Emr::emr.assessment.assessment_history_view')
                </div>

                <div class="clearfix"></div>
                <div class="ht5"></div>

            </div>

            <div class="clearfix"></div>
            <div class="ht5"></div>

            <div style="display: flex;align-items: center;justify-content: center;flex-direction: row;"
                class="col-md-4 padding_sm">
                <select name="forms" id="forms" style="box-shadow:none;" class="bottom-border-text">
                    <option value="">Select Template</option>
                </select>
                {{-- <button class="btn light_purple_bg" style="margin: 0 0 0 5px;" onclick="loadPreviewTemplate()"><i class="fa fa-eye"></i> Draft Clinical Notes</button> --}}
            </div>
            <div class="col-md-2 padding_sm">
                <button type="button" onclick="saveFavTemplate()"
                    class="btn btn-default btn-flat fav-button-forms no-margin" title="Add To Bookmarks"><i
                        class="fa fa-star"></i>
                </button>

            </div>

            <div class="clearfix"></div>
            <div class="ht5"></div>
            <div class="clearfix"></div>
            <div class="ht5"></div>
            <div class="card">
                <!-- Card Body -->
                <form id="ca-data-form">
                    <div class="card_body notes_box">
                        <div class="theadscrolll" style="position: relative; height: auto;">
                            <div class="col-md-12" id="template-content-load-div"
                                style="background-image: url({{ asset('packages/extensionsvalley/default/img/medical_bg.jpg') }}); background-repeat: repeat;">

                            </div>
                        </div>
                    </div>
                </form>
                <!-- Card Body -->
            </div>
        </div>

        {{-- <div class="col-md-6 preview_right_box hidden">
        <div class="preview_div"></div>
    </div> --}}
    </div>
</div>
<div id="form_det_list_modal1" class="modal fade " role="dialog" style="margin-top: 60px;height: 460px; z-index:9999;">
    <div class="modal-dialog" style="width:60%">

        <div class="modal-content" style="background: #e4e3e3;">
            <div class="modal-header" style="background: #07a5ef; color:#ffffff;">
                <button type="button" class="assessmnt_clos" data-dismiss=""  id="assessmnt_clos" aria-label="Close">                    
                    <span aria-hidden="true">&times;</span>
                </button>
                    <h4 class="modal-title">Details</h4>
                </div>
            <div class="modal-body" id="" style="height:232px">
                <div class="row">
                    <div class="col-md-12" id="form_det_list_modal_content1">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default assessmnt_clos" data-dismiss="" aria-label="" id="assessmnt_clos">Close</button>
            </div>
        </div>
    </div>

</div>
<div id="nursing_notes_modal1" class="modal fade " role="dialog" style="margin-top: 60px;height: 460px;">
    <div class="modal-dialog" style="width:60%">

        <div class="modal-content" style="background: #e4e3e3;">
            <div class="modal-header" style="background: #07a5ef; color:#ffffff;">
                <button type="button" class="nursing_notes_clos" data-dismiss=""  id="nursing_notes_clos" aria-label="Close">                    
                    <span aria-hidden="true">&times;</span>
                </button>
                    <h4 class="modal-title">Nursing Notes</h4>
                </div>
            <div class="modal-body" id="" style="height:232px">
                <div class="row">
                    <div class="col-md-12" id="nursing_notes_modal_data1">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default nursing_notes_clos" data-dismiss="" aria-label="" id="nursing_notes_clos">Close</button>
            </div>
        </div>
    </div>

</div>
<div id="form_fav_list_modal1" class="modal fade " role="dialog" style="margin-top: 60px;height: 460px;z-index:1050;">
    <div class="modal-dialog" style="width:60%">

        <div class="modal-content" style="background: #e4e3e3;">
            <div class="modal-header" style="background: #07a5ef; color:#ffffff;">
                <button type="button" class="form_fav_list_clos" data-dismiss=""  id="form_fav_list_clos" aria-label="Close">                    
                    <span aria-hidden="true">&times;</span>
                </button>
                    <h4 class="modal-title">Form Favorites List</h4>
                </div>
            <div class="modal-body" id="" style="height:232px">
                <div class="row">
                    <div class="col-md-12">
                        <div class="theadscroll always-visible" style="position: relative;height: 300px;">
                            <table class="table no-margin table_sm no-border table-striped table-bordered">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th>#</th>
                                        <th>Form</th>
                                        <th>Created At</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="form_fav_list_body1">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default form_fav_list_clos" data-dismiss="" aria-label="" id="form_fav_list_clos">Close</button>
            </div>
        </div>
    </div>

</div>
