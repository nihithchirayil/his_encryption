<div class="col-md-12 card-body ns_main_search bg-custom-blue">
   <div class="col-md-3 ns_search_patient">
    <label>Search Patient</label>
        <input type="text" placeholder="Search Patient" class="form-control ns_dashboard-text" name="patient_search_txt" id="patient_search_txt" style="margin-top: 3.5px !important;"/>
   </div>

   <div class="col-md-3 ns_search_room_type">
        <label>Room Types</label>
            {!! Form::select('room_type', $room_type_list,0, ['class' => 'form-control ns_dashboard-text', 'id' =>'room_type','style'=>'font-size:14px !important;color:#a69999 !important;','placeholder'=>'Room Types']) !!}
   </div>
   <div class="col-md-2 ns_search_casuality_date">
    <label>Search By Date</label>
    <div class="clearfix"></div>
    <!-- <input type="text" placeholder="" class="form-control ns_dashboard-text datepicker" name="casuality_date" id="casuality_date" @if($op_list_enabled == '1') onblur="loadOpPatients();" @else onblur="loadCasulityPatients();" @endif value="{{date('M-d-Y')}}" style="margin-top: 3.5px !important;"/> -->
    <input type="text" placeholder="" class="form-control  datepicker ns_dashboard-text" name="casuality_date" id="casuality_date" value="{{date('M-d-Y')}}" style="margin-top: 3.5px !important;"/>
   </div>
   <div class="col-md-2 ns_patient_search">
    <label>Search Patient</label>
    <input type="text" placeholder="Search Patient" class="form-control ns_dashboard-text" name="patient_search_txt1" id="patient_search_txt1" style="margin-top: 3.5px !important;"/>
   </div>
   <div class="col-md-2 ns_patient_search hide_for_ip_global_patient">
    <label>Search By Doctor</label>
    {{-- @if($op_list_enabled == '1') --}}
    {!! Form::select('doctors_list', $doctors_list,0, ['class' => 'form-control ns_dashboard-text', 'id' =>'doctors_list','style'=>'font-size:14px !important;color:#a69999 !important; margin-top: 15px !important;','placeholder'=>'All', 'onchange'=> 'loadOpPatients();']) !!}
    {{-- @else --}}
    {!! Form::select('casuality_doctors', $casuality_doctors,0, ['class' => 'form-control ns_dashboard-text', 'id' =>'casuality_doctors','style'=>'font-size:14px !important;color:#a69999 !important; margin-top: 15px !important;','placeholder'=>'All', 'onchange'=> 'loadCasulityPatients();']) !!}
    {{-- @endif --}}
   </div>
   <div class="col-md-3 global_patient_search">
    <label>Global Patient Search</label>
    <input type="text" autocomplete="off" placeholder="Global Patient Search" class="form-control ns_dashboard-text" name="patient_search_global" id="patient_search_global" style="margin-top: 3.5px !important;"/>
   </div>
   <div class="col-md-3 pull-right text-right">
        <button class="btn bg-blue btn_refresh_dashboard BtnRefreshDashboard" style="width:38px; color:white;" id="btn_load_ip" onclick="loadIpPatients();"> IP </button>
   {{-- @if($op_list_enabled == '1') --}}
    <!-- <button class="btn bg-blue btn_refresh_dashboard" style="width:110px;color:white;" onclick="loadOpPatients();">OP Patients</button> -->
        <button class="btn bg-blue btn_refresh_dashboard BtnRefreshDashboard" style="width:38px; color:white;" id="btn_load_op" onclick="loadOpPatients();"> OP</button>
    {{-- @else --}}
    <!-- <button class="btn bg-blue btn_refresh_dashboard" id="load_casuality_patient_btn" style="width:110px;color:white;" onclick="loadCasulityPatients();">Casuality Patients</button> -->
        <button class="btn bg-blue btn_refresh_dashboard BtnRefreshDashboard" style="width:60px;color:white;" id="btn_load_casuality" onclick="loadCasulityPatients();"> Casualty</button>
    {{-- @endif --}}

    <!-- @if($is_casuality_user == '1')
    <button class="btn bg-blue btn_refresh_dashboard" id="load_ip_patient_btn" style="color:white;" onclick="loadIpPatients();">IP</button>
    @else
    <button class="btn bg-yellow color-blue btn_refresh_dashboard" onclick="$('#refresh_dashboard').addClass('fa-spin');window.location.reload();">
        <i id="refresh_dashboard" class="fa fa-refresh"></i>
        </button>
    @endif -->

    <button title="Registration/Renewal Patient" class="btn color-white btn_refresh_dashboard" style="background:#00ec00;" onclick="regOrRenNewPatient();">
        <i class="fa fa-user" aria-hidden="true"></i>
        <i class="fa fa-plus" aria-hidden="true"></i>
        </button>
   </div>
</div>

<div class="col-md-12 card-body ns_main_patients_container bg-custom-blue theadscroll">
    {{-- @include('Emr::nursing_new.ns_patient_list_container') --}}
</div>
