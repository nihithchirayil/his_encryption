<div class="col-md-12 no-padding to-do-list-toolbar">
    <div class="col-md-6">
        <span><i class="fa fa-clock-o" aria-hidden="true"></i> To-do list</span>
    </div>
    <div class="col-md-6 no-padding">
        <span class="pull-right">
            <button class="btn btn-sm round-btn-sm pull-right color-white bg-gray" id="expand_to_do_list" title="Expand.">
                <i class="fa fa-chevron-up" aria-hidden="true"></i>
            </button>
            <button class="btn btn-sm round-btn-sm pull-right color-white bg-gray" id="collapse_to_do_list" title="Collapse.">
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
            </button>
            <button class="btn btn-sm round-btn-sm pull-right color-white bg-gray" id="add_new_to_do" title="Add new task.">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
            <button class="btn btn-sm round-btn-sm pull-right color-white bg-gray" id="refresh_to_do" title="Refresh task list.">
                <i id="i_refresh_to_do" class="fa fa-refresh" aria-hidden="true"></i>
            </button>
            <button class="btn btn-sm round-btn-sm pull-right color-white bg-gray" id="history_to_do" title="History task list.">
                <i class="fa fa-history" aria-hidden="true"></i>
            </button>
        </span>
    </div>
</div>
<div class="col-md-12 no-padding theadscroll" id="to_do_list_content">

</div>
