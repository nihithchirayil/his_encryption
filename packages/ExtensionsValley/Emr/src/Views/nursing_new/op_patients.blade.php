<style>
    /* .zoom-effect {
        overflow: hidden;

    }

    .zoom-effect:hover {
        transform: scale(1.3);
        z-index: 99999;
        transition: all .3s ease-out .25s;
        /* transform: translate(0px,0px) scale(2); */
        /* background-color: white;

    } */

    /* .hover_color:hover {

    } */


    .small_bounce:hover {
        scale: 1.5 !important;
    }

</style>
<div class="col-md-12" style="text-align: center;">
    <h4 style="color:black;">OP Patients List</h4>
</div>
@if(sizeof($res)>0)
@foreach($res as $data)
<div class="col-md-3 ns_patient_container_main no-padding"
    data-attr-patient="{{strtolower(str_replace(' ', '',$data->patient_name))}}" data-attr-patient_id="{{$data->id}}" data-attr-uhid="{{strtolower(str_replace(' ', '',$data->uhid))}}">
    <div class="col-md-12 card-body ns_patient_container no-padding">
        <div class="col-md-12 no-padding">
            <div class="col-md-9 no-padding">
                <label class="name-label"
                    style="height: 21px !important;text-overflow: ellipsis;
                    white-space: nowrap;">{{$data->title}} {{ucfirst(strtolower($data->patient_name))}}</label>
            </div>
            <div class="col-md-3 no-padding pull-right">
                <label>{{$data->age}}/{{$data->gender}}</label>
            </div>
        </div>
        <div class="col-md-12 no-padding ">
            <div class="col-md-3 no-padding">
                <label>Uhid</label>
            </div>
            <div class="col-md-9 no-padding pull-right">
                <label>: {{$data->uhid}}</label>
            </div>
        </div>

        <div class="col-md-12 no-padding">
            <div class="col-md-3 no-padding">
                <label>Doctor:</label>
            </div>
            <div class="col-md-9 no-padding pull-right doctor-label"
                title="{{ucfirst(strtolower($data->doctor_name))}}">
                <label>{{ucfirst(strtolower($data->doctor_name))}}</label>
            </div>
        </div>
        <div class="col-md-12 no-padding" data-uhid="{{$data->uhid}}"
            data-patient="{{ucfirst(strtolower($data->patient_name))}}">
            <i class="fa fa fa-sticky-note-o small_bounce" style="color:rgb(97, 99, 233);
            font-size: 16px !important;" title="Doctor Notes" id="btn_private_notes"
               onclick="btn_private_notes('{{$data->id}}','{{$data->visit_id}}',this);" data-uhid="{{$data->uhid}}" data-patient="{{ucfirst(strtolower($data->patient_name))}}"></i>
            @if($administration_in_nurse_dashboard == '1')
            <i class="fa fa-gear" style="color: teal;
                font-size: 16px !important;" title="Administration"
                onclick="ViewPatientMedicationMin(this, '{{$data->id}}','{{$data->visit_id}}', 'administration');"></i>
            @endif

            <i class="fa fa fa-medkit" style="color: teal;
            font-size: 16px !important;" title="Prescription"
            onclick="ViewPatientMedicationMin(this, '{{$data->id}}','{{$data->visit_id}}', 'medication');"></i>

            <i class="fa fa fa-search" style="color:rgb(64, 0, 128);
                font-size: 16px !important;" title="Investigation"
                onclick="ViewPatientInvestigation(this, '{{$data->id}}','{{$data->visit_id}}','{{$data->doctor_id}}');"></i>

            <i class="fa fa fa-list" style="color:purple;
            font-size: 16px !important;" title="Clinical Assesment"
            onclick="ViewPatientAssesment(this, '{{$data->id}}','{{$data->visit_id}}','{{$data->doctor_id}}');"></i>

            <i class="fa fa-plus-circle btn-blue-sm addAllergies"
                style="font-size: 17px !important;margin-top: 4px !important;" title="Allergies"
                data-patient_id="{{ $data->id }}"
                onclick="getPatientMedicineAlergy(this,'{{$data->id}}','{{$data->visit_id}}')"></i>

                <i class="fa fa-list small_bounce" aria-hidden="true" style="font-size: 17px !important;margin-top: 4px !important;color:orange;" data-patient_id="{{ $data->id }}" title="Patient Intend History" onclick="getPatientIntendHistory('{{$data->id}}')"></i>

            </i>

        </div>

    </div>

    <div class="col-md-12 ns_patient_container_sub no-padding">

        <div class="col-md-8 room_label no-padding">

        </div>
        <div class="col-md-4 no-padding">
            <a tabindex="0" class="btn btn-sm bg-gold color-white pull-right" data-toggle="popover" data-trigger="focus"
                role="button" title="More Information" data-content="<table>

                        <tr>
                            <td>Visited on</td>
                            <td>:</td>
                            <td>{{date('M-d-Y',strtotime($data->visit_date))}}</td>
                        </<tr>
                        <tr>
                            <td>Area</td>
                            <td>:</td>
                            <td>{{ucfirst(strtolower($data->area))}}</td>
                        </<tr>
                        <tr>
                            <td>Phone</td>
                            <td>:</td>
                            <td>{{$data->phone}}</td>
                        </<tr>
                    </table>">
                <i class="fa fa-info-circle" title="More Information" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>


@endforeach
@else
<div class="col-md-12">
    <h5>No Patients Found</h5>
</div>
@endif
