<link
        href="{{ asset('packages/extensionsvalley/master/default/css/new_emr.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
        rel="stylesheet">
<style>
    #inv_nursing_contaner ul{
        display:inline-grid !important;
    }
    #inv_nursing_contaner li{
        margin:3px !important;
    }
    #inv_nursing_contaner .chekbox_table_lab{
        margin-top:3px !important;
    }
    .inv_sub_container {
        overflow-x: auto;    }
   .invest_view_close{
      float: right;
      font-size: 21px;
      font-weight: 700;
      line-height: 1;
      color: #000;
      text-shadow: 0 1px 0 #fff;
      filter: alpha(opacity=20);
      border: white;
   }
   .fav_dropdown_btn {
  border-radius: 3px;
  color: #fff !important;
  display: inline-block;
  font-family: Arial,Helvetica,sans-serif;
  font-size: 13px;
  font-weight: 400;
  padding: 6px 15px;
}
.fav_dropdown_wrapper {
  position: relative;
}
.fav_dropdown {
  background: #fff none repeat scroll 0 0;
  box-shadow: 0 0 5px #c0c0c0;
  padding: 10px;
  position: absolute;
  right: 0;
  width: 770px;
  z-index: 1500;
  display: none;
  height: 492px;
  top: 29px;
}
</style>
<input type="hidden" id="investigation_patient_id" value="{{$patient_id}}"/>
<input type="hidden" id="investigation_visit_id" value="{{$visit_id}}"/>
<input type="hidden" id="investigation_encounter_id" value="{{$encounter_id}}"/>
<input type="hidden" id="investigation_admitting_doctor_id" value="{{$admitting_doctor_id}}"/>
<input type="hidden" id="investigation_draft_mode" value="0">
<input type="hidden" id="investigation_head_id" value="{{ $investition_drafted_head_id }}"
        name="investigation_head_id">
<div class="box no-border no-margin right_wrapper_box anim">
    <div class="box-body clearfix" style="height: 633px;">
        <div class="col-md-12 no-padding " style="cursor: pointer;padding-top:7px !important;">


            <div class="col-md-5">
                <span style="display: inline-block;width:150px;">
                    <h4 class="card_title">
                        Investigation <span class="draft_mode_indication_inv"></span>
                    </h4>
                </span>
                <span style="display: inline-block;">
                    <div class="radio_container">
                        <input type="radio" id="inted_1" name="investigation_intend_type" value="1"  checked>
                        <label for="inted_1">Regular</label>

                        <input type="radio" id="inted_2" name="investigation_intend_type" value="2">
                        <label for="inted_2">Admission</label>

                        <input type="radio" id="inted_3" name="investigation_intend_type" value="3">
                        <label for="inted_3">Emergency</label>

                        <input type="radio" id="inted_4" name="investigation_intend_type" value="4">
                        <label for="inted_4">Discharge</label>
                    </div>
                </span>
            </div>
            <div class="col-md-2" style="padding-top:7px;@if (!session()->has('nursing_station'))visibility:hidden;@endif">
                <div class="padding_sm">
                    @if (session()->has('nursing_station'))
                        {!! Form::select('doctor_investigation', $doctor_list, isset($doctor_id) ? $doctor_id : null, ['class' => 'form-control select2 bottom-border-text', 'title' => 'Doctor', 'id' => 'doctor_investigation', 'style' => 'color:#555555; padding:2px 12px; ']) !!}
                    @else
                        {!! Form::select('doctor_investigation', $doctor_list, isset($doctor_id) ? $doctor_id : null, ['class' => 'form-control select2 bottom-border-text', 'title' => 'Doctor', 'id' => 'doctor_investigation', 'style' => 'color:#555555; padding:2px 12px; ', 'disabled' => 'disabled']) !!}
                    @endif
                </div>
            </div>

            <div class="col-md-5" style="padding:0 15px 0 0;">
                {{-- <div class="col-md-12 no-padding"> --}}
                    <div class="col-md-12 no-padding pull-right">

                            <button type="button" onclick="print_patient_investigation();"
                                class="btn btn-success save_class" name="print_investigation" id="print_investigation" style="width:98px !important;float: right;height: 25px;">
                                <i class="fa fa-save"></i>
                                Save and print
                            </button>
                            <button type="button" onclick="save_investigation();"
                                class="btn btn-success" name="save_inv" id="save_inv" style="width:98px !important;float: right;height: 25px;">
                                <i class="fa fa-save" aria-hidden="true"></i>
                            Save
                           </button>
                            <button type="button" onclick="save_draft_investigation();"
                                class="btn btn-info" name="save_as_draft" id="save_as_draft" style="width:98px !important;float: right;height: 25px;">
                                <i class="fa fa-file" aria-hidden="true"></i>
                                Save As Draft
                            </button>

                            <button class="btn btn-primary inv_history_btn" onclick="loadInvestigationHistory(1, 1);" data-toggle="collapse" data-target="#investigationCollapse" style="width:98px !important;float: right;height: 25px;">
                                <i class="fa fa-history"></i>
                                History
                            </button>

                            <button type="button" onclick="showInvestigationBookmarks();" class="btn btn-primary"
                                name="showInvestigationBookmarks" id="showInvestigationBookmarks" style="width:98px !important;float: right;height: 25px;">
                                <i class="fa fa-star"></i>
                                Bookmarks
                            </button>
                    </div>
                {{-- </div> --}}

            </div>

            <div class="fav_dropdown show_inv_bookmarks_box" style="background-color:#f1f1f1;">
                <div class="slidedown_close_btn show_inv_dropdown_close right_close show_inv_close_bookmark"> X </div>
                <div class="col-md-12 padding_sm">

                    <div class="clearfix"></div>
                    <div class="h10"></div>
                    <div class="col-md-4" style="height: auto;">
                        <h4 class="card_title">Groups
                            <button type="button" id="toggle_fav_group_add" title="Add Group" class="btn btn-sm bg-green"
                            style="padding-bottom:0px;">
                                <i class="fa fa-plus"></i>
                            </button>

                            <input type="text" class="form-control bottom-border-text invstigation_fav_add_group_text" placeholder="Group Name"
                            name="invst_fav_add_group_text"
                            style="width:72%;display:none;
                            float:right;">


                        </h4>

                        <div class="clearfix"></div>
                        <div class="ht5"></div>
                        <div class="col-md-12 theadscroll" style="height: 330px;
                        position: absolute;
                        margin-top:5px;
                        border: 1px solid gainsboro;
                        border-radius: 5px;
                        padding: 9px;overflow-x: auto;"

                        >
                            <ul class="nav sm_nav vertical_tab_btn_fav_group text-center add_investigation_group_list"></ul>
                        </div>

                    </div>
                    <div class="col-md-8" style="height: auto;">
                        <div class="col-md-4">
                            <h4 class="card_title">Items</h4>

                            <div class="col-md-3 checkAllFavItemsBtn" style="display:none;">
                                <input style="margin-left:15px;" class="styled" type="checkbox"
                                onclick="checkAllFavItemsBtn();">
                            </div>

                            <div class="col-md-3 unCheckAllFavItemsBtn" style="display:none;">
                                <input style="margin-left:15px;" checked class="styled" type="checkbox"
                                onclick="unCheckAllFavItemsBtn();">
                            </div>

                        </div>
                        <div class="col-md-6 investigation_fav_search_div" style="display:none;padding-top:10px;">
                            <i class="fa fa-search" style="margin-right:3px;float:left;margin-top:5px;"></i>
                            <input type="text" class="form-control investigation_fav_search_text bottom-border-text" placeholder="Search" style="width:85%;"
                                name="investigation_fav_search_text">
                        </div>


                        <div class="col-md-2 deleteAllFavItemsBtn" style="display:none;padding-top:10px;">
                            <button type="button" class="btn btn-danger"  onclick="deleteAllFavItemsBtn();"> <i
                            class="fa fa-close"> </i> Delete </button>

                        </div>
                        <div class="clearfix"></div>
                        <div class="ht5"></div>
                        <div class="col-md-12 theadscroll" style="height: 330px;
                        position: absolute;
                        border: 1px solid gainsboro;
                        border-radius: 5px;
                        padding: 9px;
                        margin: 7px;
                        width: 96%;margin-top:-7px;overflow-x: auto;">
                            <ul class="nav sm_nav vertical_tab_btn_fav_group text-center investigation_group_items"></ul>
                        </div>
                    </div>

                </div>

            </div>


        </div>
        <div class="col-md-12" style="padding: 2px;">
            <div class="card collapse" id="investigationCollapse">

                <div class="col-md-2 padding_sm visit_wise_filter_inv_div pull-right" style="display:none;    margin-right: -5px !important;
                margin-bottom: 5px;
                margin-top:5px;">
                    @php
                        $visit_type_arr = [
                            'OP' => 'OP',
                            'IP' => 'IP',
                            'ALL' => 'ALL',
                        ];
                    @endphp
                    {!! Form::select('visit_wise_filter_inv', $visit_type_arr, 'ALL', ['class' => 'form-control select2', 'title' => 'Visit Wise Filter', 'onchange' => 'loadInvestigationHistory(1);', 'id' => 'visit_wise_filter_inv', 'style' => 'color:#555555; padding:2px 12px;font-weight:600;']) !!}
                </div>


                <div class="card_body" id="investigation-history-data-list">
                    @include('Emr::emr.investigation.investigation_history_view')
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="ht5"></div>
            <div class="col-md-9" style="margin-top:5px;">
                <div class="col-md-12 box-body form-group" style="padding-top: 10px !important;">

                    <div class="col-md-9 form-group" style="padding-top:3px;">
                        <span style="width:5%"><i class="fa fa-search" aria-hidden="true"></i></span>
                        <input style="width:95%" type="text" placeholder="Search items" name="investigation_item_search_box"
                        autocomplete="off" id="investigation_item_search_box" class="form-control bottom-border-text">
                    </div>

                    <div class="col-md-5">
                        <div class="btn-group btn-group-toggle gen_exam_acne" data-toggle="buttons">
                            <label class="btn bg-radio_grp active"  >
                                <input type="radio" id="inlineRadio11" onchange="changeRadio(this);" name="radioInline11" checked autocomplete="off" value="lab" checked> Lab
                            </label>
                            <label class="btn bg-radio_grp">
                                <input type="radio" id="inlineRadio22" onchange="changeRadio(this);" name="radioInline11" autocomplete="off" value="radiology"> Radiology
                            </label>
                            <label class="btn bg-radio_grp">
                                <input type="radio" id="inlineRadio33" onchange="changeRadio(this);" name="radioInline11" autocomplete="off" value="procedure"> Procedure
                            </label>
                        </div>
                    </div>

                        <!-- investigation List -->
                        <div class="investigation-list-div" style="display: none;min-width:580px !important;margin-top:30px;">
                            <a style="float: left;" class="close_btn_inv_search">X</a>
                            <div class="inv_theadscroll" style="position: relative;">
                                <table id="InvestigationTable"
                                    class="table table-bordered no-margin table_sm table-striped inv_theadfix_wrapper">
                                    {{-- <thead>
                                    <tr class="light_purple_bg">
                                    <th>Frequency</th>
                                    </tr>
                                </thead> --}}
                                    <tbody id="ListInvestigationSearchData">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- investigation List -->

                        <!-- Investigation Favorite -->

                        <div class="fav_dropdown fav_investigation_box">
                            <div class="slidedown_close_btn fav_dropdown_close right_close invst_close_fav"> X
                            </div>
                            <input type="hidden" class="fav_service_code" value="" name="fav_service_code">
                            <div class="col-md-12 padding_sm">
                                <h4 class="card_title">Create Group</h4>
                                <div class="clearfix"></div>
                                <div class="ht5"></div>
                                <div class="col-md-8 padding_xs">
                                    <input type="text" class="form-control bottom-border-text invst_fav_add_group_text"
                                        placeholder="Group Name" name="invst_fav_add_group_text">
                                </div>
                                <div class="col-md-4 text-left padding_xs">
                                    <button type="button" class="btn bg-green invst_add_group_btn"><i
                                            class="fa fa-plus add_group_icon"></i></button>
                                </div>
                                <div class="clearfix"></div>
                                <div class="h10"></div>
                                <div class="" style="height: auto;  margin-top:10px;">
                                    <h4 class="card_title">Available Groups</h4>
                                    <div class="clearfix"></div>
                                    <!-- <div class="ht5"></div> -->
                                    <div class="theadscroll" style="height:300px;position: relative;">
                                        <ul class="nav sm_nav vertical_tab_btn_fav_group text-center add_fav_group_list"
                                        style="max-height:"></ul>
                                    </div>
                                </div>

                            </div>

                        </div>

                </div>

                <div class="col-md-12 box no-border no-margin">
                    <div style="background: #FFF;" class="col-md-12 box-body clearfix inv_main_container" >
                        <div class="nav-tabs-custom blue_theme_tab no-margin">
                            <div class="col-md-3 padding_sm" id="inv_nursing_contaner">
                                <div class="theadscroll inv_sub_container" style="background:white">
                                    <ul
                                        class="nav nav-tabs sm_nav vertical_tab_btn investigation_fav_tabs_list text-center" style="border:none !important;">

                                        @if (sizeof($favorite_group_head) > 0)

                                            @foreach ($favorite_group_head as $fh)
                                                @php
                                                    $has_lab = 0;
                                                    $has_radiology = 0;
                                                    $has_procedure = 0;
                                                    if (in_array($fh->id, $has_lab_group_list)){
                                                        $has_lab = 1;
                                                    }
                                                    if (in_array($fh->id, $has_radiology_group_list)){
                                                        $has_radiology = 1;
                                                    }
                                                    if (in_array($fh->id, $has_procedure_group_list)){
                                                        $has_procedure = 1;
                                                    }
                                                @endphp

                                                <li class="{{ $loop->first ? 'active' : '' }}" data-has-lab="{{$has_lab}}" data-has-radiology="{{$has_radiology}}" data-has-procedure="{{$has_procedure}}"
                                                    id="head_tab_{{ $fh->id }}" data-id="{{ $fh->id }}"
                                                    style="display:flex;height: 30px;">

                                                    <input style="margin-right: 5px;margin-left: 5px;"
                                                        id="checkbox_check_all_{{ $fh->id }}"
                                                        class="styled fav_group_check_box" type="checkbox"
                                                        name="{{ $fh->id }}" value="{{ $fh->id }}"
                                                        onclick="check_all_investigation(this)">
                                                    <a href="#invs_{{ $fh->id }}" data-toggle="tab" class="btn bg-info"
                                                        aria-expanded="true" style="width:85%"
                                                        onclick=" loadFavoriteItemsTab('{{ $fh->id }}'); ">
                                                        <h5 style="margin-top:2px;margin-left:5px;">
                                                            {{ucwords(strtolower($fh->group_name))}}</h5>
                                                    </a>
                                                    <span class="badge badge-light" style="background:orange;color:white;width: 19px;
                                                    padding: 7px;
                                                    padding-right: 14px;">
                                                        @php
                                                            $arr_keys = array_keys($groupIdCountArray);
                                                        @endphp

                                                        @foreach ($groupIdCountArray as $key => $value)
                                                            @if($key == $fh->id)
                                                            {{$value}}
                                                            @endif
                                                        @endforeach
                                                        @if(!in_array($fh->id,$arr_keys))
                                                            0
                                                        @endif
                                                    </span>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-9 padding_sm" id="investigation_check">
                                <div class="theadscroll inv_sub_container">
                                    <div class="tab-content" id="investigation_tab_content" style="margin-top:5px;">
                                        {{-- @php
                                            echo '<pre>';
                                            print_r($favorite_group_items);

                                        @endphp --}}

                                        @if (sizeof($favorite_group_head) > 0)
                                            @foreach ($favorite_group_head as $fn => $fh)
                                            <div @if ($fn == 0) class ="tab-pane fade active in" @else class="tab-pane fade" @endif id="invs_{{ $fh->id }}">
                                                <div class="ggg">
                                                    @if (sizeof($favorite_group_items) > 0)
                                                        @foreach ($favorite_group_items as $fi)
                                                            @if($fi->group_id == $fh->id)
                                                                @if ($fi->checked_status == 'checked_true')
                                                                    @php
                                                                        $checked_status = 'checked=""';
                                                                    @endphp
                                                                @else
                                                                    @php
                                                                        $checked_status = '';
                                                                    @endphp
                                                                @endif

                                                                <div
                                                                    class="checkbox checkbox-primary chekbox_table_lab col-md-4 padding_sm" style="margin-left: 15px;margin-top: 5px;">
                                                                    <input id="{{ @$fi->service_code !=' ' ? $fi->service_code : 'outside_'.$fi->service_desc  }}"
                                                                        class="fav_inv styled {{ $fh->id }}"
                                                                        type="checkbox" <?php echo $checked_status; ?>
                                                                        onclick="InsertInvestigation(this)"
                                                                        data-id="undefined"
                                                                        value="{{ @$fi->service_code !=' ' ? $fi->service_code : 'outside_'.$fi->service_desc  }}" edit_price="{{ $edit_price }}" data-price_editable="{{ $fi->is_price_editable }}">
                                                                    <label for="{{ @$fi->service_code !=' ' ? $fi->service_code : 'outside_'.$fi->service_desc  }}"
                                                                        class="{{ @$fi->service_code !=' ' ? $fi->service_code : 'outside_'.$fi->service_desc  }}">{{ $fi->service_desc }}</label>
                                                                </div>

                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top:11px;padding-bottom:18px !important;background-color:#fbfbfb;">
                        <div class="col-md-6" style="margin-top: 10px; padding-left: 10px; ">
                            <label>Remarks</label>
                            <textarea type="text" class="form-control" style="height:110px !important;" name="inv_remarks" id="inv_remarks"
                            onblur="saveInvRemarks()"></textarea>
                        </div>
                        <div class="col-md-6" style="margin-top: 10px; padding-left: 10px; ">
                            <label>Clinical History</label>
                            <textarea class="form-control" style="height:110px !important;" name="inv_clinical_history" id="inv_clinical_history"
                                onblur="saveClinicalHistory()"></textarea>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-md-3 no-padding">
                <div class="box no-border no-margin anim">
                    <label style="margin: 4px 6px 0px 6px;">Marked Investigations</label>
                    <div class="box-body clearfix theadscroll" id="investigation_clipboard"
                        style="padding: 5px !important;
                        font-size: 12px !important;
                        position: relative;
                        height:455px;">

                          <input type="hidden" id="is_price_editable" value="{{ $is_price_editable }}" >
                          <input type="hidden" id="edit_price" value="{{ $edit_price }}" >
                          <input type="hidden" id="enable_edit_price" value="{{ $enable_edit_price }}" >
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="ht5"></div>
            <div class="card">
                <div class="clearfix"></div>
                <div class="ht5"></div>

            </div>
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="investigation_det_list_modal" tabindex="-1" role="dialog"
    aria-labelledby="investigation_det_list_modal" aria-hidden="true" style="height: 550px;
    margin-top: 64px;" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 50%;background-color: #dfe5eb;height: 0px;" id="invest_modal">
        <!-- Modal content-->
        <div class="modal-content" style="background-color: #f1eded;" >
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="invest_view_close" data-dismiss=""  id="invest_view_clos" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table no-margin table_sm no-border">
                            <thead>
                            </thead>
                            <tbody id="investigation_det_list_modal_content">

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default invest_view_close" data-dismiss="" aria-label="" id="invest_view_clos">Close</button>
            </div>
        </div>

    </div>
</div>

<script src="{{ asset('packages/extensionsvalley/nursing_new/js/investigation_min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
