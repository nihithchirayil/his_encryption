<!----allergy modal-------------------->
<div class="modal fade" id="getPatientAllegryModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 1100px;width: 100%">
        <div class="modal-content">
            <div class="modal-header modal_box" style="height:37px;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getPatientAllegryModelHeader">NA</h4>
            </div>
            <div class="modal-body" id="getPatientAllegryModelDiv">
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" id="savePatientAllergyBtn" class="btn btn-blue" type="button"
                    onclick="savePatientAllergy()"><i id="savePatientAllergySpin" class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="getPatientVitalModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="max-width: 600px;width: 100%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="getPatientVitalModelHeader"><span class="getPatientVitalModelHeader"></span></h4>
            </div>
            <div class="modal-body" id="getPatientVitalModelDiv">
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                <button style="padding: 3px 3px" id="savePatientVitalsBtn" class="btn btn-blue" type="button"
                    onclick="savePatientVitals()"><i id="savePatientVitalsSpin" class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
</div>

<!----special notes modal---------------------------------->
<div class="modal fade" id="special_notes_modal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Special Notes</h4>
            </div>
            <div class="modal-body" style="min-height: 250px; max-height: 450px;" id="special_notes_modal_body">

            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<!---------lab results modal-------------------------------->
<div class="modal fade" id="patientLabResultsModal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 88%">
        <div class="modal-content">
            <div class="modal-header modal_box" style="height:37px;">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Lab Results</h4>
            </div>
            <div class="modal-body" style="min-height: 450px;" id="patientLabResultsModalBody">
                <div class="row" id="lab_restuls_data">

                </div>
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<!-----------radiology results modal--------------------------->
<div class="modal fade" id="radiology_results_modal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Radiology Results</h4>
            </div>
            <div class="modal-body" style="min-height: 250px; max-height: 450px;" id="radiology_results_modal_body">
                <div class="col-md-12">
                    <div class="col-md-2 padding_sm">
                        <div class="mate-input-box">
                            <label for="">Service Description</label>
                            <div class="clearfix"></div>
                            <select id="search_service_id" class="form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" id="radiology_results_data">

                </div>
            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<!----discharge summary list modal--------------------------------------->
<div class="modal fade" id="discharge_summary_list_modal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Discharge Summary List</h4>
            </div>
            <div class="modal-body" style="min-height: 250px;" id="discharge_summary_list_modal_body">

            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>
