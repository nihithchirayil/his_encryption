<div class="col-md-12" id="request_history" style="margin-bottom:35px !important;">
    @if(!empty($inv_his) || !empty($pharmacy_history) || !empty($yellow_data))
    <div class="theadscroll" style="position: relative;margin-bottom:30px; height:65vh;">
        <table class="table table-striped theadfix_wrapper table-bordered table-condensed">
            <thead>
                <tr class="headergroupbg_blue">
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Intend Id</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Type</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Created On</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Created By</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Doctor</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Visit Status</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Billconverted Status</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">BillNo</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Payment Type</th>
                    <th style="font-weight: 100;color:white;background-color:#21a4f1;">Paid Status</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($inv_his))

                @foreach($inv_his as $res)
                <tr @if(empty($res->bill_no))style="background:#fcec74;  cursor: pointer; "@endif onclick="toggle_indent_history_tr('investigation_detail', {{$res->id}})" >
                    <td>{{$res->id}}</td>
                    <td>Investigation</td>
                    <td>{{date('M-d-Y h:i A',strtotime($res->created_at))}}</td>
                    <td>{{$res->created_user}}</td>
                    <td>{{$res->doctor_name}}</td>
                    <td>{{$res->visit_type}}</td>

                    <td>@if(!empty($res->bill_no))
                            Converted
                        @else
                        Pending
                        @endif
                    </td>
                    <td>{{$res->bill_no}}</td>
                    <td>{{$res->payment_type}}</td>
                    <td>@if($res->paid_status ==1)
                            Paid
                        @else
                            Pending
                        @endif
                    </td>
                </tr>
                @php 
                    $investigation_detail_array = @$investigation_details[$res->id] ? $investigation_details[$res->id] : array();
                @endphp
                @if (!empty($investigation_detail_array) && count($investigation_detail_array) != 0) 
                <tr class="investigation_detail_{{ $res->id }}">
                    <th></th>
                    <th style="background: #97ddb0">Item Code</th>
                    <th style="background: #97ddb0">Item Name</th>
                    <th colspan="8"></th>
                </tr>
                    @foreach($investigation_detail_array as $val) 
                        <tr class="investigation_detail_{{ $res->id }} ">
                            <td></td>
                            <td>{{ $val->service_code }}</td>
                            <td>{{ $val->service_desc }}</td>
                            <td colspan="8"></td>
                        </tr>
                    @endforeach
                @endif
                
                @endforeach
                @endif

                @if(!empty($pharmacy_history))
                @foreach($pharmacy_history as $res)
                <tr style=" @if(empty($res->bill_no)) background:#fcec74;  @endif cursor: pointer; " onclick="toggle_indent_history_tr('pharmacy_detail', {{$res->id}})">
                    <td>{{$res->id}}</td>
                    <td>Pharmacy</td>
                    <td>{{date('M-d-Y h:i A',strtotime($res->created_at))}}</td>
                    <td>{{$res->name}}</td>
                    <td>{{$res->doctor_name}}</td>
                    <td>{{$res->visit_status}}</td>

                    <td>@if(!empty($res->bill_no))
                            Converted
                        @else
                        Pending
                        @endif
                    </td>
                    <td>{{$res->bill_no}}</td>
                    <td>{{$res->payment_type}}</td>
                    <td>@if($res->paid_status ==1)
                            Paid
                        @else
                            Pending
                        @endif
                    </td>
                </tr>
                @php 
                    $pharmacy_detail_array = @$pharmacy_details[$res->id] ? $pharmacy_details[$res->id] : array();
                @endphp
                @if (!empty($pharmacy_detail_array) && count($pharmacy_detail_array) != 0) 
                <tr class="pharmacy_detail_{{ $res->id }} ">
                    <th></th>
                    <th style="background: #97ddb0">Item Code</th>
                    <th style="background: #97ddb0">Item Name</th>
                    <th colspan="8"></th>
                </tr>
                    @foreach($pharmacy_detail_array as $val) 
                        <tr class="pharmacy_detail_{{ $res->id }} ">
                            <td></td>
                            <td>{{ $val->item_code }}</td>
                            <td>{{ $val->item_desc }}</td>
                            <td colspan="8"></td>

                        </tr>
                    @endforeach
                @endif                
                
                @endforeach
                @endif


                @if(!empty($yellow_data))
                @foreach($yellow_data as $result)
                    @php 
                        $yellow_sheet_details_array = @$yellow_sheet_details[$result->service_datetime] ? $yellow_sheet_details[$result->service_datetime] : array();
                    @endphp
                    <tr style="cursor:pointer; background: #b8dbef;" onclick="toggle_indent_history_tr('yellow_sheet_detail', '{{$result->service_datetime}}')" >
                        <td colspan="2">Procedure</td>
                        <td>{{date('M-d-Y',strtotime($result->service_datetime))}}</td>
                        <td colspan="7"></td>
                    </tr>
                   
                    @if (!empty($yellow_sheet_details_array) && count($yellow_sheet_details_array) != 0)   
                    <tr class="yellow_sheet_detail_{{ $result->service_datetime }} ">
                        <th></th>
                        <th style="background: #97ddb0">Item Code</th>
                        <th style="background: #97ddb0">Item Name</th>
                        <th colspan="8"></th>
                    </tr>              
                    @foreach($yellow_sheet_details_array as $res) 
                        <tr @if(empty($res->bill_no)) style="background:#fcec74;"@endif class="yellow_sheet_detail_{{ $result->service_datetime }} ">
                            <td>{{$res->id}}</td>
                            <td>{{$res->service_code}}</td>
                            <td>{{$res->service_desc}}</td>
                            <td>{{$res->user_name}}</td>
                            <td>{{$res->doctor_name}}</td>
                            <td>{{$res->visit_status}}</td>
                            <td>@if(!empty($res->bill_no))
                                    Converted
                                @else
                                Pending
                                @endif
                            </td>
                            <td>{{$res->bill_no}}</td>
                            <td>{{$res->payment_type}}</td>
                            <td>@if($res->paid_status ==1)
                                    Paid
                                @else
                                    Pending
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif               
               
                @endforeach
                @endif
                

            </tbody>
        </table>
    </div>
    @else
    <div class="col-md-12">
        <h5>No result Found!</h5>
    </div>
    @endif
</div>
