<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var patient_id = $('#lab_result_patient').val();
            var lab_result_fromdate = $('#lab_result_fromdate_t').val();
            var lab_result_todate = $('#lab_result_todate_t').val();
            var type = $('#report_type').val();

            var param = {
                patient_id: patient_id,
                lab_result_fromdate: lab_result_fromdate,
                lab_result_todate: lab_result_todate,
                type: type

            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    $('#lab_result_view_content').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(msg) {
                    $('#lab_result_view_content').html(msg);
                },
                complete: function() {
                    $('#lab_result_view_content').LoadingOverlay("hide");
                }
            });
            return false;
        });

    });
</script>
<table class='' style="font-size: 12px;width:100%">
    <thead>
        <tr class="headerclass"
            style="background-color:rgb(33 164 241);color:white;border-spacing: 0 1em;font-family:sans-serif;text-align:center">
            <th width="15%">Sample No.</th>
            <th width="15%">Sample Type</th>
            <th width="30%">Services</th>
            <th width="10%">Visit Status</th>
            <th width="20%">Uploaded Date</th>
            <th width="10%" style="text-align: center"> <i class="fa fa-eye"> </i></th>
        </tr>

    </thead>
    <tbody>
        @if (count($resultData) > 0)
            @foreach ($resultData as $data)
                <tr style="cursor: pointer" class="result-tr">

                    <td class="common_td_rules give-border-td">{{ $data->sample_no }}
                    </td>
                    <td class="common_td_rules give-border-td">{{ ucfirst(strtolower($data->type)) }}
                    </td>
                    @php
                        $jsonString = $data->service_type_array;
                        $serviceTypeArray = explode('@ ', $jsonString);
                    @endphp
                    <td class="common_td_rules give-border-td"><ol>@foreach ($serviceTypeArray as $item)
                        <li>{{ $item }}</li>
                    @endforeach

                </ol>
                    </td>
                    <td class="common_td_rules give-border-td">{{ $data->visit_status }}
                    </td>
                  
                    <td class="common_td_rules give-border-td">
                        {{ @$data->created_at ? date('M-d-Y h:i A', strtotime($data->created_at)) : '' }}
                    </td>
                    @php
                        if ($data->report_path) {
                            $destination = '/packages/extensionsvalley/emr/uploads/' . $data->patient_id . '/' . $data->report_path;
                        }
                    @endphp
                    @if ($data->report_path)
                        <td class="give-border-td" style="text-align: center" onclick="viewFile('{{ base64_encode($destination) }}', '{{ $data->report_path }}')">
                            <i class="fa fa-eye"
                                ></i>
                        </td>
                    @else
                        <td class="not-uploaded-td" style="text-align: center">Not Uploaded</td>
                    @endif


                </tr>
            @endforeach
        @else
            <tr>
                <td class="" colspan="6" style="text-align: center">No Record Found
                </td>
            </tr>
        @endif

    </tbody>
</table>
<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important;">
        {!! $page_links !!}
    </ul>
</div>
