<div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      @if($enable_externel_patient_registration == 0)
      <li role="registration" @if($enable_externel_patient_registration == 0)class="active" @endif><a href="#tab_registration" aria-controls="tab_registration" role="tab" data-toggle="tab">Registration</a></li>
      <li role="renewal"><a href="#tab_renewal" aria-controls="tab_renewal" role="tab" data-toggle="tab">Renewal</a></li>
      @endif
      @if($enable_externel_patient_registration == 1)
        <li role="renewal" class="active"><a href="#tab_externel_patient_registration" aria-controls="tab_renewal" role="tab" data-toggle="tab">Externel Patient Registration</a></li>
      @endif
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
      @if($enable_externel_patient_registration == 0)
      <div role="tabpanel" @if($enable_externel_patient_registration == 0)class="tab-pane active"  @else  class="tab-pane" @endif id="tab_registration">
            @include('Emr::nursing_new.ns_registration_patient')
      </div>
      <div role="tabpanel" class="tab-pane" id="tab_renewal">
            @include('Emr::nursing_new.ns_renewal_patient')
      </div>
      @endif
      @if($enable_externel_patient_registration == 1)
      <div role="tabpanel" class="tab-pane active" id="tab_externel_patient_registration">
            @include('Emr::nursing_new.ns_registration_externel_patient')
      </div>
      @endif
    </div>

  </div>
