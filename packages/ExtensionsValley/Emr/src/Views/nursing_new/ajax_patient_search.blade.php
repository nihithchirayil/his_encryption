@if (isset($patientNameSearch))

    @if(!empty($patientNameSearch))
        @foreach($patientNameSearch as $patient)

            <li class="list_hover" style="display: block; padding:5px 10px 5px 5px;color:black; "
            onclick='fillRenewalPatientValues("{{ htmlentities($patient->id) }}","{{ htmlentities(base64_encode($patient->patient_name)) }}","{{ htmlentities(base64_encode($patient->title)) }}","{{ htmlentities($patient->uhid) }}","{{ htmlentities($patient->age) }}","{{ htmlentities($patient->dob) }}","{{ htmlentities($patient->gender) }}","{{ htmlentities($patient->phone) }}")'>
            {{ htmlentities($patient->patient_name) }} [ID:{{ htmlentities($patient->uhid) }}]
            </li>

        @endforeach
    @else
    {{'No Results Found'}}
    @endif
@endif
