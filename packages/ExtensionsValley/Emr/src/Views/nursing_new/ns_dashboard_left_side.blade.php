

<div class="col-md-12">
    <span style="padding-top:6px !important;" class="left_side_span to-do-list-toolbar col-md-12 no-padding"><i class="fa fa fa-heartbeat" style="padding-left:2px !important;"></i> Nursing Station

    <i style="margin-right:-20px;" class="fa fa-search pull-right" onclick="fetchInvStatus(0);" title="Investigation intend status"></i>
    </span>
    <div style="margin-top:20px;">
      {!! Form::select('ns_station_list',$ns_station_list,$default_nursing_station, ['class' => 'form-control select2', 'id' =>'ns_station_list','style'=>'height:53px !important;']) !!}
    </div>
</div>

{{-- <div class="col-xs-12" style="margin-top:30px;">
    <span class="to-do-list-toolbar col-md-12 no-padding" style="margin-bottom:0px !important;"><i class="fa fa-wheelchair"></i> Search Patients</span>
    <div style="margin-top:6px;">
        <input class="form-control ns_dashboard-text" value="" autocomplete="off" type="text" id="patient_uhid" name="patient_uhid" placeholder="">
        <div id="patient_uhidAjaxDiv" style="border:none;" class="ajaxSearchBox"></div>
        <input class="filters" type="hidden" name="patient_uhid_hidden" value="" id="patient_uhid_hidden">
    </div>
</div> --}}


<div class="col-md-12" style="margin-top:20px;">
    <span class="left_side_span to-do-list-toolbar col-md-12 no-padding"><i class="fa fa-bed"></i> Bed Occupancy</span>
    <table class="table_bed_occupancy_count" style="margin-top:20px;">
        <thead>

            <tr>
                <td><span class="badge badge-circle badge-reserved">2</span> Reserved</td>
                <td><span class="badge badge-circle badge-occupied">2</span> Occupied</td>
                <td><span class="badge badge-circle badge-ready">0</span> Ready</td>
            </tr>
            <tr>
                <td><span class="badge badge-circle badge-vacant">4</span> Vacant</td>
                <td><span class="badge badge-circle badge-blocked">0</span> Blocked</td>
                <td><span style="color:white !important;" class="badge badge-circle badge-total">8</span> Total</td>
            </tr>
        </thead>
    </table>
</div>

