<div id="" class=" always-visible" style="overflow-x: scroll;">
    <table class="table table_sm theadfix_wrapper table-striped table-condensed styled-table" border="1px"
        style="border: 2px solid #CCC;">
        <thead>
            <tr style="background-color:#21a4f1;">
                <th colspan="<?= count($date_array) + 2 ?>" style="text-align: center;">Yellow Sheet Service Report</th>
            </tr>
            <tr class="table_header_bg">
                <th width="25%" style="text-align: left;">Procedure Name</th>
                <?php
                foreach ($date_array as $each) {
                    echo "<th width='3%'>$each</th>";
                }
                ?>
                <th width="4%">Total</th>
            </tr>
        </thead>
        <tbody>
            <?php
                    if (count($yellowsheet) != 0) {
                        foreach ($yellowsheet as $each) {
                            $total_cnt=0;
                            ?>
            <tr>
                <td style="text-align: left;"><?= $each->service_desc ?></td>
                <?php
                foreach ($date_array as $date) {
                    $cnt = 0;
                    if (sizeof($cnt_array) > 0) {
                        if (in_array($each->id, $cnt_array['yellow_sheet_id'])) {
                            if (isset($cnt_array['cnt'][$date][$each->id])) {
                                $cnt = $cnt_array['cnt'][$date][$each->id];
                            }
                        }
                    }
                    $total_cnt += $cnt;
                    if ($cnt != '0') {
                        echo "<td title='$each->service_desc' style='text-align: center;'> <span class='btn bg-teal-active'>$cnt</span></td>";
                    } else {
                        echo "<td title='$each->service_desc' style='text-align: center;'>-</td>";
                    }
                }
                echo "<td title='$each->service_desc' style='text-align: center;'> <span class='btn bg-blue'>$total_cnt</span></td>";
                ?>
            </tr>
            <?php
                                }
                            } else {
                                ?>
            <tr>
                <td colspan="<?= count($date_array) + 2 ?>" style="text-align: center;"> No Result Found</td>
            </tr>
            <?php
                                }
                                ?>
        </tbody>
    </table>
</div>
<div id="DoctorListReportScroll" class="theadscroll always-visible" style="overflow-x: scroll;">
    <table class="table table_sm theadfix_wrapper table-striped table-condensed styled-table" border="1px"
        style="border: 2px solid #CCC;">
        <thead>
            <tr style="background-color:#21a4f1;">
                <th colspan="<?= count($date_array) + 2 ?>" style="text-align: center;">Doctor Encounter Report</th>
            </tr>
            <tr class="table_header_bg">
                <th width="25%" style="text-align: left;">Doctor Name</th>
                <?php
                foreach ($date_array as $each) {
                    echo "<th width='3%'>$each</th>";
                }
                ?>
                <th width="4%">Total</th>
            </tr>
        </thead>
        <tbody>
            <?php
                    if (count($doctor_array) != 0) {
                        foreach ($doctor_array as $each) {
                            $total_cnt=0;
                            ?>
            <tr>
                <td style="text-align: left;"><?= $each->doctor_name ?></td>
                <?php
                foreach ($date_array as $date) {
                    $cnt = 0;
                    if (sizeof($doc_cnt) > 0) {
                        if (in_array($each->id, $doc_cnt['doc_id'])) {
                            if (isset($doc_cnt['cnt'][$date][$each->id])) {
                                $cnt = $doc_cnt['cnt'][$date][$each->id];
                            }
                        }
                    }

                    $total_cnt += $cnt;
                    if ($cnt != '0') {
                        echo "<td title='$each->doctor_name' style='padding: 0px 1px;'> <span class='btn bg-teal-active'>$cnt</span></td>";
                    } else {
                        echo "<td title='$each->doctor_name' style='text-align: center;'>-</td>";
                    }
                }
                echo "<td title='$each->doctor_name' style='text-align: center;'> <span class='btn bg-blue'>$total_cnt</span></td>";
                ?>
            </tr>
            <?php
                                    }
                            } else {
                                ?>
            <tr>
                <td colspan="<?= count($date_array) + 2 ?>" style="text-align: center;"> No Result Found</td>
            </tr>
            <?php
                                }
                                ?>
        </tbody>
    </table>
</div>
