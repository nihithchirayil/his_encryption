<style>
    .container-fluid .form-control{
         height: 36px !important;
         border-radius: 5px !important;
         font-size: 15px !important;
         padding-left: 7px !important;
     }
     .container-fluid label{
         font-size: 13px !important;
         color: gray !important;
     }
     .reg_title{
         margin-top: 15px;
         margin-bottom:15px;
     }

     .container-fluid .btn{
         height: 35px !important;
         width: 90px !important;
     }
     .datepicker{
         border:1px solid #d9d4d4 !important;
     }

     .container-datepicker .form-control{
        padding-left:0px !important;
     }
     .readonly{
        border: none;
        box-shadow: none;
        color: darkred;
     }
     .ajaxSearchBox>li{
        min-width: 295px !important;
     }

 </style>
 <form class="container-fluid">
    <div class="col-md-12 reg_title">
        <h5><b>Patient Renewal</b></h5>
    </div>
    @php
        $gender = \DB::table('gender')->pluck('name','id');
        $title = \DB::table('title_master')->where('status',1)->pluck('title','title');
        $specialities = \DB::table('speciality')->where('status','1')->orderBy('name','ASC')->pluck('name','id');
        $doctors_list = \DB::table('doctor_master')->where('status','1')->orderBy('doctor_name','ASC')->pluck('doctor_name','id');
    @endphp
    <div class="col-md-12">
        <div class="col-md-3">
            <label>Search Patient(Name/Uhid)</label>
            <input class="form-control hidden_search" value="" autocomplete="off" type="text" id="ren_patient_uhid" name="ren_patient_uhid">
            <div id="ren_patient_uhidAjaxDiv" style="position:absolute;" class="ajaxSearchBox"></div>
            <input type="hidden" name="ren_patient_uhid_hidden" value="" id="ren_patient_uhid_hidden"/>
            <input type="hidden" name="ren_patient_id_hidden" value="" id="ren_patient_id_hidden"/>
        </div>
    </div>
    <div class="col-md-12" style="margin-top:10px;">
        <div class="col-md-1">
            <label>Patient Name</label>
            <input type="text" name="new_ren_title" id="new_ren_title" readonly class="form-control readonly"/>
        </div>
        <div class="col-md-3">
            <label>&nbsp;</label>
            <input type="text" name="new_ren_patient_name" readonly id="new_ren_patient_name" class="form-control readonly"/>
        </div>
        <div class="col-md-2">
            <label>Age</label>
            <input type="text" name="new_ren_age" readonly id="new_ren_age" class="form-control readonly"/>
        </div>
        <div class="col-md-2">
            <label>DOB</label>
            <input type="text" name="new_ren_dob" readonly id="new_ren_dob" class="form-control readonly datepicker"/>
        </div>
    </div>
    <div class="col-md-12" style="margin-top:10px;">
        <div class="col-md-2">

            <label>Gender</label>
            {!! Form::select('new_ren_gender',$gender,null, ['class' => 'form-control readonly','Placeholder'=>'Select','readonly'=>'readonly', 'id' =>'new_ren_gender']) !!}
        </div>
        <div class="col-md-2">
            <label>Phone</label>
            <input type="text" name="new_ren_phone" readonly id="new_ren_phone" class="form-control readonly"/>
        </div>
    </div>

    <div class="col-md-12 container-datepicker" style="margin-top:10px;">
        <div class="col-md-4">
            <label for="select">Select Department</label>
            {!! Form::select('new_ren_department',$specialities,null, ['class' => 'form-control select2', 'id' =>'new_ren_department','placeholder'=>'select','onchange'=>'selectDoctorsList1()','style' => 'color:#555555; padding:4px 12px;']) !!}
        </div>
        <div class="col-md-4">
            <label for="select">Select Doctor</label>
            {!! Form::select('new_ren_doctor_id',$doctors_list,null, ['class' => 'form-control select2', 'id' =>'new_ren_doctor_id','placeholder'=>'select', 'style' => 'color:#555555; padding:4px 12px;']) !!}
        </div>
    </div>


     <div class="col-md-12" style="margin-top:10px;">
         <div class="col-md-6 pull-right">
            <button type="reset" id="reset_patient_renewal" class="btn bg-orange">
                <i class="fa fa-repeat"></i> Reset
            </button>
            <button type="button" class="btn bg-blue" onclick="nsRenewalNewPatient();">
                <i class="fa fa-save"></i> Save
            </button>
         </div>
         <div class="col-md-6"></div>
     </div>
 </form>
