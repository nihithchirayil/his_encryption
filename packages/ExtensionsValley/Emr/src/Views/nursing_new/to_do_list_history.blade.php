<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            // var user_name = $('#search_username').val();
            // var status = $('#search_status').val();
            var param = {
                // user_name: user_name,
                // status: status
            };
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function() {
                    // $('#searchUserBtn').attr('disabled', true);
                    // $('#searchUserSpin').removeClass('fa fa-search');
                    // $('#searchUserSpin').addClass('fa fa-spinner');
                    $('#modal_history_to_do_content').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(msg) {
                    $('#modal_history_to_do_content').html(msg);
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                    setTimeout(function() {
                        $('.theadfix_wrapper').floatThead({
                            position: 'absolute',
                            scrollContainer: true
                        });
                    }, 400);
                },
                complete: function() {
                    // $('#searchUserBtn').attr('disabled', false);
                    // $('#searchUserSpin').removeClass('fa fa-spinner');
                    // $('#searchUserSpin').addClass('fa fa-search');
                    $('#modal_history_to_do_content').LoadingOverlay("hide");
                }
            });
            return false;
        });

    });
</script>
<div class="box no-border no-margin">
    <div class="box-body clearfix">
        <div class="theadscroll" style="position: relative; height:485px;">
            <table class="table no-margin theadfix_wrapper table-condensed style-table"
                style="border: 0px solid #CCC;">
                <thead>
                    <tr>
                        <th width="10%">Nursing Station</th>
                        <th width="10%">Assigned By</th>
                        <th width="10%">Assigned To</th>
                        <th width="40%">Description</th>
                        <th width="10%">Created At</th>
                        <th width="10%">Status</th>
                        <th class="text-right" width="10%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($resultData) > 0)
                        @foreach ($resultData as $data)
                            <tr  class="" id="to_do_history_list_<?= $data->id ?>" style="cursor: pointer;">
                                <td class="common_td_rules name" title="{{ucfirst(strtolower($data->location_name))}}">{{ucfirst(strtolower($data->location_name))}}
                                </td>
                                <td class="common_td_rules email" title="{{ ucfirst(strtolower($data->assigned_by)) }}">{{ ucfirst(strtolower($data->assigned_by)) }}
                                </td>
                                <td class="common_td_rules email" title="{{ ucfirst(strtolower($data->assigned_to)) }}">
                                    {{ ucfirst(strtolower($data->assigned_to)) }}
                                </td>
                                <td class="common_td_rules status" title="{{ $data->description }}">
                                    {{ $data->description }}
                                </td>
                                <td class="common_td_rules status" title="{{date('Y-m-d h:i A',strtotime($data->updated_at))}}">
                                    {{date('Y-m-d h:i A',strtotime($data->updated_at))}}
                                </td>
                                <td class="common_td_rules status">
                                    @if($data->status == 1)
                                        Completed
                                    @else
                                        Assigned
                                    @endif
                                </td>
                                <td class="common_td_rules status text-right">
                                    @if($user_id == $data->assigned_to_id && $data->status == 0)
                                        <button onclick="updateToDoStatus('{{$data->id}}')" class="btn btn-sm bg-green" title="Complete task">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </button>
                                    @endif
                                    @if($user_id == $data->updated_by)

                                        <button class="btn btn-sm bg-orange" onclick="editTodo('{{$data->id}}','{{$data->nursing_station}}','{{$data->assigned_to_id}}','{{$data->description}}','{{$data->patient_id}}','{{$data->patient_name}}');" title="Edit task">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button class="btn btn-sm bg-red" onclick="deleteTodo('{{$data->id}}')" title="Delete task">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="user_name">No Task found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>

        <div class="clearfix"></div>
        <div class="col-md-12 text-right">
            <ul class="pagination purple_pagination" style="text-align:right !important;">
                {!! $page_links !!}
            </ul>
        </div>
    </div>
</div>
