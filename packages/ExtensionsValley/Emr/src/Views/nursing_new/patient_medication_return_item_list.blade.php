@if ($from_type == 'details')
    @if (count($result) > 0)
        @foreach($result as $each)
            @php
            $available_qty = ($each->available_quantity - $each->total_req_qty);
            @endphp
        <tr onclick="addMedicineToReturn(this)" data-item-desc="{{$each->item_desc}}" data-item-desc-search="{{strtolower($each->item_desc)}}" data-item-id="{{$each->item_id}}" data-bill-detail-id="{{$each->id}}" data-med-detail-id="{{$each->medication_detail_id}}" data-qty="{{$each->qty}}" data-available-qty="{{$available_qty}}" data-bill-head-id="{{$each->bill_head_id}}">
            <td>{{$loop->iteration}}</td>
            <td>{{$each->item_desc}}</td>
            <td>{{$each->batch}}</td>
            <td>{{$each->bill_no}}</td>
            <td>{{$each->qty}}</td>
            <td>{{$available_qty}}</td>
            <td>{{date('M-d-Y', strtotime($each->created_at))}}</td>
        </tr>
        @endforeach
    @else
        <tr>
            <td colspan="7">No Data Found</td>
        </tr>
    @endif
@elseif ($from_type == 'history_list')
<div class="theadscroll" style="height: 165px;">
    <table class="table no-margin theadfix_wrapper table_sm no-border" style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th width="15%">Date</th>
                <th width="15%">Created By</th>
                <th width="20%">Remarks</th>
                <th width="15%">Status</th>
                <th width="10%">Visit Status</th>
                <th width="25%" colspan="3"></th>
            </tr>
        </thead>
        <tbody>

            @if (!empty($result) && count($result) != 0)
            @foreach($result as $each)
                @php
                    $requested_at = !empty($each->requested_at) ? date('M-d-Y h:i A', strtotime($each->requested_at)) : '';
                @endphp
                <tr>
                    <td> {{$requested_at}} </td>
                    <td>{{ucwords($each->created_by)}}</td>
                    <td>{{$each->remarks}}</td>
                    <td>{{$each->return_status}}</td>
                    <td>{{strtoupper($each->visit_status)}}</td>                    
                    <td>
                        <button class="btn btn-block light_purple_bg" onclick="loadReturnView('{{$each->head_id}}')" style="width: 80px;"><i class="fa fa-eye"></i> View
                        </button>
                    </td>
                    @if ($each->status == '0')
                    <td>
                        <button class="btn btn-block light_purple_bg" onclick="editReturnView('{{$each->head_id}}')" style="width: 80px;"><i class="fa fa-pencil-square-o"></i> Edit
                        </button>
                    </td>
                    <td>
                        <button class="btn btn-block light_purple_bg" onclick="deleteReturnData(this, '{{$each->head_id}}')" style="width: 80px;"><i class="fa fa-trash delete_return_icon"></i> Delete
                        </button>
                    </td>
                    @endif
                </tr>
            @endforeach
            @else
            <tr>
                <th colspan='6' style='text-align: center'>No Result Found</th>
            </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="return_history_pages pagination no-margin" style="text-align:right !important;">
        {!! $paginator->render() !!}
    </ul>
</div>
@else 
    @if (count($result) > 0)
        @foreach($result as $each)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$each->item_desc}}</td>
            <td>{{$each->billed_qty}}</td>
            <td>{{$each->requested_qty}}</td>
            <td>{{date('M-d-Y h:i A', strtotime($each->created_at))}}</td>
        </tr>
        @endforeach
    @else
        <tr>
            <td colspan="5">No Data Found</td>
        </tr>
    @endif
@endif