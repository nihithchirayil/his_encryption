<div class="box no-border no-margin">
    <div class="box-body clearfix">
        <div class="theadscroll" style="position: relative; height:485px;">
            <table class="table no-margin theadfix_wrapper table-condensed style-table"
                style="border: 0px solid #CCC;">
                <thead>
                    <tr style="background-color:cornflowerblue;color:white;">
                        <th width="20%">patient name</th>
                        <th width="10%">Uhid</th>
                        <th width="35%">Investigation</th>
                        <th width="20%">Ordered At</th>
                        <th width="15%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($res) > 0)
                        @foreach ($res as $data)
                            <tr  class="">
                                <td class="common_td_rules name" title="{{ucfirst(strtolower($data->patient_name))}}">{{ucfirst(strtolower($data->patient_name))}}
                                </td>
                                <td class="common_td_rules name" title="{{$data->uhid}}">{{$data->uhid}}
                                </td>
                                <td class="common_td_rules name" title="{{$data->service_desc}}">{{$data->service_desc}}
                                </td>
                                <td class="common_td_rules name" title="{{date('M-d-Y h:i A',strtotime($data->ordered_at))}}">
                                    {{date('M-d-Y h:i A',strtotime($data->ordered_at))}}
                                </td>
                                <td class="common_td_rules name" title="{{$data->status}}">{{$data->status}}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="user_name">No Investigations found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
