
@if(count($rec_loc) > 0)
<div class="theadscroll" style="position: relative; height: 410px;">

    @foreach ($rec_loc as $loc)
        @if($loc->status==1)
        @php
            $bg_color='#4bec4bcc'; 
            $doc_active_status='isActive';
        @endphp
        @else
        @php
            $bg_color='#d86363cc'; 
            $doc_active_status='isInActive'

        @endphp
        @endif
                <div class="col-md-12 padding_sm search_user {{ $doc_active_status }}" id="doctorTr{{ $loc->id }}" style="border-bottom: 2px solid #a52a2a59;margin-bottom: 4px;background-color:{{ $bg_color }};height:30px;">
                        <div class="col-md-9"  onclick="" >
                           <label for="" style="font-weight: 700;margin-top:5px;"> {{ $loc->name }} <span style="font-weight: 300 !important">{{ @$loc->speciality_name ? '('.$loc->speciality_name.')': '' }}</span></label>
                        </div>
                        <div class="col-md-1"  onclick="" style="margin: 5px">
                            <button type="button" class="btn btn-primary" id="usrDetailsEdit{{ $loc->id }}" onclick="docDetailsEdit({{ $loc->id }})"><i id="usrDetailsSpin{{ $loc->id }}" class="fa fa-edit"></i></button>
                        </div>
                        <div class="col-md-1"  onclick="" style="margin: 5px">
                            <button type="button" id="usrDetailsDelete{{ $loc->id }}" onclick="docDetailsDelete({{ $loc->id }})" class="btn btn-danger"><i id="usrDetailsDelSpin{{ $loc->id }}" class="fa fa-trash"></i></button>

                        </div>
                </div>
       
    @endforeach

</div>
@else
<div class="col-md-12">
    <label for="" style="font-weight: 700;margin-top:5px;text-align:center"> No Record Found.</label>
</div>
@endif

