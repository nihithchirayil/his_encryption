<style>
    table, th, td {
  border: 1px solid #4ae083b7;
}
</style>
<div class="col-md-6" style="margin-top: 4px;padding:0px;">
    <div class="mate-input-box" style="height: 43px !important">
        <label for="">Assessment</label>
        <input type="text" id="Search_assessment2" onkeyup="searchDivAssessment2()" class="form-control" style="width: 90% !important;float: left;"
        placeholder="Search Assessment" title="Type in a name"> 
    
    </div>
</div>

    <div class="col-md-4 padding_sm" style="margin-bottom:10px;">
        <div class="mate-input-box" style="height: 44px !important; margin-top:3px;">
        <label for="">Type</label>
        <div class="clearfix"></div>
        <select class="form-control type" name="type" id="type" >
            <option value="dyn_textarea">{{__('TEXTAREA')}}</option>
            <option value="dyn_textbox">{{__('TEXTBOX')}}</option>
                   </select>
    </div>
    </div>
    <div class="col-md-1 padding_sm pull-right"  style="margin-bottom: 19px;
    margin-top: -8px;margin-right: 43px;">
        <label for="">&nbsp;</label>
        <div class="clearfix"></div>
        <button type="button" class="btn btn-block btn-success spin_assessment_add" id="spin_assessment_add" style="color: white" onclick="addNewAssessment()"><i class="fa fa-save" style="color: white"></i> Save </button>
    </div>

<div class="clearfix"></div>
<div class="theadscroll" style="position: relative; height: 300px;" id="assesment_list_init_2">
    <table class="col-md-12 padding_sm assessment_search2"  style="border: 1px solid #4ae083b7;margin-bottom: 4px;padding: 5px !important;">
    <thead>
        <tr class="table_header_bg">
            <th width="50%">Assessment</th>
            <th width="49%">Type</th>
            <th style = "width:1%">X</th>
        </tr>
    </thead>
    <tbody>
@if(count($rec_loc) > 0)

    @foreach ($rec_loc as $loc)
    <tr id="assessment_set_2{{ $loc->id }}">
        <td class ="common_td_rules" id="assessment_set_2{{ $loc->id }}" style="padding-left: 4px;">{{ $loc->name }}</td>
        <td class ="common_td_rules" id="assessment_set_2{{ $loc->id }}" style="padding-left: 4px;">{{ $loc->type }}</td>

                        <td>
                        <i id="delete_spin{{ $loc->id }}" onclick="deleteAssessemnt({{ $loc->id }})" class="fa fa-times pull-right" style="margin-top:4px;padding-inline: 7px;"></i>
                        </td>
                    </tr>
                    </div>

                </div>
       
    @endforeach
</tbody>
</table>
</div>
@else
<div class="col-md-12">
    <label for="" style="font-weight: 700;margin-top:5px;text-align:center"> No Record Found.</label>
</div>
@endif