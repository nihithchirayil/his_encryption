@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <style>
        .tr_active{
                   background: lightblue !important;
        }
    </style>
@endsection
@section('content-area')
<div id="addAssessmentModal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:50%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Add/Remove Assessment</h4>
            </div>
            <div class="modal-body" id="addAssessmentList" style="height:440px">

                


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

</div>
    <!-- page content -->
    <div class="right_col">
        <div class="row codfox_container">
            <h5 style="margin: 0px !important" class="blue pull-right"><strong> <?= $title ?> </strong></h4>
                <div class="clearfix"></div>
                <div class="col-md-4 padding_sm">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow:0 0 3px #36A693!important; border: 2px solid #36A693!important;height: 10vh;">
                            <div class="col-md-12 padding_sm">
                                <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
                                <input type="hidden" id="token" value="">
                                <input type="hidden" id="save_doc" value="">
                                <input type="hidden" id="user_id" value="0">
                                <input type="hidden" name="hidden_id" id="hidden_id" value="">
                                <div class="col-md-8 padding_sm ml">
                                    <div class="mate-input-box" style="height: 43px !important">
                                        <label for="">Doctor</label>
                                        <input type="text" id="Search_usr" class="form-control"
                                            onkeyup="searchDivUser()" placeholder="Search Doctor">

                                    </div>
                                </div>
                                <div class="col-md-4" style="padding: 0px;">
                                   <div class="col-md-6" style="padding: 0px;">
                                    <div class="checkbox checkbox-success inline">
                                        <input id="doc_active" class="" type="checkbox" value="" 
                                                            name="doc_active" >
                                                        <label style="font-weight:600"class="text-blue" for="doc_active">
                                                            Active
                                                        </label>
                                            </div>
                                   </div>
                                   <div class="col-md-6" style="padding: 0px;">
                                    <div class="checkbox checkbox-success inline">
                                        <input id="doc_all" class="" type="checkbox" value="" 
                                                            name="doc_all" checked>
                                                        <label style="font-weight:600"class="text-blue" for="doc_all">
                                                            All
                                                        </label>
                                            </div>
                                   </div>
                                   <div class="clearfix"></div>
                                   <div class="col-md-6" style="font-size: 10px;font-weight:500;padding:0px"><i class="fa fa-square" style="color: #4bec4bcc;margin-left:1px"></i>Active</div>
                                   <div class="col-md-6" style="font-size: 10px;font-weight:500;padding:0px"><i class="fa fa-square" style="color: #d86363cc;margin-left:1px"></i>InActive</div>
                                </div>




                            </div>
                        </div>
                    </div>
                    <div class="box no-border no-margin" style="margin-top: 10px !important">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow:0 0 3px #36A693!important; border: 2px solid #36A693!important;min-height: 69.5vh;"
                            id="searchDataDiv_body">
                            <div id="searchDataDiv"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 padding_sm" id="patientappoinment_div">
                    <div class="box no-border no-margin">
                        <div class="box-footer"
                            style="padding: 5px; box-shadow:0 0 3px #36A693!important; border: 2px solid #36A693!important;min-height: 81vh;">
                            <div class="clearfix"></div>
                           
                            <div class="col-md-12 padding_sm" style="margin-top: 10px;min-height: 500px;">
                                <div class="box-footer col-md-12" id="assessment_list_body"
                                    style="padding: 5px; box-shadow:0 0 3px #36A693!important;margin-left:-4px;min-height:440px;">
                                    <div class="col-md-5 padding_sm text-center table_header_bg" >
                                        <strong id="" class="">Assessments</strong>
                                    </div>
                                    <div class="col-md-5 padding_sm text-center table_header_bg" >
                                        <strong  class="">Doctor:<span id="this_is_head" >Not Selected</span></strong>
                                    </div>
                                    <div class="col-md-2 padding_sm text-center table_header_bg" id="add_new_assesment" >
                                        <i class="fa fa-plus" title="Add new assessment" style="cursor: pointer;" id="add_btn_spin" onclick="showInputModal()"></i>Add New Assessment
                                    </div>
                                    
                                    <div class="col-md-12" id="assesment_list">

                                    </div>
                                </div>
                               

                                <div class="col-md-1 pull-right" style="margin-top: 10px;">
                                    <button type="button" class="btn btn-success" id="saveUserMappedDetails"
                                        onclick="saveDoctorMappedDetails()">Save <i id="saveUserMappedDetailsSpin"
                                            class="fa fa-save"></i></button>
                                </div>
                                <div class="col-md-1 pull-right" style="margin-top: 10px;">
                                    <button type="button" class="btn btn-warning" id=""
                                        onclick="resetSettingDiv()">Reset <i id=""
                                            class="fa fa-recycle"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

        </div>
    </div>
    </div>

    <!-- /page content -->

@stop
@section('javascript_extra')

    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/doctor_mapping_screen.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
   
@endsection
