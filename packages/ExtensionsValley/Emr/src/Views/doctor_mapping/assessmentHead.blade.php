@if(count($rec_loc) > 0)
<div class="col-md-6" style="margin-top: 4px;padding:0px;">
    <div class="mate-input-box" style="height: 43px !important">
        <label for="">Assessment</label>
        <input type="text" id="Search_assessment" onkeyup="searchDivAssessment()" class="form-control" style="width: 100% !important;" 
        placeholder="Search Assessment" title="Type in a name">
    
    </div>
</div>
<div class="clearfix"></div>

<div class="theadscroll" style="position: relative; height: 300px;" id="assesment_list_init">

    @foreach ($rec_loc as $loc)
       
            
                <div class="col-md-12 padding_sm assessment_search" id="assessment_set{{ $loc->id }}" style="border: 1px solid #a52a2a59;margin-bottom: 4px;padding: 5px !important;">
                    <div class="checkbox checkbox-success inline">
                        <input id="rec_checkbox{{ $loc->id }}" class="save_assessment_check resetMe" type="checkbox" value="{{ $loc->id }}" 
                            name="rec_checkbox{{ $loc->id }}">
                        <label class="text-blue" style="font-weight: 600" for="rec_checkbox{{ $loc->id }}">
                            {{ $loc->name }}
                        </label>
                    </div>
                </div>
       
    @endforeach

</div>
@else
<div class="col-md-12">
    <label for="" style="font-weight: 700;margin-top:5px;text-align:center"> No Record Found.</label>
</div>
@endif