<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<style>
    .mate-input-box{
        height:45px !important;
    }
    .mate-input-box label {
        margin-top:0px !important;
    }
    .select2-container{
        box-shadow:none !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__display {
    cursor: default;
    padding-left: 2px;
    padding-right: 5px;
    font-size: 9px;
    font-weight: 600;
    }
</style>
{!! Form::open(['name' => 'form_surgery_request', 'method' => 'post', 'id' => 'form_surgery_request']) !!}
<div class="col-md-12">
    <div class="col-md-4 box-body" style="padding:10px !important;height:700px;">

        <div class="col-md-6">
            <div class="mate-input-box" style="z-index:99999">
                <label class="filter_label">UHID</label>
                <input class="form-control hidden_search" value="" autocomplete="off" type="text" id="patient_uhid" name="patient_uhid">
                <div id="patient_uhidAjaxDiv" style="margin-top:14px;" class="ajaxSearchBox"></div>
                <input class="filters" type="hidden" name="ot_patient_id_hidden" value="" id="ot_patient_id_hidden">
                <input class="filters" type="hidden" name="patient_current_visit_id_hidden" value="" id="patient_current_visit_id_hidden">
            </div>
        </div>


        <div class="col-md-6">
            <div class="mate-input-box" style="z-index:9999">
                <label style="margin_top:0px !important;" for="">Surgery Date</label>
                <div class="clearfix"></div>
                <input type="text" name="surgery_date" autocomplete="off" value="{{ date('M-d-Y') }}"
                    class="form-control datepicker" id="surgery_date" placeholder="From Date">
            </div>
        </div>
        <div class="col-md-4">
            <div class="mate-input-box" style="z-index:9999">
                <label style="margin_top:0px !important;" for="">Surgery Time</label>
                <div class="clearfix"></div>
                    <input type="text" name="surgery_time" id="surgery_time" value="" class="form-control timepicker timepickerclass" data-attr="date" placeholder="Time" style="">
            </div>
        </div>
        <div class="col-md-8">
            <div class="mate-input-box" style="z-index:9999">
                <label  style="margin_top:0px !important;" for="nursing_station">Nursing Station</label>
                {!! Form::select('nursing_station', $nursing_stations, '', ['class' => 'form-control select2', 'title' => 'Nursing Station', 'id' => 'nursing_station', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;']) !!}
            </div>
        </div>
        <div class="col-md-12">
            <div class="mate-input-box">
                <label  style="margin_top:0px !important;" for="anesthesia"> Anesthesia</label>
                {!! Form::select('anesthesia', $anaesthia_list, '', ['class' => 'form-control select2', 'title' => 'Anesthesia', 'id' => 'anesthesia', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;']) !!}
            </div>
        </div>
        <div class="col-md-12" style="height:100px;">
            <div class="mate-input-box" style="height:95px !important;">
                <label  style="margin_top:0px !important;" for="anesthesia"> Surgeon</label>
                {!! Form::select('surgeon', $doctor_list,0, ['class' => 'form-control select2', 'title' => 'Surgeon', 'id' => 'surgeon','multiple' => 'multiple-select', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;']) !!}
            </div>
        </div>
        <div class="col-md-12" style="height:100px;">
            <div class="mate-input-box" style="height:95px !important;">
                <label  style="margin_top:0px !important;" for="anesthetist"> Anesthetist</label>
                {!! Form::select('anesthetist', $anesthetist_list,0, ['class' => 'form-control select2', 'title' => 'Anesthetist','multiple' => 'multiple-select', 'id' => 'anesthetist', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;']) !!}
            </div>
        </div>


        <div class="col-md-12" style="height:100px;">
            <div class="mate-input-box" style="height:95px !important;">
                <label  style="margin_top:0px !important;" for="anesthetist">Diagnosis</label>
                <textarea name="diagnosis" id="diagnosis" style="height:75px !important" class="form-control" placeholder="Diagnosis"></textarea>
            </div>
        </div>
        <div class="col-md-12" style="height:100px;">
            <div class="mate-input-box primary_surgery_search_container" style="height:95px !important;">
                <label style="margin_top:0px !important;" for="primary_surgery_id">Surgery</label>
                {!! Form::select('primary_surgery_id', $surgery_master, '', ['class' => 'form-control select2 filters','multiple' => 'multiple-select', 'title' => 'Select Primary Surgery', 'id' => 'primary_surgery_id', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;z-index:99999']) !!}
            </div>
        </div>
        {{-- <div class="col-md-12">
            <div class="mate-input-box">
                <label  style="margin_top:0px !important;" for="surgery_desc">Surgery Description</label>
                <input type="text" name="surgery_desc" id="surgery_desc" class="form-control" placeholder="Surgery Description"/>
            </div>
        </div> --}}
        {{-- <div class="col-md-12">
            <div class="mate-input-box">
                <label  style="margin_top:0px !important;" for="surgery_id">Successive Surgery</label>
                {!! Form::select('surgery_id', $surgery_master, '', ['class' => 'form-control select2 filters', 'multiple' => 'multiple-select', 'title' => 'Select Procedure', 'id' => 'surgery_id', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;z-index:99999']) !!}
            </div>
        </div> --}}

        <div class="col-md-12" style="margin-top:70px !important;">
            <button type="button" class="btn bg-green pull-right" onclick="NewSurgeryRequest();" id="add_surgery_request">
                <i class="fa fa-save"></i> Save
            </button>
            <button type="reset" class="btn bg-orange pull-right" id="reset_surgery_request">
                <i class="fa fa-save"></i> Reset
            </button>
        </div>
    </div>
    <div class="col-md-8 box-body" style="height:700px;">
        <div class="col-md-12">
            <label class="pull-right"><h6><b>Select Special Instruments</b></h6></label>
        </div>
        <div class="col-md-12" id="instrument_list">

        </div>
    </div>
</div>

{!! Form::token() !!} {!! Form::close() !!}
