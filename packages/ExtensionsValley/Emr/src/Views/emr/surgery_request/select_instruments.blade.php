<table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
    style="font-size: 12px;">
    <thead>
        <tr class="common_table_header">
            <th class="common_td_rules" width="5%">Select</th>
            <th class="common_td_rules" width="95%">
                <input id="issue_search_box" onkeyup="searchbyName();" type="text" placeholder="Name Search.. "
                    style="color: #000;display: none;width: 22%;">
                <span id="item_search_btn" style="padding: 0px 5px;" class="btn btn-warning"><i class="fa fa-search"></i></span>
                <span id="item_search_btn_text">Instrument Name</span>
            </th>

        </tr>
    </thead>
    <tbody>
        @foreach ($instruments as $row)
            <tr>

                <td>
                    <div class="checkbox checkbox-success inline">
                        <input class="instruments" id="select" type="checkbox" value="{{$row->code}}" name="checkbox">
                        <label class="text-blue" for="checkbox">
                        </label>
                    </div>
                </td>
                <td class="common_td_rules">{{$row->name}}</td>
            </tr>
        @endforeach
    </tbody>

</table>
