<table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
    style="font-size: 12px;">
    <thead>
        <tr class="common_table_header">
            <th class="common_td_rules" >Slno</th>
            <th class="common_td_rules" >Surgery Date</th>
            <th class="common_td_rules" >Surgery</th>
            <th class="common_td_rules" >Anesthesia</th>
            <th class="common_td_rules" >Surgeon</th>
            <th class="common_td_rules" >Anaesthetist</th>
            <th class="common_td_rules" >Diaganosis</th>
            <th class="common_td_rules" >Instruments</th>
            <th class="common_td_rules" >Actions</th>
        </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach ($result as $data)
            <tr>
                <td>{{$i}}</td>
               <td>{{date('M-d-Y',strtotime($data->surgery_date))}}</td>
               <td></td>
               <td>{{$data->anesthesia}}</td>
               <td>{{$data->surgeon}}</td>
               <td>{{$data->anaesthetist}}</td>
               <td>{{$data->diaganosis}}</td>
               <td></td>
               <td>
                   <button type="btn btn-sm bg-green" onclick="edit_surgery_request('{{$data->id}}')"><i class="fa fa-edit"></i></button>
                     <button type="btn btn-sm bg-red" onclick="delete_surgery_request('{{$data->id}}')"><i class="fa fa-trash"></i></button>
               </td>
            </tr>
            @php
            $i++;
        @endphp
        @endforeach
    </tbody>

</table>
