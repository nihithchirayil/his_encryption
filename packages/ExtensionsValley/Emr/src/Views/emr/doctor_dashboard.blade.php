@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-list.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-slide.css') }}" rel="stylesheet">
    <style>
        .right_btn_widget ul li button.btn {
            background: #9ceed5 !important;
            color: #000;
        }

        .waiting_time {
            color: white;
            background: #7495d2;
            min-width: 57px;
            text-align: center;
        }

        .light_purple_bg {
            padding: 0px 0px;
        }

        .badge_purple_bg {
            color: white;
            background: #33415a;
        }

        .right_btn_widget .badge_purple_bg {
            color: white;
            background: #33415a;
        }

        /* .pat_det_img {
                                    font-size: 36px;
                                    padding-left: 7px;
                                    } */
        .patient_det_widget {
            float: left;
            width: 100%;
            border: 1px solid #CCC;
            color: black;
        }

        .seenpatient {
            background: rgb(167, 224, 186) !important;
            background: linear-gradient(90deg, rgba(233, 251, 239, 1) 0%, rgba(247, 250, 247, 1) 45%, rgba(255, 255, 255, 1) 100%) !important;
        }

        .notseenpatient {
            background: rgb(251, 245, 245) !important;
            background: linear-gradient(90deg, rgba(251, 245, 245, 1) 0%, rgba(247, 250, 247, 1) 45%, rgba(255, 255, 255, 1) 100%) !important;
        }

        .notseenpatient:hover {
            /* background: rgb(243, 236, 236) !important; */

        }

        .seenpatient:hover {
            /* background: rgb(245, 252, 244) !important; */
        }

        .patient_image_contianer {
            width: 52px;
            height: 52px;
        }

        .patient_image_contianer:hover {
            transform: scale(1.5);
            transition: all .2s ease-in-out;
            z-index: 9999999;
            position: absolute;
            margin-left: 20px;
            border-radius: 2px;
        }

        .btn-group>.active {
            background-color: #8afcd6;
            color: #5b7b85;
        }

        .bg-purple>.active {
            background-color: #fd5bf5;
            color: black;
        }

        .btn-group>.btn-success:hover {
            background-color: #8afcd6;
            color: #5b7b85;
        }

        /* .ajaxSearchBox {
            display: none;
            text-align: left;
            list-style: none;
            cursor: pointer;
            max-height: 350px;
            margin: -2px 0px 0px 0px;
            overflow-y: auto;
            width: auto;
            z-index: 99999;
            position: absolute;
            background: #ffffff;
            border-radius: 3px;
            border: 1px solid rgba(0, 0, 0, 0.3);
        } */


        .ajaxSearchBox{
        z-index: 9999 !important;
    }
    .ajaxSearchBox>li{
        font-size:13px !important;
        font-family: Arial !important;

    }
    .liHover{
        background: linear-gradient(90deg, #18dc9d, #02cf99, #00c194, #00b48e, #00a788, #009a81, #058d7a, #118072) !important;
    }

        .searched_items a {
            color: #555;
        }

        .inbox_count {
            position: absolute;
            z-index: 10;
            font-size: 11px;
            border-radius: 3px;
            font-weight: 700;
            text-align: center;
            left: -8px;
            top: -7px;
            padding: 2px;
            background-color: #ff9800;
        }

        .dr_appointments_table>tbody>tr:hover {
            background-color: #fcfcbf !important;
        }

        .appointment_invalid {
            background-color: #ccc0c0;
        }

        .appointment_booked {
            background-color: #cfecfc;
        }

        .appointment_checked_in {
            background-color: #92f0b6;
        }

        li.searched_items_discharge:hover {
            background: #cacaca !important;
        }

        label {
            font-weight: 300;
            font-size: 13px;
            color: #847f7f;
        }



        #container {
            max-width: 1024px;
            margin: auto;
        }

        #monitor {
            background: #000;
            position: relative;
            border-top: 3px solid #888;
            margin: 5%;
            padding: 2% 2% 4% 2%;
            border-radius: 10px;
            border-bottom-left-radius: 50% 2%;
            border-bottom-right-radius: 50% 2%;
            transition: margin-right 1s;
        }

        #monitor:after {
            content: '';
            display: block;
            position: absolute;
            bottom: 3%;
            left: 36%;
            height: .5%;
            width: 28%;
            background: #ddd;
            border-radius: 50%;
            box-shadow: 0 0 3px 0 white;
        }

        #monitorscreen {
            position: relative;
            background-color: white;
            background-size: cover;
            background-position: top center;
            height: 0;
            padding-bottom: 52%;
            position: relative;
            overflow: hidden;
            box-shadow: 0px 19px 15px #2a3f54;
        }

        .current_token_monitor {
            color: #399f62;
            font-size: 24px;
            text-align: center;
            border: 1px solid #ececec;
            box-shadow: 3 8 black;
            box-shadow: 5px 3px 5px #2a3f54;
            width: auto;
            margin: 5px;
            margin-top: 15px;
        }


        @media all and (min-width: 960px) {
            #monitor {
                -webkit-animation: tvflicker .2s infinite alternate;
                -moz-animation: tvflicker .5s infinite alternate;
                -o-animation: tvflicker .5s infinite alternate;
                animation: tvflicker .5s infinite alternate;
            }

            @-webkit-keyframes tvflicker {
                0% {
                    box-shadow: 0 0 100px 0 rgba(200, 235, 255, 0.4);
                }

                100% {
                    box-shadow: 0 0 95px 0 rgba(200, 230, 255, 0.45);
                }
            }

            @-moz-keyframes tvflicker {
                0% {
                    box-shadow: 0 0 100px 0 rgba(225, 235, 255, 0.4);
                }

                100% {
                    box-shadow: 0 0 60px 0 rgba(200, 220, 255, 0.6);
                }
            }

            @-o-keyframes tvflicker {
                0% {
                    box-shadow: 0 0 100px 0 rgba(225, 235, 255, 0.4);
                }

                100% {
                    box-shadow: 0 0 60px 0 rgba(200, 220, 255, 0.6);
                }
            }

            @keyframes tvflicker {
                0% {
                    box-shadow: 0 0 100px 0 rgba(225, 235, 255, 0.4);
                }

                100% {
                    box-shadow: 0 0 60px 0 rgba(200, 220, 255, 0.6);
                }
            }
        }

        .emergency_marquee {
            height: 50px;
            overflow: hidden;
            position: relative;
        }

        .emergency_marquee span {
            font-size: 1em;
            color: #d43f3a;
            position: absolute;
            width: 100%;
            height: auto;
            margin: 0;
            line-height: 10px;
            text-align: center;
            white-space: nowrap;
            /* Starting position */
            -moz-transform: translateX(100%);
            -webkit-transform: translateX(100%);
            transform: translateX(100%);
            /* Apply animation to this element */
            -moz-animation: emergency_marquee 15s linear infinite;
            -webkit-animation: emergency_marquee 15s linear infinite;
            animation: emergency_marquee 15s linear infinite;
        }

        /* Move it (define the animation) */
        @-moz-keyframes emergency_marquee {
            0% {
                -moz-transform: translateX(100%);
            }

            100% {
                -moz-transform: translateX(-100%);
            }
        }

        @-webkit-keyframes emergency_marquee {
            0% {
                -webkit-transform: translateX(100%);
            }

            100% {
                -webkit-transform: translateX(-100%);
            }
        }

        @keyframes emergency_marquee {
            0% {
                -moz-transform: translateX(100%);
                /* Firefox bug fix */
                -webkit-transform: translateX(100%);
                /* Firefox bug fix */
                transform: translateX(100%);
            }

            100% {
                -moz-transform: translateX(-100%);
                /* Firefox bug fix */
                -webkit-transform: translateX(-100%);
                /* Firefox bug fix */
                transform: translateX(-100%);
            }
        }

        .patient_list_tile {
            border-radius: 5px !important;
            background-color:
                /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#b4ddb4+0,83c783+4,52b152+33,36a693+68,36a693+95,36a693+100 */
                background: #b4ddb4;
            /* Old browsers */
            background: -moz-linear-gradient(top, #b4ddb4 0%, #83c783 4%, #52b152 33%, #36a693 68%, #36a693 95%, #36a693 100%);
            /* FF3.6-15 */
            background: -webkit-linear-gradient(top, #b4ddb4 0%, #83c783 4%, #52b152 33%, #36a693 68%, #36a693 95%, #36a693 100%);
            /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, #b4ddb4 0%, #83c783 4%, #52b152 33%, #36a693 68%, #36a693 95%, #36a693 100%);
            /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#b4ddb4', endColorstr='#36a693', GradientType=0);
            /* IE6-9 */

        }

        .box-body {
            background-color: #fff !important;
        }
        .patient_seen{
            background-color: #b5fbba !important;
        }

    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">

        <div class="col-sm-12 box no-margin no-border" style="margin:0px">
            <div class="nav-tabs-custom blue_theme_tab no-margin">
                <ul class="nav nav-tabs sm_nav text-center no-border" style="background-color:white">
                    @if(isset($radiology_flg) && $radiology_flg == 1)
                    @php $radio_class = 'active'; $normal_clas = '';$normal_clas_tab = ''; @endphp
                    @else
                    @php $radio_class = ''; $normal_clas = 'active';$normal_clas_tab = 'active in'; @endphp
                    @endif
                    <li class="{{$normal_clas}}"> <a style="padding: 4px 8px;" href="#tab1" data-toggle="tab"
                            aria-expanded="true">OP List</a></li>
                    <li class=""> <a style="padding: 4px 8px;" href="#tab2" data-toggle="tab"
                            aria-expanded="false">IP List</a></li>
                    @if(isset($radiology_flg) && $radiology_flg == 1)
                    <li class="{{$radio_class}}"> <a style="padding: 4px 8px;" href="#tab6" data-toggle="tab"
                            aria-expanded="false">Radiology</a></li>
                    @endif
                    <li class=""> <a style="padding: 4px 8px;" href="#tab4" data-toggle="tab"
                            aria-expanded="false" onclick="getHealthPackagePatientList();">Health Package</a></li>
                    <li class=""> <a style="padding: 4px 8px;" href="#tab3" data-toggle="tab"
                            aria-expanded="false">Todo List</a></li>

                    @if($usg_verfy_status == 1)
                    <li class=""> <a onclick="usg_report_search();" style="padding: 4px 8px;" href="#tab5" data-toggle="tab"
                            aria-expanded="false">Reports Verification</a></li>
                    @endif

                </ul>
                <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
                <input type="hidden" id="doctor_id" value="{{ $doctor_id }}">
                <input type="hidden" id="app_url" value="{{ $app_url }}">
                <input type="hidden" id="enable_emr_lite_version" value="{{ $enable_emr_lite_version }}">
                <div class="tab-content">
                    <div class="tab-pane fade {{$normal_clas_tab}}" id="tab1">
                        <div class="box no-margin no-border">
                            <div class="box-body bg-white clearfix">
                                <div class="col-md-12 padding_sm">
                                    <div class="box no-border no-margin anim">
                                        <div class="box-body clearfix">
                                            <div class="col-md-2 padding_sm">
                                                <div class="" data-toggle="buttons" style="margin-top:24px;">
                                                    <label class=" " style="margin-right:4px;">
                                                        <input type="radio" id="ip_name_check" value="1" name="search_type"
                                                            checked="checked"> Name
                                                    </label>
                                                    <label class=" " style="margin-right:4px;">
                                                        <input type="radio" id="opnumber_checkbox" value="2"
                                                            name="search_type" checked=""> UHID
                                                    </label>
                                                    <label class=" active">
                                                        <input type="radio" id="phone_checkbox" value="3" name="search_type"
                                                            checked="checked"> Phone
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-2 padding_sm">

                                                <label for="">Search Patients</label>
                                                <div class="clearfix"></div>
                                                <div class="input-group">
                                                    <input type="text" name="patient_search_txt" id="patient_search_txt"
                                                        onkeyup="search_patient(this.value);" class="form-control">
                                                    <div class="input-group-btn">
                                                        <button style="height: 23px;
                                                                            width: 30px;" class="btn light_purple_bg"><i
                                                                class="fa fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="col-md-2 padding_sm" style="width:185px;">
                                                <label for="">Global Patient Search</label>
                                                <div class="clearfix"></div>
                                                {{-- <div class="input-group"> --}}
                                                <input type="text" id='patient_general_search_txt'
                                                    onkeyup="patient_general_search(this.value);" class="form-control"
                                                    value="">
                                                {{-- <div class="input-group-btn">
                                        <button style="height: 23px;
                                        width: 30px;" class="btn light_purple_bg">
                                        <i class="fa fa-search" onclick="patient_general_search();"></i></button>
                                    </div> --}}
                                                <div style="width:300px; display: none;z-index: 9999; min-height: 250px;"
                                                    id="completepatientbox" class="ajaxSearchBox">
                                                    {{-- <li class="searched_items"></li> --}}
                                                </div>
                                                {{-- </div> --}}
                                            </div>
                                            <div class="col-md-2 padding_sm">
                                                <label for="">Search By Date</label>
                                                <div class="clearfix"></div>
                                                <div class="input-group">
                                                    <input type="text" id='op_patient_search_date'
                                                        onblur="show_op_patients_list();" class="form-control datepicker"
                                                        value="{{ $search_date_web }}" data-attr="date"
                                                        placeholder="Date">
                                                    <div class="input-group-btn">
                                                        <button style="height: 23px;
                                                                            width: 30px;" class="btn light_purple_bg">
                                                            <i class="fa fa-search"
                                                                onclick="show_op_patients_list();"></i></button>
                                                    </div>
                                                </div>
                                            </div>

                                            @php
                                                $sort_by = [
                                                    1 => 'Token Wise',
                                                    2 => 'Patient Name (A-Z)',
                                                    3 => 'Patient Name (Z-A)',
                                                ];
                                            @endphp

                                            <div class="col-md-2 padding_sm">
                                                <label for="">Sort By</label>
                                                <div class="clearfix"></div>
                                                {!! Form::select('sort_by', $sort_by, $doctor_id, ['class' => 'form-control', 'id' => 'sort_by', 'onchange' => 'show_op_patients_list();', 'style' => ' color:#555555; padding:4px 12px;']) !!}
                                            </div>


                                            @php
                                                $user_id = \Auth::user()->id;
                                                $doctor = \DB::table('doctor_master')->where('user_id', $user_id)->select('speciality', 'id')->get();
                                                $speciality_id = $doctor[0]->speciality;
                                                $doctor_id = $doctor[0]->id;
                                                $grouped_doctor_list = [];
                                                if(env('DOCTOR_WISE_GROUP_LOGIN', 0) == 0){
                                                    $chk_dr_exisits = \DB::table('login_speciality_map')
                                                        ->where('doctor_id', $doctor_id)
                                                        ->first();
                                                    if (!empty($chk_dr_exisits)) {
                                                        $grouped_doctor_list = \DB::table('doctor_master')
                                                            ->join('login_speciality_map', 'login_speciality_map.doctor_id', '=', 'doctor_master.id')
                                                            ->where('login_speciality_map.speciality_id', $speciality_id)
                                                            ->where('login_speciality_map.status', 1)
                                                            ->distinct()
                                                            ->orderBy('doctor_master.id')
                                                            ->pluck('doctor_master.doctor_name', 'doctor_master.id');
                                                    }
                                                }else {
                                                    $chk_dr_exisits = \DB::table('doctor_wise_group_login')
                                                        ->where('doctor_id', $doctor_id)
                                                        ->whereNull('deleted_at')
                                                        ->first();
                                                    if (!empty($chk_dr_exisits)) {
                                                        $group_parent_doctor_id = \DB::table('doctor_wise_group_login')->where('doctor_id', $doctor_id)->whereNull('deleted_at')->value('parent_doctor_id');
                                                        $grouped_doctor_list = \DB::table('doctor_master')
                                                            ->join('doctor_wise_group_login', 'doctor_wise_group_login.doctor_id', '=', 'doctor_master.id')
                                                            ->where('doctor_wise_group_login.parent_doctor_id', $group_parent_doctor_id)
                                                            ->orderBy('doctor_master.id')
                                                            ->pluck('doctor_master.doctor_name', 'doctor_master.id');
                                                    }
                                                }

                                            @endphp
                                            @if (count($grouped_doctor_list) > 0)
                                                <div class="col-md-2 padding_sm pull-right">
                                                    <!-- <div class="input-group">
                                                                        <input type="checkbox" class="my_own_visit" value="1" onchange="show_op_patients_list();"> My Own Patients
                                                                    </div> -->
                                                    <label for="">Select Doctor</label>
                                                    <div class="clearfix"></div>
                                                    {!! Form::select('group_doctor', $grouped_doctor_list, $doctor_id, ['class' => 'form-control', 'id' => 'group_doctor', 'onchange' => 'show_op_patients_list();', 'style' => ' color:#555555; padding:4px 12px;']) !!}
                                                </div>
                                            @endif



                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 padding_sm">

                                    <div class="clearfix"></div>
                                    <div class="ht10"></div>

                                    <div id="show_op_patient_list_html">
                                        <div class="col-md-6 padding_sm">
                                            <div class="theadscroll" style="position:relative; height:510px;">
                                                <table class="table no-margin no-border theadfix_wrapper">
                                                    <thead>
                                                        <tr class="bg-default patient_list_tile" style="color:white">
                                                            <th class="text-center">Not Seen &nbsp;<span
                                                                    class="badge badge_purple_bg not_seen_count">0</span>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding: 0px;">
                                                                <div class="col-md-12 padding-sm"
                                                                    style="padding-right: 20px !important;">
                                                                    @php
                                                                        $not_seen_count = 0;
                                                                    @endphp
                                                                    @foreach ($op_patients as $data)
                                                                        @if ($data->dr_seen_status == 0)
                                                                            @php
                                                                                $shift_data = '';
                                                                                $not_seen_count = $not_seen_count + 1;
                                                                                $class = '';
                                                                                if ($data->share_holder_status != 0) {
                                                                                    $class .= ' share_holder_status';
                                                                                }
                                                                                if ($data->is_highrisk != 0) {
                                                                                    $class .= ' is_highrisk';
                                                                                }
                                                                                if ($data->is_mlc != 0) {
                                                                                    $class .= ' is_mlc';
                                                                                }
                                                                                if ($data->referred_to_status != 0) {
                                                                                    $class .= ' referred_to_status';
                                                                                }
                                                                                if ($data->health_checkup != 0) {
                                                                                    $class .= ' health_checkup';
                                                                                }
                                                                                if ($data->is_walkin != 0) {
                                                                                    $class .= ' is_walkin';
                                                                                }
                                                                                if ($data->session_id == 1) {
                                                                                    $shift_data = 'Morning';
                                                                                } elseif ($data->session_id == 2) {
                                                                                    $shift_data = 'Evening';
                                                                                }
                                                                            @endphp
                                                                            <div data-app-doctor-id="{{ $data->doctor_id }}"
                                                                                data-uhid="{{ str_replace(' ', '', $data->uhid) }}"
                                                                                data-phone="{{ str_replace(' ', '', $data->mobile_no) }}"
                                                                                data-patientname="{{ str_replace(' ', '', $data->patient_name) }}"
                                                                                onclick="goToSelectedPatient('{{ $data->patient_id }}');"
                                                                                class=" box-body notseenpatient patient_det_widget card {{ $class }}"
                                                                                style="margin:2px;cursor: pointer;border:0px !important;border-radius:3px;margin-bottom:5px; border:1px solid rgb(111, 190, 135) !important;margin-right:5px !important;box-shadow: 2px 2px 3px 1px rgba(45,150,51,0.31);
                                                                                        -webkit-box-shadow: 2px 2px 3px 1px rgba(45,150,51,0.31);
                                                                                        -moz-box-shadow: 2px 2px 3px 1px rgba(45,150,51,0.31);">

                                                                                <div class="pt_container"
                                                                                    style="color: #beafa5;">
                                                                                    <table
                                                                                        class="table no-margin no-border table_sm">
                                                                                        <tbody>

                                                                                            <tr>
                                                                                                <td width="35px">
                                                                                                    @if ($data->patient_image != '')

                                                                                                        <img class="patient_image_contianer"
                                                                                                            src="data:image/jpg;base64,{!! $data->patient_image !!}" />

                                                                                                    @else
                                                                                                        <div class="pat_det_img"
                                                                                                            style="color: #beafa5;">
                                                                                                            <i
                                                                                                                class="fa fa-user"></i>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                    <span
                                                                                                        class="token_time">{{ trim($data->token_no_prefix) }}{{ trim($data->token_no) }}</span>
                                                                                                </td>
                                                                                                <td valign="top">

                                                                                                    <table
                                                                                                        class="table no-margin table_sm pt_container pt"
                                                                                                        style="background:#fffdfc;font-size:12px !important;font-weight:600;color:rgb(63, 62, 62);margin-top: 5px !important;">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td
                                                                                                                    style="width:25%;">
                                                                                                                    {{ ucwords(strtolower($data->patient_name)) }}
                                                                                                                </td>
                                                                                                                <td
                                                                                                                    style="width:20%">
                                                                                                                    {{ $data->age }}
                                                                                                                    /
                                                                                                                    {{ $data->gender }}
                                                                                                                </td>
                                                                                                                <td
                                                                                                                    style="width:55%;padding-top:3px !important;">

                                                                                                                    <span
                                                                                                                        class="badge_new">{{ $data->visit_type }}</span>


                                                                                                                    @if ($data->waiting_time < 1440)
                                                                                                                        @if ($data->waiting_time < 0)

                                                                                                                        @elseif($data->waiting_time
                                                                                                                            >
                                                                                                                            60)
                                                                                                                            <span
                                                                                                                                title="waiting time"
                                                                                                                                class="badge_new">
                                                                                                                                {{ round($data->waiting_time / 60) }}h
                                                                                                                            </span>
                                                                                                                        @else
                                                                                                                            <span
                                                                                                                                title="waiting time"
                                                                                                                                class="badge_new">
                                                                                                                                {{ round($data->waiting_time) }}m
                                                                                                                            </span>
                                                                                                                        @endif
                                                                                                                    @endif

                                                                                                                    <span
                                                                                                                        title="sloat time"
                                                                                                                        class="badge_new">{{ date('h:i A', strtotime($data->time_slot)) }}</span>


                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td>{{ $data->uhid }}
                                                                                                                </td>
                                                                                                                <td>{{ $data->mobile_no }}
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <div
                                                                                                                        class="flag_list">
                                                                                                                        <ul class="list-unstyled no-padding no-margin"
                                                                                                                            style=";margin-top:2px !important;">

                                                                                                                            @if ($data->share_holder_status != 0)
                                                                                                                                <li class="bg-orange"
                                                                                                                                    title="Share holder">
                                                                                                                                    S
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($data->is_highrisk != 0)
                                                                                                                                <li class="bg-red"
                                                                                                                                    title="High Risk">
                                                                                                                                    H
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($data->is_mlc != 0)
                                                                                                                                <li style="background-color:#6effc3"
                                                                                                                                    title="MLC">
                                                                                                                                    M
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($data->referred_to_status != 0)
                                                                                                                                <li style="background-color:#75dfff"
                                                                                                                                    title="Doctor">
                                                                                                                                    R
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($data->health_checkup != 0)
                                                                                                                                <li style="background-color:#5b7b85;color:white"
                                                                                                                                    title="MLC">
                                                                                                                                    H
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($data->is_walkin != 0)
                                                                                                                                <li style="background-color:#fd5bf5;color:white"
                                                                                                                                    title="MLC">
                                                                                                                                    W
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($shift_data != '')
                                                                                                                                <li style="background-color:#f37ea9;color:white; line-height: 1.5; width: 55px;"
                                                                                                                                    title="{{ $shift_data }}">
                                                                                                                                    {{ $shift_data }}
                                                                                                                                </li>
                                                                                                                            @endif
                                                                                                                        </ul>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @endforeach
                                                                    <input type="hidden" class="totalNotSeenCount"
                                                                        value="{{ $not_seen_count }}" />
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </tbody>

                                                </table>

                                            </div>

                                        </div>

                                        <div class="col-md-6 padding_sm">
                                            <div class="theadscroll" style="position:relative; height:510px;">
                                                <table class="table no-margin no-border theadfix_wrapper">
                                                    <thead>
                                                        <tr class="bg-default patient_list_tile" style="color:white">
                                                            <th class="text-center">Seen &nbsp;<span
                                                                    class="badge badge_purple_bg seen_count">0</span></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding: 0px;">
                                                                <div class="col-md-12 padding-sm"
                                                                    style="padding-right: 20px !important;">
                                                                    @php
                                                                        $seen_count = 0;
                                                                    @endphp
                                                                    @foreach ($op_patients as $data)
                                                                        @if ($data->dr_seen_status == 1)
                                                                            @php
                                                                                $seen_count = $seen_count + 1;
                                                                                $class = '';
                                                                                $shift_data = '';
                                                                                if ($data->share_holder_status != 0) {
                                                                                    $class .= ' share_holder_status';
                                                                                }
                                                                                if ($data->is_highrisk != 0) {
                                                                                    $class .= ' is_highrisk';
                                                                                }
                                                                                if ($data->is_mlc != 0) {
                                                                                    $class .= ' is_mlc';
                                                                                }
                                                                                if ($data->referred_to_status != 0) {
                                                                                    $class .= ' referred_to_status';
                                                                                }
                                                                                if ($data->health_checkup != 0) {
                                                                                    $class .= ' health_checkup';
                                                                                }
                                                                                if ($data->is_walkin != 0) {
                                                                                    $class .= ' is_walkin';
                                                                                }
                                                                                if ($data->session_id == 1) {
                                                                                    $shift_data = 'Morning';
                                                                                } elseif ($data->session_id == 2) {
                                                                                    $shift_data = 'Evening';
                                                                                }
                                                                            @endphp
                                                                            <div data-app-doctor-id="{{ $data->doctor_id }}"
                                                                                data-uhid="{{ str_replace(' ', '', $data->uhid) }}"
                                                                                data-phone="{{ str_replace(' ', '', $data->mobile_no) }}"
                                                                                data-patientname="{{ str_replace(' ', '', $data->patient_name) }}"
                                                                                onclick="goToSelectedPatient('{{ $data->patient_id }}');"
                                                                                class=" box-body notseenpatient patient_det_widget card {{ $class }}"
                                                                                style="margin:2px;cursor: pointer; border:1px solid rgb(111, 190, 135) !important;margin-right:5px !important;box-shadow: 2px 2px 3px 1px rgba(1, 36, 3, 0.31);
                                                                                        -webkit-box-shadow: 2px 2px 3px 1px rgba(45,150,51,0.31);
                                                                                        -moz-box-shadow: 2px 2px 3px 1px rgba(45,150,51,0.31);">

                                                                                <div class="pt_container"
                                                                                    style="color: #beafa5;">
                                                                                    <table
                                                                                        class="table no-margin no-border table_sm">
                                                                                        <tbody>

                                                                                            <tr>
                                                                                                <td width="35px">
                                                                                                    @if ($data->patient_image != '')

                                                                                                        <img class="patient_image_contianer"
                                                                                                            src="data:image/jpg;base64,{!! $data->patient_image !!}" />

                                                                                                    @else
                                                                                                        <div class="pat_det_img"
                                                                                                            style="color: #beafa5;margin-top:5px;margin-bottom:5px;">
                                                                                                            <i
                                                                                                                class="fa fa-user"></i>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                    <span
                                                                                                        class="token_time">{{ trim($data->token_no_prefix) }}{{ trim($data->token_no) }}</span>
                                                                                                </td>
                                                                                                <td valign="top">

                                                                                                    <table
                                                                                                        class="table no-margin table_sm pt_container pt"
                                                                                                        style="background:#fffdfc;font-size:12px !important;font-weight:600;color:rgb(63, 62, 62);margin-top: 5px !important;">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td
                                                                                                                    style="width:25%;">
                                                                                                                    {{ ucwords(strtolower($data->patient_name)) }}
                                                                                                                </td>
                                                                                                                <td
                                                                                                                    style="width:20%">
                                                                                                                    {{ $data->age }}
                                                                                                                    /
                                                                                                                    {{ $data->gender }}
                                                                                                                </td>
                                                                                                                <td
                                                                                                                    style="width:55%;padding-top:3px !important;">
                                                                                                                    <span
                                                                                                                        class="badge_new">{{ $data->visit_type }}</span>


                                                                                                                    @if ($data->waiting_time < 1440)
                                                                                                                        @if ($data->waiting_time < 0)

                                                                                                                        @elseif($data->waiting_time
                                                                                                                            >
                                                                                                                            60)
                                                                                                                            <span
                                                                                                                                title="waiting time"
                                                                                                                                class="badge_new">
                                                                                                                                {{ round($data->waiting_time / 60) }}h
                                                                                                                            </span>
                                                                                                                        @else
                                                                                                                            <span
                                                                                                                                title="waiting time"
                                                                                                                                class="badge_new">
                                                                                                                                {{ round($data->waiting_time) }}m
                                                                                                                            </span>
                                                                                                                        @endif
                                                                                                                    @endif

                                                                                                                    <span
                                                                                                                        title="sloat time"
                                                                                                                        class="badge_new">{{ date('h:i A', strtotime($data->time_slot)) }}</span>

                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td>{{ $data->uhid }}
                                                                                                                </td>
                                                                                                                <td>{{ $data->mobile_no }}
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <div
                                                                                                                        class="flag_list">
                                                                                                                        <ul class="list-unstyled no-padding no-margin"
                                                                                                                            style=";margin-top:2px !important;">

                                                                                                                            @if ($data->share_holder_status != 0)
                                                                                                                                <li class="bg-orange"
                                                                                                                                    title="Share holder">
                                                                                                                                    S
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($data->is_highrisk != 0)
                                                                                                                                <li class="bg-red"
                                                                                                                                    title="High Risk">
                                                                                                                                    H
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($data->is_mlc != 0)
                                                                                                                                <li style="background-color:#6effc3"
                                                                                                                                    title="MLC">
                                                                                                                                    M
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($data->referred_to_status != 0)
                                                                                                                                <li style="background-color:#75dfff"
                                                                                                                                    title="MLC">
                                                                                                                                    R
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($data->health_checkup != 0)
                                                                                                                                <li style="background-color:#5b7b85;color:white"
                                                                                                                                    title="MLC">
                                                                                                                                    H
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($data->is_walkin != 0)
                                                                                                                                <li style="background-color:#fd5bf5;color:white"
                                                                                                                                    title="MLC">
                                                                                                                                    W
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                            @if ($shift_data != '')
                                                                                                                                <li style="background-color:#f37ea9;color:white; line-height: 1.5; width: 55px;"
                                                                                                                                    title="{{ $shift_data }}">
                                                                                                                                    {{ $shift_data }}
                                                                                                                                </li>
                                                                                                                            @endif

                                                                                                                        </ul>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @endforeach
                                                                    <input type="hidden" class="totalSeenCount"
                                                                        value="{{ $seen_count }}" />
                                                                    <div class="col-md-12">
                                                            </td>
                                                        </tr>
                                                    </tbody>

                                                </table>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-3 padding_sm">
                                    <div class="clearfix"></div>
                                    <div class="ht10"></div>
                                    @if (env('QUEUE_MANAGEMENT_ENABLED', '') == 'TRUE' && ($show_queue_management == '1'))
                                        <div class="right_btn_widget">
                                            <div id="container">
                                                <div id="monitor">
                                                    <div id="monitorscreen">
                                                        <div class="col-md-12 no-padding queue_management_div"
                                                            style="margin-top: 15px;">
                                                            <div class="col-md-6 no-padding">
                                                                <div class="current_token current_token_monitor"></div>
                                                                <div class="clerafix"></div>
                                                                <div class="ht10"></div>
                                                                <div class="selectShiftDiv" style="margin-left:7px;">
                                                                    <select class="form-control form-select form-select-sm selectShift" style="width:97%; box-shadow: 1px 3px 2px #adafb1;">
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 ">
                                                                <div class="row padding_sm">
                                                                    <button type="button" style="text-align:left;"
                                                                        class="next_general_token btn btn-sm btn-primary btn-block ">
                                                                        <i class="fa fa-forward"></i> Next Token
                                                                    </button>
                                                                </div>
                                                                <div class="row padding_sm">
                                                                    <button type="button" style="text-align:left;"
                                                                        class="next_special_token btn btn-success btn-block">
                                                                        <i class="fa fa-forward"></i> Next Special Token
                                                                    </button>
                                                                </div>
                                                                <div class="row padding_sm">
                                                                    <button type="button" style="text-align:left;"
                                                                        class="recall_token btn btn-block btn-warning">
                                                                        <i class="fa fa-refresh"></i> Recall
                                                                    </button>
                                                                </div>
                                                                <div class="row padding_sm">
                                                                    <button type="button" style="text-align:left;"
                                                                        class="emergency_btn btn btn-block btn-danger">
                                                                        <i class="fa fa-bell"></i> Emergency
                                                                    </button>

                                                                    <button type="button"
                                                                        style="text-align:left; display:none;"
                                                                        class="emergency_stop_btn btn btn-block btn-danger">
                                                                        <i class="fa fa-times"></i> Stop Emergency
                                                                    </button>


                                                                </div>
                                                            </div>
                                                            <!-- <div class="emergency_marquee row" style="margin-top: -5px;color: #f51e17;"><span>This is a sample scrolling text that has scrolls texts to left.</span></div> -->
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="clearfix"></div>
                                    <div class="ht10"></div>
                                    <div class="right_btn_list">
                                        <ul class="list-unstyled">
                                            <li>
                                                <div class="but_text">Total Booked</div> <span
                                                    id="total_booked_patients"
                                                    class="badge badge_purple_bg">{{ $total_booked_patients }}</span>
                                            </li>
                                            <li>
                                                <div class="but_text">Seen</div> <span
                                                    class="badge badge_purple_bg seen_count">{{ $total_seen_count }}</span>
                                            </li>
                                            <li>
                                                <div class="but_text">IP Patients</div> <span id="ip_patients_count"
                                                    class="badge badge_purple_bg">{{ $ip_patients_count }}</span>
                                            </li>
                                            <li>
                                                <div class="but_text">Discharge Intimated</div> <span
                                                    id="discharge_intimation_count"
                                                    class="badge badge_purple_bg">{{ $discharge_intimation_count }}</span>
                                            </li>
                                        </ul>



                                        <div class="clearfix"></div>
                                        <div class="ht10"></div>

                                        <div class="right_btn_widget">
                                            <ul class="list-unstyled no-margin no-padding">
                                                <li>
                                                    <button class="btn" onclick="myCalendar();">
                                                        <h5 style="margin:0px"><i class="fa fa-calendar"></i></h5>
                                                        <span style="margin-right: 15px !important;">My Calendar</span>
                                                    </button>
                                                </li>
                                                <li>
                                                    <button class="btn">
                                                        <h5 style="margin:0px"><i style="margin-left: -10px;
                                                                            margin-right: 8px;"
                                                                class="fa fa-medkit"></i>
                                                        </h5>
                                                        Med-Approve<span class="badge badge_purple_bg"
                                                            style="margin-left: 5px;padding-right: 13px;">0</span>
                                                    </button>
                                                </li>
                                                <li>
                                                    <button class="btn showLabResults">
                                                        <h5 style="margin:0px"><i class="fa fa-flask"></i></h5> Lab
                                                        Results <span
                                                            class="badge badge_purple_bg">{{ $current_lab_results1 }}</span>
                                                    </button>
                                                </li>
                                                <li>
                                                    <button type="button" id="showAppointmentsbtn"
                                                        class="btn showAppointments" onclick="getAppointments()"
                                                        style="padding: 9px;">
                                                        <h5 style="margin:7px"><i class="fa fa-book"></i></h5>
                                                        Appointments <span id="patient_statusspan"
                                                            class="badge badge_purple_bg">{{ $patient_status }}</span>
                                                    </button>
                                                </li>
                                                <li>
                                                    <button class="btn">
                                                        <h5 style="margin:0px"><i class="fa fa-exclamation"></i></h5> High
                                                        Risk <span class="badge badge_purple_bg">0</span>
                                                    </button>
                                                </li>
                                                <li>
                                                    <button type="button" class="btn showDischargeSummary"
                                                        style="padding: 9px;">
                                                        <h5 style="margin:7px"><i class="fa fa-tasks"></i></h5><span
                                                            style="white-space: break-spaces;"> Discharge Summary </span>
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>

                                        <!-- <div class="box no-border no-margin">
                                                                <div class="box-footer"
                                                                    style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height: 70px;">

                                                                    <div class="col-md-12 padding_sm">
                                                                        <div class="col-md-12 padding_sm">
                                                                            <div class="col-md-6 padding_sm bg_blue"><span class="but_text">Total Booked</span><span style="float:right;" id="total_booked_patients" class="badge badge_purple_bg">{{ $total_booked_patients }}</span></div>
                                                                            <div class="col-md-6 padding_sm"><span class="but_text">Seen</span><span style="float:right;" class="badge badge_purple_bg seen_count">{{ $total_seen_count }}</span></div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="ht10"></div>

                                                                        <div class="col-md-12 padding_sm">
                                                                            <div class="col-md-6 padding_sm"><span class="but_text">IP Patients</span><span style="float:right;" id="ip_patients_count" class="badge badge_purple_bg">{{ $ip_patients_count }}</span></div>
                                                                            <div class="col-md-6 padding_sm"><span class="but_text">Disch. Intimated</span> <span style="float:right;" id="discharge_intimation_count" class="badge badge_purple_bg">{{ $discharge_intimation_count }}</span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="ht10"></div>
                                    <div class="col-md-12"
                                        style="color: black;margin-left:-10px;width:107% !important;">
                                        <table class="table table-bordered" style="width: 100%;font-size:12px;">
                                            <tr>
                                                <td><button onclick="apply_filter('share_holder_status');" type="button"
                                                        class="btn btn-sm bg-orange">S</button>
                                                    <span>Share Holder</span>
                                                </td>
                                                <td>
                                                    <button onclick="apply_filter('is_highrisk');" type="button"
                                                        class="btn btn-sm bg-red">H</button>
                                                    <span>High Risk</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><button onclick="apply_filter('is_mlc');" type="button"
                                                        class="btn btn-sm" style="background-color:#6effc3">M</button>
                                                    <span>MLC</span>
                                                </td>
                                                <td>
                                                    <button onclick="apply_filter('referred_to_status');" type="button"
                                                        class="btn btn-sm" style="background-color:#75dfff">R</button>
                                                    <span>Doctor Referral</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><button onclick="apply_filter('is_walkin');" type="button"
                                                        class="btn btn-sm"
                                                        style="background-color:#fd5bf5;color:white">W</button>
                                                    <span>Walkin</span>
                                                </td>
                                                <td>
                                                    <button onclick="apply_filter('health_checkup');" type="button"
                                                        class="btn btn-sm"
                                                        style="background-color:#5b7b85;color:white">H</button>
                                                    <span>Health Checkup</span>
                                                </td>
                                            </tr>
                                            <!-- <tr>
                                                                <td colspan="2">
                                                                    <input onclick="apply_filter('all');" type="button"  class="btn btn-sm bg-info pull-right" value="Show all"/>
                                                                </td>
                                                            </tr> -->
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab2">
                        <div class="box no-margin no-border">
                            <div class="box-body bg-white clearfix">
                                <div class="row">
                                    <div class="col-md-12">
                                        @include('Emr::emr.includes.ip_list')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab3">
                        <div class="box no-margin no-border">
                            <div class="box-body bg-white clearfix">
                                <div class="row">
                                    <div class="col-md-12">
                                        @include('Emr::emr.includes.todo_list')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab4">
                        <div class="box no-margin no-border">
                            <div class="box-body bg-white clearfix">
                                <div class="row">
                                    <div class="col-md-12 health_package_list_div">
                                        @include('Emr::emr.includes.health_packages')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(isset($radiology_flg) && $radiology_flg == 1)
                    <div class="tab-pane fade active in" id="tab6">
                        <div class="box no-margin no-border">
                            <div class="box-body bg-white clearfix">
                                <div class="row">
                                    <div class="col-md-12">
                                        @include('Emr::emr.includes.radiology_patient_list')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif

                    <!----ultrasound verification-------------->
                    @if($usg_verfy_status ==1)
                        <div class="tab-pane fade" id="tab5">
                            <div class="box no-margin no-border">
                                <div class="box-body bg-white clearfix">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @include('Emr::emr.includes.usg_list')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>

            </div>
        </div>
    </div>


    </div>

    <!-- /page content -->

    @include('Emr::emr.includes.custom_modals')

@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>

    <script src="{{ asset('packages/extensionsvalley/emr/js/dr_dashboard.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/usg_list.js') }}"></script>

    <script type="text/javascript">

    </script>
@endsection
