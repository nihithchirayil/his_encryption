@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <style>
    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="row" style="text-align: right; font-size: 12px;font-weight: bold;"> {{ $title }}
            <div class="clearfix"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">



                            <div class="col-md-4 padding_sm" style="margin-top: 25px; margin-left: 1160px;">
                                <button data-toggle="modal" data-target="#revenue_division_modal" class="btn bg-primary"><i
                                        class="fa fa-plus"></i> Add</button>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="theadscroll" style="position: relative; height: 350px;">
                            <table
                                class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg" style="cursor: pointer;">
                                        <th width='4%'>Si No</th>
                                        <th>Doctor Name</th>
                                        <th>Division Type</th>
                                        <th>Service</th>
                                        <th>Department</th>
                                        <th>Percentage</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($item) > 0)
                                        @php
                                            $i = 1;

                                        @endphp
                                        @foreach ($item as $item)

                                            <tr style="cursor: pointer;">
                                                <td class="common_td_rules">{{ $i }}</td>
                                                <td class="common_td_rules">{{ $item->doctor_name }}<input type="hidden"
                                                        class="fav_check" name="rev_div_id[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="fav_doctor_name[]"
                                                        value="{{ $item->doctor_id }}"></td>
                                                <td class="common_td_rules">{{ $item->type }}<input type="hidden"
                                                        class="form-control" name="fav_type[]"
                                                        value="{{ $item->rdm_type }}"></td>
                                                <td class="common_td_rules">{{ $item->service_desc }}<input type="hidden"
                                                        class="form-control" name="fav_service_desc[]"
                                                        value="{{ $item->service_desc }}"><input type="hidden"
                                                        class="form-control" name="fav_service_id[]"
                                                        value="{{ $item->item_id }}"></td>
                                                <td class="common_td_rules">{{ $item->dept_name }}<input type="hidden"
                                                        class="form-control" name="fav_dept_name[]"
                                                        value="{{ $item->department_id }}"></td>
                                                <td class="common_td_rules">{{ $item->percentage }}<input type="hidden"
                                                        class="form-control" name="fav_percentage[]"
                                                        value="{{ $item->percentage }}"></td>
                                                <td>
                                                    <button class='btn btn-sm btn-default delete-revenue-division'><i
                                                            class="fa fa-trash"></i></button><button
                                                        class='btn btn-sm btn-default edit-revenue-division'><i
                                                            class="fa fa-edit"></i></button>
                                                </td>
                                            </tr>
                                            @php
                                                $i++;
                                            @endphp
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="12" class="location_code">No Records found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 text-center">
                            <ul class="pagination purple_pagination pull-right">
                                {!! $page_links !!}
                            </ul>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
            </div>
        </div>
    </div>
    <!-- doctor service division modal start -->
    <div id="revenue_division_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Revenue Division</h4>
                </div>
                <div class="modal-body" id="doctor_service_division_details">
                    {!! Form::open(['doctor_service_form', 'id' => 'doctor_service_form']) !!}

                    <input type="hidden" class="form-control" name="edit_rev_div" id="edit_rev_div">

                    {!! Form::label('doctor_id', 'Doctor') !!}
                    {!! Form::select('doctor_id', $doctor_id, null, [
    'class' => 'form-control',
    'placeholder' => 'Select Doctor',
    'id' => 'doctor_id',
]) !!}
                    <br>

                    {!! Form::label('division_type', 'Division Type') !!}
                    <select class="form-control" name="division_type" id="division_type">
                        <option value="">{{ __('Select Division Type') }}</option>
                        <option value="1">{{ __('Item Wise') }}</option>
                        <option value="2">{{ __('Department Wise') }}</option>
                    </select>
                    <br>

                    {!! Form::label('service', 'Service') !!}
                    <input type="text" name="search_service" id="search_service" style="cursor: pointer;" autocomplete="off"
                        class="form-control" placeholder="Type at least 3 characters">
                    <input type="hidden" name="search_service_hidden" id="search_service_hidden" value="">
                    <!-- User List -->

                    <div class="service-list-div" style="display: none; cursor: pointer;">
                        <a style="float: left;" class="close_btn_service_search">X</a>
                        <div class=" service_theadscroll" style="position: relative;">
                            <table id="ServiceTable"
                                class="table table-bordered no-margin table_sm table-striped user_theadfix_wrapper">
                                <thead>
                                    <tr class="light_purple_bg">
                                        <th colspan="2">Service</th>
                                    </tr>
                                </thead>
                                <tbody id="ListServiceSearchData">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>

                    {!! Form::label('department', 'Department') !!}
                    {!! Form::select('department', $department, null, [
    'class' => 'form-control',
    'placeholder' => 'Select Department',
    'id' => 'department',
]) !!}
                    <br>
                    {!! Form::label('percentage', 'Percentage') !!}
                    <input type="text" id='percentage' name='percentage' class="form-control" value=""
                        placeholder="Percentage">
                    <br>


                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-green" onclick="addrevenuedivision()"> <i
                            class="fa fa-check"></i> Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}

    {!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}

    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            setTimeout(function() {
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');

            }, 300);



            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });
            $('.datepicker').datetimepicker({
                format: 'DD-MMM-YYYY'
            });
            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });
            //  $('.date_time_picker').datetimepicker();
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });



        });
    </script>

@stop

@section('javascript_extra')
    <script type="text/javascript">
        function addrevenuedivision() {
            let edit_rev_div = $("#edit_rev_div").val();
            let search_service_hidden = $("#search_service_hidden").val();
            let doctor_id = $("#doctor_id").val();
            let division_type = $("#division_type").val();
            let department = $("#department").val();
            let percentage = $("#percentage").val();
            //alert(search_service_hidden);

            if (doctor_id == '') {
                toastr.warning("Doctor Name Required");
            } else if (division_type == '') {
                toastr.warning("Division Type Required");
            } else if (percentage == '') {
                toastr.warning("Percentage Required");
            } else {

                if (division_type == '1') {
                    if (search_service_hidden == '') {
                        toastr.warning("Service Required");
                        return;
                    }
                }

                if (division_type == '2') {
                    if (department == '') {
                        toastr.warning("Department Required");
                        return;
                    }
                }


                if (edit_rev_div != '' && edit_rev_div != 0) {
                    deleteRevenueDivision(edit_rev_div);
                }

                var url = $('#domain_url').val() + "/revenue_division_management/save-revenue-division";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'doctor_id=' + doctor_id + '&division_type=' + division_type + '&department=' +
                        department + '&percentage=' + percentage + '&search_service_hidden=' +
                        search_service_hidden,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(response) {
                        if (response.status == 1) {

                            Command: toastr["success"]("Saved.");
                            window.location.href = $('#domain_url').val() +
                            "/revenue_division_management/listRevenueDivision/";

                        }
                        else {

                            Command: toastr["error"](response.message);
                        }
                    },
                    complete: function() {
                        $("body").LoadingOverlay("hide");
                    }
                });
            }
        }

        $(document).on('click', '.delete-revenue-division', function() {
            if (confirm("Are you sure you want to delete.!")) {
                let tr = $(this).closest('tr');
                let edit_id = $(tr).find('input[name="rev_div_id[]"]').val();

                if (edit_id != '' && edit_id != 0) {
                    deleteRevenueDivisionFromDb(edit_id);
                    $(tr).remove();
                } else {
                    $(tr).remove();
                }
            }
        });

        //delete single row from db
        function deleteRevenueDivisionFromDb(id) {
            var url = $('#domain_url').val() + "/revenue_division_management/delete-revenue-division";
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id,
                    _token: _token,
                },
                beforeSend: function() {
                    $("body").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(data) {
                    if (data != '' && data != undefined && data != 0) {
                        Command: toastr["success"]("Deleted.");
                    }
                },
                complete: function() {
                    $("body").LoadingOverlay("hide");
                }
            });
        }

        function deleteRevenueDivision(id) {
            var url = $('#domain_url').val() + "/revenue_division_management/delete-revenue-division";
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id,
                    _token: _token,
                },
                beforeSend: function() {

                },
                success: function(data) {

                },
                complete: function() {

                }
            });
        }

        $('#division_type').on('click', function() {
            let division_type = $("#division_type").val();
            let doctor_id = $("#doctor_id").val();
            let search_service_hidden = $("#search_service_hidden").val();
            let department = $("#department").val();
            let percentage = $("#percentage").val();
            if (division_type == '1') {
                $("#department").attr('disabled', true);
                $("#search_service").attr('disabled', false);
            } else if (division_type == '2') {
                $("#search_service").attr('disabled', true);
                $('#department').attr('disabled', false);
                $("#search_service_hidden").val('');
            }
        });

        $(document).on('click', '.edit-revenue-division', function() {

            $('#revenue_division_modal').modal('show');
            let tr = $(this).closest('tr');
            let edit_id = $(tr).find('input[name="rev_div_id[]"]').val();
            let fav_doctor_name = $(tr).find('input[name="fav_doctor_name[]"]').val();
            let fav_type = $(tr).find('input[name="fav_type[]"]').val();
            let fav_dept_name = $(tr).find('input[name="fav_dept_name[]"]').val();
            let fav_percentage = $(tr).find('input[name="fav_percentage[]"]').val();
            let fav_service_desc = $(tr).find('input[name="fav_service_desc[]"]').val();
            let fav_service_id = $(tr).find('input[name="fav_service_id[]"]').val();


            if (fav_doctor_name != '' && edit_id != '') {

                $("#doctor_id option[value=" + fav_doctor_name + "]").attr('selected', 'selected');
                $("#division_type option[value=" + fav_type + "]").attr('selected', 'selected');
                $("#department option[value=" + fav_dept_name + "]").attr('selected', 'selected');
                $('input[name="percentage"]').val(fav_percentage);
                $('input[name="search_service"]').val(fav_service_desc);
                $('input[name="search_service_hidden"]').val(fav_service_id);
                $('input[name="edit_rev_div"]').val(edit_id);

            }
        });


        //user search
        var timeout = null;
        var last_search_string = '';
        $(document).on('keyup', 'input[name="search_service"]', function(event) {
            event.preventDefault();
            /* Act on the event */
            var obj = $(this);
            var search_string = $(this).val();
            var user_list = $('.service-list-div');

            if (search_string == "" || search_string.length < 3) {
                last_search_string = '';
                return false;
            } else {
                $(user_list).show();
                clearTimeout(timeout);
                timeout = setTimeout(function() {
                    if (search_string == last_search_string) {
                        return false;
                    }
                    var url = $('#domain_url').val() + "/revenue_division_management/service-search";
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: {
                            search_key_string: search_string,

                        },
                        beforeSend: function() {
                            $('#ServiceTable > tbody').html(
                                '<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>'
                            );
                        },
                        success: function(data) {
                            //alert(data);
                            let response = data;
                            let res_data = "";


                            var search_list = $('#ListServiceSearchData');


                            if (response.length > 0) {
                                for (var i = 0; i < response.length; i++) {

                                    let service_desc = response[i].service_desc;
                                    let service_id = response[i].service_id;

                                    res_data += '<tr><td>' + service_desc +
                                        '</td><input type="hidden" name="list_service_desc_hid[]" id="list_service_desc_hid-' +
                                        i + '" value="' + service_desc +
                                        '"><input type="hidden" name="list_service_id_hid[]" id="list_service_id_hid-' +
                                        i + '" value="' + service_id + '"></tr>';
                                }
                            } else {
                                res_data =
                                    '<tr class="text-center"><td colspan="2">No Data Found..!</td></tr>';
                            }

                            $(search_list).html(res_data);
                            last_search_string = search_string;
                            $(".user_theadscroll").animate({
                                scrollTop: 0
                            }, "slow");

                        },
                        complete: function() {
                            $('.user_theadfix_wrapper').floatThead("reflow");
                        }
                    });
                }, 500)

            }

        });


        $(document).on('dblclick', '#ListServiceSearchData tr', function(event) {
            event.preventDefault();

            let _token = $('#c_token').val();
            let tr = $(this);
            let name = $(tr).find('input[name="list_service_desc_hid[]"]').val();
            let service_id = $(tr).find('input[name="list_service_id_hid[]"]').val();


            if (name != '' && service_id != '') {
                $('input[name="search_service"]').val(name);
                $('input[name="search_service_hidden"]').val(service_id);

                $(".service-list-div").hide();

            }

        });
    </script>

@endsection
