   <div class="theadscroll" style="position: relative; height: 430px;">
                    <table class="table theadfix_wrapper table-striped table_sm table-condensed styled-table"
                        style="border: 1px solid #CCC;">
                    <thead>
                        <tr class="table_header_bg" style="cursor: pointer;">
                            <th width="30%" style="text-align: left;">Doctor </th>
                            <th width="30%" style="text-align: left;">Service </th>
                            <th width="20%" style="text-align: left;">Percentage</th>
                             <th width="20%" style="text-align: left;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                   
                   
@if (count($data) != 0) 
   
   @foreach ($data as $each)
      
                        <tr >
                            <td style="text-align: left;">{{$each->doctor_name}} <input type="hidden" class="form-control" name="fav_doctor_name[]" value="{{$each->doctor_id}}"> <input type="hidden" class="fav_check" name="dctr_service_id[]" value="{{$each->id}}"></td>
                            <td style="text-align: left;">{{$each->service_desc}} <input type="hidden" class="form-control" name="fav_service_desc[]" value="{{$each->service_id}}"></td>
                            <td style="text-align: left;">{{$each->percentage}} <input type="hidden" class="form-control" name="fav_percentage[]" value="{{$each->percentage}}"></td>
                            <td  style="text-align: left;">
             <i class='fa fa-trash delete-doctor-service'></i> &nbsp<i class='fa fa-edit edit-doctor-service'></i>
        </td>
                        </tr>
                  @endforeach
                @else
                    <tr><td colspan="4" style="text-align: center;"> No Result Found</td></tr>
                              @endif

                    </tbody>
                </table>
        </div>
      

      


