@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset('packages/extensionsvalley/emr/toastr/toastr.min.css')}}" rel="stylesheet">

@endsection
@section('content-area')
    <div class="right_col" role="main">
    <div class="row padding_sm">
    <div class="col-md-1 padding_sm pull-right">
      
    </div>
    <input type="hidden" id="base_url" value="{{URL::to('/')}}">
     <input type="hidden" id="c_token" value="{{csrf_token()}}">
    </div>
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">
                    <div class="col-md-4 padding_sm">
                        {!! Form::label('doctor_id', 'Doctor') !!}
                        {!! Form::select('doctor_id', array("0"=> " Select Doctor") + $doctor_list->toArray(),null, [
                            'class'       => 'form-control',
                            'required'    => 'required'
                        ]) !!}
                    </div>

                    <div class="col-md-4 padding_sm">
                        {!! Form::label('service_id', 'Service') !!}
                        {!! Form::select('service_id', array("0"=> " Select Service") + $service_list->toArray(), null, [
                            'class'       => 'form-control',
                            'required'    => 'required'
                        ]) !!}
                    </div>
                    <div class="col-md-4 padding_sm" style="margin-top: 25px;">
                         <button  data-toggle="modal" data-target="#doctor_service_division_modal" class="btn bg-primary"><i class="fa fa-plus"></i> Add</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="x_panel">
        <div class="row padding_sm" style="min-height: 450px;">
            <div class="col-md-12 padding_sm" id="doctorservicedatadiv">

            </div>
        </div>
        </div>

</div>

<!-- doctor service division modal start -->
 <div id="doctor_service_division_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-green">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Doctor Service Division</h4>
        </div>
        <div class="modal-body" id="doctor_service_division_details">
              {!! Form::open(['doctor_service_form','id'=>'doctor_service_form']) !!}

<input type="hidden" class="form-control" name="edit_dctr_service" id="edit_dctr_service">

                  {!! Form::label('confirmed_doctor', 'Doctor') !!}
                        {!! Form::select('confirmed_doctor', $doctor_list, null, [
                            'class'       => 'form-control',
                             'placeholder' => 'Select Doctor',
                             'id'          => 'confirmed_doctor'
                        ]) !!}
                        <br>

                    {!! Form::label('service', 'Service') !!}
                        {!! Form::select('service', $service_list, null, [
                            'class'       => 'form-control',
                             'placeholder' => 'Select Service',
                             'id'          => 'service'
                        ]) !!}
                          <br>

                  
                  {!! Form::label('percentage', 'Percentage *', []) !!}
                  {!! Form::text('percentage', NULL, ['class'=>'form-control']) !!}

              {!! Form::close() !!}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn bg-green" onclick="addDctrService()"> <i class="fa fa-check"></i> Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
@stop

@section('javascript_extra')
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/doctorservicedivision.js")}}"></script>

@endsection