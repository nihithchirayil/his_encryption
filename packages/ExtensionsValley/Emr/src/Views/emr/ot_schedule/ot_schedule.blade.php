@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/wizard.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/wizard_checklist.css') }}" rel="stylesheet">
    <style>
        .modal-open {
            overflow: hidden !important;
        }
    </style>
@endsection


@section('content-area')
    <div class="right_col">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        <input type="hidden" id="head_id" value="0">
        <input type="hidden" id="surgery_id_hidden" value="0">
        <input type="hidden" id="surgery_detail_id_hidden" value="">

        <input type="hidden" id="parameters_approved_status" value="">
        <input type="hidden" id="pre_op_approved_status" value="">
        <input type="hidden" id="sign_in_approved_status" value="">
        <input type="hidden" id="signout_approved_status" value="">
        <input type="hidden" id="timeout_approved_status" value="">
        <input type="hidden" id="nursing_record_approved_status" value="">
        <input type="hidden" id="signout_approved_status" value="">

        <input type="hidden" id="patient_id_hidden" value="">
        <input type="hidden" id="list_type" value="0">
        <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
        <input type="hidden" id="font_awsome_css_path" value="{{ $font_awsome_css_path }}">
        <input type="hidden" id="bootstrap_css_path" value="{{ $bootstrap_css_path }}">
        <input type="hidden" id="bootstrap_js_path" value="{{ $bootstrap_js_path }}">

        @if(in_array("OT Approval Access", $groupArray))
        <input type="hidden" id="surgery_approval_access" value="1">
        @else
        <input type="hidden" id="surgery_approval_access" value="0">
        @endif



        <div class="col-md-12 pull-right" style="text-align: right; font-size: 12px; font-weight: bold;">
            {{ $title }}</div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <form action="{{ route('extensionsvalley.ot_schedule.OTSchedule') }}" id="categorySearchForm"
                                method="POST">
                                {!! Form::token() !!}
                                <div class="col-md-1 padding_sm">
                                    <div class="mate-input-box">
                                        <label style="top:0px;color:blue;" for="">From Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" name="from_date" autocomplete="off"
                                            value="{{ date('M-d-Y') }}" class="form-control datepicker" id="from_date"
                                            placeholder="From Date">
                                    </div>
                                </div>

                                <div class="col-md-1 padding_sm">
                                    <div class="mate-input-box">
                                        <label style="top:0px;color:blue;" for="">To Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" name="to_date" autocomplete="off" value="{{ date('M-d-Y') }}"
                                            class="form-control datepicker" id="to_date" placeholder="To Date">
                                    </div>
                                </div>


                                <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="" style="color:blue;">Surgeon Name</label>
                                        <div class="clearfix"></div>
                                        <select id="surgeon_name" class="form-control select2"
                                            style="color:#555555; padding:2px 12px;overflow: auto;">
                                            <option value="All">Select</option>
                                            @foreach ($doctor_list as $key => $val)
                                                <option value="{{ $key }}">{{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="" style="color:blue;">Surgery Name</label>
                                        <div class="clearfix"></div>
                                        <select id="surgery_name" class="form-control select2"
                                            style="color:#555555; padding:2px 12px;overflow: auto;">
                                            <option value="All">Select</option>
                                            @foreach ($surgery_master as $key => $val)
                                                <option value="{{ $key }}">{{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>



                                <div class="col-md-1 padding_sm" style="margin-top: -10px">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button type="button" id="searchOTScheduleBtn" onclick="searchOTSchedule()"
                                        class="btn btn-block btn-primary"><i id="searchOTScheduleSpin"
                                            class="fa fa-search"></i>
                                        Search</button>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: -10px">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <a href="{{ Request::url() }}" class="btn btn-block btn-warning"><i
                                            class="fa fa-times"></i> Clear</a>
                                </div>
                                <div class="col-md-1 padding_sm" style="margin-top: -10px">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button type="button" id="CreateNewOTScheduleBtn" onclick="CreateNewOTSchedule();"
                                        class="btn btn-block bg-green"><i id="searchCreateNewOTScheduleSpin"
                                            class="fa fa-plus"></i>
                                        Create</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix" id="searchOtSchedule">

                    </div>
                </div>
            </div>

        </div>
    </div>

    <!---------Ot postpone modal------------------------------>
    <div class="modal fade" id="postpone_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        style="z-index:1051 !important">
        <div class="modal-dialog modal-md" role="document"
            style="width:94%;
                                    margin-right: 10px;margin-top:5px;">
            <div class="modal-content modal-md">
                <div class="modal-header" style="background-color:cornflowerblue;color:white;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Postpone Surgery</h4>
                </div>
                <div class="modal-body" id="postpone_modal_body" style="height:275px;">
                    <input type="hidden" id="postpone_request_id"/>
                    <div class="col-md-4">
                        <label style="top:0px;" for=""><b>Postponed at</b></label>
                        <div class="clearfix"></div>
                        <input type="text" style="font-weight: 600;
                            font-size: 11px;
                            border: 2px solid #e0e0e0;
                            border-radius: 5px;
                            padding: 13px !important;" name="postponed_at" autocomplete="off" value="{{ date('M-d-Y') }}" class="form-control datepicker" id="postponed_at" placeholder="Postponed at">
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <label><b>Remarks</b></label>
                        <textarea class="form-control" style="font-weight: 600;
                        font-size: 11px;
                        border: 2px solid #e0e0e0;
                        border-radius: 5px;
                        padding: 13px !important;" id="postpone_remarks"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn bg-green pull-right" onclick="ConfirmPostponeSurgery();"> <i class="fa fa-save"></i>  Save</button>
                </div>
            </div>
        </div>
    </div>
    <!---------Ot schedule modal------------------------------>
    <div class="modal fade" id="surgery_schedule_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        style="z-index:1051 !important">
        <div class="modal-dialog" role="document"
            style="width:94%;
                                    margin-right: 10px;margin-top:5px;">
            <div class="modal-content">
                <div class="modal-header" style="background-color: rgb(14, 124, 60);color:white;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Schedule Surgery</h4>
                </div>
                <div class="modal-body" id="surgery_schedule_modal_body" style="height:825px;">

                </div>
            </div>
        </div>
    </div>
    <!---------Ot Checklist modal------------------------------>

    <div class="modal fade" id="surgery_checklist_modal" role="dialog" aria-labelledby="myModalLabel"
        style="z-index:1052;">
        <div class="modal-dialog" style="margin-left: 5%;max-width: 95%;width: 100%;margin-top: 0%;">
            <div class="modal-content">
                <div class="modal-header" style="background:#337ab7;color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="checkListDataHeader">Surgery Medicine Indent</h4>
                </div>

                <div class="modal-body1" style="min-height:90vh;">
                    <div class="col-md-12 padding_sm theadscroll" id="surgery_checklist" style="position:relative;height:88vh !important;">

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    {{-- <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button> --}}
                    {{-- <button style="padding: 3px 3px" onclick="saveSurgeryChecKList();" type="button" title="Save"
                        class="btn btn-success" id="save_checklistbtn">Save<i id="save_checklistspin"
                            class="fa fa-save padding_sm"></i>
                    </button> --}}

                    {{-- <button style="padding: 3px 3px" onclick="saveAnaesthesiaChecKList();" type="button" title="Save"
                            class="btn btn-success" style="display:none;" id="save_ans_checklistbtn">Save<i id="save_ans_checklistspin"
                                class="fa fa-save padding_sm"></i>
                    </button> --}}



                </div>
            </div>
        </div>
    </div>


    {{-- <div class="modal fade" id="anaesthesia_checklist_modal" role="dialog" aria-labelledby="myModalLabel"
    style="z-index:1052;">
        <div class="modal-dialog" style="max-width: 90%;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="checkListDataHeader">Surgery Medicine Indent</h4>
                </div>
                <div class="modal-body" style="min-height: 550px;">
                    <div class="col-md-12 padding_sm" id="anaesthesia_checklist_modal_data">

                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" onclick="saveSurgeryChecKList()" type="button" title="Save"
                        class="btn btn-success" id="save_checklistbtn">Save<i id="save_checklistspin"
                            class="fa fa-save padding_sm"></i></button>
                </div>
            </div>
        </div>
    </div> --}}





    <!---------Anaesthesia Checklist modal------------------------------>

    {{-- <div class="modal fade" id="anaesthesia_checklist_modal" role="dialog" aria-labelledby="myModalLabel"
        style="z-index:1052;">
        <div class="modal-dialog" style="max-width: 90%;width: 100%;">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="AnscheckListDataHeader">Surgery Medicine Indent</h4>
                </div>
                <div class="modal-body" style="min-height: 550px;">
                    <div class="col-md-12 padding_sm" id="anaesthesia_checklist_modal_data">

                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" onclick="saveAnaesthesiaChecKList()" type="button" title="Save"
                        class="btn btn-success" id="save_checklistbtn">Save<i id="save_checklistspin"
                            class="fa fa-save padding_sm"></i>
                    </button>
                </div>
            </div>
        </div>
    </div> --}}

    <!---------HISTOPATHOLOGY modal------------------------------>

    <div class="modal fade" id="histopathogy_data_modal" role="dialog" aria-labelledby="myModalLabel"
        style="z-index:1052;">
        <div class="modal-dialog" style="max-width: 90%;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="histopathogy_data_modal_header"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 padding_sm" id="histopathogy_data_modal_div">

                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" onclick="saveHistopathogyData()" type="button" title="Save"
                        class="btn btn-success" id="saveHistopathogyDataBtn">Save <i id="saveHistopathogyDataSpin"
                            class="fa fa-save"></i></button>
                </div>
            </div>
        </div>
    </div>

    <!---------Microbiology Ivestigation Modal------------------------------>

    <div class="modal fade" id="microbiology_investigation_data_modal" role="dialog" aria-labelledby="myModalLabel"
        style="z-index:1052;">
        <div class="modal-dialog" style="max-width: 90%;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="microbiology_investigation_data_header"></h4>
                </div>
                <div class="modal-body" style="min-height: 500px;">
                    <div class="col-md-12 padding_sm" id="microbiology_investigation_data_modal_div">

                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                        class="btn btn-danger">Close <i class="fa fa-times"></i></button>
                    <button style="padding: 3px 3px" onclick="saveMicrobiologyInvestigation()" type="button" title="Save"
                        class="btn btn-success" id="saveMicrobiologyInvestigationBtn">Save <i id="saveMicrobiologyInvestigationBtnSpin"
                            class="fa fa-save"></i></button>
                </div>
            </div>
        </div>
    </div>

    <!-- medicine indent modal  -->

    <div class="modal fade" id="medicine_indent_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        data-keyboard="false" data-backdrop="static" aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 90%;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="checkListDataHeader"></h4>
                </div>
                <div class="modal-body" style="min-height: 520px;">

                    <div class="col-md-12 padding_sm" id="medicine_indent">

                    </div>
                </div>
            </div>
        </div>
    </div>




    {{-- ot checklist print modal --}}

    <div id="ot_checklist_print_modal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:97%">

            <div class="modal-content">
                <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close close_white">&times;</button>
                    <h4 class="modal-title">OT CHECKLIST</h4>
                </div>
                <div class="modal-body" id="ot_checklist_print_data" style="height:560px;">

                </div>
            </div>

        </div>
    </div>


    <!----------Surgery Request modal------------------------------>
    <div class="modal fade" id="surgery_request_modal" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel" style="z-index:1051 !important">
        <div class="modal-dialog" role="document" style="width:91%;">
            <div class="modal-content">
                <div class="modal-header" style="background-color: rgb(14, 124, 60);color:white;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Surgery Request</h4>
                </div>
                <div class="modal-body theadscroll" id="surgery_request_modal_body"
                    style="position: relative;;height:580px;">

                </div>
            </div>
        </div>
    </div>

    <!----- Anaesthetia record checklist modal--------------------------------------->
    <div class="modal fade" id="anesthetia_record_checklist_modal" role="dialog" aria-labelledby="myModalLabel"
    style="z-index:1052;">
        <div class="modal-dialog" style="
            margin-left: 5%;
            max-width: 95%;
            width: 100%;
            margin-top: 0%;
        ">
            <div class="modal-content">
                <div class="modal-header" style="background:#337ab7; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="AnscheckListDataHeader">Anaestheia Record</h4>
                </div>
                <div class="modal-body1" style="min-height:90vh;">
                    <div class="col-md-12 padding_sm theadscroll" id="anesthetia_record_checklist" style="position:relative;height:88vh !important;">

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="chartCodeDataModal" role="dialog" aria-labelledby="myModalLabel"
    style="z-index:1052;" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog" style="
            margin-left: 35%;
            max-width: 25%;
            width: 100%;
            margin-top: 0%;
        ">
            <div class="modal-content">
                <div class="modal-header" style="background:#3c33b7; color: #FFFFFF;">
                    <button type="button" class="close close_white" onclick="closeChartCodeModal();"  aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="">Chart codes</h4>
                </div>
                <div class="modal-body1" style="">
                    <div class="col-md-12 padding_sm" id="chartCodeDataModal_data">
                        <input type="hidden" id="current_chart_colum_count"/>
                        <table class="table" style="width: 100%;font-size: 22px;">
                            <tr>
                                <td>
                                    <input class="form-control chart_code_reading" type="text" id="val_chart_codes_1" style="width:55px;
                                    float: left;
                                    margin-right: 10px;
                                    clear: right !important;" value=''/>
                                    <input type="checkbox" value="1" id="chk_chart_codes_1"  class="chk_chart_codes chk_chart_codes_1"/>
                                    <label for="chk_chart_codes_1">Anaes</label>
                                </td>
                                <td>
                                    <input class="form-control chart_code_reading" type="text" id="val_chart_codes_2" style="width:55px;
                                    float: left;
                                    margin-right: 10px;
                                    clear: right !important;" value=''/>
                                    <input type="checkbox" value="2" id="chk_chart_codes_2"  class="chk_chart_codes chk_chart_codes_2"/>
                                    <label for="chk_chart_codes_2"> Operation</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input class="form-control chart_code_reading" type="text" id="val_chart_codes_3" style="width:55px;
                                    float: left;
                                    margin-right: 10px;
                                    clear: right !important;" value=''/>
                                    <input type="checkbox" value="3" id="chk_chart_codes_3"  class="chk_chart_codes chk_chart_codes_3"/>
                                    <label for="chk_chart_codes_3">BP</label>
                                </td>
                                <td>
                                    <input class="form-control chart_code_reading" type="text" id="val_chart_codes_4" style="width:55px;
                                    float: left;
                                    margin-right: 10px;
                                    clear: right !important;" value=''/>
                                    <input type="checkbox" value="4" id="chk_chart_codes_4"  class="chk_chart_codes chk_chart_codes_4"/>
                                    <label for="chk_chart_codes_4">Pulse</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input class="form-control chart_code_reading" type="text" id="val_chart_codes_5" style="width:55px;
                                    float: left;
                                    margin-right: 10px;
                                    clear: right !important;" value=''/>
                                    <input type="checkbox" value="5" id="chk_chart_codes_5"  class="chk_chart_codes chk_chart_codes_5"/>
                                    <label for="chk_chart_codes_5">ETCO2</label>
                                </td>
                                <td>
                                    <input class="form-control chart_code_reading" type="text" id="val_chart_codes_6" style="width:55px;
                                    float: left;
                                    margin-right: 10px;
                                    clear: right !important;" value=''/>
                                    <input type="checkbox" value="6" id="chk_chart_codes_6"  class="chk_chart_codes chk_chart_codes_6"/>
                                    <label for="chk_chart_codes_6">SPO2</label>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
                <div class="modal-footer">
                    <button style="padding: 3px 3px" onclick="applyChartCodeModal();" type="button" title="Save"
                            class="btn btn-success" style="display:none;" id="">Save <i
                                class="fa fa-save padding_sm"></i>
                    </button>
                    </div>
            </div>
        </div>
    </div>




    @include('Master::flexy.report_print_modal')
    @include('Emr::emr.includes.custom_modals')

@stop

@section('javascript_extra')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/ot_schedule.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/ot_checklist.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
      src="{{ asset('packages/extensionsvalley/emr/js/surgery_bill.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

    <script src="{{ asset('packages/extensionsvalley/emr/js/prescription_ot.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/anaesthetia_record.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

    <script type="text/javascript">
        @include('Purchase::messagetemplate')
    </script>

@endsection
