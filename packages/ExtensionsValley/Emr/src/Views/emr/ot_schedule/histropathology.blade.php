<div class="theadscroll" style="position: relative; height: 400px;">
    <div class="col-md-12 padding_sm text-center " style="padding: 10px;"><b class="checklist_head_text">HISTOPATHOLOGY
            FORM</b></div>

            <div class="col-md-12">
                <table class="table" style="">
                <tr>
                    <td>
                        Surgeon
                        {!! Form::select('or_surgeon',$surgen_list, $surgery_details[0]->surgeon_id, ['class' => 'form-control select2','placeholder' =>'Select surgeon', 'id' =>'or_surgeon', 'style' => 'color:#555555; padding:4px 12px;','disabled'=>'disabled']) !!}
                    </td>
                    <td>
                        Date <input id="histo_scheduled_date" type="text" class="form-control datepicker" value="{{$histo_scheduled_date}}"/>
                    </td>
                    <td>
                        Anesthesia Type
                        {!! Form::select('or_anaesthesia_type',$anaestha_list,$surgery_details[0]->anaesthesia_id, ['class' => 'form-control select2','placeholder' =>'Select surgeon', 'id' =>'or_anaesthesia_type', 'style' => 'color:#555555; padding:4px 12px;','disabled'=>'disabled']) !!}
                    </td>
                </tr>
                <tr>
                    <td>Assistant
                        <input id="histo_assistant" type="text" class="form-control" value="{{$histo_assistant}}"/>
                    </td>
                    <td>Time
                        <input id="histo_scheduled_time" type="text" class="form-control timepicker" value="{{$histo_scheduled_time}}"/>
                    </td>
                    <td>Anaesthetist
                        {!! Form::select('or_anaesthetist',$anaesthetist, $surgery_details[0]->anaesthetist_id, ['class' => 'form-control select2','placeholder' =>'Select Anaesthetist', 'id' =>'or_anaesthetist', 'style' => 'color:#555555; padding:4px 12px;','disabled'=>'disabled']) !!}
                    </td>
                </tr>
                <tr>
                    <td>Nurse
                        {!! Form::select('histo_nurse',$nurse_list,$histo_nurse, ['class' => 'form-control select2','placeholder' =>'Select Scrub Nurse', 'id' =>'histo_nurse', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                    </td>
                    <td>To <input id="histo_to" type="text" value="{{$histo_to}}" class="form-control timepicker"/></td>
                    <td>Operation Type  <input id="histo_operation_type" value="{{$histo_operation_type}}" type="text" class="form-control"/></td>
                </tr>

                </table>
            </div>
            <div class="col-md-12 padding_sm">
                <input type="hidden" id="histopathology_id" value="{{ $histopathology_id }}">
                <div class="col-md-6 padding_sm">
                    <label for="">Clinical Diagnosis </label>
                    <textarea name="" id="clinical_diagnosis" class="form-control" style="resize: none">{{ $clinical_diagnosis }}</textarea>
                </div>
                <div class="col-md-6 padding_sm">
                    <label for="">Clinical History - Intra Operative Findings </label>
                    <textarea name="" id="clinical_history" class="form-control" style="resize: none">{{ $clinical_history }}</textarea>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 padding_sm" style="margin-bottom: 3px !important;">
                <div class="col-md-6 padding_sm">
                    <label for="">Nature of Specimen </label>
                    <textarea name="" id="nature_of_specimen" class="form-control" style="resize: none">{{ $nature_of_specimen }}</textarea>
                </div>
                <div class="col-md-6 padding_sm">
                    <label for="">Investigations Done</label>
                    <textarea name="" id="investigations_done" class="form-control" style="resize: none">{{ $investigations_done }}</textarea>
                </div>
            </div>
</div>
