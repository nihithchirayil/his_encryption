<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var from_date = $("#from_date").val();
            var to_date = $("#to_date").val();
            var surgeon_name = $("#surgeon_name").val();
            var surgery_name = $("#surgery_name").val();

            $.ajax({
                url: url,
                type: "POST",
                data: {
                    _token: token,
                    from_date: from_date,
                    to_date: to_date,
                    surgeon_name: surgeon_name,
                    surgery_name: surgery_name
                },
                beforeSend: function() {
                    $('#searchOtSchedule').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                    $('#searchOTScheduleBtn').attr('disabled', true);
                    $('#searchOTScheduleSpin').removeClass('fa fa-search');
                    $('#searchOTScheduleSpin').addClass('fa fa-spinner fa-spin');
                },
                success: function(data) {
                    $('#searchOtSchedule').html(data);
                },
                complete: function() {
                    $('#searchOtSchedule').LoadingOverlay("hide");
                    $('#searchOTScheduleBtn').attr('disabled', false);
                    $('#searchOTScheduleSpin').removeClass('fa fa-spinner fa-spin');
                    $('#searchOTScheduleSpin').addClass('fa fa-search');
                },
            });
            return false;
        });

    });
</script>
<style>
    .btn:hover {
    transform: scale(1.2);
    }
    .common_td_rules{
        font-size: 12px !important;
        color:rgb(82, 82, 82) !important;
        font-family: 'Roboto', sans-serif !important;
    }

    .pagination .purple_pagination{
        margin-bottom:30px !important;
    }


    .glowing {

        text-decoration: none;
        -webkit-animation: glowing 1500ms infinite;
        -moz-animation: glowing 1500ms infinite;
        -o-animation: glowing 1500ms infinite;
        animation: glowing 1500ms infinite;
    }
    @-webkit-keyframes glowing {
    0% { background-color: #0003b2; -webkit-box-shadow: 0 0 1px #0003b2; }
    50% { background-color: #006aff; -webkit-box-shadow: 0 0 6px #006aff; }
    100% { background-color: #0003b2; -webkit-box-shadow: 0 0 3px #0003b2; }
    }

    @-moz-keyframes glowing {
    0% { background-color: #0003b2; -moz-box-shadow: 0 0 1px #0003b2; }
    50% { background-color: #006aff; -moz-box-shadow: 0 0 6px #006aff; }
    100% { background-color: #0003b2; -moz-box-shadow: 0 0 3px #0003b2; }
    }

    @-o-keyframes glowing {
    0% { background-color: #0003b2; box-shadow: 0 0 1px #0003b2; }
    50% { background-color: #006aff; box-shadow: 0 0 6px #006aff; }
    100% { background-color: #0003b2; box-shadow: 0 0 3px #0003b2; }
    }

    @keyframes glowing {
    0% { background-color: #0003b2; box-shadow: 0 0 1px #0003b2; }
    50% { background-color: #006aff; box-shadow: 0 0 6px #006aff; }
    100% { background-color: #0003b2; box-shadow: 0 0 3px #0003b2; }
    }

</style>

<div class="theadscroll" style="position: relative; min-height: 450px;">
    <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
        style="border: 1px solid #CCC;font-weight:550;">
        <thead>
            <tr style="color:white;background-color:cornflowerblue;">
                <th style="width:7%;">Date of Surgery</th>
                <th style="width:10%;">Patient</th>
                <th style="width:7%;">UHID</th>
                <th style="width:10%;">Surgery</th>
                <th style="width:8%;">Surgery Time</th>
                <th style="width:5%;">Anaesthesia</th>
                <th style="width:8%;">Surgeon</th>
                <th style="width:8%;">Anaesthetist</th>
                <th style="width:8%;">Created By</th>
                <th style="width:8%;">Diaganosis</th>
                <th style="width:21%;">Action</th>
            </tr>
        </thead>
        <tbody style="margin-bottom:30px;">
            @if (count($request_list) > 0)
                @foreach ($request_list as $data)
                    @php
                        $is_surgery_checklist_completed = 'btn-default';
                        $is_anasthesia_checklist_completed = 'btn-default';
                        $is_surgery_notes_added = 'btn-default';
                        $is_medicine_indented = 'btn-default';
                        $is_surgery_bill_generated = 'btn-default';
                        $is_scheduled = 'btn-default';
                        $is_histopathology_completed = 'btn-default';
                        $is_microbiology_completed = 'btn-default';
                    @endphp
                    @if ($data->is_surgery_checklist_completed == 1)
                        @php $is_surgery_checklist_completed='btn-success'; @endphp
                    @endif
                    @if ($data->is_anasthesia_checklist_completed == 1)
                        @php $is_anasthesia_checklist_completed='btn-success'; @endphp
                    @endif
                    @if ($data->is_surgery_notes_added == 1)
                        @php $is_surgery_notes_added='btn-success'; @endphp
                    @endif
                    @if ($data->is_medicine_indented == 1)
                        @php $is_medicine_indented='btn-success'; @endphp
                    @endif
                    @if ($data->is_surgery_bill_generated == 1)
                        @php $is_surgery_bill_generated='btn-success'; @endphp
                    @endif
                    @if ($data->is_histopathology_completed == 1)
                        @php $is_histopathology_completed ='btn-success'; @endphp
                    @endif
                    @if ($data->is_microbiology_completed == 1)
                        @php $is_microbiology_completed ='btn-success'; @endphp
                    @endif
                    @if ($data->is_scheduled == 1)
                        @php $is_scheduled='btn-success'; @endphp
                    @endif
                    <tr style="cursor: pointer;">
                        <td title="{{ date('M-d-Y', strtotime($data->surgery_date)) }}" class="surgery_date common_td_rules">{{ date('M-d-Y', strtotime($data->surgery_date)) }}
                        </td>
                        <td title="{{ ucwords(strtolower($data->patient_name)) }}" class=" common_td_rules"
                            id="SurgeryRequestPatient{{ $data->id }}{{ $data->surgery_id }}">
                            {{ ucwords(strtolower($data->patient_name)) }}</td>
                        <td title="{{ $data->uhid }}" class="common_td_rules">{{ $data->uhid }}</td>
                        <td title="{{ $data->surgery_name }}" class="surgery_name common_td_rules"
                            id="SurgeryRequestSurgery{{ $data->id }}{{ $data->surgery_id }}">
                            {{ $data->surgery_name }}</td>

                        <td title="@if($data->actual_start_time !='')
                            {{date('h:i A',strtotime($data->actual_start_time))}}
                        @endif
                        -
                        @if($data->actual_end_time !='')
                            {{date('h:i A',strtotime($data->actual_end_time))}}
                        @endif" class="anaesthesia common_td_rules">

                            @if($data->actual_start_time !='' || $data->actual_end_time !='')

                                @if($data->actual_start_time !='')
                                    {{date('h:i A',strtotime($data->actual_start_time))}}
                                @endif
                                -
                                @if($data->actual_end_time !='')
                                    {{date('h:i A',strtotime($data->actual_end_time))}}
                                @endif
                            @else

                                @php
                                    $surgery_checklist_data = [];
                                    $surgery_checklist_data = \DB::table('surgery_checklist')->where('surgery_request_id',$data->surgery_detail_id)->whereNull('deleted_at')->first();
                                    if(!empty($surgery_checklist_data)){
                                        $sign_in_data = $surgery_checklist_data->sign_in_data;
                                        $sign_out_data = $surgery_checklist_data->sign_out_data;
                                        if($sign_in_data !=''){
                                            $sign_in_data = json_decode($sign_in_data);
                                            if(isset($sign_in_data->signin_completed_at)){
                                                $signin_completed_at = $sign_in_data->signin_completed_at;
                                            }else{
                                                $signin_completed_at = '';
                                            }
                                        }else{
                                            $signin_completed_at='';
                                        }
                                        if($sign_out_data !=''){
                                            $sign_out_data = json_decode($sign_out_data);
                                            if(isset($sign_out_data->signout_handover_taken_at)){
                                                $signout_handover_taken_at = $sign_out_data->signout_handover_taken_at;
                                            }else{
                                                $signout_handover_taken_at = '';
                                            }
                                        }else{
                                            $signout_handover_taken_at = '';
                                        }
                                    }else{
                                        $signin_completed_at='';
                                        $signout_handover_taken_at = '';
                                    }

                                @endphp

                                    @if($signin_completed_at !='')
                                        {{date('h:i A',strtotime($signin_completed_at))}}
                                    @endif
                                    -
                                    @if($signin_completed_at !='')
                                        {{date('h:i A',strtotime($signout_handover_taken_at))}}
                                    @endif
                            @endif

                        </td>
                        <td title="{{ $data->anaesthesia }}" class="anaesthesia common_td_rules">{{ $data->anaesthesia }}</td>
                        <td title="{{ ucwords(strtolower($data->surgeon)) }}" class="surgeon common_td_rules" style="">

                            {!!ucwords(strtolower($data->surgeon))!!}
                        </td>
                        <td class="anaesthetist common_td_rules" title="{{ ucwords(strtolower($data->anaesthetist)) }}">
                            @if( trim(ucwords(strtolower($data->anaesthetist)))!='External')
                                {!! ucwords(strtolower($data->anaesthetist))!!}
                            @endif
                        </td>
                        <td title="{{ ucwords(strtolower($data->created_by)) }}" class="created_by common_td_rules">{{ ucwords(strtolower($data->created_by)) }}
                            @if($data->location_name !='')
                            ({{ ucwords(strtolower($data->location_name))}})
                            @endif
                        </td>
                        <td title="{{ ucwords(strtolower($data->diaganosis)) }}" class="diaganosis common_td_rules">{{ ucwords(strtolower($data->diaganosis)) }}</td>
                        <td style="text-align:left !important;">
                            @if(in_array("OT Schedule", $groupArray))
                            <button type="button" class="btn {{ $is_scheduled }} btn-primary btn-sm btn-default"
                                id="schedule_surgerybtn{{ $data->id }}{{ $data->surgery_detail_id }}"
                                onclick="schedule_surgery('{{ $data->id }}','{{ $data->surgery_date }}','{{ $data->surgery_detail_id }}');"
                                title="Schedule">
                                <i id="schedule_surgeryspin{{ $data->id }}{{ $data->surgery_detail_id }}"
                                    class="fa fa-clock-o" aria-hidden="true"></i>
                            </button>
                            @endif
                            <button type="button"
                                class="btn btn-sm {{ $is_surgery_checklist_completed }} btn-primary checklist_btn"
                                id="patient_checklist_btn{{ $data->id }}{{ $data->surgery_id }}"
                                onclick="getPatientChecklist('{{ $data->id }}','{{ $data->surgery_id }}','{{ $data->surgery_detail_id }}','{{ $data->patient_id }}');"
                                title="Surgery Check List">
                                <i id="patient_checklist_spin{{ $data->id }}{{ $data->surgery_id }}"
                                    class="fa fa-check-square-o" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-primary {{ $is_anasthesia_checklist_completed }}"
                                id="anesthesia_checklist_btn{{ $data->id }}{{ $data->surgery_id }}"
                                onclick="getAnesthesiaRecord('{{ $data->id }}','{{ $data->surgery_id }}','{{ $data->surgery_detail_id}}');"
                                title="Anesthesia Check List">
                                <i id="anesthesia_checklist_spin{{ $data->id }}{{ $data->surgery_id }}"
                                    class="fa fa-low-vision" aria-hidden="true"></i>
                            </button>
                            @if(in_array("OT Operation Notes", $groupArray))
                            <button type="button" class="btn btn-sm btn-primary {{ $is_surgery_notes_added }} operation_notes"
                                id="operation_notes_btn{{ $data->id }}{{ $data->surgery_id }}"
                                onclick="getOperationNotes('{{ $data->id }}','{{ $data->surgery_id }}','{{ $data->surgery_detail_id}}');"
                                title="Operation Notes">
                                <i id="operation_notes_spin{{ $data->id }}{{ $data->surgery_id }}"
                                    class="fa fa-file" aria-hidden="true"></i>
                            </button>
                            @endif
                            @if(in_array("OT Medicine Indent", $groupArray))
                            <button type="button" class="btn btn-sm btn-primary {{ $is_medicine_indented }} medicineIndent"
                                id="medicineIndent_btn{{ $data->id }}{{ $data->surgery_id }}"
                                onclick="medicineIndent('{{ $data->id }}','{{ $data->surgery_id }}');"
                                title="Medicine Indent">
                                <i id="medicineIndent_spin{{ $data->id }}{{ $data->surgery_id }}"
                                    class="fa fa-medkit" aria-hidden="true"></i>
                            </button>
                            @endif
                            @if(in_array("OT Billing", $groupArray))
                            <button type="button"
                                class="btn btn-sm btn-primary {{ $is_surgery_bill_generated }} gotoSurgeryBilling"
                                id="gotoSurgeryBilling_btn{{ $data->id }}{{ $data->surgery_id }}"
                                onclick="gotoSurgeryBilling('{{ $data->id }}','{{ $data->surgery_id }}');"
                                title="Surgery Billing">
                                <i id="gotoSurgeryBilling_spin{{ $data->id }}{{ $data->surgery_id }}"
                                    class="fa fa-list" aria-hidden="true"></i>
                            </button>
                            @endif
                            @if(in_array("OT Histopathology Form", $groupArray))
                            <button type="button"
                                class="btn btn-sm btn-primary {{ $is_histopathology_completed }} HistopathologyRequestForm"
                                id="HistopathologyRequestFormBtn{{ $data->id }}{{ $data->surgery_id }}"
                                onclick="HistopathologyRequestForm('{{ $data->id }}','{{ $data->surgery_id }}','{{ $data->patient_id }}','{{ $data->surgery_detail_id }}')"
                                title="Histopathology Request Form">
                                <i id="HistopathologyRequestFormSpin{{ $data->id }}{{ $data->surgery_id }}"
                                    class="fa fa-flask" aria-hidden="true"></i>
                            </button>
                            @endif
                            @if(in_array("OT Microbiology Form", $groupArray))
                            <button type="button"
                                class="btn btn-sm btn-primary {{ $is_microbiology_completed }} MicrobiologyInvestigation"
                                id="MicrobiologyInvestigationBtn{{ $data->id }}{{ $data->surgery_id }}"
                                onclick="MicrobiologyInvestigation('{{ $data->id }}','{{ $data->surgery_id }}','{{ $data->patient_id }}','{{ $data->surgery_detail_id }}')"
                                title="Microbiology Request Form">
                                <i id="MicrobiologyInvestigationSpin{{ $data->id }}{{ $data->surgery_id }}"
                                    class="fa fa-microchip" aria-hidden="true"></i>
                            </button>
                            @endif

                            <button title="print surgery checklist" type="button" class="btn btn-sm btn-warning btn-default"
                                id="print_surgerybtn{{ $data->surgery_detail_id }}"
                                onclick="PrintOtCheckList('{{ $data->surgery_detail_id }}');" title="Print">
                                <i id="print_surgeryspin{{ $data->surgery_detail_id }}" class="fa fa-print"
                                    aria-hidden="true"></i>
                            </button>
                            <button title="print operation notes" type="button" class="btn btn-sm btn-warning btn-default"
                                id="print_operation_notes{{ $data->surgery_detail_id }}"
                                onclick="PrintOperationNotes('{{ $data->surgery_detail_id }}','{{ $data->patient_id }}');" title="Print">
                                <i id="print_operation_notes{{ $data->surgery_detail_id }}" class="fa fa-print"
                                    aria-hidden="true"></i>
                            </button>
                            <button title="print anaesthetia checklist" type="button" class="btn btn-sm btn-warning btn-default"
                                id="print_Ansbtn{{ $data->surgery_detail_id }}"
                                onclick="PrintAnsCheckList('{{ $data->surgery_detail_id }}');" title="Print">
                                <i id="print_ans_surgeryspin{{ $data->surgery_detail_id }}" class="fa fa-print"
                                    aria-hidden="true"></i>
                            </button>
                            <button title="Print Histopathology" type="button" class="btn btn-sm btn-warning btn-default"
                                id="print_Ansbtn{{ $data->surgery_detail_id }}"
                                onclick="PrintHistoPathology('{{ $data->id }}','{{ $data->surgery_detail_id }}');" title="Print">
                                <i id="print_histo_spin{{ $data->surgery_detail_id }}" class="fa fa-print"
                                    aria-hidden="true"></i>
                            </button>
                            @if(in_array("OT Request Delete Access", $groupArray))
                            <button type="button" class="btn btn-sm btn-danger"
                                id="delete_surgerybtn{{ $data->surgery_id }}"
                                onclick="deleteOtRequest('{{ $data->id }}');"
                                title="Delete">
                                <i id="delete_surgeryspin{{ $data->surgery_id }}" class="fa fa-times"
                                    aria-hidden="true"></i>
                            </button>
                            @endif
                            @if(in_array("OT Request Delete Access", $groupArray))
                            <button type="button" class="btn btn-sm bg-blue"
                                id="postpone_surgerybtn{{ $data->surgery_id }}"
                                onclick="postponeOtRequest('{{ $data->id }}','{{date('M-d-Y',strtotime($data->surgery_date))}}');"
                                title=" Postpone surgery ">
                                <i id="postpone_surgeryspin{{ $data->id }}" class="fa fa-forward"
                                    aria-hidden="true"></i>
                            </button>
                            @endif
                            @if($data->is_postponed == 1)
                            <button type="button" class="btn btn-sm bg-blue glowing"
                                id="postpone_infobtn{{ $data->surgery_id }}"
                                title="Postponed informations"  data-toggle="popover"
                                data-trigger="focus" role="button"  data-content="<table>
                                    <tr>
                                        <td>Previous Scheduled Date</td>
                                        <td>:</td>
                                        <td>{{date('M-d-Y',strtotime($data->previous_surgery_date))}}</td>
                                    </<tr>
                                    <tr>
                                        <td>Remarks</td>
                                        <td>:</td>
                                        <td>{{$data->postponed_remark}}</td>
                                    </<tr>
                                </table>">
                                <i id="postpone_infospin{{ $data->id }}" class="fa fa-list" aria-hidden="true"></i>
                            </button>
                            @endif
                            @if(in_array("ot_start_end_time_access", $groupArray))
                            <button type="button" class="btn btn-sm bg-blue"
                                id="surgery_time_track_{{ $data->surgery_id }}"
                                onclick="surgeryTimeTrack('{{ $data->id }}','{{date('M-d-Y',strtotime($data->surgery_date))}}');"
                                title="Surgery Time Track"  data-toggle="popover"
                                role="button"  data-content="<table id='surgey_track_table_{{$data->id}}'>
                                    <tr>
                                        <td>Surgery Starts At</td>
                                        <td>:</td>
                                        <td>
                                            <input autocomplete='off' data-lpignore='true' type='text' id='actual_start_time_{{$data->id}}' value='@if($data->actual_start_time !='')
                                            {{date('h:i A',strtotime($data->actual_start_time))}}
                                        @endif' class='form-control timepicker'/>
                                        </td>
                                    </<tr>
                                    <tr>
                                        <td>Surgery Ends At</td>
                                        <td>:</td>
                                        <td><input autocomplete='off' data-lpignore='true' type='text' id='actual_end_time_{{$data->id}}' value='@if($data->actual_end_time !='')
                                            {{date('h:i A',strtotime($data->actual_end_time))}}
                                        @endif' class='form-control timepicker'/></td>
                                    </<tr>
                                    <tr>
                                        <td colspan='3'>
                                            <button class='btn bg-blue pull-right' type='button' onclick='saveSurgeryTimeTrack({{$data->id}})'><i class='fa fa-save'></i> Save
                                            </button>
                                        </td>
                                    </tr>
                                </table>">
                                <i id="time_track_surgeryspin{{ $data->id }}" class="fa fa-clock-o"
                                    aria-hidden="true"></i>
                            </button>
                            @endif
                        </td>

                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9" class="location_code">No Records found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right" style="">
    <ul class="pagination purple_pagination" style="text-align:right !important;">
        {!! $page_links !!}
    </ul>
</div>

