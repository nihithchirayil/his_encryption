<style>
    table td.highlighted {
        background-color:rgb(182, 255, 87);
    }
    table td.scheduled {
        background-color:rgb(201 255 240);
    }
    table td.scheduled_fixed {
        background-color:rgb(201 255 240);
    }

    .tooltip {
        position: absolute;
        z-index: 99999;
        color:white !important;
        background-color: rgb(245, 43, 184) !important;
        min-width:250px !important;
        border-radius:5px !important;
        padding:10px !important;
    }

    .tooltip-inner {
        background-color: rgb(245, 43, 184);
    }

    .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td{
        border:1px solid #f4f4f4 !important;
    }
    .mate-input-box{
        height:45px !important;
    }
    .mate-input-box label {
        margin-top:0px !important;
    }

    .txt-overflow{
        max-width: 290px;
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
        font-weight:600;
        font-size: 9px !important;
    }
    .booked_sloat{
        border-radius:9px !important;
    }
    .select2-container{
        box-shadow:none !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__display {
    cursor: default;
    padding-left: 2px;
    padding-right: 5px;
    font-size: 9px;
    font-weight: 600;
    margin-top: 3px;
    }

    .fa-times-circle{
        font-size: 18px;
        color: steelblue;
        margin: -7px;
        margin-bottom: 3px;
    }

    .fa-times-circle:hover{
        scale: 1.2 !important;
    }

    .common_table_header{
        background-color:cornflowerblue;
    }

</style>
@php
    $surgery_detail_id = $detail_data[0]->id;
    $surgery_array = [];
    $surgeon_array = [];
    $anaesthetist_array = [];
    $surgery_sql = \DB::select("select srs.surgery_id from surgery_request_surgery srs
    where srs.request_detail_id = $surgery_detail_id ");
    if(!empty($surgery_sql)){
        foreach($surgery_sql as $surgery_sql_data){
            array_push($surgery_array,$surgery_sql_data->surgery_id);
        }
    }else{
        $surgery_array = '';
    }

    $surgeon_sql = \DB::select("select srs.surgeon_id from surgery_request_surgeon srs
    where srs.request_detail_id = $surgery_detail_id ");
    if(!empty($surgeon_sql)){
        foreach($surgeon_sql as $surgeon_sql_data){
            array_push($surgeon_array,$surgeon_sql_data->surgeon_id);
        }
    }else{
        $surgeon_array = [];
    }

    $anaesthetist_sql = \DB::select("select srs.anaesthetist_id from surgery_request_anaesthetist srs
    where srs.request_detail_id = $surgery_detail_id ");
    if(!empty($anaesthetist_sql)){
        foreach($anaesthetist_sql as $anaesthetist_sql_data){
            array_push($anaesthetist_array,$anaesthetist_sql_data->anaesthetist_id);
        }
    }else{
        $anaesthetist_array = [];
    }

@endphp

<div class="col-md-12">
    <table class="table table-bordered table-condensed table_sm">
        <tr style="background: #d1e2ff;
    color: #383c51;">
            <td style="text-align:left;">Patient :</td>
            <td style="text-align:left;">{{$patient_details[0]->patient_name}}</td>
            <td style="text-align:left;">Age/Gender :</td>
            <td style="text-align:left;">{{$patient_details[0]->age}}/{{$patient_details[0]->gender}}</td>
            <td style="text-align:left;">UHID :</td>
            <td style="text-align:left;">{{$patient_details[0]->uhid}}</td>
        </tr>
        <tr style="background: #d1e2ff;
    color: #383c51;">
            <td style="text-align:left;">Admitted_by :</td>
            <td style="text-align:left;">{{$patient_details[0]->admitting_doctor_name}}</td>
            <td style="text-align:left;">Phone :</td>
            <td style="text-align:left;">{{$patient_details[0]->phone}}</td>
            <td style="text-align:left;">Admission No :</td>
            <td style="text-align:left;">{{$patient_details[0]->admission_no}}</td>
        </tr>
    </table>
</div>

<div class="col-md-12">
    <input type="hidden" name="surgery_head_id_hidden" id="surgery_head_id_hidden" value="{{$request_id}}">
    <div class="col-md-3" style="height: 740px;">
        <div class="col-md-6" style="z-index:99999">
            <div class="mate-input-box">
                <label style="top:0px;" for="">Surgery Date</label>
                <div class="clearfix"></div>
                <input type="text" onblur="listOtScheduleOnDateChange();" name="schedule_surgery_date" autocomplete="off" value="@if($head_data[0]->surgery_date !=''){{ date('M-d-Y',strtotime($head_data[0]->surgery_date)) }}@else{{ date('M-d-Y') }}@endif"
                    class="form-control datepicker" id="schedule_surgery_date" placeholder="From Date">
            </div>
        </div>
        <div class="col-md-6">
            <div class="mate-input-box" style="z-index:9999">
                <label style="top:0px;" for="">Surgery Time </label>
                <div class="clearfix"></div>
                    <input type="text" name="schedule_surgery_time" id="schedule_surgery_time" class="form-control timepicker timepickerclass" data-attr="date" placeholder="Time" style="" value="@if($head_data[0]->surgery_time !=''){{ date('h:i A',strtotime($head_data[0]->surgery_time)) }}@else{{ date('h:i A') }}@endif">
            </div>
        </div>
        <div class="col-md-12">
            <div class="mate-input-box" style="z-index:9999">
                <label style="margin-top:-3px !important;" for="nursing_station">Nursing Station</label>
                {!! Form::select('schedule_nursing_station', $nursing_stations, $head_data[0]->nursing_station, ['class' => 'form-control select2', 'title' => 'Nursing Station', 'id' => 'schedule_nursing_station', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;']) !!}
            </div>
        </div>
        <div class="col-md-12">
            <div class="mate-input-box">
                <label  style="top:0px;" for="anesthesia">Select Anesthesia</label>
                {!! Form::select('schedule_anesthesia', $anaesthia_list,$head_data[0]->anaesthesia_id, ['class' => 'form-control select2', 'title' => 'Anesthesia', 'id' => 'schedule_anesthesia', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;']) !!}
            </div>
        </div>


        <div class="col-md-12" style="height:130px;">
            <div class="mate-input-box" style="height:125px !important;">
                <label  style="margin-top:-3px !important;" for="surgeon"> Surgeon</label>
                {!! Form::select('schedule_surgeon', $doctor_list, $surgeon_array, ['class' => 'form-control select2', 'title' => 'Surgeon', 'id' => 'schedule_surgeon','multiple' => 'multiple-select', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;']) !!}
            </div>
        </div>


        <div class="col-md-12" style="height:130px;">
            <div class="mate-input-box" style="height:125px !important;">
                <label style="margin-top:-3px !important;" for="anesthetist"> Anesthetist</label>
                {!! Form::select('schedule_anesthetist', $anesthetist_list,$anaesthetist_array, ['class' => 'form-control select2', 'title' => 'Anesthetist','multiple' => 'multiple-select', 'id' => 'schedule_anesthetist', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;']) !!}
            </div>
        </div>


        <div class="col-md-12" style="height:100px;">
            <div class="mate-input-box" style="height:97px !important;">
                <label style="top:0px;" for="anesthetist">Diagnosis</label>
                <textarea name="schedule_diagnosis" id="schedule_diagnosis" class="form-control" placeholder="Diagnosis">
                    {{$head_data[0]->diaganosis}}
                </textarea>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12" style="height:130px;">
            <div class="mate-input-box" style="height:125px !important;">
                <label  style="top:0px;" for="anesthetist">Select Procedure</label>
                {!! Form::select('schedule_surgery_id', $surgery_master, $surgery_array, ['class' => 'form-control select2 filters', 'multiple' => 'multiple-select', 'title' => 'Select Procedure', 'id' => 'schedule_surgery_id', 'style' => 'color:#555555; padding:2px 12px;overflow: auto;z-index:99999']) !!}
            </div>
        </div>

        <div class="col-md-12" style="margin-top:57px;">
            @if($approval_status==0)
                <button type="button" class="btn bg-green pull-right" onclick="SaveOTschedule(0);" id="BtnSaveOTschedule">
                    <i class="fa fa-save"></i> Save
                </button>
                <button type="button" class="btn bg-blue pull-right" onclick="SaveOTschedule(1);" id="BtnSaveOTschedule">
                    <i class="fa fa-save"></i> Approve
                </button>
                <button type="reset" class="btn bg-orange pull-right" id="reset_surgery_request">
                    <i class="fa fa-save"></i> Reset
                </button>
            @else
                @if(in_array("ot_schedule_reschedule_access", $groupArray))
                    <button type="button" class="btn bg-green pull-right" onclick="SaveOTschedule(0);" id="BtnSaveOTschedule">
                        <i class="fa fa-save"></i> Save
                    </button>
                    <button type="button" class="btn bg-green pull-right" onclick="SaveOTschedule(1);" id="BtnSaveOTschedule">
                        <i class="fa fa-save"></i> save & Approve
                    </button>
                    <button type="reset" class="btn bg-orange pull-right" id="reset_surgery_request">
                        <i class="fa fa-save"></i> Reset
                    </button>
                @endif
            @endif
        </div>
        <input type="hidden" name="request_id" id="request_id" value="{{$head_data[0]->id}}">
    </div>
    <!---time allocation------------------>
    <div class="col-md-9 no-padding" id="time_allocation_container">

    </div>

</div>
<script>
    $(document).ready(function () {
        setTimeout(function () {
            console.log("ss");
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
        },2000);
        listOtScheduleOnDateChange();
    });

    $('#schedule_surgery_date').on('change',function(){
        listOtScheduleOnDateChange();
    });
</script>
