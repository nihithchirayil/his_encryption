<div class="theadscroll" style="position: relative; height: 400px;">
    <div class="col-md-12 padding_sm text-center " style="padding: 10px;"><b class="checklist_head_text">Microbiology
            Investigation</b></div>


            <div class="col-md-12">
                <table class="table" style="">
                <tr>
                    <td>
                        Surgeon
                        {!! Form::select('or_surgeon',$surgen_list, $surgeon, ['class' => 'form-control select2','placeholder' =>'Select surgeon', 'id' =>'or_surgeon', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                    </td>
                    <td>
                        Date <input id="or_scheduled_date" type="text" class="form-control datepicker" value=""/>
                    </td>
                    <td>
                        Anesthesia Type
                        {!! Form::select('or_anaesthesia_type',$anaestha_list,null, ['class' => 'form-control select2','placeholder' =>'Select surgeon', 'id' =>'or_anaesthesia_type', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                    </td>
                </tr>
                <tr>
                    <td>Assistant
                        <input id="or_assistant" type="text" class="form-control"/>

                    </td>
                    <td>Time
                        <input id="or_scheduled_time" type="text" class="form-control timepicker" value=""/>
                    </td>
                    <td>Anaesthetist
                        {!! Form::select('or_anaesthetist',$anaesthetist, $anaestha_doctor, ['class' => 'form-control select2','placeholder' =>'Select Anaesthetist', 'id' =>'or_anaesthetist', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                    </td>
                </tr>
                <tr>
                    <td>Nurse
                        {!! Form::select('or_nurse',$nurse_list, null, ['class' => 'form-control select2','placeholder' =>'Select Scrub Nurse', 'id' =>'or_nurse', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                    </td>
                    <td>To <input id="or_to" type="text" class="form-control"/></td>
                    <td>Operation Type  <input id="or_operation_type" type="text" class="form-control"/></td>
                </tr>

                </table>
            </div>

    <div class="col-md-12 padding_sm">
        <input type="hidden" id="microbiology_id" value="{{ $microbiology_id }}">
        <div class="col-md-6 padding_sm">
            <label for="">Short Histroy of the Case</label>
            <textarea name="" id="ShortHistroyoftheCase" class="form-control" style="resize: none">{{ $ShortHistroyoftheCase }}</textarea>
        </div>
        <div class="col-md-6 padding_sm">
            <label for="">Provisional Diagnosis</label>
            <textarea name="" id="ProvisionalDiagnosis" class="form-control" style="resize: none">{{ $ProvisionalDiagnosis }}</textarea>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 padding_sm">
        <label for="">Nature of specimen & site of collection</label>
        <textarea name="" id="NatureSpecimenCollection" class="form-control"
            style="height: 36px !important;resize: none;">{{ $NatureSpecimenCollection }}</textarea>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 padding_sm">
        <label for="">Time of collection of specimen</label>
        <textarea name="" id="TimeCollectionSpecimen" class="form-control" style="height: 36px !important;resize: none;">{{ $TimeCollectionSpecimen }}</textarea>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 padding_sm">
        <label for="">Investigations Required</label>
        <textarea name="" id="InvestigationsRequired" class="form-control" style="height: 36px !important;resize: none;">{{ $InvestigationsRequired }}</textarea>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-3 padding_sm">
        <label for="">Antibiotic Date Time</label>
        <input name="" id="AntibioticTimeDate" class="form-control"
            value="{{ $AntibioticTimeDate ? date('Mon-dd-YYYY', strtotime($AntibioticTimeDate)) : '' }}"
            style="height: 36px !important;resize: none;"></textarea>
    </div>
    <div class="col-md-5 padding_sm" style="margin-bottom:3px !important;">
        <label for="">Antibiotic Type</label>
        <textarea name="" id="AntibioticType" class="form-control" style="height: 36px !important;resize: none;">{{ $AntibioticType }}</textarea>
    </div>
</div>
