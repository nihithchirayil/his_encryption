<div class=" box-body theadscroll table-responsive text-nowrap" style="position:relative;height:740px;">
    <table id="shedule_time_table" width="100%;" class="table table-bordered table-hover table-condensed table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:11px !important;">
        <thead>
            @php
                $ot_list = [];
                $time_list = [];
                $otval = '';
                $time_val ='';
                $data_array = [];
                $ot_array = [];
                $timevalue  = '';
                $schedule_id_value = '';
                $schedule_id_array = [];


                foreach($shedule_data as $data){
                    if($otval != $data->ot_id){
                        $ot_list[$data->ot_id] = $data->ot_name;
                    }

                    if($time_val != $data->ret_slot_from.'-'.$data->ret_slot_to){
                        array_push($time_list,date('h:i A', strtotime($data->ret_slot_from)).'-'.date('h:i A', strtotime($data->ret_slot_to)));
                    }

                    $otval = $data->ot_id;
                    $time_val = $data->ret_slot_from.'-'.$data->ret_slot_to;

                    $ot_array[]= $data->ot_id;
                }

                $time_list = array_unique($time_list);
                $ot_array = array_unique($ot_array);




                $temp_ot = '';
                foreach($shedule_data as $data1){
                    if($data1->request_id !=''){
                        $data_array[] = array(
                            'ot_id' => $data1->ot_id,
                            'ot_name' => $data1->ot_name,
                            'ret_slot_from' => $data1->ret_slot_from,
                            'ret_slot_to' => $data1->ret_slot_to,
                            'r_date' => $data1->r_date,
                            'request_id' => $data1->request_id,
                            'surgery_id' => $data1->surgery_id,
                            // 'surgeon_name' => $data1->surgeon_name,
                            'surgeon_name' => '',
                            'uhid' => $data1->uhid,
                            'patient_name' => $data1->patient_name,
                            'surgery_name' => $data1->surgery_name,
                            'start_time' => $data1->start_time,
                            'schedule_id' => $data1->schedule_id,
                        );

                        $schedule_id_array[] = $data1->schedule_id;
                    }
                }

                $schedule_id_array = array_unique($schedule_id_array);

            @endphp
            <tr class="common_table_header">
                    <td style="width:20%">Time
                    <input type="hidden" name="schedule_id_hidden_val" id="schedule_id_hidden_val" value="{{implode(',',$schedule_id_array)}}">

                    </td>
                @foreach($ot_list as $key=>$value)
                    <td id="ot_{{$key}}">{{$value}}</td>
                @endforeach
            </tr>
        </thead>

        <tbody>
            @for($i=0;$i<count($time_list);$i++)
            <tr id="time_{{$time_list[$i]}}">
                <td class="time_list" style="color:white;font-weight: 600;width:7%;background:cornflowerblue;">
                    {{$time_list[$i]}}
                </td>

                @for($j=0;$j<count($ot_list);$j++)
                    @php
                        $html = '';
                        $tooltip = '';
                        $cls = '';
                        $booked_cls = '';
                        $start_time_value = '';
                        $schedule_id_value = '';
                    @endphp
                    @foreach ($data_array as $data1)

                        @php
                            $timevalue = '';
                            $req_id = '';
                            $timevalue = explode("-", $time_list[$i], 2);
                            $timevalue = $timevalue[0];
                            $timevalue = date('H:i:s',strtotime($timevalue));
                        @endphp


                        @if($data1['ot_id'] == ($j+1) && $data1['ret_slot_from'] == $timevalue)
                            @php
                                $tooltip = "<div class='col-md-12'>";
                                $tooltip .= "<label class='col-md-6 text-left'>Patient:</label>";
                                $tooltip .= "<label class='col-md-6 text-left'>".$data1['patient_name']."</label> <div class='clearfix'></div>";
                                $tooltip .= "<label class='col-md-6 text-left'>UHID:</label>";
                                $tooltip .= "<label class='col-md-6 text-left'>".$data1['uhid']."</label> <div class='clearfix'></div>";
                                $tooltip .=  "<label class='col-md-6 text-left'>Surgery Name:</label>";
                                $tooltip .=  "<label class='col-md-6 text-left'>".$data1['surgery_name']."</label> <div class='clearfix'></div>";
                                $tooltip .= "<label class='col-md-6 text-left'>Surgeon Name:</label>";
                               $tooltip .= "<label class='col-md-6 text-left'>".$data1['surgeon_name']."</label> <div class='clearfix'></div>";
                                $tooltip .="</div>";
                                $html = '<div class="col-md-12 no-padding" data-toggle="tooltip" data-placement="right" data-html="true" data-original-title="'.$tooltip.'">';
                                $html .= "<i onclick='deleteOtSchedule(".$data1['request_id'].");' class='fa fa-times-circle pull-right'></i>";
                                $html .= '<div class="col-md-12" style="padding:0px;">';
                                $html .= '<label class="col-md-12 txt-overflow no-padding text-left" style="color:blue;">'.$data1['patient_name'].'</label>';
                                $html .= "<label class='col-md-12 txt-overflow no-padding text-left'   style='color:blue;'>".$data1['surgery_name']."</label>";
                                $html.="</div>";

                                $cls = 'scheduled_fixed';
                                $booked_cls = 'booked_sloat';
                                $start_time_value = $data1['start_time'];
                                $schedule_id_value = $data1['schedule_id'];
                            @endphp

                        @else

                        @php

                            @endphp

                        @endif


                    @endforeach

                        <td class="{{$cls}} {{$schedule_id_value}} {{$booked_cls}}" data-attr-start_time={{$start_time_value}} data-attr-ret_slot_from="{{$timevalue}}"  data-attr-time="{{$time_list[$i]}}" data-ot="{{$j+1}}" id="cell_{{$j+1}}" style="">
                            {!!$html!!}
                        </td>

                @endfor
            </tr>
            @endfor
        </tbody>
    </table>
</div>
