<div id="msform">
    <ul id="progressbar" style="cursor: pointer;margin: 3px;" class="nav nav-tabs" role="tablist">
        <li class="nav-item" id="medicine_pack_list_li">
            <a class="nav-link" id="medicine_pack_list-tab" href="#medicine_pack_list_tab" role="tab"
                aria-controls="medicine_pack_list" aria-selected="true"><strong><i class="fa fa-medkit"></i> Medicine
                </strong></a>
        </li>
        <li class="nav-item" id="consumables_and_disposables_list_li">
            <a class="nav-link" id="consumables_and_disposables_list-tab"
                href="#consumables_and_disposables_list_tab" role="tab" aria-controls="consumables_and_disposables_list"
                aria-selected="true"><strong><i class="fa fa-database"></i> Consumables </strong></a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade" id="medicine_pack_list_tab" role="tabpanel" aria-labelledby="nav-medicine_pack-tab">
            <div class="form-card">
                <div class="box no-border no-margin">
                    @include('Emr::surgery_billing.medicine_bill')
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="consumables_and_disposables_list_tab" role="tabpanel"
            aria-labelledby="nav-consumables_and_disposables-tab">
            <div class="form-card">
                <div class="box no-border no-margin">
                    @include('Emr::surgery_billing.consumables_bill')
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $('#medicine_serach').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        if (event.keyCode == 13) {
            ajaxlistenter('medicine_serachAjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            var search_key = $('#medicine_serach').val();
            if (search_key == "") {
                $('#medicine_serachAjaxDiv').hide();
                $("#medicine_serach_hidden").val('');
            } else {
                var url = base_url + "/surgery/ajaxSearch";
                var listArrayString = JSON.stringify(medicine_list);
                var medicine_search = $('input[name="medicine_name_search"]:checked').val();
                var param = { _token: token, medicine_search: medicine_search, list_array: listArrayString, search_key: search_key, search_key_id: 'medicine_serach' };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("#medicine_serachAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#medicine_serachAjaxDiv").html(html).show();
                        $("#medicine_serachAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            }
        } else {
            ajax_list_key_down('medicine_serachAjaxDiv', event);
        }
    });



    $('#consumables_search').keyup(function (event) {
        var keycheck = /[a-zA-Z0-9 ]/; // now only accept alphabets, number and space noe need to change it.
        var value = event.key; //get the charcode and convert to char
        if (event.keyCode == 13) {
            ajaxlistenter('consumables_serachAjaxDiv');
            return false;
         } else if ((value.match(keycheck) || parseInt(event.keyCode) == 8 || parseInt(event.keyCode) == 46) && (parseInt(event.keyCode) != 40 && parseInt(event.keyCode) != 38)) {
            var search_key = $('#consumables_search').val();
            if (search_key == "") {
                $('#consumables_serachAjaxDiv').hide();
                $("#consumables_search_hidden`").val('');
            } else {
                var url = base_url + "/surgery/ajaxSearch";
                var listArrayString = JSON.stringify(consumables_list);
                var medicine_search = $('input[name="medicine_name_search"]:checked').val();
                var param = { _token: token, medicine_search: medicine_search, list_array: listArrayString, search_key: search_key, search_key_id: 'consumables_search' };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: param,
                    beforeSend: function () {
                        $("#consumables_serachAjaxDiv").html('<li style="width:200px;text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></li>').show();
                    },
                    success: function (html) {
                        $("#consumables_serachAjaxDiv").html(html).show();
                        $("#consumables_serachAjaxDiv").find('li').first().addClass('liHover');
                    },
                    complete: function () {
                    },
                    error: function () {
                        toastr.error('Please check your internet connection and try again');
                    }
                });
            }
        } else {
            ajax_list_key_down('consumables_serachAjaxDiv', event);
        }
    });

</script>
