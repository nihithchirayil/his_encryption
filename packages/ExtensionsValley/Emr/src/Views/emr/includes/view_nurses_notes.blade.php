<div class="theadscroll" style="position: relative; height:475px;">
    @if($res > 0)
    <table id="ip_patientlist_table" class="table table-bordered table_sm no-margin theadfix_wrapper">
        <tbody>
            @php
                $i = 1;
            @endphp
            @foreach($res as $data)
                    @php
                        $json = json_decode($data->data_text, TRUE);
                    @endphp
            <tr style="background-color:#55d499 !important;color:white;">
                <td>{{$data->name}} :
                    @if(isset($json['Timetaken']))
                        @if($json['Timetaken'] !='')
                            {{date('M-d-Y h:i A',strtotime($json['Timetaken']))}}
                        @endif
                    @else
                        {{date('M-d-Y h:i A',strtotime($data->created_at))}}
                    @endif
                </td>
            </tr>
            <tr>
                <td>
                    <span style="padding-left:10px;">
                        <b>{!!$json['TITLE']!!}</b>
                    </span>
                    </br>

                    <p>
                        {!!nl2br($json['Description'])!!}
                    </p>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
       <label>No Notes Found! </label>
    @endif
</div>
