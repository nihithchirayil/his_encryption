<style>
    .table_header_bg.header_normal_padding>th {
        padding: 8px !important;
    }

    .timepickerclass {
        font-weight: 600;
    }

    .timepickerclass_default {
        color: #a8a8a8 !important;
    }

    .bootstrap-datetimepicker-widget .form-control,
    .bootstrap-datetimepicker-widget .btn {
        box-shadow: none !important;
    }

    .bootstrap-datetimepicker-widget .timepicker-hour,
    .bootstrap-datetimepicker-widget .timepicker-minute,
    .bootstrap-datetimepicker-widget .timepicker-second {
        width: 70% !important;
    }

    .bootstrap-datetimepicker-widget td span {
        height: 34px !important;
        line-height: 34px !important;
    }

</style>

@php
$is_nursing_station = 0;
$nursing_station = session()->get('nursing_station');
if ($nursing_station != '') {
    $is_nursing_station = 1;
}
$enable_ward_stock_data =  \DB::table('config_detail')->where('name', 'is_ward_stock')->get();
$enable_ward_stock = 0;
        if (count($enable_ward_stock_data) > 0 && $is_nursing_station >0) {
            $enable_ward_stock = (int)$enable_ward_stock_data[0]->value;
        } else {
            $enable_ward_stock = 0;
        }
@endphp
<input type="hidden" value="{{ $enable_ward_stock }}" id='enable_ward_stock'>
<div class="box no-border no-margin prescription_wrapper anim" id="prescription_wrapper">
    <div class="box-body clearfix">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#drug" style="border-radius: 0px;"><b>Drug</b></a></li>
            <li><a data-toggle="tab" href="#consumables" style="border-radius: 0px;"><b>Consumables</b></a></li>
        </ul>
        <div class="tab-content">
            <div class="col-md-12" style="padding: 5px;">
                <div class="col-md-5 padding_sm pull-right" style="padding-right: 15px !important;">
                    <button style="width: 110px !important;float: right;height: 25px;" class="btn bg-primary" onclick="loadCompletePrescriptionHistory(1);"><i class="fa fa-history"
                            aria-hidden="true"></i> Complete History</button>
                    <button style="width: 110px !important;float: right;height: 25px;" class="btn bg-primary presc_history_btn" onclick="loadPrescriptionHistory(1, 1);"
                        data-toggle="collapse" data-target="#prescriptionCollapse"><i class="fa fa-history"></i>
                        History</button>
                    <button style="width: 110px !important;float: right;height: 25px;background-color:#337ab7 !important;color:white;" class="btn  fav_dropdown_btn light_purple_bg bg-primary"><i class="fa fa-star"></i>
                        Bookmarks</button>
                    {{-- <span style="margin-left: 10px;">
                        <input type="checkbox" name="print_prescription" checked="checked" id="print_prescription" value="1">
                        <label for="print_prescription">Print Prescription</label>
                    </span> --}}

                </div>
                <div class="col-md-3 padding_sm">
                    @if (session()->has('nursing_station'))
                        {!! Form::select('doctor_prescription', $doctor_list, isset($doctor_id) ? $doctor_id : null, ['class' => 'form-control select2', 'title' => 'Doctor', 'id' => 'doctor_prescription', 'style' => 'color:#555555; padding:2px 12px; ']) !!}
                    @else
                        {!! Form::select('doctor_prescription', $doctor_list, isset($doctor_id) ? $doctor_id : null, ['class' => 'form-control select2', 'title' => 'Doctor', 'id' => 'doctor_prescription', 'style' => 'color:#555555; padding:2px 12px; ', 'disabled' => 'disabled']) !!}
                    @endif
                </div>
                <div class="col-md-3 padding_sm">
                    @if (session()->has('nursing_station'))
                        @php
                            $default_station = session()->get('nursing_station');
                            $list_nursing_station = \DB::table('location')
                                ->where('is_nursing_station', '1')
                                ->orderBy('location_name')
                                ->pluck('location_name', 'id');
                        @endphp
                        {!! Form::select('prescription_nursing_station', $list_nursing_station, isset($default_station) ? $default_station : 0, ['class' => 'form-control select2', 'title' => 'Nursing station', 'id' => 'prescription_nursing_station', 'style' => 'color:#555555; padding:2px 12px;font-weight:600;']) !!}
                    @endif
                </div>

                <div class="col-md-1 padding_sm visit_wise_filter_presc_div " style="display:none;">
                    @php
                        $visit_type_arr = [
                            'OP' => 'OP',
                            'IP' => 'IP',
                            'ALL' => 'ALL',
                        ];
                    @endphp
                    {!! Form::select('visit_wise_filter_presc', $visit_type_arr, 'ALL', ['class' => 'form-control select2', 'title' => 'Visit Wise Filter', 'onchange' => 'loadPrescriptionHistory(1);', 'id' => 'visit_wise_filter_presc', 'style' => 'color:#555555; padding:2px 12px;font-weight:600;']) !!}
                </div>

                <!-- <div class="col-md-2 padding_sm">
                    <button type="button" onclick="saveDoctorPrescriptions(2);" class="btn btn-primary" name="save_and_print_prescription" id="save_and_print_prescription" style="margin-left: 45px;"><i class="fa fa-save"></i> Save and print</button>
                </div> -->

                <!-- Prescription History View -->
                <div class="col-md-12 no-padding">
                    <div class="card collapse" id="prescriptionCollapse">
                        <div class="card_body" id="prescription-history-data-list">
                            @include('Emr::emr.prescription.prescription_history_view')
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht5"></div>
                        <div class="col-md-12 no-padding ">

                        </div>
                    </div>
                </div>
                <!-- Prescription History View -->

                @include('Emr::emr.prescription.prescription_favorite')

            </div>
            <input type="hidden" id="base_url" value="{{URL::to('/')}}">

            <div id="drug" class="tab-pane fade in active">
                <div class="col-md-12 no-padding expand_prescription_btn" style="cursor: pointer;">
                    {{-- <div class="col-md-12" style="padding:0 15px 0 0;">
                        <h4 class="card_title">
                            Prescription <span class="draft_mode_indication_p_consumable"></span>
                        </h4>
                    </div> --}}
                </div>
                <div class="col-md-12" style="padding: 2px;">

                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <div class="card">
                        <div class="card_body" style="background: #FFF;" id="prescription_wrapper_card_body">
                            <div class="col-md-4">
                            <span style="display: inline-block;">
                                <label>Search Medicine by :</label>
                                <div class="radio_container" style="width:170px;">
                                    <input type="radio" id="m_s_type_brand" value="brand"
                                    name="m_search_type" checked >
                                    <label for="m_s_type_brand">Brand</label>

                                    <input type="radio" id="m_s_type_generic" value="generic"
                                    name="m_search_type">
                                    <label for="m_s_type_generic">Generic</label>

                                    <input type="radio" id="m_s_type_both" value="both" name="m_search_type"
                                    checked="">
                                    <label for="m_s_type_both">Both</label>
                                </div>
                            </span>
                            </div>

                            <div class="col-md-8">
                            <span style="display: inline-block;">
                                <label> Intend Type:</label>
                                <div class="radio_container"  style="width:560px !important;">
                                    <input type="radio" id="p_type_regular" value="1" name="p_search_type"
                                    checked="">
                                    <label for="p_type_regular">Regular</label>

                                    <input type="radio" id="p_type_new_adm" value="2" name="p_search_type">
                                    <label for="p_type_new_adm" style="width: 115px !important;">New Admission</label>

                                    <input type="radio" id="p_type_emergency" value="3"
                                    name="p_search_type">
                                    <label for="p_type_emergency">Emergency</label>

                                    <input type="radio" id="p_type_discharge" value="4"
                                    name="p_search_type">
                                    <label for="p_type_discharge">Discharge</label>

                                    <input type="radio" id="p_type_outside" value="5" name="p_search_type">
                                    <label for="p_type_outside" style="    width: 106px !important;"> Own Medicine</label>
                                </div>
                            </span>
                            </div>



                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <table class="table no-margin no-border">
                                <thead>
                                    <tr class="bg-default" style="background-color:#e6e6e6;">
                                        <th style="width: 2%;">&nbsp;</th>
                                        <th style="width: 3%;">&nbsp;</th>
                                        <th style="width: 20%;">Medicine</th>
                                        <th style="width: 13%;">Generic Name</th>
                                        <th style="width: 7%;">Dose</th>
                                        <th style="width: 10%;">Frequency <i class="fa fa-plus"
                                                style="cursor: pointer;" onclick="addFrequency();"></i></th>
                                        <th style="width: 5%;">Days</th>
                                        <th style="width: 5%;">Quantity</th>
                                        <th style="width: 10%;">Start Time</th>
                                        <th style="width: 10%;">Route <i id="addDoctorRoutesBtn" class="fa fa-plus"
                                                style="cursor: pointer;" onclick="addRoute(0);"></i></th>
                                        <th style="width: 10%;">Instructions</th>
                                        @if($enable_ward_stock != 0)
                                        <th style="width: 4%;">W.Stk</th>
                                        <th width="1%" align="center" style="text-align:center"><button
                                                class="btn light_purple_bg" onclick="addNewMedicine();"><i
                                                    class="fa fa-plus"></i></button></th>
                                        @else
                                        <th width="5%" align="center" style="padding:0; padding-left: 15px;text-align:center"><button
                                                class="btn light_purple_bg" onclick="addNewMedicine();"><i
                                                    class="fa fa-plus"></i></button></th>
                                        @endif

                                    </tr>
                                </thead>

                            </table>
                            <select name="search_route" id="search_route" class="form-control" style="display:none;">
                                <option value="">Select Route</option>
                                @if (count($routesList) > 0)
                                    @foreach ($routesList as $ind => $opt)
                                        <option value="{{ $opt->route }}" data-route-id="{{ $opt->id }}">
                                            {{ $opt->route }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="frequency-list-div" style="display: none;">
                                <a style="float: left;" class="close_btn_freq_search">X</a>
                                <div class="freq_theadscroll" style="position: relative;">
                                    <table id="FrequencyTable"
                                        class="table table-bordered-none no-margin table_sm table-striped presc_theadfix_wrapper">
                                        {{-- <thead>
                                        <tr class="light_purple_bg">
                                            <th>Frequency</th>
                                        </tr>
                                        </thead> --}}
                                        <tbody id="ListFrequencySearchData">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <div class="fav_dropdown fav_prescription_box" style="">
                                <div class="slidedown_close_btn fav_dropdown_close right_close presc_close_fav"> X
                                </div>
                                <input type="hidden" class="fav_service_code" value="" name="fav_service_code">
                                <div class="col-md-12 padding_sm">
                                    <h4 class="card_title">Create Group</h4>
                                    <div class="clearfix"></div>
                                    <div class="ht5"></div>
                                    <div class="col-md-6 padding_xs">
                                        <input type="text" class="form-control presc_fav_add_group_text"
                                            placeholder="Group Name" name="presc_fav_add_group_text">
                                    </div>
                                    <div class="col-md-4 text-right padding_xs">
                                        <button type="button" class="btn btn-block btn-primary presc_add_group_btn"><i
                                                class="fa fa-plus presc_add_group_icon"></i> Add Group</button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="h10"></div>
                                    <div class="" style="height: auto;  margin-top:10px;">
                                        <h4 class="card_title">Available Groups</h4>
                                        <div class="clearfix"></div>
                                        <div class="ht5"></div>
                                        <ul class="nav sm_nav vertical_tab_btn_fav_group text-center presc_add_fav_group_list"
                                            style="max-height: 350px; overflow-y:scroll;"></ul>
                                    </div>

                                </div>

                            </div>

                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <div id="presc-data-form" action="" style="min-height:200px;">
                                <div class="" style="">
                                    <table border="1"
                                        style="width: 100%;height: 15%;border:1px solid;border-collapse: collapse !important"
                                        class="table  table_sm  table-bordered" id="medicine-listing-table">
                                        <tbody>

                                        </tbody>
                                    </table>
                                    @if ($is_nursing_station == 0)
                                        <div class="col-md-2 padding_sm" style="float:right;">
                                            <button type="button" onclick="saveDoctorPrescriptions(2);"
                                                class="btn btn-primary save_class" name="save_and_print_prescription1"
                                                id="save_and_print_prescription1" style="margin-left: 45px;"><i
                                                    class="fa fa-save"></i> Save and print</button>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div id="consumables" class="tab-pane">
                <div class="col-md-12" style="padding:0 15px 0 0;">
                    <h4 class="card_title">
                        Prescription <span class="draft_mode_indication_p"></span>
                    </h4>
                </div>
                <div class="col-md-12" style="padding: 2px;">
                    <div class="card">
                        <div class="card_body" style="background: #FFF;"
                            id="prescription_consumable_wrapper_card_body">
                            <table class="table no-margin table_sm table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Search Medicine by :</td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_m_s_type_brand" value="brand"
                                                    name="consumable_m_search_type" checked="">
                                                <label for="consumable_m_s_type_brand"> Brand </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_m_s_type_generic" value="generic"
                                                    name="consumable_m_search_type">
                                                <label for="consumable_m_s_type_generic"> Generic </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_m_s_type_both" value="both"
                                                    name="consumable_m_search_type">
                                                <label for="consumable_m_s_type_both"> Both </label>
                                            </div>
                                        </td>
                                        <td width="25%">&nbsp;</td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_p_type_regular" value="1"
                                                    name="consumable_p_search_type" checked="">
                                                <label for="consumable_p_type_regular"> Regular </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_p_type_new_adm" value="2"
                                                    name="consumable_p_search_type">
                                                <label for="consumable_p_type_new_adm"> New Admission </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_p_type_emergency" value="3"
                                                    name="consumable_p_search_type">
                                                <label for="consumable_p_type_emergency"> Emergency </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_p_type_discharge" value="4"
                                                    name="consumable_p_search_type">
                                                <label for="consumable_p_type_discharge"> Discharge </label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <table class="table no-margin no-border">
                                <thead>
                                    <tr class="table_header_bg header_normal_padding">
                                        <th>Medicine</th>
                                        <th>Quantity</th>
                                        <th colspan="2">Instructions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="35%">
                                            <input type="text" id="consumable_search_medicine"
                                                name="consumable_search_medicine" autocomplete="off"
                                                class="form-control" placeholder="Type at least 3 characters">

                                            <input type="hidden" name="consumable_search_item_code_hidden" value="">
                                            <input type="hidden" name="consumable_search_item_name_hidden" value="">
                                            <input type="hidden" name="consumable_search_item_price_hidden" value="">

                                            <!-- Medicine List -->
                                            <div class="consumable-medicine-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_med_search">X</a>
                                                <div class=" presc_theadscroll" style="position: relative;">
                                                    <table id="ConsumableTable"
                                                        class="table table-bordered-none no-margin table_sm table-striped presc_theadfix_wrapper">
                                                        <thead>
                                                            <tr class="light_purple_bg">
                                                                <th>Medicine</th>
                                                                <th>Stock</th>
                                                                <th>Price</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="ConsumableListMedicineSearchData">

                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                            <!-- Medicine List -->

                                        </td>
                                        <td width="10%">
                                            <input type="text" name="consumable_search_quantity" autocomplete="off"
                                                id="consumable_search_quantity" class="form-control">
                                        </td>
                                        <td width="20%">
                                            <input type="text" name="consumable_search_instructions" autocomplete="off"
                                                id="consumable_search_instructions" class="form-control">
                                        </td>
                                        <td width="3%" align="center"><button class="btn light_purple_bg"
                                                onclick="addNewConsumable();"><i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="14" class="partition_line"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <form id="consumable-presc-data-form" action="#">
                                <div class="theadscroll" style="position: relative; height: 244px;">
                                    <table border="1"
                                        style="width: 100%;border:1px solid;border-collapse: collapse !important"
                                        class="table  table_sm  table-bordered" id="consumable-medicine-listing-table">
                                        <tbody>

                                        </tbody>
                                    </table>
                                    <div class="col-md-2 padding_sm" style="float:right;">
                                        <button type="button" onclick="saveDoctorConsumablePrescriptions();"
                                            class="btn btn-primary" name="save_and_print_prescription2"
                                            id="save_and_print_prescription2" style="margin-left: 45px;"><i
                                                class="fa fa-save"></i> Save and print</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 hide_class" style="margin: 5px;">
            <br>
            <div class="col-md-3 no-padding"><label>Approx Amount</label></div>
            <div class="col-md-1 padding_sm">
                <label id="med_total_amnt"></label>
                <input type="hidden" value="0" name="approx_amnt" id="approx_amnt">
            </div>

            @if ($is_nursing_station == 0)
                <div class="col-md-1 no-padding"><label>Next Review </label></div>
                <div class="col-md-3 padding_sm">
                    <input type="text" class="form-control bottom-border-text" name="next_review_date" id="next_review_date"
                        onblur="update_prescription_footer_fields()" value="" autocomplete="off">
                </div>
            @endif


            <div class="col-md-2 no-padding">
                &nbsp;<i class="fa fa-square" style="color: #f4c4a6;"></i>&nbsp; Allergy
            </div>
            <div class="col-md-2 no-padding">
                &nbsp;<i class="fa fa-square" style="color: #C9D5F3;"></i>&nbsp; Outside
            </div>
            <div class="clearfix"></div>
            <div class="h10"></div>

            @if ($is_nursing_station == 0)
                <div class="col-md-4 padding_sm" style="margin-top:15px;">
                    <label>Reason For Visit</label>
                    <textarea id="visit_reason" name="visit_reason" class="form-control bottom-border-text"
                        onblur="update_prescription_footer_fields()" style="height:90px !important;border-bottom: 2px solid lightgrey !important;" ></textarea>
                </div>
                <div class="col-md-4 padding_sm" style="margin-top:15px;">
                    <label>Diagnosis</label>
                    <textarea id="pres_history" name="pres_history" class="form-control bottom-border-text"
                        onblur="update_prescription_footer_fields()" style="height:90px !important;border-bottom: 2px solid lightgrey !important;" ></textarea>
                </div>
                <div class="col-md-4 padding_sm" style="margin-top:15px;">
                    <label>Special Notes</label>
                    <textarea id="pres_notes" name="pres_notes" class="form-control bottom-border-text"
                        onblur="update_prescription_footer_fields()" style="height:90px !important;border-bottom: 2px solid lightgrey !important;" ></textarea>
                </div>
            @endif

        </div>
    </div>
</div>

<!-- Prescription Print Config Modal -->
<div id="prescription_print_config_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 30%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Print Config</h4>
            </div>
            <div class="modal-body" style="height:200px;">
                <div class="col-md-12" id="prescription_print_config_list_data">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="print_assessment_check" id="print_assessment_check">
                        Clinical Assessment
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="print_investigation_check" id="print_investigation_check">
                        Investigation
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="print_prescription_check" id="print_prescription_check" checked="checked">
                        Prescription
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="printPatientPrescriptionWithInvestigation();" id="print_prescription_and_investigation_btn" class="btn bg-primary pull-right" style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Prescription Print Config Modal -->


<script src="{{ asset('packages/extensionsvalley/emr/js/prescription.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
