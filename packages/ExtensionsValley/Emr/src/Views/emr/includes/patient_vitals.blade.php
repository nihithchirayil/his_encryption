@php
$vitals =  ExtensionsValley\Emr\VitalMaster::getVitalEntryDetails($patient_id,$age);
$batch = 0;
$bp_systolic = '';
$bp_diastolic = '';
$pulse = '';
$respiration = '';
$temperature_loc ='';
$oxygen = '';
$head_circ = '';
$bmi ='';
$weight_status = '';
$spo2status = '';
$bmistatus = '';
$weight_val = '';
$height_val = '';
$bp_systolic_status = '';
$bp_diastolic_status = '';
$spirometry = '';
$pulse_status = '';
$temperature_status = '';
$temp = '';
$temp_f = '';
$time_taken = date('M-d-Y h:i A');
$vitals_count = count($vitals);
$ShowFarenheit = @$ShowFarenheit ? $ShowFarenheit :0;

@endphp
<?php
foreach ($vitals as $vital) {
    if (isset($vital->batch_no) && $vital->batch_no != '') {
        $batch = $vital->batch_no;
    }
    if (!empty($vital->vital_master_id)) {
        if ($vital->vital_master_id == 1 || $vital->vital_master_id == 2) {
            $weight_status = $vital->vital_status;
            $weight_val = $vital->vital_value;
        }
        if ($vital->vital_master_id == 3 || $vital->vital_master_id == 4) {
            $height_val = $vital->vital_value;
        }
        if ($vital->vital_master_id == 5) {
            $bp_systolic_status = $vital->vital_status;
            $bp_systolic = $vital->vital_value;
        }
        if ($vital->vital_master_id == 6) {
            $bp_diastolic_status = $vital->vital_status;
            $bp_diastolic = $vital->vital_value;
        }
        if ($vital->vital_master_id == 7) {
            $pulse_status = $vital->vital_status;
            $pulse = $vital->vital_value;
        }
        if ($vital->vital_master_id == 8) {
            $respiration = $vital->vital_value;
        }
        if ($vital->vital_master_id == 9) {
            $temperature_status = $vital->vital_status;
            $temp_f = $vital->vital_value;
        }
        if ($vital->vital_master_id == 10) {
            $temperature_status = $vital->vital_status;
            $temp = $vital->vital_value;

        }


        if ($vital->vital_master_id == 11) {
            $temperature_loc = $vital->vital_value;
        }
        if ($vital->vital_master_id == 12) {
            $spo2status = $vital->vital_status;
            $oxygen = $vital->vital_value;
        }
        if ($vital->vital_master_id == 13) {
            $head_circ = $vital->vital_value;
        }
        if ($vital->vital_master_id == 14) {
            $bmistatus = $vital->vital_status;
            $bmi = $vital->vital_value;
        }
        if ($vital->vital_master_id == 20) {
            $spirostatus = $vital->vital_status;
            $spirometry = $vital->vital_value;
        }
    }
    if (isset($vital->time_taken) && $vital->time_taken != '') {
        $time_taken = date('M-d-Y h:i A',strtotime($vital->time_taken));
    }

}
?>
<style>
.blinking-red{
    animation:blinkingText 0.8s infinite;

}
@keyframes blinkingText{
    0%{     color: #FF0000;    }
    49%{    color: #FF0000; }
    60%{    color: transparent; }
    99%{    color:transparent;  }
    100%{   color: #FF0000;    }
}

.blinking-orange{
    animation:blinkingTextOrange 0.8s infinite;

}
@keyframes blinkingTextOrange{
    0%{     color: #FFA500;    }
    49%{    color: #FFA500; }
    60%{    color: transparent; }
    99%{    color:transparent;  }
    100%{   color: #FFA500;    }
}
</style>
<div class="no-border no-margin" id="vital_entry_details">
    <div class="box-body vital_body" style='height:86px;border-radius:5px;'>
        <table class="table table_sm no-border no-margin table_bordered" id="vital_tble">
            <tbody>
                <tr>
                    <td width="8%"><b>Weight</b></td>
                    <td width="3%"><b>:</b></td>
                    @if($weight_status && $weight_status == 'upper')
                    <td class="blinking-red"><b>{{$weight_val}}</b></td>
                    @elseif($weight_status && $weight_status == 'under')
                    <td class="blinking-orange"><b>{{$weight_val}}</b></td>
                    @else
                    <td><b>{{$weight_val}}</b></td>
                    @endif

                    <td width="8%"><b>BP</b></td>
                    <td width="3%"><b>:</b></td>

                    @if($bp_systolic_status && $bp_systolic_status == 'upper')
                    <td class="blinking-red"><b>{{$bp_systolic}}</b></td>
                    @elseif($bp_systolic_status && $bp_systolic_status == 'under')
                    <td class="blinking-orange"><b>{{$bp_systolic}}</b></td>
                    @else
                    <td class=""><b>{{$bp_systolic}}</b></td>
                    @endif



                    <td >@if($bp_diastolic || $bp_diastolic) / @endif </td>

                    @if($bp_diastolic_status && $bp_diastolic_status == 'upper')
                    <td class="blinking-red"><b>{{$bp_diastolic}}</b></td>
                    @elseif($bp_diastolic_status && $bp_diastolic_status == 'under')
                    <td class="blinking-orange"><b>{{$bp_diastolic}}</b></td>
                    @else
                    <td class=""><b>{{$bp_diastolic}}</b></td>
                    @endif

                    <td width="2%" >

                        <button style="margin-bottom: 4px; padding: 1px 6px;border:1px solid green;" class="btn btn-sm btn-default"  data-toggle="modal" data-target="#editvitalModal" onclick="clear_vital_vlues();" >
                            <i class="fa fa-plus" id="add_vital_modal_btn"></i>
                        </button>

                    </td>
                </tr>
                <tr>
                    <td width="8%"><b>Temp</b></td>
                    <td width="3%"><b>:</b></td>
                    @php
                    $tempc =''; 
                    $tempf =''; 
                    $temper =''; 
                   
                    if($temp_f !='' || $temp != ''){
                    $tempf = '°f' ;  
                    $tempc = '°C' ;                        
                    if($ShowFarenheit == 0){
                        $temper = "( $temp_f  $tempf )" ;                        

                    }else{
                        $temper = "(  $temp $tempc )" ;                        
                    }
                  
                    }
                    @endphp
                    @if($temperature_status && $temperature_status == 'upper')
                        @if($ShowFarenheit == 0)
                            <td class="blinking-red"><b>{{$temp }}{{ $tempc }} {{ $temper }}</b></td>
                        @else 
                            <td class="blinking-red"><b>{{$temp_f }}{{ $tempf }} {{ $temper}}</b></td>
                        @endif
                    @elseif($temperature_status && $temperature_status == 'under')
                        @if($ShowFarenheit == 0)
                            <td class="blinking-orange"><b>{{$temp }}{{ $tempc }} {{ $temper }}</b></td>
                        @else 
                            <td class="blinking-orange"><b>{{$temp_f }}{{ $tempf }} {{ $temper}}</b></td>
                        @endif
                    @else
                        @if($ShowFarenheit == 0)
                        <td ><b>{{$temp }}{{ $tempc }} {{ $temper }}</b></td>
                        @else 
                        <td ><b>{{$temp_f }}{{ $tempf }} {{ $temper}}</b></td>
                        @endif
                    @endif

                    <td width="8%"><b>Pulse</b></td>
                    <td width="3%"><b>:</b></td>
                    @if($pulse_status && $pulse_status == 'upper')
                    <td colspan="3" class="blinking-red"><b>{{$pulse}}</b></td>
                    @elseif($pulse_status && $pulse_status == 'under')
                    <td colspan="3" class="blinking-orange"><b>{{$pulse}}</b></td>
                    @else
                    <td colspan="3" class=""><b>{{$pulse}}</b></td>
                    @endif

                    <td width="2%" >
                        @if($batch !=0)
                                <button style="margin-bottom: 1px; padding: 1px 5px;border:1px solid green;" class="btn btn-sm btn-default"  data-toggle="modal" data-target="#editvitalModal" onclick="edit_vital_values({{$batch}});"  >
                                <i class="fa fa-edit" id="edit_vital_modal_btn"></i>
                                </button>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td width="10%"><b>SpO2</b></td>
                    <td width="3%"><b>:</b></td>
                    @if($spo2status && $spo2status == 'upper')
                    <td class="blinking-red"><b>{{$oxygen}}</b></td>
                    @elseif($spo2status && $spo2status == 'under')
                    <td class="blinking-orange"><b>{{$oxygen}}</b></td>
                    @else
                    <td class=""><b>{{$oxygen}}</b></td>
                    @endif

                    <td width="10%"><b>BMI</b></td>
                    <td width="3%"><b>:</b></td>
                    @if($bmistatus && $bmistatus == 'upper')
                    <td colspan="3" class="blinking-red"><b>{{$bmi}}</b></td>
                    @elseif($bmistatus && $bmistatus == 'under')
                    <td colspan="3" class="blinking-orange"><b>{{$bmi}}</b></td>
                    @else
                    <td colspan="3" class=""><b>{{$bmi}}</b></td>
                    @endif


                    <td width="2%"  >
                        <button style="margin-bottom: 1px; padding: 1px 4px;margin-top:3px;border:1px solid green;" class="btn btn-sm btn-default" data-toggle="modal" data-target="#vital_graph_modal" onclick="show_vital_history()" ><i class="fa fa-line-chart"></i></button>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="editvitalModal" tabindex="-1" role="dialog" aria-labelledby="vitalModal" aria-hidden="true">
    <div class="modal-dialog" style="width:43%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                Patient Vitals
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" value="{{$batch}}" id="vital_batch">
                        <input type="hidden" value="9" id="temperature">

                        <div>
                            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table">
                                <tr>
                                    <td><label for="">Weight in Kg</label></td>
                                    <td>    <input type="text" class="form-control" name="weight_value" id="weight_value" value="{{$weight_val}}">
                                    </td>

                                    <td><label for="">Height/Length in cm</label></td>
                                    <td><input type="text" class="form-control" name="height_value" id="height_value"
                                        value="{{$height_val}}">
                                    </td>
                                </tr>

                                @php
                                    $weight =  ExtensionsValley\Emr\VitalMaster::getVitalDetails('Weight');
                                    $height =  ExtensionsValley\Emr\VitalMaster::getVitalDetails('Height');
                                    $temperature  =  ExtensionsValley\Emr\VitalMaster::getVitalDetails('Temperature');
                                @endphp
                                {!! Form::select('weight', $weight->toArray(),'', ['class' => 'form-control hidden','id' => 'weight','onchange' => 'weightSelect(this)']) !!}
                                {{-- {!! Form::select('temperature', $temperature->toArray(),'', ['class' => 'form-control hidden','id' => 'temperature','onchange' => 'temperatureSelect(this)']) !!} --}}
                                {!! Form::select('height', $height->toArray(),'', ['class' => 'form-control hidden','id' => 'height','onchange' => 'heightSelect(this)']) !!}

                                <tr>
                                    <td><label for="">BP Systolic mmHg</label></td>
                                    <td><input type="text"  value="{{$bp_systolic}}" class="form-control" name="bp_systolic" id="bp_systolic">
                                    </td>

                                    <td><label for="">BP Diastolic mmHg</label></td>
                                    <td><input type="text" class="form-control" name="bp_diastolic" id="bp_diastolic"
                                        value="{{$bp_diastolic}}">
                                    </td>

                                </tr>

                                <tr>

                                    <td><label for="">Temperature in C</label></td>
                                    <td><input type="text" class="form-control int_type " name="temperature_value" id="temperature_value"
                                        value="{{$temp}}">
                                    </td>
                                    <td><label for="">Temperature in F</label></td>
                                    <td><input type="text" class="form-control int_type " name="temperature_value_f" id="temperature_value_f"
                                        value="{{$temp_f}}">
                                    </td>
                                </tr>



                                <tr>
                                    <td><label for="">Pulse/Min</label></td>
                                    <td><input type="text" class="form-control int_type " name="pulse" id="pulse"
                                        value="{{$pulse}}">
                                    </td>
                                    <td><label for="">Respiration/Min</label></td>
                                    <td><input type="text" class="form-control int_type " name="respiration" id="respiration"
                                        value="{{$respiration}}">
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="">Head Circ-cm</label></td>
                                    <td><input type="text" class="form-control" name="head" id="head"
                                        value="{{$head_circ}}">
                                    </td>
                                    <td><label for="">Oxygen Saturation %</label></td>
                                    <td><input type="text" class="form-control" name="oxygen" id="oxygen"
                                        value="{{$oxygen}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="">BMI Kg/M^2</label></td>
                                    <td><input type="text" class="form-control" name="bmi" id="bmi"
                                        value="{{$bmi}}">
                                    </td>
                                    <td><label for="">Spirometry</label></td>
                                    <td><input type="text" class="form-control" name="spirometry" id="spirometry"
                                        value="{{$spirometry}}">
                                    </td>                                    
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>


                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <div class="col-md-2"><label for="">Time Taken</label></div>
                    <div class="col-md-4"><input type="text" class="form-control date_time_picker" name="time_taken" id="time_taken" value="{{$time_taken}}"/>
                    </td></div>
                    <div class="col-md-6">
                        <button style="width:90px;" class="btn btn-default col-md-2  pull-right light_purple_bg" id="update_vital" onclick="saveVitals()"> Save </button>
                        <button style="width:90px;" type="button" class="btn btn-default col-md-2  pull-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
