        <div class="col-md-12 no-padding global_search_dis_container" style="margin-bottom: 15px;">
            <div class="col-md-4 no-padding" id="global_search_dis">
                <div class="input-group custom-float-label-control">
                    <span style="color:rgb(80, 80, 80);">General Patient Search</span>
                        <input class="form-control global_op_no" value=""  autocomplete="off" type="text" placeholder="" name="op_no" />
                        <div id="OpAjaxDiv_dis" class="ajaxSearchBox" style="z-index:1005;width:295px; top:40px;"></div>
                    <input value=""  type="hidden" name="op_no_hidden" value="" id="op_no_hidden">
                </div>
            </div>
            <div class="col-md-1 no-padding">
                <div class="input-group custom-float-label-control">
                    <span style="color:rgb(80, 80, 80);">&nbsp;</span>
                    <button class="btn btn-block btn-primary resetGlobalPatientSearch"><i class="fa fa-refresh"></i> Reset </button>
                </div>
            </div>
            
        </div>
        <div class="clearfix"></div>

        <div class="theadscroll always-visible" style="position: relative; height: 310px;">
            @if(sizeof($res)>0)
            <table class="table table_sm table-bordered theadfix_wrapper">
                <thead>
                    <tr class="table_header_bg">
                        <th>Sl No</th>
                        <th>Admit Date</th>
                        <th>Admit Doctor</th>
                        <th>Patient Name</th>
                        <th>Uhid</th>
                        <th>Discharge Date</th>
                        <th>Summary Prepared At</th>
                        <th>Finalize</th>
                        <th>Summary</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i =1;
                    @endphp
                    @foreach($res as $data)

                        <tr>
                            <td >{{$i}}</td>
                            <td >{{date('M-d-Y',strtotime($data->admit_date))}}</td>
                            <td >{{$data->doctor_name}}</td>
                            <td >{{$data->patient_name}}</td>
                            <td >{{$data->uhid}}</td>
                            <td >
                                @if($data->discharge_date !='')
                                    {{date('M-d-Y',strtotime($data->discharge_date))}}
                                @endif
                            </td>
                            <td >{{date('M-d-Y',strtotime($data->last_updated_at))}}</td>
                            @if($data->status !=1 && $data->final_status !=0)
                            <td style="text-align:center">
                                    <button title="Mark as Finalized" onclick="finalizeDischargeSummary('{{$data->id}}');" class="btn btn-success">
                                        <i class="fa fa-check"></i>
                                    </button>

                            </td>
                            @elseif($data->status == 1)
                            <td style="text-align:center"><span style="color:green;">Finalized</span></td>
                            @else
                            <td style="text-align:center"></td>
                            @endif
                            @if($data->final_status !=0)
                            <td style="text-align:center">
                                    <button title="View Summary" onclick="showSummary('{{$data->id}}');" class="btn bg-primary">
                                        <i class="fa fa-list"></i>
                                    </button>

                            </td>
                            @else
                            <td style="text-align:center"><span style="color:red;">Summary Not Generated</span></td>
                            @endif

                        </tr>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
            @else
            <span>No Summary found </span>
            @endif
        </div>
        @if(!empty($res))

        <div id="pagination">{{{ $res->links() }}}</div>
        @endif
