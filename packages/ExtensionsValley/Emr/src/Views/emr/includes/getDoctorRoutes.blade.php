<div class="row">
    <div class="col-md-6 padding_sm">
        <input type="text" name="route_nameserach" class="form-control" onkeyup="routeListSorting('route_list_table','route_nameserach')" id="route_nameserach" autocomplete="off" placeholder="Search Route">
    </div>
    <div class="col-md-4 padding_sm">
        <input type="text" name="route_name" class="form-control" id="route_name" autocomplete="off" placeholder="Add Route">
    </div>
    <div class="col-md-2 padding_sm">
        <button type="button" id="saveRouteBtn" class="btn btn-success btn-block" onclick="save_route();">Save <i class="fa fa-save" id="saveRouteSpin"></i></button>
    </div>

    <div class="theadscroll always-visible" style="position: relative;height:300px;">
        <table id="route_list_table" class="table table_sm table-bordered theadfix_wrapper">
            <thead>
                <tr class="table_header_bg">
                    <th style="width:90%;">Route</th>
                    <th style="width:10%;">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($route_list as $data)
                    <tr id="prescriptiondata{{ $data->id }}">
                        <td>{{ $data->route }}</td>
                        <td style="text-align:center;"><button id="deleteprescriptionbtn<?= $data->id ?>"
                                onclick="delete_route('{{ $data->id }}','{{ base64_encode($data->route) }}');"
                                class="btn btn-danger">
                                <i class='fa fa-trash' id="deleteprescriptionspin<?= $data->id ?>"
                                    style="cursor: pointer;"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
