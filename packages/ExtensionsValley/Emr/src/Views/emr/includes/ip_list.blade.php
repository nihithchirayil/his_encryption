
<div class="box no-margin no-border">
    <div class="box-body bg-white clearfix">

        <div class="col-md-12 padding_sm">
            <div class="box no-border no-margin anim">
                <div class="col-md-4 box-body clearfix">
                    <div class="col-md-6 padding_sm">

                        <label for="">Search</label>
                        <div class="clearfix"></div>
                        <div class="input-group">
                            <input type="text" name="patient_search_txt" id="patient_search_txt" onkeyup="search_ip_patient(this.value);" class="form-control" >
                            <div class="input-group-btn">
                                <button style="height: 23px;
                                width: 30px;" class="btn bg_ip_title"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm">
                        <div class="btn-group" data-toggle="buttons" style="margin-top:24px;">
                            <label class="btn bg_ip_title active">
                            <input type="radio" id="ip_name_check" value="1" name="search_type" checked="checked"> Name
                            </label>
                            <label class="btn bg_ip_title">
                            <input type="radio" id="opnumber_checkbox" value="2" name="search_type"> UHID
                            </label>
                            <label class="btn bg_ip_title ">
                            <input type="radio" id="phone_checkbox" value="3" name="search_type" > Phone
                            </label>
                    </div>
                    </div>
                </div>
                <div class="col-md-4 box-body clearfix" style="height:67px;">
                    <div class="col-md-4 padding_sm ">
                        <label for="">From Date</label>
                        <div class="clearfix"></div>
                        <div class="input-group">
                            <input type="text" id='ip_patient_search_from_date' class="form-control datepicker" value="{{$search_date_web}}" data-attr="date" placeholder="Date">
                        </div>
                    </div>
                    <div class="col-md-4 padding_sm ">
                        <label for="">To Date</label>
                        <div class="clearfix"></div>
                        <div class="input-group">
                            <input type="text" id='ip_patient_search_to_date' class="form-control datepicker" value="{{$search_date_web}}" data-attr="date" placeholder="Date">
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm ">
                        <input style="margin-top:24px;" type="button" class="btn bg-primary" name="btn_search_ip_patients" value="Search" onclick="get_ip_list_by_daterange();" id="btn_search_ip_patients"/>
                    </div>
                </div>
                <div class="col-md-2 box-body clearfix" style="height:67px;text-align:center">
                    <button style="margin-top:24px;" type="button" class="btn bg-primary" name="btn_current_occupancy" onclick="get_current_occupancy();" id="btn_current_occupancy">
                        <i class="fa fa-class"></i>
                        Current Ocuupancy
                    </button>
                </div>
                @php
                    $user_id = \Auth::user()->id;
                    $nursing_locations =  \DB::table("location")->where('is_nursing_station', 1)
                        ->orderBy('location_name', 'ASC')
                        ->whereNull('deleted_at')
                        ->where('status', 1)
                        ->pluck('location_name', 'id');
                    $nursing_locations = array("0"=> " Select Nursing Station") + $nursing_locations->toArray();

                @endphp
                <div class="col-md-2 box-body clearfix" style="height:67px;">
                    <div class="col-md-12 padding_sm ">
                        <label for="">Nursing Station</label>
                        <div class="clearfix"></div>
                        {!! Form::select('nursing_station_select', $nursing_locations, null, ['class' => 'form-control','id' => 'nursing_station_select', 'onchange'=> 'filter_nursing_station();', 'style' => ' color:#555555; padding:4px 12px;']) !!}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="ht10"></div>
        </div>

        <div class="col-md-9 padding_sm">
            

            <div id="ip_patients_list_data">
                {!!$current_occupancy!!}
            </div>
        </div>

        <div class="col-md-3 padding_sm">
            <div class="right_btn_widget">
                <ul class="list-unstyled no-margin no-padding">

                    <li>
                        <button class="btn" style="background: #ffeeca !important;">
                            <h5 style="margin:0px"><i style="margin-left: -10px;
                                margin-right: 8px;" class="fa fa-medkit"aria-hidden="true"></i></h5>  Med-Approve<span class="badge badge_purple_bg" style="margin-left: 5px;padding-right: 13px;">0</span>
                        </button>
                    </li>
                    <li>
                        <button class="btn" style="background: #ffeeca !important;">
                            <h5 style="margin:0px"><i class="fa fa-flask"aria-hidden="true"></i></h5> Lab Results <span class="badge badge_purple_bg">0</span>
                        </button>
                    </li>
                    <li>
                        <button class="btn" style="background: #ffeeca !important;">
                            <h5 style="margin:0px"><i class="fa fa-user-plus"aria-hidden="true"></i></h5> Referral by <span class="badge badge_purple_bg">0</span>
                        </button>
                    </li>
                    <li>
                        <button class="btn" style="background: #ffeeca !important;">
                            <h5 style="margin:0px"><i class="fa fa-exclamation"aria-hidden="true"></i></h5> High Risk <span class="badge badge_purple_bg">0</span>
                        </button>
                    </li>

                </ul>
            </div>
        <div class="clearfix"></div>
        <div class="ht10"></div>
            <div class="right_btn_list">
                <ul class="list-unstyled">

                    <li>
                        <div class="but_text">Discharge Intimated</div> <span id="discharge_intimation_count" class="badge badge_purple_bg">0</span>
                    </li>
                    <li>
                        <div class="but_text">Summary</div>
                        <span class="badge badge_purple_bg">0</span>
                    </li>
                </ul>
            </div>
        <div class="clearfix"></div>
        <div class="ht10"></div>
        <div class="col-md-12" style="color: black;margin-left:-10px;width:107% !important;">
            <table class="table table-bordered" style="width: 100%;font-size:12px;">
                <tr>
                    <td><button onclick="apply_filter('share_holder_status');" type="button" class="btn btn-sm bg-orange">S</button>
                        <span>Share Holder</span>
                    </td>
                    <td>
                        <button onclick="apply_filter('is_highrisk');" type="button" class="btn btn-sm bg-red">H</button>
                        <span>High Risk</span>
                    </td>
                </tr>
                <tr>
                    <td><button onclick="apply_filter('is_mlc');" type="button" class="btn btn-sm" style="background-color:#6effc3">M</button>
                        <span>MLC</span>
                    </td>
                    <td>
                        <button onclick="apply_filter('referred_to_status');" type="button" class="btn btn-sm" style="background-color:#75dfff">R</button>
            <span>Doctor Referral</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input onclick="apply_filter('all');" type="button"  class="btn btn-sm bg-info pull-right" value="Show all"/>
                    </td>
                </tr>
            </table>
        </div>
        </div>
    </div>
</div>
