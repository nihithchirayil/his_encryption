
<form method="POST" id="io_chart" name="io_chart" class="col-md-12">
    <table class="col-md-12 table no-margin table_sm table-bordered" id="intake_output_border">
        <thead>
            <tr>
                <td colspan="8" style="text-align: center">
                    <h4>INTAKE-OUTPUT RECORD</h4>
                    <span style="float:right">
                        <button type="button" class="btn bg-orange disabled" onclick="show_ip_chart();">
                            <i class="fa fa-bar-chart" aria-hidden="true"></i> Chart
                        </button>
                        <button type="button" class="btn bg-purple" onclick="show_io_report();">
                            <i class="fa fa-bar-chart" aria-hidden="true"></i> Report
                        </button>

                    </span>
                </td>
            </tr>
        </thead>
    </table>
    <div class="col-md-6 padding_sm">
        <table class="table no-margin table_sm table-bordered" id="intake_border">
            <thead>
                <tr>
                    <td colspan="8" style="text-align: center">
                        <h4>INTAKE</h4>
                        <span style="float:right">
                            <button type="button" class="btn bg-primary" onclick="SaveIntake();">
                                <i class="fa fa-save"></i> Save
                            </button>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="width:40%">Date&Time</td>
                    <td style="width:15%">IV</td>
                    <td style="width:15%">Oral</td>
                    <td style="width:13%">Total</td>
                    <td style="width:7%"></td>
                </tr>
                @if(isset($intake_list) && count($intake_list) > 0)
                @foreach($intake_list as $intake => $data)

                @php
                    $intake_time = ($data->date_and_time !='') ? date('H:i:s', strtotime($data->date_and_time)) : $search_time_web;
                    $intake_date = ($data->date_and_time !='') ? date('M-d-Y', strtotime($data->date_and_time)) : $search_date_web;
                @endphp
                <tr>
                    <td>
                        <div class="col-md-7 padding_xs" style="text-align:center">
                            <input type="text" value="{{$intake_date}}" id='io_date_3' name="io_date[]" class="form-control datepicker intake" data-attr="date" placeholder="Date">
                        </div>
                        <div class="col-md-5 padding_xs" style="text-align:center">
                            <input type="text" value="{{$intake_time}}" onclick='add_new_intake(this);' id='io_time_3' name="io_time[]" class="form-control intake timepicker" data-attr="date" placeholder="Time">
                        </div>
                    </td>
                    <td>
                        <input type="text" onclick='add_new_intake(this);' onblur="calculate_intake(3);" id="intake_iv_3"  name="intake_iv[]" value="{{$data->intake_iv}}" class="form-control int_type intake"><input type="hidden" id="intake_id" name="intake_id[]" value="{{$data->id}}" >
                    </td>
                    <td>
                        <input type="text" onclick='add_new_intake(this);' onblur="calculate_intake(3);"  id="intake_oral_3"  name="intake_oral[]" value="{{$data->intake_oral}}" class="form-control int_type intake"></td>
                    <td>
                        <input type="text" readonly id="intake_total_3" value="{{$data->intake_total}}" name="intake_total[]"  class="form-control int_type intake">
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm bg-red del-intake-list-row" name="delete_intake[]" id="delete_intake_3 ">
                            <i class="fa fa-trash"></i>
                        </button>
                    </td>
                </tr>
                    @endforeach
            @else
                       <tr>
                    <td>
                        <div class="col-md-7 padding_xs" style="text-align:center">
                            <input type="text" value="{{$search_date_web}}" id='io_date_3' name="io_date[]" class="form-control datepicker intake" data-attr="date" placeholder="Date">
                        </div>
                        <div class="col-md-5 padding_xs" style="text-align:center">
                            <input type="text" value="{{$search_time_web}}" onclick='add_new_intake(this);' id='io_time_3' name="io_time[]" class="form-control intake timepicker" data-attr="date" placeholder="Time">
                        </div>
                    </td>
                    <td>
                        <input type="text" onclick='add_new_intake(this);' onblur="calculate_intake(3);" id="intake_iv_3"  name="intake_iv[]"  class="form-control int_type intake">
                    </td>
                    <td>
                        <input type="text" onclick='add_new_intake(this);' onblur="calculate_intake(3);"  id="intake_oral_3"  name="intake_oral[]"  class="form-control int_type intake"></td>
                    <td>
                        <input type="text" readonly id="intake_total_3"  name="intake_total[]"  class="form-control int_type intake">
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm bg-red del-intake-list-row" name="delete_intake[]" id="delete_intake_3 ">
                            <i class="fa fa-trash"></i>
                        </button>
                    </td>
                </tr>
            @endif
            </thead>
        </table>
    </div>
    <div class="col-md-6 padding_sm">
        <table class="table no-margin table_sm table-bordered" id="output_border">
            <thead>
                <tr>
                    <td colspan="8" style="text-align: center">
                        <h4>OUTPUT</h4>
                        <span style="float:right">
                            <button type="button" class="btn bg-primary" onclick="SaveOutPut();">
                                <i class="fa fa-save"></i> Save
                            </button>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="width:40%">Date&Time</td>
                    <td style="width:15%">Urine</td>
                    <td style="width:15%">Others</td>
                    <td style="width:13%">Total</td>
                    <td style="width:7%"></td>
                </tr>
                 @if(isset($output_list) && count($output_list) > 0)
                @foreach($output_list as $output => $data)

                 @php
                    $output_time = ($data->date_and_time !='') ? date('H:i:s', strtotime($data->date_and_time)) : $search_time_web;
                    $output_date = ($data->date_and_time !='') ? date('M-d-Y', strtotime($data->date_and_time)) : $search_date_web;
                @endphp
                <tr>
                    <td>
                        <div class="col-md-7 padding_xs" style="text-align:center">
                            <input type="text" id='out_date_3' value="{{$output_date}}" name="out_date[]" class="form-control datepicker output" data-attr="date" placeholder="Date">
                        </div>
                        <div class="col-md-5 padding_xs" style="text-align:center">
                            <input type="text" value="{{$output_time}}" onclick='add_out_put(this);' id='out_time_3' name="out_time[]" class="form-control output timepicker" data-attr="date" placeholder="Time">
                        </div>
                    </td>
                    <td>
                        <input type="text" onclick='add_out_put(this);' onblur="calculate_output(3);" id="output_urine_3"  name="output_urine[]" value="{{$data->output_urine}}" class="form-control int_type"><input type="hidden" id="output_id" name="output_id[]" value="{{$data->id}}" >
                    </td>
                    <td>
                        <input type="text" onclick='add_out_put(this);' onblur="calculate_output(3);" id="output_others_3"  name="output_others[]" value="{{$data->output_others}}" class="form-control int_type">
                    </td>
                    <td>
                        <input type="text" readonly id="output_total_3"  name="output_total[]" value="{{$data->output_total}}" class="form-control int_type">
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm bg-red del-ouput-list-row" name="delete_ouput[]" id="delete_ouput_3">
                            <i class="fa fa-trash"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
                @else
                 <tr>
                    <td>
                        <div class="col-md-7 padding_xs" style="text-align:center">
                            <input type="text" id='out_date_3' value="{{$search_date_web}}" name="out_date[]" class="form-control datepicker output" data-attr="date" placeholder="Date">
                        </div>
                        <div class="col-md-5 padding_xs" style="text-align:center">
                            <input type="text" value="{{$search_time_web}}" onclick='add_out_put(this);' id='out_time_3' name="out_time[]" class="form-control output timepicker" data-attr="date" placeholder="Time">
                        </div>
                    </td>
                    <td>
                        <input type="text" onclick='add_out_put(this);' onblur="calculate_output(3);" id="output_urine_3"  name="output_urine[]"  class="form-control int_type">
                    </td>
                    <td>
                        <input type="text" onclick='add_out_put(this);' onblur="calculate_output(3);" id="output_others_3"  name="output_others[]"  class="form-control int_type">
                    </td>
                    <td>
                        <input type="text" readonly id="output_total_3"  name="output_total[]"  class="form-control int_type">
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm bg-red del-ouput-list-row" name="delete_ouput[]" id="delete_ouput_3">
                            <i class="fa fa-trash"></i>
                        </button>
                    </td>
                </tr>
                @endif
            </thead>
        </table>
    </div>
</form>



