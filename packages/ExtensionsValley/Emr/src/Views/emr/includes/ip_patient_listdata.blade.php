<div class="col-md-12 padding_sm">
    <div class="col-md-3 padding_sm">
        <strong>Total Count : <?=count($res)?></strong>
    </div>
    <div class="col-md-3 padding_sm">
        <i class="fa fa-circle blue"></i> Selected Patient
    </div>
    <div class="col-md-3 padding_sm">
        <i class="fa fa-circle green"></i> Ward Patient
    </div>
    <div class="col-md-3 padding_sm">
        <i class="fa fa-circle red"></i> ICU Patient
    </div>
</tr>
<div class="theadscroll" style="position: relative; height: 350px;">
    <table id="ip_patientlist_table" class="table table-bordered table_sm no-margin theadfix_wrapper">
        <thead>
            <tr class="table_header_bg">
                <th width='4%'>Si No</th>
                <th width='10%'>UHID</th>
                <th width='10%'>Name</th>
                <th width='10%'>IP No.</th>
                <th width='10%'>Age/Gender</th>
                <th width='12%'>Admitted On</th>
                <th width='14%'>Doctor</th>
                <th width='7%'>Bed</th>
                <th width='10%'>Room</th>
                <th width='13%'>Nursing Station</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(count($res)!=0){
                    $i=1;
                    foreach($res as $each){
                        $bg_color="green";
                        if ($each->patient_id==$patient_id) {
                            $bg_color="blue";
                        }else if($each->is_icu=='t'){
                            $bg_color='red';
                        }
                        ?>
                    <tr class="<?= $bg_color ?>" style="cursor:pointer" onclick="getIPPatient(<?= $each->patient_id ?>)">
                        <td><?= $i ?></td>
                        <td><?= $each->uhid ?></td>
                        <td><?= $each->patient_name ?></td>
                        <td><?= $each->ip_no ?></td>
                        <td><?= $each->age . '/' . $each->gender ?></td>
                        <td><?= date('M-d-Y h:i A', strtotime($each->actual_admission_date)) ?></td>
                        <td><?= $each->doctor_name ?></td>
                        <td><?= $each->bed_name ?></td>
                        <td><?= $each->room_type_name ?></td>
                        <td><?= $each->ward_name ?></td>
                    </tr>
            <?php
                        $i++;
                    }
                }
            ?>
        </tbody>
    </table>
</div>
