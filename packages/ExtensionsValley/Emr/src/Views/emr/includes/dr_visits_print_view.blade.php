<html>
    <head></head>
    <body>
        <style>
            .hospital-header-table table{
                border: none !important;
            }
        </style>
        <div class="col-md-12 padding_sm" style="width:100%;">
            <!-- Hospital Header -->
            <div class="hospital-header-table">
            @if(!empty($hospital_header))
                {!!$hospital_header!!}
            @endif
            </div>
            <!-- Hospital Header -->
            <table class="table table-striped   no-border no-margin">
                <tbody>
                @if($total_count > 0)
                    @php
                    $i=1;
                    $ca_count = [];
                    $inv_count = [];
                    $med_count = [];
                    $vitals_count = [];
                    @endphp
                    @foreach($data as $datas)
                    @php
                        $doctor_name = (!empty($datas->doctor_name)) ? $datas->doctor_name : "";
                        $encounter_date_time = (!empty($datas->encounter_date_time)) ? ExtensionsValley\Emr\CommonController::getDateFormat($datas->encounter_date_time,'d-M-Y h:i') : "";

                          if($case_sheet_dates != ''){
                        foreach($case_sheet_dates as $ca_date){
                            if($ca_date == $encounter_date_time){
                                $patient_formdata = (!empty($datas->patient_formdata)) ? (Array) $datas->patient_formdata : "";
                                $ca_count = (!empty($patient_formdata)) ? sizeof($patient_formdata) : 0;
                            }
                        }
                    }

                          if($medicine_dates != ''){
                        foreach($medicine_dates as $md_date){
                            if($md_date == $encounter_date_time){
                                $patient_medication = (!empty($datas->patient_medication)) ? (Array) $datas->patient_medication : "";
                                $med_count = (!empty($patient_medication)) ? sizeof($patient_medication) : 0;
                            }
                        }
                    }

                         if($investigation_dates != ''){
                        foreach($investigation_dates as $inv_date){
                            if($inv_date == $encounter_date_time){
                                $patient_invsestigation = (!empty($datas->patient_invsestigation)) ? (Array) $datas->patient_invsestigation : "";
                                $inv_count = (!empty($patient_invsestigation)) ? sizeof($patient_invsestigation) : 0;
                            }
                        }
                    }

                        if($vital_dates != ''){
                        foreach($vital_dates as $vt_date){
                            if($vt_date == $encounter_date_time){
                                $patient_vitals = (!empty($datas->patient_vitals)) ? (Array) $datas->patient_vitals : "";
                                $vitals_count = (!empty($patient_vitals)) ? sizeof($patient_vitals) : 0;
                            }
                        }
                        }

                        $class_highlight = "";
                        if($i==1):
                            $class_highlight = "tr_highlight";
                        else:
                            $class_highlight = "";
                        endif;
                        $scroll_to_tab = "scrollto-".$i;
                        $selected_tab = "selectedtab-".$i;


                    @endphp
                    @foreach($visit_dates as $v_dates)
                    @if($v_dates == $encounter_date_time)
                    <tr id="{{$scroll_to_tab}}" class="table_header_bg_grey {{$class_highlight}} {{$selected_tab}}" style="margin-bottom:5px;">
                        <td colspan="3" class="text-left">
                            <b>{{$encounter_date_time}} &nbsp; - &nbsp; {{$doctor_name}}</b>
                        </td>
                        <td class="text-right">&nbsp;</td>
                    </tr>


                    <tr>
                          @if($case_sheet_dates != '')
                        @foreach($case_sheet_dates as $ca_date)
                            @if($ca_date == $v_dates)
                        <td width="34%">
                            <table class="table no-margin table-striped   ">
                                <tbody>
                                    @if($ca_count > 0)
                                        @for ($n = 0; $n < $ca_count; $n++)
                                        @php
                                            $name = (!empty($patient_formdata[$n]->name)) ? $patient_formdata[$n]->name : "";
                                            $form_id = (!empty($patient_formdata[$n]->form_id)) ? $patient_formdata[$n]->form_id : "";
                                            $data_text = (!empty($patient_formdata[$n]->data_text)) ? json_decode($patient_formdata[$n]->data_text) : [];
                                        @endphp
                                            <tr style="background-color:ebfaef;"><td colspan="2">Case Sheet <b> : {{$name}}</b></td></tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table  " style="margin-bottom:4px;">
                                                        <tbody>
                                                            @php
                                                            $result = '';
                                                            if(!empty($form_id)){
                                                                $result = ExtensionsValley\Emr\FormDataList::getFormData($form_id, $data_text);
                                                            }
                                                            @endphp
                                                            @if(!empty($result))
                                                                @foreach($result as $key => $data)
                                                                <tr style="padding-left:0px;">
                                                                    @php
                                                                        $field_label = (!empty($data['field_label'])) ? $data['field_label'] : '';
                                                                        $res = (!empty($data['result'])) ? $data['result'] : '';
                                                                    @endphp
                                                                    @if(!empty($res))
                                                                        @php
                                                                            $res = nl2br($res);
                                                                        @endphp
                                                                    <td colspan="4">
                                                                        <b>{!!$field_label!!}</b>
                                                                        <br> {!!$res!!}
                                                                    </td>
                                                                    @endif
                                                                </tr>
                                                                @endforeach
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        @endfor
                                    @endif
                                </tbody>
                            </table>
                       </td>
                            @endif
                        @endforeach
                          @endif
                    </tr>

                        <tr>
                    @if($medicine_dates != '')
                        @foreach($medicine_dates as $md_date)
                            @if($md_date == $v_dates)
                              <td width="45%" valign="top">
                            <table class="table no-margin table-striped  " style="margin-bottom:4px;">
                                <tbody>

                                    <tr style="background-color:ebfaef;">
                                        <td colspan="2"><b>Medicine</b></td>
                                         <td><b>Remarks</b></td>
                                    </tr>
                                    @if($med_count > 0)
                                        @for ($m = 0; $m < $med_count; $m++)
                                            @php
                                            $head_id = (!empty($patient_medication[$m]->head_id)) ? $patient_medication[$m]->head_id : 0;
                                            $medicine_code = (!empty($patient_medication[$m]->medicine_code)) ? $patient_medication[$m]->medicine_code : "";
                                            $item_desc = (!empty($patient_medication[$m]->item_desc)) ? $patient_medication[$m]->item_desc : "";
                                            $duration = (!empty($patient_medication[$m]->duration)) ? $patient_medication[$m]->duration : 0;
                                            $frequency = (!empty($patient_medication[$m]->frequency)) ? $patient_medication[$m]->frequency : "";
                                            $quantity = (!empty($patient_medication[$m]->quantity)) ? $patient_medication[$m]->quantity : 0;
                                            $notes = (!empty($patient_medication[$m]->notes)) ? $patient_medication[$m]->notes : "";
                                            @endphp
                                            <tr>
                                                <td colspan="2">
                                                    {!!$item_desc!!} ({!!$duration!!} Days / {!!$frequency!!})
                                                </td>
                                                <td>
                                                    @if(!empty($notes))
                                                    <br>
                                                   {!!$notes!!} <br>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endfor
                                    @else
                                        <tr>
                                            <td colspan="2">No Data Found..</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            </td>
                            @endif
                        @endforeach
                          @endif
                    </tr>

                        <tr>
                    @if($investigation_dates != '')
                        @foreach($investigation_dates as $inv_date)
                            @if($inv_date == $v_dates)
                              <td width="20%" valign="top">
                            <table class="table no-margin table-striped  ">
                                <tbody>

                                    <tr style="background-color:ebfaef;">
                                        <td><b>Investigation</b></td>
                                        <td><b>Result</b></td>
                                        <td><b>N.Range</b></td>
                                        <td><b>Impression</b></td>
                                    </tr>
                                    @if($inv_count > 0)
                                        @for ($c = 0; $c < $inv_count; $c++)
                                            @php
                                            $head_id = (!empty($patient_invsestigation[$c]->head_id)) ? $patient_invsestigation[$c]->head_id : 0;
                                            $service_desc = (!empty($patient_invsestigation[$c]->service_desc)) ? $patient_invsestigation[$c]->service_desc : 0;
                                            $investigation_type = (!empty($patient_invsestigation[$c]->investigation_type)) ? $patient_invsestigation[$c]->investigation_type : 0;
                                            $quantity = (!empty($patient_invsestigation[$c]->quantity)) ? $patient_invsestigation[$c]->quantity : 0;
                                            @endphp
                                            <tr>
                                                <td>{{$service_desc}}</td>
                                                <td>
                                                    <div class="text-left text_limit">-</div>
                                                </td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                        @endfor
                                    @else
                                        <tr>
                                            <td colspan="4">No Data Found..</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                           </td>
                            @endif
                        @endforeach
                        @endif
                    </tr>

                        <tr>
                    @if($vital_dates != '')
                        @foreach($vital_dates as $vt_date)
                            @if($vt_date == $v_dates)
                        <td width="20%" valign="top">
                            <table class="table no-margin table-striped  ">
                                <tbody>
                                    <tr style="background-color:ebfaef;">
                                        <td><b>Vital</b></td>
                                        <td><b>Value</b></td>
                                    </tr>
                                    @if($vitals_count > 0)
                                        @for ($v = 0; $v < $vitals_count; $v++)
                                            @php
                                            $name = (!empty($patient_vitals[$v]->vital_name)) ? $patient_vitals[$v]->vital_name : 0;
                                            $value = (!empty($patient_vitals[$v]->vital_value)) ? $patient_vitals[$v]->vital_value : 0;
                                            $min_value = (!empty($patient_vitals[$v]->min_value)) ? $patient_vitals[$v]->min_value : 0;
                                            $max_value = (!empty($patient_vitals[$v]->max_value)) ? $patient_vitals[$v]->max_value : 0;
                                            @endphp
                                            <tr>
                                                <td>{{$name}}</td>
                                                <td>{{$value}}</td>
                                            </tr>
                                        @endfor
                                    @else
                                        <tr>
                                            <td colspan="2">No Data Found..</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                        </td>
                            @endif
                        @endforeach
                        @endif

                    </tr>
                   
                    <tr>
                        
                        @if($special_note_dates != '' && isset($special_notes[$v_dates]))
                            @if(count($special_notes[$v_dates])>0)
                                <td width="20%" valign="top">
                                    <table class="table no-margin table-striped  ">
                                        <tbody>
                                            <tr style="background-color:ebfaef;">
                                                <td><b>Special Notes</b></td>
                                            </tr>
                                            @if(count($special_notes[$v_dates]) > 0)
                                               @foreach($special_notes[$v_dates] as $row)
                                                     <tr><td>{{ $row }}</td></tr>
                                               @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="2">No Data Found..</td>
                                                </tr>
                                            @endif
                                
                                        </tbody>
                                    </table>
                                </td>
                            @endif
                            @endif

                    
                        </tr>
                        <tr>
                        
                            @if($diagnosis_dates != '' && isset($diagnosis[$v_dates]))
                                @if(count($diagnosis[$v_dates])>0)
                                    <td width="20%" valign="top">
                                        <table class="table no-margin table-striped  ">
                                            <tbody>
                                                <tr style="background-color:ebfaef;">
                                                    <td><b>Diagnosis</b></td>
                                                </tr>
                                                @if(count($diagnosis[$v_dates]) > 0)
                                                   @foreach($diagnosis[$v_dates] as $row)
                                                         <tr><td>{{ $row }}</td></tr>
                                                   @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="2">No Data Found..</td>
                                                    </tr>
                                                @endif
                                    
                                            </tbody>
                                        </table>
                                    </td>
                                @endif
                                @endif
    
                        
                            </tr>
                            <tr>
                        
                                @if($reason_for_visit_dates != '' && isset($reason_for_visit[$v_dates]))
                                    @if(count($reason_for_visit[$v_dates])>0)
                                        <td width="20%" valign="top">
                                            <table class="table no-margin table-striped  ">
                                                <tbody>
                                                    <tr style="background-color:ebfaef;">
                                                        <td><b>Reason For Visit</b></td>
                                                    </tr>
                                                    @if(count($reason_for_visit[$v_dates]) > 0)
                                                       @foreach($reason_for_visit[$v_dates] as $row)
                                                             <tr><td>{{ $row }}</td></tr>
                                                       @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="2">No Data Found..</td>
                                                        </tr>
                                                    @endif
                                        
                                                </tbody>
                                            </table>
                                        </td>
                                    @endif
                                    @endif
        
                            
                                </tr>
                    @php
                        $i++;
                    @endphp
                    @endif
                    @endforeach

                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </body>
</html>
