
<div class="box no-margin no-border">
    <div class="col-md-12 padding_sm">
        <div class="box no-border no-margin anim">
            <div class="box-body clearfix">
                <div class="col-md-2 padding_sm">
                    <div class="" data-toggle="buttons" style="margin-top:24px;">
                        <label class=" " style="margin-right:4px;">
                            <input type="radio" id="patient_name_search_radio_hp" value="1" name="search_type_hp" checked="checked"> Name
                        </label>
                        <label class=" " style="margin-right:4px;">
                            <input type="radio" id="uhid_search_radio_hp" value="2" name="search_type_hp" checked=""> UHID
                        </label>
                        <label class=" active">
                            <input type="radio" id="patient_phone_search_radio_hp" value="3" name="search_type_hp" checked="checked"> Phone
                        </label>
                    </div>
                </div>
                <div class="col-md-2 padding_sm">

                    <label for="">Search Patients</label>
                    <div class="clearfix"></div>
                    <div class="input-group">
                        <input type="text" id="patient_search_txt_hp" onkeyup="search_patient_hp();" class="form-control">
                        <div class="input-group-btn">
                            <button style="height: 23px; width: 30px;" class="btn light_purple_bg" onclick="search_patient_hp();"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 padding_sm">
                    <label for="">Search By Date</label>
                    <div class="clearfix"></div>
                    <div class="input-group">
                        <input type="text" id="op_patient_search_date_hp" onblur="getHealthPackagePatientList();" class="form-control datepicker" value="{{date('M-d-Y')}}" data-attr="date" placeholder="Date">
                        <div class="input-group-btn">
                            <button style="height: 23px; width: 30px;" class="btn light_purple_bg">
                                <i class="fa fa-search" onclick="getHealthPackagePatientList();"></i>
                            </button>
                        </div>
                    </div>
                </div>


                @php
                    $user_id = \Auth::user()->id;
                    $doctor = \DB::table('doctor_master')->where('user_id', $user_id)->select('speciality', 'id')->get();
                    $speciality_id = $doctor[0]->speciality;
                    $doctor_id = $doctor[0]->id;
                    $grouped_doctor_list = [];
                    if(env('DOCTOR_WISE_GROUP_LOGIN', 0) == 0){
                        $chk_dr_exisits = \DB::table('login_speciality_map')
                            ->where('doctor_id', $doctor_id)
                            ->first();
                        if (!empty($chk_dr_exisits)) {
                            $grouped_doctor_list = \DB::table('doctor_master')
                                ->join('login_speciality_map', 'login_speciality_map.doctor_id', '=', 'doctor_master.id')
                                ->where('login_speciality_map.speciality_id', $speciality_id)
                                ->where('login_speciality_map.status', 1)
                                ->distinct()
                                ->orderBy('doctor_master.id')
                                ->pluck('doctor_master.doctor_name', 'doctor_master.id');
                        }
                    }else {
                        $chk_dr_exisits = \DB::table('doctor_wise_group_login')
                            ->where('doctor_id', $doctor_id)
                            ->whereNull('deleted_at')
                            ->first();
                        if (!empty($chk_dr_exisits)) {
                            $group_parent_doctor_id = \DB::table('doctor_wise_group_login')->where('doctor_id', $doctor_id)->whereNull('deleted_at')->value('parent_doctor_id');
                            $grouped_doctor_list = \DB::table('doctor_master')
                                ->join('doctor_wise_group_login', 'doctor_wise_group_login.doctor_id', '=', 'doctor_master.id')
                                ->where('doctor_wise_group_login.parent_doctor_id', $group_parent_doctor_id)
                                ->orderBy('doctor_master.id')
                                ->pluck('doctor_master.doctor_name', 'doctor_master.id');
                        }
                    }

                @endphp
                @if (count($grouped_doctor_list) > 0)
                    <div class="col-md-3 padding_sm">
                        <label for="">Select Doctor</label>
                        <div class="clearfix"></div>
                        {!! Form::select('group_doctor_hp', $grouped_doctor_list, $doctor_id, ['class' => 'form-control', 'id' => 'group_doctor_hp', 'onchange' => 'getHealthPackagePatientList();', 'style' => ' color:#555555; padding:4px 12px;']) !!}
                    </div>
                @endif


            </div>
        </div>
    </div>

    <div class="col-md-12 padding_sm health_package_list_table_container">
        
    </div>

</div>
