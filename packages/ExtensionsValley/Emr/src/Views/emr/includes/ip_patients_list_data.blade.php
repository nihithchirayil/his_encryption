    <div class="col-md-6 padding_sm">
        <div class="theadscroll" style="position:relative; height:510px;">
            <table class="table no-margin no-border theadfix_wrapper">
                <thead>
                    <tr class="bg_ip_title">
                        <th class="text-center">
                            <i class='fa fa-bed'></i>
                            ICU Bed &nbsp; <span class="badge badge_purple_bg icu_patients_count">00</span>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td style="padding: 0px;">
                            <div class="col-md-12 padding-sm" style="padding-right: 20px !important;">
                                @php
                                    $icu_patients_count = 0;
                                @endphp
                            @foreach($res as $data)
                                @if($data->is_icu == 1)
                                @php
                                    $class= '';
                                    if($data->share_holder_status!=0){
                                        $class .= " share_holder_status";
                                    }
                                    if($data->is_highrisk!=0){
                                        $class .= " is_highrisk";
                                    }
                                    if($data->is_mlc!=0){
                                        $class .= " is_mlc";
                                    }
                                    if($data->referred_to_status!=0){
                                        $class .= " referred_to_status";
                                    }
                                    if($data->health_checkup!=0){
                                        $class .= " health_checkup";
                                    }

                                    $icu_patients_count = $icu_patients_count + 1;

                                @endphp
                                <div data-uhid="{{str_replace(' ','',$data->uhid)}}" data-phone="{{str_replace(' ','',$data->mobile_no)}}" data-patientname="{{str_replace(' ','',$data->patient_name)}}"
                                    @if($casuality_template_status == 1)
                                        onclick="goToSelectedPatientCasuality('{{ $data->patient_id }}');"
                                    @else
                                        onclick="goToSelectedPatient('{{$data->patient_id}}');"
                                    @endif
                                 class=" box-body notseenpatient ip_patient_det_widget card {{$class}}" style="margin:2px;cursor: pointer;border:0px !important;border-radius:3px;;margin-bottom:5px;  border:1px solid rgb(75, 200, 238) !important;margin-right:5px !important;box-shadow: 2px 2px 3px 1px rgba(59, 91, 199, 0.31);
                                    -webkit-box-shadow: 2px 2px 3px 1px rgba(64, 62, 226, 0.31);
                                    -moz-box-shadow: 2px 2px 3px 1px rgba(44, 41, 179, 0.31);" title="Room Type: {{$data->room_type_name}} | Ward:{{$data->ward_name}}">

                                    <div class="pt_container" style="color: #fffdfc;">
                                        <table class="table no-margin no-border table_sm">
                                            <tbody>

                                                <tr>
                                                    <td width="35px">
                                                        @if($data->patient_image !='')

                                                        <img class="patient_image_contianer" src="data:image/jpg;base64,{!!$data->patient_image!!}"/>

                                                        @else
                                                            <div class="pat_det_img" style="color: darkgray;">
                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td valign="top">

                                                            <table class="table no-margin table_sm" style="font-size:11px;">
                                                                <tbody >
                                                                    <tr>
                                                                        <td style="font-size: 11px !important;max-width:125px !important"><b>{{$data->patient_name}}
                                                                        </b></td>
                                                                        <td style="font-size: 12px !important"><b>{{$data->age}} / {{$data->gender}}</b></td>
                                                                        <td>

                                                                            <div class="col-md-12 padding_sm" style="margin-left: 10px;">
                                                                                <div class="btn-group pull-right" role="group" aria-label="Basic example">

                                                                                    @if($data->patient_ip_status != 5)
                                                                                        <span id="mark_discharge_{{$data->visit_id}}" class="badge_new bg-primary mark_discharge" title="Mark For Discharge" style="background-color:#337ab7 !important; color:white !important;margin-right: 5px">Discharge
                                                                                        </span>
                                                                                    @endif

                                                                                    <span data-title="Room Type: {{$data->room_type_name}} | Ward:{{$data->ward_name}}" class="badge_new waiting_time">{{$data->bed_name}}
                                                                                    </span>
                                                                                </div>
                                                                            </div>




                                                                            {{--  <div data-title="Room Type: {{$data->room_type_name}} | Ward:{{$data->ward_name}}" style="margin-right:-10px;width:65px;text-align:center" class="waiting_time btn bg-primary">{{$data->bed_name}}</div>  --}}
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width:105px !important;font-size: 11px !important"><b>{{$data->uhid}}</b></td>
                                                                        <td style="font-size: 12px !important"><b>{{$data->mobile_no}}</b></td>
                                                                        <td>
                                                                            <div class="flag_list">
                                                                                <ul class="list-unstyled no-padding no-margin" style="margin-right:-12px !important;margin-top: 2px !important;">

                                                                                    @if($data->share_holder_status!=0)
                                                                                        <li class="bg-orange" title="Share holder">S</li>
                                                                                    @endif

                                                                                    @if($data->is_highrisk!=0)
                                                                                        <li class="bg-red" title="High Risk">H</li>
                                                                                    @endif

                                                                                    @if($data->is_mlc!=0)
                                                                                        <li style="background-color:#6effc3" title="MLC">M</li>
                                                                                    @endif

                                                                                    @if($data->referred_to_status!=0)
                                                                                        <li style="background-color:#75dfff" title="MLC">R</li>
                                                                                    @endif

                                                                                    @if($data->health_checkup!=0)
                                                                                        <li style="background-color:#5b7b85;color:white" title="MLC">H</li>
                                                                                    @endif



                                                                                </ul>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="color:#399f62;"><b>Room Type: {{$data->room_type_name}} | Ward:{{$data->ward_name}}</b></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                                <input type="hidden" class="icuPatientsCount" value="{{$icu_patients_count}}"/>
                        </td>
                    </tr>

                </tbody>

            </table>

        </div>

    </div>

    <div class="col-md-6 padding_sm">
        <div class="theadscroll" style="position:relative; height:510px;">
            <table class="table no-margin no-border theadfix_wrapper">
                    <tr class="bg_ip_title">
                        <th class="text-center"> <i class='fa fa-bed'></i> Others
                            &nbsp; <span class="badge badge_purple_bg normal_patients_count">00</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="padding: 0px;">
                            <div class="col-md-12 padding-sm" style="padding-right: 20px !important;">
                                @php
                                    $normal_patients_count = 0;
                                @endphp
                            @foreach($res as $data)
                                @if($data->is_icu == 0)
                                @php
                                    $class= '';
                                    if($data->share_holder_status!=0){
                                        $class .= " share_holder_status";
                                    }
                                    if($data->is_highrisk!=0){
                                        $class .= " is_highrisk";
                                    }
                                    if($data->is_mlc!=0){
                                        $class .= " is_mlc";
                                    }
                                    if($data->referred_to_status!=0){
                                        $class .= " referred_to_status";
                                    }
                                    if($data->health_checkup!=0){
                                        $class .= " health_checkup";
                                    }

                                    $normal_patients_count = $normal_patients_count+1;

                                @endphp
                                <div data-uhid="{{str_replace(' ','',$data->uhid)}}" data-phone="{{str_replace(' ','',$data->mobile_no)}}" data-patientname="{{str_replace(' ','',$data->patient_name)}}"

                                @if($casuality_template_status == 1)
                                    onclick="goToSelectedPatientCasuality('{{ $data->patient_id }}');"
                                @else
                                    onclick="goToSelectedPatient('{{$data->patient_id}}');"
                                @endif


                                 class=" box-body notseenpatient ip_patient_det_widget card {{$class}}" style="margin:2px;cursor: pointer;border:0px !important;border-radius:3px;;margin-bottom:5px;  border:1px solid rgb(75, 200, 238) !important;margin-right:5px !important;box-shadow: 2px 2px 3px 1px rgba(44, 49, 65, 0.31);
                                    -webkit-box-shadow: 2px 2px 3px 1px rgba(25, 154, 214, 0.31);
                                    -moz-box-shadow: 2px 2px 3px 1px rgba(44, 41, 179, 0.31);" title="Room Type: {{$data->room_type_name}} | Ward:{{$data->ward_name}}">

                                    <div class="pt_container" style="color: #fffdfc;">
                                        <table class="table no-margin no-border table_sm">
                                            <tbody>

                                                <tr>
                                                    <td width="35px">
                                                        @if($data->patient_image !='')

                                                        <img class="patient_image_contianer" src="data:image/jpg;base64,{!!$data->patient_image!!}"/>

                                                        @else
                                                            <div class="pat_det_img" style="color: darkgray;">
                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td valign="top">

                                                            <table class="table no-margin table_sm" style="font-size:11px;">
                                                                <tbody >
                                                                    <tr>
                                                                        <td style="font-size: 11px !important;max-width:125px !important"><b>{{$data->patient_name}}
                                                                        </b></td>
                                                                        <td style="font-size: 12px !important"><b>{{$data->age}} / {{$data->gender}}</b></td>
                                                                        <td>
                                                                            <div class="col-md-12 padding_sm" style="margin-left: 10px;">
                                                                                <div class="btn-group pull-right" role="group" aria-label="Basic example">

                                                                                    @if($data->patient_ip_status != 5)
                                                                                        <span id="mark_discharge_{{$data->visit_id}}" class="badge_new bg-primary mark_discharge" title="Mark For Discharge" style="background-color:#337ab7 !important; color:white !important;margin-right: 5px">Discharge
                                                                                        </span>
                                                                                    @endif

                                                                                    <span data-title="Room Type: {{$data->room_type_name}} | Ward:{{$data->ward_name}}" class="badge_new waiting_time">{{$data->bed_name}}
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width:105px !important;font-size: 11px !important"><b>{{$data->uhid}}</b></td>
                                                                        <td style="font-size: 12px !important"><b>{{$data->mobile_no}}</b></td>
                                                                        <td>
                                                                            <div class="flag_list">
                                                                                <ul class="list-unstyled no-padding no-margin" style="margin-right:-12px !important;margin-top: 2px !important;">

                                                                                    @if($data->share_holder_status!=0)
                                                                                        <li class="bg-orange" title="Share holder">S</li>
                                                                                    @endif

                                                                                    @if($data->is_highrisk!=0)
                                                                                        <li class="bg-red" title="High Risk">H</li>
                                                                                    @endif

                                                                                    @if($data->is_mlc!=0)
                                                                                        <li style="background-color:#6effc3" title="MLC">M</li>
                                                                                    @endif

                                                                                    @if($data->referred_to_status!=0)
                                                                                        <li style="background-color:#75dfff" title="MLC">R</li>
                                                                                    @endif

                                                                                    @if($data->health_checkup!=0)
                                                                                        <li style="background-color:#5b7b85;color:white" title="MLC">H</li>
                                                                                    @endif



                                                                                </ul>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="color:#399f62;"><b>Room Type: {{$data->room_type_name}} | Ward:{{$data->ward_name}}</b></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </td>
                            <input type="hidden" class="normalPatientsCount" value="{{$normal_patients_count}}"/>
                    </tr>
                </tbody>

            </table>
        </div>

    </div>
