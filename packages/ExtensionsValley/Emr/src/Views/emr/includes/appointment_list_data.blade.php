<div class="theadscroll always-visible " style="position: relative; height: 410px;">
    <table class="table table_sm table-bordered theadfix_wrapper">
        <thead>
            <tr class="table_header_bg">
                <th>Sl No</th>
                <th>Appointment Time</th>
                <th>Name</th>
                <th>Uhid</th>
                <th>Age/Gender</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Status</th>

            </tr>
        </thead>
        <tbody>
            @if(sizeof($res)>0)
            @php
            $i =1;
            $status = '';
            $cls = '';
            @endphp
            @foreach($res as $data)

            @php
            if($data->status ==1){
            $status = 'Booked';
            $cls = 'bg-blue';
            }else if($data->status ==2){
            $status = 'Checked In';
            $cls = 'bg-green';
            }else if($data->status ==3){
            $status = 'Cancelled';
            $cls = 'bg-red';
            }
            @endphp

            <tr class='{{$cls}}'>
                <td>{{$i}}</td>
                <td>{{date('h:i:A',strtotime($data->time_slot))}}</td>
                <td>{{$data->patient_name}}</td>
                <td>{{$data->uhid}}</td>
                <td>{{$data->age}} / {{$data->name}}</td>

                <td>{{$data->address}}</td>
                <td>{{$data->mobile_no}}</td>
                <td>

                    {{$status}}
                </td>



            </tr>
            @php
            $i++;
            @endphp
            @endforeach
            @else
            <tr style="text-align: center;">
                <td colspan="8">No Data Found.!</td>
            </tr>
            @endif
        </tbody>
    </table>
    <input type="hidden" id="booking_listdatacount" value="<?= count($res) ?>" >
</div>