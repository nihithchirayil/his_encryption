<div class="col-md-12 box-body">
    <div class="clearfix">
        <button style="width:100px;" type='button' onclick="add_medicine_return();" class='btn bg-green pull-right'><i class="fa fa-plus"> Add</i></button>
    </div>
    <div class="col-md-12" id="medicine_return_intent_results">

    </div>
</div>

<!-----------lab subtest results modal------------------------------>
<div id="modal_add_medicine_return" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:83%">

      <div class="modal-content" style="min-height: 450px;">
        <div class="modal-header" style="background:#26b99a; color:#ffffff;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
          <h4 class="modal-title">Add Medication Return</h4>
        </div>
        <div class="modal-body" id="add_medication_return_data">

        </div>
      </div>

    </div>
</div>
<!-----------lab subtest results modal ends------------------------->

<!-----------lab subtest results edit modal ----------------------------->
<div id="modal_add_medicine_edit_return" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:33%">

      <div class="modal-content" style="min-height: 150px;">
        <div class="modal-header" style="background:#26b99a; color:#ffffff;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
          <h4 class="modal-title">Edit Medicine Return</h4>
        </div>
        <div class="modal-body">
            <div class="col-md-12">
                <div class="col-md-10">
                    <input type="hidden" id="medicine_return_edit_id" value=""/>
                    <input type="hidden" id="medicine_return_current_qty" value=""/>
                    <input type="hidden" id="medicine_return_edit_available" value=""/>
                    <input type="number" id="med_edit_return_qty" class="form-control"/>

                </div>
                <div class="col-md-2">
                    <button type="button" onclick="update_medicine_return();" class="btn btn-success pull-right">
                        <i class="fa fa-save"></i> Update
                    </button>
                </div>
            </div>
        </div>
      </div>

    </div>
</div>
<!-----------lab subtest results edit modal ends------------------------->
