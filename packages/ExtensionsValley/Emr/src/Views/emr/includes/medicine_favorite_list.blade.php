            <style>

    .table_header_bg.header_normal_padding>th{
        padding: 8px !important;
    }
</style>

<div class="box no-border no-margin prescription_wrapper anim" id="prescription_wrapper">
    <div class="box-body clearfix">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#drug" style="border-radius: 0px;"><b>Drug</b></a></li>
            <li><a data-toggle="tab" href="#iv-fluid" style="border-radius: 0px;"><b>IV Fluids</b></a></li>
        </ul>
        <div class="tab-content">
            <div id="drug" class="tab-pane fade in active">
                <div class="col-md-12 no-padding expand_prescription_btn" style="cursor: pointer;">
                    <div class="col-md-11" style="padding:0 15px 0 0;">
                        <h4 class="card_title">
                            Prescription <span class="draft_mode_indication_p"></span>
                            <button class="btn fav_dropdown_btn light_purple_bg"><i class="fa fa-star"></i> Bookmarks</button>
                        </h4>
                    </div>
                    <div class="col-md-1 text-right">
                    </div>
                </div>
                <div class="col-md-12" style="padding: 2px;">


                    @include('Emr::emr.prescription_favourite.prescription_favorite_list')

                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <div class="card">
                        <div class="card_body" style="background: #FFF;" id="prescription_wrapper_card_body">
                            <table class="table no-margin table_sm table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Search Medicine by :</td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="m_s_type_brand" value="brand" name="m_search_type" checked="">
                                                <label for="m_s_type_brand"> Brand </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="m_s_type_generic" value="generic" name="m_search_type">
                                                <label for="m_s_type_generic"> Generic </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="m_s_type_both" value="both" name="m_search_type">
                                                <label for="m_s_type_both"> Both </label>
                                            </div>
                                        </td>
                                        <td width="25%">&nbsp;</td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="p_type_regular" value="1" name="p_search_type" checked="">
                                                <label for="p_type_regular"> Regular </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="p_type_new_adm" value="2" name="p_search_type">
                                                <label for="p_type_new_adm"> New Admission </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="p_type_emergency" value="3" name="p_search_type">
                                                <label for="p_type_emergency"> Emergency </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="p_type_discharge" value="4" name="p_search_type">
                                                <label for="p_type_discharge"> Discharge </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="p_type_outside" value="5" name="p_search_type">
                                                <label for="p_type_outside"> Outside </label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <table class="table no-margin no-border">
                                <thead>
                                    <tr class="prescription_entry_head">
                                        <th>Medicine</th>
                                        <th>Dose</th>
                                        <th>Frequency</th>
                                        <th>Days</th>
                                        <th>Quantity</th>
                                        <th>Route</th>
                                        <th colspan="2">Instructions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="30%">
                                            <input type="text" name="search_medicine" style="cursor: pointer;" autocomplete="off" class="form-control" placeholder="Type at least 3 characters">

                                            <input type="hidden" name="search_item_code_hidden" value="">
                                            <input type="hidden" name="search_item_name_hidden" value="">
                                            <input type="hidden" name="search_item_price_hidden" value="">

                                            <!-- Medicine List -->
                                            <div class="medicine-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_med_search">X</a>
                                                <div class=" presc_theadscroll" style="position: relative;">
                                                  <table id="MedicationTable"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                      <thead>
                                                        <tr class="light_purple_bg">
                                                          <th colspan="2">Medicine</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody id="ListMedicineSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          <!-- Medicine List -->

                                        </td>
                                        <td width="10%">
                                            <input type="text" name="search_dose" style="cursor: pointer;" autocomplete="off" id="search_dose" class="form-control">
                                        </td>
                                        <td width="10%">
                                            <input type="text" name="search_frequency" style="cursor: pointer;" autocomplete="off" id="search_frequency" class="form-control">

                                            <input type="hidden" name="search_freq_value_hidden" value="">
                                            <input type="hidden" name="search_freq_name_hidden" value="">

                                            <!-- Frequency List -->
                                            <div class="frequency-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_freq_search">X</a>
                                                <div class="freq_theadscroll" style="position: relative;">
                                                  <table id="FrequencyTable"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                      {{-- <thead>
                                                        <tr class="light_purple_bg">
                                                          <th>Frequency</th>
                                                        </tr>
                                                      </thead> --}}
                                                      <tbody id="ListFrequencySearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          <!-- Frequency List -->

                                        </td>
                                        <td width="10%">
                                            <input type="text" name="search_duration" style="cursor: pointer;" autocomplete="off" id="search_duration" class="form-control">
                                        </td>
                                        <td width="7%">
                                            <input type="text" name="search_quantity" style="cursor: pointer;" autocomplete="off" id="search_quantity" class="form-control">
                                        </td>
                                        <td width="15%">
                                            <input type="text" name="search_route" style="cursor: pointer;" autocomplete="off" id="search_route" class="form-control">

                                            <!-- Route List -->
                                            <div class="route-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_route_search">X</a>
                                                <div class="route_theadscroll" style="position: relative;">
                                                  <table id="RouteTable"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                      {{-- <thead>
                                                        <tr class="light_purple_bg">
                                                          <th>Route</th>
                                                        </tr>
                                                      </thead> --}}
                                                      <tbody id="ListRouteSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          <!-- Route List -->

                                        </td>
                                        <td width="15%">
                                            <input type="text" name="search_instructions" style="cursor: pointer;" autocomplete="off" id="search_instructions" class="form-control">

                                            <!-- Instruction List -->
                                            <div class="instruction-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_instruction_search">X</a>
                                                <div class="instruction_theadscroll" style="position: relative;">
                                                  <table id="InstructionTable"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                      <tbody id="ListInstructionSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- Instruction List -->

                                        </td>
                                        <td width="3%" align="center"><button class="btn light_purple_bg" onclick="addNewMedicine();" ><i class="fa fa-plus"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td colspan="14" class="partition_line"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <form id="presc-data-form" action="">
                            <div class="theadscroll" style="position: relative; height: 244px;">
                                <table border="1" style="width: 100%;height: 15%;border:1px solid;border-collapse: collapse !important" class="table  table_sm  table-bordered" id="medicine-listing-table">
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <div id="iv-fluid" class="tab-pane">
                <div class="col-md-12" style="padding: 2px;">
                    <div class="card">
                        <div class="card_body" style="background: #FFF;" id="prescription_iv_fluid_wrapper_card_body">
                            <table class="table no-margin table_sm table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Search Medicine by :</td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="iv_m_s_type_brand" value="brand" name="iv_m_search_type" checked="">
                                                <label for="iv_m_s_type_brand"> Brand </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="iv_m_s_type_generic" value="generic" name="iv_m_search_type">
                                                <label for="iv_m_s_type_generic"> Generic </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="iv_m_s_type_both" value="both" name="iv_m_search_type">
                                                <label for="iv_m_s_type_both"> Both </label>
                                            </div>
                                        </td>
                                        <td width="25%">&nbsp;</td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="iv_p_type_regular" value="1" name="iv_p_search_type" checked="">
                                                <label for="iv_p_type_regular"> Regular </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="iv_p_type_new_adm" value="2" name="iv_p_search_type">
                                                <label for="iv_p_type_new_adm"> New Admission </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="iv_p_type_emergency" value="3" name="iv_p_search_type">
                                                <label for="iv_p_type_emergency"> Emergency </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="iv_p_type_discharge" value="4" name="iv_p_search_type">
                                                <label for="iv_p_type_discharge"> Discharge </label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <table class="table no-margin no-border">
                                <thead>
                                    <tr class="table_header_bg header_normal_padding">
                                        <th>Medicine</th>
                                        <th>Volume</th>
                                        <th>Rate</th>
                                        <th colspan="2">Instructions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="40%">
                                            <input type="text" name="iv_search_medicine" autocomplete="off" class="form-control" placeholder="Type at least 3 characters">

                                            <input type="hidden" name="iv_search_item_code_hidden" value="">
                                            <input type="hidden" name="iv_search_item_name_hidden" value="">
                                            <input type="hidden" name="iv_search_item_price_hidden" value="">

                                            <!-- Medicine List -->
                                            <div class="iv-medicine-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_med_search">X</a>
                                                <div class=" presc_theadscroll" style="position: relative;">
                                                  <table id="IvMedicationTable"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                      <thead>
                                                        <tr class="light_purple_bg">
                                                          <th>Medicine</th>
                                                          <th>Generic Name</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody id="IvListMedicineSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          <!-- Medicine List -->

                                        </td>
                                        <td width="20%">
                                            <input type="text" name="iv_search_volume" autocomplete="off" id="iv_search_volume" class="form-control">
                                        </td>
                                        <td width="20%">
                                            <input type="text" name="iv_search_rate" autocomplete="off" id="iv_search_rate" class="form-control">

                                            <!-- iv rate List -->
                                            <div class="iv-rate-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_iv_rate_search">X</a>
                                                <div class="iv_rate_theadscroll" style="position: relative;">
                                                  <table id="IvRateTable"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                      <tbody id="ListIvRateSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- iv rate List -->
                                        </td>
                                        <td width="20%">
                                            <input type="text" name="iv_search_instructions" autocomplete="off" id="iv_search_instructions" class="form-control">

                                            <!-- Instruction List -->
                                            <div class="iv-instruction-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_iv_instruction_search">X</a>
                                                <div class="iv_instruction_theadscroll" style="position: relative;">
                                                  <table id="IvInstructionTable"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                      <tbody id="ListIvInstructionSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- Instruction List -->

                                        </td>
                                        <td width="3%" align="center"><button class="btn light_purple_bg" onclick="addNewIvFluid();" ><i class="fa fa-plus"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td colspan="14" class="partition_line"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <form id="iv-presc-data-form" action="">
                            <div class="theadscroll" style="position: relative; height: 244px;">
                                <table border="1" style="width: 100%;border:1px solid;border-collapse: collapse !important" class="table  table_sm  table-bordered" id="iv-medicine-listing-table">
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
