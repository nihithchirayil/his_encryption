<style>
.bgimg {
    background-image: url('{{asset("packages/extensionsvalley/dashboard/images/temp_bg.jpg")}}');
    height:430px;
    background-position: center;
    background-size: cover;
    position: relative;
    color: white;
    font-family: "Courier New", Courier, monospace;
    font-size: 25px;
    opacity: 75%;
  }

  .topleft {
    position: absolute;
    top: 0;
    left: 16px;
  }


</style>
<div class="bgimg">
    <div class="topleft" style="padding:10px;">
        <img class="" src="{{asset("packages/extensionsvalley/dashboard/images/codfoxxlogo_white.png")}}">
    </div>
    <div class="middle" style="width:75%;padding:9pc;">
      <h1>This Webpage Is Under Construction</h1>
    </div>
    <div class="bottomleft">
    </div>
</div>

