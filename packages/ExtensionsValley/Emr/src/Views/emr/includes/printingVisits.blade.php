<div class="row">
        <div class="theadscroll" style="position: relative; height: 300px;">
            <div class="col-md-12 padding_sm">
            <ul class="list-unstyled">
                <li style="margin:2px;padding:0px;">
                        <input type="checkbox" class="all_visit_dates" name="all_visit_dates">
                            <b> Select All Visits</b>
                </li><br />
                @if($total_count > 0)
                @php
                    $i=1;
                @endphp
                    @foreach($data as $datas)
                        @php
                            $doctor_name = (!empty($datas->doctor_name)) ? $datas->doctor_name : "";
                            $encounter_date_time = (!empty($datas->encounter_date_time)) ? ExtensionsValley\Emr\CommonController::getDateFormat($datas->encounter_date_time,'d-M-Y h:i') : "";
                        @endphp
                    <li style="margin:2px;padding:0px;">
                        <input type="checkbox" onclick="visit_chk_status(this);" value="{{$encounter_date_time}}" class="visit_dates" name="visit_dates[]">
                        <input type="hidden" value="{{$encounter_date_time}}" name="visit_dates_hidden[]">
                         <a class="btn bg-green">   <b> {{date('M-d-Y',strtotime($encounter_date_time))}} </b><br/>
                            <span style="font-size:10px;">{{$doctor_name}}</span></a>
                    
                    <div class="visit_chk_li hide">
                        <ul class="list-unstyled">
                            <li style="margin:2px;padding:0px;">
                             <input type="checkbox" class="visit_dates_chk" value="{{$encounter_date_time}}" name="case_sheet[]">CASE SHEET
                            </li>
                            <li style="margin:2px;padding:0px;">
                             <input type="checkbox" class="visit_dates_chk" value="{{$encounter_date_time}}" name="medicine[]">MEDICINE
                            </li>
                            <li style="margin:2px;padding:0px;">
                             <input type="checkbox" class="visit_dates_chk" value="{{$encounter_date_time}}" name="investigation[]">INVESTIGATION
                            </li>
                            <li style="margin:2px;padding:0px;">
                             <input type="checkbox" class="visit_dates_chk" value="{{$encounter_date_time}}" name="vital[]">VITAL
                            </li>  
                            <li style="margin:2px;padding:0px;">
                             <input type="checkbox" class="visit_dates_chk" value="{{$encounter_date_time}}" name="special_note[]">SPECIAL NOTE
                            </li>  
                            <li style="margin:2px;padding:0px;">
                             <input type="checkbox" class="visit_dates_chk" value="{{$encounter_date_time}}" name="diagnosis[]">Diagnosis
                            </li>  
                            <li style="margin:2px;padding:0px;">
                             <input type="checkbox" class="visit_dates_chk" value="{{$encounter_date_time}}" name="reason_for_visit[]">Reason For Visit
                            </li>  
                        </ul>
                    </div>
                    </li>
                    <!-- <br /> -->
                    @php
                        $i++;
                    @endphp
                    @endforeach
                @endif
            </ul>
        </div>
        </div>
    </div>