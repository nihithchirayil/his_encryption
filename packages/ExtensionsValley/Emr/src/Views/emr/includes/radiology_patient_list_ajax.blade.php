

<div class="box no-border no-margin anim" style="min-height:360px;">
    <div class="box-body clearfix">
        <table class="table table-bordered no-margin table_sm no-border theadfix_wrapper">
            <thead>
                <tr class="table_header_bg">
                    <th>SL.No.</th>
                    <th>UHID</th>
                    <th>Patient Name</th>
                    <th>Age/Gender</th>
                    <th>Visit Type</th>
                    <th>Phone</th>
                </tr>
            </thead>
            <tbody class="health_package_list_table_body">
            @php $sl = 1; @endphp
            @isset($op_patients)
            @if(count($op_patients) > 0)
            @foreach($op_patients as $rd)
            <tr class="patient_det_widget_radio" data-patientname="{{$rd->patient_name}}" data-uhid="{{$rd->uhid}}" data-phone="{{$rd->mobile_no}}" onclick="goToSelectedPatient('{{$rd->patient_id}}');" style="cursor:pointer;">
                <td>{{$sl}}</td>
                <td>{{$rd->uhid}}</td>
                <td>{{$rd->patient_name}}</td>
                <td>{{$rd->age}}/{{$rd->gender}}</td>
                <td>{{$rd->visit_type}}</td>
                <td>{{$rd->mobile_no}}</td>
            </tr>
            @php $sl++; @endphp
            @endforeach
            @else
            <tr>
                <td colspan="8">No records found..!</td>
            </tr>
            @endif
            @endisset
            </tbody>
        </table>

    </div>
</div>