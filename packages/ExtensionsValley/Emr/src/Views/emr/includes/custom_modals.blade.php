@php
$search_date_web = date('M-d-Y');
@endphp
<!-- Prescription Modal -->
<div id="med_det_list_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 85%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Prescription Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="theadscroll always-visible" style="position: relative;height: 300px;">
                            <table class="table no-margin table_sm table-borderd table-striped">
                                <thead id="med_det_list_modal_head">
                                    <tr class="table_header_bg" style="background: #96babd;">
                                        <th>#</th>
                                        <th width="22%">Medicine</th>
                                        <th width="18%">Generic Name</th>
                                        <th>Dose</th>
                                        <th>Frequency</th>
                                        <th>Days</th>
                                        <th>Quantity</th>
                                        <th>Route</th>
                                        <th>Instructions</th>
                                    </tr>
                                </thead>
                                <tbody id="med_det_list_modal_content">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm applyPatientMed">Apply</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- Prescription Modal -->


<!-- Clinical Assessment Modal -->
<div id="form_det_list_modal" class="modal fade" role="dialog" style="z-index:10000;">
    <div class="modal-dialog" style="width: 90%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" id="form_det_list_modal_content">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- Clinical Assessment Modal -->

<!-- Special Note Modal -->
<div id="speial_note_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Special Note</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label><b>Note</b></label>
                        <p id="special_notes_result"></p>

                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- Special Note Modal -->


<!-- combined view modal starts here -->

<div id="combined_view_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 98%; width: 100%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Patient History</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4">

                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-success active" id="btn_my-visit" style="margin-right:3px;">
                                    <input type="radio" id="my-visit" value="my-visit" name="visittype"> My Visits
                                </label>
                                <label class="btn btn-success" id="btn_all-visit">
                                    <input type="radio" id="all-visit" value="all-visit" name="visittype"> All Visits
                                </label>
                            </div>


                        </div>
                        <div class="col-md-4 pull-right text-right">

                            <div class="btn-group" data-toggle="buttons">
                                <button type="button" id="print_visit" class="btn btn-info" style="margin-right:3px;"
                                    onclick="btnPrintVisit();"> Print Visit
                                </button>
                            </div>


                        </div>
                        <!-- <div class="col-md-4 pull-right text-right">
                     <div class="radio radio-primary radio-inline">
                        <input type="radio" id="sort-desc" value="desc" checked="checked" name="sortresult">
                        <label for="sort-desc">Sort Latest</label>
                    </div>
                    <div class="radio radio-primary radio-inline">
                        <input type="radio" id="sort-asc" value="asc" name="sortresult">
                        <label for="sort-asc">Sort Past</label>
                    </div>
                </div> -->
                    </div>
                    <div id="combined-view-loader" class="hide">
                        <div class="row" style="text-align: center;">
                            <i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12 combined_view_wrapper">

                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>

                </div>
            </div>
            <!-- <div class="modal-footer">
    <button style="margin: 0 5px 0 0;" data-dismiss="modal" class="btn light_purple_bg"><i class="fa fa-times"></i> Cancel</button>
    <button class="btn light_purple_bg"><i class="fa fa-save"></i> Save</button>
</div> -->
        </div>

    </div>
</div>

<!-- combined view modal ends here -->

<!----Printing Visits modal---------->

<div class="modal bs-example-modal-md mkModalFloat" id="printingVisits" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Printing Visits</h4>
            </div>
            <form id="printVisitForm" name="printVisitForm" method="POST">
                <div id="print_visit-view-loader" class="hide">
                    <div class="row" style="text-align: center;">
                        <i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="ht10"></div>
                <div class="modal-body" id="fetch_visit_data_div_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn save_btn_bg" id="print_save_btn" onclick="printDrVisits()"
                        data-dismiss="modal"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!----Printing Visits modal Ends---------->

<!-- vital graph modal starts here -->

<div id="vital_graph_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 1300px; width: 100%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg" style="height:37px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Vital and Progress</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box no-border no-margin">
                            <div class="box-body clearfix">
                                <div class="col-md-12">
                                    <div class="form-group control-required col-md-3 padding_sm">
                                        <label for="lstVitalItems">Filter By Vitals </label>
                                        <select class="form-control " name="lstVitalItems" id="lstVitalItems">
                                            <option value="">Fiter By Vitals</option>
                                            <option value="5">BP Systolic/ BP Diastolic</option>
                                            <option value="6">BP Diastolic / BP Systolic</option>
                                            <option value="7">Pulse / Min</option>
                                            <option value="8">Respiration / Min</option>
                                            <option value="9">Temperature F </option>
                                            <option value="12">Oxygen Saturation %</option>
                                            <option value="2">Weight in Kg</option>
                                            <option value="4">Height in CM</option>
                                            <option value="17">BMI Kg/M^2</option>
                                            <option value="20">Spirometry</option>
                                        </select>
                                    </div>
                                    <div class="form-group  control-required col-md-3 padding_sm">
                                        <label for="vital_graph_from_date">From Date</label>
                                        <input id="vital_graph_from_date" value="" class="form-control datepicker"
                                            name="vital_graph_from_date" type="text">
                                    </div>
                                    <div class="form-group  control-required col-md-3 padding_sm">
                                        <label for="vital_graph_to_date">To Date</label>
                                        <input id="vital_graph_to_date" value="" class="form-control datepicker"
                                            name="vital_graph_to_date" type="text">
                                    </div>
                                    <div class="form-group  control-required col-md-2 padding_sm">
                                        <label>&nbsp;</label><br>
                                        <button class="btn btn-block save_btn_bg" onclick="show_vital_history();"><i
                                                class="fa fa-check"></i> Submit</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12">
                        <div class="col-md-3 padding_sm">
                            <div class="theadscroll" style="position: relative; height: 400px;">
                                <table
                                    class="table table-bordered no-margin table_sm table-striped theadfix_wrapper vital_his_table">
                                    <thead>
                                        <tr class="table_header_bg">
                                            <th>Vitals</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>
                                    <tbody id="vital_his_table">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-9 padding_sm">
                            <div class="box no-margin no-border">
                                <div class="box-body">
                                    <div id="vital_graph"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="modal-footer">
    <button style="margin: 0 5px 0 0;" data-dismiss="modal" class="btn light_purple_bg"><i class="fa fa-times"></i> Cancel</button>
    <button class="btn light_purple_bg"><i class="fa fa-save"></i> Save</button>
</div> -->
        </div>

    </div>
</div>

<!-- vital graph modal ends here -->


<!-- Admit Patient Modal content-->
<!-- Modal -->
<div id="admitModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 98%; width: 70%;">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['name' => 'formAdmission', 'id' => 'formAdmission', 'method' => 'post']) !!}
            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Admission Request</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" name="admit_btn" id="admit_btn" class="pull-right btn save_btn_bg"
                    onclick="admitHere()">Request Admit</button>
            </div>
            {!! Form::token() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>


<!----Referring Doctor---------->

<div class="modal bs-example-modal-md mkModalFloat" id="referDoctor" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Refer Doctor</h4>
            </div>
            <form id="referDoctorForm" name="referDoctorForm" method="POST">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn save_btn_bg save_class" id="refer_save_btn"
                        onclick="saveReferDoctor()" data-dismiss="modal">Refer</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!----Referring Doctor Ends---------->


<!----manage Docs---------->

<div class="modal bs-example-modal-md mkModalFloat" id="manageDocs" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">MANAGE DOCUMENT</h4>
            </div>

            <div class="modal-body">

                <div class="row">
                    @php
                        $document_types = \DB::table('document_types')
                            ->WhereNull('deleted_at')
                            ->Where('status', 1)
                            ->OrderBy('name', 'DESC')
                            ->pluck('name', 'id');
                    @endphp

                    {!! Form::open(['id' => 'manageDocsForm', 'name' => 'manageDocsForm', 'method' => 'post', 'file' => 'true', 'enctype' => 'multipart/form-data']) !!}
                    @php $i = 1; @endphp
                    <div class="col-sm-12 no-padding">

                        <div class="col-md-3 padding_sm"></div>

                        <div class="col-md-2 padding_sm"></div>

                        <div class="col-md-4 padding_sm"></div>

                        <div class="col-md-2 padding_xs visit-action-button">
                            <!-- <button class="btn btn-success btn-block" id="doc_file_upload" type="submit">
                            <i class="fa fa-upload" ></i> Upload
                        </button> -->
                        </div>

                        <!-- <div class="col-md-1 padding_xs visit-action-button">
                        <button onclick="add_new_row_files()" class="btn btn-primary btn-block">
                        <i class="fa fa-plus"></i> Add
                        </button>
                    </div> -->

                    </div>

                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <div class="doc-type-hidden" style="display: none;">
                        {!! Form::select('document_type_hidden', $document_types, null, ['class' => 'form-control', 'placeholder' => 'Select Type']) !!}
                    </div>

                    <div class="remove_00 col-sm-12 no-padding visit-action-button">
                        <div class="col-md-3 padding_sm">
                            {!! Form::select('document_type[]', $document_types, old('document_type'), ['class' => 'form-control document_type_upload', 'placeholder' => 'Select Type', 'required' => 'required']) !!}
                        </div>

                        <!-- <div class="col-md-2 padding_sm">
                        <input type="text" name="title[]" class="form-control" id="title" placeholder="Title">
                    </div>  -->

                        <div class="col-md-3 padding_sm">
                            <input type="text" name="description[]" class="form-control description_upload"
                                id="description" placeholder="Description">
                        </div>

                        <div class="col-md-3 padding_sm">
                            @php $f = $i - 1; @endphp
                            <input name="file_upload[]" type="file" id="f_upload"
                                class="inputfile inputfile-1 hidden document_upload" data-show-upload="false"
                                data-multiple-caption="{count} files selected" />
                            <label class="no-margin btn btn-block custom-uploadfile_label" for="f_upload">
                                <i class="fa fa-folder"></i>
                                <span id="uploadspan_documentsdocumenttab">
                                    Choose a file ...
                                </span>
                            </label>
                            <a class="uploadedImg" style="display:none;" href="" target="_blank"> </a>
                            <span style="color: #d14;">@if ($errors->first('f_upload.' . $f) != '') {{ 'Upload only doc,docx,pdf,jpeg,jpg,png,gif,bmp and File size < 10MB' }} @endif</span>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <button class="btn btn-success btn-block save_class" id="doc_file_upload" type="submit">
                                <i class="fa fa-upload" id="upload_file_doc"></i> Upload
                            </button>
                        </div>

                        <div class="col-md-1 padding_sm">
                            <button type="button" class="btn btn-danger btn-block" onclick="clear_upload_file();"><i
                                    class="fa fa-trash-o"></i> Clear</button>
                        </div>
                        <!-- <div class="col-md-2 padding_sm">
                        <button type="button" class="btn btn-danger btn-block" onclick="remove_upload_file('default');"><i class="fa fa-trash-o"></i> Clear</button>
                    </div> -->
                    </div>

                    <div class="clearfix"></div>
                    <div class="ht5"></div>

                    <div class="col-md-12 no-padding">
                        <div class="theadscroll sm_margin_bottom" id="roww" style="max-height: 160px; height: auto;">
                        </div>
                    </div>

                    @php $i++; @endphp

                    {!! Form::token() !!}
                    {!! Form::close() !!}

                </div>

                <hr>

                <div class="row" id="document-data-list">
                    @include('Emr::emr.document.ajax_document_pagination')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<!----manage Docs Ends---------->

<!-------My calendar modals begin------------------------>
<div id="calendarModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 85%;">
        <!-- Modal content-->
        <div class="modal-content" style="min-height:620px;">

            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Calendar</h4>
            </div>
            <div class="modal-body">

                <div class="col-md-12">

                    @php
                        $doctor_id = 0;
                        $doctor_id = session()->get('DOCTOR_ID');
                        $doctor_name = '';
                        if ($doctor_id != '') {
                            $doctor_name = \DB::table('doctor_master')
                                ->where('id', $doctor_id)
                                ->value('doctor_name');
                        }

                    @endphp
                    {{ $doctor_id }}{{ $doctor_name }}
                    <div class="col-md-3 padding_sm">
                        <div class="input-group custom-float-label-control">
                            <label class="custom_floatlabel">Doctor name</label>
                            <input style="width: 265px;" class="form-control" autocomplete="off" type="text"
                                placeholder="Doctor Name" value="{{ $doctor_name }}" id="calendar_doctor_name"
                                name="calendar_doctor_name">
                            <div id="calendar_doctorAjaxDiv" class="ajaxSearchBox" style="margin-top:22px;width:100%">
                            </div>
                            <input type="hidden" value="{{ $doctor_id }}" name="calendar_doctor_name_hidden"
                                id="calendar_doctor_name_hidden">
                        </div>
                    </div>

                    <div class="col-md-2 padding_sm">
                        <label for="">Appointment Date</label>
                        <div class="clearfix"></div>
                        <input type="text" id='calendar_search_date' onblur="myCalendarData();"
                            class="form-control datepicker" value="{{ $search_date_web }}" data-attr="date"
                            placeholder="Date">
                    </div>
                    <div class="col-md-4 padding_sm pull-right">
                        <span style="background-color:#ccc0c0;margin-top: 18px;color:black"
                            class="badge">Invalid</span>
                        <span style="background-color:#cfecfc;margin-top: 18px;color:black"
                            class="badge">Booked</span>
                        <span style="background-color:#92f0b6;margin-top: 18px;color:black"
                            class="badge">Checked In</span>
                    </div>

                </div>
                <div class="col-md-2 padding_sm card">
                    <div class="card-body" id="calendarData"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-------My calendar modals ends------------------------->



<!--- Patient Allergy Modal--------->


<div id="modal_allergy" class="modal fade " role="dialog">
    <div class="modal-dialog modal-lg" style="width:80%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Patient Allergy</h4>
            </div>
            <div class="modal-body" style="min-height:400px;">
                <div class="col-md-12 no-padding">
                    <div class="col-md-6 no-padding"
                        style="min-height: 380px; border-right:1px solid #e5e5e5; padding-left:5px;">

                        <div class="col-md-12">
                            <h4>Medicines</h4>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht10"></div>

                        <div class="medicationAllergyData">
                            @include('Emr::emr.includes.medicineallergy')
                        </div>
                    </div>

                    <div class="col-md-6 no-padding" style="min-height: 380px;">

                        <div class="col-md-12">
                            <h4>Others</h4>
                        </div>
                        <div class="otherAllergyData">
                            <div class="padding_sm">
                                <label for="allergy">Allergy Description:</label>
                                <br>
                                <textarea class="form-control otherAllergies" rows="5"></textarea>
                            </div>
                        </div>

                    </div>

                </div>



            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success saveMedicationAllergy"
                    onClick="saveMedicationAllergy();">Save</button>
            </div>

        </div>

    </div>
</div>

<!--- Patient Allergy Modal Ends---->

<!----Pain score modal-------------->
<div id="modal_painScore" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:68%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Pain score</h4>
            </div>
            <div class="modal-body">
                <div class="modal-body" style="max-height: 500px; padding-bottom: 35px;">
                    <form method="POST" action="" accept-charset="UTF-8" name="frm-score" id="frm-score">

                        {{-- Data Table Codes --}}
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="list-unstyled pain_score_img" style="margin-left:70px">
                                    <li><label for="score1">{!! Html::image('packages/extensionsvalley/default/img/pain_1.jpg', '', ['class' => 'img-responsive', 'data-score' => '0']) !!}</label>
                                        <input type="radio" id="score1" name="pain_score" checked="true" value="0"
                                            style="margin-left:50px">
                                    </li>
                                    <li><label for="score2">{!! Html::image('packages/extensionsvalley/default/img/pain_2.jpg', '', ['class' => 'img-responsive', 'data-score' => '2']) !!}</label>
                                        <input type="radio" id="score2" name="pain_score" checked="true" value="2"
                                            style="margin-left:50px">
                                    </li>
                                    <li><label for="score3">{!! Html::image('packages/extensionsvalley/default/img/pain_3.jpg', '', ['class' => 'img-responsive', 'data-score' => '4']) !!}</label>
                                        <input type="radio" id="score3" name="pain_score" checked="true" value="4"
                                            style="margin-left:50px">
                                    </li>
                                    <li><label for="score4">{!! Html::image('packages/extensionsvalley/default/img/pain_4.jpg', '', ['class' => 'img-responsive', 'data-score' => '6']) !!}</label>
                                        <input type="radio" id="score4" name="pain_score" checked="true" value="6"
                                            style="margin-left:50px">
                                    </li>
                                    <li><label for="score5">{!! Html::image('packages/extensionsvalley/default/img/pain_5.jpg', '', ['class' => 'img-responsive', 'data-score' => '8']) !!}</label>
                                        <input type="radio" id="score5" name="pain_score" checked="true" value="8"
                                            style="margin-left:50px">
                                    </li>
                                    <li><label for="score6">{!! Html::image('packages/extensionsvalley/default/img/pain_6.jpg', '', ['class' => 'img-responsive', 'data-score' => '10']) !!}</label>
                                        <input type="radio" id="score6" name="pain_score" checked="true" value="10"
                                            style="margin-left:50px">
                                    </li>
                                </ul>
                                <input type="button" class="btn btn-primary pull-right " value="Save"
                                    onclick="saveScore('add')">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
</div>
<!----Pain score modal Ends--------->

<!-----------lab results modal------------------------------>
<div id="modal_lab_results" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:98%">


        <div class="modal-content" style="border-radius:6px !important;">
            <div class="modal-header" style="background:#31b0b7; color:#ffffff;padding:5px !important;padding-left: 20px !important;border-top-left-radius:6px;border-top-right-radius:6px;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Lab Results</h4>
            </div>
            <div class="modal-body" style="min-height:580px;">
                <div class="row" id="lab_restuls_data">

                </div>
            </div>
        </div>

    </div>
</div>
<!-----------lab results modal ends------------------------->
<!-----------lab subtest results modal------------------------------>
<div id="modal_lab_sub_results" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:63%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Lab Subtest Results</h4>
            </div>
            <div class="modal-body" id="lab_sub_restuls_data">

            </div>
        </div>

    </div>
</div>
<!-----------lab subtest results modal ends------------------------->

<!-----------pathology results modal------------------------------>
<div id="modal_pathology_results" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:98%">


        <div class="modal-content" style="border-radius:6px !important;">
            <div class="modal-header" style="background:#31b0b7; color:#ffffff;padding:5px !important;padding-left: 20px !important;border-top-left-radius:6px;border-top-right-radius:6px;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Pathology Results</h4>
            </div>
            <div class="modal-body" style="min-height:580px;">
                <div class="row" id="pathology_restuls_data">

                </div>
            </div>
        </div>

    </div>
</div>
<!-----------pathology results modal ends------------------------->

<!-----------take appointment--------------------------------------->

<div id="appointment_modal" class="modal fade " role="dialog" style="z-index:9999">
    <div class="modal-dialog" style="width:63%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Take Appointment</h4>
            </div>
            <div class="modal-body" id="next_appointment_list_data">

            </div>
        </div>

    </div>
</div>
<!-----------take appointment ends---------------------------------->

<!----------------create booking model------------------->

<div class="modal fade bs-example-modal-lg" id="create_appointment_modal" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" style="margin:auto; width:auto">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header modal-header-info bg-orange">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create Appointment</h4>

            </div>

            <div class="modal-body" style="min-height: 215px; padding-bottom:25px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8 padding_sm">
                            <div class="input-group date custom-float-label-control">
                                <input type="hidden" value='{{ csrf_token() }}' id="hidden_filetoken">
                                <label class="custom_floatlabel">Patient Name/Uhid</label>

                                <input style="width: 370px;" type="text" name="patient_list_search" autocomplete="off"
                                    class="form-control " id="patient_list_search" value="">
                                <div id="patient_searchCodeAjaxDiv"
                                    style="margin: 0; z-index: 999; font-size: 12px; width: 100%;margin-top: 23px;"
                                    class="ajaxSearchBox"></div>
                                <input type="hidden" name="patient_search_hidden" id="patient_search_hidden" value="" />
                                <input type="hidden" name="create_booking_date" id="create_booking_date" value="" />
                                <input type="hidden" name="slot_time" id="slot_time" value="" />
                            </div>

                        </div>
                        <div class="col-md-4 padding_sm" style="">
                            <div class="clearfix"></div>
                            <label class="custom_floatlabel" style="margin-top: 0px;margin-left: 5px;">Age</label>
                            <input type="text" name="patient_age" class="form-control " id="patient_age" value="">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 padding_sm" style="">
                            <div class="clearfix"></div>
                            @php
                                $gender = \DB::table('gender')->pluck('name', 'id');
                            @endphp
                            <label class="custom_floatlabel" style="margin-top: 0px;margin-left: 5px;">Gender</label>

                            {!! Form::select('patient_gender', $gender, null, ['id' => 'patient_gender', 'class' => 'form-control']) !!}
                        </div>
                        <div class="col-md-4 padding_sm" style="">
                            <div class="clearfix"></div>
                            <label class="custom_floatlabel" style="margin-top: 0px;margin-left: 5px;">Phone</label>
                            <input type="text" name="patient_phone" class="form-control" id="patient_phone" value="">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 padding_sm" style="">
                            <div class="clearfix"></div>
                            <label class="custom_floatlabel" style="margin-top: 0px;margin-left: 5px;">Address</label>
                            <input type="text" name="patient_address" class="form-control" id="patient_address"
                                value="">
                        </div>
                    </div>
                </div>
                <div class="row" style="">
                    <div class="col-md-12 padding_sm" style="margin-top: 10px;">
                        <input type="button" onclick="create_new_appointment();" class="btn btn-primary" value='save'
                            style="float:right;margin-right: 12px;" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Model Create Booking -->

<!-----------patient summary model list------------------------------->

<div id="summary_list_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:73%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Discharge Summary List</h4>
            </div>
            <div class="modal-body" style="height:520px;">


                <div class="col-md-12" id="summary_list_data">

                </div>
            </div>
        </div>

    </div>
</div>
<div id="summary_view_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:73%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Discharge Summary</h4>
            </div>
            <div class="modal-body" id="summary_view_data" style="height:510px;overflow:scroll;">

            </div>
        </div>

    </div>
</div>

<!-----------patient summary model list ends-------------------------->


<!----Frequency popup---------->

<div class="modal bs-example-modal-md mkModalFloat" id="frequencypopup" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header bg-green">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">FREQUENCY DETAILS</h4>
            </div>

            <div class="modal-body">
                <div class="row">

                    <div class="col-md-2 padding_sm">
                        <input type="text" name="frequency_name" class="form-control" id="frequency_name"
                            placeholder="Frequency">
                    </div>

                    <div class="col-md-2 padding_sm">
                        <input type="text" name="frequency_value" class="form-control" id="frequency_value"
                            placeholder="Value/day">
                    </div>

                    <div class="col-md-2 padding_sm">
                        <button type="button" class="btn btn-success btn-block"
                            onclick="save_frequency_details();">Save</button>
                    </div>

                </div>
                <hr>
                <div class="row">

                    <!---- Common frequency table ----->
                    <div class="col-md-6">
                        <h5 style="margin-left:10px;">Available List</h5>
                        <div class="col-md-12" style="margin-bottom: 15px;">
                            <input type="text" name="searchFreq" class="form-control" id="searchFreq"
                                placeholder="search..">
                        </div>
                        <div class=" theadscroll padding_sm" style="position: relative; height: 350px;">
                            <table id="common_freq_table" class="table table-striped theadfix_wrapper table-sm">
                                <thead>
                                    <tr style="background-color: #0f996b; color:white;padding:0px;">
                                        <th>Frequency Name</th>
                                        <th style="text-align:center;">Value</th>
                                    </tr>
                                </thead>
                                <tbody class="globalFreqList">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!---- Common frequency table end ----->



                    <!---- user frequency table ----->
                    <div class="col-md-6">
                        <h5 style="margin-left:10px;">Selected List</h5>

                        <div class="col-md-12" style="margin-bottom: 15px;">
                            <input type="text" name="searchSelectedFreq" class="form-control" id="searchSelectedFreq"
                                placeholder="search..">
                        </div>
                        <div class=" theadscroll padding_sm" style="position: relative; height: 350px;">
                            <table id="selected_freq_table" class="table table-striped theadfix_wrapper table-sm">
                                <thead>
                                    <tr style="background-color: #0f996b; color:white;padding:0px;">
                                        <th style="text-align: left;">Frequency Name</th>
                                        <th style="text-align: center;">Value</th>
                                        <th style="text-align: center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="globalSelectedFreqList">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!---- user frequency table ends ----->




                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<!----Frequency popup ends---------->

<!-----Route popup------------------>
<div class="modal bs-example-modal-md mkModalFloat" id="routepopup" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width:80%">
        <div class="modal-content">
            <div class="modal-header bg-green">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Route List</h4>
            </div>

            <div class="modal-body" id="routtedatadiv">


            </div>
        </div>
    </div>
</div>
<!-----Route popup ends------------->


<div id="change_voume_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:53%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Change Rate</h4>
            </div>
            <div class="modal-body" id="change_volume_data" style="height:90px;text-align:center;">
                <input type="hidden" name="iv_chart_id_chagne" id="iv_chart_id_chagne" />
                <div class="col-md-12">

                    <div class="col-md-4">

                        <input style="font-weight:600px;" type="text" value="" name="iv_search_rate_change"
                            autocomplete="off" id="iv_search_rate_change"
                            class="form-control iv_search_rate_change timepickerclass">

                        <!-- iv rate List -->
                        <div class="iv-rate-list-div_change" id="iv-rate-list-div_change"
                            style="display: none;width:200px;">
                            <a style="float: left;" class="close_btn_iv_rate_search">X</a>
                            <div class="iv_rate_theadscroll" id="iv_rate_theadscroll_change"
                                style="position: absolute;z-index:9999 !important;postition:absolute;">
                                <table id="IvRateTable_change"
                                    class="table table-bordered-none no-margin table_sm table-striped presc_theadfix_wrapper">
                                    <tbody id="ListIvRateSearchData_change">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- iv rate List -->
                        <label style="margin-top: 3px;margin-left:5px;"
                            for="iv_rate_change"><b>Rate/Duration</b></label>

                    </div>

                    <div class="col-md-5">
                        <div class="col-md-12 padding_xs" style="text-align:center">
                            <input style="text-align:center" id="start_at_change" value="" name="start_at_change"
                                type="text" autocomplete="off" class="form-control date_floatinput timepickerclass"
                                placeholder="" />
                            <label style="margin-top: 3px;margin-left:5px;" for="start_at"><b>Time Started</b></label>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <button type="button" onclick="saveChangeVolume();" class="btn bg-primary btn-block"
                            name="saveChangeVolume" id="saveChangeVolume">
                            <i class="fa fa-save"></i> Save
                        </button>
                    </div>



                </div>

            </div>
        </div>

    </div>
</div>


<div id="view_document_modal" class="modal fade " role="dialog" style="z-index: 10025;">
    <div class="modal-dialog" style="width:93%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="document_data" style="height:550px">

                <iframe src="" width="100%" height="100%" id="myframe"></iframe>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

</div>

<div id="lab_results_rtf_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:73%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Lab Result</h4>
            </div>
            <div class="modal-body" id="lab_results_rtf_data" style="height:550px;overflow-y:scroll;">

            </div>

        </div>
    </div>

</div>

<div id="path_results_rtf_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:73%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Pathology Result</h4>
            </div>
            <div class="modal-body" id="path_results_rtf_data" style="height:550px;overflow-y:scroll;">

            </div>

        </div>
    </div>

</div>

<!-- billing report starts here -->

<div id="billing_report_modal" class="modal fade" role="dialog" style="margin-top:-20px;">
    <div class="modal-dialog" style="max-width: 98%; width: 100%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-sm modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Billing Report</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div id="billing-report-loader" class="hide">
                        <div class="row" style="text-align: center;">
                            <i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>
                    <div class="col-md-12 billing_report_wrapper">

                    </div>
                    <div class="clearfix"></div>
                    <div class="ht10"></div>

                </div>
            </div>
            <!-- <div class="modal-footer">
    <button style="margin: 0 5px 0 0;" data-dismiss="modal" class="btn light_purple_bg"><i class="fa fa-times"></i> Cancel</button>
    <button class="btn light_purple_bg"><i class="fa fa-save"></i> Save</button>
</div> -->
        </div>

    </div>
</div>

<!-- billing report modal ends here -->


<!-----------patient summary model list------------------------------->

<div id="complete_medication_history_modal" class="modal fade " role="dialog" style="z-index:9999">
    <div class="modal-dialog" style="width:90%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Complete Medication History</h4>
            </div>
            <div class="modal-body" style="min-height:1074px !important;">
                <div class='col-md-12' style="margin-bottom:15px;">
                    <div class='col-md-4 pull-left'>
                        <input type="text" class="form-control" onkeyup="loadCompletePrescriptionHistory(1);"
                            id="serchCompletePrescription" placeholder="Search Medicine" />
                    </div>
                    </br>
                </div>
                <div class="col-md-12" id="complete_medication_history_data"></div>
            </div>
        </div>

    </div>
</div>

<!-----------patient summary model list ends-------------------------->


<div id="modalChangeMedicationTime" class="modal fade " role="dialog" style="z-index:9999">
    <div class="modal-dialog" style="width:35%">
        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Change Medication Time</h4>
            </div>
            <div class="modal-body" style="height:55px;">
                <input type="hidden" name="change_med_chart_id" id="change_med_chart_id" value='' />
                <div class="md-form col-md-5 no-padding">
                    <input type="text" value="" id="med_chart_change_date" name="med_chart_change_date"
                        class="form-control datepicker  timepickerclass" data-attr="date" placeholder="Date">
                </div>

                <div class="md-form col-md-5">
                    <input type="time" name="med_chart_change_time" id="med_chart_change_time" value=""
                        class="form-control timepickerclass">
                    <label style="margin-top: 3px;" for="timepicker_"><b> </b></label>
                </div>
                <div class="col-md-2">
                    <button type="button" onclick="editGivenMedicationSave();" class="btn bg-primary"
                        id="btn_change_md_chart_time">
                        <i class="fa fa-save"></i> Save
                    </button>
                </div>

            </div>
        </div>

    </div>
</div>

<!-----------lab results trends modal------------------------------>
<div id="modal_lab_result_trends" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 92%;margin-left: 95px;">

        <div class="modal-content">
            <div class="modal-header" style="background:#267cb9; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Lab Result Graph View</h4>
            </div>
            <div class="modal-body" style="height:580px;">
                <div class="row" id="lab_result_trends_data">

                </div>
            </div>
        </div>
    </div>
</div>
<!-----------lab results trends modal ends------------------------->
<!-----------lab results trends modal------------------------------>
<div id="modal_pathology_result_trends" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 92%;margin-left: 95px;">

        <div class="modal-content">
            <div class="modal-header" style="background:#267cb9; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Pathology Result Graph View</h4>
            </div>
            <div class="modal-body" style="height:580px;">
                <div class="row" id="pathology_result_trends_data">

                </div>
            </div>
        </div>
    </div>
</div>
<!-----------lab results trends modal ends------------------------->

<!-----------outside investigation modal--------------------------------->
<div id="outsideInvestigationModal" class="modal fade " role="dialog" style="z-index:9999">
    <div class="modal-dialog" style="width:75%">
        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Outside Investigation</h4>
            </div>
            <div class="modal-body" style="height:auto;">
                <button type="button" class="btn btn-primary" style="float:right;" data-original-title="Save"><i
                        class="fa fa-plus fa-save "></i>
                    Save
                </button>
                <button type="button" data-toggle="tooltip" data-placement="top" title="" class="btn btn-success"
                    style="float:right;" data-original-title="Add" onclick="outsideInvestigationRow()"><i
                        class="fa fa-plus fa-lg "></i>
                    Add
                </button>

                <table id="outsideInvestigationTable" class="table table_sm no-border">
                    <tbody id="outsideInvestigationListData" class="">

                        @for ($j = 1; $j <= 3; $j++)


                            <tr style="
                        cursor:pointer">
                                <td width="90%">
                                    <input type="text" class="form-control" value=""
                                        onfocus="PreviewDescription(this)" autocomplete='off'
                                        placeholder="Outside Investigation" data-med="0" name="outsideInvestigation[]"
                                        id="outsideInvestigation-{{ $j }}">
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger" style="" value=""
                                        onclick="deleteoutsideInvestigation(this)" id="delete-{{ $j }}"><i
                                            class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>

                        @endfor

                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>

<!-----------outside investigation modal ends---------------------------->


<!----Fav Form Modal---------->

<div class="modal bs-example-modal-md mkModalFloat" id="form_fav_list_modal" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document" style="width:80%">
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Favorites List</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="theadscroll always-visible" style="position: relative;height: 300px;">
                            <table class="table no-margin table_sm no-border table-striped table-bordered">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th>#</th>
                                        <th>Form</th>
                                        <th>Created At</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="form_fav_list_body">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!----Fav Form Modal Ends---------->



<!-- Booking list Modal -->
<div id="appointments_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 73%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Booking list</h4>
            </div>
            <div class="modal-body" style="height:520px;">

                <div class="col-md-12" id="appointments_list_data">

                </div>

            </div>

        </div>

    </div>
</div>
<!-- Booking list Modal -->


<!-- lab result Modal -->
<div id="lab_result_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 73%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Lab Results</h4>
            </div>
            <div class="modal-body" style="height:520px;">

                <div class="col-md-12" id="lab_result_data">

                </div>

            </div>

        </div>

    </div>
</div>
<!-- lab result Modal -->

<!-- nursing notes Modal -->
<div id="nursing_notes_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 73%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nursing Notes</h4>
            </div>
            <div class="modal-body" style="height:520px;">

                <div class="col-md-12" id="nursing_notes_modal_data">

                </div>

            </div>

        </div>

    </div>
</div>
<!-- nursing notes Modal -->


<!-- nursing notes Modal -->
<div id="covid_info_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 73%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Covid-19 Information</h4>
            </div>
            <div class="modal-body" style="height:520px;">

                <div class="col-md-12" id="covid_info_modal_data">

                </div>

            </div>

        </div>

    </div>
</div>
<!-- nursing notes Modal -->


<!-----------emergency queue modal------------------------------>
<div class="modal fade emergency_queue_modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Emergency Message</h4>
            </div>
            <div class="modal-body">
                <div class="row" id="emergency_queue_modal_data">
                    <textarea class="form-control emergency_message_text" rows='6'> </textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm applyEmergencyMessage">Apply</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-----------emergency queue modal ends------------------------->

<!-----------private_notes modal------------------------------>
<div class="modal fade private_notes_modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Private Notes</h4>
            </div>
            <div class="modal-body" style="height:250px;">
                <div class="row" id="private_notes_modal_data">
                    @php
                        if (\WebConf::getConfig('private_note_visibility_default_checked') == 1) {
                            $visibility_status = 1;
                        } else {
                            $visibility_status = 0;
                        }
                    @endphp
                    @isset($private_note)
                        {!! Form::textarea('patient_notes', $private_note, ['class' => 'form-control', 'placeholder' => 'Private Notes. ', 'title' => 'Private Note', 'id' => 'patient_notes', 'required' => 'required', 'style' => 'resize: none;height:180px !important;border-radius:0px;', 'onblur' => 'add_private_note();']) !!}
                    @endisset
                    <div class="col-md-6 padding_xs">
                        <label class="visible visit-action-button"
                            style="margin-top: 3px;float:left; white-space: nowrap;">Visible to Others
                            <input class="" name="agree" id="visiblility_status" type="checkbox"
                                @if ($visibility_status == 1) checked @endif> </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-----------private_notes modal ends------------------------->


<!-----------select default doctor modal------------------------------>
<div class="modal fade select_default_doctor_modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">Select Default Doctor</h4>
            </div>
            <div class="modal-body" style="min-height:350px; height:auto;">
                <div class="col-md-12 padding_sm">
                    <div class="row select_default_doctor_modal_data">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-----------select default doctor modal ends------------------------->

<!-----modal usg results for verification----------------------------->
<div class="modal" id="usg_template_viewer_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width:80%">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#26b99a;color:white;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:white;">View Result</h4>
            </div>
            <div class="modal-body" style="min-height:500px;overflow:auto;">
                <div class="col-md-12" id="usg_result_view">

                </div>
            </div>
        </div>
    </div>
</div>
<!-----modal usg results for verification ends------------------------>

<!----------modal window list favourite template------------------------->

<div class="modal fade" id="favourite_template_list_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 9999999999">
    <div class="modal-dialog" role="document" style="width:95%;">
        <div class="modal-content">

            <div class="modal-body" style="height:575px;">
                <h4 style="color:blue;">Bookmarks</h4>
                <div class="col-md-12" style="width: 100%;" id="favourite_template_list_data">
                </div>
            </div>

        </div>
    </div>
</div>

<!----------Favourite template edit modal------------------------------>
<div class="modal fade" id="fav_template_edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1052">
    <div class="modal-dialog" role="document" style="width:70.5%;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(14, 124, 60);color:white;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Favourite Content</h4>
            </div>
            <div class="modal-body" id="fav_edit_modal_content" style="height:500px;">
                <div class="col-md-12" style="width: 100%;">
                        {!! Form::label('fav_temp_edit_lable', 'Name', ['class' => '']) !!}
                    <input type="hidden" style="height:34px !important;border-radius:3px;" name="fav_template_id" id="fav_template_id" value="" />
                    <input type="text" class="form-control" id="favourite_name_edit"/>
                </div>
                <div class="col-md-12" style="margin-top:10px;">
                    {!! Form::label('', 'Content', ['class' => '']) !!}
                    <textarea class="" name="favourite_template_content" id="favourite_template_content"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="usg_save_fav_template_edit();">Save changes</button>
            </div>

        </div>
    </div>
</div>


<!----------Surgery Request modal------------------------------>
<div class="modal fade" id="surgery_request_modal" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel" style="z-index:1051 !important">
    <div class="modal-dialog" role="document" style="width:91%;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(14, 124, 60);color:white;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Surgery Request</h4>
            </div>
            <div class="modal-body theadscroll" id="surgery_request_modal_body"
                style="position: relative;;height:580px;">

            </div>
        </div>
    </div>
</div>

<!-----------radiology results modal------------------------------>

<div class="modal fade" id="radiology_results_modal" tabindex="-1" role="dialog" style="display:none;">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header modal_box table_header_bg">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title">Radiology Results</h4>
            </div>
            <div class="modal-body" style="min-height: 250px; max-height: 450px;" id="radiology_results_modal_body">

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs sm_nav nav-pills" style="margin-left: 15px; ">
                        <li class="active"><a data-toggle="tab" href="#tab_1" onclick="fetchRadiologyResults()" style="padding:5px;">Radiology Results </a></li>
                        <li><a data-toggle="tab" href="#tab_2" onclick="fetchRadiologyUploadsData();" style="padding:5px;">Radiology Uploads </a></li>
                    </ul>
                    <div class="tab-content" style="min-height: 535px; margin-top: 10px;">
                        <div id="tab_1" class="tab-pane active">
                            <div class="col-md-12">
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Service Description</label>
                                        <div class="clearfix"></div>
                                        <select id="search_service_id" class="form-control"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="radiology_results_data">

                            </div>
                        </div>

                        <div id="tab_2" class="tab-pane">
                            <div class="col-md-12" id="radiology_upload_data">

                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<!-----------radiology results modal ends------------------------->


<!-----------radiology results modal------------------------------>
<div id="radiology_pacs_viewer_iframe_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:97%">

        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title"> Radiology Image Viewer</h4>
            </div>
            <div class="modal-body no-padding" style="min-height:560px;" >
                <div class="col-md-12" style="margin-top:10px">
                    <iframe src="" title="PACS Viewer" id="pacs_viewer_iframe" style="width:100%; height: 550px;"></iframe>
                </div>
            </div>
        </div>

    </div>
</div>
<!-----------radiology results modal ends------------------------->
<!----------Drug Reaction Modal------------------------------>
<div class="modal fade" id="drug_reaction_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index:1052">
    <div class="modal-dialog" role="document" style="width:89.5%;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(14, 124, 60);color:white;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Drug Reaction</h4>
            </div>
            <div class="modal-body" id="drug_reaction_modal_content" style="height:620px;">

            </div>
        </div>
    </div>
</div>

{{-- usg shortcodes --}}
<div class="modal" id="short_code_modal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width:80%">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#26b99a;color:white;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:white;">Ultrasound Shortcodes</h4>
            </div>
            <div class="modal-body" style="min-height:500px;overflow:auto;">
                <div class="col-md-12" id="short_code_modal_data">

                </div>
            </div>
        </div>
    </div>
</div>
{{-- password confirmation  --}}
<div class="modal right fade" id="modalLoginForm">
    <input type="hidden" value="0" id="modalform_id_hidden" />
    <input type="hidden" value="0" id="modalhead_id_hidden" />
    <div class="modal-dialog" style="2px 3px 3px 0px #0000007a !important;width: 400px ">
      <div class="modal-content" >
        <!-- header -->
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal" style="color: black">&times;</button>
          <h3 class="modal-title">Confirm Password</h3>
        </div>
        <!-- body -->
        <div class="modal-body" style="min-height: 67px">
             <div class="col-md-12">
                <div class="col-md-12" >
                    <input type="password" class="form-control" id="emr_con_password" style="height: 35px !important" autocomplete="off" placeholder="Confirm your password" / >
                </div>
             </div>
        </div>
        <!-- footer -->
        <div class="modal-footer">
          <button class="btn btn-primary btn-block" title="Confirm password" id="con_emr_login_btn" onclick="confirmLoginEmrUser(this)">Confirm <i class="fa fa-check"></i></button>
        </div>

      </div>
    </div>
  </div>
<!---------------------------NEW BORN BABY--------------------------------------------->
<div id="new_born_baby_modal" class="modal fade " role="dialog" style="margin-top: -28px;">
    <div class="modal-dialog" style="width:99%">
      <input type="hidden" id="baby_count" value="">
      <input type="hidden" id="baby_edit_id" value="">
        <div class="modal-content">
            <div class="modal-header" style="background:#26b99a; color:#ffffff;padding:5px!important;">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>

                        <span class="" style="font-size: 15px !important">Mother UHID : </span>
                        <span style="font-size: 15px !important;margin-right:10px;" id="mother_uhid_1" ></span>
                       |<span style="font-size: 15px !important;margin-left:10px;" class="">Mother Name : </span>
                        <span style="font-size: 15px !important" id="mother_name_1" ></span>

            </div>
            <div class="modal-body" id="new_born_baby" style="height:660;margin:-15px !important;">
              <div class="col-md-12">
                <div class="col-md-8 box-body" style="height:610px">
                    <div class="col-md-12" style="background:#26b99a; color:#ffffff;text-align:center;font-weight:700;">
                         Birth Registration History
                    </div>
                    <div class="col-md-12" style="margin-top: 10px">
                        <div class="col-md-6">
                            <div class="col-md-6">
                             <div class="mate-input-box">
                                 <label for="">From Date</label>
                                 <div class="clearfix"></div>
                                 <input autocomplete="off" type="text" class="form-control baby_datepicker searchables_date" id="new_born_from_date">
                             </div>
                            </div>
                            <div class="col-md-6">
                             <div class="mate-input-box">
                                 <label for="">To Date</label>
                                 <div class="clearfix"></div>
                                 <input autocomplete="off" type="text" class="form-control baby_datepicker searchables_date" id="new_born_to_date">
                             </div>
                            </div>
                         </div>
                         <div class="col-md-5">
                            <div class="col-md-6">
                                <div class="mate-input-box">
                                    <label for="">Baby UHID</label>
                                    <div class="clearfix"></div>
                                    <input autocomplete="off" type="text" class="form-control searchables" id="new_born_baby_uhid">
                                </div>
                               </div>
                               <div class="col-md-6">
                                <div class="mate-input-box">
                                    <label for="">Baby Name</label>
                                    <div class="clearfix"></div>
                                    <input autocomplete="off" type="text" class="form-control searchables" id="new_born_baby_name">
                                </div>
                               </div>
                         </div>
                         <div class="col-md-1">
                            <button type="button" class="btn btn-warning" id="" onclick="resetSearch()"><i class="fa fa-refresh"></i></button>
                         </div>
                    </div>
                    <div class="col-md-12" id="birthRegistrationHistoryTable" style="padding: 0px;">
                       <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;font-size: 12px;;  " width="100%" border="1" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr class="headerclass"
                                style="background:#5d9b7b;color:white;border-spacing: 0 1em;font-family:sans-serif">
                                <th width='3%' style="white-space:pre;text-align: center;">SL. No.</th>
                                <th width='13%' style="white-space:pre;text-align: center;">Mother UHID</th>
                                <th width='10%'style="white-space:pre;text-align: center;">Mother Name</th>
                                <th width='10%' style="white-space:pre;text-align: center;">Baby UHID</th>
                                <th width='10%' style="white-space:pre;text-align: center;">Baby Name</th>
                                <th width='8%' style="white-space:pre;text-align: center;">Gender</th>
                                <th width='14%' style="white-space:pre;text-align: center;">Birth date time</th>
                                <th width='8%' style="white-space:pre;text-align: center;">Weight</th>
                                <th width='10%' style="white-space:pre;text-align: center;">Lenght</th>
                                <th width='15%'style="white-space:pre;text-align: center;">Head Circumference</th>
                            </tr>


                        </thead>
                        </table>

                    </div>
                </div>
                <div class="col-md-4 box-body" style="height:610px">
                   <div class="col-md-12" style="background:#26b99a; color:#ffffff;text-align:center;font-weight:700;"> <span >Birth Registration</span></div>
                   <div class="col-md-12" style="background:#5d9b7b;color:#ffffff;margin-top:5px;font-weight:700;"> <span >Mother Details</span></div>
                   <div class="col-md-12" style="margin-top: 5px;">
                    <div class="col-md-4">
                     <div class="mate-input-box">
                         <label for="">Mother UHID</label>
                         <div class="clearfix"></div>
                         <input autocomplete="off" type="text" class="form-control" id="add_mother_uhid" disabled>
                     </div>
                    </div>
                    <div class="col-md-4">
                     <div class="mate-input-box">
                         <label for="">Mother Name</label>
                         <div class="clearfix"></div>
                         <input autocomplete="off" type="text" class="form-control" id="add_mother_name" disabled>
                     </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mate-input-box">
                            <label for="">Mother Age</label>
                            <div class="clearfix"></div>
                            <input autocomplete="off" type="text" class="form-control" id="add_mother_age" disabled>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mate-input-box">
                            <label for="">Admitted Doctor</label>
                            <div class="clearfix"></div>
                            <input autocomplete="off" type="text" class="form-control" id="add_admit_doc" disabled>
                            <input autocomplete="off" type="hidden" class="form-control" id="add_admit_doc_id">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mate-input-box">
                            <label for="">Admitted Date</label>
                            <div class="clearfix"></div>
                            <input autocomplete="off" type="text" class="form-control" id="add_admit_date" disabled>
                            <input autocomplete="off" type="hidden" class="form-control" id="bed_id">
                        </div>
                    </div>
                    <div class="col-md-12" style="background: #5d9b7b;color:#ffffff;margin-top:5px;font-weight:700;">
                        <span >Baby Details</span><button class="btn btn-default" style="float:right;margin-top: 2px;height: 15px;cursor: pointer;" id="addBabyDetailsBOX" onclick="addNewBabyDetailsDiv()" ><i  class="fa fa-plus pull-right"   id="addBabyDetailsBOX_btn"style=""></i></button></div>
                    <div class="col-md-12" style="margin-top: 5px;" id="add_baby_details">
                        <input type="hidden" id="add_baby_details_count" value="0">
                        <div id="msform">
                            <ul id="progressbar" style="cursor: pointer;font-size: 11px;height: 39px;" class="nav nav-tabs" role="tablist" >
                            </ul>
                            <div class="tab-content" id="baby_details_tab_body" style="height: 315px;">
                            </div>
                        </div>
                       <div class="clearfix"></div>
                    </div>
                    <div  class="col-md-12" style="margin-top: 5px;" id="add_baby_details_edit" style="display: none">

                    </div>
                 </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" style="margin-top:10px;" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-default" style="margin-top:10px;display:none;"  id="resetBabyTabLI_int" onclick="resetBabyTabLI_int()" ><i class="fa fa-refresh" id=""></i> Reset</button>
                <button class="btn btn-success pull-right" style="border-radius: 10px 2px 10px 2px;margin-top:10px;" id="save_new_born" onclick="saveNewBabyDetails()" ><i class="fa fa-save" id="save_new_born_spin"></i> Save</button>

            </div>
        </div>
    </div>

</div>

<div class="modal fade " id="presc_comman_validate_modal" role="dialog" aria-hidden="true" style='z-index:999999'>
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header bg-green">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close close_white">&times;</button>
                <h4 class="modal-title">These medicines already added in this visit. Do you want to continue ?</h4>
            </div>

            <div class="modal-body" id="presc_comman_validate_modal_data">

            </div>
            <div class="modal-footer" style="border-top: 1px solid #1abb9c;">
                <button type="button" id="btn_add_medicine_save" onclick="add_medicine_save()" class="btn btn-success" style=" width:100px;">Yes</button>
            </div>
        </div>
    </div>
</div>


<!----Caualty visit print---------->

<div class="modal bs-example-modal-md mkModalFloat" id="modalCasualtyVisit" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Select Casualty Visit</h4>
            </div>
            <div class="modal-body" id="modalCasualtyVisitData"></div>
        </div>
    </div>
</div>

<!----Caualty visit print---------->
<div class="modal bs-example-modal-lg mkModalFloat" id="modalCasualtyVisitPrint" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Casualty Visit Summary</h4>
            </div>
            <div class="modal-body theadscroll" style="height:615px;position: relative;margin-bottom:30px;" id="modalCasualtyVisitPrintData"></div>
            <div class="modal-footer">
                <button type="button" class="btn bg-primary pull-right" onclick="takePrintCsSummary();"><i class='fa fa-print'></i> Print</button>
            </div>
        </div>
    </div>
</div>

