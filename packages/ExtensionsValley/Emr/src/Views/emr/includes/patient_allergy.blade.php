<div class="no-border no-margin" id="patient_allergy_div_container">
    <div class="box-body allergy_body" style="max-height:86px;border-radius:5px;">

    <div class="theadscroll always-visible" style="position: relative;height:80px;">
        <table class="table theadfix_wrapper no-margin table_sm no-border" style="width:100%;">
                    <thead>
                        <tr style="">
                            <th width="68%">Allergies</th>
                            <th width="28%">
                                <span  style="margin-right:10px; cursor: pointer;" id="add_allergy_new" onclick="fetchPatientAllergies(this);" class="pull-right ">
                                    <i class="fa fa-plus"></i>
                                </span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="allegy_details">

                    </tbody>
        </table>
    </div>
    </div>
</div>
