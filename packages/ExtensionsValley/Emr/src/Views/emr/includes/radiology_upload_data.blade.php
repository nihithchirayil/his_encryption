
<div class="box no-border no-margin anim" style="min-height:450px;">
    <div class="col-md-12">
        <div class="row">
            {!! Form::open(['id' => 'manageDocsForm_tk', 'name' => 'manageDocsForm', 'method' => 'post', 'file' => 'true', 'enctype' => 'multipart/form-data']) !!}
            @php $i = 1; @endphp
           

            <div class="clearfix"></div>
            <div class="ht5"></div>
           
                <div class="col-md-3 padding_sm">
                    <input type="text" name="description[]" class="form-control description_upload_tk"
                        id="description_tk" placeholder="Description">
                </div>

                <div class="col-md-3 padding_sm">
                    @php $f = $i - 1; @endphp
                    <input name="file_upload[]" type="file" id="f_upload_tk"
                        class="inputfile inputfile-1 hidden document_upload_tk" data-show-upload="false"
                        data-multiple-caption="{count} files selected" />
                    <label class="no-margin btn btn-block custom-uploadfile_label" for="f_upload_tk">
                        <i class="fa fa-folder"></i>
                        <span id="uploadspan_documentsdocumenttab">
                            Choose a file ...
                        </span>
                    </label>
                    <a class="uploadedImg_tk" style="display:none;" href="" target="_blank"> </a>
                    <span style="color: #d14;">@if ($errors->first('f_upload_tk.' . $f) != '') {{ 'Upload only doc,docx,pdf,jpeg,jpg,png,gif,bmp and File size < 10MB' }} @endif</span>
                </div>

                <div class="col-md-1 padding_sm">
                    <button class="btn btn-success btn-block save_class" id="doc_file_upload_tk" type="submit">
                        <i class="fa fa-plus" style="color: white" id="upload_file_doc"></i> 
                    </button>
                </div>

                {{-- <div class="col-md-1 padding_sm">
                    <button type="button" class="btn btn-danger btn-block" onclick="clear_upload_file_tk();"><i
                            class="fa fa-trash-o"></i> Clear</button>
                </div> --}}
                <!-- <div class="col-md-2 padding_sm">
                <button type="button" class="btn btn-danger btn-block" onclick="remove_upload_file('default');"><i class="fa fa-trash-o"></i> Clear</button>
            </div> -->

            <div class="clearfix"></div>

            <div class="col-md-12 no-padding">
                <div class="theadscroll sm_margin_bottom" id="roww" style="max-height: 160px; height: auto;">
                </div>
            </div>

            @php $i++; @endphp

            {!! Form::token() !!}
            {!! Form::close() !!}

        </div>
    </div>
    <div class="box-body clearfix">
        <table class="table table-bordered no-margin table_sm no-border theadfix_wrapper">
            <thead>
                <tr class="table_header_bg">
                    <th>Sl.No.</th>
                    <th>Date</th>
                    {{-- <th>Title</th> --}}
                    <th>Description</th>
                    <th>Preview</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody class="radiology_list_table_body">
            @isset($radiology_list)
            @if(count($radiology_list) > 0)
            @foreach($radiology_list as $radiology)
            @php
                $created_at = (!empty($radiology->created_at)) ? date('M-d-Y', strtotime($radiology->created_at)) : '';
            @endphp
            <tr class="patient_det_widget_hp" style="cursor:pointer;" >
                <td>{{$loop->iteration}}</td>
                <td>{{$created_at}}</td>
                {{-- <td>{{$radiology->title}}</td> --}}
                <td>{{$radiology->description}}</td>
                <td><a style="cursor: poiner;" onclick="docImgPreview('{{$radiology->id}}')"><i class="fa fa-eye"></i></a></td>
                <td><a style="cursor: poiner;" onclick="docRadioImgDelete('{{$radiology->id}}')"><i class="fa fa-trash"></i></a></td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="7">No records found..!</td>
            </tr>
            @endif
            @endisset
            </tbody>
        </table>

    </div>
</div>