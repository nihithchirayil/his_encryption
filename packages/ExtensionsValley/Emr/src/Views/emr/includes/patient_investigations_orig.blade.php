<div class="box no-border no-margin right_wrapper_box anim">
    <div class="box-body clearfix" style="height: 475px; overflow: auto;">
        <div class="col-md-12 no-padding">
                   <div class="col-md-11" style="padding:0 15px 0 0;">
                    <h4 class="card_title">
                        History
                    </h4>
                </div>
                <div class="col-md-1 text-right">
                    <i class="fa expand_rightbox_btn fa-expand"></i>
                </div>
            </div>
        <div class="col-md-12" style="padding: 2px;">
            <div class="card">
                <div class="card_body">
                    <div class="theadscroll always-visible" style="position: relative; height: 70px;">
                                <table class="table no-margin theadfix_wrapper table_sm no-border">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th></th>
                                        <th style="width: 20%;">Request No.</th>
                                        <th style="width: 17%;">Doctor</th>
                                        <th style="width: 10%;"></th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>21-10-2020 11:32</td>
                                        <td>2764</td>
                                        <td>G Surgery</td>
                                        <td><span class="badge badge_purple_bg">OP</span></td>
                                        <td><button class="btn btn-block light_purple_bg"><i class="fa fa-eye"></i></button></td>
                                        <td><button class="btn btn-block light_purple_bg"><i class="fa fa-print"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td>15-10-2020 12:09</td>
                                        <td>1234</td>
                                        <td>IMC</td>
                                        <td><span class="badge badge_purple_bg">IP</span></td>
                                        <td><button class="btn btn-block light_purple_bg"><i class="fa fa-eye"></i></button></td>
                                        <td><button class="btn btn-block light_purple_bg"><i class="fa fa-print"></i></button></td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                </div>
                <div class="clearfix"></div>
                <div class="ht5"></div>
                <div class="col-md-12 no-padding text-right">
                    <button class="btn light_purple_bg" style="margin: 0 5px 0 0;"><i class="fa fa-star"></i> Bookmarks</button>
                    <button class="btn light_purple_bg"> Frequent</button>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="ht5"></div>
            <table class="table no-margin table_sm no-border">
                <tbody>
                    <tr>
                        <td width="10%">
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="inlineRadio11" value="option1" name="radioInline11" checked="">
                                <label for="inlineRadio11"> Lab </label>
                            </div>
                        </td>
                        <td width="10%">
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="inlineRadio22" value="option2" name="radioInline11">
                                <label for="inlineRadio22"> Radiology </label>
                            </div>
                        </td>
                        <td>
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="inlineRadio33" value="option3" name="radioInline11">
                                <label for="inlineRadio33"> Porcedures </label>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="clearfix"></div>
            <div class="ht5"></div>
            <div class="card">
                <div class="clearfix"></div>
                <div class="ht5"></div>
                <div class="nav-tabs-custom blue_theme_tab no-margin">
                    <ul class="nav nav-tabs sm_nav text-center">
                        <li class="active"> <a href="#tab1" data-toggle="tab" aria-expanded="true">Biochemistry</a></li>
                        <li class=""> <a href="#tab2" data-toggle="tab" aria-expanded="false">Hormones</a></li>
                        <li class=""> <a href="#tab3" data-toggle="tab" aria-expanded="false">Serology</a></li>
                        <li class=""> <a href="#tab4" data-toggle="tab" aria-expanded="false">Urine Analysis</a></li>
                        <li class=""> <a href="#tab5" data-toggle="tab" aria-expanded="false">Stool Analysis</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab1">
                            <table class="table no-margin no_bg table_sm no-border">
                                <tbody>
                                    <tr>
                                        <td>
                                            <table class="table no_bg no-margin table_sm no-border chekbox_table">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox checkbox-primary">
                                                                <input id="checkbox2" class="styled" type="checkbox" checked="">
                                                                <label for="checkbox2">
                                                                    AKA
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox checkbox-primary">
                                                                <input id="checkbox3" class="styled" type="checkbox" checked="">
                                                                <label for="checkbox3">
                                                                    Bilrubin
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox checkbox-primary">
                                                                <input id="checkbox4" class="styled" type="checkbox" checked="">
                                                                <label for="checkbox4">
                                                                    BUN
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox checkbox-primary">
                                                                <input id="checkbox5" class="styled" type="checkbox" checked="">
                                                                <label for="checkbox5">
                                                                    Creatine
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            <table class="table no_bg no-margin table_sm no-border chekbox_table">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox checkbox-primary">
                                                                <input id="checkbox22" class="styled" type="checkbox" checked="">
                                                                <label for="checkbox22">
                                                                    Ammonia
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox checkbox-primary">
                                                                <input id="checkbox33" class="styled" type="checkbox" checked="">
                                                                <label for="checkbox33">
                                                                    Globulin
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox checkbox-primary">
                                                                <input id="checkbox44" class="styled" type="checkbox" checked="">
                                                                <label for="checkbox44">
                                                                    GRBS
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="tab2">
                            Hormones
                        </div>
                        <div class="tab-pane fade" id="tab3">
                            Serology
                        </div>
                        <div class="tab-pane fade" id="tab4">
                            Urine Analysis
                        </div>
                        <div class="tab-pane fade" id="tab5">
                            Stool Analysis
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
