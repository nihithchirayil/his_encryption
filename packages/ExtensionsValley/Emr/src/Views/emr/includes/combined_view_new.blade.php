<style>
    .revenue_main_shadow {
        box-shadow: 1px 1px 1px 1px #5f6162;
        border-radius: 10px;
        border: 3px solid #f5dfdf;
    }

    .listBtn {
        margin: 0px px;
        padding: 0px;
        width: 100%;
        margin-top: 5px;
        margin-left: 5px;
    }
</style>

<div class="col-md-2 padding_sm" style="width:10%;" id="history_date_container">
    <div class="box no-border no-margin">
        <div class="box-footer revenue_main_shadow" style="min-height: 520px;">
            <div class="theadscroll" style="position: relative; height: 500px; padding-right: 10px;">
                <div class="combined_view_date_list">
                    <ul class="list-unstyled">
                        @if($total_count > 0)
                        @php
                        $i=1;
                        @endphp
                        @foreach($data as $datas)
                        @php
                        $doctor_name = (!empty($datas->doctor_name)) ? $datas->doctor_name : "";
                        $encounter_date_time = (!empty($datas->encounter_date_time)) ?
                        ExtensionsValley\Emr\CommonController::getDateFormat($datas->encounter_date_time,'d-M-Y h:i') :
                        "";
                        $class_active = "";
                        if($i==1):
                        $class_active = "active";
                        else:
                        $class_active = "";
                        endif;
                        $scroll_to_tab = "scrollto-".$i;
                        $selected_tab = "selectedtab-".$i;
                        @endphp
                        <li class="btn btn-success listBtn" title="{{$doctor_name}}" style="background: #4f9989 !important">
                            <a id="{{$selected_tab}}" class="smooth_scroll {{$class_active}}" href="#{{$scroll_to_tab}}"
                                title="{{date('M-d-Y',strtotime($encounter_date_time))}} - {{$doctor_name}}"
                                style="border:none;color:rgb(3, 29, 9);margin-top:.5px !important;margin-left:-0.5px ​!important;">
                                @php $doctor_name = trim($doctor_name); @endphp
                                <b style="color: #fff">{{date('M-d-Y',strtotime($encounter_date_time))}}</b> <br />
                                <span style="font-size:10px;color: #fff">
                                    {{substr($doctor_name, 0, strpos($doctor_name, " "))}}
                                </span>
                            </a>
                        </li>
                        @php
                        $i++;
                        @endphp
                        @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-10 padding_sm" style="width:90%;" id="history_data_container">
    <div class="box no-border no-margin">
        <div class="box-footer revenue_main_shadow" style="min-height: 520px;">
            <div class="theadscroll combined_theadscroll always-visible"
                style="position: relative; height: 500px; padding-right: 10px;">
                <table class="table table-striped table_sm padding_sm">
                    <tbody>
                        @if($total_count > 0)
                        @php
                        $i=1;
                        //dd($datas);
                        @endphp
                        @foreach($data as $datas)
                        @php

                        $visit_id = $datas->visit_id;
                        $class_highlight = "";
                        if($i==1):
                        $class_highlight = "tr_highlight";
                        else:
                        $class_highlight = "";
                        endif;
                        $scroll_to_tab = "scrollto-".$i;
                        $selected_tab = "selectedtab-".$i;

                        @endphp
                        @if($datas->is_casuality_visit ==1 && \DB::table('patient_casuality_visit')
                        ->where('visit_id',$visit_id)
                        ->whereNull('deleted_at')
                        ->exists())
                        @php
                        $visit_id = $datas->visit_id;
                        $visit_data = \DB::table('patient_casuality_visit')
                        ->where('visit_id',$visit_id)
                        ->whereNull('deleted_at')
                        ->select('template_print_view','created_at','doctor_id')->get();
                        $encounter_date_time = date('M-d-Y h:i A',strtotime($visit_data[0]->created_at));;
                        $doctor_name =
                        \DB::table('doctor_master')->where('id',$visit_data[0]->doctor_id)->value('doctor_name');
                        @endphp

                        <tr id="{{$scroll_to_tab}}" class="{{$class_highlight}} {{$selected_tab}}"
                            style="margin-bottom:5px; background: #26b99a; color: #fff;">
                            <td colspan="3" class="text-left">
                                <b>{{$encounter_date_time}} &nbsp; - &nbsp; {{$doctor_name}} &nbsp; - &nbsp;
                                </b>
                                <span style="background: #0414128c  !important;color: #ffffff"
                                    class="badge">{{$visit_status}}</span>
                            </td>
                            <td class="text-right">&nbsp;</td>
                        </tr>

                        <tr style="margin-bottom:5px;">
                            <td colspan="3">{!! $visit_data[0]->template_print_view !!}</td>
                        </tr>
                        @else
                        @php
                        $doctor_name = (!empty($datas->doctor_name)) ? $datas->doctor_name : "";
                        $visit_status = (!empty($datas->visit_status)) ? $datas->visit_status : "";
                        $encounter_date_time = (!empty($datas->encounter_date_time)) ?
                        ExtensionsValley\Emr\CommonController::getDateFormat($datas->encounter_date_time,'d-M-Y h:i') :
                        "";
                        $patient_medication = (!empty($datas->patient_medication)) ? (Array) $datas->patient_medication
                        : "";
                        $patient_invsestigation = (!empty($datas->patient_invsestigation)) ? (Array)
                        $datas->patient_invsestigation : "";
                        $patient_formdata = (!empty($datas->patient_formdata)) ? (Array) $datas->patient_formdata : "";
                        $patient_vitals = (!empty($datas->patient_vitals)) ? (Array) $datas->patient_vitals : "";
                        //$doctor_assessment = @$datas->doctor_assessment ? (Array) $datas->doctor_assessment : array();


                        $gynecology_template = !empty($datas->gynecology_template) ? (array) $datas->gynecology_template : '';
                        $gynecology_usg_result = !empty($datas->gynecology_usg_result_entry) ? (array) $datas->gynecology_usg_result_entry : '';
                        $gynecology_obstretics_history =  !empty($datas->gynecology_obstretics_history) ? (array) $datas->gynecology_obstretics_history : '';
                        $antinatal_template = !empty($datas->antinatal_visit_data) ? (array) $datas->antinatal_visit_data : '';
                        $antinatal_clinical_data = !empty($datas->antinatal_clinical_data) ? (array) $datas->antinatal_clinical_data : '';


                        $doctor_assessment = @$datas->emr_lite_doctor_assessment ? (Array) $datas->emr_lite_doctor_assessment : array();
                        $invsetigation_result_entry = @$datas->invsetigation_result_entry ? (Array)
                        $datas->invsetigation_result_entry : array();

                        $ca_count = [];
                        $inv_count = [];
                        $med_count = [];
                        $vitals_count = [];

                        $gynec_count = [];
                        $gynec_usg_count = [];
                        $gynec_obstretics_count = [];
                        $antinatal_count = [];
                        $antinatal_clinical_count = [];

                        $med_count = (!empty($patient_medication)) ? sizeof($patient_medication) : 0;
                        $inv_count = (!empty($patient_invsestigation)) ? sizeof($patient_invsestigation) : 0;
                        $ca_count = (!empty($patient_formdata)) ? sizeof($patient_formdata) : 0;
                        $vitals_count = (!empty($patient_vitals)) ? sizeof($patient_vitals) : 0;

                        $gynec_count = !empty($gynecology_template) ? sizeof($gynecology_template) : 0;
                        $gynec_usg_count = !empty($gynecology_usg_result) ? sizeof($gynecology_usg_result) : 0;
                        $gynec_obstretics_count = !empty($gynecology_obstretics_history) ? sizeof($gynecology_obstretics_history) : 0;
                        $antinatal_count = !empty($antinatal_template) ? sizeof($antinatal_template) : 0;
                        $antinatal_clinical_count = !empty($antinatal_clinical_data) ? sizeof($antinatal_clinical_data) : 0;


                        @endphp
                        <tr id="{{$scroll_to_tab}}" class="{{$class_highlight}} {{$selected_tab}}"
                            style="margin-bottom:5px;  background: #26b99a; color: #fff;">
                            <td colspan="3" class="text-left">
                                <b>{{$encounter_date_time}} &nbsp; - &nbsp; {{$doctor_name}} &nbsp; - &nbsp;
                                </b>
                                <span style="background: #0414128c !important;color: #ffffff"
                                    class="badge">{{$visit_status}}</span>
                            </td>
                            <td class="text-right">&nbsp;</td>
                        </tr>

                        <td width="34%">
                            <table class="table no-margin table-striped   ">
                                <tbody>
                                    <tr style="background: #92979b7d;">
                                        <td colspan="2"><b style="color:#3c3434;">Notes</b></td>
                                    </tr>
                                    @if($ca_count > 0)
                                    @for ($n = 0; $n < $ca_count; $n++) @php $name=(!empty($patient_formdata[$n]->
                                        name)) ?
                                        $patient_formdata[$n]->name : "";
                                        $form_id = (!empty($patient_formdata[$n]->form_id)) ?
                                        $patient_formdata[$n]->form_id
                                        : "";
                                        if(isset($patient_formdata[$n]->typ) && $patient_formdata[$n]->typ ==
                                        'old'){
                                        $data_text = (!empty($patient_formdata[$n]->data_text)) ?
                                        $patient_formdata[$n]->data_text : '';
                                        } else {
                                        $data_text = (!empty($patient_formdata[$n]->data_text)) ?
                                        json_decode($patient_formdata[$n]->data_text) : [];
                                        }
                                        @endphp
                                        <tr style="background: #dfe3e7  ;">
                                            <td colspan="2">Case Sheet <b> : {{$name}}</b></td>
                                        </tr>
                                        @if(isset($patient_formdata[$n]->typ) && $patient_formdata[$n]->typ ==
                                        'old')
                                        <tr>
                                            <td colspan="2">
                                                <table class="table " style="margin-bottom:4px;">
                                                    <tbody>
                                                        <tr style="padding-left:0px;">
                                                            <td colspan="4">
                                                                {!!$data_text!!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        @else
                                        <tr>
                                            <td colspan="2">
                                                <table class="table  " style="margin-bottom:4px;">
                                                    <tbody>
                                                        @php
                                                        $result = '';
                                                        if(!empty($form_id)){
                                                        $result =
                                                        ExtensionsValley\Emr\FormDataList::getFormData($form_id,
                                                        $data_text);
                                                        }
                                                        @endphp
                                                        @if(!empty($result))
                                                        @foreach($result as $key => $data)
                                                        <tr style="padding-left:0px;">
                                                            @php
                                                            $field_label = (!empty($data['field_label'])) ?
                                                            $data['field_label'] : '';
                                                            $res = (!empty($data['result'])) ? $data['result'] : '';
                                                            @endphp
                                                            @if(!empty($res))
                                                            @php
                                                            $res = nl2br($res);
                                                            @endphp
                                                            <td colspan="4">
                                                                <b>{!!$field_label!!}</b>
                                                                <br> {!!$res!!}
                                                            </td>
                                                            @endif
                                                        </tr>
                                                        @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        @endif
                                        @endfor
                                        @endif

                                        @foreach ($doctor_assessment as $each)
                                        @php
                                        $notesdata_text=@$each->data_text ? json_decode($each->data_text,true) :
                                        array();
                                        @endphp
                                        @foreach ($notesdata_text as $key=>$val)
                                        @if(trim($val))
                                        <tr>
                                            <td colspan="2">
                                                @php
                                                $assisment_name= @$EmrAssessmentHead[$key] ? $EmrAssessmentHead[$key]
                                                :'';
                                                @endphp
                                                <i>{{ $assisment_name }}</i> : {!!$val!!}
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                        @endforeach


                                        @if ($gynec_count > 0)
                                            <tr>
                                                <td valign="top">
                                                    <table class="table patientCombinedHistoryTable no-margin   " style="margin-bottom:4px;">
                                                        <tbody>

                                                            <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                                                <td colspan="2"><b>Gynecology Details</b></td>
                                                            </tr>


                                                            @for ($m = 0; $m < $gynec_count; $m++) @php
                                                                $created_at=!empty($gynecology_template[$m]->created_at) ?
                                                                $gynecology_template[$m]->created_at : '';
                                                                $doctor_name = !empty($gynecology_template[$m]->doctor_name) ?
                                                                $gynecology_template[$m]->doctor_name : '';
                                                                $gynec_data = !empty($gynecology_template[$m]->gynec_data) ?
                                                                $gynecology_template[$m]->gynec_data : '';

                                                                @endphp
                                                                @if ($gynec_data != '')
                                                                <tr>
                                                                    <td>Doctor:</td>
                                                                    <td>{{ $doctor_name }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Lmp:</td>
                                                                    <td>
                                                                        @if ($gynec_data[0]->lmp != '')
                                                                        {!! $gynec_data[0]->lmp !!}
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Pmp:</td>
                                                                    <td>
                                                                        @if ($gynec_data[0]->pmp != '')
                                                                        {!! $gynec_data[0]->pmp !!}
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Pmp:</td>
                                                                    <td>
                                                                        @if ($gynec_data[0]->pmp != '')
                                                                        {!! $gynec_data[0]->pmp !!}
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Presenting Complaints:</td>
                                                                    <td>
                                                                        {!! $gynec_data[0]->presenting_complaints !!}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Local Examinations:</td>
                                                                    <td>
                                                                        {!! $gynec_data[0]->local_examinations !!}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>USG:</td>
                                                                    <td>
                                                                        {!! $gynec_data[0]->usg !!}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Provisional Diagnosis:</td>
                                                                    <td>
                                                                        {!! $gynec_data[0]->provisional_diagnosis !!}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Plan Of Care:</td>
                                                                    <td>
                                                                        {!! $gynec_data[0]->plan_of_care !!}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Follow Up Care:</td>
                                                                    <td>
                                                                        {!! $gynec_data[0]->follow_up_care !!}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Special Care:</td>
                                                                    <td>
                                                                        {!! $gynec_data[0]->special_care !!}
                                                                    </td>
                                                                </tr>
                                                                @endif
                                                                @endfor
                                                        </tbody>
                                                    </table>

                                                </td>
                                            </tr>
                                        @endif

                                        @if($gynec_usg_count > 0)
                                            <tr>
                                                <td valign="top">
                                                    <table class="table patientCombinedHistoryTable no-margin   " style="margin-bottom:4px;">
                                                        <tbody>
                                                            <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                                                <td colspan="2"><b>Gynecology USG Result Entry</b></td>
                                                            </tr>
                                                            @for ($m = 0; $m < $gynec_usg_count; $m++)
                                                                @php
                                                                    $created_at=!empty($gynecology_usg_result[$m]->created_at) ?
                                                                    $gynecology_usg_result[$m]->created_at : '';
                                                                    $doctor_name = !empty($gynecology_usg_result[$m]->doctor_name) ?
                                                                    $gynecology_usg_result[$m]->doctor_name : '';
                                                                @endphp


                                                                <tr>
                                                                    <td>Usg taken at : </td>
                                                                    <td>
                                                                        @if($gynecology_usg_result[$m]->usg_results_entry_date !='')
                                                                            {{date('M-d-Y',strtotime($gynecology_usg_result[$m]->usg_results_entry_date))}}
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>POG : </td>
                                                                    <td>
                                                                        @if($gynecology_usg_result[$m]->pog_wk !='')
                                                                            {{$gynecology_usg_result[$m]->pog_wk}} Weeks
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                @if($gynecology_usg_result[$m]->siugs !='')
                                                                    <tr>
                                                                        <td>SIUGS : </td>
                                                                        <td>
                                                                            {{$gynecology_usg_result[$m]->siugs}} Weeks
                                                                        </td>
                                                                    </tr>
                                                                @endif

                                                                @if($gynecology_usg_result[$m]->sliug !='')
                                                                <tr>
                                                                    <td>SLIUG : </td>
                                                                    <td>
                                                                        {{$gynecology_usg_result[$m]->sliug}} Weeks
                                                                    </td>
                                                                </tr>
                                                                @endif

                                                                @if($gynecology_usg_result[$m]->slf !='')
                                                                <tr>
                                                                    <td>SLF : </td>
                                                                    <td>
                                                                        {{$gynecology_usg_result[$m]->slf}} Weeks
                                                                    </td>
                                                                </tr>
                                                                @endif

                                                                @if($gynecology_usg_result[$m]->ceph !='')
                                                                <tr>
                                                                    <td  colspan="2">CEPH </td>

                                                                </tr>
                                                                @endif
                                                                @if($gynecology_usg_result[$m]->breech !='')
                                                                <tr>
                                                                    <td  colspan="2">BREECH </td>

                                                                </tr>
                                                                @endif
                                                                @if($gynecology_usg_result[$m]->transverse_lie !='')
                                                                <tr>
                                                                    <td colspan="2">Transverse lie</td>

                                                                </tr>
                                                                @endif
                                                                <tr>
                                                                    <td>AFI : </td>
                                                                    <td>{{$gynecology_usg_result[$m]->afi}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>EFW : </td>
                                                                    <td>{{$gynecology_usg_result[$m]->efw}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>EDD </td>
                                                                    <td>{{$gynecology_usg_result[$m]->edd}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Placenta </td>
                                                                    <td>{{$gynecology_usg_result[$m]->placenta}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>NT </td>
                                                                    <td>{{$gynecology_usg_result[$m]->nt}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>FHR </td>
                                                                    <td>{{$gynecology_usg_result[$m]->fhr}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>NB </td>
                                                                    <td>{{$gynecology_usg_result[$m]->nb}}</td>
                                                                </tr>
                                                            @endfor
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        @endif


                                        @if ($antinatal_count > 0 && !empty($antinatal_template))
                                            <tr>
                                                <td valign="top">
                                                    <table class="table patientCombinedHistoryTable no-margin   " style="margin-bottom:4px;">
                                                        <tbody>


                                                            <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                                                <td colspan="8"><b>Antenatal</b></td>
                                                            </tr>
                                                            <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                                                <td><b>Date</b></td>
                                                                <td><b>GA</b></td>
                                                                <td><b>BP</b></td>
                                                                <td><b>Wt</b></td>
                                                                <td><b>F.Ht</b></td>
                                                                <td><b>FHS</b></td>
                                                                <td><b>Present</b></td>
                                                                <td><b>Remarks</b></td>
                                                            </tr>
                                                            @php

                                                            @endphp

                                                            @for ($m = 0; $m < $antinatal_count; $m++)
                                                                <tr>
                                                                    <td>{{ date('M-d-Y', strtotime($antinatal_template[$m]->antinatal_data[0]->antinatal_date)) }}
                                                                    </td>
                                                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->ga }}</td>
                                                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->bp }}</td>
                                                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->wt }}</td>
                                                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->fht }}</td>
                                                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->fhs }}</td>
                                                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->present }}</td>
                                                                    <td>{{ $antinatal_template[$m]->antinatal_data[0]->antinatal_note }}</td>
                                                                </tr>
                                                            @endfor

                                                        </tbody>
                                                    </table>

                                                </td>
                                            </tr>
                                        @endif


                                        @if ($antinatal_clinical_count > 0 && !empty($antinatal_clinical_data))
                                        @php
                                            $antinatal_clinical_data = $antinatal_clinical_data[0];
                                        @endphp
                                        <tr>
                                            <td valign="top">
                                                <table class="table patientCombinedHistoryTable no-margin  antinatal_clinical_data_table  " style="margin-bottom:4px;">
                                                    <tbody>
                                                        <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                                            <td colspan="9"><b>Antenatal Clinical Data</b></td>
                                                        </tr>


                                                        <tr>
                                                            <td class="label_class">
                                                                <span >Parity :</span>
                                                            </td>
                                                            <td colspan="8" class="value_class">
                                                                {{$antinatal_clinical_data->parity}}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label_class">
                                                                <span >TT :</span>
                                                            </td>
                                                            <td  class="label_class">
                                                                <label>1st :</label>
                                                                @if($antinatal_clinical_data->tt_1 == 1)
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                                @endif
                                                            </td>
                                                            <td  class="label_class">
                                                                <label>2nd :</label>
                                                                @if($antinatal_clinical_data->tt_2 == 1)
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                                @endif
                                                            </td>
                                                            <td class="label_class">
                                                                <span >HB :</span>
                                                            </td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->hb}}
                                                            </td>
                                                            <td class="label_class">
                                                                <span>Ht :</span>
                                                            </td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->ht}}
                                                            </td>
                                                            <td class="label_class">
                                                                <span>H/o Allergy :</span>
                                                            </td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->ho_allergy}}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label_class">
                                                                <span >BG</span>
                                                                <span  class="value_class">{{$antinatal_clinical_data->blood_group}}</span>
                                                            </td>
                                                            <td class="label_class" colspan="2">
                                                                <span >Rh Type</span>
                                                                <span  class="value_class">{{$antinatal_clinical_data->rh_type}}</span>
                                                            </td>
                                                            <td class="label_class">
                                                                <span >PCV</span>
                                                            </td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->rh_type}}

                                                            </td>
                                                            <td class="label_class">
                                                                <span >MH</span>
                                                            </td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->mh}}

                                                            </td>
                                                            <td class="label_class" rowspan="5" colspan="2">
                                                                <label ><b>Usg Report</b></label><br>
                                                                <span  class="value_class">
                                                                    @if($antinatal_clinical_data->usg_date !='')
                                                                        {{date('M-d-Y',strtotime($antinatal_clinical_data->usg_date))}}<br>
                                                                    @endif
                                                                    {{$antinatal_clinical_data->usg}}
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label_class">
                                                            bloodgroup
                                                            </td>
                                                            <td colspan="2"  class="value_class">
                                                                {{$antinatal_clinical_data->blood_group}} {{$antinatal_clinical_data->rh_type }}
                                                            </td>
                                                            <td class="label_class">
                                                                <span >Plt</span>
                                                            </td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->plt}}
                                                            </td>
                                                            <td class="label_class">
                                                                <span >FH</span>
                                                            </td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->fh}}
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td class="label_class">
                                                                <span >HIV</span>
                                                            </td>
                                                            <td colspan="2"  class="value_class">
                                                                @if($antinatal_clinical_data->hiv !='')
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                                @endif
                                                            </td>
                                                            <td class="label_class">
                                                                <span >RBS</span>
                                                            </td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->rbs}}
                                                            </td>
                                                            <td class="label_class">
                                                                <span >CVS</span>
                                                            </td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->cvs}}
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td class="label_class">
                                                                <span >HCV</span>
                                                            </td>
                                                            <td colspan="2"  class="value_class">
                                                                {{$antinatal_clinical_data->hcv}}
                                                            </td>
                                                            <td class="label_class">
                                                                <span >UrineRIE</span>
                                                            </td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->urine_rie}}
                                                            </td>
                                                            <td  class="label_class">
                                                                <span>Rs</span>
                                                            </td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->rs}}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label_class"><span >HBsAg</span></td>
                                                            <td colspan="2"  class="value_class">
                                                                {{$antinatal_clinical_data->hbsag}}
                                                            </td>

                                                            <td class="label_class"><span >VDRL</span></td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->vdrl}}
                                                            </td>

                                                            <td class="label_class"><span >TSH</span></td>
                                                            <td  class="value_class">
                                                                {{$antinatal_clinical_data->tsh}}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label_class" rowspan="2">
                                                                <span>Personal History</span>
                                                            </td>
                                                            <td colspan='2' rowspan="2"  class="value_class">
                                                                {{$antinatal_clinical_data->vdrl}}
                                                            </td>
                                                            <td class="label_class" rowspan="2">
                                                                <span>Professional Details</span>
                                                            </td>
                                                            <td colspan='3'>
                                                                <span><i>Husband</i></span><br>
                                                            </td>
                                                            <td colspan='2'>
                                                                <span><i>Wife</i></span><br>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='3'><span  class="value_class">{{$antinatal_clinical_data->profession_husband}}<br></span></td>
                                                            <td colspan='2'><span  class="value_class">{{$antinatal_clinical_data->profession_wife}}</span></td>
                                                        </tr>
                                                        <tr></tr>

                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        @endif


                                        @if($gynec_obstretics_count > 0)
                                            <tr>
                                                <td valign="top">
                                                    <table class="table patientCombinedHistoryTable no-margin" style="margin-bottom:4px;width:100%;">

                                                        <tbody>
                                                            <tr class="border_dashed_bottom" style="background-color:ebfaef;">
                                                                <td colspan="2"><b>Antinatal Obstretics History</b></td>
                                                            </tr>

                                                            @for ($m = 0; $m < $gynec_obstretics_count; $m++)
                                                                @php
                                                                    $created_at=!empty($gynec_obstretics_count[$m]->created_at) ?
                                                                    $gynec_obstretics_count[$m]->created_at : '';
                                                                    $doctor_name = !empty($gynec_obstretics_count[$m]->doctor_name) ?
                                                                    $gynec_obstretics_count[$m]->doctor_name : '';
                                                                @endphp

                                                                @if($gynecology_obstretics_history[$m]->delevery_type1 !='')
                                                                    <tr>
                                                                        <td colspan="2"><b>Child-1</b></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Delivery Type : </td>
                                                                        <td>
                                                                            @if($gynecology_obstretics_history[$m]->delevery_type1 == 1)
                                                                            FTND
                                                                            @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 2)
                                                                            LSCS
                                                                            @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 3)
                                                                            AB
                                                                            @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 4)
                                                                            ECT
                                                                            @elseif($gynecology_obstretics_history[$m]->delevery_type1 == 5)
                                                                            MTP
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                    @if(isset($gynecology_obstretics_history[$m]->gender1))
                                                                    <tr>
                                                                        <td>Gender : </td>
                                                                        <td>
                                                                            @if($gynecology_obstretics_history[$m]->gender1 == 1)
                                                                            Girl
                                                                            @else
                                                                            Boy
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                    @endif
                                                                    @if(isset($gynecology_obstretics_history[$m]->birth_weight1))
                                                                    <tr>
                                                                        <td>Birth Weight : </td>
                                                                        <td>
                                                                            {{$gynecology_obstretics_history[$m]->birth_weight1 }}
                                                                        </td>
                                                                    </tr>
                                                                    @endif
                                                                    @if(isset($gynecology_obstretics_history[$m]->age1))
                                                                    <tr>
                                                                        <td>Age : </td>
                                                                        <td>
                                                                            {{$gynecology_obstretics_history[$m]->age1 }}
                                                                        </td>
                                                                    </tr>
                                                                    @endif
                                                                    @if(isset($gynecology_obstretics_history[$m]->place_of_birth1))
                                                                    <tr>
                                                                        <td>Place of Birth : </td>
                                                                        <td>
                                                                            {{$gynecology_obstretics_history[$m]->place_of_birth1 }}
                                                                        </td>
                                                                    </tr>
                                                                    @endif
                                                                    @if(isset($gynecology_obstretics_history[$m]->any_complications1))
                                                                    <tr>
                                                                        <td>Any Complications : </td>
                                                                        <td>
                                                                            {{$gynecology_obstretics_history[$m]->any_complications1 }}
                                                                        </td>
                                                                    </tr>
                                                                    @endif

                                                                @endif

                                                                @if($gynecology_obstretics_history[$m]->delevery_type2 !='')
                                                                    <tr>
                                                                        <td colspan="2"><b>Child-2</b></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Delivery Type : </td>
                                                                        <td>
                                                                            @if($gynecology_obstretics_history[$m]->delevery_type2 == 1)
                                                                            FTND
                                                                            @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 2)
                                                                            LSCS
                                                                            @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 3)
                                                                            AB
                                                                            @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 4)
                                                                            ECT
                                                                            @elseif($gynecology_obstretics_history[$m]->delevery_type2 == 5)
                                                                            MTP
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                    @if(isset($gynecology_obstretics_history[$m]->gender2))
                                                                    <tr>
                                                                        <td>Gender : </td>
                                                                        <td>
                                                                            @if($gynecology_obstretics_history[$m]->gender2 == 1)
                                                                            Girl
                                                                            @else
                                                                            Boy
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                    @endif
                                                                    @if(isset($gynecology_obstretics_history[$m]->birth_weight2))
                                                                    <tr>
                                                                        <td>Birth Weight : </td>
                                                                        <td>
                                                                            {{$gynecology_obstretics_history[$m]->birth_weight2 }}
                                                                        </td>
                                                                    </tr>
                                                                    @endif
                                                                    @if(isset($gynecology_obstretics_history[$m]->age2))
                                                                    <tr>
                                                                        <td>Age : </td>
                                                                        <td>
                                                                            {{$gynecology_obstretics_history[$m]->age2 }}
                                                                        </td>
                                                                    </tr>
                                                                    @endif
                                                                    @if(isset($gynecology_obstretics_history[$m]->place_of_birth2))
                                                                    <tr>
                                                                        <td>Place of Birth : </td>
                                                                        <td>
                                                                            {{$gynecology_obstretics_history[$m]->place_of_birth2 }}
                                                                        </td>
                                                                    </tr>
                                                                    @endif
                                                                    @if(isset($gynecology_obstretics_history[$m]->any_complications2))
                                                                    <tr>
                                                                        <td>Any Complications : </td>
                                                                        <td>
                                                                            {{$gynecology_obstretics_history[$m]->any_complications2 }}
                                                                        </td>
                                                                    </tr>
                                                                    @endif
                                                                @endif
                                                                @if($gynecology_obstretics_history[$m]->additional_child_information !='')
                                                                    @php
                                                                        $additional_child_information = json_decode($gynecology_obstretics_history[$m]->additional_child_information);
                                                                        // dd($additional_child_information[1]);
                                                                    @endphp

                                                                    @if(sizeof($additional_child_information) > 0)
                                                                        @php
                                                                            $additional_child_count = 3;
                                                                        @endphp
                                                                        @for($n = 0; $n < sizeof($additional_child_information); $n++)

                                                                        <tr>
                                                                            <td colspan="2"><b>Child-{{$additional_child_count}}</b></td>
                                                                        </tr>
                                                                        @if(isset($additional_child_information[$n]->delevery_type))
                                                                        <tr>
                                                                            <td>Delivery Type : </td>
                                                                            <td>
                                                                                @if($additional_child_information[$n]->delevery_type == 1)
                                                                                FTND
                                                                                @elseif($additional_child_information[$n]->delevery_type == 2)
                                                                                LSCS
                                                                                @elseif($additional_child_information[$n]->delevery_type == 3)
                                                                                AB
                                                                                @elseif($additional_child_information[$n]->delevery_type == 4)
                                                                                ECT
                                                                                @elseif($additional_child_information[$n]->delevery_type == 5)
                                                                                MTP
                                                                                @endif
                                                                            </td>
                                                                        </tr>
                                                                        @endif
                                                                        @if(isset($additional_child_information[$n]->gender))
                                                                        <tr>
                                                                            <td>Gender : </td>
                                                                            <td>
                                                                                @if($additional_child_information[$n]->gender == 1)
                                                                                Girl
                                                                                @else
                                                                                Boy
                                                                                @endif
                                                                            </td>
                                                                        </tr>
                                                                        @endif
                                                                        <tr>
                                                                            <td>Birth Weight : </td>
                                                                            <td>
                                                                                {{$additional_child_information[$n]->birth_weight }}
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Age : </td>
                                                                            <td>
                                                                                {{$additional_child_information[$n]->age }}
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Place of Birth : </td>
                                                                            <td>
                                                                                {{$additional_child_information[$n]->place_of_birth }}
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Any Complications : </td>
                                                                            <td>
                                                                                {{$additional_child_information[$n]->any_complications }}
                                                                            </td>
                                                                        </tr>
                                                                        @php
                                                                            $additional_child_count++;
                                                                        @endphp

                                                                        @endfor
                                                                    @endif

                                                                @endif


                                                            @endfor
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        @endif



                                </tbody>
                            </table>
                        </td>

                        <td width="45%" valign="top">
                            <table class="table no-margin table-striped  " style="margin-bottom:4px;">
                                <tbody>
                                    <tr style="background: #92979b7d;">
                                        <td colspan="2"><b style="color:#3c3434;">Medicine</b></td>
                                    </tr>
                                    @php

                                    @endphp
                                    @if($med_count > 0)
                                    @for ($m = 0; $m < $med_count; $m++) @php $head_id=(!empty($patient_medication[$m]->
                                        head_id)) ? $patient_medication[$m]->head_id : 0;
                                        $medicine_code = (!empty($patient_medication[$m]->medicine_code)) ?
                                        $patient_medication[$m]->medicine_code : "";
                                        $item_desc = (!empty($patient_medication[$m]->item_desc)) ?
                                        $patient_medication[$m]->item_desc : "";
                                        $duration = (!empty($patient_medication[$m]->duration)) ?
                                        $patient_medication[$m]->duration : 0;
                                        $frequency = (!empty($patient_medication[$m]->frequency)) ?
                                        $patient_medication[$m]->frequency : "";
                                        $quantity = (!empty($patient_medication[$m]->quantity)) ?
                                        $patient_medication[$m]->quantity : 0;
                                        $notes = (!empty($patient_medication[$m]->notes)) ?
                                        $patient_medication[$m]->notes :
                                        "";
                                        @endphp
                                        <tr>
                                            <td colspan="2">
                                                <b>{!!$item_desc!!} ({!!$duration!!} Days / {!!$frequency!!})</b>
                                                @if(!empty($notes))
                                                <br>
                                                <i>Remarks : {!!$notes!!} </i><br>
                                                @endif
                                            </td>
                                        </tr>
                                        @endfor
                                        @else
                                        <tr>
                                            <td colspan="2">No Data Found..</td>
                                        </tr>
                                        @endif
                                </tbody>
                            </table>
                        </td>

                        <td width="20%" valign="top">
                            <table class="table no-margin table-striped  ">
                                <tbody>
                                    <tr style="background: #92979b7d;">
                                        <td><b style="color:#3c3434;">Vital</b></td>
                                        <td><b style="color:#3c3434;">Value</b></td>
                                    </tr>
                                    @if(intval($vitals_count) > 0)
                                    @php $time_taken1 = ''; @endphp
                                    @for ($v = 0; $v < $vitals_count; $v++) @php $name=(!empty($patient_vitals[$v]->
                                        vital_name)) ? $patient_vitals[$v]->vital_name : 0;
                                        $value = (!empty($patient_vitals[$v]->vital_value)) ?
                                        $patient_vitals[$v]->vital_value : 0;
                                        $min_value = (!empty($patient_vitals[$v]->min_value)) ?
                                        $patient_vitals[$v]->min_value : 0;
                                        $max_value = (!empty($patient_vitals[$v]->max_value)) ?
                                        $patient_vitals[$v]->max_value : 0;
                                        $time_taken = (!empty($patient_vitals[$v]->time_taken)) ?
                                        $patient_vitals[$v]->time_taken : 0;
                                        $time_taken = date('M-d-Y h:i A',strtotime($time_taken));
                                        @endphp
                                        @if($patient_vitals[$v]->vital_master_id == 5)
                                        @php $bp_sis = $value @endphp
                                        @elseif($patient_vitals[$v]->vital_master_id == 6)
                                        @php $bp_dia = $value @endphp
                                        @else
                                        @if($time_taken != $time_taken1)
                                        <tr class="bg-green">
                                            <td colspan="2">Take at : {{$time_taken}}</td>
                                        </tr>
                                        @endif

                                        <tr>
                                            <td>{{$name}}</td>
                                            <td>{{$value}}</td>
                                        </tr>
                                        @php
                                        $time_taken1 = $time_taken;
                                        @endphp
                                        @endif
                                        @endfor
                                        <tr>
                                            <td>BP</td>
                                            <td>
                                                @if(isset($bp_sis)){{$bp_sis}} @endif /
                                                @if(isset($bp_dia)){{$bp_dia}}
                                                @endif


                                            </td>
                                        </tr>
                                        @else
                                        <tr>
                                            <td colspan="2">No Data Found..</td>
                                        </tr>
                                        @endif

                                </tbody>
                            </table>
                        </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table class="table no-margin table-striped " style="width:100%;">
                                    <tbody>
                                        <tr style="background: #92979b7d;">
                                            <td colspan="5"><b style="color:#3c3434;">Investigation</b></td>
                                        </tr>
                                        <tr style="background: #dfe3e7 !important;color: #3c3434">
                                            <td><b>Test</b></td>
                                            <td><b>Description</b></td>
                                            <td><b>Value</b></td>
                                            <td><b>Normal Range</b></td>
                                            <td><b>Remarks</b></td>
                                        </tr>
                                        @if($inv_count > 0)
                                        @php $service_desc = "";
                                        $current_date = date('M-d-Y', strtotime($current_date));
                                        if(is_string($emr_changes)){
                                            $emr_changes=json_decode($emr_changes,true);
                                        }
                                        $investigation_future_date = @$emr_changes['investigation_future_date'] ? $emr_changes['investigation_future_date'] : 0;
                                        @endphp
                                        @for ($c = 0; $c < $inv_count; $c++) @php
                                            $head_id=(!empty($patient_invsestigation[$c]->
                                            head_id)) ? $patient_invsestigation[$c]->head_id : 0;
                                            $min = "";
                                            $max = "";
                                            $style = "";
                                            $direction = "";
                                            $html_content = "";
                                            if($service_desc != $patient_invsestigation[$c]->service_desc){
                                            $service_desc = $patient_invsestigation[$c]->service_desc;
                                            }else{
                                            $service_desc = '';
                                            }
                                            if($investigation_future_date == 1){
                                                $service_date = !empty($patient_invsestigation[$c]->service_date) ? $patient_invsestigation[$c]->service_date : '';
                                            }

                                            $investigation_type =
                                            (!empty($patient_invsestigation[$c]->investigation_type)) ?
                                            $patient_invsestigation[$c]->investigation_type : 0;

                                            $quantity = (!empty($patient_invsestigation[$c]->quantity)) ?
                                            $patient_invsestigation[$c]->quantity : 0;

                                            $sub_test_name = (!empty($patient_invsestigation[$c]->detail_description)) ?
                                            $patient_invsestigation[$c]->detail_description : '-';

                                            $actual_result = (!empty($patient_invsestigation[$c]->actual_result)) ?
                                            $patient_invsestigation[$c]->actual_result : '-';

                                            $normal_range = (!empty($patient_invsestigation[$c]->normal_range)) ?
                                            $patient_invsestigation[$c]->normal_range : '-';

                                            $remarks = (!empty($patient_invsestigation[$c]->remarks)) ?
                                            $patient_invsestigation[$c]->remarks : '-';

                                            if (!empty($patient_invsestigation[$c]->min_value) &&
                                            is_numeric($patient_invsestigation[$c]->actual_result) &&
                                            $patient_invsestigation[$c]->max_value != "-")
                                            {

                                            if (!empty($patient_invsestigation[$c]->min_value)) {
                                            $min = $patient_invsestigation[$c]->min_value;
                                            }

                                            if (!empty($patient_invsestigation[$c]->max_value)) {
                                            $max = $patient_invsestigation[$c]->max_value;
                                            }

                                            if($max < $patient_invsestigation[$c]->actual_result &&
                                                !empty($patient_invsestigation[$c]->actual_result)){
                                                $style = "style=color:red;";
                                                $direction = '&nbsp;<i class="fa fa-arrow-up"></i>';
                                                }

                                                if($min > $patient_invsestigation[$c]->actual_result &&
                                                !empty($patient_invsestigation[$c]->actual_result)){
                                                $style = "style=color:#FF7701;";
                                                $direction = '&nbsp;<i class="fa fa-arrow-down"></i>';
                                                }
                                                }


                                                if($patient_invsestigation[$c]->report_type == 'T'){
                                                $html_content = "<button class='btn bg-primary' type='button'
                                                    onclick='getLabResultRtf(".$patient_invsestigation[$c]->labresult_id.")'><i
                                                        class='fa fa-list'></i></button>";
                                                }else if($patient_invsestigation[$c]->report_type == 'N' ||
                                                $patient_invsestigation[$c]->report_type == 'F'){
                                                $html_content = "<div class='text-left'".$style.">
                                                    ".$patient_invsestigation[$c]->actual_result. $direction."</div>";
                                                }else{
                                                $html_content = "";
                                                }


                                                $result_finalized =
                                                (!empty($patient_invsestigation[$c]->result_finalized)) ?
                                                $patient_invsestigation[$c]->result_finalized : 0;
                                                if($result_finalized == 0){
                                                $html_content = "";
                                                }


                                                @endphp
                                                 @php
                                                 $style = '';
                                                 $service_date1 = '';
                                                 if($investigation_future_date == 1 && $service_date > $current_date){
                                                     $service_date1 = '('.$service_date.')';
                                                     $style = 'style=color:#7383d1;';
                                                 }
                                                @endphp
                                                <tr>
                                                    <td {{ $style }}><b>{!!$service_desc!!}{{ $service_date1 }}</b></td>
                                                    <td>
                                                        <div class="text-left text_limit">{{$sub_test_name}}</div>
                                                    </td>
                                                    <td>{!!$html_content!!}</td>
                                                    <td>{!!$normal_range!!}</td>
                                                    <td>{!!$remarks!!}</td>
                                                </tr>
                                                @php
                                                $service_desc = $patient_invsestigation[$c]->service_desc;
                                                @endphp

                                                @endfor
                                                @else
                                                <tr>
                                                    <td colspan="4">No Data Found..</td>
                                                </tr>
                                                @endif

                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <table class="table no-margin table-striped " style="width:100%;">
                                    <tbody>
                                        <tr style="background: #92979b7d;">
                                            <td colspan="5"><b style="color:#3c3434;">Investigation Result Entry</b></td>
                                        </tr>
                                        <tr style="background: #dfe3e7 !important;color: #3c3434">
                                            <td><b>Date</b></td>
                                            <td><b>Investigation</b></td>
                                            <td><b>Result</b></td>
                                        </tr>

                                        @if(count($invsetigation_result_entry)!=0)
                                        @foreach ($invsetigation_result_entry as $each)
                                        <tr>
                                            <td>{{ $each->investiagation_date }}</td>
                                            <td>{{ $each->investiagation }}</td>
                                            <td>{{ $each->investiagation_result }}</td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="3">No Result Found</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        @php
                        $i++;
                        @endphp
                        @endif
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
