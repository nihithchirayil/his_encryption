
<div class="box no-margin no-border">
    <div class="col-md-12 padding_sm">
        <div class="box no-border no-margin anim">
            <div class="box-body clearfix">
                <div class="col-md-2 padding_sm">
                    <div class="" data-toggle="buttons" style="margin-top:24px;">
                        <label class=" " style="margin-right:4px;">
                            <input type="radio" id="patient_name_search_radio_radio" value="1" name="search_type_radio" checked="checked"> Name
                        </label>
                        <label class=" " style="margin-right:4px;">
                            <input type="radio" id="uhid_search_radio_radio" value="2" name="search_type_radio" checked=""> UHID
                        </label>
                        <label class=" active">
                            <input type="radio" id="patient_phone_search_radio_radio" value="3" name="search_type_radio" checked="checked"> Phone
                        </label>
                    </div>
                </div>
                <div class="col-md-2 padding_sm">

                    <label for="">Search Patients</label>
                    <div class="clearfix"></div>
                    <div class="input-group">
                        <input type="text" id="patient_search_txt_radio" onkeyup="search_patient_radio();" class="form-control">
                        <div class="input-group-btn">
                            <button style="height: 23px; width: 30px;" class="btn light_purple_bg" onclick="search_patient_radio();"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 padding_sm">
                    <label for="">Search By Date</label>
                    <div class="clearfix"></div>
                    <div class="input-group">
                        <input type="text" id="op_patient_search_date_radio" onblur="getRadiologyPatientList();" class="form-control datepicker" value="{{date('M-d-Y')}}" data-attr="date" placeholder="Date">
                        <div class="input-group-btn">
                            <button style="height: 23px; width: 30px;" class="btn light_purple_bg">
                                <i class="fa fa-search" onclick="getRadiologyPatientList();"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 padding_sm radiology_patient_list_table_container">
        <div class="box no-border no-margin anim" style="min-height:360px;">
            <div class="box-body clearfix">
                <table class="table table-bordered no-margin table_sm no-border theadfix_wrapper">
                    <thead>
                        <tr class="table_header_bg">
                            <th>SL.No.</th>
                            <th>UHID</th>
                            <th>Patient Name</th>
                            <th>Age/Gender</th>
                            <th>Visit Type</th>
                            <th>Phone</th>
                        </tr>
                    </thead>
                    <tbody class="health_package_list_table_body">
                        @php $sl = 1; @endphp
                        @isset($op_patients)
                        @if(count($op_patients) > 0)
                        @foreach($op_patients as $rd)
                        <tr class="patient_det_widget_radio" data-patientname="{{$rd->patient_name}}" data-uhid="{{$rd->uhid}}" data-phone="{{$rd->mobile_no}}" onclick="goToSelectedPatient('{{$rd->patient_id}}');" style="cursor:pointer;" >
                <td>{{$sl}}</td>
                <td>{{$rd->uhid}}</td>
                <td>{{$rd->patient_name}}</td>
                <td>{{$rd->age}}/{{$rd->gender}}</td>
                <td>{{$rd->visit_type}}</td>
                <td>{{$rd->mobile_no}}</td>
                        </tr>
                        @php $sl++; @endphp
                        @endforeach
                        @else
                        <tr>
                            <td colspan="8">No records found..!</td>
                        </tr>
                        @endif
                        @endisset
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>
