
        <div class="theadscroll always-visible" style="position: relative; height: 310px;">
            @if(sizeof($res)>0)
            <table class="table table_sm table-bordered theadfix_wrapper">
                <thead>
                    <tr class="table_header_bg">
                        <th width="10%">Sl No</th>
                        <th width="15%">Admit Date</th>
                        <th width="30%">Admitting Doctor</th>
                        <th width="15%">Discharge Date</th>
                        <th width="15%">Summary Prepared At</th>
                        <th width="15%">Summary</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i =1;
                    @endphp
                    @foreach($res as $data)

                        <tr>
                            <td >{{$i}}</td>
                            <td >{{date('M-d-Y',strtotime($data->admit_date))}}</td>
                            <td >{{$data->doctor_name}}</td>
                            <td >
                                @if($data->discharge_date !='')
                                    {{date('M-d-Y',strtotime($data->discharge_date))}}
                                @endif
                            </td>
                            <td >{{date('M-d-Y',strtotime($data->last_updated_at))}}</td>
                            <td style="text-align:center">
                                @if($data->final_status !=0)
                                    <button onclick="showSummary('{{$data->id}}');" class="btn bg-primary">
                                        <i class="fa fa-list"></i>
                                    </button>
                                @endif

                            </td>

                        </tr>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
            @else
            <span>No Summary found </span>
            @endif
             <button type="button" style="margin-left:880px;margin-top:-30px" onclick="createDischargeSummary()" class="btn btn-success ">Create</button>
        </div>
