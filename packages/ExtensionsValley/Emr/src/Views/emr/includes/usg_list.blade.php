<div class="col-md-12 box-body">
    <div class="col-md-12" style="background: rgb(86,200,242);
    background: radial-gradient(circle, rgba(86,200,242,1) 0%, rgba(6,113,160,1) 100%);color:white;">
        <h5>Reports Verification</h5>
    </div>
    <input name="token" type="hidden" class="form-control" id="c_token" value="{{csrf_token()}}">
    <input type="hidden" name="base_url" id="base_url" value="{{url('/')}}"/>
    <div class="col-md-12" style="margin-top:15px;">
        <div class="col-md-2 padding_sm ">
            <label for="">From Date</label>
            <div class="clearfix"></div>
            <div class="input-group">
                <input type="text" id="usg_from_date" class="form-control datepicker" value="{{date('M-d-Y')}}" data-attr="date" placeholder="Date" style="height:34px !important;border-radius:3px;">
            </div>
        </div>

        <div class="col-md-2 padding_sm ">
            <label for="">To Date</label>
            <div class="clearfix"></div>
            <div class="input-group">
                <input type="text" id="usg_to_date" class="form-control datepicker" value="{{date('M-d-Y')}}" data-attr="date" placeholder="Date" style="height:34px !important;border-radius:3px;">
            </div>
        </div>
        <div class="col-md-4 padding_sm ">
            <label for="">Patient Search</label>
            <div class="clearfix"></div>

            <input type="text" id="usg_patient_search_txt" onkeyup="usg_patient_search(this.value);" class="form-control" value="" style="height:34px !important;border-radius:3px;">
            <input type="hidden" id="usg_patient_search_hidden" value="">

            <div style="width:300px; display: none;z-index: 9999; min-height: 250px;" id="usgpatientbox" class="ajaxSearchBox">
            </div>
        </div>
        <div class="col-md-2 padding_sm pull-right" style="padding-top:25px !important;">
            <div class="clearfix"></div>
            <button class="btn btn-primary" onclick="usg_report_search();"><i class="fa fa-search"></i> Search</button>
            <button class="btn btn-warning" onclick="usg_report_reset();"><i class="fa fa-repeat"></i> Reset</button>
        </div>
    </div>
    <!---- verification reports list--------------->
    <div class="col-md-12" id="usg_reports_list_container" style="margin-top:5px !important;">

    </div>


</div>


