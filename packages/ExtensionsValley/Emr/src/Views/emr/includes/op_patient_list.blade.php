<div class="slide_box op_slide_box anim" style="width:95%;-webkit-box-shadow: -8px 12px 18px 4px rgba(0,0,0,0.75);
-moz-box-shadow: -8px 12px 18px 4px rgba(0,0,0,0.75);
box-shadow: -8px 12px 18px 4px rgba(0,0,0,0.75);height:580px;">
    <div class="slide_header" style="background: rgb(72, 118, 179)">
        <div class="col-md-8">
            <h4 class="no-margin">OP LIST</h4>
        </div>
        <div class="col-md-4 padding_sm text-right">
            <div class="slide_close_btn">x</div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="slide_body">
        <div class="row">
            <div class="col-md-8">
                <div class="box no-border no-margin anim">
                    <div class="box-body clearfix">
                        <div class="col-md-1 padding_sm">
                            <label for="">Name</label>
                            <div class="clearfix"></div>
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="ip_name_check" value="6" name="searchby" id="searchbyname"
                                    value="6" checked="checked">
                                <label for="ip_name_check"> </label>
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">UHID</label>
                            <div class="clearfix"></div>
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" name="searchby" id="searchbyopno" value="5">
                                <label for="opnumber_checkbox"> </label>
                            </div>

                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">Phone</label>
                            <div class="clearfix"></div>
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" name="searchby" id="searchbyopno" value="9">
                                <label for="phone_checkbox"> </label>
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <label for="">Search</label>
                            <div class="clearfix"></div>
                            <div class="input-group">
                                <input type="text" id="myInput" autocomplete="off"
                                    onkeyup="myFunction('searchby','myInput','myTable');" class="form-control">
                                <div class="input-group-btn">
                                    <button style="height: 22px;" class="btn light_purple_bg"><i
                                            class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <input type="radio" checked="checked" name="serch_by_type" value="0" /><label> Search Starts
                                with</label>
                            <br>
                            <input type="radio" name="serch_by_type" value="1" /><label> Search In Between</label>
                        </div>



                    </div>
                </div>
            </div>
            <div class='col-md-2'>
                <div class="box no-border no-margin anim">
                    <div class="box-body clearfix">

                        <div class="col-md-12 padding_sm" style='min-height: 62px; !important'>
                            <label for="">Search By Date</label>
                            <div class="clearfix"></div>
                            <input type="text" id='op_patient_search_date' class="form-control datepicker"
                                autocomplete="off" data-attr="date" onchange="show_op_patients_list();"
                                placeholder="Date">
                        </div>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="ht10"></div>
            <div class="col-md-12 btn_class">

                <button onclick="applyFilter('all');" class="btn bg-primary pill_button">Today's Visits
                    <span id="allCount" class="badge col-sm-3 no-padding pull-right pill_button_badge">0</span>
                </button>
                <button onclick="applyFilter('follow_up');" class="btn pill_button" style="background-color: {{ \WebConf::getConfig('DashboardColors.follow_up_bg') }} !important;
                 color:{{ \WebConf::getConfig('DashboardColors.follow_up_color') }} !important">Follow up
                    <span id="followUpCount" class="badge col-sm-3 no-padding pull-right pill_button_badge">0</span>
                </button>
                <button onclick="applyFilter('seen');" class="btn pill_button" style="background-color: {{ \WebConf::getConfig('DashboardColors.seen_bg') }} !important;
                color:{{ \WebConf::getConfig('DashboardColors.seen_color') }} !important">Seen
                    <span id="seenCount" class="badge col-sm-3 no-padding pull-right pill_button_badge">0</span>
                </button>
                <button onclick="applyFilter('not_seen');" class="btn pill_button " style="background-color: {{ \WebConf::getConfig('DashboardColors.not_seen_bg') }} !important;
                color:{{ \WebConf::getConfig('DashboardColors.not_seen_color') }} !important">Not seen
                    <span id="notseenCount" class="badge col-sm-3 no-padding pull-right pill_button_badge">0</span>
                </button>
                <button onclick="applyFilter('mlc');" class="btn pill_button" style="background-color: {{ \WebConf::getConfig('DashboardColors.mlc_bg') }} !important;
                color:{{ \WebConf::getConfig('DashboardColors.mlc_color') }} !important">MLC
                    <span id="mlcCount" class="badge col-sm-3 no-padding pull-right pill_button_badge">0</span>
                </button>
                <button onclick="applyFilter('walkin');" class="btn pill_button" style="background-color: {{ \WebConf::getConfig('DashboardColors.walkin_bg') }} !important;
                color:{{ \WebConf::getConfig('DashboardColors.walkin_color') }} !important">Walkin
                    <span id="WalkinCount" class="badge col-sm-3 no-padding pull-right pill_button_badge">0</span>
                </button>
                <span class="btn pill_button"
                    style="background-color:rgba(136, 101, 5, 0.808);color:white;margin-left:125px;width: 160px;">
                    Total Booked Patients <span id="TotalBookedPatientsCount"
                        class="badge col-sm-3 no-padding pull-right pill_button_badge">0</span>
                </span>
            </div>
            <div class="clearfix"></div>
            <div class="ht10"></div>
            <div class="col-md-12">
                <div class="theadscroll" style="position: relative; height: 300px;">
                    <table id="myTable" class="table table-bordered table-striped table_sm no-margin theadfix_wrapper">
                        <thead>
                            <tr class="table_header_bg">
                                <th onclick="sortTable('myTable',0,1)">Sl No</th>
                                <th onclick="sortTable('myTable',0,1)">Token No</th>
                                <th>Call</th>
                                <th>Time</th>
                                <!-- <th onclick="sortTable('myTable',2,2)">Time</th> -->
                                <th onclick="sortTable('myTable',3,2)">Visit Type</th>
                                <th onclick="sortTable('myTable',4,2)">UHID</th>
                                <th onclick="sortTable('myTable',5,2)">Name</th>
                                <th onclick="sortTable('myTable',6,1)">Age/Gender</th>
                                <th onclick="sortTable('myTable',7,2)">Address</th>
                                <th onclick="sortTable('myTable',8,2)">Phone</th>
                                <th onclick="sortTable('myTable',9,1)">Waiting time(Min)</th>
                            </tr>
                        </thead><a>
                            <tbody id='op_patients_list_data'>

                            </tbody>
                        </a>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
