<div class="no-border no-margin">
    <div class="box-body" style="min-height: 85px;border-radius:5px;">
        <table class="table table_sm  no-margin no-border">
            <tbody>
                <tr>
                    <td rowspan="3" style="width:14%;align:left">
                        <div class="user_patient_img pos_rel pop-div">
                            @if($patient_info[0]->patient_image_url !='')
                            @php
                            $img_url=URL::to('/').'/packages/extensionsvalley/patient_profile/'.$patient_info[0]->patient_image_url;
                         @endphp
                                <img class="img-circle user_img" src="{{ $img_url }}" style="height:60px;" alt="" id='poplink'/>
                            @else

                                <div class="img-circle user_img" style="color: darkgray;height: 60px;
                                padding-left: 16px;
                                font-size: 37px;" alt="" id='poplink'>
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </div>

                            @endif
                            <div class="smiley_status">
                                <img src="{{asset("packages/extensionsvalley/dashboard/images/smiley.png")}}" alt="">
                                <img class="hidden" src="{{asset("packages/extensionsvalley/dashboard/images/sad_smiley.png")}}" alt="">
                            </div>
                            <div class="pop-content">
                                @include('Emr::emr.includes.patient_info')
                            </div>


                        </div>

                    </td>

                    <td><b>{{$patient_info[0]->patient_name}}({{$patient_info[0]->age}}/{{$patient_info[0]->gender}})</b> </td>


                </tr>
                <tr>
                    <td><b>UHID :</b>{{$patient_info[0]->uhid}}</td>

                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><b>Last Visited Date :</b>{{date(\WebConf::getConfig('date_format_web'),strtotime($patient_info[0]->last_visit_datetime))}}</td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
