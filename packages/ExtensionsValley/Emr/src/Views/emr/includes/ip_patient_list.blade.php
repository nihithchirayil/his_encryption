<div class="slide_box ip_slide_box anim" style="width:95%;-webkit-box-shadow: -8px 12px 18px 4px rgba(0,0,0,0.75);
-moz-box-shadow: -8px 12px 18px 4px rgba(0,0,0,0.75);
box-shadow: -8px 12px 18px 4px rgba(0,0,0,0.75);height:580px;">
    <div class="slide_header">
        <div class="col-md-8"><h4 class="no-margin">IP LIST</h4></div>
        <div class="col-md-4 padding_sm text-right"><div class="slide_close_btn">x</div></div>
    </div>
    <div class="clearfix"></div>
    <div class="slide_body">
        <div class="row">
            <div class="col-md-12">
                <div class="box no-border no-margin anim">
                    <div class="box-body clearfix" >
                        <div class="col-md-1 padding_sm" style="width: 60px;">
                            <label for="">Name</label>
                            <div class="clearfix"></div>
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="ip_name_check" value="2" name="ip_patientlist_radio" checked="">
                                <label for="ip_name_check"> </label>
                            </div>
                        </div>
                        <div class="col-md-1 padding_sm" style="width: 60px;">
                            <label for="">UHID</label>
                            <div class="clearfix"></div>
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="opnumber_checkbox" value="1" name="ip_patientlist_radio" checked="">
                                <label for="opnumber_checkbox"> </label>
                            </div>
                        </div>
                        <div class="col-md-3 padding_sm">
                            <label for="">Search</label>
                            <div class="clearfix"></div>
                            <div class="input-group">
                                <input onkeyup="iplistSorting('ip_patientlist_table','ip_list_searchtext');" id="ip_list_searchtext" type="text" class="form-control">
                                <div class="input-group-btn">
                                    <button onclick="iplistSorting('ip_patientlist_table','ip_list_searchtext');" style="height: 22px;" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 padding_sm">
                            <input type="radio" checked="checked" name="ip_serch_by_type" value="0"/><label> Search Starts with</label>
                            <br>
                            <input type="radio" name="ip_serch_by_type" value="1"/><label> Search In Between</label>
                        </div>

                        <div class="col-md-2 padding_sm" style="margin-top: 20px;">
                            <button type="button" onclick="getIPPatientList(0)" class="btn btn-primary"> Current Ocuupancy</button>
                         </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">From Date</label>
                            <div class="clearfix"></div>
                            <input type="text" id="iplist_admitted_fromdate" class="form-control datepicker" placeholder="From Date">
                        </div>
                        <div class="col-md-1 padding_sm">
                            <label for="">To Date</label>
                            <div class="clearfix"></div>
                            <input  type="text" id="iplist_admitted_todate" class="form-control datepicker" placeholder="To Date">
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top: 20px;">
                            <button type="button" onclick="getIPPatientList(1)" class="btn btn-primary">Search <i class="fa fa-search"></i></button>
                        </div>




                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="ht10"></div>
            <div class="clearfix"></div>
            <div class="ht10"></div>
            <div class="col-md-12" id="patient_iplist_data">

            </div>
        </div>
    </div>
</div>


<!-- IP slide ends here -->
