
        <div class="theadscroll always-visible" style="position: relative; ">
            @if(sizeof($res)>0)

                    @php
                        $i =1;
                        $time_taken = '';
                    @endphp
                    @foreach($res as $data)
                        @if($data->time_taken != $time_taken)
                            <tr class="bg-green">
                                <td colspan="2">Take at : {{date('M-d-Y h:i A',strtotime($data->time_taken))}}</td>
                            </tr>
                        @endif
                        <tr>
                            <td >{{$data->name}}</td>
                            <td >{{$data->vital_value}}</td>
                        </tr>
                        @php
                            $time_taken = $data->time_taken;
                        @endphp
                    @endforeach

            @else
            <span>No Summary found </span>
            @endif
        </div>
