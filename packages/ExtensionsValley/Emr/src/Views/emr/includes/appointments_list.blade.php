
        @php
            $search_date_web =  date('M-d-Y');
        @endphp
        <div class="col-md-12 no-padding" style="margin-bottom: 15px;">
            <div class="col-md-4 no-padding">
                <label for="">Search By Date</label>
                                <div class="clearfix"></div>
                                <div class="input-group">
                                    <input type="text" id='pat_search_date' name="pat_search_date" onblur="show_appoint_list();"class="form-control datepicker" value="{{$search_date_web}}" data-attr="date" placeholder="Date">
                                    <div class="input-group-btn">
                                        <button style="height: 23px;
                                        width: 30px;" class="btn light_purple_bg ">
                                        <i class="fa fa-search" onclick="show_appoint_list();"></i></button>
                                    </div>
                                </div>
            </div>
            @php
                $user_id = \Auth::user()->id;
                $doctor = \DB::table('doctor_master')->where('user_id', $user_id)->select('speciality', 'id')->get();
                $speciality_id = $doctor[0]->speciality;
                $doctor_id = $doctor[0]->id;
                $grouped_doctor_list = [];
                if(env('DOCTOR_WISE_GROUP_LOGIN', 0) == 0){
                    $chk_dr_exisits = \DB::table('login_speciality_map')
                        ->where('doctor_id', $doctor_id)
                        ->first();
                    if (!empty($chk_dr_exisits)) {
                        $grouped_doctor_list = \DB::table('doctor_master')
                            ->join('login_speciality_map', 'login_speciality_map.doctor_id', '=', 'doctor_master.id')
                            ->where('login_speciality_map.speciality_id', $speciality_id)
                            ->where('login_speciality_map.status', 1)
                            ->distinct()
                            ->orderBy('doctor_master.id')
                            ->pluck('doctor_master.doctor_name', 'doctor_master.id');
                    }
                }else {
                    $chk_dr_exisits = \DB::table('doctor_wise_group_login')
                        ->where('doctor_id', $doctor_id)
                        ->whereNull('deleted_at')
                        ->first();
                    if (!empty($chk_dr_exisits)) {
                        $group_parent_doctor_id = \DB::table('doctor_wise_group_login')->where('doctor_id', $doctor_id)->whereNull('deleted_at')->value('parent_doctor_id');
                        $grouped_doctor_list = \DB::table('doctor_master')
                            ->join('doctor_wise_group_login', 'doctor_wise_group_login.doctor_id', '=', 'doctor_master.id')
                            ->where('doctor_wise_group_login.parent_doctor_id', $group_parent_doctor_id)
                            ->orderBy('doctor_master.id')
                            ->pluck('doctor_master.doctor_name', 'doctor_master.id');
                    }
                }

            @endphp
            @if (count($grouped_doctor_list) > 0)
                <div class="col-md-4 ">
                    <label for="">Select Doctor</label>
                    <div class="clearfix"></div>
                    {!! Form::select('group_doctor_app_list', $grouped_doctor_list, $doctor_id, ['class' => 'form-control', 'id' => 'group_doctor_app_list', 'onchange' => 'show_appoint_list();', 'style' => ' color:#555555; padding:4px 12px;']) !!}
                </div>
            @endif

            <div class="col-md-4 pull-right">
                <label for="">&nbsp;</label>
                <div class="clearfix"></div>
                <span class="badge bg-blue">Booked</span>
                <span class="badge bg-green">Checked In</span>
                <span class="badge bg-red">Cancelled</span>
            </div>
                <div class="col-md-4 text-center"><span class="badge bg_count" id="booking_listcount"></span>
            </div>


        </div>
        <div class="clearfix"></div>

        <div class="appointment_list_container" >

        </div>
        