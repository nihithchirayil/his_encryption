        <h5>Discharge vitals</h5>
         <table class="table no-margin  table-striped table_sm table-condensed table-col-bordered" style="font-size:10px;">
            <tbody>
            @if(sizeof($res)>0)

                    @php
                        $i =1;
                        $time_taken = '';
                    @endphp
                    @foreach($res as $data)
                        @if($data->time_taken != $time_taken)
                            <tr style="background-color:rgb(223, 223, 223);">
                                <td colspan="2">Taken at : {{date('M-d-Y h:i A',strtotime($data->time_taken))}}</td>
                            </tr>
                        @endif
                        <tr>
                            <td >{{$data->name}}</td>
                            <td >{{$data->vital_value}}</td>
                        </tr>
                        @php
                            $time_taken = $data->time_taken;
                        @endphp
                    @endforeach

                    @else
                    <span>No Summary found </span>
                    @endif
            </tbody>
         </table>
