<div class="theadscroll always-visible" style="position: relative;height:450px;">
    <style>
        #usg_fav_result_table tr:hover{
            cursor: pointer;
            background-color:burlywood;
            color:blue;
        }
    </style>
    <table id="usg_fav_result_table" class="table table_sm table-bordered theadfix_wrapper">
        <thead>
            <tr class="table_header_bg">
                <th style="width:20%;">Key</th>
                <th style="width:80%;">Value</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($result as $data)
                <tr onclick="fetchUsgFavToEditor('{{$data->content}}')">
                    <td>{{$data->name}}</td>
                    <td>{{$data->content}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
