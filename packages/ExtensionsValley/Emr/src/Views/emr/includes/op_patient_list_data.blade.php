
  @if($res != 0)
    <?php $i = 1; ?>
    @foreach($res as $data)
    <?php
       $cls = '';
       if($data->dr_seen_status == 1){
        $cls.= 'seen';
        $seen_bg =
        $style_attr = "background-color:".\WebConf::getConfig('DashboardColors.seen_bg')."!important;
                color:".\WebConf::getConfig('DashboardColors.seen_color')."!important";
       }else{
        $cls.= 'not_seen';
        $style_attr = "background-color:".\WebConf::getConfig('DashboardColors.not_seen_bg')."!important;
                color:".\WebConf::getConfig('DashboardColors.not_seen_color')."!important";
       }
       if($data->visit_type == 'Renewal' || $data->visit_type == 'Free visit'){
        $cls.= ' follow_up';
       }
       if($data->is_mlc == 't'){
        $cls.= ' mlc';
       }
       if($data->is_walkin == 1){
        $cls.= ' walkin';
       }

    ?>

        <tr class="{{$cls}} patient_list_row" style="{{$style_attr}}">
            <td onclick="goToSelectedPatient('{{$data->patient_id}}');">{!!$i!!}</td>
            <td onclick="goToSelectedPatient('{{$data->patient_id}}');">{!!$data->appointment_no!!}</td>
            <td onclick="callTokenByBookingId('{{$data->booking_id}}')">@if($data->call_token_status == 1)<button class="btn call_token_by_booking_id_btn" style="background-color:#c31b1b;color:white; width: 55px;" title="Call Token"><i class="fa fa-phone"></i> Call </button>@endif</td>
            <td onclick="goToSelectedPatient('{{$data->patient_id}}');">{!!date('h:i:A',strtotime($data->time_slot))!!}</td>
              @if($data->visit_type == 'Renewal' || $data->visit_type == 'Free visit')
                <td style="background-color:{{\WebConf::getConfig('DashboardColors.follow_up_bg')}}!important;
                color:{{\WebConf::getConfig('DashboardColors.follow_up_color')}}!important">
                  {!!$data->visit_type!!}
                </td>
              @else
                <td>{!!$data->visit_type!!}</td>
              @endif
            <td onclick="goToSelectedPatient('{{$data->patient_id}}');">{!!$data->uhid!!}</td>
            <td onclick="goToSelectedPatient('{{$data->patient_id}}');">{!!$data->patient_name!!}</td>
            <td onclick="goToSelectedPatient('{{$data->patient_id}}');">{!!$data->age!!}/{!!$data->gender!!}</td>
            <td onclick="goToSelectedPatient('{{$data->patient_id}}');">{!!$data->address!!}</td>
              @if($data->is_mlc == 't')
                <td onclick="goToSelectedPatient('{{$data->patient_id}}');" style="background-color:{{\WebConf::getConfig('DashboardColors.mlc_bg')}}!important;
                color:{{\WebConf::getConfig('DashboardColors.mlc_color')}}!important">{!!$data->mobile_no!!}</td>
              @else
              <td onclick="goToSelectedPatient('{{$data->patient_id}}');">{!!$data->mobile_no!!}</td>
              @endif
              @if($data->is_walkin == 1)
              <td onclick="goToSelectedPatient('{{$data->patient_id}}');" style="background-color:{{\WebConf::getConfig('DashboardColors.walkin_bg')}}!important;
              color:{{\WebConf::getConfig('DashboardColors.walkin_color')}}!important">{!!$data->waiting_time!!}</td>
              @elseif($data->dr_seen_status == 0)
              <td onclick="goToSelectedPatient('{{$data->patient_id}}');">{!!$data->waiting_time!!}</td>
              @else
              <td onclick="goToSelectedPatient('{{$data->patient_id}}');"></td>
              @endif
            </tr>

        <?php $i++; ?>
    @endforeach
  @else
  <tr>
     <td colspan=10>No patiens found!</td>
  </tr>
  @endif
<script>
 function callTokenByBookingId(booking_id){
      var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem('selected_dr_id')) : 0;
        if(booking_id) {

          var shift_id = $('.selectShift').val();

            var url = $('#base_doumenturl').val() + "/emr/callTokenByBookingId";
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    current_booking_id: booking_id,
                    selected_dr_id:selected_dr_id,
                    shift_id:shift_id
                },
                beforeSend:function (){
                      $(".current_token").html("<i class='fa fa-spinner fa-spin'></i>");
                },
                success: function(data) {
                    if(data){
                        if(data.status === 1){
                           localStorage.setItem('current_booking_id', data.booking_id);
                            $(".current_token").html(data.token);
                            // toastr.success("Token Called.");
                            $(".current_token").addClass("blink_me");
                            setTimeout(() => {
                                $(".current_token").removeClass("blink_me");
                            }, (2000));
                        } else {
                            $(".current_token").html(0);
                            toastr.success("No appointments found.");
                        }
                    }
                }, 
                error: function(){
                    toastr.success("Call Failed");
                }
            });
        }
  }
  </script>