<div class="box no-border no-margin preview_container_wrapper anim">
    <div class="box-body clearfix">
        <div class="col-md-12 no-padding " style="cursor: pointer;padding:0 15px 0 0;padding-top:5px !important;">

                <div class="col-md-6 padding_sm pull-right">

                    <button type="button" style="width: 110px !important;float: right;height: 25px;background-color:#36A693;color:white;margin-right:13px;" class="btn bg-primary save_class" onclick="saveClinicalTemplate(2);">
                        <i class="fa fa-save"></i> Save and print
                    </button>

                    <button class="btn bg-primary" onclick="fetchAssessmentHistory(1);" data-toggle="collapse"
                    data-target="#assesmentCollapse" style="width: 110px !important;float: right;height: 25px;">
                        <i class="fa fa-history"></i> History
                    </button>
                    <button type="button" onclick="fetchFavoriteTemplates()"
                    class="btn bg-primary fetchFavoriteTemplatesBtn" title="Bookmarks" style="width: 110px !important;float: right;height: 25px;"><i
                        class="fa fa-list"></i> Bookmarks
                    </button>

                </div>
                <div class="col-md-2 ">
                    <h4 class="card_title">Clinical Notes</h4><span class="draft_mode_indication_ca"></span>
                </div>
                <div class="col-md-4 pull-right">
                    @php
                    $nursing_station = session()->get('nursing_station');
                    @endphp
                    <input type="hidden" name="ca_edit_status" id="ca_edit_status" value="0" />
                    @if ($nursing_station != '')
                        <button type="button" class="btn bg-green" onclick="viewNursesNotes();"><i
                                class="fa fa-list"></i> View Nurses Notes</button>


                        <input type="checkbox" name="chk_show_doctor_notes" id="chk_show_doctor_notes" value=""
                            onclick="fetchAssessmentHistory(1);" data-target="#assesmentCollapse"
                            aria-expanded="false" /> <label for="chk_show_doctor_notes">Show Doctor Assessment</label>

                    @else

                        <label class="switch">
                            <input type="checkbox" name="chk_show_nursing_notes" id="chk_show_nursing_notes" value="" onclick="fetchAssessmentHistory(1);"  data-target="#assesmentCollapse"
                            aria-expanded="false" >
                            <span class="slider round"></span>
                        </label>
                        <label for="chk_show_nursing_notes">Show Nursing Notes</label>
                    @endif
                </div>

                {{-- <span style="margin-left: 10px;">
                    <input type="checkbox" name="print_clinical_notes" id="print_clinical_notes">
                    <label for="print_clinical_notes">Print Clinical Notes</label>
                </span> --}}

        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 preview_left_box" style="padding: 2px;">
            <div class="card collapse" id="assesmentCollapse">

                <div class="card_body" id="ca-history-data-list">
                    @include('Emr::emr.assessment.assessment_history_view')
                </div>

                <div class="clearfix"></div>
                <div class="ht5"></div>

            </div>

            <div class="clearfix"></div>
            <div class="ht5"></div>

            <div style="display: flex;align-items: center;justify-content: center;flex-direction: row;"
                class="col-md-4 padding_sm">
                <select name="forms" id="forms" style="box-shadow:none;" class="bottom-border-text">
                    <option value="">Select Template</option>
                </select>
                {{-- <button class="btn light_purple_bg" style="margin: 0 0 0 5px;" onclick="loadPreviewTemplate()"><i class="fa fa-eye"></i> Draft Clinical Notes</button> --}}
            </div>
            <div class="col-md-2 padding_sm">
                <button type="button" onclick="saveFavTemplate()"
                    class="btn btn-default btn-flat fav-button-forms no-margin" title="Add To Bookmarks"><i
                        class="fa fa-star"></i>
                </button>

            </div>

            <div class="clearfix"></div>
            <div class="ht5"></div>
            <div class="clearfix"></div>
            <div class="ht5"></div>
            <div class="card">
                <!-- Card Body -->
                <form id="ca-data-form">
                    <div class="card_body notes_box">
                        <div class="theadscrolll" style="position: relative; height: auto;">
                            <div class="col-md-12" id="template-content-load-div"
                                style="background-image: url({{ asset('packages/extensionsvalley/default/img/medical_bg.jpg') }}); background-repeat: repeat;">

                            </div>
                        </div>
                    </div>
                </form>
                <!-- Card Body -->
            </div>
        </div>

        {{-- <div class="col-md-6 preview_right_box hidden">
        <div class="preview_div"></div>
    </div> --}}
    </div>
</div>
