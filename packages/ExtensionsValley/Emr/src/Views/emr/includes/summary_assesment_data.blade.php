         <table class="table table-striped  no-border no-margin" style="width:100%;">
            <thead>
                <tr style="background-color:rgb(187, 186, 186);">
                    <td>Date</td>
                    <td>Form Name</td>
                    <td>Doctor</td>
                    <td>Visit Status</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
            @if(sizeof($res)>0)

                    @foreach($res as $data)

                        <tr>
                            <td>{{date('M-d-Y h:i A',strtotime($data->created_at))}}</td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->doctor_name}}</td>
                            <td>{{$data->visit_status}}</td>
                            <td>
                                <button class="btn btn-block light_purple_bg" title="View" onclick="loadFormView('{{$data->id}}','{{$data->form_id}}')"><i class="fa fa-eye"></i></button>
                            </td>
                        </tr>

                    @endforeach

            @endif
            </tbody>
         </table>
