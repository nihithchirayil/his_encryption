

<div class="box no-border no-margin anim" style="min-height:360px;">
    <div class="box-body clearfix">
        <table class="table table-bordered no-margin table_sm no-border theadfix_wrapper">
            <thead>
                <tr class="table_header_bg">
                    <th>SL.No.</th>
                    <th>UHID</th>
                    <th>Patient Name</th>
                    <th>Age/Gender</th>
                    <th>Dob</th>
                    <!-- <th>Address</th> -->
                    <th>Phone</th>
                    <th>Package Name</th>
                    <th>Doctor Name</th>
                </tr>
            </thead>
            <tbody class="health_package_list_table_body">
            @isset($health_package_list)
            @if(count($health_package_list) > 0)
            @foreach($health_package_list as $package)
            <tr class="patient_det_widget_hp @if($package->seen_status == 1) patient_seen @endif" data-patientname="{{$package->patient_name_wthout_title}}" data-uhid="{{$package->uhid}}" data-phone="{{$package->phone}}" onclick="goToSelectedPatient('{{$package->patient_id}}');" style="cursor:pointer;" >
                <td>{{($health_package_list->currentPage() - 1) * $health_package_list->perPage() + $loop->iteration}}</td>
                <td>{{$package->uhid}}</td>
                <td>{{$package->patient_name}}</td>
                <td>{{$package->age}}/{{$package->gender}}</td>
                <td>{{$package->dob}}</td>
                <!-- <td>{{$package->address}}</td> -->
                <td>{{$package->phone}}</td>
                <td>{{$package->package_name}}</td>
                <td>{{$package->doctor_name}}</td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="8">No records found..!</td>
            </tr>
            @endif
            @endisset
            </tbody>
        </table>

    </div>
</div>

<div class="clearfix"></div>
<div class="col-md-12 text-right">
    <ul class="pagination purple_pagination" style="text-align:right !important; margin: 5px 0px;">
        {!! $page_links !!}
    </ul>
</div>

<div class="clearfix"></div>
<div class="col-md-12" style="margin-bottom: 20px; margin-top: 15px;">
    <div class="col-md-2" style="text-align: left;">
        <span class="badge" style="background-color:#b5fbba; border: 1px solid #ccc; border-radius: 0; width: 10px; height: 17px;">&nbsp;&nbsp;&nbsp;</span> Seen
    </div>
    <div class="col-md-2" style="text-align: left;">
        <span class="badge" style="background-color:#ffffff; border: 1px solid #ccc; border-radius: 0; width: 10px; height: 17px;">&nbsp;&nbsp;&nbsp;</span> Not Seen
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            var file_token = $('#hidden_filetoken').val();
            var param = { _token: file_token};
            $.ajax({
                type: "POST",
                url: url,
                data: param,
                beforeSend: function () {
                    $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
                },
                success: function (data) {
                    //console.log(data);
                    $(".health_package_list_table_container").html(data);
                    var $table = $('table.theadfix_wrapper');
                    $table.floatThead({
                        scrollContainer: function ($table) {
                            return $table.closest('.theadscroll');
                        }
                    });
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                },
                complete: function () {
                    $('.theadfix_wrapper').floatThead('reflow');
                    $("body").LoadingOverlay("hide");
                }
            });
            return false;
        });

    });
</script>