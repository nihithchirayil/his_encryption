    <script src="{{asset("packages/extensionsvalley/emr/js/dr_dashboard.js")}}"></script>

                @php
                   $search_date_web =  date('M-d-Y');
                @endphp
        <div class="col-md-12 no-padding" style="margin-bottom: 15px;">
            <div class="col-md-4 no-padding">
                <label for="">Search By Date</label>
                                <div class="clearfix"></div>
                                <div class="input-group">
                                    <input type="text" id='pat_search_date' name="pat_search_date" onblur="show_lab_result_list();"class="form-control datepicker" value="{{$search_date_web}}" data-attr="date" placeholder="Date">
                                    <div class="input-group-btn">
                                        <button style="height: 23px;
                                        width: 30px;" class="btn light_purple_bg ">
                                        <i class="fa fa-search" onclick="show_lab_result_list();"></i></button>
                                    </div>
                                </div>
            </div>
           


        </div>
        <div class="clearfix"></div>

        <div class="theadscroll always-visible" style="position: relative; height: 410px;">

            <table class="table table_sm table-bordered theadfix_wrapper">
                <thead>
                    <tr class="table_header_bg">
                        <th>Sl No</th>
                        <th>Name</th>
                        <th>Uhid</th>
                        <th>Age/Gender</th>
                        <th>Address</th>
                        <th>Phone</th>
                       

                    </tr>
                </thead>
                <tbody>
                      @if(sizeof($res)>0)
                    @php
                        $i =1;
                        $status = '';
                        $cls = '';
                    @endphp
                    @foreach($res as $data)


                        <tr >
                            <td >{{$i}}</td>
                         
                             <td >{{$data->patient_name}}</td>
                             <td >{{$data->uhid}}</td>
                              <td >{{$data->age}} / {{$data->name}}</td>

                            <td >{{$data->address}}</td>
                            <td >{{$data->phone}}</td>
                           



                        </tr>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                     @else
          <tr style="text-align: center;">
    <td colspan="8" >No Data Found.!</td>
</tr>
            @endif
                </tbody>
            </table>

        </div>

