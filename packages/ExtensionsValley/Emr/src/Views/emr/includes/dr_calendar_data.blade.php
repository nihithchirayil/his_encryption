
        @if(!empty($patient_data))
            <input type="hidden" id="next_review_patient_name" value="{{$patient_data[0]->patient_name}}"/>
            <input type="hidden" id="next_review_patient_id" value="{{$patient_data[0]->patient_id}}"/>
            <input type="hidden" id="next_review_age" value="{{$patient_data[0]->age}}"/>
            <input type="hidden" id="next_review_gender" value="{{$patient_data[0]->gender}}"/>
            <input type="hidden" id="next_review_phone" value="{{$patient_data[0]->phone}}"/>
            <input type="hidden" id="next_review_address" value="{{$patient_data[0]->address}}"/>
            <input type="hidden" id="next_review_doctor_id" value="{{$doctor_id}}"/>
        @endif
@php $NoOfSlotsEditableBlockInEmr = @$NoOfSlotsEditableBlockInEmr ? $NoOfSlotsEditableBlockInEmr : 0; @endphp
    @if ($NoOfSlotsEditableBlockInEmr == 0)
        <div class="col-md-12" style="margin-bottom:10px;">
            <span class="badge bg-orange pull-right">Blocked</span>
        </div>
    @endif
        @php
            //print_r($res);
        @endphp
        @if(!empty($res))
        <div class="theadscroll always-visible" style="position: relative; height: 480px;">
            <table class="table table_sm table-bordered theadfix_wrapper">
                <thead>
                    <tr class="table_header_bg">
                        <th>#</th>
                        <th>Token #</th>
                        <th>Time</th>
                        <th>Patient</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i =1;
                    @endphp
                    @foreach($res as $data)
                        @php
                            $cls = '';
                            if($data->isslotvalid == '0' || $data->booked_status == 1){
                                $cls = 'appointment_invalid';
                            }
                            if($data->status == 1){
                                $cls = 'appointment_booked';
                            }
                            if($data->status == 2){
                                $cls = 'appointment_checked_in';
                            }
                        @endphp

                        <tr class="{{$cls}}" 
                        @if ($NoOfSlotsEditableBlockInEmr == 0)
                            @if($data->ret_seq <= 10) title="Slot not available for booking" style="background-color:orange;color:white;" @endif
                             @if($data->ret_seq > 10) @if($data->isslotvalid != '0' && $data->booked_status == 0) 
                             onclick="create_appointment('{{$data->slot}}', '{{$data->ret_seq}}', '{{$data->session_id}}');"
                             @endif @endif
                        @else
                             @if($i > $NoOfSlotsEditableBlockInEmr) 
                                @if($data->isslotvalid != '0' && $data->booked_status == 0) 
                                onclick="create_appointment('{{$data->slot}}', '{{$data->ret_seq}}', '{{$data->session_id}}');"
                                @endif
                             @endif
                        @endif
                         >
                            <td width="5%">{{$i}}</td>
                            <td width="5%">{{$data->ret_seq}}</td>
                            <td width="8%">{{date('h:i A',strtotime($data->slot))}}</td>
                            <td width="80%"> {{$data->uhid}}  {{$data->patient_name}}  {{$data->mobile_no}}</td>
                        </tr>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
        <span><b>Doctor is not available.</b></span>
        @endif
