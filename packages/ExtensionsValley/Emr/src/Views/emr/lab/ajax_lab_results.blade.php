
    <script src="{{asset("packages/extensionsvalley/emr/datatables/datatables.min.js")}}"></script>
    <link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom-emr.css")}}" rel="stylesheet">
     <link href="{{asset("packages/extensionsvalley/emr/datatables/dataTables.min.css")}}" rel="stylesheet">
  <style>
      .headergroupbg{
          background-color:#01987a !important;
          color:white;
      }
      #lab_result_table tbody tr td {
            text-align: left;
      }

      .slidingDiv {
        height:300px;
        background-color: #99CCFF;
        padding:20px;
        height:450px;
        background-color: #f9f9f9;
        margin-top:10px;
        border-bottom:5px solid #3399FF;
        border-bottom:5px solid #ffffff;
        margin-top:-400px;
        margin-left: 221px;
        width: 83%;
        border:2px solid #d4d4d4;
        border-radius:5px;
    }

       #selectInvestigation{
        width: 100% !important;
        border: aliceblue !important;
        margin-top: 7px !important;
      }
      .table-col-bordered >tr{
          height:45px !important;
      }
      .lab_result_table_info .paging_simple_numbers{
          margin-top:10px;
      }
    .sorting, .sorting_asc, .sorting_desc {
        background : none;
    }

    table.dataTable thead .sorting{
        background-image:none !important;
    }

    .mate-input-box label{
        top:-1px !important;
    }

  </style>
<div style="max-height: 480px;padding:20px;margin-top: -20px;">
<input type="hidden" id="hide_table_elements" value="{{$hide_elements}}">
<form method="POST" action="" accept-charset="UTF-8" name="frm-poc" id="frm-poc">
  <div class="row">
      <div class="col-md-12" style="margin-bottom: 5px;">

        <div class="col-md-2 padding_sm">
            <div class="mate-input-box">
                <label for="" style="font-size: 12px !important;">From date</label>
                <div class="clearfix"></div>
                <input type="text" onblur="serachLabData();" value="" id='investigation_fromdate' name="investigation_fromdate" class="form-control datepicker" data-attr="date" placeholder="Date">
            </div>
        </div>

        <div class="col-md-2 padding_sm">
            <div class="mate-input-box">
                <label for="" style="font-size: 12px !important;">To date</label>
                <div class="clearfix"></div>
                <input type="text" onblur="serachLabData();" value="" id='investigation_todate' name="investigation_todate" class="form-control datepicker" data-attr="date" placeholder="Date">
            </div>
        </div>

        <div class="col-md-4 padding_sm">
            <div class="mate-input-box">
                <label for="" style="font-size: 12px !important;">Investigation</label>
                <div class="clearfix"></div>
                {!! Form::select('lab_test',$testArray,'0', [
                        'class'=>'form-control ',
                        'placeholder'=>'Select Investigation',
                        'id'=>'lab_test',
                        'onchange'=>'serachLabData()',
                ])!!}
            </div>
        </div>

        <div class="col-md-2 padding_sm" style="padding-top: 17px !important;">
            <button style="" class="btn btn-sm bg-purple pull-right lab_graph_view">
                <i class="fa fa-area-chart" aria-hidden="true"></i> Analyse Result Trend
            </button>
        </div>

      </div>
      @php
      $style = '';
      if ($hide_elements == '1') {
        $style = 'background: #21a4f1 !important;';
      }
      @endphp
      <div class="col-md-12 theadscroll" style="position:relative;height:460px;">
          <table class="table no-margin theadfix_wrapper table-striped table_sm table-condensed styled-table floatThead-table table-col-bordered" id="lab_result_table" style="border: 1px solid rgb(204, 204, 204); table-layout: fixed;width:100%;">
              <thead class="headergroupbg" style="{{$style}}">
                <th style="width:10%">Date</th>
                <th style="width:2%"><i class="fa print_icon fa-file-pdf-o" aria-hidden="true" style="font-size: 12px; color: white;"></i></th>
                <th style="width:20%">Test</th>
                <th style="width:17%">Description</th>
                <th style="width:9%;align:center;">Value </th>
                <th style="width:9%;align:center;">Normal Range</th>
                {{-- <th style="width:20%">Sub Test</th> --}}
                <th style="width:6%">Remark</th>
                <th style="width:10%">Result Status</th>
                <th style="width:10%">Finalized By</th>
                @if($hide_elements == '0')
                <th style="width:12%">Approx. Result Time</th>
                <th style="width:6%">TAT</th>
                <th style="width:8%">Pro.time</th>
                @endif
              </thead>
          </table>
      </div>
  </div>
  <div class="col-md-12 slidingDiv" style="display:none;">
    {{-- <a href="#" class="show_hide">hide</a> --}}
   <a class="imagesDiv-link" href="#" onclick="hideGraphView();"
   style="font-size:26px;
   position: absolute;
   margin-top: -15px !important;
   margin-left: -15px;
   z-index: 99999;">
       <i class="fa fa-times-circle" aria-hidden="true"></i>
   </a>
    <div class="col-md-12 no-padding" id="slidingDivData" style="margin-top:19px !important;"></div>
 </div>
</form>

</div>

<script>
    var hide_elements = 0;
    $(document).ready(function () {
        setTimeout(function(){
        $('#lab_result_table').dataTable().fnDestroy();
        $('.slidingDiv').hide();
        loadLabData();
        $.fn.dataTable.ext.errMode = 'none';
        $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
        },1000);
        // loadLabData();
        // $.fn.dataTable.ext.errMode = 'none';
        // $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });
        hide_elements = $("#hide_table_elements").val();
    });

    function serachLabData(){
        $('#lab_result_table').dataTable().fnDestroy();
        table = $('#lab_result_table').DataTable(
            {
			"dom": '<"dt-buttons"Bfli>rtp',
			"paging": false,
			"autoWidth": true,
			"fixedHeader": true,

                }
         );
        table.destroy();
        loadLabData();
    }

    function filterLabTest(test_desc){
        var test_description = atob(test_desc);
        $('#lab_test').val(test_description);
        serachLabData();
    }


    function loadLabData(){
        var patientid = $('#patient_id').val();
        var visitid = $('#visit_id').val();
        var lab_test = $('#lab_test').val();
        var from_date = $('#investigation_fromdate').val();
        var to_date = $('#investigation_todate').val();

        $('#lab_result_table').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

        var column_array = [
            { "data": "created_at" },
            { "data": "sample_no_print" },
            { "data": "service_desc" },
            { "data": "sub_service_desc" },
            { "data": "result" },
            { "data": "normal_range" },
            // { "data": "sub_test_name" },
            { "data": "remark" },
            { "data": "lab_result_status" },
            { "data": "finalized_by" },
        ]
        if (parseInt(hide_elements) == 0) {
            var second_array = [
                { "data": "expected_result_time" },
                { "data": "time_gap" },
                { "data": "tat" }
            ];
            jQuery.merge(column_array, second_array);
        }

        $('#lab_result_table').DataTable({
            "processing": true,
            "serverSide": true,
            "searching":false,
            "pageLength": 50,
            "scrollY": 300,
            "ajax": {
                "url": $('#base_url').val() + "/nursing/labResultTrends",
                "dataType": "json",
                "type": "GET",
                "data": {_token: "{{csrf_token()}}", poc_action:"all_poc", patient_id:patientid, visit_id:visitid,lab_test:lab_test,from_date:from_date,to_date:to_date, hide_elements:hide_elements},
                "error" : function(e){  console.log(e);
                }
            },
            "columns": column_array,

        });

        $('#lab_result_table').LoadingOverlay("hide");
    }

    $("#checklaball").on('click', function () {
        if ($('#checklaball').is(':checked')) {
            $('.lab_chk').prop('checked', 'checked');
        } else {
            $('.lab_chk').removeAttr('checked')
        }
    });


    $(document).on('click','.lab_graph_view', function (e) {
    e.preventDefault();
    var patient_id = $('#patient_id').val();
    var encounter_id = $('#encounter_id').val();
    var lab_test = $('#lab_test').val();
    if(patient_id > 0) {
    var url = $('#base_url').val() + "/emr/get-graph-view-lab";
    $.ajax({
                url:url,
                type: "GET",
                // data: "patient_id="+patient_id+"&encounter_id="+ encounter_id,
                data: 'patient_id=' + patient_id +'&encounter_id='+ encounter_id +'&start_date='+$('#investigation_fromdate').val()+'&end_date='+$('#investigation_todate').val()+"&lab_service_name="+lab_test,

                beforeSend: function() {
                    $("#modal_lab_result_trends").modal({backdrop: 'static', keyboard: false});
                },
                success:function(data){
                    $('#slidingDivData').html('');
                    $('#lab_result_trends_data').html(data);
                },
                complete: function(){

                }
            });
        }
    });
    function showTrend(service_id){
        var patient_id = "{{$patient_id}}";

        var url = $('#base_url').val() + "/emr/get-graph-view-lab";

        $.ajax({
            type: "GET",
            url: url,
            data: 'patient_id=' + patient_id+'&start_date='+$('#investigation_fromdate').val()+'&end_date='+$('#investigation_todate').val()+"&lab_service_id="+service_id,
            beforeSend: function () {
                $('.slidingDiv').animate({
                    width: 'toggle'
                }, "slow");
                $('#slidingDivData').html('');
                $('#slidingDivData').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });
            },
            success: function (popup_response) {
                setTimeout(function() {
                    $('#slidingDivData').html(popup_response);
                },600);
                $('.theadscroll').perfectScrollbar({
                    wheelPropagation: true,
                    minScrollbarLength: 30
                });
            },
            complete: function () {
                $('#slidingDivData').LoadingOverlay("hide");

            },
            error: function () {

            }
        });
    }

    function hideGraphView(){
        $('.slidingDiv').animate({
            width: 'toggle'
        }, "slow")
    }


</script>

