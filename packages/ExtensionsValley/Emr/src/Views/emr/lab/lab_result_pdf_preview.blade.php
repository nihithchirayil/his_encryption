<div class="row" id="lab_result_data">
  <div class="col-md-12 padding_sm">
    <?php
    if (count($lab_data) != 0) {
    ?>
      <table class="table no-margin table_sm no-border" style="width: 100%; font-size:14px;">
        <thead>
          <tr>
            <th style="text-align: center" colspan="4">
              <?= @$hospital_header ? $hospital_header : '' ?>
            </th>
          </tr>
          <tr>
            <td colspan="4" style="border: 2px solid #000;">
              <table class="table no-margin table_sm no-border" style="width: 100%;">
                <thead>
                  <tr>
                    <td width="13%" style="font-weight: 600;"> Name </td>
                    <td width="2%" style="text-align: center;"> : </td>
                    <td width="35%"> <?= $lab_data['patient_details']['patient_name'] ?> </td>

                    <td width="13%" style="font-weight: 600;"> UHID </td>
                    <td width="2%" style="text-align: center;"> : </td>
                    <td width="35%"> <?= $lab_data['patient_details']['uhid'] ?> </td>
                  </tr>
                  <tr>
                    <td width="13%" style="font-weight: 600;"> Age/Gender </td>
                    <td width="2%" style="text-align: center;"> : </td>
                    <td width="35%"> <?= $lab_data['patient_details']['age'] . '/' . $lab_data['patient_details']['genderdesc'] ?> </td>

                    <td width="13%" style="font-weight: 600;"> IP Number </td>
                    <td width="2%" style="text-align: center;"> : </td>
                    <td width="35%"> <?= $lab_data['patient_details']['ip_no'] ?> </td>
                  </tr>
                  <tr>
                    <td width="13%" style="font-weight: 600;"> Referred By </td>
                    <td width="2%" style="text-align: center;"> : </td>
                    <td width="35%"> <?= $lab_data['patient_details']['doctor_name'] ?> </td>

                    <td width="13%" style="font-weight: 600;"> Collected </td>
                    <td width="2%" style="text-align: center;"> : </td>
                    <td width="35%"> <?= $lab_data['patient_details']['sample_collection_date'] ?> </td>
                  </tr>
                  <tr>
                    <td width="13%" style="font-weight: 600;"> Location </td>
                    <td width="2%" style="text-align: center;"> : </td>
                    <td width="35%"> <?= $lab_data['patient_details']['location'] ?> </td>

                    <td width="13%" style="font-weight: 600;"> Reported </td>
                    <td width="2%" style="text-align: center;"> : </td>
                    <td width="35%"> <?= $lab_data['patient_details']['report_date'] ?> </td>
                  </tr>
                  <tr>
                    <td width="13%" style="font-weight: 600;"> Remarks </td>
                    <td width="2%" style="text-align: center;"> : </td>
                    <td width="35%"> <?= $lab_data['patient_details']['remarks'] ?> </td>
                  </tr>
                </thead>
              </table>
            </td>
          </tr>
        </thead>
        <tbody>
          @if ($lab_data['sample_details']['servicereporttype'] != 'T')
          <tr>
            <td style="padding: 10px 0 !important; font-weight: 600;" width="35%"> TEST NAME </td>
            <td style="padding: 10px 0 !important; font-weight: 600;" width="20%"> RESULT </td>
            <td style="padding: 10px 0 !important; font-weight: 600;" width="20%"> UNIT </td>
            <td style="padding: 10px 0 !important; font-weight: 600;" width="25%"> REF RANGE </td>
          </tr>
          @endif
          <tr>
            <td colspan="4" style="text-align: center; font-weight: 600; text-decoration: underline; padding: 5px 0 !important;"> <?= $lab_data['sample_details']['sub_dept_name'] ?> </td>
          </tr>
          <tr>
            <td colspan="4" style="border: dashed 2px #ada9a9; border-radius: 10px;">
              <table class="table no-margin table_sm no-border" style="width: 100%">
                <tr>
                  <td>Sample</td>
                  <td>:</td>
                  <td><?= $lab_data['sample_details']['sampletypename'] ?></td>
                  <td>Sample No</td>
                  <td>:</td>
                  <td><?= $lab_data['sample_details']['sample_no'] ?></td>
                  <td width="30%"></td>
                </tr>
              </table>
            </td>
          </tr>
          @if ($lab_data['sample_details']['servicereporttype'] == 'G')
          <tr>
            <td colspan="4" style="font-weight: 600; text-decoration: underline; padding: 7px 0 !important;"> <?= $lab_data['sample_details']['lab_service_name'] ?> </td>
          </tr>
          @endif
          @foreach ($lab_data['result'] as $key => $result)
          @if (!empty($key))
          <tr>
            <td colspan="4" style="font-weight: 600; text-decoration: underline; padding: 7px 0 !important;"> <?= $key ?> </td>
          </tr>
          @endif
          @foreach ($result as $k => $res)
          @if ($lab_data['sample_details']['servicereporttype'] != 'T')
          <tr>
            <td style="padding: 7px 0 !important;"> <?= $res['test_name'] ?> </td>
            <td style="padding: 7px 0 !important;"> <?= $res['result'] ?> </td>
            <td style="padding: 7px 0 !important;"> <?= $res['unit'] ?> </td>
            <td style="padding: 7px 0 !important;"> <?= $res['ref_range'] ?> </td>
          </tr>
          @else
          <tr>
            <td colspan="4" style="padding: 7px 0 !important;"> <?= $res['result'] ?> </td>
          </tr>
          @endif
          @endforeach
          @endforeach
          <tr>
            <td colspan="4" style="padding-top: 20px;">
              <table class="table no-margin table_sm no-border" style="width: 100%">
                <tr>
                  <td style="vertical-align: top;">
                    <table class="table no-margin table_sm no-border" style="width: 100%">
                      <tbody>
                        <tr>
                          <td style="padding-bottom: 5px; border-bottom: dashed 1px #000; text-align: center; font-weight: 600;">Reviewed By</td>
                        </tr>
                        <tr>
                          <td style="text-align: center;">
                            @if($lab_data['result_footer']['reviewed_signature'] !='')
                            @php
                            $signatureImage = "data:image/jpeg;base64,".($lab_data['result_footer']['reviewed_signature']);
                            @endphp
                            <img id="reviewed_signature" style="width:175px; height:70px;" src="{{$signatureImage}}">
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <td style="text-align: center;"> <?= $lab_data['result_footer']['reviewed_by'] ?> </td>
                        </tr>
                        <tr>
                          <td style="text-align: center;"> <?= $lab_data['result_footer']['reviewed_designation'] ?> </td>
                        </tr>
                        <tr>
                          <td style="text-align: center;"> <?= $lab_data['result_footer']['reviewed_qualification'] ?> </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td width="40%"></td>
                  <td style="vertical-align: top;">
                    <table class="table no-margin table_sm no-border" style="width: 100%">
                      <tbody>
                        <tr>
                          <td style="padding-bottom: 5px; border-bottom: dashed 1px #000; text-align: center; font-weight: 600;">Approved By</td>
                        </tr>
                        <tr>
                          <td style="text-align: center;">
                            @if($lab_data['result_footer']['approved_signature'] !='')
                            @php
                            $signatureImage = "data:image/jpeg;base64,".($lab_data['result_footer']['approved_signature']);
                            @endphp
                            <img id="approved_signature" style="width:175px; height:70px;" src="{{$signatureImage}}">
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <td style="text-align: center;"> <?= $lab_data['result_footer']['approved_by'] ?> </td>
                        </tr>
                        <tr>
                          <td style="text-align: center;"> <?= $lab_data['result_footer']['approved_designation'] ?> </td>
                        </tr>
                        <tr>
                          <td style="text-align: center;"> <?= $lab_data['result_footer']['approved_qualification'] ?> </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    <?php
    } else {
    ?>
      <table class="table no-margin table_sm no-border" style="width: 100%">
        <tr>
          <th style="text-align: center">No Records Found</th>
        </tr>
      </table>
    <?php
    }
    ?>
  </div>
</div>