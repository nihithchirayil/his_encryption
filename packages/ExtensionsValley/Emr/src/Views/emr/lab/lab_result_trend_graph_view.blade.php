<div class="col-md-12"><?php
    $lab_items = array();
    $selected_test_name = "";
    foreach($lab_items_list as $lab_item){
        $item_name = $lab_item->item_desc;
        $item_code = $lab_item->item_code;
        if(empty($lab_item->item_desc)){
            $item_name = $lab_item->item_name;
        }
        $lab_items[$item_code] = $item_name;
        asort($lab_items);
        if($lab_service_id == $item_code){
            $selected_test_name = $item_name;
        }
    }
    ?>

    <div id="lab_graph_container" class="theadscroll" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

<style type="text/css">
    .popup_form_add_edit{
        min-height: 500px;
    }
    .highcharts-credits{
        display: none !important;
    }
    .highcharts-button-symbol {
        stroke:#fff !important;
    }

</style>
<script type="text/javascript">
jQuery(document).ready(function(){
    $('.date_picker').datetimepicker({format: "MMM-DD-YYYY"});
});

function FilterLabGraph() {
    var patient_id = "{{$patient_id}}";
    var lab_item = $("select[name=lab_graph_item]").val();
    var url = $('#base_url').val() + "/emr/get-graph-view-lab";

    $.ajax({
        type: "GET",
        url: url,
        data: 'patient_id=' + patient_id+'&start_date='+$('#from_date').val()+'&end_date='+$('#to_date').val()+"&lab_service_id="+lab_item,
        beforeSend: function () {

        },
        success: function (popup_response) {
            $('#lab_result_trends_data').html('');
            $('#lab_result_trends_data').html(popup_response);
        },
        complete: function () {
        },
        error: function () {

        }
    });
}


Highcharts.chart('lab_graph_container', {
    chart: {
        type: 'spline'
    },
    title: {
        // text: "{{$patient_data->patient_name}}"
        text:''
    },

    subtitle: {
        //    text: ' ({{date("d-M-Y",strtotime($patient_data->dob))}}/{{($patient_data->gender == 1) ? "M" : (($patient_data->gender == 2) ? "F" : "T")}}) - {{$patient_data->uhid}}'
        text:"{{$patient_data->patient_name}} - {{$patient_data->uhid}}"
    },

    xAxis: {

        categories: [@foreach($result_set as $key=>$values) '{{date("M-d-Y h:i A",strtotime($values->created_at))}}', @endforeach],
        tickmarkPlacement: 'on',
        title: {
            enabled: true
        }
    },
    yAxis: {
        title: {
            text: 'Result Values'
        },
        labels: {
            formatter: function () {
                return this.value;
            }
        }
    },
    tooltip: {
        split: true,
    },
     plotOptions: {
       spline: {
            marker: {
                enabled: true
            },
            lineWidth: 4,
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: true
        }
    },

    series: [@foreach($dataset as $key => $items){
        name: '{{$key}}',
        data: [@foreach($datasetdatewise as $subkey => $subitems)
                        @if(!empty($datasetdatewise[$subkey][$key]))
                            {{str_replace(array(","),"",$datasetdatewise[$subkey][$key])}},
                        @else
                            '',
                        @endif
                @endforeach]
    },@endforeach]
    ,

});
function resetGraphViewProfileFields(){
    $("#graph_v_parent_item_code").val('');
    $("#is_parent_test").val('');
}
</script>
