
<script src="{{asset("packages/extensionsvalley/emr/datatables/datatables.min.js")}}"></script>
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
 <link href="{{asset("packages/extensionsvalley/emr/datatables/dataTables.min.css")}}" rel="stylesheet">
<style>
  .headergroupbg{
      background-color:#01987a !important;
      color:white;
  }
  #path_result_table tbody tr td {
        text-align: left;
  }


   #selectInvestigation{
    width: 100% !important;
    border: aliceblue !important;
    margin-top: 7px !important;
  }
  .table-col-bordered >tr{
      height:45px !important;
  }
  .path_result_table_info .paging_simple_numbers{
      margin-top:10px;
  }
.sorting, .sorting_asc, .sorting_desc {
    background : none;
}

table.dataTable thead .sorting{
    background-image:none !important;
}

</style>
<div style="max-height: 480px;padding:20px;margin-top: -20px;">
<form method="POST" action="" accept-charset="UTF-8" name="frm-poc" id="frm-poc">
<div class="row">
  <div class="col-md-12" style="margin-bottom: 5px;">

    <div class="col-md-2 padding_sm">
        <div class="mate-input-box">
            <label for="">From date</label>
            <div class="clearfix"></div>
            <input type="text" onblur="serachLabData();" value="" id='investigation_fromdate' name="investigation_fromdate" class="form-control datepicker" data-attr="date" placeholder="Date">
        </div>
    </div>

    <div class="col-md-2 padding_sm">
        <div class="mate-input-box">
            <label for="">To date</label>
            <div class="clearfix"></div>
            <input type="text" onblur="serachLabData();" value="" id='investigation_todate' name="investigation_todate" class="form-control datepicker" data-attr="date" placeholder="Date">
        </div>
    </div>

    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label for="">Investigation</label>
            <div class="clearfix"></div>
            {!! Form::select('lab_test',$testArray,'0', [
                    'class'=>'form-control ',
                    'placeholder'=>'Select Investigation',
                    'id'=>'lab_test',
                    'onchange'=>'serachLabData()',
            ])!!}
        </div>
    </div>



    <div class="col-md-2 padding_sm" style="padding-top: 17px !important;">
        <button style="" class="btn btn-sm bg-purple pull-right path_graph_view">
            <i class="fa fa-area-chart" aria-hidden="true"></i> Analyse Result Trend
        </button>
    </div>

  </div>
  <div class="col-md-12 theadscroll" style="position:relative;height:460px;">
      <table class="table no-margin theadfix_wrapper table-striped table_sm table-condensed styled-table floatThead-table table-col-bordered" id="path_result_table" style="border: 1px solid rgb(204, 204, 204); table-layout: fixed;width:100%;">
          <thead class="headergroupbg">
            <th style="width:10%">Date</th>
            <th style="width:20%">Test</th>
            <th style="width:17%">Description</th>
            <th style="width:9%;align:center;">Value </th>
            <th style="width:9%;align:center;">Normal Range</th>
            {{-- <th style="width:20%">Sub Test</th> --}}
            <th style="width:6%">Remark</th>
            <th style="width:10%">Result Status</th>
            <th style="width:10%">Finalized By</th>
            <th style="width:12%">Approx. Result Time</th>
            <th style="width:6%">TAT</th>
            <th style="width:8%">Pro.time</th>
          </thead>
      </table>
  </div>
</div>
</form>

</div>

<script>
$(document).ready(function () {

    loadLabData();
    $.fn.dataTable.ext.errMode = 'none';
    $(".select2").select2({ placeholder: "", maximumSelectionSize: 6 });

});

function serachLabData(){
    table = $('#path_result_table').DataTable(
        {
        "dom": '<"dt-buttons"Bfli>rtp',
        "paging": false,
        "autoWidth": true,
        "fixedHeader": true,

            }
     );
    table.destroy();
    loadLabData();
}

function filterLabTest(test_desc){
    var test_description = atob(test_desc);
    $('#lab_test').val(test_description);
    serachLabData();
}


function loadLabData(){
    var patientid = $('#patient_id').val();
    var visitid = $('#visit_id').val();
    var lab_test = $('#lab_test').val();
    var from_date = $('#investigation_fromdate').val();
    var to_date = $('#investigation_todate').val();

    $('#path_result_table').LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#009869' });

    $('#path_result_table').DataTable({
        "processing": true,
        "serverSide": true,
        "searching":false,
        "pageLength": 50,
        "scrollY": 300,
        "ajax": {
            "url": $('#base_url').val() + "/nursing/PathologyResults",
            "dataType": "json",
            "type": "GET",
            "data": {_token: "{{csrf_token()}}", poc_action:"all_poc", patient_id:patientid, visit_id:visitid,lab_test:lab_test,from_date:from_date,to_date:to_date},
            "error" : function(e){  console.log(e);
            }
        },
        "columns": [
            { "data": "created_at" },   
            { "data": "service_desc" },
            { "data": "sub_service_desc" },
            { "data": "result" },
            { "data": "normal_range" },
            // { "data": "sub_test_name" },
            { "data": "remark" },
            { "data": "lab_result_status" },
            { "data": "finalized_by" },
            { "data": "expected_result_time" },
            { "data": "time_gap" },
            { "data": "tat" }

        ],

    });

    $('#path_result_table').LoadingOverlay("hide");
}

$("#checklaball").on('click', function () {
    if ($('#checklaball').is(':checked')) {
        $('.lab_chk').prop('checked', 'checked');
    } else {
        $('.lab_chk').removeAttr('checked')
    }
});


$(document).on('click','.path_graph_view', function (e) {
e.preventDefault();

var patient_id = $('#patient_id').val();
var encounter_id = $('#encounter_id').val();
if(patient_id > 0) {
var url = $('#base_url').val() + "/nursing/getgraphviewpathology";
$.ajax({
            url:url,
            type: "GET",
            data: "patient_id="+patient_id+"&encounter_id="+ encounter_id,
            beforeSend: function() {
                $("#modal_pathology_result_trends").modal({backdrop: 'static', keyboard: false});
            },
            success:function(data){
                $('#pathology_result_trends_data').html(data);
            },
            complete: function(){

            }
        });
    }
});

</script>

