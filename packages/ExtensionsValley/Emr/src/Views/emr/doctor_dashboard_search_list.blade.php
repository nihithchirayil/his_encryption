<div class="col-md-6 padding_sm">
    <div class="theadscroll" style="position:relative; height:510px;">
        <table class="table no-margin no-border theadfix_wrapper">
            <thead>
                <tr class="bg-default patient_list_tile" style="color:white;">
                    <th class="text-center">Not Seen &nbsp;<span class="badge badge_purple_bg not_seen_count">0</span>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="padding: 0px;">
                        <div class="col-md-12 padding-sm" style="padding-right: 20px !important;">
                            @php
                                $not_seen_count = 0;
                            @endphp
                            @foreach ($op_patients as $data)
                                @if ($data->dr_seen_status == 0)
                                    @php
                                        $not_seen_count = $not_seen_count + 1;
                                        $class = '';
                                        $shift_data = '';
                                        if ($data->share_holder_status != 0) {
                                            $class .= ' share_holder_status';
                                        }
                                        if ($data->is_highrisk != 0) {
                                            $class .= ' is_highrisk';
                                        }
                                        if ($data->is_mlc != 0) {
                                            $class .= ' is_mlc';
                                        }
                                        if ($data->referred_to_status != 0) {
                                            $class .= ' referred_to_status';
                                        }
                                        if ($data->health_checkup != 0) {
                                            $class .= ' health_checkup';
                                        }
                                        if ($data->is_walkin != 0) {
                                            $class .= ' is_walkin';
                                        }
                                        if ($data->session_id == 1) {
                                            $shift_data = 'Morning';
                                        } elseif ($data->session_id == 2) {
                                            $shift_data = 'Evening';
                                        }
                                    @endphp
                                    <div data-uhid="{{ str_replace(' ', '', $data->uhid) }}"
                                        data-phone="{{ str_replace(' ', '', $data->mobile_no) }}"
                                        data-patientname="{{ str_replace(' ', '', $data->patient_name) }}"
                                        class=" col-md-12 box-body notseenpatient patient_det_widget card {{ $class }}"
                                        style="margin:2px;cursor: pointer;border:0px !important;border-radius:3px;;margin-bottom:5px;  border:1px solid rgb(111, 190, 135) !important;margin-right:5px !important;box-shadow: 2px 2px 3px 1px rgba(1, 36, 3, 0.31);
                            -webkit-box-shadow: 2px 2px 3px 1px rgba(45,150,51,0.31);
                            -moz-box-shadow: 2px 2px 3px 1px rgba(45,150,51,0.31);">

                                        <div class="pt_container col-md-9 no-padding" style="color: #fffdfc; "
                                            @if($is_gynecolgy_doctor == 1 )
                                                onclick="goToSelectedPatient('{{ $data->patient_id }}');"
                                            @elseif($casuality_template_status == 1)
                                                onclick="goToSelectedPatientCasuality('{{ $data->patient_id }}');"
                                            @else
                                                 onclick="goToSelectedPatient('{{ $data->patient_id }}');"
                                            @endif >

                                            <table class="table no-margin no-border table_sm">
                                                <tbody>

                                                    <tr>
                                                        <td width="35px">
                                                            @if ($data->patient_image != '')

                                                                <img class="patient_image_contianer"
                                                                    src="data:image/jpg;base64,{!! $data->patient_image !!}" />

                                                            @else
                                                                <div class="pat_det_img" style="color: #86736a;">
                                                                    <i class="fa fa-user" aria-hidden="true"></i>
                                                                </div>
                                                            @endif
                                                            <div class="token_time">@if ($data->special_token == ''){{ trim($data->token_no_prefix) }}{{ trim($data->token_no) }}@else{{ trim($data->special_token) }}@endif</div>
                                                        </td>
                                                        <td valign="top">

                                                            <table class="table no-margin table_sm pt_container pt"
                                                                style="background:#ffffff;font-size:12px !important;font-weight:600;color:rgb(63, 62, 62);margin-top: 5px !important;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width:65%;">
                                                                            {{ ucwords(strtolower($data->patient_name)) }}
                                                                        </td>
                                                                        <td style="width:20%;">
                                                                            {{ $data->age }} / {{ $data->gender }}
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width:65%;">{{ $data->uhid }}</td>
                                                                        <td style="width:20%;">{{ $data->mobile_no }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="flag_list">
                                                                                <ul class="list-unstyled no-padding no-margin"
                                                                                    style=";margin-top:2px !important;   justify-content: flex-start;">
                                                                                    @if ($data->share_holder_status != 0)
                                                                                        <li class="bg-orange"
                                                                                            title="Share holder">S</li>
                                                                                    @endif

                                                                                    @if ($data->is_highrisk != 0)
                                                                                        <li class="bg-red"
                                                                                            title="High Risk">H</li>
                                                                                    @endif

                                                                                    @if ($data->is_mlc != 0)
                                                                                        <li style="background-color:#6effc3"
                                                                                            title="MLC">M</li>
                                                                                    @endif

                                                                                    @if ($data->referred_to_status != 0)
                                                                                        <li style="background-color:#75dfff"
                                                                                            title="Doctor Referral">R
                                                                                        </li>
                                                                                    @endif

                                                                                    @if ($data->health_checkup != 0)
                                                                                        <li style="background-color:#5b7b85;color:white"
                                                                                            title="MLC">H</li>
                                                                                    @endif

                                                                                    @if ($data->is_walkin != 0)
                                                                                        <li style="background-color:#fd5bf5;color:white"
                                                                                            title="MLC">W</li>
                                                                                    @endif

                                                                                    @if ($shift_data != '')
                                                                                        <li style="background-color:#f37ea9;color:white; line-height: 1.5; width: 55px;"
                                                                                            title="{{ $shift_data }}">
                                                                                            {{ $shift_data }}</li>
                                                                                    @endif


                                                                                </ul>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <span title="slot time"
                                                                                class="badge_new btn-block"
                                                                                style="padding: 0; float: left; background: #fafcfa !important; border: none; box-shadow: none; text-align: left; font-size: 12px; color: #d9534f !important; ">{{ date('h:i A', strtotime($data->time_slot)) }}</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-3 padding_sm" style=" padding:0;">
                                            <div class="col-md-12 padding_sm" style="margin-top: 5px;">
                                                <span class="badge_new btn-block"
                                                    style="float: left; background: #fafcfa !important; border: none; box-shadow: none; text-align: center; ">{{ $data->visit_type }}</span>

                                                @if($is_gynecolgy_doctor == 1 )
                                                <button onclick="goToGynecologyPatient('{{ $data->patient_id }}')" class="btn btn-warning" style="color:white; float: left;" title="Gynecology">
                                                    <i class="fa fa-asterisk"></i>  
                                                </button>
                                                <button onclick="goToInfertilityPatient('{{ $data->patient_id }}')" class="btn btn-info" style="color:white; float: left;" title="Infertility">
                                                    <i class="fa fa-certificate"></i>  
                                                </button>
                                                @endif

                                                @if ($data->waiting_time < 1440)
                                                    {{-- @if ($data->waiting_time < 0) --}}
                                                    {{-- @else --}}
                                                        <span title="waiting time" class="badge_new btn-block"
                                                            style="float: left; background: #fafcfa !important; border: none; box-shadow: none; text-align: center;">
                                                            {{ round($data->waiting_time) }}m
                                                        </span>
                                                    {{-- @endif --}}
                                                @endif

                                                @if ($data->call_token_status == 1)
                                                    <button onclick="callTokenByBookingId('{{ $data->booking_id }}')"
                                                        class="btn btn-primary call_token_btn btn-block"
                                                        style="color:white; float: left;" title="Call Token"><i
                                                            class="fa fa-phone"></i> @if($data->token_called_at != "") Recall @else Call  @endif </button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            <input type="hidden" class="totalNotSeenCount" value="{{ $not_seen_count }}" />
                        </div>
                    </td>
                </tr>

            </tbody>

        </table>

    </div>

</div>

<div class="col-md-6 padding_sm">
    <div class="theadscroll" style="position:relative; height:510px;">
        <table class="table no-margin no-border theadfix_wrapper">
            <thead>
                <tr class="bg-default patient_list_tile" style="color:white;">
                    <th class="text-center">Seen &nbsp;<span class="badge badge_purple_bg seen_count">0</span></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="padding: 0px;">
                        <div class="col-md-12 padding-sm" style="padding-right: 20px !important;">
                            @php
                                $seen_count = 0;
                            @endphp
                            @foreach ($op_patients as $data)
                                @if ($data->dr_seen_status == 1)
                                    @php
                                        $seen_count = $seen_count + 1;
                                        $class = '';
                                        $shift_data = '';
                                        if ($data->share_holder_status != 0) {
                                            $class .= ' share_holder_status';
                                        }
                                        if ($data->is_highrisk != 0) {
                                            $class .= ' is_highrisk';
                                        }
                                        if ($data->is_mlc != 0) {
                                            $class .= ' is_mlc';
                                        }
                                        if ($data->referred_to_status != 0) {
                                            $class .= ' referred_to_status';
                                        }
                                        if ($data->health_checkup != 0) {
                                            $class .= ' health_checkup';
                                        }
                                        if ($data->is_walkin != 0) {
                                            $class .= ' is_walkin';
                                        }
                                        if ($data->session_id == 1) {
                                            $shift_data = 'Morning';
                                        } elseif ($data->session_id == 2) {
                                            $shift_data = 'Evening';
                                        }
                                    @endphp
                                    <div data-uhid="{{ str_replace(' ', '', $data->uhid) }}"
                                        data-phone="{{ str_replace(' ', '', $data->mobile_no) }}"
                                        data-patientname="{{ str_replace(' ', '', $data->patient_name) }}"
                                        class=" col-md-12 box-body notseenpatient patient_det_widget card {{ $class }}"
                                        style="margin:2px;cursor: pointer;border:0px !important;border-radius:3px;;margin-bottom:5px;  border:1px solid rgb(111, 190, 135) !important;margin-right:5px !important;box-shadow: 2px 2px 3px 1px rgba(1, 36, 3, 0.31);
                            -webkit-box-shadow: 2px 2px 3px 1px rgba(45,150,51,0.31);
                            -moz-box-shadow: 2px 2px 3px 1px rgba(45,150,51,0.31);">

                                        <div class="pt_container col-md-9 no-padding" style="color: #fffdfc; "
                                        @if($is_gynecolgy_doctor == 1 ) onclick="goToSelectedPatient('{{ $data->patient_id }}');"
                                        @elseif($casuality_template_status == 1)
                                            onclick="goToSelectedPatientCasuality('{{ $data->patient_id }}');"
                                        @else
                                         onclick="goToSelectedPatient('{{ $data->patient_id }}');"
                                         @endif>
                                            <table class="table no-margin no-border table_sm">
                                                <tbody>

                                                    <tr>
                                                        <td width="35px">
                                                            @if ($data->patient_image != '')

                                                                <img class="patient_image_contianer"
                                                                    src="data:image/jpg;base64,{!! $data->patient_image !!}" />

                                                            @else
                                                                <div class="pat_det_img" style="color: #86736a;">
                                                                    <i class="fa fa-user" aria-hidden="true"></i>
                                                                </div>
                                                            @endif
                                                            <div class="token_time">@if ($data->special_token == ''){{ trim($data->token_no_prefix) }}{{ trim($data->token_no) }}@else{{ trim($data->special_token) }}@endif</div>
                                                        </td>
                                                        <td valign="top">

                                                            <table class="table no-margin table_sm pt_container pt"
                                                                style="background:#ffffff;font-size:12px !important;font-weight:600;color:rgb(63, 62, 62);margin-top: 5px !important;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width:65%;">
                                                                            {{ ucwords(strtolower($data->patient_name)) }}
                                                                        </td>
                                                                        <td style="width:20%;">
                                                                            {{ $data->age }} / {{ $data->gender }}
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width:65%;">{{ $data->uhid }}</td>
                                                                        <td style="width:20%;">{{ $data->mobile_no }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="flag_list">
                                                                                <ul class="list-unstyled no-padding no-margin"
                                                                                    style=";margin-top:2px !important;   justify-content: flex-start;">
                                                                                    @if ($data->share_holder_status != 0)
                                                                                        <li class="bg-orange"
                                                                                            title="Share holder">S</li>
                                                                                    @endif

                                                                                    @if ($data->is_highrisk != 0)
                                                                                        <li class="bg-red"
                                                                                            title="High Risk">H</li>
                                                                                    @endif

                                                                                    @if ($data->is_mlc != 0)
                                                                                        <li style="background-color:#6effc3"
                                                                                            title="MLC">M</li>
                                                                                    @endif

                                                                                    @if ($data->referred_to_status != 0)
                                                                                        <li style="background-color:#75dfff"
                                                                                            title="Doctor Referral">R
                                                                                        </li>
                                                                                    @endif

                                                                                    @if ($data->health_checkup != 0)
                                                                                        <li style="background-color:#5b7b85;color:white"
                                                                                            title="MLC">H</li>
                                                                                    @endif

                                                                                    @if ($data->is_walkin != 0)
                                                                                        <li style="background-color:#fd5bf5;color:white"
                                                                                            title="MLC">W</li>
                                                                                    @endif

                                                                                    @if ($shift_data != '')
                                                                                        <li style="background-color:#f37ea9;color:white; line-height: 1.5; width: 55px;"
                                                                                            title="{{ $shift_data }}">
                                                                                            {{ $shift_data }}</li>
                                                                                    @endif


                                                                                </ul>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <span title="slot time"
                                                                                class="badge_new btn-block"
                                                                                style="padding: 0; float: left; background: #fafcfa !important; border: none; box-shadow: none; text-align: left; font-size: 12px; color: #d9534f !important; ">{{ date('h:i A', strtotime($data->time_slot)) }}</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-3 padding_sm" style=" padding:0;">
                                            <div class="col-md-12 padding_sm" style="margin-top: 5px;">
                                                <span class="badge_new btn-block"
                                                    style="float: left; background: #fafcfa !important; border: none; box-shadow: none; text-align: center; ">{{ $data->visit_type }}</span>

                                                @if($is_gynecolgy_doctor == 1 )
                                                <button onclick="goToGynecologyPatient('{{ $data->patient_id }}')" class="btn btn-warning" style="color:white; float: left;" title="Gynecology">
                                                    <i class="fa fa-asterisk"></i>  
                                                </button>
                                                <button onclick="goToInfertilityPatient('{{ $data->patient_id }}')" class="btn btn-info" style="color:white; float: left;" title="Infertility">
                                                    <i class="fa fa-certificate"></i>  
                                                </button>
                                                @endif
                                               
                                                       
                                                @if ($data->waiting_time < 1440)
                                                    {{-- @if ($data->waiting_time < 0) --}}
                                                    {{-- @else --}}
                                                        <span title="waiting time" class="badge_new btn-block"
                                                            style="float: left; background: #fafcfa !important; border: none; box-shadow: none; text-align: center;">
                                                            {{ round($data->waiting_time) }}m
                                                        </span>
                                                    {{-- @endif --}}
                                                @endif

                                                @if ($data->call_token_status == 1)
                                                    <button onclick="callTokenByBookingId('{{ $data->booking_id }}')"
                                                        class="btn btn-primary call_token_btn btn-block"
                                                        style="color:white; float: left;" title="Call Token"><i
                                                            class="fa fa-phone"></i>  @if($data->token_called_at != "") Recall @else Call  @endif  </button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            <input type="hidden" class="totalSeenCount" value="{{ $seen_count }}" />
                            <div class="col-md-12">
                    </td>
                </tr>
            </tbody>

        </table>
    </div>

</div>
