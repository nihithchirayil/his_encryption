@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <div class="right_col" role="main">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        <div class="row padding_sm">
            <div class="col-md-12 padding_sm">
                <div class="x_panel">
                    <div class="col-md-10 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:110px;">
                                <div class="col-md-3 padding_sm">
                                    <div class="mate-input-box" style="height: 76px !important">
                                        <label class="filter_label ">Nurse Station</label>
                                        <div class="clearfix"></div>
                                        <select id="location_id" onchange="getNursingWardBed()"
                                            class="form-control select2">
                                            <option value="0">Select Nursing Station</option>
                                            <?php
                                   foreach ($location_list->toArray() as $key=>$val) {
                                      ?>
                                            <option value="<?= $key ?>"><?= $val ?></option>
                                            <?php
                                   }
                                    ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-9 padding_sm">
                                    <div class="mate-input-box" style="height: 76px !important">
                                        <label class="filter_label ">Selected Bed Only</label>
                                        <?php
                                     $from_type=0;
                    if (count($config_detail) != 0){
                        foreach ($config_detail as $each){
                            $font_class="";
                            if($each->value=='5'){
                                $font_class="red";
                            }else if($each->value=='1'){
                                $font_class="green";
                            }else if($each->value=='2'){
                                $font_class="blue";
                            }else if($each->value=='3'){
                                $font_class="orange";
                            }else if($each->value=='4'){
                                $font_class="gray";
                            }else if($each->value=='6'){
                                $font_class="yellow";
                                if($house_keeping=='1'){
                                    $from_type=2;
                                }
                            }else if($each->value=='7'){
                                    $font_class="lime";
                                if($nurse_status=='1'){
                                    $from_type=1;
                                }
                           }
                            ?>

                                        <div class="col-md-2 padding_sm">
                                            <div class="checkbox checkbox-warning inline no-margin">
                                                <input type="checkbox" onclick="getNursingWardBed()" class="bedstatus_check"
                                                    id="bed_statuscheck<?= $each->value ?>" value="<?= $each->value ?>">
                                                <label for="bed_statuscheck<?= $each->value ?>">
                                                    <i class="fa fa-circle <?= $font_class ?>">
                                                        <?= strtoupper($each->name) ?></i>
                                                </label>

                                            </div>
                                        </div>


                                        <?php }
                                } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm">
                        <div class="box no-border no-margin">
                            <div class="box-footer"
                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;min-height:110px;">

                                <?php
                                if($select_flag=='1'){
                                    $from_type=3;
                                ?>
                                <div class="col-md-12 padding_sm">
                                    <div class="radio radio-success inline no-margin">
                                        <input name="bedstatuschange" type="radio" id="bedstatus_ready" value="1">
                                        <label for="bedstatus_ready">
                                            Ready
                                        </label>
                                    </div>
                                    <div class="radio radio-success inline no-margin">
                                        <input name="bedstatuschange" type="radio" id="bedstatus_housekeeping" value="6">
                                        <label for="bedstatus_housekeeping">
                                            House Keeping
                                        </label>
                                    </div>
                                </div>
                                <?php } ?>

                                <div class="col-md-6 padding_sm" style="margin-top: 5px;">
                                    <div class="checkbox checkbox-warning inline no-margin">
                                        <input onclick="checkallBed()" type="checkbox" id="checkall_bed" value="">
                                        <label for="checkall_bed">
                                            Check All
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 padding_sm" style="margin-top: 5px;">
                                    <button onclick="changebedStatus(<?= $from_type ?>)"
                                        class="btn btn-primary btn-block" type="button">Save
                                        <i id="save" class="fa fa-save"></i></button>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="x_panel">
                    <div class="row padding_sm" style="min-height: 450px;">
                        <div class="col-md-12 padding_sm" id="bedlistdiv">

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/change_bed_status.js') }}"></script>

@endsection
