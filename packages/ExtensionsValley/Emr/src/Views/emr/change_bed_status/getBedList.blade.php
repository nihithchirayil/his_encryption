<div class="box no-border no-margin">
    <div class="box-footer" style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF">
        <div class="theadscroll" style="position: relative; height: 550px;">
            <?php
        if (count($data) != 0){
            $i=1;
            $location_name='';
            foreach ($data as $each){
                $disabled_status=0;
                $font_class="";
                if($each->value=='5'){
                    $font_class="red";
                }else if($each->value=='1'){
                    $font_class="green";
                }else if($each->value=='2'){
                    $font_class="blue";
                }else if($each->value=='3'){
                    $font_class="orange";
                }else if($each->value=='4'){
                    $font_class="gray";
                }else if($each->value=='6'){
                    $font_class="yellow";
                    if($house_keeping=='1' || $select_flag=='1'){
                        $disabled_status=1;
                    }
                }else if($each->value=='7'){
                    $font_class="lime";
                    if($nurse_status=='1' || $select_flag=='1'){
                        $disabled_status=1;
                    }
                }

                if($i>6){
                    $i=1;
                echo "<div class='clearfix'></div>";
                }
                if($location_name!=$each->location_name){
                    $i=1;
                    $location_name=$each->location_name;
                    ?>
            <div class='clearfix'></div>
            <div class="col-md-12 padding_sm bg-info text-center">
                <h4><?= $each->location_name ?></h4>
            </div>
            <div class='clearfix'></div>
            <?php
                }
                ?>
            <div class="col-md-2 padding_sm">
                <?php
                if($disabled_status==1){
                ?>
                <div class="checkbox checkbox-success inline no-margin" style="margin-top: 12px !important">
                    <input type="checkbox" class="columnname_check" id="bead_deatils<?= $each->bead_master_id ?>"
                        value="<?= $each->bead_master_id ?>">
                        <label for="bead_deatils<?= $each->bead_master_id ?>">
                            <i title="<?= strtoupper($each->bed_status_name) ?>"
                                class="fa fa-bed fa-3x <?= $font_class ?>"></i>
                            <span style="margin-top:-7px"><?= $each->bed_name ?></span>
                        </label>

                </div>
                <?php }else{
                    ?>
                <div style="margin-top: 12px !important;margin-left:25px;">
                        <label id="bead_deatils<?= $each->bead_master_id ?>">
                            <i title="<?= strtoupper($each->bed_status_name) ?>" style="margin-top:-7px"
                                class="fa fa-bed fa-3x <?= $font_class ?>"></i>
                            <span style="margin-top:-7px"><?= $each->bed_name ?></span>
                        </label>
                </div>

                <?php
                }
                $i++;?>
            </div>
            <?php
            }
        }else{
            ?>
            <div class="col-md-12 padding_sm text-center">
                <span class="">No Result Found</span>
            </div>
            <?php
        }
        ?>
        </div>
    </div>
</div>


