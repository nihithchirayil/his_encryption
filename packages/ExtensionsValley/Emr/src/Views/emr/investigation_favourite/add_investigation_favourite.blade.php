@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/ip-op-slide.css")}}" rel="stylesheet">
<style>
  select.user-select {
    font-size: 12px !important;
    height: 53px !important;
    /* height: 40px; */
    padding: 0 !important;
}
</style>

@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col"  role="main" style="min-height: 1124px;">

        <div class="row codfox_container">

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="form-group control-required">
                        <label><b>User</b></label>
                         <input type="text" name="search_user" id="search_user" style="cursor: pointer;" autocomplete="off" class="form-control" placeholder="Type at least 3 characters">
                        <input type="hidden" name="search_user_id_hidden" value="">
                                            <!-- User List -->

                                            <div class="user-list-div" style="display: none; cursor: pointer;">
                                                <a style="float: left;" class="close_btn_user_search">X</a>
                                                <div class=" user_theadscroll" style="position: relative;">
                                                  <table id="UserTable"  class="table table-bordered no-margin table_sm table-striped user_theadfix_wrapper">
                                                      <thead>
                                                        <tr class="light_purple_bg">
                                                          <th colspan="2">User</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody id="ListUserSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          <!-- User List -->
                    </div>
                </div>

               <div class="col-xs-12 col-sm-12 col-md-3">
                  <td width="3%" align="center">
                    <button class="btn light_purple_bg" style="margin-top:24px;" onclick="copyInvData();" >Copy</button></td>
                </div>

            <div class="clearfix"></div>
            <div class="ht10"></div>


            <div class="theadscroll always-visible" style="position: relative; height: auto">
                <div class="col-md-12 no-padding" style="scroll-behavior: smooth;">


                    <div class="col-md-12 padding_sm emr_sub">
                                         <style>

    .table_header_bg.header_normal_padding>th{
        padding: 8px !important;
    }
</style>




        <div class="tab-content">
              <div class="col-md-12" style="padding: 5px;">

              @include('Emr::emr.investigation_favourite.investigation_favourite_lists')
            <div id="drug" class="tab-pane fade in active">

                <div class="col-md-12" style="padding: 2px;">


                    <div class="clearfix"></div>
                    <div class="ht5"></div>


                            <table id="search_inves_type" style="display: table; width:98%;" class="table no-margin table_sm table-bordered">
                                <tbody>
                                    <tr>

                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="m_s_type_lab" value="lab" name="i_search_type" checked="">
                                                <label for="i_type_lab"> Lab </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="m_s_type_procedure" value="procedure" name="i_search_type">
                                                <label for="i_type_procedure"> Procedure </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="m_s_type_radiology" value="radiology" name="i_search_type">
                                                <label for="i_type_radiology"> Radiology </label>
                                            </div>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <table id="inves_list" style="display: table; width:98%;" class="table no-margin no-border">
                                <thead>
                                    <tr class="bg-default header_normal_padding" style="background-color: lightgrey">
                                        <th  colspan="2">Item</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="100%">
                                            <input type="text" name="search_inv_item" style="cursor: pointer;" autocomplete="off" class="form-control" placeholder="Type at least 3 characters">

                                             <input type="hidden" name="edit_id" value="">
                                              <input type="hidden" name="get_grp_id" value="">
                                            <input type="hidden" name="search_service_code_hidden" value="">
                                            <input type="hidden" name="search_type_hidden" value="">


                                            <!-- investigation List -->
                                            <div class="investigation-list-div" style="display: none; cursor: pointer;">
                                                <a style="float: left;" class="close_btn_med_search">X</a>
                                                <div class="inv_theadscroll" style="position: relative;">
                                                  <table id="InvestigationTable"  class="table table-bordered no-margin table_sm table-striped inv_theadfix_wrapper">
                                                      <thead>
                                                        <tr class="light_purple_bg">
                                                          <th colspan="2">Item</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody id="ListInvestigationSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          <!-- investigation List -->

                                        </td>


                                        <td width="3%" ><button class="btn light_purple_bg" onclick="addNewService();" >Save</button></td>
                                    </tr>

                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>

                            <div class="theadscroll" style="position: relative; height: 244px;">
                                <table border="1" style="width: 100%;height: 15%;border:1px solid;border-collapse: collapse !important" class="table  table_sm  table-bordered" id="investigation-listing-table">
                                    <tbody id="investigationfav-listing-table">

                                    </tbody>
                                </table>
                            </div>



                </div>
            </div>


        </div>
          </div>


                    </div>

                </div>
            </div>

        </div>


        <input type="hidden" id="base_url" value="{{URL::to('/')}}">
        <input type="hidden" id="c_token" value="{{csrf_token()}}">

       <div id="group_add_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:30%">

      <div class="modal-content">
        <div class="modal-header" style="background:#26b99a; color:#ffffff;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
          <h4 class="modal-title">Add Group</h4>
        </div>
        <div class="modal-body" id="group_add_data" >
            <label >Group name</label>
                            <input  class="form-control" value=""  type="text" placeholder="Group Name" id="groupname" name="groupname">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn bg-green" onclick="SaveGroup()">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
</div>


 <div id="user_list_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:30%">

      <div class="modal-content">
        <div class="modal-header" style="background:#26b99a; color:#ffffff;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
          <h4 class="modal-title">User</h4>
        </div>
        <div class="modal-body" id="user_list_data" >
           <div class="" style="position: relative; height: 90px;">
            <form id="user-data-form" action="">
              <table id="common_freq_table" class ="table table-striped theadfix_wrapper table-sm">

            <tbody>
               <div class="col-md-12 padding_sm">
                                     <div class="form-group control-required">
                          {!! Form::label('user_name', 'User') !!}
                       <select multiple="multiple" class="form-control user-select" name="user_name" id="user_name">
                                @foreach($user_list as $key => $list)

                                        <option value="{{$key}}">{{$list}}</option>

                                @endforeach
                      </select>
                    </div>
            </tbody>
        </table>
      </form>
      </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn bg-green" id="SaveUser">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
</div>


    </div>




@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>

<!-- Prescription -->
<script src="{{asset("packages/extensionsvalley/emr/js/investigation_favourite.js")}}"></script>

<script src="{{asset("packages/extensionsvalley/jquery-mousewheel-master/jquery.mousewheel.min.js")}}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/js/jquery-ui.min.js') }}" ></script>

<script type="text/javascript">


</script>
@endsection
