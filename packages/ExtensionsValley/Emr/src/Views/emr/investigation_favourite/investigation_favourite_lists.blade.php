
<!-- Medicine Favorite List -->
<div class="favorite-inves-list-popup" style="display: none;margin-left:10px;width:96%;">
   
    <div class="col-md-12 no-padding" style="border: 1px solid #ccc;">
        <div class="col-md-2 no-padding">
            <div class="group_list theadscroll always-visible" style="height: 460px;border-right: 1px solid #ccc;">
        <button class="btn light_purple_bg" style="float:right;" title="Add Group" onclick="addGroup()">Add Group</button>
         
           <table id="fav_groups" class="fav-groups-row table table-bordered no-margin table_sm table-striped">  
           </table>
            </div>
        </div>
        <div class="col-md-10 no-padding">

           <form id="investigation-data-form" action="">
                <table id="FavTableList"  class="table table-bordered no-margin table_sm table-striped ">
                    <thead>
                        <tr class="table_header_bg header_normal_padding">
                            <th width="50%">Item</th>
                             <th width="5%">Action</th>
                        </tr>
                    </thead>
                    <tbody id="ListInvesFavorites" >

                    </tbody>
                </table>
               </form>
          
        </div>
    </div>
</div>
<!-- Medicine Favorite List -->
