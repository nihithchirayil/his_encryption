@if(sizeof($data_list) > 0)
  @foreach($data_list as $key => $value)
  @php
    $id = (!empty($value->id)) ? $value->id : "";
    $service_code = (!empty($value->service_code)) ? $value->service_code : "";
    $service_name = (!empty($value->service_desc)) ? $value->service_desc : "";
    $type = (!empty($value->type)) ? $value->type : "";
     $groupid = (!empty($value->group_id)) ? $value->group_id : "";
    
   
  
  @endphp
              <input type="hidden" class="form-control group_id"  value="{{$groupid}}">

    <tr id="{{$id}}" class="{{$groupid}}" data-group-id="{{ $groupid }}">
        <td width="50%">
            {{$service_name}}
            <input type="hidden" class="form-control" name="fav_service_name[]" value="{{$service_name}}">
            <input type="hidden" class="form-control" name="fav_service_code[]" value="{{$service_code}}">
             <input type="hidden" class="fav_check" name="favitemid[]" value="{{$id}}">
        </td>
      
         <td width="5%" style="text-align:center;">
             <button class="btn btn-success edit-investigation-row" title="Edit" type="button"><i  class="fa fa-edit"></i></button> &nbsp;<button title="Delete" class="btn  btn-danger delete-investigation-row"  type="button"><i  class="fa fa-trash-o"></i></button>
        </td>
    </tr>
  @endforeach
@else
<tr style="text-align: center;">
    <td colspan="8" id="td_1">No Data Found.!</td>
</tr>
@endif
