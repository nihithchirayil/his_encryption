@extends('Dashboard::dashboard.dashboard')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
{{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<style>
</style>
@endsection
@section('content-area')
<!-- page content -->
<div class="right_col" >
<div class="row" style="text-align: right; font-size: 12px;font-weight: bold;"> {{$title}}
<div class="clearfix"></div></div>
    <div class="row codfox_container">
        <div class="col-md-12 padding_sm">
            <div class="col-md-12 no-padding">
                <div class="box no-border" >
                    <div class="box-body clearfix">
                       


                             <div class="col-md-4 padding_sm" style="margin-top: 25px; margin-left: 1160px;">
                         <button  data-toggle="modal" data-target="#sentinal_event_modal" class="btn bg-primary"><i class="fa fa-plus"></i> Add</button>
                    </div>
                       

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="box no-border no-margin">
                <div class="box-body clearfix">
                    <div class="theadscroll" style="position: relative; height: 350px;">
                        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                               style="border: 1px solid #CCC;">
                            <thead>
                                <tr class="table_header_bg" style="cursor: pointer;">
                                    <th>Event Name</th>
                                    <th>Event Start Time</th>
                                    <th>Event Stop Time</th>
                                    <th >Event Type</th>
                                   
                                <th>Delete/Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($item) > 0)
                                @foreach ($item as $item)

                                <tr style="cursor: pointer;"  >
                                   
                                    <td class="common_td_rules">{{ $item->event_name }}<input type="hidden" class="fav_check" name="sent_evnt_id[]" value="{{$item->id}}"><input type="hidden" class="form-control" name="fav_event_name[]" value="{{$item->event_name}}"></td>
                                      <td  class="common_td_rules">{{date('M-d-Y h:i A', strtotime($item->event_start_time))}}<input type="hidden" class="form-control" name="fav_event_start_time[]" value="{{date('h:i A', strtotime($item->event_start_time))}}"><input type="hidden" class="form-control" name="fav_event_start_date[]" value="{{date('M-d-Y', strtotime($item->event_start_time))}}"></td>
                                       <td class="common_td_rules">{{date('M-d-Y h:i A', strtotime($item->event_stop_time))}}<input type="hidden" class="form-control" name="fav_event_stop_time[]" value="{{date('h:i A', strtotime($item->event_stop_time))}}"><input type="hidden" class="form-control" name="fav_event_stop_date[]" value="{{date('M-d-Y', strtotime($item->event_stop_time))}}"></td>
                                       <td class="common_td_rules">{{ $item->name }}<input type="hidden" class="form-control" name="fav_name[]" value="{{$item->et_id}}"><input type="hidden" class="form-control" name="fav_description[]" value="{{$item->event_description}}"></td>
                                      <td>
             <button class='btn btn-sm btn-default delete-sentinal-event'><i class="fa fa-trash"></i></button><button class='btn btn-sm btn-default edit-sentinal-event'><i class="fa fa-edit"></i></button>
        </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="12" class="location_code">No Records found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                   <div class="col-md-12 text-center">
    <ul class="pagination purple_pagination pull-right">
        {!! $page_links !!}
    </ul>
</div>
                </div>
            </div>
            <input type="hidden" name="domain_url" id="domain_url" value="{{url('/')}}"/>
        </div>
    </div>
</div>
<!-- doctor service division modal start -->
 <div id="sentinal_event_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-green">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sentinal Event</h4>
        </div>
        <div class="modal-body" id="doctor_service_division_details">
              {!! Form::open(['doctor_service_form','id'=>'doctor_service_form']) !!}

<input type="hidden" class="form-control" name="edit_sent_evnt" id="edit_sent_evnt">

                  {!! Form::label('event_name', 'Event Name') !!}
                       {!! Form::text('event_name', NULL, ['class'=>'form-control']) !!}
                        <br>

                    {!! Form::label('event_start_date', 'Event Start Date') !!}
                        <input type="text" id='event_start_date' name='event_start_date' class="form-control datepicker" value="" data-attr="date" placeholder="Event Start Date">
                          <br>
                     {!! Form::label('event_start_time', 'Event Start Time') !!}
                        <input type="text" id='event_start_time' name='event_start_time' class="form-control timepicker" value="" data-attr="date" placeholder="Event Start Time">
                          <br>
 {!! Form::label('event_stop_time', 'Event Stop Date') !!}
                        <input type="text" id='event_stop_date' name='event_stop_date' class="form-control datepicker" value="" data-attr="date" placeholder="Event Stop Date">
                          <br>
                        {!! Form::label('event_stop_time', 'Event Stop Time') !!}
                        <input type="text" id='event_stop_time' name='event_stop_time' class="form-control timepicker" value="" data-attr="date" placeholder="Event Stop Time">
                          <br>
{!! Form::label('event_description', 'Event Description') !!}
                           <textarea style="overflow-y:scroll; resize:vertical; border:1px solid #dddddd; width:100%; height: 100px" cols="50" id="event_description" name="event_description"></textarea>
                            <br>
                  
                  {!! Form::label('event_type', 'Event Type') !!}
                        {!! Form::select('event_type', $event_type, null, [
                            'class'       => 'form-control',
                             'placeholder' => 'Select event type',
                             'id'          => 'event_type'
                        ]) !!}
                          <br>

              {!! Form::close() !!}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn bg-green" onclick="addsentinalevent()"> <i class="fa fa-check"></i> Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
{!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
{!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}

{!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
{!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
{!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}

 <script src="{{asset("packages/extensionsvalley/default/js/loadingoverlay.min.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function() {

    setTimeout(function() {
    $('.theadscroll').perfectScrollbar("update");
    $(".theadfix_wrapper").floatThead('reflow');

    }, 300);
   
   
   
    var $table = $('table.theadfix_wrapper');
    $table.floatThead({
    scrollContainer: function($table) {
    return $table.closest('.theadscroll');
    }

    });
    $('.datepicker').datetimepicker({
    format: 'DD-MMM-YYYY'
    });
     $('.timepicker').datetimepicker({
    format: 'hh:mm A'
    });
  //  $('.date_time_picker').datetimepicker();
    $('.theadscroll').perfectScrollbar({
    wheelPropagation: true,
            minScrollbarLength: 30
    });
    


            });</script>

@stop

@section('javascript_extra')
<script type="text/javascript">
   
  
    function addsentinalevent() {
    let edit_sent_evnt = $("#edit_sent_evnt").val();
    //alert(edit_sent_evnt);
    let event_name = $("#event_name").val();
    let event_start_date = $("#event_start_date").val();
    let event_start_time = $("#event_start_time").val();
    let event_stop_date = $("#event_stop_date").val();
    let event_stop_time = $("#event_stop_time").val();
    let event_type = $("#event_type").val();
    let event_description = $("#event_description").val();
  
  if(event_name ==''){
    toastr.warning("Event Name Required");
  }else if(event_start_date==''){
         toastr.warning("Event Start Date Required");
  }else if(event_start_time ==''){
         toastr.warning("Event Start Time Required");
  }else if(event_stop_date==''){
         toastr.warning("Event Stop Date Required");
  }else if(event_stop_time==''){
         toastr.warning("Event Stop Time Required");
  }else if(event_type==''){
         toastr.warning("Event Type Required");
  }

  else{

       if (edit_sent_evnt != '' && edit_sent_evnt != 0) { 
            deleteSentinalRow(edit_sent_evnt);
        }

        var url =  $('#domain_url').val() + "/sentinalevent/save-sentinal-event";
        $.ajax({
            type: "GET",
            url: url,
            data: 'event_name=' + event_name + '&event_start_time=' + event_start_time + '&event_stop_time=' + event_stop_time + '&event_type=' + event_type + '&event_description=' + event_description+ '&event_start_date=' + event_start_date+ '&event_stop_date=' + event_stop_date,
            beforeSend: function() {
                $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
            },
            success: function(response) { 
                if (response.status == 1) {
                  
                    Command: toastr["success"]("Saved.");
                     window.location.href = $('#domain_url').val() + "/sentinalevent/listSentinalEvent/";
               
                } else {
                  
                    Command: toastr["error"]("Error.");
                }
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
            }
        });
    }
    }

    $(document).on('click', '.delete-sentinal-event', function () { 
    if (confirm("Are you sure you want to delete.!")) {
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="sent_evnt_id[]"]').val();

        if (edit_id != '' && edit_id != 0) {
            deleteSentinalRowFromDb(edit_id);
            $(tr).remove();
        } else {
            $(tr).remove();
        }
    }
});

    //delete single row from db
function deleteSentinalRowFromDb(id) {  
    var url = $('#domain_url').val() + "/sentinalevent/delete-sentinal-event";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {
             $("body").LoadingOverlay("show", { background: "rgba(255, 255, 255, 0.7)", imageColor: '#337AB7' });
        },
        success: function (data) {
            if (data != '' && data != undefined && data != 0) {
                Command: toastr["success"]("Deleted.");
            }
        },
        complete: function () {
             $("body").LoadingOverlay("hide");
        }
    });
}

function deleteSentinalRow(id) {  
    var url = $('#domain_url').val() + "/sentinalevent/delete-sentinal-event";
    let _token = $('#c_token').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id,
            _token: _token,
        },
        beforeSend: function () {
           
        },
        success: function (data) {
          
        },
        complete: function () {
           
        }
    });
}

$(document).on('click', '.edit-sentinal-event', function () { 

         $('#sentinal_event_modal').modal('show');
        let tr = $(this).closest('tr');
        let edit_id = $(tr).find('input[name="sent_evnt_id[]"]').val();
        let fav_event_name =  $(tr).find('input[name="fav_event_name[]"]').val();
        let fav_event_stop_time =  $(tr).find('input[name="fav_event_stop_time[]"]').val();
        let fav_event_stop_date =  $(tr).find('input[name="fav_event_stop_date[]"]').val();
        let fav_event_start_time =  $(tr).find('input[name="fav_event_start_time[]"]').val();
        let fav_event_start_date =  $(tr).find('input[name="fav_event_start_date[]"]').val();
        let fav_name =  $(tr).find('input[name="fav_name[]"]').val();
         let fav_description =  $(tr).find('input[name="fav_description[]"]').val();
    
        if (fav_name != '' && edit_id != '') {
           
            $("#event_type option[value="+ fav_name +"]").attr('selected', 'selected');
            $('input[name="event_name"]').val(fav_event_name);
            $('input[name="event_stop_time"]').val(fav_event_stop_time);
            $('input[name="event_stop_date"]').val(fav_event_stop_date);
            $('input[name="event_start_time"]').val(fav_event_start_time);
            $('input[name="event_start_date"]').val(fav_event_start_date);
            $('#event_description').val(fav_description)
            $('input[name="edit_sent_evnt"]').val(edit_id);
          
        } 
});

</script>

@endsection
