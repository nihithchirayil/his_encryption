<div id="parent" style="overflow: auto;" id="patient_allergy_add_edit_content">
    <div id="allergyHistoryEditModalContent">
        <!-- Prescription Details -->
                    <div style="padding:0 15px 15px 15px;">

                      <!-- Top Button Start Here -->
                      <table width= "100%">
                        <tr>
                          <td style="border: none;" width="15%">
                           <button type="button" class="btn btn-success btn-block" data-toggle="tooltip" data-placement="left"  id="add_allergy"><span class="fa fa-medkit fa-lg" ></span> Add Allergy</button>
                          </td>
                          <td style="border: none;" width="85%">

                          </td>
                        </tr>
                      </table>


                      <!-- Top Button Ends Here -->
<!--                   {!! Form::open(['name'=>'frm-prescription','id'=>'frm-prescription','method'=>'post','route'=>'']) !!}-->
                   <!-- Head Table Goes Here -->
                   <table width="100%" class="head-table" style="border: 0px">
                      <tr>
                      </tr>
                    </table>
                    <!-- Head Table Ends Here -->
                    <div class="table-responsive" id="allergy_Scroll" style="border: none;min-height:50px;overflow: auto;" >
                    <table border="0" width="95%" id="allergyTable" class="head-table" >
                      <tr class="headergroupbg">
                        <td>Allergy Type</td>
                        <td>Allergy Item</td>
                        <td>Trash</td>
                      </tr>
                      <tbody id="AllergyListData">
                      	<?php //dd($patient_details['allergy_data']); ?>

						@if(isset($allergy_data) && count($allergy_data))
                      	@foreach($allergy_data as $allergy_data_index=>$allergy_data_val)
                      	<tr>
                            <td width="35%">
                                <input type="text" class="form-control btn-without-border" value="{!! $allergy_data_val->allergytypename or \Input::old('allergytype')!!}" onKeyup='searchAllergyType(this.id,event,{{ $allergy_data_index }})' autocomplete='off' placeholder="Allergy Type" name="allergytype[]" id="allergytype-e{{ $allergy_data_index }}">
                              <div class="ajaxSearchBox" id="allergyTypeAjaxList-e{{ $allergy_data_index }}" style="display:none; text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px;overflow-y: auto; width:250px; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;border: 1px solid rgba(0, 0, 0, 0.3);"></div>
                              <input type="hidden" id="allergyTypeCodeId-e{{ $allergy_data_index }}" name="allergytype_code_hidden[]" value="{!!  $allergy_data_val->allergytypeid !!}" >
                            </td>
                            <td width="55%">
                              <input type="text" class="form-control btn-without-border" placeholder="Allergy" value="{!! $allergy_data_val->allergies_name or \Input::old('allergy')!!}" onKeyup='searchAllergy(this.id,event,{{ $allergy_data_index }})' autocomplete='off' name="allergy[]" id="allergy-e{{ $allergy_data_index }}">
                              <div class="ajaxSearchBox" id="allergyAjaxList-e{{ $allergy_data_index }}" style="display:none; text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px;overflow-y: auto; width:350px; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;border: 1px solid rgba(0, 0, 0, 0.3);"></div>
                              <input type="hidden" id="AlmedCodeId-e{{ $allergy_data_index }}" name="allergy_code_hidden[]" value="{!!  $allergy_data_val->allergyid !!}" >
                            </td>
                            <td width="15%" style="text-align:center; ">
                              <div style="text-align:center; ">
                              <button class="btn btn-danger btn-circle btn-xs" tabindex="-1" data-toggle="tooltip" id="delete-e{{ $allergy_data_index }}" data-placement="top"
                              onclick="deleteNewRow(this);"  ><i class="fa fa-trash" aria-hidden="true"></i> </button>
                              </div>
                            </td>
                          </tr>

                      	@endforeach
                      	@endif

                       <?php
                       $fa = sizeof((array)\Input::old('allergy'));$loopSize = 5;
                       if($fa >= 5){
                           $loopSize = $fa;
                       }
                       ?>
                      @for ($j = 1; $j <= $loopSize; $j++)

                          <tr>
                            <td width="35%">
                              <input type="text" class="form-control btn-without-border" placeholder="NKA"  value="{{\Input::old('allergytype.'.($j-1))}}" onKeyup='searchAllergyType(this.id,event,{{ $j }})' autocomplete='off' name="allergytype[]" id="allergytype-{{ $j }}">
                              <div class="ajaxSearchBox" id="allergytypeAjaxList-{{ $j }}" style="display:none; text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px;overflow-y: auto; width:250px; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;border: 1px solid rgba(0, 0, 0, 0.3);"></div>
                              <input type="hidden" id="allergyTypeCodeId-{{ $j }}" name="allergytype_code_hidden[]" value="{{\Input::old('allergytype_code_hidden.'.($j-1))}}">
                            </td>
                            <td width="55%">
                              <input type="text" class="form-control btn-without-border" placeholder="NKA"  value="{{\Input::old('allergy.'.($j-1))}}" onKeyup='searchAllergy(this.id,event,{{ $j }})' autocomplete='off' name="allergy[]" id="allergy-{{ $j }}">
                              <div class="ajaxSearchBox" id="allergyAjaxList-{{ $j }}" style="display:none; text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: -2px 0px 0px 0px;overflow-y: auto; width:350px; z-index: 599;  position:absolute; background: #ffffff;  border-radius: 3px;border: 1px solid rgba(0, 0, 0, 0.3);"></div>
                              <input type="hidden" id="AlmedCodeId-{{ $j }}" name="allergy_code_hidden[]" value="{{\Input::old('allergy_code_hidden.'.($j-1))}}">
                            </td>
                            <td width="15%" style="text-align:center; ">
                              <div style="text-align:center;">
                              <button class="btn btn-danger btn-circle btn-xs" tabindex="-1" data-toggle="tooltip" id="delete-{{ $j }}" data-placement="top"
                              onclick="deleteNewRow(this);"  ><i class="fa fa-trash" aria-hidden="true"></i> </button>
                              </div>
                            </td>
                          </tr>

                       @endfor

                      </tbody>
                    </table>
                   </div>

<!--                      {!! Form::token() !!}
                      {!! Form::close() !!}-->

                    <!-- Text Area Goes Here -->
                    <div class="row">
                    <div class="col-md-12">
                      <br>
                    <!--<textarea style="min-height: 70px;" id="description" name="description" class="form-control" style="margin-top:15px;">This is content of medi....</textarea>-->
                    </div>
                    </div>
                    <!-- Text Area Ends Here -->

                    <!-- Warning Model Popup Goes Here -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                       <div class="modal-header" style="background:#f0ad4e; color:#fff;" >
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Warning</h4>
                       </div>
                       <div class="modal-body"> Warning text goes here </div>
                       <div class="modal-footer" style="border-top:1px solid #ddd;">
                        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                       </div>
                    </div>
                    </div>
                    </div>
                    <!-- Warning Model Popup Ends Here -->

               <div class="col-md-12">
		<div  class="col-md-6"></div>
		<div  class="col-md-6">
			<button style='margin-right: 15px;' class="btn btn-primary btn-sm pull-right" data-toggle="tooltip" title="Save" name="patient_allergy_add_edit_save" id="patient_allergy_add_edit_save" style="float: right;"><i id="patient_allergy_add_edit_save_spin" class="fa fa-save"></i>&nbsp;Save</button>
		</div>
	       </div>



            </div>
            </div>

</div>
