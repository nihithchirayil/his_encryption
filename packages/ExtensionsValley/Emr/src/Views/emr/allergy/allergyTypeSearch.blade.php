<?php
if (isset($alergyTypeDetails)) {

    if(!empty($alergyTypeDetails) && count($alergyTypeDetails) > 0 ){
        foreach ($alergyTypeDetails as $item) {
    ?>
            <li style="display: block; padding:5px 10px 5px 5px; "
                onclick='fillallegeryTypeValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->allergy_type)}}")'>
                <?php echo htmlentities($item->allergy_type);?>
            </li>
    <?php  }
    } else {
        $add = '<span style="color: #72AFD2;" data-toggle="modal" onclick="AddNewAllergyType(this)" data-target="#addAllergyType">Add</span>';
        echo 'No Results Found '.$add;
    }
}
