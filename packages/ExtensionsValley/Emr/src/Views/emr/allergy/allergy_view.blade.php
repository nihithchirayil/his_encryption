@if((isset($allergy_details)))
    @if(count($allergy_details) > 0)
        @foreach ($allergy_details as $key => $value)

            <tr id='<?php if(isset($key)) echo $key; ?>' >
                <td width="100%" colspan="2" align="left">
                    <div class="text-left" title="<?php if(isset($value['created_at'])) echo $value['created_at']; ?>">
                    <h4 class="no-margin" style="font-size: 11px;" ><?php if(isset($value['Item'])) echo ucfirst($value['Item']); ?> <?php if(isset($value['Category'])) echo '('.ucfirst($value['Category']).')'; ?></h4>
                    </div>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="3" align='center'>
                <span >No Records Found</span>
            </td>
        </tr>
    @endif
@endif

@if(isset($otherAllergies)))
@if(count($otherAllergies) > 0)
    <tr >
        <td width="100%" colspan="2" align="left">
            <div class="ht10"></div>
        </td>
    </tr>
    <tr >
        <td width="100%" colspan="2" align="left">
            <div class="text-left">
                <h4 class="no-margin" style="font-size: 11px;" ><b>Other Allergies: </b></h4>
            </div>
        </td>
    </tr>
    @foreach ($otherAllergies as $key => $value)
        <tr id='otherAllergiesItem' >
            <td width="100%" colspan="2" align="left">
                <div class="text-left" title="<?php if(isset($value->created_at)) echo $value->created_at; ?>">
                <h4 class="no-margin" style="font-size: 11px;" ><?php if(isset($value->allergy)) echo ucfirst($value->allergy); ?> </h4>
                </div>
            </td>
        </tr>
    @endforeach
@endif
@endif
