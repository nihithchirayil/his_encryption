<?php
if (isset($allergymedicineCodeDetails)) {

    if (!empty($allergymedicineCodeDetails)) {

        foreach ($allergymedicineCodeDetails as $item) {

            $selctedFlag = 0;
            if (isset($allergycurrentMedicines)) {
                if (in_array($item->item_code, $allergycurrentMedicines)) {
                    $selctedFlag = 1;
                }
            }
            if($selctedFlag == 0){

?>
                <li style="display: block; padding:5px 10px 5px 5px; "
                    onclick='fillAllergyItemValues(this,"{{htmlentities($item->item_code)}}","{{htmlentities($item->item_desc)}}","{{htmlentities($item->chemical_name)}}","{{ $alergy_type }}")'>
                  {{ htmlentities($item->chemical_name)}}    {{htmlentities($item->item_desc)}}
                </li>

            <?php
            }
        }
    } else {

        $add = '<span style="color: #72AFD2;" data-toggle="modal" onclick="AddNewMedicine(this)"></span>';
        echo 'No Results Found ' . $add;
    }
}
