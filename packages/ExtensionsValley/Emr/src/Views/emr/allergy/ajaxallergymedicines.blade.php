<table width="100%" class="table_sm no-border">
    @if(isset($resultAllergy) && count($resultAllergy) > 0)
        @php
            $j = 1;
        @endphp
        @foreach($resultAllergy as $result)
        <?php if($result->type == 0)
        $type= 'Brand';
        else if($result->type == 1)
        $type = 'Generic';

        ?>
        <tr>
            <td width="90%">
                <input type="text" readonly="readonly" class="form-control bottom-border-text" value="{{trim($result->description)}}&nbsp;- ({{$result->generic_name}})" onfocus="PreviewDescription(this)"
                        onKeyup='searchMedicine2(this.id,event)' autocomplete='off' placeholder=""
                        data-med="0"
                        name="allergymedicine[]" id="allergymedicine-{{$j}}">
                <div class="ajaxSearchBox" id="alergymedicineAjaxList-{{$j}}"
                        style="width:100%;max-height: 515px;">
                </div>
                        <input type="hidden" value="{{trim($result->item_code)}}" id="AllergymedCodeId-{{ $j }}" name="alergylist_medicine_code_hidden[]" >
                        <input type="hidden" name="alrg_type[]" id="alrg_type-{{$j}}" value="{{$result->type}}">
                        <input type="hidden" name="saved_hidden_id[]" id="saved_hidden_id-{{$j}}" value="{{$result->id}}">
            </td>
            <td>
                <button type="button" class="btn btn-danger" style="" value=""
                        onclick="deleteNews(this)"
                        id="delete-{{ $j }}"><i class="fa fa-times-circle" aria-hidden="true"></i>
                </button>
            </td>
        </tr>
         {{--*/$j++;/*--}}
    @endforeach
    @else
    <?php
    for($i=1;$i<4;$i++){
    ?>
        <tr>
           <td width="90%">
               <input type="text" class="form-control bottom-border-text" value="" onfocus="PreviewDescription(this)"
                      onKeyup='searchMedicine2(this.id,event)' autocomplete='off' placeholder=""
                      data-med="0"
                      name="allergymedicine[]" id="allergymedicine-{{$i}}">
               <div class="ajaxSearchBox" id="alergymedicineAjaxList-{{$i}}"
                    style="width:100%;max-height: 515px;"></div>
                    <input type="hidden" value="" id="AllergymedCodeId-{{ $i }}" name="alergylist_medicine_code_hidden[]" >
                                      <input type="hidden" name="alrg_type[]" id="alrg_type-{{$i}}">

           </td>
           <td>
               <button type="button" class="btn btn-danger" style="" value=""
                       onclick="deleteNews(this)"
                       id="delete-{{ $i }}"><i class='fa fa-times-circle' aria-hidden='true'></i>
               </button>
           </td>
         </tr>
    <?php
    }
    ?>
    @endif
    </table>
