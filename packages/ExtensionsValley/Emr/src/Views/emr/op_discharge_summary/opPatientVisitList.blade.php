<div class="theadscroll always-visible" style="position: relative; height: 330px;">
    <?php
 
    if (isset($visitData) ) {
       
        if(sizeof($visitData)>0){
            ?>
                <table class="table table-responsive theadfix_wrapper table-hover floatThead-table">
                    <thead>
                    <tr class="headergroupbg">
                        <th>Visit Dates</th>
                    </tr>
                 </thead>
           <?php
           foreach ($visitData as $visits) {

//                ?>
                 <tr style="cursor: pointer;"  onclick="selectPatientVisits('{!!$visits->visit_id!!}')">
                        @if($visits->visit_date !='')
                          <td>{!! date('M-d-Y h:i A',strtotime($visits->visit_date))!!}</td>  
                        @endif
                </tr>



                <?php
            }?>
            </table>
                <?php
        } else {
            echo " No admissions found! ";
        }
    }

?>
</div>

