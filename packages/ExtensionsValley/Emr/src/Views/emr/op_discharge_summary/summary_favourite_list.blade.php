<?php
$i=0;
$count = sizeof($favoriteList);
?>
<label for="Fillables"><b><u>Fillables </u></b></label>
<div class="col-md-12" style="border:1px solid #F1EDED;margin-bottom:20px;">
    <table id="fillablesTable" style="width:100%;text-align:right">
        <tr>
            <td>Diagnosis <input type="checkbox" name="diagnosis" id="diagnosis" value="diagnosis" /></td>
            <td>Allergies <input type="checkbox" name="allergies" id="allergies" value="allergies" /></td>
            <td>Surgery <input type="checkbox" name="surgery" id="surgery" value="surgery" /></td>
            <td>History & Physical Findings <input type="checkbox" name="history_findings" id="history_findings" value="history_findings" /></td>
        </tr>
        <tr>
            <td>Surgery Findings & Treatments <input type="checkbox" name="surgery_findings_treatments" id="surgery_findings_treatments" value="surgery_findings_treatments" /></td>
            <td>Treatment & Progress <input type="checkbox" name="treatment_progress" id="treatment_progress" value="treatment_progress" /></td>
            <td>Course in Hospital <input type="checkbox" name="course_in_hospital" id="course_in_hospital" value="course_in_hospital" /></td>
            <td>Discussion <input type="checkbox" name="discussion" id="discussion" value="discussion" /></td>
        </tr>
        <tr>
            <td>Investigation <input type="checkbox" name="investigation" id="investigation" value="investigation" /></td>
            <td>Plan <input type="checkbox" name="plan" id="plan" value="plan" /></td>
            <td>Advice on Discharge <input type="checkbox" name="advice_on_discharge" id="advice_on_discharge" value="advice_on_discharge" /></td>
            <td>Addendum <input type="checkbox" name="addendum" id="addendum" value="addendum" /></td>
        </tr>
    </table>
</div>
<table class="table table-striped table_sm">
    <thead>
        <tr>
            <th>Slno</th>
            <th>Date</th>
            <th>Doctor</th>
            <th>UHID</th>
            <th>Patient Name</th>
            <th colspan="2">List</th>
        </tr>
    </thead>
    <tbody id="ListLoopBody">
        @if($count > 0)
        @php
            $i = 1;
        @endphp
        @foreach($favoriteList as $list)
            <tr id="{{$list->visit_id}}">
                <td>{{$i}}</td>
                <td>{{date('M-d-Y h:i A',strtotime($list->created_at))}}</td>
                <td>{{$list->doctor_name}}</td>
                <td>{{$list->uhid}}</td>
                <td>{{$list->patient_name}}</td>
                <td style="width:12%;">
                    <div class="hidden" id="final_data-{{$i}}">

                    </div>
                    <button class="btn btn-sm btn-primary" onclick="fillData({{$list->visit_id}})"> <i class="fa fa-plus"></i></button>
                    <button class="btn btn-sm btn-info" onclick="loadViewData('{{$list->visit_id}}')"> <i class="fa fa-eye"></i></button>
                    <button class="btn btn-sm btn-danger" onclick="deleteFavouriteSummary('{{$list->visit_id}}')"> <i class="fa fa-trash"></i></button>
                </td>
            </tr>
            <?php
            $i++;
            ?>
        @endforeach
        @else
            <tr>
                <td colspan="7"> No Data Found ..! </td>
            </tr>
        @endif
        </tbody>
    </table>
