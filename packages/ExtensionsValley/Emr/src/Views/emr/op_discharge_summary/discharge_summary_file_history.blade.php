
<div class="theadscroll always-visible" style="position: relative; height: 470px;">
    <table class="table table-striped table-bordered table-condensed theadfix_wrapper">

               @if(sizeof($res)>0)
               <thead>
                   <tr class="headergroupbg" style="text-align: left;font-size:14px; background-color: #d2e8fc;">
                   <th>#</th>
                   <th>Patient Name</th>
                   <th>UHID</th>
                   <th>Doctor Name</th>
                   <th>Address</th>
                   <th>visit Date</th>
                   <th>Summary Prepared By</th>
                   <th>Created At</th>
                   <th>Last Updated At</th>
                   <th>Discharge Summary</th>
                </tr>
               </thead>
                <tbody>
                    @php
                        $i=1;
                    @endphp
                @foreach($res as $data)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$data->patient_name}}</td>
                    <td>{{$data->uhid}}</td>
                    <td>{{$data->doctor_name}}</td>
                    <td>{{$data->address}}</td>
                    <td>{{$data->visit_date}}</td>
                    <td>{{$data->created_by}}</td>
                    <td>{{$data->created}}</td>
                    <td>{{$data->last_updated}}</td>
                    <td style="text-align:center"><a target="_blank" onclick="createSummaryPrintFile('{{$data->summary_id}}')">
                          <i data-toggle="tooltip" data-placement="top" title="" class="fa fa-book fa-lg" aria-hidden="true" data-original-title="Summary File"></i>
                        </a>
                    </td>
                </tr>
                @php
                        $i++;
                    @endphp
                @endforeach
                @else
                <tr>
                    <td colspan="6">No Results Found</td>
                </tr>
           </tbody>
        </table>
</div>
       @endif
