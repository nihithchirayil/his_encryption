@php
    $summary_fields = [];
    foreach($summary_order as $data){
        $summary_fields[$data->order_no]['field_name'] = $data->field_name;
        $summary_fields[$data->order_no]['field_label'] = $data->field_label;
    }
        // echo '<pre>';
        // print_r($summary_fields);
        // exit;

    // $summaryTextHeaders = [
    //     'Diagnosis'=>'diagnosis',
    //     'Allergies'=>'allergies',
    //     'Surgery'=>'surgery',
    //     'History & Physical Findings'=>'history_findings',
    //     'Investigation'=>'investigation',
    //     'Surgery Findings & Treatments'=>'surgery_findings_treatments',
    //     'Treatment & Progress'=>'treatment_progress',
    //     'Course in Hospital'=>'course_in_hospital',
    //     'Discussion'=>'discussion',
    //     'Plan'=>'plan',
    //     'Advice on Discharge'=>'advice_on_discharge',
    //     'Conditions Requiring Urgent Care'=>'condition_of_patient',
    // ];
@endphp
{!!Form::open(['name'=>'frm-summary','id'=>'frm-summary','method'=>'post']) !!}

@foreach($summary_fields as $key => $value)

            <div class="col-md-12 padding_sm" style="margin-top:15px !important">
        <div class="col-md-12">
            <div class="col-md-10">
                <h5 style="font-size:15px;"><b>{!!ucwords($value['field_label'])!!}</b></h5>
             </div>
             <div class="col-md-2">
                 <button onclick="saveDischargeSummaryContent('{!!$value['field_name']!!}');" title="Save {!!$value['field_label']!!}" type="button" class="btn btn-success" style="float: right;
                 margin-right: -18px;
                 margin-top: 5px;
             "><i class="fa fa-save"></i> Save </button>
             </div>
        </div>
        <textarea class="tiny_editor" id="{!!$value['field_name']!!}"></textarea>
    </div>

@endforeach

{!! Form::token() !!}
{!! Form::close() !!}
