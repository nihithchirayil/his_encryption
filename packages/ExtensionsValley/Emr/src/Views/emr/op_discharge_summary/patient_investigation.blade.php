<?php
   if (sizeof($res) > 0) {
?>
 <div class="col-md-12">
    <table border="1" bordercolor="#808080" class="table table-condensed table-striped table_sm table-borderd" id="" style="border-collapse: collapse !important; font-size:14px;padding:3px;">
        <thead>
            <tr style="background-color:rgb(211, 209, 209);">
                <th style="width:12%">Date</th>
                <th style="width:20%">Test</th>
                <th style="width:20%">Description</th>
                <th style="width:9%;align:center;">Value </th>
                <th style="width:15%;align:center;">Normal Range</th>
                <th style="width:20%">Sub Test</th>
                <th style="width:10%">Remark</th>
            </tr>
        </thead>

        <tbody>

            <?php

                $service_description = '';
                $service_created_at = '';
                $totalData = sizeof($res) ? (!empty($res[0]->total_count) ? $res[0]->total_count : 0) : 0;
                $totalFiltered = $totalData;
                $viewData['res'] = $res;

                    $i = 1;
                    foreach ($res as $result) {

                        $min = "";
                        $max = "";
                        $direction = '';
                        $style = '';
                        if (!empty($result->min_value) && is_numeric($result->actual_result) && $result->max_value != "-") {

                            if (!empty($result->min_value)) {
                                $min = $result->min_value;
                            }

                            if (!empty($result->max_value)) {
                                $max = $result->max_value;
                            }

                            if($max < $result->actual_result && !empty($result->actual_result)){
                                $style = "style=color:red;";
                                $direction = '&nbsp;<i class="fa fa-arrow-up"></i>';
                            }
                            if($min > $result->actual_result && !empty($result->actual_result)){
                                $style = "style=color:#FF7701;";
                                $direction = '&nbsp;<i class="fa fa-arrow-down"></i>';
                            }
                        }

                        if($result->report_type == 'T'){
                            $html_content = "<button class='btn bg-primary' type='button' onclick='getLabResultRtf(".$result->id.")'><i class='fa fa-list'></i></button>";
                        }else if($result->report_type == 'N' || $result->report_type == 'F'){
                            $html_content = "<div class='text-left'  $style >".$result->actual_result.$direction."</div>";
                        }else{
                            $html_content = "";
                        }

                        $normal_range = "";
                        if(!empty($min)){
                            $normal_range = $min.' - '.$max;
                        }
                        $result_finalized = (!empty($result->result_finalized)) ? $result->result_finalized : 0;
                        if($result_finalized == 0){
                            $html_content = "";
                        }

                        // if($result->service_desc != $service_description){
                        //     $service_description = $result->service_desc;
                        // }else{
                        //     $service_description = '';
                        // }

                        // if($result->result_created_at != $service_created_at){
                        //     $data['created_at'] ='<span style="font-size:13px;">'.date('M-d-Y h:i A',strtotime($result->result_created_at)).'</span>';
                        // }else{
                        //     $data['created_at'] = '';
                        // }

                        $i++;
            ?>

            <tbody>
                <tr>
                    <td>
                        @if($service_description != $result->service_desc)
                            {!!date('M-d-Y h:i A',strtotime($result->result_created_at))!!}
                        @endif
                    </td>
                    <td>
                        @if($service_description != $result->service_desc)
                            {!!$result->service_desc!!}
                        @endif

                    </td>
                    <td>{!!$result->detail_description!!}</td>
                    <td>{!!$html_content!!}</td>
                    <td>{!!$normal_range!!}</td>
                    <td>{!!$result->sub_test_name!!}</td>
                    <td>{!!$result->remarks!!}</td>
                </tr>
                <?php

                    $service_description = $result->service_desc;

                  }
                }
                ?>
            </tbody>
          </table>
      </div>
