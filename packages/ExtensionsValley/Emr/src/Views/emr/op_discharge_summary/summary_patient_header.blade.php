@if (isset($patient_data))


    <table class='table table-condensed' border='1' bordercolor='#dddddd'
        style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
        <thead>
            <tr>
                <th id="discharge_summary_main_header" style='padding: 5px 0;' colspan='6' class='text-center'>
                    <h4 style='margin: 0;' class="summary_label"><b>TREATMENT SUMMARY</b></h4>
                </th>
            </tr>
        </thead>
        <tbody style=''>
            <tr>
                <td style=' padding-left:5px;;text-align:left; '>Patient Name</td>
                <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data['patient_name'] !!}</td>
                <td style=' padding-left:5px;;text-align:left; '>Age</td>
                <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data['age'] !!}</td>
                <td style=' padding-left:5px;;text-align:left;'>UHID</td>
                <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data['uhid'] !!}</td>

            </tr>

            <tr>

                <td colspan='1' style=' padding-left:5px;;text-align:left;'> Address</td>
                <td colspan='5' style=' padding-left:5px;;text-align:left;'>{!! @$patient_data['address'] ? $patient_data['address'] : '' !!}</td>

            </tr>

            <tr>
                <td style=' padding-left:5px;; text-align:left;'>DOV </td>
                @php
                    $visit_datetime = @$patient_data['visit_datetime'] ? $patient_data['visit_datetime'] : '';
                @endphp
                <td style=' padding-left:5px;; text-align:left;'>
                    @if ($visit_datetime)
                        {!! date('M-d-Y', strtotime($visit_datetime)) !!}
                    @endif
                </td>
                <td colspan="2" style=' padding-left:5px;;text-align:left; '>DOP</td>
                <td colspan="2" style=' padding-left:5px;; text-align:left;' id="summary_discharge_date_time">
                    @php
                        $date_of_procedure = @$patient_data['date_of_procedure'] ? $patient_data['date_of_procedure'] : '';
                        $time_of_prodedure = @$patient_data['time_of_prodedure'] ? $patient_data['time_of_prodedure'] : '';
                    @endphp
                    @if ($date_of_procedure)
                        {!! $date_of_procedure !!} {!! $time_of_prodedure !!}
                    @endif
                </td>
            </tr>

        </tbody>
    </table>
@endif
