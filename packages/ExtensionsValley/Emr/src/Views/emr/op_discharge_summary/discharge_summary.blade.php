@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/summary.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">

    <style>
        .mce-item-table,
        .mce-item-table td,
        .mce-item-table th,
        .mce-item-table caption {
            border-top: 1pt solid #000000 !important;
            border-right: 2px solid #000000 !important;
            border-bottom: 1pt solid #000000 !important;
            border-left: 2px solid #000000 !important;
        }

        .headerbtn {
            border: 1px solid green;
            min-width: 115px;
        }



        .tox .tox-tbtn {
            transform: scale(0.7) !important;
            height: 20px !important;
            /* width: unset !important; */
        }

        .tox .tox-mbtn {
            height: 20px !important;

        }

        .tox .tox-toolbar,
        .tox .tox-toolbar__overflow,
        .tox .tox-toolbar__primary {
            background: none !important;
            border-top: 1px solid #CCC;
            border-bottom: 1px solid #CCC;
        }

        .tox-tbtn__select-label {
            font-size: 16px !important;
            font-weight: 600 !important;
        }



        .round_btn {
            width: 35px !important;
            min-width: 0px !important;
            border-radius: 20px !important;
        }
        .round_btn:hover,.round_btn:focus {
            background-color: #00a65a !important;
            border-color: #00a65a !important;
            color:white !important;
        }

        #save_and_print_prescription1{
            display: none;
        }


        /* .summaryViewArea {
          background: white;
          width: 21cm;
          height: 29.7cm;
          display: block;
          margin: 0 auto;
          padding: 10px 25px;
          margin-bottom: 0.5cm;
          box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
          overflow-y: scroll;
          box-sizing: border-box;
          font-size: 12pt;
        }

        @media print {
          .page-break {
            display: block;
            page-break-before: always;
          }
          size: A4 portrait;
        }

        @media print {
          body {
            margin: 0;
            padding: 0;
          }
          .summaryViewArea {
            box-shadow: none;
            margin: 0;
            padding: 10px 25px;
            width: auto;
            height: auto;
          }
          .noprint {
            display: none;
          }
          .enable-print {
            display: block;
          }
        } */



    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <div class="right_col">
        <div class="container-fluid">
            <div class="col-md-12">
                <div class="container-fluid no-padding">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-3">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <h3 style="margin-left: -30px;" class=""> <b style="color:green">Treatment
                                            Summary</b></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <br>
                            <button title="Add Favourite" class="btn bg-default headerbtn round_btn" onclick="addToFavorites();"><i
                                    class="fa fa-star-o"></i></button>
                            <a title="Favorites" class="btn bg-default headerbtn round_btn" onclick="loadFavorites();">
                                <i class="fa fa-list fa-lg"></i>
                            </a>
                            <?php
                            $doctor_id_session = '';
                            ?>
                            <a  title="Finalize" id='summary_finalize_btn' class="btn bg-default headerbtn round_btn" onclick="approve_summary(1);">
                                <i class="fa fa-check fa-lg"></i>
                            </a>
                            <a title="De Finalize" id='summary_de_finalize_btn' class="btn bg-default headerbtn round_btn" onclick="approve_summary(2);">
                                <i class="fa fa-ban fa-lg"></i>
                            </a>

                            <a style="display:none"  title="Templates" id='summary_template_btn' class="btn bg-default headerbtn round_btn"
                                onclick="show_discharge_summary_templates();"><i class="fa fa-desktop"
                                    aria-hidden="true"></i>
                                </a>

                            <a style="display:none"  title="Death Summary" id='death_summary_template_btn' class="btn bg-default headerbtn round_btn"
                                onclick="fetch_death_summary_templates();"><i class="fa fa-desktop" aria-hidden="true"></i>

                            </a>
                            <a style="display:none"      title="Summary History" id='summary_history_btn' class="btn bg-default headerbtn round_btn" onclick="showSummaryModal();"><i
                                    class="fa fa-book" aria-hidden="true"></i>
                                </a>


                            <button   onclick="updatePreview();" id="op_patient_list_btn"
                                class="btn op_btn expand_btn anim btn-info headerbtn">
                                <i class="fa fa-desktop" aria-hidden="true"></i>
                                &nbsp;Save & Preview
                            </button>
                            <button onclick="summaryRefresh();" id="btn_summaryrefresh" class="btn bg-success headerbtn">
                                <i class="fa fa-refresh" aria-hidden="true"></i>
                                &nbsp;Refresh
                            </button>
                            </br>
                        </div>

                        <div class="col-md-1" style="visibility:hidden;">
                            @php
                                $finalized_by = isset($finalized_by) ? $finalized_by : 0;
                            @endphp
                            <span class="text-blue">Finalized By:</span>
                            {!! Form::select('finalized_by', $doctor_list, $finalized_by, ['class' => 'form-control filters ', 'placeholder' => 'finalized by', 'id' => 'finalized_by', 'style' => 'color:#555555; padding:4px 12px;font-weight:700;']) !!}
                        </div>



                        <div class="col-md-2">
                            <span class="text-blue">Select patient using UHID:</span>
                            <input type="text" name="op_no" autocomplete="off" value=""
                                class="form-control custom_floatinput" id="op_no" style="font-weight:700;">
                            <div style="width:140%;margin-left:-100px;" id="op_noAjaxDiv" class="ajaxSearchBox"></div>
                            <input type="hidden" name="patient_id_hidden" value="" id="patient_id_hidden">
                            <input type="hidden" name="patient_id" value="" id="patient_id">
                            <input type="hidden" name="encounter_id" value="" id="encounter_id">
                            <input type="hidden" id="visit_id" name="visit_id" value="{{$visit_id}}">

                            {{-- <input type="hidden" id="doctor_prescription"name="doctor_prescription" value=""> --}}

                            <input type="hidden" id="prescription_head_id" name="prescription_head_id" value="">

                            <input type="hidden" id="tinyDataStat_hidden" name="tinyDataStat_hidden" value="">
                            <input type="hidden" id="summary_updated_at" name="summary_updated_at" value="">

                            <input type="hidden" id="summary_prescription" name="summary_prescription" value="1">
                        </div>
                    </div>

                    <div class="col-md-12 padding_sm" style="margin-top:8px;">
                        <table class="table table-condensed no-margin table-bordered table_sm">
                            <thead>
                                <tr class="headergroupbg_purple"
                                    style="width:100%;background-color:#57ad85 !important;color:white">
                                    <th style="width:5%">UHID</th>
                                    <th style="width:15%">Patient Name</th>
                                    <th style="width:20%">Age/Gender</th>
                                    <th style="width:20%">Date of Visit</th>
                                    <th style="width:20%">Select Doctor/Dept</th>
                                    <th style="width:20%">Date of Procedure</th>
                                    <th style="width:20%">Time of Procedure</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><span id="uhid_label">UHID</span></td>
                                    <td><span id="patient_name_label">Patient Name</span></td>
                                    <td><span id="age_gender">Age/Gender</span></td>
                                    <td><span id="dov">Date of Visit</span></td>
                                    <th>
                                        {!! Form::select('discharge_department', $deptlist, '', ['class' => 'form-control custom_floatinput', 'placeholder' => 'Select Doctor(Department)', 'title' => 'Select Doctor(Department)', 'id' => 'discharge_department', 'style' => 'width:100%;color:#555555;']) !!}
                                    </th>

                                    <th>
                                        <input type="text" onblur="summary_dates_change(this.value);" id="procedure_date"
                                            name="procedure_date" autocomplete="off" class="form-control date_floatinput"
                                            placeholder="">
                                    </th>
                                    <th>
                                        <input type="text" onblur="summary_dates_change(this.value);" id="procedure_time"
                                            name="procedure_time" autocomplete="off" class="form-control time_floatinput"
                                            placeholder="">
                                    </th>

                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                @include('Emr::emr.discharge_summary.discharge_summary_edit_area')
            </div>
            <div class="col-md-12">
                <button style="float:right;margin-right:15px;" onclick="updatePreview();"
                    class="btn op_btn expand_btn anim btn-info headerbtn">
                    <i class="fa fa-desktop" aria-hidden="true"></i>
                    &nbsp;Save &amp; Preview
                </button>
            </div>
        </div>
    </div>

    <div class="slide_box op_slide_box anim" style="width:63%;margin-top:-60px;">
        <div class="slide_header slide_close_btn" style="background: rgb(72, 118, 179);border-radius:0px;height:40px;">

            <div class="col-md-12">
                <div style="width:30px;height:30px;padding-top:7px;" class="slide_close_btn pull-left"> >> </div>
                <div onclick="print_summary();" style="width:30px;height:30px;padding-top:7px;margin-left:5px;"
                    class="slide_close_btn pull-left">
                    <i class="fa fa-print" aria-hidden="true"></i>
                </div>
                <div onclick="email_summary();" style="width:30px;height:30px;padding-top:7px;margin-left:5px;"
                    class="slide_close_btn pull-left">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                </div>
                <h4 class="no-margin">Preview</h4>
            </div>
        </div>
        <div class="slide_body theadscroll" style="height:650px;overflow:scroll;padding:40px;background-color:#e7ecf2">

            {!! Form::open(['name' => 'formSummaryViewArea', 'method' => 'post', 'id' => 'formSummaryViewArea']) !!}
            <page size="A4" id="summaryViewArea" style="padding:25px;">
                @include('Emr::emr.op_discharge_summary.summary_preview')
            </page>
            {!! Form::token() !!} {!! Form::close() !!}
        </div>
    </div>

    <!-- Modal window for select visit -->
    <div class="modal fade" id="ModalSelectVisit" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-info bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Select Visit</h4>
                </div>
                <div class="modal-body">
                    <div id="ModalSelectVisitData" class="container " style="width:100%;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal window for select visit  ends-->

    <!-- Modal window for prescription -->
    <div class="modal fade" id="ModalPrescription" role="dialog" style="overflow-y:scroll;">
        <div class="modal-dialog modal-lg" style="width: 95%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-info bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Medication</h4>
                </div>
                <div class="modal-body" id="prescription_modal_body_content">
                    {{-- @include('Emr::emr.includes.patient_prescription') --}}
                </div>
                <div class="modal-footer">
                    <input type="button" style="width:120px;" class="btn bg-green" onclick="save_medication();"
                        value="Save" />
                </div>
            </div>
        </div>
    </div>
    <!-- Modal window for prescription  ends-->
    <!-- modal window for investigation --->
    <div class="modal fade" id="modalFetchLab" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-info bg-primary">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Select Investigation</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="input-group custom-float-label-control">
                                    <label class="custom_floatlabel"> Investigation From</label>
                                    <input type="text" id="investigation_from" name="investigation_from" autocomplete="off"
                                        class="form-control date_floatinput" style="border:1px " placeholder=""
                                        value="{{ $current_date_web }}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group custom-float-label-control">
                                    <label class="custom_floatlabel"> Investigation To</label>
                                    <input type="text" id="investigation_to" name="investigation_to" autocomplete="off"
                                        class="form-control date_floatinput" style="border:1px " placeholder=""
                                        value="{{ $current_date_web }}">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <input type="button" class="btn btn-primary" value="Select" Name="fetchLabButton"
                                    onclick="fetchSummaryPatientInvestigation();" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- modal window for investigation ends --->

    <!-- Modal window for favorite list-->
    <div class="modal fade" id="myModalFavoriteList" role="dialog">
        <div class="modal-dialog " style="width: 1200px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-info bg-orange">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Favorites List</h4>
                </div>
                <div class="modal-body">
                    <div id="myModalFavoriteListData" class="container" style="width:100%;">

                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal window for favorite list ends-->


    <!---------summary history--------------------->
    <div class="modal fade" id="summaryHistoryModal" role="dialog">
        <div class="modal-dialog modal-sm" style="width:90%;">
            <!-- Modal content-->
            <div class="modal-content" style="">
                <div class="modal-header modal-header-info" style="background-color:#6a6d6a">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:white">Disharge Summary List</h4>
                </div>
                <div class="modal-body" style="padding:6px;height: 560px;">
                    @php
                        $current_date_web = date('M-d-Y');
                    @endphp
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <label class="custom_floatlabel">From Date</label>
                                <input type='text' id="history_date_from" name="history_date_from" autocomplete="off"
                                    class="form-control date_floatinput" placeholder="MMM-DD-YYYY"
                                    value='{{ $current_date_web }}' />

                            </div>

                            <div class="col-md-2">
                                <label class="custom_floatlabel">To Date</label>
                                <input type="text" id="history_date_to" name="history_date_to" autocomplete="off"
                                    class="form-control date_floatinput" placeholder="MMM-DD-YYYY"
                                    value="{{ $current_date_web }}" />
                            </div>

                            <div class="col-md-3 padding_sm">
                                @php
                                    $user_id = !empty(\Auth::user()->id) ? \Auth::user()->id : 0;
                                    $user_name = \DB::table('users')
                                        ->where('id', $user_id)
                                        ->value('name');
                                @endphp

                                <div class="input-group custom-float-label-control">
                                    <label style="margin: -2px;margin-left: 15px;" class="custom_floatlabel">User
                                        name</label>
                                    <input class="form-control" value="{{ $user_name }}" autocomplete="off"
                                        type="text" placeholder="User Name" id="user_name" name="user_name" />
                                    <div id="userAjaxDiv" class="ajaxSearchBox" style="margin-top: 25px;z-index: 999;">
                                    </div>
                                    <input value="{{ $user_id }}" type="hidden" name="user_name_hidden" value=""
                                        id="user_name_hidden">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <a style="margin-top: 15px;" onclick="showDischargeSummaryHistory();"
                                    class="btn btn-success"><i class="fa fa-search"></i> Search </a>
                            </div>
                        </div>
                    </div>


                    <div id="summary_history_content">

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!---------summary history ends---------------->

    <!-- Modal window for template master-->
    <div class="modal fade" id="modalTemplateMaster" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-info" style="background-color:#57ad85 !important;color:white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Template master</h4>
                </div>
                <div class="modal-body" style="height:400px;">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="col-md-4">
                                <div class="input-group custom-float-label-control">
                                    <span>Template Name</span>
                                    <input class="form-control" autocomplete="off" type="text"
                                        placeholder="Template Name" id="template_name_search" name="template_name_search" />
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">

                                <?php
                                $tagList = [];
                                $tagList = \DB::table('summary_template_category')
                                    ->orderBy('name', 'ASC')
                                    ->pluck('id', 'name');
                                ?>
                                <div class="input-group custom-float-label-control">
                                    <span>Category Name</span></br>
                                    <select classs="form-control filters" name="category_name_search"
                                        placeholder="Category Name " id="category_name_search"
                                        style="width: 275px;height: 24px; color:#555555;">
                                        <option value="">select category</option>
                                        <?php
                           if(isset($tagList) && count($tagList) > 0){
                               ?>
                                        <?php
                               foreach($tagList as $name => $value) {
                               ?>
                                        <option value="{{ $value }}">{{ $name }}</option>
                                        <?php
                               }
                           }
                           ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm" style="margin-top:21px;">

                                <button type="button" class="btn btn-primary" Name="template_serach"
                                    onclick="show_discharge_summary_templates();"><i class="fa fa-search"></i></button>
                                <input type="button" class="btn btn-success pull-right" value="Add new template"
                                    Name="btn_add_template" onclick="addTemplate();" />
                            </div>
                        </div>
                    </div>
                    <div id="summary_template_data" style="margin-top:10px;">

                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal window for template master ends-->

    <!-- Modal window for add template-->
    <div class="modal fade" id="modalAddSummaryTemplate" role="dialog">
        <div class="modal-dialog modal-lg" style="width:1000px;">
            <!-- Modal content-->
            <div class="modal-content" style="min-height:606px;">
                <div class="modal-header modal-header-info" style="background-color:#57ad85 !important;color:white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Summary Template</h4>
                </div>
                <div class="modal-body" style="min-height:10% !important">
                    {!! Form::open(['name' => 'form_add_template', 'method' => 'post', 'id' => 'form_add_template']) !!}
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="summary_template_add_id" value="0" id="summary_template_add_id" />
                            <input type="hidden" name="summary_template_edit_status" value="0"
                                id="summary_template_edit_status" />
                            <div class="col-md-3 padding_sm">
                                <div class="input-group custom-float-label-control">
                                    <label style="margin: -2px;margin-left: 2px;" class="custom_floatlabel">Template
                                        Name</label>
                                    <input class="form-control" autocomplete="off" type="text"
                                        placeholder="Template Name" id="template_name_add" name="template_name_add" />
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">

                                <?php
                                $tagList = [];
                                $tagList = \DB::table('summary_template_category')
                                    ->orderBy('name', 'ASC')
                                    ->pluck('id', 'name');
                                ?>
                                <div class="input-group custom-float-label-control">
                                    <label style="margin: -2px;margin-left: 2px;" class="custom_floatlabel">Category
                                        Name</label>
                                    <span class="btn bg-primary"
                                        style="float:right;margin-top: -10px;font-size: 11px;margin-right:84px;"
                                        onclick="add_new_template_category();" title="Add new category"><i
                                            class="fa fa-plus"></i></span>
                                    <select classs="form-control" name="category_name_add" placeholder="Category Name "
                                        id="category_name_add" style="width:230px;">
                                        <?php
                              if(isset($tagList) && count($tagList) > 0){
                                  ?>
                                        <?php
                                  foreach($tagList as $name => $value) {
                                  ?>
                                        <option value="{{ $value }}">{{ $name }}</option>
                                        <?php
                                  }
                              }
                              ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4 padding_sm">
                                <div class="box no-border" style="margin: 12px 0 0 0;">
                                    <div class="box-footer">
                                        <div class="col-md-6 no-padding"><input type="radio" name="visibility_status"
                                                checked="checked" value="0" />&nbsp; Visible to others</div>
                                        <div class="col-md-6 no-padding"><input type="radio" name="visibility_status"
                                                value="1" />&nbsp; Visible to me</div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-1 padding_sm">
                                <button style="margin: 12px 0 0 0;height: 28px;" type="button" name="btn_save_template"
                                    id="btn_save_template" class="btn btn-block btn-primary pull-right fa fa-save"
                                    title="Save"> Save</button>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top:20px;">
                        <div id="templateDataDiv" class="container" style="width:100%;">
                            <textarea class="tiny_editor" id="template_conent_text"
                                name="template_conent_text"></textarea>
                        </div>
                    </div>
                    {!! Form::token() !!} {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
    <!-- Modal window for add template ends-->

    <!--------custom template preview----------------->
    <div class="modal fade" id="customTemplatePreviewModel" role="dialog">
        <div class="modal-dialog modal-sm" style="width:1000px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-header-info" style="background-color:#57ad85 !important;color:white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:white">Summary Custom Template Preview</h4>
                </div>
                <div class="modal-body" style="min-height:10% !important">
                    <div class="row">
                        <div class="col-md-12" id="customTemplatePreviewContent">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------custom template preview ends------------>

    <!-- Modal window for add template-->
    <div class="modal fade" id="modalAddTemplate" role="dialog">
        <div class="modal-dialog modal-lg" style="width:1000px;">
            <!-- Modal content-->
            <div class="modal-content" style="min-height:606px;">
                <div class="modal-header modal-header-info bg-success">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Template</h4>
                </div>
                <div class="modal-body" style="min-height:10% !important">
                    {!! Form::open(['name' => 'form_add_template', 'method' => 'post', 'id' => 'form_add_template']) !!}
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="summary_template_add_id" value="0" id="summary_template_add_id" />
                            <input type="hidden" name="summary_template_edit_status" value="0"
                                id="summary_template_edit_status" />
                            <div class="col-md-4 padding_sm">
                                <div class="input-group custom-float-label-control">
                                    <label style="margin: -2px;margin-left: 2px;" class="custom_floatlabel">Template
                                        Name</label>
                                    <input class="form-control" autocomplete="off" type="text"
                                        placeholder="Template Name" id="template_name_add" name="template_name_add" />
                                </div>
                            </div>
                            <div class="col-md-3 padding_sm">

                                <?php
                                $tagList = [];
                                $tagList = \DB::table('summary_template_category')
                                    ->orderBy('name', 'ASC')
                                    ->pluck('id', 'name');
                                ?>
                                <div class="input-group custom-float-label-control">
                                    <label style="margin: -2px;margin-left: 2px;" class="custom_floatlabel">Category
                                        Name</label>
                                    <span class="btn bg-primary" style="float:right;margin-top: -10px;font-size: 11px;"
                                        onclick="add_new_template_category();" title="Add new category"><i
                                            class="fa fa-plus"></i></span>
                                    <select classs="form-control" name="category_name_add" placeholder="Category Name "
                                        id="category_name_add">
                                        <?php
                                  if(isset($tagList) && count($tagList) > 0){
                                      ?>
                                        <?php
                                      foreach($tagList as $name => $value) {
                                      ?>
                                        <option value="{{ $value }}">{{ $name }}</option>
                                        <?php
                                      }
                                  }
                                  ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4 padding_sm">
                                <div class="box no-border" style="margin: 12px 0 0 0;">
                                    <div class="box-footer">
                                        <div class="col-md-6 no-padding"><input type="radio" name="visibility_status"
                                                checked="checked" value="0" />&nbsp; Visible to others</div>
                                        <div class="col-md-6 no-padding"><input type="radio" name="visibility_status"
                                                value="1" />&nbsp; Visible to me</div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-1 padding_sm">
                                <button style="margin: 12px 0 0 0;height: 28px;" type="button" name="btn_save_template"
                                    id="btn_save_template" class="btn btn-block btn-primary pull-right fa fa-save"
                                    title="Save"> Save</button>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div id="templateDataDiv" class="container" style="width:100%;">
                            <textarea class="tiny_editor" id="template_conent_text"
                                name="template_conent_text"></textarea>
                        </div>
                    </div>
                    {!! Form::token() !!} {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
    <!-- Modal window for add template ends-->

    <!-- Modal window for add template category -->
    <div class="modal fade" id="modalAddTemplateCategory" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content" style="min-height:93%;">
                <div class="modal-header modal-header-info bg-success">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Template Category</h4>
                </div>
                <div class="modal-body" style="min-height:10% !important">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12 padding_sm">
                                <div class="input-group custom-float-label-control">
                                    <label style="margin: -2px;margin-left: 2px;" class="custom_floatlabel">Category
                                        Name</label>
                                    <input class="form-control" autocomplete="off" type="text"
                                        placeholder="Category Name" id="template_category_name_add"
                                        name="template_category_name_add" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4 padding_sm" style="float: right">
                                <button style="margin: 12px 0 0 0;height: 28px;" type="button"
                                    onclick="save_new_template_category();" name="btn_save_template_category"
                                    id="btn_save_template_category" class="btn btn-block btn-primary pull-right fa fa-save"
                                    title="Save"> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal window for add template category ends-->


    <!-- Modal window for assesment data -->
    <div class="modal fade" id="modalAssesmentData" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content" style="min-height:93%;">
                <div class="modal-header modal-header-info bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Clinical Notes</h4>
                </div>
                <div class="modal-body" style="min-height:10% !important" id="modalAssesmentDataContent">

                </div>
            </div>
        </div>
    </div>
    <!-- Modal window for assesment data ends-->

    <!-- Modal window for assesment data -->
    <div class="modal fade" id="modalAssesmentDataView" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content" style="min-height:93%;">
                <div class="modal-header modal-header-info bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Clinical Notes Data</h4>
                </div>
                <div class="modal-body" style="min-height:10% !important" id="modalAssesmentDataContentview">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-primary" style="float:right;"
                        onclick="fetchAssesmentDataToEditor();">Ftech Clinical Data</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal window for assesment data ends-->

    <!--------email in discharge summary-------------->
    <div class="modal fade" id="summaryEmailModal" role="dialog">
        <div class="modal-dialog modal-sm" style="width:550px;">
            <!-- Modal content-->
            <div class="modal-content" style="height: 150px;">
                <div class="modal-header modal-header-info bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:white">Please enter patient email address</h4>
                </div>
                <div class="modal-body" style="min-height:10% !important">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="email" autocomplete="off" class="form-control" name="patient_email"
                                id="patient_email" placeholder="Enter email address here...">
                        </div>
                    </div>
                    <br>
                    <div class="row" style="float:right">
                        <div class="col-md-12">
                            <a onclick="send_summary_via_email();" class="btn btn-success"
                                style="width: 75px;height:21px;"><i class="fa fa-envelope"></i> Send</a>
                            <a data-dismiss="modal" class="btn btn-danger" style="width: 75px;height:21px;"> Skip</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------email in discharge summary ends--------->


    @include('Emr::emr.includes.custom_modals')
@stop
@section('javascript_extra')

    {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}

    <script src="{{ asset('packages/extensionsvalley/emr/js/clinical.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/op_discharge_summary.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/prescription.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">

    </script> -->
    <script type="text/javascript">
        @if ($iv_subcategory)
            window.iv_sub_cat_id = '{{ $iv_subcategory }}';
        @endif


        tinymce.init({
            selector: 'textarea.tiny_editor',
            max_height: 450,
            autoresize_min_height: '90',

            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons ',
            imagetools_cors_hosts: ['picsum.photos'],
            menubar: 'file edit view insert format tools table help',
            toolbar: 'undo redo | bold italic underline strikethrough | customInsertButton | fontselect fontsizeselect formatselect | FetchInvestigation | fetchMedication | Consultation | AssesmentData | Vitals | fetchTemplate |Ucase  Lcase  Icase Ccase |alignleft aligncenter alignright alignjustify |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | outdent indent | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
            browser_spellcheck: true,
            toolbar_sticky: true,
            autosave_ask_before_unload: true,
            autosave_interval: '30s',
            autosave_prefix: '{path}{query}-{id}-',
            autosave_restore_when_empty: false,
            autosave_retention: '2m',
            paste_enable_default_filters: false,
            image_advtab: true,
            contextmenu: false,

            importcss_append: true,

            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: 'mceNonEditable',
            toolbar_mode: 'sliding',

            //contextmenu: "paste | link image inserttable | cell row column deletetable",
            //content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px;line-height: 1;}',
            branding: false,
            statusbar: false,
            forced_root_block: '',
            setup: function(editor) {

                editor.ui.registry.addButton('FetchInvestigation', {
                    icon: 'paste-column-after',
                    tooltip: 'Fetch Patient Investigation',
                    //disabled :true,
                    onAction: function(_) {
                        $('#modalFetchLab').modal('toggle');
                    }
                });


                editor.ui.registry.addButton('fetchMedication', {
                    tooltip: 'Fetch Patient Medication',
                    icon: 'table-insert-column-after',
                    onAction: function() {
                        show_medication();
                    }
                });

                editor.ui.registry.addButton('fetchTemplate', {
                    tooltip: 'Fetch discharge summary templates',
                    icon: 'document-properties',
                    onAction: function() {
                        show_discharge_summary_templates();
                    }
                });

                editor.ui.registry.addButton('Consultation', {
                    tooltip: 'Fetch Cross Consultation',
                    icon: 'user',
                    onAction: function() {
                        fetchConsultation();
                    }
                });
                editor.ui.registry.addButton('Vitals', {
                    tooltip: 'Fetch Vitals',
                    icon: 'paste',
                    onAction: function() {
                        fetchVitals();
                    }
                });
                editor.ui.registry.addButton('AssesmentData', {
                    tooltip: 'Fetch AssesmentData',
                    icon: 'document-properties',
                    onAction: function() {
                        fetchAssesmentData();
                    }
                });

                editor.ui.registry.addButton('Ucase', {
                    text: 'A^',
                    onAction: function() {
                        TextToUpperCase();
                    },
                });
                editor.ui.registry.addButton('Lcase', {
                    text: 'a^',
                    onAction: function() {
                        TextToLowerCase();
                    },
                });
                editor.ui.registry.addButton('Icase', {
                    text: 'I^',
                    onAction: function() {
                        TextToInterCase();
                    },
                });
                editor.ui.registry.addButton('Ccase', {
                    text: 'C^',
                    onAction: function() {
                        FirstLetterToInterCase();
                    },
                });

                editor.on('focusout', function(e) {
                    saveDischargeSummaryContent(editor.id);
                });

            },


        });

        /*-------text to upper case---------------------*/
        function TextToUpperCase() {

            var textData = tinyMCE.activeEditor.selection.getContent({
                format: "html"
            });
            textData = textData.toUpperCase();
            tinyMCE.execCommand('mceReplaceContent', false, textData);
            return false;

        }

        function TextToLowerCase() {

            var textData = tinyMCE.activeEditor.selection.getContent({
                format: "html"
            });
            textData = textData.toLowerCase();
            tinyMCE.execCommand('mceReplaceContent', false, textData);
            return false;

        }

        function TextToInterCase() {

            var textData = tinyMCE.activeEditor.selection.getContent({
                format: "html"
            });
            textData = textData.replace(/\w\S*/g, function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            tinyMCE.execCommand('mceReplaceContent', false, textData);
            return false;

        }

        function FirstLetterToInterCase() {
            var textData = tinyMCE.activeEditor.selection.getContent({
                format: "html"
            });
            textData = capitalizeSentences(textData);
            tinyMCE.execCommand('mceReplaceContent', false, textData);
            return false;
        }

        function capitalizeSentences(capText) {
            capText = capText.trim();

            capText = capText.replace(/\!\s/g, "##!. ");
            capText = capText.replace(/\?\s/g, "##?. ");



            var allCaps = "";

            var capLock = "";


            if (capLock == 1 || capLock == true) {
                capText = capText.toLowerCase();
            }


            if (allCaps == 1 || allCaps == true) {
                capText = capText.replace(/\n/g, ". [-<br>-] ");
                //Get rid of extra whitespaces now that linebreaks are protected
                re1 = /\s+/g;
                capText = capText.replace(re1, " ");
                var wordSplit = ' ';
            } else {
                capText = capText.replace(/\.\n/g, ".[-<br>-]. ");
                capText = capText.replace(/\.\s\n/g, ". [-<br>-]. ");
                //Get rid of extra whitespaces now that linebreaks are protected
                re1 = /\s+/g;
                capText = capText.replace(re1, " ");
                var wordSplit = '. ';
            }

            var wordArray = capText.split(wordSplit);

            var numWords = wordArray.length;

            for (x = 0; x < numWords; x++) {

                wordArray[x] = wordArray[x].replace(wordArray[x].charAt(0), wordArray[x].charAt(0).toUpperCase());

                if (allCaps == 1 || allCaps == true) {
                    if (x == 0) {
                        capText = wordArray[x] + " ";
                    } else if (x != numWords - 1) {
                        capText = capText + wordArray[x] + " ";
                    } else if (x == numWords - 1) {
                        capText = capText + wordArray[x];
                    }
                } else {
                    if (x == 0) {
                        capText = wordArray[x] + ". ";
                    } else if (x != numWords - 1) {
                        capText = capText + wordArray[x] + ". ";
                    } else if (x == numWords - 1) {
                        capText = capText + wordArray[x];
                    }

                }


            }

            if (allCaps == 1 || allCaps == true) {
                capText = capText.replace(/\.\s\[-<br>-\]\s/g, "\n");
                capText = capText.replace(/\.\s\[-<br>-\]/g, "\n");
            } else {
                capText = capText.replace(/\[-<br>-\]\.\s/g, "\n");
            }

            capText = capText.replace(/\si\s/g, " I ");

            //var capText = capText.replace("##?. ", "? ");
            var capText = capText.replace(/\##\?\./gm, "?");
            //var capText = capText.replace("##!. ", "! ");
            var capText = capText.replace(/\##\!\./gm, "!");

            return capText;
        }
        /*-------text to upper case end---------------------*/


        function fetchConsultation() {
            var visit_id = $('#visit_id').val();
            if (visit_id == '') {
                Command: toastr["warning"]('Select patient!');
                return;
            }

            var url = $('#base_url').val() + "/summary/fetchConsultation";
            $.ajax({
                type: "GET",
                url: url,
                data: 'visit_id=' + visit_id,
                beforeSend: function() {

                },
                success: function(html) {
                    if (html != 0) {
                        //obj = JSON.parse(html);
                        var parentEditor = parent.tinyMCE.activeEditor;
                        parentEditor.execCommand('mceInsertRawHTML', false, html);
                    }

                },
                complete: function() {

                }
            });
        }


        function fetchVitals() {
            var visit_id = $('#visit_id').val();
            var patient_id = $('#patient_id_hidden').val();
            if (visit_id == '') {
                Command: toastr["warning"]('Select patient!');
                return;
            }

            var url = $('#base_url').val() + "/summary/fetchVitals";
            $.ajax({
                type: "GET",
                url: url,
                data: 'patient_id=' + patient_id + '&visit_id=' + visit_id,
                beforeSend: function() {

                },
                success: function(html) {
                    //console.log(html);return;
                    if (html != 0) {
                        obj = JSON.parse(html);
                        var parentEditor = parent.tinyMCE.activeEditor;
                        parentEditor.execCommand('mceInsertRawHTML', false, obj);
                    }

                },
                complete: function() {

                }
            });
        }

        function fetchAssesmentData() {
            var visit_id = $('#visit_id').val();
            var patient_id = $('#patient_id_hidden').val();
            if (visit_id == '') {
                Command: toastr["warning"]('Select patient!');
                return;
            }

            var url = $('#base_url').val() + "/summary/fetchAssesmentData";
            $.ajax({
                type: "GET",
                url: url,
                data: 'patient_id=' + patient_id + '&visit_id=' + visit_id,
                beforeSend: function() {
                    $('#modalAssesmentData').modal('toggle');
                },
                success: function(html) {
                    $('#modalAssesmentDataContent').html(html);
                },
                complete: function() {

                }
            });
        }


        function loadFormView(head_id, form_id) {

            if (head_id != '' && head_id != undefined) {

                let url = $('#base_url').val() + "/emr/load-preview";
                let patient_id = $("#patient_id_hidden").val();
                let _token = $('#c_token').val();
                let popover = '1';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        head_id: head_id,
                        form_id: form_id,
                        patient_id: patient_id,
                        _token: _token,
                        popover: popover
                    },
                    beforeSend: function() {
                        $('#modalAssesmentDataView').modal('show');
                    },
                    success: function(data) {
                        $('#modalAssesmentDataContentView').html('');
                        let response = '';

                        if (data.status = 1) {
                            response = data.html;
                        } else {
                            response =
                                '<div class="row"><div colspan="5" class="col-md-12 text-center">No Data Found..!</div></div>';
                        }

                        $('#modalAssesmentDataContentview').html(response);

                    },
                    complete: function() {

                    }
                });
            }

        }

        function fetchAssesmentDataToEditor() {
            var htmldata = $("#modalAssesmentDataContentview").html();
            var parentEditor = parent.tinyMCE.activeEditor;
            parentEditor.execCommand('mceInsertRawHTML', false, htmldata);
            $('#modalAssesmentDataView').modal('hide');
            $('#modalAssesmentData').modal('toggle');
        }




        $(document).on('click', '.slide_close_btn', function(event) {
            $(".op_slide_box").removeClass('open');
        });
        $(document).on('click', '.op_btn', function(event) {
            $(".op_slide_box").addClass('open');
        });
    </script>
    <style>
        .mce-ico {
            font-size: 12px !important;
            line-height: 12px !important;
        }

    </style>
@endsection
