
<div class="col-md-12">
    <table border="1" bordercolor="#dddddd" class="table table-condensed table-striped table_sm table-borderd" id="" style="border:1px solid rgba(104, 103, 103, 0.671) !important;border-collapse: collapse !important; font-size:14px;padding:3px;">
        <thead>
            <tr style="background-color:rgb(211, 209, 209);">
                <th style="width:10%">Slno</th>
                <th style="text-align:left;width:25%">Medicine</th>
                <th style="text-align:left;width:25%">Generic Name</th>
                <th style="width:10%">Dose</th>
                <th style="width:10%">Frequency</th>
                <th style="width:10%">Days</th>
                <th style="width:10%">Quantity</th>
                <th style="width:10%">Route</th>
                <th style="width:15%">Instructions</th>
            </tr>
        </thead>

        <tbody>
            @php
                $i = 1;

            @endphp
            @foreach($res as $data)
            <tr>
                <td>{{$i}}</td>
                <td>{{$data->medicine}}</td>
                <td>{{$data->generic_name}}</td>
                <td>{{$data->dose}}</td>
                <td>{{$data->frequency}}</td>
                <td>{{$data->duration}}</td>
                <td>{{$data->quantity}}</td>
                <td>{{$data->route}}</td>
                <td>{{$data->notes}}</td>
            </tr>
            @php
                $i++;
            @endphp
            @endforeach
        </tbody>
    </table>
</div>
