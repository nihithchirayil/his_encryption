<table class="table no-margin table-striped table-col-bordered table_sm table-condensed" style="border: 1px solid #CCC;">
    <thead>
        <tr class="table_header_bg">
            <th width="12%">Date</th>
            <!-- <th>Title</th> -->
            <th>Type</th>
            <th>Description</th>
            <th width="5%">Preview</th>
            <th width="5%">Delete</th>
        </tr>
    </thead>
    <tbody>
    @if(isset($docList) && count($docList) > 0)
        @foreach($docList as $ind => $data)
        @php
            $created_at = (!empty($data->created_at)) ? date('d-M-Y', strtotime($data->created_at)) : '';
        @endphp
          <tr>
            <td>{{$created_at}}</td>
            <!-- <td>{{$data->title}}</td> -->
            <td>{{$data->type_name}}</td>

            <td>{{$data->description}}</td>
            <td><a style="cursor: poiner;" onclick="docImgPreview('{{$data->id}}','{{trim($data->storage_type)}}','{{trim($data->mime_type)}}')"><i class="fa fa-eye"></i></a></td>

            <td>
                @if(trim($data->storage_type) != 'local')
                    <a style="cursor: poiner;" onclick="docImgDelete('{{$data->id}}')"><i class="fa fa-trash"></i></a>
                @endif
            </td>
          </tr>
        @endforeach
    @endif
    </tbody>
</table>
<div class="clearfix"></div>
<div class="col-md-12 text-right purple_pagination document_pagination">
    @if(isset($docList) && count($docList) > 0)
    <?php echo $docList->render(); ?>
    @endif
</div>
