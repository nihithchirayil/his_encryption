<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<script type="text/javascript">
    $(document).ready(function() {
        $(".page-link").click(function() {
            var url = $(this).attr("href");
            $.ajax({
                url: url,
                type: "GET",
                data: "notification=1",
                beforeSend: function() {
                    $('#doctordocument_modaldata').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                },
                success: function(data) {
                    $("#doctor_documentuploadlist").html(data);
                },
                complete: function() {
                    $('#doctordocument_modaldata').LoadingOverlay("hide");
                }
            });
            return false;
        });

    });
</script>
<div class="theadscroll" style="position: relative; height: 400px;">
    <table class="table no-margin table-striped table-col-bordered table_sm table-condensed"
        style="border: 1px solid #CCC;">
        <thead>
            <tr class="table_header_bg">
                <th class="common_td_rules" width="40%">Titile</th>
                <th class="common_td_rules" width="30%">Doctor Name</th>
                <th width="5%"><i class="fa fa-print"></i></th>
                <th width="5%"><i class="fa fa-trash"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php
    if(isset($doctor_datalist) && count($doctor_datalist) > 0){
        foreach($doctor_datalist as $each){
            $upload_view_path = url('/packages/extensionsvalley/emr/uploads/');
            $filePath = $upload_view_path . "/" . $each->doctor_id . "/" . $each->filename;
            ?>
            <tr>
                <td class="common_td_rules">{{ $each->title }}</td>
                <td class="common_td_rules">{{ $each->doctor_name }}</td>
                <td>
                    <?php
                    if($each->mime_type=='application/pdf'){
                        ?>
                    <button title="Print File" id="print_doctor_documentbtn<?= $each->document_id ?>"
                        onclick="docDocumentDataPreview('{{ $each->document_id }}')" class="btn btn-primary"><i
                            id="print_doctor_documentspin<?= $each->document_id ?>" class="fa fa-print"></i></button>
                    <?php
                    }else{
                        ?>
                    <a title="Download File" href="<?= $filePath ?>" download style="padding: 0px 5px;"
                        class="btn btn-warning"><i class="fa fa-download"></i></a>
                    <?php
                    }
                ?>

                </td>
                <td><button title="Delete File" id="delete_doctor_documentbtn<?= $each->document_id ?>"
                        onclick="docDocumentDataDelete('{{ $each->document_id }}')" class="btn btn-danger"><i
                            id="delete_doctor_documentspin<?= $each->document_id ?>"
                            class="fa fa-trash"></i></button></button></td>
            </tr>
            <?php
        }
    }else{
        ?>
            <tr>
                <th colspan="4" style="text-align: center">
                    No Result Found
                </th>
            </tr>

            <?php
    }
    ?>
        </tbody>
    </table>
    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <ul class="pagination purple_pagination pull-right">
            {!! $page_links !!}
        </ul>
    </div>
</div>
