<div class="theadscroll" style="position: relative; height: 150px; padding-right:10px;">
    <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed">
        <thead>
            <tr style="background-color: #1abb9c; color:white; ">
                <th style="width: 5%;">Sl.no</th>
                <th style="width: 5%;">
                    <div class="checkbox checkbox-success inline no-margin" style="margin-top:4px !important">
                        <input checked='' type="checkbox" id="check_all_medicine">
                        <label style="padding-left: 2px;" for="check_all_medicine">
                        </label>
                    </div>
                <th style="width: 25%;">Medicine</th>
                <th style="width: 25%;">Medicine Code</th>
            </tr>
        </thead>
        <tbody>
            @php $i= 1; @endphp
            @if (!empty($result_medicine_detail))
            @foreach ($result_medicine_detail as $value)
            <tr>
                <td>{{ $i++ }}</td>
                <td>
                    <div class="checkbox checkbox-success inline no-margin" style="margin-top:4px !important">
                        <input checked='' type="checkbox" id="check_remove_medicine_{{$i}}" value="{{$value->medicine_code}}" class="check_remove_medicine">
                        <label style="padding-left: 2px;" for="check_remove_medicine_{{$i}}">
                        </label>
                    </div>
                </td>
                <td>{{ $value->item_desc }}</td>
                <td>{{ $value->medicine_code }}</td>
            </tr>
            @endforeach
            @endif

        </tbody>
    </table>
</div>
