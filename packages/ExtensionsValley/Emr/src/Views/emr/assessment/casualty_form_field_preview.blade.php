<!-- .css -->
<style>
    #doctorHeadClass p:first-child {
        margin: 0 !important;
        padding: 0 !important;
    }
    #tableAssessmentContent td{
        padding-left:8px !important;
        font-weight: 200 !important;
        font-size:12px !important;
        background: white !important;
        padding-top:2px !important;
        padding-bottom:2px !important;
        /* color:#2e2e2e !important; */
    }
    #tableAssessmentContent strong{
        font-size: 12px !important;
        color:black !important;
    }
    #tableAssessmentContent .bg-thead{
       background: aquamarine !important;
       color:black !important;
    }
</style>
<style type="text/css" media="print">
    @page {
        margin: 15px;
    }

    table {
        font-size: 11px !important;
    }
    td{
        padding: 5px !important;

    }
    h5{
        color:black !important;
    }
</style>
<!-- .css -->
<!-- .box-body -->
<div class="">
@if($include_patient_header == 1)
<style >
    .margintinymce {margin-left: 10px !important;}
    </style>
@else
<style>
    .margintinymce {}
    </style>
    @endif

    @php
        $hospital_header_disable_in_prescription = 0;
    @endphp
    <!-- .col-md-12 -->
    <div class="col-md-12 margintinymce" @if ($hospital_header_disable_in_prescription == 1) style="margin-top:3cm;" @endif>
        @if ($include_patient_header != 1)
            @if (!empty($hospitalHeader) && $header_status == 1)
                {!! $hospitalHeader !!}
                <hr style="margin-top:0px; margin-bottom:20px; ">
            @endif

            @if($header_status==1)
            <table style="border-color:rgb(241, 235, 235);font-size:12px !important;margin-left:7px;" width="95%" border="0" cellspacing="0" cellpadding="0">

                <tr>
                    <td><strong>Patient Name</strong></td>
                    <td align="center">:</td>
                    <td>{{ $patient_details[0]->patient_name }}</td>
                    <td>&nbsp;</td>
                    <td><strong>Age/Gender</strong></td>
                    <td align="center">:</td>
                    <td>
                        {{ $patient_details[0]->age }}/{{ $patient_details[0]->gender }}
                    </td>
                </tr>
                <tr>
                    <td><strong>UHID</strong></td>
                    <td align="center">:</td>
                    <td>{{ $patient_details[0]->uhid }}</td>
                    <td>&nbsp;</td>
                    <td><strong>Doctor</strong></td>
                    <td align="center">:</td>
                    <td>{{ $doctor_name }}</td>
                </tr>
                <tr>
                    <td><strong>Date</strong></td>
                    <td align="center">:</td>
                    <td>{{ Date('M-d-Y', strtotime($date)) }}</td>
                    <td>&nbsp;</td>
                    <td><strong>Speciality</strong></td>
                    <td align="center">:</td>
                    <td>{{ $speciality }}</td>
                </tr>
            </table>
            @endif
        @endif



        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>

                <h5 style="background-color:darkcyan;color:white;padding-left:7px;padding-top:3px;padding-bottom:3px;margin-bottom:1px;">{{ucwords(strtolower($form_name))}} <span style="float:right;font-size:11px;margin-right:4px;">{{date('M-d-Y h:i A',strtotime($updated_at))}}</span></h5>

                <table id="tableAssessmentContent" style="border-collapse:collapse;border-color:rgb(241, 235, 235);" width="100%"
                    @if ($include_patient_header != 1)border="1" @endif cellspacing="0" cellpadding="0">

                    @if (!empty($all_form_fields))
                        @if (sizeof($all_form_fields) > 0)
                            @for ($i = 0; $i < sizeof($all_form_fields); $i++)
                                @php
                                    $type_name = '';
                                    $type = '';
                                    $default_value = '';
                                    $entered_value = '';
                                    $field_value = '';
                                    $is_required = '';
                                    $field_name = '';
                                    $field_label = '';
                                    $progress_table = '';
                                    $progress_value = '';
                                    $progress_text = '';
                                    $class_name = '';
                                    $table_structure = '';
                                    $table_details = '';
                                    $field_width = '';
                                    $field_height = '';
                                    $form_id = '';
                                    $form_field_id = '';

                                    $type_name = !empty($all_form_fields[$i]->type_name) ? $all_form_fields[$i]->type_name : '';
                                    $type = !empty($all_form_fields[$i]->type) ? $all_form_fields[$i]->type : '';
                                    $type = !empty($all_form_fields[$i]->type) ? $all_form_fields[$i]->type : '';
                                    $field_name = !empty($all_form_fields[$i]->field_name) ? $all_form_fields[$i]->field_name : '';
                                    $field_label = !empty($all_form_fields[$i]->field_label) ? $all_form_fields[$i]->field_label : '';
                                    //$default_value = (!empty($all_form_fields[$i]->default_value)) ? $all_form_fields[$i]->default_value : '';
                                    $form_field_id = !empty($all_form_fields[$i]->form_field_id) ? $all_form_fields[$i]->form_field_id : '';
                                    $form_id = !empty($all_form_fields[$i]->form_id) ? $all_form_fields[$i]->form_id : '';
                                    //progress search
                                    $progress_table = !empty($all_form_fields[$i]->progress_table) ? $all_form_fields[$i]->progress_table : '';
                                    $progress_value = !empty($all_form_fields[$i]->progress_value) ? $all_form_fields[$i]->progress_value : '';
                                    $progress_text = !empty($all_form_fields[$i]->progress_text) ? $all_form_fields[$i]->progress_text : '';
                                    //table
                                    $table_structure = !empty($all_form_fields[$i]->table_structure) ? $all_form_fields[$i]->table_structure : '';
                                    $table_details = !empty($all_form_fields[$i]->table_details) ? $all_form_fields[$i]->table_details : '';

                                    $radio = [];
                                    $gcheckbox = [];
                                    $selected_value = [];
                                    $optionsListSelected = [];

                                @endphp


                                <tr>
                                    @if ($type_name == 'TEXT')

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }
                                        @endphp

                                        @if ($field_value != '')
                                            <td width="25%"><strong>{{ $field_label }}</strong></td>
                                            <td>{!! $field_value !!}</td>
                                        @endif

                                    @elseif($type_name == "TEXT AREA")

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }
                                        @endphp

                                        @if ($field_value != '')
                                            <td width="30%"><strong>{{ $field_label }}</strong></td>
                                            <td>{!! nl2br($field_value) !!}</td>
                                        @endif

                                    @elseif($type_name == "DATE")

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }
                                        @endphp

                                        @if ($field_value != '')
                                            <td width="30%"><strong>{{ $field_label }}</strong></td>
                                            <td>{{ $field_value }}</td>
                                        @endif

                                    @elseif($type_name == "TINYMCE")

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }
                                        @endphp


                                        @if ($field_value != '')
                                            @if ($include_patient_header != 1)
                                                <td width="30%"><strong>{{ $field_label }}</strong></td>
                                            @endif
                                            <td>{!! $field_value !!}</td>
                                        @endif

                                    @elseif($type_name == "CERTIFICATE")

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }
                                        @endphp

                                        @if ($field_value != '')
                                            <td width="30%"><strong>{{ $field_label }}</strong></td>
                                            <td>{!! $field_value !!}</td>
                                        @endif

                                    @elseif($type_name == "CHECK BOX")

                                        @php

                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';

                                            $checked_box_default_value = '';

                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;

                                                //if check box checked then get default value as name
                                                $checked_box_default_value = !empty($all_form_fields[$i]->default_value) ? $all_form_fields[$i]->default_value : '';
                                            } else {
                                                $field_value = $default_value;
                                            }

                                        @endphp

                                        @if ($field_name == $field_value)
                                            <td width="30%"><strong>{{ $field_label }}</strong></td>
                                            <td>{{ $checked_box_default_value }}</td>
                                        @endif

                                    @elseif($type_name == "SELECT BOX")

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }

                                            $options = '';
                                            if (!empty($field_value)):
                                                $options = ExtensionsValley\Emr\FormFieldSelectboxOptions::where('form_field_id', '=', $form_field_id)
                                                    ->where('value', '=', $field_value)
                                                    ->value('name');
                                            endif;
                                        @endphp

                                        @if (!empty($options))
                                            <td width="30%"><strong>{{ $field_label }}</strong></td>
                                            <td>{!! $options !!}</td>
                                        @endif

                                    @elseif($type_name == "RADIO BUTTON")

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }
                                        @endphp

                                        @if (isset($form_field_id) && !empty($form_field_id))
                                            @php
                                                $radio = '';
                                                if (!empty($field_value)):
                                                    $radio = ExtensionsValley\Emr\FormFieldRadioOptions::where('form_field_id', '=', $form_field_id)
                                                        ->where('value', '=', $field_value)
                                                        ->value('name');
                                                endif;
                                            @endphp
                                        @endif

                                        @if (!empty($radio))
                                            <td width="30%"><strong>{{ $field_label }}</strong></td>
                                            <td>{!! $radio !!}</td>
                                        @endif

                                    @elseif($type_name == "GROUPED CHECKBOX")

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }
                                        @endphp

                                        @if (sizeof((array) $field_value) > 0)

                                            @php
                                                $datArr = [];
                                                $datArr = (array) $field_value;
                                            @endphp

                                            @if ($field_value != '')
                                                <td width="30%"><strong>{{ $field_label }}</strong></td>
                                                <td>
                                                    @foreach ($datArr as $ind => $data)
                                                        {!! $data !!} <br>
                                                    @endforeach
                                                </td>
                                            @endif
                                        @endif

                                    @elseif($type_name == "HEADER TEXT")

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }
                                        @endphp
                                        @if($default_value !='')
                                            <td colspan="2"><b>{!! $default_value !!}</b></td>
                                        @endif

                                    @elseif($type_name == "PROGRESSIVE SEARCH")

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }
                                        @endphp

                                        @if (sizeof((array) $field_value) > 0)

                                            @php
                                                $datProArr = [];
                                                $datProArr = (array) $field_value;
                                            @endphp

                                            @if ($field_value != '')
                                                <td width="30%"><strong>{{ $field_label }}</strong></td>
                                                <td>
                                                    @foreach ($datProArr as $ind => $pro_data)
                                                        {!! $pro_data !!} <br>
                                                    @endforeach
                                                </td>
                                            @endif
                                        @endif

                                    @elseif($type_name == "TABLE")

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }

                                            $tableDatArr = [];
                                            $tableDatArr = (array) $field_value;

                                            $Tdetails = json_decode($table_details);
                                            $table_details = $Tdetails->$field_name;

                                            $rowHeader = isset($table_details->rowHeader) ? $table_details->rowHeader : 0;
                                            $colHeader = isset($table_details->colHeader) ? $table_details->colHeader : 0;

                                            $Tdata = [];
                                            $TdataCount = 0;
                                            $fixed_headers = 0;
                                            $fixed_headers = $rowHeader + $colHeader;

                                            if (count($tableDatArr) > 0) {
                                                $Tdata = array_filter($tableDatArr);
                                                if (is_array($Tdata) && count($Tdata) > 0) {
                                                    $TdataCount = sizeof($Tdata);
                                                }
                                            }
                                        @endphp

                                        @if (isset($tableDatArr))
                                            @if (sizeof($tableDatArr) > 0 && $TdataCount > $fixed_headers)
                                                <td colspan="2" class="no-padding">
                                                    <table width="100%"
                                                        class="table tabe_sm table-bordered table-striped" style="margin-left:-4px !important;">
                                                        @php
                                                            $k=0;
                                                        @endphp
                                                        @foreach ($tableDatArr as $rows => $colsArr)
                                                            <tr>
                                                                @foreach ($colsArr as $colsind => $cols)
                                                                    <td @if($k==0) class='bg-thead' @endif>{{ $cols }} &nbsp;</td>
                                                                @endforeach
                                                            </tr>
                                                            @php
                                                            $k++;
                                                        @endphp
                                                        @endforeach
                                                    </table>
                                                </td>
                                            @endif
                                        @endif


                                    @elseif($type_name == "CUSTOM DATA")

                                        @php
                                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                            if (!empty($entered_value)) {
                                                $field_value = $entered_value;
                                            } else {
                                                $field_value = $default_value;
                                            }
                                        @endphp

                                        @if ($field_value != '')
                                            <td width="30%"><strong>{{ $field_label }}</strong></td>
                                            <td>{!! $field_value !!}</td>
                                        @endif

                                    @endif
                                </tr>

                            @endfor
                        @endif
                    @endif
                </table>
            </td>
        </tr>


        </table>
    </div>
    @if($addendum!= NULL && $addendum !='undefined')
    <div class="col-md-12 box-body" style="margin-top:15px;">
        <h5><b>Addendum</b></h5>
        <p>{!! $addendum !!}</p>
        @php
            $doctor_name = \DB::table('doctor_master')->where('user_id', $updated_by)->value('doctor_name');
            $updated_at = date('M-d-Y h:i A', strtotime($updated_at));
        @endphp
        <p style="text-align: right;">{!! $doctor_name !!}
            <br>
            {!! $updated_at !!}
        </p>
    </div>
    @endif
    <!-- .col-md-12 -->
</div>
<!-- .box-body -->
