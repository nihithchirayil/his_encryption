<!-- Field Content -->
@php
$tableNamesArr = [];
$tinyNamesArr = [];
$certNamesArr = [];
$customDataArr = [];
$gcheckboxNamesArr = [];
$psearchNamesArr = [];
$is_favorite_option = !empty($form_list->is_favorite_option) ? $form_list->is_favorite_option : 0;
$patientVitalsArr = [];
$patientAllergyArr = [];
//exit('@@@@@'.$is_finalized);
$is_usg_template = !empty($form_list->is_usg_template) ? $form_list->is_usg_template : 0;
if ($has_custom_fields > 0) {
    if (!empty($patient_id)) {
        //vitals list start
        $patientVitalsArr = \DB::select("select ve.vital_value,vm.name
                from vital_entry ve
                join vital_master vm on vm.id = ve.vital_master_id
                where ve.patient_id = $patient_id
                and vm.defaultvalue = 1
                and ve.batch_no =( select batch_no from vital_entry where patient_id =$patient_id
                order by id desc limit 1 )
                and ve.deleted_at is null order by ve.id desc");
        //vitals list end

        //allergy list start
        $other_allergies = \DB::table('patient_other_allergies')
            ->where('patient_id', $patient_id)
            ->whereNull('deleted_at')
            ->select('allergy')
            ->first();

        if (!empty($other_allergies->allergy)) {
            $patientAllergyArr['other_allergies'] = $other_allergies->allergy;
        }

        $sql = "(Select CASE WHEN pa.type = '0' THEN p.item_desc
                ELSE G.generic_name END  as description
                from patient_allergy_medication pa
                left join product p on p.item_code = pa.item_code
                left join generic_name G on G.id = p.generic_name_id
                where pa.patient_id = '{$patient_id}' and pa.deleted_at is null)
                union
                (select gn.generic_name as description
                from generic_patient_allergy gpa
                join generic_name gn on gn.id = gpa.generic_id
                where gpa.patient_id = '{$patient_id}' and gpa.deleted_at is null )";

        $patient_allergy = \DB::select($sql);

        if (sizeof($patient_allergy) > 0) {
            $patientAllergyArr['patient_allergy'] = $patient_allergy;
        }
        //allergy list end
    }

}
@endphp
<input type="hidden" id="usg_fav_content" value={{$usg_template_favourites}}/>
<style>
    .tox .tox-collection--grid .tox-collection__group {
        /* display: flex; */
        flex-wrap: wrap;
        max-height: 208px;
        overflow-x: hidden;
        overflow-y: auto;
        padding: 0;
        width: 750px !important;
    }

    .tox .tox-collection--grid .tox-collection__item {
        width: 930px !important;
    }

    .tox .tox-collection__item {
        align-items: center;
        color: #222f3e;
        cursor: pointer;
        display: block !important;
    }

    .tox .tox-menu.tox-collection.tox-collection--grid {
        padding: 4px;
        width: 960px !important;
    }

    .tox .tox-autocompleter .tox-menu{
        max-width :125em !important;
    }
    .tox .tox-autocompleter{
        max-width : 125em !important;
    }
</style>






@if (!empty($all_form_fields))
    @if (sizeof($all_form_fields) > 0)
        @for ($i = 0; $i < sizeof($all_form_fields); $i++)
            @php
                $field_value = '';
                $entered_value = '';
                $form_id = '';
                $type_name = '';
                $type = '';
                $default_value = '';
                $is_required = '';
                $field_name = '';
                $field_label = '';
                $progress_table = '';
                $progress_value = '';
                $progress_text = '';
                $class_name = '';
                $table_structure = "";
                $table_details = '';
                $field_width = '';
                $field_height = '';
                $form_field_id = '';
                $style = '';
                
                $type_name = !empty($all_form_fields[$i]->type_name) ? $all_form_fields[$i]->type_name : '';
                $type = !empty($all_form_fields[$i]->type) ? $all_form_fields[$i]->type : '';
                $field_name = !empty($all_form_fields[$i]->field_name) ? $all_form_fields[$i]->field_name : '';
                $field_label = !empty($all_form_fields[$i]->field_label) ? $all_form_fields[$i]->field_label : '';
                $default_value = !empty($all_form_fields[$i]->default_value) ? $all_form_fields[$i]->default_value : '';
                $form_field_id = !empty($all_form_fields[$i]->form_field_id) ? $all_form_fields[$i]->form_field_id : '';
                $form_id = !empty($all_form_fields[$i]->form_id) ? $all_form_fields[$i]->form_id : '';
                //progress search
                $progress_table = !empty($all_form_fields[$i]->progress_table) ? $all_form_fields[$i]->progress_table : '';
                $progress_value = !empty($all_form_fields[$i]->progress_value) ? $all_form_fields[$i]->progress_value : '';
                $progress_text = !empty($all_form_fields[$i]->progress_text) ? $all_form_fields[$i]->progress_text : '';
                //table
                $table_structure = !empty($all_form_fields[$i]->table_structure) ? $all_form_fields[$i]->table_structure : '';
                $table_details = !empty($all_form_fields[$i]->table_details) ? $all_form_fields[$i]->table_details : '';
                $field_width = !empty($all_form_fields[$i]->field_width) ? $all_form_fields[$i]->field_width : 'col-md-12';

                $radio = [];
                $gcheckbox = [];
                $selected_value = [];
                $optionsListSelected = [];
                $favorite_option_enabled = 0;

                //favorite option enabled then adjust width
                if ($is_favorite_option == 1 && ($type_name == 'TEXT' || $type_name == 'TEXT AREA')) {
                    $favorite_option_enabled = 1;
                }

                //field width
                if ($type_name == 'DATE' || $type_name == 'SELECT BOX' || $type_name == 'TIME' || $type_name == 'RADIO BUTTON') {
                    // $field_full_width = 'col-md-3';
                    $field_content_width = $field_width;
                    $field_favorite_width = 'col-md-4 col-md-offset-4';
                } else if($type_name == 'TEXT'){
                    // $field_full_width = 'col-md-12';
                    $style = 'margin-top: -1px;';
                    $field_content_width = $field_width;
                    $field_favorite_width = 'col-md-4 col-md-offset-4';
                }else if($type_name == 'TINYMCE'){
                    // $field_full_width = 'col-md-12';
                    $field_content_width = 'col-md-12';
                    $field_favorite_width = '';
                } else if($type_name == 'PROGRESSIVE SEARCH'){
                    // $field_full_width = 'col-md-12';
                    $style = 'margin-top: -2px;';
                    $field_content_width = $field_width;
                    $field_favorite_width = 'col-md-4 col-md-offset-4';
                }
                else if($type_name == 'GROUPED CHECKBOX'){
                    // $field_full_width = 'col-md-12';
                    $style = 'margin-top: 17px;';
                    $field_content_width = $field_width;
                    $field_favorite_width = 'col-md-4 col-md-offset-4';
                }else {
                    // $field_full_width = 'col-md-3';
                    $field_content_width = 'col-md-12';
                    $field_favorite_width = 'col-md-4';
                }

            @endphp

            {{-- <div class="{{ $field_full_width }} ca_border_bottom" style="margin:0;"> --}}

                <div class="{{ $field_content_width }}" style = "{{ $style }}">

                    @if (!empty($field_label))
                        @if ($type_name != 'HEADER TEXT')
                            <span class="header_label_text">{!! $field_label !!}</span><br />
                        @endif
                    @endif

                    @if ($type_name == 'TEXT')

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        <input type="text" class="form-control custom-text-box" name="{{ $field_name }}"
                            id="{{ $field_name }}" placeholder="" value="{{ $field_value }}">

                        @if ($favorite_option_enabled == 1)
                            <a class="btn btn-sm btn-default fav-button-bottom assess_input_fav" title="Add To Bookmark"
                                onclick="addToFavFormItem(this,'{{ $form_field_id }}')"><i
                                    class="fa fa-star-o"></i></a>
                        @endif

                        <a class="btn btn-sm btn-default fav-button-bottom assess_input_refresh"
                            title="Set Last Entered Data"
                            onclick="oldDataPreFetch('{{ $field_name }}','{{ $form_id }}','{{ $patient_id }}')"><i
                                class="fa fa-history"></i></a>

                    @elseif($type_name == "DATE")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        <input type="text" class="form-control datepicker" name="{{ $field_name }}"
                            id="{{ $field_name }}" placeholder="" value="{{ $field_value }}">

                        @elseif($type_name == "TIME")

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    if ($head_id == 0):
                                        $field_value = $default_value;
                                    endif;
                                }
                            @endphp
    
                            <input type="time" class="form-control timepicker" name="{{ $field_name }}"
                                id="{{ $field_name }}" placeholder="" value="{{ $field_value }}">

                    @elseif($type_name == "PATIENT DETAILS")

                        @php
                            $field_value = '';
                            if (isset($default_value) && !empty($default_value) && $patient_id != '') {
                                $fetchfields = explode('|', $default_value);
                                foreach ($fetchfields as $field) {
                                    $field = trim($field);
                                    if ($patient_id) {
                                        //field name is gender then fetch gender
                                        if ($field == 'gender') {
                                            $fieldgender = \DB::table('patient_master')
                                                ->where('id', '=', $patient_id)
                                                ->value($field);
                                            $field_value .=
                                                \DB::table('gender')
                                                    ->where('id', '=', $fieldgender)
                                                    ->value('name') . ' ';
                                        } elseif ($field == 'admitting_doctor') {
                                            $fieldadmitting_doctor = \DB::table('ip_visits')
                                                ->where('id', '=', $patient_id)
                                                ->orderBy('id', 'desc')
                                                ->limit(1)
                                                ->value('admitting_doctor');

                                            $field_value .= $fieldipno . ' ';
                                        } elseif ($field == 'admission_no') {
                                            $fieldipno = \DB::table('ip_visits')
                                                ->where('id', '=', $patient_id)
                                                ->orderBy('id', 'desc')
                                                ->limit(1)
                                                ->value($field);
                                            $field_value .= $fieldipno . ' ';
                                        } elseif ($field == 'marital_status') {
                                            $fieldpatient = \DB::table('patient_master')
                                                ->where('id', '=', $patient_id)
                                                ->value($field);
                                            if ($fieldpatient == '2') {
                                                $field_value .= ' Married ';
                                            } elseif ($fieldpatient == '1') {
                                                $field_value .= ' Single ';
                                            } else {
                                                $field_value .= ' Not Specified ';
                                            }
                                        } else {
                                            $field_value .=
                                                \DB::table('patient_master')
                                                    ->where('id', '=', $patient_id)
                                                    ->value($field) . ' ';
                                        }
                                    }
                                }
                            }
                        @endphp

                        <input type="text" class="form-control" name="{{ $field_name }}" id="{{ $field_name }}"
                            placeholder="" value="{{ $field_value }}">

                    @elseif($type_name == "TINYMCE")

                        @php

                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                            $field_value = str_replace("{{patient@name}}","<i style='font-weight:400;'>".$patient_details[0]->patient_name."</i>",$field_value);
                            $field_value = str_replace("{{patient@age}}","<i style='font-weight:400;'>".$patient_details[0]->age."</i>",$field_value);
                            $field_value = str_replace("{{patient@address}}","<i style='font-weight:400;'>".$patient_details[0]->address."</i>",$field_value);
                            $field_value = str_replace("{{patient@mobile}}","<i style='font-weight:400;'>".$patient_details[0]->phone."</i>",$field_value);
                            $field_value = str_replace("{{patient@uhid}}","<i style='font-weight:400;'>".$patient_details[0]->uhid."</i>",$field_value);
                            $field_value = str_replace("{{patient@consulting_doctor}}","<i style='font-weight:400;'>".$patient_details[0]->doctor_name."</i>",$field_value);



                            if ($include_patient_header == 1 && empty($entered_value)) {
                                $field_value = base64_decode($patient_header) . '<br>' . $field_value;
                            }

                            array_push($tinyNamesArr, $field_name);
                        @endphp

                        <textarea style="width:100%;" name="{{ $field_name }}" class="form-control texteditor"
                            id="{{ $field_name }}" cols="30" rows="10">
                            <div style="width:100% !important; word-break: break-word !important;">
                                {!! $field_value !!}
                            </div>
                        </textarea>
                        <input type="hidden" name="include_patient_header" id="include_patient_header"
                            value="{!! $include_patient_header !!}" />

                    @elseif($type_name == "CHECK BOX")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        <ul class="select_button no-padding">
                            <li @if ($field_name == $field_value) class="active" @endif data-value="{{ $field_name }}">
                                <i class="fa fa-check-circle"></i>{{ $default_value }}
                                <input type="hidden" name="{{ $field_name }}" id="{{ $field_name }}"
                                    @if ($field_name == $field_value) value="{{ $field_name }}" @else disabled="disabled" value="" @endif />
                            </li>
                        </ul>

                    @elseif($type_name == "SELECT BOX")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }

                            $options = [];
                            $options = ExtensionsValley\Emr\FormFieldSelectboxOptions::where('form_field_id', '=', $form_field_id)->pluck('name', 'value');

                        @endphp

                        {!! Form::select($field_name, $options, !empty($field_value) ? $field_value : [], ['class' => 'form-control', 'placeholder' => 'Select Option', 'title' => $field_label]) !!}

                    {{-- @elseif($type_name == "RADIO BUTTON")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        @if (isset($form_field_id) && !empty($form_field_id))
                            @php
                                $radio = ExtensionsValley\Emr\FormFieldRadioOptions::where('form_field_id', '=', $form_field_id)->pluck('name', 'value');
                                $k = 0;
                            @endphp
                        @endif
                        @php
                        $value = 0;
                        $checked = '';
                        @endphp
                        @if (sizeof($radio) > 0)
                            @foreach ($radio as $value => $name)
                            
                                @php
                               if ($field_value == $value){
                                $checked="checked";
                               }  else{
                                $checked="";

                               }

                                $value = $value ;
                                    $k++;
                                @endphp
                            @endforeach
                        @endif
                        <input type="radio" $checked  name="hurts"
                        id="radio-{{ $k }}" value="{{ $value }}"> --}}
                        @elseif($type_name == "RADIO BUTTON")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        @if (isset($form_field_id) && !empty($form_field_id))
                            @php
                                $radio = ExtensionsValley\Emr\FormFieldRadioOptions::where('form_field_id', '=', $form_field_id)->orderBy('value')->pluck('name', 'value');
                                $k = 0;
                            @endphp
                        @endif

                        @if (sizeof($radio) > 0)
                            @foreach ($radio as $value => $name)
                                <input type="radio" @if ($field_value == $value) checked="checked" @endif name="{{ $field_name }}"
                                    id="radio-{{ $k }}" value="{{ $value }}">
                                {{ $name }}
                                @php
                                    $k++;
                                @endphp
                            @endforeach
                        @endif
                    @elseif($type_name == "GROUPED CHECKBOX")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                        @endphp

                        @if (isset($form_field_id) && !empty($form_field_id))
                            @php
                                $gcheckbox = ExtensionsValley\Emr\FormFieldGroupedCheckboxOptions::where('form_field_id', '=', $form_field_id)->pluck('name', 'value');
                                $g = 0;
                            @endphp
                        @endif

                        @if (sizeof($gcheckbox) > 0)
                            <ul class="select_button no-padding">
                                @foreach ($gcheckbox as $value => $name)

                                    @php
                                        $key_exist = property_exists($field_value, $value);
                                    @endphp
                                    <li @if ($key_exist) class="active" @endif data-value="{{ $value }}">
                                        <i class="fa fa-check-circle"></i>{{ $name }}
                                        <input type="hidden" name="{{ $field_name }}_gbox_{{ $value }}"
                                            id="gcheckbox-{{ $g }}" @if ($key_exist) value="{{ $value }}" @else disabled="disabled" value="" @endif>
                                    </li>
                                    @php
                                        array_push($gcheckboxNamesArr, $field_name . '_gbox_' . $value);
                                        $g++;
                                    @endphp
                                @endforeach
                            </ul>
                        @endif

                    @elseif($type_name == "PROGRESSIVE SEARCH")
                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }

                            if (!empty($progress_table)):
                                if (!empty((array) $field_value)):
                                    $selected_value = array_keys((array) $field_value);
                                    $selected_list = array_values((array) $field_value);
                                endif;

                                $optionsListSelected = \DB::table($progress_table)
                                    ->select([$progress_value, $progress_text])
                                    ->whereIn($progress_value, $selected_value)
                                    ->whereIn($progress_text, $selected_list)
                                    ->whereNull('deleted_at')
                                    ->get();
                            endif;

                            array_push($psearchNamesArr, $field_name);

                        @endphp

                        <select class="progress-search-multiple form-control" name="{{ $field_name }}"
                            id="{{ $field_name }}" data-table="{{ $progress_table }}"
                            data-value="{{ $progress_value }}" data-text="{{ $progress_text }}"
                            multiple="multiple">
                            @if (count($optionsListSelected) > 0)
                                @for ($p = 0; $p < count($optionsListSelected); $p++)
                                    <option selected="selected" value="{!! $optionsListSelected[$p]->$progress_value !!}">{!! $optionsListSelected[$p]->$progress_text !!}
                                    </option>
                                @endfor
                            @endif
                        </select>

                    @elseif($type_name == "TABLE")

                        @if (!empty($table_details))

                            @php
                                $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : 0;
                                if (!empty($entered_value)) {
                                    $field_value = $entered_value;
                                } else {
                                    if ($head_id == 0):
                                        $field_value = $default_value;
                                    endif;
                                }

                                array_push($tableNamesArr, $field_name);

                                $Tdetails = json_decode($table_details);
                                $Tstructure = json_decode($table_structure,true);

                                $table_details = $Tdetails->$field_name;

                                $table_structure = @$Tstructure->$field_name ? $Tstructure->$field_name : array();

                                $rows = isset($table_details->rows) ? $table_details->rows : 0;

                                $columns = isset($table_details->columns) ? $table_details->columns : 0;

                                $rowHeader = isset($table_details->rowHeader) ? $table_details->rowHeader : 0;
                                $colHeader = isset($table_details->colHeader) ? $table_details->colHeader : 0;
                                if ($rowHeader == 1) {
                                    ++$columns;
                                }
                                if ($colHeader == 1) {
                                    ++$rows;
                                }

                                $tableColsArr = [];

                            @endphp

                            <div class="clearfix"></div>
                            <div class="theadscroll1 always-visible table_{{ $form_id }}">
                                <table class="table table-bordered table-striped">
                                    @if($field_name == 'pain_score_table')
                                    <tr>
                                        <td>DATE</td>
                                        <td>TIME</td>
                                        <td>PAIN SCORE</td>
                                        <td>INTERVENTION PO/IV</td>
                                        <td>TIME</td>
                                        <td>PAINSCORE POST INTERVENTION</td>
                                        <td>COMPLICATION</td>
                                        <td>NAME & SIGN</td>
                                    </tr>
                                    @elseif($field_name == 'drug_history1')
                                    <tr>
                                        <td>DATE</td>
                                        <td>TIME</td>
                                        <td>DRUGS</td>
                                        <td>DOSE</td>
                                        <td>ROUTE</td>
                                        <td>REFFERED BY</td>
                                        <td>FREQUENCY</td>
                                        <td>ADMINISTERED BY</td>
                                    </tr>
                                    @elseif($field_name == 'reassessment_vital1')
                                    <tr>
                                        <td>TIME</td>
                                        <td>HR</td>
                                        <td>PR</td>
                                        <td>BP</td>
                                        <td>SPO2</td>
                                        <td>IVF</td>
                                        <td>URINE</td>
                                        <td>OTHERS</td>
                                    </tr>
                                    @endif    
                                    @for ($m = 0; $m < $rows; $m++)
                                        <tr>
                                            @for ($n = 0; $n < $columns; $n++)
                                                @php
                                                    $fieldSelValue = '';

                                                    $rwName = 'rows_' . $m;

                                                    //already enterd data
                                                    $already_entered_row_key = 'rows_' . $m;
                                                    $already_entered_column = !empty($field_value->$already_entered_row_key) ? $field_value->$already_entered_row_key : [];
                                                    if (sizeof($already_entered_column) > 0) {
                                                        $fieldSelValue = !empty($already_entered_column[$n]) ? $already_entered_column[$n] : '';
                                                    } else {
                                                        $row_data = !empty($table_structure->$rwName) ? $table_structure->$rwName : '';
                                                        $fieldSelValue = !empty($row_data[$n]) ? $row_data[$n] : '';

                                                    }

                                                    $fieldId = $field_name . '_' . $m . '_' . $n;
                                                    $fieldNm = $field_name . '_row_' . $m . '_' . $n . '[]';
                                                    array_push($tableColsArr, $fieldNm);
                                                @endphp
                                                <td>
                                                    <input @if ($colHeader == 1 && $m == 0) readonly tabIndex="-1" @endif @if ($rowHeader == 1 && $n == 0) readonly tabIndex="-1" @endif type="text"
                                                        name="{{ $fieldNm }}" id="{{ $fieldId }}"
                                                        value="{{ $fieldSelValue }}"
                                                        class="form-control assessment_table">
                                                </td>
                                            @endfor
                                        </tr>
                                    @endfor
                                </table>
                            </div>
                            <!-- all table column names array -->
                            <input type="hidden" name="{{ $field_name . '_#colsCount' }}" disabled
                                @if (isset($n)) value="{{ json_encode($n) }}" @endif>
                            <input type="hidden" name="{{ $field_name . '_#rowsCount' }}" disabled
                                @if (isset($m)) value="{{ json_encode($m) }}" @endif>
                        @endif

                    @elseif($type_name == "HEADER TEXT")

                        @php
                            $style = '';
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }
                            if( $field_name == 'emergency'){
                                $style = 'text-align: center;font-size: 23px;text-decoration: underline;';
                            }else{
                                $style="text-align: left;";
                            }
                        @endphp

                        <div class="col-md-12 header-text text-center" style="{{ $style }}">{!! $default_value !!}</div>

                    @elseif($type_name == "CERTIFICATE")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):
                                    $field_value = $default_value;
                                endif;
                            }

                            array_push($certNamesArr, $field_name);
                        @endphp

                        <textarea name="{{ $field_name }}" class="form-control texteditor" id="{{ $field_name }}"
                            cols="30" rows="10">{{ $field_value }}</textarea>

                    @elseif($type_name == "TEXT AREA")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                if ($head_id == 0):


                                    //$field_value = $default_value;
                                endif;
                            }
                        @endphp

                        <textarea name="{{ $field_name }}" class="form-control custom-text-area"
                            id="{{ $field_name }}" placeholder="">{{ $field_value }}</textarea>

                        @if ($favorite_option_enabled == 1)
                            <a class="btn btn-sm fav_textarea_icon btn-default fav-button-bottom"
                                title="Add To Bookmark" style="float: right;margin:2px 18px 2px 2px;"
                                onclick="addToFavFormItem(this,'{{ $form_field_id }}')"><i
                                    class="fa fa-star-o"></i></a>
                        @endif

                        <a class="btn btn-sm refresh_textarea_icon btn-default prev-button-bottom"
                            title="Set Last Entered Data" style="float: right;margin:2px 18px 2px 2px;"
                            onclick="oldDataPreFetch('{{ $field_name }}','{{ $form_id }}','{{ $patient_id }}')"><i
                                class="fa fa-history"></i></a>

                    @elseif($type_name == "CUSTOM DATA")

                        @php
                            $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                            $default_value = !empty($default_value) ? trim($default_value) : '';

                            if (!empty($entered_value)) {
                                $field_value = $entered_value;
                            } else {
                                //Patient Allergy
                                if ($default_value == 'GET_PATIENT_ALLERGIES') {
                                    if (!empty($patientAllergyArr['other_allergies'])) {
                                        $other_allergy_html =
                                            "<table width='50%' border='1' style='border-collapse: collapse;margin-top:20px;'>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <tr>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <th colspan='2' style='height: 25px;'>Other Allergies</th>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <tr>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <td colspan='2'>" .
                                            $patientAllergyArr['other_allergies'] .
                                            "</td>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </table>";
                                        $field_value .= $other_allergy_html;
                                    }
                                    if (!empty($patientAllergyArr['patient_allergy'])) {
                                        if (sizeof($patientAllergyArr['patient_allergy']) > 0) {
                                            $allergy_html = "<table width='50%' border='1' style='border-collapse: collapse;margin-top:20px;'>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <tr>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <th colspan='2' style='height: 25px;'>Allergies</th>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </tr>";
                                            for ($a = 0; $a < sizeof($patientAllergyArr['patient_allergy']); $a++) {
                                                $rdata = !empty($patientAllergyArr['patient_allergy'][$a]) ? $patientAllergyArr['patient_allergy'][$a] : [];
                                                $allergy_html .= "<tr><td colspan='2'>" . $rdata->description . '</td></tr>';
                                            }
                                            $allergy_html .= '</table>';
                                            $field_value .= $allergy_html;
                                        }
                                    }
                                } elseif ($default_value == 'GET_PATIENT_VITALS') {
                                    //Patient Vitals
                                    if (!empty($patientVitalsArr)) {
                                        if (sizeof($patientVitalsArr) > 0) {
                                            $vital_html = "<table width='50%' border='1' style='border-collapse: collapse;margin-top:20px;'>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <tr>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <th colspan='2' style='height: 25px;'>Vitals</th>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </tr>";
                                            for ($a = 0; $a < sizeof($patientVitalsArr); $a++) {
                                                $rdata = !empty($patientVitalsArr[$a]) ? $patientVitalsArr[$a] : [];
                                                $vital_html .=
                                                    "<tr>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <td>" .
                                                    $rdata->name .
                                                    "</td>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <td>" .
                                                    $rdata->vital_value .
                                                    "</td>\n
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </tr>";
                                            }
                                            $vital_html .= '</table>';
                                            $field_value .= $vital_html;
                                        }
                                    }
                                }
                            }

                            array_push($customDataArr, $field_name);
                        @endphp

                        <textarea name="{{ $field_name }}" class="form-control texteditor" id="{{ $field_name }}"
                            cols="30" rows="10">{{ $field_value }}</textarea>

                    @endif

                </div>

                <!-- If Favorite Option Enabled -->
                @if ($favorite_option_enabled == 1)
                    <div class="{{ $field_favorite_width }}">
                        <div class="floating-table-list">
                            <div class="theadscroll" id="fav-{{ $field_name }}"
                                style="position: relative; height: auto; max-height: 400px; padding-right: 5px;">
                                @php
                                    $favformitmlistTextBox = [];
                                    if (!empty($favorited_items_field_arr[$form_field_id])) {
                                        $favformitmlistTextBox = !empty($favorited_items_field_arr[$form_field_id]) ? $favorited_items_field_arr[$form_field_id] : [];
                                    }
                                @endphp

                                @if (isset($favformitmlistTextBox) && !empty($favformitmlistTextBox) && count($favformitmlistTextBox) > 0)
                                    <table id="favListTable-{{ $i }}"
                                        class="table table-bordered table_sm no-margin table-striped fav-list-table ">
                                        <thead>
                                            <tr class="table_header_bg">
                                                <th colspan="2">Bookmarks &nbsp;
                                                    <i class="fa fa-caret-down"></i>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($favformitmlistTextBox as $dataitms)
                                                @if (isset($form_field_id) && !empty($form_field_id) && isset($dataitms['form_field_id']) && !empty($dataitms['form_field_id']) && $form_field_id == $dataitms['form_field_id'])
                                                    @php
                                                        $cur_fav_form_item_id = isset($dataitms['form_field_id']) ? $dataitms['form_field_id'] : '';
                                                        $cur_fav_table_id = isset($dataitms['id']) ? $dataitms['id'] : '';
                                                        $cur_fav_val = isset($dataitms['value']) ? $dataitms['value'] : '';
                                                        $cur_fav_text = isset($dataitms['text']) ? $dataitms['text'] : '';
                                                    @endphp

                                                    <tr>
                                                        <td style="height:30px;cursor: pointer;"
                                                            onclick='addToSelected("{{ $field_name }}","{!! base64_encode($cur_fav_text) !!}","{{ $type_name }}")'>
                                                            {!! $cur_fav_text !!}</td>
                                                        <td width="5%">
                                                            <a style="float: right;" class="color-red"
                                                                name="remove"
                                                                onclick='removeFavorite("{{ $cur_fav_table_id }}","{{ base64_encode($cur_fav_text) }}",this)'
                                                                title="remove From List"><i
                                                                    class="fa fa-trash-o"></i></a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <table id="favListTable-{{ $i }}"
                                        class="table table-bordered table_sm no-margin table-striped fav-list-table hidden">
                                        <thead>
                                            <tr class="table_header_bg">
                                                <th colspan="2">Bookmarks &nbsp;
                                                    <i class="fa fa-caret-down"></i>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
                <!-- If Favorite Option Enabled -->

            {{-- </div> --}}
          
        @endfor
            {{-- <div class="btn-group col-md-12 " data-toggle="buttons"   >

                <label>PAIN SCORE</label>
                <div class="clearfix"></div>
                <span class="show_print" id="show_print_pain"></span>
                <span class="hide_print">
                    <div id="static_component_pain_assessment_scale" style="display: flex;">
                        <div class="pain_scale_radio">
                            <input type="radio" id="option_0" name="pain_assessment_scale" value="0">
                            <label for="option_0" class="radio-image">
                                <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_1.png') }}">
                                <span>0 <br> No <br> Hurt</span>
                            </label>
                        </div>
                        <div class="pain_scale_radio">
                            <input type="radio" id="option_1" name="pain_assessment_scale" value="1">
                            <label for="option_1" class="radio-image">
                                <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_2.png') }}">
                                <span>2 <br> Hurts <br> Little Bit</span>
                            </label>
                        </div>
                        <div class="pain_scale_radio">
                            <input type="radio" id="option_2" name="pain_assessment_scale" value="2">
                            <label for="option_2" class="radio-image">
                                <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_3.png') }}">
                                <span>4 <br> Hurts <br> Little More</span>
                            </label>
                        </div>
                        <div class="pain_scale_radio">
                            <input type="radio" id="option_3" name="pain_assessment_scale" value="3">
                            <label for="option_3" class="radio-image">
                                <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_4.png') }}">
                                <span>6 <br> Hurts <br> Even More</span>
                            </label>
                        </div>
                        <div class="pain_scale_radio">
                            <input type="radio" id="option_4" name="pain_assessment_scale" value="4">
                            <label for="option_4" class="radio-image">
                                <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_5.png') }}">
                                <span>8 <br> Hurts <br> Whole Lot</span>
                            </label>
                        </div>
                        <div class="pain_scale_radio">
                            <input type="radio" id="option_5" name="pain_assessment_scale" value="5">
                            <label for="option_5" class="radio-image">
                                <img class="pain_scale_smiley" src="{{ url('packages/extensionsvalley/default/img/template/option_9.png') }}">
                                <span>10 <br> Hurts <br> Worst</span>
                            </label>
                        </div>
                        
                    
                        
                    </div>
                </span>
            </div> --}}
        @if($is_finalized == 1)

            <div class="col-md-8" style="margin-top:10px;">
                <div class="form-group">
                    <label class="control-label"><h5>Addendum</h5></label>
                    <textarea name="addendum" class="form-control" id="addendum" cols="30" rows="10"></textarea>
                </div>
            </div>
        @endif



    @endif
    <input type="hidden" disabled name="tinymce_names" id="tinymce_names" value="{{ json_encode($tinyNamesArr) }}" />
    <input type="hidden" disabled name="cert_names" id="cert_names" value="{{ json_encode($certNamesArr) }}" />
    <input type="hidden" disabled name="custom_field_data" id="custom_field_data"
        value="{{ json_encode($customDataArr) }}" />
    <input type="hidden" disabled name="table_names" id="table_names" value="{{ json_encode($tableNamesArr) }}" />
    <input type="hidden" disabled name="gcheckbox_names" id="gcheckbox_names"
        value="{{ json_encode($gcheckboxNamesArr) }}" />
    <input type="hidden" disabled name="psearch_names" id="psearch_names"
        value="{{ json_encode($psearchNamesArr) }}" />
@endif
<!-- Field Content -->
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
