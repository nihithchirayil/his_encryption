
    <table class="table tabe_sm table-bordered" width="100%" style="border-collapse: collapse;">
        <tr>
            <td colspan="2">
                <table width="100%" class="table tabe_sm table-bordered table-striped" style="border-collapse: collapse;border-color: #ccc;">
                @if(!empty($all_form_fields))
                    @if(sizeof($all_form_fields) > 0)
                        @for($i=0; $i<sizeof($all_form_fields); $i++)
                            @php
                                $type_name = "";
                                $type = "";
                                $default_value = "";
                                $entered_value = "";
                                $field_value = "";
                                $is_required = "";
                                $field_name = "";
                                $field_label = "";
                                $progress_table = "";
                                $progress_value = "";
                                $progress_text = "";
                                $class_name = "";
                                $table_structure = "";
                                $table_details = "";
                                $field_width = "";
                                $field_height = "";
                                $form_id = "";
                                $form_field_id = "";

                                $type_name = (!empty($all_form_fields[$i]->type_name)) ? $all_form_fields[$i]->type_name : '';
                                $type = (!empty($all_form_fields[$i]->type)) ? $all_form_fields[$i]->type : '';
                                $type = (!empty($all_form_fields[$i]->type)) ? $all_form_fields[$i]->type : '';
                                $field_name = (!empty($all_form_fields[$i]->field_name)) ? $all_form_fields[$i]->field_name : '';
                                $field_label = (!empty($all_form_fields[$i]->field_label)) ? $all_form_fields[$i]->field_label : '';
                                //$default_value = (!empty($all_form_fields[$i]->default_value)) ? $all_form_fields[$i]->default_value : '';
                                $form_field_id = (!empty($all_form_fields[$i]->form_field_id)) ? $all_form_fields[$i]->form_field_id : '';
                                $form_id = (!empty($all_form_fields[$i]->form_id)) ? $all_form_fields[$i]->form_id : '';
                                //progress search
                                $progress_table = (!empty($all_form_fields[$i]->progress_table)) ? $all_form_fields[$i]->progress_table : '';
                                $progress_value = (!empty($all_form_fields[$i]->progress_value)) ? $all_form_fields[$i]->progress_value : '';
                                $progress_text = (!empty($all_form_fields[$i]->progress_text)) ? $all_form_fields[$i]->progress_text : '';
                                //table
                                $table_structure = (!empty($all_form_fields[$i]->table_structure)) ? $all_form_fields[$i]->table_structure : '';
                                $table_details = (!empty($all_form_fields[$i]->table_details)) ? $all_form_fields[$i]->table_details : '';

                                $radio = [];
                                $gcheckbox = [];
                                $selected_value = [];
                                $optionsListSelected = [];

                            @endphp


                            <tr>
                                @if($type_name == "TEXT")

                                    @php
                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                        if(!empty($entered_value)){
                                            $field_value = $entered_value;
                                        }else{
                                            $field_value = $default_value;
                                        }
                                    @endphp

                                    @if($field_value != '')
                                    <td width="30%">{{$field_label}}</td>
                                    <td>{!!$field_value!!}</td>
                                    @endif

                                @elseif($type_name == "TEXT AREA")

                                    @php
                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                        if(!empty($entered_value)){
                                            $field_value = $entered_value;
                                        }else{
                                            $field_value = $default_value;
                                        }
                                    @endphp

                                    @if($field_value != '')
                                    <td width="30%">{{$field_label}}</td>
                                    <td>{!!$field_value!!}</td>
                                    @endif

                                @elseif($type_name == "DATE")

                                    @php
                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                        if(!empty($entered_value)){
                                            $field_value = $entered_value;
                                        }else{
                                            $field_value = $default_value;
                                        }
                                    @endphp

                                    @if($field_value != '')
                                    <td width="30%">{{$field_label}}</td>
                                    <td>{!!$field_value!!}</td>
                                    @endif

                                @elseif($type_name == "TINYMCE")

                                    @php
                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                        if(!empty($entered_value)){
                                            $field_value = $entered_value;
                                        }else{
                                            $field_value = $default_value;
                                        }
                                    @endphp

                                    @if($field_value != '')
                                    <td width="30%">{{$field_label}}</td>
                                    <td>{!!$field_value!!}</td>
                                    @endif

                                @elseif($type_name == "CERTIFICATE")

                                    @php
                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                        if(!empty($entered_value)){
                                            $field_value = $entered_value;
                                        }else{
                                            $field_value = $default_value;
                                        }
                                    @endphp

                                    <td width="30%">{{$field_label}}</td>
                                    <td>{!!$field_value!!}</td>

                                @elseif($type_name == "CHECK BOX")

                                    @php

                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';

                                        $checked_box_default_value = "";

                                        if(!empty($entered_value)){

                                            $field_value = $entered_value;

                                            //if check box checked then get default value as name
                                            $checked_box_default_value = (!empty($all_form_fields[$i]->default_value)) ? $all_form_fields[$i]->default_value : '';

                                        }else{

                                            $field_value = $default_value;

                                        }

                                    @endphp

                                    @if($field_name == $field_value)
                                        <td width="30%">{{$field_label}}</td>
                                        <td>{{$checked_box_default_value}}</td>
                                    @endif

                                @elseif($type_name == "SELECT BOX")

                                    @php
                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                        if(!empty($entered_value)){
                                            $field_value = $entered_value;
                                        }else{
                                            $field_value = $default_value;
                                        }

                                        $options = "";
                                        if(!empty($field_value)):
                                            $options = ExtensionsValley\Emr\FormFieldSelectboxOptions::where('form_field_id', '=', $form_field_id)->where('value','=',$field_value)->value('name');
                                        endif;
                                    @endphp

                                    @if(!empty($options))
                                        <td width="30%">{{$field_label}}</td>
                                        <td>{!!$options!!}</td>
                                    @endif

                                @elseif($type_name == "RADIO BUTTON")

                                    @php
                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                        if(!empty($entered_value)){
                                            $field_value = $entered_value;
                                        }else{
                                            $field_value = $default_value;
                                        }
                                    @endphp

                                    @if (isset($form_field_id) && !empty($form_field_id))
                                        @php
                                            $radio = "";
                                            if(!empty($field_value)):
                                                $radio = ExtensionsValley\Emr\FormFieldRadioOptions::where('form_field_id', '=', $form_field_id)->where('value','=',$field_value)->value('name');
                                            endif;
                                        @endphp
                                    @endif

                                    @if(!empty($radio))
                                        <td width="30%">{{$field_label}}</td>
                                        <td>{!!$radio!!}</td>
                                    @endif

                                @elseif($type_name == "GROUPED CHECKBOX")

                                        @php
                                            $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                            if(!empty($entered_value)){
                                                $field_value = $entered_value;
                                            }else{
                                                $field_value = $default_value;
                                            }
                                        @endphp

                                        @if(sizeof((array)$field_value) > 0)
                                            @php
                                                $datArr = [];
                                                $datArr = (array)$field_value;
                                            @endphp

                                            @if($field_value != '')
                                            <td width="30%">{{$field_label}}</td>
                                            <td>
                                                @foreach($datArr as $ind => $data)
                                                    {!!$data!!} <br>
                                                @endforeach
                                            </td>
                                            @endif
                                        @endif

                                @elseif($type_name == "HEADER TEXT")

                                    @php
                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                        if(!empty($entered_value)){
                                            $field_value = $entered_value;
                                        }else{
                                            $field_value = $default_value;
                                        }
                                    @endphp

                                    <td colspan="2"><b>{!!$default_value!!}</b></td>

                                @elseif($type_name == "PROGRESSIVE SEARCH")

                                    @php
                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                        if(!empty($entered_value)){
                                            $field_value = $entered_value;
                                        }else{
                                            $field_value = $default_value;
                                        }
                                    @endphp

                                    @if(sizeof((array)$field_value) > 0)
                                        @php
                                            $datProArr = [];
                                            $datProArr = (array)$field_value;
                                        @endphp

                                        @if($field_value != '')
                                        <td width="30%">{{$field_label}}</td>
                                        <td>
                                            @foreach($datProArr as $ind => $pro_data)
                                                {!!$pro_data!!} <br>
                                            @endforeach
                                        </td>
                                        @endif
                                    @endif

                                @elseif($type_name == "TABLE")

                                    @php
                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                        if(!empty($entered_value)){
                                            $field_value = $entered_value;
                                        }else{
                                            $field_value = $default_value;
                                        }

                                        $tableDatArr = [];
                                        $tableDatArr = (array)$field_value;
                                    @endphp

                                    @if(sizeof($tableDatArr) > 0)
                                    @if(isset($tableDatArr[1]))
                                    @if(sizeof($tableDatArr[1]) > 0)
                                    <td colspan="2" class="no-padding">
                                    <table width="99%" class="table tabe_sm no-margin table-bordered table-striped" >
                                        @foreach($tableDatArr as $rows => $colsArr)
                                            <tr>
                                                @foreach($colsArr as $colsind => $cols)
                                                    <td>{{$cols}}</td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </table>
                                    </td>
                                    @endif
                                    @endif
                                    @endif


                                @elseif($type_name == "CUSTOM DATA")

                                    @php
                                        $entered_value = (!empty($already_entered->$field_name)) ? $already_entered->$field_name : '';
                                        if(!empty($entered_value)){
                                            $field_value = $entered_value;
                                        }else{
                                            $field_value = $default_value;
                                        }
                                    @endphp

                                    @if($field_value != '')
                                    <td width="30%">{{$field_label}}</td>
                                    <td>{!!$field_value!!}</td>
                                    @endif
                                @endif
                            </tr>

                        @endfor
                    @endif
                    @endif
                    </table>
                </td>
            </tr>
            </table>

