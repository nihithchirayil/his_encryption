<style>
    .box-body{
        background-color: white !important;
        border-color:black !important;
    }
    .pain_score_img li{
        width:75px;
    }
    .col-md-12 { width: 100%; }.col-md-11 { width: 91.66666667%; }.col-md-10 { width: 83.33333333%; }.col-md-9 { width: 75%; }.col-md-8 { width: 66.66666667%; }.col-md-7 { width: 58.33333333%; }.col-md-6 { width: 50%; }.col-md-5 { width: 41.66666667%; }.col-md-4 { width: 33.33333333%; }.col-md-3 { width: 25%; }.col-md-2 { width: 16.66666667%; }.col-md-1 { width: 8.33333333%; }

    @media print {

    .pagebreak{
          page-break-before: always !important;
    }

    #pageborder{
        position: fixed;
        left: 0;
        right: 0;
        top: 0;
        bottom:0;
        border: 1px solid black;
    }
}
</style>


<div id="pageborder"></div>
<div class="col-md-12" style="width: 100%;float:none;">
    @if (!empty($hospitalHeader))
    @php
        $hospitalHeader = str_replace('border="1"', '', $hospitalHeader);
    @endphp

        {!! $hospitalHeader !!}
        <hr style="margin-top:0px; margin-bottom:20px; ">
    @endif
    <div class="col-md-12 no-padding" style="text-align:center;
    padding: 5px;">
        INITIAL ASSESSMENT FORM FOR OP/IP
    @php
        $all_form_fields = json_decode($all_form_fields,true);
        // print_r($all_form_fields['PASTHISTORYGSUR']);
        // exit;
    @endphp
    </div>
    <div class="col-md-12 no-padding box-body" style="display: flex;">
        <table class='table' style='width: 100%; font-size: 14px;font-family: Open Sans;
        font-size: 12px;
        font-weight: 500;'>
            <thead>
                <tr>
                    <td style="text-align: left;padding:5px;">Name:</td>
                    <td> {{ $patient_details[0]->patient_name }} </td>
                    <td style="text-align: left;padding:5px;">Age/Gender:</td>
                    <td>{{ $patient_details[0]->age }}/{{ $patient_details[0]->gender }}</td>
                    <td style="text-align: left;padding:5px;">Uhid:</td>
                    <td>{{ $patient_details[0]->uhid }}</td>

                </tr>
                <tr>
                    <td style="text-align: left;padding:5px;">Department</td>
                    <td> {{ $speciality }}</td>
                    <td style="text-align: left;padding:5px;">Ward/Room</td>
                    <td></td>
                    <td>Consultant</td>
                    <td> {{ $doctor_name }}</td>
                </tr>
            </thead>
        </table>
    </div>
    <div class="col-md-12 no-padding" style="display: flex;margin-top:15px;">
        <div class="col-md-7 no-padding" style="min-height:150px;border:1px solid black;">
            <h5 style="padding: 5px;margin-top:0px;text-align:center;"><b>Presenting Complaints & History</b></h5>
            <div class="col-md-12 no-padding" style="padding-left:5px !important;">
                {{@$all_form_fields['PRESENTINGCOMPLAINTS']??''}}
            </div>
        </div>
        <div class="col-md-5 no-padding" style="min-height:150px;border:1px solid black;">
            <h5 style="padding: 5px;margin-top:0px;text-align:center;"><b>Diaganosis</b></h5>
            <div class="col-md-12 no-padding" style="padding-left:5px !important;">
                {{@$all_form_fields['DIAGNOSISGSURR']??''}}
            </div>
        </div>
    </div>

    <div class="col-md-12 no-padding" style="margin-top:0px;">
        <h5 style="padding: 5px;background: #f6f6f6;"><b>Past History</b></h5>
        @php
            $PASTHISTORYSUR = @$all_form_fields['PASTHISTORYSUR']??[];
            $PASTHISTORYSUR_STRING = '';
            if(!empty($PASTHISTORYSUR)>0){
                $PASTHISTORYSUR_STRING = implode(',',$PASTHISTORYSUR);
            }
        @endphp
        <span style="padding-left:10px !important;">{{$PASTHISTORYSUR_STRING}}</span>
    </div>
    <div class="col-md-12 no-padding" style="margin-top:0px;">
        <h5 style="padding: 5px;background: #f6f6f6;"><b>Surgical History</b></h5>
        <span style="padding-left:10px !important;">{{@$all_form_fields['SURGICALHISTORYGSUR']??''}}</span>
    </div>
    <div class="col-md-12 no-padding" style="margin-top:0px;">
        <h5 style="background: #f6f6f6;
        padding: 5px;"><b>Family History</b></h5>
        <span style="padding-left:10px !important;">{{@$all_form_fields['FAMILYHISTORYGSUR']??''}}</span>
    </div>
    <div class="col-md-12 no-padding" style="break-after: always; page-break-after: always;">
        <h5 style="background: #f6f6f6;
        padding: 5px;"><b>Pesrsonal History</b></h5>
        @php
            $PERSONALHISTORYSUR = @$all_form_fields['PERSONALHISTORY']??[];
            $PERSONALHISTORYSUR_STRING = '';
            if(!empty($PERSONALHISTORYSUR)>0){
                $PERSONALHISTORYSUR_STRING = implode(',',$PERSONALHISTORYSUR);
            }
        @endphp
        <span style="padding-left:10px !important;">{{$PERSONALHISTORYSUR_STRING}}</span>
    </div>

    <div class="col-md-12 no-padding">
        <h5 style="background: #f6f6f6;
        padding: 5px;"><b>Drug History</b></h5>
        <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
            <thead style="color: rgb(53, 53, 53);
            background:#f6f6f6;">
                <tr>
                    <th style="font-family:roboto;">SlNo</th>
                    <th style="font-family:roboto;">Current Medications</th>
                    <th style="font-family:roboto;">Dosage</th>
                    <th style="font-family:roboto;">Schedule</th>
                    <th style="font-family:roboto;">Whether to be continued during hospitalisation yes/no</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $drug_history = $all_form_fields['DRUGHISTORYORTHOIP'];
                @endphp
                @for($i=1;$i<10;$i++)
                    @if($drug_history['rows_'.$i][1] !='')
                    <tr>
                        <td>{{$drug_history['rows_'.$i][0]}}</td>
                        <td>{{$drug_history['rows_'.$i][1]}}</td>
                        <td>{{$drug_history['rows_'.$i][2]}}</td>
                        <td>{{$drug_history['rows_'.$i][3]}}</td>
                        <td>{{$drug_history['rows_'.$i][4]}}</td>
                    </tr>
                    @endif
                @endfor
            </tbody>
        </table>
    </div>
    <div style="break-after: page"></div>
    <div class="col-md-12 no-padding" >
        <h5 style="background: #f6f6f6;
        padding: 5px;"><b>Gynaec & Obstetric History</b></h5>
        <span style="padding-left:10px !important;">{{@$all_form_fields['GYNAECOBSTETRICHISTORYORTHOIP']??''}}</span>
    </div>
    <div class="col-md-12 no-padding">
        <h5 style="background: #f6f6f6;
        padding: 5px;"><b>Physical Examination: Vital Signs</b></h5>
        <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 12px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;font-weight:200;'>
            @php
                $vital_signs = $all_form_fields['PHYSICALEXAMINATIONVITALSIGNSORTHOIP'];
            @endphp
            <thead>
                <tr>
                    <th style="background: #f6f6f6;color:#898787;">BP</th>
                    <th>{{$vital_signs['rows_0'][1]}}</th>
                    <th style="background: #f6f6f6;color:#898787;">PR</th>
                    <th>{{$vital_signs['rows_1'][1]}}</th>
                    <th style="background: #f6f6f6;color:#898787;">RR</th>
                    <th>{{$vital_signs['rows_2'][1]}}</th>
                    <th style="background: #f6f6f6;color:#898787;">TEMP</th>
                    <th>{{$vital_signs['rows_3'][1]}}</th>
                    <th style="background: #f6f6f6;color:#898787;">SPO2</th>
                    <th>{{$vital_signs['rows_4'][1]}}</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="col-md-12 no-padding">
        <h5 style="background: #f6f6f6;padding: 5px;"><b>General Examination</b></h5>
        @php
            $GENERALEXAMINATIONORTHOIP = @$all_form_fields['GENERALEXAMINATIONORTHOIP']??[];
            $GENERALEXAMINATIONORTHOIP_STRING = '';
            if(!empty($GENERALEXAMINATIONORTHOIP)>0){
                $GENERALEXAMINATIONORTHOIP_STRING = implode(',',$GENERALEXAMINATIONORTHOIP);
            }
        @endphp
        <span style="padding-left:10px !important;">{!!$GENERALEXAMINATIONORTHOIP_STRING!!}</span>
    </div>
    <div class="col-md-12 no-padding">
        <h5 style="background: #f6f6f6;padding: 5px;margin-top:0px;"><b>Lymph Node</b></h5>
        <span style="padding-left:10px !important;">{{@$all_form_fields['LymphNode']??''}}</span>
    </div>
    <div class="col-md-12 no-padding">
        <div class="col-md-6 no-padding">
            <h5 style="background: #f6f6f6;padding: 5px;margin-top:0px;"><b>Local Examination</b></h5>
            <span style="padding-left:10px !important;">{{@$all_form_fields['LOCALEXAMINATION']??''}}</span>
        </div>
        <div class="col-md-6 no-padding">
            <h5 style="background: #f6f6f6;padding: 5px;margin-top:0px;"><b>Investigation</b></h5>
            <span style="padding-left:10px !important;">{!!@$all_form_fields['invst']??''!!}</span>
        </div>
    </div>
    <div class="col-md-12 no-padding">
        <h5 style="background: #f6f6f6;padding: 5px;"><b>Systemic Examination</b></h5>
        <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
            @php
                $systemic_examinations = $all_form_fields['SYSTEMICEXAMINATION'];
            @endphp
            <tr>
                <td style="padding-left:5px;text-align:left;width:35%;background: #f6f6f6;color:#898787;">CVS</td>
                <td style="padding-left:5px;text-align:left;">{!!@$systemic_examinations['rows_0'][1]??''!!}</td>
            </tr>
                <td style="padding-left:5px;text-align:left;background: #f6f6f6;color:#898787;">Respiratory System</td>
                <td style="padding-left:5px;text-align:left;">{!!@$systemic_examinations['rows_1'][1]??''!!}</td>
            </tr>
                <td style="padding-left:5px;text-align:left;background: #f6f6f6;color:#898787;">GIT</td>
                <td style="padding-left:5px;text-align:left;">{!!@$systemic_examinations['rows_2'][1]??''!!}</td>
            </tr>
                <td style="padding-left:5px;text-align:left;background: #f6f6f6;color:#898787;">CNS</td>
                <td style="padding-left:5px;text-align:left;">{!!@$systemic_examinations['rows_3'][1]??''!!}</td>
            </tr>
        </table>
    </div>
    <div class="col-md-12 no-padding">
        <h5 style="background: #f6f6f6;padding: 5px;"><b>Psycological Examination</b></h5>

        <span style="padding-left:10px !important;">
            @php
                $PSYCHOLOGICALEVALUATIONORTHOIP = @$all_form_fields['PSYCHOLOGICALEVALUATIONORTHOIP']??'';
            @endphp
            @if($PSYCHOLOGICALEVALUATIONORTHOIP==1)
            Normal
            @elseif($PSYCHOLOGICALEVALUATIONORTHOIP==2)
            Anxious
            @elseif($PSYCHOLOGICALEVALUATIONORTHOIP==3)
            Depressed
            @endif
            <br>
            <span style="padding-left:10px !important;color:#898787;">Immunocompromised status : </span>
            @php
                $IMMUNOCOMPROMISED = @$all_form_fields['IMMUNOCOMPROMISED']??'';
            @endphp
            @if($IMMUNOCOMPROMISED==1)
                Yes
            @else
                No
            @endif
        </span>
    </div>

    <div class="col-md-12 no-padding">
        <h5 style="background: #f6f6f6;padding: 5px;"><b>Nutritional Screening</b></h5>
        <span style="padding-left:10px !important;">
            @php
                $nutritional_screening = $all_form_fields['NUTRITIONALSCREENING'];
            @endphp
            1)<span style="color:#898787;">Height:</span> {{$nutritional_screening['rows_0'][1]}}
            <span  style="color:#898787;">(cm) &nbsp;&nbsp;&nbsp;  2)Weight:</span> {{$nutritional_screening['rows_1'][1]}}<span  style="color:#898787;">kg &nbsp;&nbsp;&nbsp;  3)BMI</span> {{$nutritional_screening['rows_2'][1]}}<span  style="color:#898787;">kg/m2 &nbsp;&nbsp;&nbsp;  4)IBW</span>{{$nutritional_screening['rows_3'][1]}}<span  style="color:#898787;">kg</span>
            <br>
            <br>
            <span style="color:#898787;padding-left:10px;">Recent Weight Change</span>
            @php
                $recent_weight_change = @$all_form_fields['RECENTWEIGHTCHANGE']??0;
            @endphp
            @if($recent_weight_change == 1)
                Yes
            @else
                No
            @endif
            <br>
            <br>
            <span style="color:#898787;padding-left:10px;">Dietitian Consultation</span>
            @php
                $diettiancounsultationorthopip = @$all_form_fields['DIETITIANCONSULTATIONORTHOIP']??0;
            @endphp
            @if($diettiancounsultationorthopip==1)
                Yes
                @else
                No
                @endif
                <br>
            <br>
        </span>
    </div>

    <div style="break-after: page"></div>

    <div class="col-md-12 no-padding" style="display: flex;margin-top:10px;">
        <div class="col-md-6 box-body">
            <h5 style="text-align:center;background: #f6f6f6;padding: 5px;margin:3px;"><b>Pain Assessment</b></h5>
            <h6 style="padding-left:10px;"><b>Wong-Baker faces pain rating scale</b></h6>
            <ul class="list-unstyled pain_score_img" style="">
                <li><label for="score1">{!! Html::image('packages/extensionsvalley/default/img/pain_1.jpg')!!}</label>
                    @php
                        $PAINSCORESURE = @$all_form_fields['PAINSCORESURE']??'';
                    @endphp
                    @if($PAINSCORESURE  == 1 || $PAINSCORESURE == 2)
                        <i style="font-size: 41px;
                        text-align: center;
                        position: absolute;
                        z-index: 10000;
                        margin-top: 68px;
                        margin-left: -52px;
                        color: cornflowerblue;
                        opacity: .5;" class="fa fa-check"></i>
                    @endif
                </li>
                <li><label for="score2">{!! Html::image('packages/extensionsvalley/default/img/pain_2.jpg') !!}</label>
                    @if($PAINSCORESURE ==3 || $PAINSCORESURE ==4)
                        <i style="font-size: 41px;
                        text-align: center;
                        position: absolute;
                        z-index: 10000;
                        margin-top: 68px;
                        margin-left: -52px;
                        color: cornflowerblue;
                        opacity: .5;" class="fa fa-check"></i>
                    @endif

                </li>
                <li><label for="score3">{!! Html::image('packages/extensionsvalley/default/img/pain_3.jpg') !!}</label>
                    @if($PAINSCORESURE ==5 || $PAINSCORESURE ==6)
                        <i style="font-size: 41px;
                        text-align: center;
                        position: absolute;
                        z-index: 10000;
                        margin-top: 68px;
                        margin-left: -52px;
                        color: cornflowerblue;
                        opacity: .5;" class="fa fa-check"></i>
                    @endif
                </li>
                <li><label for="score4">{!! Html::image('packages/extensionsvalley/default/img/pain_4.jpg') !!}</label>
                    @if($PAINSCORESURE ==7 || $PAINSCORESURE ==8)
                        <i style="font-size: 41px;
                        text-align: center;
                        position: absolute;
                        z-index: 10000;
                        margin-top: 68px;
                        margin-left: -52px;
                        color: cornflowerblue;
                        opacity: .5;" class="fa fa-check"></i>
                    @endif
                </li>
                <li><label for="score5">{!! Html::image('packages/extensionsvalley/default/img/pain_5.jpg') !!}</label>
                    @if($PAINSCORESURE ==9 || $PAINSCORESURE ==10)
                        <i style="font-size: 41px;
                        text-align: center;
                        position: absolute;
                        z-index: 10000;
                        margin-top: 68px;
                        margin-left: -52px;
                        color: cornflowerblue;
                        opacity: .5;" class="fa fa-check"></i>
                    @endif
                </li>
                <li><label for="score6">{!! Html::image('packages/extensionsvalley/default/img/pain_6.jpg') !!}</label>
                    @if($PAINSCORESURE == 11 || $PAINSCORESURE == 12)
                        <i style="font-size: 41px;
                        text-align: center;
                        position: absolute;
                        z-index: 10000;
                        margin-top: 68px;
                        margin-left: -52px;
                        color: cornflowerblue;
                        opacity: .5;" class="fa fa-check"></i>
                    @endif
                </li>
            </ul>
        </div>
        <div class="col-md-6 no-padding box-body" style="min-height: 211px;">
            <h5 style="background: #f6f6f6;padding: 5px;margin:3px;"><b>Allergies(if any)</b></h5>
            <span style="padding-left:10px !important;">

                <table class='table' style='width: 100%;font-size:11px;border-collapse: collapse;font-weight:200;margin-left:5px;'>
                    @php
                        $allergies = $all_form_fields['AllergiesIFANYORTHOIP'];
                    @endphp

                        @for($i=0;$i<=5;$i++)
                            @if($allergies['rows_'.$i][0] !='')
                                <tr>
                                    <th style="text-align:left;">{{$allergies['rows_'.$i][0]}}</th>
                                </tr>
                            @endif
                        @endfor

                </table>
            </span>
        </div>
    </div>
    <div class="col-md-12 no-padding">
        <h5 style="background: #f6f6f6;padding: 5px;"><b>Admission Orders And Patient Care Plan</b></h5>
        <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
            @php
                $admission_orders_and_care_plan = $all_form_fields['ADMISSIONORDERSANDPATIENTCAREPLAN'];
            @endphp
            <tr>
                <td  style="padding-left:5px;text-align:left;width:35%;background: #f6f6f6;color:#898787;">Isolation if any</td>
                <td  style="padding-left:5px;text-align:left;">{{$admission_orders_and_care_plan['rows_0'][1]}}</td>
            </tr>
                <td  style="padding-left:5px;text-align:left;background: #f6f6f6;color:#898787;">Vital signs(Q1h,Q2h,Q3h,Q4h,Q5h)</td>
                <td style="padding-left:5px;text-align:left;">{{$admission_orders_and_care_plan['rows_1'][1]}}</td>
            </tr>
                <td style="padding-left:5px;text-align:left;background: #f6f6f6;color:#898787;">Diet(Regular/Specific)</td>
                <td style="padding-left:5px;text-align:left;">{{$admission_orders_and_care_plan['rows_2'][1]}}</td>
            </tr>
                <td style="padding-left:5px;text-align:left;background: #f6f6f6;color:#898787;">Activity(Bed rest/As tolerated)</td>
                <td style="padding-left:5px;text-align:left;">{{$admission_orders_and_care_plan['rows_3'][1]}}</td>
            </tr>
            </tr>
                <td style="padding-left:5px;text-align:left;width:50px%;background: #f6f6f6;color:#898787;">Plan</td>
                <td style="padding-left:5px;text-align:left;">{{$admission_orders_and_care_plan['rows_4'][1]}}</td>
            </tr>
        </table>
    </div>
    <div class="col-md-12 no-padding" style="padding-left:3px;">
        <span style="color:#898787;">Expected Outcome Briefed To :</span>
        @php
            $expected_outcome_briefed_to = @$all_form_fields['EXPECTEDOUTCOMEBRIEFEDTO']??0;
        @endphp
            @if($expected_outcome_briefed_to == 1)
                Patient
            @else
                Bystander
            @endif
        <br>
        <br>
        <span style="color:#898787;">Expected Cost Briefed To :</span>
            @if($expected_outcome_briefed_to == 1)
                Patient
            @else
                Bystander
            @endif
    </div>
    <div class="col-md-12 no-padding" style="min-height:100px;">
        <h5 style="background: #f6f6f6;padding: 5px;"><b>Cross Consultation</b></h5>
        <span style="padding-left:10px !important;">{{@$all_form_fields['CrossConsultation']??''}}</span>
    </div>
    <div class="col-md-12 no-padding" style="min-height:100px;">
        <h5 style="background: #f6f6f6;padding: 5px;"><b>Advice On Discharge</b></h5>
        <span style="padding-left:10px !important;">{{@$all_form_fields['ADVICEONDISCHARGE']??''}}</span>
    </div>
    <div class="col-md-12 no-padding" style="min-height:100px;">
        <h5 style="background: #f6f6f6;padding: 5px;"><b>Post Operative Complaint</b></h5>
        <span style="padding-left:10px !important;">{{@$all_form_fields['POSTOPERATIVECOMPLAINT']??''}}</span>
    </div>
    <div class="col-md-12 no-padding">
        <h5 style="background: #f6f6f6;padding: 5px;"><b>Follow-up 1<sup>st</sup> Visit</b></h5>
        <span style="padding-left:10px !important;">{{@date('M-d-Y',strtotime($all_form_fields['FOLLOWUP1STVISITDATE']))??''}}</span>
    </div>
    <div style="break-after: page"></div>
    <div class="col-md-12 no-padding" style="min-height:100px;">
        <h5 style="background: #f6f6f6;padding: 5px;"><b>Complaints</b></h5>
        <span style="padding-left:10px !important;">{{@$all_form_fields['COMPLAINTS']??''}}</span>
    </div>
    <div class="col-md-12 no-padding">
        <h5 style="background: #f6f6f6;padding: 5px;"><b>Further Follow-up</b></h5>
        <span style="padding-left:10px !important;">{{@$all_form_fields['FURTHERFOLLOWUP']??''}}</span>
    </div>

    <div class="col-md-12 no-padding">
        <br><br>
        <br><br>
        <div class="col-md-8"></div>
        <div class="col-md-4" style="text-align: center;color:#898787;float:right;">
        <span style="padding-left:10px !important;float:right;">
            <b>{{ $doctor_name }}</b><br>
            <b>{{ $speciality }}</b><br>
            <b>{{@date('M-d-Y',strtotime($all_form_fields['CONSULTATIONTIME']))??''}}</b><br>
        </span>
        </div>
    </div>
</div>


