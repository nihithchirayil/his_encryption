<!-- <div class="theadscroll pos_stat_hover always-visible" style="position: relative; height: 110px;"> -->
<div class="" style="position: relative; min-height: 220px; max-height:250px;">
    <table class="table no-margin table_sm no-border">
        <thead>
            <tr class="" style="background: aliceblue; color: #000000; ">
                <th style="width: 17%;">Date</th>
                <th style="width: 20%;">Form Name</th>
                <th style="width: 15%;">Doctor</th>
                <th style="width: 15%;">Prepared By</th>
                <th style="width: 15%;">Updated By</th>
                <th style="width: 10%;"></th>
                <th style="width: 3%;">&nbsp;</th>
                <th style="width: 3%;">&nbsp;</th>
                <th style="width: 3%;">&nbsp;</th>
                <th style="width: 3%;">&nbsp;</th>
                <th style="width: 3%;">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($form_data_his))
              @for ($i = 0; $i < sizeof($form_data_his); $i++)
                  @php
                     $id = $form_data_his[$i]->id;
                     $form_name = $form_data_his[$i]->name;
                     $form_id = $form_data_his[$i]->form_id;
                     $doctor_name = $form_data_his[$i]->doctor_name;
                     $visit_status = $form_data_his[$i]->visit_status;
                     $doctor_name_ = (strlen($form_data_his[$i]->doctor_name) > 18) ? substr($form_data_his[$i]->doctor_name,0,18).'..' : $form_data_his[$i]->doctor_name;
                     $created = (!empty($form_data_his[$i]->created_at)) ? ExtensionsValley\Emr\CommonController::getDateFormat($form_data_his[$i]->created_at,'M-d-Y h:i A') : '';
                     $status = ($form_data_his[$i]->status == 1) ? "" : "Draft";
                     $time_diff = $form_data_his[$i]->time_diff;
                     $my_note = $form_data_his[$i]->my_note;
                     $group_id = \DB::table('user_group')->where('user_id', '=', \Auth::user()->id)->pluck('group_id');
                     $group_name = \DB::table('groups')->wherein('id', $group_id)->pluck('name')->toArray();
                     $nurse_admin = 0;
                     $nurse = 0;
                     if (in_array("Nursing_Admin", $group_name)) {
                      $nurse_admin = 1;
                     }
                     if (in_array("NURSE", $group_name)) {
                      $nurse = 1;
                     }
                     
                     $created_by = $form_data_his[$i]->created_by;

                  @endphp
                  <tr>
                      <td title="{{$created}}">{{$created}}</td>
                      <td title="{{$form_name}}">
                        <a onmouseover="loadFormViewPopover('{{$id}}','{{$form_id}}');" style="cursor:pointer;" class="pop-history-btn">
                            {{$form_name}}
                        </a>
                      </td>
                      <td title="{{$doctor_name}}">{{$doctor_name_}}</td>
                      <td title="{{$doctor_name}}">{{$form_data_his[$i]->created_user}}</td>
                      <td title="{{$doctor_name}}">{{$form_data_his[$i]->updated_user}}</td>
                      <td>
                        <span class="badge badge_purple_bg">{{$visit_status}}</span>
                        @if(!empty($status))
                            <span class="badge bg-orange">{{$status}}</span>
                        @endif
                      </td>
                      <td>
                        <button class="btn btn-block light_purple_bg" title="View" onclick="loadFormView('{{$id}}','{{$form_id}}')"><i class="fa fa-eye"></i> View</button>
                      </td>
                      <td>
                        <button id="printFormViewbtn{{$id}}" onclick="printFormView('{{$id}}','{{$form_id}}')" class="btn btn-block light_purple_bg" title="Print"><i id="printFormViewspin{{$id}}" class="fa fa-print"></i> Print</button>
                      </td>
                      <td>
                        <button onclick="favoriteForm('{{$id}}','{{$form_id}}')" class="btn btn-block light_purple_bg" title="Bookmark"><i class="fa fa-star"></i> Bookmark </button>
                      </td>
                      <td>
                        @if(\Auth::user()->id == $created_by)
                            <button class="btn btn-block light_purple_bg" title="Copy" onclick="assessmentCopyMode('{{$form_id}}','{{$id}}','copy')"><i class="fa fa-clone"></i> Copy</button>
                        @else
                            <button disabled="disabled" class="btn btn-block light_purple_bg" title="Copy" onclick="assessmentCopyMode('{{$form_id}}','{{$id}}','copy')"><i class="fa fa-clone"></i> Copy</button>
                        @endif
                      </td>
                      <td>
                      @if($nurse == 1)
                      {{-- ($time_diff < \WebConf::getConfig('nursing_notes_edit_max_time') && $my_note == 1) --}}
                        @if($my_note == 1 || $nurse_admin == 1)
                          <button class="btn btn-block light_purple_bg" title="Edit" onclick="assessmentEditModeNew('{{$form_id}}','{{$id}}','edit',{{ $time_diff }})"><i class="fa fa-edit"></i> Edit</button>
                        @endif
                      @else
                        @if(\Auth::user()->id == $created_by)
                          <button class="btn btn-block light_purple_bg" title="Edit" onclick="assessmentEditModeNew('{{$form_id}}','{{$id}}','edit',{{ $time_diff }})"><i class="fa fa-edit"></i> Edit</button>
                        {{-- @else
                        <button class="btn btn-block light_purple_bg" disabled="disabled" title="Edit" onclick="assessmentEditMode('{{$form_id}}','{{$id}}','edit')"><i class="fa fa-edit"></i> Edit</button> --}}
                        @endif

                      @endif
                      </td>
                      <td>
                      @if($nurse == 1)
                        @if(($time_diff < \WebConf::getConfig('nursing_notes_edit_max_time') && $my_note == 1 ) || $nurse_admin == 1)
                          <button class="btn btn-block light_purple_bg" title="Delete" onclick="assessmentDeleteMode(this,'{{$id}}')"><i class="fa fa-trash"></i> Delete</button>
                        @endif
                      @else
                        @if(\Auth::user()->id == $created_by)
                          <button class="btn btn-block light_purple_bg" title="Delete" onclick="assessmentDeleteMode(this,'{{$id}}')"><i class="fa fa-trash"></i> Delete</button>
                        @else
                          <button class="btn btn-block light_purple_bg" disabled="disabled" title="Delete" onclick="assessmentDeleteMode(this,'{{$id}}')"><i class="fa fa-trash"></i> Delete</button>
                        @endif
                      @endif
                      </td>
                  </tr>
              @endfor
            @else
            <td colspan="6">No Data Found..!</td>
            @endif
        </tbody>
    </table>
</div>

@if(!empty($form_data_his))

<div id="pagination">{{{ $form_data_his->links() }}}</div>
@endif


