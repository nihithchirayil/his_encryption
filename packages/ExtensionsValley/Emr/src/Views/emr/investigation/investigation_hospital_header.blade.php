@if(!empty($hospitalHeader))
{!!$hospitalHeader!!}
<br>
<hr>
<br>
@endif
 <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;" width="100%" border="1" cellspacing="0" cellpadding="0">


    <tr>
        <td><strong>Patient Name</strong></td>
        <td align="center">:</td>
        <td>{{$patient[0]->patient_name}}</td>
        <td align="center">:</td>
        <td><strong>Date</strong></td>
        <td align="center">:</td>
        <td>
            {{$date}}
        </td>
    </tr>
    <tr>
        <td><strong>{{\WebConf::getConfig('op_no_label')}}</strong></td>
        <td align="center">:</td>
        <td>{{$patient[0]->uhid}}</td>
        <td align="center">:</td>
        <td><strong>Doctor</strong></td>
        <td align="center">:</td>
        <td>{{$doctor['doctor_name']}}</td>
    </tr>
    <tr>
        <td><strong>Gender/Age</strong></td>
        <td align="center">:</td>
        <td>{{$patient[0]->gender}} / {{$patient[0]->age}}</td>
        <td align="center">:</td>
        <td><strong>Department</strong></td>
        <td align="center">:</td>
        <td>{{$doctor['doctor_speciality']}}</td>
    </tr>
    <tr style="display: none;">
        <td colspan="4">&nbsp;</td>
        <td><strong>Token</strong></td>
        <td align="center">:</td>
        <td>token_no</td>
    </tr>
  </table>
