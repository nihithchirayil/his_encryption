
<div class="col-md-12" style="margin-top:10px">
    <h5 style="font-size: 15px;
    font-weight: 600;">Treatment Plan</h5>
</div>
     <div id="table_container_div" class="col-md-12">
         <table id="resultDataTable" border="1" width="100%;" class="table table-striped table-bordered table-hover table-condensed table_sm theadfix_wrapper" style="border-collapse: collapse;">
     @if(sizeof($res)>0)
     <thead>
         <tr style="background-color:darkcyan;color:white;">
             {{-- <th onclick="sortTable('result_data_table',0,2);" style="padding: 2px;width: 120px;">Date</th> --}}
             <th width="12%">Date</th>
             <th width="15%">Test</th>
             <th width="17%">Description</th>
             <th width="8%">Value </th>
             <th width="8%">Normal Range</th>
             <th width="10%">Sub Test</th>
             <th width="8%">Result Status</th>
             <th width="10%">Finalized By</th>
             <th width="12%">Approx. Result Time</th>
         </tr>
     </thead>


     <tbody>
         <?php
             $min = "";
             $max = "";
             $direction = '';
             $style = '';
             $created_at = '';
             $service_desc = '';
             $remarks = '';
             $lab_result_status = '';
             $finalized_by = '';
             $service_description = '';
             $service_created_at = '';
         ?>
     @foreach ($res as $result)
         <?php


             if (!empty($result->min_value) && is_numeric($result->actual_result) && $result->max_value != "-") {

                 if (!empty($result->min_value)) {
                     $min = $result->min_value;
                 }

                 if (!empty($result->max_value)) {
                     $max = $result->max_value;
                 }

                 if($max < $result->actual_result && !empty($result->actual_result)){
                     $style = "style=color:red;";
                     $direction = '&nbsp;<i class="fa fa-arrow-up"></i>';
                 }
                 if($min > $result->actual_result && !empty($result->actual_result)){
                     $style = "style=color:#FF7701;";
                     $direction = '&nbsp;<i class="fa fa-arrow-down"></i>';
                 }
             }

             $pdf_download_prefix = env('LAB_REPORT_FILE_PREFIX_PUBLIC','');

             if($result->report_type == 'T' && $result->pdf_format){
                 $html_content = "<a target='_blank' href='".$host_name.$pdf_download_prefix.$result->pdf_format."' class='btn bg-primary' style='color:white !important;'><i class='fa fa-list'></i></a>";
             } else if($result->report_type == 'T' && !$result->pdf_format){
                 $html_content = "<button class='btn bg-primary btn_lab' type='button' onclick='getLabResultRtf(".$result->id.")'><i class='fa fa-list'></i></button>";
             }else if($result->report_type == 'N' || $result->report_type == 'F'){
                 $html_content = "<div class='text-left '  $style >$result->actual_result $direction</div>";
             }else{
                 $html_content = "";
             }

             $normal_range = "";
             if(!empty($min)){
                 $normal_range = $min.' - '.$max;
             }
             $result_finalized = (!empty($result->result_finalized)) ? $result->result_finalized : 0;

             if($result->service_desc != $service_description){
                 $service_desc = '<span onclick="filterLabTest(\''.base64_encode($result->service_desc).'\');">'.$result->service_desc.'</span>';
             }else{
                 $service_desc = '';
             }

             if($result->result_created_at != $service_created_at){
                 $created_at='<span>'.date('M-d-Y h:i a',strtotime($result->result_created_at)).'</span>';
             }else{
                 $created_at= '';
             }

             $service_created_at = $result->result_created_at;
             $service_description = $result->service_desc;

         ?>
         <tr @if($result->pdf_format!='') class="pdf_row" @endif >
             <td style="text-align:left;">{!!$created_at!!}</td>
             <td style="text-align:left;">{!!$service_desc!!}</td>
             <td style="text-align:left;">{!!$result->detail_description!!}</td>
             <td style="text-align:left;">{!!$html_content!!}</td>
             <td style="text-align:left;">{!!$normal_range!!}</td>
             <td style="text-align:left;">{!!$result->sub_test_name!!}</td>
             {{-- <td>
                     @php $remarks = !empty($result->remarks)  ? $result->remarks : '-'; @endphp
                     {!!$remarks!!}
                 </td>
              --}}
             <td style="text-align:left;">
                 @php $lab_result_status = !empty($result->lab_result_status)  ? $result->lab_result_status : '-';@endphp
                 {!!$lab_result_status!!}
             </td>
             <td style="text-align:left;">@php $finalized_by = !empty($result->finalized_by)  ? $result->finalized_by : '-';@endphp
                 {!!$finalized_by!!}
             </td>
             <td style="text-align:left;">
                 @php
                     $seconds = (int)$result->expected_result_time * 60;
                     $expected_result_time = strtotime($result->sample_collection_date1) + $seconds;
                     $expected_result_time = date('d-M-Y h:i A', $expected_result_time);
                 @endphp
                 {{$expected_result_time}}
             </td>
         </tr>

     @endforeach

     </tbody>
     @else
     <tr>
         <td>No Results Found!</td>
     </tr>
     @endif

 </table>
 </div>
