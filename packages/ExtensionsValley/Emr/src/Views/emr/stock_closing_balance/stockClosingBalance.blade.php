@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<style>

    .search_header{
        background: #36A693 !important;
        color: #FFF !important;
    }

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">

<div class="right_col">
    <div class="col-md-12" style="margin:0px">
        <div class="container-fluid" style="padding: 15px;">
            <div class="row codfox_container">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-12 no-padding">
                        <div class="box no-border" >
                            <div class="box-header search_header">

                            </div>
                            <div class="box-body clearfix">

                                <div class="col-md-4 padding_sm">

                                </div>

                                <div class="col-md-1 col-md-offset-6 padding_sm saveConfigurationDiv" >
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary" onclick="SaveStockDetails()"><i class="fa fa-save"></i> Save</button>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ht10"></div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12  no-border no-margin" >
                        <form method="POST" id="stock_data" name="stock_data" >       
                            <table id ="stock_details" class="table table-bordered table_sm no-margin no-border theadfix_wrapper">
                                <thead>
                                    <tr class="table_header_bg">
                                        <th class="text-center" width="20%">Closing Month</th>
                                        <th class="text-center" width="20%">Stock Of Medicine</th>
                                        <th class="text-center" width="20%">Stock Of X-Ray</th>
                                        <th class="text-center" width="20%">Stock Value</th>

                                    </tr>
                                </thead>
                                <tbody class="bill_configuration_body" style="min-height:450px;">


                                    <?php
                                    $start = strtotime('first day of last month');
                                    $end = strtotime('2021-01-01');
                                    $start_date = date('M-Y', $start);

                                    if (isset($stock_list) && count($stock_list) > 0) {
                                        if(date('m', $start)-count($stock_list) !=0){
                                            $start11 = strtotime('first day of last month');
                                            $first_date = date('M-Y', $start);
                                            for($j=0;$j<(date('m', $start)-count($stock_list));$j++){ 
                                                $first_date = date('M-Y', $start11);?>
                                                <tr>
                                                <td>
                                                    <input type="text" value="{{$first_date}}" name="stock_date[]" class="form-control">
                                                </td>
                                                <td>
                                                    <input type="text"   value="0" name="stock_medicine[]"  class="form-control" onkeyup="calc_total(this)">
                                                </td>
                                                <td>
                                                    <input type="text"   value="0" name="stock_xray[]"  class="form-control" onkeyup="calc_total(this)">
                                                </td>
                                                <td>
                                                    <input type="text"  value="0" name="stock_value[]"  class="form-control">
                                                </td>
                                            </tr>
                                                
                                            <?php $start11 = strtotime("-1 month", $start11);}
                                           }
                                        foreach ($stock_list as $stock => $data) {
                                            $stock_date = date('M-Y', strtotime($data->closing_month));
                                            ?>
                                            <tr>
                                                <td>
                                                    <input type="text" value="{{$stock_date}}" name="stock_date[]" class="form-control">
                                                </td>
                                                <td>
                                                    <input type="text"   value="{{$data->stock_of_medicine}}" name="stock_medicine[]"  class="form-control" onkeyup="calc_total(this)">
                                                </td>
                                                <td>
                                                    <input type="text"   value="{{$data->stock_of_xray}}" name="stock_xray[]"  class="form-control" onkeyup="calc_total(this)">
                                                </td>
                                                <td>
                                                    <input type="text"  value="{{$data->stock_value}}" name="stock_value[]"  class="form-control">
                                                </td>
                                            </tr>
                                            <?php
                                            $start = strtotime("-1 month", $start);
                                        }
                                    } else {
                                        while ($start >= $end) {
                                            $start_date = date('M-Y', $start);
                                            ?>
                                            <tr>
                                                <td>
                                                    <input type="text" value="{{$start_date}}" name="stock_date[]" class="form-control">
                                                </td>
                                                <td>
                                                    <input type="text"   value="0" name="stock_medicine[]"  class="form-control" onkeyup="calc_total(this)">
                                                </td>
                                                <td>
                                                    <input type="text"   value="0" name="stock_xray[]"  class="form-control" onkeyup="calc_total(this)">
                                                </td>
                                                <td>
                                                    <input type="text"   value="0" name="stock_value[]"  class="form-control">
                                                </td>
                                            </tr>
                                            <?php
                                            $start = strtotime("-1 month", $start);
                                        }
                                    }
                                    ?>


                                </tbody>
                            </table>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>


@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/stock_close_balance.js")}}"></script>


<script type="text/javascript">
                                                function calc_total(e) { 
                                                    var vals = $(e).val();
                                                    if(isNaN(vals) || vals ==''){
                                                      var vals =0;
                                                    }
                                                    var current_stck = $(e).closest("tr").find("input[name='stock_value[]']").val();
                                                    if(isNaN(current_stck)){
                                                       var current_stck =0;
                                                    }
                                                    var ttl = parseFloat(current_stck) + parseFloat(vals);
                                                    $(e).closest("tr").find("input[name='stock_value[]']").val(ttl);
                                                }
</script>
@endsection
