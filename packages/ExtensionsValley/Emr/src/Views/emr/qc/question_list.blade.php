@if (!empty($question_list))
    <?php
    $question_subtitle = \DB::table('quality_question_subtitle')
        ->where('status', 1)
        ->orderBy('name', 'ASC')
        ->pluck('name', 'id');
    ?>
    <div class="theadscroll" style="position: relative; height: 435px;">
        <table class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
            style="border: 1px solid #CCC;">
            <thead>
                <tr class="table_header_bg" style="cursor: pointer;">
                    <td width="25%">Subtitle</td>
                    <td width="60%">Question</td>
                    <td width="15%">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($question_list as $data)
                    <tr id="question_list_row_{{ $data->question_id }}">
                        <td class="common_td_rules">
                            <span id="question_subtitle_label_{{ $data->question_id }}"
                                class="question_subtitle_label">{{ $data->subtitle }}</span>

                            {!! Form::select('question_subtitle_select_' . $data->question_id, $question_subtitle, $data->subtitle_id, [
    'class' => 'form-control',
    'placeholder' => 'Select Subtitle',
    'id' => 'question_subtitle_select_' . $data->question_id,
    'style' => 'display: none;',
    'onchange' => 'updateQuestion(' . $data->question_id . ')',
]) !!}


                        </td>
                        <td class="common_td_rules"><span id="question_label_{{ $data->question_id }}"
                                class="question_label">{{ $data->question }}</span>
                            <input type="text" class="form-control" name="question_text_{{ $data->question_id }}"
                                id="question_text_{{ $data->question_id }}" value="{{ $data->question }}"
                                style="display: none;" onchange="updateQuestion('{{ $data->question_id }}')">
                        </td>
                        <td>
                            <button type="button" onclick="deleteQd({{ $data->question_id }}');"
                                class='btn btn-sm btn-default delete-qc'><i class="fa fa-trash"></i>
                            </button>
                            <button type="button" class='btn btn-sm btn-default'
                                onclick="edit_question('{{ $data->question_id }}')"><i class="fa fa-edit"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@else
    <span>No Questions found </span>
@endif
