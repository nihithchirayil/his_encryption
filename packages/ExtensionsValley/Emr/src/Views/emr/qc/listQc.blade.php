@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <style>
    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="row" style="text-align: right; font-size: 12px;font-weight: bold;"> {{ $title }}
            <div class="clearfix"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">



                            <div class="col-md-1 padding_sm" style="margin-top: 25px;float:right;margin-right:0px;">
                                <button data-toggle="modal" data-target="#qc_modal" class="btn bg-primary"
                                    style="float:right;"><i class="fa fa-plus"></i> Add</button>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="theadscroll" style="position: relative; height: 350px;">
                            <table
                                class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg" style="cursor: pointer;">
                                        <th>Name</th>
                                        <th>Department</th>
                                        <th>Created By</th>
                                        <th>Updated BY</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th>Delete/Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($item) > 0)
                                        @foreach ($item as $item)

                                            <tr style="cursor: pointer;">

                                                <td class="common_td_rules">{{ $item->name }}<input type="hidden"
                                                        class="fav_check" name="qc_name[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="qc_name[]" value="{{ $item->name }}">
                                                </td>
                                                <td class="common_td_rules">{{ $item->name }}<input type="hidden"
                                                        class="form-control" name="department[]"
                                                        value="{{ $item->department }}"><input type="hidden"
                                                        class="form-control" name="department[]"
                                                        value="{{ $item->department }}">
                                                </td>
                                                <td class="common_td_rules">{{ $item->created_by }}<input type="hidden"
                                                        class="form-control" name="created_by[]"
                                                        value="{{ $item->created_by }}"><input type="hidden"
                                                        class="form-control" name="created_by[]"
                                                        value="{{ $item->created_by }}"></td>
                                                <td class="common_td_rules">{{ $item->updated_by }}<input type="hidden"
                                                        class="form-control" name="updated_by[]"
                                                        value="{{ $item->updated_by }}"><input type="hidden"
                                                        class="form-control" name="updated_by[]"
                                                        value="{{ $item->updated_by }}"></td>
                                                <td class="common_td_rules">
                                                    {{ date('M-d-Y h:i A', strtotime($item->created_at)) }}<input
                                                        type="hidden" class="form-control" name="created_at[]"
                                                        value="{{ date('M-d-Y h:i A', strtotime($item->created_at)) }}"><input
                                                        type="hidden" class="form-control" name="created_at[]"
                                                        value="{{ date('M-d-Y h:i A', strtotime($item->created_at)) }}">
                                                </td>
                                                <td class="common_td_rules">
                                                    {{ date('M-d-Y h:i A', strtotime($item->updated_at)) }}<input
                                                        type="hidden" class="form-control" name="updated_at[]"
                                                        value="{{ date('M-d-Y h:i A', strtotime($item->updated_at)) }}"><input
                                                        type="hidden" class="form-control" name="updated_at[]"
                                                        value="{{ date('M-d-Y h:i A', strtotime($item->updated_at)) }}">
                                                </td>
                                                <td class="">
                                                    <button class='btn btn-sm btn-default delete-qc'><i
                                                            class="fa fa-trash"></i>
                                                    </button>
                                                    <button class='btn btn-sm btn-default edit-qc'
                                                        onclick="edit_qc('{{ $item->id }}','{{ $item->name }}')"><i
                                                            class="fa fa-edit"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="12" class="location_code">No Records found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 text-center">
                            <ul class="pagination purple_pagination pull-right">
                                {!! $page_links !!}
                            </ul>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
            </div>
        </div>
    </div>
    <!-- doctor service division modal start -->





    <div class="modal fade" id="qc_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content" style="min-height:600px;">
                <div class="modal-header" style="background: #1ABB9C; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title">Quality Control Questions</h4>
                </div>
                {!! Form::open(['qc_form', 'id' => 'qc_form']) !!}
                <input type="hidden" class="form-control" name="edit_qc" id="edit_qc">
                <div class="modal-body col-md-12" id="qc_details">
                    <div class="col-md-12">
                        <div class="col-md-6 no-padding">
                            <div class="box-body" style="padding:10px !important;">
                                <h5>Question Title</h5>
                                <input type="text" class="form-control" id="question_name" name="question_name" />
                                <br>
                                {!! Form::label('department_list', 'Department List') !!}
                                {!! Form::select('department_list', $department_list, null, [
    'class' => 'form-control',
    'placeholder' => 'Select Department',
    'id' => 'department_list',
]) !!}<br>
                            </div>
                            <br>


                            <div class="box-body" style="padding: 10px !important; margin-top: 20px;">
                                <h5>Add New Question</h5><br>

                                {!! Form::label('question_subtitle', 'Subtitle') !!}
                                {!! Form::select('question_subtitle', $question_subtitle, null, [
    'class' => 'form-control',
    'placeholder' => 'Select Subtitle',
    'id' => 'question_subtitle',
]) !!}
                                <br>
                                {!! Form::label('question', 'Question') !!}
                                <textarea
                                    style="overflow-y:scroll; resize:vertical; border:1px solid #dddddd; width:100%; height: 100px"
                                    cols="50" id="question" name="question">
                                                                                                                                                                                                                                                                                                                                                                                                </textarea>

                                <div class="col-md-12" style="padding-right:0px ​!important">
                                    <br>
                                    <button style="width: 100px;" type="button" class="btn bg-green pull-right"
                                        onclick="addqc()">
                                        <i class="fa fa-check"></i> Save</button>
                                    <button style="width: 100px;" type="button" class="btn btn-info pull-right"
                                        data-dismiss="modal">Clear</button>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="col-md-12">
                                <h5>Questions List</h5><br>
                            </div>
                            <div style="margin-top:-20px;" class="col-md-12" id="question_list_content"></div>
                        </div>
                    </div>
                    <br>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}

    {!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}

    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            setTimeout(function() {
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');

            }, 300);



            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            //  $('.date_time_picker').datetimepicker();
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });



        });
    </script>

@stop

@section('javascript_extra')
    <script type="text/javascript">
        function addqc() {
            let edit_qc = $("#edit_qc").val();
            let question_name = $("#question_name").val();
            let department_list = $("#department_list").val();
            let question_subtitle = $("#question_subtitle").val();
            let question = $("#question").val();


            if (question_name == '') {
                toastr.warning("Question Title Required");
            } else if (department_list == '') {
                toastr.warning("Department is Required");
            } else if (question_subtitle == '') {
                toastr.warning("Subtitle is Required");
            } else if (question == '') {
                toastr.warning("Question Required");
            } else {

                if (edit_qc != '' && edit_qc != 0) {
                    deleteQc(edit_qc);
                }

                var url = $('#domain_url').val() + "/quality_control/save-qc";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'question_name=' + question_name + '&department_list=' + department_list +
                        '&question_subtitle=' + question_subtitle +
                        '&question=' + question,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(response) {
                        if (response.status == 1) {

                            Command: toastr["success"]("Saved.");
                            window.location.href = $('#domain_url').val() + "/quality_control/listQcQuestions/";

                        }
                        else {

                            Command: toastr["error"]("Error.");
                        }
                    },
                    complete: function() {
                        $("body").LoadingOverlay("hide");
                    }
                });
            }
        }

        $(document).on('click', '.delete-sentinal-event', function() {
            if (confirm("Are you sure you want to delete.!")) {
                let tr = $(this).closest('tr');
                let edit_id = $(tr).find('input[name="sent_evnt_id[]"]').val();

                if (edit_id != '' && edit_id != 0) {
                    deleteSentinalRowFromDb(edit_id);
                    $(tr).remove();
                } else {
                    $(tr).remove();
                }
            }
        });

        //delete single row from db
        function deleteSentinalRowFromDb(id) {
            var url = $('#domain_url').val() + "/sentinalevent/delete-sentinal-event";
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id,
                    _token: _token,
                },
                beforeSend: function() {
                    $("body").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(data) {
                    if (data != '' && data != undefined && data != 0) {
                        Command: toastr["success"]("Deleted.");
                    }
                },
                complete: function() {
                    $("body").LoadingOverlay("hide");
                }
            });
        }

        function deleteSentinalRow(id) {
            var url = $('#domain_url').val() + "/sentinalevent/delete-sentinal-event";
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id,
                    _token: _token,
                },
                beforeSend: function() {

                },
                success: function(data) {

                },
                complete: function() {

                }
            });
        }

        function edit_qc(id, name) {
            var url = $('#domain_url').val() + "/quality_control/edit-qc";
            let _token = $('#c_token').val();
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    id: id
                },
                beforeSend: function() {
                    $('#qc_modal').modal('show');
                    $("#question_list_content").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(data) {
                    var obj = JSON.parse(data);
                    $("#edit_qc").val(id);
                    $("#question_name").val(obj.head_title);
                    $("#department_list").val(obj.department_id);
                    $('#question_list_content').html(obj.html);
                },
                complete: function() {
                    $("#question_list_content").LoadingOverlay("hide");
                    $('.theadscroll').perfectScrollbar({
                        wheelPropagation: true,
                        minScrollbarLength: 30
                    });
                    $(".theadfix_wrapper").floatThead('reflow');
                }
            });
        }

        function edit_question(id) {
            // alert(id);
            $('#question_subtitle_label_' + id).hide();
            $('#question_subtitle_select_' + id).show();
            $('#question_subtitle_select_' + id).focus();

            $('#question_label_' + id).hide();
            $('#question_text_' + id).show();
            $('#question_text_' + id).focus();
        }

        function updateQuestion(id) {
            let question_subtitle = $("#question_subtitle_select_" + id).val();
            let question = $("#question_text_" + id).val();

            if (question_subtitle == '') {
                toastr.warning("Subtitle is Required");
            } else if (question == '') {
                toastr.warning("Question Required");
            } else {
                var url = $('#domain_url').val() + "/quality_control/update-question";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'id=' + id + '&question_subtitle=' + question_subtitle +
                        '&question=' + question,
                    beforeSend: function() {
                        $('#question_list_row_' + id).LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });

                    },
                    success: function(response) {
                        if (response.status == 1) {

                            Command: toastr["success"]("Saved.");
                            $('#question_list_row_' + id).LoadingOverlay("hide");

                            $('#question_subtitle_label_' + id).show();
                            $('#question_subtitle_select_' + id).hide();

                            $('#question_label_' + id).show();
                            $('#question_subtitle_select_' + id).html(question);
                            $('#question_text_' + id).hide();

                        }
                        else {

                            Command: toastr["error"]("Error.");
                        }
                    },
                    complete: function() {}
                });
            }
        }
    </script>

@endsection
