@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-slide.css') }}" rel="stylesheet">


<style>
    .headerbtn {
        border: 1px solid green;
        min-width: 90px;
    }

    .headerbtn:hover {
        background-color: green;
        color: white;
    }

    .blink-text {
        color: #000;
        font-size: 2rem;
        animation: blinkingText 1s infinite;
    }

    .blink-btn {
        animation: blinkingButton 1s infinite;
    }

    @keyframes blinkingText {
        0% {
            color: #000000;
        }

        25% {
            color: #1056c0;
        }

        50% {
            color: #ef0a1a;
        }

        75% {
            color: #254878;
        }

        100% {
            color: #04a1d5;
        }
    }

    @keyframes blinkingButton {
        0% {
            color: #10c018;
        }

        25% {
            color: #399f62;
        }

        50% {
            color: #399f62;
        }

        75% {
            color: #399f62;
        }

        100% {
            color: #000000;
        }
    }

    .no-stock {
        color: #ef0a1a;
    }

    .slow-spin {
        -webkit-animation: fa-spin 4s infinite linear;
        animation: fa-spin 4s infinite linear;
    }

    .loader {
        width: 10em;
        height: 10em;
        font-size: 35px;
        position: relative;
        display: flex;
        align-items: center;
        justify-content: center;
        margin: auto;
    }

    .loader .face {
        position: absolute;
        border-radius: 50%;
        border-style: solid;
        animation: animate 3s linear infinite;
    }

    .loader .face:nth-child(1) {
        width: 100%;
        height: 100%;
        color: lime;
        border-color: currentColor transparent transparent currentColor;
        border-width: 0.2em 0.2em 0em 0em;
        --deg: -45deg;
        animation-direction: normal;
    }

    .loader .face:nth-child(2) {
        width: 70%;
        height: 70%;
        color: lime;
        border-color: currentColor currentColor transparent transparent;
        border-width: 0.2em 0em 0em 0.2em;
        --deg: -135deg;
        animation-direction: reverse;
    }

    .loader .face .circle {
        position: absolute;
        width: 50%;
        height: 0.1em;
        top: 50%;
        left: 50%;
        background-color: transparent;
        transform: rotate(var(--deg));
        transform-origin: left;
    }

    .loader .face .circle::before {
        position: absolute;
        top: -0.5em;
        right: -0.5em;
        content: '';
        width: 1em;
        height: 1em;
        background-color: currentColor;
        border-radius: 50%;
        box-shadow: 0 0 2em,
            0 0 4em,
            0 0 6em,
            0 0 8em,
            0 0 10em,
            0 0 0 0.5em rgba(255, 255, 0, 0.1);
    }

    @keyframes animate {
        to {
            transform: rotate(1turn);
        }
    }

    .pop-history-btn:hover .pos_stat_hover {
        position: static;
    }

    .ht25 {
        height: 25px;
    }


    #container {
        max-width: 1024px;
        margin: auto;
    }

    #monitor {
        background: #000;
        position: relative;
        border-top: 3px solid #888;
        margin: 5%;
        padding: 2% 2% 4% 2%;
        border-radius: 10px;
        border-bottom-left-radius: 50% 2%;
        border-bottom-right-radius: 50% 2%;
        transition: margin-right 1s;
    }

    #monitor:after {
        content: '';
        display: block;
        position: absolute;
        bottom: 3%;
        left: 36%;
        height: .5%;
        width: 28%;
        background: #ddd;
        border-radius: 50%;
        box-shadow: 0 0 3px 0 white;
    }

    #monitorscreen {
        position: relative;
        background-color: white;
        background-size: cover;
        background-position: top center;
        height: 0;
        padding-bottom: 52%;
        position: relative;
        overflow: hidden;
        box-shadow: 0px 19px 15px #2a3f54;
    }

    .current_token_monitor {
        color: #399f62;
        font-size: 24px;
        text-align: center;
        border: 1px solid #ececec;
        box-shadow: 3 8 black;
        box-shadow: 5px 3px 5px #2a3f54;
        width: auto;
        margin: 5px;
        margin-top: 15px;
    }


    @media all and (min-width: 960px) {
        #monitor {
            -webkit-animation: tvflicker .2s infinite alternate;
            -moz-animation: tvflicker .5s infinite alternate;
            -o-animation: tvflicker .5s infinite alternate;
            animation: tvflicker .5s infinite alternate;
        }

        @-webkit-keyframes tvflicker {
            0% {
                box-shadow: 0 0 100px 0 rgba(200, 235, 255, 0.4);
            }

            100% {
                box-shadow: 0 0 95px 0 rgba(200, 230, 255, 0.45);
            }
        }

        @-moz-keyframes tvflicker {
            0% {
                box-shadow: 0 0 100px 0 rgba(225, 235, 255, 0.4);
            }

            100% {
                box-shadow: 0 0 60px 0 rgba(200, 220, 255, 0.6);
            }
        }

        @-o-keyframes tvflicker {
            0% {
                box-shadow: 0 0 100px 0 rgba(225, 235, 255, 0.4);
            }

            100% {
                box-shadow: 0 0 60px 0 rgba(200, 220, 255, 0.6);
            }
        }

        @keyframes tvflicker {
            0% {
                box-shadow: 0 0 100px 0 rgba(225, 235, 255, 0.4);
            }

            100% {
                box-shadow: 0 0 60px 0 rgba(200, 220, 255, 0.6);
            }
        }
    }

    .tox-collection__group {
        width: 360px !important;
    }

    .tox .tox-collection--grid .tox-collection__item {
        width: 360px !important;
    }

    .tox .tox-collection__item-checkmark,
    .tox .tox-collection__item-icon {
        display: inline !important;
        width: 360px !important;
        align-items: left !important;
    }
</style>
<link
    href="{{ asset('packages/extensionsvalley/master/default/css/new_emr.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
@endsection
@section('content-area')
<input type="hidden" id="app_url" value="{{ $app_url }}">
@php
$font_awsome_css_path = asset('packages/extensionsvalley/dashboard/css/font-awesome.min.css');
@endphp
<input type="hidden" id="font_awsome_css_path" value="{{ $font_awsome_css_path }}">

<!-- page content -->

<div class="modal fade" id="investigationDateTimePickerModel" tabindex="-1" role="dialog" style="z-index:99999; display:none;">
    <div class="modal-dialog" style="width: 40%">
        <div class="modal-content">
            <div class="modal-header modal_box">
                <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                <h4 class="modal-title" id="investigationDateTimePickerModelHeader"></h4>
            </div>
            <div class="modal-body" style="min-height: 75px;">
                <div class="col-md-12">
                    <div class="mate-input-box">
                        <label class="filter_label">Service Date</label>
                        <input type="hidden" value="" id="invest_cnt_hiddden">
                        <input type="text" autocomplete="off" value="" class="form-control"
                            id="investigationServiceDate">
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="margin-top: 10px;">
                <button style="padding: 3px 3px; margin-top: 5px;" type="button" data-dismiss="modal" aria-label="Close"
                    class="btn btn-danger">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="right_col" role="main">

    <div class="row codfox_container">

        <div class="position_fixed_header" @if (env('QUEUE_MANAGEMENT_ENABLED', '' )=='TRUE' &&
            ($show_queue_management=='1' )) style="z-index:1050;width:73%;margin-top:35px;" @else
            style="z-index:1050;width:93%;margin-top:35px;" @endif>

            <div class="col-md-8 padding_sm">
                <div class="col-md-7 padding_sm">
                    @include('Emr::emr.includes.patient_header')
                </div>

                <div class="col-md-5 padding_sm">
                    @include('Emr::emr.includes.patient_allergy')
                </div>
            </div>

            <div class="col-md-4 padding_sm">

                <div class="col-md-12" style="padding: 0 2px !important; ">
                    @include('Emr::emr.includes.patient_vitals')
                </div>

            </div>

            <div class="clearfix"></div>
            <div class="ht25"></div>
            <div class="col-md-12 padding_sm text-left">

                @php
                $current_visit = \DB::table('patient_master')
                ->where('id', $patient_id)
                ->value('current_visit_id');
                $visit_status = \DB::table('patient_master')
                ->where('id', $patient_id)
                ->value('current_visit_status');
                @endphp

                <button class="btn bg-default headerbtn " onclick="combinedView()"><i class="fa fa-history"
                        aria-hidden="true"></i> History</button>
                {{-- <button class="btn bg-default headerbtn " name='admission_request' id='admission_request'><i
                        class="fa fa-bed" aria-hidden="true"></i> Admit Patient</button> --}}
                <button @if (trim($visit_status)=='IP' ) disabled="disabled" @endif class="btn bg-default headerbtn "
                    name='admission_request' id='admission_request'><i class="fa fa-bed" aria-hidden="true"></i> Admit
                    Patient</button>
                <button class="btn bg-default headerbtn " onclick="specialNotes()" data-toggle="modal"
                    data-target="#speial_note_modal"> <i class="fa fa-book" aria-hidden="true"></i>Special
                    Notes</button>
                <button class="btn bg-default headerbtn " onclick="referDoctor();"> <i class="fa fa-user-plus"
                        aria-hidden="true"></i> Refer Patient</button>
                <button data-toggle="modal" onclick="LabResultsTrend();" data-target="#modal_lab_results"
                    class="btn bg-default headerbtn  btn-nurse-task labResultBtn"><i class="fa fa-flask"></i> Lab
                    Results</button>
                <button data-toggle="modal" onclick="PathologyResults();" data-target="#modal_pathology_results"
                    class="btn bg-default headerbtn  btn-nurse-task labResultBtn"><i class="fa fa-flask"></i> Pathology
                    Results</button>
                <button data-toggle="modal" class="btn bg-default headerbtn  btn-nurse-task radiologyResultsBtn"><i
                        class="fa fa-flask"></i> Radiology Results</button>
                <button class="btn bg-default headerbtn " id="btn_advnc_dtail" onclick="manageDocs();"><i
                        class="fa fa-archive" aria-hidden="true"></i> Documents</button>
                <button class="btn bg-default headerbtn " id="btn_private_notes"><i class="fa fa-list"
                        aria-hidden="true"></i> Private Notes</button>
                <button class="btn bg-default headerbtn showDischargeSummary" onclick="showDischargeSummary();"><i
                        class="fa fa-tasks" aria-hidden="true"></i> Discharge
                    Summary </button>


                <button @if (trim($visit_status) !='IP' ) disabled="disabled" @endif type="button"
                    onclick="editDischargeSummary('{{ $current_visit }}');" class="btn bg-default headerbtn"><span><i
                            class="fa fa-plus"></i></span> Create Summary
                </button>
                <button type="button" onclick="surgery_request();" class="btn bg-default headerbtn"><span><i
                            class="fa fa-heart"></i></span> Surgery Posting
                </button>

                <button type="button" onclick="drugReaction();" class="btn bg-default headerbtn"
                    style="color:brown;"><span><i class="fa fa-medkit"></i></span> Adverse Drug Reaction
                </button>
                <button type="button" onclick="CasualtySummary();" class="btn bg-default headerbtn"
                   ><span><i class="fa fa-list"></i></span> Casualty Summary
                </button>


            </div>

        </div>
        @if (env('QUEUE_MANAGEMENT_ENABLED', '') == 'TRUE' && ($show_queue_management == '1'))
        <div style="float:right; width:21%;" class="position_monitor_header">
            <div id="container" style="z-index:1050; width:20%; position:fixed;">
                <div id="monitor" style="margin:0px !important;">
                    <div id="monitorscreen">
                        <div class="col-md-12 no-padding queue_management_div" style="margin-top: 15px;">
                            <div class="col-md-6 no-padding">
                                <div class="current_token current_token_monitor"></div>
                                <div class="clerafix"></div>
                                <div class="ht10"></div>
                                <div class="selectShiftDiv" style="margin-left:7px;">
                                    <select class="form-control form-select form-select-sm selectShift"
                                        style="width:97%; box-shadow: 1px 3px 2px #adafb1;">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="row padding_sm">
                                    <button type="button" style="text-align:left;"
                                        class="next_general_token btn btn-sm btn-primary btn-block ">
                                        <i class="fa fa-forward"></i> Next Token
                                    </button>
                                </div>
                                <div class="row padding_sm">
                                    <button type="button" style="text-align:left;"
                                        class="next_special_token btn btn-success btn-block">
                                        <i class="fa fa-forward"></i> Next Special Token
                                    </button>
                                </div>
                                <div class="row padding_sm">
                                    <button type="button" style="text-align:left;"
                                        class="recall_token btn btn-block btn-warning">
                                        <i class="fa fa-refresh"></i> Recall
                                    </button>
                                </div>
                                <div class="row padding_sm">
                                    <button type="button" style="text-align:left;"
                                        class="emergency_btn btn btn-block btn-danger">
                                        <i class="fa fa-bell"></i> Emergency
                                    </button>

                                    <button type="button" style="text-align:left; display:none;"
                                        class="emergency_stop_btn btn btn-block btn-danger">
                                        <i class="fa fa-times"></i> Stop Emergency
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- <marquee class="emergency_marquee" direction="left" style="margin-top: -5px;color: #f51e17;">This is a sample scrolling text that has scrolls texts to left.</marquee> -->

                    </div>
                </div>
            </div>
        </div>

        @endif


        <div class="clearfix"></div>
        <div class="ht10"></div>

        <!----op patient list-->
        @include('Emr::emr.includes.op_patient_list')
        <!----ip patient list-->
        @include('Emr::emr.includes.ip_patient_list')

        <!-- <div class="theadscroll always-visible" style="position: relative; height: auto; margin-top:140px;"> -->
        <div style="position: relative; height: auto; margin-top:180px;" class="main_content_div">
            <div class="col-md-12 no-padding">
                <div class="col-md-12 body_spinner" style="text-align: center;">
                    <div class="loader">
                        <div class="face">
                            <div class="circle"></div>
                        </div>
                        <div class="face">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 padding_sm emr_sub clinical_data_content" style="display:none;" id="section1">
                    @include('Emr::emr.includes.patient_assessments')
                </div>
                <div class="col-md-12 padding_sm emr_sub investigation_data_content" style="display:none;"
                    id="section3">

                    @include('Emr::emr.includes.patient_investigations')
                </div>
                <div class="col-md-12 padding_sm emr_sub prescription_data_content" style="display:none;" id="section2">
                    @include('Emr::emr.includes.patient_prescription')
                    <input type="hidden" id="sub_code" value="{{ $sub_code }}">

                </div>

            </div>
        </div>



    </div>


    <input type="hidden" id="notes_edit_max_time" value="{{ \WebConf::getConfig('nursing_notes_edit_max_time') }}">
    <input type="hidden" id="patient_id" value="{{ $patient_id }}">
    <input type="hidden" id="encounter_id" value="{{ $encounter_id }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="visit_id" value="{{ $visit_id }}">
    <input type="hidden" id="admitting_doctor_id" value="{{ $doctor_id }}">
    <input type="hidden" id="current_visit_id" value="{{ $current_visit_id }}">
    <input type="hidden" id="prescription_head_id" value="0">
    <input type="hidden" id="prescription_consumable_head_id" value="0">
    <input type="hidden" id="ca_head_id" value="0">
    <input type="hidden" id="fav_form_id" value="{{ $fav_form_id }}">
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="investigation_head_id" value="{{ $investition_drafted_head_id }}"
        name="investigation_head_id">
    <input type="hidden" id="age" value="{{ $age }}">
    <input type="hidden" id="favourited_form_id" value="{{ $favourited_form_id }}">
    <input type="hidden" id="pacs_viewer_prefix" value="{{ $pacs_viewer_prefix }}">
    <input type="hidden" id="pacs_report_prefix" value="{{ $pacs_report_prefix }}">

    <input type="hidden" id="investigation_draft_mode" value="0">
    <input type="hidden" id="assesment_draft_mode" value="0">
    <input type="hidden" id="prescription_changes_draft_mode" value="0">
    <input type="hidden" id="emr_changes" value="{{ @$emr_changes ? $emr_changes : ''}}">
    <input type="hidden" id="prescription_investigation_same_print_config"
        value="{{$prescription_investigation_same_print_config}}">
    <input type="hidden" id="hidden_prescription_warning_flag"value="{{$prescription_warning_flag}}">

</div>
<div class="ip_op_btn_group position-fixed" style="position: fixed" data-spy="affix" data-offset-top="50">
    <button id='op_patient_list_btn' class="btn op_btn expand_btn anim bg-info btn_right_side_slide">
        <div class="short_btn_name anim">OP</div>
        <div class="full_btn_name anim">OP List</div>
    </button>
    <button id="ip_patient_list_btn" onclick="getIPPatientList(0)"
        class="btn ip_btn expand_btn anim btn_right_side_slide">
        <div class="short_btn_name anim">IP</div>
        <div class="full_btn_name anim">IP List</div>
    </button>
    <a class="btn-sm bg-blue smooth_scroll_class expand_btn anim " style="margin-bottom: 5px;color:white"
        title="Assesment" id="clinical_notes" href="#section1">
        <b>
            <div class="short_btn_name anim">C</div>
            <div class="full_btn_name anim">Clinical Notes</div>
        </b>
    </a>

    <a class="btn-sm bg-blue smooth_scroll_class expand_btn anim " title="Investigation" id="investigation"
        style="margin-bottom: 5px" href="#section3">
        <b>
            <div class="short_btn_name anim">I</div>
            <div class="full_btn_name anim">Investigation</div>
        </b>
    </a>

    <a class="btn-sm bg-blue smooth_scroll_class expand_btn anim " title="Prescription" id="prescription"
        style="margin-bottom: 5px; cursor:pointer;" href="#section2">
        <b>
            <div class="short_btn_name anim">P</div>
            <div class="full_btn_name anim">Prescription</div>
        </b>
    </a>



    <a class="btn-sm save_btn_bg  expand_btn anim " id="save_btn_emr" title="Save"
        style="margin-top: 50px;cursor: pointer;" onclick="saveDoctorEncounterDatas()">
        <b>
            <div class="short_btn_name anim"><i class="fa fa-save"></i></div>
            <div class="full_btn_name anim">Save All</div>
        </b>
    </a>
    {{-- <a class="btn-sm save_btn_bg  expand_btn anim" id="save_complete_btn_emr" title="Save & Complete"
        style="margin-top: 5px;cursor: pointer;" onclick="saveDoctorEncounterDatas()">
        <b>
            <div class="short_btn_name anim"><i class="fa fa-check-circle"></i></div>
            <div class="full_btn_name anim">Save & Complete</div>
        </b>
    </a> --}}
    <button class="btn bg-green " style="margin-top: 180px; cursor: pointer;  font-size: 16px;" onclick="topFunction()"
        id="myBtn" title="Go to top"><i class="fa fa-arrow-circle-up"></i></button>


    <button class="btn bg-green" style="margin-top: 280px; cursor: pointer;  font-size: 16px;" onclick="topFunction()"
        id="myBtn" title="Go to top"><i class="fa fa-arrow-circle-up"></i></button>

</div>
<!-- /page content -->

<div class="pop-content-history" style="display:none;"></div>

<!-- Custom Modals -->
@include('Emr::emr.includes.custom_modals')

@stop
@section('javascript_extra')

<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>


{!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}

<!-- Prescription -->

<!-- Investigation -->
<script
    src="{{ asset('packages/extensionsvalley/emr/js/investigation.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<!-- Vitals -->
<script src="{{ asset('packages/extensionsvalley/emr/js/vitals.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<!-- Clinical -->
<script src="{{ asset('packages/extensionsvalley/emr/js/clinical.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>

<script
    src="{{ asset('packages/extensionsvalley/emr/js/patient-emr-view.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>

<script src="{{ asset('packages/extensionsvalley/emr/js/allergy.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>

<script
    src="{{ asset('packages/extensionsvalley/emr/js/manage-document.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/drug_reaction.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="{{ asset('packages/extensionsvalley/jquery-mousewheel-master/jquery.mousewheel.min.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/surgery_request.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
            getInteractionDrugList();

            $("#investigationServiceDate").datetimepicker({
                format: 'MMM-DD-YYYY'
            });
            $("input[data-attr='date']").datetimepicker({
                format: 'MMM-DD-YYYY'
            });
            $('#op_patient_search_date').val('<?php echo Date('M-d-Y'); ?>');
            //select 1st tab investigation
            let group_id = $('.investigation_fav_tabs_list li:first').attr('data-id');
            if (group_id != "" && group_id != undefined) {
                loadFavoriteItemsTab(group_id, 1);
            }
            //load all allergy
            fetch_all_allergy('{{ $patient_id }}');
            //load fotter data prescription
            fetch_all_presc_footer_fields('{{ $encounter_id }}');

            //load drafts
            @if (!empty($investition_drafted_head_id))
                $('.draft_mode_indication_inv').html('<span class="bg-orange draft-badge"> Draft Mode <i
                        class="fa fa-exclamation-circle" title="Investigation saved as draft" aria-hidden="true"></i></span>');
                investigationClipboard('{{ $investition_drafted_head_id }}');
            @endif
            @if (!empty($medication_drafted_head_id))
                prescriptionEditMode('{{ $medication_drafted_head_id }}','edit');
                $('.draft_mode_indication_p').html('<span class="bg-orange draft-badge"> Draft Mode <i
                        class="fa fa-exclamation-circle" title="Medicine saved as draft" aria-hidden="true"></i></span>');
                medicine_saved_after_edit =1;
            @endif

            @if ($latest_form_head_id != '')
                window.latest_form_id = '{{ $latest_form_id }}';
                window.latest_form_head_id = '{{ $latest_form_head_id }}';
            @endif


            @if ($iv_subcategory)
                window.iv_sub_cat_id = '{{ $iv_subcategory }}';
            @endif

            // $('#menu_toggle').trigger('click');

            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }


            //---------------OP LIST SHOWING FUNCTIONS---------------------------------------

            function show_op_patients_list() {
                var doctor_id = $('#admitting_doctor_id').val();
                var op_patient_search_date = $('#op_patient_search_date').val();
                var url = $('#base_url').val() + "/emr/show-op-patient-list";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'doctor_id=' + doctor_id + '&search_date=' + op_patient_search_date,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(data) {
                        if (data == 0) {
                            $('#op_patients_list_data').html('No patients found!');
                        } else {
                            $('#op_patients_list_data').html(data);
                            checkEmergencyStatus();
                        }
                        var $table = $('table.theadfix_wrapper');
                        $table.floatThead({
                            scrollContainer: function($table) {
                                return $table.closest('.theadscroll');
                            }
                        });
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                    },
                    complete: function() {
                        $('.theadfix_wrapper').floatThead('reflow');
                        filterButtonsReset();
                        totalBookedPatients();
                        $("body").LoadingOverlay("hide");
                    }
                });
            }
            $(document).on('click', '#op_patient_list_btn', function() {
                show_op_patients_list();
            });
            $(document).on('blur', '#op_patient_search_date', function() {
                show_op_patients_list();
            });

            function totalBookedPatients() {
                var doctor_id = $('#admitting_doctor_id').val();
                var op_patient_search_date = $('#op_patient_search_date').val();
                var url = $('#base_url').val() + "/emr/totalBookedPatients";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'doctor_id=' + doctor_id + '&search_date=' + op_patient_search_date,
                    beforeSend: function() {

                    },
                    success: function(data) {
                        $('#TotalBookedPatientsCount').html(data);
                    },
                    complete: function() {

                    }
                });
            }


            function filterButtonsReset() {
                var allCount = $('#myTable tbody tr').length;
                var followUpCount = $("#myTable tbody tr.follow_up").length;
                var seenCount = $("#myTable tbody tr.seen").length;
                var notseenCount = $("#myTable tbody tr.not_seen").length;
                var mlcCount = $("#myTable tbody tr.mlc").length;
                var WalkinCount = $("#myTable tbody tr.walkin").length;
                $('#allCount').html(allCount);
                $('#followUpCount').html(followUpCount);
                $('#seenCount').html(seenCount);
                $('#notseenCount').html(notseenCount);
                $('#mlcCount').html(mlcCount);
                $('#WalkinCount').html(WalkinCount);
            }



            //---------------OP LIST SHOWING FUNCTIONS ENDS---------------------------------------



            $(".select_button li").click(function() {
                // if($(this).hasClass("active")){
                //     //alert('act');
                //     $(this).removeClass('active');
                // }else{
                //     //alert('inact');
                //     $(this).addClass('active');
                // }
            });

            $(document).on('mouseenter', '.expand_btn', function() {
                $('.expand_btn').css('width', '30px');
                $('.full_btn_name').hide();
                $('.short_btn_name').show();
                $(this).css('width', 'auto');
                $(this).find('.short_btn_name').hide();
                $(this).find('.full_btn_name').show();
            });
            $(document).on('mouseleave', '.expand_btn', function() {
                $('.expand_btn').css('width', '30px');
                $('.full_btn_name').hide();
                $('.short_btn_name').show();
            });
            $(document).on('click', '.notes_sec_list ul li', function() {
                var disset = $(this).attr("id");
                $('.notes_sec_list ul li').removeClass("active");
                $(this).addClass("active");
                $(this).closest('.notes_box').find(".note_content").css("display", "none");
                $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
            });



            $('.month_picker').datetimepicker({
                format: 'MM'
            });
            $('.year_picker').datetimepicker({
                format: 'YYYY'
            });


            var $table = $('table.theadfix_wrapper');

            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });

            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });

            $('.date_time_picker').datetimepicker({
                format: 'MMM-DD-YYYY hh:mm A'
            });


            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });


            $('#existing_patient_Modal').on('shown.bs.modal', function(e) {
                var $table = $('table.theadfix_wrapper');
                $table.floatThead({
                    scrollContainer: function($table) {
                        return $table.closest('.theadscroll');
                    }
                });
            });
            common_check_all_tab({{ $first_tab }});


            $(document).on('click', '.op_btn', function(event) {
                $(".op_slide_box").addClass('open');
            });

            $(document).on('click', '.slide_close_btn', function(event) {
                $(".op_slide_box").removeClass('open');
            });


            $(document).on('click', '.slide_close_btn', function(event) {
                $(".ip_slide_box").removeClass('open');
            });



            $(document).on('click', '.collapse_class', function(event) {
                $('.theadfix_wrapper').floatThead('reflow');
            });

            setTimeout(function() {
                addNewMedicine();
            }, 1000);


            function checkEmergencyStatus() {
                var doctor_emergency_status = localStorage.getItem('doctor_emergency_status') ? localStorage
                    .getItem('doctor_emergency_status') : 0;
                if (doctor_emergency_status && doctor_emergency_status == "TRUE") {
                    $(".call_token_by_booking_id_btn").attr("disabled", true);
                } else {
                    $(".call_token_by_booking_id_btn").attr("disabled", false);
                }
            }

        });

        $('#dropdownMenuButton').hover(function() {
            $('.pop-div').addClass('open');
        });

        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }

        document.getElementById('clinical_notes').onclick = function() {
            //$("#t_past_history_remark").focus();
            setTimeout(function() {
                $('#t_past_history_remark').focus()
            }, 3000);
        };

        document.getElementById('prescription').onclick = function() {
            //$("#t_past_history_remark").focus();
            setTimeout(function() {
                $('input[name="search_medicine"]').focus()
            }, 3000);
        };
        function getInteractionDrugList(){
    var url = $('#base_url').val()+"/master/getInteractionDrugList";
    $.ajax({
        type: "POST",
        url: url,
        data: {},
        beforeSend: function() {

        },
        success: function(html) {
           if(html!=0){
           localStorage.setItem("IntractionListLocal", html);

           }
        },
        complete: function() {

        }
    });
}
</script>
@endsection
