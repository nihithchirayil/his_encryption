  <div class="box no-border no-margin">
      <div class="box-body clearfix">
          <div class="theadscroll" style="position: relative; max-height: 500px;">
              <table id="discount_table"
                  class="table no-margin table_sm table-striped theadfix_wrapper  table-condensed styled-table"
                  style="border: 1px solid #CCC;">
                  <thead>
                      <tr class="table_header_bg">
                          <th width='11%'>Bill No</th>
                          <th width='15%'>Patient Name </th>
                          <th width='8%'>Bill Date</th>
                          <th width='15%'>Bill Tag</th>
                          <th width='7%'>Status</th>
                          <th width='7%'>Bill Amt.</th>
                          <th width='7%'>Dis. Type</th>
                          <th width='7%'>Dis. Value</th>
                          <th width='7%'>Dis. Amount</th>
                          <th width='7%'>Net Amt.</th>
                          <th style="text-align: center" width='3%'><i class='fa fa-eye'></i></th>
                          <th style="text-align: center" width='3%'><i class='fa fa-save'></i></th>
                          <th style="text-align: center" width='3%'><i class='fa fa-trash'></i></th>
                      </tr>
                  </thead>
                  <tbody>

                      @if (count($formDetails) != 0)

                          @foreach ($formDetails as $each)
                              @php
                                  $bill_date = $each->bill_date != '' ? date('M-d-Y', strtotime($each->bill_date)) : '';
                                  $paid_status = $each->paid_status;
                                  $discout_amt = floatval($each->bill_amount) - floatval($each->net_amount_wo_roundoff);
                              @endphp
                              <tr>
                                  <td id="bill_nodata{{ $each->id }}" class="common_td_rules">{{ $each->bill_no }}
                                  </td>
                                  <td class="common_td_rules">{{ $each->patient_name }} </td>
                                  <td class="common_td_rules">{{ $bill_date }} </td>
                                  <td class="common_td_rules">{{ $each->name }}</td>
                                  <td class="common_td_rules">{{ $each->paid_status }} </td>
                                  <td class="common_td_rules">
                                      <input type="text" name="bill_amount[]" readonly=""
                                          value="{{ $each->bill_amount }}" autocomplete="off"
                                          id="bill_amount_{{ $each->id }}" class="form-control">
                                      <input type="hidden" name="bill_head_id[]" id="bill_head_id{{ $each->id }}"
                                          value="{{ $each->id }}">
                                  </td>
                                  @if ($paid_status == 'Paid')
                                      <td class="common_td_rules">
                                          <select name="discount_type[]" id="discount_type_{{ $each->id }}"
                                              readonly onchange="myFunction('{{ $each->id }}')"
                                              class="form-control">
                                              <?php if($each->discount_type == 'Amount'){?>
                                              <option value="2" selected="selected">Amount</option>
                                              <option value="1">Percentage</option>
                                              <?php } elseif($each->discount_type == 'Percentage'){ ?>
                                              <option value="1" selected="selected">Percentage</option>
                                              <option value="2">Amount</option>
                                              <?php } else{ ?>
                                              <option value="2" selected="selected">Amount</option>
                                              <option value="1">Percentage</option>
                                              <?php } ?>
                                          </select>
                                      </td>
                                  @else
                                      <td class="common_td_rules">
                                          <select name="discount_type[]" id="discount_type_{{ $each->id }}"
                                              onchange="myFunction('{{ $each->id }}')" class="form-control">

                                              <?php if($each->discount_type == 'Amount'){?>
                                              <option value="2" selected="selected">Amount</option>
                                              <option value="1">Percentage</option>
                                              <?php } elseif($each->discount_type == 'Percentage'){ ?>
                                              <option value="1" selected="selected">Percentage</option>
                                              <option value="2">Amount</option>
                                              <?php } else{ ?>
                                              <option value="2" selected="selected">Amount</option>
                                              <option value="1">Percentage</option>
                                              <?php } ?>
                                          </select>
                                      </td>
                                  @endif

                                  @if ($paid_status == 'Paid')
                                      <td class="common_td_rules">
                                          <input type="text" name="discount_value[]"
                                              oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                              onkeyup="itemAmountCalculation('{{ $each->id }}');"
                                              value="{{ $each->discount }}" readonly autocomplete="off"
                                              id="discount_value_{{ $each->id }}" class="form-control">
                                      </td>
                                  @else
                                      <td class="common_td_rules">
                                          <input type="text" name="discount_value[]"
                                              oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                              onkeyup="itemAmountCalculation('{{ $each->id }}');" value="0.00"
                                              autocomplete="off" id="discount_value_{{ $each->id }}"
                                              class="form-control">
                                      </td>
                                  @endif

                                  <td class="common_td_rules"> <input type="text" readonly name="discount_amount[]"
                                          value="{{ $discout_amt }}" autocomplete="off"
                                          id="discount_amount_{{ $each->id }}" class="form-control"></td>
                                  <td class="common_td_rules"> <input type="text" readonly=""
                                          name="net_amount[]" value="{{ $each->net_amount_wo_roundoff }}"
                                          autocomplete="off" id="net_amount_{{ $each->id }}"
                                          class="form-control">
                                      <input type="hidden" id="netamtroundhidden<?= $each->id ?>"
                                          value="<?= $each->net_amount_wo_roundoff ?>">
                                  </td>

                                  <input type="hidden" id="bill_amount_hidden<?= $each->id ?>"
                                      value="<?= $each->bill_amount_hidden ?>">
                                  <td><button class="btn btn-warning" style="text-align: center"
                                          title="View Bill Details" id="showBillingDetailBtn<?= $each->id ?>"
                                          onclick="showBillingDetail('{{ $each->id }}','{{ $each->bill_type }}')"><i
                                              id="showBillingDetailSpin<?= $each->id ?>"
                                              class='fa fa-eye'></i></button>
                                  </td>

                                  @if ($paid_status == 'Paid')
                                      <td style="text-align: center">-</td>
                                  @else
                                      <td><button class="btn btn-success" style="text-align: center"
                                              title="Save Discount" id="SaveDiscountBtn<?= $each->id ?>"
                                              onclick="SaveDiscount('{{ $each->id }}','{{ $each->bill_type }}')"><i
                                                  id="SaveDiscountSpin<?= $each->id ?>"
                                                  class='fa fa-save'></i></button>
                                      </td>
                                  @endif
                                  @if ($paid_status == 'Paid' || $each->ot_bill_group_code == 'B4')
                                      <td style="text-align: center">-</td>
                                  @else
                                      <td><button class="btn btn-danger" style="text-align: center"
                                              title="Delete Discount" id="deleteDiscountBtn<?= $each->id ?>"
                                              onclick="deleteDiscount('{{ $each->id }}','{{ $each->bill_type }}')"><i
                                                  id="deleteDiscountSpin<?= $each->id ?>"
                                                  class='fa fa-trash'></i></button>
                                      </td>
                                  @endif
                              </tr>
                              <tr>

                              </tr>
                          @endforeach
                      @else
                          <tr>
                              <td colspan="12" style="text-align: center;"> No Result Found</td>
                          </tr>
                      @endif
                  </tbody>
              </table>
          </div>
      </div>
  </div>
