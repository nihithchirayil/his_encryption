<div class="theadscroll" style="position: relative; max-height: 500px;">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered' style="font-size: 12px;">
        <thead>
            <tr class="table_header_bg">
                <th width='45%'>Item Description</th>
                <th width='10%'>Qty.</th>
                <th width='30%'>Bill No.</th>
                <th width='15%'>Net Amount</th>
            </tr>
        </thead>
        <tbody>
            <?php
                        if(count($data)!=0){
                            $net_total=0.0;
                            foreach ($data as $each) {
                                $net_total+=floatval($each->net_amount);
                                ?>
            <tr>
                <td class="common_td_rules"><?= $each->item_desc ?></td>
                <td class="common_td_rules"><?= $each->qty ?></td>
                <td class="common_td_rules"><?= $each->bill_no ?></td>
                <td class="td_common_numeric_rules"><?= number_format($each->net_amount, 2, '.', '') ?></td>
            </tr>
            <?php

                            }
                            ?>
            <tr class="bg-info">
                <td class="common_td_rules" colspan="3">Total</td>
                <td class="td_common_numeric_rules"><?= number_format($net_total, 2, '.', '') ?></td>
            </tr>
            <?php
                        }else {
                            ?>
            <tr>
                <td colspan="4" style="text-align: center">No Result Found</td>
            </tr>
            <?php
                        }
                        ?>

        </tbody>
    </table>
</div>
