@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
@endsection
@section('content-area')

    <div class="modal fade" id="billing_detail_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display:none;">
        <div class="modal-dialog" style="max-width: 1200px;width: 100%">
            <div class="modal-content">
                <div class="modal-header" style="background: #01987a; color: #FFFFFF;">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h4 class="modal-title" id="billing_detail_head">NA</h4>
                </div>
                <div class="modal-body theadscroll always-visible" id="billing_detail_content"
                    style="position: relative; height: 400px;">
                </div>
                <div class="modal-footer" id="billing_detail_footer"></div>
            </div>
        </div>
    </div>
    <div class="right_col">
        <div class="row">
            <div class="col-md-2 pull-right padding-sm">
                {{ $title }}
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <div class="col-md-2 padding_sm" style="margin-top: 20px; padding-left: 9px !important;">
                                <div class="checkbox checkbox-success inline no-margin">
                                    <input checked="" onclick="searchBill()" type="checkbox" id="contain_search" name="contain_search" value="1">
                                    <label style="padding-left: 2px;" for="contain_search">
                                        Contain Search</label><br>
                                </div>
                            </div>
                            <div class="col-md-4 padding_sm">
                                <div class="mate-input-box">
                                    <label for="">Bill No.</label>
                                    <div class="clearfix"></div>
                                    <input type="text" autocomplete="off" class="form-control" id="bill_no" name="bill_no"
                                        value="">
                                    <div id="bill_no_AjaxDiv" class="ajaxSearchBox" style="margin-top:14px;z-index: 99999;max-height: 300px !important"></div>
                                    <input type="hidden" name="bill_no_hidden" value="" id="bill_no_hidden">
                                    <input type="hidden" name="bill_id_hidden" value="" id="bill_id_hidden">
                                </div>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-block btn-warning" onclick="ClearDiscount()"><i
                                        class="fa fa-times"></i>
                                    Clear</button>
                            </div>
                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button id="searchdatabtn" type="button" class="btn btn-block btn-success"
                                    onclick="searchBill()"><i id="searchdataspin" class="fa fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
                <input type="hidden" id="token_hiddendata" value="<?= csrf_token() ?>">
                <div class="clearfix"></div>
                <div class="box no-border no-margin" id='pending_bil_list'>
                </div>
            </div>
        </div>
    </div>

    {!! Html::script('packages/extensionsvalley/default/js/bootbox.js') !!}
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/discount_bill.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
@stop
