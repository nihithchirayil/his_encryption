@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <style>
    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <div class="row col-md-12" style="text-align: right; font-size: 12px;font-weight: bold;"> {{ $title }}
            <div class="clearfix"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">



                            <div class="col-md-4 padding_sm" style="margin-top: 25px; margin-left: 1160px;">
                                <button data-toggle="modal" data-target="#critical_equipment_downtime_modal"
                                    class="btn bg-primary"><i class="fa fa-plus"></i> Add</button>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="theadscroll" style="position: relative; height: 350px;">
                            <table
                                class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg" style="cursor: pointer;">
                                        <th>Patient</th>
                                        <th>Uhid</th>
                                        <th>Surgery</th>
                                        <th>Surgeon</th>
                                        <th>Anaesthetist</th>
                                        <th>Theater</th>
                                        <th>Start Time</th>
                                        <th>Stop Time</th>
                                        <th>Delete/Edit</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if (count($item) > 0)
                                        @foreach ($item as $item)

                                            <tr style="cursor: pointer;">

                                                <td class="common_td_rules">{{ $item->patient_name }}<input type="hidden"
                                                        class="fav_check" name="patient_name[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="patient_name[]"
                                                        value="{{ $item->patient_name }}">
                                                </td>
                                                <td class="common_td_rules">{{ $item->uhid }}<input type="hidden"
                                                        class="fav_check" name="uhid[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="uhid[]" value="{{ $item->uhid }}">
                                                </td>

                                                <td class="common_td_rules">{{ $item->surgery }}<input type="hidden"
                                                        class="fav_check" name="surgery_id[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="surgery_name[]"
                                                        value="{{ $item->surgery }}">
                                                </td>
                                                <td class="common_td_rules">{{ $item->doctor_name }}<input type="hidden"
                                                        class="fav_check" name="doctor_name[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="doctor_name[]"
                                                        value="{{ $item->doctor_name }}">
                                                </td>
                                                <td class="common_td_rules">{{ $item->anaesthetist }}<input type="hidden"
                                                        class="fav_check" name="anaesthetist[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="anaesthetist[]"
                                                        value="{{ $item->anaesthetist }}">
                                                </td>
                                                <td class="common_td_rules">{{ $item->theater }}<input type="hidden"
                                                        class="fav_check" name="theater_name[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="theater[]"
                                                        value="{{ $item->theater }}">
                                                </td>
                                                <td class="common_td_rules">
                                                    {{ date('M-d-Y h:i A', strtotime($item->start_time)) }}<input
                                                        type="hidden" class="form-control" name="fav_start_time[]"
                                                        value="{{ date('h:i A', strtotime($item->start_time)) }}"><input
                                                        type="hidden" class="form-control" name="fav_start_date[]"
                                                        value="{{ date('M-d-Y', strtotime($item->start_time)) }}">
                                                </td>
                                                <td class="common_td_rules">
                                                    {{ date('M-d-Y h:i A', strtotime($item->stop_time)) }}<input
                                                        type="hidden" class="form-control" name="fav_stop_time[]"
                                                        value="{{ date('h:i A', strtotime($item->stop_time)) }}"><input
                                                        type="hidden" class="form-control" name="fav_stop_date[]"
                                                        value="{{ date('M-d-Y', strtotime($item->stop_time)) }}">
                                                </td>
                                                <td class="">
                                                    <button class='btn btn-sm btn-default delete-critical-downtime'><i
                                                            class="fa fa-trash"></i></button><button
                                                        class='btn btn-sm btn-default edit-critical-downtime'><i
                                                            class="fa fa-edit"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="12" class="location_code">No Records found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 text-center">
                            <ul class="pagination purple_pagination pull-right">
                                {!! $page_links !!}
                            </ul>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
            </div>
        </div>
    </div>
    <!-- doctor service division modal start -->
    <div id="critical_equipment_downtime_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Post Surgery List</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['post_surgery_list_form', 'id' => 'post_surgery_list_form']) !!}

                    <input type="hidden" class="form-control" name="edit_post_surgery_list" id="edit_post_surgery_list">

                    <div class="col-md-12 padding_sm" style="margin-top:8px;padding:10px !important;">
                        <div class="col-md-6 padding_sm">

                            <label for="">Select Patient</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" id='patient_general_search_txt'
                                onkeyup="patient_general_search(this.value);" class="form-control" value="">

                            <div style="width:300px; display: none;z-index: 9999; min-height: 250px;"
                                id="completepatientbox" class="ajaxSearchBox">
                            </div>
                            <input type="hidden" id="patient_id_hidden" name="patient_id_hidden" />
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            {!! Form::label('surgery', 'Surgery') !!}
                            {!! Form::select('surgery', $surgery_list, null, [
    'class' => 'form-control',
    'placeholder' => 'Select Surgery',
    'id' => 'surgery',
]) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('theater', 'Theater') !!}
                            {!! Form::select('theater', $theater_list, null, [
    'class' => 'form-control',
    'placeholder' => 'Select Theater',
    'id' => 'theater',
]) !!}
                        </div>
                    </div>
                    <br>
                    <div class="col-md-12 padding_sm" style="margin-top:8px;padding:10px !important;">
                        <div class="col-md-6 padding_sm">
                            <div class="col-md-6 padding_sm">
                                {!! Form::label('start_date', 'Starts At') !!}
                                <input type="text" id='start_date' name='start_date' class="form-control datepicker"
                                    value="" data-attr="date" placeholder="Start Date">
                            </div>
                            <div class="col-md-6 padding_sm">
                                {!! Form::label('start_time', '&nbsp;') !!}
                                <input type="text" id='start_time' name='start_time' class="form-control timepicker"
                                    value="" data-attr="date" placeholder="Start Time">
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <div class="col-md-6 padding_sm">
                                {!! Form::label('stop_date', 'Stop At') !!}
                                <input type="text" id='stop_date' name='stop_date' class="form-control datepicker" value=""
                                    data-attr="date" placeholder="Stop Date">
                            </div>
                            <div class="col-md-6 padding_sm">
                                {!! Form::label('stop_time', '&nbsp;') !!}
                                <input type="text" id='stop_time' name='stop_time' class="form-control timepicker" value=""
                                    data-attr="date" placeholder="Stop Time">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm" style="margin-top:8px;padding:10px !important;">
                        <div class="col-md-6 padding_sm">
                            {!! Form::label('surgeon', 'Surgeon') !!}
                            {!! Form::select('surgeon', $doctor_list, null, [
    'class' => 'form-control',
    'placeholder' => 'Select Surgeon',
    'id' => 'surgeon',
]) !!}
                        </div>
                        <div class="col-md-6 padding_sm">
                            {!! Form::label('anaesthetist', 'Anaesthetist') !!}
                            {!! Form::select('anaesthetist', $anaesthia_list, null, [
    'class' => 'form-control',
    'placeholder' => 'Select Anaesthetist',
    'id' => 'anaesthetist',
]) !!}
                        </div>
                    </div>

                    <div class="col-md-12 padding_sm" style="margin-top:8px;padding:10px !important;">
                        <div class="col-md-6 padding_sm">

                            <label for="">Select Scrub</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" id='scrub_search_txt' onkeyup="scrub_search(this.value);"
                                class="form-control" value="">

                            <div style="width:300px; display: none;z-index: 9999; min-height: 250px;" id="completescrubbox"
                                class="ajaxSearchBox">
                            </div>
                            <input type="hidden" id="scrub_id_hidden" name="scrub_id_hidden" />
                        </div>
                        <div class="col-md-6 padding_sm">

                            <label for="">Select Technition</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" id='technition_search_txt'
                                onkeyup="technition_search(this.value);" class="form-control" value="">

                            <div style="width:300px; display: none;z-index: 9999; min-height: 250px;"
                                id="completetechnitionbox" class="ajaxSearchBox">
                            </div>
                            <input type="hidden" id="technition_id_hidden" name="technition_id_hidden" />
                        </div>
                    </div>



                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-green" onclick="addPostSurgery()"> <i class="fa fa-check"></i>
                        Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}

    {!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}

    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            setTimeout(function() {
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');

            }, 300);



            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });
            $('.datepicker').datetimepicker({
                format: 'DD-MMM-YYYY'
            });
            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });
            //  $('.date_time_picker').datetimepicker();
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });



        });
    </script>

@stop

@section('javascript_extra')
    <script type="text/javascript">
        function addPostSurgery() {
            let edit_post_surgery_list = $("#edit_post_surgery_list").val();
            let patient_id = $("#patient_id_hidden").val();
            let start_date = $("#start_date").val();
            let start_time = $("#start_time").val();
            let stop_date = $("#stop_date").val();
            let stop_time = $("#stop_time").val();
            let surgery = $("#surgery").val();
            let theater = $("#theater").val();
            let surgeon = $("#surgeon").val();
            let anaesthetist = $("#anaesthetist").val();
            let scrub_id = $("#scrub_id_hidden").val();
            let technition_id = $("#technition_id_hidden").val();

            if (patient_id == '') {
                toastr.warning("Patient Required");
                return;
            } else if (start_date == '') {
                toastr.warning("Start Date Required");
                return;
            } else if (start_time == '') {
                toastr.warning("Start Time Required");
                return;
            } else if (surgery == '') {
                toastr.warning("Surgery Required");
                return;
            } else if (theater == '') {
                toastr.warning("Theater Time Required");
                return;
            } else if (surgeon == '') {
                toastr.warning("Surgeon Time Required");
                return;
            } else {

                if (edit_post_surgery_list != '' && edit_post_surgery_list != 0) {
                    deletePostSurgeryList(edit_post_surgery_list);
                }

                var url = $('#base_url').val() + "/surgery/save_surgery";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'patient_id=' + patient_id + '&start_date=' + start_date +
                        '&start_time=' + start_time + '&stop_date=' + stop_date +
                        '&stop_time=' + stop_time + '&surgery=' + surgery +
                        '&theater=' + theater + '&surgeon=' + surgeon + '&anaesthetist=' + anaesthetist +
                        '&scrub_id=' + scrub_id + '&technition_id=' + technition_id,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(response) {
                        if (response.status == 1) {
                            Command: toastr["success"]("Saved.");
                            window.location.href = $('#domain_url').val() +
                            "/surgery/PostSurgeryList/";

                        }
                        else {
                            Command: toastr["error"]("Error.");
                        }
                    },
                    complete: function() {
                        $("body").LoadingOverlay("hide");
                    }
                });
            }
        }

        $(document).on('click', '.delete-critical-downtime', function() {
            if (confirm("Are you sure you want to delete.!")) {
                let tr = $(this).closest('tr');
                let edit_id = $(tr).find('input[name="sent_evnt_id[]"]').val();

                if (edit_id != '' && edit_id != 0) {
                    deleteSentinalRowFromDb(edit_id);
                    $(tr).remove();
                } else {
                    $(tr).remove();
                }
            }
        });

        //delete single row from db
        function deleteSentinalRowFromDb(id) {
            var url = $('#base_url').val() + "/sentinalevent/delete-critical-downtime";
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id,
                    _token: _token,
                },
                beforeSend: function() {
                    $("body").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(data) {
                    if (data != '' && data != undefined && data != 0) {
                        Command: toastr["success"]("Deleted.");
                    }
                },
                complete: function() {
                    $("body").LoadingOverlay("hide");
                }
            });
        }

        function deleteSentinalRow(id) {
            var url = $('#base_url').val() + "/sentinalevent/delete-critical-downtime";
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id,
                    _token: _token,
                },
                beforeSend: function() {

                },
                success: function(data) {

                },
                complete: function() {

                }
            });
        }

        function SelectedPatient(id, name, uhid) {

            $('#patient_general_search_txt').val(name + '[' + uhid + ']');
            $('#patient_id_hidden').val(id);
            $('.ajaxSearchBox').css("display", "none");
        }

        function select_scrub(id, name) {
            $('#scrub_search_txt').val(name);
            $('#scrub_id_hidden').val(id);
            $('.ajaxSearchBox').css("display", "none");
        }

        function select_technition(id, name) {
            $('#technition_search_txt').val(name);
            $('#technition_id_hidden').val(id);
            $('.ajaxSearchBox').css("display", "none");
        }

        function technition_search(txt) {
            var last_search_key = '';
            var request_flag = '';
            if (txt.length >= 2 && $.trim(txt) != '') {



                if (last_search_key == txt || request_flag == 1) {
                    return false;
                }
                last_search_key = txt;
                var url = $('#base_url').val() + "/surgery/TechnitionSearch";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'txt=' + txt,
                    beforeSend: function() {
                        $("#completetechnitionbox").empty();
                        $("#completetechnitionbox").show();
                        $(".input_rel .input_spinner").removeClass('hide');
                        $("#completetechnitionbox").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                        request_flag = 1
                    },
                    success: function(data) {

                        $("#completetechnitionbox").LoadingOverlay("hide");
                        $("#completetechnitionbox").html(data);
                        $(".input_rel .input_spinner").addClass('hide');
                        request_flag = 0;
                    }
                });

            } else if (txt.length === 0) {
                $("#completetechnitionbox").hide();
                $("#completetechnitionbox").LoadingOverlay("hide");
            }
        }


        function scrub_search(txt) {
            var last_search_key = '';
            var request_flag = '';
            if (txt.length >= 2 && $.trim(txt) != '') {



                if (last_search_key == txt || request_flag == 1) {
                    return false;
                }
                last_search_key = txt;
                var url = $('#base_url').val() + "/surgery/scrubSearch";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'txt=' + txt,
                    beforeSend: function() {
                        $("#completescrubbox").empty();
                        $("#completescrubbox").show();
                        $(".input_rel .input_spinner").removeClass('hide');
                        $("#completescrubbox").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                        request_flag = 1
                    },
                    success: function(data) {

                        $("#completescrubbox").LoadingOverlay("hide");
                        $("#completescrubbox").html(data);
                        $(".input_rel .input_spinner").addClass('hide');
                        request_flag = 0;
                    }
                });

            } else if (txt.length === 0) {
                $("#completescrubbox").hide();
                $("#completescrubbox").LoadingOverlay("hide");
            }
        }



        function patient_general_search(txt) {
            var last_search_key = '';
            var request_flag = '';
            if (txt.length >= 2 && $.trim(txt) != '') {



                if (last_search_key == txt || request_flag == 1) {
                    return false;
                }
                last_search_key = txt;
                var url = $('#base_url').val() + "/surgery/PatientSearch";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'txt=' + txt,
                    beforeSend: function() {
                        $("#completepatientbox").empty();
                        $("#completepatientbox").show();
                        $(".input_rel .input_spinner").removeClass('hide');
                        $("#completepatientbox").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                        request_flag = 1
                    },
                    success: function(data) {

                        $("#completepatientbox").LoadingOverlay("hide");
                        $("#completepatientbox").html(data);
                        $(".input_rel .input_spinner").addClass('hide');
                        request_flag = 0;
                    }
                });

            } else if (txt.length === 0) {
                $("#completepatientbox").hide();
                $("#completepatientbox").LoadingOverlay("hide");
            }
        }

        $(document).on('click', '.edit-critical-downtime', function() {

            $('#critical_equipment_downtime_modal').modal('show');
            let tr = $(this).closest('tr');
            let edit_id = $(tr).find('input[name="sent_evnt_id[]"]').val();
            let fav_event_name = $(tr).find('input[name="fav_event_name[]"]').val();
            let fav_event_stop_time = $(tr).find('input[name="fav_event_stop_time[]"]').val();
            let fav_event_stop_date = $(tr).find('input[name="fav_event_stop_date[]"]').val();
            let fav_event_start_time = $(tr).find('input[name="fav_event_start_time[]"]').val();
            let fav_event_start_date = $(tr).find('input[name="fav_event_start_date[]"]').val();
            let fav_name = $(tr).find('input[name="fav_name[]"]').val();
            let fav_description = $(tr).find('input[name="fav_description[]"]').val();

            if (fav_name != '' && edit_id != '') {

                $("#event_type option[value=" + fav_name + "]").attr('selected', 'selected');
                $('input[name="event_name"]').val(fav_event_name);
                $('input[name="event_stop_time"]').val(fav_event_stop_time);
                $('input[name="event_stop_date"]').val(fav_event_stop_date);
                $('input[name="event_start_time"]').val(fav_event_start_time);
                $('input[name="event_start_date"]').val(fav_event_start_date);
                $('#event_description').val(fav_description)
                $('input[name="edit_sent_evnt"]').val(edit_id);

            }
        });
    </script>

@endsection
