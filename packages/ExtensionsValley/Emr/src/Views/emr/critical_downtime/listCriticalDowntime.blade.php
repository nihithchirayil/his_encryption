@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <style>
    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="row col-md-12" style="text-align: right; font-size: 12px;font-weight: bold;"> {{ $title }}
            <div class="clearfix"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">



                            <div class="col-md-4 padding_sm" style="margin-top: 25px; margin-left: 1160px;">
                                <button data-toggle="modal" data-target="#critical_equipment_downtime_modal"
                                    class="btn bg-primary"><i class="fa fa-plus"></i> Add</button>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="theadscroll" style="position: relative; height: 350px;">
                            <table
                                class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg" style="cursor: pointer;">
                                        <th>Equipment</th>
                                        <th>Location</th>
                                        <th>Start Time</th>
                                        <th>Stop Time</th>
                                        <th>Description</th>
                                        <th>Delete/Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($item) > 0)
                                        @foreach ($item as $item)

                                            <tr style="cursor: pointer;">

                                                <td class="common_td_rules">{{ $item->equipment }}<input type="hidden"
                                                        class="fav_check" name="downtime_id[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="equipment_name[]"
                                                        value="{{ $item->equipment }}">
                                                </td>
                                                <td class="common_td_rules">{{ $item->location_name }}<input type="hidden"
                                                        class="fav_check" name="location_name[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="location[]"
                                                        value="{{ $item->location_name }}">
                                                </td>
                                                <td class="common_td_rules">
                                                    {{ date('M-d-Y h:i A', strtotime($item->start_time)) }}<input
                                                        type="hidden" class="form-control" name="fav_start_time[]"
                                                        value="{{ date('h:i A', strtotime($item->start_time)) }}"><input
                                                        type="hidden" class="form-control" name="fav_start_date[]"
                                                        value="{{ date('M-d-Y', strtotime($item->start_time)) }}">
                                                </td>
                                                <td class="common_td_rules">
                                                    {{ date('M-d-Y h:i A', strtotime($item->stop_time)) }}<input
                                                        type="hidden" class="form-control" name="fav_stop_time[]"
                                                        value="{{ date('h:i A', strtotime($item->stop_time)) }}"><input
                                                        type="hidden" class="form-control" name="fav_stop_date[]"
                                                        value="{{ date('M-d-Y', strtotime($item->stop_time)) }}">
                                                </td>
                                                <td class="common_td_rules">{{ $item->description }}<input type="hidden"
                                                        class="form-control" name="fav_description[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="description[]"
                                                        value="{{ $item->description }}"></td>
                                                <td>
                                                    <button class='btn btn-sm btn-default delete-critical-downtime'><i
                                                            class="fa fa-trash"></i></button><button
                                                        class='btn btn-sm btn-default edit-critical-downtime'><i
                                                            class="fa fa-edit"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="12" class="location_code">No Records found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 text-center">
                            <ul class="pagination purple_pagination pull-right">
                                {!! $page_links !!}
                            </ul>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
            </div>
        </div>
    </div>
    <!-- doctor service division modal start -->
    <div id="critical_equipment_downtime_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Critical Equipment Downtime</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['critical_equipment_downtime_form', 'id' => 'critical_equipment_downtime_form']) !!}

                    <input type="hidden" class="form-control" name="edit_critical_downtime" id="edit_critical_downtime">

                    <div class="col-md-12">
                        <div class="col-md-6">
                            {!! Form::label('equipment', 'Equipment') !!}
                            {!! Form::select('equipment', $equipment, null, [
    'class' => 'form-control',
    'placeholder' => 'Select Equipment',
    'id' => 'equipment',
]) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('location', 'Location') !!}
                            {!! Form::select('location', $location_list, null, [
    'class' => 'form-control',
    'placeholder' => 'Select Location',
    'id' => 'location',
]) !!}
                        </div>
                    </div>
                    <br>
                    <div class="col-md-12 padding_sm" style="margin-top:8px;padding:10px !important;">
                        <div class="col-md-6 padding_sm">
                            <div class="col-md-6 padding_sm">
                                {!! Form::label('start_date', 'Starts At') !!}
                                <input type="text" id='start_date' name='start_date' class="form-control datepicker"
                                    value="" data-attr="date" placeholder="Start Date">
                            </div>
                            <div class="col-md-6 padding_sm">
                                {!! Form::label('start_time', '&nbsp;') !!}
                                <input type="text" id='start_time' name='start_time' class="form-control timepicker"
                                    value="" data-attr="date" placeholder="Start Time">
                            </div>
                        </div>
                        <div class="col-md-6 padding_sm">
                            <div class="col-md-6 padding_sm">
                                {!! Form::label('stop_date', 'Stop At') !!}
                                <input type="text" id='stop_date' name='stop_date' class="form-control datepicker" value=""
                                    data-attr="date" placeholder="Stop Date">
                            </div>
                            <div class="col-md-6 padding_sm">
                                {!! Form::label('stop_time', '&nbsp;') !!}
                                <input type="text" id='stop_time' name='stop_time' class="form-control timepicker" value=""
                                    data-attr="date" placeholder="Stop Time">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="col-md-12" style="margin-top:8px;padding:20px;">
                        {!! Form::label('description', 'Description') !!}
                        <textarea
                            style="overflow-y:scroll; resize:vertical; border:1px solid #dddddd; width:100%; height: 100px"
                            cols="50" id="description" name="description"></textarea>
                    </div>
                    <br>



                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-green" onclick="addcriticalDowntime()"> <i
                            class="fa fa-check"></i> Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}

    {!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}

    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            setTimeout(function() {
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');

            }, 300);



            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });
            $('.datepicker').datetimepicker({
                format: 'DD-MMM-YYYY'
            });
            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });
            //  $('.date_time_picker').datetimepicker();
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });



        });
    </script>

@stop

@section('javascript_extra')
    <script type="text/javascript">
        function addcriticalDowntime() {
            let edit_critical_downtime = $("#edit_critical_downtime").val();
            //alert(edit_sent_evnt);
            let equipment = $("#equipment").val();
            let start_date = $("#start_date").val();
            let start_time = $("#start_time").val();
            let stop_date = $("#stop_date").val();
            let stop_time = $("#stop_time").val();
            let location = $("#location").val();
            let description = $("#description").val();

            if (equipment == '') {
                toastr.warning("Equipment Required");
                return;
            } else if (start_date == '') {
                toastr.warning("Start Date Required");
                return;
            } else if (start_time == '') {
                toastr.warning("Start Time Required");
                return;
            } else if (location == '') {
                toastr.warning("Location Required");
                return;
            } else {

                if (edit_critical_downtime != '' && edit_critical_downtime != 0) {
                    deleteCriticalDowntime(edit_sent_evnt);
                }
                var url = $('#domain_url').val() + "/critical_downtime/save_critical_downtime";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'equipment=' + equipment + '&start_date=' + start_date +
                        '&start_time=' + start_time + '&stop_date=' + stop_date +
                        '&stop_time=' + stop_time + '&location=' + location +
                        '&description=' + description,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(response) {
                        if (response.status == 1) {
                            Command: toastr["success"]("Saved.");
                            window.location.href = $('#domain_url').val() +
                            "/critical_downtime/listCriticalDowntime/";

                        }
                        else {
                            Command: toastr["error"]("Error.");
                        }
                    },
                    complete: function() {
                        $("body").LoadingOverlay("hide");
                    }
                });
            }
        }

        $(document).on('click', '.delete-critical-downtime', function() {
            if (confirm("Are you sure you want to delete.!")) {
                let tr = $(this).closest('tr');
                let edit_id = $(tr).find('input[name="sent_evnt_id[]"]').val();

                if (edit_id != '' && edit_id != 0) {
                    deleteSentinalRowFromDb(edit_id);
                    $(tr).remove();
                } else {
                    $(tr).remove();
                }
            }
        });

        //delete single row from db
        function deleteSentinalRowFromDb(id) {
            var url = $('#domain_url').val() + "/sentinalevent/delete-critical-downtime";
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id,
                    _token: _token,
                },
                beforeSend: function() {
                    $("body").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(data) {
                    if (data != '' && data != undefined && data != 0) {
                        Command: toastr["success"]("Deleted.");
                    }
                },
                complete: function() {
                    $("body").LoadingOverlay("hide");
                }
            });
        }

        function deleteSentinalRow(id) {
            var url = $('#domain_url').val() + "/sentinalevent/delete-critical-downtime";
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id,
                    _token: _token,
                },
                beforeSend: function() {

                },
                success: function(data) {

                },
                complete: function() {

                }
            });
        }

        $(document).on('click', '.edit-critical-downtime', function() {

            $('#critical_equipment_downtime_modal').modal('show');
            let tr = $(this).closest('tr');
            let edit_id = $(tr).find('input[name="sent_evnt_id[]"]').val();
            let fav_event_name = $(tr).find('input[name="fav_event_name[]"]').val();
            let fav_event_stop_time = $(tr).find('input[name="fav_event_stop_time[]"]').val();
            let fav_event_stop_date = $(tr).find('input[name="fav_event_stop_date[]"]').val();
            let fav_event_start_time = $(tr).find('input[name="fav_event_start_time[]"]').val();
            let fav_event_start_date = $(tr).find('input[name="fav_event_start_date[]"]').val();
            let fav_name = $(tr).find('input[name="fav_name[]"]').val();
            let fav_description = $(tr).find('input[name="fav_description[]"]').val();

            if (fav_name != '' && edit_id != '') {

                $("#event_type option[value=" + fav_name + "]").attr('selected', 'selected');
                $('input[name="event_name"]').val(fav_event_name);
                $('input[name="event_stop_time"]').val(fav_event_stop_time);
                $('input[name="event_stop_date"]').val(fav_event_stop_date);
                $('input[name="event_start_time"]').val(fav_event_start_time);
                $('input[name="event_start_date"]').val(fav_event_start_date);
                $('#event_description').val(fav_description)
                $('input[name="edit_sent_evnt"]').val(edit_id);

            }
        });
    </script>

@endsection
