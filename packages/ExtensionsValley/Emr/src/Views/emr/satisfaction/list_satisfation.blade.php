@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <style>
    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <div class="row col-md-12" style="text-align: right; font-size: 12px;font-weight: bold;"> {{ $title }}
            <div class="clearfix"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">



                            <div class="col-md-4 padding_sm" style="margin-top: 25px; margin-left: 1160px;">
                                <button data-toggle="modal" data-target="#critical_equipment_downtime_modal"
                                    class="btn bg-primary"><i class="fa fa-plus"></i> Add</button>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="theadscroll" style="position: relative; height: 350px;">
                            <table
                                class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg" style="cursor: pointer;">
                                        <th>Ip Satisfaction</th>
                                        <th>Op Satisfaction</th>
                                        <th>Employee Satisfaction</th>
                                        <th>Overall Compliance</th>
                                        <th>Created By</th>
                                        <th>Updated By</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th>Delete/Edit</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if (count($item) > 0)
                                        @foreach ($item as $item)

                                            <tr style="cursor: pointer;">

                                                <td class="">{{ $item->ip_satisfaction . '%' }}<input
                                                        type="hidden" class="fav_check" name="ip_satisfaction[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="ip_satisfaction[]"
                                                        value="{{ $item->ip_satisfaction }}">
                                                </td>
                                                <td class="">{{ $item->op_satisfaction . '%' }}<input
                                                        type="hidden" class="fav_check" name="op_satisfaction[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="op_satisfaction[]"
                                                        value="{{ $item->op_satisfaction }}">
                                                </td>

                                                <td class="">{{ $item->employee_satisfaction . '%' }}<input
                                                        type="hidden" class="fav_check" name="employee_satisfaction[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="employee_satisfaction[]"
                                                        value="{{ $item->employee_satisfaction }}">
                                                </td>
                                                <td class="">{{ $item->overall_compliance . '%' }}<input
                                                        type="hidden" class="fav_check" name="overall_compliance[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="overall_compliance[]"
                                                        value="{{ $item->overall_compliance }}">
                                                </td>
                                                <td class="common_td_rules">{{ $item->created_by }}<input type="hidden"
                                                        class="fav_check" name="created_by[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="created_by[]"
                                                        value="{{ $item->created_by }}">
                                                </td>
                                                <td class="common_td_rules">{{ $item->updated_by }}<input type="hidden"
                                                        class="fav_check" name="updated_by[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="updated_by[]"
                                                        value="{{ $item->updated_by }}">
                                                </td>

                                                <td class="common_td_rules">
                                                    {{ date('M-d-Y h:i A', strtotime($item->created_at)) }}<input
                                                        type="hidden" class="form-control" name="created_at[]"
                                                        value="{{ date('M-d-Y h:i A', strtotime($item->created_at)) }}"><input
                                                        type="hidden" class="form-control" name="created_at[]"
                                                        value="{{ date('M-d-Y h:i A', strtotime($item->created_at)) }}">
                                                </td>
                                                <td class="common_td_rules">
                                                    {{ date('M-d-Y h:i A', strtotime($item->updated_at)) }}<input
                                                        type="hidden" class="form-control" name="updated_at[]"
                                                        value="{{ date('M-d-Y h:i A', strtotime($item->updated_at)) }}"><input
                                                        type="hidden" class="form-control" name="updated_at[]"
                                                        value="{{ date('M-d-Y h:i A', strtotime($item->updated_at)) }}">
                                                </td>

                                                <td class="">
                                                    <button class='btn btn-sm btn-default delete-critical-downtime'><i
                                                            class="fa fa-trash"></i></button><button
                                                        class='btn btn-sm btn-default edit-critical-downtime'><i
                                                            class="fa fa-edit"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="12" class="location_code">No Records found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 text-center">
                            <ul class="pagination purple_pagination pull-right">
                                {!! $page_links !!}
                            </ul>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
            </div>
        </div>
    </div>
    <!-- doctor service division modal start -->
    <div id="critical_equipment_downtime_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Satisfaction Index</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['satisfaction_index_form', 'id' => 'satisfaction_index_form']) !!}

                    <input type="hidden" class="form-control" name="edit_satisfaction_index" id="edit_satisfaction_index">

                    <div class="col-md-12 padding_sm" style="margin-top:8px;padding:10px !important;">
                        <div class="col-md-6 padding_sm">

                            {!! Form::label('ip_satisfaction', 'Ip Satisfaction') !!}
                            <input type="text" id='ip_satisfaction' name='ip_satisfaction' class="form-control" value=""
                                placeholder="Ip Satisfaction">
                        </div>
                        <div class="col-md-6 padding_sm">

                            {!! Form::label('op_satisfaction', 'Op Satisfaction') !!}
                            <input type="text" id='op_satisfaction' name='op_satisfaction' class="form-control" value=""
                                placeholder="Op Satisfaction">
                        </div>
                    </div>

                    <div class="col-md-12 padding_sm" style="margin-top:8px;padding:10px !important;">
                        <div class="col-md-6 padding_sm">

                            {!! Form::label('employee_satisfaction', 'Employee Satisfaction') !!}
                            <input type="text" id='employee_satisfaction' name='employee_satisfaction'
                                class="form-control" value="" placeholder="Employee Satisfaction">
                        </div>
                        <div class="col-md-6 padding_sm">

                            {!! Form::label('overall_compliance', 'Overall Compliance') !!}
                            <input type="text" id='overall_compliance' name='overall_compliance' class="form-control"
                                value="" placeholder="Overall Compliance">
                        </div>
                    </div>


                    <br>


                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-green" onclick="addSatisfactionIndex();"> <i
                            class="fa fa-check"></i>
                        Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}

    {!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}

    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            setTimeout(function() {
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');

            }, 300);



            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });
            $('.datepicker').datetimepicker({
                format: 'DD-MMM-YYYY'
            });
            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });
            //  $('.date_time_picker').datetimepicker();
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });



        });
    </script>

@stop

@section('javascript_extra')
    <script type="text/javascript">
        function addSatisfactionIndex() {
            let edit_satisfaction_index = $("#edit_satisfaction_index").val();
            let ip_satisfaction = $("#ip_satisfaction").val();
            let op_satisfaction = $("#op_satisfaction").val();
            let employee_satisfaction = $("#employee_satisfaction").val();
            let overall_compliance = $("#overall_compliance").val();



            if (edit_satisfaction_index != '' && edit_satisfaction_index != 0) {
                deleteSatisfactionIndex(edit_satisfaction_index);
            }

            var url = $('#base_url').val() + "/satisfaction_index/save_satisfaction_index";
            $.ajax({
                type: "GET",
                url: url,
                data: 'edit_satisfaction_index=' + edit_satisfaction_index + '&ip_satisfaction=' + ip_satisfaction +
                    '&op_satisfaction=' + op_satisfaction + '&employee_satisfaction=' + employee_satisfaction +
                    '&overall_compliance=' + overall_compliance,
                beforeSend: function() {
                    $("body").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(response) {
                    if (response.status == 1) {
                        Command: toastr["success"]("Saved.");
                        window.location.href = $('#domain_url').val() +
                        "/satisfaction_index/satisfactionIndexList/";

                    }
                    else {
                        Command: toastr["error"]("Error.");
                    }
                },
                complete: function() {
                    $("body").LoadingOverlay("hide");
                }
            });

        }

        $(document).on('click', '.delete-critical-downtime', function() {
            if (confirm("Are you sure you want to delete.!")) {
                let tr = $(this).closest('tr');
                let edit_id = $(tr).find('input[name="sent_evnt_id[]"]').val();

                if (edit_id != '' && edit_id != 0) {
                    deleteSentinalRowFromDb(edit_id);
                    $(tr).remove();
                } else {
                    $(tr).remove();
                }
            }
        });

        //delete single row from db
        function deleteSatisfactionIndex(id) {
            var url = $('#base_url').val() + "/satisfaction_index/delete-satisfaction_index";
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id,
                    _token: _token,
                },
                beforeSend: function() {
                    $("body").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(data) {
                    if (data != '' && data != undefined && data != 0) {
                        Command: toastr["success"]("Deleted.");
                    }
                },
                complete: function() {
                    $("body").LoadingOverlay("hide");
                }
            });
        }
    </script>

@endsection
