@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<style>
    table > tbody > tr > td {
        text-align: left !important;
    }
</style>
@endsection
@section('content-area')

 <!-- page content -->
        <div class="right_col"  role="main">
        <div class="row codfox_container">
            <!-- title section -->
            <div class="col-md-12">
                <b>{{strtoupper($title)}}</b>
                <hr>
            </div>
            <!-- title section -->

            <!-- Mesage Container -->
            <div class="col-md-12">
                @if (\Session::has('successMsg'))
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> {{\Session::get("successMsg")}}.
                    </div>
                @elseif (\Session::has('failureMsg'))
                    <div class="alert alert-danger alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Error!</strong> {{\Session::get("failureMsg")}}.
                    </div>
                @endif
            </div>
            <!-- Mesage Container -->

            <div class="col-md-12 padding_sm">
                {!!Form::open(array('route' =>'extensionsvalley.forms.categorylistview', 'method' => 'post'))!!}
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            @php
                                $search = (!empty($searchFields['search_cat_name'])) ? $searchFields['search_cat_name'] : '';
                            @endphp

                            <div class="col-md-4 padding_sm">
                                <label for="">Category Name</label>
                                <div class="clearfix"></div>
                                <input type="text" class="form-control" name="search_cat_name" id="search_cat_name" value="{{$search}}">
                            </div>

                            <div class="col-md-6 padding_sm"></div>

                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i> Search</button>
                            </div>

                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{url('forms/category-add')}}" class="btn btn-block light_purple_bg"><i class="fa fa-plus"></i> Add</a>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::token() !!}
                {!! Form::close() !!}

                <div class="clearfix"></div>
                    <div class="box no-border no-margin" style="height: 450px;">
                        <div class="box-body clearfix">
                            <div class="theadscroll" style="position: relative; height: 350px;">
                                <table class="table no-margin theadfix_wrapper table-striped table-col-bordered table_sm table-condensed" style="border: 1px solid #CCC;">
                                        <thead>
                                        <tr class="table_header_bg">
                                            <th style="text-align: left;">Category Name</th>
                                            <th width="15%">Status</th>
                                            <th class="text-center" colspan="2" width="10%">Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($form_list) && count($form_list))
                                            @foreach($form_list as $checks)
                                            <tr>
                                                <td style="text-align: left;">{{ (!empty($checks->name)) ?  $checks->name : '' }}</td>
                                                <td>{{ (!empty($checks->status) && $checks->status == 1) ?  'Active' : 'In-Active' }}</td>
                                                <td class="text-center">
                                                    <a class="btn btn-default" onclick="EditCategory('{{$checks->id}}')" title="Edit" ><i class="fa fa-pencil"></i></a>
                                                </td>
                                                <td class="text-center">
                                                    <a class="btn btn-default" onclick="deleteData(this,'{{$checks->id}}');" title="Delete" ><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        @else
                                        <tr>
                                            <td colspan="7">{!! trans('master.records.not_found') !!}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 text-center purple_pagination">
                            <?php echo $paginator->appends($searchFields)->render(); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- /page content -->
@stop
@section('javascript_extra')
     <script type="text/javascript">
       $(document).ready(function() {

        $(document).on('click', '.grn_drop_btn', function(e) {
            e.stopPropagation();
            $(".grn_btn_dropdown").hide();
            $(this).next().slideDown('');
        });

        $(document).on('click', '.btn_group_box', function(e) {
            e.stopPropagation();
        });

        $(document).on('click', function() {
                    $(".grn_btn_dropdown").hide();
        });

        $(".select_button li").click(function() {
            $(this).toggleClass('active');
        });


        $(document).on('click', '.notes_sec_list ul li', function() {
            var disset = $(this).attr("id");
            $('.notes_sec_list ul li').removeClass("active");
            $(this).addClass("active");
            $(this).closest('.notes_box').find(".note_content").css("display", "none");
            $(this).closest('.notes_box').find("div#" + disset).css("display", "block");
        });



        $('.month_picker').datetimepicker({
            format: 'MM'
        });
        $('.year_picker').datetimepicker({
            format: 'YYYY'
        });


        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.datepicker').datetimepicker({
            format: 'DD-MM-YYYY'
        });
        $('.date_time_picker').datetimepicker();


        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });




        $('#existing_patient_Modal').on('shown.bs.modal', function(e) {
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }
            });
        });

    });

    function EditCategory(id){
      window.location.href = "{{url('forms/category-add')}}"+"/"+ id;
    }

    function deleteData(obj,id){
        if(id != ""){
             bootbox.confirm({
                message: "Do you want to delete it?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result == true){
                        var url = "{{url('forms/category-delete')}}";
                        $.ajax({
                            type: "GET",
                            url: url,
                            data: 'delete_category='+id,
                            beforeSend: function () {

                            },
                            success: function (data) {
                                if(data == 1){
                                    $(obj).parent().parent().remove();
                                    Command: toastr["success"]('Successfully Deleted ..');
                                }else{
                                    if(data == 2){
                                        Command: toastr["warning"]('Cannot delete, Fields under Category');
                                    }else{
                                        Command: toastr["warning"]('Error ..');
                                    }
                                }
                            },
                            error: function () {
                              Command: toastr["warning"]('Please check your internet connection and try again');
                            },
                            complete: function () {
                            }
                        });
                    }
                }
            });

        }else{
            Command: toastr["warning"]('Error..!');
        }
     }

    </script>
@endsection
