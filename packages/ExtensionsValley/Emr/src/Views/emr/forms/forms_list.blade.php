@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<style>
    table > tbody > tr > td {
        text-align: left !important;
    }
</style>
@endsection
@section('content-area')

 <!-- page content -->
        <div class="right_col"  role="main">
        <div class="row codfox_container">
            <!-- title section -->
            <div class="col-md-12">
                <b>{{strtoupper($title)}}</b>
                <hr>
            </div>
            <!-- title section -->

            <!-- Mesage Container -->
            <div class="col-md-12">
                @if (\Session::has('successMsg'))
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> {{\Session::get("successMsg")}}.
                    </div>
                @elseif (\Session::has('failureMsg'))
                    <div class="alert alert-danger alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Error!</strong> {{\Session::get("failureMsg")}}.
                    </div>
                @endif
            </div>
            <!-- Mesage Container -->

            <div class="col-md-12 padding_sm">

                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            @php
                                $search = (!empty($searchFields['search_cat_name'])) ? $searchFields['search_cat_name'] : '';
                            @endphp

                            <div class="col-md-4 padding_sm">
                                <label for="">Form Name</label>
                                <div class="clearfix"></div>
                                <input type="text" placeholder="Field Wise Search" name="fieldSearch" id="fieldSearch"  class="form-control" value="{{$search_string_form_list}}">
                            </div>

                            <div class="col-md-4 padding_sm">
                                <label for="">Category Name</label>
                                <div class="clearfix"></div>
                                {!!Form::select('category',$categoryArr,$category, ['class'=>'form-control category-select-in-fields-list'])!!}
                            </div>

                            <div class="col-md-2 padding_sm"></div>

                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="searchFieldWise(this)" class="btn btn-block light_purple_bg"><i class="fa fa-search"></i> Search</button>
                            </div>

                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{url('forms/create-form')}}" class="btn btn-block light_purple_bg"><i class="fa fa-plus"></i> Add</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                    <div class="box no-border no-margin" style="height: 450px;">
                        <div class="box-body clearfix">
                            <div class="theadscroll" id="list_form_container" style="position: relative; height: 350px;">

                                @include('Emr::emr.forms.ajax_pagination_forms_list')

                            </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 text-center purple_pagination">
                            <?php echo $form_list->appends(['search_string_form_list'=>$search_string_form_list])->render(); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- /page content -->


    <!-- Modal -->
    <div class="modal fade " id="formview" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 1200px;">

      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header" style="background-color:#01987A">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form</h4>
        </div>
        <div class="modal-body">
             <div class="row">
                <div class="col-md-12" id="FormItemsListView">

                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
    </div>

@stop
@section('javascript_extra')
     <script type="text/javascript">
       $(document).ready(function() {

        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function($table) {
                return $table.closest('.theadscroll');
            }

        });


        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });


    });

    function searchFieldWise(obj){
        let search_string_form_list = $('#fieldSearch').val();
        let category = $('select[name="category"]').val();
        let url = "";
        search_string_form_list = search_string_form_list.trim();
        if(search_string_form_list !== undefined){
            $.ajax({
                type: "GET",
                url: url,
                data: 'search_string_form_list='+search_string_form_list+"&category="+category,
                beforeSend: function () {

                },
                success: function (data) {
                  $("#list_form_container").html(data);
                },
                error: function () {
                  Command: toastr["warning"]('Please check your internet connection and try again');
                },
                complete: function () {
                }
            });
        }
     }

    function viewForm(id){
        if(id){
            var url = "{{url('forms/view-form')}}";
            $.ajax({
              type: "GET",
              url: url,
              data: "select_form=" + id,
              beforeSend: function () {
              },
              success: function (data) {
                $('#FormItemsListView').html(data);
                $('#formview').modal('show');
                $('.fav-button-bottom').addClass('disabled');
                $('formview > button').addClass('disabled');
                $('.add-fav-text-btn').addClass('disabled');
                $('.refresh_textarea_icon').addClass('disabled');
              },
              complete: function () {
                initTinymce();

                $(".select_button li").click(function(){
                    if($(this).hasClass("active")){
                        //alert('act');
                        $(this).removeClass('active');
                    }else{
                        //alert('inact');
                        $(this).addClass('active');
                    }
                });
              }
            });
        }
    }

    function initTinymce(){
        tinymce.init({
            selector: 'textarea.texteditor',
            theme: "modern",
            subfolder: "",
            height : "400",
            relative_urls: false,
            remove_script_host: false,
            convert_urls: true,
            convert_fonts_to_spans : false,
            paste_auto_cleanup_on_paste : true,
            paste_remove_styles: true,
            paste_remove_styles_if_webkit: true,
            paste_strip_class_attributes: true,
            paste_remove_spans  : true,
            menubar  : false,
            toolbar: "bold italic underline | alignleft aligncenter alignright | bullist numlist outdent indent | forecolor backcolor",
        });
    }

    function editForm(id){
        window.location.href = "{{url('forms/create-form')}}"+ '/' + id;
    }

    </script>
@endsection
