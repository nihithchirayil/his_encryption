@php
    $hospitalHeader = \DB::table('company')->where('id', '=', 1)->value('radiology_header');
    if(!isset($hospitalHeader) || $hospitalHeader == NULL){
        $hospitalHeader = \DB::table('company')->where('id', '=', 1)->value('hospital_header');
    }
@endphp
{!!$hospitalHeader!!}
<table class='table table-condensed' border='1' bordercolor='#dddddd'
    style='margin:0px;width: 100%; font-size: 14px; margin: 0; border-collapse: collapse;margin-top:10px;'>
    <tr>
        <td><i><b>Name:</b></i> {{ $res[0]->patient_name }}</td>
        <td><i>Age:</i> {{ $res[0]->age }}</td>
        <td><i>Gender:</i> {{ $res[0]->gender }}</td>
        <td><i>Date:</i> {{ date('M-d-Y') }}</td>
    </tr>
    <tr>
        <td colspan="4"><i>Dr.</i> {{ $res[0]->doctor_name }}</td>
    </tr>
    <tr>
        <td colspan="3"><i>Clinical diagnosis:</i></td>
        <td><i>Uhid:</i>{{ $res[0]->uhid }}</td>
    </tr>
</table>
