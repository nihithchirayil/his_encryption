<table class="table no-margin theadfix_wrapper table-striped table-col-bordered table_sm table-condensed">
    <thead>
        <tr class="table_header_bg">
            <th style="width: 17%;">Name</th>
            <th style="width: 17%;">Category</th>
            <th colspan="2" style="width: 2% !important;text-align: center">Options</th>
        </tr>
    </thead>
    <tbody>

       @if(isset($form_list) && count($form_list) > 0)
        @foreach($form_list as $checks)

        <tr>
             <td> {{ (!empty($checks->name)) ?  $checks->name : '' }}</td>
             <td> {{ (!empty($checks->category_name)) ?  $checks->category_name : '' }}</td>
             <td class="text-center">
                <a class="btn btn-default" style="margin-right: 2px;" onclick="viewForm('{{$checks->id}}');" title="View" ><i class="fa fa-eye"></i></a>
             </td>
             <td class="text-center">
                <a class="btn btn-default" style="margin-right: 2px;" onclick="editForm('{{$checks->id}}');" title="Edit" ><i class="fa fa-pencil"></i></a>
             </td>
        </tr>
       @endforeach
        @else
        <tr>
            <td colspan="7">{!! trans('master.records.not_found') !!}</td>
        </tr>
        @endif

    </tbody>
</table>
