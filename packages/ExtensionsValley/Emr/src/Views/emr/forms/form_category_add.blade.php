@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')

@endsection
@section('content-area')

 <!-- page content -->
        <div class="right_col"  role="main">
        <div class="row codfox_container">

            <!-- title section -->
            <div class="col-md-12">
                <b>{{strtoupper($title)}}</b>
                <hr>
            </div>
            <!-- title section -->

            <div class="col-md-12 padding_sm">

                {!!Form::open(array('route' => 'extensionsvalley.forms.categorysave', 'method' => 'post','id'=>'CategoryForm','files'=>true))!!}
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="col-md-4"><label for="Category Name">Category Name<span style="color: #d14;font-size: 16px;">*&nbsp;</span></label></div>
                                    <div class="col-md-8">
                                        <input type="text" name="category_name" id="category_name" value="{{$name_field}}" class="form-control" placeholder="Category Name">
                                        <input type="hidden" name="category_id" value="{{$id_field}}">
                                        <span style="color: #d14;"> {{ $errors->first('category_name') }}</span>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="col-md-6">
                                 <div class="col-md-4"><label for="Category Name">Status<span style="color: #d14;font-size: 16px;">*&nbsp;</span></label></div>
                                        <div class="col-md-8">
                                            {!! Form::select('status', [1=>'Active',0=>'In-Active'], $status_field , ['class' => 'form-control','id' => 'status', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                                            <span style="color: #d14;">{{ $errors->first('status') }}</span>
                                        </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="col-md-11 padding_sm"></div>

                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <a class="btn btn-block light_purple_bg" id="save_form_category"><i class="fa fa-save"></i>&nbsp; Save</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                {!! Form::token() !!}
                {!! Form::close() !!}

            </div>

        </div>
    </div>

    <!-- /page content -->
@stop
@section('javascript_extra')
     <script type="text/javascript">
       $(document).ready(function() {






    });

    $(document).on('click','#save_form_category',function(event) {
          event.preventDefault();

          $("#CategoryForm").submit();
     });
    </script>
@endsection
