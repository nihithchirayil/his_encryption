<table id="form_generate_table" class="table table-bordered table_sm table-striped table-condensed  form_theadfix_wrapper form_generate_table">
    <thead>
        <tr class="table_header_bg">
            <th colspan="2">Field Item List &nbsp; <i>[ Field Label (Field Name) / Default Value / Type  ]</i></th>
        </tr>
    </thead>
    <tbody>
            @if(isset($formItems) && count($formItems) > 0)
            <?php $i=0; ?>
            @foreach($formItems as $key=>$values)
            <?php
            $tags = [];
            $tagslist='';
            $class = '';
        if(ExtensionsValley\Emr\FormTagMapper::where('form_field_id','=',$values->id)->select('tag_id')->exists()){
            $tags = ExtensionsValley\Emr\FormTagMapper::where('form_tag_mapper.form_field_id','=',$values->id)
            ->join('form_tags_list','form_tags_list.id','=','form_tag_mapper.tag_id')
            ->pluck('form_tags_list.id','form_tags_list.name');
            foreach ($tags as $tag=>$index) {
                $class .= $tag.' ';
            }
        }
            ?>
            <tr class="{{$class or ''}}">
                <td class="text-center set-option-width">
                    <input type="checkbox" name="field_item[]" onclick="selectFormItem(this)" value="{{$values->id}}">
                </td>
                <td title="{{$values->default_value}}">
                    <input type="hidden" name="hidden_field_label[]" value="{{$values->field_label}}">
                    <input type="hidden" name="hidden_type_name[]" value="{{$values->type_name}}">
                    <input type="hidden" name="hidden_default_value[]" value="{{$values->default_value}}">
                    <input type="hidden" name="hidden_field_name[]" value="{{$values->field_name}}">
                    <span id="field_name">{{$values->field_label}} ( {{$values->field_name}} )</span>
                    <span class="label label-primary"> {{$values->type_name}}</span>
                </td>
            </tr>
            <?php $i++; ?>
            @endforeach
            @else
            <tr>
                <td colspan="2">No Records Found</td>
            </tr>
            @endif
    </tbody>
</table>
<div class="row" style="margin:0;">
    <?php echo $formItems->appends(['selected_tag'=>$selected_tag,'search_string'=>$search_string])->render(); ?>
</div>
