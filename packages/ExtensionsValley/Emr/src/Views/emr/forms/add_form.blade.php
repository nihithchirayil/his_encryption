@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<style>
  table > tbody > tr > td {
        text-align: left !important;
  }
  .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--multiple{
        min-height: 23px !important;
        border-color: #CCC !important;
    }
    .select2-container {
        width: 100% !important;
        padding: 0;
    }
    .select2-container .select2-selection--single {
        height: 23px !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow b{
        margin: -4px !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #444;
        line-height: 12px !important;
    }
    .modal .select2-container{
      z-index: 4000 !important;
      text-align: left !important;
      font-size: 13px !important;
    }
    .select2-container{
      text-align: left !important;
      font-size: 13px !important;
      box-shadow: 2px 3px 7px #e4e4e4;
    }
    .select2-container--open{
      z-index: 4000 !important;
    }

    .select2-container .select2-selection--single{
      height: auto !important;
      border-radius: 4px !important;
    }

</style>
@endsection
@section('content-area')

 <!-- page content -->
        <div class="right_col"  role="main">
        <div class="row codfox_container">

            <!-- title section -->
            <div class="col-md-12">
                <b>{{strtoupper($title)}}</b>
                <hr>
            </div>
            <!-- title section -->

            <!-- Mesage Container -->
            <div class="col-md-12">
                @if (\Session::has('successMsg'))
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> {{\Session::get("successMsg")}}.
                    </div>
                @elseif (\Session::has('failureMsg'))
                    <div class="alert alert-danger alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Error!</strong> {{\Session::get("failureMsg")}}.
                    </div>
                @endif
            </div>
            <!-- Mesage Container -->

            <div class="col-md-12 padding_sm">

              <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">

                            <div class="col-md-4 padding_sm">
                                <label for="">Tag Name</label>
                                <div class="clearfix"></div>
                                <select class="search-tag form-control" name="tagName" placeholder="Select Tag Here " id="tagName"   >
                                <option value="all">All</option>
                                <?php
                                if(isset($tagList) && count($tagList) > 0){
                                    foreach($tagList as $name => $value) {
                                    ?>
                                    <option value="{{$value}}">{{$name}}</option>
                                    <?php
                                    }
                                }
                                ?>
                                </select>
                            </div>

                            <div class="col-md-4 padding_sm">
                                <label for="">Field Name</label>
                                <div class="clearfix"></div>
                                <input type="text" placeholder="Field Wise Search" name="fieldSearch" id="fieldSearch" onKeyup="searchFieldWise(this)"  class="form-control">
                            </div>

                            <div class="col-md-4 padding_sm"></div>

                            {{-- <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button onclick="searchFieldWise(this)" class="btn btn-block light_purple_bg"><i class="fa fa-search"></i> Search</button>
                            </div> --}}

                        </div>
                    </div>
                </div>

                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                          <div class="box-body clearfix">

                          <div class="row" style="margin-top:10px;">
                              <div class="col-md-5 mycontent-left ">
                                  <div class="form_theadscroll always-visible" id="list_item_container" /*style="height: 300px; padding-right:10px; position: relative;"*/>
                                      @include('Emr::emr.forms.ajax_pagination_add_form')
                                  </div>
                              </div>

                              {!!Form::open(array('route' => 'extensionsvalley.forms.saveform', 'method' => 'post','id'=>'GenerateForm','files'=>true))!!}

                              <div class="col-md-7">
                                  <div class="form_theadscroll always-visible" style="height: 300px;overflow-y: auto;overflow-x: none;position: relative;">
                                      <table id="form_generate_table_selected" class="table table-bordered table_sm table-striped table-condensed form_theadfix_wrapper form_generate_table_selected">
                                          <thead>
                                              <tr class="table_header_bg">
                                                  <th>Form</th>
                                                  <th class="set-option-width">Order</th>
                                                  <th class="set-option-width" style="font-size: 11px;">is Header</th>
                                                  <th class="set-option-width2" style="font-size: 11px;">Header Elements</th>
                                                  <th class="set-option-width">Remove</th>
                                              </tr>
                                          </thead>
                                          <tbody id="ListFormSelElement">
                                                  @if(isset($selectedItems) && count($selectedItems) > 0)
                                                      <?php $j=0;  ?>
                                                      @foreach($selectedItems as $key=>$values)
                                                      <tr title="{{$values->default_value}}">
                                                          <td>
                                                              <input type="hidden" name="hidden_form_field_label[]" value="{{$values->field_label}}">
                                                              <input type="hidden" name="hidden_form_type_name[]" value="{{$values->type_name}}">
                                                              <input type="hidden" name="hidden_form_default_value[]" value="{{$values->default_value}}">
                                                              <input type="hidden" name="hidden_form_field_item_id[]" value="{{$values->id}}">
                                                              <input type="hidden" name="hidden_form_field_name[]" value="{{$values->field_name}}">
                                                              <span id="field_name">{{$values->field_label}} ({{$values->field_name}})</span>
                                                              <span class="label label-primary"> {{$values->type_name}}</span>
                                                           </td>
                                                           <td class="text-center set-option-width">
                                                              <input type="text" name="order[]" onblur="checkOrderVal(this)" value="{{$values->ordering}}" class="form-control">
                                                           </td>

                                                           <td class="text-center set-option-width">
                                                              <input type="checkbox" class="is_group_header" data-optionname="{{$values->field_label}}" data-optionval="{{$values->id}}" name="is_group_header[{{$values->id}}]" @if($values->is_group_header == 1) checked @endif value="1" >
                                                           </td>
                                                           <td class="text-center set-option-width2 group-header-element">
                                                              <select class="form-control"  name="group_header[]" @if($values->is_group_header == 1) style="display: none;" @endif >
                                                                  <option value="">Group Header</option>
                                                                      @if(sizeof($groupedListArr) > 0)
                                                                          @foreach ($groupedListArr as $key => $element)
                                                                             <option @if($values->is_group_header == 0 && $values->group_header_fields_id == $key) selected @endif value="{{$key}}">{{$element}}</option>
                                                                          @endforeach
                                                                      @endif
                                                              </select>
                                                           </td>
                                                           <td class="text-center set-option-width">
                                                               <a onclick="removeFormItem(this)" data-value="{{$values->id}}" style="cursor: pointer;">
                                                               <i class="fa fa-minus"></i>
                                                               </a>
                                                           </td>
                                                      </tr>
                                                      <?php $j++; ?>
                                                      @endforeach
                                                      @else
                                                      <tr id="form_generate_table_selected_no_data">
                                                          <td colspan="7">No Records Found.!</td>
                                                      </tr>
                                                      @endif
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                          </div>

                          <div class="row" style="margin-top:30px;">
                            @php
                              if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                                $dir = public_path().'\\packages\\extensionsvalley\\emr\\templates';
                              } else {
                                $dir = public_path().'/packages/extensionsvalley/emr/templates';
                              }
                              $templatesUrlList = array_slice(scandir($dir), 2);
                              $templatesName = [];
                              if(sizeof($templatesUrlList) > 0){
                                  foreach($templatesUrlList as $url){
                                      if(!empty($url)){
                                         if(pathinfo($url, PATHINFO_EXTENSION) == 'php' || pathinfo($url, PATHINFO_EXTENSION) == 'html'){
                                              $templatesName['/packages/extensionsvalley/emr/templates/'.$url] = pathinfo($url, PATHINFO_FILENAME);
                                          }
                                      }
                                  }
                              }
                              $sel_Template = (isset($formDetails->template_url) && !empty($formDetails->template_url)) ? $formDetails->template_url : '';
                              $is_favorite_option = (isset($formDetails->is_favorite_option) && $formDetails->is_favorite_option == 1 ) ? 1 : 0;
                              $is_suggest_option = (isset($formDetails->is_suggest_option) && $formDetails->is_suggest_option == 1 ) ? 1 : 0;
                              $is_footer_note = (isset($formDetails->is_footer_note) && $formDetails->is_footer_note == 1 ) ? 1 : 0;
                              $footer_note = (!empty($formDetails->footer_note)) ? $formDetails->footer_note : '';
                              $form_name = (!empty($formDetails->name)) ? $formDetails->name : '';
                              $form_id = (!empty($formDetails->form_id)) ? $formDetails->form_id : '';
                            @endphp
                           <div class="col-md-12">

                              <div class="col-md-6">
                                  <div class="col-md-3"><label for="Form Name"> Form Name</label></div>
                                  <div class="col-md-9">
                                      <input type="text" name="form_name" id="form_name" value="{{$form_name}}" class="form-control" placeholder="Form Name">
                                      <input type="hidden" name="form_id" value="{{$form_id}}">
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="col-md-3"><label for="Form Name"> Template</label></div>
                                  <div class="col-md-9">
                                  {!! Form::select('template_name',$templatesName,$sel_Template, ['class' => 'form-control','placeholder' => 'Select Template','title' => 'Select Template Name', 'id' => 'template_name']) !!}
                                 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="col-md-3"><label for="Form Name"> Category</label></div>
                                  <div class="col-md-9">
                                     <?php
                                              $categorySelected = [];
                                              $categoryName = \DB::table('category_master')
                                                 ->WhereNull('deleted_at')
                                                 ->pluck('category_name','id');
                                            if(!empty($form_id)){
                                              $categorySelected = \DB::table('forms_category_list')
                                                 ->Where('form_id','=',$form_id)
                                                 ->pluck('category_id');
                                             }
                                          ?>
                                          {!! Form::select('category_name[]',$categoryName,$categorySelected, ['class' => 'js-data-example-ajax js-example-basic-multiple form-control', 'id' => 'category_name','multiple'=>'multiple']) !!}
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="col-md-3"><label for="Favorite"> Favorite (If Yes)</label></div>
                                  &nbsp;&nbsp;&nbsp; <input type="checkbox" @if($is_favorite_option == 1) checked @endif name="is_favorite_form" value="1" id="is_favorite_form">
                              </div>
                              <div class="col-md-6">
                                  <div class="col-md-3"><label for="Favorite"> Auto Suggest  (If Yes)</label></div>
                                  &nbsp;&nbsp;&nbsp; <input type="checkbox" @if($is_suggest_option == 1) checked @endif name="is_suggest_form" value="1" id="is_suggest_form">
                              </div>
                              <div class="col-md-6"></div>
                              <div class="col-md-6">
                                  <div class="col-md-4"><label for="Form Fotter"> Footer Note (If Yes)</label></div>
                                  <div class="col-md-1"> <input type="checkbox" @if($is_footer_note == 1) checked @endif name="is_footer_note" value="1" id="is_footer_note"> </div>
                                  <div class="col-md-7">
                                    <textarea name="footer_note" id="footer_note" class="form-control">{{$footer_note or ''}}</textarea>
                                  </div>
                              </div>

                            </div>

                          </div>

                          <div class="col-md-12">
                              <div class="col-md-11 padding_sm"></div>

                              <div class="col-md-1 padding_sm">
                                  <label for="">&nbsp;</label>
                                  <div class="clearfix"></div>
                                  {!! Form::submit('Save',['class' => 'btn btn-block light_purple_bg','id'=>'save_generate_form_items']) !!}
                              </div>
                          </div>

                          {!! Form::token() !!}
                          {!! Form::close() !!}

                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>

    <!-- /page content -->
@stop
@section('javascript_extra')
     <script type="text/javascript">
      $(document).ready(function() {
        // $('#menu_toggle').trigger('click');

        let data = JSON.parse(JSON.stringify({!!$groupedList!!}));
        if(data != '' && data != undefined){
          groupList = Object.keys(data);
        }

        $('.form_theadscroll').perfectScrollbar({
          wheelPropagation: true,
          minScrollbarLength: 30
        });
        $('.form_theadfix_wrapper').floatThead({
          position: 'absolute',
          scrollContainer: true
        });

        $(".js-data-example-ajax").select2({
              placeholder: 'Select Category...',
              width: '350px',
              allowClear: false,
              ajax: {
                  url: "{{url('forms/category-search')}}",
                  dataType: 'json',
                  data: function (params) {
                        var query = {
                          term: params.term,
                        }
                        return query;
                  },
                  processResults: function (data) {
                      return {
                          results: data
                      };
                  },
                  cache: true
              }
        });

      });


      var groupList = [];
      function selectFormItem(obj){
        var fieldId = $(obj).val();
        if(fieldId != ''){
         var ArrSelectedItemId = [];
         var AlredySel = [];
        //check already exist
        $('#ListFormSelElement tr').each(function(element){
            let hiddenItemId = $(this).find('input[name="hidden_form_field_item_id[]"]').val();
            if(hiddenItemId != '' && hiddenItemId != undefined){
                ArrSelectedItemId.push(hiddenItemId);
            }
        });

        AlredySel = $.inArray(fieldId,ArrSelectedItemId);
        if(AlredySel == -1){
            //no match found
        }else{
            Command: toastr["warning"]('Already Selected !.');
            $('input[type="checkbox"][name="field_item[]"][value='+fieldId+']').attr('checked',false);
            return false;
        }

        var fieldFieldLabel = $(obj).closest('tr').find('input[name="hidden_field_label[]"]').val();
        var fieldFieldName = $(obj).closest('tr').find('input[name="hidden_field_name[]"]').val();
        var fieldTypeName = $(obj).closest('tr').find('input[name="hidden_type_name[]"]').val();
        var fieldDefaultValue = $(obj).closest('tr').find('input[name="hidden_default_value[]"]').val();
            //remove selected
            $(obj).parent().parent().remove();

            //add to form
            var table = document.getElementById("form_generate_table_selected").getElementsByTagName('tbody')[0];
            var countRow = document.getElementById("form_generate_table_selected").rows.length - 1;
            if (countRow == 0) {
                 setIDval = 1;
            }

            var row = table.insertRow(countRow);
            var row_length = table.rows.length;
            if(row_length == 2){
                $('#form_generate_table_selected_no_data').remove();
            }
            row.title=fieldDefaultValue;
            var cell0 = row.insertCell(0);
            var cell1 = row.insertCell(1);
            var cell2 = row.insertCell(2);
            var cell3 = row.insertCell(3);
            var cell4 = row.insertCell(4);
            cell1.className = 'text-center set-option-width';
            cell2.className = 'text-center set-option-width';
            cell3.className = 'text-center set-option-width2';
            cell4.className = 'text-center set-option-width';
            cell0.innerHTML = '<input type="hidden" name="hidden_form_field_label[]" value="'+fieldFieldLabel+'"><input type="hidden" name="hidden_form_type_name[]" value="'+fieldTypeName+'"><input type="hidden" name="hidden_form_default_value[]" value="'+fieldDefaultValue+'"><input type="hidden" name="hidden_form_field_item_id[]" value="'+fieldId+'" ><input type="hidden" name="hidden_form_field_name[]" value="'+fieldFieldName+'"><span id="field_name">'+fieldFieldLabel+' ( '+fieldFieldName+' ) </span><span class="label label-primary">'+fieldTypeName+'</span>';
            cell1.innerHTML = '<input type="text" name="order[]" onblur="checkOrderVal(this)" class="form-control" >';
            cell2.innerHTML = '<input type="checkbox" data-optionname="'+fieldFieldLabel+'" data-optionval="'+fieldId+'"  class="is_group_header" name="is_group_header['+fieldId+']" value="1" >';
            cell3.innerHTML = '<select class="form-control"  name="group_header[]" ><option value="">Group Header</option></select>';
            cell4.innerHTML = '<a onclick="removeFormItem(this)" data-value="'+fieldId+'" style="cursor: pointer;"><i class="fa fa-minus"></i></a>';
        }

        if($('select[name="group_header[]"]:visible:first option').length > 0){
            $('select[name="group_header[]"]:visible:first option').map(function(ind,elem){
                if($(elem).attr('value') != ''){
                   $(cell6).children('select').append(elem);
                }
            });
        }

     }

     function removeFormItem(obj){
         var fieldsId = $(obj).attr('data-value');
         if(fieldsId != ""){
            var tr = $(obj).closest('tr');
            var FieldLabel = $(tr).find('input[name="hidden_form_field_label[]"]').val();
            var FieldName = $(tr).find('input[name="hidden_form_field_name[]"]').val();
            var TypeName = $(tr).find('input[name="hidden_form_type_name[]"]').val();
            var DefaultValue = $(tr).find('input[name="hidden_form_default_value[]"]').val();
            //remove selected
            $(obj).parent().parent().remove();

            let checked = $(tr).find('.is_group_header').is(':checked');
            if(checked == true){
                let optionval = $(tr).find('.is_group_header').attr('data-optionval');
                if(($.inArray(optionval,groupList) >= 0)){
                   let index = $.inArray(optionval,groupList);
                    groupList.splice(index, 1);
                    let select2 = $('select[name="group_header[]"]');
                    $(select2).find('option[value="'+optionval+'"]').remove();
                }
            }

            //add to Label List
            var table = document.getElementById("form_generate_table").getElementsByTagName('tbody')[0];
            var countRow = document.getElementById("form_generate_table").rows.length - 1;
            if (countRow == 0) {
                 setIDval = 1;
            }

            var row = table.insertRow(countRow);
            row.title=DefaultValue;
            var row_length = table.rows.length;
            var cell0 = row.insertCell(0);
            var cell1 = row.insertCell(1);
            cell0.className = 'text-center set-option-width';
            cell0.innerHTML = '<input type="checkbox" name="field_item[]" onclick="selectFormItem(this)"  value="'+fieldsId+'">';
            cell1.innerHTML = '<input type="hidden" name="hidden_field_label[]" value="'+FieldLabel+'"><input type="hidden" name="hidden_type_name[]" value="'+TypeName+'"><input type="hidden" name="hidden_default_value[]" value="'+DefaultValue+'"><input type="hidden" name="hidden_field_name[]" value="'+FieldName+'"><span id="field_name">'+FieldLabel+' ( '+FieldName+') </span><span class="label label-primary">'+TypeName+'</span>';


         }
     }

     function checkOrderVal(obj){
        var cur_order = $(obj).val();
        var status = $.isNumeric(cur_order);
        if(status == false){
            Command: toastr["warning"]('Please Enter a Number.');
            $(obj).val('');
            return false;
        }
     }

     $(document).on('change','.is_group_header',function(){
          //is group header checked
          let element = $(this);
          let groupHeader = $(element).closest('tr').find('select[name="group_header[]"]');
          let optionname = $(element).attr('data-optionname');
          let optionval = $(element).attr('data-optionval');
          if($(element).is(":checked")){
              $(groupHeader).hide();
              $(groupHeader).val('');
              if(optionname != '' && optionval != '' && optionname != undefined && optionval != undefined){
                  if(!($.inArray(optionval,groupList) >= 0)){
                      groupList.push(optionval);
                      let select2 = $('select[name="group_header[]"]');
                      $(select2).append("<option value='"+optionval+"'>"+optionname+"</option>");
                  }
              }
          }else{
              $(groupHeader).show();
              if(optionname != '' && optionval != '' && optionname != undefined && optionval != undefined){
                  if(($.inArray(optionval,groupList) >= 0)){
                     let index = $.inArray(optionval,groupList);
                      groupList.splice(index, 1);
                      let select2 = $('select[name="group_header[]"]');
                      $(select2).find('option[value="'+optionval+'"]').remove();
                  }
              }
          }
      });

     $(document).on('click','#save_generate_form_items',function(event) {
          event.preventDefault();
          var form_name = $('#form_name').val();
          var category_name = $('#category_name').val();
          var template_name = $('#template_name').val();

          if($('#is_favorite_form:checkbox:checked').length > 0){
            var is_favorite_form = 1;
          }else{
            var is_favorite_form = 0;
          }


          if(form_name == ""){
              Command: toastr["warning"]('Please fill Form Name.');
              return false;
          }
          if(category_name == ""){
              Command: toastr["warning"]('Please Select Category.');
              return false;
          }

            var val = $('#form_generate_table_selected tr').find('input[name="hidden_form_field_label[]"]').val();
            if(val == undefined){
                Command: toastr["warning"]('Please Select Minimum One Item.');
                return false;
            }

          $("#GenerateForm").submit();
     });

      $(document).on('change', '#tagName', function(event) {
        event.preventDefault();
        /* Act on the event */
              var sel = $("#tagName option:selected").val();
              if(sel == 'All'){
                  var url = "";
                  $.ajax({
                    type: "GET",
                    url: url,
                    data: "selected_tag=" + sel,
                    beforeSend: function () {
                    },
                    success: function (data) {
                      $('#list_item_container').html(data);
                    },
                    complete: function () {
                    }
                  });
              }else{
                  var url = "";
                  $.ajax({
                    type: "GET",
                    url: url,
                    data: "selected_tag=" + sel,
                    beforeSend: function () {
                    },
                    success: function (data) {
                      $('#list_item_container').html(data);
                    },
                    complete: function () {
                    }
                  });
              }
              $('.form_theadfix_wrapper').floatThead("reflow");
      });

      function searchFieldWise(obj){

          let search_string = $(obj).val();
          search_string = search_string.trim();
          if(search_string != undefined){
              var url = "";
                  $.ajax({
                    type: "GET",
                    url: url,
                    data: "search_string=" + search_string,
                    beforeSend: function () {
                    },
                    success: function (data) {
                      $('#list_item_container').html(data);
                    },
                    complete: function () {
                    }
                  });
          }

      }

      $(window).on('hashchange', function() {
          if (window.location.hash) {
              var page = window.location.hash.replace('#', '');
              if (page == Number.NaN || page <= 0) {
                  return false;
              }else{
                  getData(page);
              }
          }
      });

      $(document).ready(function(){
          $(document).on('click', '.pagination a',function(event){
              $('li').removeClass('active');
              $(this).parent('li').addClass('active');
              event.preventDefault();
              var myurl = $(this).attr('href');
             var page=$(this).attr('href').split('page=')[1];
             getData(page);
          });
      });

      function getData(page){
          var selected_tag=$('#tagName').val();
          var search_string=$('#fieldSearch').val();
          $.ajax({
              url: '?page=' + page+'&selected_tag='+selected_tag+'&search_string='+search_string,
              type: "get",
              datatype: "html",
              // beforeSend: function()
              // {
              //     you can show your loader
              // }
          })
          .done(function(data)
          {
              $("#list_item_container").empty().html(data);
              location.hash = page;
          })
          .fail(function(jqXHR, ajaxOptions, thrownError)
          {
                bootbox.alert('No response from server');
          });
      }

      $(document).on('click','#save_form_category',function(event) {
          event.preventDefault();

          $("#CategoryForm").submit();
      });
    </script>
@endsection
