@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<style>
    table > tbody > tr > td {
        text-align: left !important;
    }

    .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--multiple{
        min-height: 23px !important;
        border-color: #CCC !important;
    }
    .select2-container {
        width: 100% !important;
        padding: 0;
    }
    .select2-container .select2-selection--single {
        height: 23px !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow b{
        margin: -4px !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #444;
        line-height: 12px !important;
    }
    .modal .select2-container{
      z-index: 4000 !important;
      text-align: left !important;
      font-size: 13px !important;
    }
    .select2-container{
      text-align: left !important;
      font-size: 13px !important;
      box-shadow: 2px 3px 7px #e4e4e4;
    }
    .select2-container--open{
      z-index: 4000 !important;
    }

    .select2-container .select2-selection--single{
      height: auto !important;
      border-radius: 4px !important;
    }


     .row{
        margin-top:40px;
        padding: 0 10px;
    }
    .clickable{
        cursor: pointer;
    }

    .panel-heading div {
        margin-top: -18px;
        font-size: 15px;
    }
    .panel-heading div span{
        margin-left:5px;
    }
    /*.panel-body{
            display: none;
    }*/
    #ajaxSearchBox {
        display:none;
        text-align: left;
        list-style: none;
        cursor: pointer;
        max-height: 350px;
        margin: -2px 0px 0px 0px;
        overflow-y: auto;
        width: 89%;
        z-index: 599;
        position:absolute;
        background: #ffffff;
        border-radius: 3px;
        border: 1px solid rgba(0, 0, 0, 0.3);
    }

    .liHover{
         background: #cde3eb;
    }

    /*popup*/
  /*  .select_pop_up {
    left: 481px !important;
    }*/
    .pop_up {
    background: #DEECF9 none repeat scroll 0 0;
    border-radius: 6px;
    box-shadow: 1px 1px 3px #d0d0d0;
    padding: 10px;
    position: absolute;
    top: 30px;
    right:  -24px;
    /* max-width: 114%; */
    z-index: 850;
    width: 450px;
    }
    .main_table_heading {
        background: #07709B;
        color: #FFF;
        padding: 5px 2px;
        font-weight: bold;
    }
    .pop_up::before {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: rgba(194, 225, 245, 0) rgba(194, 225, 245, 0) #DEECF9 rgba(194, 225, 245, 0);
    border-image: none;
    border-style: solid;
    border-width: 16px;
    content: " ";
    height: 0;
    left: 74%;
    top: -30px;
    pointer-events: none;
    position: absolute;
    width: 0;
   }
   .pop_up_2 {
    background: #DEECF9 none repeat scroll 0 0;
    border-radius: 6px;
    box-shadow: 1px 1px 3px #d0d0d0;
    padding: 10px;
    position: absolute;
    top: 30px;
    right: -8px;
    /* max-width: 114%; */
    z-index: 850;
    width: 450px;
    }
   .pop_up_2::before {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: rgba(194, 225, 245, 0) rgba(194, 225, 245, 0) #DEECF9 rgba(194, 225, 245, 0);
    border-image: none;
    border-style: solid;
    border-width: 16px;
    content: " ";
    height: 0;
    left: 87%;
    top: -30px;
    pointer-events: none;
    position: absolute;
    width: 0;
   }
    .pos-relative{
        position: relative;
    }

    .pop_up_table {
    background: #DEECF9 none repeat scroll 0 0;
    border-radius: 6px;
    box-shadow: 1px 1px 3px #d0d0d0;
    padding: 10px;
    position: absolute;
    top: 30px;
    right:  -46px;
    /* max-width: 114%; */
    z-index: 850;
    width: 450px;
    }
   .pop_up_table::before {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: rgba(194, 225, 245, 0) rgba(194, 225, 245, 0) #DEECF9 rgba(194, 225, 245, 0);
    border-image: none;
    border-style: solid;
    border-width: 16px;
    content: " ";
    height: 0;
    left: 74%;
    top: -30px;
    pointer-events: none;
    position: absolute;
    width: 0;
   }

   .pop_top{
      top: inherit !important;
      bottom: 30px;
    }
    .showpoptable{
      display: block !important;
    }
    .hidepoptable{
      display: none !important;
    }
    .pop_container{
      position: relative !important;
    }

  .pop_closebtn {
    background: #333 none repeat scroll 0 0;
    border-radius: 50%;
    box-shadow: 0 0 5px #a2a2a2;
    color: #fff;
    cursor: pointer;
    font-size: 12px;
    font-weight: bold;
    height: 20px;
    padding: 2px 6px;
    position: absolute;
    right: -10px;
    top: -4px;
    width: 20px;
  }
  .pop table{
    font-size: 13px;
    margin-bottom: 0 !important;
  }
  .pop table tbody {
    display: table-header-group !important;
    height: auto !important;
  }
  .pop table thead {
    display: table-header-group !important;
  }
  .pop .table-striped > tbody > tr:nth-child(n+1){
   background: #F1F1F1;
  }
  .pop .table-striped > tbody > tr:nth-child(2n+1){
   background: #fff;
  }
  .pop table thead {
    background: #19a0d8 none repeat scroll 0 0;
    color: #fff;
  }

  .pop table thead th {
    background: #19a0d8 none repeat scroll 0 0 !important;
    font-weight: normal !important;
  }

</style>
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
@endsection
@section('content-area')

  <!-- page content -->
  <div class="right_col"  role="main">
  <div class="row codfox_container">

    <!-- title section -->
    <div class="col-md-12">
        <b>{{strtoupper($title)}}</b>
        <hr>
    </div>
    <!-- title section -->

    <!-- Mesage Container -->
    <div class="col-md-12">
        @if (\Session::has('successMsg'))
            <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Success!</strong> {{\Session::get("successMsg")}}.
            </div>
        @elseif (\Session::has('failureMsg'))
            <div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Error!</strong> {{\Session::get("failureMsg")}}.
            </div>
        @endif
    </div>
    <!-- Mesage Container -->


    <div class="col-md-12 padding_sm">
    <div class="box no-border">
    <div class="box-body clearfix">

    {!!Form::open(array('route' => 'extensionsvalley.forms.formfieldsave', 'method' => 'post','id'=>'AddFormFieldForm','files'=>true))!!}

      @php
        $type_selected = (!empty($data[0]->type)) ? $data[0]->type : "";
        $type_name_selected = (!empty($data[0]->type_name)) ? $data[0]->type_name : "";
        $id_selected = (!empty($data[0]->id)) ? $data[0]->id : "";
        $default_value_selected = (!empty($data[0]->default_value)) ? $data[0]->default_value : "";
        $is_required_selected = (!empty($data[0]->is_required)) ? $data[0]->is_required : 0;
        $field_name_selected = (!empty($data[0]->field_name)) ? $data[0]->field_name : "";
        $field_label_selected = (!empty($data[0]->field_label)) ? $data[0]->field_label : "";
        $placeholder_selected = (!empty($data[0]->placeholder)) ? $data[0]->placeholder : "";
        $field_height_selected = (!empty($data[0]->field_height)) ? $data[0]->field_height : "";
        $progress_table_selected = (!empty($data[0]->progress_table)) ? $data[0]->progress_table : "";
        $progress_value_selected = (!empty($data[0]->progress_value)) ? $data[0]->progress_value : "";
        $progress_text_selected = (!empty($data[0]->progress_text)) ? $data[0]->progress_text : "";
        $class_name_selected = (!empty($data[0]->class_name)) ? $data[0]->class_name : "";
        $table_details_selected = (!empty($data[0]->table_details)) ? json_decode($data[0]->table_details) : "";
        $table_structure_selected = (!empty($data[0]->table_structure)) ? json_decode($data[0]->table_structure) : "";
        $field_width_selected = (!empty($data[0]->field_width)) ? $data[0]->field_width : "col-md-12";
        $optionDetails = [];
        $rowsWithoutHead = "";
        $columnsWithoutHead = "";
        $rows = 0;
        $columns = 0;
        $rowHeader = 0;
        $colHeader = 0;
        $viewTablebtn = 0;
        $genTablebtn = 0;


      //edit mode
      if(!empty($id_selected)){

        $types = [$type_selected => $type_name_selected];

        //Radio Button
        if($type_name_selected == "RADIO BUTTON"){
            $optionDetails = ExtensionsValley\Emr\FormFieldRadioOptions::where('form_field_id',$id_selected)
            ->get();
        }elseif($type_name_selected == "GROUPED CHECKBOX"){
            $optionDetails = ExtensionsValley\Emr\FormFieldGroupedCheckboxOptions::where('form_field_id',$id_selected)
            ->get();
        }else {
            //Select Box
            $optionDetails = ExtensionsValley\Emr\FormFieldSelectboxOptions::where('form_field_id',$id_selected)
              ->get();

        }

        //Table Structure
        if($type_name_selected == "TABLE"){

            $table_details = $table_details_selected->$field_name_selected;
            $table_structure = $table_structure_selected->$field_name_selected;
            //no rows
            $rowsWithoutHead = (isset($table_details->rows))? $table_details->rows : 0;
            $columnsWithoutHead = (isset($table_details->columns))? $table_details->columns : 0;
            $rows = (isset($table_details->rows))? $table_details->rows : 0;
            $columns = (isset($table_details->columns))? $table_details->columns : 0;
            $rowHeader = (isset($table_details->rowHeader))? $table_details->rowHeader : 0;
            $colHeader = (isset($table_details->colHeader))? $table_details->colHeader : 0;
            if($rowHeader == 1){
                ++$columns;
            }
            if($colHeader == 1){
                ++$rows;
            }

            if($rows > 0 && $columns > 0){
              $viewTablebtn = 1;
              $genTablebtn = 1;
            }

        }
      }
      @endphp

        <div class="row" style="margin-top:0;margin-top:0px;">
            <div class="col-md-12" style="float: right;margin-bottom: 10px;">
                  <b>Note</b> :   <a style="cursor: pointer;" title="Show" onclick="showHidenotes('show')"><i class="fa fa-caret-square-o-down"></i></a>
                  <a style="cursor: pointer;" title="Hide" onclick="showHidenotes('hide')"><i class="fa fa-caret-square-o-up"></i></a>
                  <div id="notes" class="sr-only">
                      <br> <i><i class="fa fa fa-info-circle"></i> Field name must be unique. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating date picker add class <b>date_picker</b> to a text box. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating datetime picker add class <b>date_time_picker</b> to a text box. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating time picker add class <b>time_picker</b> to a text box. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating progressive search set <b>table name</b> for listing data is sets on <b>master table</b> field. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating progressive search when user viewing data field name enter on <b>text</b> field and database saving data field name enter on <b>value</b> field.  </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating imo icd search add class <b>imo_icd</b> to a progressive Search field. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating imo cpt search add class <b>imo_cpt</b> to a progressive Search field. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating Prepopulating Doctor name add label <b>doctor</b> to the default value field. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating Prepopulating Doctor Speciality add label <b>doctor_speciality</b> to the default value field. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Prepopulating Patient Details add label to the default value field based on the fields created in patient table. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating stroke score add class <b>strokescore</b> to a Radio button. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating stroke score sum add class <b>scoresum</b> to a Text box. </i>
                      <br> <i><i class="fa fa fa-info-circle"></i> Creating certificate under `Certificate` Category.When Creating Certificate use Placeholders to get Patient Details  <b><?php echo "{{patient@opNo}}"; ?></b>,<b><?php echo "{{patient@name}}"; ?></b>,<b><?php echo "{{patient@address}}"; ?></b>,<b><?php echo "{{patient@mobile}}"; ?></b>,<b><?php echo "{{patient@gender}}"; ?></b>,<b><?php echo "{{patient@age}}"; ?></b> </i>
                      <br><i><i class="fa fa fa-info-circle"></i> To calculate LMP date using EDC date by adding class <b>obstetic_lmp_date</b>,<b>obstetic_edc_date</b> respectively for both fields. </i>
                      <br><i><i class="fa fa fa-info-circle"></i> To specify custom width to table column by adding class <b>catm-table-col-{column}-{width}</b>,Ex :<u>catm-table-col-1-2</u> (width 2 = 20%). </i>
                       <br><i><i class="fa fa fa-info-circle"></i> To add parent element wise hide & show child elements to set class as parent <b>catm-parent</b> and add another class of the child element <b>catm-child-{specific key}</b>. Then set child element with calss <b>catm-child</b> and add class name of the child element (specified in the parent class). <b>catm-child-{specific key}</b> Ex : for parent class - <u>catm-parent catm-child-element1</u> , for child class - <u>catm-child catm-child-element1</u> (parent element must be radio button or grouped checkbox.in radio button work based on yes or no and in grouped checkbox based on field value as other ) </i>
                  </div>
              </div>
           <div class="col-md-6">
               <input type="hidden" name="fieldnamelist" id="fieldnamelist" value="{{$formFieldItemsNames}}">
                <div class="col-md-2 text-center">Tag</div>
                <div class="col-md-8" style="padding: 1px;">
                    <select class="search-tag-to-add11 form-control" multiple1 name="tagName[]" placeholder="Select Tag Here " id="tagName"   >
                    <option value="">Select</option>
                    @if(count($tagList) > 0)
                    @foreach($tagList as $name=>$value)
                    <option value="{{$value}}">{{$name}}</option>
                    @endforeach
                    @endif
                    @if(count($tagListSelected) > 0)
                    @foreach($tagListSelected as $name=>$value)
                    <option selected value="{{$value}}">{{$name}}</option>
                    @endforeach
                    @endif
                    </select>
                </div>
                <div class="col-md-2" style="padding: 1px;">
                    <a style="cursor: pointer;" onclick="addTag();" class="btn btn-sm btn-default" onclick="" title="Add Tag"><i class="fa fa-plus"></i></a>
                </div>
            </div>
           <div class="col-md-6 text-right">
            @if(empty($id_selected))
                <button type="button" class="btn btn-sm light_purple_bg pull-right" onclick="add_form_details(this);" data-toggle="tooltip" data-placement="left" title="" data-original-title="Add Form Details"><i class="fa fa-plus fa-lg"></i></button>
            @endif
           </div>
        </div>

        <div class="clearfix"></div>
        <div class="h10"></div>
        <div class="form_theadscroll always-visible" style="height: 300px;padding-right:10px;position: relative;">
            <table id="form_builder_table" class="table table-bordered table_sm table-striped table-condensed form_theadfix_wrapper doctor_leave_table form_builder_table">
                <thead>
                    <tr class="table_header_bg">
                        <th>Type</th>
                        <th width="8%" class="set-class" >Class</th>
                        <th width="20%">Default Value</th>
                        <th width="5%">Mandatory</th>
                        <th width="5%">Dropdown</th>
                        <th>Field Name</th>
                        <th>Field Label</th>
                        <th width="9%">Placeholder</th>
                        <th width="9%">Height / Width</th>
                        <th colspan="2">Master Table</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr_clone">
                        <td>
                           {!! Form::select('type[]', $types, "", ['class' => 'form-control','id' => 'type', 'style' => 'color:#555555; padding:4px 12px;','id'=>'type_#0']) !!}

                        </td>
                       <td class="set-class text-center" >
                       <a class="add_class_btn" title="set Class Name For The Field" style="cursor: pointer;line-height: 25px;"><i class="fa fa-plus"></i></a>
                          <input type="hidden" name="class_name[]" autocomplete="off"  value="{{$class_name_selected}}" class="form-control" id="class_name" placeholder="">
                       </td>
                       <td id="default_value_field-0">
                         <input type="text" list="prepopdata" name="default_value[]" class="form-control" id="default_value-0" value="{{$default_value_selected}}" >
                          <datalist id="prepopdata">
                          </datalist>
                          @if($type_name_selected == "TINYMCE" || $type_name_selected == "CERTIFICATE")
                          <a class="btn btn-default" onClick="loadDefaultValModal('0')" style="float:right;"><i class="fa fa-file-code-o"></i></a>
                          @endif
                       </td>
                        <td>
                       {!! Form::select('mandatory[]', [0=>'No',1=>'Yes'], $is_required_selected, ['class' => 'form-control','id' => 'mandatory', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                       </td>
                        <td class="td_drop_down_vals" style="text-align: center;position: relative;">
                           <input type="button" class="btn btn-sm btn-primary light_purple_bg drop_dwn_val" @if($type_name_selected == "SELECT BOX" || $type_name_selected == "RADIO BUTTON" || $type_name_selected == "GROUPED CHECKBOX") @else disabled @endif value="+" />
                           <!-- Option values popup start -->
                            <div class="pop_up select_pop_up sr-only" style="width: 400px;">
                            <div class="main_table_heading"> Select Box Fields
                            <a onclick="addSelectOptionFields(0)" style="font-size: 18px;color: white;cursor: pointer;float: right;padding-right: 5px;line-height: 17px;">+</a></div>
                            <div class="pop_closebtn pop_closebtnfield">X</div>
                                <div class="">
                                    <table class="table table-responsive table-striped " id="selectFieldValPop0" style="margin-bottom:0px;">
                                    <thead>
                                        <tr class="fixtr">
                                            <th title="Text">Text</th>
                                            <th title="Value">Value</th>
                                        </tr>
                                     </thead>
                                     <tbody>
                                     @if(count($optionDetails) > 0)
                                       @foreach($optionDetails as $data)
                                         <tr class="fixtr" >
                                             <td>
                                             <input type="text" class="form-control" tabindex="1" name="select_field_text[0][]" value="{{ $data->name }}" id="select_field_text_#0" title="Field Text" placeholder="Option Text">
                                             </td>
                                             <td>
                                             <input type="text" class="form-control" tabindex="2" value="{{ $data->value }}" name="select_field_value[0][]" id="select_field_value_#0" title="Field Value" placeholder="Option Value">
                                             </td>

                                         </tr>
                                       @endforeach
                                     @else
                                        <tr class="fixtr" >
                                           <td>
                                           <input type="text" class="form-control" tabindex="1" name="select_field_text[0][]" value="" id="select_field_text_#0" title="Field Text" placeholder="Option Text">
                                           </td>
                                           <td>
                                           <input type="text" class="form-control" tabindex="2" value="" name="select_field_value[0][]" id="select_field_value_#0" title="Field Value" placeholder="Option Value">
                                           </td>
                                        </tr>
                                     @endif
                                     </tbody>
                                     </table>
                                 </div>
                             </div>
                            <!-- Option values popup end -->
                            <!-- Table popup start -->
                              <input type="button" style="float: center;" class="btn btn-sm btn-primary light_purple_bg table_generate" @if(isset($type_name_selected) && $type_name_selected == "TABLE") @else disabled @endif value="+" />
                                <div class="pop_up_table table_pop_up sr-only" style="width: 400px;">
                                    <div class="pop_closebtn pop_closebtnfield_table">X</div>
                                    <div class="">
                                        <table class="table table-responsive table-striped " style="margin-bottom:0px;">
                                        <thead>
                                            <tr class="fixtr">
                                                <th title="Text">Rows</th>
                                                <th title="Value">Columns</th>
                                            </tr>
                                         </thead>
                                         <tbody>
                                         <tr class="fixtr">
                                             <td>
                                             <input type="text" class="form-control" tabindex="-1" name="rows[]" value="{{$rowsWithoutHead}}" id="" title=" Rows" placeholder="Rows" onkeyup="enableGenerateTableBtn(this)">
                                             </td>
                                             <td>
                                             <input type="text" class="form-control" tabindex="-1" value="{{$columnsWithoutHead}}" name="columns[]" id="" title=" Columns" placeholder="Columns" onkeyup="enableGenerateTableBtn(this)">
                                             </td>
                                         </tr>
                                         </tbody>
                                         </table>
                                         <div style="margin-top: 5px;">
                                             <input type="button" class="btn btn-sm btn-primary light_purple_bg" name="genTable[]" value="Generate" @if($genTablebtn == 1)  @else disabled @endif onclick="genTableRowColWise(this,0)">
                                             <input type="button" class="btn btn-sm btn-primary light_purple_bg" name="viewTable[]" value="View Table" @if($viewTablebtn == 1)  @else disabled @endif onclick="viewTableRowColWise(this,0)">
                                             &nbsp;
                                             Row Header : <input @if($rowHeader == 1) checked @endif style="line-height: 25px;" type="checkbox" name="rowHeader[0]" value="1">
                                             &nbsp;
                                             Column Header : <input @if($colHeader == 1) checked @endif style="line-height: 25px;" type="checkbox" name="colHeader[0]" value="1">
                                         </div>
                                     </div>
                                 </div>
                            <!-- Table popup end -->
                            <!-- Modal Table Structure -->
                            <div id="tableStruct-0" class="modal fade" role="dialog">
                              <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Table</h4>
                                  </div>
                                  <div class="modal-body">
                                      <table id="tableGen-0" class="table table-responsive table-striped ">
                                          @for($m = 0; $m < $rows; $m++)
                                              <tr>
                                              @for($n = 0; $n < $columns; $n++)
                                                  <?php
                                                   $fieldSelValue = "";

                                                   $rwName = 'rows_'.$m;
                                                   $fieldNm = $field_name_selected.'_row_'.$m.'[]';

                                                   /* pre set datas(table generate) fetch */
                                                   if(isset($table_structure->$rwName) && count($table_structure->$rwName) > 0){
                                                      $row_data_arr = $table_structure->$rwName;
                                                      $fieldSelValue = (isset($row_data_arr[$n])) ? $row_data_arr[$n] : '';
                                                   }
                                                   ?>
                                                  <td>
                                                      <input type="text" class="form-control" name="{{$fieldNm}}" value="{{$fieldSelValue}}">
                                                  </td>
                                              @endfor
                                              </tr>
                                          @endfor
                                      </table>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                                </div>

                              </div>
                            </div>
                            <!-- Modal Table Structure ends -->
                      </td>
                        <td>
                            <input type="text" name="field_name[]" onkeyup="changeTablenames(this,0)" value="{{$field_name_selected}}" class="form-control" autocomplete="off" id="field_name" placeholder="" >
                        </td>
                         <td>
                            <input type="text" name="field_label[]" value="{{$field_label_selected}}" class="form-control" autocomplete="off" id="field_label" placeholder="">
                        </td>
                        <td>
                            <input type="text" name="place_holder[]" value="{{$placeholder_selected}}" class="form-control" autocomplete="off" id="place_holder" placeholder="">
                        </td>
                         <td>
                            <input type="text" name="text_area_height[]" value="{{$field_height_selected}}" maxlength="4" style="width:45px;float:left" class="form-control" autocomplete="off" id="text_area_height" @if(isset($type_name_selected) && $type_name_selected != "TEXT AREA") readonly @endif placeholder="">
                            {!! Form::select('field_area_width[]', $widthList, $field_width_selected, ['id'=>'field_area_width','class'=>'form-control','placeholder'=>'','style'=>'width:60px;float:right']) !!}
                        </td>
                        <td width="10%" style="position: relative;" >
                            <input type="text" style="width: 80%;float: left;" name="master_table[]" value="{{$progress_table_selected}}" class="form-control" autocomplete="off" id="master_table"  @if(isset($type_name_selected) && $type_name_selected != "PROGRESSIVE SEARCH") readonly @endif  placeholder="">
                            <input type="button" style="float: right;" class="btn btn-sm btn-primary light_purple_bg master_table_text_val" @if(isset($type_name_selected) && $type_name_selected != "PROGRESSIVE SEARCH") disabled @endif value="+" />
                            <div class="pop_up_2 select_pop_up_2 sr-only" style="width: 400px;">
                            <div class="pop_closebtn pop_closebtnfield_2">X</div>
                                <div class="">
                                    <table class="table table-responsive table-striped " style="margin-bottom:0px;">
                                    <thead>
                                        <tr class="fixtr">
                                            <th title="Text">Text</th>
                                            <th title="Value">Value</th>
                                        </tr>
                                     </thead>
                                     <tbody>
                                     <tr class="fixtr">
                                         <td>
                                         <input type="text" class="form-control" tabindex="-1" name="master_table_text[]" value="{{$progress_text_selected}}" id="" title=" Text" placeholder="Text">
                                         </td>
                                         <td>
                                         <input type="text" class="form-control" tabindex="-1" value="{{$progress_value_selected}}" name="master_table_value[]" id="" title=" Value" placeholder="Value">
                                         </td>
                                     </tr>
                                     </tbody>
                                     </table>
                                 </div>
                             </div>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                      </tr>
                </tbody>
            </table>
        </div>



        <input type="hidden" name="update_id" value="{{$id_selected}}" >
        <div class="row" style="text-align:center">
            <div class="col-md-11"></div>
            <div class="col-md-1">
              {!! Form::submit('Save',['class' => 'btn btn-block light_purple_bg','id'=>'save_form_labels']) !!}
            </div>
        </div>

    {!! Form::token() !!}
    {!! Form::close() !!}

    <!-- Modal Default Value -->
    <div id="defaultValueModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md" style="width: 1200px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Default Value</h4>
            </div>
            <div class="modal-body">
                <textarea name="default_value_tinymce" id="default_value_tinymce" class="form-control texteditor" autocomplete="off"></textarea>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="setDefVal()"  data-dismiss="modal">Add</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="hidden" name="add_def_val_attr_id" id="add_def_val_attr_id" value="">
            </div>
        </div>

        </div>
    </div>
    <!-- Modal Default Value ends -->

  </div>
  </div>
  </div>


  </div>
</div>


@endsection

@section('javascript_extra')
<!-- Tiny Mce Editor JavaScript -->
<script src="{{asset("packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script type="text/javascript">

 toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }

$(document).on('click', '.add_class_btn', function(event) {
    event.preventDefault();
    /* Act on the event */
    $(this).next('input[type="hidden"]').attr('type', 'text');
    $(this).hide();
    $(".theadfix_wrapper").floatThead('reflow');
    //console.log(obj);
});
$(document).on('click', '.pop_closebtnfield', function(event) {
    event.preventDefault();
    /* Act on the event */
    $(this).closest('tr').find('.select_pop_up').addClass('sr-only');
});
$(document).on('click', '.drop_dwn_val', function(event) {
    event.preventDefault();
    /* Act on the event */
    $(this).closest('tr').find('.select_pop_up').removeClass('sr-only');
});
$(document).on('click', '.pop_closebtnfield_2', function(event) {
    event.preventDefault();
    /* Act on the event */
    $(this).closest('tr').find('.select_pop_up_2').addClass('sr-only');
});
$(document).on('click', '.master_table_text_val', function(event) {
    event.preventDefault();
    /* Act on the event */
    $(this).closest('tr').find('.select_pop_up_2').removeClass('sr-only');
});
$(document).on('click', '.table_generate', function(event) {
    event.preventDefault();
    /* Act on the event */
    $(this).closest('tr').find('.table_pop_up').removeClass('sr-only');
    if($(this).closest('tr').find('input[name="field_name[]"]').val() == null){
        $(this).closest('tr').find('input[name="field_name[]"]').focus();
    }
});
$(document).on('click', '.pop_closebtnfield_table', function(event) {
    event.preventDefault();
    /* Act on the event */
    $(this).closest('tr').find('.table_pop_up').addClass('sr-only');
});
$(document).on('change', 'select[name="type[]"]', function(event) {
    event.preventDefault();
    /* Act on the event */
    var selected = $(this).val();
    var selected_text = $(this).find("option:selected").text();
    var selectedId = $(this).attr('id');
    //case text box
    if(selected_text == "TEXT" || selected_text == "DATE"){

    }else{

    }
    //case select box or multi select
    if(selected_text == "SELECT BOX"){
        $(this).closest('tr').find('.drop_dwn_val').attr('disabled', false);
        $(this).closest('tr').find('.table_generate').attr('disabled', true);
    }else{
        $(this).closest('tr').find('.drop_dwn_val').attr('disabled', true);
    }
    //case progressive search
    if(selected_text == "PROGRESSIVE SEARCH"){
        $(this).closest('tr').find('input[name="master_table[]"]').attr('readonly', false);
        $(this).closest('tr').find('.master_table_text_val').attr('disabled', false);
    }else{
        $(this).closest('tr').find('input[name="master_table[]"]').attr('readonly', true);
        $(this).closest('tr').find('input[name="master_table[]"]').val('');
        $(this).closest('tr').find('.master_table_text_val').attr('disabled', true);
    }
    //case radio button
    if(selected_text == "RADIO BUTTON"){
        $(this).closest('tr').find('.drop_dwn_val').attr('disabled', false);
        $(this).closest('tr').find('.table_generate').attr('disabled', true);
    }
    //case table
    if(selected_text == "TABLE"){
        $(this).closest('tr').find('.table_generate').attr('disabled', false);
        $(this).closest('tr').find('.drop_dwn_val').attr('disabled', true);
    }
    //case grouped checkbox
    if(selected_text == "GROUPED CHECKBOX"){
        $(this).closest('tr').find('.drop_dwn_val').attr('disabled', false);
        $(this).closest('tr').find('.table_generate').attr('disabled', true);
    }

    //case text area
    if(selected_text == "TEXT AREA"){
     $(this).closest('tr').find('input[name="text_area_height[]"]').attr('readonly', false);
    }else{
     $(this).closest('tr').find('input[name="text_area_height[]"]').attr('readonly', true);
    }
    //when textarea or tinyMce
    var id = selectedId.replace('type_#', '');
    var fieldDef = '';
    if(selected_text == "TINYMCE" || selected_text == "TEXT AREA"){
        if(selected_text == "TINYMCE"){
            var fieldDef = '<textarea name="default_value[]"  class="form-control" autocomplete="off" id="default_value-'+id+'" placeholder=""></textarea><a class="btn btn-default" onClick="loadDefaultValModal(\''+id+'\')" style="float:right;"><i class="fa fa-file-code-o"></i></a>';
        }else{
            var fieldDef = '<textarea name="default_value[]"  class="form-control" autocomplete="off" id="default_value-'+id+'" placeholder=""></textarea>';
        }
    }else{
        var fieldDef = '<input type="text" name="default_value[]" value="" list="prepopdata" class="form-control" autocomplete="off" id="default_value-'+id+'" placeholder=""><datalist id="prepopdata"></datalist>';
    }
    $('#default_value_field-'+id).html(fieldDef);



     //patient details case
    if(selected_text == "HEADER TEXT" && selected_text != "CERTIFICATE"){
        var patientdetailslist = {
            'first_name|last_name':'Patient Name',
            'doctor':'Doctor Name',
            'doctor_speciality':'Doctor Speciality',
            'patient_id':'Op Number',
            'mobile_no':'Mobile',
            'gender':'Gender',
            'dob':'Date Of Birth',
            'address':'Address',
        };
        $('#prepopdata').html("");
        $.each( patientdetailslist, function( i, val ) {
            if(val != ''){
              addVitalDataList(i,val);
            }
        });

    }

    if(selected_text != "HEADER TEXT" && selected_text != "CERTIFICATE"){
        $('#prepopdata').html("");
    }
});

function showHidenotes(type){
    if(type !== '' && type !== undefined && type == 'hide'){
        $('#notes').addClass('sr-only');
    }else{
        $('#notes').removeClass('sr-only');
    }
}

$(document).on('click', '#save_form_labels', function(event) {
    event.preventDefault();
    /* Act on the event */
   var status = true;
   var dropdwnstatus = true;
   var mastertable = true;
   var structuretable = true;
   var elm_def_val_status = true;
   var statustag = true;

   $('select[name="type[]"]').map(function(index, type) {
        var elmtype = $(type).val();
        var elmtype_text = $(type).find("option:selected").text();
        var elmtypeId = $(type).attr('id');
        var id = elmtypeId.replace('type_#', '');
        //check if dropdown or radio , groped checkbox
        if(elmtype_text == "SELECT BOX" || elmtype_text == "RADIO BUTTON" || elmtype_text == "GROUPED CHECKBOX"){
            //CHECK 1ST CHECKBOX EMPTY OR NOT
            var elm1 = $('input[name="select_field_text['+id+'][]"]').val();
            var elm2 = $('input[name="select_field_value['+id+'][]"]').val();
            if(elm1 == "" || elm2 == ""){
                dropdwnstatus = false;
            }
        }
        //progressive search
        if(elmtype_text == "PROGRESSIVE SEARCH"){
            var elm11 = $(type).closest('tr').find('input[name="master_table_text[]"]').val();
            var elm22 = $(type).closest('tr').find('input[name="master_table_value[]"]').val();
            var elmmnt = $(type).closest('tr').find('input[name="master_table[]"]').val();

            if(elm11 == "" || elm22 == "" || elmmnt == ""){
                mastertable = false;
            }
        }
        //check if table
        if(elmtype_text == "TABLE"){
           var elm_row = $(type).closest('tr').find('input[name="rows[]"]').val();
           var elm_col = $(type).closest('tr').find('input[name="columns[]"]').val();
           if(elm_row == "" || elm_col == ""){
                structuretable = false;
            }
        }
        if(elmtype_text == "HEADER TEXT"){
           var elm_def_val = $(type).closest('tr').find('input[name="default_value[]"]').val();
            var res = elm_def_val.indexOf(' ') >= 0;
            if(res == true){
               elm_def_val_status = false;
            }
            if(elm_def_val == '' && elm_def_val == 'id'){
                elm_def_val_status = false;
            }
        }
        if(elmtype_text == ""){
            status = false;
        }
    });

   $('select[name="tagName[]"]').map(function(index1, tag) {
        var elmttag = $(tag).val();
        if(elmttag == null){
            statustag = false;
        }
   });

    if(statustag == false){ Command: toastr["warning"]('Please Select Tag.'); return false; }
    if(status == false){ Command: toastr["warning"]('Please fill all Type.'); return false; }
    if(dropdwnstatus == false){ Command: toastr["warning"]('Please fill Minimum One Dropdown Values.'); return false; }
    if(mastertable == false){ Command: toastr["warning"]('Please fill Master Table Fields.'); return false; }
    if(structuretable == false){ Command: toastr["warning"]('Please fill Table Fields.'); return false; }
    if(elm_def_val_status == false){ Command: toastr["warning"]("Space Not Allowd..! In Default Value Field When Type Is Patient Details or Empty Field"); return false; }

    var fieldnamelist = $('#fieldnamelist').val();
    var fieldNameExist = true;

    $('input[name="field_name[]"]').map(function(index, fieldname) {
        var elmfieldname = $(fieldname).val();
        var check = $.inArray(elmfieldname,JSON.parse(fieldnamelist));

        if(check != '-1'){
            fieldNameExist = false;
        }
        if(elmfieldname == ""){
            status = false;
        }
    });
    if(status == false){ Command: toastr["warning"]('Please fill all Field Names.'); return false; }
    if(fieldNameExist == false){ Command: toastr["warning"]('Field Name Already Exists.'); return false; }

    $('input[name="text_area_height[]"]').map(function(ind,field){
        var fieldVal = $(field).val();
        if(isNaN(fieldVal) == true){
            status = false;
        }
    });
    if(status == false){ Command: toastr["warning"]('Area Height Must Be a Number.'); return false; }

    if(status == true){
         $("#AddFormFieldForm").submit();
    }
});

function addSelectOptionFields(IDval){
        var tableDt = document.getElementById("selectFieldValPop"+IDval).getElementsByTagName('tbody')[0];
        var countRowDt = document.getElementById("selectFieldValPop"+IDval).rows.length - 1;
        if (countRowDt == 0) {
             setIDvalDt = 1;
        }
        else {
            /*var lastChildSelFieldIdDt = $('input[name="select_field_text['+setIDval+'][]"]').last().attr('id');
            var replcedFieldIDDt = lastChildSelFieldIdDt.replace('select_field_text_#', '');
            setIDvalDt = parseInt(replcedFieldIDDt) + 1;*/
        }
        var rowDt = tableDt.insertRow(countRowDt);
        var row_length = tableDt.rows.length;
        var fid = row_length-1;
        var cellDt0 = rowDt.insertCell(0);
        var cellDt1 = rowDt.insertCell(1);
        cellDt0.innerHTML = '<input type="text" class="form-control" tabindex="'+(fid * 2+1)+'" name="select_field_text['+IDval+'][]" value="" id="select_field_text_#'+fid+'" title="Field Text" placeholder="Option Text">';
        cellDt1.innerHTML = '<input type="text" class="form-control" tabindex="'+(fid * 2+2)+'" value="" name="select_field_value['+IDval+'][]" id="select_field_value_#'+fid+'" title="Field Value" placeholder="Option Value">';
}

function add_form_details(obj){

            var table = document.getElementById("form_builder_table").getElementsByTagName('tbody')[0];
            var countRow = document.getElementById("form_builder_table").rows.length - 1;

            showHidenotes('hide');

            if(countRow > 30){
                Command: toastr["warning"]('Maximum Rows Exceeded..!');
                return false;
            }

            if (countRow == 0) {
                setIDval = 1;
            }
            else {
                var lastChildTypeId = $('select[name="type[]"]').last().attr('id');
                var replcedFieldID = lastChildTypeId.replace('type_#', '');
                setIDval = parseInt(replcedFieldID) + 1;
            }

            var row = table.insertRow(countRow);
            var row_length = table.rows.length;
            var j = row_length-1;

            var cell0 = row.insertCell(0);
            var cell1 = row.insertCell(1);
            var cell2 = row.insertCell(2);
            var cell3 = row.insertCell(3);
            var cell4 = row.insertCell(4);
            var cell5 = row.insertCell(5);
            var cell6 = row.insertCell(6);
            var cell7 = row.insertCell(7);
            var cell8 = row.insertCell(8);
            var cell9 = row.insertCell(9);
            var cell10 = row.insertCell(10);

            cell1.className = 'text-center';
            cell2.setAttribute('id','default_value_field-'+setIDval);
            cell4.className = 'text-center pos-relative';
            cell10.className = 'text-center pos-relative';

            //clone the first tr td select box
            attrNewId = 'type_#'+setIDval;
            var cloneData = $("#form_builder_table").find('tbody tr:first td:first select');
            var newSelect = cloneData.clone().appendTo(cell0);
            newSelect.attr('id',attrNewId);

            cell1.innerHTML = '<a class="add_class_btn" title="set Class Name For The Field" style="cursor: pointer;line-height: 25px;"><i class="fa fa-plus"></i></a><input type="hidden" name="class_name[]" autocomplete="off"  value="" class="form-control" id="class_name" placeholder=""> ';
            cell2.innerHTML = '<input type="text" name="default_value[]" value="" class="form-control" autocomplete="off" id="default_value-'+setIDval+'" placeholder=""><datalist id="prepopdata"></datalist>';
            cell3.innerHTML = '<select class="form-control" id="mandatory" style="color:#555555; padding:4px 12px;" name="mandatory[]"><option value="0">Yes</option><option value="1">No</option></select>';
            cell4.innerHTML = '<input type="button" class="btn btn-sm btn-primary drop_dwn_val" disabled="" value="+">'+
                                '<div class="pop_up select_pop_up sr-only" style="width: 400px;">'+
                                '<div class="main_table_heading"> Select Box Fields  '+
                                '<a onclick="addSelectOptionFields('+setIDval+')" style="font-size: 18px;color: white;cursor: pointer;float: right;padding-right: 5px;line-height: 17px;">+</a></div>  '+
                                '<div class="pop_closebtn pop_closebtnfield">X</div> '+
                                        '<table class="table table-responsive table-striped " id="selectFieldValPop'+j+'" style="margin-bottom:0px;"> '+
                                        '<thead><tr class="fixtr"> <th title="Text">Text</th>'+
                             '<th title="Value">Value</th></tr></thead><tbody><tr class="fixtr"><td>'+
                             '<input type="text" class="form-control" tabindex="-1" name="select_field_text['+setIDval+'][]" value="" id="select_field_text_#'+j+'" title="Field Text" placeholder="Option Text">'+
                             '</td><td>'+
                            '<input type="text" class="form-control" tabindex="-1" value="" name="select_field_value['+setIDval+'][]" id="select_field_value_#'+j+'" title="Field Value" placeholder="Option Value"></td></tr></tbody></table></div>&nbsp;<input type="button" style="float: center;" class="btn btn-sm btn-primary table_generate" disabled value="+" />'+
                                    '<div class="pop_up_table table_pop_up sr-only" style="width: 400px;">'+
                                        '<div class="pop_closebtn pop_closebtnfield_table">X</div>'+
                                    '<div class=""><table class="table table-responsive table-striped " style="margin-bottom:0px;">'+
                                            '<thead><tr><th title="Text">Rows</th><th title="Value">Columns</th></tr>'+
                                             '</thead><tbody><tr><td>'+
                                                 '<input type="text" class="form-control" tabindex="-1" name="rows[]" onkeyup="enableGenerateTableBtn(this)" value="" id="" title=" Rows" placeholder="Rows">'+
                                                 '</td><td>'+
                                                 '<input type="text" class="form-control" tabindex="-1" value="" name="columns[]" onkeyup="enableGenerateTableBtn(this)" id="" title=" Columns" placeholder="Columns">'+
                                                 '</td></tr></tbody></table>'+
                                             '<div style="margin-top: 5px;">'+
                                                 '<input type="button" class="btn btn-sm btn-primary" name="genTable[]" value="Generate" disabled onclick="genTableRowColWise(this,'+setIDval+')">'+
                                                 '&nbsp;Row Header : <input style="line-height: 25px;" type="checkbox" name="rowHeader['+setIDval+']" value="1">'+
                                                 '&nbsp;Column Header : <input style="line-height: 25px;" checked type="checkbox" name="colHeader['+setIDval+']" value="1">'+
                                            '</div></div></div><div id="tableStruct-'+setIDval+'" class="modal fade" role="dialog">'+
                                  '<div class="modal-dialog"><div class="modal-content"><div class="modal-header">'+
                                  '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
                                    '<h4 class="modal-title">Table</h4></div><div class="modal-body">'+
                                    '<table id="tableGen-'+setIDval+'" class="table table-responsive table-striped "></table>'+
                                      '</div><div class="modal-footer">'+
                                       '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                                      '</div></div></div></div>';
            cell5.innerHTML = '<input type="text" name="field_name[]" onkeyup="changeTablenames(this,0)" value="" class="form-control" autocomplete="off" id="field_name" placeholder="">';
            cell6.innerHTML = '<input type="text" name="field_label[]" value="" class="form-control" autocomplete="off" id="field_label" placeholder="">';
            cell7.innerHTML = '<input type="text" name="place_holder[]" value="" class="form-control" autocomplete="off" id="place_holder" placeholder="">';
            cell8.innerHTML = '<input type="text" name="text_area_height[]" maxlength="4" readonly value="" class="form-control" autocomplete="off" id="text_area_height" placeholder="" style="width:45px;float:left">';
            cell9.innerHTML = '<input style="width: 80%;float: left;" type="text" name="master_table[]" value="" readonly class="form-control" autocomplete="off" id="master_table" placeholder=""><input type="button" style="float: right;" class="btn btn-sm btn-primary master_table_text_val" disabled value="+" /><div class="pop_up_2 select_pop_up_2 sr-only" style="width: 400px;">'+
                '<div class="pop_closebtn pop_closebtnfield_2">X</div><div class="">'+
                '<table class="table table-responsive table-striped " style="margin-bottom:0px;">'+
                '<thead><tr class="fixtr"><th title="Text">Text</th><th title="Value">Value</th></tr></thead><tbody><tr class="fixtr">'+
                '<td><input type="text" class="form-control" tabindex="-1" name="master_table_text[]" value="" id="" title=" Text" placeholder="Text">'+
                '</td><td><input type="text" class="form-control" tabindex="-1" value="" name="master_table_value[]" id="" title=" Value" placeholder="Value"></td></tr></tbody></table></div></div>';
            cell10.innerHTML = '<a style="line-height: 22px;" onclick="removeCurrentRow(this)" ><i class="fa fa-minus"></i></a>';


            attrNewIdFormWidth = 'field_area_width_'+setIDval;
            var cloneWidthData = $("#form_builder_table").find('tbody tr:first select[name="field_area_width[]"]');
            var newSelectFormWidth = cloneWidthData.clone().appendTo(cell8);
            newSelectFormWidth.attr('id',attrNewIdFormWidth);

}

function removeCurrentRow(obj){
    $(obj).parent().parent().remove();
}

$(".search-tag-to-add").select2({
    placeholder: 'Select Tag Here...',
    width: '450px',
    allowClear: false,
});

function addTag(){
    bootbox.prompt({
    title: "Enter Tag Name",
    inputType: 'text',
    buttons: {
        confirm: {
            label: "Save Tag",
            className: 'btn-primary',
            callback: function(){
            }
        },
        cancel: {
            label: "Cancel",
            className: 'btn-danger',
            callback: function(){
            }
        },
    },
    callback: function (result) {
        if(result != "" && result != undefined){
            var res = result.indexOf(' ') >= 0;
            if(res == true){
                Command: toastr["warning"]("Space Not Allowd..!");
            }else{
                saveTag(result);
            }
        }
    }
    });
}

function saveTag(text){
            var url = '';
            $.ajax({
              type: "GET",
              url: url,
              data: "tag=" + text,
              beforeSend: function () {

              },
              success: function (data) {
                if(data.status == 1){
                    Command: toastr["success"]("Tag Saved.");
                    $('#tagName').append($("<option></option>").attr("value",data.id).text(data.text));
                }else{
                    Command: toastr["warning"]("Error.!");
                }
              },
              complete: function () {
                $('.form_theadfix_wrapper').floatThead("reflow");
              }
            });
}

 $(document).ready(function() {
    $('#menu_toggle').trigger('click');

    $('.form_theadscroll').perfectScrollbar({
      // suppressScrollX : true,
      wheelPropagation: true,
      minScrollbarLength: 30
    });
    $('.form_theadfix_wrapper').floatThead({
      position: 'absolute',
      scrollContainer: true
    });

    setTimeout(function() {
            $('.sidebar-toggle').trigger('click');
    }, 1000);

});

 function genTableRowColWise(obj,i){
    var rows = $(obj).closest('tr').find('input[name="rows[]"]').val();
    var cols = $(obj).closest('tr').find('input[name="columns[]"]').val();
    var fieldname = $(obj).closest('tr').find('input[name="field_name[]"]').val();

    if(fieldname == "" || fieldname == undefined){
        Command: toastr["warning"]("Error.! Fill Field Name.!");
        return false;
    }
    if(rows == "" || rows == undefined){
        Command: toastr["warning"]("Error.! Fill Rows .!");
        return false;
    }
    if(cols == "" || cols == undefined){
        Command: toastr["warning"]("Error.! Fill Columns .!");
        return false;
    }
    if($.isNumeric(rows) == false){
        Command: toastr["warning"]("Error.! Fill Columns as Number .!");
        return false;
    }
    if($.isNumeric(cols) == false){
        Command: toastr["warning"]("Error.! Fill Rows as Number .!");
        return false;
    }

    $('#tableStruct-'+i).modal({backdrop: 'static', keyboard: false});

    var rowHeaderChecked = $(obj).closest('tr').find('input[name="rowHeader['+i+']"]:checked').length > 0;
    var colHeaderChecked = $(obj).closest('tr').find('input[name="colHeader['+i+']"]:checked').length > 0;
    if(rowHeaderChecked == true){
        ++cols;
    }
    if(colHeaderChecked == true){
       ++rows;
    }
    var table = document.getElementById("tableGen-"+i);
    var countRow = document.getElementById("tableGen-"+i).rows.length;
    $(table).html('');
    for (var i = 0; i < rows; i++) {
        var row = table.insertRow(i);
        for (var j = 0; j < cols; j++) {
            var cell = row.insertCell(j);
            var fieldNm = fieldname+'_row_'+i+'[]';
            cell.innerHTML = '<input type="text" name="'+fieldNm+'" class="form-control">';
        }
    }
    $(obj).closest('table').parent().find('input[name="viewTable[]"]').attr('disabled',false);

 }

 function viewTableRowColWise(obj,i){
    $('#tableStruct-'+i).modal({backdrop: 'static', keyboard: false});
 }

 function enableGenerateTableBtn(obj){
    var row = $.isNumeric($(obj).closest('tr').find('input[name="rows[]"]').val());
    var col = $(obj).closest('tr').find('input[name="columns[]"]').val();
    var field = $(obj).parent().parent().parent().closest('tr').find('input[name="field_name[]"]').val();

    if(row != '' && col != '' && field != ''){
        $(obj).closest('table').parent().find('input[name="genTable[]"]').attr('disabled',false);
    }else{
        $(obj).closest('table').parent().find('input[name="genTable[]"]').attr('disabled',true);
    }
 }

 function changeTablenames(obj,m){
    var field_name = $(obj).val();
    if(field_name == ""){
        return false;
    }
    //is a number
    if(!isNaN(field_name)){
        Command: toastr["warning"]("Name must be a string .!");
        $(obj).val("");
        return false;
    }
    //regular expression validate
    var regex_cell = /[^[0-9A-Za-z_+]]*/gi;
    var new_value = field_name.replace(regex_cell, '');
    $(obj).val(new_value);

    $('#tableGen-'+m).find(':input').map(function(ind,element){
        var cur_name = $(element).attr('name');
        var data = cur_name.split('_row');
        var newname = field_name+'_row'+data[1];
        $(element).attr('name',newname);
    });
 }

function addVitalDataList(id,value){
 $('#prepopdata').append('<option value="'+id+'">'+value+'</option>');
}

function loadDefaultValModal(id){
    tinymceload();
    $('#add_def_val_attr_id').val(id);
    $('#defaultValueModal').modal({backdrop: 'static', keyboard: false});
    setTimeout(function(){
        tinymce.get("default_value_tinymce").setContent($('#default_value-'+id).val());
    }, 250);
}

// function tinymceload(field) {
//   if(field != '' || field != undefined){
//         tinymce.init({
//                   width: "300",
//                   height: "200",
//                   browser_spellcheck : true,
//                   relative_urls : true,
//                   advlist_bullet_styles: "square | disc | circle",
//                   plugins: " spellchecker  table   advlist  hr  pagebreak  fullscreen  preview  insertdatetime  textcolor colorpicker  nonbreaking  lineheight uploadimage image",
//                   content_style: ".mce-content-body {font-size:18px;font-family:Times New Roman;}",
//                   advlist_number_styles: "lower-alpha" ,
//                   fontsize_formats: "2px 3px 4px 5px 6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 20px 22 px 24px 30px 36px 48px 72px",
//                   advlist_bullet_styles: "square",
//                   lineheight:"2pt",
//                   indentation : '1pt',
//                   force_br_newlines : true,
//                   force_p_newlines : false,
//                   forced_root_block : '', // Needed for 3.x,
//                   menubar:true,
//                   mewnubar: "insert",
//                   lineheight_formats: "2pt 3pt 4pt 5pt 6pt 7py 8pt 9pt 10pt 11pt 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
//                   nonbreaking_force_tab: true,
//                   statusbar: false,
//                   selector: '#'+field,
//                   toolbar: ' undo redo  | \n\
//                         bold italic underline | bullist numlist outdent indent | fullscreen | preview | pagebreak | insertdatetime | nonbreaking | hr | table | forecolor backcolor | fontselect | fontsizeselect | alignleft aligncenter alignright alignjustify | \n\
//                           | clearBtn | lineheightselect | MedicalSummary | Ucase | Lcase | Icase | Ccase | uploadimage' ,
//                   font_formats: 'Arial=arial,helvetica,sans-serif;Times New Roman=times new roman;Courier New=courier new,courier;',
//                   table_default_attributes: {
//                     border: 1,
//                     cellpadding: 4
//                   },
//                   table_default_styles: {
//                       borderCollapse: "collapse",
//                       width: '100%'
//                   },
//                   theme_advanced_fonts :"Times New Roman=times new roman,times",
//                   menu: {
//                       file: {title: 'File', items: 'newdocument'},
//                       edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
//                       insert: {title: 'Insert', items: 'link media | template hr'},
//                       view: {title: 'View', items: 'visualaid'},
//                       format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
//                       table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
//                   },
//                   color_picker_callback: function(callback, value) {
//                    callback('#FF00FF');
//                   },
//                   setup: function (editor) {
//                         editor.addButton('clearBtn', {
//                           text: '< Clear >',
//                           icon: false,
//                           onclick: function () {
//                              clearContent()
//                           },

//                       });

//                       editor.addButton('Ucase', {
//                           text: 'A^',
//                           icon: false,
//                           onclick: function () {
//                             TextToUpperCase();
//                           },
//                       });
//                       editor.addButton('Lcase', {
//                           text: 'a^',
//                           icon: false,
//                           onclick: function () {
//                             TextToLowerCase();
//                           },
//                       });
//                       editor.addButton('Icase', {
//                           text: 'I^',
//                           icon: false,
//                           onclick: function () {
//                             TextToInterCase();
//                           },
//                       });
//                       editor.addButton('Ccase', {
//                           text: 'C^',
//                           icon: false,
//                           onclick: function () {
//                             FirstLetterToInterCase();
//                           },
//                       });
//                   }
//         });
//       }
//         return;
// }
function setDefVal(){
   let cur_d_id = $('#add_def_val_attr_id').val();
   let cur_d_data = tinymce.get("default_value_tinymce").getContent();
    $('#default_value-'+cur_d_id).val(cur_d_data);
}

// function tinymceload(){
//   tinymce.init({
//             selector: '#default_value_tinymce',
//             theme: "modern",
//             subfolder: "",
//             height : "400",
//             relative_urls: false,
//             remove_script_host: false,
//             convert_urls: true,
//             convert_fonts_to_spans : false,
//             paste_auto_cleanup_on_paste : true,
//             paste_remove_styles: true,
//             paste_remove_styles_if_webkit: true,
//             paste_strip_class_attributes: true,
//             paste_remove_spans  : true,
//             menubar  : false,
//             toolbar: "bold italic underline | alignleft aligncenter alignright | bullist numlist outdent indent | forecolor backcolor",
//         });
// }

function tinymceload(){
    tinymce.init({
        selector: '#default_value_tinymce',
        max_height: 300,
        autoresize_min_height: '90',
        imagetools_cors_hosts: ['picsum.photos'],
        autosave_interval: '30s',
        autosave_prefix: '{path}{query}-{id}-',
        autosave_retention: '2m',
        paste_enable_default_filters: false,
        importcss_append: false,
        height: 400,
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',
        branding: false,
        forced_root_block: '',
    });
}


</script>

@endsection
