@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<style>
    table > tbody > tr > td {
        text-align: left !important;
    }
</style>
@endsection
@section('content-area')

 <!-- page content -->
        <div class="right_col"  role="main">
        <div class="row codfox_container">
            <!-- title section -->
            <div class="col-md-12">
                <b>{{strtoupper($title)}}</b>
                <hr>
            </div>
            <!-- title section -->

            <!-- Mesage Container -->
            <div class="col-md-12">
                @if (\Session::has('successMsg'))
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> {{\Session::get("successMsg")}}.
                    </div>
                @elseif (\Session::has('failureMsg'))
                    <div class="alert alert-danger alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Error!</strong> {{\Session::get("failureMsg")}}.
                    </div>
                @endif
            </div>
            <!-- Mesage Container -->

            <div class="col-md-12 padding_sm">
                {!!Form::open(array('route' =>'extensionsvalley.forms.formfieldslist', 'method' => 'post'))!!}
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">

                            <div class="col-md-3 padding_sm">
                                <label for="">Type</label>
                                <div class="clearfix"></div>
                                {!!Form::select('type',$formTypes,(isset($searchFields['type'])) ? $searchFields['type'] : null, ['class'=>'form-control'])!!}
                            </div>

                            <div class="col-md-3 padding_sm">
                                <label for="">Field Name</label>
                                <div class="clearfix"></div>
                                <input type="text" name="field_name" id="field_name" class="form-control" placeholder="Field Name" value="{{$searchFields['field_name'] or ''}}" />
                            </div>

                            <div class="col-md-3 padding_sm">
                                <label for="">Form</label>
                                <div class="clearfix"></div>
                                {!!Form::select('form',$forms, (isset($searchFields['form'])) ? $searchFields['form'] : null, ['class'=>'form-control form-select-in-fields-list'])!!}
                            </div>

                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <button class="btn btn-block light_purple_bg"><i class="fa fa-search"></i> Search</button>
                            </div>

                            <div class="col-md-1 padding_sm"></div>

                            <div class="col-md-1 padding_sm">
                                <label for="">&nbsp;</label>
                                <div class="clearfix"></div>
                                <a href="{{url('forms/form-field-add')}}" class="btn btn-block light_purple_bg"><i class="fa fa-plus"></i> Add</a>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::token() !!}
                {!! Form::close() !!}

                <div class="clearfix"></div>
                    <div class="box no-border no-margin" style="height: 450px;">
                        <div class="box-body clearfix">
                            <div id="Show">
                            <div class="theadscroll" style="position: relative; height: 350px;">
                                <table class="table no-margin theadfix_wrapper table-striped table-col-bordered table_sm table-condensed" style="border: 1px solid #CCC;">
                                    <thead>
                                        <tr class="table_header_bg">
                                            <th style="width: 17%;">Type</th>
                                            <th style="width: 27%;">Default Value</th>
                                            <th style="width: 7%;">Mandatory</th>
                                            <th style="width: 17%;">Field Name</th>
                                            <th style="width: 17%;">Field Label</th>
                                            <th style="width: 5%;">Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($formDetails) && count($formDetails))
                                            @foreach($formDetails as $checks)
                                            @php
                                            $str = (!empty($checks->default_value)) ?  $checks->default_value : '';
                                            if (strlen($str) > 70):
                                                $str = substr($str, 0, 67) . '...';
                                            endif;
                                            @endphp
                                            <tr>
                                                 <td> {{ (!empty($checks->type_name)) ?  $checks->type_name : '' }}</td>
                                                 <td> {{ (!empty($checks->default_value)) ?  $str : '' }}</td>
                                                 <td> {{ (isset($checks->is_required) && $checks->is_required == 0   ) ? 'Yes'  : 'No' }}</td>
                                                 <td> {{ (!empty($checks->field_name)) ?  $checks->field_name : '' }}</td>
                                                 <td> {{ (!empty($checks->field_label)) ?  $checks->field_label : '' }}</td>
                                                 <td class="text-center" width="6%">
                                                    <a class="btn btn-default"  onclick="EditField('{{$checks->id}}');" title="Edit" ><i class="fa fa-pencil"></i></a>
                                                    &nbsp;
                                                    <a class="btn btn-default" onclick="deleteData(this,'{{$checks->id}}');" title="Delete" ><i class="fa fa-remove"></i></a>
                                                 </td>
                                            </tr>
                                           @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7">{!! trans('master.records.not_found') !!}</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 text-center purple_pagination">
                            <?php echo $paginator->appends($searchFields)->render(); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- /page content -->
@stop
@section('javascript_extra')
     <script type="text/javascript">
       $(document).ready(function() {

        var $table = $('table.theadfix_wrapper');

        $table.floatThead({
            scrollContainer: function($table) {
                return $table.closest('.theadscroll');
            }

        });

        $('.datepicker').datetimepicker({
            format: 'DD-MM-YYYY'
        });
        $('.date_time_picker').datetimepicker();


        $('.theadscroll').perfectScrollbar({
            wheelPropagation: true,
            minScrollbarLength: 30
        });

    });

    function EditField(id){
      window.location.href = "{{url('forms/form-field-add')}}"+"/"+ id;
    }

    function deleteData(obj,id){
        if(id != ""){
             bootbox.confirm({
                message: "Do you want to delete it?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result == true){
                        var url = "{{url('forms/form-field-delete')}}"+"/"+ id;
                        $.ajax({
                            type: "GET",
                            url: url,
                            data: 'delete_form_bulder_item='+id,
                            beforeSend: function () {

                            },
                            success: function (data) {
                                if(data == 1){
                                    $(obj).parent().parent().remove();
                                    Command: toastr["success"]('Successfully Deleted ..');
                                }else{
                                    if(data == 2){
                                        Command: toastr["warning"]('Cannot delete, Fields under Category');
                                    }else{
                                        Command: toastr["warning"]('Error ..');
                                    }
                                }
                            },
                            error: function () {
                              Command: toastr["warning"]('Please check your internet connection and try again');
                            },
                            complete: function () {
                            }
                        });
                    }
                }
            });

        }else{
            Command: toastr["warning"]('Error..!');
        }
     }

    $(window).on('hashchange', function () {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            } else {
                getPosts(page);
            }
        }
    });

    function getPosts(page) {
        $.ajax({
            url: page,
            dataType: 'html',
        }).done(function (data) {
            $('#Show').html(data);
        }).fail(function () {
            alert('Posts could not be loaded.');
        });
    }

    </script>
@endsection
