@if(sizeof($medicineList) > 0)
    @php
        $i=$rows;
        //dd($medicineList);
    @endphp
    @foreach ($medicineList as $ind => $data)
    @php
        $item_code = (!empty($data->item_code)) ? $data->item_code : "";
        $chemical_name = (!empty($data->chemical_name)) ? $data->chemical_name : "";
        $duration = (!empty($data->duration)) ? $data->duration : "";
        $route = (!empty($data->route)) ? $data->route : "";
        $frequency = (!empty($data->frequency)) ? $data->frequency : "";
        $quantity = (!empty($data->quantity)) ? $data->quantity : "";
        $dose = (!empty($data->dose)) ? $data->dose : "";
        $notes = (!empty($data->notes)) ? $data->notes : "";
        $item_desc = (!empty($data->item_desc)) ? $data->item_desc : "";
        $generic_name = (!empty($data->generic_name)) ? $data->generic_name : "";
        $generic_id = (!empty($data->generic_id)) ? $data->generic_id : "";
        $price = (!empty($data->price)) ? $data->price : "";
        $stock = (!empty($data->stock)) ? $data->stock : "";
        $id = (!empty($data->id)) ? $data->id : "";
        $frequency_value = (!empty($data->value)) ? $data->value : "";
        $frequency_id = (!empty($data->frequency_id)) ? $data->frequency_id : "";
        $iv_fluid_status = (!empty($data->iv_fluid_status)) ? $data->iv_fluid_status : "";
        $w_stock = @$data->w_stock ? $data->w_stock : 0;
        $iv_started_at = date('d-M-Y H:i');
    @endphp
    @if ($administration_in_nurse_dashboard == '1') 
    <tr class="selected_medicine_row bookmark_row  ">
        <td width="5%" class="med_serial_no "></td>
        <td width="27%">
            <span class='med_name'>{{$item_desc}}</span> <input type='hidden' class="selected_fav_medicine_code" name='selected_item_code[]' id='selected_item_code-{{$i}}' value='{{$item_code}}'><input type='hidden' class='form-control @if(!$stock) no-stock @endif' name='selected_item_name[]' id='selected_item_name-{{$i}}' value='{{$item_desc}}' data-generic-name='{{$generic_name }}' item_code='{{$item_code }}' data-generic-id='{{$generic_id }}'><input type='hidden' name='selected_edit_id[]' id='selected_edit_id-{{$i}}' value=''><input type='hidden' name='selected_item_price[]' id='selected_item_price-{{$i}}' value='{{$price}}'><div class="medicine-list-div-row-listing" style="display: none;"><a style="float: left;" class="close_btn_med_search">X</a><div class=" presc_theadscroll" style="position: relative;"><table id="MedicationTableRowListing-{{$i}}"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><thead><tr class="light_purple_bg"><th>Medicine</th><th>Generic Name</th><th>Therapeutic Category</th><th>Therapeutic Sub Category</th><th>Stock</th><th>Price</th></tr></thead><tbody id="ListMedicineSearchDataRowListing-{{$i}}" class="list-medicine-search-data-row-listing" ></tbody></table></div></div>
        </td>
        <td width="18%">
            <input type="text" disabled="" class="generic_name_input form-control" value="{{$chemical_name}}">
        </td>
        <td width="7%">
            <span class='dose'>{{$dose}}</span> <input type='hidden' class='form-control' name='selected_item_dose[]' id='selected_item_dose-{{$i}}' value='{{$dose}}'>     
        </td>
        <td width="10%">
            <span class='frequency'>{{$frequency}}</span>
            <input type='hidden' class='form-control' name='selected_item_frequency[]' id='selected_item_frequency-{{$i}}' value='{{$frequency}}'>
            <input type='hidden' name='selected_frequency_value[]' id='selected_frequency_value-{{$i}}' value='{{$frequency_value}}'>
            <input type='hidden' name='selected_frequency_id[]' id='selected_frequency_id-{{$i}}' value='{{$frequency_id}}'>
            <div class="frequency-list-div-row-listing" style="display: none;">
                <a style="float: left;" class="close_btn_freq_search">X</a>
                <div class=" freq_theadscroll" style="position: relative;">
                    <table id="FrequencyTableRowListing-{{$i}}"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                        <tbody id="ListFrequencySearchDataRowListing-{{$i}}" class="list-frequency-search-data-row-listing" >

                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td width="5%">
            <span class='duration'>{{$duration}}</span> <input type='hidden' class='form-control' name='selected_item_duration[]' id='selected_item_duration-{{$i}}' value='{{$duration}}'>
        </td>
        <td width="10%">
            <span class='quantity'>{{$quantity}}</span> <input type='hidden' class='form-control' name='selected_item_quantity[]' id='selected_item_quantity-{{$i}}' value='{{$quantity}}'>
            <input type="hidden" class="form-control" name="selected_calculate_quantity[]" value="{{ $quantity }}" autocomplete="off">
        </td>
        <td width="13%">
            <span class='route'>{{$route}}</span> <input type='hidden' class='form-control' name='selected_item_remarks[]' id='selected_item_remarks-{{$i}}' value='{{$route}}'>
            <div class="instruction-list-div-row-listing" style="position: absolute;display: none;z-index: 99;">
                <a style="float: left;" class="close_btn_instruction_search">X</a>
                <div class=" instruction_theadscroll" style="position: relative;">
                    <table id="InstructionTableRowListing-{{$i}}"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                        <tbody id="ListInstructionSearchDataRowListing-{{$i}}" class="list-instruction-search-data-row-listing" >
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td width="5%" style="text-align:center;">
            <button class='btn_sm delete_row del-presc-list-row'><i class='fa fa-trash'></i></button>
        </td>
    </tr>
    @else 
    <tr class="selected_medicine_row bookmark_row  ">
        <td width="5%" class="med_serial_no "></td>
        <td width="30%">
            <span class='med_name'>{{$item_desc}}</span> <input type='hidden' class="selected_fav_medicine_code" name='selected_item_code[]' id='selected_item_code-{{$i}}' value='{{$item_code}}'><input type='hidden' class='form-control @if(!$stock) no-stock @endif' name='selected_item_name[]' id='selected_item_name-{{$i}}' value='{{$item_desc}}' data-generic-name='{{$generic_name }}' item_code='{{$item_code }}' data-generic-id='{{$generic_id }}'><input type='hidden' name='selected_edit_id[]' id='selected_edit_id-{{$i}}' value=''><input type='hidden' name='selected_item_price[]' id='selected_item_price-{{$i}}' value='{{$price}}'><div class="medicine-list-div-row-listing" style="display: none;"><a style="float: left;" class="close_btn_med_search">X</a><div class=" presc_theadscroll" style="position: relative;"><table id="MedicationTableRowListing-{{$i}}"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper"><thead><tr class="light_purple_bg"><th>Medicine</th><th>Generic Name</th><th>Therapeutic Category</th><th>Therapeutic Sub Category</th><th>Stock</th><th>Price</th></tr></thead><tbody id="ListMedicineSearchDataRowListing-{{$i}}" class="list-medicine-search-data-row-listing" ></tbody></table></div></div>
        </td>
        <td width="25%">
            <input type="text" disabled="" class="generic_name_input form-control" value="{{$chemical_name}}">
        </td>
        <td width="10%">
            <span class='quantity'>{{$quantity}}</span> <input type='hidden' class='form-control' name='selected_item_quantity[]' id='selected_item_quantity-{{$i}}' value='{{$quantity}}'>
            <input type="hidden" class="form-control" name="selected_calculate_quantity[]" value="{{ $quantity }}" autocomplete="off">
        </td>
        <td width="20%">
            <span class='route'>{{$route}}</span> <input type='hidden' class='form-control' name='selected_item_remarks[]' id='selected_item_remarks-{{$i}}' value='{{$route}}'>
            <div class="instruction-list-div-row-listing" style="position: absolute;display: none;z-index: 99;">
                <a style="float: left;" class="close_btn_instruction_search">X</a>
                <div class=" instruction_theadscroll" style="position: relative;">
                    <table id="InstructionTableRowListing-{{$i}}"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                        <tbody id="ListInstructionSearchDataRowListing-{{$i}}" class="list-instruction-search-data-row-listing" >
                        </tbody>
                    </table>
                </div>
            </div>

            </select>
        </td>
        <td width="5%" style="text-align:center;">
            <button class='btn_sm delete_row del-presc-list-row'><i class='fa fa-trash'></i></button>
        </td>
    </tr>
    @endif

    @php
        $i++;
    @endphp
    @endforeach
@endif
