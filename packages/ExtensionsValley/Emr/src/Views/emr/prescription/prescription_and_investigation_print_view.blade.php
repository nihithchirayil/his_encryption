<!-- .css -->
<style>
  #doctorHeadClass p:first-child{
      margin: 0 !important;
      padding: 0 !important;
  }
</style>
<style type="text/css" media="print">
  @page {
  margin: 15px;
  }
  table {
    font-size : 13px;
  }
  @media print {
    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
  }
</style>
<!-- .css -->
<!-- .box-body -->
<div class="box-body">

    @if(!empty($already_entered))
    <div class="col-md-12 no-padding">
        @include('Emr::emr.investigation.investigation_hospital_header')
        
        <div class="col-md-12" style="margin:30px; text-align:center;">
            <h3>CLINICAL ASSESSMENT</h3>
        </div>


        <table border="1" style="border-collapse:collapse;border-color:rgb(241, 235, 235);" width="100%" cellspacing="0" cellpadding="0">

            @if (!empty($all_form_fields))
                @if (sizeof($all_form_fields) > 0)
                    @for ($i = 0; $i < sizeof($all_form_fields); $i++)
                        @php
                            $type_name = '';
                            $type = '';
                            $default_value = '';
                            $entered_value = '';
                            $field_value = '';
                            $is_required = '';
                            $field_name = '';
                            $field_label = '';
                            $progress_table = '';
                            $progress_value = '';
                            $progress_text = '';
                            $class_name = '';
                            $table_structure = '';
                            $table_details = '';
                            $field_width = '';
                            $field_height = '';
                            $form_id = '';
                            $form_field_id = '';

                            $type_name = !empty($all_form_fields[$i]->type_name) ? $all_form_fields[$i]->type_name : '';
                            $type = !empty($all_form_fields[$i]->type) ? $all_form_fields[$i]->type : '';
                            $type = !empty($all_form_fields[$i]->type) ? $all_form_fields[$i]->type : '';
                            $field_name = !empty($all_form_fields[$i]->field_name) ? $all_form_fields[$i]->field_name : '';
                            $field_label = !empty($all_form_fields[$i]->field_label) ? $all_form_fields[$i]->field_label : '';
                            //$default_value = (!empty($all_form_fields[$i]->default_value)) ? $all_form_fields[$i]->default_value : '';
                            $form_field_id = !empty($all_form_fields[$i]->form_field_id) ? $all_form_fields[$i]->form_field_id : '';
                            $form_id = !empty($all_form_fields[$i]->form_id) ? $all_form_fields[$i]->form_id : '';
                            //progress search
                            $progress_table = !empty($all_form_fields[$i]->progress_table) ? $all_form_fields[$i]->progress_table : '';
                            $progress_value = !empty($all_form_fields[$i]->progress_value) ? $all_form_fields[$i]->progress_value : '';
                            $progress_text = !empty($all_form_fields[$i]->progress_text) ? $all_form_fields[$i]->progress_text : '';
                            //table
                            $table_structure = !empty($all_form_fields[$i]->table_structure) ? $all_form_fields[$i]->table_structure : '';
                            $table_details = !empty($all_form_fields[$i]->table_details) ? $all_form_fields[$i]->table_details : '';

                            $radio = [];
                            $gcheckbox = [];
                            $selected_value = [];
                            $optionsListSelected = [];

                        @endphp


                        <tr>
                            @if ($type_name == 'TEXT')

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }
                                @endphp

                                @if ($field_value != '')
                                    <td width="25%"><strong>{{ $field_label }}</strong></td>
                                    <td>{!! $field_value !!}</td>
                                @endif

                            @elseif($type_name == "TEXT AREA")

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }
                                @endphp

                                @if ($field_value != '')
                                    <td width="30%"><strong>{{ $field_label }}</strong></td>
                                    <td>{!! nl2br($field_value) !!}</td>
                                @endif

                            @elseif($type_name == "DATE")

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }
                                @endphp

                                @if ($field_value != '')
                                    <td width="30%"><strong>{{ $field_label }}</strong></td>
                                    <td>{{ $field_value }}</td>
                                @endif

                            @elseif($type_name == "TINYMCE")

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }
                                @endphp


                                @if ($field_value != '')
                                    @if ($include_patient_header != 1)
                                        <td width="30%"><strong>{{ $field_label }}</strong></td>
                                    @endif
                                    <td>{!! $field_value !!}</td>
                                @endif

                            @elseif($type_name == "CERTIFICATE")

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }
                                @endphp

                                @if ($field_value != '')
                                    <td width="30%"><strong>{{ $field_label }}</strong></td>
                                    <td>{!! $field_value !!}</td>
                                @endif

                            @elseif($type_name == "CHECK BOX")

                                @php

                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';

                                    $checked_box_default_value = '';

                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;

                                        //if check box checked then get default value as name
                                        $checked_box_default_value = !empty($all_form_fields[$i]->default_value) ? $all_form_fields[$i]->default_value : '';
                                    } else {
                                        $field_value = $default_value;
                                    }

                                @endphp

                                @if ($field_name == $field_value)
                                    <td width="30%"><strong>{{ $field_label }}</strong></td>
                                    <td>{{ $checked_box_default_value }}</td>
                                @endif

                            @elseif($type_name == "SELECT BOX")

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }

                                    $options = '';
                                    if (!empty($field_value)):
                                        $options = ExtensionsValley\Emr\FormFieldSelectboxOptions::where('form_field_id', '=', $form_field_id)
                                            ->where('value', '=', $field_value)
                                            ->value('name');
                                    endif;
                                @endphp

                                @if (!empty($options))
                                    <td width="30%"><strong>{{ $field_label }}</strong></td>
                                    <td>{!! $options !!}</td>
                                @endif

                            @elseif($type_name == "RADIO BUTTON")

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }
                                @endphp

                                @if (isset($form_field_id) && !empty($form_field_id))
                                    @php
                                        $radio = '';
                                        if (!empty($field_value)):
                                            $radio = ExtensionsValley\Emr\FormFieldRadioOptions::where('form_field_id', '=', $form_field_id)
                                                ->where('value', '=', $field_value)
                                                ->value('name');
                                        endif;
                                    @endphp
                                @endif

                                @if (!empty($radio))
                                    <td width="30%"><strong>{{ $field_label }}</strong></td>
                                    <td>{!! $radio !!}</td>
                                @endif

                            @elseif($type_name == "GROUPED CHECKBOX")

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }
                                @endphp

                                @if (sizeof((array) $field_value) > 0)

                                    @php
                                        $datArr = [];
                                        $datArr = (array) $field_value;
                                    @endphp

                                    @if ($field_value != '')
                                        <td width="30%"><strong>{{ $field_label }}</strong></td>
                                        <td>
                                            @foreach ($datArr as $ind => $data)
                                                {!! $data !!} <br>
                                            @endforeach
                                        </td>
                                    @endif
                                @endif

                            @elseif($type_name == "HEADER TEXT")

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }
                                @endphp

                                <td colspan="2"><b>{!! $default_value !!}</b></td>

                            @elseif($type_name == "PROGRESSIVE SEARCH")

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }
                                @endphp

                                @if (sizeof((array) $field_value) > 0)

                                    @php
                                        $datProArr = [];
                                        $datProArr = (array) $field_value;
                                    @endphp

                                    @if ($field_value != '')
                                        <td width="30%"><strong>{{ $field_label }}</strong></td>
                                        <td>
                                            @foreach ($datProArr as $ind => $pro_data)
                                                {!! $pro_data !!} <br>
                                            @endforeach
                                        </td>
                                    @endif
                                @endif

                            @elseif($type_name == "TABLE")

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }

                                    $tableDatArr = [];
                                    $tableDatArr = (array) $field_value;

                                    $Tdetails = json_decode($table_details);
                                    $table_details = $Tdetails->$field_name;

                                    $rowHeader = isset($table_details->rowHeader) ? $table_details->rowHeader : 0;
                                    $colHeader = isset($table_details->colHeader) ? $table_details->colHeader : 0;

                                    $Tdata = [];
                                    $TdataCount = 0;
                                    $fixed_headers = 0;
                                    $fixed_headers = $rowHeader + $colHeader;

                                    if (count($tableDatArr) > 0) {
                                        $Tdata = array_filter($tableDatArr);
                                        if (is_array($Tdata) && count($Tdata) > 0) {
                                            $TdataCount = sizeof($Tdata);
                                        }
                                    }
                                @endphp

                                @if (isset($tableDatArr))
                                    @if (sizeof($tableDatArr) > 0 && $TdataCount > $fixed_headers)
                                        <td colspan="2" class="no-padding">
                                            <table width="99%"
                                                class="table tabe_sm no-margin table-bordered table-striped">
                                                @foreach ($tableDatArr as $rows => $colsArr)
                                                    <tr>
                                                        @foreach ($colsArr as $colsind => $cols)
                                                            <td>{{ $cols }}</td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                    @endif
                                @endif


                            @elseif($type_name == "CUSTOM DATA")

                                @php
                                    $entered_value = !empty($already_entered->$field_name) ? $already_entered->$field_name : '';
                                    if (!empty($entered_value)) {
                                        $field_value = $entered_value;
                                    } else {
                                        $field_value = $default_value;
                                    }
                                @endphp

                                @if ($field_value != '')
                                    <td width="30%"><strong>{{ $field_label }}</strong></td>
                                    <td>{!! $field_value !!}</td>
                                @endif

                            @endif
                        </tr>

                    @endfor
                @endif
            @endif
        </table>

    </div>
    @endif


    @if(count($investigation_data) > 0)
        @php
        $hospital_header_disable_in_prescription = 0;
        $investigation_types = array();
        foreach($investigation_data as $inves){
            array_push($investigation_types,strtoupper($inves->investigation_type));
        }
        @endphp

        <div class="col-md-12 no-padding" @if($hospital_header_disable_in_prescription == 1) style="margin-top:3cm;" @endif>
            @if(empty($already_entered))
            @include('Emr::emr.investigation.investigation_hospital_header')
            @endif
            <!-----------Laboratory Investigations----------------------------------->
            @if (in_array("LAB", $investigation_types))
                <div class="col-md-12">
                    <div class="col-md-12" style="margin:20px; text-align:center;">
                        <h3>LABORATORY INVESTIGATIONS</h3>
                    </div>
                    <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table" style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">

                            <tr>
                                <td style="width:5%;"><strong>SL#</strong></td>
                                <td style="width:35%;"><strong><center>Department</center></strong></td>
                                <td style="width:60%;"><strong><center>Item</center></strong></td>

                            </tr>

                            @foreach($investigation_data as $inves)
                                @if(strtoupper($inves->investigation_type) == 'LAB')
                                <tr>
                                    <td style="width:5%;"> {{ $loop->iteration }} </td>
                                    <td style="width:35%; text-align:left;padding-left:5px;"> {!! $inves->sub_dept_name!!} </td>
                                    <td style="width:60%; text-align:left;padding-left:5px;"> {!!$inves->service_desc!!} </td>
                                </tr>
                                @endif
                            @endforeach
                        </table>
                </div>
            @endif

            <!-------------------------------- Radiology Investigation------------------------------------------->
            @if (in_array("RADIOLOGY", $investigation_types))
            <!-- <div class="pagebreak"> </div> -->
            <div class="col-md-12">
                <div class="col-md-12" style="margin:20px; text-align:center;">
                    <h3>RADIOLOGY INVESTIGATIONS</h3>
                </div>
                <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"  style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">

                        <tr>
                            <td style="width:5%;"><strong>SL#</strong></td>
                            <td style="width:35%;"><strong><center>Department</center></strong></td>
                            <td style="width:60%;"><strong><center>Item</center></strong></td>

                        </tr>

                        @foreach($investigation_data as $inves)
                            @if(strtoupper($inves->investigation_type) == 'RADIOLOGY')
                            <tr>
                                <td style="width:5%;"> {{ $loop->iteration }} </td>
                                <td style="width:35%; text-align:left;padding-left:5px;"> {!! $inves->sub_dept_name!!} </td>
                                <td style="width:60%; text-align:left;padding-left:5px;"> {!!$inves->service_desc!!} </td>
                            </tr>
                            @endif
                        @endforeach
                    </table>
            </div>
            @endif

            <!-------------------------------- Procedure Investigation------------------------------------------->
            @if (in_array("PROCEDURE", $investigation_types))
            <!-- <div class="pagebreak"> </div> -->
            <div class="col-md-12">
                    <div class="col-md-12" style="margin:20px; text-align:center;">
                        <h3>PROCEDURES</h3>
                    </div>
                    <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table"  style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">

                        <tr>
                            <td style="width:5%;"><strong>SL#</strong></td>
                            <td style="width:35%;"><strong><center>Department</center></strong></td>
                            <td style="width:60%;"><strong><center>Item</center></strong></td>

                        </tr>

                        @foreach($investigation_data as $inves)
                            @if(strtoupper($inves->investigation_type) == 'PROCEDURE')
                            <tr>
                                <td style="width:5%;"> {{ $loop->iteration }} </td>
                                <td style="width:35%; text-align:left;padding-left:5px;"> {!! $inves->sub_dept_name!!} </td>
                                <td style="width:60%; text-align:left;padding-left:5px;"> {!!$inves->service_desc!!} </td>
                            </tr>
                            @endif
                        @endforeach
                    </table>
            </div>
            @endif

            @if(!empty($inv_remarks) || !empty($inv_clinical_history))
            <div class="col-md-12" style="margin-top:40px;">
                <table class="table no-border no-margin theadfix_wrapper table-striped table-condensed styled-table"  style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px;" width="100%" border="1" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="20%"><strong>REMARKS</strong></td>
                        <td style="text-align:left;padding-left:5px;"> {!! $inv_remarks !!} </td>
                    </tr>
                    <tr>
                        <td width="20%"><strong>CLINICAL HISTORY</strong></td>
                        <td style="text-align:left;padding-left:5px;"> {!! $inv_clinical_history !!} </td>
                    </tr>
                </table>
            </div>
            @endif
        </div>
    @endif

    @if(count($prescription_data) > 0)

    <div class="col-md-12 no-padding">
        @if(count($investigation_data) == 0 && empty($already_entered))
        @include('Emr::emr.investigation.investigation_hospital_header')
        @endif
        <div class="col-md-12" style="margin:30px; text-align:center;">
            <h3>PRESCRIPTION DETAILS</h3>
        </div>

        <table border="1" cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; font-size: 12px; margin-top:15px;">
            <tbody>
            <tr>
                <td style="padding: 0px; border-top: none; font-size: 11px;" width="15%" align="left" colspan="2">
                <table width="100%">
                    <tbody>
                    @if($encounterDetails->diagnosis)
                    <tr>
                        <td colspan="5" style="font-size:11px;">
                        <strong> Diagnosis : </strong> <br/>
                        {{$encounterDetails->diagnosis}}
                        </td>
                    </tr>
                    @endif
                    </tbody>
                </table>
                </td>
            </tr>
            <tr>
                <td width="25%" valign="top" style="padding: 20px 5px 5px 5px; font-size: 11px;">
                <span>
                    @if(!empty($medication_note))
                    {!!$medication_note!!}
                    @endif
                </span>
                </td>
                <td width="75%" valign="top" style="padding: 20px 5px 5px 5px;">
                    <span style="float:right">
                    @if(!empty($intend_type) && $intend_type == 1)
                        Regular
                    @elseif(!empty($intend_type) && $intend_type == 2)
                        New Admission
                    @elseif(!empty($intend_type) && $intend_type == 3)
                        Emergency
                    @else
                        Discharge
                    @endif
                    </span>
                    <br><br>
                    <table border="1" cellspacing="0" cellpadding="2" width="100%" style="border-collapse: collapse; font-size: 13px;">
                    @if($is_iv_fluid)
                        <tr>
                        <td><strong>SL#</strong></td>
                        <td><strong>Generic Name</strong></td>
                        <td><strong>Drug Name</strong></td>
                        <td><strong>Quantity</strong></td>
                        <td><strong>Rate</strong></td>
                        <td><strong>Remarks</strong></td>
                        </tr>
                    @elseif($consumable_ind == 1)
                        <tr>
                        <td><strong>SL#</strong></td>
                        <td><strong>Drug Name</strong></td>
                        <td><strong>Quantity</strong></td>
                        <td><strong>Remarks</strong></td>
                        </tr>
                    @else
                        <tr>
                        <td><strong>SL#</strong></td>
                        <!-- <td><strong>Generic Name</strong></td> -->
                        <td width="40%"><strong>Drug Name</strong></td>
                        <td><strong>Dose</strong></td>
                        <td><strong>Frequency</strong></td>
                        <td><strong>Route</strong></td>
                        <td><strong>Days</strong></td>
                        <td><strong>Qty</strong></td>
                        <td><strong>Remarks</strong></td>
                        </tr>
                    @endif
                    <?php $i = 1;?>
                    @foreach($prescription_data as $pres)
                    <?php

                    /********* Medication Period **********/
                    $start_date = "";
                    if(!empty($pres->start_date)){
                        $start_date = \Carbon::parse($pres->start_date)->format('d-m-y');
                    }

                    $stop_date = "";
                    if(!empty($pres->stop_date)){
                        $stop_date = \Carbon::parse($pres->stop_date)->format('d-m-y');
                    }
                    /********* Medication Period **********/

                    $medicine = "";
                    if(!empty($pres->medicine_code)){
                        $medicine = $pres->item_desc;
                    }else{
                        $medicine = $pres->medicine_name;
                    }

                    ?>
                    @if($is_iv_fluid)
                    <tr>
                        <td> {{$i}} </td>
                        <td> {{ $pres->generic_name }}</td>
                        <td> {{ $medicine }}</td>
                        <td valign="top"> {!! $pres->quantity!!} </td>
                        <td><center>{!! $pres->iv_rate!!}</center> </td>
                        <td><center>{!! $pres->notes!!} </center></td>
                    </tr>
                    @elseif($consumable_ind == 1)
                    <tr>
                        <td> {{$i}} </td>
                        <td> {{ $medicine }}</td>
                        <td valign="top"> {!! $pres->quantity!!} </td>
                        <td><center>{!! $pres->notes!!} </center></td>
                    </tr>
                    @else
                    <tr>
                        <td> {{$i}} </td>
                        <!-- <td> {{ $pres->generic_name }}</td> -->
                        <td width="40%"> <div>{{ $medicine }}</div><div style="font-size:11px;">Generic : {{ $pres->generic_name }} </div> </td>
                        <td><center> {!! $pres->dose!!} </center></td>
                        <td><center>{!! $pres->frequency!!}</center> </td>
                        <td><center> {!!$pres->route!!}</center> </td>
                        <td><center> {!! $pres->duration!!} </center> </td>
                        <td><center> {!! $pres->quantity!!}</center> </td>
                        <td><center>{!! $pres->notes!!} </center></td>
                    </tr>
                    @endif
                    <?php $i++;?>
                    @endforeach
                    </table>
                    <br>
                    <table width="100%" style="font-size:12px;">
                    <tr>
                        <td width="49%">
                        @if($encounterDetails->reason_for_visit)
                            <strong style="vertical-align:top; float: left;">Reason For Visit</strong>
                            <br/><br/>
                            @if($encounterDetails->reason_for_visit)
                                {!!trim(nl2br($encounterDetails->reason_for_visit))!!}
                            @endif
                            <br/>
                        @endif
                        </td>
                        <td  width="49%">
                        @if(!empty($encounterDetails->special_notes))
                            <strong style="vertical-align:top; float: left;">Special Notes</strong>
                                <br /><br />
                                @if(!empty($encounterDetails->special_notes))
                                {!!trim(nl2br($encounterDetails->special_notes))!!}
                                @endif
                                <br/>
                        @endif
                        </td>
                    </tr>
                </table>
                    <table width="100%" style="font-size:12px;">
                    <tr>
                        <td width="49%">
                        @if(!empty($next_review_date))
                            <strong style="font-size: 11px;">REVIEW ON: </strong> &nbsp;
                            <input value="{{$next_review_date}}" style="border-left: none; border-top: none; border-right: none; border-bottom: none;" type="text">
                        @endif
                        </td>
                        <td  width="49%">
                        @if(!empty($doctor['doctor_name']))
                        <span style="float: right; margin-right: 10px; font-size: 11px;">
                            {{$doctor['doctor_name']}}<br/>
                            {{$doctor['qualification']}} ( {{$doctor['doctor_speciality']}} )
                            <br/>
                            <br/>
                            <strong>SIGNATURE:</strong>
                        </span>
                        @endif
                        </td>
                    </tr>
                    </table>
                    <br><br>
                </td>
            </tr>
            @if(!empty($medication_footer_text))
            <tr>
                <td style='padding: 3px; vertical-align: middle;' width='15%' align='center' colspan="2">
                <span style="float: right; margin-right: 180px; font-size: 11px;"><strong>{!! $medication_footer_text !!}</strong></span>
                </td>
            </tr>
            @endif
            </tbody>
        </table>
    </div>
    @endif

</div>
<!-- .box-body -->

