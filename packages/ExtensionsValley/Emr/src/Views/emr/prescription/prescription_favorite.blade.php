
<!-- Medicine Favorite List -->
<div class="favorite-medicine-list-popup" style="display: none;">
    <a style="float: left;" class="close_btn_fav_med_search">X</a>
    <div class="col-md-12 no-padding">
        <div class="col-md-2" style="background-color:#eaeaea; border-radius:5px;margin:4px;">
            <div class="group_list theadscroll always-visible" style="height: 460px;">
            <input type="hidden" name="hidden_groups" value="" >
            <ul class="list-unstyled" id="fav_groups">

            </ul>
            </div>
        </div>
        <div class="col-md-10" style="margin:-5px;">

            <div class="col-md-12 no-padding" style="margin-top: 10px;margin-bottom:5px;">
                <div class="col-md-4 padding_xs">
                    <div class="input-group">
                        <input name="favourite_search" id="favourite_search" placeholder="Search" class="form-control hidden" type="text">
                        <span class="input-group-btn">
                        <button type="button" onclick="searchPrescription()" class="hidden btn btn-success btn-flat">
                        <i id="favsearch" class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
                <div class="col-md-3 padding_xs">
                    <input type="text" class="form-control add_group_text hidden" placeholder="Add Group" name="add_fav_groups">
                </div>
                <div class="col-md-2 text-right padding_xs">
                    <button type="button" class="hidden btn btn-block btn-success add_group_btn">Add Group</button>
                    <button type="button" class="hidden btn btn-block btn-success add_to_group_save no-margin" style="display: none;"> Move to Group</button>
                </div>
                <div class="col-md-1 text-right padding_xs">
                    <button type="button" class="hidden btn btn-block btn-success delete_fav_btn"><i class="fa fa-trash"></i> Delete</button>
                </div>
                <div class="col-md-2 no-padding text-right padding_xs">
                    <button type="button" class="btn btn-block btn-info fav_apply_btn">
                        Add to Prescription <i class="fa fa-check"></i></button>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="h10"></div>

            <div class=" presc_fav_theadscroll" style="position: relative;">
                <table id="FavTableList"  class="table no-margin table_sm table-striped presc_fav_theadfix_wrapper">
                    <thead>
                        <tr style="background-color:rgb(84, 146, 231);color:white !important;">
                            <th><input type="checkbox" class="fav_presc_select_checkbox" name="fav_presc_select_checkbox" /></th>
                            <th>Medicine</th>
                            <th>Dosage</th>
                            <th>Quantity</th>
                            <th>Frequency</th>
                            <th>Duration</th>
                            <th>Route</th>
                            <th>Instruction</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="ListMedicineFavorites" >

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Medicine Favorite List -->
