@php
  $hospital_header_disable_in_prescription = 0;
  $medication_footer_text_disable_in_prescription = 0;
  $department_head = (isset($doctor->prescription_department_head)) ? $doctor->prescription_department_head : '';
  $medication_footer_text = (isset($doctor->medication_footer_text)) ? $doctor->medication_footer_text : '';
  $medication_note = (isset($doctor->medication_note)) ? $doctor->medication_note : '';
  $next_review_date = (!empty($encounterDetails->next_review_date)) ? date('d-M-Y', strtotime($encounterDetails->next_review_date)) : '';

    if(\DB::table('config_head')->where('name','hospital_header_disable_in_prescription')->exists()){
        $hospital_header_disable_in_prescription = \DB::table('config_head')->where('name','hospital_header_disable_in_prescription')->value('value');
    }
    if(\DB::table('config_head')->where('name','medication_footer_text_disable_in_prescription')->exists()){
        $medication_footer_text_disable_in_prescription = \DB::table('config_head')->where('name','medication_footer_text_disable_in_prescription')->value('value');
    }

@endphp

<div class="col-md-12 box-body" style="border-radius:3px;">

    <!-- Hospital Header -->
    @if($hospital_header_disable_in_prescription == 0)
    <div class="col-md-12" @if($hospital_header_disable_in_prescription == 1) style="margin-top:140px;" @endif >
        @if(!empty($hospitalHeader))
            {!!$hospitalHeader!!}
        @endif
    </div>
    @endif

    <!-- Patient Details -->
    <div class="col-md-12" @if($hospital_header_disable_in_prescription == 1) style="margin-top:140px;" @endif >
        <table border="1" width="100%;" class="table table-bordered table_sm" style="border-collapse: collapse;table-layout:fixed;font-size:15px !important;color:#484848 !important;">
             <!-- Department Header -->
            <thead>
                <tr>
                    <td colspan="4" style="text-align:center;">{!! $department_head !!}</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="15%">Name:</td>
                    <td width="50%">{{$patient[0]->patient_name}}</td>
                    <td width="15%">UHID:</td>
                    <td width="20%">{{$patient[0]->uhid}}</td>
                </tr>
                <tr>
                    <td>Gender/Age:</td>
                    <td>{{$patient[0]->gender}} / {{$patient[0]->age}}</td>
                    <td>Date:</td>
                    <td>{{$date}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <!---Diaganosis------>
    @if($encounterDetails->diagnosis)
    <div class="col-md-12">
        <table>
            <tr>
                <td style="font-weight: 600;color:#484848 !important;">
                    Diaganosis
                </td>
            </tr>
            <tr>
                <td style="font-size:12px !important;color:#484848 !important;">{{$encounterDetails->diagnosis}}</td>
            </tr>
        </table>
    </div>
    @endif

    <!---Medication note----->
    <div class="col-md-12" style="margin-top:8px;color:#484848 !important;">
        @if(!empty($medication_note))
        <table>
            <tr>
                <td style="font-size:13px !important;font-weight:600;color:#484848 !important;">
                    {!!$medication_note!!}
                </td>
            </tr>
        </table>
        @endif
    </div>

    <!---medication type------->
    <div class="col-md-12">
      <span style="float:right;margin-right:5px;color:#484848 !important;">
          @if(!empty($intend_type) && $intend_type == 1)
              Regular
          @elseif(!empty($intend_type) && $intend_type == 2)
              New Admission
          @elseif(!empty($intend_type) && $intend_type == 3)
              Emergency
          @else
              Discharge
          @endif
        </span>
    </div>

    <!----medications--------------->
    <div class="col-md-12" style="min-height:450px;">
      <table border="1" width="100%;" class="table table-striped table-bordered table-hover table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:12px !important;color:black !important;">
        <thead style="font-weight:600;font-size:13px !important;">
        @if($is_iv_fluid)
          <tr>
            <td style="text-align:center" width="5%">SL#</td>
            <td style="text-align:center" width="25%">Generic Name</td>
            <td style="text-align:center" width="25%">Drug Name</td>
            <td style="text-align:center" width="10%">Quantity</td>
            <td style="text-align:center" width="10%">Rate</td>
            <td style="text-align:center" width="25%">Remarks</td>
          </tr>
        @elseif($consumable_ind == 1)
          <tr>
            <td style="text-align:center" width="10%">SL#</td>
            <td style="text-align:center" width="50%">Drug Name</td>
            <td style="text-align:center" width="10%">Quantity</td>
            <td style="text-align:center" width="30%">Remarks</td>
          </tr>
        @else
          <tr>
            <td style="text-align:center" width="5%">SL#</td>
            <td style="text-align:center" width="34%">Drug Name</td>
            <td style="text-align:center" width="6%">Dose</td>
            <td style="text-align:center" width="10%">Frequency</td>
            <td style="text-align:center" width="8%">Route</td>
            @if ($sub_code=='KEYHOLE')
            <td style="text-align:center" width= "7%">Duration</td>
            @else
       <td style="text-align:center" width= "7%">Days</td>
            @endif
            <td style="text-align:center" width="5%">Qty</td>
            <td style="text-align:center" width="25%">Remarks</td>
          </tr>
        @endif
        </thead>
        <?php $i = 1;?>
        @foreach($data as $pres)
        <?php

        /********* Medication Period **********/
        $start_date = "";
        if(!empty($pres->start_date)){
          $start_date = \Carbon::parse($pres->start_date)->format('d-m-y');
        }

        $stop_date = "";
        if(!empty($pres->stop_date)){
          $stop_date = \Carbon::parse($pres->stop_date)->format('d-m-y');
        }
        /********* Medication Period **********/

        $medicine = "";
        if(!empty($pres->medicine_code)){
            $medicine = $pres->item_desc;
        }else{
            $medicine = $pres->medicine_name;
        }

        ?>
        @if($is_iv_fluid)
        <tr>
          <td style="text-align:center">{{$i}}</td>
          <td style="text-align:left">{{$pres->generic_name}}</td>
          <td style="text-align:left">{{$medicine}}</td>
          <td style="text-align:center">{!! $pres->quantity!!}</td>
          <td style="text-align:center">{!! $pres->iv_rate!!}</td>
          <td style="text-align:left">{!! $pres->notes!!}</td>
        </tr>
        @elseif($consumable_ind == 1)
        <tr>
          <td style="text-align:center">{{$i}} </td>
          <td style="text-align:left">{{ $medicine }}</td>
          <td style="text-align:center" valign="top"> {!! $pres->quantity!!} </td>
          <td style="text-align:left">{!! $pres->notes!!} </td>
        </tr>
        @else
        <tr>
          <td style="text-align:center"> {{$i}} </td>
          <td style="text-align:left;padding:3px;">
            <div>
              {{ $medicine }}</div>
              <div style="font-size:11px;">Generic : {{ $pres->generic_name }}
            </div>
          </td>
          <td style="text-align:center"> {!! $pres->dose!!} </td>
          <td style="text-align:center">{!! $pres->frequency!!} </td>
          <td style="text-align:center"> {!!$pres->route!!} </td>
          @if ($sub_code=='KEYHOLE')
          @if ($pres->duration_unit_id == 1)
          @php
              $duration_unit = 'Days';
          @endphp
          @elseif($pres->duration_unit_id == 2)
          @php
              $duration_unit = 'Week';
          @endphp
          @elseif($pres->duration_unit_id == 3)
          @php
              $duration_unit = 'Month';
          @endphp
         @else
          @php
              $duration_unit = 'Days';
          @endphp
          @endif
            
        
          <td><center> {!! $pres->duration!!} {{ $duration_unit }} </center> </td> 
          @else
          <td><center> {!! $pres->duration!!} </center> </td> 
          @endif
          {{-- <td>{!! $start_date !!} to {!! $stop_date !!}</td> --}}
          <td style="text-align:center"> {!! $pres->quantity!!} </td>
          {{-- <td> {!! $pres->iv_start_at !!} </td> --}}
          <td style="text-align:left">{!! $pres->notes!!} </td>
        </tr>
        @endif
        <?php $i++;?>
        @endforeach
      </table>
    </div>

    <!----special notes----------->
    <div class="col-md-12" style="width:100%;">
      <div class="col-md-6" style="width:50%;float:left;clear:right;">
      @if(!empty($encounterDetails->special_notes))
      <div class="col-md-12" style="margin-top:5px;">
        <table width="100%">
            <tr>
                <td style="font-size:13px !important;color:#484848 !important;">
                  Special Notes
                </td>
            </tr>
            <tr>
                <td style="font-size:13px !important;color:#484848 !important;">
                  {!!trim(nl2br($encounterDetails->special_notes))!!}
                </td>
            </tr>
        </table>
      </div>
      @endif
      <!----Next Review Date----------->
      @if(!empty($next_review_date))
      <div class="col-md-12">
        <span style="font-size:13px !important;margin-left:7px;color:#484848 !important;">
          Review On<br>
          {{$next_review_date}}
        </span>
      </div>
      @endif
      </div>
      <div class="col-md-6" style="width:50% !important;float:right;">
        <!----Signature----------->
        @if(!empty($doctor['doctor_name']))
        <div class="col-md-12" style="margin-top:5px;">
          <span style="font-size:12px !important;float:right;margin-right:5px;color:#484848 !important;">
            {{$doctor['doctor_name']}}<br/>
            {{$doctor['qualification']}} ({{$doctor['doctor_speciality']}})<br/>
            SIGNATURE:
          </span>
        </div>
        @endif
      </div>
    </div>

        <!---Medication footer text----->
        @if(!empty($medication_footer_text) && $medication_footer_text_disable_in_prescription == 0)
        <div class="col-md-12 font-gray" style="margin-top:12px;">
          <table width="100%">
              <tr>
                  <td style="font-size:13px !important;font-weight:600;float:right;color:#484848 !important;">
                    {!!$medication_footer_text!!}
                  </td>
              </tr>
          </table>
        </div>
        @endif
      </div>

    </div>

</div>

