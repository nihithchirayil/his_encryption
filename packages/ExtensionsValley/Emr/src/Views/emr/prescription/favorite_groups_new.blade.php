<div class="col-md-5 box-body leaf_border" style="height: 64vh;">
    <div class="patient_details_patient_name text-normal" id="addnewgrp"><label style="margin-top: 2px" for="">Add New Group:</label>
        <input type="text" id="addNewGrp" class="form-control leaf_border" style="border: 1px solid #ccc !important;">
        <button type="button" class="btn btn-warning leaf_border" id="addGrpBtn" style="margin-left:3px;" onclick="saveNewGroup($('#addNewGrp').val().trim(),this)"><i id="addGrpBtnIcon" class="fa fa-plus"></i></button>
    </div>
    <div class="col-md-12 padding_sm theadscroll" style="height: 250px;margin-top:10px" id="itemRelativeGrps">
        <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed floatThead-table" style="border-collapse: collapse; border: 0px none rgb(128, 128, 128); display: table; margin: 0px; table-layout: fixed; width: 100%;">
            <thead>
                <tr class="tableRowHead">
                    <th width='10%'>Sl.No.</th>
                    <th width='60%'>Group</th>
                    <th width='10%'><i class="fa fa-eye"></i></th>
                    <th width='10%'><i class="fa fa-clone"></i></th>
                    <th width='10%'><i class="fa fa-trash"></i></th>
                </tr>
            </thead>
            <tbody id="addGrpTableBody">
                @if (count($data) != 0)
                @php $i=1; @endphp
                @foreach ($data as $key => $value)
                <tr class="activeTr row_class_newGrp">
                    <td class="td_common_numeric_rules row_count_class_newGrp" onclick="itemListGrpWise(this,{{ $key }})" data-grp-id="{{ $key }}">{{ $i }}.</td>
                    <td class="common_td_rules">
                        <div class="input-group " style="margin:0px; width: 90%;">
                            <input type="text" class="form-control editGrpName" data-oldGrpName="{{ $value }}" id="editGrpName" value="{{ $value }}" style="border: 1px solid #ccc !important; margin-right: 5px;">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2"><button type="button" title="Change Group Name" id="grpEditBtn" onclick="saveChangedGrpName(this,{{ $key }})" class="btn btn-blue grpEditBtn"><i class="fa fa-save"></i></button></span>
                            </div>
                        </div>
                    </td>
                    <td><button type="button" title="Items In Group" id="itemListGrpWise" onclick="itemListGrpWise(this,{{ $key }})" class="btn btn-blue"><i class="fa fa-eye"></i></button></td>
                    <td><button type="button" title="Clone Group Items" id="cloneItemListGrpWisae" onclick="cloneItemListGrpWisae(this,{{ $key }})" class="btn btn-blue grpCloneBtn"><i class="fa fa-clone"></i></button></td>
                    <td><button type="button" title="Delete Group" id="" onclick="deleteGrpName(this,{{ $key }})" class="btn btn-danger grpdeleteBtn"><i class="fa fa-trash"></i></button></td>


                </tr>
                @php $i++; @endphp  
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>


<div class="col-md-7 padding_sm box-body " style="height: 64vh;" id="prescription_list_table_grp">
    <div class="patient_details_patient_name text-normal" style="box-shadow: 0px 3px  0px 0px;"> </div>
    <div class="col-md-3 no-padding padding_xs" style="float: right;">
        <button type="button" class="btn btn-block btn-info" onclick="addFavouriteToPrescription();">
            Add to Prescription <i class="fa fa-check"></i></button>
    </div>
    <table id="prescriptiondata_table_grp" class="table no-margin  table-border-radius-top table-striped  table_sm table-condensed">
        <thead>
            <tr class="tableRowHead">
                <th width="3%"></th>
                <th width="5%">Sl.No.</th>
                <th width="25%">Medicine</th>
                <th width="15%">Quantity</th>
                <th width="5%"><i onclick="addNewMediceneGrp()" class="fa fa-plus"></i></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>



</div>