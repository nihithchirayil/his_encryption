
    <link href="{{asset("packages/extensionsvalley/emr/datatables/dataTables.min.css")}}" rel="stylesheet">
    <script src="{{asset("packages/extensionsvalley/emr/datatables/datatables.min.js")}}"></script>
  <style>
      .headergroupbg{
          background-color:#1ABB9C;
          color:white;
      }
  </style>
<div class="modal-body" style="max-height: 500px; padding-bottom: 35px;">
<form method="POST" action="" accept-charset="UTF-8" name="frm-poc" id="frm-poc">
  <div class="row">
      <div class="col-md-12">
          <table class="table table-bordered table_sm table-condensed" id="medication_return_table">
              <thead class="headergroupbg" style="background-color:bg-green">

                <th>Medicine</th>
                <th>Requested Qty</th>
                <th>Billed Qty</th>
                <th>Requested At</th>
                <th>Approved At</th>
                <th>Remarks</th>
                <th>Action</th>
              </thead>
          </table>
      </div>
  </div>
</form>

</div>

<script>
    $(document).ready(function () {
        var patientid = $('#patient_id').val();
        var visitid = $('#visit_id').val();
        $('#medication_return_table').DataTable({
            "processing": true,
            "serverSide": true,
            "searching":false,
            "ajax": {
                "url": $('#base_url').val() + "/nursing/LoadMedicineReturnResults",
                "dataType": "json",
                "type": "GET",
                "data": {_token: "{{csrf_token()}}", poc_action:"all_poc", patient_id:patientid, visit_id:visitid},
                "error" : function(){  alert('error..');
                }
            },
            "columns": [

                { "data": "item_desc" },
                { "data": "requested_qty" },
                { "data": "approved_qty" },
                { "data": "created_at" },
                { "data": "approved_at" },
                { "data": "remarks" },
                { "data": "action" }
            ]
        });

    });

    $("#checklaball").on('click', function () {
        if ($('#checklaball').is(':checked')) {
            $('.lab_chk').prop('checked', 'checked');
        } else {
            $('.lab_chk').removeAttr('checked')
        }
    });

</script>
