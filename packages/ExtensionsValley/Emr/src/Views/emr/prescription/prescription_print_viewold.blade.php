<!-- .css -->
<style>
  #doctorHeadClass p:first-child{
      margin: 0 !important;
      padding: 0 !important;
  }
</style>
<style type="text/css" media="print">
  @page {
  margin: 15px;
  }
  table {
    font-size : 13px;
  }
</style>
<!-- .css -->
<!-- .box-body -->
<div class="box-body">
    @php
    $hospital_header_disable_in_prescription = 0;
    $department_head = (!empty($doctor['prescription_department_head'])) ? $doctor['prescription_department_head'] : '';
    $medication_footer_text = (!empty($doctor['medication_footer_text'])) ? $doctor['medication_footer_text'] : '';
    $medication_note = (!empty($doctor['medication_note'])) ? $doctor['medication_note'] : '';
    @endphp
  <!-- .col-md-12 -->
    <div class="col-md-12" @if($hospital_header_disable_in_prescription == 1) style="margin-top:3cm;" @endif>
    @if(!empty($hospitalHeader))
     {!!$hospitalHeader!!}
    <br>
    <hr>
    <br>
    @endif
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
              <td style='padding: 0px; vertical-align: middle; border-bottom: none' align='center' colspan="7"><strong><u>{!! $department_head !!}</u></strong>
              </td>
          </tr>
          {{-- <tr>
            <td colspan="7"><strong><u>PRESCRIPTION</u></strong></td>
          </tr> --}}
          <tr>
            <td colspan="7">&nbsp;</td>
          </tr>
          <tr>
            <td><strong>Patient Name</strong></td>
            <td align="center">:</td>
            <td>{{$patient[0]->patient_name}}</td>
            <td>&nbsp;</td>
            <td><strong>Date</strong></td>
            <td align="center">:</td>
            <td>
                {{$date}}
            </td>
          </tr>
          <tr>
            <td><strong>{{\WebConf::getConfig('op_no_label')}}</strong></td>
            <td align="center">:</td>
            <td>{{$patient[0]->uhid}}</td>
            <td>&nbsp;</td>
            <td><strong>Consultant</strong></td>
            <td align="center">:</td>
            <td>{{$doctor['doctor_name']}}</td>
          </tr>
          <tr>
            <td><strong>Gender/Age</strong></td>
            <td align="center">:</td>
            <td>{{$patient[0]->gender}} / {{$patient[0]->age}}</td>
            <td>&nbsp;</td>
            <td><strong>Department</strong></td>
            <td align="center">:</td>
            <td>{{$doctor['doctor_speciality']}}</td>
          </tr>
          <tr style="display: none;">
            <td colspan="4">&nbsp;</td>
            <td><strong>Token</strong></td>
            <td align="center">:</td>
            <td>token_no</td>
          </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td style="border-bottom: 1px solid #000;">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>

      <tr style="display: none;">
        <td>Allergies : Allergies </td>
      </tr>
      <tr style="display: none;">
        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>
        @if(!empty($encounterDetails->diagnosis))
          DIAGNOSIS : {{$encounterDetails->diagnosis}}
        @endif
          <span style="float:right">
            @if(!empty($intend_type) && $intend_type == 1)
                Regular
            @elseif(!empty($intend_type) && $intend_type == 2)
                New Admission
            @elseif(!empty($intend_type) && $intend_type == 3)
                Emergency
            @else
                Discharge
            @endif
          </span>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <table style="border-collapse:collapse;" width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
              <tr>
                <td width="25%" valign="top" style="padding: 20px 5px 5px 5px;">
                  @if(!empty($medication_note))
                    {!!$medication_note!!}
                  @endif
                </td>
                <td width="75%" valign="top" style="padding: 20px 5px 5px 5px;">
                  <table style="border-collapse:collapse;" width="100%" border="1" cellspacing="0" cellpadding="0">
                    @if($is_iv_fluid)
                      <tr>
                        <td><strong>SL#</strong></td>
                        <td><strong>Generic Name</strong></td>
                        <td><strong>Drug Name</strong></td>
                        <td><strong>Quantity</strong></td>
                        <td><strong>Rate</strong></td>
                        <td><strong>Remarks</strong></td>
                      </tr>
                    @elseif($consumable_ind == 1)
                      <tr>
                        <td><strong>SL#</strong></td>
                        <td><strong>Drug Name</strong></td>
                        <td><strong>Quantity</strong></td>
                        <td><strong>Remarks</strong></td>
                      </tr>
                    @else
                      <tr>
                        <td><strong>SL#</strong></td>
                        <td><strong>Generic Name</strong></td>
                        <td><strong>Drug Name</strong></td>
                        <td><strong>Dose</strong></td>
                        <td><strong>Frequency</strong></td>
                        <td><strong>Route</strong></td>
                        <td><strong>Days</strong></td>
                        {{-- <td><strong>Medication Period</strong></td> --}}
                        <td><strong>Qty</strong></td>
                        {{-- <td><strong>Start At</strong></td> --}}
                        <td><strong>Remarks</strong></td>
                      </tr>
                    @endif
                    <?php $i = 1;?>
                    @foreach($data as $pres)
                    <?php

                    /********* Medication Period **********/
                    $start_date = "";
                    if(!empty($pres->start_date)){
                      $start_date = \Carbon::parse($pres->start_date)->format('d-m-y');
                    }

                    $stop_date = "";
                    if(!empty($pres->stop_date)){
                      $stop_date = \Carbon::parse($pres->stop_date)->format('d-m-y');
                    }
                    /********* Medication Period **********/

                    $medicine = "";
                    if(!empty($pres->medicine_code)){
                        $medicine = $pres->item_desc;
                    }else{
                        $medicine = $pres->medicine_name;
                    }

                    ?>
                    @if($is_iv_fluid)
                    <tr>
                      <td> {{$i}} </td>
                      <td> {{ $pres->generic_name }}</td>
                      <td> {{ $medicine }}</td>
                      <td valign="top"> {!! $pres->quantity!!} </td>
                      <td><center>{!! $pres->iv_rate!!}</center> </td>
                      <td><center>{!! $pres->notes!!} </center></td>
                    </tr>
                    @elseif($consumable_ind == 1)
                    <tr>
                      <td> {{$i}} </td>
                      <td> {{ $medicine }}</td>
                      <td valign="top"> {!! $pres->quantity!!} </td>
                      <td><center>{!! $pres->notes!!} </center></td>
                    </tr>
                    @else
                    <tr>
                      <td> {{$i}} </td>
                      <td> {{ $pres->generic_name }}</td>
                      <td> {{ $medicine }}</td>
                      <td valign="top"> {!! $pres->dose!!} </td>
                      <td><center>{!! $pres->frequency!!}</center> </td>
                      <td><center> {!!$pres->route!!}</center> </td>
                      <td><center> {!! $pres->duration!!} </center> </td>
                      {{-- <td><center>{!! $start_date !!} to {!! $stop_date !!}</center></td> --}}
                      <td><center> {!! $pres->quantity!!}</center> </td>
                      {{-- <td><center> {!! $pres->iv_start_at !!}</center> </td> --}}
                      <td><center>{!! $pres->notes!!} </center></td>
                    </tr>
                    @endif
                    <?php $i++;?>
                    @endforeach
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      @if(!empty($encounterDetails->special_notes))
      <tr>
        <td>
          Special Notes :  {{$encounterDetails->special_notes}}
          <span style="float:right"> Date of Next review : {{$encounterDetails->next_review_date}} </span>
          <div class="clearfix"></div>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      @endif
      @if(!empty($encounterDetails->reason_for_visit))
      <tr>
        <td> Reason :  {{$encounterDetails->reason_for_visit}} </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      @endif
      <tr>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0">
              <tr>

                <td colspan="2" style="float:right;margin-right: 10px;">
                  <u>Electronic Prescription</u><br/>

                    <strong>
                      {{$doctor['doctor_name']}}<br/>
                      {{$doctor['qualification']}} ( {{$doctor['doctor_speciality']}} )<br/>
                    </strong>

                </td>
              </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td style="text-align: right;">SIGN</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="border-bottom: 1px solid #000;">&nbsp;</td>
      </tr>
      @if(!empty($medication_footer_text))
      <tr>
        <td>{{$medication_footer_text}}</td>
      </tr>
      @endif
    </table>
  </div>
  <!-- .col-md-12 -->
</div>
<!-- .box-body -->

