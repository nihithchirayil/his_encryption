
    <!----medications------------------->
    <style>
        #casMedTable td{
            background: white !important;
        }
    </style>

    <div class="col-md-12" id="casMedTable" >
        <h5 style="background-color:darkcyan;color:white;padding-left:7px;padding-top:3px;padding-bottom:3px;margin-bottom:1px;">{{ucwords(strtolower($doctor_name))}}
            <span style="float:right;font-size:11px;margin-right:4px;">{{date('M-d-Y h:i A',strtotime($created_at))}}</span></h5>

      <table border="1" width="100%;" class="table table-bordered table_sm theadfix_wrapper no-padding" style="border-collapse: collapse;table-layout:fixed;font-size:12px !important;color:black !important;">
        <thead style="font-weight:600;font-size:13px !important;">
        @if($is_iv_fluid)
          <tr>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="5%">SlNo</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="25%">Generic Name</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="25%">Drug Name</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="10%">Quantity</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="10%">Rate</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="25%">Remarks</td>
          </tr>
        @elseif($consumable_ind == 1)
          <tr>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="10%">SlNo</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="50%">Drug Name</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="10%">Quantity</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="30%">Remarks</td>
          </tr>
        @else
          <tr>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="5%">SlNo</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="34%">Drug Name</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="6%">Dose</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="10%">Frequency</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="8%">Route</td>
            @if ($sub_code=='KEYHOLE')
            <td style="text-align:center;background:aquamarine !important;color:black;" width= "7%">Duration</td>
            @else
       <td style="text-align:center;background:aquamarine !important;color:black;" width= "7%">Days</td>
            @endif
            <td style="text-align:center;background:aquamarine !important;color:black;" width="5%">Qty</td>
            <td style="text-align:center;background:aquamarine !important;color:black;" width="25%">Remarks</td>
          </tr>
        @endif
        </thead>
        <?php $i = 1;?>
        @foreach($data as $pres)
        <?php

        /********* Medication Period **********/
        $start_date = "";
        if(!empty($pres->start_date)){
          $start_date = \Carbon::parse($pres->start_date)->format('d-m-y');
        }

        $stop_date = "";
        if(!empty($pres->stop_date)){
          $stop_date = \Carbon::parse($pres->stop_date)->format('d-m-y');
        }
        /********* Medication Period **********/

        $medicine = "";
        if(!empty($pres->medicine_code)){
            $medicine = $pres->item_desc;
        }else{
            $medicine = $pres->medicine_name;
        }

        ?>
        @if($is_iv_fluid)
        <tr>
          <td style="text-align:center">{{$i}}</td>
          <td style="text-align:left">{{$pres->generic_name}}</td>
          <td style="text-align:left">{{$medicine}}</td>
          <td style="text-align:center">{!! $pres->quantity!!}</td>
          <td style="text-align:center">{!! $pres->iv_rate!!}</td>
          <td style="text-align:left">{!! $pres->notes!!}</td>
        </tr>
        @elseif($consumable_ind == 1)
        <tr>
          <td style="text-align:center">{{$i}} </td>
          <td style="text-align:left">{{ $medicine }}</td>
          <td style="text-align:center" valign="top"> {!! $pres->quantity!!} </td>
          <td style="text-align:left">{!! $pres->notes!!} </td>
        </tr>
        @else
        <tr>
          <td style="text-align:center"> {{$i}} </td>
          <td style="text-align:left;padding:3px;">
            <div>
              {{ $medicine }}</div>
              <div style="font-size:11px;">Generic : {{ $pres->generic_name }}
            </div>
          </td>
          <td style="text-align:center"> {!! $pres->dose!!} </td>
          <td style="text-align:center">{!! $pres->frequency!!} </td>
          <td style="text-align:center"> {!!$pres->route!!} </td>
          @if ($sub_code=='KEYHOLE')
          @if ($pres->duration_unit_id == 1)
          @php
              $duration_unit = 'Days';
          @endphp
          @elseif($pres->duration_unit_id == 2)
          @php
              $duration_unit = 'Week';
          @endphp
          @elseif($pres->duration_unit_id == 3)
          @php
              $duration_unit = 'Month';
          @endphp
         @else
          @php
              $duration_unit = 'Days';
          @endphp
          @endif
          <td><center> {!! $pres->duration!!} {{ $duration_unit }} </center> </td>
          @else
          <td><center> {!! $pres->duration!!} </center> </td>
          @endif
          {{-- <td>{!! $start_date !!} to {!! $stop_date !!}</td> --}}
          <td style="text-align:center"> {!! $pres->quantity!!} </td>
          {{-- <td> {!! $pres->iv_start_at !!} </td> --}}
          <td style="text-align:left">{!! $pres->notes!!} </td>
        </tr>
        @endif
        <?php $i++;?>
        @endforeach
      </table>
    </div>
    </div>

