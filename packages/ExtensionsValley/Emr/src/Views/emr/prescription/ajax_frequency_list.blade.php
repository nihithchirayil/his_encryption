
<!---- Common frequency table ----->
<div class="col-md-6">
    <h5 style="margin-left:10px;">Available List</h5>
    <div class="col-md-12" style="margin-bottom: 15px;">
        <input type="text" name="searchFreq" class="form-control" id="searchFreq" placeholder="search..">
    </div>
    <div class=" theadscroll padding_sm" style="position: relative; height: 350px;">
        <table id="common_freq_table" class ="table table-striped theadfix_wrapper table-sm">
            <thead>
                <tr style="background-color: #0f996b; color:white;padding:0px;">
                    <th>Frequency Name</th>
                    <th style="text-align:center;">Value</th>
                </tr>
            </thead>
            <tbody class="globalFreqList">
                @if(isset($frequency_list) && count($frequency_list) > 0)
                        @foreach($frequency_list as $ind => $data)
                            <tr id="{{$data->frequency}}" style="padding:0px !important;" data-frequency-name="{{$data->frequency}}" data-frequency-id="{{$data->freq_id}}" data-frequency-value="{{$data->value}}">
                                <td style="text-align:left;padding:0px !important;">&nbsp;
                                  @if($data->user_freq_id =='')<input id="{{$data->freq_id}}" name="chk[]" class="{{$data->frequency}}"
                                       type="checkbox" onclick="InsertFrequency(this)"  data-id="undefined" value="{{$data->value}}">&nbsp;&nbsp;@else<input id="{{$data->freq_id}}"
                                       type="checkbox" onclick="deleteuserFrequency('{{$data->freq_id}}')"   data-id="undefined"  checked>&nbsp;&nbsp;@endif
                                    <label>{{$data->frequency}}</label>
                                </td>
                                <td style="padding:0px !important;text-align:center" class="freq_row_val">{{$data->value}}</td>
                            </tr>
                        @endforeach
                @else
                        <tr style=" background-color: #f2f2f2;"><td colspan="2" style="text-align: center; padding: 6px;">No Data Found</td></tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<!---- Common frequency table end ----->



<!---- user frequency table ----->
<div class="col-md-6">
    <h5 style="margin-left:10px;">Selected List</h5>

    <div class="col-md-12" style="margin-bottom: 15px;">
        <input type="text" name="searchSelectedFreq" class="form-control" id="searchSelectedFreq" placeholder="search..">
    </div>
    <div class=" theadscroll padding_sm" style="position: relative; height: 350px;">
            <table id="selected_freq_table" class ="table table-striped theadfix_wrapper table-sm">
                <thead>
                    <tr style="background-color: #0f996b; color:white;padding:0px;">
                        <th style="text-align: left;">Frequency Name</th>
                        <th style="text-align: center;">Value</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody class="globalSelectedFreqList">
                @if(isset($selected_freq_list) && count($selected_freq_list) > 0)
                    @foreach($selected_freq_list as $ind => $data)


                        <tr id="{{$data->frequency_id}}" style="padding:0px !important;height:27px;" data-frequency-name="{{$data->frequency}}" data-frequency-id="{{$data->frequency_id}}" data-frequency-value="{{$data->value}}">
                            <td style="text-align: left;padding:0px !important;">
                               &nbsp;<label>{{$data->frequency}}</label>
                            </td>
                            <td style="padding:0px !important;text-align: center;" class="freq_row_val">{{$data->value}}</td>
                            <td style="text-align: center;padding:0px !important;"> <input type="hidden"  name="frequency_id[]" value="{{$data->frequency_id}}"><button type="button" style="padding-top:3px;" class="btn btn-sm btn-danger delete-frequency-row"><i class="fa fa-trash"></i> Remove</button></td>
                        </tr>
                    @endforeach
                @else
                    <tr style=" background-color: #f2f2f2;"><td  colspan="8" id="hiderow" style="text-align: center; padding: 6px;">No Data Found</td></tr>
                @endif
                </tbody>
            </table>
    </div>
</div>
<!---- user frequency table ends ----->


