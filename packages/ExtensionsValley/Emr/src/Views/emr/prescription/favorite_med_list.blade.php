@if(sizeof($data_list) > 0)
  @foreach($data_list as $key => $value)
  @php
    $id = (!empty($value->id)) ? $value->id : "";
    $group_id = (!empty($value->group_id)) ? $value->group_id : "";
    $medicine_name = (!empty($value->medicine_name)) ? $value->medicine_name : "";
    $duration = (!empty($value->duration)) ? $value->duration : "";
    $frequency = (!empty($value->frequency)) ? $value->frequency : "";
    $route = (!empty($value->route)) ? $value->route : "";
    $item_code = (!empty($value->item_code)) ? $value->item_code : "";
    $dose = (!empty($value->dose)) ? $value->dose : "";
    $quantity = (!empty($value->quantity)) ? $value->quantity : "";
    $notes = (!empty($value->notes)) ? $value->notes : "";
  @endphp
    <tr>
        <td width="2%">
            <input type="checkbox" class="fav_check" name="favitemid[]" value="{{$id}}">
        </td>
        <td>
            {{$medicine_name}}
            <input type="hidden" class="form-control" name="fav_medicine_name[]" value="{{$medicine_name}}">
            <input type="hidden" class="form-control" name="fav_medicine_code[]" value="{{$item_code}}">
        </td>
        <td>
            {{$dose}}
            <input type="hidden" class="form-control" name="fav_dose[]" value="{{$dose}}">
        </td>
        <td>
            {{$quantity}}
            <input type="hidden" class="form-control" name="fav_quantity[]" value="{{$quantity}}">
        </td>
        <td>
            {{$frequency}}
            <input type="hidden" class="form-control" name="fav_frequency[]" value="{{$frequency}}">
        </td>
        <td>
            {{$duration}}
            <input type="hidden" class="form-control" name="fav_duration[]" value="{{$duration}}">
        </td>
        <td>
            {{$route}}
            <input type="hidden" class="form-control" name="fav_route[]" value="{{$route}}">
        </td>
        <td>
            {{$notes}}
            <input type="hidden" class="form-control" name="fav_notes[]" value="{{$notes}}">
        </td>
        <td style="text-align:center;">
            <i style="color:red" onclick="removeMedicineFromBookmark('{{$id}}', this);" class="fa fa-times-circle" aria-hidden="true"></i>
        </td>
    </tr>
  @endforeach
@else
<tr style="text-align: center;">
    <td colspan="9">No medicines found!</td>
</tr>
@endif
