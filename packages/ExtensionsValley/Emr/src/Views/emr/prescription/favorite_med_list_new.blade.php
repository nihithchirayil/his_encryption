<input type="hidden" id="addItemTogrp_grpId" value="{{ $group_id }}">
<div class="col-md-12 theadscroll" style="height: 56vh">
    <div class="col-md-3 no-padding padding_xs" style="float: right;">
        <button type="button" class="btn btn-block btn-info" onclick="addFavouriteToPrescription();">
            Add to Prescription <i class="fa fa-check"></i></button>
    </div>
    <table id=""
        class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed">
        <thead>
            <tr class="tableRowHead">
                <th width="3%"><input type="checkbox" class="prescription_bookmark_item_checkbox_all"></th>
                <th width="5%">Sl.No.</th>
                <th width="25%">Medicine</th>
                <th width="15%">Quantity</th>
                <th width="5%"><i onclick="addNewMediceneGrp()" class="fa fa-plus"></i></th>
            </tr>
        </thead>
        <tbody class="prescription_list_table_body_grp">
            @if (count($data_list) > 0)
            @php $i=1; @endphp
            @foreach ($data_list as $each)
            <tr class='row_class_addGrp'>
                <td><input type='checkbox' class='prescription_bookmark_item_checkbox' data-row-id='{{ $each->id }}'></td> 
                <td style='text-align: center; line-height: 25px;' class='row_count_class_addGrp'>{{ $i }}.</td>
                <td><input type='text' autocomplete='off' name='medicine_name' readonly
                        class='form-control bottom-border-text borderless_textbox item_description'
                        data-item-code='{{ $each->item_code }}' value="{{ $each->medicine_name }}" />
                </td>
                <td><input type='text' autocomplete='off'
                        class='form-control bottom-border-text borderless_textbox quantity'
                        value="{{$each->quantity}}" /></td>
                <td class='text-center'><i onclick='removeAddGrpRow(this)'
                        class='fa fa-times red removeAddGrpRowBtn'></i></td>
            </tr>
            @php $i++; @endphp
            @endforeach
            @endif
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<div class="col-md-1 pull-right " style="margin-right: 18px;">
    <button style="padding: 3px 3px" type="button" id="saveNewItemsToGroupBtn" onclick="saveNewItemsToGroup(this)"
        class="btn btn-brown"><i class="fa fa-save padding_sm"></i>Save</button>
</div>

<!-- medicine search list div start -->
<div class="medicine-list-div-row-listing-new" style="display: none;">
    <a style="float: left;" class="close_btn_dialog close_btn_med_search-new">X</a>
    <div class="theadscroll" style="position: relative; height:300px;">
        <table class="table no-margin theadfix_wrapper table-border-radius-top table-striped table_sm">
            <thead>
                <tr class="table_header_common">
                    <th width="26%">Medicine</th>
                    <th width="18%">Generic Name</th>
                    <th width="23%">Therapeutic Category</th>
                    <th width="23
                    %">Therapeutic Subcategory</th>
                    <th width="10%">Stock</th>
                    {{-- <th width="10%">Price</th> --}}
                </tr>
            </thead>
            <tbody class="list-medicine-search-data-row-listing-new">
            </tbody>
        </table>
    </div>
</div>
<!-- medicine search list div end -->