@php
  $hospital_header_disable_in_prescription = 0;
  $department_head = (isset($doctor->prescription_department_head)) ? $doctor->prescription_department_head : '';
  $medication_footer_text = (isset($doctor->medication_footer_text)) ? $doctor->medication_footer_text : '';
  $medication_note = (isset($doctor->medication_note)) ? $doctor->medication_note : '';
  $next_review_date = (!empty($encounterDetails->next_review_date)) ? date('d-M-Y', strtotime($encounterDetails->next_review_date)) : '';
@endphp
<div class="col-md-12" @if($hospital_header_disable_in_prescription == 1) style="margin-top:3cm;" @endif>
  <!-- Hospital Header -->
  @if(!empty($hospitalHeader))
     {!!$hospitalHeader!!}
  @endif
  <!-- Hospital Header -->

  <table border="1" cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; font-size: 12px;">
    <tbody>
      <tr style="border-bottom: none">
          <td style="padding: 0px; vertical-align: middle; border-bottom: none" width="15%" align="center" colspan="2">
          <h3>{!! $department_head !!}</h3>
          </td>
      </tr>
      <tr>
        <td style="padding: 0px; border-top: none; font-size: 11px;" width="15%" align="left" colspan="2">
          <table width="100%">
            <tbody>
              <tr>
                <td><strong>NAME:</strong></td>
                <td>{{$patient[0]->patient_name}}</td>
                <td> <strong>{{\WebConf::getConfig('op_no_label')}}:</strong></td>
                <td>{{$patient[0]->uhid}}</td>
              </tr>
              <tr>
                <td><strong> GENDER/AGE:</strong> </td>
                <td>{{$patient[0]->gender}} / {{$patient[0]->age}}</td>
                <td><strong>DATE :</strong></td>
                <td>{{$date}}</td>
              </tr>
              <tr style="display: none;">
                <td colspan="4" style="font-size:11px;">
                  <strong> Allergies if any : </strong> <br>
                </td>
                <td style="font-size:11px;" style="display:none;">
                  <strong> Token : </strong>&nbsp;
                  <b>141</b>
                </td>
              </tr>
              @if($encounterDetails->diagnosis)
              <tr>
                <td colspan="5" style="font-size:11px;">
                <strong> Diagnosis : </strong> <br/>
                {{$encounterDetails->diagnosis}}
                </td>
              </tr>
              @endif
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td width="25%" valign="top" style="padding: 20px 5px 5px 5px; font-size: 11px;">
          <span>
            @if(!empty($medication_note))
              {!!$medication_note!!}
            @endif
          </span>
        </td>
        <td width="75%" valign="top" style="padding: 20px 5px 5px 5px;">
            <span style="float:right">
              @if(!empty($intend_type) && $intend_type == 1)
                  Regular
              @elseif(!empty($intend_type) && $intend_type == 2)
                  New Admission
              @elseif(!empty($intend_type) && $intend_type == 3)
                  Emergency
              @else
                  Discharge
              @endif
            </span>
            <br><br>
            <table border="1" cellspacing="0" cellpadding="2" width="100%" style="border-collapse: collapse; font-size: 13px;">
              @if($is_iv_fluid)
                <tr>
                  <td><strong>SL#</strong></td>
                  <td><strong>Generic Name</strong></td>
                  <td><strong>Drug Name</strong></td>
                  <td><strong>Quantity</strong></td>
                  <td><strong>Rate</strong></td>
                  <td><strong>Remarks</strong></td>
                </tr>
              @elseif($consumable_ind == 1)
                <tr>
                  <td><strong>SL#</strong></td>
                  <td><strong>Drug Name</strong></td>
                  <td><strong>Quantity</strong></td>
                  <td><strong>Remarks</strong></td>
                </tr>
              @else
                <tr>
                  <td><strong>SL#</strong></td>
                  <!-- <td><strong>Generic Name</strong></td> -->
                  <td width="40%" ><strong>Drug Name</strong></td>
                  <td><strong>Dose</strong></td>
                  <td><strong>Frequency</strong></td>
                  <td><strong>Route</strong></td>
                  @if ($sub_code=='KEYHOLE')
                  <td><strong>Duration</strong></td>
                  @else
                  <td><strong>Days</strong></td> 
                  @endif
                 
                  {{-- <td><strong>Medication Period</strong></td> --}}
                  <td><strong>Qty</strong></td>
                  {{-- <td><strong>Start At</strong></td> --}}
                  <td><strong>Remarks</strong></td>
                </tr>
              @endif
              <?php $i = 1;?>
              @foreach($data as $pres)
              <?php

              /********* Medication Period **********/
              $start_date = "";
              if(!empty($pres->start_date)){
                $start_date = \Carbon::parse($pres->start_date)->format('d-m-y');
              }

              $stop_date = "";
              if(!empty($pres->stop_date)){
                $stop_date = \Carbon::parse($pres->stop_date)->format('d-m-y');
              }
              /********* Medication Period **********/

              $medicine = "";
              if(!empty($pres->medicine_code)){
                  $medicine = $pres->item_desc;
              }else{
                  $medicine = $pres->medicine_name;
              }

              ?>
              @if($is_iv_fluid)
              <tr>
                <td> {{$i}} </td>
                <td> {{ $pres->generic_name }}</td>
                <td> {{ $medicine }}</td>
                <td valign="top"> {!! $pres->quantity!!} </td>
                <td><center>{!! $pres->iv_rate!!}</center> </td>
                <td><center>{!! $pres->notes!!} </center></td>
              </tr>
              @elseif($consumable_ind == 1)
              <tr>
                <td> {{$i}} </td>
                <td> {{ $medicine }}</td>
                <td valign="top"> {!! $pres->quantity!!} </td>
                <td><center>{!! $pres->notes!!} </center></td>
              </tr>
              @else
              <tr>
                <td> {{$i}} </td>
                <!-- <td> {{ $pres->generic_name }}</td> -->
                <td width="40%"> <div>{{ $medicine }}</div><div style="font-size:11px;">Generic : {{ $pres->generic_name }} </div> </td>
                <td><center> {!! $pres->dose!!} </center></td>
                <td><center>{!! $pres->frequency!!}</center> </td>
                <td><center> {!!$pres->route!!}</center> </td>
                @if ($sub_code=='KEYHOLE')
                @if ($pres->duration_unit_id == 1)
                @php
                    $duration_unit = 'Days';
                @endphp
                @elseif($pres->duration_unit_id == 2)
                @php
                    $duration_unit = 'Week';
                @endphp
                @elseif($pres->duration_unit_id == 3)
                @php
                    $duration_unit = 'Month';
                @endphp
               @else
                @php
                    $duration_unit = 'Days';
                @endphp
                @endif
                  
              
                <td><center> {!! $pres->duration!!} {{ $duration_unit }} </center> </td> 
                @else
                <td><center> {!! $pres->duration!!} </center> </td> 
                @endif
                {{-- <td><center> {!! $pres->duration!!} </center> </td> --}}
                {{-- <td><center>{!! $start_date !!} to {!! $stop_date !!}</center></td> --}}
                <td><center> {!! $pres->quantity!!}</center> </td>
                {{-- <td><center> {!! $pres->iv_start_at !!}</center> </td> --}}
                <td><center>{!! $pres->notes!!} </center></td>
              </tr>
              @endif
              <?php $i++;?>
              @endforeach
            </table>
            <br>
            <table width="100%" style="font-size:12px;">
              <tr>
                <td width="49%">
                  @if($encounterDetails->reason_for_visit)
                      <strong style="vertical-align:top; float: left;">Reason For Visit</strong>
                      <br/><br/>
                      @if($encounterDetails->reason_for_visit)
                        {!!trim(nl2br($encounterDetails->reason_for_visit))!!}
                      @endif
                      <br/>
                  @endif
                </td>
                <td  width="49%">
                  @if(!empty($encounterDetails->special_notes))
                    <strong style="vertical-align:top; float: left;">Special Notes</strong>
                        <br /><br />
                        @if(!empty($encounterDetails->special_notes))
                          {!!trim(nl2br($encounterDetails->special_notes))!!}
                        @endif
                        <br/>
                  @endif
                </td>
              </tr>
           </table>
            <table width="100%" style="font-size:12px;">
              <tr>
                <td width="49%">
                  @if(!empty($next_review_date))
                    <strong style="font-size: 11px;">REVIEW ON: </strong> &nbsp;
                    <input value="{{$next_review_date}}" style="border-left: none; border-top: none; border-right: none; border-bottom: none;" type="text">
                  @endif
                </td>
                <td  width="49%">
                  @if(!empty($doctor['doctor_name']))
                  <span style="float: right; margin-right: 10px; font-size: 11px;">
                    {{$doctor['doctor_name']}}<br/>
                    {{$doctor['qualification']}} ( {{$doctor['doctor_speciality']}} )
                    <br/>
                    <br/>
                    <strong>SIGNATURE:</strong>
                  </span>
                  @endif
                </td>
              </tr>
            </table>
            <br><br>
          </td>
      </tr>
      @if(!empty($medication_footer_text))
      <tr>
        <td style='padding: 3px; vertical-align: middle;' width='15%' align='center' colspan="2">
          <span style="float: right; margin-right: 180px; font-size: 11px;"><strong>{!! $medication_footer_text !!}</strong></span>
        </td>
      </tr>
      @endif
    </tbody>
  </table>
</div>

@if($sub_code=='DAYAVAL')
  <img src="{{URL::to('/')}}/packages/extensionsvalley/dashboard/images/profile/ourservicescopy.png" style="width: 1289px;height: 211px;margin-left: -91px;" alt="logo"/>

@endif
