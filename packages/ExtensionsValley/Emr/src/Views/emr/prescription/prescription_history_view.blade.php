<!-- <div class="theadscroll always-visible" style="position: relative; height: 157px;"> -->
<div style="position: relative; height: 157px;">
  <table class="table no-margin theadfix_wrapper table_sm no-border">
  <thead>
      <tr style="background-color: aliceblue;">
          <th style="width: 8%;">Date</th>
          <th style="width: 10%;">Request No.</th>
          <th style="width: 15%;">Doctor</th>
          <th style="width: 13%;">Created By</th>
          <th style="width: 5%;">Type</th>
          @if (isset($intend_paid_status) && $intend_paid_status == '1')
          <th style="width: 10%;">Payment Status</th>
          @endif
          <th style="width: 14%;"></th>
          <th style="width: 25%;" colspan="5"></th>
      </tr>
  </thead>
  <tbody>
    @if(!empty($medications_list))
      @for ($i = 0; $i < sizeof($medications_list); $i++)
          @php
            $id = $medications_list[$i]->id;
            $medication_detail_list = [];
            $medication_detail_content = '';
            $prescription_id = $medications_list[$i]->prescription_id;
            if($medications_list[$i]->intend_type == 1){
            $type ='Regular';
            }elseif($medications_list[$i]->intend_type == 2){
            $type = 'New Admission';
            }elseif($medications_list[$i]->intend_type == 3){
            $type = 'Emergency';
            }elseif($medications_list[$i]->intend_type == 4){
            $type = 'Discharge';
            }elseif($medications_list[$i]->intend_type == 5){
            $type = 'Outside';
            } else{
            $type = 'Regular';
            }
            $name = $medications_list[$i]->name;
            $consumable_ind = $medications_list[$i]->consumable_ind;
            $doctor_name = $medications_list[$i]->doctor_name;
            $medincine_count = $medications_list[$i]->medincine_count;
            $visit_status = $medications_list[$i]->visit_status;
            $doctor_name_ = (strlen($medications_list[$i]->doctor_name) > 18) ? substr($medications_list[$i]->doctor_name,0,18).'..' : $medications_list[$i]->doctor_name;
            $name = (strlen($medications_list[$i]->name) > 18) ? substr($medications_list[$i]->name,0,18).'..' : $medications_list[$i]->name;
            $created = (!empty($medications_list[$i]->created_at)) ? ExtensionsValley\Emr\CommonController::getDateFormat($medications_list[$i]->created_at,'d-M-Y h:i A') : '';
            $status = ($medications_list[$i]->status == 1) ? "" : "Draft";
            $billconverted_status = isset($medications_list[$i]->billconverted_status) ? $medications_list[$i]->billconverted_status : "Draft";

            //$medication_detail_list = \DB::table("patient_medication_detail")
              //      ->where('patient_medication_detail.head_id',$id)
                //    ->leftJoin('product','product.item_code','=','patient_medication_detail.medicine_code')
              //      ->select('patient_medication_detail.medicine_name,product.item_desc')->get();

            $medication_detail_list = \DB::select("select pmd.medicine_name,p.item_desc, pmd.id as intend_id from patient_medication_detail pmd
            left join product p on p.item_code = pmd.medicine_code
            where pmd.head_id = $id and pmd.deleted_at is null");

            $intend_id = 0;
            $paid_status = 0;
            $paid_status_name = '';
            if(!empty($medication_detail_list)){
                // echo '<pre></pre>';
                // print_r($medication_detail_list);
                // exit;
                $intend_id = $medication_detail_list[0]->intend_id ?? 0;
                foreach($medication_detail_list as $medicine_data){
                    if($medicine_data->item_desc == ''){
                        $medication_detail_content = $medication_detail_content.$medicine_data->medicine_name.', </br>';
                    }else{
                        $medication_detail_content = $medication_detail_content.$medicine_data->item_desc.', </br>';
                    }
                }
                if (isset($intend_paid_status) && $intend_paid_status == '1') {
                  $paid_status = ExtensionsValley\Emr\CommonController::getIntendPaidStatus($patient_id, $intend_id);
                  $paid_status_name = intval($paid_status) == 1 ? 'Paid' : 'Not Paid';
                }
            }

          @endphp
          <tr @if($consumable_ind == 1) ondblclick="consumablePrescriptionEditMode('{{$id}}','copy');" @else ondblclick="prescriptionEditMode('{{$id}}','copy');" @endif >
              <td title="{{$created}}" style="white-space:nowrap; ">{{$created}}</td>
              <td title="{{$prescription_id}}">{{$prescription_id}}</td>
              <td title="{{$doctor_name}}">{{$doctor_name_}}</td>
              <td title="{{$name}}">{{$name}}</td>
              <td title="{{$type}}">{{$type}}</td>
              @if (isset($intend_paid_status) && $intend_paid_status == '1')
                <td>{{$paid_status_name}}</td>
              @endif
              <td>
                <span style="padding: 4px;border-radius: 4px;" class="badge_purple_bg">{{$visit_status}}</span>
                @if(!empty($status))
                    <span style="padding: 4px;border-radius: 4px;" class="bg-orange">{{$status}}</span>
                @endif
                @if($consumable_ind == 1)
                  <span style="padding: 4px;border-radius: 4px;" class="bg-orange">CONSUMABLE</span>
                @else
                  <span style="padding: 4px;border-radius: 4px;" class="bg-orange">DRUG</span>
                @endif

              </td>
              <td>
                <button data-toggle="popover" data-trigger="hover" data-content="{{$medication_detail_content}}" class="btn btn-block light_purple_bg popoverData" rel="popover" data-placement="left" onclick="loadPescriptionView('{{$id}}')"><i class="fa fa-eye"></i> View ({{$medincine_count}})</button>


              </td>
              <td>
                <button class="btn btn-block light_purple_bg" title="Print" onclick="printPescriptionView('{{$id}}')"><i class="fa fa-print"></i> Print</button>
              </td>
              <td>
                <button class="btn btn-block light_purple_bg" title="Copy" @if($consumable_ind == 1) onclick="consumablePrescriptionEditMode('{{$id}}','copy');" @else onclick="prescriptionEditMode('{{$id}}','copy');" @endif ><i class="fa fa-clone"></i> Copy</button>
              </td>
              <td>
                @if($billconverted_status == 0)
                <button class="btn btn-block light_purple_bg" title="Edit" @if($consumable_ind == 1) onclick="consumablePrescriptionEditMode('{{$id}}','edit');" @else onclick="prescriptionEditMode('{{$id}}','edit');" @endif ><i class="fa fa-edit"></i> Edit</button>
                @endif
              </td>
              @if($billconverted_status == 0)
              <td>
                <button class="btn btn-block light_purple_bg" title="Delete" onclick="prescriptionDeleteMode(this,'{{$id}}');"><i class="fa fa-trash"></i> Delete</button>
              </td>
              @endif
          </tr>
      @endfor
    @else
    <td colspan="6">No Data Found..!</td>
    @endif
  </tbody>
  </table>
</div>
@if(!empty($medications_list))

<div id="pagination">{{{ $medications_list->links() }}}</div>
@endif
{{-- <div class="clearfix"></div>
<div class="ht5"></div> --}}
{{-- <div class="col-md-12 text-right prescription_pagination">
    {{$medications_list->links('Emr::emr.prescription.prescription-pagintion')}}
</div> --}}
