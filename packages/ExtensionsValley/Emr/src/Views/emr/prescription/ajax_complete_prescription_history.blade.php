<div class="theadscroll always-visible" style="position: relative; min-height:300px;">
    <input type="hidden" id="patient_id" value="{{$patient_id}}"/>
    <input type="hidden" id="visit_id" value="{{$visit_id}}"/>
    <input type="hidden" id="encounter_id" value="{{$encounter_id}}"/>
    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <table class="table no-margin table-striped table-bordered table-condensed">
        <thead class="table_header_bg">
            <tr>
                <th><input type="checkbox" onclick="complete_prescription_select_all();" id="complete_prescription_select_all" name="complete_prescription_select_all"/></th>
                <th>Request No.</th>
                <th>Medicine</th>
                <th>Frequency</th>
                <th>Route</th>
                <th>Duration</th>
                <th>Quantity</th>
                <th>Remarks</th>
                <th>Status</th>
                <th>Created By</th>
                <th>Created On</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($medications_list))
            @for ($i = 0; $i < sizeof($medications_list); $i++)
                @php
                    $id = $medications_list[$i]->id;
                    $medicine_name = $medications_list[$i]->item_desc;
                    $medicine_code = @$medications_list[$i]->medicine_code? $medications_list[$i]->medicine_code : '';                    
                    $stop_flag = $medications_list[$i]->stop_flag;
                    $frequency = $medications_list[$i]->frequency;
                    $duration = $medications_list[$i]->duration;
                    $route = $medications_list[$i]->route;
                    $quantity = $medications_list[$i]->quantity;
                    $notes = $medications_list[$i]->notes;
                    $prescription_id = $medications_list[$i]->prescription_id;
                    $doctor_name = $medications_list[$i]->name;
                    $created = (!empty($medications_list[$i]->created_at)) ? ExtensionsValley\Emr\CommonController::getDateFormat($medications_list[$i]->created_at,'d-M-Y H:i:s') : '';
                    $status = ($medications_list[$i]->billconverted_status == 1) ? "Billed" : "Not Billed";
                @endphp
                <tr style="@if($stop_flag == 1) background:#ccd814; @elseif($stop_flag == 2) background:#f17025; @endif">
                    <td title="{{$prescription_id}}">
                        <input type="checkbox" class="complete_prescription" medi_code='{{$medicine_code}}' name="complete_prescription[]" value="{{$id}}"/>
                    </td>
                    <td title="{{$prescription_id}}">{{$prescription_id}}</td>
                    <td title="{{$medicine_name}}" style="text-align:left;">{{$medicine_name}}</td>
                    <td title="{{$frequency}}">{{$frequency}}</td>
                    <td title="{{$route}}">{{$route}}</td>
                    <td title="{{$duration}}">{{$duration}}</td>
                    <td title="{{$quantity}}">{{$quantity}}</td>
                    <td title="{{$notes}}" style="text-align:left;">{{$notes}}</td>
                    <td title="{{$status}}">{{$status}}</td>
                    <td title="{{$doctor_name}}">{{$doctor_name}}</td>
                    <td title="{{$created}}">{{$created}}</td>
                </tr>
            @endfor
            @else
            <td colspan="10">No Data Found..!</td>
            @endif
        </tbody>
    </table>
</div>
@if($medications_list)
<div id="pagination">{{{ $medications_list->links() }}}</div>
@endif
<div class="modal-footer">
    <div class="col-md-1 padding_xs">
        <span style="float:left">&nbsp;<i class="fa fa-square" style="color: #ccd814;"></i>&nbsp; Stopped</span>
    </div>
    <div class="col-md-4 padding_xs pull-right">
        <button class="btn bg-blue" type="button" name="btn_reorder_medication" id="btn_reorder_medication" onclick="addToMedication();" style="width:120px;"><i class="fa fa-arrow-down"></i> Add to Presription</button>

        <button class="btn bg-orange" type="button" name="btn_reorder_medication" id="btn_reorder_medication" onclick="reorderMedication();" style="width:120px;"><i class="fa fa-save"></i> Save Prescription</button>
    </div>
</div>
