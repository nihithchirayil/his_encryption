@if(sizeof($data) > 0)
  @foreach($data as $key => $value)
    <li id="favourite_group_{{$key}}">
      <div class="radio radio-success" style="padding-left:25px;">
        <input type="radio" name="favgroups" id="groupradio" value="{{$key}}">
        <label for="groupradio" title=""> {{$value}} </label>
        <a class="" onclick="delete_prescription_group('{{$key}}');" style="cursor: pointer;color:red;float:right;line-height:18px;padding-right: 15px;" >
            <i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;</a>
      </div>
    </li>
  @endforeach
@else
<li>
  <div>No Data Found.!</div>
</li>
@endif
