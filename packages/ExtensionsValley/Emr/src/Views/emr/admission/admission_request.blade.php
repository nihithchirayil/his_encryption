@php
  $patient_name = (!empty($patientDetails[0]->patient_name)) ? $patientDetails[0]->patient_name : "";
  $age = (!empty($patientDetails[0]->age)) ? $patientDetails[0]->age : "";
  $gender = (!empty($patientDetails[0]->gender)) ? $patientDetails[0]->gender : "";
  $uhid = (!empty($patientDetails[0]->uhid)) ? $patientDetails[0]->uhid : "";

  $doctor_name = (!empty($doctorDetails->doctor_name)) ? $doctorDetails->doctor_name : "";
  $speciality = (!empty($doctorDetails->speciality)) ? $doctorDetails->speciality : "";
  $doctor_speciality = (!empty($doctorDetails->doctor_speciality)) ? $doctorDetails->doctor_speciality : "";

  $adm_doc = (!empty($admDoctorDetails->id)) ? $admDoctorDetails->id : "";
  $adm_doc_name = (!empty($admDoctorDetails->doctor_name)) ? $admDoctorDetails->doctor_name : "";
@endphp

<div class="row" >

    <div class="col-md-10">
      <div class="col-md-5">
        <span>Doctor Name</span>
      </div>
      <div class="col-md-5">
        {{$doctor_name}}
      </div>
    </div>

    <div class="col-md-10">
      <div class="col-md-5">
        <span>Department</span>
      </div>
      <div class="col-md-5">
        {{$doctor_speciality}}
        <input type="hidden" class="form-control"  name="department_id" value="{{$speciality}}"  id="department_id">
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>{{\WebConf::getConfig('op_no_label')}}</span>
      </div>
      <div class="col-md-5">
        {{$uhid}}
        <input type="hidden" class="form-control"  name="uhid_no" value="{{$uhid}}" placeholder="UHID No" id="uhid_no">
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Patient Name</span>
      </div>
      <div class="col-md-5">
      {{ucfirst($patient_name)}}
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Gender</span>
      </div>
      <div class="col-md-5">
        {{$gender}}
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Age</span>
      </div>
      <div class="col-md-5">
        {{$age}}
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Admitting Doctor</span>
      </div>
      <div class="col-md-5">
        {{$adm_doc_name}}
        <input type="hidden" class="form-control" readonly  name="admitting_doctor_id" value="{{$adm_doc}}" placeholder="Admitting Doctor" id="admitting_doctor_id">
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Date of Admission *</span>
      </div>
      <div class="col-md-5">
        <input type="text" class="form-control adm-datepicker"  name="admission_date" value="{{date('M-d-Y')}}" placeholder="Admission Date" id="admission_date">
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Admission Type</span>
      </div>
      <div class="col-md-5">
        <label for="emergency">Emergency</label>
        <input type="radio"  name="type"  value="EM" class="" id="emergency" placeholder="">&nbsp;&nbsp;
        <label for="general">General</label>
        <input type="radio" checked="checked" name="type"  value="General" class="" id="general" placeholder="">
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Referring Doctor (if any)</span>
      </div>
      <div class="col-md-5">
        <input class="form-control" style="margin-bottom: 10px;" type="text" name="referring_doctor" value="" placeholder="Referring Doctor" id="referring_doctor" autocomplete="off" title="Doctor Name">
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Provisional Diagnosis</span>
      </div>
      <div class="col-md-5">
        <textarea class="form-control"  name="provisional_diagnosis" id="provisional_diagnosis"></textarea>
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Precautions</span>
      </div>
      <div class="col-md-5">
        <input type="text" class="form-control" autofocus="off" name="precautions" value="" placeholder="Precautions" id="precautions">
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Class of Bed *</span>
      </div>
      <div class="col-md-5">
        {!! Form::select('priority', $admPriority, "", ['class' => 'form-control','placeholder' => 'Select Admission Priority','id' => 'priority', 'style' => 'color:#555555; padding:4px 12px;']) !!}
      </div>
    </div>


    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Expected number of days in the hospital</span>
      </div>
      <div class="col-md-5">
        <input type="text" class="form-control"  name="no_of_days" value="" placeholder="Number of Days" id="no_of_days">
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Name of the Interventions/Procedure planned</span>
      </div>
      <div class="col-md-5">
        <textarea class="form-control"  name="procedure" id="procedure"></textarea>
      </div>
    </div>

    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Tentative date of Surgery/Procedure</span>
      </div>
      <div class="col-md-5">
        <input type="text" class="form-control adm-datepicker" name="surgery_date" value="" placeholder="Date" id="surgery_date">
      </div>
    </div>


    <div class="col-md-10" style="padding-top:5px;">
      <div class="col-md-5">
        <span>Any special requests/concerns</span>
      </div>
      <div class="col-md-5">
        <input type="text" class="form-control"  name="spl_req" value="" placeholder="" id="spl_req">
      </div>
    </div>

</div>
<input type="hidden" name="adm_req_loaded" id="adm_req_loaded" value="1">
<script>
  $(document).ready(function() {
    $(".adm-datepicker").datetimepicker({format: 'MMM-DD-YYYY'});
  });
</script>


