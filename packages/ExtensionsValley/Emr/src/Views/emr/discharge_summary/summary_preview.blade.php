<div id="summary_content" contenteditable="true" style="margin:15px;">
    @php
        $hospital_header = \DB::table('company')->where('id',1)->value('hospital_header');

    @endphp
    @if($disable_summary_hospital_header == 0)
        {!! $hospital_header !!}
    @endif
    {{-- @include('Emr::emr.discharge_summary.hospital_header') --}}
    <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='margin:0px;width: 100%; font-size: 14px; margin: 0; border-collapse: collapse;@if($disable_summary_hospital_header == 0)margin-top:10px; @else margin-top:160px;@endif'>
    {{-- <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='margin:0px;width: 100%; font-size: 14px; margin: 0; border-collapse: collapse;@if($disable_summary_hospital_header == 0)margin-top:10px; @else margin-top:10px;@endif'> --}}
        <thead>
            <tr style='background-color: #dddddd;'>
                <th style='padding-top: 0px 0px;' colspan='4' class='text-center vendorListHeading'>
                    <h4 id='label_department_name' style='margin: 0;font-size:15px'>{!! strtoupper($department_name) or null !!}</h4>
                </th>
            </tr>
        </thead>
    </table>
    <span id="department_dr_head" style="margin:0px;"></span>
    <span id="summary_patient_header_section" contenteditable="true"></span>
    <div id="summary_content_section" contenteditable="true" style="min-height:330px !important;@if(env('APP_NAME') == 'JACOBS')font-size: 16px; @else font-size: 14px; @endif"></div>
    <span style="margin-top:10px;" id="summary_footer_section"></span>
</div>

