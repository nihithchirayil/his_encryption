

    <div class="col-md-12" style="font-size: 14px;">

        @php
            $summary_order = \DB::table('summary_field_order')
                        ->where('status',1)
                        ->whereNull('deleted_at')
                        ->orderBy('order_no','ASC')
                        ->select('field_name','field_label','order_no')
                        ->get();
            $summary_fields = [];
            foreach($summary_order as $data1){
                $summary_fields[$data1->order_no]['field_name'] = $data1->field_name;
                $summary_fields[$data1->order_no]['field_label'] = $data1->field_label;
            }

            // dd($summary_fields);
        @endphp

        @foreach($summary_fields as $key => $value)
            @if(isset($value['field_name'])&& !empty($value['field_name']) && trim($value['field_name']) !='')
                @if(isset($data[$value['field_name']]) && $data[$value['field_name']] !=' ')
                <div id="{{$value['field_name']}}_data" style="margin-top:2px;margin-bottom: 2px">
                    <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>{!!ucwords($value['field_label'])!!}</u></b></span></p>
                    <p id="{{$value['field_name']}}_data_content">
                        {!! trim($data[$value['field_name']]) !!}
                    </p>
                </div>
                @endif
            @endif
        @endforeach



        {{-- @if(!empty($data['diagnosis']) && trim($data['diagnosis']) !='')
        <div id='diagnosis_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>DIAGNOSIS </u></b></span></p>
            <p id="diagnosis_data_content">{!! trim($data['diagnosis'])!!}</p>
        </div>
        @endif



        @if(!empty($data['allergies']) && trim($data['allergies']) != '')
        <div id='allergies_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>ALLERGIES</u></b></span></p>
            <p id="allergies_data_content">{!! trim($data['allergies'])!!}</p>
        </div>
        @endif
        @if(!empty($data['surgery']) && trim($data['surgery']) !='')
        <div id='surgery_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>SURGERY </u></b></span></p>
            <p id="surgery_data_content">{!! trim($data['surgery'])!!}</p>
        </div>
        @endif
        @if(!empty($data['history_findings']) && trim($data['history_findings']) !='')
        <div id='history_findings_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>HISTORY AND PHYSICAL FINDINGS </u></b></span></p>
            <p id="history_findings_data_content">{!! trim($data['history_findings'])!!}</p>
        </div>
        @endif
        @if(!empty($data['investigation']) && trim($data['investigation']) !='')
        <div id='investigation_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>INVESTIGATION </u></b></span></p>
            <p id="investigation_data_content">{!! trim($data['investigation'])!!}</p>
        </div>
        @endif
        @if(!empty($data['surgery_findings_treatments']) && trim($data['surgery_findings_treatments']) !='')
        <div id='surgery_findings_treatments_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>SURGERY FINDINGS AND TREATMENTS </u></b></span></p>
            <p id="surgery_findings_treatments_data_content" >{!! trim($data['surgery_findings_treatments'])!!}</p>
        </div>
        @endif
        @if(!empty($data['treatment_progress']) && trim($data['treatment_progress']) !='')
        <div id='treatment_progress_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>TREATMENT AND PROGRESS </u></b></span></p>
            <p id="treatment_progress_data_content" >{!! trim($data['treatment_progress']) !!}</p>
        </div>
        @endif
        @if(!empty($data['course_in_hospital']) && trim($data['course_in_hospital']) !='')
        <div id='course_in_hospital_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>COURSE IN HOSPITAL </u></b></span></p>
            <p id="course_in_hospital_data_content" >{!! trim($data['course_in_hospital'])!!}</p>
        </div>
        @endif
        @if(!empty($data['discussion']) && trim($data['discussion']) !='')
        <div id='discussion_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>DISCUSSION </u></b></span></p>
            <p id="discussion_data_content" >{!! trim($data['discussion'])!!}</p>
        </div>
        @endif
        @if(!empty($data['plan']) && trim($data['plan']) !='')
        <div id='plan_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>PLAN </u></b></span></p>
            <p id="plan_data_content" >{!! trim($data['plan'])!!}</p>
        </div>
        @endif
        @if(!empty($data['advice_on_discharge']) && trim($data['advice_on_discharge']) !='')
        <div id='advice_on_discharge_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>ADVICE ON DISCHARGE </u></b></span></p>
            <p id="advice_on_discharge_data_content" >{!! trim($data['advice_on_discharge'])!!}</p>
        </div>
        @endif
        @if(!empty($data['urgent_care']) && trim($data['urgent_care']) !='')
        <div id='urgent_care_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>CONDITIONS REQUIRING URGENT CARE </u></b></span></p>
            <p id="urgent_care_content" >{!! trim($data['urgent_care'])!!}</p>
        </div>
        @endif --}}




        {{-- @if(!empty($data['presenting_symptoms']) && trim($data['presenting_symptoms']) !='')
        <div id='presenting_symptoms_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>PRESENTING SYMPTOMS </u></b></span></p>
            <p id="presenting_symptoms_content" >{!! trim($data['presenting_symptoms'])!!}</p>
        </div>
        @endif

        @if(!empty($data['physical_findings']) && trim($data['physical_findings']) !='')
        <div id='physical_findings_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>PHYSICAL FINDINGS</u></b></span></p>
            <p id="physical_findings_content" >{!! trim($data['physical_findings'])!!}</p>
        </div>
        @endif
        @if(!empty($data['clinical_impression']) && trim($data['clinical_impression']) !='')
        <div id='clinical_impression_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>CLINICAL IMPRESSION</u></b></span></p>
            <p id="clinical_impression_content" >{!! trim($data['clinical_impression'])!!}</p>
        </div>
        @endif
        @if(!empty($data['treatment_and_course']) && trim($data['treatment_and_course']) !='')
        <div id='treatment_and_course_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>TREATMENT AND COURSE</u></b></span></p>
            <p id="treatment_and_course_content" >{!! trim($data['treatment_and_course'])!!}</p>
        </div>
        @endif
        @if(!empty($data['final_diagnosis']) && trim($data['final_diagnosis']) !='')
        <div id='final_diagnosis_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>FINAL DIAGANOSIS</u></b></span></p>
            <p id="final_diagnosis_content" >{!! trim($data['final_diagnosis'])!!}</p>
        </div>
        @endif
        @if(!empty($data['condition_at_discharge']) && trim($data['condition_at_discharge']) !='')
        <div id='condition_at_discharge_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>CONDITION AT DISCHARGE</u></b></span></p>
            <p id="condition_at_discharge_content" >{!! trim($data['condition_at_discharge'])!!}</p>
        </div>
        @endif
        @if(!empty($data['recommendations']) && trim($data['recommendations']) !='')
        <div id='recommendations_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>RECOMMENDATIONS</u></b></span></p>
            <p id="recommendations_content" >{!! trim($data['recommendations'])!!}</p>
        </div>
        @endif
        @if(!empty($data['follow_up_details']) && trim($data['follow_up_details']) !='')
        <div id='follow_up_details_data' style="margin-top:2px;margin-bottom: 2px">
            <p style="margin-bottom:1px;  !important;">  <span style="font-size:16px;"><b><u>FOLLOW UP DETAILS</u></b></span></p>
            <p id="follow_up_details_content" >{!! trim($data['follow_up_details'])!!}</p>
        </div>
        @endif --}}

    </div>

    @if(isset($data['followup_date']) && ($data['followup_date']!=''))
    <div style="margin-top:2px;margin-bottom:10px">
        <strong style="margin-bottom:1px;  !important;"><b>Follow Up Date</b></strong><br>
          <span> {!! date('M-d-Y',strtotime($data['followup_date'])) !!}</span>
    </div>
    @endif
