@if(isset($patient_data))


<table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
    <thead>
        <tr>
            <th id="discharge_summary_main_header" style='padding: 5px 0;' colspan='4' class='text-center'>
                <h4 style='margin: 0;' class="summary_label"><b>DISCHARGE SUMMARY</b></h4>
            </th>
        </tr>
    </thead>
    <tbody style=''>
        <tr>
            <td style=' padding-left:5px;;text-align:left; '>Patient Name</td>
            <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data['patient_name'] !!}</td>
            <td style=' padding-left:5px;;text-align:left;'>UHID No.</td>
            <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data['uhid'] !!}</td>
        </tr>
        <tr>
            <td style=' padding-left:5px;;text-align:left; '>Age &amp; Gender</td>
            <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data['age']   !!} / {!! $patient_data['gender']   !!}</td>
            <td style=' padding-left:5px;; text-align:left;'> IP No. </td>
            <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data['admission_no']   !!}</td>
        </tr>
        <tr>
            <td rowspan='2' style=' padding-left:5px;;text-align:left;'> Address</td>
            <td rowspan='2' style=' padding-left:5px;;text-align:left;'>{!! $patient_data['address']   !!}</td>
            <td style=' padding-left:5px;; text-align:left;'>DOA </td>
            <td style=' padding-left:5px;; text-align:left;' id="summary_admit_date_time">@if($patient_data['admission_date']!='')
                {!! date('M-d-Y',strtotime($patient_data['admission_date'])) !!}
                @endif
            </td>
        </tr>
        <tr>
            <td style=' padding-left:5px;;text-align:left; '>DOD</td>
            <td style=' padding-left:5px;; text-align:left;' id="summary_discharge_date_time">
                @if($patient_data['discharge_datetime'] != '')
                    {!! $patient_data['discharge_datetime'] !!} {!!$patient_data['time_of_discharge']!!}
                @endif
            </td>
        </tr>

        <tr>
            <td style=' padding-left:5px;;text-align:left; '>Current Room</td>
            <td style=' padding-left:5px;;text-align:left; '>{{$patient_data['bed_name']}}</td>
            <td style=' padding-left:5px;;text-align:left; '>DOS</td>
            <td style=' padding-left:5px;; text-align:left;' id="summary_surgery_date_time">
            </td>

        </tr>
    </tbody>
</table>
@endif
