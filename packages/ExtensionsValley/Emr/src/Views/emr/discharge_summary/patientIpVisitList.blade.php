<?php

    if (isset($visitData) ) {

        if(sizeof($visitData)>0){
            ?>
                <table class="table table-responsive theadfix_wrapper table-hover floatThead-table">
                    <thead>
                    <tr class="headergroupbg">
                        <th>Admission No</th>
                        <th>Admit Date</th>
                        <th>Discharge Date</th>
                    </tr>
                 </thead>
           <?php
           foreach ($visitData as $visits) {

//                ?>

                 <tr style="cursor: pointer;"  onclick="selectPatientVisits('{!!$visits->visit_id!!}')">
                    <td>{!!$visits->fin!!}</td>
                    <td>{!!date('M-d-Y', strtotime($visits->admission_date))!!}</td>
                    <td>
                        @if($visits->discharge_datetime !='')
                            {!! date('M-d-Y h:i A',strtotime($visits->discharge_datetime))!!}
                        @endif
                    </td>
                </tr>



                <?php
            }?>
            </table>
                <?php
        } else {
            echo " No admissions found! ";
        }
    }

?>
