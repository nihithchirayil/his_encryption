<style>

    .summary_template_pagination .pagination li a, .summary_template_pagination .pagination li span{
          padding: 2px 8px;
          font-size: 11px;
      }

  </style>
  <?php $i = 1;?>
  <div class="presc_theadscroll" style="height: 258px; position: relative;">
      <table class="table table-condensed table-striped presc_theadfix_wrapper">
      <thead>
          <tr class="headergroupbg">
             <th style="text-align:left;">Template Name</th>
             <th style="text-align:left;">Category</th>
             <th style="text-align:left;">Created by</th>
             <th style="text-align:center">Activity</th>
          </tr>
      </thead>
      <tbody id="PrescListDt">

      @if(isset($res) && !empty($res))
      @foreach ($res as $value)
      <tr class="his_apply_btn">

          <td style="text-align:left;">{{ $value->template_name }}</td>
          <td style="text-align:left;">{{ $value->category }}</td>
          <td style="text-align:left;">{{ $value->created_by }}</td>
          <td style="text-align:center">
              <button type="button" class="btn bg-primary" onclick="fetch_discharge_summary_template({{$value->template_id}},'{!!$value->category!!}')" title="Fetch to template"><i class="fa fa-copy"></i></button>
              <button type="button" class="btn bg-green-active" onclick="edit_discharge_summary_template({{$value->template_id}});" title="Edit template"><i class="fa fa-pencil"></i></button>
              <button type="button" class="btn bg-red-active" title="Delete template"  onclick="delete_summary_template({{$value->template_id}});" ><i class="fa fa-trash"></i></button>
              <button type="button" class="btn bg-orange" title="Template preview"  onclick="preview_summary_custom_template({{$value->template_id}});" ><i class="fa fa-eye"></i></button>
          </td>
      </tr>
      <?php $i++;?>
      @endforeach
      @else
      <tr><td colspan="8"><center>NO RESULT FOUND</center></td></tr>
      @endif
      </tbody>
      </table>
      <div class="row summary_template_pagination" style="margin:0;padding:0;float:right;">
       {!! $res->render(); !!}
      </div>
  </div>
