@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/ip-op-slide.css")}}" rel="stylesheet">


@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col"  role="main" style="min-height: 1124px;">

        <div class="row codfox_container">

   <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="form-group control-required">
                        <label>User</label>
                         <input type="text" name="search_user" id="search_user" style="cursor: pointer;" autocomplete="off" class="form-control" placeholder="Type at least 3 characters">
                        <input type="hidden" name="search_user_id_hidden" value="">
                                            <!-- User List -->

                                            <div class="user-list-div" style="display: none; cursor: pointer;">
                                                <a style="float: left;" class="close_btn_user_search">X</a>
                                                <div class=" user_theadscroll" style="position: relative;">
                                                  <table id="UserTable"  class="table table-bordered no-margin table_sm table-striped user_theadfix_wrapper">
                                                      <thead>
                                                        <tr class="light_purple_bg">
                                                          <th colspan="2">User</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody id="ListUserSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          <!-- User List -->
                    </div>
                </div>



            <div class="clearfix"></div>
            <div class="ht10"></div>


            <div class="theadscroll always-visible" style="position: relative; height: auto">
                <div class="col-md-12 no-padding" style="scroll-behavior: smooth;">


                    <div class="col-md-12 padding_sm emr_sub">
                                         <style>

    .horizontal { display: inline;}
    .table_header_bg.header_normal_padding>th{
        padding: 8px !important;

    }
</style>


        <div class="tab-content">
             <div class="col-md-12" style="padding: 5px;">



                @include('Emr::emr.prescription_favourite.prescription_favourite_lists')

        
            <div id="drug" class="tab-pane fade in active">
               
                <div class="col-md-12" style="padding: 2px;">




                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                   
                            <table  id="search_med_type" style="display: table; width:98%;" class="table no-margin table_sm table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Search Medicine by :</td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="m_s_type_brand" value="brand" name="m_search_type" checked="">
                                                <label for="m_s_type_brand"> Brand </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="m_s_type_generic" value="generic" name="m_search_type">
                                                <label for="m_s_type_generic"> Generic </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="m_s_type_both" value="both" name="m_search_type">
                                                <label for="m_s_type_both"> Both </label>
                                            </div>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <table  id="prescription_list" style="display: table; width:98%;" class="table no-margin no-border">
                                <thead>
                                    <tr class="bg-default prescription_entry_head">
                                        <th>Medicine</th>
                                        <th>Dose</th>
                                        <th>Frequency</th>
                                        <th>Days</th>
                                        <th>Quantity</th>
                                        <th>Route</th>
                                        <th colspan="2">Instructions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>

                                        <td width="30%">
                                            <input type="text" name="search_medicine" style="cursor: pointer;" autocomplete="off" class="form-control" placeholder="Type at least 3 characters">

                                             <input type="hidden" name="edit_id" value="">
                                              <input type="hidden" name="get_grp_id" value="">
                                            <input type="hidden" name="search_item_code_hidden" value="">
                                            <input type="hidden" name="search_item_name_hidden" value="">
                                            <input type="hidden" name="search_item_price_hidden" value="">

                                            <!-- Medicine List -->
                                            <div class="medicine-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_med_search">X</a>
                                                <div class=" presc_theadscroll" style="position: relative;">
                                                  <table id="MedicationTable"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                      <thead>
                                                        <tr class="light_purple_bg">
                                                          <th colspan="2">Medicine</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody id="ListMedicineSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          <!-- Medicine List -->

                                        </td>
                                        <td width="10%">
                                            <input type="text" name="search_dose" style="cursor: pointer;" autocomplete="off" id="search_dose" class="form-control">
                                        </td>
                                        <td width="10%">
                                            <input type="text" name="search_frequency" style="cursor: pointer;" autocomplete="off" id="search_frequency" class="form-control">

                                            <input type="hidden" name="search_freq_value_hidden" value="">
                                            <input type="hidden" name="search_freq_name_hidden" value="">

                                            <!-- Frequency List -->
                                            <div class="frequency-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_freq_search">X</a>
                                                <div class="freq_theadscroll" style="position: relative;">
                                                  <table id="FrequencyTable"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                      {{-- <thead>
                                                        <tr class="light_purple_bg">
                                                          <th>Frequency</th>
                                                        </tr>
                                                      </thead> --}}
                                                      <tbody id="ListFrequencySearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          <!-- Frequency List -->

                                        </td>
                                        <td width="10%">
                                            <input type="text" name="search_duration" style="cursor: pointer;" autocomplete="off" id="search_duration" class="form-control">
                                        </td>
                                        <td width="7%">
                                            <input type="text" name="search_quantity" style="cursor: pointer;" autocomplete="off" id="search_quantity" class="form-control">
                                        </td>
                                        <td width="15%">
                                            <input type="text" name="search_route" style="cursor: pointer;" autocomplete="off" id="search_route" class="form-control">

                                            <!-- Route List -->
                                            <div class="route-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_route_search">X</a>
                                                <div class="route_theadscroll" style="position: relative;">
                                                  <table id="RouteTable"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                      {{-- <thead>
                                                        <tr class="light_purple_bg">
                                                          <th>Route</th>
                                                        </tr>
                                                      </thead> --}}
                                                      <tbody id="ListRouteSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          <!-- Route List -->

                                        </td>
                                        <td width="15%">
                                            <input type="text" name="search_instructions" style="cursor: pointer;" autocomplete="off" id="search_instructions" class="form-control">

                                            <!-- Instruction List -->
                                            <div class="instruction-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_instruction_search">X</a>
                                                <div class="instruction_theadscroll" style="position: relative;">
                                                  <table id="InstructionTable"  class="table table-bordered no-margin table_sm table-striped presc_theadfix_wrapper">
                                                      <tbody id="ListInstructionSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- Instruction List -->

                                        </td>
                                        <td width="3%" align="center"><button class="btn light_purple_bg" onclick="addNewMedicine();" >Save</button></td>
                                    </tr>
                                   
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                         
                            <div class="theadscroll" style="position: relative; height: 244px;">
                                <table border="1" style="width: 100%;height: 15%;border:1px solid;border-collapse: collapse !important" class="table  table_sm  table-bordered" id="medicine-listing-table">
                                    <tbody id="medicinefav-listing-table">

                                    </tbody>
                                </table>
                            </div>
                          

                       
                </div>
            </div>


        </div>

    

                    </div>

                </div>
            </div>

        </div>


        <input type="hidden" id="base_url" value="{{URL::to('/')}}">
        <input type="hidden" id="c_token" value="{{csrf_token()}}">


<div id="group_add_modal" class="modal fade " role="dialog">
    <div class="modal-dialog" style="width:30%">

      <div class="modal-content">
        <div class="modal-header" style="background:#26b99a; color:#ffffff;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
          <h4 class="modal-title">Add Group</h4>
        </div>
        <div class="modal-body" id="group_add_data" >
            <label >Group name</label>
                            <input  class="form-control" value=""  type="text" placeholder="Group Name" id="groupname" name="groupname">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn bg-green" onclick="SaveGroup()">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
</div>

    </div>

</div>


@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>

<!-- Prescription -->
<script src="{{asset("packages/extensionsvalley/emr/js/prescription_favourite.js")}}"></script>

<script src="{{asset("packages/extensionsvalley/jquery-mousewheel-master/jquery.mousewheel.min.js")}}"></script>

<script type="text/javascript">


</script>
@endsection
