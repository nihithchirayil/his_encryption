
<!-- Medicine Favorite List -->
<div class="favorite-pres-list-popup" style="display: none;">
   
    <div class="col-md-12 no-padding" style="border: 1px solid #ccc;">
        <div class="col-md-2 no-padding">
            <div class="group_list theadscroll always-visible" style="height: 460px;border-right: 1px solid #ccc;">
        <button class="btn light_purple_bg" style="float:right;" title="Add Group" onclick="addGroup()">Add Group</button>
         
           <table id="fav_groups" class="fav-groups-row table table-bordered no-margin table_sm table-striped">  
           </table>
            </div>
        </div>
        <div class="col-md-10 no-padding">

           <form id="presc-data-form" action="">
                <table id="FavTableList"  class="table table-bordered no-margin table_sm table-striped ">
                    <thead>
                        <tr class="table_header_bg header_normal_padding">
                            <th >Medicine</th>
                            <th>Dose</th>
                             <th>Frequency</th>
                              <th>Days</th>
                            <th>Quantity</th>
                            <th>Route</th>
                            <th>Instruction</th>
                             <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="ListMedicineFavorites" >

                    </tbody>
                </table>
               </form>
          
        </div>
    </div>
</div>
<!-- Medicine Favorite List -->
