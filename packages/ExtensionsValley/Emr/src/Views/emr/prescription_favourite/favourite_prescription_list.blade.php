@if(sizeof($data_list) > 0)
  @foreach($data_list as $key => $value)
  @php
    $id = (!empty($value->id)) ? $value->id : "";
    $group_id = (!empty($value->group_id)) ? $value->group_id : "";
    $medicine_name = (!empty($value->medicine_name)) ? $value->medicine_name : "";
    $duration = (!empty($value->duration)) ? $value->duration : "";
    $frequency = (!empty($value->frequency)) ? $value->frequency : "";
    $route = (!empty($value->route)) ? $value->route : "";
    $item_code = (!empty($value->item_code)) ? $value->item_code : "";
    $dose = (!empty($value->dose)) ? $value->dose : "";
    $quantity = (!empty($value->quantity)) ? $value->quantity : "";
    $notes = (!empty($value->notes)) ? $value->notes : "";
    $frequency_value = (!empty($value->frequency_value)) ? $value->frequency_value : "";


  @endphp
    <tr id="{{$id}}">
        <td style="height:30px;cursor: pointer;">
            {{$medicine_name}}
            <input type="hidden" class="form-control" name="fav_medicine_name[]" value="{{$medicine_name}}">
            <input type="hidden" class="form-control" name="fav_medicine_code[]" value="{{$item_code}}">
            <input type="hidden" class="form-control" name="fav_frequency_value[]" value="{{$frequency_value}}">
            <input type="hidden" class="fav_check"  name="favitemid[]" value="{{$id}}">
        </td>
        <td style="height:30px;cursor: pointer;">
            {{$dose}}
            <input type="hidden" class="form-control" name="fav_dose[]" value="{{$dose}}">
        </td>
        <td style="height:30px;cursor: pointer;">
            {{$frequency}}
            <input type="hidden" class="form-control" name="fav_frequency[]" value="{{$frequency}}">
        </td>
        <td style="height:30px;cursor: pointer;">
            {{$duration}}
            <input type="hidden" class="form-control" name="fav_duration[]" value="{{$duration}}">
        </td>
        <td style="height:30px;cursor: pointer;">
            {{$quantity}}
            <input type="hidden" class="form-control" name="fav_quantity[]" value="{{$quantity}}">
        </td>
        
        
        <td style="height:30px;cursor: pointer;">
            {{$route}}
            <input type="hidden" class="form-control" name="fav_route[]" value="{{$route}}">
        </td>
        <td style="height:30px;cursor: pointer;">
            {{$notes}}
            <input type="hidden" class="form-control" name="fav_notes[]" value="{{$notes}}">
        </td>
        <td style="height:30px;cursor: pointer;">
            <button class="btn btn-success edit-medicine-row" title="Edit" type="button"><i  class="fa fa-edit"></i></button> &nbsp;<button title="Delete" class="btn  btn-danger delete-medicine-row"  type="button"><i  class="fa fa-trash-o"></i></button>
        </td>
    </tr>
  @endforeach
@else
<tr style="text-align: center;">
    <td style="height:30px;cursor: pointer;" colspan="8" id="td_1">No Data Found.!</td>
</tr>
@endif
