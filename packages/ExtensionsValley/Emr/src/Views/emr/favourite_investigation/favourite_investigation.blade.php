@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
<style>
.bottom-border-text{
    border: none !important;
    border-bottom: 2px solid rgb(200, 200, 200) !important;
    box-shadow: none;
}
.row{
    margin-top:10px;
}
.bg-radio_grp{
    background-color:#b9b9b9 !important;
    color:white !important;
    margin-right:6px !important;
}
.bg-radio_grp.active{
    background-color:#36a693 !important;
    color:white !important;
    margin-right:6px !important;
    box-shadow: 3px 2px 3px #55ff79;
}

.ajaxSearchBox{
        z-index: 9999 !important;
        max-height:320px !important;
        overflow: hidden !important;
        margin-top:16px !important;
    }
.ajaxSearchBox>li{
    font-size:10px !important;
    font-weight: 600 !important;
    font-family: Arial !important;
}
.liHover{
    background-color:rgb(143, 183, 250) !important;
}

.grp_table>tbody>tr>td{
    padding:3px !important;
}
.panel-info>.panel-heading {
    color: #ffffff;
    background-color: #00aaff;
    border-color: #00aaff;
}
label{
    font-size: 12px;
    font-weight: 600;
    color:darkslategrey;
}

#doctor_selection_block{
    display: block;
}

#nursing_station_block{
    display: none;
}

.select2-container{
    box-shadow: none !important;
    border-bottom:2px solid #ccc;
}
</style>

@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title text-right">
                        Favourite Investigations
                    </h3>
                </div>
            </div>
        </div>

        <input type="hidden" name="edit_id" value="">
        <input type="hidden" name="get_grp_id" value="">
        <input type="hidden" id="base_url" value="{{url('/')}}">
        <input type="hidden" id="c_token" value="{{csrf_token()}}">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">
                                <strong>
                                    Select User Type
                                </strong>
                            </label>
                            <br>
                            <div class="col-md-1">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="inv_user_type" id="inv_user_type1" value='1' checked="checked">
                                    <label class="form-check-label" for="inv_user_type1">
                                    Doctor
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="inv_user_type" id="inv_user_type2" value='2'>
                                    <label class="form-check-label" for="inv_user_type2">
                                    Nurse
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="doctor_selection_block">
                            <div class="col-md-6">
                                <label class="control-label">
                                    <strong>
                                        Select Doctor
                                    </strong>
                                </label>
                                {{Form::select('doctor_id',$users,null,['class'=>'form-control select2 bottom-border-text','id'=>'doctor_id','placeholder'=>'select'])}}
                            </div>
                        </div>

                        <div class="col-md-12" id="nursing_station_block">
                            <div class="col-md-6">
                                <label class="control-label">
                                    <strong>
                                        Select Nursing Station
                                    </strong>
                                </label>
                                {{Form::select('nursing_station',$nursing_stations,null,['class'=>'form-control select2 bottom-border-text','id'=>'nursing_station','placeholder'=>'select'])}}
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="btn-group btn-group-toggle gen_exam_acne" data-toggle="buttons" style="padding-top:24px;">
                                    <label class="btn bg-radio_grp active">
                                        <input type="radio" id="m_s_type_lab" name="i_search_type" checked autocomplete="off" value="lab"> Lab
                                    </label>
                                    <label class="btn bg-radio_grp">
                                        <input type="radio" id="m_s_type_radiology" name="i_search_type" autocomplete="off" value="radiology"> Radiology
                                    </label>
                                    <label class="btn bg-radio_grp">
                                        <input type="radio" id="m_s_type_procedure" name="i_search_type" autocomplete="off" value="procedure"> Procedure
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">
                                    <strong>
                                        Select Investigation
                                    </strong>
                                </label>
                                <input type="text" autocomplete="off" id="search_inv_item" name="search_inv_item"
                                style="cursor: pointer;" autocomplete="off" class="form-control bottom-border-text" placeholder="">
                                <input type="hidden" name="search_service_code_hidden" value="">
                                <input type="hidden" name="search_type_hidden" value="">

                                <div id="invAjaxDiv" class="ajaxSearchBox" style="width:130px;"></div>

                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="raw" style="margin-top:10px;">
                        <div class="col-md-12">
                            <!---Favorite Groups-->
                            <div class="col-md-3 box-body" style="padding:10px;border-radius:5px;">
                                <div class="col-md-12" style="background-color: deepskyblue;
                                color: white;margin-bottom:15px;">
                                    <h5>Groups</h5>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-10">
                                        <input autocomplete="off" class="form-control bottom-border-text" value="" type="text" placeholder="Group Name" id="groupname" name="groupname">
                                    </div>
                                    <div class="col-md-2" style="padding-left:6px;">
                                        <button onclick="saveGroup();" class="btn bg-green btn-sm" id="add_group" type="button">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <div class="col-md-12 theadscroll" style="height:320px;position:relative;" id="group_list">
                                        <table class="table theadfix_wrapper table-striped table-sm grp_table">
                                            <tbody id="group_list_data">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!--Favorite Investigations-->
                            <div class="col-md-9 box-body" style="padding:10px;border-radius:5px;">
                                <div class="col-md-12" style="background-color: deepskyblue;
                                color: white;">
                                    <h5>Favourite Investigations</h5>
                                </div>
                                <div class="col-md-12 no-padding theadscroll" style="height:357px;position:relative;" id='inv_list'>
                                    <table class="table theadfix_wrapper table-striped table-sm">
                                        <thead style="background-color:white;">
                                        <tr>
                                            <th width="10%"></th>
                                            <th width="60%">Investigation</th>
                                            <th width="20%">Type</th>
                                            <th width="10%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="inv_list_data">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('javascript_extra')
    <script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
    <script src="{{asset("packages/extensionsvalley/emr/js/favorite_investigation.js")}}"></script>
    <script type="text/javascript">
    </script>
@endsection
