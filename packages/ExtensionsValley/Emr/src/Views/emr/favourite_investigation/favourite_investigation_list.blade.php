@if(sizeof($favorite_items) > 0)
    @foreach($favorite_items as $item)
    <tr>
        <td style="text-align:center;">
            <input type="checkbox" name="fav_item_list[]" id="fav_item_list_{{$item->id}}" value="{{$item->id}}"/>
        </td>
        <td>
            {{ucfirst(strtolower($item->service_desc))}}
        </td>
        <td>
            {{ucfirst(strtolower($item->type))}}
        </td>
        <td>
            {{-- delete button --}}
            <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deleteFavItem({{$item->id}})">
                <i class="fa fa-times-circle" aria-hidden="true"></i>
            </a>
        </td>
    </tr>
    @endforeach
@else
<tr>
    <td colspan="4" class="text-center">
        <h4>No Data Found</h4>
        <h4>Please Add Favourite Investigations</h4>
    </td>
</tr>
@endif
