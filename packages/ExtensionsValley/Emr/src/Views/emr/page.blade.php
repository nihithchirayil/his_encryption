<!DOCTYPE html>
<html lang="en">
@php
$license_expired_status = 0;
$date_difference = 0;
$current_date = date('Y-m-d');
$license_expiry_date = (env("LICENCE_EXPIRY_DATE")) ? base64_decode(env("LICENCE_EXPIRY_DATE")) : '';
if($license_expiry_date != ''){
    $date1=date_create($current_date);
    $date2=date_create($license_expiry_date);
    $diff=date_diff($date1,$date2);
    $date_difference = $diff->format("%a");
    if($current_date > date('Y-m-d', strtotime($license_expiry_date)) ){
        $license_expired_status = 1;
    }

}
@endphp

@php
$user_id = \Auth::user()->id;
$dr_status = 0;
$sql = "select g.name from user_group ug join groups g on g.id=ug.group_id where ug.user_id=$user_id and ug.status = 1 and g.status = 1 and g.deleted_at is null and g.name = 'DOCTOR'";
$res_users = \DB::select($sql);
if (count($res_users) > 0) {
    $dr_status = 1;
}
@endphp

@if($license_expired_status == 1)
<script type="text/javascript">
    localStorage.clear();
    window.location.href = "{{ route('extensionsvalley.admin.logout') }}";
</script>
@else

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }} | {{ \WebConf::get('site_name') }}</title>
    @if (\WebConf::get('fav_icon') != '')
        <link rel="shortcut icon" href="{{ URL::to('/') }}/{{ \WebConf::get('fav_icon') }}" />
    @endif

    <!-- Bootstrap -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/custom.min.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/core-admin.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/green.css') }}" rel="stylesheet">

    <!-- Select2 -->
    {{-- <link href="{{asset("packages/extensionsvalley/dashboard/css/select2.min.css")}}" rel="stylesheet"> --}}
    <link href="{{ asset('packages/extensionsvalley/default/css/select2.min.css') }}" rel="stylesheet">

    <!-- highcharts -->
    <link href="{{ asset('packages/extensionsvalley/highcharts/css/highcharts.css') }}" rel="stylesheet">

    <!-- bootstrap-progressbar -->

    <link href="{{ asset('packages/extensionsvalley/dashboard/plugins/font-awesome/css/font-awesome.min.css') }}"
        rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/dashboard/plugins/custom-input-icons/custom-input-icons.css') }}"
        rel="stylesheet">
    <link
        href="{{ asset('packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"
        rel="stylesheet">
    <link
        href="{{ asset('packages/extensionsvalley/dashboard/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}"
        rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    {{-- <link href="{{asset("packages/extensionsvalley/dashboard/css/specimen_stylesheet.css")}}" rel="stylesheet"> --}}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/timepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/bootstrap-select/css/bootstrap-multiselect.css') }}"
        rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/new_fonts/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/new_fonts/Poppins.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/datatables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/default/plugins/cute-alert-master/cute-alert-style.css') }}"
        rel="stylesheet">

    @yield('css_extra')

    <!-- jQuery -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/bootstrap.min.js') }}"></script>
    <!---overlay loading effect--->
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/bootbox.js') }}"></script>

    <script src="{{ asset('packages/extensionsvalley/bootstrap-select/js/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <script type="text/javascript">
        if (localStorage.getItem('loginstatus') == 0) {
            document.location.href = "{{ route('extensionsvalley.admin.login') }}";
        }
    </script>
</head>

<body id="left_sidebar_container" class="nav-sm">
    <div class="container body">
        <div class="main_container">
            @yield('content-header')
            @yield('content-area')
            <!-- Footer Starts-->
            @include('Dashboard::dashboard.partials.footer')
            <!-- Footer Ends-->
        </div>
    </div>
    <!-- iCheck -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/icheck.min.js') }}"></script>

    <!-- highcharts -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/plugins/highcharts/js/highcharts.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/plugins/highcharts/js/export-data.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/plugins/highcharts/js/exporting.js') }}"></script>

    <!-- Datatables -->
    <script src="{{ asset('packages/extensionsvalley/emr/datatables/datatables.min.js') }}"></script>

    <!-- select 2 JavaScript -->
    {{-- <script src="{{asset("packages/extensionsvalley/dashboard/js/select2.full.min.js")}}"></script> --}}
    <!-- Custom Core JavaScript -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/core-admin.js') }}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/custom.js') }}"></script>

    {{-- new design integrated start --}}
    <script src="{{ asset('packages/extensionsvalley/dashboard/plugins/momentjs/moment.min.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/dashboard/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}">
    </script>

    <script
        src="{{ asset('packages/extensionsvalley/dashboard/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/dashboard/plugins/floathead/jquery.floatThead.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/select2.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/timepicker.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/js/jquery-3.4.1.slim.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/default/plugins/cute-alert-master/cute-alert.js') }}"></script>

    <script src="{{ asset('packages/extensionsvalley/emr/bootbox/bootbox.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/bootbox/bootbox.locales.min.js') }}"></script>


    <!-- Tiny Mce Editor JavaScript -->
    <script src="{{ asset('packages/extensionsvalley/dashboard/js/tinymce/tinymce.min.js') }}"></script>

    <script>
        $(window).load(function(){
            var date_difference = '<?php echo $date_difference ?>';
            var license_expiry_date = '<?php echo $license_expiry_date ?>';
            if((!localStorage.getItem('licence_exp_status') || localStorage.getItem('licence_exp_status') == 0) && license_expiry_date != ''){
                if(parseInt(date_difference) == 0 ){
                    bootbox.alert('Your licence is going to expired on today.');
                    localStorage.setItem('licence_exp_status', 1);
                } else if(parseInt(date_difference) <= 10 && parseInt(date_difference) != 0 ){
                    bootbox.alert('Your licence is about to expire in '+date_difference+ ' days');
                    localStorage.setItem('licence_exp_status', 1);
                } else {
                    bootbox.alert('Your licence has expired. Please contact S/W team for licence renewal.');
                }
            }
            if(parseInt(date_difference) <= 10 && license_expiry_date != ''){
                $(".licence_warning_indication").show();
            }


            $(document).on('click', '.treeview', function(){
                $('body').addClass('nav-md');
                $('body').removeClass('nav-sm');
            });
        })
        function showGroupedDoctorsList() {
            var url = $('#base_doumenturl').val() + "/emr/fetchGroupedDoctorsList";
            $.ajax({
                url: url,
                type: "POST",
                data: {},
                beforeSend: function() {
                    $(".change_default_dr_btn").find('i').removeClass('fa-user-md').addClass('fa-spinner')
                        .addClass('fa-spin');
                    $(".select_default_doctor_modal_data").empty();
                },
                success: function(data) {
                    if (data) {
                        $(".change_default_dr_btn").find('i').addClass('fa-user-md').removeClass('fa-spinner')
                            .removeClass('fa-spin');

                        var selected_dr_id = localStorage.getItem('selected_dr_id') ? localStorage.getItem(
                            'selected_dr_id') : 0;
                        selected_dr_id = parseInt(selected_dr_id);
                        if (data.status == 1) {
                            $(".select_default_doctor_modal").modal('show');
                            $.each(data.data, function(key, val) {
                                var selected_dr_cond1 = 'style="display:none;"';
                                var selected_dr_cond2 = '';
                                if (selected_dr_id == parseInt(val.id)) {
                                    selected_dr_cond1 = '';
                                    selected_dr_cond2 = ' selected_dr_icon ';
                                }
                                var doctor_div =
                                    '<div class="col-md-3 padding_sm" style="margin-bottom: 10px;"><div class="select-doctor-box" data-doctor-id="' +
                                    val.id + '" title="Click to Select"><div ' + selected_dr_cond1 +
                                    ' class="selected_dr_div "><i class="fa fa-check selected_dr_check" ></i></div><i class="fa fa-user-md fa-3x select_default_dr_icon ' +
                                    selected_dr_cond2 +
                                    '" ></i><div class="clearfix"></div><span class="select_dr_name">' +
                                    val.doctor_name +
                                    '</span><div class="clearfix"></div><span class="select_dr_speciality">' +
                                    val.speciality + '</span></div></div>';
                                $(".select_default_doctor_modal_data").append(doctor_div);
                            });


                        }

                    }
                }
            });

        }



        function setPracticeLocation() {
            var practice_location = $('#practice_location_modal').find('select').val();
            var url = $('#base_doumenturl').val() + "/emr/setPracticeLocation";
            $.ajax({
                url: url,
                type: "GET",
                data: {
                    practice_location: practice_location
                },
                beforeSend: function() {
                    $(".set_practice_location_btn").find('i').removeClass('fa-map-marker').addClass(
                        'fa-spinner').addClass('fa-spin');
                },
                success: function(data) {
                    if (data) {
                        $(".set_practice_location_btn").find('i').addClass('fa-map-marker').removeClass(
                            'fa-spinner').removeClass('fa-spin');
                        if (data.status == 1) {

                        }

                    }
                }
            });
        }

        var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem('selected_dr_id')) : 0;
        if (selected_dr_id == 0 && $('#doctor_id').val()!='') {
            //console.log(selected_dr_id+'#'+ $('#doctor_id').val());
            @if (env('QUEUE_MANAGEMENT_ENABLED', '') == 'TRUE' && $dr_status == 1)
            localStorage.setItem('selected_dr_id', $('#doctor_id').val());
            showGroupedDoctorsList();
            @endif

        }

        $(document).ready(function() {

            // showAllNotifications();
            var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem(
                'selected_dr_id')) : 0;
            var noise_image = $('#base_doumenturl').val() + '/packages/extensionsvalley/emr/images/noise.svg';
            var url = $('#base_doumenturl').val() + "/emr/getDoctorCheckInStatus";
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    selected_dr_id: selected_dr_id
                },
                success: function(data) {
                    if (data) {
                        if (data.check_in_status == "TRUE") {
                            $(".check_in_box").prop('checked', true);
                            $(".queue_management_div").show();
                            $("#monitorscreen").css("background", "white");
                        } else {
                            $(".check_in_box").prop('checked', false);
                            $(".queue_management_div").hide();
                            $("#monitorscreen").css("background",
                                "linear-gradient(to right, #353535, transparent), url(" +
                                noise_image + ") ");
                        }
                        if (data.emergency_status == 1) {
                            localStorage.setItem('doctor_emergency_status', "TRUE");
                        }
                        checkEmergencyStatus();
                    }
                }
            });




            function checkEmergencyStatus() {
                var doctor_emergency_status = localStorage.getItem('doctor_emergency_status') ? localStorage
                    .getItem('doctor_emergency_status') : 0;
                if (doctor_emergency_status && doctor_emergency_status == "TRUE") {
                    $(".next_general_token").attr("disabled", true);
                    $(".next_special_token").attr("disabled", true);
                    $(".recall_token").attr("disabled", true);
                    $(".call_token_btn").attr("disabled", true);
                    $(".call_token_by_booking_id_btn").attr("disabled", true);
                    $(".emergency_btn").hide();
                    $(".emergency_stop_btn").show();
                } else {
                    $(".next_general_token").attr("disabled", false);
                    $(".next_special_token").attr("disabled", false);
                    $(".recall_token").attr("disabled", false);
                    $(".call_token_btn").attr("disabled", false);
                    $(".call_token_by_booking_id_btn").attr("disabled", false);
                    $(".emergency_stop_btn").hide();
                    $(".emergency_btn").show();
                }
            }
            checkEmergencyStatus();

            var booking_id = localStorage.getItem('current_booking_id') ? parseInt(localStorage.getItem(
                'current_booking_id')) : 0;
            if (isNaN(booking_id)) {
                booking_id = 0;
            }
            var shift_id = $('.selectShift').val();

            var url = $('#base_doumenturl').val() + "/emr/callTokenByBookingId";
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    current_booking_id: booking_id,
                    selected_dr_id: selected_dr_id,
                    display: "FALSE",
                    shift_id: shift_id
                },
                beforeSend: function() {
                    $(".current_token").html("<i class='fa fa-spinner fa-spin'></i>");
                },
                success: function(data) {
                    if (data) {
                        if (data.status == 1) {
                            $(".current_token").html(data.token);
                        } else {
                            $(".current_token").html(0);
                        }
                    }
                },
                error: function() {
                    toastr.success("Call Failed");
                }
            });


            $(document).on("click", '.select-doctor-box', function() {
                var doctor_id = $(this).attr("data-doctor-id");
                doctor_id = parseInt(doctor_id);
                localStorage.setItem('selected_dr_id', doctor_id);
                toastr.success("Success.");
                $(".selected_dr_div").hide();
                $('.select_default_dr_icon').removeClass('selected_dr_icon');
                $(this).parent().find('.select_default_dr_icon').addClass('selected_dr_icon');
                $(this).parent().find('.selected_dr_div').show();
                $("#group_doctor").val(doctor_id).trigger('change');
                localStorage.removeItem('shift_id');
                getDoctorShiftDetails();
                $(".select_default_doctor_modal").modal('hide');
            });

            $(".check_in_box").click(function() {
                var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem(
                    'selected_dr_id')) : 0;

                if ($("#group_doctor").length > 0) {
                    if (selected_dr_id == 0) {
                        toastr.error("Please select a doctor");
                        return false;
                    } else {
                        var check_in_status = $(this).is(':checked') ? "TRUE" : "FALSE";
                        updateCheckInCheckout(check_in_status);
                    }
                } else {
                    var check_in_status = $(this).is(':checked') ? "TRUE" : "FALSE";
                    updateCheckInCheckout(check_in_status);
                }

            });

            function updateCheckInCheckout(check_in_status) {
                var url = $('#base_doumenturl').val() + "/emr/updateCheckInStatus";
                var noise_image = $('#base_doumenturl').val() + '/packages/extensionsvalley/emr/images/noise.svg';
                var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem(
                    'selected_dr_id')) : 0;
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        check_in_status: check_in_status,
                        selected_dr_id: selected_dr_id
                    },
                    beforeSend: function() {
                        $('#monitorscreen').LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#009869'
                        });
                    },
                    success: function(data) {
                        if (data) {
                            $('#monitorscreen').LoadingOverlay('hide');
                            if (data.status == 1) {
                                toastr.info("Online");
                                $(".queue_management_div").show();
                                $(".call_token_btn").show();
                                $("#monitorscreen").css("background", "white");
                                if (data.emergency_status == 1) {
                                    localStorage.setItem('doctor_emergency_status', "TRUE");
                                }
                            } else if (data.status == 2) {
                                toastr.info("Offline");
                                $(".queue_management_div").hide();
                                $("#monitorscreen").css("background",
                                    "linear-gradient(to right, #353535, transparent), url(" +
                                    noise_image + ") ");
                                $(".call_token_btn").hide();
                                localStorage.removeItem('doctor_emergency_status');
                            }

                            checkEmergencyStatus();
                        }
                    },
                    error: function() {
                        $('#monitorscreen').LoadingOverlay('hide');
                    }
                });
            }

            $(".next_general_token").click(function() {
                var current_booking_id = localStorage.getItem('current_booking_id');
                current_booking_id = parseInt(current_booking_id, 10);

                var current_normal_booking_id = localStorage.getItem('current_normal_booking_id');
                current_normal_booking_id = parseInt(current_normal_booking_id, 10);

                if (isNaN(current_booking_id)) {
                    current_booking_id = 0;
                }

                if (!isNaN(current_normal_booking_id) && current_normal_booking_id > 0) {
                    current_booking_id = current_normal_booking_id;
                    localStorage.removeItem('current_normal_booking_id');
                }

                var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem(
                    'selected_dr_id')) : 0;

                var shift_id = $('.selectShift').val();


                var url = $('#base_doumenturl').val() + "/emr/getNextGeneralToken";
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        current_booking_id: current_booking_id,
                        selected_dr_id: selected_dr_id,
                        shift_id: shift_id
                    },
                    beforeSend: function() {
                        $(".current_token").html("<i class='fa fa-spinner fa-spin'></i>");
                    },
                    success: function(data) {
                        if (data.status == 1) {
                            $(".current_token").html(data.prefix + '' + data
                                .next_general_token);
                            localStorage.setItem('current_booking_id', data.booking_id);
                            localStorage.setItem('current_normal_booking_id', data.booking_id);
                            // toastr.success("Token Called.");
                            $(".current_token").addClass("blink_me");
                            setTimeout(() => {
                                $(".current_token").removeClass("blink_me");
                            }, (2000));
                        } else {
                            toastr.success("No appointments found.");
                            $(".current_token").html(0);
                        }
                    }
                });
            });

            $(".next_special_token").click(function() {
                var current_booking_id = localStorage.getItem('current_booking_id');
                current_booking_id = parseInt(current_booking_id, 10);

                var current_special_booking_id = localStorage.getItem('current_special_booking_id');
                current_special_booking_id = parseInt(current_special_booking_id, 10);

                var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem(
                    'selected_dr_id')) : 0;
                if (isNaN(current_booking_id)) {
                    current_booking_id = 0;
                }

                if (!isNaN(current_special_booking_id) && current_special_booking_id > 0) {
                    current_booking_id = current_special_booking_id;
                    localStorage.removeItem('current_special_booking_id');
                }
                var shift_id = $('.selectShift').val();


                var url = $('#base_doumenturl').val() + "/emr/getNextSpecialToken";
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        current_booking_id: current_booking_id,
                        selected_dr_id: selected_dr_id,
                        shift_id: shift_id
                    },
                    beforeSend: function() {
                        $(".current_token").html("<i class='fa fa-spinner fa-spin'></i>");
                    },
                    success: function(data) {
                        if (data.status == 1) {
                            $(".current_token").html(data.prefix + '' + data
                                .next_special_token);
                            localStorage.setItem('current_booking_id', data.booking_id);
                            localStorage.setItem('current_special_booking_id', data.booking_id);
                            $(".current_token").addClass("blink_me");
                            setTimeout(() => {
                                $(".current_token").removeClass("blink_me");
                            }, (2000));
                            // toastr.success("Token Called.");
                        } else {
                            toastr.success("No appointments found.");
                            $(".current_token").html(0);
                        }
                    }
                });
            });

            $(".recall_token").click(function() {
                var current_token = $(".current_token").html();
                var current_booking_id = localStorage.getItem('current_booking_id');
                current_booking_id = parseInt(current_booking_id, 10);
                var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem(
                    'selected_dr_id')) : 0;
                if (isNaN(current_booking_id)) {
                    current_booking_id = 0;
                }
                var shift_id = $('.selectShift').val();

                if (current_booking_id > 0) {
                    var url = $('#base_doumenturl').val() + "/emr/recallCurrentToken";
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: {
                            current_booking_id: current_booking_id,
                            selected_dr_id: selected_dr_id,
                            shift_id: shift_id
                        },
                        beforeSend: function() {
                            $(".current_token").html("<i class='fa fa-spinner fa-spin'></i>");
                        },
                        success: function(data) {
                            if (data) {
                                if (data.status == 1) {
                                    // toastr.success("Token Called");
                                    $(".current_token").html(current_token);
                                    $(".current_token").addClass("blink_me");
                                    setTimeout(() => {
                                        $(".current_token").removeClass("blink_me");
                                    }, (2000));
                                } else {
                                    localStorage.removeItem('current_booking_id');
                                    $(".current_token").html(0);
                                    toastr.success("No appointments found.");
                                }

                            } else {
                                toastr.success("Recall Failed");
                            }
                        },
                        error: function() {
                            toastr.success("Recall Failed");
                        }
                    });
                } else {
                    toastr.success("No appointments found.");
                }

            });

            $(".emergency_btn").click(function() {
                // $(".emergency_queue_modal").modal('show');
                var emergency_message_text = "Emergency";
                var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem(
                    'selected_dr_id')) : 0;
                var url = $('#base_doumenturl').val() + "/emr/applyEmergencyMessage";
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        emergency_message: emergency_message_text,
                        selected_dr_id: selected_dr_id
                    },
                    success: function(data) {
                        if (data) {
                            $(".next_general_token").attr("disabled", true);
                            $(".next_special_token").attr("disabled", true);
                            $(".recall_token").attr("disabled", true);
                            $(".call_token_btn").attr("disabled", true);
                            $(".emergency_btn").hide();
                            $(".emergency_stop_btn").show();
                            toastr.success("Emergency enabled.");
                            localStorage.setItem('doctor_emergency_status', "TRUE");
                            // $(".emergency_queue_modal").modal('hide');
                        } else {
                            toastr.success("Failed to enable emergency.");
                        }
                    },
                    error: function() {
                        toastr.success("Failed to enable emergency.");
                    }
                });
            });

            $(".emergency_stop_btn").click(function() {
                var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem(
                    'selected_dr_id')) : 0;
                var url = $('#base_doumenturl').val() + "/emr/disableEmergencyMode";
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        selected_dr_id: selected_dr_id
                    },
                    success: function(data) {
                        if (data) {
                            $(".next_general_token").attr("disabled", false);
                            $(".next_special_token").attr("disabled", false);
                            $(".recall_token").attr("disabled", false);
                            $(".call_token_btn").attr("disabled", false);
                            $(".emergency_stop_btn").hide();
                            $(".emergency_btn").show();
                            toastr.success("Emergency disabled.");
                            localStorage.removeItem('doctor_emergency_status');
                        } else {
                            toastr.success("Failed to disabled emergency.");
                        }
                    },
                    error: function() {
                        toastr.success("Failed to disabled emergency.");
                    }
                });
            });

            $(document).on("click", '.selectShift', function() {
                localStorage.setItem('shift_id', $(this).val());
            });

            // $(document).on('click', ".applyEmergencyMessage", function(){
            //     var emergency_message_text = $(".emergency_message_text").val();
            //     var selected_dr_id = localStorage.getItem('selected_dr_id') ? parseInt(localStorage.getItem('selected_dr_id')) : 0;
            //     var url = $('#base_doumenturl').val() + "/emr/applyEmergencyMessage";
            //     $.ajax({
            //         url: url,
            //         type: "POST",
            //         data: {
            //             emergency_message: emergency_message_text,
            //             selected_dr_id: selected_dr_id
            //         },
            //         success: function(data) {
            //             if (data) {
            //                 toastr.success("Emergency message sent.");
            //                 $(".emergency_queue_modal").modal('hide');


            //             } else {
            //                 toastr.success("Failed to sent emergency message.");
            //             }
            //         },
            //         error: function(){
            //             toastr.success("Failed to sent emergency message.");
            //         }
            //     });
            // });

        });

        tinymce.init({
            selector: 'textarea.texteditor',
            theme: "modern",
            subfolder: "",
            height: "400",
            relative_urls: false,
            remove_script_host: false,
            convert_urls: true,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor filemanager"
            ],
            image_advtab: true,
            content_css: '{{ asset('packages/extensionsvalley/dashboard/js/tinymce/skins/lightgray/content.min.css') }}',
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
            style_formats: [{
                    title: 'Bold text',
                    inline: 'b'
                },
                {
                    title: 'Red text',
                    inline: 'span',
                    styles: {
                        color: '#ff0000'
                    }
                },
                {
                    title: 'Red header',
                    block: 'h1',
                    styles: {
                        color: '#ff0000'
                    }
                },
                {
                    title: 'Example 1',
                    inline: 'span',
                    classes: 'example1'
                },
                {
                    title: 'Example 2',
                    inline: 'span',
                    classes: 'example2'
                },
                {
                    title: 'Table styles'
                },
                {
                    title: 'Table row 1',
                    selector: 'tr',
                    classes: 'tablerow1'
                }
            ],
        });

        function goToNotification(targetUrl, id) {
            var url = $('#base_url').val() + "/admin/notificationViewed";
            targetUrl = atob(targetUrl);
            $.ajax({
                url: url,
                type: "GET",
                data: "notification_id=" + id + '&targetUrl=' + targetUrl,
                beforeSend: function() {
                    $('#notification_contianer').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                },
                success: function(data) {
                    if (data == 1) {
                        window.location.href = $('#base_url').val() + targetUrl;
                    } else {
                        Command: toastr["success"]("Someting went wrong!");
                    }
                },
                complete: function() {
                    $('#notification_contianer').LoadingOverlay("hide");

                }
            });
        }

        function showAllNotifications() {
            var url = $('#base_url').val() + "/admin/showAllNotifications";
            $.ajax({
                url: url,
                type: "GET",
                data: "notification=1",
                beforeSend: function() {
                    $('#notification_contianer').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                },
                success: function(data) {
                    if (data == 0) {
                        // Command: toastr["success"]("Someting went wrong!");
                    } else {
                        var obj = JSON.parse(data);
                        $('#notification_count').html(obj.count);
                        $('#notification_contianer').html(obj.list);
                    }
                },
                complete: function() {
                    $('#notification_contianer').LoadingOverlay("hide");
                    $('.theadscroll').perfectScrollbar({
                        minScrollbarLength: 30
                    });
                    var $table = $('table.theadfix_wrapper');
                    $table.floatThead({
                        scrollContainer: function($table) {
                            return $table.closest('.theadscroll');
                        }
                    });
                }
            });
        }

        function redirectToDashboard() {
            var assesment_draft_mode = $('#assesment_draft_mode').val();
            var investigation_draft_mode = $('#investigation_draft_mode').val();
            var prescription_changes_draft_mode = $('#prescription_changes_draft_mode').val();
            if (assesment_draft_mode == 1) {
                checkAssesmentSaveValidation();
            } else if (investigation_draft_mode == 1) {
                checkInvestigationSaveValidation();
            } else if (prescription_changes_draft_mode == 1) {
                checkPrescriptionSaveValidation();
            } else {
                window.location.href = "{{ route('extensionsvalley.admin.dashboard') }}";
            }
        }

        function redirectToBackBtn() {
            history.back();
            return false;
        }

        function resetBootstrapTable(){
            $('.theadscroll').perfectScrollbar("update");
            $(".theadfix_wrapper").floatThead('reflow');
            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.theadscroll');
                }
            });
        }
    </script>

    @yield('javascript_extra')

</body>
@endif
</html>
