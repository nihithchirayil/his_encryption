@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/ip-op-slide.css")}}" rel="stylesheet">


@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col"  role="main">

        <div class="row codfox_container">

    <div class="col-xs-12 col-sm-12 col-md-2">
                    <div class="form-group control-required">
                        <label>User</label>
                        <input type="text" name="search_user" style="cursor: pointer;" autocomplete="off" class="form-control" placeholder="Type at least 3 characters">

                                            <input type="hidden" name="search_user_id_hidden" value="">


                                            <!-- User List -->

                                            <div class="user-list-div" style="display: none;z-index:9999">
                                                <a style="float: left;" class="close_btn_user_search">X</a>

                                                <div class=" user_theadscroll" style="position: relative;">
                                                  <table id="UserTable"  class="table table-bordered no-margin table_sm table-striped user_theadfix_wrapper">
                                                      <thead>
                                                        <tr class="light_purple_bg">
                                                          <th colspan="2">User</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody id="ListUserSearchData" >

                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          <!-- User List -->
                    </div>
                </div>


                 <div class="col-xs-12 col-sm-12 col-md-2">
                    <div class="form-group control-required">
                        <label>Frequency</label>
                        {!! Form::text('frequency_name', isset($company->frequency_name) ? $company->frequency_name : request()->old('frequency_name'), [
                            'class'       => 'form-control',
                            'id'          => 'frequency_name',
                            'placeholder' => 'Frequency',
                            'required'    => 'required',
                        ]) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-1">
                    <div class="form-group control-required">
                       <label>Value</label>
                        {!! Form::text('frequency_value', isset($company->frequency_value) ? $company->frequency_value : request()->old('frequency_value'), [
                            'class'       => 'form-control',
                            'id'          => 'frequency_value',
                            'placeholder' => 'Value/day',
                            'required'    => 'required',
                        ]) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-1">
                                    <button type="button" style="margin-top:24px;" class="btn btn-success btn-block" onclick="save_frequency_details();">Save</button>
                                </div>
 <hr>
            <div class="clearfix"></div>
            <div class="ht10"></div>


                          <!---- Common frequency table ----->
<div class="col-md-6">
    <h5 style="margin-left:10px;">Available List</h5>
     <div class=" theadscroll padding_sm" style="position: relative; height: 350px;">
    <table id="common_freq_table" class ="table table-striped theadfix_wrapper table-sm">
          <thead>
              <tr style="background-color: #0f996b; color:white;padding:0px;">
                    <th>Frequency Name</th>
                    <th style="text-align:center;">Value</th>
                </tr>
          </thead>
        <tbody>
            @if(isset($frequency_list) && count($frequency_list) > 0)
                    @foreach($frequency_list as $ind => $data)
                        <tr id="{{$data->frequency}}" style="padding:0px !important;">

                             <td style="text-align:left;padding:0px !important;">&nbsp;
                                    <input id="{{$data->id}}" name="chk[]" class="{{$data->frequency}}"
                                       type="checkbox" onclick="InsertFrequency(this)" data-id="undefined" value="{{$data->value}}">&nbsp;&nbsp;
                                    <label>{{$data->frequency}}</label>
                                </td>
                             <td style="padding:0px !important;text-align:center">{{$data->value}}</td>
                        </tr>

                    @endforeach
            @else
                       <tr  style=" background-color: #f2f2f2;"><td  style="text-align: center; padding: 6px;">No Data Found</td></tr>
            @endif
        </tbody>
    </table>
  </div>
</div>
<!---- Common frequency table end ----->



<!---- user frequency table ----->
<div class="col-md-6">
   <h5 style="margin-left:10px;">Selected List</h5>
    <div class=" theadscroll padding_sm" style="position: relative; height: 350px;">
        <table id="selected_freq_table" class ="table table-striped theadfix_wrapper table-sm">
            <thead>
               <tr style="background-color: #0f996b; color:white;padding:0px;">
                        <th style="text-align: left;">Frequency Name</th>
                        <th style="text-align: center;">Value</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
            </thead>
            <tbody id="frequency-listing-table">

                <tr style="text-align: center; background-color: #f2f2f2;"><td  id="hiderow" colspan="8">No Data Found</td></tr>

            </tbody>
        </table>
      </div>
</div>
<!---- user frequency table ends ----->




        </div>


        <input type="hidden" id="base_url" value="{{URL::to('/')}}">
        <input type="hidden" id="c_token" value="{{csrf_token()}}">


    </div>




@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>

<!-- frequency -->
<script src="{{asset("packages/extensionsvalley/emr/js/frequency.js")}}"></script>

<script src="{{asset("packages/extensionsvalley/jquery-mousewheel-master/jquery.mousewheel.min.js")}}"></script>

<script type="text/javascript">


</script>
@endsection
