@if(sizeof($data_list) > 0)
  @foreach($data_list as $key => $value)
  @php
    $id = (!empty($value->id)) ? $value->id : "";
    $frequency = (!empty($value->frequency)) ? $value->frequency : "";
    $value = (!empty($value->value)) ? $value->value : "";
   
   
  
  @endphp
    <tr style="padding:0px !important;height:27px;">
        <td style="text-align: left;padding:0px !important;"> &nbsp;
            {{$frequency}}
            <input type="hidden" class="form-control" name="fav_frequency[]" value="{{$frequency}}">
           
             <input type="hidden" class="fav_check" name="frequency_id[]" value="{{$id}}">
        </td>
        <td style="padding:0px !important;text-align: center;">
            {{$value}}
            <input type="hidden" class="form-control" name="fav_value[]" value="{{$value}}">
        </td>
      
        <td style="text-align: center;padding:0px !important;">
             <button type="button" style="padding-top:3px;" class='btn btn-sm btn-danger delete-frequency-row'><i class="fa fa-trash-o"></i>Remove</button>
        </td>
    </tr>
  @endforeach
@else
<tr style="text-align: center;">
    <td  id="hiderow" colspan="8">No Data Found.!</td>
</tr>
@endif
