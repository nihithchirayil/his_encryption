@extends('Emr::emr.page')
@section('content-header')
    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/wizard.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/wizard_checklist.css') }}" rel="stylesheet">
    <style>
        table td.highlighted {
            background-color: rgb(182, 255, 87);
        }

        table td.scheduled {
            background-color: rgb(201 255 240);
        }

        table td.scheduled_fixed {
            background-color: rgb(201 255 240);
        }

        .tooltip {
            position: absolute;
            z-index: 99999;
            color: white !important;
            background-color: rgb(245, 43, 184) !important;
            min-width: 250px !important;
            border-radius: 5px !important;
            padding: 10px !important;
        }

        .tooltip-inner {
            background-color: rgb(245, 43, 184);
        }

        .table-bordered>thead>tr>th,
        .table-bordered>tbody>tr>th,
        .table-bordered>tfoot>tr>th,
        .table-bordered>thead>tr>td,
        .table-bordered>tbody>tr>td,
        .table-bordered>tfoot>tr>td {
            border: 1px solid #f1dede !important;
        }

        .mate-input-box {
            height: 45px !important;
        }

        .mate-input-box label {
            margin-top: 0px !important;
        }

        .txt-overflow {
            max-width: 290px;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            font-weight: 600;
            font-size: 9px !important;
        }

        .booked_sloat {
            border-radius: 9px !important;
        }

        .select2-container {
            box-shadow: none !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__display {
            cursor: default;
            padding-left: 2px;
            padding-right: 5px;
            font-size: 9px;
            font-weight: 600;
            margin-top: 3px;
        }

        .fa-times-circle {
            font-size: 18px;
            color: steelblue;
            margin: -7px;
            margin-bottom: 3px;
        }

        .fa-times-circle:hover {
            scale: 1.2 !important;
        }

        .common_table_header {
            background-color: cornflowerblue;
        }
    </style>
@endsection
@section('content-area')
    <div class="right_col">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">


        <div class="col-md-12">



            {{-- Schedule area --}}

            <div class="col-md-3 box-body" style="height: 595px;">
                <div class="col-md-6" style="z-index:99999">
                    <div class="mate-input-box">
                        <label style="top:0px;" for="">Schedule Date</label>
                        <div class="clearfix"></div>
                        <input type="text" name="schedule_date" autocomplete="off" value="{{ date('M-d-Y') }}"
                            class="form-control datepicker" id="schedule_date" placeholder="From Date" onblur="listRadiologyScheduleOnDateChange()">
                    </div>
                </div>
                <div class="col-md-12 padding_sm">
                    <div class="mate-input-box">
                        <label for="">Patient Name/UHID</label>
                        <div class="clearfix"></div>
                        <input type="text" autocomplete="off" class="form-control hidden_search" name="patient_name"
                            id="patient_name" value="" autocomplete="false">
                        <input type="hidden" class="form-control" name="patient_name_hidden" id="patient_name_hidden"
                            value="">
                        <div id="AjaxDiv" class="ajaxSearchBox"
                            style="margin: -15px 0px 0px 0px !important;z-index: 9999999;max-height: 250px !important">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mate-input-box">
                        <label style="top:0px;" for="radiologist">Select Radiologist</label>
                        {!! Form::select('surgeon', ['-1' => ' Select'] + $doctor_list->toArray(), 0, [
                            'class' => 'form-control select2',
                            'title' => 'Radiologist',
                            'id' => 'radiologist',
                            'style' => 'color:#555555; padding:2px;overflow: auto;',
                        ]) !!}
                    </div>
                </div>

                {{-- <div class="col-md-12 padding_sm">
                    <div class="mate-input-box" >
                      <label for="">Select Procedure</label>
                      {!! Form::select('service_id', $service_list,0, ['class' => 'form-control select2', 'id' =>'service_id', 'style' => 'color:#555555; padding:4px 12px;','title' => 'Select Procedure','style' => 'color:#555555; padding:2px 12px;overflow: auto;z-index:99999']) !!}
                    </div>
                </div> --}}


                <div class="col-md-12">
                    <div class="mate-input-box">
                        <label style="top:0px;" for="anesthetist">Select Procedure</label>
                        {!! Form::select('service_id', ['-1' => ' Select'] + $service_list->toArray(), 0, [
                            'class' => 'form-control select2',
                            'title' => 'Select Procedure',
                            'id' => 'service_id',
                            'style' => 'color:#555555; padding:2px;overflow: auto;z-index:99999',
                        ]) !!}
                    </div>
                </div>

                <div class="col-md-12" style="margin-top:15px;">
                    <button type="button" class="btn bg-green pull-right" onclick="saveRadiologyschedule();"
                        id="BtnSaveRadiologyschedule">
                        <i class="fa fa-save"></i> Save
                    </button>
                    <button type="reset" class="btn bg-orange pull-right" id="reset_radiology_request">
                        <i class="fa fa-save"></i> Reset
                    </button>
                </div>
                <input type="hidden" name="request_id" id="request_id" value="">
            </div>


            {{-- Time allocation--}}


            <div class="col-md-9 no-padding" id="time_allocation_container" style="min-height:300px">

        </div>
        </div>
    </div>
@stop

@section('javascript_extra')
    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/radiology_schedule.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

@endsection
