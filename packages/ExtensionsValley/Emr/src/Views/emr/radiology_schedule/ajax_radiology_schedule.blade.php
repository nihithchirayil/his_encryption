<style>

    .wrapper {
        /* position: relative; */
        overflow: auto;
        /* white-space: nowrap; */
    }


</style>
<div class=" box-body wrapper theadscroll" style="position:relative;height:595px">
    <table id="shedule_time_table" class='table table-bordered theadfix_wrapper'
        style="font-size: 12px;">
        <thead>
            @php
                $ot_list = [];
                $ot_list_start_time = [];
                $ot_list_end_time = [];
                $time_list = [];
                $otval = '';
                $time_val = '';
                $data_array = [];
                $ot_array = [];
                $timevalue = '';
                $schedule_id_value = '';
                $schedule_id_array = [];
                $c= 0;

                foreach ($shedule_data as $data) {
                    if ($otval != $data->radiology_id) {
                        $ot_list[$data->radiology_id] = $data->radiology_name;
                        $ot_list_start_time[$c] = $data->radiology_start_time;
                        $ot_list_end_time[$c] = $data->radiology_end_time;
                        $c++;
                    }

                    if ($time_val != $data->ret_slot_from . '-' . $data->ret_slot_to) {
                        array_push($time_list, date('h:i A', strtotime($data->ret_slot_from)) . '-' . date('h:i A', strtotime($data->ret_slot_to)));
                    }

                    $otval = $data->radiology_id;
                    $time_val = $data->ret_slot_from . '-' . $data->ret_slot_to;

                    $ot_array[] = $data->radiology_id;
                }

                $time_list = array_unique($time_list);
                $ot_array = array_unique($ot_array);

                $temp_ot = '';
                foreach ($shedule_data as $data1) {
                    if ($data1->appointment_id != '') {
                        $data_array[] = [
                            'radiology_id' => $data1->radiology_id,
                            'radiology_name' => $data1->radiology_name,
                            'ret_slot_from' => $data1->ret_slot_from,
                            'ret_slot_to' => $data1->ret_slot_to,
                            'r_date' => $data1->r_date,
                            // 'request_id' => $data1->request_id,
                            'mechine_id' => $data1->mechine_id,
                            'radiologist' => $data1->radiologist,
                            'uhid' => $data1->uhid,
                            'patient_name' => $data1->patient_name,
                            'mechine_name' => $data1->mechine_name,
                            'start_time' => $data1->start_time,
                            'appointment_id' => $data1->appointment_id,
                        ];

                        $schedule_id_array[] = $data1->appointment_id;
                    }
                }

                $schedule_id_array = array_unique($schedule_id_array);

            @endphp
            <tr class="common_table_header">
                <th style="width:130px !important;">Time
                    <input type="hidden" name="schedule_id_hidden_val" id="schedule_id_hidden_val"
                        value="{{ implode(',', $schedule_id_array) }}">

                </th>
                @foreach ($ot_list as $key => $value)
                    <th id="ot_{{ $key }}"  style="width:130px !important;padding-right: 13px !important;">{{ $value }}</th>
                @endforeach
            </tr>
        </thead>

        <tbody>
            @foreach ($time_list as $time)
                <tr id="time_{{ $time }}">
                    <td class="time_list"style="color:white;font-weight: 600;width:7%;background:cornflowerblue;">
                        {{ $time }}
                    </td>

                    @for ($j = 0; $j < count($ot_list); $j++)
                        @php
                            $html = '';
                            $tooltip = '';
                            $cls = '';
                            $booked_cls = '';
                            $start_time_value = '';
                            $schedule_id_value = '';
                        @endphp
                        @foreach ($data_array as $data1)
                            @php
                                $timevalue = '';
                                $req_id = '';
                                $timevalue = explode('-', $time, 2);
                                $timevalue = $timevalue[0];
                                $timevalue = date('H:i:s', strtotime($timevalue));
                            @endphp

                            @if ($data1['radiology_id'] == $j + 1 && $data1['ret_slot_from'] == $timevalue)
                                @php
                                    $tooltip = "<div class='col-md-12' style='z-index:999'>";
                                    $tooltip .= "<label class='col-md-6 text-left'>Patient</label>";
                                    $tooltip .= "<label class='col-md-6 text-left'>" . $data1['patient_name'] . '</label>'."<div class='clearfix'></div>";
                                    $tooltip .= "<label class='col-md-6 text-left'>UHID:</label>";
                                    $tooltip .= "<label class='col-md-6 text-left'>" . $data1['uhid'] . '</label>'."<div class='clearfix'></div>";
                                    $tooltip .= "<label class='col-md-6 text-left'>Mechine Name:</label>";
                                    $tooltip .= "<label class='col-md-6 text-left'>(" . $data1['mechine_name'] . ')</label>'."<div class='clearfix'></div>";
                                    $tooltip .= "<label class='col-md-6 text-left'>Radiologist:</label>";
                                    $tooltip .= "<label class='col-md-6 text-left'>" . $data1['radiologist'] . '</label>';
                                    $tooltip .= '</div>';

                                    $html = '<div class="col-md-12 no-padding" data-toggle="tooltip"  data-container="body" data-placement="left" data-html="true" data-original-title="' . $tooltip . '">';
                                    $html .= "<i onclick='deleteRadiologySchedule(" . $data1['appointment_id'] . ");' style='cursor:pointer' class='fa fa-times-circle pull-right'></i>";
                                    $html .= '<div class="col-md-12" style="padding:0px;">';
                                    $html .= '<label class="col-md-12 txt-overflow no-padding text-left" style="color:blue;">' . $data1['patient_name'] . '</label>';
                                    $html .= '</div></div>';
                                    $cls = 'scheduled_fixed';
                                    $booked_cls = 'booked_sloat';
                                    $start_time_value = $data1['start_time'];
                                    $schedule_id_value = $data1['appointment_id'];

                                @endphp
                            @else
                                @php

                                @endphp
                            @endif
                        @endforeach
                        @php
                        $timec = explode('-',$time);
                        $timecc=  date('H:i:s', strtotime($timec[0]));
                        @endphp
                        @if($ot_list_start_time[$j] > $timecc)
                            @php $cls = 'scheduled_fixed'; @endphp
                        @endif
                        @if($ot_list_end_time[$j] <= $timecc)
                            @php $cls = 'scheduled_fixed'; @endphp
                        @endif
                        <td class="{{ $cls }} {{ $schedule_id_value }} {{ $booked_cls }}"
                            data-attr-start_time={{ $start_time_value }} data-attr-ret_slot_from="{{ $timevalue }}"
                            data-attr-time="{{ $time }}" data-ot="{{ $j + 1 }}"
                            id="cell_{{ $j + 1 }}">
                            {!! $html !!}
                        </td>
                    @endfor
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
