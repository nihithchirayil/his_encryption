<table class="table no-margin theadfix_wrapper table-border-radius-top table-striped  table_sm table-condensed floatThead-table" style="border-collapse: collapse; border: 0px none rgb(128, 128, 128); display: table; margin: 0px; table-layout: fixed; width: 100%;margin-top:10px;">
    <thead>
        <tr class="tableRowHead">
            <th style="width:30%">Form</th>
            <th style="width:30%">Doctor Name</th>
            <th style="width:20%">Created At</th>
            <th style="width:20%">Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($res as $data)
            <tr>
                <td style="text-align:left;">{{ucwords(strtolower($data->template_name))}}</td>
                <td style="text-align:left;">{{ucwords(strtolower($data->doctor_name))}}</td>
                <td style="text-align:left;">{{date('M-d-Y h:i:A',strtotime($data->created_at))}}</td>
                <td>
                    <button type="button" onclick="printPreviewDynamicTemplate('{{$data->id}}','0');"class="btn btn-sm btn-primary" title="View"><i class="fa fa-eye"></i></button>
                    {{-- <button type="button" class="btn btn-sm btn-primary" title="Edit"><i class="fa fa-pencil"></i></button> --}}
                    <button type="button" onclick="printPreviewDynamicTemplate('{{$data->id}}','1');" class="btn btn-sm btn-primary" title="Print"><i class="fa fa-print"></i></button>
                    <button type="button" onclick="deleteDynamicTemplate('{{$data->id}}');" class="btn btn-sm btn-danger" title="Delete"><i class="fa fa-times"></i></button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
