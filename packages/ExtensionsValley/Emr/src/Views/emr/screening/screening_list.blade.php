@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <style>
        .right_btn_widget ul li button.btn {
            background: #9ceed5 !important;
            color: #000;
        }

        .ajaxSearchBox {
            display: none;
            text-align: left;
            list-style: none;
            cursor: pointer;
            max-height: 350px;
            margin: -2px 0px 0px 0px;
            overflow-y: auto;
            width: auto;
            z-index: 99999;
            position: absolute;
            background: #ffffff;
            border-radius: 3px;
            border: 1px solid rgba(0, 0, 0, 0.3);
        }

        .searched_items a {
            color: #555;
        }

        .search_header{
            background: #36A693 !important;
            color: #FFF !important;
        }

        .box-body {
            background-color: #fff !important;
        }

        .screening_completed{
            background-color:#feffa6;
        }
        .screening_not_completed{
            background-color:#ffffff;
        }
        .legend{
            display: inline-block;
            min-width: 10px;
            padding: 3px 7px;
            font-size: 12px;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
        }

        

    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <div class="right_col">
    <div class="box no-margin no-border">
    <div class="col-md-12 padding_sm">
        <div class="box no-border no-margin anim">
            <div class="box-header search_header">
                <span class="padding_sm">{{$title}}</span>
            </div>
            <div class="box-body clearfix">
                <div class="col-md-2 padding_sm">
                    <div class="" data-toggle="buttons" style="margin-top:24px;">
                        <label class=" " style="margin-right:4px;">
                            <input type="radio" id="patient_name_search_radio" value="1" name="search_type" > Name
                        </label>
                        <label class=" " style="margin-right:4px;">
                            <input type="radio" id="uhid_search_radio" value="2" name="search_type" > UHID
                        </label>
                        <label class=" active">
                            <input type="radio" id="patient_phone_search_radio" value="3" name="search_type" checked="checked"> Phone
                        </label>
                    </div>
                </div>
                <div class="col-md-2 padding_sm">

                    <label for="">Search Patients</label>
                    <div class="clearfix"></div>
                    <div class="input-group">
                        <input type="text" id="patient_search" onkeyup="search_patient();" class="form-control">
                        <div class="input-group-btn">
                            <button type="button" style="height: 23px; width: 30px;" class="btn light_purple_bg" onclick="search_patient();"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 padding_sm">
                    <label for="">Search By Date</label>
                    <div class="clearfix"></div>
                    <div class="input-group">
                        <input type="text" id="search_date" onblur="fetchScreeningListData();" class="form-control datepicker" value="{{date('M-d-Y')}}" data-attr="date" placeholder="Date">
                        <div class="input-group-btn">
                            <button type="button" style="height: 23px; width: 30px;" class="btn light_purple_bg">
                                <i class="fa fa-search" onclick="fetchScreeningListData();"></i>
                            </button>
                        </div>
                    </div>
                </div>


                @php
                    $doctor_list = \DB::table('doctor_master')
                        ->whereNull('doctor_master.deleted_at')
                        ->where('doctor_master.status', 1)
                        ->orderBy('doctor_master.doctor_name')
                        ->pluck('doctor_master.doctor_name', 'doctor_master.id');
                @endphp
                @if (count($doctor_list) > 0)
                    <div class="col-md-3 padding_sm">
                        <label for="">Select Doctor</label>
                        <div class="clearfix"></div>
                        {!! Form::select('group_doctor', $doctor_list, null, ['class' => 'form-control', 'id' => 'group_doctor', 'onchange' => 'fetchScreeningListData();', 'style' => ' color:#555555; padding:4px 12px;']) !!}
                    </div>
                @endif

                <div class="col-md-2 padding_sm" style="width:185px;">
                    <label for="">Global Patient Search</label>
                    <input type="checkbox" name="global_patient_check" class="global_patient_check" style="width: 13px; float: right; " />
                    <div class="clearfix"></div>
                    <input type="text" readonly id='patient_general_search_txt' onkeyup="patient_general_search(this.value);" class="form-control" value="">
                    <div style="width:300px; display: none;z-index: 9999; min-height: 250px;"
                        id="completepatientbox" class="ajaxSearchBox">
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="col-md-12 padding_sm screening_list_table_container">
        
    </div>

</div>


<div class="modal fade" id="editvitalModal" tabindex="-1" role="dialog" aria-labelledby="vitalModal" aria-hidden="true">
    <div class="modal-dialog" style="width:43%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                Patient Vitals
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" value="" id="vital_batch">
                        <input type="hidden" value="9" id="temperature">

                        <div>
                            @php
                                $weight =  ExtensionsValley\Emr\VitalMaster::getVitalDetails('Weight');
                                $height =  ExtensionsValley\Emr\VitalMaster::getVitalDetails('Height');
                                $temperature  =  ExtensionsValley\Emr\VitalMaster::getVitalDetails('Temperature');
                            @endphp
                            {!! Form::select('weight', $weight->toArray(),'', ['class' => 'form-control hidden','id' => 'weight','onchange' => 'weightSelect(this)']) !!}
                            {{-- {!! Form::select('temperature', $temperature->toArray(),'', ['class' => 'form-control hidden','id' => 'temperature','onchange' => 'temperatureSelect(this)']) !!} --}}
                            {!! Form::select('height', $height->toArray(),'', ['class' => 'form-control hidden','id' => 'height','onchange' => 'heightSelect(this)']) !!}

                            <table class="table no-margin theadfix_wrapper table-striped table-condensed styled-table" >
                                <tr>
                                    <td><label for="">Weight in Kg</label></td>
                                    <td>    <input type="text" class="form-control" name="weight_value" id="weight_value" value="">
                                    </td>

                                    <td><label for="">Height/Length in cm</label></td>
                                    <td><input type="text" class="form-control" name="height_value" id="height_value"
                                        value="">
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="">BP Systolic mmHg</label></td>
                                    <td><input type="text"  value="" class="form-control" name="bp_systolic" id="bp_systolic">
                                    </td>

                                    <td><label for="">BP Diastolic mmHg</label></td>
                                    <td><input type="text" class="form-control" name="bp_diastolic" id="bp_diastolic"
                                        value="">
                                    </td>

                                </tr>

                                <tr>

                                    <td><label for="">Temperature in C</label></td>
                                    <td><input type="text" class="form-control int_type " name="temperature_value" id="temperature_value"
                                        value="">
                                    </td>
                                    <td><label for="">Temperature in F</label></td>
                                    <td><input type="text" class="form-control int_type " name="temperature_value_f" id="temperature_value_f"
                                        value="">
                                    </td>
                                </tr>



                                <tr>
                                    <td><label for="">Pulse/Min</label></td>
                                    <td><input type="text" class="form-control int_type " name="pulse" id="pulse"
                                        value="">
                                    </td>
                                    <td><label for="">Respiration/Min</label></td>
                                    <td><input type="text" class="form-control int_type " name="respiration" id="respiration"
                                        value="">
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="">Head Circ-cm</label></td>
                                    <td><input type="text" class="form-control" name="head" id="head"
                                        value="">
                                    </td>
                                    <td><label for="">Oxygen Saturation %</label></td>
                                    <td><input type="text" class="form-control" name="oxygen" id="oxygen"
                                        value="">
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="">BMI Kg/M^2</label></td>
                                    <td><input type="text" class="form-control" name="bmi" id="bmi"
                                        value="">
                                    </td>
                                    <td><label for="">Spirometry</label></td>
                                    <td><input type="text" class="form-control" name="spirometry" id="spirometry"
                                        value="">
                                    </td>                                     
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>


                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <div class="col-md-2"><label for="">Time Taken</label></div>
                    <div class="col-md-6"><input type="text" class="form-control date_time_picker" name="time_taken" id="time_taken" value="" />

                    </td></div>
                </div>
                <div class="col-md-12">

                    <div class="col-md-2"><label for="">Vital Remarks</label></div>
                    <div class="col-md-10"> <textarea class="form-control remarks" name="remarks" id="remarks"></textarea>
                    </div>
                </div>

                <div class="col-md-6 pull-right" style="margin-top: 15px;"> 
                    <button style="width:90px;" class="btn btn-default col-md-1  pull-right light_purple_bg" id="update_vital" onclick="saveVitals()"> Save </button>
                    <button style="width:90px;" type="button" class="btn btn-default col-md-1  pull-right" data-dismiss="modal">Close</button>
                </div>
        </div>

    </div>
</div>


@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/screening_list.js') }}"></script>
    <script type="text/javascript">

    </script>
@endsection
