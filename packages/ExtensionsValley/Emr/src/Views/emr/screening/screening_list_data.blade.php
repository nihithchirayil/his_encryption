
<div class="box no-border no-margin anim" style="min-height:360px;">
    <div class="box-body clearfix">
    <div class="theadscroll" style="position: relative; max-height: 400px;">
        @if((string)$global_search == "0")
        <table class="table table-bordered no-margin table_sm no-border">
            <thead>
                <tr class="table_header_bg">
                    <th>SL.No.</th>
                    <th>UHID</th>
                    <th>Patient Name</th>
                    <th>Age/Gender</th>
                    <th>Dob</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Token No</th>
                    <th>Shift</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="screening_list_table_body">
            @isset($screening_list)
            @if(count($screening_list) > 0)
            @foreach($screening_list as $item)
            <tr class="patient_det_widget_screening @if($item->screening_status == 1) btn-info @else screening_not_completed @endif " data-patientname="{{$item->patient_name_whout_title}}" data-uhid="{{$item->uhid}}" data-phone="{{$item->phone}}" style="cursor:pointer;" >
                <td>{{$loop->iteration}}</td>
                <td>{{$item->uhid}}</td>
                <td>{{$item->patient_name}}</td>
                <td>{{$item->age}}/{{$item->gender}}</td>
                <td>{{date('M-d-Y', strtotime($item->dob))}}</td>
                <td>{{$item->address}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->token_no}}</td>
                <td>{{$item->schedule_description}}</td>
                <td>
                    @if(date('Y-m-d', strtotime($item->booking_date)) == date('Y-m-d'))
                    <button type="button" title="Manage Patient Vital" class="btn btn-sm bg-blue" onclick="showpatientVital('{{$item->patient_id}}', '{{$item->visit_id}}', '{{$item->visit_id}}');">
                        <i id="add_vital_modal_btn" class="fa fa-line-chart"></i>
                    </button>
                    @endif
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="10">No records found..!</td>
            </tr>
            @endif
            @endisset
            </tbody>
        </table>
</div>
        @else
    <div class="theadscroll" style="position: relative; max-height: 400px;">
        <table class="table table-bordered no-margin table_sm no-border">

            <thead>
                <tr class="table_header_bg">
                    <th>SL.No.</th>
                    <th>UHID</th>
                    <th>Patient Name</th>
                    <th>Age/Gender</th>
                    <th>Dob</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="screening_list_table_body">
            @isset($screening_list)
            @if(count($screening_list) > 0)
            @foreach($screening_list as $item)
            <tr class="patient_det_widget_screening @if($item->screening_status == 1) btn-info @else screening_not_completed @endif " data-patientname="{{$item->patient_name_whout_title}}" data-uhid="{{$item->uhid}}" data-phone="{{$item->phone}}" style="cursor:pointer;" >
                <td>{{$loop->iteration}}</td>
                <td>{{$item->uhid}}</td>
                <td>{{$item->patient_name}}</td>
                <td>{{$item->age}}/{{$item->gender}}</td>
                <td>{{date('M-d-Y', strtotime($item->dob))}}</td>
                <td>{{$item->address}}</td>
                <td>{{$item->phone}}</td>
                <td>
                    @if(date('Y-m-d', strtotime($item->booking_date)) == date('Y-m-d'))
                    <button type="button" title="Manage Patient Vital" class="btn btn-sm bg-blue" onclick="showpatientVital('{{$item->patient_id}}', '{{$item->visit_id}}');">
                        <i id="add_vital_modal_btn" class="fa fa-line-chart"></i>
                    </button>
                    @endif
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="8">No records found..!</td>
            </tr>
            @endif
            @endisset
            </tbody>
        </table>
        @endif
    </div>
</div>
</div>

<div class="clearfix"></div>
<div class="col-md-12" style="margin-bottom: 20px; margin-top: 15px;">
    <div class="col-md-2" style="text-align: left;">
        <span class="legend btn-info" style=" border: 1px solid #ccc; border-radius: 0; width: 10px; height: 17px;">&nbsp;&nbsp;&nbsp;</span> Screening Done
    </div>
    <div class="col-md-2" style="text-align: left;">
        <span class="legend screening_not_completed" style=" border: 1px solid #ccc; border-radius: 0; width: 10px; height: 17px;">&nbsp;&nbsp;&nbsp;</span> Screening Not Done
    </div>
</div>