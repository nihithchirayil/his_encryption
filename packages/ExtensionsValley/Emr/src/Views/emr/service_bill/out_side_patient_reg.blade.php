
                <div class="row codfox_container"  >
                    <input type="hidden" id="employee_id_hidden" value="">
                    <input type="hidden" id="date_formatt" value="{{ config('hrm.default_date_format_js') }}">
                    <input type="hidden" id="hidden_uhid">

                    <div class="col-md-12 padding_sm">

                    </div>
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-12 padding_sm">
                            <div class="box no-border">
                                <div class="box-body clearfix">
                                    <div class="col-md-1 padding_sm" style="margin-top:10px;">
                                        <div class="mate-input-box">
                                            <label for="">Title</label>
                                            <div class="clearfix"></div>
                                            <select class="form-control select2" id='billtag' onchange="">
                                                <option value="">Select</title>
                                                </option>
                                                @if (isset($title_head))
                                                @foreach ($title_head as $each)
                                                    <option value="{{ $each->title }}">{{ $each->title }}
                                                    </option>
                                                @endforeach
                                            @endif
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top:10px;">
                                        <div class="mate-input-box">
                                            <label for="">Patient Name</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" class="form-control"
                                                id="Patient_name" name="Patient_name" value="" required>
                                        </div>

                                    </div>

                                    <div class="col-md-3 padding_sm" style="margin-top:10px;">
                                        <div class="mate-input-box">
                                            <label for="">Phone Number </label>
                                            <div class="clearfix"></div>
                                            <input type="phone" autocomplete="off" onblur="checkphone();"
                                                class="form-control" id="PhoneNumb" name="Patient_name" value=""
                                                required>
                                        </div>

                                    </div>
                                    <div class="col-md-2 padding_sm" style="margin-top:10px;">
                                        <div class="mate-input-box">
                                            <label for="">Age</label>
                                            <div class="clearfix"></div>
                                            <input type="text" onblur="checkAge();" autocomplete="off"
                                                class="form-control" id="Age" name="Patient_name" value="" required>
                                        </div>

                                    </div>
                                    <div class="col-md-2 padding_sm" style="margin-top:10px;">
                                        <div class="mate-input-box">
                                            <label for="">Select Gender</label>
                                            <div class="clearfix"></div>
                                            <select class="form-control" id="gender">
                                                <option value="">Gender</option>
                                                <option value="1">Male</option>
                                                <option value="2">Female</option>
                                                <option value="3">Transgender</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-3 padding_sm" id="DocCol" style="margin-top:10px; display:block">
                                        <div class="mate-input-box">
                                            <label for="">Doctor</label>
                                            <div class="clearfix"></div>
                                            <select name="" id="DocName" class="form-control select2">
                                                <option value="2396">EXTERNAL</option>
                                                @if (isset($doctor_master))
                                                    @foreach ($doctor_master as $item)
                                                        <option value="{{ $item->id }}">{{ $item->doctor_name }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-3 padding_sm" id="outDocCol"
                                        style="margin-top:10px; display:none">
                                        <div class="mate-input-box">
                                            <label for="">Out Side Doctor</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" class="form-control" id="outDocName"
                                                name="OutSideName" value="" required>
                                        </div>

                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top:10px;">
                                        <input type="checkbox" autocomplete="off" class="padding_sm" id="outSideDoc"
                                            onchange="outSidedoc();" name="outsidePatient" value="" required>
                                        <label for="">Out Side Doctor</label>

                                    </div>
                                    <div class="col-md-3 padding_sm" style="margin-top:10px;">
                                        <button type="button" autocomplete="off" class="btn btn-success "
                                            style="margin-top: 10px;!important;" onclick="saveOutsidePtient();">Save
                                            Patient</button>

                                    </div>


                                </div>


                            </div>
                        </div>

                    </div>


                </div>
            
