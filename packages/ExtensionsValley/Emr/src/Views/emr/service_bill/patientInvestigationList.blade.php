         @php
         $i=1;
         @endphp
         @foreach($SearchInvesti as $item)
         @php
         $color_code = ' ';
         if ($item->billqty < 0) {
            $color_code = 'darkgoldenrod';
         }else if( $item->billqty > 0 ){
            $color_code = 'fff';
         }
         else if( $item->billqty == 0 ){
            $color_code ='darkcyan';
         }
     @endphp
             <tr style="">    
             <td class="td_common_numeric_rules">{{$i}}</td>
             
             @if($item->billqty==0)
               <td class="common_td_rules"><input type="checkbox" disabled class="convert" id="conver_{{$item->id}}"   onclick="convert('{{$item->patient_id}}','{{$item->doctor_id}}'','{{$item->id}}','{{$item->bill_tag}}','{{$item->outsideinvcnt}}')" data-patient-id='{{$item->patient_id}}' data-head_id="{{$item->id}}" data-doctor-id = '{{$item->doctor_id}}' data-investigation_type={{$item->bill_tag}} data-uhid = "{{$item->uhid}}" data-intend_type ={{$item->intend_type}}></td>
             @else
             <td class="common_td_rules"><input type="checkbox" class="convert" id="conver_{{$item->id}}" onclick="convert('{{$item->patient_id}}','{{$item->doctor_id}}','{{$item->id}}','{{$item->bill_tag}}','{{$item->outsideinvcnt}}')" data-patient-id='{{$item->patient_id}}' data-head_id="{{$item->id}}" data-doctor-id = '{{$item->doctor_id}}'  data-investigation_type={{$item->bill_tag}} data-uhid = "{{$item->uhid}}" data-intend_type ={{$item->intend_type}} ></td>
            @endif
             <td class="common_td_rules"><button onclick="getdetails({{$item->id}}, '{{$item->investigation_type}}')"><i class="fa fa-eye"></i></button></td>
             <td class="common_td_rules"><button id="print_list" onclick="printInvestigationHistoryList({{$item->id}})"><i class="fa fa-print"></i></button></td>
             <td class="common_td_rules"style="background-color:{{$color_code}}" title="{{$item->uhid}}">{{$item->uhid}}</td>
             <td class="common_td_rules" title="{{$item->patient_name}}"> {{$item->patient_name}}</td>
             <td class="common_td_rules">{{$item->visit_type }}</td>
             <td class="common_td_rules">{{$item->investigation_type }}</td>
             <td class="common_td_rules">{{ date('M-d-Y', strtotime($item->created_at))}}</td>
             <td class="common_td_rules" title="{{$item->doctor_name}}">{{$item->doctor_name}}</td>
             <td class="common_td_rules">{{$item->location_name }}</td>
             <td class="common_td_rules">{{$item->createduser }}</td>
             <td class="common_td_rules">{{$item->inv_remarks}}</td>
             </tr>
             @php
              $i++;
              @endphp
             @endforeach
             
             
  