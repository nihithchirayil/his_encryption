@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet"> -->
    <style>

        ::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 4px;
        }
        ::-webkit-scrollbar-thumb {
            border-radius: 2px;
            background-color: rgba(0,0,0,.5);
            -webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
        }
        .popupDiv {
            position: absolute;
        }


        .popDiv {
            display: none;
            background: #DEECF9 none repeat scroll 0 0;
            border-radius: 6px;
            box-shadow: 1px 1px 3px #d0d0d0;
            padding: 10px;
            position: absolute;
            top: 25px;
            min-width: 200pxf;
            z-index: 850 !important;
            width: 450px;
            /* right: -125px; */
        }

        .popDiv .show {
            display: block;
        }

        .pop_closebtn {
            background: #333 none repeat scroll 0 0;
            border-radius: 50%;
            box-shadow: 0 0 5px #a2a2a2;
            color: #fff;
            cursor: pointer;
            font-size: 12px;
            font-weight: bold;
            height: 20px;
            padding: 2px 6px;
            position: absolute;
            right: -10px;
            top: -4px;
            min-width: 20px;
        }


        .checkfilter {
            margin-right: 5px;
        }

        .box_header {
            background: #3b926a !important;
            color: #fff;
        }
        .table_box_header{
            background: #3b926a !important;
            color: #fff;
            height: 33px;
            text-align: center;
        }
        .box_body{
            font-size: 11px;
            height: 33px;     
        }
       

        .btn-success {
            background: #3b926a !important;
        }

        .mate-input-box{
            width: 100%;
            position: relative;
            padding: 15px 4px 4px 4px !important;
            border-bottom: 1px solid #01A881;
            box-shadow: 0 0 3px #CCC;
        }
        .mate-input-box label{
            position: absolute;
            top: -2px;
            left: 6px;
            font-size: 12px;
            font-weight: 700;
            color: #107a8c;
            padding-top: 2px;
        }
        .select2-container .select2-choice{
            height: 20px;
            line-height: 0.8;
        }
        .select2-arrow{
            top: -5px !important;
        }

        .ajaxSearchBox{
            width: 150% !important;
        }

    </style>
@endsection
@section('content-area')
    <!-- page content -->

    @if (isset($bill_head) && count($bill_head) > 0)
        <input type="hidden" id="edit_head_id" value="{{ $bill_head[0]->id }}">
        <input type="hidden" id="edit_uhid" value="{{ $bill_head[0]->uhid }}">
        <input type="hidden" id="edit_bill_no" value="{{ $bill_head[0]->bill_no }}">
        <input type="hidden" id="edit_bill_tag" value="{{ $bill_head[0]->bill_tag }}">
        <input type="hidden" id="edit_bill_amount" value="{{ $bill_head[0]->net_amount_wo_roundoff }}">
        <input type="hidden" id="edit_package_id" value="{{ $bill_head[0]->package_id }}">
        <input type="hidden" id="edit_actual_amount" value="{{ $bill_head[0]->bill_amount }}">
        <input type="hidden" id="edit_discount_type" value="{{ $bill_head[0]->discount_type }}">
        <input type="hidden" id="edit_discount_val" value="{{ $bill_head[0]->discount_amount }}">
        <input type="hidden" id="edit_outside_status" value="{{ $bill_head[0]->is_outside_patient }}">
        <input type="hidden" id="paid_status" value="{{ $bill_head[0]->paid_status }}">
        <input type="hidden" id="cancelled_status" value="{{ $bill_head[0]->cancelled_status }}">
        <input type="hidden" id="hidden_payment_type" value="{{ $bill_head[0]->payment_type }}">
        <input type="hidden" id="hidden_company_id" value="{{ $bill_head[0]->company_id }}">
        <input type="hidden" id="hidden_pricing_id" value="{{ $bill_head[0]->pricing_id }}">
    @endif


    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <div class="right_col" role="main" style="background: white; height: 730px;">
        {{-- <h5 class style="margin-right: 73px;">Service Bill</h5> --}}

        <input type="hidden" id="hospital_header" style="width:100%;" value="{{ $hospital_header }}">
        <input type="hidden" id="patient_id">
        <input type="hidden" id="op_no_hidden">
        <input type="hidden" id="pending_bill_show_config" value="{{ $pending_bill_show_config }}">
        <input type="hidden" id="pricing_id">
        <input type="hidden" id="visit_id">
        <input type="hidden" id="bill_head_ids">
        <input type="hidden" id="package_id">
        <input type="hidden" id="patient_age">
        <input type="hidden" id="visit_type">
        <input type="hidden" id="admitting_doctor_id">
        <input type="hidden" id="company_id">
        <input type="hidden" id="consulting_doctor_id">
        <input type="hidden" id="insured_status">
        <input type="hidden" id="edit_bd_id" value="">
        <input type="hidden" id="print_bill_id" value="">
        {{-- <input type="hidden" id="print_status" value=""> --}}
        <input type="hidden" id="service_id" value="">
        <input type="hidden" id="convert_code" value="{{ @$codeType ? $codeType : '' }}">
        <input type="hidden" id="convert_head_id" value=>
        <input type="hidden" id="convert_head" value=>
        <input type="hidden" id="hidden_convert_detail_ids" value=>

        <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
        <input type="hidden" id="intend_type">

        @if (isset($bill_detail))

            @foreach ($bill_detail as $item)
                <input type="hidden" id="bd_id_{{ $item->id }}" class="bill_details" data-id="{{ $item->id }}"
                    data-qty="{{ $item->qty }}" data-net-amount="{{ $item->net_amount }}"
                    data-item-id="{{ $item->item_id }}" data-pkg-id="{{ $item->package_id }}"
                    data-item-disc="{{ $item->item_desc }}" value="{{ $item->id }}" data-intend-id="{{$item->intend_id}}">
            @endforeach

        @endif


        <div class="col-md-12 padding_sm" style="margin-top:1px; ">
            <div class="box no-border no-margin " id="ser_bill_detail_div_1">
                <div class="box-header box_header" style="margin-bottom: 5px;">
                    <span class="padding_sm">Service Billing</span>
                </div>
                <div class="col-md-2 no-padding" style="margin-top: 1px;">
                    <div class="mate-input-box" id="bill_tag"style="height:45px!important;">
                        <label for="">Bill tag</label>
                        <div class="clearfix"></div>
                        <select class="form-control" id='search_bill_tag' onchange="billTagSearch();"
                            style="overflow:hidden">
                            <option value="">Select Bill Tag</option>
                            @if(isset($bill_tag))
                            @foreach ($bill_tag as $each)
                                <option value="{{ $each->code }}"> {{ $each->name }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-2" style="margin-top: 1px;">
                    <div class="mate-input-box" style="height:45px!important;">
                        <label for="">UHID</label>
                        <div class="clearfix"></div>
                        <input type="text" autocomplete="off" class="form-control hidden_search" id="patient_uhid" value="">
                        <input type="hidden" value="0" id="patient_uhid_hidden">
                        <button type="button" class="btn btn-sm btn-primary advanceSearchBtn"
                            style=" position: absolute; top: 15px; right: 0;"><i class="fa fa-search"></i></button>
                        <div class="ajaxSearchBox" id="patient_uhidAjaxDiv"
                            style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999;
                                                        position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-1 padding_sm" style="margin-top: 11px;">
                    <div class="checkbox checkbox-success inline no-margin">
                        <input id="out_side_patient" type="checkbox" class="filters" onclick=""
                            data-bs-backdrop="static" data-bs-keyboard="false" name="">
                        <label for="out_side_patient">Out Patient</label>
                    </div>
                </div>
                <div class="col-md-2 padding_sm " style="margin-top: 1px;">
                    <div class="mate-input-box" style="height:45px!important;">
                        <label for="">Patient Name</label>
                        <div class="clearfix"></div>
                        <input type="text" autocomplete="off" class="form-control" readonly="readonly" id="patient_name"
                            name="patient_name" value="" required>
                    </div>
                </div>
                <div class="col-md-2 padding_sm " style="margin-top:1px;">
                    <div class="mate-input-box" style="height:45px!important;">
                        <label for="">IP Number</label>
                        <div class="clearfix"></div>
                        <input type="text" autocomplete="off" class="form-control" readonly="readonly" id="Ipnumber"
                            name="Ipnumber" value="" required>

                    </div>
                </div>

                <div class="col-md-3 padding_sm" style="margin-top: 1px;">
                    <div class="col-md-12 no-padding">
                        
                        <div class="col-md-6 padding_sm">
                            <div class="mate-input-box" style="height:45px!important;">
                                <label for="">Phone Number</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" readonly="readonly" class="form-control" id="phone_number"
                                    name="phone_number" value="" required>
                            </div>
                        </div>

                        <div class="col-md-6 padding_sm">
                            <div class="mate-input-box" style="height:45px!important;">
                                <label for="">Room Type</label>
                                <div class="clearfix"></div>
                                <select class="form-control" id="roomType" disabled="true">
                                    <option value=""></option>

                                    @if (isset($room_type))
                                        @foreach ($room_type as $item)
                                            <option value="{{ $item->id }}">{{ $item->room_type_name }} </option>
                                        @endforeach
                                    @endif
                                </select>

                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

            

           



            <div class="col-md-12 no-padding " style="margin-top:0px;">
                <div class="col-md-2 padding_sm" id="consultDoc" style="margin-top: 1px;display:block;">
                    <div class="mate-input-box" style="height:45px!important;">
                        <label for="">Consulting Doctor</label>
                        <div class="clearfix"></div>
                        <select class="form-control" style="overflow: hidden;" id="consulting_doctor_name" style="">
                            <option value="">Select Doctor</option>
                            @if (isset($doctor_name))
                                @foreach ($doctor_name as $item)
                                    <option value="{{ $item->id }}">{{ $item->doctor_name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                </div>
                
                <div class="col-md-2 padding_sm " id="addOutsideDoctor" style="margin-top: px; display:block">
                    <div class="mate-input-box" style="height:45px!important;">

                        <label for="">Outside Doctor</label>
                        <div class="clearfix"></div>
                        <div class="col-md-1 no-padding">
                        <div class="">
                            <input id="DocFrmOut" type="checkbox" onclick="Docfromout();" class=""
                                name="outSideregistration" disabled="true">
                        </div>
                    </div>
                        <div class="col-md-10 no-padding">
                        <input type="text" autocomplete="off" class="form-control" readonly="readonly" id="docFrmOutside"
                            name="docFromoutside" value="" required>
                        </div>
                    </div>
                </div>
               
                {{-- <div class="col-md-1 padding_sm " style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Vist Type</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" class="form-control" readonly="readonly" id="visit_type"
                                name="visit_type" value="" required>
                        </div>
                    </div> --}}
                    <div class="col-md-2 padding_sm" style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Payment Type</label>
                            <div class="clearfix"></div>
                            <select class="form-control" id="paymentType" onchange="sumFunction();">
    
                                @if (isset($bill_details))
                                @foreach($bill_details as $item)
                                    <option>{{ $item->payment_type }} </option>
                                @endforeach
                                @endif
    
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm " style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for="">Company</label>
                            <div class="clearfix"></div>
                            <select class="form-control" id="company" onchange="editPricing();" disabled="true">
                                <option value="">Company</option>
                                @if (isset($company))
                                    @foreach ($company as $item)
                                        <option value="{{ $item->id }}">{{ $item->company_name }}</option>
                                    @endforeach
                                @endif
                            </select>
    
                        </div>
                    </div>
                    <div class="col-md-2 padding_sm " style="margin-top: 1px;">
                        <div class="mate-input-box" style="height:45px!important;">
                            <label for=""> Pricing</label>
                            <div class="clearfix"></div>
                            <select class="form-control" id="pricing" onchange="changePricingId();" disabled="true">
                                <option value="">Pricing</option>
                                @if (isset($pricing))
                                    @foreach ($pricing as $item)
                                        <option value="{{ $item->id }}">{{ $item->pricing_name }} </option>
                                    @endforeach
                                @endif
                            </select>
    
    
                        </div>
                    </div>
                    <div class="col-md-1 no-padding" style="margin-left: 15px;margin">
                        <div class="col-md-12 no-padding">
                        <input type="checkbox" autocomplete="off" class="padding_sm" onchange="checkPricing();" id="editpricing"
                            name="editpricing" style="margin-left: 10px;" disabled="true" value="" required>

                        <label for="Edit Pricing">Edit Pricing</label>
                        <button type="button" autocomplete="off" class="btn btn-primary btn-block" id="updatePricing" name=""
                            style="margin-top:0px;font-size:9px !important; height:" disabled="true" onclick="updatePricing();">
                            Update Pricing</button>
                        </div>
                    </div>
                   
                
                
               
                
            </div>
            <div class="col-md-3 padding_sm " style="margin-top: 1px; display:none">
                <div class="mate-input-box" style="height:45px!important;">
                    <label for="">Admitting Doctor</label>
                    <div class="clearfix"></div>
                    <input type="text" autocomplete="off" class="form-control" readonly="readonly"
                        id="admitting_doctor_name" name="admitting_doctor_name" value="" required>
                </div>
            </div>



            <div>



                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="col-md-1" style=" display:none;">
                        <div class="col-md-1 padding_sm" style="margin-top:10px;margin-left:10px;width:150px;">
                            <div class="checkbox checkbox-success inline no-margin">
                                <input id="billPackage" type="checkbox" class="filters" name="billPackage"
                                    disabled="true">
                                <label for="billPackage">Package</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 padding_sm">

                        <div class="col-md-1 padding_sm" style="margin-top:10px;margin:px;width:150px;">
                            <button type="button" autocomplete="off" class="btn btn-primary btn-block" id="" name=""
                                style="" onclick="packageLIst();" value="" required>
                                SELECT PACKAGE</button>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top:10px;margin:px;width:150px;">
                            <button type="button" autocomplete="off" onclick="billListDetails(1);"
                                class="btn btn-primary btn-block" id="bill_list" name="" style="" value="" required>BILL
                                LIST</button>
                        </div>
                        <div class="col-md-1 padding_sm" style="margin-top:10px;margin:px; width:150px;">
                            <button type="button" id="pending_bills_btn" class="btn btn-primary btn-block" style="">PENDING
                                BILL</button>
                        </div>
                        <div class="col-md-2 padding_sm" style="margin-top:10px;width:150px;">
                            <button type="button" autocomplete="off" class="btn btn-primary btn-block" id="" name="" style
                                value="" onclick="PatientInvestigation();" required>SELECT INVESTIGATION</button>
                        </div>
                        {{-- <div class="col-md-2 padding_sm" style="margin-top:10px;width:150px;">
                            <button type="button" autocomplete="off" class="btn btn-primary btn-block" id="" name=""
                                style="" value="" required> ADD/VIEW SPECIAL NOTES</button>
                        </div> --}}
                    </div>
                    <div class="col-md-3 padding_sm">
                        <div class="col-md-1 padding_sm pull-right" style="margin-left:0px;width:100px;">

                            <button type="button" autocomplete="off" class="btn btn-primary btn-block" id="" name=""
                                style="height:px;margin-top:10px;" onclick="addNewBill();">
                                Refresh</button>
                        </div>
                        <div class="col-md-1 padding_sm pull-right" style="margin-left:0px;width:100px; display:none;">

                            <button type="button" autocomplete="off" class="btn btn-primary btn-block" id="" name=""
                                style="height:px;margin-top:10px;" onclick="clearDetails();">
                                Clear</button>
                        </div>
                    </div>


                </div>

            </div>
        </div>


        <div class="col-md-12 box-body">

            <div class="col-md-12 table_box_header" style="padding:0!important;">
                <div class="col-md-4" style="padding:0!important;">
                    <div class="">
                        <div class="col-md-1 box-body table_box_header">#</div>
                        <div class="col-md-7 box-body table_box_header">
                            <input id="issue_search_box" onkeyup="searchbyName();" type="text" placeholder="Name Search.. "
                                style="color: rgb(0, 0, 0); width: 80%; display:none;">
                                <span id="item_search_btn" style=" float: right" class="btn btn-warning">
                                <i class="fa fa-search"></i></span><span id="item_search_btn_text"
                                style="display: block;">Item Description
                        </div>
                        <div class="col-md-1 box-body table_box_header">Qty</div>
                        <div class="col-md-3 box-body table_box_header">Total Amount</div>
                    </div>
                </div>
                @for($i=0;$i<2;$i++)
                <div class="col-md-4"style="padding:0!important;">
                    <div class="">
                        <div class="col-md-1 box-body table_box_header">#</div>
                        <div class="col-md-7 box-body table_box_header">Item Description</div>
                        <div class="col-md-1 box-body table_box_header">Qty</div>
                        <div class="col-md-3 box-body table_box_header">Total Amount</div>
                    </div>
                </div>
                @endfor
            </div>


            
            <div id="searchlist_div" class="theadscroll col-md-12"
                style="padding:0!important;position: relative;height:250px; overflow:scroll">
                
                <div class="col-md-12"  style="padding:0!important;"id="serviceListDiv"></div>

            </div>
            <div>
                <div class="col-md-4 box-body">
                    <div class="col-md-6 no-padding">
                        <div class="col-md-12">
                            <div class="mate-input-box" style="margin-top: 3px;height:45px!important;">
                                <label for="">Outstanding Amount</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" readonly=readonly
                                    class="form-control outstanding_amount" name="outStandingAmount" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mate-input-box" style="margin-top: 3px;height:45px!important;">
                                <label for="">Advance Amount</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" readonly=readonly class="form-control"
                                    id="advanceAmount" name="advanceAmount" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mate-input-box" style="margin-top: 1px;height:45px!important;">
                                <label for="">Balance In Amount</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" readonly=readonly class="form-control"
                                    id="balanceAmount" name="balanceAmount" value="" required>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 no-padding">

                        <div class="col-md-12">
                            <div class="mate-input-box" style="margin-top: 3px;height:45px!important;">
                                <label for="">Pricing Discount</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" readonly=readonly class="form-control"
                                    id="pricingDiscount" name="pricingDiscount" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mate-input-box" style="margin-top: 3px;height:45px!important;">
                                <label for="">Item Discount</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" readonly=readonly class="form-control"
                                    id="discount_val" name="totalDisc" value="0" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mate-input-box" style="margin-top: 3px;height:45px!important;">
                                <label for="">Total Tax</label>
                                <div class="clearfix"></div>
                                <input type="text" autocomplete="off" readonly=readonly class="form-control"
                                    id="totalTax" name="totalTax" value="" required>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                    <table class=" col-md-12" id="service_bill_save"   style="height: 175px;">
                        {{-- <thead>
                            <th style="margin-top: 0px;">Bill Discount</th>
                        </thead> --}}
                        <tbody>
                            <tr class="highlighted" style="background-color: rgb(228, 228, 228);">

                                <td>
                                    <div class="col-md-12">
                                        <h6>Bill Discount</h6>
                                        <div class="col-md-6 padding_sm">

                                            <div class="clearfix"></div>
                                            <input type="checkbox" autocomplete="off" onclick="discountAmtPer();"
                                                class="" id="discountPer" name="discount" value="" required>
                                            <label for="">%</label>
                                        </div>


                                        <div class="col-md-6 padding_sm">

                                            <div class="clearfix"></div>
                                            <input type="checkbox" autocomplete="off" onclick="addDiscountAmt();"
                                                class="" id="discountAmt" name="amount" value="" required>
                                            <label for="">Amount</label>

                                        </div>
                                        <div>
                                </td>
                                <td>
                                    <div class="col-md-12 padding_sm">
                                        <div class="clearfix"></div>
                                        <div class="mate-input-box" style="margin-top:3px;height:45px!important;">
                                            <label for="">Discount Amount</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" onkeyup="discountAdd();"
                                                class="form-control" id="dicAmnt" name="dicBillAmnt" value="" required>
                                        </div>
                                    </div>

                                </td>
                                <td>
                                    <div class="col-md-12 padding_sm">
                                        <div class="clearfix"></div>
                                        <div class="mate-input-box" style="margin-top:3px;height:45px!important;">
                                            <label for="">In%</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" class="form-control"
                                                id="inPercentgDisc" name="inPercentg" value="" readonly="readonly" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class=" col-md-12 padding_sm">
                                        <div class="mate-input-box" style="margin-top:3px;height:45px!important;">
                                            <label for="">Amount</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" class="form-control" readonly=readonly
                                                id="inPercenAmount" name="Amount" value="" required>
                                        </div>
                                    </div>
                                </td>


                            </tr>
                            <tr>
                                <td>
                                    <div class="col-md-12">
                                        <div class="mate-input-box" style="margin-top: 3px;height:45px!important;">
                                            <label for="">Bill Amount</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" readonly=readonly class="form-control"
                                                id="billAmount" name="billAmount" value="" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="col-md-12">
                                        <div class="mate-input-box" style="margin-top: 3px;height:45px!important;">
                                            <label for="">Patient Payable</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" class="form-control" readonly=readonly
                                                id="patientPayable" name="menu_name" value="" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="col-md-12">
                                        <div class="mate-input-box" style="margin-top: 3px;height:45px!important;">
                                            <label for="">Payor Payable</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" class="form-control" id="payorPayable"
                                                name="menu_name" value="" readonly=readonly required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="col-md-12">
                                        <div class="mate-input-box" style="margin-top: 3px;height:45px!important;">
                                            <label for="">Net Amount</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" class="form-control" id="netAmount"
                                                name="netAmount" value="" readonly=readonly required>
                                        </div>
                                    </div>

                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <div class="col-md-12">
                                        <div class="mate-input-box" style="margin-top: 3px;height:45px!important;">
                                            <label for="">Item Total</label>
                                            <div class="clearfix"></div>
                                            <input type="text" autocomplete="off" readonly=readonly class="form-control"
                                                id="itemTotal" name="itemTotal" value="" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{-- <div class="col-md-12">
                                        <div class="" style="margin-top: 3px;">
                                            <div class="clearfix"></div>
                                            <button type="button" autocomplete="off" class="btn light_purple_bg btn-block"
                                                id="" name="" value="" required><i id="searchresultspin" readonly=readonly
                                                    class="fa fa-add" aria-hidden="true">DISCOUNT
                                                    NARRATION</i></button>
                                        </div>
                                    </div> --}}
                                </td>
                                <td>
                                    {{-- <div class="col-md-12">
                                <div class="" style="margin-top: 3px;">
                                    <div class="clearfix"></div>
                                    <button type="button" autocomplete="off" class="btn btn-success btn-block"
                                        id="" name="" onclick="SaveDataJson();" value="" required><i
                                            id="searchresultspin" class="fa fa-add"
                                            aria-hidden="true">SAVE</i></button>
                                </div>
                            </div> --}}
                                </td>
                                <td>
                                    <div class="col-md-12">
                                        <div class="" style="margin-top: 3px;">
                                            <div class="clearfix"></div>
                                            <button type="button" id="printAndSave" autocomplete="off" class="btn btn-success btn-block"
                                                id="" name="" onclick="SaveDataJson();" value="" required><i
                                                    id="searchresultspin" readonly=readonly class="fa fa-add"
                                                    aria-hidden="true">SAVE/PRINT</i></button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>




                </div>
            </div>
        </div>
    </div>
    </div>

    {{-- out side patient --}}
    {{-- <div class="modal fade hidden.bs.modal" id="outsidepatient-lg" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Outside Registration</h4>
                    <button type="button"  onclick="uncheckcheckbox();" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="text-success"  aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" id="outSideReg">

                </div>
            </div>
        </div>
    </div> --}}

    {{-- pending bill model --}}
    <div class="modal fade hidden.bs.modal " id="bd-pending-bill-modal-lg" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width:1000px;">
                <div class="modal-header box-header box_header">
                    <h4 class="modal-title" id="myLargeModalLabel">PENDING REQUEST</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="text-success" aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" id="pending_bill_list" style="min-height: 300px;">

                </div>
            </div>
        </div>
    </div>
    <!-------print modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="serv_print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    {{-- <h4 class="modal-title" style="color: white;">Print Configuration</h4> --}}

                    <div class="col-md-12">
                        

                        <div class="col-md-2 pull-right">
                            <button type="button" onclick="addNewBill();" class="close pull-right" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>


                </div>
                <div class="modal-body" style="height:145px;">
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                    <div class="col-md-10">
                            <div id="bill_saved_name">

                            </div>
                            <div id="bill_saved_bill_no">

                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                        <input style="margin-left: 15px;" type="checkbox" name="is_duplicate" id="duplicate">
                        Duplicate Print
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="addNewBill();" class="btn btn-primary pull-right" data-dismiss="modal"
                            style="color:white">
                            <i class="" aria-hidden="true"></i> OK
                        </button>

                        <!-- <button onclick="" id="PayNow" class="btn btn-primary pull-right" style="color:white;display:block;">
                            <i class="fa fa-credit-card" aria-hidden="true"></i> Pay Now
                        </button> -->
                        <button onclick=" PrintBill();" class="btn bg-primary pull-right" style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>

                    </div>


                </div>
            </div>
        </div>
    </div>


    @include('Emr::emr.service_bill.patientInvestigation')
    @include('Master::pharmacy_billing.outside_patient_registration')
    @include('Master::RegistrationRenewal.advancePatientSearch')
    @include('Master::pharmacy_billing.pending_bills_list')
    @include('Emr::pharmacy_bill_list.cash_receive_model')

@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/service_bill.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/patientInvestigation.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/cash_receive.js') }}"></script>

    <script
        src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>



@endsection
