
<div class="">
    @if (isset($service_list))
    
    @foreach($service_list as $each)
    <div class="col-md-4  box_body search_service_class" style="padding: 0px!important;" data-attr-service-desc='{{$each->service_descrip}}'>
        <div class="">
            <div class="col-md-1 box-body box_body " style="text-align: center;">
                <input   type="checkbox" style=" margin-left: -2px;" onchange="sumFunction();"  class="serv_list"  data_detail_id=""data-actual-amt="{{$each->actualvalue}}" data-doc-required="{{$each->is_doc_required}}" data-is-is_price_editable="{{$each->is_price_editable}}" data-is-refundable="{{$each->is_refundable}}" data-isdiscountable="{{$each->discountable}}" data-exemption="{{$each->isinsuranceexemption}}" data-pricing-disc="{{$each->pricingdiscamt}}" data-pridisc-per="{{$each->pricingdiscper}}" data-id="{{$each->id}}" data-discount="{{$each->discvalue}}"  data-dep-code="{{$each->dept_code}}" data-subdep="{{$each->sub_dept_code}}"  data-serv-code="{{$each->service_code}}" data-serv-desc="{{$each->service_descrip}}" data-price ="{{$each->calculatedprice}}"  id = "add_{{$each->id}}">
                <label for= "service_list_check"></label>
            </div>
            <div class="col-md-7 box_body box-body padding-top:0px!important; " text="{{$each->service_descrip}}">
                <span class="col-md-10"style="overflow:auto;">{{$each->service_descrip}}</span><button type="button" class="btn-info pull-right popUpBtn" onclick="popupDiv({{$each->id}});" style="border-radius: 50%; width: 20px; height: 20px; border: none;"  >
                    <i class="fa fa-info"> </i>
                </button>
                
                <div class="popupDiv">
                    <div class="popDiv" id="popup{{$each->id}}" style="display: none;">
                        @php
                                if($each->is_discountable == true){
                                    $disabled = '';
                                }
                                else{
                                    $disabled = 'disabled';
                                }
                                @endphp
                        {{-- <input type="checkbox" class="discamount" @if($each->is_discountable == true) disabled="disabled" @endif onchange="autosum({{$each->id}},{{$each->pricingdiscamt}})" {{$disabled}}  name="dicountamt{{$each->id}}" id="perType{{$each->id}}"  value="1">In % --}}

                        {{-- <input type="checkbox" class="discamount" @if($each->is_discountable == true) disabled="disabled" @endif onchange="autosum({{$each->id}},{{$each->pricingdiscamt}})"  {{$disabled}} name="dicountamtper{{$each->id}}" id="amtType{{$each->id}}"   value="2">In Amount --}}
                       <button type="button" class="close" id="ppclose{{$each->id}}"data-dismiss="modal" aria-label="Close">
                           <span class="text-info" aria-hidden="true">×</span>
                       </button>
                        <div class="col-md-12">
                            <div class="col-md-6"><b>Name</b></div>
                            <div class="col-md-6"><b>Details</b></div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">Actual Amount</div>
                            <div class="col-md-6">
                                <input type="text" class = "form-control" id="Actualamount{{$each->id}}" value="{{$each->calculatedprice}}" readonly="readonly">
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">Discount
                                

                                
                            </div>
                            <div class="col-md-6">
                               
                                       

                                        <input type="number" name="itemDiscount"class="form-control" disabled="disabled" id="discount{{$each->id}}" onkeyup="autosum({{$each->id}},{{$each->pricingdiscamt}});" min="0"max="100" value="0" maxlength="3"  >

                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">Pricing Discount</div>
                            <div class="col-md-6">
                                <input type="text" class ="form-control" id="p_discount{{$each->id}}" value="{{$each->pricingdiscamt}}" readonly="readonly">
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">Qty</div>
                            <div class="col-md-6">
                                    <input type="text" class = "form-control" id="qty{{$each->id}}" value="1" readonly="readonly">
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">Total Price</div>
                            <div class="col-md-6">
                                <input type="number" class="form-control" id="price_amount{{$each->id}}"  id="amount_{{$each->id}}"  value="{{$each->calculatedprice}}" readonly="readonly">
                            </div>
                        </div> 
               
                    </div>
                </div>

            </div>
            <div class="col-md-1 box_body box-body">
                <input type = "text" class ="form-control" onkeyup="autosum({{$each->id}},{{$each->pricingdiscamt}});" id="qty_{{$each->id}}" value="1">
            </div>
            <div class="col-md-3 box_body box-body ">
                @if($each->is_price_editable==1)
                    <input type = "text" class="form-control" onkeyup="autosum({{$each->id}},{{$each->pricingdiscamt}});"  id="amount_{{$each->id}}"  value="{{$each->calculatedprice}}" >
                @else
                    <input type = "text" class="form-control" onkeyup="autosum({{$each->id}},{{$each->pricingdiscamt}});"  id="amount_{{$each->id}}"  value="{{$each->calculatedprice}}" readonly="readonly">
                @endif
            </div>
        </div>
        
    </div>
    @endforeach
    
    @else
        No Records found
    @endif
</div>