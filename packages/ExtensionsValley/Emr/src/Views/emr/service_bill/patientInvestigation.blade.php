<div class="modal fade modal" id="patientinvestigation-lg" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width:1200px;">
        <div class="modal-content">
            <div class="modal-header  box_header">
                <h4 class="modal-title pull-left" id="myLargeModalLabel"> PATIENT INVESTIGATION</h4>
                <button type="button" onclick="" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span id="close_patient_inves" class="text-success" aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="patient_investigation">
                <div class="">
                    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
                    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">

                    <div class="row codfox_container">


                        <div class="box-body clearfix">
                            <div class="col-md-12">
                                <div class="box no-border">

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="col-md-2 pull-right">
                                                <div><button onclick="convert_bill_details()"
                                                        class="btn btn-primary btn-block pull-right">Convert To
                                                        Bill</button> </div>
                                            </div>
                                            <div class="col-md-3 " style="margin-top:0px;">
                                                <table class="table table-contensed table_sm"
                                                    style="margin-bottom:4px;">
                                                    <thead>
                                                        <tr class="box_header">
                                                            <th colspan="11">
                                                                PATIENT INVESTIGATION
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>

                                                <div class="col-md-12">
                                                    <div class="mate-input-box">
                                                        <label for="">UHID</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" autocomplete="off"
                                                            class="form-control hidden_search" id="conuhid" value="">
                                                        <input type="hidden" value="0" id="conuhid_hidden">
                                                        <div class="ajaxSearchBox" id="conuhidAjaxDiv"
                                                            style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999;
                                                position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="mate-input-box  ">
                                                        <label for="">Patient Name</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" autocomplete="off"  class="form-control " id="intent_patient_name" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="mate-input-box" style="">
                                                        <label for="">Doctor</label>
                                                        <input type="text" autocomplete="off"
                                                            class="form-control hidden_search" id="condoctor" value="">
                                                        <input type="hidden" value="0" id="condoctor_hidden">
                                                        <div class="ajaxSearchBox" id="condoctorAjaxDiv"
                                                            style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999;
                                                position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="mate-input-box  " style="">
                                                        <label for="">Investigation Type</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" autocomplete="off"
                                                            class="form-control hidden_search" id="contype" value="">
                                                        <input type="hidden" value="0" id="contype_hidden">
                                                        <div class="ajaxSearchBox" id="contypeAjaxDiv"
                                                            style="text-align: left; list-style: none;  cursor: pointer; max-height: 350px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999;
                                                position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                                {{-- <div class="col-md-12">
                                                    <div class="mate-input-box  " style="">
                                                        <label for="">Investigation No</label>
                                                        <div class="clearfix"></div>
                                                        <input type="text" autocomplete="off" class="form-control"
                                                            id="investigation_no" value="">

                                                    </div>
                                                </div> --}}
                                                <div class="col-md-12">
                                                    <div class="col-md-4 padding_sm box-body text-alignment "
                                                        style="margin-top: 1px; background-color:darkcyan !important">
                                                        <input class="padding_sm" class="checkfilter"
                                                            id="converted" name="checkfilter"
                                                            onclick="patientInvestiGationList();" type="radio"
                                                            style="margin-left: 1px;margin-right:3px;">Converted
                                                    </div>
                                                    <div class="col-md-4 padding_sm box-body text-alignment "
                                                        style="margin-top: 1px;background-color:darkgoldenrod !important">
                                                        <input class="padding_sm" class="checkfilter"
                                                            id="partially" name="checkfilter"
                                                            onclick="patientInvestiGationList();" type="radio"
                                                            style="margin-left: 1px;margin-right:3px;">Partially
                                                    </div>
                                                    <div class="col-md-4 padding_sm box-body text-alignment "
                                                        style="margin-top: 1px;background-color:#fff!important">
                                                        <input class="padding_sm" class="checkfilter"
                                                            id="pending" name="checkfilter"
                                                            onclick="patientInvestiGationList();" type="radio"
                                                            style="margin-left: 1px;margin-right:3px;">Pending
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-12 padding_sm box-body text-alignment "
                                                        style="margin-top: 1px; background-color:rgb(151, 128, 85) !important">
                                                        <input type="radio" class="checkfilter" checked
                                                            id="prt_pending" onclick="patientInvestiGationList()"
                                                            name="checkfilter"
                                                            style="margin-right:3px;">Pending/Partially
                                                        Converted
                                                    </div>
                                                </div>
                                                    <div class="col-md-12 text-alignment" style="margin-top:10px;">
                                                        
                                                        <div class="col-md-4 ">
                                                            <div class="">
                                                                <input class="ipop" id="IP" type="checkbox"
                                                                    name="visit_type" onclick="patientInvestiGationList();" value='IP'>
                                                                <label class="text-blue" for="IP">
                                                                    IP
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 ">
                                                            <div class="">
                                                                <input class="ipop" id="OP" type="checkbox"
                                                                    name="visit_type" onclick="patientInvestiGationList();" value='OP'>
                                                                <label class="text-blue" for="OP">
                                                                    OP
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 padding_sm " style="margin-top: 10px;">
                                                        <div class="mate-input-box">
                                                            <label for="">From Date</label>
                                                            <div class="clearfix"></div>
                                                            <input data-attr="date" autocomplete="off" 
                                                                id="from_date" name="from_date"
                                                                value="{{date('M-d-y')}}" class="form-control datepicker filters reset"
                                                                placeholder="YYYY-MM-DD" id="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 padding_sm " style="margin-top: 10px;">
                                                        <div class="mate-input-box">
                                                            <label for="">to Date</label>
                                                            <div class="clearfix"></div>
                                                            <input data-attr="date" autocomplete="off"
                                                                id="to_date"  value="{{date('M-d-y')}}" class="form-control datepicker filters reset"
                                                                placeholder="YYYY-MM-DD">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="margin-top:10px">

                                                        <button class="btn btn-block btn-primary col-md-6 pull-right"
                                                            onclick="refreshForm();"> 
                                                            <i class="fa fa-refresh"></i> Refresh</button>
                                                    </div>
                                                    <div class="col-md-6" style="margin-top:10px">
                                                            <button class="btn btn-block btn-primary col-md-6 pull-right"
                                                            onclick="patientInvestiGationList();">
                                                            <i class="fa fa-search"></i> Search</button>

                                                    </div>
                                                    
                                                    
                                                {{-- </div> --}}

                                            </div>
                                            <div class="col-md-9 box-body">
                                                <div id="" class="theadscroll" style="position: relative; height: 200px; border: 1px solid #dcdcdc; background:white;width:102%" >
                                                    <table class=" theadfix_wrapper  table-bordered table_sm no-margin no-border">
                                                        <thead>
                                                        <tr class="table_header_bg">
                                                        <th style="width:2%">#</th>
                                                        <th style="width:2%">Add</th>
                                                        <th style="width:2%">View</th>
                                                        <th style="width:2%">Print</th>
                                                        <th style="width:12%">UHID</th>
                                                        <th style="width:10%">Patient Name</th>
                                                        <th style="width:5%">Visit</th>
                                                        <th style="width:5%">Type</th>
                                                        <th style="width:5%">Date</th>
                                                        <th style="width:10%">Doctor</th>
                                                        <th style="width:3%">Location</th>
                                                        <th style="width:10%">Created By</th>
                                                        <th style="width:5%">Remarks</th>
                                                        </tr>
                                                        </thead>
                                                       
                                                        <tbody id="patient_investigation_div" style="min-height:200px; max-height: 200px;">

                                                        </tbody>  
                                                    </table>


                                                </div>

                                                <div>
                                                    <div class="col-md-12 box-body">


                                                        <div class="col-md-2 padding_sm box-body text-alignment  "
                                                            style="margin-top: 1px;">
                                                            <input type="radio" class="checkfilter" id="regular"
                                                                name="checkfilter" onclick="patientInvestiGationList();"
                                                                style="margin-left: 10px;margin-right:3px;">Regular
                                                        </div>
                                                        <div class="col-md-2 padding_sm box-body text-alignment "
                                                            style="margin-top: 1px; background-color:coral !important">
                                                            <input type="radio" class="checkfilter" id="emergency"
                                                                name="checkfilter" onclick="patientInvestiGationList();"
                                                                style="margin-left: 10px;margin-right:3px;">Emergency
                                                        </div>


                                                        <div class="col-md-2 padding_sm box-body text-alignment "
                                                            style="margin-top: 1px; background-color:blueviolet !important">
                                                            <input type="radio" class="checkfilter"
                                                                id="new_admission" onclick="patientInvestiGationList();"
                                                                name="checkfilter" style="margin-left: 10px;margin-right:3px;">New
                                                            Admission
                                                        </div>
                                                        <div class="col-md-2 padding_sm box-body text-alignment "
                                                            style="margin-top: 1px; background-color:rgb(72, 119, 72) !important">
                                                            <input type="radio" class="checkfilter" id="discharge"
                                                                name="checkfilter" onclick="patientInvestiGationList();"
                                                                style="margin-left: 10px;margin-right:3px;">Discharge
                                                        </div>


                                                        
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-12 ">
                                                    <h6><span> INVESTIGATION DETAILS</span></h6>
                                                    <div id="bill_details" class="theadscroll"
                                                        style="position: relative;min-height:200px; max-height: 200px; overflow:scroll margin-right:3px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!----------------replace item model----------------------->
            <style>
                .modal-header {
                    background-color: #169F85 !important;
                }

            </style>

            <div class="modal fade" id="replace-lg" tabindex="-1" role="dialog"
                aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title pull-left" id="myLargeModalLabel">Replace Service Item</h4>
                            <button type="button" onclick="replaceModalclose();" class="close"
                                aria-label="">
                                <span class="text-success" aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="data_div box-body" style="height:300px;">
                            <input type="hidden" value="" id="replace_id">
                            <input type="hidden" value="" id="replace_item_code">
                        
                            <input type="hidden" value="" id="head_id_hidden">
                            <input type="hidden" value="" id="investigation_type_hidden">
                            <div class="col-md-12">
                                <div class="col-md-6 padding_sm " style="margin-top: 10px;">
                                    <div class="mate-input-box">
                                        <label for="">Item Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control hidden_search"
                                            id="replace_item" value="">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding_sm">
                                <div class="col-md-6 padding_sm " style="margin-top: 10px;">
                                    <div class="mate-input-box">
                                        <label for="">Replace Item</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control hidden_search"
                                            id="replace" value="">
                                        <input type="hidden" value="0" id="replace_hidden">
                                        <div class="ajaxSearchBox" id="replaceAjaxDiv"
                                            style="text-align: left; list-style: none;  cursor: pointer; max-height: 200px; margin: 14px -5px 0px; &quot; &quot;overflow-y: auto; width: 34%; z-index: 99999;
                                            position:absolute; background: #ffffff;  border-radius: 3px;  &quot;border: 1px solid rgba(0, 0, 0, 0.3); ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm margin-top:10px;">
                                    <button class="btn btn-primary" onclick="replace();"> Replace</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
    </div>
    </div>
    {{-- @section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/patientInvestigation.js') }}"></script>

@endsection --}}
