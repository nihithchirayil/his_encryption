<div class="col-md-12 no-padding box-body " style="margin-top: 10px;">

    <table class="theadfix_detail  table-bordered table_sm no-margin no-border" style="width:100%;">

        <thead>
            <tr class="table_header_bg">
                <th class="text-center">#</th>
                <th class="text-center">Created At</th>
                <th class="text-center">Service Name</th>
                <th class="text-center">Type</th>
                <th class="text-center">Department</th>
                <th class="text-center">Subdepartment</th>
                <th class="text-center">Qty</th>
                <th class="text-center">Rem Qty</th>
                <th class="text-center">Replace</th>
                <th class="text-center">Cancel</th>
            </tr>
        </thead>
        @php
            $i = 1;
        @endphp
        <tbody>
            @foreach ($inv_detail as $item)
                <tr style="width:100%" data-id="{{ $item->id }}" data-head_id="{{$item->head_id}}" data-item_name="{{$item->service_disc}}" data-investigation-type="{{$item->type}}" >
                    <td class="td_common_numeric_rules">{{ $i }}</td>
                    <td class="common_td_rules">{{ date('M-d-Y', strtotime($item->created_at))}}</td>
                    <td class="common_td_rules">{{ $item->service_disc }}</td>
                    <td class="common_td_rules">{{ $item->type }}</td>
                    <td class="common_td_rules">{{ $item->dept_name }}</td>
                    <td class="common_td_rules">{{ $item->sub_dept_name }}</td>
                    <td class="common_td_rules">{{ $item->qty }}</td>
                    <td class="common_td_rules">{{ $item->rem_qty }}</td>
                    <td class="common_td_rules"><button id="replace_button" 
                             data-bs-backdrop="static" data-bs-keyboard="false"
                             onclick="replaceItems(this);"><i class="fa fa-exchange"></i> </button></td>
                    <td class="common_td_rules"><button onclick="cancel_item({{$item->id}},{{$item->head_id}}, '{{$item->type}}')"><i class="fa fa-trash"></i></button></td>
                </tr>
                @php
                    $i++;
                @endphp
            @endforeach
        </tbody>


    </table>

</div>
  