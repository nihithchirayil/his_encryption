<style>
    label{
        margin-right:10px;
        float:left;clear:right;
        font-size:12px !important;
    }
    .form-control{
        float: left;
        clear:right;
        margin-right:10px;

    }
    .col-md-12{
        margin-top:8px;
    }
    .radio-success{
        width: 130px;
        margin-left: 10px;
        float: left;
        clear:right;
    }
</style>
@php
    $post_op_assessment_data = $resultData->post_op_assessment_data;
    if($post_op_assessment_data !=''){
        $post_op_assessment_data = json_decode($post_op_assessment_data);
    }
@endphp

@if($post_op_assessment_data !='')
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Level of consciousness</label>
    <label style="font-weight:600;width:80px;">{{$post_op_assessment_data->imm_post_op_level_of_consciouness}}</label>
    <label style="margin-right:10px;float:left;clear:right;">Coherence</label>
    <label style="font-weight:600;width:80px;">{{$post_op_assessment_data->imm_post_op_coherence}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Reflexes</label>
    <label style="font-weight:600;width:80px;">{{$post_op_assessment_data->imm_post_op_reflexes}}</label>
    <label style="margin-right:10px;float:left;clear:right;">Head lift</label>
    <label style="font-weight:600;width:50px;">{{$post_op_assessment_data->imm_post_op_head_lift}}</label>
    <label style="margin-right:10px;float:left;clear:right;">P.R</label>
    <label style="font-weight:600;width:50px;">{{$post_op_assessment_data->imm_post_op_pr}}</label>

    <label style="margin-right:10px;float:left;clear:right;">BP</label>
    <label style="font-weight:600;width:50px;">{{$post_op_assessment_data->imm_post_op_bp}}</label>

    <label style="margin-right:10px;float:left;clear:right;">R.R</label>
    <label style="font-weight:600;width:50px;">{{$post_op_assessment_data->imm_post_op_rr}}</label>
    <label style="margin-right:10px;float:left;clear:right;">SPO2</label>
    <label style="font-weight:600;width:50px;">{{$post_op_assessment_data->imm_post_op_spop2}}</label>
</div>

<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">SA/EA:Sensory Level</label>
    <label style="font-weight:600;width:400px;">{{$post_op_assessment_data->imm_post_op_sa_ea_sensory_level}}</label>
    <label style="margin-right:10px;float:left;clear:right;">Motor Level</label>
    <label style="font-weight:600;width:400px;">{{$post_op_assessment_data->imm_post_op_motor_level}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Patient transferred to</label>
    <div class=" inline no-margin ">

        @if(isset($post_op_assessment_data->imm_post_op_patient_transferred_to_recovery) && $post_op_assessment_data->imm_post_op_patient_transferred_to_recovery == 'recovery')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
         @endif
        <label class="text-blue" for="">
            Recovery
        </label>
    </div>
    <div class=" inline no-margin ">

        @if(isset($post_op_assessment_data->imm_post_op_patient_transferred_to_icu) && $post_op_assessment_data->imm_post_op_patient_transferred_to_icu == 'icu')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
         @endif
        <label class="text-blue" for="">
            ICU
        </label>
    </div>
    <div class=" inline no-margin ">

        @if(isset($post_op_assessment_data->imm_post_op_patient_transferred_to_others) && $post_op_assessment_data->imm_post_op_patient_transferred_to_others == 'others')
        <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
 @endif
        <label class="text-blue" for="imm_post_op_patient_transferred_to_others">
            Other (specify)
        </label>
    </div>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Other Notes (if any)</label>
    <label style="font-weight:600;">{{$post_op_assessment_data->imm_post_op_other_notes_if_any}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Monitoring during shifting:</label>
    <label style="font-weight:600;width:400px;">{{$post_op_assessment_data->imm_post_op_monitoring_drug_shifting}}</label>
    <label style="margin-right:10px;float:left;clear:right;">Duration of shifting</label>
    <label style="font-weight:600;width:400px;">{{$post_op_assessment_data->imm_post_op_duration_of_shifting}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Post Shifting Vitals (After shifting</label>
    <label style="font-weight:600;">{{$post_op_assessment_data->imm_post_op_post_shifting_vitals}}</label>
</div>

<div class="col-md-12" style="white-space:nowrap;">
    <div class="col-md-6" style="white-space:nowrap;">
        <h4>Pre-Discharge assessment (Recovery Room)</h4>
        <br>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:95px;float:left;clear:right;">Vitals Stable</label>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_vitals_stable) && $post_op_assessment_data->imm_post_op_vitals_stable == 1)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="">
                    Yes
                </label>
            </div>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_vitals_stable) && $post_op_assessment_data->imm_post_op_vitals_stable == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue" for="">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:95px;float:left;clear:right;">Patient able to walk</label>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_patient_able_to_walk) && $post_op_assessment_data->imm_post_op_patient_able_to_walk == 'independent')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_patient_able_to_walk_Independent">
                    Independent
                </label>
            </div>
            <div class=" inline no-margin" style="margin-left:50px !important;">

                @if(isset($post_op_assessment_data->imm_post_op_patient_able_to_walk) && $post_op_assessment_data->imm_post_op_patient_able_to_walk == 'with_assist')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_patient_able_to_walk_with_assist">
                    With Assist
                </label>
            </div>
            <div class=" inline no-margin " style="margin-left:45px !important;">


                @if(isset($post_op_assessment_data->imm_post_op_patient_able_to_walk) && $post_op_assessment_data->imm_post_op_patient_able_to_walk == 'unable')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_patient_able_to_walk_unable">
                    Unable
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:95px;float:left;clear:right;">Vomiting</label>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_vomiting) && $post_op_assessment_data->imm_post_op_vomiting == 1)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_vomiting_yes">
                    Yes
                </label>
            </div>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_vomiting) && $post_op_assessment_data->imm_post_op_vomiting == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue" for="imm_post_op_vomiting_no">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:117px;float:left;clear:right;">Pain</label>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_pain) && $post_op_assessment_data->imm_post_op_pain == 'mild')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_pain_mild">
                    Mild
                </label>
            </div>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_pain) && $post_op_assessment_data->imm_post_op_pain == 'moderate')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_pain_moderate">
                    Moderate
                </label>
            </div>
            <div class=" inline no-margin " style="margin-left:35px !important;">

                @if(isset($post_op_assessment_data->imm_post_op_pain) && $post_op_assessment_data->imm_post_op_pain == 'severe')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_pain_severe">
                    Severe
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:94px;float:left;clear:right;">Bleeding</label>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_bleeding) && $post_op_assessment_data->imm_post_op_bleeding == 1)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_bleeding_yes">
                    Yes
                </label>
            </div>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_bleeding) && $post_op_assessment_data->imm_post_op_bleeding == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue" for="imm_post_op_bleeding_no">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:19px;float:left;clear:right;">Condition at Discharge</label>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_condition_discharge) && $post_op_assessment_data->imm_post_op_condition_discharge == 'stable')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_condition_discharge_stable">
                    Stable
                </label>
            </div>
            <div class=" inline no-margin " style="margin-left:15px !important;">

                @if(isset($post_op_assessment_data->imm_post_op_condition_discharge) && $post_op_assessment_data->imm_post_op_condition_discharge == 'fair')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_condition_discharge_fair">
                    fair
                </label>
            </div>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_condition_discharge) && $post_op_assessment_data->imm_post_op_condition_discharge == 'unstable')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_condition_discharge_unstable">
                    Unstable
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:44px;float:left;clear:right;">Discharge Criteria</label>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_discharge_criteria) && $post_op_assessment_data->imm_post_op_discharge_criteria == 'met')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue" for="imm_post_op_discharge_criteria_met">
                    Met
                </label>
            </div>
            <div class=" inline no-margin ">

                @if(isset($post_op_assessment_data->imm_post_op_discharge_criteria) && $post_op_assessment_data->imm_post_op_discharge_criteria == 'not_met')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                <label class="text-blue" for="imm_post_op_discharge_criteria_not_met">
                    Not Met
                </label>
            </div>
        </div>
    </div>
    <div class="col-md-6" style="white-space:nowrap;">
        <h4>Aldrete Score</h4>
        <br>Criteria for Determination of Discharge Score For
Release from the Post Anaesthesia Care unit

        <table id="result_data_table" class="table table-condensed table_sm table-bordered theadfix_wrapper" style="font-size: 12px;">
            <thead>
                <tr>
                    <td>
                        Variable Evaluated
                    </td>
                    <td>
                        Score
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="text-align: left;" colspan="2"><b>Activity</b></td>
                </tr>
                <tr>
                    <td style="text-align: left;">Able to move four extremities on command</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_activity) && $post_op_assessment_data->imm_post_op_activity == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif

                            <label class="text-blue" for="imm_post_op_activity_2">
                                2
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Able to move two extremities on command</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_activity) && $post_op_assessment_data->imm_post_op_activity == 1)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif

                            <label class="text-blue" for="imm_post_op_activity_1">
                                1
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Able to move no extremities on command</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_activity) && $post_op_assessment_data->imm_post_op_activity == 0)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif

                            <label class="text-blue" for="imm_post_op_activity_0">
                                0
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2"><b>Breathing
                    </b></td>
                </tr>
                <tr>
                    <td style="text-align: left;">Able to breathe deeply and cough freely</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_breathing) && $post_op_assessment_data->imm_post_op_breathing == 2)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif

                            <label class="text-blue" for="imm_post_op_breathing_2">
                                2
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Dyspnea</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_breathing) && $post_op_assessment_data->imm_post_op_breathing == 1)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif

                            <label class="text-blue" for="imm_post_op_breathing_1">
                                1
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Apnea</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_breathing) && $post_op_assessment_data->imm_post_op_breathing == 0)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif

                            <label class="text-blue" for="imm_post_op_breathing_0">
                                0
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2"><b>Circulation
                    </b></td>
                </tr>
                <tr>
                    <td style="text-align: left;">Blood pressure+/-20% of the preanesthetic level</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_circulation) && $post_op_assessment_data->imm_post_op_circulation == 2)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif

                            <label class="text-blue" for="imm_post_op_circulation_2">
                                2
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Blood pressure is +/-20% to 49% of the preanesthetic level</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_circulation) && $post_op_assessment_data->imm_post_op_circulation == 1)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif

                            <label class="text-blue" for="imm_post_op_circulation_1">
                                1
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Blood pressure >+/-50% of the preanesthetic level</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_circulation) && $post_op_assessment_data->imm_post_op_circulation == 0)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif
                            <label class="text-blue" for="imm_post_op_circulation_0">
                                0
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2"><b>Consciousness
                    </b></td>
                </tr>

                <tr>
                    <td style="text-align: left;">Fully awake</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_consciousness) && $post_op_assessment_data->imm_post_op_consciousness == 2)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif

                            <label class="text-blue" for="imm_post_op_consciousness_2">
                                2
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Arousable</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_consciousness) && $post_op_assessment_data->imm_post_op_consciousness == 1)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif

                            <label class="text-blue" for="imm_post_op_consciousness_1">
                                1
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Not responding</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_consciousness) && $post_op_assessment_data->imm_post_op_consciousness == 0)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif


                            <label class="text-blue" for="imm_post_op_consciousness_0">
                                0
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2"><b>Oxygen Saturation (Pulse Oximetry)
                    </b></td>
                </tr>

                <tr>
                    <td style="text-align: left;"> >92% while breathing room air</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_o2_saturation) && $post_op_assessment_data->imm_post_op_o2_saturation == 2)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif


                            <label class="text-blue" for="imm_post_op_o2_saturation_2">
                                2
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Needs supplemental oxygen to maintain saturation >90%</td>
                    <td>
                        <div class=" inline no-margin ">
                            @if(isset($post_op_assessment_data->imm_post_op_o2_saturation) && $post_op_assessment_data->imm_post_op_o2_saturation == 1)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif

                            <label class="text-blue" for="imm_post_op_o2_saturation_1">
                                1
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">< 90% even with supplemental oxygen</td>
                    <td>
                        <div class=" inline no-margin ">

                            @if(isset($post_op_assessment_data->imm_post_op_o2_saturation) && $post_op_assessment_data->imm_post_op_o2_saturation == 0)
                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                        @endif

                            <label class="text-blue" for="imm_post_op_o2_saturation_0">
                                0
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>Total Score
                    </b></td>
                    <td>
                        <label style="width:80px;font-weight:700;">{{$post_op_assessment_data->imm_post_op_total_score}}</label>

                </tr>



            </tbody>
        </table>
        A score of 9 or 10 is needed to discharge the patien
    </div>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    @if($signatureImage !='')
    @php
        $signatureImage = "data:image/jpeg;base64,".base64_decode($signatureImage);
    @endphp
        <img id="dr_signature" style="width:175px; height:70px;"
        src="{{$signatureImage}}"><br>
    @endif
    <label style="margin-right:10px;float:left;clear:right;">{{ucwords($anaesthetist)}}</label><br>
    <div class="clearfix"></div>
    <label style="margin-right:10px;float:left;clear:right;">License No:{{ucwords($anaesthetist_license_no)}}</label>

</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Date & Time :</label>
    <label style="width:400px;font-weight:700;">{{$post_op_assessment_data->imm_post_op_date_and_time}}</label>
</div>
@endif

