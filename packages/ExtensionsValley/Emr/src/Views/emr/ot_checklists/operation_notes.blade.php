<div class="theadscroll" style="position: relative; height:560px;">

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#operation_notes">Operation Notes</a></li>
        <li><a data-toggle="tab" href="#operation_orders">Operation Orders</a></li>
    </ul>

    <div class="tab-content">
        {{-- Operation notes section --}}
        <div id="operation_notes" class="tab-pane active">
            <div class="col-md-12 text-center " style="padding: 10px;">
                <b class="checklist_head_text">OPERATION RECORD</b>
                <button type="button" onclick="showAdditionalOrNotes();" class="btn bg-blue pull-right"><i class="fa fa-plus"></i></button>
            </div>
                <input type="hidden" id="or_surgery_req_detail_id" value="{{$surgery_detail_id}}"/>
                <input type="hidden" id="or_patient_id" value="{{$patient_id}}"/>
            <div class="col-md-12">
                <table class="table" style="">
                <tr>
                    <td>
                        Surgeon
                        <input id="or_surgeon" type="text" class="form-control" value="{{$surgeon_name}}"/>
                        {{-- {!! Form::select('or_surgeon',$surgen_list, $surgeon, ['class' => 'form-control select2','placeholder' =>'Select surgeon', 'id' =>'or_surgeon', 'style' => 'color:#555555; padding:4px 12px;']) !!} --}}
                    </td>
                    <td>
                        Date <input id="or_scheduled_date" type="text" class="form-control datepicker" value="@if($surgery_details[0]->surgery_date !=''){{date('M-d-Y',strtotime($surgery_details[0]->surgery_date))}}@endif"/>
                    </td>
                    <td>
                        Anesthesia Type
                        {!! Form::select('or_anaesthesia_type',$anaestha_list, $surgery_details[0]->anaesthesia_id, ['class' => 'form-control select2','placeholder' =>'Select surgeon', 'id' =>'or_anaesthesia_type', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                    </td>
                </tr>
                <tr>
                    <td>Assistant
                        <input id="or_assistant" type="text" class="form-control"/>

                    </td>
                    <td>Time
                        <input id="or_scheduled_time" type="text" class="form-control timepicker" value="@if($surgery_from_time !=''){{date('h:i A',strtotime($surgery_from_time))}}@endif"/>
                    </td>
                    <td>Anaesthetist

                        <input id="or_anaesthetist" type="text" class="form-control" value="{{$anaesthetist_name}}"/>
                        {{-- {!! Form::select('or_anaesthetist',$anaesthetist, $anaestha_doctor, ['class' => 'form-control select2','placeholder' =>'Select Anaesthetist', 'id' =>'or_anaesthetist', 'style' => 'color:#555555; padding:4px 12px;']) !!} --}}
                    </td>
                </tr>
                <tr>
                    <td>Nurse
                        {!! Form::select('or_nurse',$nurse_list, null, ['class' => 'form-control select2','placeholder' =>'Select Scrub Nurse', 'id' =>'or_nurse', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                    </td>
                    <td>To <input id="or_to" type="text" class="form-control timepicker"  value="@if($surgery_to_time !=''){{date('h:i A',strtotime($surgery_to_time))}}@endif"/></td>
                    <td>Operation Type  <input id="or_operation_type" type="text" class="form-control"/></td>
                </tr>

                </table>
            </div>
            <div class="col-md-12">
                <div class="col-md-6"><label for="">Pre Operation Diagnosis:</label>
                    <input type="text" class="form-control" id="or_pre_operative_diagnosis">
                </div>
                <div class="col-md-6"><label for="">Post Operation Diagnosis:</label>
                    <input type="text" class="form-control" id="or_post_operative_diagnosis">
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12">
                    <label for="">Incision:</label>
                    <textarea name="" id="or_incision" class="form-control" style="height: 36px !important;resize: none;"></textarea>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <label for="">Findings: </label>
                    <textarea name="" id="or_findings" class="form-control"  style="resize: none"></textarea>
                </div>
                <div class="col-md-6">
                    <label for="">Procedure: </label>
                    <textarea name="" id="or_procedure" class="form-control" style="resize: none"></textarea>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12">
                    <label for="">Instructions: </label>
                    <textarea name="" id="or_instructions" class="form-control"  style="resize: none"></textarea>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12">
                        <label for="">Procedure Done: </label>
                        <textarea name="" id="or_procedure_done" class="form-control" style="height: 36px !important;resize: none;"></textarea>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12">
                        <label for="">Drain: </label>
                        <textarea name="" id="or_drain" class="form-control" style="height: 36px !important;resize: none;"></textarea>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 10px;margin-bottom:20px;">
                <div class="col-md-10">
                    <label for="" style="display:flex">Material for H.P.E:
                        <div class="col-md-4">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="radio radio-success inline no-margin ">
                                        <input class="checkit" id="or_yes_hpe" type="radio" name="or_hpe" value=1>
                                        <label class="text-blue" for="or_yes_hpe">
                                            Yes
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="radio radio-success inline no-margin ">
                                        <input class="checkit" id="or_no_hpe" type="radio" name="or_hpe" value=2>
                                        <label class="text-blue" for="or_no_hpe">
                                            No.
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <div class="col-md-12" style="margin-top:15px;">
                <div class="col-md-9"></div>
                <div class="col-md-3" style="text-align: left;">
                    Doctor:<span id='label_or_dr_name'></span>
                    <br>
                    Reg No: <span id='label_or_dr_licenseno'></span>
                    <br>
                        <span id='label_or_dr_sign'></span>
                        <img id="dr_signature" style="width:175px; height:130px;"
                        src="">
                </div>
            </div>






        {{-- ================== additional opertion recodrd ========================== --}}

            <div class="additional_operaion_note col-md-12 no-padding no-margin" style="display: none;">
                <br>
                <hr>
                <br>
                <div class="col-md-12" style="text-align: center;">
                    <b class="checklist_head_text">ADDITIONAL OPERATION RECORD</b>
                </div>
                <br>

                <div class="col-md-12">
                    <table class="table" style="">
                    <tr>
                        <td>
                            Surgeon
                            <input id="or_surgeon2" type="text" class="form-control" value=""/>
                            {{-- {!! Form::select('or_surgeon',$surgen_list, $surgeon, ['class' => 'form-control select2','placeholder' =>'Select surgeon', 'id' =>'or_surgeon', 'style' => 'color:#555555; padding:4px 12px;']) !!} --}}
                        </td>
                        <td>
                            Date <input id="or_scheduled_date2" type="text" class="form-control datepicker" value="@if($surgery_details[0]->surgery_date !=''){{date('M-d-Y',strtotime($surgery_details[0]->surgery_date))}}@endif"/>
                        </td>
                        <td>
                            Anesthesia Type
                            {!! Form::select('or_anaesthesia_type2',$anaestha_list, $surgery_details[0]->anaesthesia_id, ['class' => 'form-control select2','placeholder' =>'Select surgeon', 'id' =>'or_anaesthesia_type2', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Assistant
                            <input id="or_assistant2" type="text" class="form-control"/>
                        </td>
                        <td>Time
                            <input id="or_scheduled_time2" type="text" class="form-control timepicker" value="@if($surgery_from_time !=''){{date('h:i A',strtotime($surgery_from_time))}}@endif"/>
                        </td>
                        <td>Anaesthetist

                            <input id="or_anaesthetist2" type="text" class="form-control" value=""/>

                        </td>
                    </tr>
                    <tr>
                        <td>Nurse
                            {!! Form::select('or_nurse2',$nurse_list, null, ['class' => 'form-control select2','placeholder' =>'Select Scrub Nurse', 'id' =>'or_nurse2', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                        </td>
                        <td>To <input id="or_to2" type="text" class="form-control timepicker"  value="@if($surgery_to_time !=''){{date('h:i A',strtotime($surgery_to_time))}}@endif"/></td>
                        <td>Operation Type  <input id="or_operation_type2" type="text" class="form-control"/></td>
                    </tr>

                    </table>
                </div>


                <div class="col-md-12">
                    <div class="col-md-6"><label for="">Pre Operation Diagnosis:</label>
                        <input type="text" class="form-control" id="or_pre_operative_diagnosis2">
                    </div>
                    <div class="col-md-6"><label for="">Post Operation Diagnosis:</label>
                        <input type="text" class="form-control" id="or_post_operative_diagnosis2">
                    </div>
                </div>



                <div class="col-md-12">
                    <div class="col-md-12">
                        <label for="">Incision:</label>
                        <textarea name="" id="or_incision2" class="form-control" style="height: 36px !important;resize: none;"></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <label for="">Findings: </label>
                        <textarea name="" id="or_findings2" class="form-control"  style="resize: none"></textarea>
                    </div>
                    <div class="col-md-6">
                        <label for="">Procedure: </label>
                        <textarea name="" id="or_procedure2" class="form-control" style="resize: none"></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12">
                        <label for="">Instructions: </label>
                        <textarea name="" id="or_instructions2" class="form-control"  style="resize: none"></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12">
                            <label for="">Procedure Done: </label>
                            <textarea name="" id="or_procedure_done2" class="form-control" style="height: 36px !important;resize: none;"></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12">
                            <label for="">Drain: </label>
                            <textarea name="" id="or_drain2" class="form-control" style="height: 36px !important;resize: none;"></textarea>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 10px;margin-bottom:20px;">
                    <div class="col-md-10">
                        <label for="" style="display:flex">Material for H.P.E:
                            <div class="col-md-4">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="radio radio-success inline no-margin ">
                                            <input class="checkit" id="or_yes_hpe2" type="radio" name="or_hpe2" value=1>
                                            <label class="text-blue" for="or_yes_hpe2">
                                                Yes
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="radio radio-success inline no-margin ">
                                            <input class="checkit" id="or_no_hpe2" type="radio" name="or_hpe2" value=2>
                                            <label class="text-blue" for="or_no_hpe2">
                                                No.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </label>
                    </div>
                </div>

            </div>
        {{-- additional opertion recodrd ends --}}











        <div class="col-md-12" style="text-align:right;">
            <button style="padding: 3px 3px" onclick="saveOperationNotes();" type="button" title="Save"
                class="btn btn-success" style="display:none;" id="save_operation_notes_btn0">Save<i id="save_operation_notes_spin"
                class="fa fa-save padding_sm"></i>
            </button>

            <button style="padding: 3px 3px" onclick="saveOperationNotes(1);" type="button" title="Save"
                    class="btn btn-success" style="display:none;" id="save_operation_notes_btn1">Save & Print <i id="save_operation_notes_spin"
                        class="fa fa-print padding_sm"></i>
            </button>
            @if(in_array("ot_operation_note_approval_access", $groupArray))
                <button style="padding: 3px 3px" onclick="saveOperationNotes(2);" type="button" title="Save" class="btn btn-info" style="display:none;"  id="save_operation_notes_btn2">Final Approve<i id="save_operation_notes_spin"
                            class="fa fa-save padding_sm"></i>
                </button>
            @endif
        </div>

        </div>
        {{-- Operation orders section --}}
        <div id="operation_orders" class="tab-pane fade">
            @include('Emr::emr.ot_checklists.prescription_ot')
        </div>
    </div>
</div>
