<div class="theadscroll" style="position: relative; height: 440px;">
<div class="col-md-12 text-center " style="padding: 10px;"><b class="checklist_head_text">BEFORE SKIN INCISION</b></div>


<table id="result_data_table" class="table table-condensed table_sm table-bordered theadfix_wrapper"
    style="font-size: 12px;">
    <thead>
        <tr class="common_table_header">
            <th class="common_td_rules" width="5%">SL.No</th>
            <th class="common_td_rules" width="70%">PARAMETERS</th>
            <th class="common_td_rules" width="20%">OPTIONS</th>

        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="common_td_rules">1</td>
            <td class="common_td_rules">Correct Patient</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="correct_patient_yes" type="radio" name="correct_patient" value=1>
                            <label class="text-blue" for="correct_patient_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="correct_patient_no" type="radio" name="correct_patient" value=2>
                            <label class="text-blue" for="correct_patient_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="correct_patient_na" type="radio" name="correct_patient" value=0>
                            <label class="text-blue" for="correct_patient_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td class="common_td_rules">2</td>
            <td class="common_td_rules">Correct Site And Side</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="correct_site_and_side_yes" type="radio" name="correct_site_and_side" value=1>
                            <label class="text-blue" for="correct_site_and_side_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="correct_site_and_side_no" type="radio" name="correct_site_and_side" value=2>
                            <label class="text-blue" for="correct_site_and_side_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="correct_site_and_side_na" type="radio" name="correct_site_and_side" value=0>
                            <label class="text-blue" for="correct_site_and_side_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="common_td_rules">3</td>
            <td class="common_td_rules">Agreement on Procedure</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="agreement_on_procedure_yes" type="radio" name="agreement_on_procedure" value=1>
                            <label class="text-blue" for="agreement_on_procedure_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="agreement_on_procedure_no" type="radio" name="agreement_on_procedure" value=2>
                            <label class="text-blue" for="agreement_on_procedure_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="agreement_on_procedure_na" type="radio" name="agreement_on_procedure" value=0>
                            <label class="text-blue" for="agreement_on_procedure_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="common_td_rules">4</td>
            <td class="common_td_rules">Availability of Implants/Equipment Circulating</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="availability_of_implants_yes" type="radio" name="availability_of_implants" value=1>
                            <label class="text-blue" for="availability_of_implants_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="availability_of_implants_no" type="radio" name="availability_of_implants" value=2>
                            <label class="text-blue" for="availability_of_implants_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="availability_of_implants_na" type="radio" name="availability_of_implants" value=0>
                            <label class="text-blue" for="availability_of_implants_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="common_td_rules">5</td>
            <td class="common_td_rules">Relevant Investigation And Images</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="relevant_investigation_and_images_yes" type="radio" name="relevant_investigation_and_images" value=1>
                            <label class="text-blue" for="relevant_investigation_and_images_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="relevant_investigation_and_images_no" type="radio" name="relevant_investigation_and_images" value=2>
                            <label class="text-blue" for="relevant_investigation_and_images_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="relevant_investigation_and_images_na" type="radio" name="relevant_investigation_and_images" value=0>
                            <label class="text-blue" for="relevant_investigation_and_images_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="common_td_rules">6</td>
            <td class="common_td_rules">Pre Counts Completed</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="pre_counts_completed_yes" type="radio" name="pre_counts_completed" value=1>
                            <label class="text-blue" for="pre_counts_completed_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="pre_counts_completed_no" type="radio" name="pre_counts_completed" value=2>
                            <label class="text-blue" for="pre_counts_completed_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="pre_counts_completed_na" type="radio" name="pre_counts_completed" value=0>
                            <label class="text-blue" for="pre_counts_completed_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>



        <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                <label class="text-blue" for="anesthesia_safety_check_completed">
                    <b>Team</b>
                </label>
        </div>
        <div class="col-md-6 padding_sm">

            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                <div class="col-md-6">
                    <label class="text-blue" for="difficlty_airways">
                        Surgeon
                    </label>
                </div>
                <div class="col-md-6" style="display: flex;">
                    : &nbsp;&nbsp;
                    <input id="timeout_surgeon" type="text" class="form-control" value="{{$scheduled_surgeon_name}}"/>
                          {{-- {!! Form::select('timeout_surgeon',$surgen_list, $surgeon, ['class' => 'form-control select2','placeholder' =>'Select surgeon', 'id' =>'timeout_surgeon', 'style' => 'color:#555555; padding:4px 12px;']) !!} --}}
                </div>
            </div>


            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                <div class="col-md-6">
                    <label class="text-blue" for="timeout_anaesthetist">
                        Anaesthetist
                    </label>
                </div>
                <div class="col-md-6" style="display: flex;">
                    : &nbsp;&nbsp;
                    <input id="timeout_anaesthetist" type="text" class="form-control" value="{{$scheduled_anaesthetist_name}}"/>
                    {{-- {!! Form::select('timeout_anaesthetist',$anaesthetist, $anaestha_doctor, ['class' => 'form-control select2','placeholder' =>'Select Anaesthetist', 'id' =>'timeout_anaesthetist', 'style' => 'color:#555555; padding:4px 12px;']) !!} --}}
                </div>
            </div>

            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                <div class="col-md-6">
                    <label class="text-blue" for="timeout_scrub_nurse">
                       Scrub Nurse
                    </label>
                </div>
                <div class="col-md-6" style="display: flex;">
                    : &nbsp;&nbsp;
                    {!! Form::select('timeout_scrub_nurse',$all_staff_ot_nurse, null, ['class' => 'form-control select2','placeholder' =>'Select Scrub Nurse', 'id' =>'timeout_scrub_nurse', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                </div>
            </div>
        </div>
        <div class="col-md-6 padding_sm">
            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                <div class="col-md-6">
                    <label class="text-blue" for="timeout_nurse">
                        Nurse
                    </label>
                </div>
                <div class="col-md-6" style="display: flex;">
                    : &nbsp;&nbsp;
                    {!! Form::select('timeout_nurse',$all_staff_ot_nurse, null, ['class' => 'form-control select2','placeholder' =>'Select Nurse', 'id' =>'timeout_nurse', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                </div>
            </div>
            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                <div class="col-md-6" >
                    <label class="text-blue" for="timeout_technician">
                        Technician
                    </label>
                </div>
                <div class="col-md-6" style="display: flex;">
                    : &nbsp;&nbsp;
                    <input  id="timeout_technician" type="text" name="timeout_technician" value='' class="form-control">
                </div>
            </div>
            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                <div class="col-md-6">

                    <label class="text-blue" for="timeout_others">
                        Others
                    </label>
                </div>
                    <div class="col-md-6" style="display: flex;">
                        : &nbsp;&nbsp;<input  id="timeout_others" type="text" name="timeout_others" value='' class="form-control">
                    </div>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-top:10px;margin-bottom:20px;">
        <div class="col-md-6">
            Completed By :{!! Form::select('timeout_completed_by',$ot_users_names_list,0, ['class' => 'form-control select2', 'id' =>'timeout_completed_by','placeholder' =>'select', 'style' => 'color:#555555; padding:4px 12px;']) !!}
        </div>
        <div class="col-md-6">
            Completed at :<input class="form-control timepicker" name="timeout_completed_at" id="timeout_completed_at" style="">
        </div>

    </div>

    <div class="col-md-12 " style="margin-top:5px;margin-bottom:5px;">
        <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
            class="btn btn-danger pull-right">Close <i class="fa fa-times"></i></button>
        @if(in_array("OT Surgery Checklist Timeout", $groupArray))
        <button style="padding: 3px 3px" onclick="saveSurgeryTimeOut();" type="button" title="Save"
            class="btn btn-success pull-right btn_save_timeout" id="save_checklistbtn">Save<i id="save_checklistspin"
                class="fa fa-save padding_sm"></i>
        </button>
        @endif
        {{-- @if(in_array("OT Approval Access", $groupArray)) --}}
        {{-- <button style="padding: 3px 3px" onclick="ApproveSurgeryChecklist(4);" type="button" title="Save"
            class="btn btn-primary pull-right btn_approve_timeout" id="approve_checklistbtn">Approve<i id="save_checklistspin"
                class="fa fa-check padding_sm"></i>
        </button> --}}
        {{-- @endif --}}
    </div>

