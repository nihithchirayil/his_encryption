<div id="msform">

    <ul id="progressbar" style="cursor: pointer;margin-bottom: 2px;" class="nav nav-tabs" role="tablist">

        <li class="nav-item" id="parameters_li">
            <a class="nav-link" id="parameters-tab" href="#parameters_tab" role="tab" aria-controls="parameters"
                aria-selected="true"><strong><i class="fa fa fa-params"></i>Parameters</strong></a>
        </li>
        <li class="nav-item" id="op_vitals_li">
            <a class="nav-link" id="instrument_list-tab" href="#op_vitals_tab" role="tab" aria-controls="op_vitals"
            aria-selected="true"><strong><i class="fa fa-op"></i> Pre OP Vitals</strong></a>
        </li>
        <li class="nav-item" id="sign_in_li">
            <a class="nav-link" id="sign_in-tab" href="#sign_in_tab" role="tab" aria-controls="sign_in_list"
            aria-selected="true"><strong><i class="fa fa-signin"></i> Sign In </strong></a>
        </li>
        <li class="nav-item" id="time_out_li">
            <a class="nav-link" id="time_out-tab" href="#time_out_tab" role="tab" aria-controls="time_out"
            aria-selected="true"><strong><i class="fa fa-time"></i> Time Out </strong></a>
        </li>
        <li class="nav-item" id="nursing_record_li">
            <a class="nav-link" id="nursing_record-tab" href="#nursing_record_tab" role="tab"
            aria-controls="nursing_record" aria-selected="true"><strong><i class="fa fa-nurse"></i> Nursing
                Record </strong></a>
        </li>
        <li class="nav-item" id="sign_out_li">
            <a class="nav-link" id="sign_out-tab" href="#sign_out_tab" role="tab" aria-controls="sign_out"
            aria-selected="true"><strong><i class="fa fa-signout"></i> Sign Out </strong></a>
        </li>
    </ul>
    <div class="tab-content">

        <div class="tab-pane  active " id="parameters_tab" role="tabpanel" aria-labelledby="nav-parameters-tab">
            <div class="form-card">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 1px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;height: 430px;">
                        @include('Emr::emr.ot_checklists.parameters')
                    </div>
                </div>
            </div>
        </div>


        <div class="tab-pane  active" id="op_vitals_tab" role="tabpanel" aria-labelledby="nav-op_vitals-tab">
            <div class="form-card">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 1px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;height: 448px;">
                        @include('Emr::emr.ot_checklists.op_vital')
                    </div>
                </div>
            </div>
        </div>


        <div class="tab-pane fade" id="sign_in_tab" role="tabpanel" aria-labelledby="nav-sign_in-tab">
            <div class="form-card">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 1px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;height: 448px;">
                        @include('Emr::emr.ot_checklists.ot_signin')
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="time_out_tab" role="tabpanel" aria-labelledby="nav-time_out-tab">
            <div class="form-card">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 1px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;height: 448px;">
                        @include('Emr::emr.ot_checklists.ot_timeout')
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="nursing_record_tab" role="tabpanel" aria-labelledby="nav-nursing_record-tab">
            <div class="form-card">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 1px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;height: 448px;">
                        @include('Emr::emr.ot_checklists.nursing_record')
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="sign_out_tab" role="tabpanel" aria-labelledby="nav-sign_out-tab">
            <div class="form-card">
                <div class="box no-border no-margin">
                    <div class="box-footer"
                        style="padding: 1px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;height: 460px;">
                        @include('Emr::emr.ot_checklists.sign_out')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


