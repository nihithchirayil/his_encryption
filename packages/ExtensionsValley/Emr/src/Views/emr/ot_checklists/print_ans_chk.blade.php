<div class="col-md-12 theadscroll" style="position: relative; height:520px;">
    <div class="col-md-12">
        <button onclick="takePrintOtChecklist();" type="button" class="btn btn-primary pull-right">
            <i class="fa fa-print"></i> Print
        </button>
    </div>
    <div class="col-md-12" id="ResultDataContainer">
        <style>
            @media print {
                .pagebreak { page-break-before: always; } /* page-break-after works, as well */
            }
        </style>

        <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
            <thead>
                <tr>
                    <th style='padding: 5px 0;' colspan='4' class='text-center'>
                        <h4 class="checklist_head_text"><b>ANAESTHESIA CHECKLIST</b></h4>
                    </th>
                </tr>
            </thead>
            <tbody style=''>
                <tr>
                    <td style=' padding-left:5px;;text-align:left; '>Patient Name</td>
                    <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data[0]->patient_name !!}</td>
                    <td style=' padding-left:5px;;text-align:left;'>UHID No.</td>
                    <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->uhid !!}</td>
                </tr>
                <tr>
                    <td style=' padding-left:5px;;text-align:left; '>Age &amp; Gender</td>
                    <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data[0]->age   !!} / {!! $patient_data[0]->gender   !!}</td>
                    <td style=' padding-left:5px;; text-align:left;'> IP No. </td>
                    <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->admission_no   !!}</td>
                </tr>
                <tr>
                    <td rowspan='2' style=' padding-left:5px;;text-align:left;'> Address</td>
                    <td rowspan='2' style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->address   !!}</td>
                    {{-- <td style=' padding-left:5px;; text-align:left;'>DOA </td>
                    <td style=' padding-left:5px;; text-align:left;' id="summary_admit_date_time">@if($patient_data[0]->admission_date!='')
                        {!! date('M-d-Y',strtotime($patient_data[0]->admission_date)) !!}
                        @endif
                    </td> --}}
                </tr>
                <tr>
                    {{-- <td style=' padding-left:5px;;text-align:left; '>DOD</td>
                    <td style=' padding-left:5px;; text-align:left;' id="summary_discharge_date_time">
                        @if($patient_data[0]->discharge_datetime != '')
                            {!! $patient_data[0]->discharge_datetime !!} {!!$patient_data[0]->discharge_datetime!!}
                        @endif
                    </td> --}}
                </tr>

                <tr>
                    <td style=' padding-left:5px;;text-align:left; '>Current Room</td>
                    <td style=' padding-left:5px;;text-align:left; '>{{$patient_data[0]->bed_name}}</td>
                    <td rowspan="2" style=' padding-left:5px;;text-align:left; '>DOS</td>
                    <td rowspan="2" style=' padding-left:5px;; text-align:left;'>
                        @if(!empty($surgery_date))
                            {{date("Y-m-d",strtotime($surgery_date))}}
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
        @if($resultData->pre_anaesthetic_data !='')
        <div class="col-md-12 no-padding" style="margin-top:10px">
            <div class="col-md-12 no-padding">
                <h5 style="background-color:#36a693 !important;color:white !important;height:35px;padding-top:10px;" class="text-center"><b class="checklist_head_text_new">PRE-ANAESTHETIC ASSESSMENT</b></h5>
                @include('Emr::emr.ot_checklists.print_pre_anesthetic_assesment')
            </div>
        </div>
        <div class="pagebreak"> </div>
        @endif
        @if($resultData->anasthesia_record_data !='')
         <div class="col-md-12 no-padding" style="margin-top:10px">
            <div class="col-md-12 no-padding">
                <h5 style="background-color:#36a693 !important;color:white !important;height:35px;padding-top:10px;" class="text-center"><b class="checklist_head_text_new">ANAESTHESIA RECORD</b></h5>
                @include('Emr::emr.ot_checklists.print_anesthetia_record')

            </div>
        </div>
        <div class="pagebreak"> </div>
        @endif
        @php
            // dd($ar_data);
        @endphp
        @if($ar_data !='')
        <div class="col-md-12 no-padding" style="margin-top:10px">
            <div class="col-md-12 no-padding">
                <h5 style="background-color:#36a693 !important;color:white !important;height:35px;padding-top:10px;" class="text-center"><b class="checklist_head_text_new">CHECKLIST</b></h5>
                {{-- @include('Emr::emr.ot_checklists.print_anesthetia_chklist') --}}
                @include('Emr::emr.ot_checklists.print_anesthetia_chklist_new')
            </div>
        </div>
        <div class="pagebreak"> </div>
        @endif
        @if($resultData->post_op_assessment_data !='')
         <div class="col-md-12 no-padding" style="margin-top:10px">
            <div class="col-md-12 no-padding">
                <h5 style="background-color:#36a693 !important;color:white !important;height:35px;padding-top:10px;" class="text-center"><b class="checklist_head_text_new">IMMEDIATE POST OP ASSESSMENT</b></h5>
                @include('Emr::emr.ot_checklists.print_immediate_post_op')

            </div>
        </div>
        @endif
    </div>

</div>
