<style>
    label{
        margin-right:10px;
        float:left;clear:right;
        font-size:12px !important;
    }
    .form-control{
        float: left;
        clear:right;
        margin-right:10px;

    }
    .col-md-12{
        margin-top:8px;
    }
    .radio-success{
        width: 55px !important;
        margin-left: 10px;
        float: left;
        clear:right;
    }
</style>

<div class="col-md-12 no-padding">
    <div class="col-md-12" style="white-space:nowrap;">
        <label style="margin-right:10px;float:left;clear:right;">Surgery</label><input type="text" class="form-control" id="name_of_surgery" value="{{$surgery_details[0]->surgery_name}}" placeholder="Name of surgery" style="width:1122px;clear:right;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Date & Time of the surgery</label><input type="text" id="date_and_time_of_surgery" class="form-control datetimepicker" placeholder="Date & Time of the surgery" value="@if($surgery_details[0]->scheduled_at !=''){{date('M-d-Y h:i A',strtotime($surgery_details[0]->scheduled_at))}}@endif" style="width:480px;"/>
        <label>surgeon</label><input style="width:480px;" type="text" id="surgeon_name" value="{{$surgery_details[0]->surgeon}}" class="form-control" placeholder="Surgeon"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>History –Hypertension-</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="history_hypertension_yes" type="radio" name="history_hypertension" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="history_hypertension_no" type="radio" name="history_hypertension" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Diabetes</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="diabetes_yes" type="radio" name="diabetes" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="diabetes_yes_no" type="radio" name="diabetes" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Asthma</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="asthma_yes" type="radio" name="asthma" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="asthma_no" type="radio" name="asthma" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Epilepsy</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="epilepsy_yes" type="radio" name="epilepsy" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="epilepsy_no" type="radio" name="epilepsy" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>Allergy</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="allergy_yes" type="radio" name="allergy" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="allergy_no" type="radio" name="allergy" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>I.H.D</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="i_h_d_yes" type="radio" name="i_h_d" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="i_h_d_no" type="radio" name="i_h_d" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <label>Previous Surgery &Type of Anaesthesia</label><input type="text" id="previous_surgeory_and_anesthesia" class="form-control" placeholder="Previous Surgery &Type of Anaesthesia" style="width:355px;"/>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>Thyroid</label>
            <div class="radio radio-success inline no-margin " style="width:60px !important;">
                <input class="checkit" id="hyper" type="radio" name="thyroid_condition" value="hyper">
                <label class="text-blue">
                    Hyper
                </label>
            </div>
            <div class="radio radio-success inline no-margin " style="width:60px !important;">
                <input class="checkit" id="hypo" type="radio" name="thyroid_condition" value="hypo">
                <label class="text-blue">
                    Hypo
                </label>
            </div>
        </div>
        <label>Hyper History of TB</label><input type="text" id="hyper_history_of_tb" class="form-control" placeholder="Hyper History of TB" style="width:655px;"/>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <label>Medications</label><input type="text" id="anesthesia_medications" class="form-control" placeholder="Medications" style="width:1102px;"/>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <label>Other Relevant History</label><input type="text" id="other_relevant_history" class="form-control" placeholder="Other Relevant History" style="width:1175px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>PHYSICAL EXAMINATION -</label>
        <div class="col-md-3 no-padding">
            <label>Icterus</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="icterus_yes" type="radio" name="icterus" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="icterus_no" type="radio" name="icterus" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Pallor</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="pallor_yes" type="radio" name="pallor" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="pallor_no" type="radio" name="pallor" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Lymphadenopathy</label>
            <input type="text" id="lymphadenopathy" class="form-control" placeholder="Lymphadenopathy" style="width:175px;"/>
        </div>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>Edema</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="edema_yes" type="radio" name="edema" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="edema_no" type="radio" name="edema" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <label>Airway-Dentition</label>
        <input type="text" id="airway_dentition" class="form-control" placeholder="Airway-Dentition" style="width:175px;"/>
        <label>Thyroid</label>
        <input type="text" id="thyroid_desc" class="form-control" placeholder="Thyroid" style="width:175px;"/>
        <label>Spine</label>
        <input type="text" id="spine" class="form-control" placeholder="Thyroid" style="width:175px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Mallampattl Grade</label>
        <input type="text" id="mallampattl_grade" class="form-control" placeholder="Mallampattl Grade" style="width:175px;"/>
        <label>TM Distance</label>
        <input type="text" id="tm_distance" class="form-control" placeholder="TM Distance" style="width:175px;"/>

    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Pulse</label>
        <input type="text" id="anesthesia_pulse" class="form-control number_only" placeholder="Pulse" style="width:200px;"/>
        <label>/min BP</label>
        <input type="text" id="anesthesia_bp" class="form-control number_only" placeholder="BP" style="width:200px;"/>
        <label>mmHg Temperature</label>
        <input type="text" id="anesthesia_temprature" class="form-control number_only" placeholder="Temperature" style="width:200px;"/>
        <label>F: Weight</label>
        <input type="text" id="anesthesia_weight" class="form-control number_only" placeholder="Weight" style="width:200px;"/>kg

    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <label>C.V.S</label>
        <input type="text" id="cvs" class="form-control" placeholder="cvs" style="width:1133px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Respiratory System
        </label>
        <input type="text" id="respiratory_system" class="form-control" placeholder="respiratory_system" style="width:1058px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>GIT
        </label>
        <input type="text" id="git" class="form-control" placeholder="git" style="width:1143px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>C.N.S
        </label>
        <input type="text" id="cns" class="form-control" placeholder="C.N.S" style="width:1130px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Other Relevant findings
        </label>
        <input type="text" id="other_relevant_findings" class="form-control" placeholder="Other Relevant findings" style="width:1036px;"/>
    </div>

    <div class="col-md-12" style="margin:15px">
        <h4>INVESTIGATIONS</h4>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <label>1. Hb %
        </label>
        <input type="text" id="hb" class="form-control" placeholder="hb" style="width:200px;"/>
        <label>TLC:
        </label>
        <input type="text" id="tlc" class="form-control" placeholder="tlc" style="width:200px;"/>
        <label>Platelets:
        </label>
        <input type="text" id="platelets" class="form-control" placeholder="Platelets" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>2. Blood Sugar (mg %):Fasting
        </label>
        <input type="text" id="blood_sugar" class="form-control" placeholder="blood_sugar" style="width:200px;"/>
        <label>PP:
        </label>
        <input type="text" id="pp" class="form-control" placeholder="pp" style="width:200px;"/>
        <label>Random:
        </label>
        <input type="text" id="random" class="form-control" placeholder="Random" style="width:200px;"/>
        <label>HbA1C:
        </label>
        <input type="text" id="hba1c" class="form-control" placeholder="HbA1C" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>3. Blood Grouping & Typing
        </label>
        <input type="text" id="blood_grouping" class="form-control" placeholder="Blood Grouping & Typing" style="width:200px;"/>
        <label>Bleeding Time:
        </label>
        <input type="text" id="bleeding_time" class="form-control timepicker" placeholder="Bleeding Time" style="width:200px;"/>
        <label>Clotting Time:
        </label>
        <input type="text" id="clotting_time" class="form-control timepicker" placeholder="Clotting Time" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>4. PT (Control
        </label>
        <input type="text" id="pt_control" class="form-control" placeholder="" style="width:130px;"/>
        <label>/test
        </label>
        <input type="text" id="pt_test" class="form-control" placeholder="" style="width:130px;"/>
        <label>)INR
        </label>
        <input type="text" id="pt_inr" class="form-control" placeholder="" style="width:130px;"/>
        <label>)aPTT(Control
        </label>
        <input type="text" id="apt_control" class="form-control" placeholder="" style="width:130px;"/>
        <label>)/test
        </label>
        <input type="text" id="apt_test" class="form-control" placeholder="" style="width:130px;"/>
        <label>)
        </label>
        <input type="text" id="apt_others" class="form-control" placeholder="" style="width:130px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>5. Serum creatinine
        </label>
        <input type="text" id="creatine" class="form-control" placeholder="creatine" style="width:200px;"/>
        <label>Blood Urea:
        </label>
        <input type="text" id="blood_urea" class="form-control" placeholder="blood_urea" style="width:200px;"/>
        <label>C.U.E:
        </label>
        <input type="text" id="cue" class="form-control" placeholder="C.U.E" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>6. Serum Electrolytes Na
        </label>
        <input type="text" id="na" class="form-control" placeholder="Na" style="width:200px;"/>
        <label>K:
        </label>
        <input type="text" id="k" class="form-control" placeholder="k" style="width:200px;"/>
        <label>Others:
        </label>
        <input type="text" id="serum_electrolytes_others" class="form-control" placeholder="Others" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>7. HBsAg
        </label>
        <input type="text" id="hbsag" class="form-control" placeholder="HBsAg" style="width:200px;"/>
        <label>HCV:
        </label>
        <input type="text" id="hcv" class="form-control" placeholder="HCV" style="width:200px;"/>
        <label>HIV:
        </label>
        <input type="text" id="hiv" class="form-control" placeholder="hiv" style="width:200px;"/>
        <label>X-RAY Chest:
        </label>
        <input type="text" id="xray_chest" class="form-control" placeholder="X-RAY Chest" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>8. E.C.G
        </label>
        <input type="text" id="ecg" class="form-control" placeholder="E.C.G" style="width:200px;"/>
        <label>ECHO:
        </label>
        <input type="text" id="echo" class="form-control" placeholder="ECHO" style="width:200px;"/>
        <label>LFT:
        </label>
        <input type="text" id="lft" class="form-control" placeholder="LFT" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>9. TFT : TSH
        </label>
        <input type="text" id="tsh" class="form-control" placeholder="tsh" style="width:200px;"/>
        <label>T3:
        </label>
        <input type="text" id="t3" class="form-control" placeholder="T3" style="width:200px;"/>
        <label>T4:
        </label>
        <input type="text" id="t4" class="form-control" placeholder="T4" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>Other Specific Investigations:</b>
        </label>
        <input type="text" id="other_specific_investigations" class="form-control" placeholder="Other Specific Investigations" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>PRE-OP OPINION</b>
        </label>
        <textarea id="pre_op_openion" class="form-control" placeholder="PRE-OP OPINION" style="width:1169px;"></textarea>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>REVIEW PAC</b>
        </label>
        <textarea id="review_pac" class="form-control" placeholder="REVIEW PAC" style="width:1169px;"></textarea>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>ASA CATEGORY</b>
        </label>
        <input type="text" id="asa_category" class="form-control" placeholder="ASA CATEGORY" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Type of Anaesthesia planned & Explained to patient
        </label>
        <input type="text" id="type_of_anaesthesia_planned" class="form-control" placeholder="Type of Anaesthesia planned & Explained to patient" value="{{$surgery_details[0]->anaesthesia}}" style="width:1170px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;margin-bottom:20px;">
        <label>Completed by
        </label>
        <input type="text" id="completed_by" value="{{$user_name}}" class="form-control" placeholder="Completed by" style="width:175px;"/>
        <label>Date
        </label>
        <input type="text" id="completed_date" class="form-control datepicker" placeholder="" style="width:175px;"/>
        <label>Time
        </label>
        <input type="text" id="completed_time" class="form-control timepicker" placeholder="" style="width:175px;"/>
    </div>
</div>

<div class="col-md-12 " style="margin-top:5px;margin-bottom:5px;">
    <button style="padding: 3px 3px" onclick="saveAnaesthetiaPreAnaesthetic();" type="button" title="Save"
        class="btn btn-success pull-right" id="save_checklistbtn">Save<i id="save_checklistspin"
            class="fa fa-save padding_sm"></i>
    </button>
    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
        class="btn btn-danger pull-right">Close <i class="fa fa-times"></i></button>
</div>
