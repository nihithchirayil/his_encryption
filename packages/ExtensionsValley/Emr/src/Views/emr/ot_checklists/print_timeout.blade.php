<div class="col-md-12">
    @php
        $time_out_data = $resultData->time_out_data;
        $time_out_data = json_decode($time_out_data);
    @endphp
    <div class="col-md-12"><b class="checklist_head_text">BEFORE SKIN INCISION</b></div>
            <div class="col-md-6 padding_sm">
                <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                    @if(isset($time_out_data->correct_patient) && $time_out_data->correct_patient == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                    <label class="text-blue" for="correct_patient">
                        CORRECT PATIENT
                    </label>

                </div>
                <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">

                    @if(isset($time_out_data->correct_site_and_side) && $time_out_data->correct_site_and_side == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                    <label class="text-blue" for="correct_patient">
                        CORRECT SITE & SIDE
                    </label>
                </div>
                <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">

                    @if(isset($time_out_data->agreement_on_procedure) && $time_out_data->agreement_on_procedure == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                    <label class="text-blue" for="correct_patient">
                        AGREEMENT ON PROCEDURE
                    </label>

                </div>
                <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">

                    @if(isset($time_out_data->correct_patient_position) && $time_out_data->correct_patient_position == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                    <label class="text-blue" for="correct_patient">
                        CORRECT PATIENT POSITION
                    </label>

                 </div>
                 <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">

                    @if(isset($time_out_data->availability_of_implants) && $time_out_data->availability_of_implants == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                    <label class="text-blue" for="correct_patient">
                        AVAILABILITY OF IMPLANTS/EQUIPMENTCIRCULATING
                    </label>


               </div>
                 <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">

                    @if(isset($time_out_data->relevant_investigation_and_images) && $time_out_data->availability_of_implants == 1)

                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                    <label class="text-blue" for="correct_patient">
                        RELEVANT INVESTIGATION & IMAGES
                    </label>


               </div>
                 <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">

                    @if(isset($time_out_data->pre_counts_completed) && $time_out_data->pre_counts_completed == 1)

                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                    <label class="text-blue" for="correct_patient">
                        PRE COUNTS COMPLETED
                    </label>

               </div>
            </div>
            <div class="col-md-6 padding_sm">
                <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                        <label class="text-blue" for="anesthesia_safety_check_completed">
                            <b>TEAM</b>
                        </label>
                </div>


                <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                    <div class="col-md-6">
                        <label class="text-blue" for="difficlty_airways">
                            SURGEON
                        </label>
                    </div>
                    <div class="col-md-6" style="display: flex;">
                        : &nbsp;&nbsp;
                        @if(isset($time_out_data->timeout_surgeon) && $time_out_data->timeout_surgeon !='')
                            {!! $time_out_data->timeout_surgeon !!}
                        @endif
                    </div>
              </div>


                <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                    <div class="col-md-6">
                        <label class="text-blue" for="blood_product_reserved">
                            ANAESTHETIST
                        </label>
                    </div>
                    <div class="col-md-6" style="display: flex;">
                        : &nbsp;&nbsp;
                        @if(isset($time_out_data->timeout_anaesthetist) && $time_out_data->timeout_anaesthetist !='')
                        {!! $time_out_data->timeout_anaesthetist !!}
                        @endif

                    </div>
                </div>



                <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                    <div class="col-md-6">
                        <label class="text-blue" for="risk_of_blood_loss">
                           SCRUB NURSE
                        </label>
                    </div>
                    <div class="col-md-6" style="display: flex;">
                        : &nbsp;&nbsp;
                        @if(isset($time_out_data->timeout_scrub_nurse) && $time_out_data->timeout_scrub_nurse !='')
                        {!! $time_out_data->timeout_scrub_nurse !!}
                        @endif
                    </div>
                </div>
                <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                    <div class="col-md-6">
                        <label class="text-blue" for="antibiotic_prophylaxis">
                            NURSE
                        </label>
                    </div>
                    <div class="col-md-6" style="display: flex;">
                        : &nbsp;&nbsp;
                        @if(isset($time_out_data->timeout_nurse) && $time_out_data->timeout_nurse !='')
                        {!! $time_out_data->timeout_nurse !!}
                        @endif

                    </div>
                </div>
                <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                    <div class="col-md-6" >
                        <label class="text-blue" for="antibiotic_prophylaxis">
                            TECHNICIAN
                        </label>
                    </div>
                    <div class="col-md-6" style="display: flex;">
                        : &nbsp;&nbsp;
                        @if(isset($time_out_data->timeout_technician) && $time_out_data->timeout_technician !='')
                        {!! $time_out_data->timeout_technician !!}
                        @endif
                    </div>
                </div>
                <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                    <div class="col-md-6">

                        <label class="text-blue" for="antibiotic_prophylaxis">
                            OTHERS
                        </label>
                    </div>
                        <div class="col-md-6" style="display: flex;">
                            : &nbsp;&nbsp;
                            @if(isset($time_out_data->timeout_others) && $time_out_data->timeout_others !='')
                        {!! $time_out_data->timeout_others !!}
                        @endif

                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-12"><br><br>
            <div class="col-md-6">
                Completed by:
                @if(isset($time_out_data->timeout_completed_by))
                {{$time_out_data->timeout_completed_by}}
                @endif
            </div>
        </div>
