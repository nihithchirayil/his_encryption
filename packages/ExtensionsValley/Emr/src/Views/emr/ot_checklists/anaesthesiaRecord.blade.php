<div id="ansform">
    <style>
        .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
            background:#337ab7;
            color:#FFFF;
        }
    </style>

    <ul id="progressbar" style="cursor: pointer;margin-bottom: 2px;" class="nav nav-tabs" role="tablist">
        @if(in_array("OT Anaesthesia Checklist PreAnaesthetic", $groupArray))
        <li class="nav-item" style="width: 23.6672%;">
            <a class="nav-link" id="pre_anaesthetic-assessment" href="#pre_anaesthetic_assessment" role="tab" aria-controls="parameters"
                aria-selected="true"><strong><i class="fa fa fa-params"></i>PRE–ANAESTHETIC ASSESSMENT
                </strong></a>
        </li>
        @endif
        @if(in_array("OT Anaesthesia AnaesthesiaRecord", $groupArray))
        <li class="nav-item" style="width: 23.6672%;">
            <a class="nav-link" id="anaesthesia-record_checklist" href="#anaesthesia_record_checklist" role="tab" aria-controls="anaesthesia_record_checklist"
                aria-selected="true"><strong><i class="fa fa-op"></i> ANAESTHESIA RECORD</strong></a>
        </li>
        @endif
        @if(in_array("OT Anaesthesia Checklist", $groupArray))
        <li class="nav-item" style="width: 23.6672%;">
            <a class="nav-link" id="anaesthesia-checklist" href="#anaesthesia_checklist" role="tab" aria-controls="sign_in_list" onclick="AnaesthetiaRecord('{{ $head_id }}','{{ $surgery_detail_id }}',1);"
                aria-selected="true"><strong><i class="fa fa-signin"></i> CHECKLIST</strong></a>
        </li>
        @endif
        @if(in_array("OT Anaesthesia PostOpAssessment", $groupArray))
        <li class="nav-item" style="width: 23.6672%;">
            <a class="nav-link" id="immediate_post_op-assessment" href="#immediate_post_op_assessment" role="tab" aria-controls="time_out"
                aria-selected="true"><strong><i class="fa fa-time"></i> IMMEDIATE POST-OP ASSESSMENT </strong></a>
        </li>
        @endif
    </ul>
    <div class="tab-content">
        @if(in_array("OT Anaesthesia Checklist PreAnaesthetic", $groupArray))
        <div class="tab-pane  active " id="pre_anaesthetic_assessment" role="tabpanel" aria-labelledby="nav-pre_anaesthetic_assessment">
            <div class="form-card">
                <div class="box no-border no-margin">
                    <div class="">
                        @include('Emr::emr.ot_checklists.pre_anaesthetic_assessment')
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if(in_array("OT Anaesthesia AnaesthesiaRecord", $groupArray))
        <div class="tab-pane  active" id="anaesthesia_record_checklist" role="tabpanel" aria-labelledby="nav-anaesthesia_record">
            <div class="form-card">
                <div class="box no-border no-margin">
                    <div class=""
                        >
                        @include('Emr::emr.ot_checklists.anaesthesia_record_checklist')
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if(in_array("OT Anaesthesia Checklist", $groupArray))
        <div class="tab-pane fade" id="anaesthesia_checklist" role="tabpanel" aria-labelledby="nav-anaesthesia_checklist">
            {{-- <div class="form-card">
                <div class="box no-border no-margin">
                    <div class="">
                         @include('Emr::emr.ot_checklists.anaesthesia_checklist')
                    </div>
                </div>
            </div> --}}
        </div>
        @endif
        @if(in_array("OT Anaesthesia PostOpAssessment", $groupArray))
        <div class="tab-pane fade" id="immediate_post_op_assessment" role="tabpanel" aria-labelledby="nav-immediate_post_op_assessment">
            <div class="form-card">
                <div class="box no-border no-margin">
                    <div class=""
                        >
                        @include('Emr::emr.ot_checklists.immediate_post_op_assessment')
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
<script>
    $('.aldrete_score').on('click',function(){
        var imm_post_op_activity = $('input[name="imm_post_op_activity"]:checked').val();
        var imm_post_op_breathing = $('input[name="imm_post_op_breathing"]:checked').val();
        var imm_post_op_circulation = $('input[name="imm_post_op_circulation"]:checked').val();
        var imm_post_op_consciousness = $('input[name="imm_post_op_consciousness"]:checked').val();
        var imm_post_op_o2_saturation = $('input[name="imm_post_op_o2_saturation"]:checked').val();

        if(typeof imm_post_op_activity === "undefined"){
            imm_post_op_activity = 0;
        }
        if(typeof imm_post_op_breathing === "undefined"){
            imm_post_op_breathing = 0;
        }
        if(typeof imm_post_op_circulation === "undefined"){
            imm_post_op_circulation = 0;
        }
        if(typeof imm_post_op_consciousness === "undefined"){
            imm_post_op_consciousness = 0;
        }
        if(typeof imm_post_op_o2_saturation === "undefined"){
            imm_post_op_o2_saturation = 0;
        }
        var total_score = parseInt(imm_post_op_activity)+ parseInt(imm_post_op_breathing)+ parseInt(imm_post_op_circulation)+ parseInt(imm_post_op_consciousness)+ parseInt(imm_post_op_o2_saturation);
        $('#imm_post_op_total_score').val(total_score);
    });

</script>
