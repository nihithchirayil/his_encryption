<table id="result_data_table" class="table table-condensed table_sm table-bordered theadfix_wrapper"
        style="font-size: 12px;">
        <thead>
            <tr class="common_table_header">
                <th class="common_td_rules" width="10%">TIME</th>
                <th class="common_td_rules" width="10%">TEMP °C</th>
                <th class="common_td_rules" width="10%">PULSE/Min</th>
                <th class="common_td_rules" width="10%">RESP/Min</th>
                <th class="common_td_rules" width="10%">BP Sis/Dia</th>
                <th class="common_td_rules" width="10%">SPO2</th>
                <th class="common_td_rules" width="10%">RBS</th>
                <th class="common_td_rules" width="10%">PAIN</th>
                <th class="common_td_rules" width="10%">REMARKS</th>
            </tr>
        </thead>
        <tbody>
            @php
            $pre_op_vitals_data = '';
            $pre_op_vitals_data = $resultData->pre_op_vitals_data;
            $pre_op_vitals_data = json_decode($pre_op_vitals_data);
            $pre_operative_notes = '';
            $op_vital_handover_given_by = '';
            $op_vital_handover_taken_by = '';

            @endphp

@if(!empty($pre_op_vitals_data))


@php
                $pre_operative_notes = $pre_op_vitals_data->pre_operative_notes;
                $op_vital_handover_given_by = $pre_op_vitals_data->op_vital_handover_given_by;
                $op_vital_handover_taken_by = $pre_op_vitals_data->op_vital_handover_taken_by;
                $pre_op_vitals_data = $pre_op_vitals_data->op_vitals_params;
            @endphp
                @for($i=0; $i < sizeof($pre_op_vitals_data); $i++)
                    <tr>
                        <td>
                            @if(isset($pre_op_vitals_data[$i]->time_op) && $pre_op_vitals_data[$i]->time_op !='')
                                {{date('h:i A',strtotime($pre_op_vitals_data[$i]->time_op))}}
                            @endif
                        </td>
                        <td>
                            @if(isset($pre_op_vitals_data[$i]->temp_op) && $pre_op_vitals_data[$i]->temp_op !='')
                                {{$pre_op_vitals_data[$i]->temp_op}}
                            @endif
                        </td>
                        <td>
                            @if(isset($pre_op_vitals_data[$i]->pulse_op) && $pre_op_vitals_data[$i]->pulse_op !='')
                                {{$pre_op_vitals_data[$i]->pulse_op}}
                            @endif
                        </td>
                        <td>
                            @if(isset($pre_op_vitals_data[$i]->resp_op) && $pre_op_vitals_data[$i]->resp_op !='')
                                {{$pre_op_vitals_data[$i]->resp_op}}
                            @endif
                        </td>
                        <td>
                            @if(isset($pre_op_vitals_data[$i]->bp_op) && $pre_op_vitals_data[$i]->bp_op !='')
                                {{$pre_op_vitals_data[$i]->bp_op}}
                            @endif
                        </td>
                        <td>
                            @if(isset($pre_op_vitals_data[$i]->spo2_op) && $pre_op_vitals_data[$i]->spo2_op !='')
                                {{$pre_op_vitals_data[$i]->spo2_op}}
                            @endif
                        </td>
                        <td>
                            @if(isset($pre_op_vitals_data[$i]->rbs_op) && $pre_op_vitals_data[$i]->rbs_op !='')
                                {{$pre_op_vitals_data[$i]->rbs_op}}
                            @endif
                        </td>
                        <td>
                            @if(isset($pre_op_vitals_data[$i]->pain_op) && $pre_op_vitals_data[$i]->pain_op !='')
                                {{$pre_op_vitals_data[$i]->pain_op}}
                            @endif
                        </td>
                        <td>
                            @if(isset($pre_op_vitals_data[$i]->op_remark) && $pre_op_vitals_data[$i]->op_remark !='')
                                {{$pre_op_vitals_data[$i]->op_remark}}
                            @endif
                        </td>
                    </tr>
                @endfor
            @endif
        </tbody>
</table>

<div class="col-md-12">
    Pre Operative Notes :
    @if(isset($pre_operative_notes))
    {{$pre_operative_notes}}
    @endif
</div>
<div class="col-md-12">
    <div class="col-md-6 no-padding">
        Handover given By:
        @if(isset($op_vital_handover_given_by))
        {{$op_vital_handover_given_by}}
        @endif
    </div>
    <div class="col-md-6 no-padding">
        Handover taken By:
        @if(isset($op_vital_handover_taken_by))
        {{$op_vital_handover_taken_by}}
        @endif
    </div>
</div>
