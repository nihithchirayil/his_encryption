<style>
    label{
        margin-right:10px;
        float:left;clear:right;
        font-size:12px !important;
    }
    .form-control{
        float: left;
        clear:right;
        margin-right:10px;

    }
    .col-md-12{
        margin-top:8px;
    }
    .radio-success{
        width: 130px;
        margin-left: 10px;
        float: left;
        clear:right;
    }
</style>
@php
    $anasthesia_record_data = $resultData->anasthesia_record_data;
    if($anasthesia_record_data !=''){
        $anasthesia_record_data = json_decode($anasthesia_record_data);
    }
@endphp
@if($anasthesia_record_data !='')

<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">ANAESTHESIOLOGIST 1</label>
    <label style="font-weight:600;width:120px;">{{$anasthesia_record_data->ans_record_anaesthesiologist_1}}</label>

    <label style="margin-right:10px;float:left;clear:right;">2</label>
    <label style="font-weight:600;width:120px;">{{$anasthesia_record_data->ans_record_anaesthesiologist_2}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">SURGERY </label>
    <label style="font-weight:600;width:120px;">{{$anasthesia_record_data->ans_record_surgery}}</label>

    <label style="margin-right:10px;float:left;clear:right;">Date</label>
    <label style="font-weight:600;width:120px;">@if($anasthesia_record_data->ans_record_surgery_date !=''){{date('M-d-Y h:i A',strtotime($anasthesia_record_data->ans_record_surgery_date))}} @endif</label>

</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">SURGEON </label>
    <label style="font-weight:600;width:1100px;">{{$anasthesia_record_data->ans_record_surgeon}}</label>


</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label>TYPE OF
        ANAESTHESIA</label>
        <div class=" inline no-margin ">
            <input class="checkit" id="" type="radio" name="ans_record_type_anaesthesia" value='ga'>
            @if(isset($anasthesia_record_data->ans_record_type_anaesthesia) && $anasthesia_record_data->ans_record_type_anaesthesia == 'ga')
                    <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                @endif
            <label class="text-blue">
                G.A
            </label>
        </div>
        <div class=" inline no-margin ">
            @if(isset($anasthesia_record_data->ans_record_type_anaesthesia) && $anasthesia_record_data->ans_record_type_anaesthesia == 'sa')
                    <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                @endif
            <label class="text-blue">
                S.A
            </label>
        </div>
        <div class=" inline no-margin ">
            @if(isset($anasthesia_record_data->ans_record_type_anaesthesia) && $anasthesia_record_data->ans_record_type_anaesthesia == 'ea')
            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
            <label class="text-blue">
                E.A
            </label>
        </div>
        <div class=" inline no-margin ">
            @if(isset($anasthesia_record_data->ans_record_type_anaesthesia) && $anasthesia_record_data->ans_record_type_anaesthesia == 'cse')
            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
            <label class="text-blue">
                CSE
            </label>
        </div>
        <div class=" inline no-margin ">
            @if(isset($anasthesia_record_data->ans_record_type_anaesthesia) && $anasthesia_record_data->ans_record_type_anaesthesia == 'iv_sedation')
            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
            <label class="text-blue">
                TIVA
            </label>
        </div>
        <div class=" inline no-margin " style="margin-left:60px !important;">
            @if(isset($anasthesia_record_data->ans_record_type_anaesthesia) && $anasthesia_record_data->ans_record_type_anaesthesia == 'mac')
            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
            <label class="text-blue">
                MAC
            </label>
        </div>
        <div class=" inline no-margin ">
            @if(isset($anasthesia_record_data->ans_record_type_anaesthesia) && $anasthesia_record_data->ans_record_type_anaesthesia == 'ra')
            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
            <label class="text-blue">
                RA
            </label>
        </div>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">NURSE </label>
    <label style="font-weight:600;width:450px;">{{$anasthesia_record_data->ans_record_nurse}}</label>
    <label style="margin-right:10px;float:left;clear:right;">PREMEDICATION </label>
    <label style="font-weight:600;width:450px;">{{$anasthesia_record_data->ans_record_premedication}}</label>

</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">ANAESTHESIA ASSESSMENT BEFORE INDUCTION (not earlier than 15 mins before induction) TIME </label>
    <label style="font-weight:600;width:100px;">{{$anasthesia_record_data->ans_record_assesment_before_induction}}</label>

</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">HR</label>
    <label style="font-weight:600;width:100px;">{{$anasthesia_record_data->ans_record_hr}}</label>
    <label style="margin-right:10px;float:left;clear:right;">B.P</label>
    <label style="font-weight:600;width:100px;">{{$anasthesia_record_data->ans_record_bp}}</label>

    <label style="margin-right:10px;float:left;clear:right;">SPO2</label>
    <label style="font-weight:600;width:100px;">{{$anasthesia_record_data->ans_record_spo2}}</label>

    <label style="margin-right:10px;float:left;clear:right;">RR</label>
    <label style="font-weight:600;width:100px;">{{$anasthesia_record_data->ans_record_rr}}</label>

</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">General Condition</label>
    <label style="font-weight:600;width:1100px;">{{$anasthesia_record_data->ans_record_general_condition}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">IV Access</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_iv_access}}</label>

    <label style="margin-right:10px;float:left;clear:right;">Site</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_site}}</label>

    <label style="margin-right:10px;float:left;clear:right;">Cannula Guage</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_cannula_guage}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">REGIONAL ANAESTHESIA – Technique</label>
    <label style="font-weight:600;width:400px;">{{$anasthesia_record_data->ans_record_regional_anaesthesia_technique}}</label>


    <label style="margin-right:10px;float:left;clear:right;">Approach</label>
    <label style="font-weight:600;width:400px;">{{$anasthesia_record_data->ans_record_approach}}</label>
</div>

<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Needle</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_needle}}</label>

    <label style="margin-right:10px;float:left;clear:right;">Space</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_space}}</label>

    <label style="margin-right:10px;float:left;clear:right;">Drug</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_drug}}</label>
</div>

<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Onset of action</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_onset_of_action}}</label>
    <label style="margin-right:10px;float:left;clear:right;">Sensory Level</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_sensory_level}}</label>
    <label style="margin-right:10px;float:left;clear:right;">Motor Level</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_motor_level}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">GENERAL ANAESTHESIA-Preoxygenation</label>
    <label style="font-weight:600;width:900px;">{{$anasthesia_record_data->ans_record_general_ans_preoxygenation}}</label>


</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Induction</label>
    <label style="font-weight:600;width:50px;">{{$anasthesia_record_data->ans_record_induction}}</label>
    <label style="margin-right:10px;float:left;clear:right;">Inhalational</label>
    <label style="font-weight:600;width:50px;">{{$anasthesia_record_data->ans_record_inhalational}}</label>

    a) Airway (b) Mask (c) LMA (d) ET Tube
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">ET-Nasal/Oral,Size</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_et_nasal}}</label>

    <label style="margin-right:10px;float:left;clear:right;">Cuff</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_cuff}}</label>
    <label style="margin-right:10px;float:left;clear:right;">Other Information</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_other_info}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Ventilation-Controlled/Spontaneous , Circuit/ Polymask</label>
    <label style="font-weight:600;width:900px;">{{$anasthesia_record_data->ans_record_ventilation_controlled}}</label>

</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Ventilator-MV</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_ventilator_mv}}</label>

    <label style="margin-right:10px;float:left;clear:right;">RR</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_rr2}}</label>

    <label style="margin-right:10px;float:left;clear:right;">TV</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_tv}}</label>

    <label style="margin-right:10px;float:left;clear:right;">I:E Ratio</label>
    <label style="font-weight:600;width:75px;">{{$anasthesia_record_data->ans_record_ie_ratio}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Patient Position</label>
    <label style="font-weight:600;width:900px;">{{$anasthesia_record_data->ans_record_patient_position}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">TOURNIQUET-Applied at</label>
    <label style="font-weight:600;width:400px;">{{$anasthesia_record_data->ans_record_technique_applied}}</label>
    <label style="margin-right:10px;float:left;clear:right;">Removal at</label>
    <label style="font-weight:600;width:400px;">{{$anasthesia_record_data->ans_record_removal_at}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">OTHER NOTE (IF ANY)</label>
    <label style="font-weight:600;width:400px;">{{$anasthesia_record_data->ans_record_other_note}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Time of Induction:</label>
    <label style="font-weight:600;width:400px;">{{$anasthesia_record_data->ans_record_time_of_induction}}</label>


    <label style="margin-right:10px;float:left;clear:right;">Time of Incision:</label>
    <label style="font-weight:600;width:400px;">{{$anasthesia_record_data->ans_record_time_of_incision}}</label>

</div>
@endif
