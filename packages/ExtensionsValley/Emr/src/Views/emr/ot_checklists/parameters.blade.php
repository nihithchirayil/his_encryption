<div class="theadscroll" style="position: relative; height:430px;">
    <input type="hidden" name="surgery_request_id" id="surgery_request_id" value=""/>
    <table id="result_data_table" class="table table-condensed table_sm table-bordered theadfix_wrapper"
        style="font-size: 12px;">
        <thead>
            <tr class="common_table_header">
                <th class="common_td_rules" width="5%">SL.No</th>
                <th class="common_td_rules" width="70%">PARAMETERS</th>
                <th class="common_td_rules" width="20%">OPTIONS</th>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="common_td_rules">1</td>
                <td class="common_td_rules">Vitals signs Recorded</td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_vital" type="radio" name="vital_sign_recorded" value=1>
                                <label class="text-blue" for="with_vital">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_vital" type="radio" name="vital_sign_recorded" value=2>
                                <label class="text-blue" for="not_with_vital">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_vital" type="radio" name="vital_sign_recorded" value=0>
                                <label class="text-blue" for="na_vital">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">2</td>
                <td class="common_td_rules">
                    @php
                        $surgery_detail_id = $surgery_details[0]->id;
                        $surgery_sql = \DB::select("select string_agg(sm.name,',') as surgery_name from surgery_request_surgery srs join surgery_master sm on sm.id = srs.surgery_id
                        where srs.request_detail_id = $surgery_detail_id ");
                        if(!empty($surgery_sql)){
                           $surgery_name =  $surgery_sql[0]->surgery_name;
                        }else{
                            $surgery_name = '';
                        }
                    @endphp

                    <label style="float:left;clear:right;">Name of Procedure:</label>
                    &nbsp;&nbsp;
                    <input type="text" id="name_of_procedure_text" style="width:295px;height:17px!important;float:left;clear:right;" value={{$surgery_name}} class="form-control"/>

                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_procedure" type="radio" name="name_of_procedure" value=1>
                                <label class="text-blue" for="with_procedure">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_procedure" type="radio" name="name_of_procedure"
                                    value=2>
                                <label class="text-blue" for="not_with_procedure">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_procedure" type="radio" name="name_of_procedure" value=0>
                                <label class="text-blue" for="na_procedure">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">3</td>
                <td class="common_td_rules">
                    <label style="float:left;clear:right;">NBM &nbsp; Time:  </label>
                    &nbsp;&nbsp;
                    <input type="text" id="nbm_time" style="width:95px;height:17px!important;float:left;clear:right;" class="form-control timepicker"/>
                    </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_nbm" type="radio" name="nbm" value=1>
                                <label class="text-blue" for="with_nbm">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_nbm" type="radio" name="nbm" value=2>
                                <label class="text-blue" for="not_with_nbm">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_nbm" type="radio" name="nbm" value=0>
                                <label class="text-blue" for="na_nbm">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">4</td>
                <td class="common_td_rules"> <label style="clear: both;float:left" for="">AM care</label>
                    <div class="col-md-5">
                        <div class="col-md-6">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="am_care_sponge" type="radio" name="am_care" value='sponge'>
                                <label class="text-blue" for="sponge">
                                    Sponge
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="am_care_shower" type="radio" name="am_care" value=shower>
                                <label class="text-blue" for="shower">
                                    Shower
                                </label>
                            </div>
                        </div>

                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_am" type="radio" name="am_care_status" value=1>
                                <label class="text-blue" for="with_am">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_am" type="radio" name="am_care_status" value=2>
                                <label class="text-blue" for="not_with_am">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_am" type="radio" name="am_care_status" value=3>
                                <label class="text-blue" for="na_am">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">5</td>
                <td class="common_td_rules">

                    <label style="clear: both;float:left">Urinary Output:  &nbsp; Time:  &nbsp;</label>
                    &nbsp;
                    <input type="text" id="urinary_output_time" style="width:95px;height:17px!important;float:left;clear:right;" class="form-control timepicker"/>

                    <div class="radio radio-success inline no-margin" style="width:150px;float:left;clear:right;margin-left:15px !important;">
                        <input class="checkit" id="on_catheter" type="radio" name="on_catheter" value=1>
                        <label class="text-blue" for="catheter">
                            On Catheter
                        </label>
                    </div>

                    <label style="width:60px;float:left;clear:right;margin-left:170px;" for=""> Amount: </label></label>&nbsp;&nbsp;<input type="text" id="urine_amount" style="width:95px;height:17px!important;float:left;clear:right;" class="form-control">


                    {{-- <div class="col-md-4" style="float:left;clear:right;">
                        <div class="col-md-12">

                        </div>
                    </div> --}}
                    {{-- <div class="col-md-4" style="float:left;clear:right;">

                    </div> --}}


                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_urinary" type="radio" name="urinary_output_status" value='1'>
                                <label class="text-blue" for="with_urinary">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_urinary" type="radio" name="urinary_output_status"
                                    value='2'>
                                <label class="text-blue" for="not_with_urinary">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_urinary" type="radio" name="urinary_output_status" value='0'>
                                <label class="text-blue" for="na_urinary">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">6</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Enema</label></div>
                        <div class="col-md-3">
                            <div class="col-md-12">
                                <div class="checkbox checkbox-success inline">
                                    <input class="checkit" id="bowl_wash" type="checkbox" name="bowl_wash"
                                        value=1>
                                    <label class="text-blue" for="bowl_wash">
                                        Bowel Wash
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-12">
                                <div class="checkbox checkbox-success inline">
                                    <input class="checkit" id="laxatives" type="checkbox" name="laxatives"
                                        value=1>
                                    <label class="text-blue" for="laxatives">
                                        Laxatives
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-12">
                                <div class="checkbox checkbox-success inline">
                                    <input class="checkit" id="stool_passed" type="checkbox" name="stool_passed"
                                        value=1>
                                    <label class="text-blue" for="stool_passed">
                                        Stool Passed
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>



                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_enema" type="radio" name="enema_status" value=1>
                                <label class="text-blue" for="with_enema">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_enema" type="radio" name="enema_status" value=2>
                                <label class="text-blue" for="not_with_enema">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_enema" type="radio" name="enema_status" value=0>
                                <label class="text-blue" for="na_enema">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">7</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Skin Preparation</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_skin" type="radio" name="skin_preparation" value=1>
                                <label class="text-blue" for="with_skin">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_skin" type="radio" name="skin_preparation" value=2>
                                <label class="text-blue" for="not_with_skin">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_skin" type="radio" name="skin_preparation" value=0>
                                <label class="text-blue" for="na_skin">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">8</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Care of Hair and Nail</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_care" type="radio" name="care_hair_nail" value=1>
                                <label class="text-blue" for="with_care">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_care" type="radio" name="care_hair_nail" value=2>
                                <label class="text-blue" for="not_with_care">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_care" type="radio" name="care_hair_nail" value=0>
                                <label class="text-blue" for="na_care">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">9</td>
                <td class="common_td_rules">
                    <label style="display: flex;">Pre OP
                        medication given ( if yes,time &nbsp;&nbsp;
                        <input style="width:95px;height: 17px !important;"
                            type="text" id="pre_op_medication_time" class="form-control timepicker"/>&nbsp;&nbsp;)
                    </label>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_op" type="radio" name="pre_op_medication_status" value=1>
                                <label class="text-blue" for="with_op">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_op" type="radio" name="pre_op_medication_status" value=2>
                                <label class="text-blue" for="not_with_op">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_op" type="radio" name="pre_op_medication_status" value=0>
                                <label class="text-blue" for="na_op">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">10</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Medication Profile on chart</label>
                        </div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_medication" type="radio" name="medication_profile_chart"
                                    value=1>
                                <label class="text-blue" for="with_medication">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_medication" type="radio" name="medication_profile_chart"
                                    value=2>
                                <label class="text-blue" for="not_with_medication">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_medication" type="radio" name="medication_profile_chart"
                                    value=0>
                                <label class="text-blue" for="na_medication">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">11</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Allergies</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_allergies" type="radio" name="allergies"
                                    value=1>
                                <label class="text-blue" for="with_allergies">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_allergies" type="radio" name="allergies"
                                    value=2>
                                <label class="text-blue" for="not_with_allergies">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_allergies" type="radio" name="allergies" value=3>
                                <label class="text-blue" for="na_allergies">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">12</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;">
                            <label style="display: flex;">Blood Requirement (Blood Group &nbsp; &nbsp;
                            @php
                                $blood_groups = \DB::table('blood_group_master')
                                ->where('is_active',1)->pluck('group_description','group_description');
                            @endphp
                            {!! Form::select('required_blood_group',$blood_groups,null, ['class' => 'form-control', 'id' =>'required_blood_group','placeholder'=>'select', 'style' => 'color:#555555; padding:4px 12px;width:95px;']) !!}
                            </label>
                        </div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_blood_group" type="radio" name="required_blood_group_status"
                                    value=1>
                                <label class="text-blue" for="with_blood_group">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_blood_group" type="radio" name="required_blood_group_status"
                                    value=2>
                                <label class="text-blue" for="not_with_blood_group">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_blood_group" type="radio" name="required_blood_group_status"
                                    value=0>
                                <label class="text-blue" for="na_blood_group">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">13</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>COVID Test Report</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_covid" type="radio" name="covid_test_report_status" value=1>
                                <label class="text-blue" for="with_covid">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_covid" type="radio" name="covid_test_report_status" value=2>
                                <label class="text-blue" for="not_with_covid">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_covid" type="radio" name="covid_test_report_status" value=0>
                                <label class="text-blue" for="na_covid">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">14</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Serology Report</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_serology" type="radio" name="serology" value=1>
                                <label class="text-blue" for="with_serology">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_serology" type="radio" name="serology"
                                    value=2>
                                <label class="text-blue" for="not_with_serology">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_serology" type="radio" name="serology" value=0>
                                <label class="text-blue" for="na_serology">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">15</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>All relevant investigation
                                available</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_investigation" type="radio" name="relevant_investigation"
                                    value=1>
                                <label class="text-blue" for="with_investigation">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_investigation" type="radio"
                                    name="relevant_investigation" value=2>
                                <label class="text-blue" for="not_with_investigation">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_investigation" type="radio" name="relevant_investigation"
                                    value=0>
                                <label class="text-blue" for="na_investigation">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>

            <tr>
                <td class="common_td_rules">16</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Contact lenses/glasses removed and in
                                safe keeping</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_lenses" type="radio" name="contact_lenses" value=1>
                                <label class="text-blue" for="with_lenses">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_lenses" type="radio" name="contact_lenses" value=2>
                                <label class="text-blue" for="not_with_lenses">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_lenses" type="radio" name="contact_lenses" value=0>
                                <label class="text-blue" for="na_lenses">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">17</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Dentures & Implants</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_implants" type="radio" name="dentures_implants" value=1>
                                <label class="text-blue" for="with_implants">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_implants" type="radio" name="dentures_implants"
                                    value=2>
                                <label class="text-blue" for="not_with_implants">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_implants" type="radio" name="dentures_implants" value=0>
                                <label class="text-blue" for="na_implants">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">18</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Jewellery/other metal items
                                removed</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_metal" type="radio" name="jewellery_removed" value=1>
                                <label class="text-blue" for="with_metal">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_metal" type="radio" name="jewellery_removed" value=2>
                                <label class="text-blue" for="not_with_metal">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_metal" type="radio" name="jewellery_removed" value=0>
                                <label class="text-blue" for="na_metal">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">19</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Consent for surgery</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_consent" type="radio" name="consent" value=1>
                                <label class="text-blue" for="with_consent">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_consent" type="radio" name="consent"
                                    value=2>
                                <label class="text-blue" for="not_with_consent">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_consent" type="radio" name="consent" value=0>
                                <label class="text-blue" for="na_consent">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">20</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>High risk consent</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_High_risk_consent" type="radio"
                                    name="high_risk_consent" value=1>
                                <label class="text-blue" for="with_High_risk_consent">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_High_risk_consent" type="radio"
                                    name="high_risk_consent" value=2>
                                <label class="text-blue" for="not_with_High_risk_consent">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_High_risk_consent" type="radio"
                                    name="high_risk_consent" value=0>
                                <label class="text-blue" for="na_High_risk_consent">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">21</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Anaesthesia consent & PAC done</label>
                        </div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_anaesthesia_pac" type="radio"
                                    name="anaesthesia_pac" value=1>
                                <label class="text-blue" for="with_anaesthesia_pac">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_anaesthesia_pac" type="radio"
                                    name="anaesthesia_pac" value=2>
                                <label class="text-blue" for="not_with_anaesthesia_pac">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_anaesthesia_pac" type="radio"
                                    name="anaesthesia_pac" value=0>
                                <label class="text-blue" for="na_anaesthesia_pac">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">22</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Consultations for medical fitness
                                done</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_consultations" type="radio" name="consultation_medical_fitness"
                                    value=1>
                                <label class="text-blue" for="with_consultations">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_consultations" type="radio"
                                    name="consultation_medical_fitness" value=2>
                                <label class="text-blue" for="not_with_consultations">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_consultations" type="radio" name="consultation_medical_fitness"
                                    value=0>
                                <label class="text-blue" for="na_consultations">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">23</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Marking of site</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="with_mos" type="radio" name="marking_of_site" value=1>
                                <label class="text-blue" for="with_mos">
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="not_with_mos" type="radio" name="marking_of_site" value=2>
                                <label class="text-blue" for="not_with_mos">
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="na_mos" type="radio" name="marking_of_site" value=0>
                                <label class="text-blue" for="na_mos">
                                    NA
                                </label>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
        </tbody>
    </table>
    <div class="col-md-12" style="margin-top:10px;margin-bottom:20px;">
        <label>Remarks</label>
        <textarea class="form-control" id="parameters_remarks"></textarea>
    </div>
    <div class="col-md-12" style="margin-top:10px;margin-bottom:20px;">
        <div class="col-md-6">
            Handover given By :{!! Form::select('handover_given_by',$all_staff_ot_nurse,0, ['class' => 'form-control select2', 'id' =>'handover_given_by','placeholder' =>'select', 'style' => 'color:#555555; padding:4px 12px;']) !!}
        </div>
        <div class="col-md-6">
            Handover taken By :  {!! Form::select('handover_taken_by',$all_staff_ot_nurse,0, ['class' => 'form-control select2', 'id' =>'handover_taken_by','placeholder' =>'select','style' => 'color:#555555; padding:4px 12px;']) !!}
        </div>
    </div>
</div>
<div class="col-md-12 " style="margin-top:5px;margin-bottom:5px;">

        <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
            class="btn btn-danger pull-right">Close <i class="fa fa-times"></i></button>
    @if(in_array("OT Surgery Checklist Parameters", $groupArray))
    <button style="padding: 3px 3px" onclick="saveSurgeryChecklistParameters();" type="button" title="Save"
        class="btn btn-success pull-right btn_save_parameters" id="save_checklistbtn">Save<i id="save_checklistspin"
            class="fa fa-save padding_sm"></i>
    </button>
    @endif

    {{-- @if(in_array("OT Approval Access", $groupArray)) --}}
    {{-- <button style="padding: 3px 3px" onclick="ApproveSurgeryChecklist(1);" type="button" title="Save"
        class="btn btn-primary pull-right btn_approve_parameters" id="approve_checklistbtn">Approve<i id="save_checklistspin"
            class="fa fa-check padding_sm"></i>
    </button> --}}
    {{-- @endif --}}

</div>

<script>

$('#op_vitals_tbody tr td').on('blur', function(){
});

    function removeTableData(op_vital_row_id, from_type) {
    var value = $('.op' + op_vital_row_id + '' + from_type).filter(function () {
        return this.value != '';
    });
    if (value.length != 0) {
        bootbox.confirm({
            message: "Are your sure to remove this ?",
            buttons: {
                'confirm': {
                    label: "Remove",
                    className: 'btn-danger',
                    default: 'true'
                },
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-warning'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#' + op_vital_row_id + '_row_id' + from_type).remove();
                }
            }
        });
    } else {
        $('#' + op_vital_row_id + '_row_id' + from_type).remove();
    }
}

function checkTDIsLast(id, from_type) {
    if (!$('#' + id + '_row_id' + from_type).closest('tr').next().length) {
        if (from_type == 'nursing') {
            addRowInNursing();
        }
        if (from_type == 'vital') {
            addRowOPVital();
        }

    }
}
</script>
