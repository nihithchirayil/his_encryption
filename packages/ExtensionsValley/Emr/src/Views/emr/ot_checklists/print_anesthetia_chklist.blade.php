@php
    $anaesthesia_cheklist_data = $resultData->anaesthesia_cheklist_data;
    if($anaesthesia_cheklist_data !=''){
        $anaesthesia_cheklist_data = json_decode($anaesthesia_cheklist_data);
    }
    $colum_count = count($anaesthesia_cheklist_data->chk_time);
    // dd($colum_count);
@endphp
@if($anaesthesia_cheklist_data !='')
<table id="anasthetia_checklist_table" class="table table-condensed table_sm table-bordered theadfix_wrapper"
        style="font-size: 12px;">

        <tbody>
            <tr>
                <td class="common_td_rules">Time</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_time[$i]) && $anaesthesia_cheklist_data->chk_time[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_time[$i]}}
                        @endif
                    </td>
                @endfor
            </tr>


            <tr>
                <td class="common_td_rules">Oxygen</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_oxygen[$i]) && $anaesthesia_cheklist_data->chk_oxygen[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_oxygen[$i]}}
                        @endif
                    </td>
                @endfor
            </tr>

            <tr>
                <td class="common_td_rules">Nitrous oxide</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_nitrous_oxide[$i]) && $anaesthesia_cheklist_data->chk_nitrous_oxide[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_nitrous_oxide[$i]}}
                        @endif

                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">Halothane /Isoflurane</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_halothane[$i]) && $anaesthesia_cheklist_data->chk_halothane[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_halothane[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">SevofIurane/ Desflurane</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_sevofiurane[$i]) && $anaesthesia_cheklist_data->chk_sevofiurane[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_sevofiurane[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">Anaesthesia Drugs</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_anaesthesia_drugs[$i]) && $anaesthesia_cheklist_data->chk_anaesthesia_drugs[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_anaesthesia_drugs[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">1.</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_anaesthesia_drugs1[$i]) && $anaesthesia_cheklist_data->chk_anaesthesia_drugs1[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_anaesthesia_drugs1[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">2.</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_anaesthesia_drugs2[$i]) && $anaesthesia_cheklist_data->chk_anaesthesia_drugs2[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_anaesthesia_drugs2[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">3.</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_anaesthesia_drugs3[$i]) && $anaesthesia_cheklist_data->chk_anaesthesia_drugs3[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_anaesthesia_drugs3[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">4.</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_anaesthesia_drugs4[$i]) && $anaesthesia_cheklist_data->chk_anaesthesia_drugs4[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_anaesthesia_drugs4[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">5.</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_anaesthesia_drugs5[$i]) && $anaesthesia_cheklist_data->chk_anaesthesia_drugs5[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_anaesthesia_drugs5[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>

            <tr>
                <td class="common_td_rules">Antibiotics</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_antibiotics1[$i]) && $anaesthesia_cheklist_data->chk_antibiotics1[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_antibiotics1[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>

            <tr>
                <td class="common_td_rules">Fluids 1.</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_fluids1[$i]) && $anaesthesia_cheklist_data->chk_fluids1[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_fluids1[$i]}}
                        @endif
                    </td>
                @endfor
            </tr>
            <tr>
                <td class="common_td_rules">Fluids 2.</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_fluids2[$i]) && $anaesthesia_cheklist_data->chk_fluids2[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_fluids2[$i]}}
                        @endif
                    </td>
                @endfor
            </tr>
            <tr>
                <td class="common_td_rules">Fluids 3.</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_fluids3[$i]) && $anaesthesia_cheklist_data->chk_fluids3[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_fluids3[$i]}}
                        @endif
                    </td>
                @endfor
            </tr>
            <tr>
                <td class="common_td_rules">Fluids 4.</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_fluids4[$i]) && $anaesthesia_cheklist_data->chk_fluids4[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_fluids4[$i]}}
                        @endif
                    </td>
                @endfor
            </tr>
            <tr>
                <td class="common_td_rules">1.Crystalloids .</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_crystalloids[$i]) && $anaesthesia_cheklist_data->chk_crystalloids[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_crystalloids[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">2.Colloids.</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_colloids[$i]) && $anaesthesia_cheklist_data->chk_colloids[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_colloids[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">Invasive Lines 1
                </td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_invasive_lines[$i]) && $anaesthesia_cheklist_data->chk_invasive_lines[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_invasive_lines[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">Invasive Lines 2
                </td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_invasive_lines2[$i]) && $anaesthesia_cheklist_data->chk_invasive_lines2[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_invasive_lines2[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td title="Blood and Blood Products" class="common_td_rules">Blood and Blood Products</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_blood_and_blood_products[$i]) && $anaesthesia_cheklist_data->chk_blood_and_blood_products[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_blood_and_blood_products[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">Monitoring</td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">

                        @if(isset($anaesthesia_cheklist_data->chk_monitoring[$i]) && $anaesthesia_cheklist_data->chk_monitoring[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_monitoring[$i]}}
                        @endif

                    </td>
                @endfor
            </tr>

            <tr>
                <td title="Other Monitors (ET Co2 etc)" class="common_td_rules">Other Monitors (ET Co2 etc) </td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_other_monitors[$i]) && $anaesthesia_cheklist_data->chk_other_monitors[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_other_monitors[$i]}}
                        @endif
                    </td>
                @endfor
            </tr>
            <tr>
                <td class="common_td_rules">1.
                </td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_other_monitors1[$i]) && $anaesthesia_cheklist_data->chk_other_monitors1[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_other_monitors1[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">2.
                </td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_other_monitors2[$i]) && $anaesthesia_cheklist_data->chk_other_monitors2[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_other_monitors2[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">3.
                </td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_other_monitors3[$i]) && $anaesthesia_cheklist_data->chk_other_monitors3[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_other_monitors3[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">Urine Output.
                </td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_urine_output[$i]) && $anaesthesia_cheklist_data->chk_urine_output[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_urine_output[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">Blood Loss.
                </td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_blood_loss[$i]) && $anaesthesia_cheklist_data->chk_blood_loss[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_blood_loss[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">Total IV Fliud Given.
                </td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_total_iv_fliud_given[$i]) && $anaesthesia_cheklist_data->chk_total_iv_fliud_given[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_total_iv_fliud_given[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules">Reversal.
                </td>
                @for($i=0;$i<$colum_count;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        @if(isset($anaesthesia_cheklist_data->chk_reversal[$i]) && $anaesthesia_cheklist_data->chk_reversal[$i] !='')
                        {{$anaesthesia_cheklist_data->chk_reversal[$i]}}
                        @endif
                    </td>
                @endfor

            </tr>
            <tr>
                <td class="common_td_rules" style="text-align:center" colspan=7>
                   <b> Time of Completion of Surgery:</b>
                   @if(isset($anaesthesia_cheklist_data->chk_time_of_completion[$i]) && $anaesthesia_cheklist_data->chk_time_of_completion[$i] !='')
                   {{$anaesthesia_cheklist_data->chk_time_of_completion[$i]}}
                   @endif

                </td>
                <td class="common_td_rules" style="text-align:center" colspan=7>
                    <b>Time of Extubation:</b>
                    @if(isset($anaesthesia_cheklist_data->chk_time_of_extubation[$i]) && $anaesthesia_cheklist_data->chk_time_of_extubation[$i] !='')
                    {{$anaesthesia_cheklist_data->chk_time_of_extubation[$i]}}
                    @endif

                </td>
            </tr>
        </tbody>
</table>
@endif
Charting Guidelines(Signs):BP-x,HR- ,Sao2 -% ,EtCo2 -%Kpa ,Intubation-↓,Extubation-↑,Cardiac Output –CO ,Pulmonary Artery Wedge
Pressure - PAWP
