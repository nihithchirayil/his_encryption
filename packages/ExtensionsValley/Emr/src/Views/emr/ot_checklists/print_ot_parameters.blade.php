<div class="col-md-12 no-padding">
    <table id="" class="table table-condensed table_sm table-bordered theadfix_wrapper"
        style="font-size: 12px;">
        <thead>
            <tr class="common_table_header">
                <th class="common_td_rules" width="5%">SL.No</th>
                <th class="common_td_rules" width="70%">PARAMETERS</th>
                <th class="common_td_rules" width="20%">OPTIONS</th>

            </tr>
        </thead>
        <tbody>
            @php

                $parameters_data = $resultData->parameters_data;
                $parameters_data = json_decode($parameters_data);
            @endphp
            <tr>
                <td class="common_td_rules">1</td>
                <td class="common_td_rules">Vitals signs Recorded</td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                                @if(isset($parameters_data->vital_sign_recorded) && $parameters_data->vital_sign_recorded == 1)
                                    <i class="fa fa-check"></i>
                                @endif
                                <label class="text-blue" for="with_vital">
                                    Yes
                                </label>
                        </div>
                        <div class="col-md-4">
                                @if(isset($parameters_data->vital_sign_recorded) && $parameters_data->vital_sign_recorded == 2)
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                                <label class="text-blue" for="not_with_vital">
                                    No
                                </label>
                        </div>
                        <div class="col-md-4">
                                @if(isset($parameters_data->vital_sign_recorded) && $parameters_data->vital_sign_recorded == 0)
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                                <label class="text-blue" for="na_vital">
                                    NA
                                </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">2</td>
                <td class="common_td_rules">

                    <label style="float:left;clear:right;">Name of procedure :</label>
                    &nbsp;&nbsp;

                    @if(isset($parameters_data->name_of_procedure_text) && $parameters_data->name_of_procedure_text != '')
                            {{$parameters_data->name_of_procedure_text}}
                    @endif


                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                                @if(isset($parameters_data->name_of_procedure) && $parameters_data->name_of_procedure == 1)
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                                <label class="text-blue" for="with_procedure">
                                    Yes
                                </label>
                        </div>
                        <div class="col-md-4">
                                @if(isset($parameters_data->name_of_procedure) && $parameters_data->name_of_procedure == 2)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                                <label class="text-blue" for="not_with_procedure">
                                    No
                                </label>
                        </div>
                        <div class="col-md-4">
                                @if(isset($parameters_data->name_of_procedure) && $parameters_data->name_of_procedure == 0)
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                                <label class="text-blue" for="na_procedure">
                                    NA
                                </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">3</td>
                <td class="common_td_rules">
                    <label style="float:left;clear:right;">NBM &nbsp; Time:  </label>
                    &nbsp;&nbsp;

                    @if(isset($parameters_data->nbm_time) && $parameters_data->nbm_time == '')
                            {{date('h:i A',strtotime($parameters_data->nbm_time))}}
                    @endif

                    </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                                @if(isset($parameters_data->nbm) && $parameters_data->nbm == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                                <label class="text-blue" for="with_nbm">
                                    Yes
                                </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->nbm) && $parameters_data->nbm == 2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="with_nbm">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->nbm) && $parameters_data->nbm == 0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="with_nbm">
                                NA
                            </label>
                        </div>
                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">4</td>
                <td class="common_td_rules"> <label style="clear: both;float:left" for="">AM care</label>
                    <div class="col-md-5">
                        <div class="col-md-6">

                                @if(isset($parameters_data->am_care) && $parameters_data->am_care == 'sponge')
                                <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                                <label class="text-blue" for="sponge">
                                    Sponge
                                </label>
                        </div>
                        <div class="col-md-6">

                            @if(isset($parameters_data->am_care) && $parameters_data->am_care == 'shower')
                                <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                                <label class="text-blue" for="sponge">
                                    Shower
                            </label>
                        </div>

                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->am_care_status) && $parameters_data->am_care_status == 1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="with_am">
                                Yes
                            </label>
                        </div>
                        <div class="col-md-4">

                            @if(isset($parameters_data->am_care_status) && $parameters_data->am_care_status == 2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="with_am">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->am_care_status) && $parameters_data->am_care_status == 0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="with_am">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">5</td>
                <td class="common_td_rules">

                    <label style="clear: both;float:left">Urinary Output:  &nbsp; Time:  &nbsp;</label>
                    &nbsp;
                    @if(isset($parameters_data->urinary_output_time) && $parameters_data->urinary_output_time != '')
                        {{date('h:i A',strtotime($parameters_data->urinary_output_time))}}
                    @endif


                    <div class="inline no-margin" style="width:150px;float:left;clear:right;margin-left:15px !important;">

                        @if(isset($parameters_data->on_catheter) && $parameters_data->on_catheter == 1)
                         <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                        <label class="text-blue" for="catheter">
                            On Catheter
                        </label>
                    </div>

                    <label style="width:60px;float:left;clear:right;" for=""> Amount: </label></label>&nbsp;&nbsp;
                    @if(isset($parameters_data->urine_amount) && $parameters_data->urine_amount != '')
                        {{$parameters_data->urine_amount}}
                    @endif

                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">

                            @if(isset($parameters_data->urinary_output_status) && $parameters_data->urinary_output_status ==1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="with_urinary">
                                Yes
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->urinary_output_status) && $parameters_data->urinary_output_status ==2)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="with_urinary">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->urinary_output_status) && $parameters_data->urinary_output_status ==0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="with_urinary">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">6</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Enema</label></div>
                        <div class="col-md-3">
                            <div class="col-md-12">
                                @if(isset($parameters_data->bowl_wash) && $parameters_data->bowl_wash ==1)
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                                <label class="text-blue" for="bowl_wash">
                                    Bowel Wash
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-12">

                                @if(isset($parameters_data->laxatives) && $parameters_data->laxatives ==1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                                <label class="text-blue" for="laxatives">
                                    Laxatives
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-12">

                                @if(isset($parameters_data->stool_passed) && $parameters_data->stool_passed ==1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                                <label class="text-blue" for="stool_passed">
                                    Stool Passed
                                </label>
                            </div>
                        </div>
                    </div>



                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->enema_status) && $parameters_data->enema_status ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="with_enema">
                                Yes
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->enema_status) && $parameters_data->enema_status ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="with_enema">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->enema_status) && $parameters_data->enema_status ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="with_enema">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">7</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Skin Preparation</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">

                            @if(isset($parameters_data->skin_preparation) && $parameters_data->skin_preparation ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->skin_preparation) && $parameters_data->skin_preparation ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->skin_preparation) && $parameters_data->skin_preparation ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">8</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Care of Hair and Nail</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->care_hair_nail) && $parameters_data->care_hair_nail ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->care_hair_nail) && $parameters_data->care_hair_nail ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->care_hair_nail) && $parameters_data->care_hair_nail ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">9</td>
                <td class="common_td_rules">
                    <label style="display: flex;">Pre OP
                        medication given ( if yes,time &nbsp;&nbsp;
                        @if(isset($parameters_data->pre_op_medication_time) && $parameters_data->pre_op_medication_time !='')
                        {{date('h:i A',strtotime($parameters_data->pre_op_medication_time))}}
                        @endif
                        &nbsp;&nbsp;)
                    </label>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">

                            @if(isset($parameters_data->pre_op_medication_status) && $parameters_data->pre_op_medication_status ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->pre_op_medication_status) && $parameters_data->pre_op_medication_status ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->pre_op_medication_status) && $parameters_data->pre_op_medication_status ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">10</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Medication Profile on chart</label>
                        </div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">

                            @if(isset($parameters_data->medication_profile_chart) && $parameters_data->medication_profile_chart ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">

                            @if(isset($parameters_data->medication_profile_chart) && $parameters_data->medication_profile_chart ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->medication_profile_chart) && $parameters_data->medication_profile_chart ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>

                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">11</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Allergies</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">

                            @if(isset($parameters_data->allergies) && $parameters_data->allergies ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->allergies) && $parameters_data->allergies ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->allergies) && $parameters_data->allergies ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">12</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;">
                            <label style="display: flex;">Blood Requirement (Blood Group &nbsp; &nbsp;
                                @if(isset($parameters_data->required_blood_group) &&        $parameters_data->required_blood_group ==0)
                                {{$parameters_data->required_blood_group}}
                                @endif
                            </label>
                        </div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->required_blood_group_status) && $parameters_data->required_blood_group_status ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->required_blood_group_status) && $parameters_data->required_blood_group_status ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->required_blood_group_status) && $parameters_data->required_blood_group_status ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">13</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>COVID Test Report</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->covid_test_report_status) && $parameters_data->covid_test_report_status ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->covid_test_report_status) && $parameters_data->covid_test_report_status ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>


                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->covid_test_report_status) && $parameters_data->covid_test_report_status ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">14</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Serology Report</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->serology) && $parameters_data->serology ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->serology) && $parameters_data->serology ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->serology) && $parameters_data->serology ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">15</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>All relevant investigation
                                available</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->relevant_investigation) && $parameters_data->relevant_investigation ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>


                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->relevant_investigation) && $parameters_data->relevant_investigation ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->relevant_investigation) && $parameters_data->relevant_investigation ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>

            <tr>
                <td class="common_td_rules">16</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Contact lenses/glasses removed and in
                                safe keeping</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->contact_lenses) && $parameters_data->contact_lenses ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->contact_lenses) && $parameters_data->contact_lenses ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->contact_lenses) && $parameters_data->contact_lenses ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">17</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Dentures & Implants</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->dentures_implants) && $parameters_data->dentures_implants ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->dentures_implants) && $parameters_data->dentures_implants ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->dentures_implants) && $parameters_data->dentures_implants ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">18</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Jewellery/other metal items
                                removed</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->jewellery_removed) && $parameters_data->jewellery_removed ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->jewellery_removed) && $parameters_data->jewellery_removed ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->jewellery_removed) && $parameters_data->jewellery_removed ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">19</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Consent for surgery</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->consent) && $parameters_data->consent ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->consent) && $parameters_data->consent ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->consent) && $parameters_data->consent ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">20</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>High risk consent</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->high_risk_consent) && $parameters_data->high_risk_consent ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>


                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->high_risk_consent) && $parameters_data->high_risk_consent ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->high_risk_consent) && $parameters_data->high_risk_consent ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">21</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Anaesthesia consent & PAC done</label>
                        </div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->anaesthesia_pac) && $parameters_data->anaesthesia_pac ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->anaesthesia_pac) && $parameters_data->anaesthesia_pac ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>


                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->anaesthesia_pac) && $parameters_data->anaesthesia_pac ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">22</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Consultations for medical fitness
                                done</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->consultation_medical_fitness) && $parameters_data->consultation_medical_fitness ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>


                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->consultation_medical_fitness) && $parameters_data->consultation_medical_fitness ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->consultation_medical_fitness) && $parameters_data->consultation_medical_fitness ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
            <tr>
                <td class="common_td_rules">23</td>
                <td class="common_td_rules">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-3" style="padding: 0px;"><label>Marking of site</label></div>
                    </div>
                </td>
                <td class="common_td_rules">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            @if(isset($parameters_data->marking_of_site) && $parameters_data->marking_of_site ==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                Yes
                            </label>

                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->marking_of_site) && $parameters_data->marking_of_site ==2)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                No
                            </label>
                        </div>
                        <div class="col-md-4">
                            @if(isset($parameters_data->marking_of_site) && $parameters_data->marking_of_site ==0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                            <label class="text-blue" for="">
                                NA
                            </label>
                        </div>

                    </div>

                </td>
            </tr>
        </tbody>
    </table>
    <div class="col-md-12">
        Remarks :
        @if(isset($parameters_data->parameters_remarks))
        {{$parameters_data->parameters_remarks}}
        @endif
    </div>
    <div class="col-md-12">
        <div class="col-md-6 no-padding">
            Handover given By:
            @if(isset($parameters_data->handover_given_by))
            {{$parameters_data->handover_given_by}}
            @endif
        </div>
        <div class="col-md-6 no-padding">
            Handover taken By:
            @if(isset($parameters_data->handover_taken_by))
            {{$parameters_data->handover_taken_by}}
            @endif
        </div>
    </div>
</div>
