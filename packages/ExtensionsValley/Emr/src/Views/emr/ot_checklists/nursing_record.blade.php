
<div class="theadscroll" style="position: relative; height: 440px;">
    <form id="ot_checklist_ns_record" name="ot_checklist_ns_record">
    <div>
        <h5 style="margin-left:10px;" class="checklist_head_text">INTRAOPERATIVE NURSING RECORD</h5>
    </div>

    <div class="col-md-12" >
    <div class="col-md-6">Name of Procedure:<input class="form-control" type="text" value="" id="ns_record_procedure_name"></div>
  <div class="col-md-3">Procedure start time:<input class="form-control timepicker" type="text" value="" id="ns_record_procedure_start_time"></div>
  <div class="col-md-3">Procedure end time:<input class="form-control timepicker" type="text" value="" id="ns_record_procedure_end_time"></div>
  </div>
  <div class="col-md-12">
  <div class="col-md-6">Specimen:<input class="form-control" type="text" value="" id="ns_record_specimen"></div>
  <div class="col-md-6">Lab:<input class="form-control" type="text" value="" id="ns_record_lab"></div>
  </div>
  <div class="col-md-12" style="margin-bottom:10px">
  <div class="col-md-6">No. of specimen:<input class="form-control" type="text" value="" id="ns_record_no_specimen"></div>
  <div class="col-md-6">Collected By:<input class="form-control" type="text" value="" id="ns_record_collector"></div>
  </div>

   <div><h5 style="margin-left:10px;" class="checklist_head_text">PACKING /DRAINS/ CATHETERS</h5></div>

    <table id="result_data_table" class="table table-condensed table_sm table-bordered theadfix_wrapper"
        style="font-size: 12px;margin-top:10px;">
        <thead>
            <tr class="common_table_header">
                <th class="common_td_rules" width="15%">TYPE</th>
                <th class="common_td_rules" width="15%">SITE</th>
                <th class="common_td_rules" width="15%">SIZE</th>
                <th class="common_td_rules" width="15%">AMOUNT</th>
                <th class="common_td_rules" width="15%">DATE/TIME</th>
                <th class="common_td_rules" width="15%">STAFF NAME</th>
                <th class=" " width="10%" onclick="addRowInNursing()"><i class="fa fa-plus bg-primary"></i></th>
            </tr>
        </thead>
        <tbody id="op_pack_tbody">
        </tbody>

    </table>
    </form>

    <div class="col-md-12">
        Implants:
        <textarea name="ns_implants" id="ns_implants" class="form-control"></textarea>
    </div>


</div>

{{-- <div class="col-md-12" style="margin-top:10px;margin-bottom:20px;">
        <div class="col-md-6">
            Handover given By : <input type="text" class="form-control"/>
        </div>
        <div class="col-md-6">
            Handover taken By : <input type="text" class="form-control"/>
        </div>
    </div> --}}

    <div class="col-md-12 " style="margin-top:5px;margin-bottom:5px;">
        <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
            class="btn btn-danger pull-right">Close <i class="fa fa-times"></i></button>
    @if(in_array("OT Surgery Checklist NursingRecord", $groupArray))
    <button style="padding: 3px 3px" onclick="saveSurgeryNursingRecord();" type="button" title="Save"
        class="btn btn-success pull-right btn_save_nursing_record" id="save_checklistbtn">Save<i id="save_checklistspin"
            class="fa fa-save padding_sm"></i>
    </button>
    @endif
    {{-- @if(in_array("OT Approval Access", $groupArray)) --}}
    {{-- <button style="padding: 3px 3px" onclick="ApproveSurgeryChecklist(5);" type="button" title="Save"
        class="btn btn-primary pull-right btn_approve_nursing_record" id="approve_checklistbtn">Approve<i id="save_checklistspin"
            class="fa fa-check padding_sm"></i>
    </button> --}}
    {{-- @endif --}}
</div>

