<div>
    <div class="col-md-12"><b class="checklist_head_text"><h5>BEFORE PATIENT LEAVES OPERATING ROOM</h5></b></div>
    <div><b class="checklist_head_text"><h5>COUNTS</h5></b></div>
    <table id="result" class='table table-condensed table_sm table-bordered'  style="font-size: 12px;">
        <thead >
            <tr class="common_table_header">
            <th class="">Count</th>
            <th class="">Sponge</th>
            <th class="">Gauze</th>
            <th class="">Peanut Gauze</th>
            <th class="">Roller Gauze</th>
            <th class="">Vassel Loop</th>
            <th class="">Instrument</th>
            <th class="">Needle</th>
            <th class="">Circulatory Nurse</th>
            <th class="">Scrub Nurse</th>
            </tr>

        </thead>
        @php
        $sign_out_data = $resultData->sign_out_data;
        $sign_out_data = json_decode($sign_out_data);
    @endphp
        <tbody>
            <tr><td>Pre op</td>
                <td>
                    @if(isset($sign_out_data->preSpong) && $sign_out_data->preSpong == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->preGauze) && $sign_out_data->preGauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->prePeanut_Gauze) && $sign_out_data->prePeanut_Gauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->preRoller_Gauze) && $sign_out_data->preRoller_Gauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->preVassel_Loop) && $sign_out_data->preVassel_Loop == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->preInstrument) && $sign_out_data->preInstrument == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->preNeedle) && $sign_out_data->preNeedle == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->preCirculatory_Nurse) && $sign_out_data->preCirculatory_Nurse == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->preScrub_Nurse) && $sign_out_data->preScrub_Nurse == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
            </tr>

            <tr><td>Relief</td>
                <td>
                    @if(isset($sign_out_data->ReliSpong) && $sign_out_data->ReliSpong == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->ReliGauze) && $sign_out_data->ReliGauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->ReliPeanut_Gauze) && $sign_out_data->ReliPeanut_Gauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->ReliRoller_Gauze) && $sign_out_data->ReliRoller_Gauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->ReliVassel_Loop) && $sign_out_data->ReliVassel_Loop == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->ReliInstrument) && $sign_out_data->ReliInstrument == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->ReliNeedle) && $sign_out_data->ReliNeedle == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->ReliCirculatory_Nurse) && $sign_out_data->ReliCirculatory_Nurse == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->ReliScrub_Nurse) && $sign_out_data->ReliScrub_Nurse == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>

            </tr>
            <tr><td>At close</td>
                <td>
                    @if(isset($sign_out_data->AtclSpong) && $sign_out_data->AtclSpong == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->AtclGauze) && $sign_out_data->AtclGauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->AtclPeanut_Gauze) && $sign_out_data->AtclPeanut_Gauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->AtclRoller_Gauze) && $sign_out_data->AtclRoller_Gauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->AtclVassel_Loop) && $sign_out_data->AtclVassel_Loop == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->AtclInstrument) && $sign_out_data->AtclInstrument == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->AtclNeedle) && $sign_out_data->AtclNeedle == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->AtclCirculatory_Nurse) && $sign_out_data->AtclCirculatory_Nurse == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->AtclScrub_Nurse) && $sign_out_data->AtclScrub_Nurse == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
            </tr>
            <tr><td>Final</td>
                <td>
                    @if(isset($sign_out_data->FinalSpong) && $sign_out_data->FinalSpong == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->FinalGauze) && $sign_out_data->FinalGauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->FinalPeanut_Gauze) && $sign_out_data->FinalPeanut_Gauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->FinalRoller_Gauze) && $sign_out_data->FinalRoller_Gauze == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->FinalVassel_Loop) && $sign_out_data->FinalVassel_Loop == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->FinalInstrument) && $sign_out_data->FinalInstrument == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->FinalNeedle) && $sign_out_data->FinalNeedle == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->FinalCirculatory_Nurse) && $sign_out_data->FinalCirculatory_Nurse == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>
                    @if(isset($sign_out_data->FinalScrub_Nurse) && $sign_out_data->FinalScrub_Nurse == 1)
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
            </tr>

        </tbody>

    </table>


    <div class="col-md-12">
        <br>
        <div class="col-md-2">
            <b>Count Correct :</b>
        </div>
        <div class="col-md-1">
        <div class="col-md-12">
            @if(isset($sign_out_data->count_correct) && $sign_out_data->count_correct == 1)
            <i class="fa fa-check" aria-hidden="true"></i>
            @endif
            <label class="text-blue" for="count_correct_yes">
                Yes
            </label>

        </div>
        </div>
        <div class="col-md-1">
        <div class="col-md-12">
            @if(isset($sign_out_data->count_correct) && $sign_out_data->count_correct == 2)
            <i class="fa fa-check" aria-hidden="true"></i>
            @endif
            <label class="text-blue" for="count_correct_yes">
                No
            </label>
        </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="col-md-2">
        if No,surgeon informed :
        </div>
        <div class="col-md-1">
        <div class="col-md-12">
            @if(isset($sign_out_data->surgeon_inform) && $sign_out_data->surgeon_inform == 1)
            <i class="fa fa-check" aria-hidden="true"></i>
            @endif
            <label class="text-blue" for="count_correct_yes">
                Yes
            </label>
        </div>
        </div>
        <div class="col-md-1">
        <div class="col-md-12">
            @if(isset($sign_out_data->surgeon_inform) && $sign_out_data->surgeon_inform == 2)
            <i class="fa fa-check" aria-hidden="true"></i>
            @endif
            <label class="text-blue" for="count_correct_yes">
                No
            </label>

        </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-12">
            Time informed :
            @if(isset($sign_out_data->antibiotic_given_time) && $sign_out_data->antibiotic_given_time !='')
                {{date('h:i A',strtotime($sign_out_data->antibiotic_given_time))}}
            @endif
    </div>

    </div>
    <div class="col-md-12" style="margin-top: 10px;text-align:left;">
        Action taken/Remarks :
        @if(isset($sign_out_data->action_taken_remarks) && $sign_out_data->action_taken_remarks !='')
            {{$sign_out_data->action_taken_remarks}}
            @endif

    </div>
    <div class="col-md-12" style="margin-top: 10px;text-align:left;">
        Post Notes :
        @if(isset($sign_out_data->sign_out_post_notes) && $sign_out_data->sign_out_post_notes !='')
        {{$sign_out_data->sign_out_post_notes}}
        @endif


    </div>

    <div class="col-md-12">
        <div class="col-md-6 no-padding">
            Handover given By:
            @if(isset($sign_out_data->signout_handover_given_by))
            {{$sign_out_data->signout_handover_given_by}}
            @endif
        </div>
        <div class="col-md-6 no-padding">
            Handover taken By:
            @if(isset($sign_out_data->signout_handover_taken_by))
             {{$sign_out_data->signout_handover_taken_by}}
             @endif
        </div>
    </div>
