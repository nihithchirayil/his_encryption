<link
href="{{ asset('packages/extensionsvalley/master/default/css/new_emr.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
rel="stylesheet">
<style>
    .table_header_bg.header_normal_padding>th {
        padding: 8px !important;
    }

    .timepickerclass {
        font-weight: 600;
    }

    .timepickerclass_default {
        color: #a8a8a8 !important;
    }

    .bootstrap-datetimepicker-widget .form-control,
    .bootstrap-datetimepicker-widget .btn {
        box-shadow: none !important;
    }

    .bootstrap-datetimepicker-widget .timepicker-hour,
    .bootstrap-datetimepicker-widget .timepicker-minute,
    .bootstrap-datetimepicker-widget .timepicker-second {
        width: 70% !important;
    }

    .bootstrap-datetimepicker-widget td span {
        height: 34px !important;
        line-height: 34px !important;
    }

    .radio_container >label {
        font-size: 13px !important;
    }
    #presc-data-form{
        min-height:310px !important;
    }

</style>

@php
$is_nursing_station = 0;
$nursing_station = session()->get('nursing_station');
if ($nursing_station != '') {
    $is_nursing_station = 1;
}
@endphp

<input type="hidden" id="ot_patient_id" value="{{$patient_id}}"/>
<input type="hidden" id="ot_visit_id" value="{{$patient_id}}"/>
<input type="hidden" id="ot_encounter_id" value="{{$encounter_id}}"/>
<input type="hidden" id="ot_admitting_doctor_id" value="{{$admitting_doctor_id}}"/>
<input type="hidden" id="ot_priscription_head_id" value=""/>

<div class="box no-border no-margin prescription_wrapper anim" id="prescription_wrapper">
    <div class="box-body clearfix">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#drug" style="border-radius: 0px;"><b>Drug</b></a></li>
            <li><a data-toggle="tab" href="#consumables" style="border-radius: 0px;"><b>Consumables</b></a></li>
        </ul>
        <div class="tab-content">
            <div class="col-md-12" style="padding: 5px;">
                <div class="col-md-5 padding_sm pull-right" style="padding-right: 15px !important;">
                    <button style="width: 110px !important;float: right;height: 25px;" class="btn bg-blue" onclick="loadCompletePrescriptionHistory(1);"><i class="fa fa-history"
                            aria-hidden="true"></i> Complete History</button>
                    <button style="width: 110px !important;float: right;height: 25px;" class="btn bg-blue presc_history_btn" onclick="loadPrescriptionHistory(1, 1);"
                        data-toggle="collapse" data-target="#prescriptionCollapse"><i class="fa fa-history"></i>
                        History</button>
                    <button style="width: 110px !important;float: right;height: 25px;background-color:#337ab7 !important;color:white;" class="btn  fav_dropdown_btn light_purple_bg bg-blue"><i class="fa fa-star"></i>
                        Bookmarks</button>


                </div>
                <div class="col-md-3 padding_sm">
                    @if (session()->has('nursing_station'))
                        {!! Form::select('doctor_prescription', $doctor_list, isset($doctor_id) ? $doctor_id : null, ['class' => 'form-control select2', 'title' => 'Doctor', 'id' => 'doctor_prescription', 'style' => 'color:#555555; padding:2px 12px; ']) !!}
                    @else
                        {!! Form::select('doctor_prescription', $doctor_list, isset($doctor_id) ? $doctor_id : null, ['class' => 'form-control select2', 'title' => 'Doctor', 'id' => 'doctor_prescription', 'style' => 'color:#555555; padding:2px 12px; ', 'disabled' => 'disabled']) !!}
                    @endif
                </div>
                <div class="col-md-3 padding_sm">
                    @if (session()->has('nursing_station'))
                        @php
                            $default_station = session()->get('nursing_station');
                            $list_nursing_station = \DB::table('location')
                                ->where('is_nursing_station', '1')
                                ->orderBy('location_name')
                                ->pluck('location_name', 'id');
                        @endphp
                        {!! Form::select('prescription_nursing_station', $list_nursing_station, isset($default_station) ? $default_station : 0, ['class' => 'form-control select2', 'title' => 'Nursing station', 'id' => 'prescription_nursing_station', 'style' => 'color:#555555; padding:2px 12px;font-weight:600;']) !!}
                    @endif
                </div>

                <div class="col-md-1 padding_sm visit_wise_filter_presc_div " style="display:none;">
                    @php
                        $visit_type_arr = [
                            'OP' => 'OP',
                            'IP' => 'IP',
                            'ALL' => 'ALL',
                        ];
                    @endphp
                    {!! Form::select('visit_wise_filter_presc', $visit_type_arr, 'ALL', ['class' => 'form-control select2', 'title' => 'Visit Wise Filter', 'onchange' => 'loadPrescriptionHistory(1);', 'id' => 'visit_wise_filter_presc', 'style' => 'color:#555555; padding:2px 12px;font-weight:600;']) !!}
                </div>



                <!-- Prescription History View -->
                <div class="col-md-12 no-padding">
                    <div class="card collapse" id="prescriptionCollapse">
                        <div class="card_body" id="prescription-history-data-list">
                            {{-- @include('Emr::emr.prescription.prescription_history_view') --}}
                        </div>
                        <div class="clearfix"></div>
                        <div class="ht5"></div>
                        <div class="col-md-12 no-padding ">

                        </div>
                    </div>
                </div>
                <!-- Prescription History View -->

                @include('Emr::emr.prescription.prescription_favorite')

            </div>
            <div id="drug" class="tab-pane fade in active">
                <div class="col-md-12 no-padding expand_prescription_btn" style="cursor: pointer;">
                </div>
                <div class="col-md-12" style="padding: 2px;">

                    <div class="clearfix"></div>
                    <div class="ht5"></div>
                    <div class="card">
                        <div class="card_body" style="background: #FFF;" id="prescription_wrapper_card_body">
                            <div class="col-md-4">
                            <span style="display: inline-block;">
                                <label>Search Medicine by :</label>
                                <div class="radio_container" style="width:170px !important;">
                                    <input type="radio" id="m_s_type_brand" value="brand"
                                    name="m_search_type" checked >
                                    <label for="m_s_type_brand">Brand</label>

                                    <input type="radio" id="m_s_type_generic" value="generic"
                                    name="m_search_type">
                                    <label for="m_s_type_generic">Generic</label>

                                    <input type="radio" id="m_s_type_both" value="both" name="m_search_type"
                                    checked="">
                                    <label for="m_s_type_both">Both</label>
                                </div>
                            </span>
                            </div>

                            <div class="col-md-8">
                            <span style="display: inline-block;">
                                <label> Intend Type:</label>
                                <div class="radio_container"  style="width:560px !important;margin-right:25px !important;">
                                    <input type="radio" id="p_type_regular" value="1" name="p_search_type"
                                    checked="">
                                    <label for="p_type_regular">Regular</label>

                                    <input type="radio" id="p_type_new_adm" value="2" name="p_search_type">
                                    <label for="p_type_new_adm">New Admission</label>

                                    <input type="radio" id="p_type_emergency" value="3"
                                    name="p_search_type">
                                    <label for="p_type_emergency">Emergency</label>

                                    <input type="radio" id="p_type_discharge" value="4"
                                    name="p_search_type">
                                    <label for="p_type_discharge">Discharge</label>

                                    <input type="radio" id="p_type_outside" value="5" name="p_search_type">
                                    <label for="p_type_outside"> Own Medicine</label>
                                </div>
                            </span>
                            </div>

                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <table class="table no-margin no-border">
                                <thead>
                                    <tr class="bg-default" style="background-color:#e6e6e6;">
                                        <th style="width: 2%;">&nbsp;</th>
                                        <th style="width: 3%;">&nbsp;</th>
                                        <th style="width: 20%;">Medicine</th>
                                        <th style="width: 13%;">Generic Name</th>
                                        <th style="width: 7%;">Dose</th>
                                        <th style="width: 10%;">Frequency <i class="fa fa-plus"
                                                style="cursor: pointer;" onclick="addFrequency();"></i></th>
                                        <th style="width: 5%;">Days</th>
                                        <th style="width: 5%;">Quantity</th>
                                        <th style="width: 10%;">Start Time</th>
                                        <th style="width: 10%;">Route <i id="addDoctorRoutesBtn" class="fa fa-plus"
                                                style="cursor: pointer;" onclick="addRoute(0);"></i></th>
                                        <th style="width: 10%;">Instructions</th>
                                        <th width="5%" align="center" style="padding:0; padding-left: 15px;"><button
                                                class="btn light_purple_bg" onclick="addNewMedicine();"><i
                                                    class="fa fa-plus"></i></button></th>
                                    </tr>
                                </thead>

                            </table>
                            <select name="search_route" id="search_route" class="form-control" style="display:none;">
                                <option value="">Select Route</option>
                                @if (count($routesList) > 0)
                                    @foreach ($routesList as $ind => $opt)
                                        <option value="{{ $opt->route }}" data-route-id="{{ $opt->id }}">
                                            {{ $opt->route }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="frequency-list-div" style="display: none;">
                                <a style="float: left;" class="close_btn_freq_search">X</a>
                                <div class="freq_theadscroll" style="position: relative;">
                                    <table id="FrequencyTable"
                                        class="table table-bordered-none no-margin table_sm table-striped presc_theadfix_wrapper">

                                        <tbody id="ListFrequencySearchData">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <div class="fav_dropdown fav_prescription_box" style="">
                                <div class="slidedown_close_btn fav_dropdown_close right_close presc_close_fav"> X
                                </div>
                                <input type="hidden" class="fav_service_code" value="" name="fav_service_code">
                                <div class="col-md-12 padding_sm">
                                    <h4 class="card_title">Create Group</h4>
                                    <div class="clearfix"></div>
                                    <div class="ht5"></div>
                                    <div class="col-md-6 padding_xs">
                                        <input type="text" class="form-control presc_fav_add_group_text"
                                            placeholder="Group Name" name="presc_fav_add_group_text">
                                    </div>
                                    <div class="col-md-4 text-right padding_xs">
                                        <button type="button" class="btn btn-block btn-primary presc_add_group_btn"><i
                                                class="fa fa-plus presc_add_group_icon"></i> Add Group</button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="h10"></div>
                                    <div class="" style="height: auto;  margin-top:10px;">
                                        <h4 class="card_title">Available Groups</h4>
                                        <div class="clearfix"></div>
                                        <div class="ht5"></div>
                                        <ul class="nav sm_nav vertical_tab_btn_fav_group text-center presc_add_fav_group_list"
                                            style="max-height: 350px; overflow-y:scroll;"></ul>
                                    </div>

                                </div>

                            </div>

                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <div id="presc-data-form" action="" style="min-height:200px;">
                                <div class="" style="">
                                    <table border="1"
                                        style="width: 100%;height: 15%;border:1px solid;border-collapse: collapse !important"
                                        class="table  table_sm  table-bordered" id="medicine-listing-table">
                                        <tbody>

                                        </tbody>
                                    </table>

                                        <div class="col-md-2 padding_sm" style="float:right;">
                                            <button type="button" onclick="saveDoctorPrescriptions(1);"
                                                class="btn btn-primary save_class" name="save_and_print_prescription1"
                                                id="save_and_print_prescription1" style="margin-left: 45px;"><i
                                                    class="fa fa-save"></i> Save Prescription</button>
                                        </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div id="consumables" class="tab-pane">
                <div class="col-md-12" style="padding:0 15px 0 0;">
                    <h4 class="card_title">
                        Prescription <span class="draft_mode_indication_p"></span>
                    </h4>
                </div>
                <div class="col-md-12" style="padding: 2px;">
                    <div class="card">
                        <div class="card_body" style="background: #FFF;"
                            id="prescription_consumable_wrapper_card_body">
                            <table class="table no-margin table_sm table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Search Medicine by :</td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_m_s_type_brand" value="brand"
                                                    name="consumable_m_search_type" checked="">
                                                <label for="consumable_m_s_type_brand"> Brand </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_m_s_type_generic" value="generic"
                                                    name="consumable_m_search_type">
                                                <label for="consumable_m_s_type_generic"> Generic </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_m_s_type_both" value="both"
                                                    name="consumable_m_search_type">
                                                <label for="consumable_m_s_type_both"> Both </label>
                                            </div>
                                        </td>
                                        <td width="25%">&nbsp;</td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_p_type_regular" value="1"
                                                    name="consumable_p_search_type" checked="">
                                                <label for="consumable_p_type_regular"> Regular </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_p_type_new_adm" value="2"
                                                    name="consumable_p_search_type">
                                                <label for="consumable_p_type_new_adm"> New Admission </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_p_type_emergency" value="3"
                                                    name="consumable_p_search_type">
                                                <label for="consumable_p_type_emergency"> Emergency </label>
                                            </div>
                                        </td>
                                        <td class="hide_class">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="consumable_p_type_discharge" value="4"
                                                    name="consumable_p_search_type">
                                                <label for="consumable_p_type_discharge"> Discharge </label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <table class="table no-margin no-border">
                                <thead>
                                    <tr class="table_header_bg header_normal_padding">
                                        <th>Medicine</th>
                                        <th>Quantity</th>
                                        <th colspan="2">Instructions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="35%">
                                            <input type="text" id="consumable_search_medicine"
                                                name="consumable_search_medicine" autocomplete="off"
                                                class="form-control" placeholder="Type at least 3 characters">

                                            <input type="hidden" name="consumable_search_item_code_hidden" value="">
                                            <input type="hidden" name="consumable_search_item_name_hidden" value="">
                                            <input type="hidden" name="consumable_search_item_price_hidden" value="">

                                            <!-- Medicine List -->
                                            <div class="consumable-medicine-list-div" style="display: none;">
                                                <a style="float: left;" class="close_btn_med_search">X</a>
                                                <div class=" presc_theadscroll" style="position: relative;">
                                                    <table id="ConsumableTable"
                                                        class="table table-bordered-none no-margin table_sm table-striped presc_theadfix_wrapper">
                                                        <thead>
                                                            <tr class="light_purple_bg">
                                                                <th>Medicine</th>
                                                                <th>Stock</th>
                                                                <th>Price</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="ConsumableListMedicineSearchData">

                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                            <!-- Medicine List -->

                                        </td>
                                        <td width="10%">
                                            <input type="text" name="consumable_search_quantity" autocomplete="off"
                                                id="consumable_search_quantity" class="form-control">
                                        </td>
                                        <td width="20%">
                                            <input type="text" name="consumable_search_instructions" autocomplete="off"
                                                id="consumable_search_instructions" class="form-control">
                                        </td>
                                        <td width="3%" align="center"><button class="btn light_purple_bg"
                                                onclick="addNewConsumable();"><i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="14" class="partition_line"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <div class="ht5"></div>
                            <form id="consumable-presc-data-form" action="#">
                                <div class="theadscroll" style="position: relative; height: 244px;">
                                    <table border="1"
                                        style="width: 100%;border:1px solid;border-collapse: collapse !important"
                                        class="table  table_sm  table-bordered" id="consumable-medicine-listing-table">
                                        <tbody>

                                        </tbody>
                                    </table>
                                    <div class="col-md-2 padding_sm" style="float:right;">
                                        <button type="button" onclick="saveDoctorConsumablePrescriptions();"
                                            class="btn btn-primary" name="save_and_print_prescription2"
                                            id="save_and_print_prescription2" style="margin-left: 45px;"><i
                                                class="fa fa-save"></i> Save and print</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 hide_class" style="margin: 5px;">
            <br>
            <div class="col-md-3 no-padding"><label>Approx Amount</label></div>
            <div class="col-md-1 padding_sm">
                <label id="med_total_amnt"></label>
                <input type="hidden" value="0" name="approx_amnt" id="approx_amnt">
            </div>

            @if ($is_nursing_station == 0)
                <div class="col-md-1 no-padding"><label>Next Review </label></div>
                <div class="col-md-3 padding_sm">
                    <input type="text" class="form-control bottom-border-text" name="next_review_date" id="next_review_date"
                        onblur="update_prescription_footer_fields()" value="" autocomplete="off">
                </div>
            @endif


            <div class="col-md-2 no-padding">
                &nbsp;<i class="fa fa-square" style="color: #f4c4a6;"></i>&nbsp; Allergy
            </div>
            <div class="col-md-2 no-padding">
                &nbsp;<i class="fa fa-square" style="color: #C9D5F3;"></i>&nbsp; Outside
            </div>
            <div class="clearfix"></div>
            <div class="h10"></div>

            @if ($is_nursing_station == 0)
                <div class="col-md-4 padding_sm" style="margin-top:15px;">
                    <label>Reason For Visit</label>
                    <textarea id="visit_reason" name="visit_reason" class="form-control bottom-border-text"
                        onblur="update_prescription_footer_fields()" style="height:90px !important;border-bottom: 2px solid lightgrey !important;" ></textarea>
                </div>
                <div class="col-md-4 padding_sm" style="margin-top:15px;">
                    <label>Diagnosis</label>
                    <textarea id="pres_history" name="pres_history" class="form-control bottom-border-text"
                        onblur="update_prescription_footer_fields()" style="height:90px !important;border-bottom: 2px solid lightgrey !important;" ></textarea>
                </div>
                <div class="col-md-4 padding_sm" style="margin-top:15px;">
                    <label>Special Notes</label>
                    <textarea id="pres_notes" name="pres_notes" class="form-control bottom-border-text"
                        onblur="update_prescription_footer_fields()" style="height:90px !important;border-bottom: 2px solid lightgrey !important;" ></textarea>
                </div>
            @endif

        </div>
    </div>
</div>

<!-- Prescription Print Config Modal -->
<div id="prescription_print_config_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 30%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Print Config</h4>
            </div>
            <div class="modal-body" style="height:200px;">
                <div class="col-md-12" id="prescription_print_config_list_data">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="print_assessment_check" id="print_assessment_check">
                        Clinical Assessment
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="print_investigation_check" id="print_investigation_check">
                        Investigation
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="print_prescription_check" id="print_prescription_check" checked="checked">
                        Prescription
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick="printPatientPrescriptionWithInvestigation();" id="print_prescription_and_investigation_btn" class="btn bg-primary pull-right" style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Prescription Print Config Modal -->

<script>
//when double click listing medicine search result (edit)
$(document).on('click', '.list-medicine-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);
    let generic_name = $(this).attr('data-genreric-name');
    let route = $(this).attr('data-route');
    let frequency = $(this).attr('data-frequency');
    let duration = $(this).attr('data-duration');
    let directions = $(this).attr('data-directions');
    let calculate_quantity = $(this).attr('data-calculate-quantity');
    let sel_row_td = $(tr).closest('.medicine-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let name = $(tr).find('input[name="list_med_name_hid[]"]').val();
    let code = $(tr).find('input[name="list_med_code_hid[]"]').val();

    let price = $(tr).find('input[name="list_med_price_hid[]"]').val();
    let subcategory_id = $(tr).find('input[name="list_med_sub_cat_id[]"]').val();


    let cur_row_itm_code = $(sel_row_tr).find('input[name="selected_item_code[]"]').val();
    let stock = $(this).attr('data-stock');

    if (route) {
        $(sel_row_tr).find("select").val(route);
    }
    if (frequency) {
        $(sel_row_tr).find(".freq_id_" + frequency).trigger('dblclick');
    }
    if (duration) {
        $(sel_row_tr).find("input[name='selected_item_duration[]']").val(duration);
    }
    if (directions) {
        $(sel_row_tr).find("input[name='selected_item_remarks[]']").val(directions);
    }

    if (!stock) {
        $('input[name="search_medicine"]').css("color", "#ef0a1a");
    } else {
        $('input[name="search_medicine"]').css("color", "unset");
    }

    $('input[name="selected_calculate_quantity[]"]').val(calculate_quantity);

    //same item already exists
    let alredy_exist = $('input[name="selected_item_code[]"]').filter(function (ind, obj) {
        if ($(obj).val() == code && $(obj).val() != cur_row_itm_code) {
            return code;
        }
    });

    if (alredy_exist.length > 0) {
        // Command: toastr["error"]("Medicine already exist.");
        var confirm = window.confirm("Medicine already exist. Do you want to continue ?");
        if (!confirm) {
            return false;
        }
    }

    $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.normalMedication').find("input[name='selected_item_dose[]']").focus();

    if (name != '' && code != '') {
        $(sel_row_td).removeClass('outside-medicine');
        $(sel_row_td).find('span[class="med_name"]').html(name);
        $(sel_row_td).find('input[name="selected_item_code[]"]').val(code);
        $(sel_row_td).find('input[name="selected_item_name[]"]').val(name);
        $(sel_row_td).find('input[name="selected_item_name[]"]').attr('data-generic-name', generic_name);
        $(sel_row_td).find('input[name="selected_item_name[]"]').attr('value', name);
        $(sel_row_td).find('input[name="selected_item_price[]"]').val(price);
        $(tr).closest('.medicine-list-div-row-listing').parents('tr').find('.generic_name_input').val(generic_name);

        $(sel_row_td).find(".medicine-list-div-row-listing").hide();

        if ($("#allegy_details").find('tr#' + code).length > 0) {
            $('input[name="search_medicine"]').css("background", '#f4c4a6');
            bootbox.alert('The patient is allergic to this medicine.');
        }
        else {
            if (generic_name != "") {
                var allegic = false;
                $("#allegy_details").find('h4').each(function (key, val) {
                    var allegic_item = $(val).text().toUpperCase();
                    if (allegic_item.indexOf(generic_name.toUpperCase()) !== -1) {
                        allegic = true;
                    }
                });

                if (allegic == true) {
                    $('input[name="search_medicine"]').css("background", '#f4c4a6');
                    bootbox.alert('The patient is allergic to this medicine.');
                } else {
                    $('input[name="search_medicine"]').css("background", '#ffff');
                }
            }


        }

        if ($('input[name="selected_item_name[]"]:not([value!=""])').length == 0) {
            addNewMedicine();
        }
    }

    $('.med_name').hide();

});


//medicine search listing(edit row)
var edit_row_timeout = null;
var edit_row_last_search_string = '';
$(document).on('keyup', 'input[name="selected_item_name[]"]', function (event) {
    event.preventDefault();
    if (event.keyCode == 13) {
        return false;
    }
    /* Act on the event */
    var obj = $(this);
    var edit_row_search_string = $(this).val().trim();
    var patient_id = $('#patient_id').val();
    var search_type = $("input[name='m_search_type']:checked").val();
    let tr = $(obj).closest('tr');
    let row_id = $(obj).closest('tr').attr('row-id');

    if (edit_row_search_string == "") {
        $(tr).find('span[class="med_name"]').html("");
        $(tr).find('input[name="selected_item_code[]"]').val("");
        $(tr).find('input[name="selected_item_name[]"]').val("");
        $(tr).find('input[name="selected_item_name[]"]').attr('data-generic-name', "");
        $(tr).find('input[name="selected_item_name[]"]').attr('value', "");
        $(tr).find('input[name="selected_item_price[]"]').val("");
        $(tr).find('input[name="selected_calculate_quantity[]"]').val("");
        $(tr).find('.medicine-list-div-row-listing').hide();
        $(tr).find('.medicine-list-div-row-listing').find('.list-medicine-search-data-row-listing').empty();
    }

    if (edit_row_search_string == "" || edit_row_search_string.length < 3) {
        edit_row_last_search_string = '';
        return false;
    } else {
        var med_list = $(tr).find('.medicine-list-div-row-listing');
        $(med_list).show();
        clearTimeout(edit_row_timeout);
        edit_row_timeout = setTimeout(function () {
            if (edit_row_search_string == edit_row_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/medicine-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_key_string: edit_row_search_string,
                    search_type: search_type,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $(tr).find('#MedicationTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="6"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data.medicine_list;
                    let res_data = "";

                    var search_list = $(tr).find('#ListMedicineSearchDataRowListing-' + row_id);




                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let item_desc = response[i].item_desc;
                            let item_code = response[i].item_code;
                            let generic_name = response[i].generic_name;
                            let price = response[i].price;
                            let subcategory_id = response[i].subcategory_id;
                            let stock = response[i].stock;
                            let route = response[i].route;
                            let frequency = response[i].frequency;
                            let duration = response[i].duration;
                            let directions = response[i].directions;
                            let category_name = response[i].category_name;
                            let subcategory_name = response[i].subcategory_name;
                            let calculate_quantity = response[i].calculate_quantity;
                            var nostock = '';
                            var color = '';
                            var color1 = '';
                            console.log(response[i].sound_a_like,response[i].look_a_like,item_code);
                            if(parseInt(response[i].lk_sd_prd)==2){
                                color='#0080009e';
                                color1='white';
                            }else if(parseInt(response[i].lk_sd_prd)==1){
                                color='#ffff008f';
                                color1='black';
                            }
                            if(response[i].sound_a_like==item_code){
                                color='#0080009e';
                                color1='white';
                            }else if(response[i].look_a_like==item_code){
                                color='#ffff008f';
                                color1='black';
                            }
                            if (!stock) {
                                nostock = 'no-stock'
                            }
                            res_data += '<tr style="background:'+color+';color:'+color1+'" data-genreric-name="' + generic_name + '" data-route="' + route + '" data-frequency="' + frequency + '" data-duration="' + duration + '" data-directions="' + directions + '" data-stock="' + stock + '" data-calculate-quantity="'+calculate_quantity+'"><td style="color:'+color1+'" class="' + nostock + '">' + item_desc + '</td><td style="color:'+color1+'">' + generic_name + '</td><td style="color:'+color1+'">' + category_name + '</td><td style="color:'+color1+'">' + subcategory_name + '</td><td style="color:'+color1+'">' + stock + '</td><td style="color:'+color1+'">' + price + '</td><input type="hidden" name="list_med_name_hid[]" id="list_med_name_hid-' + i + '" value="' + item_desc + '"><input type="hidden" name="list_med_code_hid[]" id="list_med_code_hid-' + i + '" value="' + item_code + '"><input type="hidden" name="list_med_price_hid[]" id="list_med_price_hid-' + i + '" value="' + price + '"><input type="hidden" name="list_med_sub_cat_id[]" id="list_med_sub_cat_id-' + i + '" value="' + subcategory_id + '"></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td colspan="6">No Data Found..!</td></tr>';
                    }

                    $(search_list).html(res_data);
                    edit_row_last_search_string = edit_row_search_string;
                    $(".presc_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.presc_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }

});

fetchFrequencyList();

// $('#medicine-listing-table tbody').sortable({
//         stop: function (event, ui) {
//             resetMedicineSerialNumbers();
//         }
// });

$(document).on("focus", "input[name='selected_item_frequency[]']", function () {
        $(this).parents('tr').find('.frequency-list-div-row-listing').show()
});

$(document).on("focus", "input[name='selected_item_duration[]'], input[name='selected_item_name[]'], input[name='selected_item_dose[]'], input[name='selected_item_quantity[]'], select[name='selected_item_route[]'], input[name='selected_item_remarks[]']", function () {
        $(this).parents('tr').find('.frequency-list-div-row-listing').hide();
});

$(document).on("focus", "input[name='selected_item_remarks[]']", function (event) {
    $(this).parents('tr').find('.instruction-list-div-row-listing').show();
    // $(event).keyup().change();
    if ($(event.currentTarget).parents('tr').find(".list-instruction-search-data-row-listing").find('tr').length == 0) {
        var esc = $.Event("keyup", { keyCode: 27 });
        $(event.currentTarget).parents('tr').find("input[name='selected_item_remarks[]']").trigger(esc);
    }

});

$(document).on("focus", "input[name='selected_item_duration[]'], input[name='selected_item_name[]'], input[name='selected_item_dose[]'], input[name='selected_item_quantity[]'], select[name='selected_item_route[]'], input[name='selected_item_frequency[]']", function () {
        $(this).parents('tr').find('.instruction-list-div-row-listing').hide();
});

$(document).on("focus", "input[name='selected_item_duration[]'], input[name='selected_item_frequency[]'], input[name='selected_item_dose[]'], input[name='selected_item_quantity[]'], select[name='selected_item_route[]'], input[name='selected_item_remarks[]']", function () {
        $(this).parents('tr').find('.medicine-list-div-row-listing').hide();
        calculateTotalMedicineAmount();
});

$(document).on("focus", "input[name='selected_item_duration[]'], input[name='selected_item_frequency[]'], input[name='selected_item_dose[]'], input[name='selected_item_quantity[]'], select[name='selected_item_route[]'], input[name='selected_item_name[]']", function () {
        $(this).parents('tr').find('.instruction-list-div-row-listing').hide();
});

$('#frequencypopup').on('hidden.bs.modal', function (e) {
        fetchFrequencyList();
});

setTimeout(function () {
    $('input[name="iv_selected_start_at[]"]').datetimepicker({
        format: 'DD-MMM-YYYY'
    })
}, 1000);


//close frequency search
$(document).on('click', '.frequency-list-div-row-listing > .close_btn_freq_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".frequency-list-div-row-listing").hide();
});

//when select frequency
$(document).on('click', '.list-frequency-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);

    let sel_row_td = $(tr).closest('.frequency-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let name = $(tr).find('input[name="list_freq_name_hid[]"]').val();
    let value = $(tr).find('input[name="list_freq_value_hid[]"]').val();
    let id = $(tr).find('input[name="list_freq_id_hid[]"]').val();

    if (name != '' && value != '') {
        $(sel_row_td).find('span[class="frequency"]').html(name);
        $(sel_row_td).find('input[name="selected_frequency_value[]"]').val(value);
        $(sel_row_td).find('input[name="selected_item_frequency[]"]').val(name);
        $(sel_row_td).find('input[name="selected_item_frequency[]"]').attr('value', name);
        $(sel_row_td).find('input[name="selected_frequency_id[]"]').val(id);

        $(sel_row_td).find(".frequency-list-div-row-listing").hide();
        $(sel_row_tr).find('input[name="selected_item_duration[]"]').focus().val($(sel_row_tr).find('input[name="selected_item_duration[]"]').val());;
    }

    $('.frequency').hide();

});


//route search (edit row)
var edit_row_route_timeout = null;
var edit_row_route_last_search_string = '';
$(document).on('keyup', 'input[name="selected_item_route[]"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var edit_row_search_route_string = $(this).val();
    var patient_id = $('#patient_id').val();
    let tr = $(obj).closest('tr');
    let row_id = $(obj).closest('tr').attr('row-id');

    if (edit_row_search_route_string == "" || edit_row_search_route_string.length < 2) {
        edit_row_route_last_search_string = '';
        return false;
    } else {
        var route_list = $(tr).find('.route-list-div-row-listing');
        $(route_list).show();
        clearTimeout(edit_row_route_timeout);
        edit_row_route_timeout = setTimeout(function () {
            if (edit_row_search_route_string == edit_row_route_last_search_string) {
                return false;
            }
            var url = $('#base_url').val() + "/emr/route-search";
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    search_route_string: edit_row_search_route_string,
                    patient_id: patient_id
                },
                beforeSend: function () {
                    $(tr).find('#RouteTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
                },
                success: function (data) {

                    let response = data;
                    let res_data = "";

                    var route_search_list = $(tr).find('#ListRouteSearchDataRowListing-' + row_id);

                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            let route = response[i].route;

                            res_data += '<tr><td>' + route + '</td></tr>';
                        }
                    } else {
                        res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                    }

                    $(route_search_list).html(res_data);
                    edit_row_route_last_search_string = edit_row_search_route_string;
                    $(".route_theadscroll").animate({ scrollTop: 0 }, "slow");

                },
                complete: function () {
                    $('.route_theadfix_wrapper').floatThead("reflow");
                }
            });
        }, 500)

    }
});

//close route search
$(document).on('click', '.route-list-div-row-listing > .close_btn_route_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".route-list-div-row-listing").hide();
});

//when select route
$(document).on('dblclick', '.list-route-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);

    let sel_row_td = $(tr).closest('.route-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let route = $(tr).text();

    if (route != '') {
        $(sel_row_td).find('input[name="selected_item_route[]"]').val(route);

        $(sel_row_td).find(".route-list-div-row-listing").hide();
        $(sel_row_td).find('input[name="selected_item_route[]"]').focus();
    }

});


$(document).on('keyup', 'input[name="selected_item_remarks[]"]', function (event) {
    event.preventDefault();
    /* Act on the event */
    var obj = $(this);
    var edit_row_search_instruction_string = $(this).val() ? $(this).val().trim() : "";
    var patient_id = $('#patient_id').val();
    let tr = $(obj).closest('tr');
    let row_id = $(obj).closest('tr').attr('row-id');

    var instruction_list = $(tr).find('.instruction-list-div-row-listing');
    $(instruction_list).show();
    clearTimeout(edit_row_instruction_timeout);
    edit_row_instruction_timeout = setTimeout(function () {
        var url = $('#base_url').val() + "/emr/instruction-search";
        var req_data = {};
        req_data.search_instruction_string = edit_row_search_instruction_string;
        req_data.patient_id = patient_id;

        if (edit_row_search_instruction_string == "") {
            req_data.all = 1;
        }
        $.ajax({
            type: "GET",
            url: url,
            data: req_data,
            beforeSend: function () {
                $(tr).find('#InstructionTableRowListing-' + row_id + ' > tbody').html('<tr class="text-center"><td colspan="2"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-fade"></i></td></tr>');
            },
            success: function (data) {

                let response = data;
                let res_data = "";
                var instruction_search_list = $(tr).find('#ListInstructionSearchDataRowListing-' + row_id);
                if (response.length > 0) {
                    for (var i = 0; i < response.length; i++) {

                        let instruction = response[i].instruction;

                        res_data += '<tr><td>' + instruction + '</td></tr>';
                    }
                } else {
                    res_data = '<tr class="text-center"><td>No Data Found..!</td></tr>';
                }

                $(instruction_search_list).html(res_data);
                $(".route_theadscroll").animate({ scrollTop: 0 }, "slow");

            },
            complete: function () {
                $('.route_theadfix_wrapper').floatThead("reflow");
            }
        });
    }, 500)
});

//close instruction search
$(document).on('click', '.instruction-list-div-row-listing > .close_btn_instruction_search', function (event) {
    event.preventDefault();
    /* Act on the event */
    $(".instruction-list-div-row-listing").hide();
});

//when select route
$(document).on('click', '.list-instruction-search-data-row-listing tr', function (event) {
    event.preventDefault();
    /* Act on the event */

    let tr = $(this);

    let sel_row_td = $(tr).closest('.instruction-list-div-row-listing').parent('td');
    let sel_row_tr = $(sel_row_td).closest('tr');

    let instruction = $(tr).text();
    if (instruction == 'No Data Found..!') {
        $(sel_row_td).find(".instruction-list-div-row-listing").hide();
        return false;
    }

    if (instruction != '') {
        $(sel_row_td).find('input[name="selected_item_remarks[]"]').val(instruction);

        $(sel_row_td).find(".instruction-list-div-row-listing").hide();
    }

});

</script>
