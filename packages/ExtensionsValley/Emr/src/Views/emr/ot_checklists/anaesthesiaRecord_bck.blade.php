<div class="col-md-12 padding_sm">
    <div class="col-md-4 padding_sm">
        <div class=" box-body theadscroll" style="position:relative;height:180px;;">
            <table class='table table-condensed table-bordered  theadfix_wrapper' style="font-size: 12px;">
                <thead>
                    <tr class="common_table_header">
                        <th colspan="3">CIRCLE AS INDICATED PREMEDICATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="common_td_rules">Too Depressed</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="too_depressed_yes" type="radio" name="too_depressed"
                                    value=1>
                                <label class="text-blue" for="too_depressed_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="too_depressed_no" type="radio" name="too_depressed"
                                    value=0>
                                <label class="text-blue" for="too_depressed_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Worm Off</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="wormoff_yes" type="radio" name="wormoff" value=1>
                                <label class="text-blue" for="wormoff_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="wormoff_no" type="radio" name="wormoff" value=0>
                                <label class="text-blue" for="wormoff_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Apprehensive</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="apprehensive_yes" type="radio" name="apprehensive"
                                    value=1>
                                <label class="text-blue" for="apprehensive_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="apprehensive_no" type="radio" name="apprehensive"
                                    value=0>
                                <label class="text-blue" for="apprehensive_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Inadequate</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="Insdequate_yes" type="radio" name="Insdequate"
                                    value=1>
                                <label class="text-blue" for="Insdequate_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="Insdequate_no" type="radio" name="Insdequate" value=0>
                                <label class="text-blue" for="Insdequate_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-4 padding_sm">
        <div class=" box-body theadscroll" style="position:relative;height:180px;;">
            <table class='table table-condensed table-bordered theadfix_wrapper' style="font-size: 12px;">
                <thead>
                    <tr class="common_table_header">
                        <th colspan="6">INDUCTION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="common_td_rules">Excitement</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_zero" type="radio" name="excitement"
                                    value=0>
                                <label class="text-blue" for="excitement_zero">
                                    0
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_one" type="radio" name="excitement"
                                    value=1>
                                <label class="text-blue" for="excitement_one">
                                    1
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_two" type="radio" name="excitement"
                                    value=2>
                                <label class="text-blue" for="excitement_two">
                                    2
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_three" type="radio" name="excitement"
                                    value=3>
                                <label class="text-blue" for="excitement_three">
                                    3
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_four" type="radio" name="excitement"
                                    value=4>
                                <label class="text-blue" for="excitement_four">
                                    4
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Laryngospasm</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="laryngospasm_zero" type="radio" name="laryngospasm"
                                    value=0>
                                <label class="text-blue" for="laryngospasm_zero">
                                    0
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="laryngospasm_one" type="radio" name="laryngospasm"
                                    value=1>
                                <label class="text-blue" for="laryngospasm_one">
                                    1
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="laryngospasm_two" type="radio" name="laryngospasm"
                                    value=2>
                                <label class="text-blue" for="laryngospasm_two">
                                    2
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="laryngospasm_three" type="radio" name="laryngospasm"
                                    value=3>
                                <label class="text-blue" for="laryngospasm_three">
                                    3
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="laryngospasm_four" type="radio" name="laryngospasm"
                                    value=4>
                                <label class="text-blue" for="laryngospasm_four">
                                    4
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Vomiting</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="vomiting_zero" type="radio" name="vomiting" value=0>
                                <label class="text-blue" for="vomiting_zero">
                                    0
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="vomiting_one" type="radio" name="vomiting" value=1>
                                <label class="text-blue" for="vomiting_one">
                                    1
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="vomiting_two" type="radio" name="vomiting" value=2>
                                <label class="text-blue" for="vomiting_two">
                                    2
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="vomiting_three" type="radio" name="vomiting" value=3>
                                <label class="text-blue" for="vomiting_three">
                                    3
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="vomiting_four" type="radio" name="vomiting" value=4>
                                <label class="text-blue" for="vomiting_four">
                                    4
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Cyanoise</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="cyanoise_zero" type="radio" name="cyanoise" value=0>
                                <label class="text-blue" for="cyanoise_zero">
                                    0
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="cyanoise_one" type="radio" name="cyanoise" value=1>
                                <label class="text-blue" for="cyanoise_one">
                                    1
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="cyanoise_two" type="radio" name="cyanoise" value=2>
                                <label class="text-blue" for="cyanoise_two">
                                    2
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="cyanoise_three" type="radio" name="cyanoise" value=3>
                                <label class="text-blue" for="cyanoise_three">
                                    3
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="cyanoise_four" type="radio" name="cyanoise" value=4>
                                <label class="text-blue" for="cyanoise_four">
                                    4
                                </label>
                            </div>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-4 padding_sm">
        <div class=" box-body theadscroll" style="position:relative;height:180px;;">
            <table class='table table-condensed table-bordered theadfix_wrapper' style="font-size: 12px;">
                <thead>
                    <tr class="common_table_header">
                        <th colspan="6">MAINTENANCE</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="common_td_rules">Excitement</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_main_zero" type="radio"
                                    name="excitement_main" value=0>
                                <label class="text-blue" for="excitement_main_zero">
                                    0
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_main_one" type="radio"
                                    name="excitement_main" value=1>
                                <label class="text-blue" for="excitement_main_one">
                                    1
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_main_two" type="radio"
                                    name="excitement_main" value=2>
                                <label class="text-blue" for="excitement_main_two">
                                    2
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_main_three" type="radio"
                                    name="excitement_main" value=3>
                                <label class="text-blue" for="excitement_main_three">
                                    3
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_main_four" type="radio"
                                    name="excitement_main" value=4>
                                <label class="text-blue" for="excitement_main_four">
                                    4
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Laryngospasm</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="laryngospasm_main_zero" type="radio"
                                    name="laryngospasm_main" value=0>
                                <label class="text-blue" for="laryngospasm_main_zero">
                                    0
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="laryngospasm_main_one" type="radio"
                                    name="laryngospasm_main" value=1>
                                <label class="text-blue" for="laryngospasm_main_one">
                                    1
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="laryngospasm_main_two" type="radio"
                                    name="laryngospasm_main" value=2>
                                <label class="text-blue" for="laryngospasm_main_two">
                                    2
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="laryngospasm_main_three" type="radio"
                                    name="laryngospasm_main" value=3>
                                <label class="text-blue" for="laryngospasm_main_three">
                                    3
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="laryngospasm_main_four" type="radio"
                                    name="laryngospasm_main" value=4>
                                <label class="text-blue" for="laryngospasm_main_four">
                                    4
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Sweating</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="sweating_main_zero" type="radio" name="sweating_main"
                                    value=0>
                                <label class="text-blue" for="sweating_main_zero">
                                    0
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="sweating_main_one" type="radio" name="sweating_main"
                                    value=1>
                                <label class="text-blue" for="sweating_main_one">
                                    1
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="sweating_main_two" type="radio" name="sweating_main"
                                    value=2>
                                <label class="text-blue" for="sweating_main_two">
                                    2
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="sweating_main_three" type="radio"
                                    name="sweating_main" value=3>
                                <label class="text-blue" for="sweating_main_three">
                                    3
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="sweating_main_four" type="radio" name="sweating_main"
                                    value=4>
                                <label class="text-blue" for="sweating_main_four">
                                    4
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Mucous</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="mucous_main_zero" type="radio" name="mucous_main"
                                    value=0>
                                <label class="text-blue" for="mucous_main_zero">
                                    0
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="mucous_main_one" type="radio" name="mucous_main"
                                    value=1>
                                <label class="text-blue" for="mucous_main_one">
                                    1
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="mucous_main_two" type="radio" name="mucous_main"
                                    value=2>
                                <label class="text-blue" for="mucous_main_two">
                                    2
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="mucous_main_three" type="radio" name="mucous_main"
                                    value=3>
                                <label class="text-blue" for="mucous_main_three">
                                    3
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="mucous_main_four" type="radio" name="mucous_main"
                                    value=4>
                                <label class="text-blue" for="mucous_main_four">
                                    4
                                </label>
                            </div>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-4 padding_sm" style="margin-top: 10px">
        <div class=" box-body theadscroll" style="position:relative;height:280px;">
            <table class='table table-condensed table-bordered theadfix_wrapper' style="font-size: 12px;">
                <thead>
                    <tr class="common_table_header">
                        <th colspan="3">OTHER INFORMATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="common_td_rules">Nasal</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="nasal_yes" type="radio" name="nasal" value=1>
                                <label class="text-blue" for="nasal_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="nasal_no" type="radio" name="nasal" value=0>
                                <label class="text-blue" for="nasal_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Blind</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="blind_yes" type="radio" name="blind" value=1>
                                <label class="text-blue" for="blind_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="blind_no" type="radio" name="blind" value=0>
                                <label class="text-blue" for="blind_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Oral</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="oral_yes" type="radio" name="oral" value=1>
                                <label class="text-blue" for="oral_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="oral_no" type="radio" name="oral" value=0>
                                <label class="text-blue" for="oral_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Tracheostomy</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="tracheostomy_yes" type="radio" name="tracheostomy"
                                    value=1>
                                <label class="text-blue" for="tracheostomy_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="tracheostomy_no" type="radio" name="tracheostomy"
                                    value=0>
                                <label class="text-blue" for="tracheostomy_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Cuff</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="cuff_yes" type="radio" name="cuff" value=1>
                                <label class="text-blue" for="cuff_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="cuff_no" type="radio" name="cuff" value=0>
                                <label class="text-blue" for="cuff_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Local Spray</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="local_spray_yes" type="radio" name="local_spray"
                                    value=1>
                                <label class="text-blue" for="local_spray_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="local_spray_no" type="radio" name="local_spray"
                                    value=0>
                                <label class="text-blue" for="local_spray_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Pack</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="pack_yes" type="radio" name="pack" value=1>
                                <label class="text-blue" for="pack_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="pack_no" type="radio" name="pack" value=0>
                                <label class="text-blue" for="pack_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-4 padding_sm" style="margin-top: 10px">
        <div class=" box-body theadscroll" style="position:relative;height:180px;">
            <table class='table table-condensed table-bordered theadfix_wrapper' style="font-size: 12px;">
                <thead>
                    <tr class="common_table_header">
                        <th colspan="3">RECOVERY</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="common_td_rules">Reflex</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="reflex_yes" type="radio" name="reflex" value=1>
                                <label class="text-blue" for="reflex_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="reflex_no" type="radio" name="reflex" value=0>
                                <label class="text-blue" for="reflex_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Retching</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="retching_yes" type="radio" name="retching" value=1>
                                <label class="text-blue" for="retching_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="retching_no" type="radio" name="retching" value=0>
                                <label class="text-blue" for="retching_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Excitement</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_rec_yes" type="radio"
                                    name="excitement_rec" value=1>
                                <label class="text-blue" for="excitement_rec_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="excitement_rec_no" type="radio" name="excitement_rec"
                                    value=0>
                                <label class="text-blue" for="excitement_rec_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Vomiting</th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="vomiting_rec_yes" type="radio" name="vomiting_rec"
                                    value=1>
                                <label class="text-blue" for="vomiting_rec_yes">
                                    Yes
                                </label>
                            </div>
                        </th>
                        <th>
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="vomiting_rec_no" type="radio" name="vomiting_rec"
                                    value=0>
                                <label class="text-blue" for="vomiting_rec_no">
                                    No
                                </label>
                            </div>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-4 padding_sm" style="margin-top: 10px">
        <div class=" box-body theadscroll" style="position:relative;height:280px;">
            <table class='table table-condensed table-bordered theadfix_wrapper' style="font-size: 12px;">
                <thead>
                    <tr class="common_table_header">
                        <th colspan="2">RECOVERY VITALS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="common_td_rules">B.P</th>
                        <th>
                            <input type="text"  class="form-control" id="bp"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Pulse</th>
                        <th>
                            <input type="text"  class="form-control" id="pulse"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">SPO2</th>
                        <th>
                            <input type="text"  class="form-control" id="spo"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">ETCO</th>
                        <th>
                            <input type="text"  class="form-control" id="etco"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Estimated Blood Loss</th>
                        <th>
                            <input type="text"  class="form-control" id="estiamted_blood_loss">
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Total fluids given in O.R</th>
                        <th>
                            <input type="text"  class="form-control" id="total_fluids_given_in_or"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Blood</th>
                        <th>
                            <input type="text"  class="form-control" id="blood"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                        </th>
                    </tr>
                    <tr>
                        <th class="common_td_rules">Other</th>
                        <th>
                            <input type="text"  class="form-control" id="other">
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
