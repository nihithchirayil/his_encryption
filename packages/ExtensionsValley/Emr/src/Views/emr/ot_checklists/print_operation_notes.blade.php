<div class="col-md-12 theadscroll" style="position: relative; height:520px;">
    <div class="col-md-12">
        <button onclick="takePrintOtChecklist();" type="button" class="btn btn-primary pull-right">
            <i class="fa fa-print"></i> Print
        </button>
    </div>
    <div class="col-md-12" id="ResultDataContainer">
        <style>
            @media print {
                .pagebreak {
                    clear: both !important;
                    page-break-after: always !important;
                }
            }

            .box-body label{
                font-size:13px !important;
                font-weight: 700 !important;
            }
            label{
                margin:10px !important;
                font-weight: 700 !important;
            }
        </style>

        @php
            $operation_details_data = $resultData->operation_details_json;
            $operation_details_created_by =  $resultData->created_by;
            if($operation_details_data !=''){
                $operation_details_data = json_decode($operation_details_data);
            }
        @endphp

        <div class="col-md-12 no-padding">
            <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
                <thead>
                    <tr>
                        <th style='padding: 5px 0;' colspan='4' class='text-center'>
                            <h4 class="checklist_head_text"><b>OPERATION RECORD</b></h4>
                        </th>
                    </tr>
                </thead>
                <tbody style=''>
                    <tr>
                        <td style=' padding-left:5px;;text-align:left; '>Patient Name</td>
                        <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data[0]->patient_name !!}</td>
                        <td style=' padding-left:5px;;text-align:left;'>UHID No.</td>
                        <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->uhid !!}</td>
                    </tr>
                    <tr>
                        <td style=' padding-left:5px;;text-align:left; '>Age &amp; Gender</td>
                        <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data[0]->age   !!} / {!! $patient_data[0]->gender   !!}</td>
                        <td style=' padding-left:5px;; text-align:left;'> IP No. </td>
                        <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->admission_no   !!}</td>
                    </tr>
                    <tr>
                        <td rowspan='2' style=' padding-left:5px;;text-align:left;'> Address</td>
                        <td rowspan='2' style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->address   !!}</td>

                    </tr>
                    <tr>

                    </tr>

                    <tr>
                        <td style=' padding-left:5px;;text-align:left; '>Current Room</td>
                        <td style=' padding-left:5px;;text-align:left; '>{{$patient_data[0]->bed_name}}</td>
                        <td rowspan="2" style=' padding-left:5px;;text-align:left; '>DOS</td>
                        <td rowspan="2" style=' padding-left:5px;; text-align:left;'>
                            @if(!empty($surgery_date))
                                {{date("Y-m-d",strtotime($surgery_date))}}
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="col-md-12 text-center " style="padding: 10px;"><b class="checklist_head_text">OPERATION RECORD</b></div>
            <div class="col-md-12">
                <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
                <tr>
                    <td>
                        <b>Surgeon</b><br> {{ucwords(strtolower($operation_details_data->or_surgeon))}}

                    </td>
                    <td>
                        <b>Date </b><br>
                        {{$operation_details_data->or_scheduled_date}}
                    </td>
                    <td>
                       <b>Anesthesia Type </b><br>
                       @php
                           $anaesthesia_type = \DB::table('surgery_anesthesia')->where('id',$operation_details_data->or_anaesthesia_type)->value('name');
                       @endphp
                       {{$anaesthesia_type}}

                    </td>
                </tr>
                <tr>
                    <td><b>Assistant</b><br>{{$operation_details_data->or_assistant}}

                    </td>
                    <td><b>Time</b><br>@if($operation_details_data->or_scheduled_time !='') {{$operation_details_data->or_scheduled_time}} @endif

                    </td>
                    <td><b>Anaesthetist</b><br>{{ucwords(strtolower($operation_details_data->or_anaesthetist))}}
                    </td>
                </tr>
                <tr>
                    <td><b>Nurse</b><br>
                        @php
                            $nurse_name = \DB::table('users')->where('id',$operation_details_created_by)->value('name');
                        @endphp

                        {{ucwords(strtolower($nurse_name))}}
                    </td>
                    <td><b>To</b><br>{{$operation_details_data->or_to}}</td>
                    <td><b>Operation Type</b><br>
                        @if($operation_details_data->or_operation_type !='') {{$operation_details_data->or_operation_type}} @endif
                    </td>
                </tr>

                </table>
            </div>
            <div class="col-md-12" style="margin-top:20px;">
                <div class="col-md-6 box-body"><label for="">Pre Operation Diagnosis:</label>
                    <br>
                    <span style="padding-left:20px !important;">{{$operation_details_data->or_pre_operative_diagnosis}}</span>

                </div>
                <div class="col-md-6 box-body"><label for="">Post Operation Diagnosis:</label>
                    <br>
                    <span style="padding-left:20px !important;">{{$operation_details_data->or_post_operative_diagnosis}}
                    </span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 box-body">
                    <label for="">Incision:</label>
                    <br>
                    <span style="padding-left:20px !important;">{{$operation_details_data->or_incision}}
                    </span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6 box-body" style="min-height:120px;">
                    <label for="">Findings: </label>
                    <br>
                    <span style="padding-left:20px !important;">{{$operation_details_data->or_findings}}</span>
                </div>
                <div class="col-md-6 box-body" style="min-height:120px;">
                    <label for="">Procedure: </label>
                    <br>
                    <span style="padding-left:20px !important;">{{$operation_details_data->or_procedure}}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 box-body" style="min-height:120px;">
                    <label for="">Instructions: </label>
                    <br>
                    <span style="padding-left:20px !important;">{{$operation_details_data->or_instructions}}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 box-body">
                    <label for="">Procedure Done: </label>
                    <br>
                    <span style="padding-left:20px !important;">{{$operation_details_data->or_procedure_done}}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 box-body">
                    <label for="">Drain: </label>
                    <br>
                    <span style="padding-left:20px !important;">{{$operation_details_data->or_drain}}</span>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 10px;margin-bottom:20px;">
                <div class="col-md-10">
                    <label for="" style="display:flex">Material for H.P.E:
                        <div class="col-md-4">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="inline no-margin ">

                                        @if(isset($operation_details_data->or_hpe) && $operation_details_data->or_hpe == 1)
                                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                                        @endif
                                        <label class="text-blue" for="or_yes_hpe">
                                            Yes
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="inline no-margin ">
                                        @if(isset($operation_details_data->or_hpe) && $operation_details_data->or_hpe == 2)
                                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                                        @endif
                                        <label class="text-blue" for="or_no_hpe">
                                            No.
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
            </div>
            <div class="col-md-12" style="">
                <div class="col-md-9" style="width:80%;"></div>
                <div class="col-md-3" style="float:right;text-align: left;width:20%;">
                    Doctor:<span id='label_or_dr_name'>{{$doctor_details[0]->doctor_name}}</span>
                    <br>
                    Reg No: <span id='label_or_dr_licenseno'>{{$doctor_details[0]->license_no}}</span>
                    <br>
                    <span id='label_or_dr_sign'></span>
                    @if($signatureImage !='')
                        @php
                            $signatureImage = "data:image/jpeg;base64,".base64_decode($signatureImage);
                        @endphp
                        <img id="dr_signature" style="width:175px; height:70px;"
                        src="{{$signatureImage}}">
                    @endif
                </div>
            </div>
        </div>



        {{-- Additional operational data --}}


        @php
            $additional_operation_details_data = $resultData->additional_operational_data;
            $operation_details_created_by =  $resultData->created_by;
            if($additional_operation_details_data !=''){
                $additional_operation_details_data = json_decode($additional_operation_details_data);
        @endphp
        <div class="col-md-12 no-padding pagebreak" style="margin-top:40px;">
            <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
                <thead>
                    <tr>
                        <th style='padding: 5px 0;' colspan='4' class='text-center'>
                            <h4 class="checklist_head_text"><b>OPERATION RECORD</b></h4>
                        </th>
                    </tr>
                </thead>
                <tbody style=''>
                    <tr>
                        <td style=' padding-left:5px;;text-align:left; '>Patient Name</td>
                        <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data[0]->patient_name !!}</td>
                        <td style=' padding-left:5px;;text-align:left;'>UHID No.</td>
                        <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->uhid !!}</td>
                    </tr>
                    <tr>
                        <td style=' padding-left:5px;;text-align:left; '>Age &amp; Gender</td>
                        <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data[0]->age   !!} / {!! $patient_data[0]->gender   !!}</td>
                        <td style=' padding-left:5px;; text-align:left;'> IP No. </td>
                        <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->admission_no   !!}</td>
                    </tr>
                    <tr>
                        <td rowspan='2' style=' padding-left:5px;;text-align:left;'> Address</td>
                        <td rowspan='2' style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->address   !!}</td>

                    </tr>
                    <tr>

                    </tr>

                    <tr>
                        <td style=' padding-left:5px;;text-align:left; '>Current Room</td>
                        <td style=' padding-left:5px;;text-align:left; '>{{$patient_data[0]->bed_name}}</td>
                        <td rowspan="2" style=' padding-left:5px;;text-align:left; '>DOS</td>
                        <td rowspan="2" style=' padding-left:5px;; text-align:left;'>
                            @if(!empty($surgery_date))
                                {{date("Y-m-d",strtotime($surgery_date))}}
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="col-md-12 text-center " style="padding: 10px;"><b class="checklist_head_text">OPERATION RECORD</b></div>
            <div class="col-md-12">
                <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
                <tr>
                    <td>
                        <b>Surgeon</b><br> {{ucwords(strtolower($additional_operation_details_data->or_surgeon2))}}

                    </td>
                    <td>
                        <b>Date </b><br>
                        {{$additional_operation_details_data->or_scheduled_date2}}
                    </td>
                    <td>
                       <b>Anesthesia Type </b><br>
                       @php
                           $anaesthesia_type = \DB::table('surgery_anesthesia')->where('id',$additional_operation_details_data->or_anaesthesia_type2)->value('name');
                       @endphp
                       {{$anaesthesia_type}}

                    </td>
                </tr>
                <tr>
                    <td><b>Assistant</b><br>{{$additional_operation_details_data->or_assistant2}}

                    </td>
                    <td><b>Time</b><br>@if($additional_operation_details_data->or_scheduled_time2 !='') {{$additional_operation_details_data->or_scheduled_time2}} @endif

                    </td>
                    <td><b>Anaesthetist</b><br>{{ucwords(strtolower($additional_operation_details_data->or_anaesthetist2))}}
                    </td>
                </tr>
                <tr>
                    <td><b>Nurse</b><br>
                        @php
                            $nurse_name = \DB::table('users')->where('id',$operation_details_created_by)->value('name');
                        @endphp

                        {{ucwords(strtolower($nurse_name))}}
                    </td>
                    <td><b>To</b><br>{{$additional_operation_details_data->or_to2}}</td>
                    <td><b>Operation Type</b><br>
                        @if($additional_operation_details_data->or_operation_type2 !='') {{$additional_operation_details_data->or_operation_type2}} @endif
                    </td>
                </tr>

                </table>
            </div>
            <div class="col-md-12" style="margin-top:20px;">
                <div class="col-md-6 box-body"><label for="">Pre Operation Diagnosis:</label>
                    <br>
                    <span style="padding-left:20px !important;">{{$additional_operation_details_data->or_pre_operative_diagnosis2}}</span>

                </div>
                <div class="col-md-6 box-body"><label for="">Post Operation Diagnosis:</label>
                    <br>
                    <span style="padding-left:20px !important;">{{$additional_operation_details_data->or_post_operative_diagnosis2}}
                    </span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 box-body">
                    <label for="">Incision:</label>
                    <br>
                    <span style="padding-left:20px !important;">{{$additional_operation_details_data->or_incision2}}
                    </span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6 box-body" style="min-height:120px;">
                    <label for="">Findings: </label>
                    <br>
                    <span style="padding-left:20px !important;">{{$additional_operation_details_data->or_findings2}}</span>
                </div>
                <div class="col-md-6 box-body" style="min-height:120px;">
                    <label for="">Procedure: </label>
                    <br>
                    <span style="padding-left:20px !important;">{{$additional_operation_details_data->or_procedure2}}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 box-body" style="min-height:120px;">
                    <label for="">Instructions: </label>
                    <br>
                    <span style="padding-left:20px !important;">{{$additional_operation_details_data->or_instructions2}}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 box-body">
                    <label for="">Procedure Done: </label>
                    <br>
                    <span style="padding-left:20px !important;">{{$additional_operation_details_data->or_procedure_done2}}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 box-body">
                    <label for="">Drain: </label>
                    <br>
                    <span style="padding-left:20px !important;">{{$additional_operation_details_data->or_drain2}}</span>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 10px;margin-bottom:20px;">
                <div class="col-md-10">
                    <label for="" style="display:flex">Material for H.P.E:
                        <div class="col-md-4">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="inline no-margin ">

                                        @if(isset($additional_operation_details_data->or_hpe2) && $additional_operation_details_data->or_hpe2 == 1)
                                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                                        @endif
                                        <label class="text-blue" for="or_yes_hpe">
                                            Yes
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="inline no-margin ">
                                        @if(isset($additional_operation_details_data->or_hpe2) && $additional_operation_details_data->or_hpe2 == 2)
                                            <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                                        @endif
                                        <label class="text-blue" for="or_no_hpe">
                                            No.
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

        </div>

       @php
           }
       @endphp

    </div>
</div>
