<div class="theadscroll" style="position: relative; height:280px;">
    <table id="result_data_table" class="table table-condensed table_sm table-bordered theadfix_wrapper"
        style="font-size: 12px;">
        <thead>
            <tr class="common_table_header">
                <th class="common_td_rules" width="10%">TIME</th>
                <th class="common_td_rules" width="10%">TEMP °C</th>
                <th class="common_td_rules" width="10%">PULSE/Min</th>
                <th class="common_td_rules" width="10%">RESP/Min</th>
                <th class="common_td_rules" width="10%">BP Sis/Dia</th>
                <th class="common_td_rules" width="10%">SPO2</th>
                <th class="common_td_rules" width="10%">RBS</th>
                <th class="common_td_rules" width="10%">PAIN</th>
                <th class="common_td_rules" width="10%">REMARKS</th>
                <th class=" " width="10%" onclick="addRowOPVital()"><i class="fa fa-plus bg-primary"></i></th>
            </tr>
        </thead>
        <tbody id="op_vitals_tbody">
        </tbody>

    </table>
</div>

<div class="col-md-12" style="margin-top: 10px;">
    Pre Operative Notes <textarea id="pre_operative_notes" style="width: 100%; height: 68px !important; resize: none;" class="form-control"> </textarea>
</div>

<div class="col-md-12" style="margin-top:10px;margin-bottom:20px;">
    <div class="col-md-6">
        Handover given By :{!! Form::select('op_vital_handover_given_by',$all_staff_ot_nurse,0, ['class' => 'form-control select2', 'id' =>'op_vital_handover_given_by','placeholder' =>'select', 'style' => 'color:#555555; padding:4px 12px;']) !!}
    </div>

    <div class="col-md-6">
        Handover taken By :  {!! Form::select('op_vital_handover_taken_by',$all_staff_ot_nurse,0, ['class' => 'form-control select2', 'id' =>'op_vital_handover_taken_by','placeholder' =>'select','style' => 'color:#555555; padding:4px 12px;']) !!}
    </div>
</div>

<div class="col-md-12 " style="margin-top:5px;margin-bottom:5px;">
    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
        class="btn btn-danger pull-right">Close <i class="fa fa-times"></i></button>
    @if(in_array("OT Surgery Checklist PreOpVitals", $groupArray))
    <button style="padding: 3px 3px" onclick="saveSurgeryOpVitals();" type="button" title="Save"
        class="btn btn-success pull-right btn_save_op_vital" id="save_checklistbtn">Save<i id="save_checklistspin"
            class="fa fa-save padding_sm"></i>
    </button>
    @endif

    {{-- @if(in_array("OT Approval Access", $groupArray)) --}}
    {{-- <button style="padding: 3px 3px" onclick="ApproveSurgeryChecklist(2);" type="button" title="Save"
        class="btn btn-primary pull-right btn_approve_op_vital" id="approve_checklistbtn">Approve<i id="save_checklistspin"
            class="fa fa-check padding_sm"></i>
    </button> --}}
    {{-- @endif --}}

</div>
