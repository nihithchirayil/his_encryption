<style>
    .radio-success{
        width:50px !important;
        float:left;
        clear:right;
    }
    .radio label{
        padding-left:0px !important;
    }
</style>
<div class="theadscroll" style="position: relative; height:400px;">
    <div class="col-md-12 text-center " style="padding: 10px;"><b class="checklist_head_text">BEFORE INTRODUCTION OF ANAESTHESIA</b></div>

    <table id="result_data_table" class="table table-condensed table_sm table-bordered theadfix_wrapper"
    style="font-size: 12px;">
    <thead>
        <tr class="common_table_header">
            <th class="common_td_rules" width="5%">SL.No</th>
            <th class="common_td_rules" width="70%">PARAMETERS</th>
            <th class="common_td_rules" width="20%">OPTIONS</th>

        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="common_td_rules">1</td>
            <td class="common_td_rules">Patient identity verified</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="patient_identity_verified_yes" type="radio" name="patient_identity_verified" value=1>
                            <label class="text-blue" for="patient_identity_verified_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="patient_identity_verified_no" type="radio" name="patient_identity_verified" value=2>
                            <label class="text-blue" for="patient_identity_verified_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="patient_identity_verified_na" type="radio" name="patient_identity_verified" value=0>
                            <label class="text-blue" for="patient_identity_verified_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td class="common_td_rules">2</td>
            <td class="common_td_rules">Correct procedure</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="correct_procedure_yes" type="radio" name="correct_procedure" value=1>
                            <label class="text-blue" for="correct_procedure_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="correct_procedure_no" type="radio" name="correct_procedure" value=2>
                            <label class="text-blue" for="correct_procedure_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="correct_procedure_na" type="radio" name="correct_procedure" value=0>
                            <label class="text-blue" for="correct_procedure_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td class="common_td_rules">3</td>
            <td class="common_td_rules">Site marked</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="site_marked_yes" type="radio" name="site_marked" value=1>
                            <label class="text-blue" for="site_marked_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="site_marked_no" type="radio" name="site_marked" value=2>
                            <label class="text-blue" for="site_marked_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="site_marked_na" type="radio" name="site_marked" value=0>
                            <label class="text-blue" for="site_marked_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td class="common_td_rules">4</td>
            <td class="common_td_rules"> Availability of required medical records/
                images/studies/investigation results</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="availability_of_required_medical_yes" type="radio" name="availability_of_required_medical" value=1>
                            <label class="text-blue" for="availability_of_required_medical_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="availability_of_required_medical_no" type="radio" name="availability_of_required_medical" value=2>
                            <label class="text-blue" for="availability_of_required_medical_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="availability_of_required_medical_na" type="radio" name="availability_of_required_medical" value=0>
                            <label class="text-blue" for="availability_of_required_medical_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="common_td_rules">5</td>
            <td class="common_td_rules"> Availability& functionality of required
                        instruments/equipment/implants/medications</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="availability_and_functionality_of_required_yes" type="radio" name="availability_and_functionality_of_required" value=1>
                            <label class="text-blue" for="availability_and_functionality_of_required_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="availability_and_functionality_of_required_no" type="radio" name="availability_and_functionality_of_required" value=2>
                            <label class="text-blue" for="availability_and_functionality_of_required_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="availability_and_functionality_of_required_na" type="radio" name="availability_and_functionality_of_required" value=0>
                            <label class="text-blue" for="availability_and_functionality_of_required_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td class="common_td_rules">6</td>
            <td class="common_td_rules"> Anesthesia safety check completed</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="anesthesia_safety_check_completed_yes" type="radio" name="anesthesia_safety_check_completed" value=1>
                            <label class="text-blue" for="anesthesia_safety_check_completed_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="anesthesia_safety_check_completed_no" type="radio" name="anesthesia_safety_check_completed" value=2>
                            <label class="text-blue" for="anesthesia_safety_check_completed_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="anesthesia_safety_check_completed_na" type="radio" name="anesthesia_safety_check_completed" value=0>
                            <label class="text-blue" for="anesthesia_safety_check_completed_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td class="common_td_rules">7</td>
            <td class="common_td_rules"> Difficulty airway/aspiration risk</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="difficlty_airways_yes" type="radio" name="difficlty_airways" value=1>
                            <label class="text-blue" for="difficlty_airways_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="difficlty_airways_no" type="radio" name="difficlty_airways" value=2>
                            <label class="text-blue" for="difficlty_airways_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="difficlty_airways_na" type="radio" name="difficlty_airways" value=0>
                            <label class="text-blue" for="difficlty_airways_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td class="common_td_rules">8</td>
            <td class="common_td_rules"> Blood/blood product reserved</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="blood_product_reserved_yes" type="radio" name="blood_product_reserved" value=1>
                            <label class="text-blue" for="blood_product_reserved_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="blood_product_reserved_no" type="radio" name="blood_product_reserved" value=2>
                            <label class="text-blue" for="blood_product_reserved_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="blood_product_reserved_na" type="radio" name="blood_product_reserved" value=0>
                            <label class="text-blue" for="blood_product_reserved_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td class="common_td_rules">9</td>
            <td class="common_td_rules"> Risk of blood loss:
                         >500ml in adult>7ml/kg in children</td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="risk_of_blood_loss_yes" type="radio" name="risk_of_blood_loss" value=1>
                            <label class="text-blue" for="risk_of_blood_loss_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="risk_of_blood_loss_no" type="radio" name="risk_of_blood_loss" value=2>
                            <label class="text-blue" for="risk_of_blood_loss_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="risk_of_blood_loss_na" type="radio" name="risk_of_blood_loss" value=0>
                            <label class="text-blue" for="risk_of_blood_loss_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td class="common_td_rules">10</td>
            <td class="common_td_rules"> Antibiotic prophylaxis given within last 15-60 mts
                <br>
                Drug name :<input type="text" name="antibiotic_prophylaxis_drug_name" class="form-control" id="antibiotic_prophylaxis_drug_name" style="width:259px;clear:both;"> Time :<input
                        type="text" id="antibiotic_given_time" style="height: 17px!important;width:70px !important;margin-bottom:5px;" class="form-control timepicker">
            </td>
            <td class="common_td_rules">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="antibiotic_prophylaxis_yes" type="radio" name="antibiotic_prophylaxis" value=1>
                            <label class="text-blue" for="antibiotic_prophylaxis_yes">
                                Yes
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="antibiotic_prophylaxis_no" type="radio" name="antibiotic_prophylaxis" value=2>
                            <label class="text-blue" for="antibiotic_prophylaxis_no">
                                No
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit" id="antibiotic_prophylaxis_na" type="radio" name="antibiotic_prophylaxis" value=0>
                            <label class="text-blue" for="antibiotic_prophylaxis_na">
                                NA
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
    </table>

    <div class="col-md-12" style="margin-top:10px;margin-bottom:20px;">
        <div class="col-md-6">
            Completed By :{!! Form::select('signin_completed_by',$all_staff_ot_nurse,0, ['class' => 'form-control select2', 'id' =>'signin_completed_by','placeholder' =>'select', 'style' => 'color:#555555; padding:4px 12px;']) !!}
        </div>
        <div class="col-md-6">
            Completed At : <input class="form-control timepicker" name="signin_completed_at" id="signin_completed_at"/>
        </div>

    </div>
</div>

<div class="col-md-12 " style="margin-top:5px;margin-bottom:5px;">
    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
        class="btn btn-danger pull-right">Close <i class="fa fa-times"></i></button>
    @if(in_array("OT Surgery Checklist SignIn", $groupArray))
    <button style="padding: 3px 3px" onclick="saveSurgerySignIn();" type="button" title="Save"
        class="btn btn-success pull-right btn_save_signin" id="save_checklistbtn">Save<i id="save_checklistspin"
            class="fa fa-save padding_sm"></i>
    </button>
    @endif
    {{-- @if(in_array("OT Approval Access", $groupArray)) --}}
    {{-- <button style="padding: 3px 3px" onclick="ApproveSurgeryChecklist(3);" type="button" title="Save"
        class="btn btn-primary pull-right btn_approve_signin" id="approve_checklistbtn">Approve<i id="save_checklistspin"
            class="fa fa-check padding_sm"></i>
    </button> --}}
    {{-- @endif --}}
</div>
