<style>
    .chk_chart_codes_1{
        background-color: #710202;
        accent-color: #710202;
    }
    .chk_chart_codes_2{
        background-color: #5719e7;
        accent-color: #5719e7;
    }
    .chk_chart_codes_3{
        background-color: #016d0c;
        accent-color: #016d0c;
    }
    .chk_chart_codes_4{
        background-color: #ff4d00;
        accent-color: #ff4d00;
    }
    .chk_chart_codes_5{
        background-color: #ff42c6;
        accent-color: #ff42c6;
    }
    .chk_chart_codes_6{
        background-color: #7ff78b;
        accent-color: #7ff78b;
    }

    .form-control{
        box-shadow: none !important;
    }
    .headerClass{
        background: cornflowerblue !important;
        color:white;
    }
    .modal-open {
        overflow: auto;
    }
    .radio-success {
        float: left;
        clear: right;
    }
    .text-blue {
        margin-right: 10px;
        float: left;
        clear: right;
        font-size: 12px !important;
    }
    /* .time_value_col{
        width: 125px !important;
    } */
    span[title]:hover::after {
        content: attr(title);
        position: absolute;
        top: -100%;
        left: 0;
        font-display: 23px;
    }

</style>
@php
    $ar_data = json_decode($ar_data);
    $time_data = $ar_data->time_data;
    $o2_ratio = $ar_data->o2_ratio;
    $o2_ratio_vals = $ar_data->o2_ratio_vals;
    $n2o_ratio = $ar_data->n2o_ratio;
    $n2o_ratio_vals = $ar_data->n2o_ratio_vals;
    $air_ratio = $ar_data->air_ratio;
    $air_ratio_vals = $ar_data->air_ratio_vals;
    $induction_ratio = $ar_data->induction_ratio;
    $induction_ratio_vals = $ar_data->induction_ratio_vals;
    $inhalation_ratio = $ar_data->inhalation_ratio;
    $inhalation_ratio_vals = $ar_data->inhalation_ratio_vals;
    $relaxant_ratio = $ar_data->relaxant_ratio;
    $relaxant_ratio_vals = $ar_data->relaxant_ratio_vals;
    $blood_ivf_ratio = $ar_data->blood_ivf_ratio;
    $blood_ivf_ratio_vals = $ar_data->blood_ivf_ratio_vals;
    $ans_others_ratio = $ar_data->ans_others_ratio;
    $ans_others_ratio_vals = $ar_data->ans_others_ratio_vals;
    $chart_code_entry_data = ($ar_data->chart_code_entry_data);
    $spontaneous_row = $ar_data->spontaneous_row;
    $assisted_row = $ar_data->assisted_row;
    $controlled_row = $ar_data->controlled_row;
    $monotor_data =[];
    $monitor_data = $ar_data->monitor_data;

    // $_200 = 200;
    // dd($chart_code_entry_data->$_200[0]);

@endphp
<div class="col-md-12" style="margin-top:10px;">
    <table class="table table-bordered theadfix_wrapper">
        <tr>
            <td colspan="2">Patient : <label style="color:#5719e7;font-weight:600;float:right;"> {{$surgery_checklist[0]->patient_name}}</label></td>
            <td>Age/Gender : <label style="color:#5719e7;font-weight:600;float:right;">{{$surgery_checklist[0]->age}}/{{$surgery_checklist[0]->gender}}</label></td>
            <td>Hospital No : <label style="color:#5719e7;font-weight:600;float:right;">{{$admission_no}}</label></td>
        </tr>
        <tr>
            <td>Pre-Op Diaganosis : <input type="text" id="pre_op_diaganosis" style="color:#5719e7;font-weight:600; width:580px; float:right;border:1px solid #d8d8d8;" class="form_control" value="{{$surgery_checklist[0]->diaganosis}}"/></td>
            <td>Operation Done : <input type="text" id="operation_done" style="color:#5719e7;font-weight:600;border:1px solid #d8d8d8;width:165px;" class="form_control" value=""/></td>
            <td colspan="2">Surgeon : <label style="color:#5719e7;font-weight:600;float:right;">{{$surgery_checklist[0]->surgeon}}</label></td>
        </tr>
        <tr>
            <td>
                {{-- PreMedication : <input type="text" id="premedication" style="color:#5719e7;font-weight:600;width:630px;border:1px solid #d8d8d8;" class="form_control" value=""/>  --}}
            </td>
            <td>Date&Time :
                <input type="text" id="ar_scheduled_at" class="form-control datetimepicker" placeholder="" style="color:#5719e7;font-weight:600;width:160px;float:right;" @if($surgery_checklist[0]->scheduled_at !='') value="{{date('M-d-Y h:i A',strtotime($surgery_checklist[0]->scheduled_at))}}" @else value="" @endif/>
            </td>
            <td colspan="2">ASA Grade : <input type="text" id="asa_grade" style="color:#5719e7;font-weight:600;border:1px solid #d8d8d8;" class="form_control" value=""/></td>
        </tr>
        <tr>
            <td colspan="2">Anaesthetist : <label style="color:#5719e7;font-weight:600;float:right;">{{$surgery_checklist[0]->anaesthetist}}</label></td>
            <td colspan="2">Technique : <input type="text" id="ans_technique" style="color:#5719e7;font-weight:600;border:1px solid #d8d8d8;margin-left:5px;" class="form_control" value="{{$surgery_checklist[0]->anaesthesia}}"/></td>
        </tr>
    </table>
</div>

<div class="col-md-12">
    <div class="col-md-9 no-padding table-responsive" style="width: 75%;float:left;clear:right;">
        <table class="table table-bordered theadfix_wrapper table-fixed" id="ans_record_table">
            <tbody id="ans_record_table_tbody">
                <tr class="headerClass time_row">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="color:white !important;">
                        Time
                    </td>
                    @for($i=0;$i<sizeof($time_data);$i++)
                        <td class="common_td_rules time_value_col" style="text-align:center;background-color:cornflowerblue;color:white !important;">
                            {{$time_data[$i]}}
                        </td>
                    @endfor
                </tr>
                @if(isset($o2_ratio))
                @for($j=0;$j<sizeof($o2_ratio);$j++)
                <tr class="o2_row">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="">
                        <label style="width:30%;float:left;clar:right;"> 0<sub>2</sub> </label>
                        <span style="width:60%;float:left;clar:right;">{{$o2_ratio[$j]}}</span>

                    </td>
                    @for($i=0;$i<sizeof($time_data);$i++)
                    <td>
                        @if($o2_ratio_vals[$j][$i]==1)
                        <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </td>
                    @endfor
                </tr>
                @endfor
                @endif
                @if(isset($n2o_ratio))
                @for($j=0;$j<sizeof($n2o_ratio);$j++)
                <tr class="">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="">
                        <label style="width:30%;float:left;clar:right;">N<sub>2</sub>o</label>
                        <span style="width:60%;float:left;clar:right;">{{$n2o_ratio[$j]}}</span>

                    </td>
                    @for($i=0;$i<sizeof($time_data);$i++)
                    <td>
                        @if($n2o_ratio_vals[$j][$i]==1)
                        <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </td>
                    @endfor
                </tr>
                @endfor
                @endif
                @if(isset($air_ratio))
                @for($j=0;$j<sizeof($air_ratio);$j++)
                <tr class="">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="">
                        <label style="width:30%;float:left;clar:right;">Air</label>
                        <span style="width:60%;float:left;clar:right;">{{$air_ratio[$j]}}</span>

                    </td>
                    @for($i=0;$i<sizeof($time_data);$i++)
                    <td>
                        @if($air_ratio_vals[$j][$i]==1)
                        <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </td>
                    @endfor
                </tr>
                @endfor
                @endif
                @if(isset($induction_ratio))
                @for($j=0;$j<sizeof($induction_ratio);$j++)
                <tr class="">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="">
                        <label style="width:30%;float:left;clar:right;">Induction</label>
                        <span style="width:60%;float:left;clar:right;">{{$induction_ratio[$j]}}</span>

                    </td>
                    @for($i=0;$i<sizeof($time_data);$i++)
                    <td>
                        @if($induction_ratio_vals[$j][$i]==1)
                        <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </td>
                    @endfor
                </tr>
                @endfor
                @endif
                @if(isset($inhalation_ratio))
                @for($j=0;$j<sizeof($inhalation_ratio);$j++)
                <tr class="">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="">
                        <label style="width:30%;float:left;clar:right;">Inhalation</label>
                        <span style="width:60%;float:left;clar:right;">{{$inhalation_ratio[$j]}}</span>

                    </td>
                    @for($i=0;$i<sizeof($time_data);$i++)
                    <td>
                        @if($inhalation_ratio_vals[$j][$i]==1)
                        <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </td>
                    @endfor
                </tr>
                @endfor
                @endif
                @if(isset($relaxant_ratio))
                @for($j=0;$j<sizeof($relaxant_ratio);$j++)
                <tr class="">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="">
                        <label style="width:30%;float:left;clar:right;">Relaxant</label>
                        <span style="width:60%;float:left;clar:right;">{{$relaxant_ratio[$j]}}</span>

                    </td>
                    @for($i=0;$i<sizeof($time_data);$i++)
                    <td>
                        @if($relaxant_ratio_vals[$j][$i]==1)
                        <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </td>
                    @endfor
                </tr>
                @endfor
                @endif
                @if(isset($blood_ivf_ratio))
                @for($j=0;$j<sizeof($blood_ivf_ratio);$j++)
                <tr class="">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="min-width: 50px !important;">
                        <label style="width:30%;float:left;clar:right;">Blood/IVF</label>
                        <span style="width:60%;float:left;clar:right;">{{$blood_ivf_ratio[$j]}}</span>

                    </td>
                    @for($i=0;$i<sizeof($time_data);$i++)
                    <td>
                        @if($blood_ivf_ratio_vals[$j][$i]==1)
                        <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </td>
                    @endfor
                </tr>
                @endfor
                @endif
                @if(isset($ans_others_ratio))
                    @for($j=0;$j<sizeof($ans_others_ratio);$j++)
                    <tr class="">
                        <td colspan="2" class="col-xs-2 common_td_rules" style="min-width: 50px !important;">
                            <label style="width:30%;float:left;clar:right;">Others</label>
                            <span style="width:60%;float:left;clar:right;">{{$ans_others_ratio[$j]}}</span>

                        </td>
                        @for($i=0;$i<sizeof($time_data);$i++)
                        <td>
                            @if($ans_others_ratio_vals[$j][$i]==1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </td>
                        @endfor
                    </tr>
                    @endfor
                @endif

                <tr class="200_row">

                    <td colspan="2" style="text-align: left;">200</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:left;">
                        @php
                            $_200 = 200;
                        @endphp
                            @if($chart_code_entry_data->$_200[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_200[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="190_row">
                    <td colspan="2" style="text-align: left;">190</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_190 = 190;
                        @endphp
                            @if($chart_code_entry_data->$_190[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_190[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="180_row">
                    <td colspan="2" style="text-align: left;">180</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_180 = 180;
                        @endphp
                            @if($chart_code_entry_data->$_180[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_180[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="170_row">
                    <td colspan="2" style="text-align: left;">170</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_170 = 170;
                        @endphp
                            @if($chart_code_entry_data->$_170[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_170[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="160_row">
                    <td colspan="2" style="text-align: left;">160</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_160 = 160;
                        @endphp
                            @if($chart_code_entry_data->$_160[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_160[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="150_row">
                    <td colspan="2" style="text-align: left;">150</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_150 = 150;
                        @endphp
                            @if($chart_code_entry_data->$_150[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_150[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="140_row">
                    <td colspan="2" style="text-align: left;">140</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_140 = 140;
                        @endphp
                            @if($chart_code_entry_data->$_140[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_140[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="130_row">
                    <td colspan="2" style="text-align: left;">130</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_130 = 130;
                        @endphp
                            @if($chart_code_entry_data->$_130[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_130[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="120_row">
                    <td colspan="2" style="text-align: left;">120</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_120 = 120;
                        @endphp
                            @if($chart_code_entry_data->$_120[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_120[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="110_row">
                    <td colspan="2" style="text-align: left;">110</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_110 = 110;
                        @endphp
                            @if($chart_code_entry_data->$_110[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_110[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="100_row">
                    <td colspan="2" style="text-align: left;">100</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_100 = 100;
                        @endphp
                            @if($chart_code_entry_data->$_100[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_100[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="90_row">
                    <td colspan="2" style="text-align: left;">90</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_90 = 90;
                        @endphp
                            @if($chart_code_entry_data->$_90[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_90[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="80_row">
                    <td colspan="2" style="text-align: left;">80</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_80 = 80;
                        @endphp
                            @if($chart_code_entry_data->$_80[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_80[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="70_row">
                    <td colspan="2" style="text-align: left;">70</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_70 = 70;
                        @endphp
                            @if($chart_code_entry_data->$_70[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_70[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="60_row">
                    <td colspan="2" style="text-align: left;">60</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_60 = 60;
                        @endphp
                            @if($chart_code_entry_data->$_60[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_60[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="50_row">
                    <td colspan="2" style="text-align: left;">50</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_50 = 50;
                        @endphp
                            @if($chart_code_entry_data->$_50[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_50[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="40_row">
                    <td colspan="2" style="text-align: left;">40</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_40 = 40;
                        @endphp
                            @if($chart_code_entry_data->$_40[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_40[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="30_row">
                    <td colspan="2" style="text-align: left;">30</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_30 = 30;
                        @endphp
                            @if($chart_code_entry_data->$_30[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_30[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="20_row">
                    <td colspan="2" style="text-align: left;">20</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_20 = 20;
                        @endphp
                            @if($chart_code_entry_data->$_20[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_20[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="10_row">
                    <td colspan="2" style="text-align: left;">10</td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                    <td class="chart_code_entry" style="text-align:center;">
                        @php
                            $_10 = 10;
                        @endphp
                            @if($chart_code_entry_data->$_10[$i] !='')
                            @php
                                $chart_data = json_decode($chart_code_entry_data->$_10[$i]);
                            @endphp
                                @if(!empty($chart_data))
                                    @foreach($chart_data as $key=>$value)
                                        @php
                                            if($key == 1){
                                                $key_label = 'Anaes';
                                            }elseif($key == 2){
                                                $key_label = "Operation";
                                            }elseif($key == 3){
                                                $key_label = "BP";
                                            }elseif($key == 4){
                                                $key_label = "Pulse";
                                            }elseif($key == 5){
                                                $key_label = "ETCO2";
                                            }elseif($key == 6){
                                                $key_label = "SPO2";
                                            }
                                        @endphp
                                        {{$key_label}}: {{$value}}<br>
                                    @endforeach
                                @endif
                            @endif
                    </td>
                    @endfor
                </tr>
                <tr class="spontaneous_row">
                    <td colspan="2" class="common_td_rules">
                        Spontaneous
                    </td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                        <td style="text-align:center;" class="ans_chk">
                            @if($spontaneous_row[$i]!=0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </td>
                    @endfor
                </tr>
                <tr class="assisted_row">
                    <td colspan="2" class="common_td_rules">
                        Assisted
                    </td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                        <td style="text-align:center;" class="ans_chk">
                            @if($assisted_row[$i]!=0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </td>
                    @endfor
                </tr>
                <tr class="controlled_row">
                    <td colspan="2" class="common_td_rules">
                        Controlled
                    </td>
                    @for($i=0; $i<sizeof($time_data); $i++)
                        <td style="text-align:center;" class="ans_chk">
                            @if($controlled_row[$i]!=0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </td>
                    @endfor
                </tr>
            </tbody>
        </table>
        <div class="col-md-12">
            <label>Position</label>
            {{$ar_data->ans_position}}
        </div>
        <div class="col-md-6">
            <label>Anaesthesia Start Time:</label>
            {{$ar_data->ans_start_time}}
        </div>
        <div class="col-md-6">
            <label>Anaesthesia End Time:</label>
            {{$ar_data->ans_end_time}}
        </div>
        <div class="col-md-6">
            <label>Surgery Start Time</label>
            {{$ar_data->surgery_start_time}}
        </div>
        <div class="col-md-6">
            <label>Surgery End Time</label>
            {{$ar_data->surgery_end_time}}

        </div>

        <div class="col-md-12" style="margin-top:30px;">
            <label style="float: left;clear:right;margin-right: 10px;">Adequate respiratory efforts</label>
                <div class="radio radio-success inline no-margin ">
                    @if(isset($ar_data->adequate_resp_efforts) && ($ar_data->adequate_resp_efforts ==1))
                        Yes
                    @else
                        No
                    @endif
                </div>
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">Eye Opening</label>
                @if(isset($ar_data->eye_opening) && ($ar_data->eye_opening ==1))
                    Yes
                @else
                    No
                @endif
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">Obeying commands</label>
            @if(isset($ar_data->obeying_commands) && ($ar_data->obeying_commands ==1))
                Yes
            @else
                No
            @endif
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">Sustained head lift more than 3 sec</label>
            @if(isset($ar_data->sustained_head_lift) && ($ar_data->sustained_head_lift ==1))
                Yes
            @else
                No
            @endif
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">Tongue protrusion more than 5s</label>
            @if(isset($ar_data->tongue_protrusion) && ($ar_data->tongue_protrusion ==1))
            Yes
        @else
            No
        @endif
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">Airway reflexes present</label>
            @if(isset($ar_data->airway_reflexes) && ($ar_data->airway_reflexes ==1))
            Yes
        @else
            No
        @endif
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">After giving thorough ET & Oropharyngeal Suctioning, patient Extubated</label>
            @if(isset($ar_data->after_giving_thorough) && ($ar_data->after_giving_thorough ==1))
            Yes
        @else
            No
        @endif
        </div>


    </div>
    <div class="col-md-3 no-padding" style="width: 25%;float:left;clear:right;">
        <table class="table table-bordered theadfix_wrapper" style="width: 100%;">
            <tr class="headerClass">
                <td style="height:38px;color:white;" colspan="2">PREMEDICATION</td>
            </tr>
            <tr>
                <td>Too Depressed</td>
                <td>
                    @if(isset($ar_data->too_depressed ) &&($ar_data->too_depressed ==1))
                    Yes
                @else
                    No
                @endif
                </td>
            </tr>

            <tr>
                <td>Worn off</td>
                <td>
                        @if(isset($ar_data->worn_off) && ($ar_data->worn_off ==1))
                        Yes
                    @else
                        No
                    @endif
                </td>
            </tr>

            <tr>
                <td>Apprehensive</td>
                <td>
                    @if(isset($ar_data->pprehensive) && ($ar_data->pprehensive ==1))
                    Yes
                @else
                    No
                @endif
            </td>
            </tr>
            <tr>
                <td>Inadequate</td>
                <td>
                    @if(isset($ar_data->inadequate) && ($ar_data->inadequate ==1))
                    Yes
                @else
                    No
                @endif
            </td>
            </tr>
        </table>

        <table class="table table-bordered theadfix_wrapper" style="width: 100%;">
            <tr class="headerClass">
                <td colspan="2" style="color:white;">INDUCTION</td>
            </tr>
            <tr>
                <td>Excitement</td>
                <td>@if(isset($ar_data->excitement)){{$ar_data->excitement}}@endif</td>
            </tr>

            <tr>
                <td>Laryngospasm</td>
                <td>@if(isset($ar_data->laryngospasm)){{$ar_data->laryngospasm}}@endif</td>
            </tr>

            <tr>
                <td>Vomiting</td>
                <td>@if(isset($ar_data->vomiting)){{$ar_data->vomiting}}@endif</td>
            </tr>

            <tr>
                <td>Cyanosis</td>
                <td>@if(isset($ar_data->cyanosis)){{$ar_data->cyanosis}}@endif</td>
            </tr>
        </table>

        <table class="table table-bordered theadfix_wrapper" style="width: 100%;">
            <tr class="headerClass">
                <td colspan="3" style="color:white !important;" >MAINTENANCE</td>
            </tr>
            <tr>
                <td>Excitement</td>
                <td colspan='2'>@if(isset($ar_data->maintenance_excitement)){{$ar_data->maintenance_excitement}}@endif</td>
            </tr>
            <tr>
                <td>Laryngospasm</td>
                <td colspan='2'>@if(isset($ar_data->maintenance_laryngospasm)){{$ar_data->maintenance_laryngospasm}}@endif</td>
            </tr>

            <tr>
                <td>Sweating</td>
                <td colspan='2'>@if(isset($ar_data->maintenance_sweating)){{$ar_data->maintenance_sweating}}@endif</td>
            </tr>

            <tr>
                <td>Mucous</td>
                <td colspan='2'>@if(isset($ar_data->maintenance_mucous)){{$ar_data->maintenance_mucous}}@endif</td>
            </tr>
            <tr>
                <td>Mask</td>
                <td colspan='2'>@if(isset($ar_data->mask)){{$ar_data->mask}}@endif</td>

            </tr>
            <tr>
                <td>Endotrachael<br>Size</td>
                <td colspan='2'>@if(isset($ar_data->endotrachael_size)){{$ar_data->endotrachael_size}}@endif</td>
            </tr>
            <tr>
                <td>
                    <label for="endotrachael_type_nasal">Nasal</label>
                    @if(isset($ar_data->endotrachael_type) && ($ar_data->endotrachael_type ==1))
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>

                    <label for="endotrachael_type_blind">Blind</label>
                    @if(isset($ar_data->endotrachael_type) && ($ar_data->endotrachael_type ==2))
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
                <td>

                    <label for="endotrachael_type_direct">Direct</label>
                    @if(isset($ar_data->endotrachael_type) && ($ar_data->endotrachael_type ==3))
                    <i class="fa fa-check" aria-hidden="true"></i>
                    @endif
                </td>
            </tr>
            <tr>
                <td>Oral</td>
                <td colspan="2">
                    @if(isset($ar_data->oral) && ($ar_data->oral ==1))
                   Yes
                   @else
                   No
                    @endif
                </td>
            </tr>
            <tr>
                <td>Tracheostomy</td>
                <td colspan="2">
                    @if(isset($ar_data->tracheostomy) && ($ar_data->tracheostomy ==1))
                    Yes
                    @else
                    No
                     @endif
                </td>
            </tr>
            <tr>
                <td>Endobrononial</td>
                <td colspan="2">
                    @if(isset($ar_data->endobrononial) && ($ar_data->endobrononial)==1)
                    Yes
                    @else
                    No
                     @endif
                    </td>
            </tr>
            <tr>
                <td>Cuff</td>
                <td colspan="2">
                    @if(isset($ar_data->cuff) && ($ar_data->cuff ==1))
                    Yes
                    @else
                    No
                     @endif
                    </td>
            </tr>
            <tr>
                <td>Local Spray</td>
                <td colspan="2">
                        @if(isset($ar_data->local_spray) && ($ar_data->local_spray ==1))
                        Yes
                        @else
                        No
                         @endif
                        </td>
                </td>
            </tr>
            <tr>
                <td>Pack</td>
                <td colspan="2">
                @if(isset($ar_data->pack) && ($ar_data->pack) ==1)
                Yes
                @else
                No
                 @endif
                </td>
            </tr>
        </table>

        <table class="table table-bordered theadfix_wrapper" style="width: 100%;">
            <tr>
                <td>Estimated Blood Loss</td>
                <td>@if(isset($ar_data->estimated_blood_loss)){{$ar_data->estimated_blood_loss}}@endif</td>

            </tr>
            <tr>
                <td>Total Fluid Given In OR</td>
                <td>@if(isset($ar_data->total_fluid_given_in_or)){{$ar_data->total_fluid_given_in_or}}@endif</td>

            </tr>
            <tr>
                <td>Blood</td>
                <td>@if(isset($ar_data->recovery_blood)){{$ar_data->recovery_blood}}@endif</td>

            </tr>
            <tr>
                <td>Other</td>
                <td>@if(isset($ar_data->recovery_other)){{$ar_data->recovery_other}}@endif</td>

            </tr>
        </table>
        <table class="table table-bordered theadfix_wrapper" style="width: 100%;">
            <tr class="headerClass">
                <td colspan="2" style="color:white !important;" >MONITORS</td>
            </tr>

            <tr>
                <td>
                    @if(in_array('1',$monotor_data))
                        <i class="fa fa-check" aria-hidden="true"></i>
                    @endif

                    <label class="form-check-label" for="monitor_1">
                        NIBP
                    </label>
                </td>

                <td>
                    @if(in_array('2',$monotor_data))
                        <i class="fa fa-check" aria-hidden="true"></i>
                    @endif

                    <label class="form-check-label" for="monitor_2">
                        Spo2
                    </label>
                </td>
            </tr>

            <tr>
                <td>
                    @if(in_array('3',$monotor_data))
                        <i class="fa fa-check" aria-hidden="true"></i>
                    @endif

                    <label class="form-check-label" for="monitor_3">
                        HR
                    </label>
                </td>

                <td>
                    @if(in_array('4',$monotor_data))
                        <i class="fa fa-check" aria-hidden="true"></i>
                    @endif

                    <label class="form-check-label" for="monitor_4">
                        RR
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    @if(in_array('5',$monotor_data))
                        <i class="fa fa-check" aria-hidden="true"></i>
                    @endif

                    <label class="form-check-label" for="monitor_5">
                        Etco2
                    </label>
                </td>

                <td>
                    @if(in_array('6',$monotor_data))
                        <i class="fa fa-check" aria-hidden="true"></i>
                    @endif

                    <label class="form-check-label" for="monitor_6">
                        IBP
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <input class="form-check-input" type="checkbox" value="7" id="monitor_7">

                    <label class="form-check-label" for="monitor_7">
                        ECG
                    </label>
                </td>
            </tr>
            <tr>

                <td colspan="2">@if(isset($ar_data->monitor_description)){{$ar_data->monitor_description}}@endif</td>
            </tr>

        </table>
    </div>
</div>

