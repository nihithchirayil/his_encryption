<div class="theadscroll" style="position: relative; height: 450px;">
<div class="col-md-12 text-center " style="padding: 10px;"><b class="checklist_head_text">BEFORE PATIENT LEAVES OPERATING ROOM</b></div>
<div class="padding_sm"><b class="checklist_head_text">COUNTS</b></div>
<table id="result" class='table table-condensed table_sm table-bordered'  style="font-size: 12px;">
    <thead >
        <tr class="common_table_header">
        <th class="">Count</th>
        <th class="">Sponge</th>
        <th class="">Gauze</th>
        <th class="">Peanut Gauze</th>
        <th class="">Roller Gauze</th>
        <th class="">Vassel Loop</th>
        <th class="">Instrument</th>
        <th class="">Needle</th>
        <th class="">Circulatory Nurse</th>
        <th class="">Scrub Nurse</th>
        </tr>

    </thead>
    <tbody>
        <tr><td>Pre op</td>
        <td ><div class="col-md-12" id="prespo"><div class=""><input class="form-control" id="preSpong" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="preGauze" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="prePeanut_Gauze" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="preRoller_Gauze" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="preVassel_Loop" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="preInstrument" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="preNeedle" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="preCirculatory_Nurse" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="preScrub_Nurse" type="text" name="" value=""></div></div></td>

        <tr><td>Relief</td>
            <td id="prespo"><div class="col-md-12"><div class=""><input class="form-control" id="ReliSpong" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="ReliGauze" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="ReliPeanut_Gauze" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="ReliRoller_Gauze" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="ReliVassel_Loop" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="ReliInstrument" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="ReliNeedle" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="ReliCirculatory_Nurse" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="ReliScrub_Nurse" type="text" name="" value=""></div></div></td>
        </tr>
        <tr><td>AtClose</td>
            <td id="prespo"><div class="col-md-12"><div class=""><input class="form-control" id="AtclSpong" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="AtclGauze" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="AtclPeanut_Gauze" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="AtclRoller_Gauze" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="AtclVassel_Loop" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="AtclInstrument" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="AtclNeedle" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="AtclCirculatory_Nurse" type="text" name="" value=""></div></div></td>
            <td> <div class="col-md-12"><div class=""><input class="form-control" id="AtclScrub_Nurse" type="text" name="" value=""></div></div></td>
        </tr>
        <tr><td>Final</td>
            <td id="prespo"><div class="col-md-12"><div class=""><input class="form-control" id="FinalSpong" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="FinalGauze" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="FinalPeanut_Gauze" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="FinalRoller_Gauze" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="FinalVassel_Loop" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="FinalInstrument" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="FinalNeedle" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="FinalCirculatory_Nurse" type="text" name="" value=""></div></div></td>
        <td> <div class="col-md-12"><div class=""><input class="form-control" id="FinalScrub_Nurse" type="text" name="" value=""></div></div></td>
        </tr>

    </tbody>

</table>


<div class="col-md-12">
    <div class="col-md-2">
        <b>Count Correct :</b>
    </div>
    <div class="col-md-1">
    <div class="col-md-12">
        <div class="radio radio-success inline no-margin ">
            <input class="checkit" id="count_correct_yes" type="radio" name="count_correct" value=1>
            <label class="text-blue" for="count_correct_yes">
                Yes
            </label>
        </div>
    </div>
    </div>
    <div class="col-md-1">
    <div class="col-md-12">
        <div class="radio radio-success inline no-margin ">
            <input class="checkit" id="count_correct_no" type="radio" name="count_correct" value=2>
            <label class="text-blue" for="count_correct_no">
                No
            </label>
        </div>
    </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;">
    <div class="col-md-2">
    if No,surgeon informed :
    </div>
    <div class="col-md-1">
    <div class="col-md-12">
        <div class="radio radio-success inline no-margin ">
            <input class="checkit" id="surgeon_inform_yes" type="radio" name="surgeon_inform" value=1>
            <label class="text-blue" for="surgeon_inform_yes"> Yes</label>
        </div>
    </div>
    </div>
    <div class="col-md-1">
    <div class="col-md-12">
        <div class="radio radio-success inline no-margin ">
            <input class="checkit" id="surgeon_inform_no" type="radio" name="surgeon_inform" value=2>
            <label class="text-blue" for="surgeon_inform_no">
                No
            </label>
        </div>

    </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
        Time informed : <input
        type="text" id="sign_out_antibiotic_given_time" style="height: 17px !important;" class="form-control timepicker">
        </div>
</div>
<div class="col-md-12" style="margin-top: 10px;">
    Action taken/Remarks : <textarea id="action_taken_remarks" style="width: 100%; height: 50px !important; resize: none;" class="form-control"> </textarea>




</div>
<div class="col-md-12" style="margin-top: 10px;">
    Post Notes: <textarea id="sign_out_post_notes" style="width: 100%; height: 68px !important; resize: none;" class="form-control"> </textarea>




</div>
</div>

<div class="col-md-12" style="margin-top:10px;margin-bottom:20px;">
    <div class="col-md-6">

        Handover given By :{!! Form::select('signout_handover_given_by',$all_staff_ot_nurse,0, ['class' => 'form-control select2', 'id' =>'signout_handover_given_by','placeholder' =>'select', 'style' => 'color:#555555; padding:4px 12px;']) !!}
    </div>
    <div class="col-md-6">
        Handover taken By :  {!! Form::select('signout_handover_taken_by',$all_staff_ot_nurse,0, ['class' => 'form-control select2', 'id' =>'signout_handover_taken_by','placeholder' =>'select','style' => 'color:#555555; padding:4px 12px;']) !!}
    </div>
</div>

<div class="col-md-12" style="margin-top:10px;margin-bottom:20px;">
    <div class="col-md-6">
        Handover taken at :<input type="text" placeholder="Handover taken at" class="form-control timepicker" id="signout_handover_taken_at"/>
    </div>
</div>

<div class="col-md-12 " style="margin-top:5px;margin-bottom:5px;">
    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
        class="btn btn-danger pull-right">Close <i class="fa fa-times"></i></button>
    @if(in_array("OT Surgery Checklist SignOut", $groupArray))
    <button style="padding: 3px 3px" onclick="saveSurgerySignOut();" type="button" title="Save"
        class="btn btn-success pull-right btn_save_signout" id="save_checklistbtn">Save<i id="save_checklistspin"
            class="fa fa-save padding_sm"></i>
    </button>
    @endif
    {{-- @if(in_array("OT Approval Access", $groupArray)) --}}
    <button style="padding: 3px 3px" onclick="ApproveSurgeryChecklist(6);" type="button" title="Save"
        class="btn btn-primary pull-right btn_approve_signout" id="approve_checklistbtn">Final Approve<i id="save_checklistspin"
            class="fa fa-check padding_sm"></i>
    </button>
    {{-- @endif --}}
</div>

