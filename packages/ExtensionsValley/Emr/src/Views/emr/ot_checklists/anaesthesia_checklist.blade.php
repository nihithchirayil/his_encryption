<div class="col-md-12 no-padding">
    <button onclick="addAnsChekListNewColum();" type="button" class="btn btn-success pull-right"><i class="fa fa-plus"></i></button>
</div>
<div class="col-md-12 no-padding">
        <div class="table-responsive">
        <table id="anasthetia_checklist_table" class="table table-bordered theadfix_wrapper"
        style="font-size: 12px;min-width:1500px;">
        <tbody>
            <tr id="ans_1">
                <td class="common_td_rules" style="min-width: 250px !important;">Time</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center;min-width:150px !important;">
                        <input type="text" id="chk_time_{{$i}}" class="form-control timepicker" placeholder="" />
                    </td>
                @endfor
            </tr>


            <tr id="ans_2">
                <td class="common_td_rules" >Oxygen</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_oxygen_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor
            </tr>

            <tr id="ans_3">
                <td class="common_td_rules">Nitrous oxide</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_nitrous_oxide_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_4">
                <td class="common_td_rules">Halothane /Isoflurane</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_halothane_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_5">
                <td class="common_td_rules">SevofIurane/ Desflurane</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_sevofiurane_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_6">
                <td class="common_td_rules">Anaesthesia Drugs</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_anaesthesia_drugs_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_7">
                <td class="common_td_rules">1.</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_anaesthesia_drugs1_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_8">
                <td class="common_td_rules">2.</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_anaesthesia_drugs2_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_9">
                <td class="common_td_rules">3.</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_anaesthesia_drugs3_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_10">
                <td class="common_td_rules">4.</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_anaesthesia_drugs4_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_11">
                <td class="common_td_rules">5.</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_anaesthesia_drugs5_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>

            <tr id="ans_12">
                <td class="common_td_rules">Antibiotics</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">

                        {!! Form::select('chk_antibiotics1_'.$i, $ot_antibiotics,null, ['class' => 'form-control select2','placeholder'=>'', 'id' =>'chk_antibiotics1_'.$i, 'style' => 'color:#555555; padding:4px 12px;']) !!}

                    </td>
                @endfor

            </tr>

            <tr id="ans_13">
                <td class="common_td_rules">Fluids 1.</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_fluids1_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor
            </tr>
            <tr id="ans_14">
                <td class="common_td_rules">Fluids 2.</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_fluids2_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor
            </tr>
            <tr id="ans_15">
                <td class="common_td_rules">Fluids 3.</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_fluids3_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor
            </tr>
            <tr id="ans_16">
                <td class="common_td_rules">Fluids 4.</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_fluids4_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor
            </tr>

            <tr id="ans_17">
                <td class="common_td_rules">1.Crystalloids .</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_crystalloids_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_18">
                <td class="common_td_rules">2.Colloids.</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_colloids_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_19">
                <td class="common_td_rules">Invasive Lines 1
                </td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_invasive_lines_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor
            </tr>
            <tr id="ans_20">
                <td class="common_td_rules">Invasive Lines 2
                </td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_invasive_lines2_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor
            </tr>
            <tr id="ans_21">
                <td title="Blood and Blood Products" class="common_td_rules">Blood and Blood Products</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_blood_and_blood_products_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr  id="ans_22">
                <td class="common_td_rules">Monitoring (Sys/Dia)</td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_monitoring_sys_{{$i}}" class="form-control" style="width:55px !important;float:left;" placeholder="" />/
                        <input type="text" id="chk_monitoring_dia_{{$i}}" class="form-control" style="width:55px !important;float:right;" placeholder="" />
                    </td>
                @endfor
            </tr>

            <tr id="ans_23">
                <td title="Other Monitors (ET Co2 etc)" class="common_td_rules">Other Monitors (ET Co2 etc) </td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_other_monitors_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor
            </tr>
            <tr id="ans_24">
                <td class="common_td_rules">1. <input type="text" id="chk_other_monitors_text_1" class="form-control" placeholder="" style="float:right;width:210px !important;" />
                </td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_other_monitors1_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_25">
                <td class="common_td_rules">2. <input type="text" id="chk_other_monitors_text_2" class="form-control" placeholder=""  style="float:right;width:210px !important;" />
                </td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_other_monitors2_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_26">
                <td class="common_td_rules">3. <input type="text" id="chk_other_monitors_text_3" class="form-control" placeholder=""  style="float:right;width:210px !important;"  />
                </td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_other_monitors3_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_27">
                <td class="common_td_rules">Urine Output.
                </td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_urine_output_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_28">
                <td class="common_td_rules">Blood Loss.
                </td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_blood_loss_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_29">
                <td class="common_td_rules">Total IV Fliud Given.
                </td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_total_iv_fliud_given_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>
            <tr id="ans_30">
                <td class="common_td_rules">Reversal.
                </td>
                @for($i=1;$i<=10;$i++)
                    <td class="common_td_rules" style="text-align:center">
                        <input type="text" id="chk_reversal_{{$i}}" class="form-control" placeholder="" />
                    </td>
                @endfor

            </tr>

            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        <label>Adequate respiratory efforts</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="adequate_resp_efforts_yes" type="radio" name="adequate_resp_efforts" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="adequate_resp_efforts_no" type="radio" name="adequate_resp_efforts" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
    </div>
    <div class="col-md-12">
        <label>Eye Opening</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="eye_opening_yes" type="radio" name="eye_opening" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="eye_opening_no" type="radio" name="eye_opening" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
    </div>
    <div class="col-md-12">
        <label>Obeying commands</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="obeying_commands_yes" type="radio" name="obeying_commands" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="obeying_commands_no" type="radio" name="obeying_commands" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
    </div>
    <div class="col-md-12">
        <label>Sustained head lift more than 3 sec</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="sustained_head_lift_yes" type="radio" name="sustained_head_lift" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="sustained_head_lift_no" type="radio" name="sustained_head_lift" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
    </div>
    <div class="col-md-12">
        <label>Tongue protrusion more than 5s</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="tongue_protrusion_yes" type="radio" name="tongue_protrusion" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="tongue_protrusion_no" type="radio" name="tongue_protrusion" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
    </div>
    <div class="col-md-12">
        <label>Airway reflexes present</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="airway_reflexes_yes" type="radio" name="airway_reflexes" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="airway_reflexes_no" type="radio" name="airway_reflexes" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
    </div>
    <div class="col-md-12">
        <label>After giving thorough ET & Oropharyngeal Suctioning, patient Extubated</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="after_giving_thorough_yes" type="radio" name="after_giving_thorough" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="after_giving_thorough_no" type="radio" name="after_giving_thorough" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
    </div>
    <div class="col-md-12">
        <table>
            <tr>
                <td colspan="2">Post extubation, vitals</td>
            </tr>
            <tr>
                <td><label>PR</label></td>
                <td><input type="text" style="width:100px;margin-bottom:3px;" class="form-control" id="post_extubation_pr"/></td>
            </tr>
            <tr>
                <td><label>RR</label></td>
                <td><input type="text" style="width:100px;margin-bottom:3px;" class="form-control" id="post_extubation_rr"/></td>
            </tr>
            <tr>
                <td><label>Spo2</label></td>
                <td><input type="text" style="width:100px;margin-bottom:3px;" class="form-control" id="post_extubation_spo2"/></td>
            </tr>
            <tr>
                <td><label>BP</label></td>
                <td><input type="text" style="width:100px;margin-bottom:3px;" class="form-control" id="post_extubation_bp"/></td>
            </tr>
        </table>
        <label></label>
    </div>
    <div class="col-md-12">
        <label><b> Time of Completion of Procedure:</b></label>
        <input type="text" id="chk_time_of_completion" class="form-control datetimepicker" placeholder="" style="width:150px;"/>
    </div>
    <div class="col-md-12">
        <label><b> Time of Completion of Procedure:</b></label>
        <input type="text" id="chk_time_of_completion" class="form-control datetimepicker" placeholder="" style="width:150px;"/>
    </div>
    <div class="col-md-12">
        <label><b>Time of Extubation:</b></label>
    <input type="text" id="chk_time_of_extubation" class="form-control datetimepicker" placeholder="" style="width:150px;"/>
    </div>

</div>
Charting Guidelines(Signs):BP-x,HR- ,Sao2 -% ,EtCo2 -%Kpa ,Intubation-↓,Extubation-↑,Cardiac Output –CO ,Pulmonary Artery Wedge
Pressure - PAWP

<div class="col-md-12 " style="margin-top:5px;margin-bottom:5px;">
    <button style="padding: 3px 3px" onclick="saveAnaesthetiaChecklist();" type="button" title="Save"
        class="btn btn-success pull-right" id="save_checklistbtn">Save<i id="save_checklistspin"
            class="fa fa-save padding_sm"></i>
    </button>
    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
        class="btn btn-danger pull-right">Close <i class="fa fa-times"></i></button>
</div>

