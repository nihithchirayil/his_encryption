<style>
    .chk_chart_codes_1{
        background-color: #710202;
        accent-color: #710202;
    }
    .chk_chart_codes_2{
        background-color: #5719e7;
        accent-color: #5719e7;
    }
    .chk_chart_codes_3{
        background-color: #016d0c;
        accent-color: #016d0c;
    }
    .chk_chart_codes_4{
        background-color: #ff4d00;
        accent-color: #ff4d00;
    }
    .chk_chart_codes_5{
        background-color: #ff42c6;
        accent-color: #ff42c6;
    }
    .chk_chart_codes_6{
        background-color: #7ff78b;
        accent-color: #7ff78b;
    }

    .form-control{
        box-shadow: none !important;
    }
    .headerClass{
        background: cornflowerblue !important;
        color:white;
    }
    .modal-open {
        overflow: auto;
    }
    .radio-success {
        float: left;
        clear: right;
    }
    .text-blue {
        margin-right: 10px;
        float: left;
        clear: right;
        font-size: 12px !important;
    }
    /* .time_value_col{
        width: 125px !important;
    } */
    span[title]:hover::after {
        content: attr(title);
        position: absolute;
        top: -100%;
        left: 0;
        font-display: 23px;
    }

</style>
<input type="hidden" id="ar_surgery_head_id" value="{{$head_id}}"/>
<div class="col-md-12" style="margin-top:10px;">
    <input type="hidden" id="default_time_duration" value="{{$default_time_duration}}"/>
    <table class="table table-bordered theadfix_wrapper">
        <tr>
            <td colspan="2">Patient : <label style="color:#5719e7;font-weight:600;float:right;"> {{$surgery_data[0]->patient_name}}</label></td>
            <td>Age/Gender : <label style="color:#5719e7;font-weight:600;float:right;">{{$surgery_data[0]->age}}/{{$surgery_data[0]->gender}}</label></td>
            <td>Hospital No : <label style="color:#5719e7;font-weight:600;float:right;">{{$hospital_no}}</label></td>
        </tr>
        <tr>
            <td>Pre-Op Diaganosis : <input type="text" id="pre_op_diaganosis" style="color:#5719e7;font-weight:600; width:580px; float:right;border:1px solid #d8d8d8;" class="form_control" value="{{$surgery_data[0]->diaganosis}}"/></td>
            <td>Operation Done : <input type="text" id="operation_done" style="color:#5719e7;font-weight:600;border:1px solid #d8d8d8;width:165px;" class="form_control" value=""/></td>
            <td colspan="2">Surgeon : <label style="color:#5719e7;font-weight:600;float:right;">{{$surgery_data[0]->surgeon}}</label></td>
        </tr>
        <tr>
            <td>
                {{-- PreMedication : <input type="text" id="premedication" style="color:#5719e7;font-weight:600;width:630px;border:1px solid #d8d8d8;" class="form_control" value=""/>
             </td> --}}
            <td>Date&Time :
                <input type="text" id="ar_scheduled_at" class="form-control datetimepicker" placeholder="" style="color:#5719e7;font-weight:600;width:160px;float:right;" @if($surgery_data[0]->scheduled_at !='') value="{{date('M-d-Y h:i A',strtotime($surgery_data[0]->scheduled_at))}}" @else value="" @endif/>
            </td>
            <td colspan="2">ASA Grade : <input type="text" id="asa_grade" style="color:#5719e7;font-weight:600;border:1px solid #d8d8d8;" class="form_control" value=""/></td>
        </tr>
        <tr>
            <td colspan="2">Anaesthetist : <label style="color:#5719e7;font-weight:600;float:right;">{{$surgery_data[0]->anaesthetist}}</label></td>
            <td colspan="2">Technique : <input type="text" id="ans_technique" style="color:#5719e7;font-weight:600;border:1px solid #d8d8d8;margin-left:5px;" class="form_control" value="{{$surgery_data[0]->anaesthesia}}"/></td>
        </tr>
    </table>
</div>

<div class="col-md-12">
    <div class="col-md-9 no-padding table-responsive">
        <table class="table table-bordered theadfix_wrapper table-fixed" id="ans_record_table">
            <tbody id="ans_record_table_tbody">
                <tr class="headerClass time_row">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="min-width: 250px !important;color:white !important;">
                        Time
                        <button onclick="addNewTimeRow();" type="button" class="btn btn-sm bg-orange pull-right"><i class="fa fa-plus"></i></button>
                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td class="common_td_rules time_value_col" style="text-align:center;min-width:67px !important;">
                            <input type="text" id="ar_time_{{$i}}" name="ar_time[]" class="form-control timepicker ar_time" placeholder=""  style="border:none;background-color:cornflowerblue;color:white;curser:pointer;"/>
                        </td>
                    @endfor
                </tr>
                <tr class="o2_row">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="min-width: 250px !important;">
                        <label style="width:30%;float:left;clar:right;"> 0<sub>2</sub> </label>
                        <input style="width:60%;float:left;clar:right;" type="text" class="form-control" name="o2_ratio[]"/>
                        <i style="width:10%;float:right;text-align:right;margin-top:5px;" class="fa fa-plus" onclick="add_new_o2_row();"></i>
                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td style="text-align:center;" class="ans_chk">
                            <input type="checkbox" name="chk_o2_ratio[]"/>
                        </td>
                    @endfor
                </tr>
                <tr class="n2o_row">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="min-width: 250px !important;">

                        <label style="width:30%;float:left;clar:right;">N<sub>2</sub>o</label>
                        <input style="width:60%;float:left;clar:right;" type="text" class="form-control" name="n2o_ratio[]"/>
                        <i style="width:10%;float:right;text-align:right;margin-top:5px;" class="fa fa-plus" onclick="add_new_n2o_row();"></i>

                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td style="text-align:center;" class="ans_chk">
                            <input type="checkbox" name="chk_n2o_ratio[]"/>
                        </td>
                    @endfor
                </tr>
                <tr class="air_row">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="min-width: 250px !important;">

                        <label style="width:30%;float:left;clar:right;">Air</label>
                        <input style="width:60%;float:left;clar:right;" type="text" class="form-control" name="air_ratio[]"/>
                        <i style="width:10%;float:right;text-align:right;margin-top:5px;" class="fa fa-plus" onclick="add_new_air_row();"></i>

                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td style="text-align:center;" class="ans_chk">
                            <input type="checkbox" name="chk_air_ratio[]"/>
                        </td>
                    @endfor
                </tr>

                <tr class="induction_row">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="min-width: 250px !important;">

                        <label style="width:30%;float:left;clar:right;">Induction</label>
                        <input style="width:60%;float:left;clar:right;" type="text" class="form-control" name="induction_ratio[]"/>
                        <i style="width:10%;float:right;text-align:right;margin-top:5px;" class="fa fa-plus" onclick="add_new_induction_label_row();"></i>
                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td style="text-align:center;" class="ans_chk">
                            <input type="checkbox" name="chk_induction_ratio[]"/>
                        </td>
                    @endfor
                </tr>

                <tr class="inhalation_row">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="min-width: 250px !important;">
                        <label style="width:30%;float:left;clar:right;">Inhalation</label>
                        <input style="width:60%;float:left;clar:right;" type="text" class="form-control" name="inhalation_ratio[]"/>
                        <i style="width:10%;float:right;text-align:right;margin-top:5px;" class="fa fa-plus" onclick="add_new_inhalation_label_row();"></i>
                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td style="text-align:center;" class="ans_chk">
                            <input type="checkbox" name="chk_inhalation_ratio[]"/>
                        </td>
                    @endfor
                </tr>

                <tr class="relaxant_row">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="min-width: 250px !important;">
                        <label style="width:30%;float:left;clar:right;">Relaxant</label>
                        <input style="width:60%;float:left;clar:right;" type="text" class="form-control" name="relaxant_ratio[]"/>
                        <i style="width:10%;float:right;text-align:right;margin-top:5px;" class="fa fa-plus" onclick="add_new_relaxant_label_row();"></i>
                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td style="text-align:center;" class="ans_chk">
                            <input type="checkbox" name="chk_relaxant_ratio[]"/>
                        </td>
                    @endfor
                </tr>
                <tr class="blood_ivf_row">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="min-width: 250px !important;">
                        <label style="width:30%;float:left;clar:right;">Blood/IVF</label>
                        <input style="width:60%;float:left;clar:right;" type="text" class="form-control" name="blood_ivf_ratio[]"/>
                        <i style="width:10%;float:right;text-align:right;margin-top:5px;" class="fa fa-plus" onclick="add_new_blood_ivf_row();"></i>
                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td style="text-align:center;" class="ans_chk">
                            <input type="checkbox" name="chk_blood_ivf_ratio[]"/>
                        </td>
                    @endfor
                </tr>

                <tr class="ans_others_row">
                    <td colspan="2" class="col-xs-2 common_td_rules" style="min-width: 250px !important;">
                        <label style="width:30%;float:left;clar:right;">Others</label>
                        <input style="width:60%;float:left;clar:right;" type="text" class="form-control" name="ans_others_ratio[]"/>
                        <i style="width:10%;float:right;text-align:right;margin-top:5px;" class="fa fa-plus" onclick="add_new_ans_others_row();"></i>
                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td style="text-align:center;" class="ans_chk">
                            <input type="checkbox" name="chk_ans_others_ratio[]"/>
                        </td>
                    @endfor
                </tr>

                <tr class="200_row">
                    <td rowspan="20">
                        <b>Chart Code</b><br><br>
                        <div class="col-md-12 no-padding">
                            <div class="col-md-6">Anaes</div>
                            <div class="col-md-6">
                                <span class="badge chk_chart_codes_1">&nbsp;</span>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="col-md-6">Operation</div>
                            <div class="col-md-6">
                                <span class="badge chk_chart_codes_2">&nbsp;</span>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="col-md-6">BP</div>
                            <div class="col-md-6">
                                <span class="badge chk_chart_codes_3">&nbsp;</span>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="col-md-6">Pulse</div>
                            <div class="col-md-6">
                                <span class="badge chk_chart_codes_4">&nbsp;</span>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="col-md-6">ETCO2</div>
                            <div class="col-md-6">
                                <span class="badge chk_chart_codes_5">&nbsp;</span>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="col-md-6">SPO2</div>
                            <div class="col-md-6">
                                <span class="badge chk_chart_codes_6">&nbsp;</span>
                            </div>
                        </div>
                    </td>
                    <td style="text-align: center;">200</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="191" data-max-range="200">
                    </td>
                    @endfor
                </tr>
                <tr class="190_row">
                    <td style="text-align: center;">190</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="181" data-max-range="190"></td>
                    @endfor
                </tr>
                <tr class="180_row">
                    <td style="text-align: center;">180</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="171" data-max-range="180"></td>
                    @endfor
                </tr>
                <tr class="170_row">
                    <td style="text-align: center;">170</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="161" data-max-range="170"></td>
                    @endfor
                </tr>
                <tr class="160_row">
                    <td style="text-align: center;">160</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="151" data-max-range="160"></td>
                    @endfor
                </tr>
                <tr class="150_row">
                    <td style="text-align: center;">150</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="141" data-max-range="150"></td>
                    @endfor
                </tr>
                <tr class="140_row">
                    <td style="text-align: center;">140</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="131" data-max-range="140"></td>
                    @endfor
                </tr>
                <tr class="130_row">
                    <td style="text-align: center;">130</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="121" data-max-range="130"></td>
                    @endfor
                </tr>
                <tr class="120_row">
                    <td style="text-align: center;">120</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="111" data-max-range="120"></td>
                    @endfor
                </tr>
                <tr class="110_row">
                    <td style="text-align: center;">110</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="101" data-max-range="110"></td>
                    @endfor
                </tr>
                <tr class="100_row">
                    <td style="text-align: center;">100</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="91" data-max-range="100"></td>
                    @endfor
                </tr>
                <tr class="90_row">
                    <td style="text-align: center;">90</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="81" data-max-range="90"></td>
                    @endfor
                </tr>
                <tr class="80_row">
                    <td style="text-align: center;">80</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="71" data-max-range="80"></td>
                    @endfor
                </tr>
                <tr class="70_row">
                    <td style="text-align: center;">70</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="61" data-max-range="70"></td>
                    @endfor
                </tr>
                <tr class="60_row">
                    <td style="text-align: center;">60</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="51" data-max-range="60"></td>
                    @endfor
                </tr>
                <tr class="50_row">
                    <td style="text-align: center;">50</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="41" data-max-range="50"></td>
                    @endfor
                </tr>
                <tr class="40_row">
                    <td style="text-align: center;">40</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="31" data-max-range="40"></td>
                    @endfor
                </tr>
                <tr class="30_row">
                    <td style="text-align: center;">30</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="21" data-max-range="30"></td>
                    @endfor
                </tr>
                <tr class="20_row">
                    <td style="text-align: center;">20</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="11" data-max-range="20"></td>
                    @endfor
                </tr>
                <tr class="10_row">
                    <td style="text-align: center;">10</td>
                    @for($i=1;$i<=10;$i++)
                        <td class="chart_code_entry" data-value-chart-code="" data-chart-code="" style="text-align:center;" data-colum-count="{{$i}}" data-min-range="0" data-max-range="10"></td>
                    @endfor
                </tr>
                <tr class="spontaneous_row">
                    <td colspan="2" class="common_td_rules">
                        Spontaneous
                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td style="text-align:center;" class="ans_chk">
                            <input type="checkbox" name="chk_spontaneous[]"/>
                        </td>
                    @endfor
                </tr>
                <tr class="assisted_row">
                    <td colspan="2" class="common_td_rules">
                        Assisted
                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td style="text-align:center;" class="ans_chk">
                            <input type="checkbox" name="chk_assisted[]" />
                        </td>
                    @endfor
                </tr>
                <tr class="controlled_row">
                    <td colspan="2" class="common_td_rules">
                        Controlled
                    </td>
                    @for($i=1;$i<=10;$i++)
                        <td style="text-align:center;" class="ans_chk">
                            <input type="checkbox" name="chk_controlled[]" />
                        </td>
                    @endfor
                </tr>
            </tbody>
        </table>
        <div class="col-md-12">
            <label>Position</label>
            <input type="text" class="form-control" id="ans_position" name="ans_position"/>
        </div>
        <div class="col-md-6">
            <label>Anaesthesia Start Time:</label>
            <input type="text" class="form-control datetimepicker" id="ans_start_time" name="ans_start_time"/>
        </div>
        <div class="col-md-6">
            <label>Anaesthesia End Time:</label>
            <input type="text" class="form-control datetimepicker" id="ans_end_time" name="ans_end_time"/>
        </div>
        <div class="col-md-6">
            <label>Surgery Start Time</label>
            <input type="text" class="form-control datetimepicker" id="surgery_start_time" name="surgery_start_time"/>
        </div>
        <div class="col-md-6">
            <label>Surgery End Time</label>
            <input type="text" class="form-control datetimepicker" id="surgery_end_time" name="surgery_end_time"/>
        </div>

        <div class="col-md-12" style="margin-top:30px;">
            <label style="float: left;clear:right;margin-right: 10px;">Adequate respiratory efforts</label>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="adequate_resp_efforts_yes" type="radio" name="adequate_resp_efforts" value=1>
                    <label class="text-blue">
                        Yes
                    </label>
                </div>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="adequate_resp_efforts_no" type="radio" name="adequate_resp_efforts" value=2>
                    <label class="text-blue">
                        No
                    </label>
                </div>
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">Eye Opening</label>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="eye_opening_yes" type="radio" name="eye_opening" value=1>
                    <label class="text-blue">
                        Yes
                    </label>
                </div>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="eye_opening_no" type="radio" name="eye_opening" value=2>
                    <label class="text-blue">
                        No
                    </label>
                </div>
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">Obeying commands</label>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="obeying_commands_yes" type="radio" name="obeying_commands" value=1>
                    <label class="text-blue">
                        Yes
                    </label>
                </div>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="obeying_commands_no" type="radio" name="obeying_commands" value=2>
                    <label class="text-blue">
                        No
                    </label>
                </div>
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">Sustained head lift more than 3 sec</label>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="sustained_head_lift_yes" type="radio" name="sustained_head_lift" value=1>
                    <label class="text-blue">
                        Yes
                    </label>
                </div>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="sustained_head_lift_no" type="radio" name="sustained_head_lift" value=2>
                    <label class="text-blue">
                        No
                    </label>
                </div>
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">Tongue protrusion more than 5s</label>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="tongue_protrusion_yes" type="radio" name="tongue_protrusion" value=1>
                    <label class="text-blue">
                        Yes
                    </label>
                </div>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="tongue_protrusion_no" type="radio" name="tongue_protrusion" value=2>
                    <label class="text-blue">
                        No
                    </label>
                </div>
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">Airway reflexes present</label>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="airway_reflexes_yes" type="radio" name="airway_reflexes" value=1>
                    <label class="text-blue">
                        Yes
                    </label>
                </div>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="airway_reflexes_no" type="radio" name="airway_reflexes" value=2>
                    <label class="text-blue">
                        No
                    </label>
                </div>
        </div>
        <div class="col-md-12">
            <label style="float: left;clear:right;margin-right: 10px;">After giving thorough ET & Oropharyngeal Suctioning, patient Extubated</label>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="after_giving_thorough_yes" type="radio" name="after_giving_thorough" value=1>
                    <label class="text-blue">
                        Yes
                    </label>
                </div>
                <div class="radio radio-success inline no-margin ">
                    <input class="checkit" id="after_giving_thorough_no" type="radio" name="after_giving_thorough" value=2>
                    <label class="text-blue">
                        No
                    </label>
                </div>
        </div>
        <div class="col-md-12">
            {{-- <table>
                <tr>
                    <td colspan="2">Post extubation, vitals</td>
                </tr>
                <tr>
                    <td><label>PR</label></td>
                    <td><input type="text" style="width:100px;margin-bottom:3px;" class="form-control" id="post_extubation_pr"/></td>
                </tr>
                <tr>
                    <td><label>RR</label></td>
                    <td><input type="text" style="width:100px;margin-bottom:3px;" class="form-control" id="post_extubation_rr"/></td>
                </tr>
                <tr>
                    <td><label>Spo2</label></td>
                    <td><input type="text" style="width:100px;margin-bottom:3px;" class="form-control" id="post_extubation_spo2"/></td>
                </tr>
                <tr>
                    <td><label>BP</label></td>
                    <td><input type="text" style="width:100px;margin-bottom:3px;" class="form-control" id="post_extubation_bp"/></td>
                </tr>
            </table> --}}
            <label></label>
        </div>
        <div class="col-md-12">
            <label><b> Time of Completion of Procedure:</b></label>
            <input type="text" id="chk_time_of_completion" class="form-control datetimepicker" placeholder="" style="width:150px;"/>
        </div>

        <div class="col-md-12">
            <label><b>Time of Extubation:</b></label>
            <input type="text" id="chk_time_of_extubation" class="form-control datetimepicker" placeholder="" style="width:150px;"/>
        </div>

    </div>
    <div class="col-md-3 no-padding">
        <table class="table table-bordered theadfix_wrapper">
            <tr class="headerClass">
                <td style="height:38px;" colspan="3">PREMEDICATION</td>
            </tr>
            <tr>
                <td>Too Depressed</td>
                <td>
                    <input type="radio" name="too_depressed" id="too_depressed_yes" value="1"/>
                    <label for="too_depressed_yes"> Yes</label>
                </td>
                <td>
                    <input type="radio" name="too_depressed" id="too_depressed_no" value="0"/>
                    <label for="too_depressed_no"> No</label>
                </td>
            </tr>

            <tr>
                <td>Worn off</td>
                <td>
                    <input type="radio" name="worn_off" id="worn_off_yes" value="1"/>
                    <label for="worn_off_yes"> Yes</label>
                </td>
                <td>
                    <input type="radio" name="worn_off" id="worn_off_no" value="0"/>
                    <label for="worn_off_no"> No</label>
                </td>
            </tr>

            <tr>
                <td>Apprehensive</td>
                <td>
                    <input type="radio" name="pprehensive" id="pprehensive_yes" value="1"/>
                    <label for="pprehensive_yes"> Yes</label>
                </td>
                <td>
                    <input type="radio" name="pprehensive" id="pprehensive_no" value="0"/>
                    <label for="pprehensive_no"> No</label>
                </td>
            </tr>
            <tr>
                <td>Inadequate</td>
                <td>
                    <input type="radio" name="inadequate" id="inadequate_yes" value="1"/>
                    <label for="inadequate_yes"> Yes</label>
                </td>
                <td>
                    <input type="radio" name="inadequate" id="inadequate_no" value="0"/>
                    <label for="inadequate_no"> No</label>
                </td>
            </tr>
        </table>

        <table class="table table-bordered theadfix_wrapper">
            <tr class="headerClass">
                <td colspan="6">INDUCTION</td>
            </tr>
            <tr>
                <td>Excitement</td>
                <td>
                    <input type="radio" name="excitement" id="excitement_0" value="0"/>
                    <label for="excitement_0">0</label>
                </td>
                <td>
                    <input type="radio" name="excitement" id="excitement_1" value="1"/>
                    <label for="excitement_1">1</label>
                </td>
                <td>
                    <input type="radio" name="excitement" id="excitement_2" value="2"/>
                    <label for="excitement_2">2</label>
                </td>
                <td>
                    <input type="radio" name="excitement" id="excitement_3" value="3"/>
                    <label for="excitement_3">3</label>
                </td>
                <td>
                    <input type="radio" name="excitement" id="excitement_4" value="4"/>
                    <label for="excitement_4">4</label>
                </td>
            </tr>

            <tr>
                <td>Laryngospasm</td>
                <td>
                    <input type="radio" name="laryngospasm" id="laryngospasm_0" value="0"/>
                    <label for="laryngospasm_0">0</label>
                </td>
                <td>
                    <input type="radio" name="laryngospasm" id="laryngospasm_1" value="1"/>
                    <label for="laryngospasm_1">1</label>
                </td>
                <td>
                    <input type="radio" name="laryngospasm" id="laryngospasm_2" value="2"/>
                    <label for="laryngospasm_2">2</label>
                </td>
                <td>
                    <input type="radio" name="laryngospasm" id="laryngospasm_3" value="3"/>
                    <label for="laryngospasm_3">3</label>
                </td>
                <td>
                    <input type="radio" name="laryngospasm" id="laryngospasm_4" value="4"/>
                    <label for="laryngospasm_4">4</label>
                </td>
            </tr>

            <tr>
                <td>Vomiting</td>
                <td>
                    <input type="radio" name="vomiting" id="vomiting_0" value="0"/>
                    <label for="vomiting_0">0</label>
                </td>
                <td>
                    <input type="radio" name="vomiting" id="vomiting_1" value="1"/>
                    <label for="vomiting_1">1</label>
                </td>
                <td>
                    <input type="radio" name="vomiting" id="vomiting_2" value="2"/>
                    <label for="vomiting_2">2</label>
                </td>
                <td>
                    <input type="radio" name="vomiting" id="vomiting_3" value="3"/>
                    <label for="vomiting_3">3</label>
                </td>
                <td>
                    <input type="radio" name="vomiting" id="vomiting_4" value="4"/>
                    <label for="vomiting_4">4</label>
                </td>
            </tr>

            <tr>
                <td>Cyanosis</td>
                <td>
                    <input type="radio" name="cyanosis" id="cyanosis_0" value="0"/>
                    <label for="cyanosis_0">0</label>
                </td>
                <td>
                    <input type="radio" name="cyanosis" id="cyanosis_1" value="1"/>
                    <label for="cyanosis_1">1</label>
                </td>
                <td>
                    <input type="radio" name="cyanosis" id="cyanosis_2" value="2"/>
                    <label for="cyanosis_2">2</label>
                </td>
                <td>
                    <input type="radio" name="cyanosis" id="cyanosis_3" value="3"/>
                    <label for="cyanosis_3">3</label>
                </td>
                <td>
                    <input type="radio" name="cyanosis" id="cyanosis_4" value="4"/>
                    <label for="cyanosis_4">4</label>
                </td>
            </tr>
        </table>

        <table class="table table-bordered theadfix_wrapper">
            <tr class="headerClass">
                <td colspan="6">MAINTENANCE</td>
            </tr>
            <tr>
                <td>Excitement</td>
                <td style="width:40px !important;">
                    <input type="radio" name="maintenance_excitement" id="maintenance_excitement_0" value="0"/>
                    <label for="maintenance_excitement_0">0</label>
                </td>
                <td style="width:40px !important;">
                    <input type="radio" name="maintenance_excitement" id="maintenance_excitement_1" value="1"/>
                    <label for="maintenance_excitement_1">1</label>
                </td>
                <td style="width:40px !important;">
                    <input type="radio" name="maintenance_excitement" id="maintenance_excitement_2" value="2"/>
                    <label for="maintenance_excitement_2">2</label>
                </td>
                <td style="width:40px !important;">
                    <input type="radio" name="maintenance_excitement" id="maintenance_excitement_3" value="3"/>
                    <label for="maintenance_excitement_3">3</label>
                </td>
                <td style="width:40px !important;">
                    <input type="radio" name="maintenance_excitement" id="maintenance_excitement_4" value="4"/>
                    <label for="maintenance_excitement_4">4</label>
                </td>
            </tr>
            <tr>
                <td>Laryngospasm</td>
                <td>
                    <input type="radio" name="maintenance_laryngospasm" id="maintenance_laryngospasm_0" value="0"/>
                    <label for="maintenance_laryngospasm_0">0</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_laryngospasm" id="maintenance_laryngospasm_1" value="1"/>
                    <label for="maintenance_laryngospasm_1">1</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_laryngospasm" id="maintenance_laryngospasm_2" value="2"/>
                    <label for="maintenance_laryngospasm_2">2</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_laryngospasm" id="maintenance_laryngospasm_3" value="3"/>
                    <label for="maintenance_laryngospasm_3">3</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_laryngospasm" id="maintenance_laryngospasm_4" value="4"/>
                    <label for="maintenance_laryngospasm_4">4</label>
                </td>
            </tr>

            <tr>
                <td>Sweating</td>
                <td>
                    <input type="radio" name="maintenance_sweating" id="maintenance_sweating_0" value="0"/>
                    <label for="maintenance_sweating_0">0</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_sweating" id="maintenance_sweating_1" value="1"/>
                    <label for="maintenance_sweating_1">1</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_sweating" id="maintenance_sweating_2" value="2"/>
                    <label for="maintenance_sweating_2">2</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_sweating" id="maintenance_sweating_3" value="3"/>
                    <label for="maintenance_sweating_3">3</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_sweating" id="maintenance_sweating_4" value="4"/>
                    <label for="maintenance_sweating_4">4</label>
                </td>
            </tr>

            <tr>
                <td>Mucous</td>
                <td>
                    <input type="radio" name="maintenance_mucous" id="maintenance_mucous_0" value="0"/>
                    <label for="maintenance_mucous_0">0</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_mucous" id="maintenance_mucous_1" value="1"/>
                    <label for="maintenance_mucous_1">1</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_mucous" id="maintenance_mucous_2" value="2"/>
                    <label for="maintenance_mucous_2">2</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_mucous" id="maintenance_mucous_3" value="3"/>
                    <label for="maintenance_mucous_3">3</label>
                </td>
                <td>
                    <input type="radio" name="maintenance_mucous" id="maintenance_mucous_4" value="4"/>
                    <label for="maintenance_mucous_4">4</label>
                </td>
            </tr>
            <tr>
                <td colspan="3">Mask</td>
                <td colspan="3">
                    <input class="form-control" type="text" name="mask" id="mask"/>
                </td>
            </tr>
            <tr>
                <td colspan="3">Endotrachael<br>Size</td>
                <td colspan="3">
                    <input class="form-control" type="text" name="endotrachael_size" id="endotrachael_size"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="radio" name="endotrachael_type" id="endotrachael_type_nasal" value="1"/>
                    <label for="endotrachael_type_nasal">Nasal</label>
                </td>
                <td colspan="2">
                    <input type="radio" name="endotrachael_type" id="endotrachael_type_blind" value="2"/>
                    <label for="endotrachael_type_blind">Blind</label>
                </td>
                <td colspan="2">
                    <input type="radio" name="endotrachael_type" id="endotrachael_type_direct" value="3"/>
                    <label for="endotrachael_type_direct">Direct</label>
                </td>
            </tr>
            <tr>
                <td colspan="2">Oral</td>
                <td colspan="2">
                    <input type="radio" name="oral" id="oral_yes" value="1"/>
                    <label for="oral_yes"> Yes</label>
                </td>
                <td colspan="2">
                    <input type="radio" name="oral" id="oral_no" value="0"/>
                    <label for="oral_no"> No</label>
                </td></td>
            </tr>
            <tr>
                <td colspan="2">Tracheostomy</td>
                <td colspan="2">
                    <input type="radio" name="tracheostomy" id="tracheostomy_yes" value="1"/>
                    <label for="tracheostomy_yes"> Yes</label>
                </td>
                <td colspan="2">
                    <input type="radio" name="tracheostomy" id="tracheostomy_no" value="0"/>
                    <label for="tracheostomy_no"> No</label>
                </td></td>
            </tr>
            <tr>
                <td colspan="2">Endobrononial</td>
                <td colspan="2">
                    <input type="radio" name="endobrononial" id="endobrononial_yes" value="1"/>
                    <label for="endobrononial_yes"> Yes</label>
                </td>
                <td colspan="2">
                    <input type="radio" name="endobrononial" id="endobrononial_no" value="0"/>
                    <label for="endobrononial_no"> No</label>
                </td></td>
            </tr>
            <tr>
                <td colspan="2">Cuff</td>
                <td colspan="2">
                    <input type="radio" name="cuff" id="cuff_yes" value="1"/>
                    <label for="cuff_yes"> Yes</label>
                </td>
                <td colspan="2">
                    <input type="radio" name="cuff" id="cuff_no" value="0"/>
                    <label for="cuff_no"> No</label>
                </td></td>
            </tr>
            <tr>
                <td colspan="2">Local Spray</td>
                <td colspan="2">
                    <input type="radio" name="local_spray" id="local_spray_yes" value="1"/>
                    <label for="local_spray_yes"> Yes</label>
                </td>
                <td colspan="2">
                    <input type="radio" name="local_spray" id="local_spray_no" value="0"/>
                    <label for="local_spray_no"> No</label>
                </td></td>
            </tr>
            <tr>
                <td colspan="2">Pack</td>
                <td colspan="2">
                    <input type="radio" name="pack" id="pack_yes" value="1"/>
                    <label for="pack_yes"> Yes</label>
                </td>
                <td colspan="2">
                    <input type="radio" name="pack" id="pack_no" value="0"/>
                    <label for="pack_no"> No</label>
                </td></td>
            </tr>
        </table>

        <table class="table table-bordered theadfix_wrapper">
            <tr class="headerClass">
                <td colspan="3">RECOVERY</td>
            </tr>
            <tr>
                <td>Reflex</td>
                <td>
                    <input type="radio" name="reflex" id="reflex_yes" value="1"/>
                    <label for="reflex_yes"> Yes</label>
                </td>
                <td>
                    <input type="radio" name="reflex" id="reflex_no" value="0"/>
                    <label for="reflex_no"> No</label>
                </td>
            </tr>
            <tr>
                <td>Retching</td>
                <td>
                    <input type="radio" name="retching" id="retching_yes" value="1"/>
                    <label for="retching_yes"> Yes</label>
                </td>
                <td>
                    <input type="radio" name="retching" id="retching_no" value="0"/>
                    <label for="retching_no"> No</label>
                </td>
            </tr>
            <tr>
                <td>Excitement</td>
                <td>
                    <input type="radio" name="excitement" id="excitement_yes" value="1"/>
                    <label for="excitement_yes"> Yes</label>
                </td>
                <td>
                    <input type="radio" name="excitement" id="excitement_no" value="0"/>
                    <label for="excitement_no"> No</label>
                </td>
            </tr>
            <tr>
                <td>Vomiting</td>
                <td>
                    <input type="radio" name="recovery_vomiting" id="recovery_vomiting_yes" value="1"/>
                    <label for="recovery_vomiting_yes"> Yes</label>
                </td>
                <td>
                    <input type="radio" name="recovery_vomiting" id="recovery_vomiting_no" value="0"/>
                    <label for="recovery_vomiting_no"> No</label>
                </td>
            </tr>
            <tr>
                <td>BP</td>
                <td colspan="2">
                    <input type="text" class="form-control" name="recovery_bp" id="recovery_bp"/>
                </td>
            </tr>
            <tr>
                <td>Pulse</td>
                <td colspan="2">
                    <input type="text" class="form-control" name="recovery_pulse" id="recovery_pulse"/>
                </td>
            </tr>
            <tr>
                <td>SPO<sub>2</sub></td>
                <td colspan="2">
                    <input type="text" class="form-control" name="recovery_spo2" id="recovery_spo2"/>
                </td>
            </tr>
            <tr>
                <td>ETCO<sub>2</sub></td>
                <td colspan="2">
                    <input type="text" class="form-control" name="recovery_etco2" id="recovery_etco2"/>
                </td>
            </tr>
            <tr>
                <td>Estimated Blood Loss</td>
                <td colspan="2">
                    <input type="text" class="form-control" name="estimated_blood_loss" id="estimated_blood_loss"/>
                </td>
            </tr>
            <tr>
                <td>Total Fluid Given In OR</td>
                <td colspan="2">
                    <input type="text" class="form-control" name="total_fluid_given_in_or" id="total_fluid_given_in_or"/>
                </td>
            </tr>
            <tr>
                <td>Blood</td>
                <td colspan="2">
                    <input type="text" class="form-control" name="recovery_blood" id="recovery_blood"/>
                </td>
            </tr>
            <tr>
                <td>Other</td>
                <td colspan="2">
                    <input type="text" class="form-control" name="recovery_other" id="recovery_other"/>
                </td>
            </tr>
        </table>
        <table class="table table-bordered theadfix_wrapper">
            <tr class="headerClass">
                <td colspan="2">MONITORS</td>
            </tr>

            <tr>
                <td>
                    <input class="form-check-input" type="checkbox" value="1" id="monitor_1">

                    <label class="form-check-label" for="monitor_1">
                        NIBP
                    </label>
                </td>

                <td>
                    <input class="form-check-input" type="checkbox" value="2" id="monitor_2">

                    <label class="form-check-label" for="monitor_2">
                        Spo2
                    </label>
                </td>
            </tr>

            <tr>
                <td>
                    <input class="form-check-input" type="checkbox" value="3" id="monitor_3">

                    <label class="form-check-label" for="monitor_3">
                        HR
                    </label>
                </td>

                <td>
                    <input class="form-check-input" type="checkbox" value="4" id="monitor_4">

                    <label class="form-check-label" for="monitor_4">
                        RR
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <input class="form-check-input" type="checkbox" value="5" id="monitor_5">

                    <label class="form-check-label" for="monitor_5">
                        Etco2
                    </label>
                </td>

                <td>
                    <input class="form-check-input" type="checkbox" value="6" id="monitor_6">

                    <label class="form-check-label" for="monitor_6">
                        IBP
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <input class="form-check-input" type="checkbox" value="7" id="monitor_7">

                    <label class="form-check-label" for="monitor_7">
                        ECG
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <textarea id="monitor_description" name="monitor_description" style="width:100%;" placeholder="description"></textarea>
                </td>
            </tr>

        </table>


    </div>
</div>
<div class="col-md-12">
    <button style="padding: 3px 3px;float: right;width: 110px;height: 30px;" onclick="saveAR();" type="button" title="Save"
                        class="btn btn-success" style="display:none;" id="save_operation_notes_btn">Save<i id="save_operation_notes_spin"
                            class="fa fa-save padding_sm"></i>
    </button>
</div>
