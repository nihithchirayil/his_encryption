<style>
    label{
        margin-right:10px;
        float:left;clear:right;
        font-size:12px !important;
    }
    .form-control{
        float: left;
        clear:right;
        margin-right:10px;

    }
    .col-md-12{
        margin-top:8px;
    }
    .radio-success{
        width: 55px !important;
        margin-left: 10px;
        float: left;
        clear:right;
    }

    #pre_anaesthetic_inv_table input[type="text"] {
        margin-bottom:5px !important;
        /* box-shadow:none !important; */
    }
</style>
<input type="hidden" id="anaesthesia_surgery_req_head_id" value="{{$head_id}}"/>
<input type="hidden" id="anaesthesia_surgery_req_detail_id" value="{{$surgery_detail_id}}"/>
<input type="hidden" id="anaesthesia_patient_id" value="{{$patient_id}}"/>
<div class="col-md-12 no-padding">
    <div class="col-md-12" style="white-space:nowrap;">
        <label style="margin-right:10px;float:left;clear:right;">Surgery</label><input type="text" class="form-control" id="name_of_surgery" value="{{$surgery_details[0]->surgery_name}}" placeholder="Name of surgery" style="width:1122px;clear:right;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Date & Time of the surgery</label><input type="text" id="date_and_time_of_surgery" class="form-control datetimepicker" placeholder="Date & Time of the surgery" value="@if($surgery_details[0]->scheduled_at !=''){{date('M-d-Y h:i A',strtotime($surgery_details[0]->scheduled_at))}}@endif" style="width:480px;"/>
        <label>surgeon</label><input style="width:480px;" type="text" id="pre_ans_surgeon_name" value="{{$surgeon_name}}" class="form-control" placeholder="Surgeon"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>History –Hypertension-</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="history_hypertension_yes" type="radio" name="history_hypertension" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="history_hypertension_no" type="radio" name="history_hypertension" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Diabetes</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="diabetes_yes" type="radio" name="diabetes" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="diabetes_yes_no" type="radio" name="diabetes" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Asthma</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="asthma_yes" type="radio" name="asthma" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="asthma_no" type="radio" name="asthma" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Epilepsy</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="epilepsy_yes" type="radio" name="epilepsy" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="epilepsy_no" type="radio" name="epilepsy" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>Allergy</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="allergy_yes" type="radio" name="allergy" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="allergy_no" type="radio" name="allergy" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>I.H.D</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="i_h_d_yes" type="radio" name="i_h_d" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="i_h_d_no" type="radio" name="i_h_d" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <label>Previous Surgery &Type of Anaesthesia</label><input type="text" id="previous_surgeory_and_anesthesia" class="form-control" placeholder="Previous Surgery &Type of Anaesthesia" style="width:301px;"/>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>Thyroid</label>
            <div class="radio radio-success inline no-margin " style="width:60px !important;">
                <input class="checkit" id="hyper" type="radio" name="thyroid_condition" value="hyper">
                <label class="text-blue">
                    Hyper
                </label>
            </div>
            <div class="radio radio-success inline no-margin " style="width:60px !important;">
                <input class="checkit" id="hypo" type="radio" name="thyroid_condition" value="hypo">
                <label class="text-blue">
                    Hypo
                </label>
            </div>
        </div>
        <label>Hyper History of TB</label><input type="text" id="hyper_history_of_tb" class="form-control" placeholder="Hyper History of TB" style="width:655px;"/>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <label>Medications</label><input type="text" id="anesthesia_medications" class="form-control" placeholder="Medications" style="width:1102px;"/>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <label>Other Relevant History</label><input type="text" id="other_relevant_history" class="form-control" placeholder="Other Relevant History" style="width:1175px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>PHYSICAL EXAMINATION -</label>
        <div class="col-md-3 no-padding">
            <label>Icterus</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="icterus_yes" type="radio" name="icterus" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="icterus_no" type="radio" name="icterus" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Pallor</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="pallor_yes" type="radio" name="pallor" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="pallor_no" type="radio" name="pallor" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Lymphadenopathy</label>
            <input type="text" id="lymphadenopathy" class="form-control" placeholder="Lymphadenopathy" style="width:175px;"/>
        </div>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>Edema</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="edema_yes" type="radio" name="edema" value=1>
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="edema_no" type="radio" name="edema" value=2>
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <label>Airway-Dentition</label>
        <input type="text" id="airway_dentition" class="form-control" placeholder="Airway-Dentition" style="width:175px;"/>
        <label>Thyroid</label>
        <input type="text" id="thyroid_desc" class="form-control" placeholder="Thyroid" style="width:175px;"/>
        <label>Spine</label>
        <input type="text" id="spine" class="form-control" placeholder="Thyroid" style="width:175px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Mallampattl Grade</label>
        <input type="text" id="mallampattl_grade" class="form-control" placeholder="Mallampattl Grade" style="width:175px;"/>
        <label>TM Distance</label>
        <input type="text" id="tm_distance" class="form-control" placeholder="TM Distance" style="width:175px;"/>

    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Pulse</label>
        <input type="text" id="anesthesia_pulse" class="form-control number_only" placeholder="Pulse" style="width:200px;"/>
        <label>/min BP</label>
        <input type="text" id="anesthesia_bp" class="form-control" placeholder="BP" style="width:200px;"/>
        <label>mmHg Temperature</label>
        <input type="text" id="anesthesia_temprature" class="form-control number_only" placeholder="Temperature" style="width:200px;"/>
        <label>F: Weight</label>
        <input type="text" id="anesthesia_weight" class="form-control number_only" placeholder="Weight" style="width:200px;"/>kg

    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <label>C.V.S</label>
        <input type="text" id="cvs" class="form-control" placeholder="cvs" style="width:1133px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Respiratory System
        </label>
        <input type="text" id="respiratory_system" class="form-control" placeholder="respiratory_system" style="width:1058px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>GIT
        </label>
        <input type="text" id="git" class="form-control" placeholder="git" style="width:1143px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>C.N.S
        </label>
        <input type="text" id="cns" class="form-control" placeholder="C.N.S" style="width:1130px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Other Relevant findings
        </label>
        <input type="text" id="other_relevant_findings" class="form-control" placeholder="Other Relevant findings" style="width:1036px;"/>
    </div>

    <div class="col-md-12" style="margin:15px">
        <h4>INVESTIGATIONS</h4>
    </div>

    <div class="col-md-11">
        <button onclick="addNewColumInvestigation();" class="btn bg-blue" style="float:right;"><i class="fa fa-plus"></i></button>
        <table id="pre_anaesthetic_inv_table" class="table table-bordered theadfix_wrapper"
            style="font-size: 12px;">
            <tbody>
                <tr id="pre_inv_1">
                    <td class="common_td_rules" style="">Time</td>
                    @for($i=1;$i<=5;$i++)
                        <td class="common_td_rules" style="text-align:center;">
                            <input type="text" id="pre_inv_time_{{$i}}" class="form-control datepicker" placeholder="" />
                        </td>
                    @endfor
                </tr>
                <tr id="pre_inv_2">
                    <td class="common_td_rules" style="">
                        1)<b><span> Hb % </span></b>
                        <br>
                        <span>TLC</span>
                        <br>
                        <span>Platelets</span>
                    </td>
                    @for($i=1;$i<=5;$i++)
                        <td class="common_td_rules" style="text-align:center;">
                            <input type="text" id="pre_inv_hb_{{$i}}" class="form-control" placeholder=""/>
                            <br>
                            <input type="text" id="pre_tlc_hb_{{$i}}" class="form-control" placeholder=""/>
                            <br>
                            <input type="text" id="pre_inv_platelets_{{$i}}" class="form-control" placeholder="" />
                        </td>
                    @endfor
                </tr>

                <tr id="pre_inv_3">
                    <td class="common_td_rules" style="">
                        <span><b>2)Blood Sugar (mg %)</b>
                         Fasting </span>
                         <br>
                         <span>PP</span>
                         <br>
                         <span>Random</span>
                         <br>
                         <span>HbA1C</span>
                    </td>
                    @for($i=1;$i<=5;$i++)
                        <td class="common_td_rules" style="text-align:center;">
                            <input type="text" id="pre_inv_blood_sugar_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_pp_{{$i}}" class="form-control" placeholder=""/>
                            <br>
                            <input type="text" id="pre_inv_random_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_hba1c_{{$i}}" class="form-control" placeholder="" />
                        </td>
                    @endfor
                </tr>

                <tr id="pre_inv_4">
                    <td class="common_td_rules" style="">
                        <span><b>3)Blood Grouping & Typing</b></span>
                        <br>
                        <span>Bleeding Time</span>
                        <br>
                        <span>Clotting Time</span>
                    </td>
                    @for($i=1;$i<=5;$i++)
                        <td class="common_td_rules" style="text-align:center;">
                            <input type="text" id="pre_inv_blood_grouping_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_bleeding_time_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_clotting_time_{{$i}}" class="form-control" placeholder="" />
                        </td>
                    @endfor
                </tr>

                <tr id="pre_inv_5">
                    <td class="common_td_rules" style="">
                        <span><b>4)PT(Control</b></span>
                        <br>
                        <span> /test</span>
                        <br>
                        <span> )INR</span>
                        <br>
                        <span>)aPTT(Control</span>
                        <br>
                        <span>)/test</span>
                        <br>
                        <span>)</span>
                    </td>
                    @for($i=1;$i<=5;$i++)
                        <td class="common_td_rules" style="text-align:center;">
                            <input type="text" id="pre_inv_pt_control_{{$i}}" class="form-control" placeholder="" style=""/>
                            <br>
                            <input type="text" id="pre_inv_pt_test_{{$i}}" class="form-control" placeholder="" style=""/>
                            <br>
                            <input type="text" id="pre_inv_pt_inr_{{$i}}" class="form-control" placeholder="" style=""/>
                            <br>
                            <input type="text" id="pre_inv_apt_control_{{$i}}" class="form-control" placeholder="" style=""/>
                            <br>
                            <input type="text" id="pre_inv_apt_test_{{$i}}" class="form-control" placeholder="" style=""/>
                            <br>
                            <input type="text" id="pre_inv_apt_others_{{$i}}" class="form-control" placeholder="" style=""/>
                        </td>
                    @endfor
                </tr>

                <tr id="pre_inv_6">
                    <td class="common_td_rules" style="">
                        <span><b>6)Serum creatinine</b></span>
                        <br>
                        <span>Blood Urea</span>
                        <br>
                        <span>C.U.E</span>

                    </td>
                    @for($i=1;$i<=5;$i++)
                        <td class="common_td_rules" style="text-align:center;">
                            <input type="text" id="pre_inv_creatine_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_blood_urea_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_cue_{{$i}}" class="form-control" placeholder="" />
                        </td>
                    @endfor
                </tr>

                <tr id="pre_inv_7">
                    <td class="common_td_rules" style="">
                        <span><b>7)Serum Electrolytes</b> Na</span>
                        <br>
                        <span>K</span>
                        <br>
                        <span>Others</span>

                    </td>
                    @for($i=1;$i<=5;$i++)
                        <td class="common_td_rules" style="text-align:center;">
                            <input type="text" id="pre_inv_na_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_k_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_serum_electrolytes_others_{{$i}}" class="form-control" placeholder="" />
                        </td>
                    @endfor
                </tr>

                <tr id="pre_inv_8">
                    <td class="common_td_rules" style="">
                        <span><b>8)HBsAg</b></span>
                        <br>
                        <span>HCV</span>
                        <br>
                        <span>HIV</span>
                        <br>
                        <span>X-RAY Chest</span>
                    </td>
                    @for($i=1;$i<=5;$i++)
                        <td class="common_td_rules" style="text-align:center;">
                            <input type="text" id="pre_inv_hbsag_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_hcv_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_hiv_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_xray_chest_{{$i}}" class="form-control" placeholder="" />
                        </td>
                    @endfor
                </tr>

                <tr id="pre_inv_9">
                    <td class="common_td_rules" style="">
                        <span><b>9)E.C.G</b></span>
                        <br>
                        <span>ECHO</span>
                        <br>
                        <span>LFT</span>
                    </td>
                    @for($i=1;$i<=5;$i++)
                        <td class="common_td_rules" style="text-align:center;">
                            <input type="text" id="pre_inv_ecg_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_echo_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_lft_{{$i}}" class="form-control" placeholder="" />
                            <br>
                        </td>
                    @endfor
                </tr>

                <tr id="pre_inv_10">
                    <td class="common_td_rules" style="">
                        <span><b>10)TFT:</b> TSH</span>
                        <br>
                        <span>T3</span>
                        <br>
                        <span>T4</span>
                    </td>
                    @for($i=1;$i<=5;$i++)
                        <td class="common_td_rules" style="text-align:center;">
                            <input type="text" id="pre_inv_tsh_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_t3_{{$i}}" class="form-control" placeholder="" />
                            <br>
                            <input type="text" id="pre_inv_t4_{{$i}}" class="form-control" placeholder="" />
                            <br>
                        </td>
                    @endfor
                </tr>
            </tbody>
        </table>
    </div>


    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>Other Specific Investigations:</b>
        </label>
        <input type="text" id="other_specific_investigations" class="form-control" placeholder="Other Specific Investigations" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>PRE-OP OPINION</b>
        </label>
        <div class="clearfix"></div>
        <textarea id="pre_op_openion" class="form-control" placeholder="PRE-OP OPINION" style="width:1169px;"></textarea>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>REVIEW PAC</b>
        </label>
        <div class="clearfix"></div>
        <textarea id="review_pac" class="form-control" placeholder="REVIEW PAC" style="width:1169px;"></textarea>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>ASA CATEGORY</b>
        </label>
        <input type="text" id="asa_category" class="form-control" placeholder="ASA CATEGORY" style="width:200px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Type of Anaesthesia planned & Explained to patient
        </label>
        <input type="text" id="type_of_anaesthesia_planned" class="form-control" placeholder="Type of Anaesthesia planned & Explained to patient" value="{{$surgery_details[0]->anaesthesia}}" style="width:1170px;"/>
    </div>
    <div class="col-md-12" style="white-space:nowrap;margin-bottom:20px;">
        <label>Completed by
        </label>
        <input type="text" id="completed_by" value="{{$user_name}}" class="form-control" placeholder="Completed by" style="width:175px;"/>
        <label>Date
        </label>
        <input type="text" id="completed_date" class="form-control datepicker" placeholder="" style="width:175px;"/>
        <label>Time
        </label>
        <input type="text" id="completed_time" class="form-control timepicker" placeholder="" style="width:175px;"/>
    </div>
</div>

<div class="col-md-12 " style="margin-top:5px;margin-bottom:5px;">
    <button style="padding: 3px 3px" onclick="saveAnaesthetiaPreAnaesthetic();" type="button" title="Save"
        class="btn btn-success pull-right" id="save_checklistbtn">Save<i id="save_checklistspin"
            class="fa fa-save padding_sm"></i>
    </button>
    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
        class="btn btn-danger pull-right">Close <i class="fa fa-times"></i></button>
</div>
