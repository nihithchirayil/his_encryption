
<div class="col-md-12">
    @php
        $nursing_record_data = $resultData->nursing_record_data;
        $nursing_record_data = json_decode($nursing_record_data);
    @endphp
    <form id="ot_checklist_ns_record" name="ot_checklist_ns_record">
    <div>
        <h5 class="checklist_head_text">INTRAOPERATIVE NURSING RECORD</h5>
    </div>

    <div class="col-md-12" >
    <div class="col-md-6">Name of Procedure:
        @if(isset($nursing_record_data->ns_record_procedure_name) && $nursing_record_data->ns_record_procedure_name !='')
            {{$nursing_record_data->ns_record_procedure_name}}
        @endif
    </div>
  <div class="col-md-3">Procedure start time:
    @if(isset($nursing_record_data->ns_record_procedure_start_time) && $nursing_record_data->ns_record_procedure_start_time !='')
    {{date('h:i A',strtotime($nursing_record_data->ns_record_procedure_start_time))}}
    @endif
    </div>
  <div class="col-md-3">Procedure end time:
    @if(isset($nursing_record_data->ns_record_procedure_end_time) && $nursing_record_data->ns_record_procedure_end_time !='')
    {{date('h:i A',strtotime($nursing_record_data->ns_record_procedure_end_time))}}
    @endif
  </div>
  </div>
  <div class="col-md-12">
  <div class="col-md-6">Specimen:
    @if(isset($nursing_record_data->ns_record_specimen) && $nursing_record_data->ns_record_specimen !='')
            {{$nursing_record_data->ns_record_specimen}}
        @endif
  </div>
  <div class="col-md-6">Lab:
    @if(isset($nursing_record_data->ns_record_lab) && $nursing_record_data->ns_record_lab !='')
    {{$nursing_record_data->ns_record_lab}}
@endif
  </div>
  </div>
  <div class="col-md-12" style="margin-bottom:10px">
  <div class="col-md-6">No. of specimen:
    @if(isset($nursing_record_data->ns_record_no_specimen) && $nursing_record_data->ns_record_no_specimen !='')
    {{$nursing_record_data->ns_record_no_specimen}}
@endif
  </div>
  <div class="col-md-6">Collected By:
    @if(isset($nursing_record_data->ns_record_collector) && $nursing_record_data->ns_record_collector !='')
    {{$nursing_record_data->ns_record_collector}}
@endif
  </div>
  </div>
  <br>

   <div><h5 style="margin-left:10px;margin-top:10px;" class="checklist_head_text">PACKING /DRAINS/ CATHETERS</h5></div>

    <table id="result_data_table" class="table table-condensed table_sm table-bordered theadfix_wrapper"
        style="font-size: 12px;margin-top:10px;">
        <thead>
            <tr class="common_table_header">
                <th class="common_td_rules" width="15%">TYPE</th>
                <th class="common_td_rules" width="15%">SITE</th>
                <th class="common_td_rules" width="15%">SIZE</th>
                <th class="common_td_rules" width="15%">AMOUNT</th>
                <th class="common_td_rules" width="15%">DATE/TIME</th>
                <th class="common_td_rules" width="15%">SIGN</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($nursing_record_data->op_pack) && sizeof($nursing_record_data->op_pack)>0)
                @for($i=0;$i< sizeof($nursing_record_data->op_pack);$i++)
                    <tr>
                        <td>
                            @if(isset($nursing_record_data->op_pack[$i]->type_op) && $nursing_record_data->op_pack[$i]->type_op !='')
                                {{$nursing_record_data->op_pack[$i]->type_op}}
                            @endif
                        </td>
                        <td>
                            @if(isset($nursing_record_data->op_pack[$i]->site_op) && $nursing_record_data->op_pack[$i]->site_op !='')
                                {{$nursing_record_data->op_pack[$i]->site_op}}
                            @endif
                        </td>
                        <td>
                            @if(isset($nursing_record_data->op_pack[$i]->size_op) && $nursing_record_data->op_pack[$i]->size_op !='')
                                {{$nursing_record_data->op_pack[$i]->size_op}}
                            @endif
                        </td>
                        <td>
                            @if(isset($nursing_record_data->op_pack[$i]->amount_op) && $nursing_record_data->op_pack[$i]->amount_op !='')
                                {{$nursing_record_data->op_pack[$i]->amount_op}}
                            @endif
                        </td>
                        <td>
                            @if(isset($nursing_record_data->op_pack[$i]->date_time_op) && $nursing_record_data->op_pack[$i]->date_time_op !='')
                                {{date('M:d:Y h:i A',strtotime($nursing_record_data->op_pack[$i]->date_time_op))}}
                            @endif
                        </td>
                        <td>
                            @if(isset($nursing_record_data->op_pack[$i]->sign_op) && $nursing_record_data->op_pack[$i]->sign_op !='')
                                {{$nursing_record_data->op_pack[$i]->sign_op}}
                            @endif
                        </td>
                    </tr>
                @endfor
            @endif
        </tbody>

    </table>
    </form>

    <div class="col-md-12">
        Implants:
        @if(isset($nursing_record_data->ns_implants) && $nursing_record_data->ns_implants !='')
            {{$nursing_record_data->ns_implants}}
        @endif
    </div>
</div>
