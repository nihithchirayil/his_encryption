<style>
    label{
        margin-right:10px;
        float:left;clear:right;
        font-size:12px !important;
    }
    .form-control{
        float: left;
        clear:right;
        margin-right:10px;

    }
    .col-md-12{
        margin-top:8px;
    }
    .radio-success{
        width: 130px;
        margin-left: 10px;
        float: left;
        clear:right;
    }
</style>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Level of consciousness</label><input type="text" id="imm_post_op_level_of_consciouness" class="form-control" placeholder="" style="width:480px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Coherence</label><input type="text" id="imm_post_op_coherence" class="form-control" placeholder="" style="width:480px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Reflexes</label><input type="text" id="imm_post_op_reflexes" class="form-control" placeholder="" style="width:200px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Head lift</label><input type="text" id="imm_post_op_head_lift" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">P.R</label><input type="text" id="imm_post_op_pr" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">BP</label><input type="text" id="imm_post_op_bp" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">R.R</label><input type="text" id="imm_post_op_rr" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">SPO2</label><input type="text" id="imm_post_op_spop2" class="form-control" placeholder="" style="width:100px;"/>
</div>

<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">SA/EA:Sensory Level</label><input type="text" id="imm_post_op_sa_ea_sensory_level" class="form-control" placeholder="" style="width:400px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Motor Level</label><input type="text" id="imm_post_op_motor_level" class="form-control" placeholder="" style="width:400px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Patient transferred to</label>
    <div class="radio radio-success inline no-margin" style="width:90px !important;">
        <input class="checkit" id="imm_post_op_patient_transferred_to_recovery" type="radio" name="imm_post_op_patient_transferred_to" value='recovery'>
        <label class="text-blue" for="">
            Recovery
        </label>
    </div>
    <div class="radio radio-success inline no-margin ">
        <input class="checkit" id="imm_post_op_patient_transferred_to_icu" type="radio" name="imm_post_op_patient_transferred_to" value='icu'>
        <label class="text-blue" for="">
            ICU
        </label>
    </div>
    <div class="radio radio-success inline no-margin ">
        <input class="checkit" id="imm_post_op_patient_transferred_to_others" type="radio" name="imm_post_op_patient_transferred_to" value='others'>
        <label class="text-blue" for="imm_post_op_patient_transferred_to_others">
            Other (specify)
        </label>
    </div>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Other Notes (if any)</label><textarea id="imm_post_op_other_notes_if_any" class="form-control" placeholder=""></textarea>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Monitoring during shifting:</label><input type="text" id="imm_post_op_monitoring_drug_shifting" class="form-control" placeholder="" style="width:400px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Duration of shifting</label><input type="text" id="imm_post_op_duration_of_shifting" class="form-control" placeholder="" style="width:400px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Post Shifting Vitals (After shifting</label><textarea id="imm_post_op_post_shifting_vitals" class="form-control" placeholder=""></textarea>
</div>

<div class="col-md-12" style="white-space:nowrap;">
    <div class="col-md-6" style="white-space:nowrap;">
        <h4>Pre-Discharge assessment (Recovery Room)</h4>
        <br>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:95px;float:left;clear:right;">Vitals Stable</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_vitals_stable_yes" type="radio" name="imm_post_op_vitals_stable" value=1>
                <label class="text-blue" for="">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_vitals_stable_no" type="radio" name="imm_post_op_vitals_stable" value=2>
                <label class="text-blue" for="">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:95px;float:left;clear:right;">Patient able to walk</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_patient_able_to_walk_Independent" type="radio" name="imm_post_op_patient_able_to_walk" value="independent">
                <label class="text-blue" for="imm_post_op_patient_able_to_walk_Independent">
                    Independent
                </label>
            </div>
            <div class="radio radio-success inline no-margin" style="margin-left:50px !important;">
                <input class="checkit" id="imm_post_op_patient_able_to_walk_with_assist" type="radio" name="imm_post_op_patient_able_to_walk" value="with_assist">
                <label class="text-blue" for="imm_post_op_patient_able_to_walk_with_assist">
                    With Assist
                </label>
            </div>
            <div class="radio radio-success inline no-margin " style="margin-left:45px !important;">
                <input class="checkit" id="imm_post_op_patient_able_to_walk_unable" type="radio" name="imm_post_op_patient_able_to_walk" value="unable">
                <label class="text-blue" for="imm_post_op_patient_able_to_walk_unable">
                    Unable
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:95px;float:left;clear:right;">Vomiting</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_vomiting_yes" type="radio" name="imm_post_op_vomiting" value=1>
                <label class="text-blue" for="imm_post_op_vomiting_yes">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_vomiting_no" type="radio" name="imm_post_op_vomiting" value=2>
                <label class="text-blue" for="imm_post_op_vomiting_no">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:117px;float:left;clear:right;">Pain</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_pain_mild" type="radio" name="imm_post_op_pain" value="mild">
                <label class="text-blue" for="imm_post_op_pain_mild">
                    Mild
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_pain_moderate" type="radio" name="imm_post_op_pain" value="moderate">
                <label class="text-blue" for="imm_post_op_pain_moderate">
                    Moderate
                </label>
            </div>
            <div class="radio radio-success inline no-margin " style="margin-left:35px !important;">
                <input class="checkit" id="imm_post_op_pain_severe" type="radio" name="imm_post_op_pain" value="severe">
                <label class="text-blue" for="imm_post_op_pain_severe">
                    Severe
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:94px;float:left;clear:right;">Bleeding</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_bleeding_yes" type="radio" name="imm_post_op_bleeding" value=1>
                <label class="text-blue" for="imm_post_op_bleeding_yes">
                    Yes
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_bleeding_no" type="radio" name="imm_post_op_bleeding" value=2>
                <label class="text-blue" for="imm_post_op_bleeding_no">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:19px;float:left;clear:right;">Condition at Discharge</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_condition_discharge_stable" type="radio" name="imm_post_op_condition_discharge" value='stable'>
                <label class="text-blue" for="imm_post_op_condition_discharge_stable">
                    Stable
                </label>
            </div>
            <div class="radio radio-success inline no-margin " style="margin-left:15px !important;">
                <input class="checkit" id="imm_post_op_condition_discharge_fair" type="radio" name="imm_post_op_condition_discharge" value='fair'>
                <label class="text-blue" for="imm_post_op_condition_discharge_fair">
                    fair
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_condition_discharge_unstable" type="radio" name="imm_post_op_condition_discharge" value='unstable'>
                <label class="text-blue" for="imm_post_op_condition_discharge_unstable">
                    Unstable
                </label>
            </div>
        </div>
        <div class="col-md-12" style="white-space:nowrap;">
            <label style="margin-right:44px;float:left;clear:right;">Discharge Criteria</label>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_discharge_criteria_met" type="radio" name="imm_post_op_discharge_criteria" value='met'>
                <label class="text-blue" for="imm_post_op_discharge_criteria_met">
                    Met
                </label>
            </div>
            <div class="radio radio-success inline no-margin ">
                <input class="checkit" id="imm_post_op_discharge_criteria_not_met" type="radio" name="imm_post_op_discharge_criteria" value='not_met'>
                <label class="text-blue" for="imm_post_op_discharge_criteria_not_met">
                    Not Met
                </label>
            </div>
        </div>
    </div>
    <div class="col-md-6" style="white-space:nowrap;">
        <h4>Aldrete Score</h4>
        <br>Criteria for Determination of Discharge Score For
Release from the Post Anaesthesia Care unit

        <table id="result_data_table" class="table table-condensed table_sm table-bordered" style="font-size: 12px;">
            <thead>
                <tr>
                    <td>
                        Variable Evaluated
                    </td>
                    <td>
                        Score
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="text-align: left;" colspan="2"><b>Activity</b></td>
                </tr>
                <tr>
                    <td style="text-align: left;">Able to move four extremities on command</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_activity_2" type="radio" name="imm_post_op_activity" value=2>
                            <label class="text-blue" for="imm_post_op_activity_2">
                                2
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Able to move two extremities on command</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_activity_1" type="radio" name="imm_post_op_activity" value=1>
                            <label class="text-blue" for="imm_post_op_activity_1">
                                1
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Able to move no extremities on command</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_activity_0" type="radio" name="imm_post_op_activity" value=0>
                            <label class="text-blue" for="imm_post_op_activity_0">
                                0
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2"><b>Breathing
                    </b></td>
                </tr>
                <tr>
                    <td style="text-align: left;">Able to breathe deeply and cough freely</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_breathing_2" type="radio" name="imm_post_op_breathing" value=2>
                            <label class="text-blue" for="imm_post_op_breathing_2">
                                2
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Dyspnea</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_breathing_1" type="radio" name="imm_post_op_breathing" value=1>
                            <label class="text-blue" for="imm_post_op_breathing_1">
                                1
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Apnea</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_breathing_0" type="radio" name="imm_post_op_breathing" value=0>
                            <label class="text-blue" for="imm_post_op_breathing_0">
                                0
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2"><b>Circulation
                    </b></td>
                </tr>
                <tr>
                    <td style="text-align: left;">Blood pressure+/-20% of the preanesthetic level</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_circulation_2" type="radio" name="imm_post_op_circulation" value=2>
                            <label class="text-blue" for="imm_post_op_circulation_2">
                                2
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Blood pressure is +/-20% to 49% of the preanesthetic level</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_circulation_1" type="radio" name="imm_post_op_circulation" value=1>
                            <label class="text-blue" for="imm_post_op_circulation_1">
                                1
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Blood pressure >+/-50% of the preanesthetic level</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_circulation_0" type="radio" name="imm_post_op_circulation" value=0>
                            <label class="text-blue" for="imm_post_op_circulation_0">
                                0
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2"><b>Consciousness
                    </b></td>
                </tr>

                <tr>
                    <td style="text-align: left;">Fully awake</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_consciousness_2" type="radio" name="imm_post_op_consciousness" value=2>
                            <label class="text-blue" for="imm_post_op_consciousness_2">
                                2
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Arousable</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_consciousness_1" type="radio" name="imm_post_op_consciousness" value=1>
                            <label class="text-blue" for="imm_post_op_consciousness_1">
                                1
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Not responding</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_consciousness_0" type="radio" name="imm_post_op_consciousness" value=0>
                            <label class="text-blue" for="imm_post_op_consciousness_0">
                                0
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2"><b>Oxygen Saturation (Pulse Oximetry)
                    </b></td>
                </tr>

                <tr>
                    <td style="text-align: left;"> >92% while breathing room air</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_o2_saturation_2" type="radio" name="imm_post_op_o2_saturation" value=2>
                            <label class="text-blue" for="imm_post_op_o2_saturation_2">
                                2
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">Needs supplemental oxygen to maintain saturation >90%</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_o2_saturation_1" type="radio" name="imm_post_op_o2_saturation" value=1>
                            <label class="text-blue" for="imm_post_op_o2_saturation_1">
                                1
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">< 90% even with supplemental oxygen</td>
                    <td>
                        <div class="radio radio-success inline no-margin ">
                            <input class="checkit aldrete_score" id="imm_post_op_o2_saturation_0" type="radio" name="imm_post_op_o2_saturation" value=0>
                            <label class="text-blue" for="imm_post_op_o2_saturation_0">
                                0
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>Total Score
                    </b></td>
                    <td><input type="text" readonly="readonly" id="imm_post_op_total_score" class="form-control" placeholder="" style="width:80px;font-weight:700;" value="0"/></td>
                </tr>



            </tbody>
        </table>
        A score of 9 or 10 is needed to discharge the patient
    </div>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    @if($signatureImage !='')
        @php
            $signatureImage = "data:image/jpeg;base64,".base64_decode($signatureImage);
        @endphp
            <img id="dr_signature" style="width:175px; height:70px;"
            src="{{$signatureImage}}"><br>
    @endif
    <label style="margin-right:10px;float:left;clear:right;">{{ucwords($anaesthetist_name)}}</label><br>
    <div class="clearfix"></div>
    <label style="margin-right:10px;float:left;clear:right;">License No:{{ucwords($anaesthetist_license_no)}}</label>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Date & Time :</label><input type="text" id="imm_post_op_date_and_time" class="form-control datetimepicker" placeholder="" style="width:400px;"/>
</div>

<div class="col-md-12 " style="margin-top:5px;margin-bottom:5px;">
    <button style="padding: 3px 3px" onclick="saveImmediatePostOp();" type="button" title="Save"
        class="btn btn-success pull-right" id="save_checklistbtn">Save<i id="save_checklistspin"
            class="fa fa-save padding_sm"></i>
    </button>
    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
        class="btn btn-danger pull-right">Close <i class="fa fa-times"></i></button>
</div>

