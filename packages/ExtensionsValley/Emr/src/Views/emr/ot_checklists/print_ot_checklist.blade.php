<div class="col-md-12 theadscroll" style="position: relative; height:520px;">
    <div class="col-md-12">
        <button onclick="takePrintOtChecklist();" type="button" class="btn btn-primary pull-right">
            <i class="fa fa-print"></i> Print
        </button>
    </div>
    <div class="col-md-12" id="ResultDataContainer">

        <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
            <thead>
                <tr>
                    <th style='padding: 5px 0;' colspan='4' class='text-center'>
                        <h4 class="checklist_head_text"><b>OT CHECKLIST</b></h4>
                    </th>
                </tr>
            </thead>
            <tbody style=''>
                <tr>
                    <td style=' padding-left:5px;;text-align:left; '>Patient Name</td>
                    <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data[0]->patient_name !!}</td>
                    <td style=' padding-left:5px;;text-align:left;'>UHID No.</td>
                    <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->uhid !!}</td>
                </tr>
                <tr>
                    <td style=' padding-left:5px;;text-align:left; '>Age &amp; Gender</td>
                    <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data[0]->age   !!} / {!! $patient_data[0]->gender   !!}</td>
                    <td style=' padding-left:5px;; text-align:left;'> IP No. </td>
                    <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->admission_no   !!}</td>
                </tr>
                <tr>
                    <td rowspan='2' style=' padding-left:5px;;text-align:left;'> Address</td>
                    <td rowspan='2' style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->address   !!}</td>
                    <td style=' padding-left:5px;; text-align:left;'>DOA </td>
                    <td style=' padding-left:5px;; text-align:left;' id="summary_admit_date_time">@if($patient_data[0]->admission_date!='')
                        {!! date('M-d-Y',strtotime($patient_data[0]->admission_date)) !!}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style=' padding-left:5px;;text-align:left; '>DOD</td>
                    <td style=' padding-left:5px;; text-align:left;' id="summary_discharge_date_time">
                        @if($patient_data[0]->discharge_datetime != '')
                            {!! $patient_data[0]->discharge_datetime !!} {!!$patient_data[0]->discharge_datetime!!}
                        @endif
                    </td>
                </tr>

                <tr>
                    <td style=' padding-left:5px;;text-align:left; '>Current Room</td>
                    <td style=' padding-left:5px;;text-align:left; '>{{$patient_data[0]->bed_name}}</td>
                    <td style=' padding-left:5px;;text-align:left; '>DOS</td>
                    <td style=' padding-left:5px;; text-align:left;'>
                        @if(!empty($surgery_date))
                            {{date("Y-m-d",strtotime($surgery_date))}}
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>

        <div class="col-md-12 no-padding" style="margin-top:10px">
            <div class="col-md-12 no-padding">
                <h5 class="text-center"><b class="checklist_head_text">PARAMETERS</b></h5>
                @include('Emr::emr.ot_checklists.print_ot_parameters')
            </div>
        </div>

         <div class="col-md-12 no-padding" style="margin-top:10px">
            <div class="col-md-12 no-padding">
                <h5 class="text-center"><b class="checklist_head_text">PRE OP VITALS</b></h5>

                @include('Emr::emr.ot_checklists.print_op_vitals')
            </div>
        </div>
        <div class="col-md-12 no-padding" style="margin-top:10px">
            <div class="col-md-12 no-padding">
                <h5 class="text-center"><b class="checklist_head_text">SIGN IN</b></h5>

                @include('Emr::emr.ot_checklists.print_signin')
            </div>
        </div>
         <div class="col-md-12 no-padding" style="margin-top:10px">
            <div class="col-md-12 no-padding">
                <h5 class="text-center"><b class="checklist_head_text">TIME OUT</b></h5>

                @include('Emr::emr.ot_checklists.print_timeout')
            </div>
        </div>
        <div class="col-md-12 no-padding" style="margin-top:10px">
            <div class="col-md-12 no-padding">
                <h5 class="text-center"><b class="checklist_head_text">NURSING RECORD</b></h5>

                @include('Emr::emr.ot_checklists.print_nursing_record')
            </div>
        </div>
        <div class="col-md-12 no-padding" style="margin-top:10px">
            <div class="col-md-12 no-padding">
                <h5 class="text-center"><b class="checklist_head_text">SIGN OUT</b></h5>
                @include('Emr::emr.ot_checklists.print_sign_out')
            </div>
        </div>

    </div>

</div>
