<div class="col-md-12">
    @php
        $sign_in_data = $resultData->sign_in_data;
        $sign_in_data = json_decode($sign_in_data);
    @endphp

    <div class="col-md-12"><b class="checklist_head_text">BEFORE INTRODUCTION OF ANAESTHESIA</b></div>
        <div class="col-md-6 padding_sm">
            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                @if(isset($sign_in_data->patient_identity_verified) && $sign_in_data->patient_identity_verified == 1)
                <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                <label class="text-blue" for="patient_identity_verified">
                    Patient identity verified
                </label>

            </div>
            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                @if(isset($sign_in_data->correct_procedure) && $sign_in_data->correct_procedure == 1)
                <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                <label class="text-blue" for="patient_identity_verified">
                    Correct procedure
                </label>


            </div>
            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                @if(isset($sign_in_data->site_marked) && $sign_in_data->site_marked == 1)
                <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                <label class="text-blue" for="patient_identity_verified">
                    Site marked
                </label>

            </div>
            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                @if(isset($sign_in_data->availability_of_required_medical) && $sign_in_data->availability_of_required_medical == 1)
                <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                <label class="text-blue" for="patient_identity_verified">
                    Availability of required medical records/
                    images/studies/investigation results
                </label>
             </div>
             <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">

                @if(isset($sign_in_data->functionality_of_required) && $sign_in_data->functionality_of_required == 1)
                <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                <label class="text-blue" for="patient_identity_verified">
                    Availability& functionality of required
                        instruments/equipment/implants/medications
                </label>
           </div>
        </div>
        <div class="col-md-6 padding_sm">
            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">

                @if(isset($sign_in_data->anesthesia_safety_check_completed) && $sign_in_data->anesthesia_safety_check_completed == 1)
                <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                <label class="text-blue" for="anesthesia_safety_check_completed">
                    Anesthesia safety check completed
                </label>

            </div>


            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">

                @if(isset($sign_in_data->difficlty_airways) && $sign_in_data->difficlty_airways == 1)
                <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                <label class="text-blue" for="anesthesia_safety_check_completed">
                    Difficulty airway/aspiration risk
                </label>
          </div>


            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">

                @if(isset($sign_in_data->blood_product_reserved) && $sign_in_data->blood_product_reserved == 1)
                <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                <label class="text-blue" for="">
                    Blood/blood product reserved
                </label>

            </div>

            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                @if(isset($sign_in_data->risk_of_blood_loss) && $sign_in_data->risk_of_blood_loss == 1)
                <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                <label class="text-blue" for="">
                    Risk of blood loss:
                         >500ml in adult>7ml/kg in children
                </label>


            </div>
            <div class="col-md-12 padding_sm txt_center" style="margin-top: 10px">
                @if(isset($sign_in_data->antibiotic_prophylaxis) && $sign_in_data->antibiotic_prophylaxis == 1)
                <i class="fa fa-check" aria-hidden="true"></i>
                @endif
                <label class="text-blue" for="" style="float: left;clear: both;display: flex;">
                    Antibiotic prophylaxis given within last 15-60 mts
                        Drug name :
                        @if(isset($sign_in_data->antibiotic_prophylaxis_drug_name) && $sign_in_data->antibiotic_prophylaxis_drug_name != '')
                            <b>{{$sign_in_data->antibiotic_prophylaxis_drug_name}}</b>
                        @endif
                        Time :
                        @if(isset($sign_in_data->antibiotic_given_time) && $sign_in_data->antibiotic_given_time != '')
                            <b>{{date('h:i A',strtotime($sign_in_data->antibiotic_given_time))}}</b>
                        @endif
                </label>

            </div>
        </div>
</div>
<div class="col-md-12"><br><br>
    <div class="col-md-6">
        Completed by:
        @if(isset($sign_in_data->signin_completed_by))
                {{$sign_in_data->signin_completed_by}}
        @endif
    </div>
</div>
