<div class="col-md-12 theadscroll" style="position: relative; height:520px;">
    <div class="col-md-12">
        <button onclick="takePrintOtChecklist();" type="button" class="btn btn-primary pull-right">
            <i class="fa fa-print"></i> Print
        </button>
    </div>
    <div class="col-md-12" id="ResultDataContainer">
        <style>
            @media print {
                .pagebreak { page-break-before: always; } /* page-break-after works, as well */
            }

            .box-body label{
                font-size:13px !important;
                font-weight: 700 !important;
            }
            label{
                margin:10px !important;
                font-weight: 700 !important;
            }
            td{
                text-align: left !important;
            }
            .checklist_head_text{
                color:white !important;
            }
        </style>



        <div class="col-md-12 no-padding">
            <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
                <thead>
                    <tr>
                        <th style='padding: 5px 0;' colspan='4' class='text-center'>
                            <h4 class="checklist_head_text"><b>HISTOPATHOLOGY FORM</b></h4>
                        </th>
                    </tr>
                </thead>
                <tbody style=''>
                    <tr>
                        <td style=' padding-left:5px;;text-align:left; '>Patient Name</td>
                        <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data[0]->patient_name !!}</td>
                        <td style=' padding-left:5px;;text-align:left;'>UHID No.</td>
                        <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->uhid !!}</td>
                    </tr>
                    <tr>
                        <td style=' padding-left:5px;;text-align:left; '>Age &amp; Gender</td>
                        <td style=' padding-left:5px;;text-align:left; '>{!! $patient_data[0]->age   !!} / {!! $patient_data[0]->gender   !!}</td>
                        <td style=' padding-left:5px;; text-align:left;'> IP No. </td>
                        <td style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->admission_no   !!}</td>
                    </tr>
                    <tr>
                        <td rowspan='2' style=' padding-left:5px;;text-align:left;'> Address</td>
                        <td rowspan='2' style=' padding-left:5px;;text-align:left;'>{!! $patient_data[0]->address   !!}</td>
                    </tr>

                    <tr>
                        <td style=' padding-left:5px;;text-align:left; '>Current Room</td>
                        <td style=' padding-left:5px;;text-align:left; '>{{$patient_data[0]->bed_name}}</td>
                        {{-- <td rowspan="2" style=' padding-left:5px;;text-align:left; '>DOS</td>
                        <td rowspan="2" style=' padding-left:5px;; text-align:left;'>
                            @if(!empty($surgery_date))
                                {{date("Y-m-d",strtotime($surgery_date))}}
                            @endif
                        </td> --}}
                    </tr>
                </tbody>
            </table>


            <div class="col-md-12 text-center " style="padding: 10px;"></div>
            <div class="col-md-12 no-padding">
                <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
                <tr>
                    <td>
                        <label>Surgeon</label> : {{$surgery_detail_data[0]->surgeon}}
                    </td>
                    <td>
                        <label>Date</label> : {{$histo_scheduled_date}}
                    </td>
                    <td>
                        <label>Anesthesia Type</label> : {{$surgery_detail_data[0]->anaesthesia}}
                    </td>
                </tr>
                <tr>
                    <td><label>Assistant</label> : {{$histo_assistant}}
                    </td>
                    <td><label>Time</label> : {{$histo_scheduled_time}}
                    </td>
                    <td><label>Anaesthetist</label> : {{$surgery_detail_data[0]->anaesthetist}}
                    </td>
                </tr>
                <tr>
                    <td><label>Nurse</label> : {{$histo_nurse}}</td>
                    <td><label>To</label> : {{$histo_to}}</td>
                    <td></td>
                </tr>

                </table>
            </div>
            <div class="col-md-12 no-padding" style="margin-top:20px;">
                <table class='table table-condensed' border='1' bordercolor='#dddddd'  style='width: 100%; font-size: 14px; margin: 5; font-size:robotoregular; margin-top:1px;border-collapse: collapse;'>
                    <tr>
                        <td  style="width: 50%;">
                            <label for="">Clinical Diagnosis </label>
                            <br>
                            <span style="padding-left:20px !important;">{{$clinical_diagnosis}}</span>
                        </td>
                        <td  style="width: 50%;">
                            <label for="">Clinical History - Intra Operative Findings </label>
                            <br>
                            <span style="padding-left:20px !important;">{{$clinical_history}}</span>
                        </td>
                    </tr>
                    <tr>
                        <td  style="width: 50%;">
                            <label for="">Nature of Specimen </label>
                            <br>
                            <span style="padding-left:20px !important;">{{$nature_of_specimen}}</span>
                        </td>
                        <td  style="width: 50%;">
                            <label for="">Investigations Done</label>
                            <br>
                            <span style="padding-left:20px !important;">{{$investigations_done}}</span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-md-12" style="">
            <div class="col-md-9" style="width:80%;"></div>
            <div class="col-md-3" style="float:right;text-align: left;width:20%;">
                Doctor:<span id='label_or_dr_name'>{{$doctor_details[0]->doctor_name}}</span>
                <br>
                Reg No: <span id='label_or_dr_licenseno'>{{$doctor_details[0]->license_no}}</span>
                <br>
                <span id='label_or_dr_sign'></span>
                @if($signatureImage !='')
                    @php
                        $signatureImage = "data:image/jpeg;base64,".base64_decode($signatureImage);
                    @endphp
                    <img id="dr_signature" style="width:175px; height:70px;"
                    src="{{$signatureImage}}">
                @endif
            </div>
        </div>

    </div>
</div>
