<style>
    label{
        margin-right:10px;
        float:left;clear:right;
        font-size:12px !important;
    }
    .form-control{
        float: left;
        clear:right;
        margin-right:10px;

    }
    .col-md-12{
        margin-top:8px;
    }
    .radio-success{
        width: 55px !important;
        margin-left: 10px;
        float: left;
        clear:right;
    }
</style>

@php
$pre_anaesthetic_data = $resultData->pre_anaesthetic_data;
if($pre_anaesthetic_data !=''){
    $pre_anaesthetic_data = json_decode($pre_anaesthetic_data);
}
// dd($pre_anaesthetic_data);
@endphp

@if(!empty($pre_anaesthetic_data))
<div class="col-md-12 no-padding">
    <div class="col-md-12" style="white-space:nowrap;">
        <label style="margin-right:10px;float:left;clear:right;">Surgery : </label> <label style="font-weight:600;">{{$pre_anaesthetic_data->name_of_surgery}}</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Date & Time of the surgery:</label>
        <label style="font-weight:600;">@if($pre_anaesthetic_data->date_and_time_of_surgery !=''){{date('M-d-Y h:i A',strtotime($pre_anaesthetic_data->date_and_time_of_surgery))}} @endif</label>
        <label>surgeon:</label>
        <label style="font-weight:600;">{{$pre_anaesthetic_data->surgeon_name}}</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>History –Hypertension-</label>
            <div class="inline no-margin ">

                @if(isset($pre_anaesthetic_data->history_hypertension) && $pre_anaesthetic_data->history_hypertension == 1)
                    <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                @endif

                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="inline no-margin ">
                @if(isset($pre_anaesthetic_data->history_hypertension) && $pre_anaesthetic_data->history_hypertension == 2)
                    <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                @endif
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Diabetes</label>
            <div class="inline no-margin ">
                 @if(isset($pre_anaesthetic_data->diabetes) && $pre_anaesthetic_data->diabetes == 1)
                    <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
                @endif
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="inline no-margin ">
                @if(isset($pre_anaesthetic_data->diabetes) && $pre_anaesthetic_data->diabetes == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Asthma</label>
            <div class="inline no-margin ">
                @if(isset($pre_anaesthetic_data->asthma) && $pre_anaesthetic_data->asthma == 1)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="inline no-margin ">
                @if(isset($pre_anaesthetic_data->asthma) && $pre_anaesthetic_data->asthma == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Epilepsy</label>
            <div class="inline no-margin ">
                @if(isset($pre_anaesthetic_data->epilepsy) && $pre_anaesthetic_data->epilepsy == 1)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="inline no-margin ">
                @if(isset($pre_anaesthetic_data->epilepsy) && $pre_anaesthetic_data->epilepsy == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>Allergy</label>
            <div class="inline no-margin ">
                @if(isset($pre_anaesthetic_data->allergy) && $pre_anaesthetic_data->allergy == 1)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="inline no-margin ">
                @if(isset($pre_anaesthetic_data->allergy) && $pre_anaesthetic_data->allergy == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>I.H.D</label>
            <div class="inline no-margin ">
                @if(isset($pre_anaesthetic_data->i_h_d) && $pre_anaesthetic_data->i_h_d == 1)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class="inline no-margin ">
                @if(isset($pre_anaesthetic_data->i_h_d) && $pre_anaesthetic_data->i_h_d == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <label>Previous Surgery &Type of Anaesthesia</label>
        <label style="font-weight:600;width:355px;">{{$pre_anaesthetic_data->previous_surgeory_and_anesthesia}}</label>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>Thyroid</label>
            <div class=" inline no-margin " style="font-weight:600;width:60px !important;">
                @if(isset($pre_anaesthetic_data->thyroid_condition) && $pre_anaesthetic_data->thyroid_condition == 'hyper')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    Hyper
                </label>
            </div>
            <div class=" inline no-margin " style="font-weight:600;width:60px !important;">
                @if(isset($pre_anaesthetic_data->thyroid_condition) && $pre_anaesthetic_data->thyroid_condition == 'hypo')
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    Hypo
                </label>
            </div>
        </div>
        <label>Hyper History of TB</label>
        <label style="font-weight:600;width:655px;">{{$pre_anaesthetic_data->hyper_history_of_tb}}</label>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <label>Medications:</label>
        <label style="font-weight:600;width:1102px;">{{$pre_anaesthetic_data->anesthesia_medications}}</label>

    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <label>Other Relevant History:</label>
        <label style="font-weight:600;width:1175px;">{{$pre_anaesthetic_data->other_relevant_history}}</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>PHYSICAL EXAMINATION -</label>
        <div class="col-md-3 no-padding">
            <label>Icterus</label>
            <div class=" inline no-margin ">
                @if(isset($pre_anaesthetic_data->icterus) && $pre_anaesthetic_data->icterus == 1)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class=" inline no-margin ">
                @if(isset($pre_anaesthetic_data->icterus) && $pre_anaesthetic_data->icterus == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Pallor</label>
            <div class=" inline no-margin ">
                @if(isset($pre_anaesthetic_data->pallor) && $pre_anaesthetic_data->pallor == 1)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class=" inline no-margin ">
                @if(isset($pre_anaesthetic_data->pallor) && $pre_anaesthetic_data->pallor == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <label>Lymphadenopathy :</label>
            <label style="font-weight:600;width:175px;">{{$pre_anaesthetic_data->lymphadenopathy}}</label>
        </div>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <div class="col-md-3 no-padding">
            <label>Edema</label>
            <div class=" inline no-margin ">
                @if(isset($pre_anaesthetic_data->edema) && $pre_anaesthetic_data->edema == 1)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    Yes
                </label>
            </div>
            <div class=" inline no-margin ">
                @if(isset($pre_anaesthetic_data->edema) && $pre_anaesthetic_data->edema == 2)
                <label style="margin-right:0px;"><i class="fa fa-check"></i></label>
            @endif
                <label class="text-blue">
                    No
                </label>
            </div>
        </div>
        <label>Airway-Dentition:</label>
        <label style="font-weight:600;width:175px;">@if(isset($pre_anaesthetic_data->airway_dentition)){{$pre_anaesthetic_data->airway_dentition}}@endif</label>

        <label>Thyroid:</label>
        <label style="font-weight:600;width:175px;">
            @if(isset($pre_anaesthetic_data->thyroid_desc)){{$pre_anaesthetic_data->thyroid_desc}}@endif</label>
        <label>Spine</label>
        <label style="font-weight:600;width:175px;">@if(isset($pre_anaesthetic_data->spine)){{$pre_anaesthetic_data->spine}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Mallampattl Grade</label>
        <label style="font-weight:600;width:175px;">@if(isset($pre_anaesthetic_data->mallampattl_grade)){{$pre_anaesthetic_data->mallampattl_grade}}@endif</label>

        <label>TM Distance</label>
        <label style="font-weight:600;width:175px;">@if(isset($pre_anaesthetic_data->tm_distance)){{$pre_anaesthetic_data->tm_distance}}@endif</label>

    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Pulse</label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->anesthesia_pulse)){{$pre_anaesthetic_data->anesthesia_pulse}}@endif</label>
        <label>/min BP</label>

        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->anesthesia_bp)){{$pre_anaesthetic_data->anesthesia_bp}}@endif</label>
        <label>mmHg Temperature</label>

        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->anesthesia_temprature)){{$pre_anaesthetic_data->anesthesia_temprature}}@endif</label>
        <label>F: Weight</label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->anesthesia_weight)){{$pre_anaesthetic_data->anesthesia_weight}}@endif</label>kg

    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <label>C.V.S</label>
        <label style="font-weight:600;width:1133px;">@if(isset($pre_anaesthetic_data->cvs)){{$pre_anaesthetic_data->cvs}}@endif</label>

    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Respiratory System
        </label>
        <label style="font-weight:600;width:1058px;">@if(isset($pre_anaesthetic_data->respiratory_system)){{$pre_anaesthetic_data->respiratory_system}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>GIT
        </label>
        <label style="font-weight:600;width:1143px;">@if(isset($pre_anaesthetic_data->git)){{$pre_anaesthetic_data->git}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>C.N.S
        </label>
        <label style="font-weight:600;width:1130px;">@if(isset($pre_anaesthetic_data->cns)){{$pre_anaesthetic_data->cns}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Other Relevant findings
        </label>
        <label style="font-weight:600;width:1036px;">@if(isset($pre_anaesthetic_data->other_relevant_findings)){{$pre_anaesthetic_data->other_relevant_findings}}@endif</label>
    </div>

    <div class="col-md-12" style="margin:15px">
        <h4>INVESTIGATIONS</h4>
    </div>

    <div class="col-md-12" style="white-space:nowrap;">
        <label>1. Hb %
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->hb)){{$pre_anaesthetic_data->hb}}@endif</label>
        <label>TLC:
        </label>
        <label style="font-weight:600;width:25px;">>@if(isset($pre_anaesthetic_data->tlc)){{$pre_anaesthetic_data->tlc}}@endif</label>
        <label>Platelets:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->platelets)){{$pre_anaesthetic_data->platelets}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>2. Blood Sugar (mg %):Fasting
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->blood_sugar)){{$pre_anaesthetic_data->blood_sugar}}@endif</label>
        <label>PP:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->pp)){{$pre_anaesthetic_data->pp}}@endif</label>
        <label>Random:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->random)){{$pre_anaesthetic_data->random}}@endif</label>
        <label>HbA1C:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->hba1c)){{$pre_anaesthetic_data->hba1c}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>3. Blood Grouping & Typing
        </label>

        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->blood_grouping)){{$pre_anaesthetic_data->blood_grouping}}@endif</label>
        <label>Bleeding Time:
        </label>

        <label style="font-weight:600;width:50px;">@if(isset($pre_anaesthetic_data->bleeding_time)){{$pre_anaesthetic_data->bleeding_time}}@endif</label>
        <label>Clotting Time:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->clotting_time)){{$pre_anaesthetic_data->clotting_time}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>4. PT (Control
        </label>
        <label style="font-weight:600;width:15px;">@if(isset($pre_anaesthetic_data->pt_control)){{$pre_anaesthetic_data->pt_control}}@endif</label>
        <label>/test
        </label>
        <label style="font-weight:600;width:15px;">@if(isset($pre_anaesthetic_data->pt_test)){{$pre_anaesthetic_data->pt_test}}@endif</label>
        <label>)INR
        </label>
        <label style="font-weight:600;width:15px;">@if(isset($pre_anaesthetic_data->pt_inr)){{$pre_anaesthetic_data->pt_inr}}@endif</label>
        <label>)aPTT(Control
        </label>
        <label style="font-weight:600;width:15px;">@if(isset($pre_anaesthetic_data->apt_control)){{$pre_anaesthetic_data->apt_control}}@endif</label>
        <label>)/test
        </label>
        <label style="font-weight:600;width:15px;">@if(isset($pre_anaesthetic_data->apt_test)){{$pre_anaesthetic_data->apt_test}}@endif</label>
        <label>)
        </label>
        <label style="font-weight:600;width:15px;">@if(isset($pre_anaesthetic_data->apt_others)){{$pre_anaesthetic_data->apt_others}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>5. Serum creatinine
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->creatine)){{$pre_anaesthetic_data->creatine}}@endif</label>
        <label>Blood Urea:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->blood_urea)){{$pre_anaesthetic_data->blood_urea}}@endif</label>
        <label>C.U.E:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->cue)){{$pre_anaesthetic_data->cue}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>6. Serum Electrolytes Na
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->na)){{$pre_anaesthetic_data->na}}@endif</label>
        <label>K:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->k)){{$pre_anaesthetic_data->k}}@endif</label>
        <label>Others:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->serum_electrolytes_others)){{$pre_anaesthetic_data->serum_electrolytes_others}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>7. HBsAg
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->hbsag)){{$pre_anaesthetic_data->hbsag}}@endif</label>
        <label>HCV:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->hcv)){{$pre_anaesthetic_data->hcv}}@endif</label>
        <label>HIV:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->hiv)){{$pre_anaesthetic_data->hiv}}@endif</label>
        <label>X-RAY Chest:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->xray_chest)){{$pre_anaesthetic_data->xray_chest}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>8. E.C.G
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->ecg)){{$pre_anaesthetic_data->ecg}}@endif</label>
        <label>ECHO:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->echo)){{$pre_anaesthetic_data->echo}}@endif</label>
        <label>LFT:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->lft)){{$pre_anaesthetic_data->lft}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>9. TFT : TSH
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->tsh)){{$pre_anaesthetic_data->tsh}}@endif</label>
        <label>T3:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->t3)){{$pre_anaesthetic_data->t3}}@endif</label>
        <label>T4:
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->t4)){{$pre_anaesthetic_data->t4}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>Other Specific Investigations:</b>
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->other_specific_investigations)){{$pre_anaesthetic_data->other_specific_investigations}}@endif</label>

    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>PRE-OP OPINION : </b>
        </label>

        <label style="font-weight:600;width:1169px;">@if(isset($pre_anaesthetic_data->pre_op_openion)){{$pre_anaesthetic_data->pre_op_openion}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>REVIEW PAC : </b>
        </label>
        <label style="font-weight:600;width:1169px;">@if(isset($pre_anaesthetic_data->review_pac)){{$pre_anaesthetic_data->review_pac}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label><b>ASA CATEGORY:</b>
        </label>
        <label style="font-weight:600;width:25px;">@if(isset($pre_anaesthetic_data->asa_category)){{$pre_anaesthetic_data->asa_category}}@endif</label>
    </div>
    <div class="col-md-12" style="white-space:nowrap;">
        <label>Type of Anaesthesia planned & Explained to patient :
        </label>
        @if(isset($pre_anaesthetic_data->type_of_anaesthesia_planned)){{$pre_anaesthetic_data->type_of_anaesthesia_planned}}@endif

    </div>
    <div class="col-md-12" style="white-space:nowrap;margin-bottom:20px;">
        <label>Completed by :
        </label>
        <label style="font-weight:600;width:175px;">@if(isset($pre_anaesthetic_data->completed_by)){{$pre_anaesthetic_data->completed_by}}@endif</label>

        <label>Date
        </label>
        <label style="font-weight:600;width:175px;">@if($pre_anaesthetic_data->completed_date !=''){{date('M-d-Y',strtotime($pre_anaesthetic_data->completed_date))}} @endif</label>
        <label>Time
        </label>
        <label style="font-weight:600;width:175px;">@if($pre_anaesthetic_data->completed_time !=''){{date('h:i A',strtotime($pre_anaesthetic_data->completed_time))}} @endif</label>
    </div>
</div>
@endif
