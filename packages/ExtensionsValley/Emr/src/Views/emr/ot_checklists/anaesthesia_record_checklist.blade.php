<style>
    label{
        margin-right:10px;
        float:left;clear:right;
        font-size:12px !important;
    }
    .form-control{
        float: left;
        clear:right;
        margin-right:10px;

    }
    .col-md-12{
        margin-top:8px;
    }
    .radio-success{
        width: 130px;
        margin-left: 10px;
        float: left;
        clear:right;
    }
</style>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">ANAESTHESIOLOGIST 1</label><input type="text" id="ans_record_anaesthesiologist_1" value="{{$anaesthetist_name}}" class="form-control" placeholder="" style="width:480px;"/>
    <label style="margin-right:10px;float:left;clear:right;">2</label><input type="text" id="ans_record_anaesthesiologist_2" class="form-control" placeholder="" style="width:480px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">SURGERY </label><input type="text" id="ans_record_surgery" class="form-control" placeholder="" value="{{$surgery_details[0]->surgery_name}}" style="width:480px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Date</label><input type="text" id="ans_record_surgery_date" class="form-control datetimepicker" placeholder="" style="width:480px;" value="@if($surgery_details[0]->scheduled_at !=''){{date('M-d-Y h:i A',strtotime($surgery_details[0]->scheduled_at))}}@endif"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">SURGEON </label><input type="text" id="ans_record_surgeon" class="form-control" placeholder="" style="width:1100px;" value="{{$surgeon_name}}"/>

</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label>TYPE OF
        ANAESTHESIA</label>
        <div class="radio radio-success inline no-margin ">
            <input class="checkit" id="" type="radio" name="ans_record_type_anaesthesia" value='ga'>
            <label class="text-blue">
                G.A
            </label>
        </div>
        <div class="radio radio-success inline no-margin ">
            <input class="checkit" id="" type="radio" name="ans_record_type_anaesthesia" value='sa'>
            <label class="text-blue">
                S.A
            </label>
        </div>
        <div class="radio radio-success inline no-margin ">
            <input class="checkit" id="" type="radio" name="ans_record_type_anaesthesia" value='ea'>
            <label class="text-blue">
                E.A
            </label>
        </div>
        <div class="radio radio-success inline no-margin ">
            <input class="checkit" id="" type="radio" name="ans_record_type_anaesthesia" value='cse'>
            <label class="text-blue">
                CSE
            </label>
        </div>
        <div class="radio radio-success inline no-margin ">
            <input class="checkit" id="" type="radio" name="ans_record_type_anaesthesia" value='iv_sedation'>
            <label class="text-blue">
                TIVA
            </label>
        </div>
        <div class="radio radio-success inline no-margin " style="margin-left:60px !important;">
            <input class="checkit" id="" type="radio" name="ans_record_type_anaesthesia" value='mac'>
            <label class="text-blue">
                MAC
            </label>
        </div>
        <div class="radio radio-success inline no-margin ">
            <input class="checkit" id="" type="radio" name="ans_record_type_anaesthesia" value='ra'>
            <label class="text-blue">
                RA
            </label>
        </div>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">NURSE </label><input type="text" id="ans_record_nurse" class="form-control" placeholder="" style="width:450px;"/>
    <label style="margin-right:10px;float:left;clear:right;">PREMEDICATION </label><input type="text" id="ans_record_premedication" class="form-control" placeholder="" style="width:450px;"/>

</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">ANAESTHESIA ASSESSMENT BEFORE INDUCTION (not earlier than 15 mins before induction) TIME </label><input type="text" id="ans_record_assesment_before_induction" class="form-control datetimepicker" placeholder="" style="width:100px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">HR</label><input type="text" id="ans_record_hr" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">B.P</label><input type="text" id="ans_record_bp" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">SPO2</label><input type="text" id="ans_record_spo2" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">RR</label><input type="text" id="ans_record_rr" class="form-control" placeholder="" style="width:100px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">General Condition</label><input type="text" id="ans_record_general_condition" class="form-control" placeholder="" style="width:1100px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">IV Access</label><input type="text" id="ans_record_iv_access" class="form-control" placeholder="" style="width:300px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Site</label><input type="text" id="ans_record_site" class="form-control" placeholder="" style="width:300px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Cannula Guage</label><input type="text" id="ans_record_cannula_guage" class="form-control" placeholder="" style="width:300px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">REGIONAL ANAESTHESIA – Technique</label><input type="text" id="ans_record_regional_anaesthesia_technique" class="form-control" placeholder="" style="width:400px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Approach</label><input type="text" id="ans_record_approach" class="form-control" placeholder="" style="width:400px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Needle</label><input type="text" id="ans_record_needle" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Catheter Fixed At</label><input type="text" id="ans_record_catheter_fixed_at" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Space</label><input type="text" id="ans_record_space" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Drug</label><input type="text" id="ans_record_drug" class="form-control" placeholder="" style="width:100px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Onset of action</label><input type="text" id="ans_record_onset_of_action" class="form-control" placeholder="" style="width:300px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Sensory Level</label><input type="text" id="ans_record_sensory_level" class="form-control" placeholder="" style="width:300px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Motor Level</label><input type="text" id="ans_record_motor_level" class="form-control" placeholder="" style="width:300px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">GENERAL ANAESTHESIA-Preoxygenation</label><input type="text" id="ans_record_general_ans_preoxygenation" class="form-control" placeholder="" style="width:900px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Induction</label><input type="text" id="ans_record_induction" class="form-control" placeholder="" style="width:50px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Inhalational</label><input type="text" id="ans_record_inhalational" class="form-control" placeholder="" style="width:150px;"/>

    <div class="radio radio-success inline no-margin " style="margin-right:18px !important;">
        <input class="checkit" id="" type="radio" name="ans_record_type_inhalation_type" value='airway'>
        <label class="text-blue">
            Airway
        </label>
    </div>
    <div class="radio radio-success inline no-margin " style="margin-right:18px !important;">
        <input class="checkit" id="" type="radio" name="ans_record_type_inhalation_type" value='mask'>
        <label class="text-blue">
            Mask
        </label>
    </div>
    <div class="radio radio-success inline no-margin " style="margin-right:18px !important;">
        <input class="checkit" id="" type="radio" name="ans_record_type_inhalation_type" value='lma'>
        <label class="text-blue">
            LMA
        </label>
    </div>
    <div class="radio radio-success inline no-margin " style="margin-right:50px !important;">
        <input class="checkit" id="" type="radio" name="ans_record_type_inhalation_type" value='et_tube'>
        <label class="text-blue">
            ET Tube
        </label>
    </div>

    <label style="margin-right:5px;float:left;clear:right;">Other</label><input type="text" id="ans_record_inhalational_other" class="form-control" placeholder="" style="width:150px;"/>

</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">ET-Nasal/Oral,Size</label><input type="text" id="ans_record_et_nasal" class="form-control" placeholder="" style="width:50px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Cuff</label><input type="text" id="ans_record_cuff" class="form-control" placeholder="" style="width:50px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Other Information</label><input type="text" id="ans_record_other_info" class="form-control" placeholder="" style="width:50px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Ventilation-Controlled/Spontaneous , Circuit/ Polymask</label><input type="text" id="ans_record_ventilation_controlled" class="form-control" placeholder="" style="width:100px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Ventilator-MV</label><input type="text" id="ans_record_ventilator_mv" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">RR</label><input type="text" id="ans_record_rr2" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">TV</label><input type="text" id="ans_record_tv" class="form-control" placeholder="" style="width:100px;"/>
    <label style="margin-right:10px;float:left;clear:right;">I:E Ratio</label><input type="text" id="ans_record_ie_ratio" class="form-control" placeholder="" style="width:100px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Patient Position</label><input type="text" id="ans_record_patient_position" class="form-control" placeholder="" style="width:900px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">TOURNIQUET-Applied at</label><input type="text" id="ans_record_technique_applied" class="form-control" placeholder="" style="width:400px;"/>
    <label style="margin-right:10px;float:left;clear:right;">Removal at</label><input type="text" id="ans_record_removal_at" class="form-control" placeholder="" style="width:400px;"/>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">OTHER NOTE (IF ANY)</label>
    <textarea id="ans_record_other_note" class="form-control" placeholder=""></textarea>
</div>
<div class="col-md-12" style="white-space:nowrap;">
    <label style="margin-right:10px;float:left;clear:right;">Time of Induction:</label><input type="text" id="ans_record_time_of_induction" class="form-control datetimepicker" placeholder="" style="width:400px;"/>

    <label style="margin-right:10px;float:left;clear:right;">Time of Incision:</label><input type="text" id="ans_record_time_of_incision" class="form-control datetimepicker" placeholder="" style="width:400px;"/>
</div>

<div class="col-md-12 " style="margin-top:5px;margin-bottom:5px;">
    <button style="padding: 3px 3px" onclick="saveAnaesthetiaRecord();" type="button" title="Save"
        class="btn btn-success pull-right" id="save_checklistbtn">Save<i id="save_checklistspin"
            class="fa fa-save padding_sm"></i>
    </button>
    <button style="padding: 3px 3px" type="button" data-dismiss="modal" aria-label="Close"
        class="btn btn-danger pull-right">Close <i class="fa fa-times"></i></button>
</div>
