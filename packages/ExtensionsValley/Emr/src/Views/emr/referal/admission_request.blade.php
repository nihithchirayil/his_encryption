@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/ip-op-slide.css")}}" rel="stylesheet">


@endsection
@section('content-area')

<div class="right_col"  role="main">
    {!! Form::open(['name'=>'formAdmission','id'=>'formAdmission','method'=>'post']) !!}
    <div class="col-md-12 padding_sm">
        <div class="col-md-4">
                <div class="col-md-10">
                   <h3 style="margin-left: -20px;"class=""> <b style="color:green">Admission Request</b></h3>
                </div>
        </div>
        <div class="col-md-8 pull-right">
            <button type="button" style="float:right;
                float: right;
                margin-top: 10px;
                width: 120px;
                margin-right: -10px;
            " class="btn bg-green" onclick="saveAdmissionRequest();"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>

        <input type="hidden" id="base_url" value="{{URL::to('/')}}"/>
        <input type="hidden" id="c_token" value="{{csrf_token()}}">
        <input type="hidden" id="patient_id" value="{{$patient_result[0]->id}}">
        <input type="hidden" id="visit_id" value="{{$patient_result[0]->visit_id}}">
        <input type="hidden" id="investigation_head_id" value="0">
        <input type="hidden" id="encounter_id" value="{{$encounter_id}}">

    <div class="col-md-12 padding_sm">
        <table class="table table-condensed no-margin table-bordered table_sm">
            <thead>
                <tr class="headergroupbg_purple" style="width:100%;background-color:#0e8b62;color:white">
                    <th style="width:12%">UHID</th>
                    <th style="width:20%">Patient Name</th>
                    <th style="width:8%">Age/Sex</th>
                    <th style="width:20%">Address</th>
                    <th style="width:8%">Phone</th>
                    <th style="width:20%">Admitting Doctor</th>
                    <th style="width:12%">Department</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$patient_result[0]->uhid}}</td>
                    <td>{{$patient_result[0]->patient_name}}</td>
                    <td>{{$patient_result[0]->age}}/{{$patient_result[0]->gender}}</td>
                    <td>{{$patient_result[0]->address}}</td>
                    <td>{{$patient_result[0]->phone}}</td>
                    <td>{{$patient_result[0]->doctor_name}}</td>
                    <td>{{$patient_result[0]->speciality}}</td>
                </tr>
            </tbody>
        </table>
      </div>

      <div class="clearfix"></div>
      <div class="clearfix"></div>
      <div class="col-md-12 card padding_sm" style="margin-left:10px;">
        <div class="row card_body" style="margin-top:15px;">
            <div class="col-md-12 padding_sm" style="padding:10px !important;">

                <div class="col-md-6">
                    <div class="col-md-5" style="">
                        <span>Admission Type*</span>
                    </div>
                    <div class="col-md-2">:</div>
                    <div class="col-md-5">
                        <label for="emergency">Emergency</label>&nbsp;
                        <input type="radio"  name="type"  value="EM" class="" id="emergency" placeholder="">&nbsp;&nbsp;
                        <label for="general">General(Planned)</label>&nbsp;
                        <input type="radio" checked="checked" name="type"  value="General" class="" id="general" placeholder="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5" style="text-align:right;">
                        <span>Date of Admission *</span>
                    </div>
                    <div class="col-md-2">:</div>
                    <div class="col-md-5">
                        <input type="text" class="form-control datepicker"  name="admission_date" value="{{date('M-d-Y')}}" placeholder="Admission Date" id="admission_date">
                    </div>
                </div>
            </div>

            <div class="col-md-12 padding_sm " style="padding:10px !important;">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <span>Referring Doctor (if any)</span>
                    </div>
                    <div class="col-md-2">:</div>
                    <div class="col-md-5" style="text-align: right;">
                        {!! Form::select('referring_doctor', $doctorList, '', ['class' => 'form-control','placeholder' => 'Select Doctor','id' => 'referring_doctor']) !!}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="col-md-5" style="text-align: right;">
                        <span>Provisional Diagnosis</span>
                    </div>
                    <div class="col-md-2">:</div>
                    <div class="col-md-5" style="text-align: right;">
                            <textarea style="height:75px !important;" class="form-control"  name="provisional_diagnosis" id="provisional_diagnosis"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-md-12 padding_sm " style="padding:10px !important;">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <span>Precautions</span>
                    </div>
                    <div class="col-md-2">:</div>
                    <div class="col-md-5" style="text-align: right;">
                        <input type="text" class="form-control" autofocus="off" name="precautions" value="" placeholder="Precautions" id="precautions">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="col-md-5" style="text-align: right;">
                        <span>Class of Bed *</span>
                    </div>
                    <div class="col-md-2">:</div>
                    <div class="col-md-5" style="text-align: right;">
                        {!! Form::select('priority', $admPriority, "", ['class' => 'form-control','placeholder' => 'Select Admission Priority','id' => 'priority', 'style' => 'color:#555555; padding:4px 12px;']) !!}
                    </div>
                </div>
            </div>

            <div class="col-md-12 padding_sm " style="padding:10px !important;">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <span>Name of the Interventions/Procedure planned</span>
                    </div>
                    <div class="col-md-2">:</div>
                    <div class="col-md-5" style="text-align: right;">
                        <textarea style="height:75px !important;" class="form-control"  name="procedure" id="procedure"></textarea>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="col-md-5" style="text-align: right;">
                        <span>Expected number of days in the hospital</span>
                    </div>
                    <div class="col-md-2">:</div>
                    <div class="col-md-5" style="text-align: right;">
                        <input type="text" class="form-control"  name="no_of_days" value="" placeholder="Number of Days" id="no_of_days">
                    </div>
                </div>
            </div>

            <div class="col-md-12 padding_sm " style="padding:10px !important;">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <span>Tentative date of Surgery/Procedure</span>
                    </div>
                    <div class="col-md-2">:</div>
                    <div class="col-md-5" style="text-align: right;">
                        <input type="text" class="form-control datepicker" name="surgery_date" value="" placeholder="Date" id="surgery_date">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="col-md-5" style="text-align: right;">
                        <span>Any special requests/concerns</span>
                    </div>
                    <div class="col-md-2">:</div>
                    <div class="col-md-5" style="text-align: right;">
                        <input type="text" class="form-control"  name="spl_req" value="" placeholder="" id="spl_req">
                    </div>
                </div>
            </div>


            {!! Form::token() !!}
            {!! Form::close() !!}



            <div class="col-md-12">
                <h5>Investigation Orders</h5>
            </div>

            <div class="col-md-12 padding_sm">
                @include('Emr::emr.referal.admit_request_investigation')
            </div>




        </div>
      </div>










</div>

@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/investigation.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/admission_request.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/jquery-mousewheel-master/jquery.mousewheel.min.js")}}"></script>


<script type="text/javascript">

</script>
@endsection
