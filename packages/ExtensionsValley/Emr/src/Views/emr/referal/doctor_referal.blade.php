<!-- <span id="docRefSpeciality">
   Speciality
   {!! Form::select('ref_speciality', $specialityList, '', ['class' => 'form-control','placeholder' => 'Select Speciality','id' => 'ref_speciality', 'style' => 'color:#555555; padding:4px 12px;']) !!}
<br>
</span> -->
<!-- <span>Reference Type</span>
 {!! Form::select('reference_type', $referenceType, '', ['class' => 'form-control','placeholder' => 'Select Reference Type','id' => 'reference_type', 'style' => 'color:#555555; padding:4px 12px;']) !!}
<br>
</span> -->
<span>Doctor</span>
 {!! Form::select('doctor_ref', $doctorList, '', ['class' => 'form-control','placeholder' => 'Select Doctor','id' => 'doctor_ref']) !!}
<br>
{{-- <span>
    Transfer: Patient to be billed
    <b style="cursor: pointer;font-size: 16px;" title="Yes - Your appointment will cancel and refund the amount. Then patient will take a new appointment to the other Doctor.
No - Your appointment will be charged , Also the patient needs to take new appointment for the other Doctor.">
( ? )
</b>
</span> --}}
{{-- <div style="margin: 10px;">
    <label class="radio-inline no-margin">
        <input type="radio" value="1"  name="payment_status"> Yes
    </label>
    <label class="radio-inline">
        <input type="radio" value="0" checked name="payment_status">No
    </label>
</div> --}}
Notes
<textarea style="resize: vertical;min-height: 250px; max-height: 250px; border-radius: 3px; border:1px solid #dddddd; width:100%;" rows="3" cols="20" class="form-control" name ="refer_notes" placeholder=""></textarea>
<input type="hidden" name="doctor_ref_loaded" id="doctor_ref_loaded" value="1">
<script>
$('select[name="doctor_ref"]').select2({
  dropdownParent: $('#referDoctorForm')
});

$('select[name="doctor_ref1"]').select2({
  minimumInputLength: 2,
  ajax: {
        url: $('#base_url').val()+'/emr/doctor-search',
        dataType: 'json',
        data: function (params) {
              var query = {
                term: params.term,
                // speciality: $('#ref_speciality').val(),
              }
              return query;
        },
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
  }
});
</script>
