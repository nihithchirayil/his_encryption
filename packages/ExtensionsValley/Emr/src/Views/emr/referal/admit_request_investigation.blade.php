<div class="box no-border no-margin right_wrapper_box anim">
    <div class="box-body clearfix">
        <div class="col-md-12 no-padding " style="cursor: pointer;">
                   <div class="col-md-11" style="padding:0 15px 0 0;">
                    <h4 class="card_title">
                        Investigation <span class="draft_mode_indication_inv"></span>
                    </h4>
                </div>
                <div class="col-md-1 text-right"></div>
        </div>
        <div class="col-md-12" style="padding: 2px;">
            <div class="clearfix"></div>
            <div class="ht5"></div>
            <table class="table no-margin table_sm no-border">
                <tbody>
                    <tr>
                        <td width="10%">
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="inlineRadio11" value="lab" name="radioInline11" checked="" onchange="changeRadio(this)">
                                <label for="inlineRadio11"> Lab </label>
                            </div>
                        </td>
                        <td width="10%">
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="inlineRadio22" value="radiology" name="radioInline11" onchange="changeRadio(this)">
                                <label for="inlineRadio22"> Radiology </label>
                            </div>
                        </td>
                        <td>
                            <div class="radio radio-primary radio-inline">
                                <input type="radio" id="inlineRadio33" value="procedure" name="radioInline11" onchange="changeRadio(this)">
                                <label for="inlineRadio33"> Procedures </label>
                            </div>
                        </td>
                        <td>
                            <input type="text" placeholder="Search items" name="investigation_item_search_box" autocomplete="off" id="investigation_item_search_box" class="form-control">
                            <!-- investigation List -->
                            <div class="investigation-list-div" style="display: none;">
                                <a style="float: left;" class="close_btn_inv_search">X</a>
                                <div class="inv_theadscroll" style="position: relative;">
                                  <table id="InvestigationTable"  class="table table-bordered no-margin table_sm table-striped inv_theadfix_wrapper">
                                      {{-- <thead>
                                        <tr class="light_purple_bg">
                                          <th>Frequency</th>
                                        </tr>
                                      </thead> --}}
                                      <tbody id="ListInvestigationSearchData">

                                      </tbody>
                                    </table>
                                </div>
                            </div>
                          <!-- investigation List -->
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="clearfix"></div>
            <div class="ht5"></div>
            <div class="card">
                <div class="clearfix"></div>
                <div class="ht5"></div>
                <div class="box no-border no-margin">
                <div class="box-body clearfix" style="background: #FFF;">
                <div class="nav-tabs-custom blue_theme_tab no-margin">
                    <div class="col-md-3 padding_sm">
                        <div class="theadscroll" style="height:380px;background:white">
                            <ul class="nav nav-tabs sm_nav vertical_tab_btn investigation_fav_tabs_list text-center">
                        @if(sizeof($favorite_group_head)>0)
                        @foreach ($favorite_group_head as $fh)
                        <li class="{{ $loop->first ? 'active' : '' }}" id="head_tab_{{ $fh->id }}"  data-id="{{ $fh->id }}" style="display:flex;height: 35px;">

                            <input style="margin-right: 5px;margin-left: 5px;" id="checkbox_check_all_{{ $fh->id }}" class="styled fav_group_check_box" type="checkbox" name="{{$fh->id}}" value="{{$fh->id}}" onclick="check_all_investigation(this)" >
                            <a href="#invs_{{ $fh->id }}"  data-toggle="tab" aria-expanded="true" style="width:93%" onclick=" loadFavoriteItemsTab('{{ $fh->id }}'); ">
                              <h5 style="margin-top:5px;margin-left:5px;">{{ $fh->group_name }}</h5>
                            </a>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                        </div>
                    </div>
                    <div class="col-md-6 padding_sm">
                        <div class="theadscroll" style="height: 250px;">
                        <div class="tab-content" id="investigation_tab_content" style="margin-top:5px;">
                         @if(sizeof($favorite_group_head)>0)
                        @foreach ($favorite_group_head as $fn=>$fh)
                        <div @if($fn == 0) class ="tab-pane fade active in" @else class="tab-pane fade" @endif  id="invs_{{$fh->id}}" >
                              <div class="ggg">
                                    @if(sizeof($favorite_group_items)>0)
                                        @foreach ($favorite_group_items as $fi)
                                        @if($fi->group_id == $fh->id)
                                        @if($fi->checked_status=='checked_true')
                                        @php
                                        $checked_status = 'checked=""';
                                        @endphp
                                        @else
                                        @php
                                        $checked_status = '';
                                        @endphp
                                        @endif

                                        <div class="checkbox checkbox-primary chekbox_table_lab">
                                            <input id="{{$fi->service_code}}" class="styled {{$fh->id}}" type="checkbox" <?php echo $checked_status ?> onclick="InsertInvestigation(this)" data-id="undefined" value="{{$fi->service_code}}">
                                            <label for="{{$fi->service_code}}" class="{{$fi->service_code}}">{{$fi->service_desc}}</label>
                                        </div>

                                        @endif
                                        @endforeach
                                        @endif
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                    </div>
                    <textarea class="form-control" name="inv_remarks" id="inv_remarks" onblur="saveInvRemarks()" placeholder="Remarks"></textarea>
                    </div>
                    <div class="col-md-3">
                        <div class="box no-border no-margin anim">
                            <div class="box-body clearfix theadscroll" id="investigation_clipboard" style="padding:5px !important;font-size:12px !important;height:120px;">

                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Modal -->
<div class="modal fade" id="investigation_det_list_modal" tabindex="-1" role="dialog" aria-labelledby="investigation_det_list_modal" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Details</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">

                    <table class="table no-margin table_sm no-border">
                        <thead>
                        </thead>
                        <tbody id="investigation_det_list_modal_content">

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>

    </div>
</div>
