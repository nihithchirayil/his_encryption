@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->
    {{-- {!! Html::style('packages/extensionsvalley/default/css/bootstrap.min.css') !!} --}}


@stop

@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <style>
    </style>
@endsection
@section('content-area')
    <!-- page content -->
    <div class="right_col">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <div class="row col-md-12" style="text-align: right; font-size: 12px;font-weight: bold;"> {{ $title }}
            <div class="clearfix"></div>
        </div>
        <div class="row codfox_container">
            <div class="col-md-12 padding_sm">
                <div class="col-md-12 no-padding">
                    <div class="box no-border">
                        <div class="box-body clearfix">



                            <div class="col-md-4 padding_sm" style="margin-top: 25px; margin-left: 1160px;">
                                <button data-toggle="modal" data-target="#critical_equipment_downtime_modal"
                                    class="btn bg-primary"><i class="fa fa-plus"></i> Add</button>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="box no-border no-margin">
                    <div class="box-body clearfix">
                        <div class="theadscroll" style="position: relative; height: 350px;">
                            <table
                                class="table no-margin theadfix_wrapper table-striped no-border table-condensed styled-table"
                                style="border: 1px solid #CCC;">
                                <thead>
                                    <tr class="table_header_bg" style="cursor: pointer;">
                                        <th>VAP</th>
                                        <th>Pressure Ulcer Rate</th>
                                        <th>Surgical Site Infection </th>
                                        <th>Hand Hygene Rate</th>
                                        <th>Catheter Associated UTI</th>
                                        <th>Created By</th>
                                        <th>Created At</th>
                                        <th>Delete/Edit</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if (count($item) > 0)
                                        @foreach ($item as $item)

                                            <tr style="cursor: pointer;">

                                                <td class="">{{ $item->vap }}<input type="hidden"
                                                        class="fav_check" name="vap[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="vap[]" value="{{ $item->vap }}">
                                                </td>
                                                <td class="">{{ $item->pressure_ulcer_rate }}<input
                                                        type="hidden" class="fav_check" name="pressure_ulcer_rate[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="uhid[]"
                                                        value="{{ $item->pressure_ulcer_rate }}">
                                                </td>

                                                <td class="">{{ $item->surgical_site_infection }}<input
                                                        type="hidden" class="fav_check"
                                                        name="surgical_site_infection[]" value="{{ $item->id }}"><input
                                                        type="hidden" class="form-control"
                                                        name="surgical_site_infection[]"
                                                        value="{{ $item->surgical_site_infection }}">
                                                </td>
                                                <td class="">{{ $item->hand_hygiene_rate }}<input
                                                        type="hidden" class="fav_check" name="hand_hygiene_rate[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="hand_hygiene_rate[]"
                                                        value="{{ $item->hand_hygiene_rate }}">
                                                </td>
                                                <td class="">{{ $item->catheter_associated_uti }}<input
                                                        type="hidden" class="fav_check"
                                                        name="catheter_associated_uti[]" value="{{ $item->id }}"><input
                                                        type="hidden" class="form-control"
                                                        name="catheter_associated_uti[]"
                                                        value="{{ $item->catheter_associated_uti }}">
                                                </td>
                                                <td class="common_td_rules">{{ $item->created_by }}<input type="hidden"
                                                        class="fav_check" name="created_by[]"
                                                        value="{{ $item->id }}"><input type="hidden"
                                                        class="form-control" name="created_by[]"
                                                        value="{{ $item->created_by }}">
                                                </td>
                                                <td class="common_td_rules">
                                                    {{ date('M-d-Y h:i A', strtotime($item->start_time)) }}<input
                                                        type="hidden" class="form-control" name="fav_start_time[]"
                                                        value="{{ date('h:i A', strtotime($item->start_time)) }}"><input
                                                        type="hidden" class="form-control" name="fav_start_date[]"
                                                        value="{{ date('M-d-Y', strtotime($item->start_time)) }}">
                                                </td>

                                                <td class="">
                                                    <button class='btn btn-sm btn-default delete-critical-downtime'><i
                                                            class="fa fa-trash"></i></button><button
                                                        class='btn btn-sm btn-default edit-critical-downtime'><i
                                                            class="fa fa-edit"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="12" class="location_code">No Records found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 text-center">
                            <ul class="pagination purple_pagination pull-right">
                                {!! $page_links !!}
                            </ul>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="domain_url" id="domain_url" value="{{ url('/') }}" />
            </div>
        </div>
    </div>
    <!-- doctor service division modal start -->
    <div id="critical_equipment_downtime_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Ifection Control Params</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['infection_params_list_form', 'id' => 'infection_params_list_form']) !!}

                    <input type="hidden" class="form-control" name="edit_infection_params_list"
                        id="edit_infection_params_list">

                    <div class="col-md-12 padding_sm" style="margin-top:8px;padding:10px !important;">
                        <div class="col-md-6 padding_sm">

                            <label for="">VAP(Ventilator Associated Pneumonia)</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" name='vap' id='vap' class="form-control numeric_only"
                                value="">
                        </div>
                        <div class="col-md-6 padding_sm">
                            <label for="">Pressure Ulcer Rate</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" name='pressure_ulcer_rate' id='pressure_ulcer_rate'
                                class="form-control numeric_only" value="">
                        </div>
                    </div>

                    <div class="col-md-12 padding_sm">

                        <div class="col-md-6 padding_sm">
                            <label for="">Surgical Site Infection</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" name='surgical_site_infection'
                                id='surgical_site_infection' class="form-control numeric_only" value="">
                        </div>
                        <div class="col-md-6 padding_sm">
                            <label for="">Hand Hygene Rate</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" name='hand_hygiene_rate' id='hand_hygiene_rate'
                                class="form-control numeric_only" value="">
                        </div>
                    </div>
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-6 padding_sm">
                            <label for="">Catheter Associated UTI</label>
                            <div class="clearfix"></div>
                            <input type="text" autocomplete="off" name='catheter_associated_uti'
                                id='catheter_associated_uti' class="form-control numeric_only" value="">
                        </div>
                    </div>

                    <br>
                    <div class="col-md-12 padding_sm" style="margin-top:8px;padding:10px !important;">
                        <div class="col-md-6 padding_sm">
                            {!! Form::label('start_date', 'Starts At') !!}
                            <input type="text" id='start_date' name='start_date' class="form-control datepicker" value=""
                                data-attr="date" placeholder="Start Date">
                        </div>
                        <div class="col-md-6 padding_sm">
                            {!! Form::label('start_time', '&nbsp;') !!}
                            <input type="text" id='start_time' name='start_time' class="form-control timepicker" value=""
                                data-attr="date" placeholder="Start Time">
                        </div>
                    </div>


                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-green" onclick="addInfectionParams()"> <i
                            class="fa fa-check"></i>
                        Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    {!! Html::script('packages/extensionsvalley/default/canvas/js/jsonh.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/ion.rangeSlider.min.js') !!}

    {!! Html::script('packages/extensionsvalley/default/js/jquery-1.11.3.js') !!}
    {!! Html::script('packages/extensionsvalley/default/js/bootstrap.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/momentjs/moment.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}

    <script src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {


            setTimeout(function() {
                $('.theadscroll').perfectScrollbar("update");
                $(".theadfix_wrapper").floatThead('reflow');

            }, 300);

            $('.numeric_only').keyup(function() {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            });

            var $table = $('table.theadfix_wrapper');
            $table.floatThead({
                scrollContainer: function($table) {
                    return $table.closest('.theadscroll');
                }

            });
            $('.datepicker').datetimepicker({
                format: 'DD-MMM-YYYY'
            });
            $('.timepicker').datetimepicker({
                format: 'hh:mm A'
            });
            //  $('.date_time_picker').datetimepicker();
            $('.theadscroll').perfectScrollbar({
                wheelPropagation: true,
                minScrollbarLength: 30
            });



        });
    </script>

@stop

@section('javascript_extra')
    <script type="text/javascript">
        function addInfectionParams() {
            let edit_infection_params_list = $("#edit_infection_params_list").val();
            let vap = $("#vap").val();
            let pressure_ulcer_rate = $("#pressure_ulcer_rate").val();
            let surgical_site_infection = $("#surgical_site_infection").val();
            let catheter_associated_uti = $("#catheter_associated_uti").val();
            let hand_hygiene_rate = $("#hand_hygiene_rate").val();
            let start_date = $("#start_date").val();
            let start_time = $("#start_time").val();

            if (start_date == '') {
                toastr.warning("Start Date Required");
                return;
            } else if (start_time == '') {
                toastr.warning("Start Time Required");
                return;
            } else {

                if (edit_infection_params_list != '' && edit_infection_params_list != 0) {
                    deleteInfectionParams(edit_infection_params_list);
                }

                var url = $('#base_url').val() + "/infection_control/save_infection";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'edit_infection_params_list=' + edit_infection_params_list + '&start_date=' + start_date +
                        '&start_time=' + start_time + '&vap=' + vap +
                        '&pressure_ulcer_rate=' + pressure_ulcer_rate + '&surgical_site_infection=' +
                        surgical_site_infection +
                        '&catheter_associated_uti=' + catheter_associated_uti + '&hand_hygiene_rate=' +
                        hand_hygiene_rate,
                    beforeSend: function() {
                        $("body").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                    },
                    success: function(response) {
                        $("body").LoadingOverlay("hide");
                        if (response.status == 1) {
                            Command: toastr["success"]("Saved.");
                            window.location.href = $('#domain_url').val() +
                            "/infection_control/InfectionControlList/";

                        }
                        else {
                            Command: toastr["error"]("Error.");
                        }
                    },
                    complete: function() {}
                });

            }




        }

        $(document).on('click', '.delete-critical-downtime', function() {
            if (confirm("Are you sure you want to delete.!")) {
                let tr = $(this).closest('tr');
                let edit_id = $(tr).find('input[name="sent_evnt_id[]"]').val();

                if (edit_id != '' && edit_id != 0) {
                    deleteSentinalRowFromDb(edit_id);
                    $(tr).remove();
                } else {
                    $(tr).remove();
                }
            }
        });

        //delete single row from db
        function deleteSentinalRowFromDb(id) {
            var url = $('#base_url').val() + "/sentinalevent/delete-critical-downtime";
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id,
                    _token: _token,
                },
                beforeSend: function() {
                    $("body").LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#337AB7'
                    });
                },
                success: function(data) {
                    if (data != '' && data != undefined && data != 0) {
                        Command: toastr["success"]("Deleted.");
                    }
                },
                complete: function() {
                    $("body").LoadingOverlay("hide");
                }
            });
        }

        function deleteSentinalRow(id) {
            var url = $('#base_url').val() + "/sentinalevent/delete-critical-downtime";
            let _token = $('#c_token').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id,
                    _token: _token,
                },
                beforeSend: function() {

                },
                success: function(data) {

                },
                complete: function() {

                }
            });
        }

        function SelectedPatient(id, name, uhid) {

            $('#patient_general_search_txt').val(name + '[' + uhid + ']');
            $('#patient_id_hidden').val(id);
            $('.ajaxSearchBox').css("display", "none");
        }

        function select_scrub(id, name) {
            $('#scrub_search_txt').val(name);
            $('#scrub_id_hidden').val(id);
            $('.ajaxSearchBox').css("display", "none");
        }

        function select_technition(id, name) {
            $('#technition_search_txt').val(name);
            $('#technition_id_hidden').val(id);
            $('.ajaxSearchBox').css("display", "none");
        }

        function technition_search(txt) {
            var last_search_key = '';
            var request_flag = '';
            if (txt.length >= 2 && $.trim(txt) != '') {



                if (last_search_key == txt || request_flag == 1) {
                    return false;
                }
                last_search_key = txt;
                var url = $('#base_url').val() + "/surgery/TechnitionSearch";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'txt=' + txt,
                    beforeSend: function() {
                        $("#completetechnitionbox").empty();
                        $("#completetechnitionbox").show();
                        $(".input_rel .input_spinner").removeClass('hide');
                        $("#completetechnitionbox").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                        request_flag = 1
                    },
                    success: function(data) {

                        $("#completetechnitionbox").LoadingOverlay("hide");
                        $("#completetechnitionbox").html(data);
                        $(".input_rel .input_spinner").addClass('hide');
                        request_flag = 0;
                    }
                });

            } else if (txt.length === 0) {
                $("#completetechnitionbox").hide();
                $("#completetechnitionbox").LoadingOverlay("hide");
            }
        }


        function scrub_search(txt) {
            var last_search_key = '';
            var request_flag = '';
            if (txt.length >= 2 && $.trim(txt) != '') {



                if (last_search_key == txt || request_flag == 1) {
                    return false;
                }
                last_search_key = txt;
                var url = $('#base_url').val() + "/surgery/scrubSearch";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'txt=' + txt,
                    beforeSend: function() {
                        $("#completescrubbox").empty();
                        $("#completescrubbox").show();
                        $(".input_rel .input_spinner").removeClass('hide');
                        $("#completescrubbox").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                        request_flag = 1
                    },
                    success: function(data) {

                        $("#completescrubbox").LoadingOverlay("hide");
                        $("#completescrubbox").html(data);
                        $(".input_rel .input_spinner").addClass('hide');
                        request_flag = 0;
                    }
                });

            } else if (txt.length === 0) {
                $("#completescrubbox").hide();
                $("#completescrubbox").LoadingOverlay("hide");
            }
        }



        function patient_general_search(txt) {
            var last_search_key = '';
            var request_flag = '';
            if (txt.length >= 2 && $.trim(txt) != '') {



                if (last_search_key == txt || request_flag == 1) {
                    return false;
                }
                last_search_key = txt;
                var url = $('#base_url').val() + "/surgery/PatientSearch";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: 'txt=' + txt,
                    beforeSend: function() {
                        $("#completepatientbox").empty();
                        $("#completepatientbox").show();
                        $(".input_rel .input_spinner").removeClass('hide');
                        $("#completepatientbox").LoadingOverlay("show", {
                            background: "rgba(255, 255, 255, 0.7)",
                            imageColor: '#337AB7'
                        });
                        request_flag = 1
                    },
                    success: function(data) {

                        $("#completepatientbox").LoadingOverlay("hide");
                        $("#completepatientbox").html(data);
                        $(".input_rel .input_spinner").addClass('hide');
                        request_flag = 0;
                    }
                });

            } else if (txt.length === 0) {
                $("#completepatientbox").hide();
                $("#completepatientbox").LoadingOverlay("hide");
            }
        }

        $(document).on('click', '.edit-critical-downtime', function() {

            $('#critical_equipment_downtime_modal').modal('show');
            let tr = $(this).closest('tr');
            let edit_id = $(tr).find('input[name="sent_evnt_id[]"]').val();
            let fav_event_name = $(tr).find('input[name="fav_event_name[]"]').val();
            let fav_event_stop_time = $(tr).find('input[name="fav_event_stop_time[]"]').val();
            let fav_event_stop_date = $(tr).find('input[name="fav_event_stop_date[]"]').val();
            let fav_event_start_time = $(tr).find('input[name="fav_event_start_time[]"]').val();
            let fav_event_start_date = $(tr).find('input[name="fav_event_start_date[]"]').val();
            let fav_name = $(tr).find('input[name="fav_name[]"]').val();
            let fav_description = $(tr).find('input[name="fav_description[]"]').val();

            if (fav_name != '' && edit_id != '') {

                $("#event_type option[value=" + fav_name + "]").attr('selected', 'selected');
                $('input[name="event_name"]').val(fav_event_name);
                $('input[name="event_stop_time"]').val(fav_event_stop_time);
                $('input[name="event_stop_date"]').val(fav_event_stop_date);
                $('input[name="event_start_time"]').val(fav_event_start_time);
                $('input[name="event_start_date"]').val(fav_event_start_date);
                $('#event_description').val(fav_description)
                $('input[name="edit_sent_evnt"]').val(edit_id);

            }
        });
    </script>

@endsection
