@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/common-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
        rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/ip-op-slide.css') }}" rel="stylesheet">

@endsection
<style>
    .ajaxSearchBox {
        margin-top: -12px !important;
    }

    .search_header {
        background: #36A693 !important;
        color: #FFF !important;
    }

    .box_header {
        background: #3b926a !important;
        /* background: #5397b1 !important; */
        color: #FFF !important;
    }

    .mate-input-box {
        width: 100% !important;
        position: relative !important;
        padding: 15px 4px 4px 4px !important;
        border-bottom: 1px solid #01A881 !important;
        box-shadow: 0 0 3px #CCC !important;
    }

    .mate-input-box label {
        position: absolute !important;
        top: -2px !important;
        left: 6px !important;
        font-size: 12px !important;
        font-weight: 700 !important;
        color: #107a8c !important;
        padding-top: 2px !important;
    }
</style>
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="hospital_header" value="{{ $hospital_header }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="user_id" value="{{ $user_id }}">
    <input type="hidden" id="head_id_hidden" value=0>
    <input type="hidden" id="bill_tag_hidden" value=0>
    <input type="hidden" id="package_id_hidden" value=0>
    <input type="hidden" id="user_name" value="{{ $user_name }}">


    <div class="right_col">
        <div class="row">
            <div class="col-md-12 padding_sm">
                <div id="filter_area" class="col-md-12"
                    style="padding-left:0px !important;width: 101.75%;margin-left: -10px;">
                    <div class="box no-border no-margin">
                        <div class="box-body" style="padding-bottom:15px;">
                            <table class="table table-contensed table_sm " style="margin-bottom:10px;">
                                <thead>
                                    <tr class="table_header_common">
                                        <th colspan="11">{{ $title }}

                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="row">
                                <div class="col-md-12 padding_sm" style="">
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">UHID</label>
                                            <input class="form-control hidden_search reset" value=""
                                                autocomplete="off" type="text" id="patient_uhid" name="patient_uhid" />
                                            <div id="patient_uhidAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters reset" value="" type="hidden"
                                                name="patient_uhid_hidden" value="" id="patient_uhid_hidden">
                                            <button type="button" class="btn btn-sm btn-primary advanceSearchBtn"
                                                style=" position: absolute; top: 15px; right: 0;"><i
                                                    class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">Patient Name</label>
                                            <input class="form-control hidden_search reset" value="" 
                                                autocomplete="off" type="text" id="patient_name" name="patient_name" />
                                            <div id="patient_nameAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters reset" value="" type="hidden"
                                                name="patient_name_hidden" value="" id="patient_name_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">IP Number</label>
                                            <input class="form-control hidden_search reset" value=""
                                                autocomplete="off" type="text" id="ip_no" name="ip_no" />
                                            <div id="ip_noAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters reset" value="" type="hidden"
                                                name="ip_no_hidden" value="" id="ip_no_hidden">
                                        </div>
                                    </div>

                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box">
                                            <label class="filter_label">Bill Number</label>
                                            <input class="form-control hidden_search reset" value=""
                                                autocomplete="off" type="text" id="bill_no" name="bill_no" />
                                            <div id="bill_noAjaxDiv" class="ajaxSearchBox"></div>
                                            <input class="filters" value="" type="hidden" name="bill_no_hidden"
                                                value="" id="bill_no_hidden">
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box ">
                                            <label class="filter_label ">Prepared By</label>
                                            <select name="prepared_by" class="form-control select2 reset"
                                                id="prepared_by">
                                                <option value="">Select</option>
                                                @foreach ($prepared_by as $data)
                                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm">
                                        <div class="mate-input-box ">
                                            <label class="filter_label ">Admitting Dr</label>
                                            <select name="admitting_dr" class="form-control select2 reset"
                                                id="admitting_dr">
                                                <option value="">Select</option>
                                                @foreach ($doc as $data)
                                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    


                                </div>
                                <div class="col-md-12 padding_sm" >
                                   
                                    <div class="col-md-2  padding_sm">
                                        <div class="mate-input-box ">
                                            <label class="filter_label ">Consulting Dr</label>
                                            <select name="consulting_dr" class="form-control select2 reset"
                                                id="consulting_dr">
                                                <option value="">Select</option>
                                                @foreach ($doc as $data)
                                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 padding_sm" style="margin-top:19px">
                                        <div class="checkbox checkbox-success inline">
                                            <input id="death_patient" class="checkit" type="checkbox" name="death_patient">
                                            <label class="text-blue " for="death_patient">
                                                <strong>Death Patient</strong>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-md-1 padding_sm">
                                        <div class="mate-input-box">
                                            <label>From Date</label>
                                            <input type="text" data-attr="date" autocomplete="off" name="from_date"
                                                value="{{ date('M-d-y') }}"
                                                class="form-control datepicker filters reset" placeholder="YYYY-MM-DD"
                                                id="from_date">
                                        </div>
                                    </div>
                                    <div class="col-md-1 padding_sm">
                                        <div class="mate-input-box">
                                            <label>To Date</label>
                                            <input type="text" data-attr="date" autocomplete="off" name="to_date"
                                                value="{{ date('M-d-y') }}"
                                                class="form-control datepicker filters reset" placeholder="YYYY-MM-DD"
                                                id="to_date">
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="margin-top: -4px;">
                                        <div class="col-md-12 padding_sm ">
                                            <div class="radio radio-success inline no-margin ">
                                                <input class="checkit " id="paid_bill" type="radio" name="paid_status"
                                                    value=5>
                                                <label class="text-blue" for="paid_bill">
                                                    <strong class="green">
                                                        Paid Bills
                                                    </strong>
                                                </label><i class="fa fa-square paid-tx" style="margin-left: 34px;"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-12 padding_sm ">
                                            <div class="radio radio-success inline no-margin ">
                                                <input class="checkit" id="Unpaid_bill" type="radio" name="paid_status"
                                                    value=6>
                                                <label class="text-blue" for="Unpaid_bill">
                                                    <strong class="green">
                                                        Unpaid Bills
                                                    </strong>
                                                </label> <i class="fa fa-square unpaid-tx" style="margin-left: 15px;"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-12 padding_sm ">
                                            <div class="radio radio-success inline no-margin ">
                                                <input class="checkit" id="cancelled_bill" type="radio"
                                                    name="paid_status" value=6>
                                                <label class="text-blue" for="cancelled_bill">
                                                    <strong class="green">
                                                        Cancelled Bills
                                                    </strong>
                                                </label> <i class="fa fa-square cancelled-tx"></i>
                                            </div>
                                        </div>
                                    
                                    </div>
                                    <div class="col-md-1 padding-sm ">
                                        {{-- <button type="button" class="btn bg-green btn-block pull-right"
                                            id="pay_bill_btn" style="height:50px;">
                                            <i class="fa fa-credit-card"></i> Pay Bill
                                        </button> --}}
                                    </div>

                                    <div class="col-md-2 pull-right" style="">
                                        <button type="button" title="Search" class="btn btn-primary btn-block "
                                            id="DischargebilllistBtn" onclick="getDischargeBillList()"><i
                                                id="Dischargebilllistspin"
                                                class="padding_sm fa fa-search search_btn"></i>Search
                                            Bill</button>

                                        <button type="button" title="Reset" class="btn btn-default  btn-block "
                                            id="pharmacybilllistResetBtn" onclick="reset()"><i id=""
                                                class="padding_sm fa fa-refresh"></i>Refresh</button>
                                    </div>






                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row padding_sm">
                <div class="col-md-12 padding_sm"
                style="min-height: 20px;box-shadow: 0px 0px 1px 0px;width: 100%;margin-left: -2px;margin-top: 10px;">
                <div class="col-md-6 padding_sm" style="margin:0px;border-right: 2px solid;margin-top: 3px;">

                        <div class="col-md-3 cash_card_bg">
                            <div class="checkbox checkbox-success inline">
    
                                <input type="checkbox" id="cash_card" name="cash_card" class="checkit" value="1" />
                                <label for="cash_card">Cash/Card</label>&nbsp;&nbsp;
                            </div>
                        </div>
                        <div class="col-md-3 credit_bg">
                            <div class="checkbox checkbox-success inline">
    
                                <input type="checkbox" id="credit" name="credit" class="checkit" value="2" />
                                <label for="credit">Credit</label>&nbsp;&nbsp;
                            </div>
    
                        </div>
                        <div class="col-md-3 insurance_bg">
                            <div class="checkbox checkbox-success inline">
    
                                <input type="checkbox" id="insurance" name="insurance" class="checkit" value="3" />
                                <label for="insurance">Insurance</label>&nbsp;&nbsp;
                            </div>
                        </div>
                        <div class="col-md-3 company_credit_bg">
                            <div class="checkbox checkbox-success inline">
    
                                <input type="checkbox" id="company_credit" name="company_credit" class="checkit"
                                    value="4" />
                                <label for="company_credit">Company Credit</label>&nbsp;&nbsp;
                            </div>
                        </div>
    
                    </div>
                    <div class="col-md-6 padding_sm" style="padding: 2px !important;">

                        <div class="col-md-4 padding_sm draft_bill">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="draft_bills" type="radio" name="bill_type"
                                    value=1>
                                <label class="text-blue" for="draft_bills">
                                   Draft Bills
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm discharge-bg">
                            <div class="radio radio-success inline no-margin">
                                <input class="checkit" id="discharge_bill" type="radio" name="bill_type" value=4>
                                <label class="text-blue" for="discharge_bill">
                                    Discharge Bills
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4 padding_sm full-return-bg">
                            <div class="radio radio-success inline no-margin ">
                                <input class="checkit" id="exclude_cancelled_bills" type="radio" name="bill_type" value=5>
                                <label class="text-blue" for="exclude_cancelled_bills">
                                    Exclude Cancelled Bills
                                </label>
                            </div>
                        </div>
                        
                    </div>
                    
                    
                </div>
            </div>
            <div class="col-md-12 padding_sm"
                style="height: 370px;box-shadow: 0px 0px 1px 0px;width: 100%;margin-left: -3px;margin-top: 10px;"
                id="Discharge_bill_data">
            </div>
           

        </div>
    </div>

    <!-------modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="cancelBillSetup">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header table_header_common">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h3 class="modal-title">Bill Cancellation</h3>
                </div>
                <div class="modal-body" style="min-height:313px;" id="cancelBillSetupBody">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="col-md-6">
                                <div class="mate-input-box">
                                    <label class="filter_label " for="">Bill number:</label><input
                                        class="form-control" type="text" id="cancel_bill" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Patient
                                        name:</label><input class="form-control" type="text" id="cancel_patient"
                                        readonly></div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px">
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Payment
                                        Type:</label></b><input class="form-control" type="text" id="cancel_paytype"
                                        readonly></div>
                            </div>
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Cancel Requested
                                        By</label></b><input class="form-control" type="text" id="cancel_by" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px">
                            <div class="col-md-12">
                                <div class="mate-input-box" style="height:  !important;height: 141px !important;">
                                    <label class="filter_label " for="">Cancel Reason:</label>
                                    <div class="clearfix"></div>
                                    <textarea name="" id="Cancel_reason" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-6 pull-right">
                            <div class="col-md-3 pull-right"><button type="button" class="btn btn-primary" style="margin-left: -36px;"
                                    onclick="cancelBill()" id="Cancel"><i id="Cancel_spin"
                                        class="fa fa-trash padding_sm"></i>Cancel Bill</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------print modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header box_header" style="">
                    <h4 class="modal-title" style="color: white;float:left">Print Configuration</h4>
                    <button type="button" class="close" onclick="" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12">
                        <span style="margin-right: 5%;">Print Mode :</span>
                        <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" id="printMode"
                            value="1">Portrait
                        <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" id="printMode"
                            value="2">Landscape
                    </div>
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="showTitle" id="showTitle">
                        Include Hospital Header
                        <input style="margin-left: 15px;" type="checkbox" name="is_duplicate" id="duplicate">
                        Duplicate Print
                    </div>

                    <div class="col-md-12" style="margin-top:4px;">
                        <button onclick="printBillDetails()" class="btn bg-primary pull-right" style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---------------------pending bill model------------------------->
    <div class="modal" tabindex="-1" role="dialog" id="pendinBill">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header box_header">
                    <button type="button" class="close close_white" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" style="font-size: 30px;">&times;</span></button>
                    <h3 class="modal-title">PENDING REQUEST</h3>
                </div>
                <div class="modal-body" style="min-height:313px;" id="pendingBillSetupBody">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="col-md-6">
                                <div class="mate-input-box">
                                    <label class="filter_label " for="">Bill number:</label><input
                                        class="form-control" type="text" id="pending_bill" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Patient
                                        name:</label><input class="form-control" type="text" id="pending_patient"
                                        readonly></div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px">
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Payment
                                        Type:</label></b><input class="form-control" type="text" id="pending_paytype"
                                        readonly></div>
                            </div>
                            <div class="col-md-6">
                                <div class="mate-input-box"><label class="filter_label " for="">Pending Requested
                                        By</label></b><input class="form-control" type="text" id="pending_by"
                                        readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px">
                            <div class="col-md-12">
                                <div class="mate-input-box" style="height:  !important;height: 141px !important;">
                                    <label class="filter_label " for="">Pending Reason:</label>
                                    <div class="clearfix"></div>
                                    <textarea name="" id="Pending_reason" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-6 pull-right">
                            <div class="col-md-3 pull-right"><button type="button" class="btn btn-primary"
                                    onclick="pendingBill()" id="pending"> <i id="pending_spin" class="fa fa-save"></i>
                                    Request</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------service print modal---------------->
    <div class="modal" tabindex="-1" role="dialog" id="serv_print_config_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(91 110 91);">
                    <h4 class="modal-title" style="color: white;">Print Configuration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:120px;">
                    <div class="col-md-12" style="margin-top:10px;margin-left: -15px;">
                        <input style="margin-left: 15px;" type="checkbox" name="titleShow" id="titleShow">
                        Include Hospital Header
                        <input style="margin-left: 15px;" type="checkbox" name="is_duplicate" id="is_duplicate">
                        Duplicate Print
                    </div>
                    <div class="col-md-12" style="margin-top:10px;">
                        <button onclick=" PrintBill();" class="btn bg-primary pull-right" style="color:white">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!---------------------cash recive modal--------------------------->

    @include('Emr::pharmacy_bill_list.cash_receive_model')


    @include('Master::RegistrationRenewal.advancePatientSearch')

@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/discharge_bill_list.js') }}"></script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/report_csvprint.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/master/default/javascript/exporttoxlsx.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

    <script src="{{ asset('packages/extensionsvalley/emr/js/cash_receive.js') }}"></script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/bootstrap4-toggle.min.js') }}"></script>


@endsection
