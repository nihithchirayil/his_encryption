<style>
    .popupDiv {
        position: absolute;
    }

    .list-group-item:first-child {
        border-top-left-radius: 0px !important;
        border-top-right-radius: 0px !important;

    }

    .list-group-item {
        padding: 10px 12px !important;
    }

    .popDiv {
        display: none;
        /* background: white; */
        border-radius: 2px;
        /* box-shadow: 0px 0px 4px #d0d0d0; */
        padding: 0px;
        position: absolute;
        top: -20px;
        min-width: 162px;
        z-index: 850 !important;
        width: 150px;
        min-height: 104px;
        left: 24px;
    }

    .popDiv .show {
        display: block;
    }

    .list-group-item {
        background-color: #ffffff !important;
    }

    button.close {
        margin: 1px -6px !important;
    }

    .list-group-item {
        padding: 2px 8px !important;
    }

    .list-group-item:hover {
        background-color: whitesmoke !important;
    }

    .pop_closebtn {
        background: #333 none repeat scroll 0 0;
        border-radius: 50%;
        box-shadow: 0 0 5px #a2a2a2;
        color: #fff;
        cursor: pointer;
        font-size: 12px;
        font-weight: bold;
        height: 20px;
        padding: 2px 6px;
        position: absolute;
        right: -10px;
        top: -4px;
        min-width: 20px;
    }

    .search_header {
        background: #36A693 !important;
        color: #FFF !important;
    }

    .close {
        opacity: 1 !important;
        color: black !important;
    }

    .ps-scrollbar-x-rail {
        left: 11px !important;
    }
</style>

<div class="theadscroll col-md-12 padding_sm" style="width: 100% !important;margin-left:-9px;">
    <div class="col-md-12" style="width: 187%;height:337px;">
        <table id="result_data_table" class='table table-condensed table_sm table-col-bordered'
        style="cursor: pointer;font-size: 12px;margin-top:5px;">
            <tr class="table_header_common">

                <th width="3%"><i class="fa fa-lsit"></i></th>
                <th width="2%">Sl.No.</th>
                <th width="10%">Bill No.</th>
                <th width="10%">Bill Generated Date & Time</th>
                <th width="10%">Discharge Date & Time</th>
                <th width="10%">Bill Tag</th>
                <th width="10%">UHID</th>
                <th width="8%">Patient Name</th>
                <th width="8%">Admission No.</th>
                <th width="8%">Admitting Doctor</th>
                <th width="8%">Consulting Doctor</th>
                <th width="8%">Speciality</th>
                <th width="8%">Last Bed</th>
                <th width="8%" class="wrap_text">Bill Amount</th>
                <th width="8%" class="wrap_text">Total Discount</th>
                <th width="8%" class="wrap_text">Net Amount</th>
                <th width="8%" class="wrap_text">Patient Payable</th>
                <th width="8%" class="wrap_text">Payor Payable</th>
                <th width="8%" class="wrap_text">Prepared By</th>
                <th width="8%" class="wrap_text">Edited By</th>
                <th width="8%" class="wrap_text">Edited Date</th>
                <th width="8%" class="wrap_text">Parent Company</th>
                <th width="8%" class="wrap_text">Company</th>
                <th width="8%" class="wrap_text">Payment Type</th>



            </tr>
            <tbody>
                @if (count($res) > 0)

                    @foreach ($res as $data)
                        @php
                            $pay_color = ' ';
                            $paid_color = ' ';
                            $cancel_color = ' ';
                            $return_color = ' ';
                            $tag_color=' ';
                            $is_includeindischarge_color=' ';
                            if ($data->paymenttypedesc == 'Credit') {
                                $pay_color = 'credit_bg';
                            } elseif ($data->paymenttypedesc == 'Cash/Card') {
                                $pay_color = 'cash_card_bg';
                            } elseif ($data->paymenttypedesc == 'Insurance') {
                                $pay_color = 'insurance_bg';
                            } elseif ($data->paymenttypedesc == 'Company Credit') {
                                $pay_color = 'company_credit_bg';
                            } elseif ($data->paymenttypedesc == 'Opcredit') {
                                $pay_color = 'Opcredit_bg';
                            } elseif ($data->paymenttypedesc == 'Return To Advance') {
                                $pay_color = 'rtn_to_adv_bg';
                            }
                            if ($data->paid_status == 0 && $data->cancelled_status != 1 && $data->cancelled_status != 2) {
                                $paid_color = 'unpaid-bg';
                            } elseif ($data->paid_status == 1 && $data->cancelled_status != 1 && $data->cancelled_status != 2) {
                                $paid_color = 'paid-bg';
                            }
                            if ($data->cancelled_status != 0) {
                                $paid_color = 'cancelled-bg';
                            }
                            if ($data->bill_tag == 'DB') {
                                $tag_color='draft_bill';
                            } elseif ($data->bill_tag == 'DBD') {
                                $tag_color='discharge-bg';
                            }
                        @endphp

                        <tr>
                            <td class="common_td_rules">
                                <button type="button" class="pull-right"
                                    onclick="displayBillDetails({{ $data->id }})"
                                    style="width: 25px; height: 20px; border: none;">
                                    <i class="fa fa-list"> </i>
                                </button>
                                @if(env('NEW_DISCHARGE_BILL', 0) == 0)
                                    @if ($data->paid_status == 0 && $data->cancelled_status == 0 && $data->discharge_status == 0)
                                    <div class="popupDiv">
                                        <div class="popDiv" id="popup{{ $data->id }}" style="display: none;">
                                            <div class="col-md-1 pull-right"
                                                style="border-radius: 50%;width:18px;height:19px;background:#bebbbb;top: -3px;left:5px">
                                                <button type=" button" class="close" id="ppclose{{ $data->id }}"
                                                    data-dismiss="modal" aria-label="Close">
                                                    <span class="text-info" aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="col-md-12">
                                                <ul class="list-group" style="cursor: pointer">
                                                    @if ($data->paid_status == 0 && $data->cancelled_status == 0)
                                                        <li class="list-group-item" id="cancel{{ $data->id }}"
                                                            onclick="displayCancelBillStatus({{ $data->id }})">
                                                            <b>Cancel</b>
                                                        </li>
                                                    @endif
                                                    @if ($data->paid_status == 0 && $data->cancelled_status == 0)
                                                    <li class="list-group-item"><a
                                                            href="{{ URL::to('/') }}//discharge/dischargeBilling/{{ $data->id }}"><b>View/Edit</b></a>
                                                    </li>
                                                    @endif

                                                    @if (($data->paid_status == 0 || $data->paid_status == 2) && $data->cancelled_status == 0)
                                                        <li class="list-group-item pay_bill_btn"
                                                            id="return{{ $data->id }}"
                                                            onclick="cashReceive({{ $data->id }})">
                                                            <b>Pay Now</b>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                    @endif
                                @else
                                    @if ($data->cancelled_status == 0)
                                    <div class="popupDiv">
                                        <div class="popDiv" id="popup{{ $data->id }}" style="display: none;">
                                            <div class="col-md-1 pull-right"
                                                style="border-radius: 50%;width:18px;height:19px;background:#bebbbb;top: -3px;left:5px">
                                                <button type=" button" class="close" id="ppclose{{ $data->id }}"
                                                    data-dismiss="modal" aria-label="Close">
                                                    <span class="text-info" aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="col-md-12">
                                                <ul class="list-group" style="cursor: pointer">
                                                    @if ($data->paid_status == 0 && $data->cancelled_status == 0)
                                                        <li class="list-group-item" id="cancel{{ $data->id }}"
                                                            onclick="displayCancelBillStatus({{ $data->id }})">
                                                            <b>Cancel</b>
                                                        </li>
                                                    @endif
                                                    @if ($data->cancelled_status == 0)
                                                    <li class="list-group-item"><a
                                                            href="{{ URL::to('/') }}//discharge_new/dischargeBilling/{{ $data->id }}"><b>View/Edit</b></a>
                                                    </li>
                                                    @endif

                                                    @if ($data->paid_status == 0 && $data->cancelled_status == 0)
                                                        <li class="list-group-item pay_bill_btn"
                                                            id="return{{ $data->id }}"
                                                            onclick="cashReceive({{ $data->id }})">
                                                            <b>Pay Now</b>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                    @endif
                                @endif


                            </td>
                            <td class="td_common_numeric_rules {{ $paid_color }}" id="paid_bg{{ $data->id }}">
                                {{ ($res->currentPage() - 1) * $res->perPage() + $loop->iteration }}</td>
                            <td class="common_td_rules {{ $cancel_color }} {{ $pay_color }}"
                                title="{{ $data->bill_no }}">
                                {{ $data->bill_no }}</td>
                            @if ($data->bill_date_desc != '')
                                <td class="common_td_rules"
                                    title="{{ date('M-d-Y h:i A', strtotime($data->bill_date_desc)) }}">
                                    {{ date('M-d-Y h:i A', strtotime($data->bill_date_desc)) }}</td>
                            @else
                                <td class="common_td_rules">{{ $data->bill_date_desc }}</td>
                            @endif
                            @if ($data->bill_date_desc != '')
                                <td class="common_td_rules"
                                    title="{{ date('M-d-Y h:i A', strtotime($data->bill_date_desc)) }}">
                                    {{ date('M-d-Y h:i A', strtotime($data->bill_date_desc)) }}</td>
                            @else
                                <td class="common_td_rules" title="">{{ $data->bill_date_desc }}</td>
                            @endif
                            <td class="common_td_rules {{ $tag_color }}" title="{{ $data->billtagdesc }}">
                                {{ $data->billtagdesc }}
                            </td>
                            <td class="common_td_rules" title="{{ $data->uhid }}">{{ $data->uhid }}</td>
                            <td class="common_td_rules" title="{{ $data->patient_name }}">
                                {{ $data->patient_name }}
                            </td>
                            <td class="common_td_rules" title="{{ $data->admission_no }}">
                                {{ $data->admission_no }}
                            </td>
                            <td class="common_td_rules" title="{{ $data->admdr }}">{{ $data->admdr }}</td>
                            <td class="common_td_rules" title="{{ $data->consdr }}">{{ $data->consdr }}</td>
                            <td class="common_td_rules" title="{{ $data->specialityname }}">
                                {{ $data->specialityname }}</td>
                            <td class="common_td_rules" title="{{ $data->last_bed_name }}">
                                {{ $data->last_bed_name }}</td>
                            <td class="common_td_rules" title="{{ $data->bill_amount }}">{{ $data->bill_amount }}
                            </td>
                            <td class="common_td_rules" title="{{ $data->total_discount }}">
                                {{ $data->total_discount }}</td>
                            <td class="common_td_rules" title="{{ $data->net_amount }}">{{ $data->net_amount }}
                            </td>
                            <td class="common_td_rules" title="{{ $data->total_patient_payable }}">
                                {{ $data->total_patient_payable }}</td>
                            <td class="common_td_rules" title="{{ $data->total_payor_payable }}">
                                {{ $data->total_payor_payable }}</td>
                            <td class="common_td_rules" title="{{ $data->credatedby }}">{{ $data->credatedby }}
                            </td>
                            <td class="common_td_rules" title="{{ $data->edituser }}">{{ $data->edituser }}</td>
                            @if ($data->editeddate != '')
                                <td class="common_td_rules"
                                    title="{{ date('M-d-Y h:i A', strtotime($data->editeddate)) }}">
                                    {{ date('M-d-Y h:i A', strtotime($data->editeddate)) }}</td>
                            @else
                                <td class="common_td_rules">{{ $data->editeddate }}</td>
                            @endif
                            <td class="common_td_rules" title="{{ $data->parent_company_name }}">
                                {{ $data->parent_company_name }}</td>
                            <td class="common_td_rules" title="{{ $data->company_name }}">
                                {{ $data->company_name }}</td>
                            <td class="common_td_rules" title="{{ $data->paymenttypedesc }}">
                                {{ $data->paymenttypedesc }}</td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="18">No Record Match</td>
                    </tr>
                @endif

            </tbody>
        </table>
    </div>
</div>
<div class="col-md-12">
    <div class="clearfix"></div>
    <div class="col-md-12 text-right">
        <ul class="pagination purple_pagination" style="text-align:right !important; margin: -24px -4px;">
            {!! $page_links !!}
        </ul>
    </div>
</div>


<script>
    $(document).ready(function() {
        $(".page-link").click(function(e) {
            var url = $(this).attr("href");
            console.log(url);
            if (url == undefined) {
                return;
            }
            var paid_bill = $('#paid_bill').is(":checked");
            var Unpaid_bill = $('#Unpaid_bill').is(":checked");
            var cancelled_bill = $('#cancelled_bill').is(":checked");
            var draft_bills = $('#draft_bills').is(":checked");
            var weekly_bill = $('#weekly_bill').is(":checked");
            var credit_discharge = $('#credit_discharge').is(":checked");
            var discharge_bill = $('#discharge_bill').is(":checked");
            var exclude_cancelled_bills = $('#exclude_cancelled_bills').is(":checked");
            var death_patient = $('#death_patient').is(":checked");
            var cash_card = $('#cash_card').is(":checked");
            var credit = $('#credit').is(":checked");
            var insurance = $('#insurance').is(":checked");
            var company_credit = $('#company_credit').is(":checked");


            var bill_no = $('#bill_no').val();
            var payment_type = $('#payment_type').val();
            var prepared_by = $('#prepared_by').val();
            var patient = $('#patient_hidden').val();
            var uhid = $('#patient_uhid').val();
            var ip_no = $('#ip_no').val();
            var admitting_dr = $('#admitting_dr').val();
            var consulting_dr = $('#consulting_dr').val();
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var params = {
                cash_card: cash_card,
                credit: credit,
                insurance: insurance,
                company_credit: company_credit,
                death_patient: death_patient,
                exclude_cancelled_bills: exclude_cancelled_bills,
                consulting_dr: consulting_dr,
                admitting_dr: admitting_dr,
                draft_bills: draft_bills,
                weekly_bill: weekly_bill,
                credit_discharge: credit_discharge,
                cancelled_bill: cancelled_bill,
                Unpaid_bill: Unpaid_bill,
                paid_bill: paid_bill,
                discharge_bill: discharge_bill,
                bill_no: bill_no,
                payment_type: payment_type,
                prepared_by: prepared_by,
                patient: patient,
                uhid: uhid,
                ip_no: ip_no,
                from_date: from_date,
                to_date: to_date,
            };
            $.ajax({
                type: "POST",
                url: url,
                data: params,
                beforeSend: function() {
                    $('#Discharge_bill_data').html(' ');
                    $('#Discharge_bill_data').LoadingOverlay("show", {
                        background: "rgba(255, 255, 255, 0.7)",
                        imageColor: '#009869'
                    });
                    $('#DischargebilllistBtn').attr('disabled', true);
                    $("#Dischargebilllistspin").removeClass("fa fa-search");
                    $("#Dischargebilllistspin").addClass("fa fa-spinner fa-spin");


                },
                success: function(data) {
                    if (data) {
                        $('#Discharge_bill_data').html(data);
                        $('.theadscroll').perfectScrollbar({
                            wheelPropagation: true,
                            minScrollbarLength: 30
                        });
                    }

                },
                complete: function() {
                    $('#DischargebilllistBtn').attr('disabled', false);
                    $('#Discharge_bill_data').LoadingOverlay("hide");
                    $("#Dischargebilllistspin").removeClass("fa fa-spinner fa-spin");
                    $("#Dischargebilllistspin").addClass("fa fa-search");

                },
                error: function() {
                    toastr.error("Error Please Check Your Internet Connection");
                },
            });
            return false;
        });

    });
</script>
