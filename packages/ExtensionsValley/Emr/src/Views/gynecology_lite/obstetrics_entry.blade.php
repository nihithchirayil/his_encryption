<div class="col-md-7 padding_sm">
    <form id="obstetrics_history">
    <div class="col-md-12 padding_sm table_box obstetrics_history_entry_block">
        <div style="margin-top: 5px;
            padding-bottom: 10px;" class="text_head">Obstetrics History</div>
        <style>
            tr{
                height:35px !important;
            }
        </style>
        <table>
            <tr>
                <td colspan="2"><b>First Child</b></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="radio" name="delevery_type1" id="ftnd1" value="1"/> <label style="margin-right:10px;" for="ftnd1">FTND</label>
                    <input type="radio" name="delevery_type1" id="lscs1" value="2"/> <label style="margin-right:10px;" for="lscs1">LSCS</label>
                    <input type="radio" name="delevery_type1" id="ab1" value="3"/> <label style="margin-right:10px;" for="ab1">AB</label>
                    <input type="radio" name="delevery_type1" id="ect1" value="4"/> <label style="margin-right:10px;" for="ect1">ECT</label>
                    <input type="radio" name="delevery_type1" id="mtp1" value="5"/> <label style="margin-right:10px;" for="mtp1">MTP</label>
                </td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>
                    <input type="radio" name="gender1" id="boy1" value="2"/>
                        <label style="margin-right:10px;" for="boy1">Girl
                            <i class="fa fa-mars"></i>
                        </label>
                    <input type="radio" name="gender1" id="girl1" value="1"/>
                        <label style="margin-right:10px;" for="girl1">Boy
                            <i class="fa fa-venus"></i>
                        </label>

                        
                </td>
            </tr>
            <tr>
                <td>Birth Weight</td>
                <td>
                    <input type="textbox" style="width:250px;margin-top:-7px !important;" name="birth_weight1" id="birth_weight1" class="form-control bottom-border-text borderless_textbox "/>
                </td>
            </tr>
            <tr>
                <td>Age</td>
                <td>
                    <input type="textbox" style="width:250px;margin-top:-7px !important;" name="age1" id="age1" class="form-control bottom-border-text borderless_textbox"/>
                </td>
            </tr>
            <tr>
                <td>Place of birth</td>
                <td>
                    <input type="textbox" style="width:250px;margin-top:-7px !important;" name="place_of_birth1" id="place_of_birth1" class="form-control bottom-border-text borderless_textbox "/>
                </td>
            </tr>
            <tr>
                <td>Any complications</td>
                <td>
                    <input type="textbox" style="width:250px;margin-top:-7px !important;" name="any_complications1" id="any_complications1" class="form-control bottom-border-text borderless_textbox "/>
                </td>
            </tr>



            <tr>
                <td colspan="2"><b>Second Child</b></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="radio" name="delevery_type2" id="ftnd2" value="1"/> <label style="margin-right:10px;" for="ftnd2">FTND</label>
                    <input type="radio" name="delevery_type2" id="lscs2" value="2"/> <label style="margin-right:10px;" for="lscs2">LSCS</label>
                    <input type="radio" name="delevery_type2" id="ab2" value="3"/> <label style="margin-right:10px;" for="ab2">AB</label>
                    <input type="radio" name="delevery_type2" id="ect2" value="4"/> <label style="margin-right:10px;" for="ect2">ECT</label>
                    <input type="radio" name="delevery_type2" id="mtp2" value="5"/> <label style="margin-right:10px;" for="mtp2">MTP</label>
                </td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>
                        <input type="radio" name="gender2" id="boy2" value="2"/>
                        <label style="margin-right:10px;" for="boy2">Girl
                            <i class="fa fa-mars"></i>
                        </label>
                        <input type="radio" name="gender2" id="girl2" value="1"/>
                        <label style="margin-right:10px;" for="girl2">Boy
                            <i class="fa fa-venus"></i>
                        </label>
                </td>
            </tr>
            <tr>
                <td>Birth Weight</td>
                <td>
                    <input type="textbox" style="width:250px;margin-top:-7px !important;" name="birth_weight2" id="birth_weight2" class="form-control bottom-border-text borderless_textbox "/>
                </td>
            </tr>
            <tr>
                <td>Age</td>
                <td>
                    <input type="textbox" style="width:250px;margin-top:-7px !important;" name="age2" id="age2" class="form-control bottom-border-text borderless_textbox"/>
                </td>
            </tr>
            <tr>
                <td>Place of birth</td>
                <td>
                    <input type="textbox" style="width:250px;margin-top:-7px !important;" name="place_of_birth2" id="place_of_birth2" class="form-control bottom-border-text borderless_textbox "/>
                </td>
            </tr>
            <tr>
                <td>Any complications</td>
                <td>
                    <input type="textbox" style="width:250px;margin-top:-7px !important;" name="any_complications2" id="any_complications2" class="form-control bottom-border-text borderless_textbox "/>
                </td>
            </tr>



        </table>
    </div>
    <input type="reset" id="obstetrics_history_reset" style="display:none;"/>
    </form>
</div>
<div class="col-md-5">
    <div class="text_head">History</div>
    <div id="obstetrics_history_history_container"></div>
</div>
