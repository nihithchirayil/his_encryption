<div class="col-md-12 ">
    @if(!empty($usg_data))
    <style>
        b{
            font-size:12px !important;
        }
        th,td {
            padding:8px !important;
            font-size: 11px;
            font-weight: 600;
        }
    </style>
    @foreach($usg_data as $data)
        <div class="col-md-12" style="border:1px solid:gainsboro;border-radius:3px;">
            <span style="float:right;margin-right:10px;"><i class="fa fa-calendar"></i> {{date('M-d-Y h:i A',strtotime($data->created_at))}}</span><br>

            <div class="col-md-12 no-padding" style="margin-bottom:10px;">
                <label><b>Notes</b></label>
                <br>
                {{$data->notes}}
            </div>

            <table class="table table-bordered table-stripped" style="border:1px solid gray;border-collapse:collapse;">
            <thead>
                <tr>
                    <th style="background-color:#eeeeee;" colspan="4">Child-1</th>
                </tr>
                <tr>
                    <td>Delivery Type</td>
                    <td style="color:black;">@if($data->delevery_type1 == 1)
                            FTND
                        @endif
                        @if($data->delevery_type1 == 2)
                            LSCS
                        @endif
                        @if($data->delevery_type1 == 3)
                            AB
                        @endif
                        @if($data->delevery_type1 == 4)
                            ECT
                        @endif
                        @if($data->delevery_type1 == 5)
                            MTP
                        @endif
                    </td>
                    <td>Gender</td>
                    <td style="color:black;">@if($data->gender1 ==1)
                            Girl
                        @else
                            Boy
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Birth Weight</td>
                    <td>{{$data->birth_weight1}}</td>
                    <td>Age</td>
                    <td>{{$data->age1}}</td>
                </tr>
                <tr>
                    <td>Place of Birth</td>
                    <td>{{$data->place_of_birth1}}</td>
                    <td>Any Complications</td>
                    <td>{{$data->any_complications1}}</td>
                </tr>
                <tr>
                    <td>Indication</td>
                    <td colspan="3">{{$data->indication1}}</td>
                </tr>
            </thead>
            </table>


            <table class="table table-bordered table-stripped" style="border:1px solid gray;border-collapse:collapse;">
                <thead>
                    <tr>
                        <th style="background-color:#eeeeee;" colspan="4">Child-2</th>
                    </tr>
                    <tr>
                        <td>Delivery Type</td>
                        <td style="color:black;">@if($data->delevery_type2 == 1)
                                FTND
                            @endif
                            @if($data->delevery_type2 == 2)
                                LSCS
                            @endif
                            @if($data->delevery_type2 == 3)
                                AB
                            @endif
                            @if($data->delevery_type2 == 4)
                                ECT
                            @endif
                            @if($data->delevery_type2 == 5)
                                MTP
                            @endif
                        </td>
                        <td>Gender</td>
                        <td style="color:black;">@if($data->gender2 ==1)
                                Girl
                            @else
                                Boy
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Birth Weight</td>
                        <td>{{$data->birth_weight2}}</td>
                        <td>Age</td>
                        <td>{{$data->age2}}</td>
                    </tr>
                    <tr>
                        <td>Place of Birth</td>
                        <td>{{$data->place_of_birth2}}</td>
                        <td>Any Complications</td>
                        <td>{{$data->any_complications2}}</td>
                    </tr>
                    <tr>
                        <td>Indication</td>
                        <td colspan="3">{{$data->indication2}}</td>
                    </tr>
                </thead>
                </table>
                @php
                    $additional_child_information = json_decode($data->additional_child_information);
                    $j=0;
                @endphp
                @if(!empty($additional_child_information))
                    @foreach($additional_child_information as $data1)
                        <table class="table table-bordered table-stripped" style="border:1px solid gray;border-collapse:collapse;">
                            <thead>
                                <tr>
                                    <th style="background-color:#eeeeee;" colspan="4">Child-{{$j+3}}</th>
                                </tr>
                                <tr>
                                    @php
                                        $delevery_type = isset($data1->delevery_type)?$data1->delevery_type:0;
                                        $gender = isset($data1->gender)?$data1->gender:0;
                                    @endphp
                                    <td>Delivery Type</td>
                                    <td style="color:black;">@if($delevery_type == 1)
                                            FTND
                                        @endif
                                        @if($delevery_type == 2)
                                            LSCS
                                        @endif
                                        @if($delevery_type == 3)
                                            AB
                                        @endif
                                        @if($delevery_type == 4)
                                            ECT
                                        @endif
                                        @if($delevery_type == 5)
                                            MTP
                                        @endif
                                    </td>
                                    <td>Gender</td>
                                    <td style="color:black;">@if($gender ==1)
                                            Girl
                                        @else
                                            Boy
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Birth Weight</td>
                                    <td>{{isset($data1->birth_weight)?$data1->birth_weight:''}}</td>
                                    <td>Age</td>
                                    <td>{{isset($data1->age)?$data1->age:''}}</td>
                                </tr>
                                <tr>
                                    <td>Place of Birth</td>
                                    <td>{{isset($data1->place_of_birth)?$data1->place_of_birth:''}}</td>
                                    <td>Any Complications</td>
                                    <td>{{isset($data1->any_complications)?$data1->any_complications:''}}</td>
                                </tr>
                                <tr>
                                    <td>Indication</td>
                                    <td colspan="3">{{isset($data1->indication)?$data1->indication:''}}</td>
                                </tr>
                            </thead>
                        </table>
                        @php
                            $j++;
                        @endphp
                    @endforeach
                @endif
            @endforeach
        </div>

    @endif
</div>
