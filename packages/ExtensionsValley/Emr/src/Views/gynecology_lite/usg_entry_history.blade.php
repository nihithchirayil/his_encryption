<div class="col-md-12 ">
    @if(!empty($usg_data))
    <style>
        b{
            font-size:12px !important;
        }
    </style>
    @foreach($usg_data as $data)
        <div class="col-md-12" style="border:1px solid:gainsboro;border-radius:3px;">
            <i class="fa fa-calendar"></i> {{date('M-d-Y h:i A',strtotime($data->created_at))}}<br>

            <b>Usg taken at:</b>
            @if($data->usg_results_entry_date != null)
                {{date('M-d-Y',strtotime($data->usg_results_entry_date))}}
            @endif
            <br>
            <b>POG:</b>
            @if($data->pog_wk != null)
                {{$data->pog_wk}} Weeks,
            @endif
            @if($data->pog_d != null)
                {{$data->pog_d}} Days,
            @endif
            <br>

            @if($data->siugs==1)
            SIUGS
            @elseif($data->sliug==1)
            SLIUG
            @elseif($data->slf==1)
            SLF
            @endif:
            @if($data->slf_wk != null)
                {{$data->slf_wk}} Weeks,
            @endif
            @if($data->slf_d != null)
                {{$data->slf_d}} Days,
            @endif
            <br>

            @if($data->ceph==1)
            Ceph
            @elseif($data->breech==1)
            Breech
            @elseif($data->transverse_lie==1)
            Transverse lie
            @endif
            <br>

            @if($data->afi !='')
            AFI: {{$data->afi}}
            <br>
            @endif

            @if($data->efw !='')
            EFW: {{$data->efw}}
            <br>
            @endif

            @if($data->edd !='')
            EDD: {{$data->edd}}
            <br>
            @endif

            @if($data->placenta !='')
            Placenta: {{$data->placenta}}
            <br>
            @endif

            @if($data->nt !='')
            NT: {{$data->nt}}
            <br>
            @endif

            @if($data->fhr !='')
            FHR: {{$data->fhr}}
            <br>
            @endif

            @if($data->nb !='')
            NB: {{$data->nb}}
            <br>
            @endif


        </div>
        @endforeach
    @endif
</div>
