    <table width="100%" id="antinatal_clinical_data_table theadfix_wrapper" style="border-spacing:10px;" class="" style="margin-top:10px !important;">
        <thead>
            <tr>
                <th width="80%">Favourite</th>
                <th width="10%">Add</th>
                <th width="10%">Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach($resdata as $data)
                <tr id="prenting_cmp_fav_row_{{$data->id}}">
                    <td>{{$data->favourite_text}}</td>
                    <td>
                        <button onclick="applyGynecPresentingFav('{{$data->favourite_text}}');" type="button" class="btn btn-sm">
                            <i style="color:blue;" class="fa fa-plus"></i>
                        </button>
                    </td>
                    <td>
                        <button onclick="deleteGynecPresentingFav('{{$data->id}}');" type="button" class="btn btn-sm">
                            <i style="color:red;" class="fa fa-times"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
