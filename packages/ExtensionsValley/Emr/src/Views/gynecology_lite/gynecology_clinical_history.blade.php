<table width="80%"  style=" border-spacing: 10px;
    width: 96%;
    text-align: center;
    margin: auto;" class="table table-sm table-bordered" style="margin-top:10px !important;">
    <thead>
            <tr style="background:#21a4f1;color:white !important;">
                <td style="width:20%"> Created at </td>
                <td style="width:60%"> Value </td>
            </tr>
    </thead>
    <tbody>

        @foreach($clinical_data as $data)
        @php
        if(!empty($data->created_at)){
            if(!empty($data->updated_at)){
                $created_at = date('M-d-Y',strtotime($data->updated_at));
            }else{
                $created_at = date('M-d-Y',strtotime($data->created_at));
            }
        }else{
            $created_at = '';
        }
        @endphp
        <tr>
            <td>
                {!!$created_at!!}
            </td>
            <td style="text-align: left;">{!!$data->$h_type!!}</td>
        </tr>
        @endforeach
    </tbody>
</table>
