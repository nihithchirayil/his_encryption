<div class="col-md-12 ">
    @if(!empty($gynec_details))
    <style>
        b{
            font-size:12px !important;
        }
    </style>
    @foreach($gynec_details as $data)
        <div class="col-md-12 gynec_assessment_history_item" gynec-data-assessment-head-id="{{ $data->id }}" style="border:1px solid:gainsboro;border-radius:3px;">
            <div class="assessment_history_head" style="display:flex;">
            <div class="assessment_history_head_left">
                <div title=" {{date('M-d-Y h:i A',strtotime($data->created_at))}} "><i class="fa fa-calendar"></i>
                {{date('M-d-Y h:i A',strtotime($data->created_at))}} </div>
            </div>
            <div class="assessment_history_head_right">
                <i class="fa fa-print printBtn printGynecAssessmentButton"></i>
                <i class="fa fa-edit editBtn editGynecAssessmentButtonBtn "></i>
                <i class="fa fa-copy copyBtn copyGynecAssessmentButtonBtn"></i>
                <i class="fa fa-trash deleteBtn deleteGynecAssessmentButtonBtn "></i>
            </div>
        </div>

            <br>
            @if(count($pregnancy_deatils)>0)

                @if($data->lmp!='')
                    <b>LMP:@if($data->lmp != ''){{$data->lmp}}@endif</b><br>
                @endif
                @if($data->pmp!='')
                    <b>PMP:@if($data->pmp != ''){{$data->pmp}}@endif</b><br>
                @endif
            @endif
            @if($data->presenting_complaints!='')
                <b>Presenting Complaints:</b><br>
                {!!$data->presenting_complaints!!}<br>
            @endif
            @if($data->local_examinations!='')
                <b>Local Examinations:</b><br>
                {!!$data->local_examinations!!}<br>
            @endif
            @if($data->usg!='')
                <b>USG:</b><br>
                {!!$data->usg!!}<br>
            @endif
            @if($data->provisional_diagnosis!='')
                <b>Provisional Diagnosis:</b><br>
                {!!$data->provisional_diagnosis!!}<br>
            @endif
            @if($data->plan_of_care!='')
                <b>Plan of Care:</b><br>
                {!!$data->plan_of_care!!}<br>
            @endif
            @if($data->follow_up_care!='')
                <b>Follow up Care:</b><br>
                {!!$data->follow_up_care!!}<br>
            @endif
            @if($data->special_care!='')
                <b>Special Care:</b><br>
                {!!$data->special_care!!}<br>
            @endif
            <br>
        </div>
        @endforeach
    @endif
</div>

