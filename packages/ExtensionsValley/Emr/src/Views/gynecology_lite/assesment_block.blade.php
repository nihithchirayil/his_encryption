<div class="col-md-7 card-body">
    <div class="col-md-4 padding_sm updateGynecAssessmentBtnDiv" style="display: none; float: right">
        <button type="button" class="btn btn-sm btn-blue updateGynecAssessmentBtn"><i class="fa fa-save"></i>  Save</button>
        <button type="button" class="btn btn-sm btn-blue canceGynecAssessmentEditBtn"><i class="fa fa-times"></i>  Cancel</button>
    </div>
    <div class="col-md-12" style="margin-top:10px;">
        <label class="text-normal">Presenting Complaints</label>
         <i style="color:#21a4f1;" title="Add to favourites" onclick="add_presenting_complaints_fav();" class="fa fa-star"></i>

         <i style="color:#21a4f1;" title="select from favourites" class="fa fa-list pull-right" onclick="list_presenting_complaints_fav();" aria-hidden="true"></i>

        {{-- <div data-attr-label="allergic_history" id="presenting_complaints" class="lined theadscroll" contenteditable="true" style="height:120px;"></div> --}}

        <textarea  id="presenting_complaints" class=" form-control" rows="4" cols="50" style="height:120px !important;">
                </textarea>

    </div>
    <div class="col-md-12 vital_form_container" style="margin-top:10px;">
        <label class="text-normal">General Examinations</label>
        <i class="fa fa-plus-circle gy-btn-blue-sm add_new_batch_vitals hidden" title="Add New Batch"></i>
        <br>
        <div class="col-md-3">
            <span class="gynec_vitals">Height-cm</span>
            <label class="gynec_height_range"></label>
            <input type="text" value="" id="gynec_height" class="form-control number_input"/>
        </div>
        <div class="col-md-3">
            <span class="gynec_vitals">Weight-kg</span>
            <label class="gynec_weight_range"></label>
            <input type="text" value="" id="gynec_weight" class="form-control number_input"/>
        </div>
        <div class="col-md-3">
            <span class="gynec_vitals">BMI</span>
            <label class="gynec_bmi_range"></label>
            <input type="text" readonly="readonly" value="" id="gynec_bmi" class="form-control number_input"/>
        </div>
        <div class="col-md-3">
            <span class="gynec_vitals">Waist Circ</span>
            <label class="gynec_waist_circ_range"></label>
            <input type="text" value="" onblur="chekMinMax(this,15);" id="gynec_waist_circ" class="form-control number_input"/>
        </div>
    </div>
    <div class="col-md-12 vital_form_container" style="margin-top:10px;">
        <div class="col-md-3">
            <span class="gynec_vitals">PR</span>
            <label class="gynec_pr_range"></label>
            <input type="text" value="" onblur="chekMinMax(this,16);" id="gynec_pr" class="form-control number_input"/>
        </div>
        <div class="col-md-3">
            <span class="gynec_vitals">BP Systolic</span>
            <label class="gynec_bp_sys_range"></label>
            <input type="text" value="" onblur="chekMinMax(this,5);" id="gynec_bp_sys" class="form-control number_input"/>
        </div>
        <div class="col-md-3">
            <span class="gynec_vitals">BP Diastolic</span>
            <label class="gynec_bp_dia_range"></label>
            <input type="text" value="" onblur="chekMinMax(this,6);" id="gynec_bp_dia" class="form-control number_input"/>
        </div>
        <div class="col-md-3">
            <span class="gynec_vitals">Pallor</span>
            <label class="gynec_pallor_range"></label>
            <input type="text" value="" onblur="chekMinMax(this,17);" id="gynec_pallor" class="form-control number_input"/>
        </div>
    </div>
    <div class="col-md-12 vital_form_container" style="margin-top:10px;">
        <div class="col-md-3">
            <span class="gynec_vitals">Edema</span>
            <label class="gynec_edema_range"></label>
            <input type="text" value="" onblur="chekMinMax(this,18);" id="gynec_edema" class="form-control number_input"/>
        </div>
        <div class="col-md-3">
            <span class="gynec_vitals">Cvs</span>
            <label class="gynec_cvs_range"></label>
            <input type="text" value="" onblur="chekMinMax(this,19);" id="gynec_cvs" class="form-control number_input"/>
        </div>
        <div class="col-md-3">
            <span class="gynec_vitals">Rs</span>
            <label class="gynec_rs_range"></label>
            <input type="text" value="" onblur="chekMinMax(this,20);" id="gynec_rs" class="form-control number_input"/>
        </div>
    </div>

    <div class="col-md-12" style="margin-top:10px;">
        <label class="text-normal">Local Examinations</label><br>
        {{-- <div data-attr-label="allergic_history" id="local_examinations" class="lined theadscroll" contenteditable="true" style="height:110px;"></div> --}}

        <textarea  id="local_examinations" class=" form-control" rows="4" cols="50" style="height:110px !important;">
        </textarea>

    </div>
</div>

<div class="col-md-5 card-body theadscroll" style="height:490px;position: relative;">

    {{-- <div class="col-md-12" style="
        margin-top: 10px;
        border: 1px solid #dadada;
        border-radius: 3px;
        padding-top: 8px;
    ">
        <label style="width:100%;" onclick="$('#gynec_history_container').toggle();$('.gynec_history_arrow').toggle();" class="text-normal">
            History
            <i class="fa fa-arrow-down pull-right gynec_history_arrow" style="display:none;" aria-hidden="true"></i>
            <i class="fa fa-arrow-up pull-right gynec_history_arrow" style="display:show;" aria-hidden="true"></i>

        </label><br>

        <span id="gynec_history_container"></span>
        <span id="gynec_vital_details"></span>

    </div> --}}


    <div class="col-md-12" style="margin-top:10px;">
        <label class="text-normal">USG</label><br>
        {{-- <div data-attr-label="allergic_history" id="usg" class="lined theadscroll" contenteditable="true" style="height:80px;"></div> --}}
        <textarea  id="usg" class=" form-control" rows="4" cols="50" style="height:80px !important;">
        </textarea>

    </div>
    <div class="col-md-12">
        <label class="text-normal">Provisional Diagnosis</label><br>
        {{-- <div data-attr-label="allergic_history" id="provisional_diagnosis" class="lined theadscroll" contenteditable="true" style="height:80px;"></div> --}}
        <textarea  id="provisional_diagnosis" class=" form-control" rows="4" cols="50" style="height:80px !important;">
        </textarea>

    </div>
    <div class="col-md-12">
        <label class="text-normal">Plan of Care</label><br>
        {{-- <div data-attr-label="allergic_history" id="plan_of_care" class="lined theadscroll" contenteditable="true" style="height:80px;"></div> --}}

        <textarea  id="plan_of_care" class=" form-control" rows="4" cols="50" style="height:80px !important;">
        </textarea>

    </div>
    <div class="col-md-12">
        <label class="text-normal">Follow Up Care</label><br>
        {{-- <div data-attr-label="allergic_history" id="follow_up_care" class="lined theadscroll" contenteditable="true" style="height:80px;"></div> --}}

        <textarea  id="follow_up_care" class=" form-control" rows="4" cols="50" style="height:80px !important;">
        </textarea>

    </div>
    <div class="col-md-12">
        <label class="text-normal">Professional Details</label><br>
        {{-- <div data-attr-label="allergic_history" id="special_care" class="lined theadscroll" contenteditable="true" style="height:80px;"></div> --}}

        <textarea  id="special_care" class=" form-control" rows="4" cols="50" style="height:80px !important;">
        </textarea>

    </div>
</div>
