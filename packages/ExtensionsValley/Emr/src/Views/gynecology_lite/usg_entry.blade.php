<div class="col-md-7 padding_sm">
    <form id="usg_res_entry">
    <div class="col-md-12 padding_sm table_box usg_results_entry_block">
        <div style="margin-top: 5px;
            padding-bottom: 10px;" class="text_head">Usg Result</div>
        <style>
            tr{
                height:35px !important;
            }
        </style>
        <table>
            <tr>
                <td>
                     Date:
                </td>
                <td>
                    <input type="textbox" style="width:250px;margin-top:-7px !important;" name="usg_results_entry_date" id="usg_results_entry_date" class="form-control bottom-border-text borderless_textbox datepicker usg_form"/>
                </td>
            </tr>
            <tr>
                <td>
                    POG:
                </td>
                <td>
                    <input type="textbox" style="width:90px;margin-top:-7px !important;float:left;clear:right;" name="pog_wk" id="pog_wk" class="form-control bottom-border-text borderless_textbox usg_form number_input"/>
                    <span width="30px !important;" class="usg_form"> Wk </span>
                    <input type="textbox" style="width:90px;margin-top:-7px !important;float:left;clear:right;" name="pog_d" id="pog_d" class="form-control bottom-border-text borderless_textbox usg_form number_input"/>
                    <span width="30px !important;" class="usg_form"> D </span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="radio" name="sonography" id="siugs"/> <label for="siugs">SIUGS</label>
                    <input type="radio" name="sonography" id="sliug"/> <label for="sliug">SLIUG</label>
                    <input type="radio" name="sonography" id="slf"/> <label for="slf">SLF</label>
                </td>
                <td>
                    <input type="textbox" style="width:90px;margin-top:-7px !important;float:left;clear:right;" name="slf_wk" id="slf_wk" class="form-control bottom-border-text borderless_textbox usg_form number_input"/>
                    <span width="30px !important;" class="usg_form"> Wk </span>
                    <input type="textbox" style="width:90px;margin-top:-7px !important;float:left;clear:right;" name="slf_d" id="slf_d" class="form-control bottom-border-text borderless_textbox usg_form number_input"/>
                    <span width="30px !important;" class="usg_form"> D </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="radio" name="baby_position" id="ceph"/> <label for="ceph">ceph</label>
                    <input type="radio" name="baby_position" id="breech"/> <label for="breech">Breech</label>
                    <input type="radio" name="baby_position" id="transverse_lie"/>
                    <label for="transverse_lie">transverse lie</label>
                </td>
            </tr>

            <tr>
                <td>
                    AFI :
                </td>
                <td>
                    <input type="textbox" style="margin-top:-7px !important;" name="afi" id="afi" class="form-control bottom-border-text borderless_textbox usg_form"/>
                </td>
            </tr>
            <tr>
                <td>
                   EFW :
                </td>
                <td>
                   <input type="textbox" style="margin-top:-7px !important;" name="efw" id="efw" class="form-control bottom-border-text borderless_textbox usg_form"/>
                </td>
            </tr>
            <tr>
                <td>
                   EDD :
                </td>
                <td>
                  <input type="textbox" style="margin-top:-7px !important;" name="usg_edd" id="usg_edd" class="form-control bottom-border-text borderless_textbox usg_form"/>
                </td>
            </tr>

            <tr>
                <td>
                   Placenta :
                </td>
                <td>
                  <input type="textbox" style="margin-top:-7px !important;" name="placenta" id="placenta" class="form-control bottom-border-text borderless_textbox usg_form"/>
                </td>
            </tr>
            <tr>
                <td>
                   NT :
                </td>
                <td>
                  <input type="textbox" style="margin-top:-7px !important;" name="nt" id="nt" class="form-control bottom-border-text borderless_textbox usg_form"/>
                </td>
            </tr>
            <tr>
                <td>
                   FHR :
                </td>
                <td>
                  <input type="textbox" style="margin-top:-7px !important;" name="fhr" id="fhr" class="form-control bottom-border-text borderless_textbox usg_form"/>
                </td>
            </tr>
            <tr>
                <td>
                   NB :
                </td>
                <td>
                  <input type="textbox" style="margin-top:-7px !important;" name="nb" id="nb" class="form-control bottom-border-text borderless_textbox usg_form"/>
                </td>
            </tr>
        </table>
    </div>
    <input type="reset" id="usg_res_entry_reset" style="display:none;"/>
    </form>
</div>
<div class="col-md-5">
    <div class="text_head">History</div>
    <div id="usg_entry_history_container"></div>
</div>
