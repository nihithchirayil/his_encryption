@extends('Emr::emr.page')
@section('content-header')
<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->
@stop
@section('css_extra')
<link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/view_patient.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/multi_tab_setup.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/view_patient/css/common_controls.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
<link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link
    href="{{ asset('packages/extensionsvalley/gynecology/css/gynecology.css?version=' . env('APP_JS_VERSION', '0.0.1')) }}"
    rel="stylesheet">


@endsection
@section('content-area')

<!--  modal boxes  -->
@include('Emr::view_patient.custom_modals_lite')

<div class="right_col" role="main">
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_date" value="{{ date('M-d-Y') }}">
    <input type="hidden" id="emr_changes" value="{{ @$emr_changes ? $emr_changes : ''}}">
    <input type="hidden" id="doctor_id" value="{{ $doctor_id }}">
    <input type="hidden" id="default_note_form_id" value="{{ $default_note_form_id }}">
    <input type="hidden" id="patient_id" value="">
    <input type="hidden" id="booking_id" value="">
    <input type="hidden" id="visit_id" value="">
    <input type="hidden" id="visit_type" value="">
    <input type="hidden" id="encounter_id" value="">
    <input type="hidden" id="ca_head_id" value="">
    <input type="hidden" id="investigation_head_id" value="">
    <input type="hidden" id="assessment_head_id" value="">
    <input type="hidden" id="prescription_head_id" value="">
    <input type="hidden" id="weight" value="2">
    <input type="hidden" id="temperature" value="10">
    <input type="hidden" id="height" value="4">
    <input type="hidden" id="vital_batch_no" value="">
    <input type="hidden" id="pacs_viewer_prefix" value="{{ $pacs_viewer_prefix }}">
    <input type="hidden" id="pacs_report_prefix" value="{{ $pacs_report_prefix }}">
    <input type="hidden" id="print_dialog_config" value="{{ $print_dialog_config }}">
    <input type="hidden" id="gynecology_lite_id" value="">
    <input type="hidden" id="vital_min_data" value="{{$vital_min}}" />
    <input type="hidden" id="vital_max_data" value="{{$vital_max}}" />
    <input type="hidden" id="quantity_auto_calculation_config" value="{{$quantity_auto_calculation_config}}">
    <input type="hidden" id="dynamic_template_enable_emr_lite" value="{{$dynamic_template_enable_emr_lite}}">
    <input type="hidden" id="showshiftpatients" value="{{@$shift_search ? $shift_search : 0}}">
    <input type="hidden" id="show_shiftwise_patient" value="{{@$show_shiftwise_patient ? $show_shiftwise_patient :0}}">
    <input type="hidden" id="add_new_vital_batch" value="0">
    
    <input type="hidden" id="font_awsome_css_path"
        value="{{ asset('packages/extensionsvalley/dashboard/css/font-awesome.min.css') }}">
    <input type="hidden" id="bootstrap_min_path"
        value="{{ asset('packages/extensionsvalley/dashboard/css/bootstrap.min.css') }}">
    <input type="hidden" id="fontawsome_min_path"
        value="{{ asset('packages/extensionsvalley/dashboard/css/font-awesome.min.css') }}">
    <input type="hidden" id="bootstrapmin_js_path"
        value="{{ asset('packages/extensionsvalley/dashboard/js/bootstrap.min.js') }}">

    <a class="toggle-ip-op-list-btn toggleIpOpListBtn leftArrow">
        <span class="toggle-ip-op-list-btn-icon">
            <i class="fa fa-arrow-left"></i>
        </span>
    </a>
    @include('Emr::gynecology_lite.ip_op_list')
    @include('Emr::gynecology_lite.patient_visit_master')
    @include('Emr::doctor_notes.doctor_notes_modal')
    <div id="gynecology_history_modal" class="modal fade " role="dialog" style="z-index:1052;height: 416px;">
        <div class="modal-dialog" style="">
    
            <div class="modal-content">
                <div class="modal-header" style="background:#21a4f1; color:#ffffff;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close close_white">&times;</button>
                    <h4 class="modal-title">History</h4>
                </div>
                <div class="modal-body" style="min-height: 282px;">
                    <div class="row theadscroll" id="gynecology_history_modal_data">
    
                    </div>
                </div>
            </div>
    
        </div>
    </div>
</div>

@stop
@section('javascript_extra')
<script src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('packages/extensionsvalley/emr/select2/select2.js') }}"></script>
<script
    src="{{ asset('packages/extensionsvalley/accounts/default/javascript/jquery-ui.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/gynecology/js/gynecology.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>


<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/allergy_vitals_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/multi_tab_setup.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/investigation_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/prescription_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/notes_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/manage-document.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/emr/js/inv_result_entry.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/addnewinvestigationpopup_lite.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
    src="{{ asset('packages/extensionsvalley/view_patient/js/dynamic_template.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
<script
src="{{ asset('packages/extensionsvalley/emr/js/doctor_notes.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
</script>
@endsection
