<div class="col-md-10 no-padding patient_data_list_div card-body" style="height:98vh;">
    <div class="col-md-12 no-padding">

        <div class="col-md-3 padding_sm patient_details_inner_div">
            <div class="patient_details_patient_name text-normal"> Patient Name
                <!-- <i id="patient_data_list_btn"
                    style="margin-top: 10px;display: none;cursor: pointer;" onclick="showHidePatientList(2)"
                    class="pull-right text-normal fa fa-bars hover_btn"></i> -->
                </div>
                <div class="patient_details_patient_uhid"> UHID </div>
                <div class="patient_details_patient_age_gender" style="float: left;
            clear: right;"> Age/Gender </div>

<label class="blink_me" style="float:right;color:green;display:none;" id="pregnant_status"><b>
    <i class="fa fa-check"></i>
    Pregnant</b></label>
    <br><span class="patient_details_patient_place">Place</span>

        </div>
        <div class="col-md-3 toolbar_inner_div padding_sm">
            <div class="col-md-12 no-padding">
                <span class="col-md-2">LMP</span>
                <div class="col-md-4 no-padding">
                    <input type="text" value="" name="lmp" id="lmp" class="form-control"/>
                </div>
                <div class="col-md-6">
                    @php
                        $list = ['1'=>'Regular', '2'=>'Irregular']
                    @endphp
                    {!! Form::select('periods', $list,null, ['class' => 'form-control', 'id' =>'periods', 'style' => 'color:#555555; padding:4px 12px;','placeholder' =>'Periods']) !!}

                </div>
            </div>
            <div class="col-md-12 no-padding">
                <span class="col-md-2">PMP</span>
                <div class="col-md-4 no-padding">
                    <input type="text" value="" name="pmp" id="pmp" class="form-control"/>
                </div>
                <div class="col-md-6">
                    @php
                        $list = ['1'=>'+', '2'=>'-']
                    @endphp
                    {!! Form::select('dysmenorrhea', $list,null, ['class' => 'form-control', 'id' =>'dysmenorrhea', 'style' => 'color:#555555; padding:4px 12px;','placeholder' =>'Dysmenorrhea']) !!}

                </div>
            </div>
        </div>

        <div class="col-md-6 toolbar_inner_div toolbar_icons padding_sm" style="text-wrap:wrap !important;">
            {{-- <a class="expanding_button mark_pregnancy_btn" id="mark_pregnancy_btn" onclick="mark_pregnant(1);">
                <span class="expanding_button_icon"><i id="i_pregnancy_btn" class="fa fa-check-circle"></i></span>
                <span id="span_pregnancy_btn" class="expanding_button_text">Mark as Pregnant</span>
            </a> --}}
            <div class="col-md-11">
                <a class="expanding_button patient_clinical_history_btn">
                    <span class="expanding_button_icon"><i class="fa fa-history"></i></span>
                    <span class="expanding_button_text">History</span>
                </a>
                <a class="expanding_button patient_special_notes_btn">
                    <span class="expanding_button_icon"><i class="fa fa-book"></i></span>
                    <span class="expanding_button_text">Special Notes</span>
                </a>
                <a class="expanding_button patient_personal_notes_btn">
                    <span class="expanding_button_icon"><i class="fa fa-commenting-o"></i></span>
                    <span class="expanding_button_text">Personal Notes</span>
                </a>
                <a class="expanding_button patient_lab_results_btn">
                    <span class="expanding_button_icon"><i class="fa fa-flask"></i></span>
                    <span class="expanding_button_text">Lab Results</span>
                </a>
                <a class="expanding_button patient_radiology_results_btn">
                    <span class="expanding_button_icon"><i class="fa fa-camera"></i></span>
                    <span class="expanding_button_text">Radiology Results</span>
                </a>
                <a class="expanding_button patient_discharge_summary_list_btn">
                    <span class="expanding_button_icon"><i class="fa fa-list"></i></span>
                    <span class="expanding_button_text">Discharge Summary</span>
                </a>

                <a class="expanding_button patient_refer_btn">
                    <span class="expanding_button_icon"><i class="fa fa-user-plus"></i></span>
                    <span class="expanding_button_text">Refer Patient</span>
                </a>
                <br>
                <a class="expanding_button patient_transfer_btn">
                    <span class="expanding_button_icon"><i class="fa fa-exchange"></i></span>
                    <span class="expanding_button_text">Transfer Patient</span>
                </a>
                <a class="expanding_button patient_documents_btn">
                    <span class="expanding_button_icon"><i class="fa fa-archive"></i></span>
                    <span class="expanding_button_text">Documents</span>
                </a>
                <a class="expanding_button patient_discharge_summary_create_btn">
                    <span class="expanding_button_icon"><i class="fa fa-plus"></i></span>
                    <span class="expanding_button_text">Create Summary</span>
                </a>
                <a class="expanding_button patient_visit_history_list_btn" style="width: 140px !important;">
                    <span class="expanding_button_icon"><i class="fa fa-refresh"></i></span>
                    <span class="expanding_button_text" style="padding: -15px !important;margin-left: -15px;">Patient Visit History</span>
                </a>
                <a class="expanding_button getDoctorNotesModelBtn" id="getDoctorNotesModelBtn" onclick="getDoctorNotesModel()">
                    <span class="expanding_button_icon"><i class="fa fa fa-sticky-note-o small_bounce"></i></span>
                    <span class="expanding_button_text">Doctor Notes</span>
                </a>
            </div>
            <div class="col-md-1">
                <div class="saveButtonDiv">
                    <button class="btn bg-green pull-right saveClinicalDataButton" style="height:33px;" onclick="save_gynec();"><i class="fa fa-save"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 no-padding clinical_data_container">
        <div class="col-md-3 no-padding toolbar_inner_div" style="margin-top:5px !important;padding: 6px !important;">
            <div class="col-md-12 no-padding">
                <div class="col-md-11 no-padding"><label class="text-normal">Allergic History </label></div>
                <div class="col-md-1 no-padding"><button class="btn bg-blue" onclick="getgynechistory('allergic_history');" style="height: 19px;">
                    <i class="fa fa-history" aria-hidden="true"></i>
                </button></div>
                <textarea data-attr-label="allergic_history" id="allergic_history_data" class="clinical_data_area form-control" rows="4" cols="50" style="height:80px !important;">
                </textarea>

                {{-- <div data-attr-label="allergic_history" id="allergic_history_data" class="lined clinical_data_area ps-container ps-theme-default theadscroll" contenteditable="true" style="height:80px;"></div> --}}
            </div>
            <div class="col-md-12 no-padding">
                <div class="col-md-11 no-padding"><label class="text-normal">Obstretic History </label></div>
                <div class="col-md-1 no-padding"><button class="btn bg-blue" onclick="getgynechistory('obstretic_history');" style="height: 19px;">
                    <i class="fa fa-history" aria-hidden="true"></i>
                </button></div>
                <textarea data-attr-label="obstretic_history" id="obstretic_history_data" class="clinical_data_area form-control" rows="4" cols="50" style="height:80px !important;">
                </textarea>

                {{-- <div data-attr-label="obstretic_history" id="obstretic_history_data" class="lined clinical_data_area ps-container ps-theme-default theadscroll" contenteditable="true" style="height:80px;"></div> --}}


            </div>
            <div class="col-md-12 no-padding">
                <div class="col-md-11 no-padding"><label class="text-normal">Family History </label></div>
                <div class="col-md-1 no-padding"><button class="btn bg-blue" onclick="getgynechistory('family_history');" style="height: 19px;">
                    <i class="fa fa-history" aria-hidden="true"></i>
                </button></div>
                {{-- <div data-attr-label="family_history" id="family_history_data" class="lined clinical_data_area ps-container ps-theme-default theadscroll" contenteditable="true" style="height:70px;"></div> --}}

                <textarea data-attr-label="family_history" id="family_history_data" class="clinical_data_area form-control" rows="4" cols="50" style="height:70px !important;">
                </textarea>

            </div>
            <div class="col-md-12 no-padding">
                <div class="col-md-11 no-padding"><label class="text-normal">Medical and Surgical History </label></div>
                <div class="col-md-1 no-padding"><button class="btn bg-blue" onclick="getgynechistory('medical_and_surgical_history');" style="height: 19px;">
                    <i class="fa fa-history" aria-hidden="true"></i>
                </button></div>
                {{-- <div data-attr-label="medical_and_surgical_history" id="medical_and_surgical_history_data" class="lined clinical_data_area ps-container ps-theme-default theadscroll" contenteditable="true" style="height:70px;"></div> --}}

                <textarea data-attr-label="medical_and_surgical_history" id="medical_and_surgical_history_data" class="clinical_data_area form-control" rows="4" cols="50" style="height:70px !important;">
                </textarea>

            </div>
            <div class="col-md-12 no-padding">
                <div class="col-md-11 no-padding"><label class="text-normal">Nurtitional and Screening </label></div>
                <div class="col-md-1 no-padding"><button class="btn bg-blue" onclick="getgynechistory('nurtitional_and_screening');" style="height: 19px;">
                    <i class="fa fa-history" aria-hidden="true"></i>
                </button></div>
                {{-- <div data-attr-label="nurtitional_and_screening" id="nurtitional_and_screening_data" class="lined clinical_data_area ps-container ps-theme-default theadscroll" contenteditable="true" style="height:80px;"></div> --}}

                <textarea data-attr-label="nurtitional_and_screening" id="nurtitional_and_screening_data" class="clinical_data_area form-control" rows="4" cols="50" style="height:70px !important;">
                </textarea>

            </div>
        </div>

        <div class="col-md-9 no-padding gyncec_main_container" style="height:537px;">

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#assesment_block">Assessment</a></li>
                <li><a data-toggle="tab" href="#assesment_history_block">Assessment History</a></li>
                <li><a data-toggle="tab" href="#investigation_block">Investigation</a></li>
                <li><a data-toggle="tab" href="#prescription_block">Prescription</a></li>
                <li><a data-toggle="tab" href="#inv_result_entry_block">Investigation Result Entry</a></li>
                @if($dynamic_template_enable_emr_lite == 1)
                    <li><a data-toggle="tab" href="#emr_dynamic_template_tab">Dynamic Template</a></li>
                @endif
                {{-- <li><a data-toggle="tab" href="#usg_entry_block">Usg Result</a></li> --}}
            </ul>
            <div class="tab-content">
                <div id="assesment_block" class="tab-pane fade in active">
                  @include('Emr::gynecology_lite.assesment_block')
                </div>
                <div id="assesment_history_block" class="tab-pane fade">
                    <div class="col-md-12 theadscroll"  style="position:absolute;height:490px;">
                        <label class="text-normal">
                            History
                        </label>
                        <span id="gynec_history_container"></span>
                        <span id="gynec_vital_details"></span>
                    </div>
                </div>
                <div id="investigation_block" class="tab-pane fade">
                  @include('Emr::view_patient.investigation_template')
                </div>
                <div id="prescription_block" class="tab-pane fade">
                    @include('Emr::view_patient.prescription_template')
                </div>
                <div id="inv_result_entry_block" class="tab-pane fade">
                    @include('Emr::view_patient.investigation_result_entry')
                </div>
                {{-- <div id="usg_entry_block" class="tab-pane fade">
                    @include('Emr::gynecology_lite.usg_entry')
                </div> --}}

                @if($dynamic_template_enable_emr_lite == 1)
                    <div  id="emr_dynamic_template_tab" class="tab-pane fade">
                        @include('Emr::view_patient.dynamic_templates.custom_forms')
                    </div>
                @endif
            </div>


        </div>
    </div>

    </div>
</div>
