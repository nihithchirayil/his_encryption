
@if(count($rec_loc) > 0)
<div class="theadscroll" style="position: relative; height: 526px;cursor: pointer;">

    @foreach ($rec_loc as $loc)
        @if($loc->status==1)
        @php
            $bg_color='#4bec4bcc'; 
            $doc_active_status='isActive';
        @endphp
        @else
        @php
            $bg_color='#d86363cc'; 
            $doc_active_status='isInActive'

        @endphp
        @endif
                <div class="col-md-12 padding_sm search_user" id="doctorTr{{ $loc->id }}" style="border-bottom: 2px solid #a52a2a59;margin-bottom: 4px;background-color:;height:30px;">
                        <div class="col-md-9"  onclick="" >
                           <label for="" style="font-weight: 700;margin-top:-6px;"> {{ $loc->package_name }} </label>
                        </div>
                        <div class="col-md-1"  onclick="" style="margin: 2px">
                            <button type="button" class="btn btn-default" id="usrDetailsEdit{{ $loc->id }}" onclick="pacDetailsEdit({{ $loc->id }})"><i id="usrDetailsSpin{{ $loc->id }}" class="fa fa-edit"></i></button>
                        </div>
                        <div class="col-md-1"  onclick="" style="margin: 2px">
                            <button type="button" id="usrDetailsDelete{{ $loc->id }}" onclick="pacDetailsDelete({{ $loc->id }})" class="btn btn-default"><i id="usrDetailsDelSpin{{ $loc->id }}" class="fa fa-trash bg-danger"></i></button>

                        </div>
                </div>
       
    @endforeach

</div>
@else
<div class="col-md-12">
    <label for="" style="font-weight: 700;margin-top:5px;text-align:center"> No Record Found.</label>
</div>
@endif

