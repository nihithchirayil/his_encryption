@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
    <style>
        .tr_active {
            background: lightblue !important;
        }
        .search_user:hover {
  background-color:rgb(54 166 147);
}
    </style>
@endsection
@section('content-area')
    <div id="addAssessmentModal" class="modal fade " role="dialog">
        <div class="modal-dialog" style="width:50%">

            <div class="modal-content">
                <div class="modal-header" style="background:#26b99a; color:#ffffff;">
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close close_white">&times;</button>
                    <h4 class="modal-title">Add/Remove Assessment</h4>
                </div>
                <div class="modal-body" id="addAssessmentList" style="height:440px">




                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>

    </div>
    <!-- page content -->
    <div class="right_col">
        <div class="row codfox_container">
            <h5 style="margin: 6px !important;" class="blue pull-right"><strong> <?= $title ?> </strong></h4>
                <div class="clearfix"></div>
                <div class="col-md-4 padding_sm">
                    <div class="box no-border no-margin" style="margin-top: 15px !important;">
                        <div class="box-footer" style="padding: 5px; box-shadow:0 0 3px #36A693!important;height:70px;">
                            <div class="col-md-12 padding_sm">
                                <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
                                <input type="hidden" id="token" value="">
                                <input type="hidden" id="user_id" value="0">
                                <input type="hidden" name="hidden_id" id="hidden_id" value="">
                                <div class="col-md-12 padding_sm ml" style="margin-top: 7px;">
                                    <div class="mate-input-box" style="height: 43px !important">
                                        <label for="">Package Name</label>
                                        <input type="text" id="Search_usr" class="form-control" onkeyup="searchDivUser()"
                                            placeholder="Search Package Name">

                                    </div>
                                </div>
                               



                            </div>
                        </div>
                    </div>
                    <div class="box no-border no-margin" style="margin-top: 10px !important">
                        <div class="box-footer" style="padding: 5px;box-shadow:0 0 3px #36A693!important;height: 535px;"
                            id="searchDataDiv_body">
                            <div id="searchDataDiv"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 padding_sm" id="patientappoinment_div">
                    <div class="box no-border no-margin">
                        <div class="box-footer" style="padding: 5px;min-height: 631px;">
                            <div class="clearfix"></div>

                            <div class="col-md-12 padding_sm" style="margin-top: 10px;min-height: 500px;">
                                <div class="box-footer col-md-12" id="assessment_list_body"
                                    style="padding: 5px; box-shadow:0 0 3px #36A693!important;margin-left:-4px;min-height:550px;">
                                    <div class="col-md-12"
                                        style="padding: 5px;box-shadow:0 0 3px #36A693!important;min-height: 70px;">
                                        <div class="col-md-6">
                                            <div class="mate-input-box" style="height: 43px !important;margin-top: 10px;">
                                                <label>Package Name</label>
                                                <input class="form-control" value="" placeholder="Package Name"
                                                autocomplete="off" type="text" id="selected_service_item" />

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mate-input-box" style="height: 43px !important;margin-top: 10px;">
                                                <label>Coordinator</label>
                                                <select name="" id="selected_package_cood" class="form-control">
                                                    <option value="">Select</option>
                                                    @foreach ($coodinators as $cod)
                                                        <option value="{{ $cod->id }}">{{ $cod->doctor_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>




                                    </div>
                                    <div class="col-md-12"
                                        style="margin-top:10px;ppadding: 5px; box-shadow:0 0 3px #36A693!important;min-height:30px;">
                                        <table style="margin-bottom:10px;font-size:12px">
                                            <thead>
                                                <th width="55%">Search Service Item</th>
                                                <th width="10%" style="text-align: center">Price</th>
                                                <th width="10%" style="text-align: center">Quantity</th>
                                                <th width="10%" style="text-align: center">Amount</th>
                                                <th width="25%" style="text-align: center">Package Price</th>
                                                <th width="10%" style="text-align: center">
                                                    <div class="checkbox checkbox-success inline" style="margin-top:2px;">
                                                        <input id="active_service" type="checkbox" name="checkbox" checked>
                                                        <label class="text-blue" for="active_service">
                                                            Active
                                                        </label>
                                                    </div>
                                                </th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input class="form-control hidden_search addSerRow" value=""
                                                            autocomplete="off" type="text" id="service_item" />
                                                        <div id="service_itemAjaxDiv" class="ajaxSearchBox addSerRow"
                                                            style="width: 100%; position: absolute; margin-top: 21px;">
                                                        </div>
                                                        <input class="filters addSerRow" value="" type="hidden"
                                                            name="service_item_hidden" value=""
                                                            id="service_item_hidden" />
                                                        <input class="filters addSerRow" value="" type="hidden"
                                                            name="service_code_hidden" value=""
                                                            id="service_code_hidden" />
                                                        <input class="filters addSerRow" value="" type="hidden"
                                                            name="service_department_id_hidden" value=""
                                                            id="service_department_id_hidden" />
                                                        <input class="filters addSerRow" value="" type="hidden"
                                                            name="service_dept_name_hidden" value=""
                                                            id="service_dept_name_hidden" />
                                                    </td>
                                                    <td><input type="text"
                                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                            class="form-control addSerRow" id="service_price" readonly></td>
                                                    <td><input type="text"
                                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                            class="form-control addSerRow" id="service_quantity"></td>
                                                    <td><input type="text"
                                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                            class="form-control addSerRow" id="service_amount" readonly></td>
                                                    <td><input type="text"
                                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                            class="form-control addSerRow" id="service_package_price"></td>
                                                    <td style="text-align: center" class="addServiceToPackage" ><i id="service_add"
                                                            class="fa fa-plus btn  btn-block btn-warning"
                                                            id="add_service_item"
                                                            style="margin-left: 6px;margin-top: 3px;"></i></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-12 "
                                        style="margin-top:10px;padding: 0px;box-shadow:0 0 3px #36A693!important;min-height:391px;">
                                        <table class='table table-condensed table_sm table-col-bordered '
                                            style="font-size: 12px;">
                                            <thead>
                                                <tr class="headerclass"
                                                    style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                                                    <th width="30%">Description</th>
                                                    <th width="20%">Doctor</th>
                                                    <th width="10%">Amount</th>
                                                    <th width="10%">Price</th>
                                                    <th width="10%">Discount</th>
                                                    <th width="20%">Department</th>
                                                    <th width="5%"><i class="fa fa-trash"></i></th>
                                                </tr>

                                            </thead>
                                            <tbody id="add_service_to_list">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                <div class="col-md-3" style="margin-top: 10px;">
                                    <div class="mate-input-box" style="height: 43px !important">
                                        <label>Total Package Amount</label>
                                        <input readonly
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            type="text" class="form-control" id="ttl_package_amount" value=""
                                            style="width: 20%;margin-left: 8px !important;">
                                    </div>

                                </div>

                                <div class="col-md-1 pull-right" style="margin-top: 10px;">
                                    <button type="button" class="btn btn-success" id="saveUserMappedDetails"
                                        onclick="">Save <i id="saveUserMappedDetailsSpin"
                                            class="fa fa-save"></i></button>
                                </div>
                                <div class="col-md-1 pull-right" style="margin-top: 10px;">
                                    <button type="button" class="btn btn-warning" id=""
                                        onclick="resetSettingDiv()">Reset <i id=""
                                            class="fa fa-recycle"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

        </div>
    </div>
    </div>

    <!-- /page content -->

@stop
@section('javascript_extra')

    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/select2/select2.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/health_packages.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>

@endsection
