<style>
    li tr:hover { 
   background-color:#4c6456 !important;   
   color: white;  
   opacity: 1.0;
}
</style>
<?php
if(count($resultData)!=0){
    if($input_id == 'service_item'){
        if (!empty($resultData)) {
            ?>
            <table class='table table-condensed table_sm table-col-bordered '
            style="font-size: 12px;margin:0px;">
                <thead>
                    <tr class="headerclass" style="background-color:rgb(54 166 147);color:black;border-spacing: 0 1em;font-family:sans-serif">
                        <th width="50%">Service Name</th>
                        <th width="20%">Service Code</th>
                        <th width="20%">Department</th>
                        <th width="10%">Price</th>
                    </tr>
                </thead>
            </table>
            <?php  
            foreach ($resultData as $item) {
                
            ?>  

<li style="display: block; "
    onclick='fillSearchDetials("{{ $item->id }}","{{ htmlentities(ucfirst($item->service_desc)) }}","{{ $input_id }}","{{ $item->service_code }}","{{ $item->department_id }}","{{ $item->dept_name }}","{{ $item->price }}")'>
    <table width="100%">
        <thead>
            <tr>
                <th width="50%"></th>
                <th width="20%"></th>
                <th width="20%"></th>
                <th width="10%"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class=""style=""> {{ $item->service_desc }}</td>
                <td class="common_td_rules"style="">{{ $item->service_code }}</td>
                <td class="common_td_rules"style="">{{ $item->dept_name }}</td>
                <td class="td_common_numeric_rules"style="">{{ $item->price }}</td>
            </tr>
        </tbody>
    </table>
</li>
<?php
            }
        } else {
            echo 'No Results Found';
        }
    }
}else {
            echo 'No Results Found';
        }
?>
