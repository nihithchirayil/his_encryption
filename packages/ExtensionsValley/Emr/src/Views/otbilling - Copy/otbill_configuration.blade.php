@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<style>

.search_header{
    background: #36A693 !important;
    color: #FFF !important;
}

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="user_id" value="{{$user_id}}">
<div class="right_col">
    <div class="col-md-12" style="margin:0px">
        <div class="container-fluid" style="padding: 15px;">
            <div class="row codfox_container">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-12 no-padding">
                        <div class="box no-border" >
                            <div class="box-header search_header">
                                <span class="padding_sm">OT Bill Configuration</span>
                            </div>
                            <div class="box-body clearfix">
                                
                                <div class="col-md-4 padding_sm">
                                    <?php
                                        $billtags = \ExtensionsValley\Purchase\CommonInvController::getAllOTBillTags([4,5]);
                                    ?>
                                    <label for="" class="header_label">Bill Tag</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('bill_tag', $billtags, null,['class' => 'form-control select2 abc','placeholder' => 'Bill Tag','title' => 'Bill Tag','id' => 'bill_tag','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                </div>

                                <div class="col-md-1 col-md-offset-6 padding_sm saveConfigurationDiv" style="display:none;">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary saveConfiguration"><i class="fa fa-save"></i> Save</button>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 box no-border no-margin" >
                                
                        <table class="table table-bordered table_sm no-margin no-border theadfix_wrapper">
                            <thead>
                                <tr class="table_header_bg">
                                    <th class="text-center" width="40%">Component Name</th>
                                    <th class="text-center" width="30%">Percentage (%)</th>
                                    <th class="text-center" width="30%">Status</th>
                                </tr>
                            </thead>
                            <tbody class="bill_configuration_body" style="min-height:450px;">
                                
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12 box no-border no-margin" >
                                
                        <table class="table table-bordered table_sm no-margin no-border theadfix_wrapper">
                            <thead>
                                <tr class="table_header_bg">
                                    <th class="text-center" width="40%">Total Percentage : </th>
                                    <th class="text-center total_percentage" width="30%">0</th>
                                    <th class="text-center total_percentage_error" width="30%" style="color:red !important;"></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
        

                </div>
            </div>

        </div>
    </div>
</div>


@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/otbillconfiguration.js")}}"></script>


<script type="text/javascript">

</script>
@endsection
