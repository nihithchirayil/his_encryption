@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<style>

.search_header{
    background: #36A693 !important;
    color: #FFF !important;
}

</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="user_id" value="{{$user_id}}">
<div class="right_col">
    <div class="col-md-12" style="margin:0px">
        <div class="container-fluid" style="padding: 15px;">
            <div class="row codfox_container">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-12 no-padding">
                        <div class="box no-border" >
                            <div class="box-header search_header">
                                <span class="padding_sm">OT Bill Updation</span>
                            </div>
                            <div class="box-body clearfix">
                                
                                <div class="col-md-2 padding_sm">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" data-attr="date" class="form-control from_date" name="from_date" value="">
                                </div>

                                <div class="col-md-2 padding_sm">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" data-attr="date" class="form-control to_date" name="to_date" value="">
                                </div>

                                <div class="col-md-2 padding_sm" >
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <input type="checkbox" class="show_procecessed" name="show_procecessed" placeholder="Show Processed" > Show Processed
                                </div>

                                <div class="col-md-1 padding_sm" >
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary serachOTUpdationBills"><i class="fa fa-search"></i> Search</button>
                                </div>
                                
                                
                                
                                <div class="col-md-2 col-md-offset-1 padding_sm" >
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <input type="checkbox" class="check_all" name="check_all" placeholder="Check All" > Check All
                                </div>
                                
                                <div class="col-md-1 padding_sm processOTBillsDiv" style="display:none;" >
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary processOTBills btn-warning"><i class="fa fa-check"></i> Process </button>
                                </div>
                                
                                <div class="col-md-1 padding_sm revertOTBillsDiv" style="display:none;" >
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary revertOTBills btn-warning"><i class="fa fa-check"></i> Revert </button>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ht10"></div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 box no-border no-margin" >
                                
                        <table class="table table-bordered table_sm no-margin no-border theadfix_wrapper table-striped">
                            <thead>
                                <tr class="table_header_bg">
                                    <th class="text-center" >Sl. No.</th>
                                    <th class="text-center" >Bill No</th>
                                    <th class="text-center" >Bill Tag</th>
                                    <th class="text-center" >Doctor</th>
                                    <th class="text-center" >Patient</th>
                                    <th class="text-center" >Total Amount</th>
                                    <th class="text-center" >Select</th>
                                </tr>
                            </thead>
                            <tbody class="bill_updation_body" >
                                
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>

        </div>
    </div>
</div>


@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/otbillupdation.js")}}"></script>


<script type="text/javascript">

</script>
@endsection
