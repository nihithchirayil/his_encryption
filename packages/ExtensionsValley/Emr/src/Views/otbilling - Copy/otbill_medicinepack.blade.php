@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/purchase-custom.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/purchase/default/css/report.css")}}" rel="stylesheet">
<style>

.search_header{
    background: #36A693 !important;
    color: #FFF !important;
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="user_id" value="{{$user_id}}">
<div class="right_col">
    <div class="col-md-12" style="margin:0px">
        <div class="container-fluid" style="padding: 15px;">
            <div class="row codfox_container">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-12 no-padding">
                        <div class="box no-border" >
                            <div class="box-header search_header">
                                <span class="padding_sm">Medicine Pack</span>
                            </div>
                            <div class="box-body clearfix">

                                <div class="col-md-2 padding_sm">
                                    <label for="">From Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" data-attr="date" class="form-control from_date" name="from_date" value="">
                                </div>

                                <div class="col-md-2 padding_sm">
                                    <label for="">To Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" data-attr="date" class="form-control to_date" name="to_date" value="">
                                </div>

                                <div class="col-md-2 padding_sm">
                                    <label for="">Doctor</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('admitting_doctor',$doctor_list, null,['class' => 'form-control select2 doctor','placeholder' => 'Doctor','title' => 'Doctor','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                </div>
                                
                                <div class="col-md-2 padding_sm">
                                    <label for="">Anesthesia Doctor</label>
                                    <div class="clearfix"></div>
                                    {!! Form::select('anesthesia_doctor',$doctor_list, null,['class' => 'form-control select2 anesthesia_doctor','placeholder' => 'Anesthesia Doctor','title' => 'Anesthesia Doctor','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                </div>

                                <div class="col-md-1  padding_sm" >
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary getReport"><i class="fa fa-search"></i> Get Report</button>
                                </div>
                                <div class="col-md-3 col-md-offset-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <span style="margin-right: 5%;">Print Mode :</span>
                                    <input style="margin-right: 1%;" type="radio" name="printMode" checked="true" value="1">Portrait
                                    <input style="margin-left:3px;margin-right: 1%;" type="radio" name="printMode" value="2">Landscape
                                </div>
                                <div class="col-md-1  padding_sm" >
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary printReport"><i class="fa fa-print"></i> Print </button>
                                </div>




                                <div class="clearfix"></div>
                                <div class="ht10"></div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 box no-border no-margin " id="printData">

                        <table class="table table_sm no-margin no-border table-striped">
                            <thead>
                                <tr>
                                    <td colspan="10" style="padding: 0px;">
                                        <table style="width: 100%; font-size: 12px;">
                                            <tbody>
                                                <tr>
                                                    <td style="margin-top: 50px;padding: 3px;border: 1px solid #FFF; text-align: center;" align="center" valign="top">
                                                        {!! $hospital_header !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="10" align="center" style="border-right: 1px solid #FFF; border-left: 1px solid #FFF; padding: 20px 0;">
                                                        <i class='report_date'></i><br>
                                                        <i class='report_date_range'></i><br>
                                                        <span>
                                                            <i> Medicine Pack </i>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </thead>
                            <tbody style="background: #36A693; color: #fff;">
                                <tr style="background-color:#fff ">
                                    <th colspan="10" style="border: 1px solid #fff;"><hr style="margin:0"></th>
                                </tr>
                                <tr class="headerclass"
                                style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                    <th>Bill No.</th>
                                    <th>UHID</th>
                                    <th>Doctor.</th>
                                    <th colspan="5">Medicine Pack.</th>
                                    <th class="printHideCols" colspan="2">Theatre and Surgeon Charge.</th>
                                </tr>
                                <tr style="background-color:#fff ">
                                    <th colspan="10" style="border: 1px solid #fff;"><hr style="margin:0"></th>
                                </tr>
                                <tr class="headerclass"
                                style='background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif'>
                                    <th style="" colspan="2">Patient Name</th>
                                    <th style="">Cash</th>
                                    <th>OT</th>
                                    <th>MED PACK</th>
                                    <th>ANAST</th>
                                    <th class="printHideCols">GF</th>
                                    <th class="printHideCols">Over Time</th>
                                    <th class="printHideCols">SUR_SH</th>
                                    <th class="printHideCols">ANS_HSP</th>
                                </tr>
                                <tr class="headerclass"
                    style="background-color:rgb(91 110 91);color:white;border-spacing: 0 1em;font-family:sans-serif">
                                        <th colspan="10" style="border: 1px solid #fff;"><hr style="margin:0"></th>
                                </tr>
                            </tbody>
                            <tbody class="otbill_medicinepack_body" >

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>

        </div>
    </div>
</div>


@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/otbillmedicinepack.js")}}"></script>


<script type="text/javascript">

</script>
@endsection
