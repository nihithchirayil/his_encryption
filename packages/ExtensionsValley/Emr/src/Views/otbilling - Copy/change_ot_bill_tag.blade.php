@extends('Dashboard::dashboard.dashboard')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/requestforquotation.css') }}" rel="stylesheet">
@endsection
@section('content-area')
    <!-- page content -->
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <input type="hidden" id="c_token" value="{{ csrf_token() }}">
    <input type="hidden" id="user_id" value="{{ $user_id }}">
    @php
    $billtags = \ExtensionsValley\Purchase\CommonInvController::getAllOTBillTags([4]);
    @endphp
    <div class="right_col">
        <div class="row">
            <div class="col-md-2 pull-right padding-sm">
                Bill Details
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12" style="margin:0px">
            <div class="container-fluid" style="padding: 15px;">
                <div class="row codfox_container">
                    <div class="col-md-12 padding_sm">
                        <div class="box no-border">
                            <div class="box-body clearfix">
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">From Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" data-attr="date" class="form-control from_date" name="from_date"
                                            value="">
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">To Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" data-attr="date" class="form-control to_date" name="to_date"
                                            value="">
                                    </div>
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Bill No.</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control bill_no_search"
                                            id="bill_no" name="bill_no" value="">
                                        <div id="bill_no_AjaxDiv" class="ajaxSearchBox"
                                            style="margin-top:12px;z-index: 99999;"></div>
                                        <input type="hidden" name="bill_no_hidden" value="" id="bill_no_hidden">
                                        <input type="hidden" name="bill_id_hidden" value="" id="bill_id_hidden">
                                    </div>
                                </div>

                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Patient Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text" autocomplete="off" class="form-control" id="patient_name"
                                            name="patient_name" value="">
                                        <div id="patient_idAjaxDiv" class="ajaxSearchBox"
                                            style="margin-top:12px;z-index: 99999;"></div>
                                        <input type="hidden" name="patient_id_hidden" value="" id="patient_id_hidden">
                                        <input type="hidden" class="patient_uhid_search" name="patient_uhid_hidden" value=""
                                            id="patient_uhid_hidden">
                                    </div>
                                </div>

                                <div class="col-md-2 padding_sm">
                                    <div class="mate-input-box">
                                        <label for="">Bill Tag</label>
                                        <div class="clearfix"></div>
                                        {!! Form::select('bill_tag_search', $billtags, null, ['class' => 'form-control select2', 'placeholder' => 'Bill Tag', 'title' => 'Bill Tag', 'id' => 'bill_tag_search', 'style' => 'color:#555555; padding:2px 12px; ']) !!}
                                    </div>
                                </div>

                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary searchBills"><i class="fa fa-search"></i>
                                        Search </button>
                                </div>
                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary clearSearch"><i class="fa fa-refresh"></i>
                                        Clear </button>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="row codfox_container searchBillContent">

                </div>

                <div class="col-md-2 padding_xs">
                    <span style="float:left">&nbsp;<i class="fa fa-square" style="color: #ccd814;"></i>&nbsp; Updation
                        Done</span>
                </div>

            </div>
        </div>
    </div>


    <!-- View OT Bills Modal -->
    <div id="change_otbilltag_modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> View Bill Details </h4>
                </div>
                <div class="modal-body">
                    <table
                        class="table table-striped table-bordered table-condensed theadfix_wrapper sorted_table table_sm">
                        <tbody>
                            <tr>
                                <td class="left_side_text">Bill No</td>
                                <td class="bill_no right_side_text"></td>
                            </tr>
                            <tr>
                                <td class="left_side_text">Bill Tag</td>
                                <td class="bill_tag right_side_text"></td>
                            </tr>
                            <tr>
                                <td class="left_side_text">Patient Name</td>
                                <td class="patient_name right_side_text"></td>
                            </tr>
                            <tr>
                                <td class="left_side_text">UHID</td>
                                <td class="uhid right_side_text"></td>
                            </tr>
                            <tr>
                                <td class="left_side_text">Admitting Doctor</td>
                                <td class="admitting_doctor right_side_text"></td>
                            </tr>
                            <tr>
                                <td class="left_side_text">Total Bill Amount</td>
                                <td class="bill_amount right_side_text"></td>
                            </tr>
                            <tr>
                                <td class="left_side_text">Total Net Amount</td>
                                <td class="total_net_amount right_side_text"></td>
                            </tr>
                            <tr>
                                <td class="left_side_text">Bill Date</td>
                                <td class="bill_date right_side_text"></td>
                            </tr>
                            <tr>
                                <td class="left_side_text">Created Date</td>
                                <td class="created_date right_side_text"></td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="clearfix"></div>

                    <table
                        class="table table-striped table-bordered table-condensed theadfix_wrapper sorted_table table_sm">
                        <tbody>
                            <tr>

                                <td class="left_side_text">New Bill Tag</td>
                                <td class="right_side_text">
                                    {!! Form::select('new_bill_tag', $billtags, null, ['class' => 'form-control select2', 'placeholder' => 'Bill Tag', 'title' => 'Bill Tag', 'id' => 'new_bill_tag', 'style' => 'color:#555555; padding:2px 12px; ']) !!}
                                </td>
                            </tr>

                        </tbody>
                    </table>



                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-success saveBillTagBtn">Save</button>
                    <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- View OT Bills Modal -->

@stop
@section('javascript_extra')

    <script src="{{ asset('packages/extensionsvalley/master/default/javascript/loadingoverlay.min.js') }}"></script>
    {!! Html::script('packages/extensionsvalley/default/js/bootbox.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
    {!! Html::script('packages/extensionsvalley/default/plugins/floathead/jquery.floatThead.js') !!}
    <script
        src="{{ asset('packages/extensionsvalley/emr/js/changeotbilltag.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
@endsection
