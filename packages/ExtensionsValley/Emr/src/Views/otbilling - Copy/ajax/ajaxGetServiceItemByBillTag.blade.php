<?php


//--------op get service item by bill tag --------------------
if (isset($service_item_details) ) {
    if(!empty($service_item_details)){
        foreach($service_item_details as $record) {
            ?>
                <tr>
                    <?php
                        foreach($record as $item) {
                    ?>
                    <td style="width: 3%; padding:0 0 0 0; text-align: center;">
                        <input name="" data-detail-id="<?php echo htmlentities(ucfirst($item->id)) ?>" data-head-id="<?php echo htmlentities(ucfirst($item->head_id)) ?>" data-bill-type="5" data-service-code="{!! $item->service_code !!}" class="deleteID bill_type_check" type="checkbox" id="bill-type-five-<?php echo htmlentities(ucfirst($item->service_code)) ?>" onclick="checkBoxClick('{!! $item->service_code !!}', '1');" value="<?php echo htmlentities(ucfirst($item->service_code)) ?>">
                    </td>
                    <td style="width: 21%;" onclick="selectOTItem('bill-type-five-<?php echo htmlentities(ucfirst($item->service_code)) ?>','0');" class="package">
                        <span><?php echo htmlentities(ucfirst($item->service_desc)) ?></span>
                    </td>
                    <td style="width: 10%;">
                        <input type="number" min="0" max="1000000" class="form-control amount item-amount-{!! $item->service_code !!}" style="text-align: right;" name="" autocomplete="off" data-service-code="<?php echo htmlentities(ucfirst($item->service_code)) ?>" onblur="calculateTotal();" id="field_selling_price-bill-type-five-<?php echo htmlentities(ucfirst($item->service_code)) ?>" value="<?php echo htmlentities(ucfirst($item->price)) ?>" id="">
                    </td>
                    <?php
                    }
                    ?>
                </tr>
            <?php
        }
    } else {
        echo '<tr> <td style="padding:0 0 0 0; text-align: center;" colspan="9"> No Results Found </td> </tr>';
    }
}

?>

        