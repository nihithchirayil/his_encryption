<?php 
?>

@foreach($bill_final as $key => $bill_details)
<style>@media all {
                .page-break	{ display: none; } }@media print {
                .page-break	{ display: block; page-break-before: always; } }</style>
    <table style='width: 700px; font-size: 12px;'>
        <tbody>
            <td colspan=6 style='padding: 3px;' width='85%' align='center' valign='top'>
                {!! $hospital_header !!}
            </td>
            <tr>
                <td colspan=6 style='border-bottom:solid;'></td>
            </tr>
            <tr>
                <td colspan=4 style='text-align: right;'>
                    @if($bill_final[$key][0]->ot_bill_group_code == "B1")
                    <b>OT INSTRUMENT BILL</b>
                    @elseif($bill_final[$key][0]->ot_bill_group_code == "B2")
                    <b>STERRAD AND OTHERS BILL</b>
                    @elseif($bill_final[$key][0]->ot_bill_group_code == "B3")
                    <b>DR.ROY PACKAGES</b>
                    @elseif($bill_final[$key][0]->ot_bill_group_code == "B4")
                    <b>{{ $bill_final[$key][0]->bill_tag_name }}</b>
                    @elseif($bill_final[$key][0]->ot_bill_group_code == "B5")
                    <b>{{ $bill_final[$key][0]->bill_tag_name }}</b>
                    @elseif($bill_final[$key][0]->ot_bill_group_code == "B6")
                    <b>{{ $bill_final[$key][0]->bill_tag_name }}</b>
                    @endif
                </td>
                <td colspan = 2 style='text-align: right;'> 
                    <b>{{ $bill_final[$key][0]->payment_type_name }}</b>
                </td>
            </tr>
            <tr>
                <td colspan=5 style='border-bottom: thin solid;'></td>
            </tr>
            <tr>
                <td colspan=5></td>
            </tr>
            <tr>
                <td colspan=2>Patient Name :{{ $bill_final[$key][0]->patient_name }} </td>
                <td colspan=2 style='text-align: center;'>Age/Gender - {{ $bill_final[$key][0]->patient_age }}/ {{ $bill_final[$key][0]->gender_name }}</td> 
                <td colspan=2 style='text-align: right;'>Consulting Doctor :{{ $bill_final[$key][0]->consulting_doctor_name }}</td>
            </tr>
            <tr>
                <td colspan=2>Op.No :{{ $bill_final[$key][0]->uhid }}</td>
                <td colspan=2 style='text-align: center;'>Bill No:<b>{{ $bill_final[$key][0]->bill_no }}</td>
                <td colspan=2 style='text-align: right;'>Admitting Doctor :{{ $bill_final[$key][0]->doctor_name }}</td>
            </tr>
            @if(isset($bill_final[$key][0]->anesthesia_doctor_name))
            <tr>
                <td colspan=2></td>
                <td colspan=2></td>
                <td colspan=2 style='text-align: right;'>Anesthesia Doctor :{{ $bill_final[$key][0]->anesthesia_doctor_name }}</td>
            </tr>
            @endif
            <tr>
                <td colspan=4>Address :{{ $bill_final[$key][0]->address }}<br>Phone No :{{ $bill_final[$key][0]->phone }}</td>
                <td colspan=2 style='text-align: right;'>Date &Time : {{ date('M-d-Y H:i:s', strtotime($bill_final[$key][0]->bill_datetime)) }}</td>
            </tr>
        </tbody>
    </table>
            
    <table style='width: 710px; font-size: 12px;'>
        <tbody>
            <tr>
                <td colspan=6 style='border-bottom: thin solid;'></td>
            </tr>
            <tr>
                <td><b>SL no </b></td>
                <td><b>Service</b></td>
                @if($bill_final[$key][0]->ot_bill_group_code == "B4")
                <td>Bill Amount</td>
                <td>Discount Amount</td>
                <td colspan="2" style="text-align:right;">Net Amount</td>
                @else
                <td colspan="4" style="text-align:right;">Net Amount</td>
                @endif
            </tr>
            <tr>
                <td colspan=6 style='border-bottom: thin solid;'></td>
            </tr>
            @foreach($bill_details as $bill_detail)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $bill_detail->item_desc }}</td>
                @if($bill_final[$key][0]->ot_bill_group_code == "B4")
                <td>{{ $bill_detail->actual_selling_price ?? 0 }}</td>
                <td>{{ $bill_detail->discount_amount ?? 0 }}</td>
                <td colspan="2" style="text-align:right;">{{ $bill_detail->net_amount ?? 0 }}</td>
                @else
                <td colspan="4" style="text-align:right;">{{ $bill_detail->net_amount ?? 0 }}</td>
                @endif
            </tr>
            @endforeach
            <tr>
                <td colspan=6 style='border-bottom: thin solid;'></td>
            </tr>
            <tr>
                <td colspan='4'>{{  \ExtensionsValley\Master\GridController::number_to_word($bill_final[$key][0]->total_payable_amount) }}</td>
                <td colspan='2' style='text-align: right;'> Total. Rs: <b>{{  $bill_final[$key][0]->total_payable_amount }}</b></td>
            </tr>
            <tr>
                <td colspan=6 style='border-bottom: thin solid;'>Prepared By: {{  $bill_final[0][0]->created_by }}</td>
            </tr>
        </tbody>
    </table>
    @if(!$loop->last)
    <br><br><br><br><div class='page-break'></div>
    @endif
@endforeach
<?php


?>