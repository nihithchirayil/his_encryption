@extends('Emr::emr.page')
@section('content-header')

<!-- Navigation Starts-->
@include('Dashboard::dashboard.partials.headersidebar')
<!-- Navigation Ends-->

@stop
@section('css_extra')
<link href="{{asset("packages/extensionsvalley/emr/toastr/toastr.css")}}" rel="stylesheet">
<link href="{{asset("packages/extensionsvalley/emr/css/emr-custom.css")}}" rel="stylesheet">
<style>
.block_list button {
    padding: 2px;
    font-size: 13px;
}
.block_list h5 {
    font-size: 13px;
}
.position_fixed_header{
    border-radius:0px;
}
.ajaxSearchBox {
    display:none;
    text-align: left;
    list-style: none;
    cursor: pointer;
    max-height: 350px;
    margin: 20px 0px 0px 0px;
    overflow-y: auto;
    width: auto;
    z-index: 599;
    position:absolute;
    background: #ffffff;
    border-radius: 3px;
    border: 1px solid rgba(0, 0, 0, 0.3);

}
.popover{
    max-height: 100%;
    max-width: 90%;
}
.custom_floatlabel{
    top: -26px;
}
.liHover{
         background: #cde3eb;
}
.col-md-4{
    padding-right: 3px;
    padding-left: 3px;
}
.custom_floatlabel {
    color: #686868;
}

.select_button > li.active {
    background: #5f89e4;
    color: #FFF;
}
.select_button > li{
    background: #aed8e7;
    color: rgb(0, 0, 0);
}
.btn-lg{
    width: 120px;
    height: 30px;
    padding-top: 6px;
}
.search_header{
    background: #36A693 !important;
    color: #FFF !important;
}
.pop_container{
    position: relative !important;
}
.pop{
    background: #DEECF9 none repeat scroll 0 0;
    border-radius: 6px;
    box-shadow: 1px 1px 3px #d0d0d0;
    padding: 10px;
    position: absolute;
    top: 41px;
    /* max-width: 114%; */
    z-index: 850;
    width: 450px;
    right: -125px;
}

.pop_closebtn{
    background: #333 none repeat scroll 0 0;
    border-radius: 50%;
    box-shadow: 0 0 5px #a2a2a2;
    color: #fff;
    cursor: pointer;
    font-size: 12px;
    font-weight: bold;
    height: 20px;
    padding: 2px 6px;
    position: absolute;
    right: -10px;
    top: -4px;
    width: 20px;
}
</style>
@endsection
@section('content-area')
<!-- page content -->
<input type="hidden" id="base_url" value="{{URL::to('/')}}">
<input type="hidden" id="c_token" value="{{csrf_token()}}">
<input type="hidden" id="user_id" value="{{$user_id}}">
<input type="hidden" id="surgery_charge_service_code" value="{{$ot_bill_config_details['surgery_charge_service_code']}}">
<input type="hidden" id="cssd_charge_service_code" value="{{$ot_bill_config_details['cssd_charge_service_code']}}">
<input type="hidden" id="asst_service_charge_service_code" value="{{$ot_bill_config_details['asst_service_charge_service_code']}}">
<input type="hidden" id="local_anesthesia_charge_service_code" value="{{$ot_bill_config_details['local_anesthesia_charge_service_code']}}">
<div class="right_col">
    <div class="col-md-12" style="margin:0px">
        <div class="container-fluid" style="padding: 15px;">
            <div class="row codfox_container">
                <div class="col-md-12 padding_sm">
                    <div class="col-md-12 no-padding">
                        <div class="box no-border" >
                            <div class="box-header search_header">
                                <span class="padding_sm">Patient Details</span>
                            </div>
                            <div class="box-body clearfix">
                                <div class="col-md-2 padding_sm">
                                    <label for="">Bill Date</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control bill_date" name="bill_date" disabled value="">
                                </div>
                                <div class="col-md-3 padding_sm">
                                    <label for="">UHID</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control patient_uhid abc" name="uhid" value="" autocomplete="off">
                                    <div class="ajaxSearchBox OpAjaxDiv" style="z-index:999;width:340px; overflow: scroll;"></div>
                                </div>

                                
                                <div class="col-md-3 padding_sm">
                                    <label for="">Patient Name</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control patient_name abc" name="patient_name" value="">
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <label for="">Age</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control age abc" name="age" value="">
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <label for="">Gender</label>
                                    <div class="clearfix"></div>
                                    <!-- <input type="text" class="form-control gender" name="gender" value=""> -->
                                    {!! Form::select('gender',$gender_list, null,['class' => 'form-control select2 abc','placeholder' => 'Gender','title' => 'Gender','id' => 'gender','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <label for="">Surgeon</label>
                                    <div class="clearfix"></div>
                                    <!-- <input type="text" class="form-control surgeon" name="surgeon" value=""> -->
                                    {!! Form::select('surgeon',$doctor_list, null,['class' => 'form-control select2','placeholder' => 'Surgeon','title' => 'Surgeon','id' => 'surgeon','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <label for="">Admitting Doctor</label>
                                    <div class="clearfix"></div>
                                    <!-- <input type="text" class="form-control admitting_doctor " name="admitting_doctor" value=""> -->
                                    {!! Form::select('admitting_doctor',$doctor_list, null,['class' => 'form-control select2','placeholder' => 'Admitting Doctor','title' => 'Admitting Doctor','id' => 'admitting_doctor','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                </div>
                                <div class="col-md-2 padding_sm">
                                    <label for="">Anesthesia Doctor</label>
                                    <div class="clearfix"></div>
                                    <!-- <input type="text" class="form-control anesthesia_doctor" name="anesthesia_doctor" value=""> -->
                                    {!! Form::select('anesthesia_doctor',$doctor_list, null,['class' => 'form-control select2','placeholder' => 'Anesthesia Doctor','title' => 'Anesthesia Doctor','id' => 'anesthesia_doctor','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                </div>
                            
                                <div class="col-md-1 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <a href="{{Request::url()}}" class="btn btn-block btn-primary"><i class="fa fa-refresh"></i> Reset</a>
                                </div>
                                
                                <div class="col-md-1 col-md-offset-2 padding_sm">
                                    <label for="">&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-block btn-primary viewBills"><i class="fa fa-search"></i> View Bills</button>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="nav-tabs-custom blue_theme_tab no-margin">
                <ul class="nav nav-tabs sm_nav text-center no-border">
                <!-- left side -->
                    <li class="active"> <a style="padding: 4px 8px;" href="#tab1" data-toggle="tab" aria-expanded="true" class="B1 bill_tabs">Bill1</a></li>
                    <li class=""> <a style="padding: 4px 8px;" href="#tab2" data-toggle="tab" aria-expanded="false" class="B2 bill_tabs">Bill2</a></li>
                    <li class=""> <a style="padding: 4px 8px;" href="#tab3" data-toggle="tab" aria-expanded="false" class="B3 bill_tabs">Bill3</a></li>
                    <li class=""> <a style="padding: 4px 8px; white-space: nowrap;" href="#tab4" data-toggle="tab" aria-expanded="false" class="B4 bill_tabs">OT Bill</a></li>
                    <li class=""> <a style="padding: 4px 8px;" href="#tab5" data-toggle="tab" aria-expanded="false" class="B5 bill_tabs">Bill5</a></li>
                    <li class=""> <a style="padding: 4px 8px;" href="#tab6" data-toggle="tab" aria-expanded="false" class="B6 bill_tabs">Bill6</a></li>
                
                <!-- right side -->
                    <li class="col-md-1 padding_sm" style="font-weight:600;"><label for="">Bill No</label></li>
                    <li class="col-md-2 padding_sm"><input type="text" class="form-control bill_no abc" name="bill_no" value="" disabled></li>
                    <li class="col-md-1 padding_sm" style="font-weight:600;"><label for="">Net Amount</label></li>
                    <li class="col-md-2 padding_sm"><input type="number" class="form-control net_amount" name="net_amount abc" value="0" disabled></li>
                    <li class="col-md-2 padding_sm payment_type_li">{!! Form::select('payment_type',$payment_types, null,['class' => 'form-control select2','placeholder' => 'Payment Type','title' => 'Payment Type','id' => 'payment_type','style' => 'color:#555555; padding:2px 12px; ']) !!} </li>
                    
                    <li class="col-md-1 padding_sm"><button class="btn btn-block btn-primary saveButton"><i style="position: unset;" class="fa fa-save"></i> Save</button></li>
                    <li class="col-md-2 padding_sm"><button class="btn btn-block btn-primary saveAndPrintButton"><i style="position: unset;" class="fa fa-save"></i>  Save & Print</button></li>
                    <li class="col-md-1 padding_sm"><button class="btn btn-block btn-primary print_bill" style="display:none;"><i style="position: unset;" class="fa fa-print"></i> Print </button></li>
                    <li class="col-md-1 padding_sm"><button class="btn btn-block btn-primary bill_cancel" style="display:none;"><i style="position: unset;" class="fa fa-trash"></i> Cancel</button></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab1">
                        <div class="container-fluid" style="padding: 10px;">
                            <div class="row codfox_container">
                                <div class="col-md-12 box no-border no-margin">
                                    <table class="table table-bordered table_sm no-margin no-border">
                                        <thead>
                                            <tr class="table_header_bg">
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody class="bill1_body">
                                            @foreach($bill1 as $row)
                                            @foreach($row as $record)
                                            <tr>
                                            @foreach($record as $item)
                                                <td style="padding: 0px; background: {!! $item['color_code'] !!} ">
                                                    <div class="col-md-12 padding-sm">
                                                        <input  name="{!! $item['group_name'] !!}" class="deleteID bill_one_check" type="checkbox" data-head-id="{!! $item['id'] !!}" id="head-id-{!! $item['id'] !!}" onclick="checkBoxClick('{!! $item['id'] !!}');" value="{!! $item['group_name'] !!}">
                                                    </div>
                                                </td>
                                                <td title="" style="width: 21%; padding: 0px; background-color: {!! $item['color_code'] !!} " class="package">
                                                    <div class="pull-left group-name-{{$item['id']}}" style="width: 79%; padding-bottom: 4px; padding-top: 4px;" onclick="selectOTItem('head-id-{!! $item['id'] !!}','{!! $item['id'] !!}');">
                                                        <input type="hidden" name="" value="{!! $item['amount'] !!} ">
                                                        <span>{!! $item['group_name'] !!} </span>
                                                    </div>
            
                                                    <button type="button" class="btn-info pull-right" onclick="displaySubPackages({!! $item['id'] !!}, '1', '1');" style="border-radius: 50%; width: 20px; height: 20px; border: none;">
                                                        <i class="fa fa-info"> </i>
                                                    </button>

                                                    <div class="pop_container">
                                                        <input type="hidden" name="" value="{!! $item['id'] !!}">
                                                        <div class="popupbox_div">
                                                            <div class="popupbox">
                                                                <div class="pop " id="popupDiv{!! $item['id'] !!}" style="width: auto; top: 35px; display:none;">
                                                                </div> 
                                                            </div>     
                                                        </div>
                                                    </div>
            
                                                </td>
                                                    <td title="" style="width: 10%; background-color:  {!! $item['color_code'] !!}">
                                                    <input type="number" min="0" max="1000000" class="form-control amount" style="text-align: right;" readonly="" name="" autocomplete="off" onblur="calculateTotal();" value="{!! $item['amount'] !!}" id="field_selling_price-head-id-{!! $item['id'] !!}">
                                                </td>
                                            @endforeach
                                            @if(count($record)%3 > 0)
                                                <td colspan="{!! (3-(count($record)%3))*3 !!}" style="background-color: {!! $record[0]['color_code'] !!}"></td>
                                            @endif
                                            </tr>
                                            @endforeach
                                            @endforeach
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="tab2">

                        <div class="container-fluid" style="padding: 10px;">
                            <div class="row codfox_container">
                                <div class="col-md-12 box no-border no-margin">
                                    <table class="table table-bordered table_sm no-margin no-border" >
                                        <thead>
                                            <tr class="table_header_bg">
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody class="bill2_body">
                                            
                                            @foreach($bill2 as $row)
                                            <tr>
                                            @foreach($row as $record)
                                            <td style="width: 3%; padding:0 0 0 0; text-align: center;">
                                                <input name="" data-detail-id="{!! $record->id !!}" data-head-id="{!! $record->head_id !!}" data-bill-type="2" data-service-code="{!! $record->service_code !!}" class="deleteID bill_type_check" type="checkbox" id="bill-type-two-{!! $record->service_code !!}" onclick="checkBoxClick('{!! $record->service_code !!}', '1');" value="{!! $record->service_code !!}">
                                            </td>
                                            <td style="width: 21%;" onclick="selectOTItem('bill-type-two-{!! $record->service_code !!}','0');" class="package">
                                                <span>{!! $record->service_desc !!}</span>
                                            </td>
                                            <td style="width: 10%;">
                                                <input type="number" min="0" max="1000000" class="form-control amount item-amount-{!! $record->service_code !!} " style="text-align: right;" name="" autocomplete="off" onblur="calculateTotal();" id="field_selling_price-bill-type-two-{!! $record->service_code !!}" value="{!! $record->price !!}" id="">
                                            </td>
                                            @endforeach
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    
                        
                    </div>
                    <div class="tab-pane fade in" id="tab3">

                        <div class="container-fluid" style="padding: 10px;">
                            <div class="row codfox_container">
                                <div class="col-md-12 box no-border no-margin">
                                    <table class="table table-bordered table_sm no-margin no-border">
                                        <thead>
                                            <tr class="table_header_bg">
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody class="bill3_body">
                                            @foreach($bill3 as $row)
                                            <tr>
                                            @foreach($row as $record)
                                            <td style="width: 3%; padding:0 0 0 0; text-align: center;">
                                                <input name="" data-detail-id="{!! $record->id !!}" data-head-id="{!! $record->head_id !!}" data-bill-type="3" data-service-code="{!! $record->service_code !!}" class="deleteID bill_type_check" type="checkbox" id="bill-type-three-{!! $record->service_code !!}" onclick="checkBoxClick('{!! $record->service_code !!}', '1');" value="{!! $record->service_code !!}">
                                            </td>
                                            <td style="width: 21%;" onclick="selectOTItem('bill-type-three-{!! $record->service_code !!}','0');" class="package">
                                                <span>{!! $record->service_desc !!}</span>
                                            </td>
                                            <td style="width: 10%;">
                                                <input type="number" min="0" max="1000000" class="form-control amount item-amount-{!! $record->service_code !!}" style="text-align: right;" name="" autocomplete="off" onblur="calculateTotal();" id="field_selling_price-bill-type-three-{!! $record->service_code !!}" value="{!! $record->price !!}" id="">
                                            </td>
                                            @endforeach
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                    <div class="tab-pane fade in" id="tab4">

                        <div class="container-fluid" style="padding: 10px;">
                            <div class="row codfox_container">
                            <div class="col-md-4 no-margin">
                                <div class="mate-input-box">
                                    <?php
                                    $billtags = \ExtensionsValley\Purchase\CommonInvController::getAllOTBillTags([4]);
                                    ?>
                                    <label for="" class="header_label">Bill Tag</label>
                                    {!! Form::select('bill_type_4',$billtags, null,['class' => 'form-control select2 abc', 'placeholder' => 'Bill Tag', 'onchange'=>'fetchAddtionalSurgeryCharge();', 'title' => 'Bill Tag','id' => 'ot_bill_tag_four','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                </div>
                            </div>
                            @if($ot_bill_config_details['discount_percentage_status'] == "TRUE")
                            <div class="col-md-3 no-margin">
                                <div class="mate-input-box">
                                    <label for="">Discount Percentage</label>
                                    <div class="clearfix"></div>
                                    <input type="number" min="0" max="100" autocomplete="off" class="form-control" id="discount_percentage_status" name="discount_percentage_status" value="20">
                                </div>
                            </div>
                            @endif

                                <div class="col-md-12 box no-border no-margin" >
                                
                                    <table class="table table-bordered table_sm no-margin no-border">
                                        <thead>
                                            <tr class="table_header_bg">
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Bill Amount</th>
                                                <th class="text-center">Discount Amount</th>
                                                <th class="text-center">Net Amount</th>
                                                <th class="text-center" colspan="4"></th>

                                            </tr>
                                        </thead>
                                        <tbody class="bill4_body">
                                            
                                            
                                            @foreach($bill4 as $row)
                                            @foreach($row as $record)
                                            <tr>
                                            <td style="width: 3%; padding:0 0 0 0; text-align: center;">
                                                <input name="" data-detail-id="{!! $record->id !!}" data-head-id="{!! $record->head_id !!}" data-bill-type="4" data-service-code="{!! $record->service_code !!}" class="deleteID bill_type_check" type="checkbox" id="bill-type-four-{!! $record->service_code !!}" onclick="checkBoxClick('{!! $record->service_code !!}', '1');" value="{!! $record->service_code !!}">
                                            </td>
                                            <td style="width: 21%;" onclick="selectOTItem('bill-type-four-{!! $record->service_code !!}','0');" class="package">
                                                <span>{!! $record->service_desc !!}</span>
                                            </td>
                                            <td style="width: 10%;">
                                                <input type="number" min="0" class="form-control bill_amount amount item-amount-{!! $record->service_code !!}"   @if($record->service_code == $ot_bill_config_details['surgery_charge_service_code']) onkeyup=fetchAddtionalSurgeryCharge(); @endif ,  style="text-align: right;" name="" autocomplete="off" onblur="calculateTotal();"  value="{!! $record->price !!}" id="">
                                            </td>
                                            <td style="width: 10%;">
                                                <input type="text" readonly="true" disabled="true" class="form-control discount_percentage_amount"  style="text-align: right;" name="" autocomplete="off"  value="">
                                            </td>
                                            <td style="width: 10%;">
                                                <input type="text" readonly="true" disabled="true" class="form-control bill_wise_net_amount" id="field_selling_price-bill-type-four-{!! $record->service_code !!}"  style="text-align: right;" name="" autocomplete="off"  value="">
                                            </td>
                                            </tr>
                                            @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                    <div class="tab-pane fade in" id="tab5">
                        
                        <div class="container-fluid" style="padding: 10px;">
                            <div class="row codfox_container">

                            <div class="col-md-4 no-margin">
                                <div class="mate-input-box">
                                    <?php
                                    $billtags = \ExtensionsValley\Purchase\CommonInvController::getAllOTBillTags([5]);
                                    ?>
                                    <label for="" class="header_label">Bill Tag</label>
                                    {!! Form::select('bill_type_5',$billtags, null,['class' => 'form-control select2 abc','placeholder' => 'Bill Tag','title' => 'Bill Tag','id' => 'ot_bill_tag_five','style' => 'color:#555555; padding:2px 12px; ']) !!}
                                </div>
                            </div>

                                <div class="col-md-12 box no-border no-margin" >
                                
                                    <table class="table table-bordered table_sm no-margin no-border">
                                        <thead>
                                            <tr class="table_header_bg">
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody id="fixTableOTBills5" class="bill5_body">
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane fade in" id="tab6">
                        
                        <div class="container-fluid" style="padding: 10px;">
                            <div class="row codfox_container">

                            <div class="col-md-4 no-margin">
                                <div class="mate-input-box">
                                    <?php
                                    $billtags = \ExtensionsValley\Purchase\CommonInvController::getAllOTBillTags([6]);
                                    ?>
                                    <label for="" class="header_label">Bill Tag</label>
                                    {!! Form::select('bill_type_6',$billtags, $selected,['class' => 'form-control select2 abc','placeholder' => 'Bill Tag','title' => 'Bill Tag','id' => 'ot_bill_tag_six','style' => 'color:#555555; padding:2px 12px;','disabled'=> 'disabled' ]) !!}
                                </div>
                            </div>

                                <div class="col-md-12 box no-border no-margin" >
                                
                                    <table class="table table-bordered table_sm no-margin no-border">
                                        <thead>
                                            <tr class="table_header_bg">
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Item Description</th>
                                                <th class="text-center">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody class="bill6_body">
                                           
                                            
                                            @foreach($bill6 as $row)
                                            <tr>
                                            @foreach($row as $record)

                                            <td style="width: 3%; padding:0 0 0 0; text-align: center;">
                                                <input name="" data-detail-id="{!! $record->id !!}" data-head-id="{!! $record->head_id !!}" data-bill-type="6" data-service-code="{!! $record->service_code !!}" class="deleteID bill_type_check" type="checkbox" id="bill-type-six-{!! $record->service_code !!}" onclick="checkBoxClick('{!! $record->service_code !!}', '1');" value="{!! $record->service_code !!}">
                                            </td>
                                            <td style="width: 21%;" onclick="selectOTItem('bill-type-six-{!! $record->service_code !!}','0');" class="package">
                                                <span>{!! $record->service_desc !!}</span>
                                            </td>
                                            <td style="width: 10%;">
                                                <input type="number" min="0" max="1000000" class="form-control amount item-amount-{!! $record->service_code !!}" style="text-align: right;" name="" autocomplete="off" onblur="calculateTotal();" id="field_selling_price-bill-type-six-{!! $record->service_code !!}" value="{!! $record->price !!}" id="">
                                            </td>
                                            @endforeach
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</div>


<!-- View OT Bills Modal -->
<div id="ot_bills_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width:87%; ">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header modal_header_dark_purple_bg modal-header-sm">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">View Bills</h4>
        </div>
        <div class="modal-body">
            <div class="row codfox_container">
                    <div class="col-md-12 padding_sm">
                        <div class="col-md-12 no-padding">
                            <div class="box no-border" >
                                <div class="box-body clearfix">
                                    <div class="col-md-2 padding_sm">
                                        <label for="">Bill No.</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control bill_no_search" data-bill-head-id="" name="bill_no_search" value="">
                                    </div>
                                    <div class="col-md-3 padding_sm">
                                        <label for="">UHID</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control patient_uhid_search" name="uhid" value="" autocomplete="off">
                                        <div class="ajaxSearchBox OpAjaxDivSearch" style="z-index:999; overflow: scroll;"></div>
                                    </div>

                                    <!-- <div class="col-md-3 padding_sm">
                                        <label for="">Patient Name</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control patient_name_search" name="patient_name_search" value="" disabled>
                                    </div> -->
                                    
                                    <div class="col-md-2 padding_sm">
                                        <label for="">From Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" data-attr="date" class="form-control from_date" name="from_date" value="">
                                    </div>

                                    <div class="col-md-2 padding_sm">
                                        <label for="">To Date</label>
                                        <div class="clearfix"></div>
                                        <input type="text" data-attr="date" class="form-control to_date" name="to_date" value="">
                                    </div>
                                    
                                
                                    <div class="col-md-1 padding_sm">
                                        <label for="">&nbsp;</label>
                                        <div class="clearfix"></div>
                                        <button class="btn btn-block btn-primary searchBills"><i class="fa fa-search"></i> Search </button>
                                    </div>
                                    <div class="col-md-1 padding_sm">
                                        <label for="">&nbsp;</label>
                                        <div class="clearfix"></div>
                                        <button class="btn btn-block btn-primary clearSearch"><i class="fa fa-refresh"></i> Clear </button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row codfox_container searchBillContent">

                </div>
            </div>
            
        <div class="modal-footer">
            <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Close</button>
        </div>
    </div>

    </div>
</div>
<!-- View OT Bills Modal -->


@stop
@section('javascript_extra')

<script src="{{asset("packages/extensionsvalley/emr/toastr/toastr.min.js")}}"></script>
<script src="{{asset("packages/extensionsvalley/emr/js/otbilling.js?version=".env('APP_JS_VERSION', '0.0.1'))}}"></script>


<script type="text/javascript">

</script>
@endsection
