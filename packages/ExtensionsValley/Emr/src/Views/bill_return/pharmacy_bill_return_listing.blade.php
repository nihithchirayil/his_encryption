<div class="col-md-12 theadscroll theadfix_wrapper" style="padding: 0px;margin-top: -5px;height:244px">
        <table class="table table-contensed table_sm" id="main_list_data">
            @if (count($bill_list)>0)
            @foreach ($bill_list as $data)
            <tr style="cursor: pointer;background-color:aqua" onclick="getBillDetails('{{ $data->bill_no }}')">

                <td class="common_td_rules bill_no" width="35%">{{ $data->bill_no }}</td>
                <td class="common_td_rules billheadid" style="display: none">{{ $data->billheadid }}</td>
                <td class="common_td_rules bill_tag" style="display: none">{{ $data->bill_tag }}</td>
                @if ($data->bill_no != ' ')
                    <td class="td_common_numeric_rules bill_date" width="25%">{{ date('M-d-Y', strtotime($data->bill_date)) }}</td>
                @else
                    <td class="common_td_rules" width="25%"></td>
                @endif
                <td class="td_common_numeric_rules net_amount" width="15%">{{ number_format($data->net_amount_wo_roundoff, 2) }}</td>
                <td class="common_td_rules " width="25%">{{ $data->bill_tagname }}</td>
            </tr>
        @endforeach  
            @else
            <tr>
                <td colspan="11" style="text-align: center">
                    No Result Found
                </td>
            </tr> 
            @endif
           
        </table>
</div>
