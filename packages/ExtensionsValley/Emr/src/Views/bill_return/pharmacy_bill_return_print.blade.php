<!-- .css -->
<style>
    #doctorHeadClass p:first-child{
        margin: 0 !important;
        padding: 0 !important;
    }

    .bold{
font-weight: 100 !important;
    }
    .number_class {
        text-align: right;
    }
    @media print
    {
        table{ page-break-after:auto; }
        tr    { page-break-inside:auto; page-break-after:auto }
        td    { page-break-inside:auto; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
    }

</style>
<style type="text/css" media="print">
    table {
        font-size : 12px;
    }
    @page{
        margin-left:15px;
        margin-right:15px;
        margin-top:40px;
        margin-bottom:35px;
    }

</style>


<div class="">
    <div class="col-md-12">
        <table style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;margin-top:15px; " width="100%"  border="1" cellspacing="0" cellpadding="0">
            <thead>

                @if((string)$hospital_header_status == "1")
                <tr>
                    <th colspan="10" >
                     {!!$hospitalHeader!!}
                    </th>
                </tr>
                @endif
                <tr>
                    <th colspan="9"  style="font-size: 14px;padding: 11px !important;" >
                        <center>
                            <strong>{{ strtoupper($patient_details[0]->billtagdesc) }} RETURN {{ @$is_duplicate ? $is_duplicate : ' ' }}</strong>
                        </center>
                    </th>
                </tr>
                <tr>
                    <th colspan="9" style="@if((string)$hospital_header_status == '0') margin-top; 3cm; @endif padding-bottom: 11px !important;" >
                        <table style="border-color:rgb(247, 246, 246) !important;" width="100%" border="1" cellspacing="0" cellpadding="0">
                            <thead class="patient_head" style="display: table-header-group;">
                            @if($printMode == "1")
                            <tr>
                                <td><strong>Patient Name</strong></td>
                                <td class="bold">:{{ $patient_details[0]->patient_name }}</td>
                                <td><strong>Gender/Age</strong></td>
                                <td  class="bold">
                                    :{{ $patient_details[0]->gender_desc }}/{{ $patient_details[0]->age }}
                                </td>
                            </tr>
                            <tr>
                                <td><strong>UHID</strong></td>
                                <td  class="bold">:{{ $patient_details[0]->uhid }}</td>
                                <td><strong>Address</strong></td>
                                <td>:{{ $patient_details[0]->address }}</td>
                            </tr>
                            <tr>
                                <td><strong>Location</strong></td>
                                <td  class="bold">:{{ $location_name }}</td>
                                <td><strong>PAYMENT TYPE</strong></td>
                                <td  class="bold">:{{ $patient_details[0]->paymenttypedesc }}
                                </td>
                            </tr>

                            <tr>
                                <td><strong>Bill Nunmber</strong></td>
                                <td  class="bold">:{{ $patient_details[0]->bill_no }}</td>
                                <td><strong>Bill Date</strong></td>
                                <td  class="bold">:{{ $patient_details[0]->bill_created_on }}</td>
                            </tr>
                            @else
                                <tr>
                                    <td><strong>Patient Name</strong></td>
                                    <td  class="bold">:{{ $patient_details[0]->patient_name }}</td>
                                    <td><strong>Gender/Age</strong></td>
                                    <td  class="bold">
                                        :{{$patient_details[0]->gender_desc }}/{{ $patient_details[0]->age }}
                                    </td>
                                    <td><strong>UHID</strong></td>
                                    <td  class="bold">:{{ $patient_details[0]->uhid }}</td>
                                    <td><strong>Address</strong></td>
                                    <td  class="bold">:{{ $patient_details[0]->address }}</td>
                                </tr>
                              
                                <tr>
                                    <td><strong>Location</strong></td>
                                    <td  class="bold">:{{ $location_name }}</td>
                                    <td><strong>Payment Type</strong></td>
                                    <td  class="bold">:{{ $patient_details[0]->paymenttypedesc  }}
                                    <td><strong>Bill Number</strong></td>
                                    <td  class="bold">:{{ $patient_details[0]->bill_no }}</td>
                                    <td><strong>Bill Date</strong></td>
                                    <td  class="bold">:{{ $patient_details[0]->bill_created_on }}</td>
                                </tr>
                            @endif
                               
                            </thead>
                        </table>
                    </th>
                </tr>
                <tr>
                    <th colspan="9">
                        <table
                            style="border-color:rgb(247, 246, 246) !important;border-colapse:colapse;font-size: 12px;"
                            width="100%" border="1" cellspacing="0" cellpadding="0">
                            <thead style="background-color: cadetblue;color:white;" >
                                <tr>
                                    <th width="2%"><strong>#</strong></th>
                                    <th width="33%"><strong>Particular</strong></th>
                                    <th width="10%"><strong>HSN</strong></th>
                                    <th width="10%"><strong>Batch No.</strong></th>
                                    <th width="15%"><strong>Exp. Date</strong></th>
                                    <th width="10%"><strong>Qty</strong></th>
                                    {{-- <th width="10%"><strong>Tax%</strong></th> --}}
                                    <th width="10%"><strong>Price</strong></th>
                                    <th width="10%"><strong>Amount</strong></th>
                                </tr>
                            </thead>
                        </table>
                    </th>
                </tr>
            </thead>

            <tbody>
                @foreach ($patient_details as $data)
                    <tr style="border-bottom: 1px solid #ccc;">
                        <td width="2%" class="number_class">{{ $loop->iteration }}</td>
                        <td width="33%" style="text-align:left">{{ $data->item_desc }}</td>
                        <td width="10%"></td>
                        <td width="10%">{{ $data->batch }}</td>
                        <td width="15%" style="text-align:left;">{{date('d-M-y',strtotime($data->exp_date))}}</td>
                        <td width="10%" class="number_class">{{ $data->quantity }}</td>
                        {{-- <td width="10%" class="number_class">{{ $data->tax_rate }}</td> --}}
                        <td width="10%" class="number_class">{{ $data->selling_price }}</td>
                        <td width="10%" class="number_class">{{ $data->total_amount }}</td>
                    </tr>
                  
                @endforeach
                <tr>
                    <th colspan="9">
                           
                            <table style="font-size:14px;float:left">
                                <thead>
                                    <tr>
                                        <th>
                                            <strong>Amount To Return</strong>:<strong>{{ round($total) }}/-</strong>
                                        </th>
                                     
                                    </tr>
                                </thead>
                                 
                            </table>
                               

                    </th>
                </tr>
                <tr>
                    <th colspan="9">
                           
                            <table style="font-size:14px;float:right">
                                <thead>
                                    <tr>
                                        <th>
                                            <strong>[Signature]</strong>
                                        </th>
                                    </tr>
                                </thead>
                                 
                            </table>
                               

                    </th>
                </tr>

                <tr>
                    <th colspan="9">
                        <div class="footer">
                            @php
                                $hed_crt = $patient_details[0]->headcreated_at;
                            @endphp
                            <p style="font-size: 12px;border-top:1px solid;margin: 4px;">Return Bill Created
                                By:{{ $patient_details[0]->createduser }}, at
                                {{ date('d-m-Y h:i A', strtotime($hed_crt)) }}
                            </p>
                            <p style="font-size: 12px;margin: 0px;">Return Bill Printed
                                By:{{ $user }}-Print
                                Date:{{ date('d-m-Y') }}-Printed Location:{{ strtoupper($location_name) }}</p>
                        </div>
                    </th>
                </tr>

            </tbody>

        </table>
    </div>
</div>
