<div class="col-md-12 theadscroll " style="padding: 0px;margin-top: -5px;height:164px">
    <table class="table table-contensed table_sm theadfix_wrapper" id="bill_detail_container">
        <input type="hidden" id="head_return_type" value="{{ @$res[0]->bill_no_type ? $res[0]->bill_no_type : ' ' }}">
        <input type="hidden" id="head_uhid" value="{{ @$res[0]->uhid ? $res[0]->uhid : ' ' }}">
        <input type="hidden" id="head_visit_id" value="{{ @$res[0]->visit_id ? $res[0]->visit_id : '0' }}">
        <input type="hidden" id="head_counter_id" value=-1>
        <input type="hidden" id="head_location" value="{{ @$res[0]->location ? $res[0]->location : ' ' }}">
        <input type="hidden" id="head_payment_type" value="{{ @$res[0]->payment_type ? $res[0]->payment_type : ' ' }}">
        @php
            $total_item_discount = 0;
        @endphp
        <tbody id="detail_search">

            @foreach ($res as $data)
                @php
                    $net_total = $data->net_amount_wo_roundoff;
                    $total_amount = $data->selling_price_with_tax * $data->qty ;
                    $exp_date = ' ';
                    if ($data->expiry != ' ') {
                        $exp_date = date('M-d-y', strtotime($data->expiry));
                    }
                    $total_item_discount = floatval($data->discount_amount + $data->pricing_discount_amount + $data->bill_discount_split);
                    $item_total = floatval($data->net_amount - $data->bill_discount_split);
                    $single_item_amt = floatval($item_total / $data->detail_qty);
                    $total_return_amt = floatval($single_item_amt * $data->qty);
                    
                @endphp
                <tr @if($data->returnable_ind == 1) class="returnable_item" @endif style="cursor: pointer;background-color:aqua"
                    onclick="@if($data->returnable_ind == 1) addToReturn('{{ $data->net_amount }}',{{ $data->tax_amount }},{{ $data->selling_price_with_tax }},{{ $data->tax_percent }},{{ $data->other_tax_per }},{{ $data->other_tax_amnt }},{{ $data->bill_detail_id }},{{ $data->billheadid }},{{ $data->item_id }},{{ $data->unit_tax_amount }},'{{ $exp_date }}','{{ $data->batch }}','{{ $data->bill_no }}','{{ $data->item_desc }}',{{ $data->actual_selling_price }},{{ $data->remainingqty }},{{ $net_total }},{{ $data->bill_discount_split }}, {{ $data->detail_qty }}) @endif">
                    <td class="common_td_rules" width="15%">{{ $data->bill_no }}</td>
                    <td class="common_td_rules" width="20%">{{ $data->item_desc }}</td>
                    @if($type==1)
                        <td class="common_td_rules" width="10%">{{ $data->batch }}</td>
                    @else
                    <td class="common_td_rules" width="10%">-:-</td>
                    @endif
                    <td class="common_td_rules" width="05%"
                        id="org_qty{{ $data->item_id }}_{{ $data->billheadid }}">
                        {{ floatval($data->qty) }}</td>
                    <td class="common_td_rules list_rem_qty" width="10%"
                        id="rem_qty{{ $data->item_id }}_{{ $data->billheadid }}">{{ $data->remainingqty }}</td>
                    <td class="common_td_rules" width="10%">{{ $data->actual_selling_price }}</td>
                    <td class="common_td_rules" width="10%">{{ $total_item_discount }}</td>
                    <td class="td_common_numeric_rules" id="replace_amt{{ $data->item_id }}_{{ $data->billheadid }}" width="10%">{{floatval($total_return_amt)}}</td>

                    <td class="td_common_numeric_rules" style="display: none">{{ $exp_date }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->billheadid }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->item_id }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->unit_tax_amount }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->selling_price_with_tax }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->tax_percent }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->other_tax_per }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->other_tax_amnt }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->bill_detail_id }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->tax_amount }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->bill_discount_split }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->net_amount }}</td>
                    <td class="td_common_numeric_rules" style="display: none">{{ $data->detail_qty }}</td>
                    @if($data->returnable_ind == 1)
                    <td class="td_common_numeric_rules"
                        onclick="addToReturn({{ $data->net_amount }},{{ $data->tax_amount }},{{ $data->selling_price_with_tax }},{{ $data->tax_percent }},{{ $data->other_tax_per }},{{ $data->other_tax_amnt }},{{ $data->bill_detail_id }},{{ $data->billheadid }},{{ $data->item_id }},{{ $data->unit_tax_amount }},'{{ $exp_date }}','{{ $data->batch }}','{{ $data->bill_no }}','{{ $data->item_desc }}',{{ $data->actual_selling_price }},{{ $data->remainingqty }},{{ $net_total }},{{ $data->bill_discount_split }}, {{ $data->detail_qty }})"
                        width="05%"><i class="fa fa-plus"></i></td>
                    @else
                    <td></td>
                    @endif
                </tr>
                @php $total_item_discount = 0; @endphp
            @endforeach
        </tbody>

    </table>
</div>
<script></script>
