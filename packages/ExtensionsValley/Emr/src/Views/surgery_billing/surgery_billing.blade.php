@extends('Emr::emr.page')
@section('content-header')

    <!-- Navigation Starts-->
    @include('Dashboard::dashboard.partials.headersidebar')
    <!-- Navigation Ends-->

@stop
@section('css_extra')
    {!! Html::style('packages/extensionsvalley/default/css/style_new.css') !!}
    <link href="{{ asset('packages/extensionsvalley/dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/emr-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/nurse-module.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/css/wizard.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/custom_input.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/emr/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/purchase-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/extensionsvalley/purchase/default/css/report.css') }}" rel="stylesheet">
@endsection
@section('content-area')
<style>
    .mate-input-box{
        height: 50px !important;
        padding: 20px 4px 4px 4px !important;
    }
    .close_btn_service {
        position: absolute;
        z-index: 99;
        color: #FFF !important;
        background: #000;
        right: -11px;
        top: -1px;
        border-radius: 100%;
        text-align: center;
        width: 20px;
        height: 20px;
        line-height: 20px;
        cursor: pointer;
    }
    .services-row-listing-search-data tr{
        cursor: pointer;
    }
</style>
    <!-- page content -->
    <div class="right_col">
        <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
        <input type="hidden" id="c_token" value="{{ csrf_token() }}">
        <input type="hidden" id="surgery_id" value="0">

        <div class="row" style="margin-bottom: 10px"> </div>
        <div class="row codfox_container" style="margin-top: -19px;margin-bottom: -36px;">
            <div class="col-md-12 padding_sm">
                <h4 class="green pull-right"><strong> <?= $title ?> </strong></h4>
            </div>
            <div class="clearfix"></div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-12 padding_sm">

                        <div id="msform">
                            <ul id="progressbar" style="cursor: pointer;margin: 3px;" class="nav nav-tabs" role="tablist">
                                <li class="nav-item" id="surgery_details_li">
                                    <a class="nav-link" id="surgery_details-tab" href="#surgery_details_tab"
                                        role="tab" aria-controls="surgery_details" aria-selected="true"><strong><i
                                                class="fa fa fa-scissors"></i> Surgery
                                            Details</strong></a>
                                </li>
                                <li class="nav-item" id="instrument_list_li">
                                    <a class="nav-link" id="instrument_list-tab" href="#instrument_list_tab"
                                        role="tab" aria-controls="instrument_list" aria-selected="true"><strong><i
                                                class="fa fa-wrench"></i> Instruments
                                            list</strong></a>
                                </li>
                                <li class="nav-item" id="equipment_list_li">
                                    <a class="nav-link" id="equipment_list-tab" href="#equipment_list_tab" role="tab"
                                        aria-controls="equipment_list" aria-selected="true"><strong><i
                                                class="fa fa-thermometer-full"></i> Equipment </strong></a>
                                </li>
                                <!-- <li class="nav-item" id="medicine_pack_list_li">
                                    <a class="nav-link" id="medicine_pack_list-tab" href="#medicine_pack_list_tab"
                                        role="tab" aria-controls="medicine_pack_list" aria-selected="true"><strong><i
                                                class="fa fa-medkit"></i> Medicine </strong></a>
                                </li>
                                <li class="nav-item" id="consumables_and_disposables_list_li">
                                    <a class="nav-link" id="consumables_and_disposables_list-tab"
                                        href="#consumables_and_disposables_list_tab" role="tab"
                                        aria-controls="consumables_and_disposables_list" aria-selected="true"><strong><i
                                                class="fa fa-database"></i> Consumables </strong></a>
                                </li> -->
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane  active" id="surgery_details_tab" role="tabpanel"
                                    aria-labelledby="nav-surgery-tab">
                                    <div class="form-card">
                                        <div class="box no-border no-margin">
                                            <div class="box-body"
                                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;height: 540px;">
                                                @include('Emr::surgery_billing.surgery_bill')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="instrument_list_tab" role="tabpanel"
                                    aria-labelledby="nav-instrument-tab">
                                    <div class="form-card">
                                        <div class="box no-border no-margin">
                                            <div class="box-body"
                                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;height: 500px;">
                                                @include('Emr::surgery_billing.instrumental_bill')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="equipment_list_tab" role="tabpanel"
                                    aria-labelledby="nav-equipment-tab">
                                    <div class="form-card">
                                        <div class="box no-border no-margin">
                                            <div class="box-footer"
                                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #0c9e61;height: 500px;">
                                                @include('Emr::surgery_billing.equipmental_bill')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="tab-pane fade" id="medicine_pack_list_tab" role="tabpanel"
                                    aria-labelledby="nav-medicine_pack-tab">
                                    <div class="form-card">
                                        <div class="box no-border no-margin">
                                            <div class="box-body"
                                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;height: 500px;">
                                                @include('Emr::surgery_billing.medicine_bill')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="consumables_and_disposables_list_tab" role="tabpanel"
                                    aria-labelledby="nav-consumables_and_disposables-tab">
                                    <div class="form-card">
                                        <div class="box no-border no-margin">
                                            <div class="box-body"
                                                style="padding: 5px; box-shadow: 0px 1px #ccc; border: 2px solid #CED9FF;height: 500px;">
                                                @include('Emr::surgery_billing.consumables_bill')
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('javascript_extra')
    <script
        src="{{ asset('packages/extensionsvalley/emr/toastr/toastr.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script
        src="{{ asset('packages/extensionsvalley/default/js/loadingoverlay.min.js?version=' . env('APP_JS_VERSION', '0.0.1')) }}">
    </script>
    <script src="{{ asset('packages/extensionsvalley/emr/js/surgery_bill.js') }}"></script>
    <script type="text/javascript">
        var surgery_request_id = '{{$surgery_request_id}}';
        window.surgery_request_id = surgery_request_id;

        $(window).load(function(){
            fetchSurgeryRequestDetails(surgery_request_id);
        })
        
    </script>
@endsection
