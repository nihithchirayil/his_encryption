<div class="col-md-12 padding_sm" style="margin-top: 10px;">
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label for="">Consumables Search</label>
            <div class="clearfix"></div>
            <input class="form-control" value="" autocomplete="off" type="text" id="consumables_search">
            <div id="consumables_serachAjaxDiv" class="ajaxSearchBox"
                style="width: 100%; position: absolute; margin-top: 21px;">
                <button type="button" class="btn btn-dark close-btn">X</button>
            </div>
            <input class="filters" value="" type="hidden" value="" id="consumables_search_hidden">
        </div>
    </div>

</div>
<div class="col-md-12 padding_sm theadscroll" style="height: 370px;" id="consumables_search">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
        style="font-size: 12px;">
        <thead>
            <tr class="common_table_header" style="color:white;">
                <th style="color:white !important;" class="common_td_rules" width="5%">
                    Sl.No.
                </th>
                <th style="color:white !important;" class="common_td_rules" width="60%">
                    Consumables Name
                </th>
                <th style="color:white !important;" class="common_td_rules" width="10%">
                    Qty
                </th>
                <th style="color:white !important;" class="common_td_rules" width="10%">
                    Price
                </th>
                <th style="color:white !important;" class="common_td_rules" width="10%">
                    Total
                </th>
                <th width="5%"> <i class="fa fa-trash"></i> </th>
            </tr>
        </thead>
        <tbody id="consumableslist">
        </tbody>

    </table>
</div>
<div class="col-md-12 padding_sm" style="margin-top: -22px;">
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" style="padding: 5px 5px;" onclick="saveSurgeryIndent()" class="btnNext btn-block btn btn-success pull-right">
            <i class="fa fa-save"></i> Save
        </button>
    </div>
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" onclick="getPreviousTab('consumables')" style="padding: 5px 5px;"
            id="personalPreviousBtn" name="Previous" class="btnPrevious btn-block btn btn-warning "><i class="fa fa-backward"></i>
            Previous</button>
    </div>
    <div class="col-md-3 padding_sm pull-right">
        <strong for="">Approximate Total Charge : <span class="td_common_numeric_rules" id="Consumables_charge">0.0</span></strong>
    </div>
</div>
