<div class="col-md-12 padding_sm" style="margin-top: 10px;">
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label for="">Equipment Search</label>
            <div class="clearfix"></div>
            <input class="form-control" value="" autocomplete="off" type="text" id="equipment_list">
            <div id="equipment_listAjaxDiv" class="ajaxSearchBox"
                style="width: 100%; position: absolute; margin-top: 21px;">
                <button type="button" class="btn btn-dark close-btn">X</button>
            </div>
            <input class="filters" value="" type="hidden" name="equipment_list_hidden" value=""
                id="equipment_list_hidden" />
        </div>
    </div>

</div>
<div class="col-md-12 padding_sm theadscroll" style="height: 370px;" id="equipment_list">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
        style="font-size: 12px;">
        <thead>
            <tr class="common_table_header">
                <th class="common_td_rules" width="5%">
                    Sl.No.
                </th>
                <th class="common_td_rules" width="80%">
                    Equipment Name
                </th>
                <th class="common_td_rules" width="10%">Price</th>
                <th width="5%"> <i class="fa fa-trash"></i> </th>
            </tr>
        </thead>
        <tbody id="equipmentlist">
        </tbody>

    </table>
</div>
<div class="col-md-12 padding_sm">
    <div class="col-md-2 padding_sm pull-right">
        <!-- <button type="button" style="padding: 5px 5px;margin-top: 15px;" onclick="saveSurgeryEquipment('equipment')"
            name="next" id="saveSurgeryequipmentBtn" class="btnNext btn-block btn btn-success pull-right">
            <i id="saveSurgeryequipmentSpin" class="fa fa-save"></i> Save & Next <i
                class="fa fa-forward"></i></button> -->
        <button type="button" style="padding: 5px 5px;margin-top: 15px;" onclick="saveSurgeryBill()" class="btnNext btn-block btn btn-success pull-right">
            <i class="fa fa-save"></i> Save 
        </button>
    </div>
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" onclick="getPreviousTab('equipment')"
            style="padding: 5px 5px;margin-top: 15px;" id="personalPreviousBtn" name="Previous"
            class="btnPrevious btn-block btn btn-warning "><i class="fa fa-backward"></i>
            Previous</button>
    </div>
    <div class="col-md-3 padding_sm pull-right" style="margin-top:20px">
        <strong for="">Total Equipment Charge : <span class="td_common_numeric_rules" id="equipment_charge">0.0</span></strong>
    </div>
</div>
