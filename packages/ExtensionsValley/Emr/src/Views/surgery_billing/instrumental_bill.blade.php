<div class="col-md-12 padding_sm" style="margin-top: 10px;">
    <div class="col-md-4 padding_sm">
        <div class="mate-input-box">
            <label for="">Instrument List</label>
            <div class="clearfix"></div>
            <input class="form-control" value="" autocomplete="off" type="text" id="instrument_list">
            <div id="instrument_listAjaxDiv" class="ajaxSearchBox"
                style="width: 100%; position: absolute; margin-top: 21px;">
                <button type="button" class="btn btn-dark close-btn">X</button>
            </div>
            <input class="filters" value="" type="hidden" name="instrument_list_hidden" value=""
                id="instrument_list_hidden" />
        </div>
    </div>

</div>
<div class="col-md-12 padding_sm theadscroll" style="height: 370px;" id="instrument_list">
    <table id="result_data_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
        style="font-size: 12px;">
        <thead>
            <tr class="common_table_header">
                <th class="common_td_rules" width="5%">
                    Sl.No.
                </th>
                <th class="common_td_rules" width="80%">
                    Instrument Name
                </th>
                <th class="common_td_rules" width="10%">Price</th>
                <th width="5%"> <i class="fa fa-trash"></i> </th>
            </tr>
        </thead>
        <tbody id="instrumentlist">
        </tbody>

    </table>
</div>
<div class="col-md-12 padding_sm">
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" style="padding: 5px 5px;margin-top: 15px;" onclick="gotoNextTab()" class="btnNext btn-block btn btn-success pull-right">
            Next <i class="fa fa-forward"></i>
        </button>
    </div>
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" onclick="getPreviousTab('instrument')"
            style="padding: 5px 5px;margin-top: 15px;" id="personalPreviousBtn" name="Previous"
            class="btnPrevious btn-block btn btn-warning "><i class="fa fa-backward"></i>
            Previous</button>
    </div>
    <div class="col-md-3 padding_sm pull-right" style="margin-top:20px">
        <strong for="">Total Instrument Charge : <span class="td_common_numeric_rules" id="Instrument_charge">0.0</span></strong>
    </div>
</div>
