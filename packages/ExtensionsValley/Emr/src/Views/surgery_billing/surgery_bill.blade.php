<div class="col-md-12 padding_sm" style="margin-top: 10px;">
    <div class="col-md-2 padding_sm">
        <div class="mate-input-box">
            <label for="">UHID</label>
            <div class="clearfix"></div>
            <input class="form-control hidden_search filters" value="" autocomplete="off" type="text" id="op_no" name="op_no" />
            <div id="op_noAjaxDiv" class="ajaxSearchBox" style="width:295px;"></div>
            <input class="filters" value="" type="hidden" name="op_no_hidden" id="op_no_hidden">
        </div>
    </div>
    <div class="col-md-2 padding_sm">
        <div class="mate-input-box">
            <label for="">Patient Name</label>
            <div class="clearfix"></div>
            <input type="text" autocomplete="off" class="form-control"  readonly="readonly" id="patient_name" name="patient_name" value="" required>
        </div>  
    </div>
    <div class="col-md-2 padding_sm">
        <div class="mate-input-box">
            <label for="">Payment Type</label>
            <div class="clearfix"></div>
            <select class= "form-control select2" id='payment_type' >
                <option value="">Payment Type</option>
            </select>
        </div>
    </div>
    <div class="col-md-2 padding_sm">
        <div class="mate-input-box">
            <label for="surgeon">Surgeon</label>
            <div class="clearfix"></div>
            {!! Form::select('surgeon',$doctor_list, null,['class' => 'form-control select2','placeholder' => 'Surgeon','title' => 'Surgeon','id' => 'surgeon','style' => 'color:#555555; padding:2px 12px; ']) !!}
        </div>
    </div>
    <div class="col-md-2 padding_sm">
        <div class="mate-input-box">
            <label for="anaesthetist">Anaesthetist</label>
            <div class="clearfix"></div>
            {!! Form::select('anaesthetist',$doctor_list, null,['class' => 'form-control select2','placeholder' => 'Anaesthetist','title' => 'Anaesthetist','id' => 'anaesthetist','style' => 'color:#555555; padding:2px 12px; ']) !!}
        </div>
    </div>
    
</div>
<div class="col-md-12 padding_sm" style="margin-top: 10px;">
    <div class="col-md-2 padding_sm">
        <div class="mate-input-box">
            <label for="">Select Surgery</label>
            <div class="clearfix"></div>
            {!! Form::select('surgery',$surgery_list, null,['class' => 'form-control select2 doctor surgery_name','placeholder' => 'Surgery','title' => 'Surgery','style' => 'color:#555555; padding:2px 12px;' ]) !!}
        </div>
    </div>
    <div class="col-md-2 padding_sm">
        <div class="mate-input-box">
            <label for="">Surgeon Charge</label>
            <div class="clearfix"></div>
            <input class="form-control number_class surgeon_charge division_input" value="" autocomplete="off" type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
        </div>
    </div>
    <div class="col-md-1 padding_sm ">
        <label for=""></label>
        <div class="clearfix"></div>
        <button type="button" style="padding: 5px 5px;" class="getSugeryDetailsBtn btn-block btn btn-primary" onclick="getBillDivisionDetails();"> <i class="fa fa-cog"></i> Get Detail 
        </button>
    </div>
</div>

<div class="col-md-12 padding_sm" style="margin-top: 10px; height: 330px;">
    <div class="col-md-6 padding_sm" style="height: 330px;">
        <div class="box-body " style="height: 330px;">
            <div class="box-header common_table_header" style="margin-bottom:15px; ">
                <span class="padding_sm">Surgery Bill Division</span>
            </div>
            <div class="bill_division_details"></div>
        </div>
    </div>
    <div class="col-md-6 padding_sm" style="height: 330px;">
        <div class="box-body " style="height: 330px;">
            <table id="additional_services_table" class='table table-condensed table_sm table-col-bordered theadfix_wrapper'
                style="font-size: 12px;">
                <thead>
                    <tr class="common_table_header">
                        <th class="common_td_rules" width="5%">
                            Sl.No.
                        </th>
                        <th class="common_td_rules" width="70%">
                            Additional Service
                        </th>
                        <th class="common_td_rules" width="20%">Price</th>
                        <th width="5%"> 
                            <button style="position: absolute; top: 2px; right: 0;" onclick="addNewAdditionalService();" class="btn btn-primary" type="button" ><i class="fa fa-plus"></i></button>
                        </th>
                    </tr>
                </thead>
                <tbody id="additional_services_table_tbody">

                </tbody>

            </table>
        </div>
    </div>
</div>

<div class="col-md-12 padding_sm">
    <div class="col-md-2 padding_sm pull-right">
        <button type="button" style="padding: 5px 5px;margin-top: 15px;" onclick="gotoNextTab();" id="surgeryNextBtn"  class="btnNext btn-block btn btn-success pull-right">
        Next <i class="fa fa-forward"></i>
        </button>
    </div>
    <div class="col-md-3 padding_sm pull-right" style="margin-top:15px">
        <strong for="">Total Surgery Charge : <span class="td_common_numeric_rules" id="surgery_charge">0.0</span></strong>
    </div>
</div>
