<?php
namespace ExtensionsValley\Apis\Validators;

class PatientRegistrationRequest
{

    public function getRules()
    {
        return [
            'Salutation' => 'required|max:255',
            'PatientName' => 'required|max:255',
            'Gender' => 'required',
            'Dob' => 'required',
            'MaritalStatus' => 'required',
            'EmailId' => 'required|email|max:255',
            'MobileNo' => 'required|max:255',
            'Address' => 'required|max:255',
            'Country' => 'required|max:255',
            'State' => 'required|max:255',
            'District' => 'required|max:255',
            'Location' => 'required|max:255',
            'PinCode' => 'required|max:255',
        ];
    }

}
