<?php

namespace ExtensionsValley\Apis;

use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
         // Load all routes
        foreach (new \DirectoryIterator(__DIR__ . '/Routes/') as $fileInfo) {
            if (!$fileInfo->isDot()) {
                include __DIR__ . '/Routes/' . $fileInfo->getFilename();
            }
        }
        // Catching up the events
       /* foreach (new \DirectoryIterator(__DIR__ . '/Events/') as $fileInfo) {
            if (!$fileInfo->isDot()) {
                include __DIR__ . '/Events/' . $fileInfo->getFilename();
            }
        }*/
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    // public function boot()
    // {
    //     // $this->loadViewsFrom(__DIR__ . '/Views', 'Emr');

    //     // $this->publishes([
    //     //     __DIR__ . '/../public' => public_path('packages/extensionsvalley/emr'),
    //     // ]);

    //     // $this->publishes([
    //     //     __DIR__ . '/Database/migrations' => $this->app->databasePath() . '/migrations',
    //     // ], 'migrations');

    //     // $this->publishes([
    //     //     __DIR__ . '/Database/seeds' => $this->app->databasePath() . '/seeds',
    //     // ], 'seeds');
    // }
}
