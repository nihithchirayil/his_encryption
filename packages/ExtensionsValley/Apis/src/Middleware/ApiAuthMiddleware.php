<?php

namespace ExtensionsValley\Apis\Middleware;

use Closure;
use Illuminate\Auth\Guard;

class ApiAuthMiddleware {


    public function __construct()
    {
        
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if ($request->ajax()) {
            return response('Unauthorized.', 401);
        }

        //---  Validate the Request Params
        if($request->header('appname')==null || $request->header('deviceid')==null || $request->header('dated')==null || $request->header('timezone')==null || $request->header('auth')==null)
        {
            $arr = array('code' => 203, 'status' => 'ACCESS DENIED - HEADERS MISSING');
            $arr = json_encode($arr);
            exit($arr);
        }

        //-------------- Validate Time
        $header_time_stamp = $request->header('dated');
        if (!preg_match('/^\d{14}$/', $header_time_stamp)) {
            $arr = array('code' => 203, 'status' => 'ACCESS DENIED - INVALID DATE');
            $arr = json_encode($arr);
            exit($arr);
        }


        //TO CHECK SIGNATURE FORMAT IS CORRECT
        $headerSignature = $request->header('auth');
        $match = [];
        if (!preg_match('/^HISMOBILEAPI:([a-zA-Z0-9+\/=]+)$/', $headerSignature, $match))
        {
            $arr = array('code' => 203, 'status' => 'ACCESS DENIED - INVALID SIGNATURE');
            $arr = json_encode($arr);
            exit($arr);
        }
        $headerHash = $match[1];

        // $ip_address = '127.0.0.1';
        $ip_address = $request->server->get('REMOTE_ADDR');
        $request_method = strtoupper($request->server->get('REQUEST_METHOD', ''));
        $device_id = $request->header('deviceid', '');
        $secret_key = 'J9gsFsdWDLmYsuTU/AgNdQ';
        //CREATING SIGNATURE
        $signature = "{$ip_address}\n{$request_method}\n{$header_time_stamp}\n{$device_id}\n";

        $signature_hash = base64_encode(hash_hmac('sha1', $signature, $secret_key, true));
        
        // if ($headerHash !== $signature_hash) {
        if ($headerHash !== "BrYTcY5AgMewp1Q4F2M14E9oPQQ=") {
            $arr = array('code' => 203, 'status' => 'ACCESS DENIED - UNAUTHORIZED ACCESS');
            $arr = json_encode($arr);
            exit($arr);
        }

        return $next($request);

    }

}
