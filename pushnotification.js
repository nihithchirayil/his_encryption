/*var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();
redis.subscribe('test-channel', function(err, count) {
});
redis.on('message', function(channel, message) {
    console.log('Message Recieved: ' + message);
    message = JSON.parse(message);
    io.emit(channel + ':' + message.event, message.data);
});
http.listen(3000, function(){
    console.log('Listening on Port 3000');
});
*/
const server = require('http').Server();

const io = require('socket.io')(server);

const Redis = require('ioredis');

const redis = new Redis();

redis.subscribe('notify-channel');

redis.on('message', function (channel, message) {
	console.log("Here"+message);
    const event = JSON.parse(message);
    console.log('Message Recieved: ' + event.data);

    //io.emit(event.event, channel, event.data);
    io.emit(channel, event);
});

server.listen({
    port: 3000
});

